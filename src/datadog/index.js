"use strict";

var StatsD = require('hot-shots'),
	async = require('async'),
	loop_delay = require('./metrics/loop-delay'),
	cpu = require('./metrics/cpu'),
	namespace = process.env.DATADOG_NAMESPACE;

const dogstatsd = new StatsD({
	prefix: namespace,
	globalTags: [namespace]
});

function monitorSystem() {
	async.auto({
		loop_delay: loop_delay,
		cpu: cpu
	}, function(err, result){
		if(err) {
			return;
		}
		var memUsage = process.memoryUsage();
		dogstatsd.gauge('.loop_delay', result.loop_delay);
		dogstatsd.gauge('.memRss', memUsage.rss);
		dogstatsd.gauge('.memHeapTotal', memUsage.heapTotal);
		dogstatsd.gauge('.memHeapUsed', memUsage.heapUsed);

		dogstatsd.gauge('.cpuUsage', result.cpu.usage);

    	dogstatsd.increment('.statsReported');
	});
}

process.on('uncaughtException', function (err) {
  console.error(err.message, '\n', err.stack)
  dogstatsd.event(`${namespace}:uncaughtException:${err.message}`, err.stack, {
    alert_type: 'error',
    tags: ['uncaughtException', namespace]
  })
})

process.on('unhandledRejection', function (err) {
  if (err) {
    console.error(err.message, '\n', err.stack)
    dogstatsd.event(`${namespace}.unhandledRejection:${err.message}`, err.stack, {
      alert_type: 'error',
      tags: ['unhandledRejection', namespace]
    })
  }
})

process.on('beforeExit', function (exitCode) {
  console.error(Date.now(), 'Exiting with status code: ', exitCode)
  dogstatsd.event(`${namespace}.beforeExit:Exiting with status code: ${exitCode}`, '', {
    alert_type: (exitCode === 0) ? 'info' : 'error',
    tags: [`exiting-${exitCode}`, namespace]
  })
})

module.exports.startOSTracking = function(){
	setInterval(monitorSystem, 1000);
}

module.exports.apiTracker = {
	trackApiIn: function(req){
		let request = req.url, reqStartTime = Date.now(),
		requestId = request;
		dogstatsd.increment(`.REQUEST_IN`, [requestId]);
		return {
			requestId,
			reqStartTime
		};
	},
	trackApiOut: function(datadogObj, res) {
		dogstatsd.increment(`.REQUEST_OUT`, [res.status, datadogObj.requestId]);
		dogstatsd.gauge(`.REQUEST_OUT_TIME`, Date.now()-datadogObj.reqStartTime, [res.status, datadogObj.requestId]);
	}
}

module.exports.tracker = {
	increment: function(key) {
		dogstatsd.increment("." + key);
	},
	event: function(key, data, options) {
		if(typeof data === "object"){
			data = JSON.stringify(data);
		}
		dogstatsd.event(key, data, options);
	}
};
