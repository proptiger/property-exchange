"use strict";
const  osUtils = require('os-utils');


module.exports = function(cb) {
    osUtils.cpuUsage(function(v){
        cb(null, {
            usage: v
        });
    });
};