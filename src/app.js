'use strict';

var express = require('express'),
    morgan = require('morgan'),
    fs = require('fs'),
    path = require('path'),
    favicon = require('serve-favicon'),
    cookieParser = require('cookie-parser'),
    bodyParser = require('body-parser'),
    etag = require('etag'),
    glob = require('glob'),
    compression = require('compression'),
    setCacheHeader = require('services/utilService').setCacheHeader,
    connect_datadog = require('connect-datadog')({
      'response_code':true,
      'tags': ['makaan:buyer','makaan:node', 'makaan']
    });

require('es6-promise').polyfill();
Object.assign(require('marko/compiler').defaultOptions,{
    // writeToDisk: !['development','localhost'].includes(app.get('env')),
    preserveWhitespace: {
        'pre': true,
        'textarea': true,
        'style': true,
        'script': true
    },
    startTagOnly: {
        'base': true,
        'img': true,
        'br': true,
        'input': true,
        'meta': true,
        'link': true,
        'hr': true
    }
});
require('marko/node-require').install();


const ALLOWED_CACHEABLE_REQUEST_METHODS = ["GET", "HEAD", "get", "head"]

var envConfig = require('configs/envConfig'),
    routeService = require('services/routeService'),
    logger = require('services/loggerService'),
    templateLoader = require('services/templateLoader'),
    sellerFeedbackService = require('services/sellerFeedbackService'),
    datadog = require("datadog");

var app = express();

app.disable('x-powered-by');

// request logger start
var logDirectory = 'log'; // ensure log directory exists
fs.existsSync(logDirectory) || fs.mkdirSync(logDirectory)

morgan.token("x-forwarded-for", function (req, res) { return req.headers['x-forwarded-for'] })

if (app.get('env') == 'development' || app.get('env') == 'localhost') {
    app.use(morgan('dev', {
        skip: function (req, res) { return res.statusCode < 400 }
    }));
} else {
    app.use(morgan('\x1b[34mconsolidated-buyer-logs\x1b[37m  :x-forwarded-for - :remote-addr - :remote-user [:date[clf]] ":method :url HTTP/:http-version" :status :res[content-length] ":referrer" ":user-agent" - :response-time ms'));
}

// compress all requests - Runtime compression is disabled, Ahead of time compilation will be used 
//app.use(compression());

app.enable('strict routing');

var router = express.Router({
    caseSensitive: app.get('case sensitive routing'),
    strict: app.get('strict routing')
});

// webP image js 
/* app.get('/images/*',function(req,res,next){
    if(!req.headers.accept || req.headers.accept.indexOf('webp')<0){
        next();
        return;
    }
    var filename,fileType;
    fileType = path.extname(req.url);
    if(['.svg','.gif','.bmp'].indexOf(fileType)>=0 || ['.jpg','.jpeg','.png'].indexOf(fileType)<0) {
        next();
        return;
    }
    if (app.get('env') == 'development' || app.get('env') == 'localhost') {
        filename = path.join(__dirname, 'public/',req.url);
        filename = filename.substr(0,filename.indexOf('.')) + '.webp';
    } else {
        filename = path.join(__dirname, '..','dist',req.url);
        filename = filename.substr(0,filename.indexOf('.')) + '*.webp';
    }
    glob(filename,function(er,files){
        if(er){
          console.log('error');
          next();
          return;
        }
        if(files.length>0){
            res.writeHead(200, {
                "Content-Type": "image/webp;",
                "Cache-Control": "public, max-age=31536000",
                "expires": Date.now() + 1000 * 60 * 60 * 24 * 365,
                "Vary" : "Accept",
                "ETag":etag(fs.lstatSync(files[0]))
            });
            var fileStream = fs.createReadStream(files[0]);
            fileStream.pipe(res);

        } else {
            next();
            return;
        }
    })
}); */

// Middlewares
if (app.get('env') == 'development' || app.get('env') == 'localhost') {
    app.use(express.static('.tmp')); // jshint ignore:line
    app.use(express.static('src/public')); // jshint ignore:line
} else {
    var staticPath = path.join(__dirname, '..', 'dist');
    /* app.get('*.js', function (request, response, next) {
        request.url = request.url + '.br';
        response.set('Content-Encoding', 'brotli');
        next();
    }); */
    app.set('staticPath', staticPath);
    app.use(favicon(staticPath + '/favicon.ico', {
        maxAge: 60 * 60 * 24 * 365 * 1000
    }));
    app.use(express.static(staticPath, {
        maxAge: 60 * 60 * 24 * 365 * 1000,
        setHeaders: function (res, path, stat) {
            res.set('expires', Date.now() + 1000 * 60 * 60 * 24 * 365);
            if(path.match(/images\//)){
                res.set('Vary','Accept');
            }
        }
    }));
}


if (app.get('env') === 'development' || app.get('env') == 'localhost') {

    var chokidar = require('chokidar');
    var markoReload = require('marko/hot-reload');
    markoReload.enable();
    var watcher = chokidar.watch([path.join(__dirname, '/templates/'), path.join(__dirname, '/modules/'), path.join(__dirname, path.resolve('/tags/')), path.join(__dirname, path.resolve('/amp/'))]);
    watcher.on('change', function(filename) {
        if (/\.marko$/.test(filename)) {
            // Resolve the filename to a full template path:
            var templatePath = path.join(filename);
            logger.log('Marko template modified: ', templatePath);
            // Pass along the *full* template path to marko
            markoReload.handleFileModified(templatePath);
        }
    });
}

app.use(bodyParser.json());
//To set true to allow full objects i.e. nested json
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(cookieParser());

// Environment specific configs
envConfig.setConfig(app.get('env'));

app.locals.environment = app.get('env'); //environment setting

templateLoader.setup(app);

// for debug/bot mode mapping
app.use(function(req, res, next){
    let queryParams = req.query || {};
    let userAgent = req.headers['user-agent'] || '';
    req.bot = !!(queryParams.bot || ~userAgent.search(/(bot|spider)/gi));
    req.imgCount = 0;
    //setting api map always, to log in datadog for any error
    req.apiMap = {};
    req.apiTime = Date.now();
    next();
});

// Middleware for setting cache control
app.use(function(req, res, next){
    var datadogObj = datadog.apiTracker.trackApiIn(req);
    const send = res.send
    res.send = function () {
        if (ALLOWED_CACHEABLE_REQUEST_METHODS.indexOf(req.method) > -1) {
            setCacheHeader(res);
        }
        send.apply(res, Array.from(arguments));
        datadog.apiTracker.trackApiOut(datadogObj, res);
    }
    next();
})

//Use datadog middlerware of express
app.use(connect_datadog);
//Also start the os level monitoring
datadog.startOSTracking();

// Routing
routeService.setup(app, router); // handles all routing related logic

var masterDetailsService = require('services/masterDetailsService').populateAllMasterData();

var config = envConfig.getConfig();
// mongoose.connect(process.env.ANALYTICS_DB_URL, function(err) {
//     if(err) {
//         logger.info('Error while connecting mongoDB', err);
//     } else {
//         logger.info('MongoDb connected ..');
//     }
// });

module.exports = app;
