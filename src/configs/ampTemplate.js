"use strict";

//name,on,selector,request,eventId,eventCategory,eventAction
var t = require('public/scripts/common/trackingConfigService'),
    ampTrackingService = require('services/ampTrackingService'),
    utilService = require('services/utilService'),
    _ = utilService._,
    globalConfig = require('configs/globalConfig'),
    GA_TRACKING_ID = process.env.GA_TRACKING_ID,
    ampTemplate = {};
let ampAnalyticsTemplate = {
    "common": {
        "ampBreadcrumb":{
            "on": "click",
            "request": "event",
            "selector": ".breadcrumb-seo",
            "vars": {
                "eventId": "ampBreadcrumb",
                "eventCategory": t.BREADCRUMB_CATEGORY,
                "eventAction": t.CLICK_EVENT
            }
        },
        "ampSimilar":{
            "on": "click",
            "request": "event",
            "selector": ".ampSimilar-data",
            "vars": {
                "eventId": "ampSimilar",
                "eventCategory": t.SIMILAR_CATEGORY,
                "eventAction": t.INTERACTION_EVENT
            }
        },
        "ampConfigurationBuckets":{
            "on": "click",
            "request": "event",
            "selector": ".ampSimilar-properties",
            "vars": {
                "eventId": "ampSimilar",
                "eventCategory": t.CONFIGURATION_CATEGORY,
                "eventAction": t.PROPERTY_TYPE_TOGGLE_EVENT
            }
        },
        "trackPageview": {
            "on": "visible",
            "request": "pageview",
            "vars": {
                "eventId": "pageview",
                "eventLabel": "123456"
            }
        },
        "social_twevent": {
            "on": "click",
            "selector": "#twitter",
            "request": "event",
            "vars": {
                "eventId": "social-tw",
                "eventCategory": t.SHARE,
                "eventAction": t.SHARED,
                "eventLabel": "social_twitter"
            }
        },
        "social_fbevent": {
            "on": "click",
            "selector": "#facebook",
            "request": "event",
            "vars": {
                "eventId": "social-fb",
                "eventCategory": t.SHARE,
                "eventAction": t.SHARED,
                "eventLabel": "social_facebook"
            }
        },
        "social_gpevent": {
            "on": "click",
            "selector": "#gplus",
            "request": "event",
            "vars": {
                "eventId": "social-gp",
                "eventCategory": t.SHARE,
                "eventAction": t.SHARED,
                "eventLabel": "social_google+"
            }
        },
        "social_emailevent": {
            "on": "click",
            "selector": "#email",
            "request": "event",
            "vars": {
                "eventId": "social-gp",
                "eventCategory": t.SHARE,
                "eventAction": t.SHARED,
                "eventLabel": "social_email"
            }
        },
        "social_ptevent": {
            "on": "click",
            "selector": "#pinterest",
            "request": "event",
            "vars": {
                "eventId": "social-gp",
                "eventCategory": t.SHARE,
                "eventAction": t.SHARED,
                "eventLabel": "social_ptevent"
            }
        }
    },
    "serp":{
        "trackExperiments": {
            "on": "visible",
            "selector": "#experimentDummyImg",
            "request": "event",
            "vars": {
                "eventId": "TrackExperimentVariations",
                "eventCategory": t.QUICK_FILTER_CATEGORY,
                "eventAction": "Experiment",
                "eventLabel": "VARIANT(quick-filters)"
            }
        },
        "ampSerpFilter": {
            "on": "click",
            "selector": "#filterSearch",
            "request": "event",
            "vars": {
                "eventId": "ampSerpFilter",
                "eventCategory": t.FILTER_CATEGORY,
                "eventAction": t.FILTER_USED
            }
        },
        "ampSerpQuickFilterClick": {
            "on": "click",
            "selector": ".quick-filter .quick-filter-options a",
            "request": "event",
            "vars": {
                "eventId": "ampSerpQuickFilterClick",
                "eventCategory": t.QUICK_FILTER_CATEGORY,
                "eventAction": t.QUICK_FILTER_USED,
                "eventLabel": "${eventLabel}"
            }
        },
        "ampSerpQuickFilterSeen": {
            "on": "visible",
            "selector": ".quick-filter-img",
            "request": "event",
            "vars": {
                "eventId": "ampSerpQuickFilterSeen",
                "eventCategory": t.QUICK_FILTER_CATEGORY,
                "eventAction": t.CARD_SEEN,
                "eventLabel": "${eventLabel}"
            }
        },
        "trackDeficitListingSeen":{
            "on": "visible",
            "selector": "#nearby_1",
            "request": "event",
            "vars": {
                "eventId": "ampLead",
                "eventCategory": t.AMP_DEFICIT_SELLER_LISTING,
                "eventAction": "Nearby First Listing Seen",
                "eventLabel": "${eventLabel}"
            }
        },
        "trackSecondDeficitListingSeen":{
            "on": "visible",
            "selector": "#nearby_2",
            "request": "event",
            "vars": {
                "eventId": "ampLead",
                "eventCategory": t.AMP_DEFICIT_SELLER_LISTING,
                "eventAction": "Nearby Second Listing Seen",
                "eventLabel": "${eventLabel}"
            }
        },
        "trackDeficitListingClick": {
            "on": "click",
            "selector": ".js-deficit-listing",
            "request": "event",
            "vars": {
                "eventId": "ClickDeficitListing",
                "eventCategory":t.AMP_DEFICIT_SELLER_LISTING,
                "eventAction":t.CLICK_EVENT,
                "eventLabel":"${eventLabel}"
            }
        },
        "trackDeficitListingConnectNowClick":{
            "on": "click",
            "selector": ".connect-top-sellers.js-deficit-listing",
            "request": "event",
            "vars": {
                "eventId": "ampLead",
                "eventCategory": t.AMP_DEFICIT_SELLER_LISTING,
                "eventAction": "Lead Form Open",
                "eventLabel": "${eventLabel}"
            }
        },
        "trackDeficitProjectSeen":{
            "on": "visible",
            "selector": "#deficitProject",
            "request": "event",
            "vars": {
                "eventId": "SeenDeficitProject",
                "eventCategory": t.DEFICIT_SELLER_PROJECT,
                "eventAction": t.CARD_SEEN,
                "eventLabel": "${eventLabel}"
            }
        },
        "trackDeficitProjectClick":{
            "on": "click",
            "selector": ".track-deficit",
            "request": "event",
            "vars": {
                "eventId": "ClickDeficitProject",
                "eventCategory": t.DEFICIT_SELLER_PROJECT,
                "eventAction": t.CARD_CLICK,
                "eventLabel": "${eventLabel}"
            }
        },
        "ampSerpSelectBanner": {
            "on": "click",
            "selector": ".ms-banner-warp",
            "request": "event",
            "vars": {
                "eventId": "ClickSelectBanner",
                "eventCategory": t.SELECT_VOUCHER_REDEMPTION_CATEGORY,
                "eventAction": t.CLICK_EVENT,
                "eventLabel":"Serp_Select_Banner"
            }
        },
        "ampSerpCardImage": {
            "on": "click",
            "selector": ".imgWrap.img-slide",
            "request": "event",
            "vars": {
                "eventId": "ampSerpCard",
                "eventCategory": t.SERP_CARD_CATEGORY,
                "eventAction": t.VIEW_DETAILS_EVENT,
                "eventLabel": t.GALLERY_LABEL
            }
        },
        "ampSerpCardFull": {
            "on": "click",
            "selector": ".type-size-wrap",
            "request": "event",
            "vars": {
                "eventId": "ampSerpCard",
                "eventCategory": t.SERP_CARD_CATEGORY,
                "eventAction": t.VIEW_DETAILS_EVENT,
                "eventLabel": t.FULL_CARD_LABEL
            }
        },
        "ampSerpCardProject": {
            "on": "click",
            "selector": ".loc-wrap.proj-name",
            "request": "event",
            "vars": {
                "eventId": "ampSerpCard",
                "eventCategory": t.SERP_CARD_CATEGORY,
                "eventAction": t.VIEW_DETAILS_EVENT,
                "eventLabel": t.VIEW_PROJECT_EVENT
            }
        },
        "ampSerpCardLocality": {
            "on": "click",
            "selector": ".loc-wrap.loc-name",
            "request": "event",
            "vars": {
                "eventId": "ampSerpCard",
                "eventCategory": t.SERP_CARD_CATEGORY,
                "eventAction": t.VIEW_DETAILS_EVENT,
                "eventLabel": t.VIEW_LOCALITY_EVENT
            }
        }
    },
    "trend": {
        "ampTrendHeaderLocality":{
            "on": "click",
            "request": "event",
            "selector": "#ampTrendHeaderLocality",
            "vars": {
                "eventId": "ampTrend",
                "eventCategory": t.HEADER_CATEGORY,
                "eventAction": t.NAVIGATION_EVENT,
                "eventLabel": "Locality"
            }
        },
        "ampTrendHeaderProject":{
            "on": "click",
            "request": "event",
            "selector": "#ampTrendHeaderProject",
            "vars": {
                "eventId": "ampTrend",
                "eventCategory": t.HEADER_CATEGORY,
                "eventAction": t.NAVIGATION_EVENT,
                "eventLabel": "Project"
            }
        },
        "ampTrendHeaderBuilderFloor":{
            "on": "click",
            "request": "event",
            "selector": "#ampTrendHeaderBuilderFloor",
            "vars": {
                "eventId": "ampTrend",
                "eventCategory": t.HEADER_CATEGORY,
                "eventAction": t.NAVIGATION_EVENT,
                "eventLabel": "Builder"
            }
        },
        "ampTrendHeaderPlot":{
            "on": "click",
            "request": "event",
            "selector": "#ampTrendHeaderPlot",
            "vars": {
                "eventId": "ampTrend",
                "eventCategory": t.HEADER_CATEGORY,
                "eventAction": t.NAVIGATION_EVENT,
                "eventLabel": "Plot"
            }
        },
        "ampTrendHeaderVilla":{
            "on": "click",
            "request": "event",
            "selector": "#ampTrendHeaderVilla",
            "vars": {
                "eventId": "ampTrend",
                "eventCategory": t.HEADER_CATEGORY,
                "eventAction": t.NAVIGATION_EVENT,
                "eventLabel": "Villa"
            }
        },
        "ampTrendHeaderApartment":{
            "on": "click",
            "request": "event",
            "selector": "#ampTrendHeaderApartment",
            "vars": {
                "eventId": "ampTrend",
                "eventCategory": t.HEADER_CATEGORY,
                "eventAction": t.NAVIGATION_EVENT,
                "eventLabel": "Apartment"
            }
        },
        "ampTrendHeaderBuy":{
            "on": "click",
            "request": "event",
            "selector": "#ampTrendHeaderBuy",
            "vars": {
                "eventId": "ampTrend",
                "eventCategory": t.HEADER_CATEGORY,
                "eventAction": t.NAVIGATION_EVENT,
                "eventLabel": "Buy"
            }
        },
        "ampTrendHeaderRent":{
            "on": "click",
            "request": "event",
            "selector": "#ampTrendHeaderRent",
            "vars": {
                "eventId": "ampTrend",
                "eventCategory": t.HEADER_CATEGORY,
                "eventAction": t.NAVIGATION_EVENT,
                "eventLabel": "Rent"
            }
        },
        "ampPriceTrendCity":{
            "on": "click",
            "request": "event",
            "selector": ".ampTrendLinkCity",
            "vars": {
                "eventId": "ampTrend",
                "eventCategory": t.PRICE_TREND_CATEGORY,
                "eventAction": t.CLICK_EVENT,
                "eventLabel": "city"
            }
        },
        "ampPriceTrendProject":{
            "on": "click",
            "request": "event",
            "selector": ".ampTrendLinkProject",
            "vars": {
                "eventId": "ampTrend",
                "eventCategory": t.PRICE_TREND_CATEGORY,
                "eventAction": t.CLICK_EVENT,
                "eventLabel": "project"
            }
        },
        "ampPriceTrendLocality":{
            "on": "click",
            "request": "event",
            "selector": ".ampTrendLinkLocality",
            "vars": {
                "eventId": "ampTrend",
                "eventCategory": t.PRICE_TREND_CATEGORY,
                "eventAction": t.CLICK_EVENT,
                "eventLabel": "locality"
            }
        },
        "ampPriceTrend":{
            "on": "click",
            "request": "event",
            "selector": ".ampTrendLinkSeeTrend",
            "vars": {
                "eventId": "ampTrend",
                "eventCategory": t.PRICE_TREND_CATEGORY,
                "eventAction": t.CLICK_EVENT,
                "eventLabel": "See Trends"
            }
        },
        "ampPriceTrendView":{
            "on": "click",
            "request": "event",
            "selector": ".ampTrendLinkView",
            "vars": {
                "eventId": "ampTrend",
                "eventCategory": t.PRICE_TREND_CATEGORY,
                "eventAction": t.CLICK_EVENT,
                "eventLabel": "View Properties"
            }
        }
    },
    "project": {
        "ampCarouselChange": {
            "on": "amp-carousel-change",
            "request": "event",
            "vars": {
                "eventId": "menubarlogo",
                "eventCategory": t.GALLERY_CATEGORY,
                "eventAction": t.GALLERY_INTERACTION_EVENT,
                "eventLabel": "Home"
            }
        },
        "availablePropertiesTracker": {
            "on": "click",
            "selector": "#availableProperties",
            "request": "event",
            "vars": {
                "eventId": "menubarlogo",
                "eventCategory": t.STICKY_HEADER_CATEGORY,
                "eventAction": t.CLICK_EVENT,
                "eventLabel": "availableProperties"
            }
        },
        "mapTracker": {
            "on": "click",
            "selector": "#map",
            "request": "event",
            "vars": {
                "eventId": "menubarlogo",
                "eventCategory": t.STICKY_HEADER_CATEGORY,
                "eventAction": t.CLICK_EVENT,
                "eventLabel": "map"
            }
        },
        "projMapTracker": {
            "on": "click",
            "selector": "#proj_map",
            "request": "event",
            "vars": {
                "eventId": "projMapTracker",
                "eventCategory": "MAP",
                "eventAction": t.CLICK_EVENT,
                "eventLabel": "Proj map Clicked"
            }
        },
        
        "descriptionTracker": {
            "on": "click",
            "selector": "#description",
            "request": "event",
            "vars": {
                "eventId": "menubarlogo",
                "eventCategory": t.STICKY_HEADER_CATEGORY,
                "eventAction": t.CLICK_EVENT,
                "eventLabel": "description"
            }
        },
        "amenitiesTracker": {
            "on": "click",
            "selector": "#amenities",
            "request": "event",
            "vars": {
                "eventId": "menubarlogo",
                "eventCategory": t.STICKY_HEADER_CATEGORY,
                "eventAction": t.CLICK_EVENT,
                "eventLabel": "amenities"
            }
        },
        "specificationsTracker": {
            "on": "click",
            "selector": "#specifications",
            "request": "event",
            "vars": {
                "eventId": "menubarlogo",
                "eventCategory": t.STICKY_HEADER_CATEGORY,
                "eventAction": t.CLICK_EVENT,
                "eventLabel": "specifications"
            }
        },
        "localityDetailsTracker": {
            "on": "click",
            "selector": "#localityDetails",
            "request": "event",
            "vars": {
                "eventId": "menubarlogo",
                "eventCategory": t.STICKY_HEADER_CATEGORY,
                "eventAction": t.CLICK_EVENT,
                "eventLabel": "localityDetails"
            }
        },
        "builderDetailsTracker": {
            "on": "click",
            "selector": "#builderDetails",
            "request": "event",
            "vars": {
                "eventId": "menubarlogo",
                "eventCategory": t.STICKY_HEADER_CATEGORY,
                "eventAction": t.CLICK_EVENT,
                "eventLabel": "builderDetails"
            }
        },
        "similarProjectTracker": {
            "on": "click",
            "selector": "#similarProjects",
            "request": "event",
            "vars": {
                "eventId": "menubarlogo",
                "eventCategory": t.SIMILAR_CATEGORY,
                "eventAction": t.INTERACTION_EVENT
            }
        }
    },
    "property": {
        "trackExperiments": {
            "on": "visible",
            "selector": "#experimentDummyImg",
            "request": "event",
            "vars": {
                "eventId": "TrackExperimentVariations",
                "eventCategory": t.PROP_PAGE_VIEW,
                "eventAction": "Experiment",
                "eventLabel": "VARIANT(prop_page)"
            }
        },
        "ampCarouselChange": {
            "on": "amp-carousel-change",
            "request": "event",
            "vars": {
                "eventId": "menubarlogo",
                "eventCategory": t.GALLERY_CATEGORY,
                "eventAction": t.GALLERY_INTERACTION_EVENT,
                "eventLabel": "Home"
            }
        },
        "ampPropSelectBanner": {
            "on": "click",
            "selector": ".ms-banner-warp",
            "request": "event",
            "vars": {
                "eventId": "ClickSelectBanner",
                "eventCategory":t.SELECT_VOUCHER_REDEMPTION_CATEGORY,
                "eventAction": t.CLICK_EVENT,
                "eventLabel":"Property_Select_Banner"
            }
        },
        "mapTracker": {
            "on": "click",
            "selector": "#map",
            "request": "event",
            "vars": {
                "eventId": "menubarlogo",
                "eventCategory": t.STICKY_HEADER_CATEGORY,
                "eventAction": t.CLICK_EVENT,
                "eventLabel": "map"
            }
        },
        "descriptionTracker": {
            "on": "click",
            "selector": "#description",
            "request": "event",
            "vars": {
                "eventId": "menubarlogo",
                "eventCategory": t.STICKY_HEADER_CATEGORY,
                "eventAction": t.CLICK_EVENT,
                "eventLabel": "description"
            }
        },
        "propMapTracker": {
            "on": "click",
            "selector": "#prop_map",
            "request": "event",
            "vars": {
                "eventId": "propMapTracker",
                "eventCategory": "MAP",
                "eventAction": t.CLICK_EVENT,
                "eventLabel": "Prop map Clicked"
            }
        },
        "amenitiesTracker": {
            "on": "click",
            "selector": "#amenities",
            "request": "event",
            "vars": {
                "eventId": "menubarlogo",
                "eventCategory": t.STICKY_HEADER_CATEGORY,
                "eventAction": t.CLICK_EVENT,
                "eventLabel": "amenities"
            }
        },
        "specificationsTracker": {
            "on": "click",
            "selector": "#specifications",
            "request": "event",
            "vars": {
                "eventId": "menubarlogo",
                "eventCategory": t.STICKY_HEADER_CATEGORY,
                "eventAction": t.CLICK_EVENT,
                "eventLabel": "specifications"
            }
        },
        "projectDetailsTracker": {
            "on": "click",
            "selector": "#projectDetails",
            "request": "event",
            "vars": {
                "eventId": "menubarlogo",
                "eventCategory": t.STICKY_HEADER_CATEGORY,
                "eventAction": t.CLICK_EVENT,
                "eventLabel": "projectDetails"
            }
        },
        "builderDetailsTracker": {
            "on": "click",
            "selector": "#builderDetails",
            "request": "event",
            "vars": {
                "eventId": "menubarlogo",
                "eventCategory": t.STICKY_HEADER_CATEGORY,
                "eventAction": t.CLICK_EVENT,
                "eventLabel": "builderDetails"
            }
        },
        "localityDetailsTracker": {
            "on": "click",
            "selector": "#localityDetails",
            "request": "event",
            "vars": {
                "eventId": "menubarlogo",
                "eventCategory": t.STICKY_HEADER_CATEGORY,
                "eventAction": t.CLICK_EVENT,
                "eventLabel": "localityDetails"
            }
        },
        "similarPropertyBuilderTracker": {
            "on": "click",
            "selector": "#similarPropertyBuilder",
            "request": "event",
            "vars": {
                "eventId": "menubarlogo",
                "eventCategory": t.STICKY_HEADER_CATEGORY,
                "eventAction": t.CLICK_EVENT,
                "eventLabel": "similarPropertyBuilder"
            }
        },
        "similarPropertySellerDetailsTracker": {
            "on": "click",
            "selector": "#similarPropertySellerDetails",
            "request": "event",
            "vars": {
                "eventId": "menubarlogo",
                "eventCategory": t.STICKY_HEADER_CATEGORY,
                "eventAction": t.CLICK_EVENT,
                "eventLabel": "similarPropertySellerDetails"
            }
        },
        "similarPropertyTracker": {
            "on": "click",
            "selector": "#similarProperty",
            "request": "event",
            "vars": {
                "eventId": "menubarlogo",
                "eventCategory": t.STICKY_HEADER_CATEGORY,
                "eventAction": t.CLICK_EVENT,
                "eventLabel": "similarProperty"
            }
        }
    },
    "ampLead": {
        "getCallBack":{
            "on": "click",
            "selector": ".connect-top-sellers a.callbtn",
            "request": "event",
            "vars": {
                "eventId": "ampLead",
                "eventCategory": t.LEAD_FORM_CATEGORY,
                "eventAction": t.OPEN_EVENT,
                "eventLabel": t.LEAD_LABELS.SingleSeller
            }
        },
        "checkMatchingSellers":{
            "on": "click",
            "selector": "#optIn",
            "request": "event",
            "vars": {
                "eventId": "ampLead",
                "eventCategory": t.BUYER_OPT_IN,
                "eventAction": t.CLICK_EVENT,
                "eventLabel": t.SMART_MULTIPLICATION_LABELS.APPLIED
            }
        },
        "closeLeadAtPhoneNumberStage":{
            "on": "click",
            "selector": "#amp-lead-stage1-head-close",
            "request": "event",
            "vars": {
                "eventId": "ampLead",
                "eventCategory": t.LEAD_FORM_CATEGORY,
                "eventAction": t.NAVIGATION_CATEGORY,
                "eventLabel": t.LEAD_LABELS.SingleSeller+'_'+t.LEAD_STEPS_MAP.CALL_NOW,
                "eventValue": t.CLOSE_LABEL
            }
        },
        /* Handles both, PhoneNumber Submit and Number Displayed*/
        "submitLeadAtPhoneNumberStage":{
            "on": "amp-form-submit-success",
            "selector": "#amp-lead-stage1-form",
            "request": "event",
            "vars": {
                "eventId": "ampLead",
                "eventCategory": t.LEAD_FORM_CATEGORY,
                "eventAction": t.SUBMIT_EVENT,
                "eventLabel": t.LEAD_STEPS_MAP.CALL_NOW,
                "eventValue": 1
            }
        },
        "backLeadAtOTP":{
            "on": "click",
            "selector": "#amp-lead-stage2-head-back",
            "request": "event",
            "vars": {
                "eventId": "ampLead",
                "eventCategory": t.LEAD_FORM_CATEGORY,
                "eventAction": t.NAVIGATION_CATEGORY,
                "eventLabel": t.LEAD_LABELS.SingleSeller+'_'+t.LEAD_STEPS_MAP.OTP
            }
        },
        "closeLeadAtOTP":{
            "on": "click",
            "selector": "#amp-lead-stage2-head-close",
            "request": "event",
            "vars": {
                "eventId": "ampLead",
                "eventCategory": t.LEAD_FORM_CATEGORY,
                "eventAction": t.NAVIGATION_CATEGORY,
                "eventLabel": t.LEAD_LABELS.SingleSeller+'_'+t.LEAD_STEPS_MAP.OTP,
                "eventValue": t.CLOSE_LABEL
            }
        },
        "resendOTP":{
            "on": "click",
            "selector": "#amp-lead-stage2-OtpOnCall-button, #amp-lead-stage2-resendOtp-button",
            "request": "event",
            "vars": {
                "eventId": "ampLead",
                "eventCategory": t.LEAD_FORM_CATEGORY,
                "eventAction": t.OTP_EVENT,
                "eventLabel": t.RESEND_LABEL,
                "eventValue": 1
            }
        },
        "telLeadPhone":{
            "on": "click",
            "selector": "#amp-lead-stage3-sellerPhone",
            "request": "event",
            "vars": {
                "eventId": "ampLead",
                "eventCategory": t.LEAD_FORM_CATEGORY,
                "eventAction": t.NAVIGATION_EVENT,
                "eventLabel": "click_phone_number"
            }
        },
        "closeLeadAtThanks": {
            "on": "click",
            "selector": "#amp-lead-stage3-head-close",
            "request": "event",
            "vars": {
                "eventId": "ampLead",
                "eventCategory": t.LEAD_FORM_CATEGORY,
                "eventAction": t.CLICK_EVENT,
                "eventLabel": t.LEAD_LABELS.SingleSeller+'_'+t.LEAD_STEPS_MAP.THANKS,
                "eventValue": t.CLOSE_LABEL
            }
        },
        "closeLeadAtCoupon": {
            "on": "click",
            "selector": "#amp-lead-copuponStage-head-close",
            "request": "event",
            "vars": {
                "eventId": "ampLead",
                "eventCategory": t.LEAD_FORM_CATEGORY,
                "eventAction": t.CLICK_EVENT,
                "eventLabel": t.CLOSE_LABEL+'_'+t.LEAD_STEPS_MAP.SELECT_COUPON
            }
        },
        "connectAtCouponStage":{
            "on": "click",
            "selector": "#amp-lead-couponStage-connect",
            "request": "event",
            "vars": {
                "eventId": "ampLead",
                "eventCategory": t.LEAD_FORM_CATEGORY,
                "eventAction": t.SUBMIT_EVENT,
                "eventLabel":t.LEAD_STEPS_MAP.SELECT_COUPON
            }
        },
        "closeLeadAtNameEmail": {
            "on": "click",
            "selector": "#amp-lead-nameEmailStage-head-close",
            "request": "event",
            "vars": {
                "eventId": "ampLead",
                "eventCategory": t.LEAD_FORM_CATEGORY,
                "eventAction": t.CLICK_EVENT,
                "eventLabel": t.CLOSE_LABEL+'_'+t.LEAD_STEPS_MAP.NAME_EMAIL
            }
        },
        "submitAtNameEmail":{
            "on": "click",
            "selector": "#amp-lead-nameEmailStage-submit",
            "request": "event",
            "vars": {
                "eventId": "ampLead",
                "eventCategory": t.LEAD_FORM_CATEGORY,
                "eventAction": t.SUBMIT_EVENT,
                "eventLabel":t.LEAD_STEPS_MAP.NAME_EMAIL
            }
        },
        "skipAtNameEmail":{
            "on": "click",
            "selector": ".skipSelector",
            "request": "event",
            "vars": {
                "eventId": "ampLead",
                "eventCategory": t.LEAD_FORM_CATEGORY,
                "eventAction": t.CLICK_SKIP,
                "eventLabel":t.LEAD_STEPS_MAP.NAME_EMAIL
            }
        },
        "trackLeadDeficitProjectClick":{
            "on": "click",
            "selector": ".track-leadDeficitProject",
            "request": "event",
            "vars": {
                "eventId": "ClicLeadDeficitProject",
                "eventCategory": t.DEFICIT_SELLER_PROJECT,
                "eventAction": t.CARD_CLICK,
                "eventLabel": "card_clicked_deficit_leadForm"
            }
        },
    },
    "allBuilder": {
        "ampBuilderCardClick":{
            "on": "click",
                "selector": ".ampBuilderCardUrl",
                "request": "event",
                "vars": {
                    "eventId": "ampBuilderCard",
                    "eventCategory": t.BUILDER_CARD,
                    "eventAction": t.CLICK_EVENT
                }
        },
        "ampBuilderTopCardClick":{
            "on": "click",
                "selector": ".ampBuilderTopCardUrl",
                "request": "event",
                "vars": {
                    "eventId": "ampBuilderTopCard",
                    "eventCategory": t.BUILDER_TOPCARD,
                    "eventAction": t.CLICK_EVENT
                }
        },
        "ampBuilderTableClick":{
            "on": "click",
                "selector": ".ampBuilderTableUrl",
                "request": "event",
                "vars": {
                    "eventId": "ampBuilderTable",
                    "eventCategory": t.BUILDER_TABLE,
                    "eventAction": t.CLICK_EVENT
                }
        }
    }
};

function _getTrackObject(on, selector, eventCategory, eventAction, eventLabel) { // jshint ignore:line
    return {
        "on": on,
        "selector": "#" + selector,
        "request": "event",
        "vars": {
            "eventCategory": eventCategory,
            "eventAction": eventAction,
            "eventLabel": eventLabel
        }
    };
}

ampTemplate.getAnalytics = function(urlDetail, data) { // jshint ignore:line

    var config = _.assign(ampAnalyticsTemplate[urlDetail.moduleName], ampAnalyticsTemplate["common"]);
    if(globalConfig.servingLeadOnAmpPages[urlDetail.moduleName]){
        config = _.assign(ampAnalyticsTemplate[urlDetail.moduleName], ampAnalyticsTemplate["ampLead"]);
    }
    /* Commenting this out as it was producing a Issue on Production */
    // switch (urlDetail.moduleName) {
    //     case 'project':
    //         _.forEach(data.similar, (similar) => {
    //             config[similar.projectId + "Tracker"] = _getTrackObject("click", "similarProjects-" + similar.projectId, t.SIMILAR_CATEGORY, t.INTERACTION_EVENT, similar.projectName)
    //         })
    //         _.forEach(data.agentService, (agent) => {
    //             config[agent.id + "Tracker"] = _getTrackObject("click", "agent-" + agent.id, t.LEAD_FORM_CATEGORY, t.SUBMIT_EVENT, agent.companyName)
    //         })

    //         if (data.configurationBuckets && data.configurationBuckets.mobileConfigurationBuckets) {
    //             _.forEach(data.configurationBuckets.mobileConfigurationBuckets.buyBucket, (listingGroup) => {
    //                 _.forEach(listingGroup.listings, (listing) => {
    //                     config[listing.listingId + "Tracker"] = _getTrackObject("click", "properties-" + listing.listingId, t.CONFIGURATION_CATEGORY, t.PROPERTY_TYPE_TOGGLE_EVENT, listing.listingUrl)
    //                 })
    //                 config[listingGroup.displayText + "ViewAllTracker"] = _getTrackObject("click", "properties-view-all-" + listingGroup.displayText, t.CONFIGURATION_CATEGORY, t.VIEW_ALL_PROPERTY, listingGroup.displayText)
    //             })
    //         }
    //         break;
    //     case 'property':
    //         if (data.similarSeller && data.similarSeller.sellerList) {
    //             _.forEach(data.similarSeller.sellerList, (agent) => {
    //                 config[agent.id + "Tracker"] = _getTrackObject("click", "agent-" + agent.id, t.LEAD_FORM_CATEGORY, t.SUBMIT_EVENT, agent.name)
    //             })
    //         }
    //         if (data.similarPropertyProj && data.similarPropertyProj.properties) {
    //             _.forEach(data.similarPropertyProj.properties, (similar) => {
    //                 config[similar.projectId + "Tracker"] = _getTrackObject("click", "similarPropertyBuilder-" + similar.projectId, t.SIMILAR_CATEGORY, t.INTERACTION_EVENT, similar.fullprojectName)
    //             })
    //         }
    //         if (data.similarPropertySell && data.similarPropertySell.properties) {
    //             _.forEach(data.similarPropertySell.properties, (similar) => {
    //                 config[similar.projectId + "Tracker"] = _getTrackObject("click", "similarPropertySellerDetails-" + similar.projectId, t.SIMILAR_CATEGORY, t.INTERACTION_EVENT, similar.fullprojectName)
    //             })
    //         }

    //         _.forEach(data.similarProperties, (similar) => {
    //             config[similar.projectId + "Tracker"] = _getTrackObject("click", "similarProperty-" + similar.projectId, t.SIMILAR_CATEGORY, t.INTERACTION_EVENT, similar.projectName)
    //         })
    //         if (data.propertyData && data.propertyData.sellerDetails) {
    //             config["agentTracker"] = _getTrackObject("click", "agent-call-wrap", t.LEAD_FORM_CATEGORY, t.SUBMIT_EVENT, data.propertyData.sellerDetails.name)
    //         }
    //         break;
    //     case 'serp':
    //         if (data.listing.listingData.listings && data.listing.listingData.listings[0]) {
    //             _.forEach(data.listing.listingData.listings[0], (listing) => {
    //                 config[listing.listingId + "-Tracker"] = _getTrackObject("click", "listing-" + listing.listingId, t.SERP_CARD_CATEGORY, t.VIEW_DETAILS_EVENT, t.TYPE_LABEL)
    //                 config[listing.postedBy.id + "-AgentTracker"] = _getTrackObject("click", "agent-" + listing.postedBy.id, t.LEAD_FORM_CATEGORY, t.SUBMIT_EVENT, listing.postedBy.name)
    //             })
    //         }
    //         break;
    // }
    var sampleTracking = {
        "vars": {
            "account": GA_TRACKING_ID,
        },
        "triggers": config,
        "extraUrlParams": ampTrackingService.getAnalyticsData(urlDetail)
    };
    return sampleTracking;
};

function _getAmpSchemaTemplate(moduleName, data) {

    var sample = {};
    switch (moduleName) {
        case 'project':
            sample = {
                "@context": "http://schema.org/",
                "@type": "ApartmentComplex"
            };
            sample["name"] = data.projectDetail.builderName + " " + data.projectDetail.title;
            sample["description"] = data.description && data.description.smallDescription;
            sample["address"] = data.projectDetail.locality + " ," + data.projectDetail.city;
            if (data.mainImage) {
                sample["image"] = { "@type": "ImageObject" };
                sample["image"]["contentUrl"] = data.mainImage.src;
                sample["image"]["caption"] = data.mainImage.title;
            }
            break;
        case 'property':
            sample = {
                "@context": "http://schema.org/",
                "@type": "SingleFamilyResidence"
            };
            sample["numberOfRooms"] = data.listing.bedrooms;
            sample["floorSize"] = data.listing.details.size;
            sample["address"] = data.locality.name + " ," + data.city.name;
            sample["photo"] = data.listing.mainImage.url;
            sample["description"] = data.listing.description;
            sample["name"] = data.listing.unitName + " " + data.builder.name + " " + data.project.name;
            break;
        case 'allBrokers':
            sample = {
                "@context": "http://schema.org/",
                "@graph": []
            };
            if(Array.isArray(data)){
                data.forEach(dElem => {
                    sample["@graph"].push({
                        "@type": 'RealEstateAgent',
                        "name": dElem.name,
                        "image": dElem.image,
                        "url": dElem.sellerUrl,
                        "address": dElem.address,
                        "award": dElem.sellerType,
                        "areaServed": (dElem.localities && dElem.localities.toString()),
                        "hasOfferCatalog": [(dElem.listingCountBuy && dElem.listingCountBuy+" Properties for Sale"), (dElem.listingCountRent && dElem.listingCountRent+" Properties for Rent")],
                        "aggregateRating": (dElem.sellerCallRatingCount || dElem.sellerFeedbackCount) ? {
                            "worstRating": 0,
                            "bestRating": 5,
                            "ratingValue": (dElem.rating && dElem.rating<0 ? 0 : dElem.rating),
                            "ratingCount": (dElem.sellerCallRatingCount<=0 ? undefined : dElem.sellerCallRatingCount),
                            "reviewCount": (dElem.sellerFeedbackCount<=0 ? undefined : dElem.sellerFeedbackCount)
                        } : undefined
                    })
                });
            }
            break;
        case 'serp':
            sample = {
                "@context": "http://schema.org/",
                "@graph": []
            };
            if (data && data.listing && data.listing.conditionalObj) {
                let dataSeller = data.listing.conditionalObj.sellerData;
                let dataSellerReviews = data.listing.conditionalObj.sellerReviews && data.listing.conditionalObj.sellerReviews.feedbacks;
                if(dataSeller) {
                    let agentSD = {
                        "@type": 'RealEstateAgent',
                        "name": dataSeller.name,
                        "url": dataSeller.sellerUrl,
                        "image": dataSeller.logo,
                        "areaServed": dataSeller.localitiesCount+' Operational Localities',
                        "description": dataSeller.fullDescription,
                        "aggregateRating": (dataSeller.callRatingCount || dataSeller.feedbackCount) ? {
                            "worstRating": 0,
                            "bestRating": 5,
                            "ratingValue": (dataSeller.rating && dataSeller.rating<0 ? 0 : dataSeller.rating),
                            "ratingCount": (dataSeller.callRatingCount<=0 ? undefined : dataSeller.callRatingCount),
                            "reviewCount": (dataSeller.feedbackCount<=0 ? undefined : dataSeller.feedbackCount)
                        } : undefined,
                        "mainEntityOfPage": dataSeller.sellerUrl
                    };
                    if((dataSeller.callRatingCount || dataSeller.feedbackCount) && dataSellerReviews && Array.isArray(dataSellerReviews) && dataSellerReviews.length>0){
                        agentSD["review"] = [];
                        dataSellerReviews.forEach(function(feedback = {}, fbIndex) {
                            if(fbIndex>50) return;
                            agentSD.review.push({
                                "@type": "Review",
                                "name": feedback.ratingCategory,
                                "author": {
                                    "@type": "Person",
                                    "name": feedback.buyerName
                                },
                                "contentLocation": feedback.city,
                                "reviewBody": feedback.comments,
                                "description": feedback.comments
                            });
                        });
                    }
                    sample["@graph"].push(agentSD);
                }
            }

            let isRental = data.listing.conditionalObj && data.listing.conditionalObj.isRental;
            let listingData = data.listing.listingData && data.listing.listingData.listings && data.listing.listingData.listings.length > 0 && data.listing.listingData.listings[0];

            for (let i in listingData) {
                let addressSchema = {
                  "url": listingData[i].localityOverviewUrl,
                  "addressLocality": listingData[i].localityName,
                  "addressRegion": listingData[i].cityName || ''
                };
                let eventSchema = {
                  "@type": "Event",
                  "image": listingData[i].mainImageURL,
                  "url": listingData[i].url,
                  "startDate": listingData[i].possessionDateFormatted || (listingData[i].verificationDate && (new Date(listingData[i].verificationDate)).toISOString()) || (listingData[i].postedDate && (new Date(listingData[i].postedDate)).toISOString()),
                  "name": (!listingData[i].isPlot?(listingData[i].bedrooms+(listingData[i].rk?' RK ':' BHK ')):(''))+(listingData[i].penthouse?'Penthouse':(listingData[i].studio?'Studio Apartment':listingData[i].propertyType))+' for '+
                            (isRental ? 'rent' : 'sale'),
                  "location": {
                    "@type": "Place",
                    "address": addressSchema
                  },
                  "offers": {
                    "@type": "Offer",
                    "url": listingData[i].url,
                    "price": listingData[i].fullPrice,
                    "priceCurrency": "INR"
                  }
                };
                sample["@graph"].push({
                    "@type": (listingData[i].propertyType ? globalConfig.schemaMap[listingData[i].propertyType.toLowerCase()] : 'Apartment'),
                    "url": listingData[i].url,
                    "numberOfRooms": listingData[i].bedrooms,
                    "image": listingData[i].mainImageURL,
                    "description": listingData[i].description,
                    "name": (!listingData[i].isPlot?(listingData[i].bedrooms+(listingData[i].rk?' RK ':' BHK ')):(''))+(listingData[i].penthouse?'Penthouse':(listingData[i].studio?'Studio Apartment':listingData[i].propertyType))+' for '+
                              (isRental ? 'rent' : 'sale'),
                    "geo": {
                      "@type": "GeoCoordinates",
                      "latitude": listingData[i].latitude,
                      "longitude": listingData[i].longitude
                    },
                    "potentialAction": {
                      "@type": (listingData[i].isRent) ? "RentAction": "BuyAction",
                      "agent": {
                        "@type": "Person",
                        "name": listingData[i].postedBy.name,
                        "award": listingData[i].sellerTransactionStatus.typeLabel,
                        "image": listingData[i].postedBy.image
                      }
                    },
                    "event": isRental ? undefined : eventSchema,
                    "address": isRental ? addressSchema : undefined
                });
            }
            break;
    }
    return sample;
}

ampTemplate.getSchema = function(urlDetail, data) {
    return _getAmpSchemaTemplate(urlDetail.moduleName, data);
};

module.exports = ampTemplate;
