"use strict";
let globalConfig = {
    itemsPerPage: 20,
    projectSerpItemsPerPage: 30,
    vernacHeader: 'p9local',
    vernacURI: {
        'hindi': 'hi-in'
    },
    enableBoundaryOnMap: false,
    disablePWA: false, //flag to deactivate service worker (pwa)
    disableVWO: true, //flag to deactivate Visual Website Optimizer
    makaanDomainId: 1,
    commercailUnitTypes: [5,6],
    wordLimit: {
        desktop: 300,
        mobile: 80,
        midLength: 200,
        longLength: 500,
        smallLength: 40,
        nonCrawlableWordLimit: 50
    },
    validStatusCodes: [400, 403, 404, 405, 414, 500, 501, 502, 503, 504],
    nonCacheableStatusCode: 501,
    roomArray: ['study', 'servant', 'pooja'],
    visibleKeyCount: 4,
    emi_tenure: 20,
    emi_rate: 8.65 / 100, //8.65% p.a.
    downpayment_percent: 12.5 / 100, //12.5% downpayment
    imageKeys: {
        "makaan3DFloorPlan": "d3",
        "makaanFloorPlan": "d2",
        "walkthrough": "wt"
    },
    floorPlanDisplayPriority: ['d3', 'wt', 'd2'],
    defaultList: {
        'amenity': ['4', '2', '14', '18', '12', '1', '11', '13', '8', '3'],
        'furnish': ['12', '11', '1', '3', '5', '9', '4', '10', '7']
    },
    defaultListCommercial: {
        'amenity': ['17', '151', '110', '5', '152', '153', '10', '120', '18', '13', '154', '155', '11', '127', '156', '157', '149']
    },
    supportedImagesType: ["jpeg", "jpg", "png"],
    floorPlanKeys: ["floorPlan", "3DFloorPlan", "VideoWalkthrough"],
    staticListingImageURL: 'http://content.makaan-ws.com/17/537628/273/1953565.jpeg',
    staticProjectImageURL: 'http://content.makaan-ws.com/17/537628/273/1953565.jpeg',
    navigationIconPriority: ["under", "budget", "builder", "ready", "facing", "house", "luxury", "rent", "studio", "independent"],
    projectImageBucketMap: [{
        key: 'Exterior Images',
        buckets: ['sponsoredImage', 'main']
    }, {
        key: 'Construction Images',
        buckets: ['constructionStatus']
    }, {
        key: 'Master Plan',
        buckets: ['clusterPlan', 'layoutPlan', 'masterPlan', '3DMasterPlan', 'locationPlan']
    }, {
        key: 'Floor Plans',
        buckets: ['floorPlan']
    }, {
        key: 'Videos',
        buckets: []
    }, {
        key: 'Amenities',
        buckets: ['amenities']
    }, {
        key: 'Others',
        buckets: ['paymentPlan', 'mainOther']
    }, {
        key: 'sitePlan',
        buckets: ['sitePlan']
    }],
    listingImageBucketMap: [{
        key: 'Main Image',
        buckets: ['mainImage', 'main']
    }, {
        key: 'Bedroom',
        buckets: ['bedroom']
    }, {
        key: 'Kitchen',
        buckets: ['kitchen']
    }, {
        key: 'Living',
        buckets: ['living']
    }, {
        key: 'Dining',
        buckets: ['dining']
    }, {
        key: 'Exterior',
        buckets: ['exterior']
    }, {
        key: 'Bathroom',
        buckets: ['bathroom']
    }, {
        key: 'Amenities',
        buckets: ['amenities']
    }, {
        key: 'Floor Plans',
        buckets: ['floorPlan']
    }, {
        key: 'Location Plan',
        buckets: ['locationPlan', 'sitePlan', 'layoutPlan']
    }, {
        key: 'Master Plan',
        buckets: ['masterPlan']
    }, {
        key: 'Cluster Plan',
        buckets: ['clusterPlan']
    }, {
        key: 'Construction',
        buckets: ['constructionStatus']
    }, {
        key: 'Others',
        buckets: ['other', 'mainOther', 'paymentPlan', 'locationMap', 'balcony']
    }],
    imageSizes: {
        smallThumbnail: {
            "width": 80,
            "height": 60
        },
        thumbnail: {
            "width": 130,
            "height": 100
        },
        tile: {
            "width": 280,
            "height": 210
        },
        squareTile: {
            "width": 210,
            "height": 210
        },
        small: {
            "width": 460,
            "height": 260
        },
        medium: {
            "width": 665,
            "height": 415
        },
        large: {
            "width": 1336,
            "height": 768
        },
        mobile: {
            "width": 220,
            "height": 120
        },
        smallSquare: {
            "width": 150,
            "height": 150
        },
        smallHeroshot: {
            "width": 340,
            "height": 153
        },
        galleryLarge: {
            "width": 1024,
            "height": 576
        },
        sponsoredBannerMobile: {
            "width": 400,
            "height": 300
        },

        sponsoredBannerDesktop: {
            "width": 900,
            "height": 400
        }
    },
    excludeQueryParameters: ['pf', 'mc', 'sc', 'lc', 'pt', 'tm', 'sourceDomain', 'debug', 'area', 'price', 'rep_date', 'ot'],
    googlePlayLink: 'https://play.google.com/store/apps/details?id=com.makaan&hl=en_US&referrer=utm_source%3Dwebsite%26utm_medium%3Dsite-link',
    proptigerCompanyId: '499',
    /* commenting the download app conditional link for mobile as onelink is not working now -- check templateLoader.js file */
    // downloadAppBuyerLink: 'https://mkn.onelink.me/535643388?pid=AppDownload',
    // downloadAppSellerLink: 'https://mknslr.onelink.me/4070643235?pid=SelrHomTop',
    semQuery_camelCase: 'utm_agentId',
    semQuery_smallCase: 'utm_agentid',
    panIndiaPageRowCount: 18,
    topAgentPageCount: 12,

    MAKAANIQ_URL: 'https://www.makaan.com/iq',
    MAKAAN_RENTSELL_URL: 'https://www.makaan.com/rent-sell-property-online',

    // social links
    FB_URL: 'https://www.facebook.com/makaan',
    TWITTER_URL: 'https://twitter.com/Makaan',
    GOOGLE_PLUS_URL: 'https://plus.google.com/+Makaandotcom/posts',
    PINTEREST_URL: "https://in.pinterest.com/makaanindia/",
    LINKEDIN_URL: "https://www.linkedin.com/company/makaan-com-pvt-ltd",
    YOUTUBE_URL: "https://www.youtube.com/results?search_query=makaan.com",

    templateId: {
        localityPriceTrend: {
            buy: 'MAKAAN_LOCALITY_PRICE_TREND_BUY',
            rent: 'MAKAAN_LOCALITY_PRICE_TREND_RENT',
            urlDomain: 'locality'
        },
        cityPriceTrend: {
            buy: 'MAKAAN_CITY_PRICE_TREND_BUY',
            rent: 'MAKAAN_CITY_PRICE_TREND_RENT',
            urlDomain: 'city'
        },
        suburbPriceTrend: {
            buy: 'MAKAAN_SUBURB_PRICE_TREND_BUY',
            rent: 'MAKAAN_SUBURB_PRICE_TREND_RENT',
            urlDomain: 'suburb'
        },
        panIndiaPriceTrend: {
            id: 'MAKAAN_INDIA_PRICE_TREND',
            url: 'price-trends'
        },
        landmarkUrl: {
            buy: 'MAKAAN_LANDMARK_PROPERTY_BUY',
            rent: 'MAKAAN_LANDMARK_PROPERTY_RENT',
        },
        cityProjectSerp: {
            "pre launch": "MAKAAN_CITY_UPCOMING_PROJECTS_SERP_BUY",
            "ready-to-move-in": "MAKAAN_CITY_READY_TO_MOVE_IN_PROJECTS_SERP_BUY",
            "under-construction": "MAKAAN_CITY_UNDER_CONSTRUCTION_PROJECTS_SERP_BUY"
        },
        localityProjectSerp: {
            "pre launch": "MAKAAN_LOCALITY_UPCOMING_PROJECTS_SERP_BUY",
            "ready-to-move-in": "MAKAAN_LOCALITY_READY_TO_MOVE_IN_PROJECTS_SERP_BUY",
            "under-construction": "MAKAAN_LOCALITY_UNDER_CONSTRUCTION_PROJECTS_SERP_BUY"
        }
    },

    //alliances activation (values: show/hide)
    ALLIANCES_ACTIVE: 'show',
    siteVisitConfigParameters: {
        entityTypeId: 15,
        feedbackTypeId: 4
    },
    ampLegacyUrl: '/amp',
    ampPageUrl: '/lite',
    servingAmpPages: {
        'trend':true,
        'project': true,
        'property': true,
        'serp': true,
        'allBuilder': true,
        'allBrokers': true,
        'cityOverview': true,
        'localityOverview': true
    },
    servingLeadOnAmpPages: {
        'project': true,
        'property': true,
        'serp': true
    },
    leadsDisclosureBenefits: [1, 2, 3, 4],
    leadsDisclosureBenefitsExpert: [5, 6, 7, 8, 9],
    schemaMap : {
        'apartment' : 'Apartment',
        'villa' : 'House',
        'independenthouse' : 'House',
        'builderfloor' : 'SingleFamilyResidence',
        'independentfloor' : 'SingleFamilyResidence',
        'independent house' : 'House',
        'builder floor' : 'SingleFamilyResidence',
        'independent floor' : 'SingleFamilyResidence',
        'plot' : 'SingleFamilyResidence',
        'residentialplot' : 'SingleFamilyResidence',
        'residential plot' : 'SingleFamilyResidence'
    }
};

module.exports = globalConfig;
