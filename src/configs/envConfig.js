"use strict";

const logger = require('services/loggerService');
var config;

let apiConfig = [];

let envConfig = {
    localhost: {
        api: {
            apiUrls: []
        }
    },
    development: {
        api: {
            apiUrls: []
        }
    },
    mpqa: {
        api: {
            apiUrls: [
                {
                    regex: new RegExp(/^\/?midl/),
                    type: 'midl',
                    baseUrl: 'http://localhost:8081/'
                },
                {
                    regex: new RegExp(/^\/?notification/),
                    type: 'notification',
                    baseUrl: 'http://localhost:8081/'
                },
                {
                    regex: new RegExp(/^\/?trend/),
                    type: 'trend',
                    baseUrl: 'http://localhost:8081/'
                },
                {
                    regex: new RegExp(/^\/?media/),
                    type: 'media',
                    baseUrl: 'http://localhost:8081/'
                },
                {
                    regex: new RegExp(/^\/?marketforce/),
                    type: 'marketforce',
                    baseUrl: 'http://localhost:8081/'
                },
                {
                    regex: new RegExp(/^\/?icrm/),
                    type: 'icrm',
                    baseUrl: 'http://localhost:8080/'
                },
                {
                    regex: new RegExp(/^\/?(app\/v1\/login|app\/v1\/logout|data\/v1\/registered|data\/v1\/entity\/user\/who-am-i|data\/v1\/entity\/user\/change-password|app\/v1\/register|app\/v1\/user\/details|app\/v1\/entity\/user\/details|app\/v1\/reset-password|data\/v1\/entity\/user\/company-user|userservice\/app|userservice\/data)/),
                    type: 'userservice',
                    baseUrl: 'http://localhost:8080/'
                },
                {
                    regex: new RegExp(/^\/?(app\/v1\/typeahead|app\/v2\/typeahead|app\/v3\/typeahead)/),
                    type: 'typeahead',
                    baseUrl: 'http://localhost:8080/'
                },
                {
                    regex: new RegExp(/^\/?data\/v1\/seo-text/),
                    type: 'seotext',
                    baseUrl: 'http://localhost:8080/'
                },
                {
                    regex: new RegExp(/^\/?columbus/),
                    type: 'columbus',
                    baseUrl: 'http://localhost:8080/'
                },
                {
                    regex: new RegExp(/^\/?app/),
                    type: 'app',
                    baseUrl: 'http://localhost:8081/'
                },
                {
                    regex: new RegExp(/^\/?data/),
                    type: 'data',
                    baseUrl: 'http://localhost:8081/'
                },
                {
                    regex: new RegExp(/^\/?seo/),
                    type: 'seo',
                    baseUrl: 'http://localhost:8080/'
                }
            ]
        }
    },
    mpqa1: {
        api: {
            apiUrls: apiConfig
        }
    },
    mpqa2: {
        api: {
            apiUrls: apiConfig
        }
    },
    beta: {
        api: {
            apiUrls: apiConfig
        }
    },
    production: {
        api: {
            apiUrls: apiConfig
        }
    }
};

module.exports.setConfig = function(env) {
    logger.info("Initialising envConfig for NODE_ENV = " + env);
    if(envConfig[env]) {
        config = envConfig[env];
    } else {
        config = envConfig['mpqa'];
    }
};

module.exports.getConfig = function() {
    if (config) {
        return config;
    }

    let e = new Error('setConfig is not yet called!');
    console.log(e.stack);
};
