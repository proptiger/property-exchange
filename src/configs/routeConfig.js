"use strict";

const logger = require('services/loggerService'),
    path = require('path');

module.exports.getRouteHandler = function(pageType, { isSEMLightPage, isAmp, globalConfig }) {

    let moduleName, modulePath, isSerp, serpType;
    switch (pageType) {
        case 'HOME_PAGE_URLS':
            moduleName = 'home';
            break;

        case 'PROJECT_URLS_OVERVIEW':
        case 'PROJECT_URLS_COMMERCIAL_OVERVIEW':
            moduleName = 'project';
            if(isSEMLightPage){
                moduleName = 'semProject';
            }
            break;

        case 'CITY_URLS_OVERVIEW':
            moduleName = 'cityOverview';
            break;

        case 'SUBURB_URLS_OVERVIEW':
        case 'LOCALITY_URLS_OVERVIEW':
            moduleName = 'localityOverview';
            break;

        case 'PROPERTY_URLS':
        case 'PROPERTY_URLS_COMMERCIAL':
            moduleName = 'property';
            break;

        case 'STATE_URLS':
        case 'CITY_URLS':
        case 'SUBURB_URLS':
        case 'PROJECT_URLS':
        case 'BUILDER_URLS':
        case 'LOCALITY_URLS':
        case 'CITY_TAXONOMY_URLS':
        case 'SUBURB_TAXONOMY_URLS':
        case 'BUILDER_TAXONOMY_URLS':
        case 'LOCALITY_TAXONOMY_URLS':
        case 'LOCALITY_SUBURB_TAXONOMY': // to handle fallback for previous support
        case 'STATIC_URLS':
        case 'NEARBY_LISTING_URLS':
        case 'NEARBY_URLS':
        case 'NEARBY_LISTING_TAXONOMY_URLS':
        case 'SELLER_PROPERTY_URLS':
        case 'SIMILAR_PROPERTY_URLS':
        case 'LISTINGS_PROPERTY_URLS':
        case 'COMPANY_URLS':
        case 'CITY_URLS_COMMERCIAL':
            moduleName = 'serp';
            isSerp     = true;
            serpType   = 'listing';
            break;

        case 'STATE_URLS_MAPS':
        case 'CITY_URLS_MAPS':
        case 'SUBURB_URLS_MAPS':
        case 'PROJECT_URLS_MAPS':
        case 'BUILDER_URLS_MAPS':
        case 'LOCALITY_URLS_MAPS':
        case 'CITY_TAXONOMY_URLS_MAPS':
        case 'SUBURB_TAXONOMY_URLS_MAPS':
        case 'BUILDER_TAXONOMY_URLS_MAPS':
        case 'LOCALITY_TAXONOMY_URLS_MAPS':
        case 'LOCALITY_SUBURB_TAXONOMY_MAPS': // to handle fallback for previous support
        case 'STATIC_URLS_MAPS':
        case 'NEARBY_LISTING_URLS_MAPS':
        case 'NEARBY_URLS_MAPS':
        case 'NEARBY_LISTING_TAXONOMY_URLS_MAPS':
        case 'SELLER_PROPERTY_URLS_MAPS':
        case 'SIMILAR_PROPERTY_URLS_MAPS':
        case 'LISTINGS_PROPERTY_URLS_MAPS':
        case 'COMPANY_URLS_MAPS':
            moduleName = 'serp-maps';
            isSerp     = true;
            serpType   = 'listing';
            break;

        case 'BUYER_DASHBOARD':
            moduleName = 'buyerDashboard';
            break;

        case 'SERVICES':
            moduleName = 'alliances';
            break;

        case 'ALL_LOCALITIES':
            moduleName = 'allLocality';
            break;

        case 'STATIC_URLS_CITY_ALL_OVERVIEW':
            moduleName = 'allCities';
            break;

        case 'STATIC_URLS_STATE_ALL_OVERVIEW':
            moduleName = 'allStates';
            break;

        case 'ALL_BUILDERS':
        case 'TOP_BUILDERS':
            moduleName = 'allBuilder';
            break;

        case 'ALL_BROKERS':
        case 'TOP_BROKERS':
            moduleName = 'allBrokers';
            break;

        case 'PRICE_TREND':
            moduleName = 'trend';
            break;

        case 'SELLER_HOME_PAGE':
            moduleName = 'sellerHomePage';
            break;

        case 'PROJECT_SERP':
        case 'PROJECT_SERP_DYNAMIC_URLS':
            moduleName = 'serpProject';
            isSerp     = true;
            serpType   = 'project';
            break;

        case 'ALL_LINKS':
            moduleName = 'allLinks';
            break;

        default:
            logger.error(`pageType ${pageType} not found.`);
            break;

    }

    if (isAmp) {
        if (globalConfig.servingAmpPages[moduleName]) {
            modulePath = path.join('amp','modules', moduleName, 'controller');
        }
    } else if (moduleName) {
        modulePath = path.join('modules', moduleName, 'controller');
    }

    return {
        modulePath,
        moduleName,
        isSerp,
        serpType
    };
};
