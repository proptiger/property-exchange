"use strict";

let footerMapping = {
    'builder': 'builder',
    'locality': 'locality',
    'suburb': 'suburb',
    'city-taxonomy': 'city-taxonomy',
    'locality-taxonomy': ' locality-taxonomy',
    'suburb-taxonomy': 'suburb-taxonomy',
    'builder-taxonomy': 'builder-taxonomy',
    'city-listing-buy': 'city-listing-buy',
    'city-listing-rent': 'city-listing-rent',
    'suburb-listing-buy': 'suburb-listing-buy',
    'suburb-listing-rent': 'suburb-listing-rent',
    'locality-listing-buy': 'locality-listing-buy',
    'locality-listing-rent': 'locality-listing-rent'
};

module.exports = footerMapping;
