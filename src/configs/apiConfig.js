"use strict";

const utils = require('services/utilService'),
    getURL = require('public/scripts/common/sharedConfig').getURL,
    globalConfig = require('configs/globalConfig'),
    ALLIANCE_APIS_END = 'alliance_new/';

let apis = {
    composite() {
        return {
            url: 'petra/app/v1/composite?',
            ignoreDomain: true
        };
    },
    seoService({ url }) {
        return {
            url: `dawnstar/data/v5/seo-text?url=${url}&checkValid`,
            mockUrl: 'mock/seoText',
            dummy: false
        };
    },
    listings({ version } = {}) {
        version = version || 4;
        return {
            url: `petra/app/v${version}/listing`,
            mockUrl: 'mock/listings'
        };
    },
    inTheNewsPostList(selector) {
        return {
            url: `iq/blog/v1/postList?${selector}`,
            ignoreDomain: true
        };
    },
    inTheNewsPostDetail(selector) {
        return {
            url: `iq/blog/v1/postDetail/${selector}`,
            ignoreDomain: true
        };
    },
    listingsWithSponsored() {
        return {
            url: `petra/app/v2/listing`,
            mockUrl: 'mock/listings'
        };
    },
    listingsWithExpansion() {
        return {
            url: 'petra/app/v4/listing',
            mockUrl: 'mock/listings'
        };
    },
    serpSimilarListings({ listingId }) {
        return {
            url: `petra/app/v1/similar/listing/${listingId}`,
            mockUrl: 'mock/serpSimilarListings'
        };
    },
    gpKeyDetail({ googlePlaceKey }) { // google place detail by google place key
        return {
            url: `petra/app/v1/gp/place-detail/${googlePlaceKey}`,
            mockUrl: 'mock/landmarkDetails'
        };
    },
    allLandmarks(selector) {
        return {
            url: `petra/data/v1/entity/landmark-url?selector=${selector}`
        };
    },
    amenities({ version = 1 } = {}) {
        version = version || 1;
        return {
            url: `petra/app/v${version}/amenity`
        };
    },
    cityDetailsById({ cityId }) {
        return {
            url: `petra/data/v1/entity/city/${cityId}`
        };
    },
    builderDetail({ builderId }) {
        return {
            url: `petra/app/v1/builder-detail/${builderId}`,
            mockUrl: 'mock/builderDetail'
        };
    },
    similarBuilder({ count, builderId }) {
        return {
            url: `/petra/data/v1/recommendation?type=similar&builderId=${builderId}&delta=1.5&limit=${count}`
        };
    },
    sellerDetailById({ companyId }) {
        return {
            url: `petra/app/v1/company/${companyId}`,
            mockUrl: 'mock/companyDetailById'
        };
    },
    sellerDetails({ selector }) {
        return {
            url: `petra/app/v1/company?selector=${selector}`,
            mockUrl: 'mock/companyDetailById'
        };
    },
    sellerDetailByUserId({ sellerId }) {
        return {
            url: `madrox/data/v1/entity/company-users/${sellerId}`
        };
    },
    sellerDetailByBuyerUserId({buyerUserId,saleType}) {
        return {
            url: `petra/app/v1/company-data/for-buyer?buyerUserId=${buyerUserId}&saleTypeId=${saleType}`
        };
    },
    storeBuyerSeller() {
        return {
            url: `petra/app/v1/entity/buyer-seller-mapping`
        };
    },
    project({ projectId }) {
        return {
            url: `petra/app/v4/project-detail/${projectId}`,
            mockUrl: 'mock/project'
        };
    },
    property({ propId, selector }) {
        return {
            url: `petra/app/v1/listing/${propId}?selector=${selector}`,
            mockUrl: 'mock/property'
        };
    },
    localityDetailsById({ localityId, selector }) {
        return {
            url: `petra/app/v3/locality/${localityId}?selector=${selector}`,
            mockUrl: 'mock/locality'
        };
    },
    suburbDetailsById({ suburbId, selector }) {
        return {
            url: `/petra/data/v1/entity/suburb/${suburbId}?selector=${selector}`
        };
    },

    cityOverview({ cityId, selector }) {
        return {
            url: `petra/app/v1/city/${cityId}?selector=${selector}`,
            mockUrl: 'mock/city'
        };
    },
    topBuilder() {
        return {
            url: 'petra/data/v2/entity/builder/top',
            mockUrl: 'mock/topBuilder'
        };
    },
    allBuilder() {
        return {
            url: 'petra/data/v1/entity/builder/listing-derived',
            mockUrl: 'mock/topBuilder'
        };
    },
    multipleBuilders(){
        return{
            url: 'petra/data/v1/entity/builder',
        }
    },
    topLocalities(selector) {
        return {
            url: `petra/data/v3/entity/locality/top?selector=${selector}`,
            mockUrl: 'mock/localities'
        };
    },
    localities(selector) {
        return {
            url: `petra/data/v3/entity/locality?selector=${selector}`,
            mockUrl: 'mock/localities'
        };
    },
    topCityProjects() {
        return {
            url: 'petra/data/v1/entity/domain?documentType=PROJECT',
            mockUrl: 'mock/topCityProjects'
        };
    },
    topCityProjectsInGA({cityId}) {
        return {
            url: `dawnstar/data/v1/city/top-suburb-projects?cityId=${cityId}`,
            mockUrl: 'mock/topCityProjectsInGA'
        };
    },
    popularCityLocalities() {
        return {
            url: 'petra/data/v1/entity/domain?documentType=LOCALITY',
            mockUrl: 'mock/popularCityLocalities'
        };
    },
    getAgentLocalities({cityId}){
        return {
            url: `petra/app/v1/${cityId}/agent-top-localities`
        };
    },
    topAgents({ cityId }) {
        return {
            url: `petra/data/v1/entity/city/${cityId}/top-agents`,
            mockUrl: 'mock/topAgents_new',
            //dummy: true
        };
    },
    featuredProjects({selector}){
        return {
            url: `petra/data/v1/entity/project/promoted?selector=${selector}`,
            mockUrl: 'mock/featuredProjects',
            //dummy: true
        };
    },
    fetchUrls(urlParams){
        return {
            url: `dawnstar/data/v3/fetch-urls?urlParam=${urlParams}`
            //urlParam will be in the form of urlParam=[{"urlDomain":"city","domainIds":[1],"urlCategoryName":"MAKAAN_CITY_PROJECTS_SERP_BUY"}]
        };
    },
    getSponsoredCompany({ projectId }) {
        return {
            url: `zenithar/app/v1/order/project/${projectId}/company-user`,
            extra: {
                noClientIP: true
            }
        };
    },
    getDetailsBySellerId() {
        return {
            url: 'madrox/data/v1/entity/company-users'
        };
    },
    getBuilderIdBySellerId(userId) {
        return {
            url: `madrox/data/v1/entity/company-users?filters=userId==${userId}`
        }
    },
    getCompanyUserIds(companyId) {
        return {
            url: `madrox/data/v1/entity/company/${companyId}/domain/company-users?domainId=1`
        }
    },
    similarListings({ propId, selector }) {
        return {
            url: `petra/app/v2/similar/unique-listing/${propId}?selector=${selector}`,
            mockUrl: 'mock/similarListings'
        };
    },
    similarLocalityListings({ selector }) {
        return {
            url: `petra/data/v1/entity/listing?selector=${selector}`
        };
    },
    priceTrend() {
        return {
            url: 'compass/data/v1/trend',
            mockUrl: 'mock/priceTrends',
            extra: {
                noClientIP: true
            }
        };
    },
    localityDescription() {
        return {
            url: 'petra/data/v1/entity/entity-description'
        };
    },
    columbusTypeahead() {
        return {
            url: 'columbus/app/v5/typeahead',
            mockUrl: 'mock/columbusTypeahead',
            dummy: false
        };
    },
    sellerFiltering({ projectId }) {
        return {
            url: `petra/app/v1/project/${projectId}/sellers`,
            // url:`petra/data/v2/entity/domain?selector=${selector}&documentType=LISTING&facets=listingCompanyId`,
            mockUrl: "mock/projectBuckets"
        };
    },
    enquiries({ enquiryId } = {}) {
        let url = "petra/data/v1/entity/enquiry";
        if (enquiryId) {
            url = `petra/data/v1/entity/enquiry/${enquiryId}`;
        }
        return {
            url: url
        };
    },
    sendOtpOnCall({ userId, userNumber }) {
        return {
            url: `madrox/app/v1/otp-on-call?userId=${userId}&number=${userNumber}`
        };
    },
    similarProject() {
        return {
            url: `petra/data/v2/recommendation?type=similar`,
            mockUrl: 'mock/similarProjects'
        };
    },
    masterUnitTypes() {
        return {
            url: 'petra/data/v1/entity/unit-types'
        };
    },
    masterFurnishings() {
        return {
            url: 'petra/data/v1/entity/master-furnishings'
        };
    },
    masterAmenities() {
        return {
            url: 'petra/data/v1/entity/master-amenities'
        };
    },
    masterCityList() {
        return {
            url: 'petra/data/v1/entity/city'
        };
    },
    masterStateList(){
        return {
            url: 'petra/data/v1/entity/state?filters=isPresent==true'
        };
    },
    wishList({ wishListId } = {}) {
        return {
            url: wishListId ? `petra/data/v1/entity/user/wish-list/${wishListId}` : 'petra/data/v1/entity/user/wish-list',
            mockUrl: 'mock/wish-list',
            dummy: false
        };
    },
    savedSearches(searchId) {
        return {
            url: searchId ? `petra/data/v1/entity/user/saved-searches/${searchId}` : 'petra/data/v1/entity/user/saved-searches',
            mockUrl: 'mock/saved-searches',
            dummy: false
        };
    },
    syncSavedSearches({ id, email }) {
        return {
            url: `petra/data/v1/entity/user/saved-searches/${id}/share?emailId=${email}`,
            dummy: false
        };

    },
    savedSearchesNonLoggedIn() {
        return {
            url: 'petra/data/v1/entity/saved-searches',
        };
    },
    enquiredListing() {
        return {
            url: 'petra/data/v1/entity/user/enquired-listing',
            mockUrl: 'mock/enquired-listing',
            dummy: false
        };
    },
    newMatches() {
        return {
            url: 'petra/data/v1/entity/user/saved-searches/new-matches'
        };
    },
    checkLogin() {
        return {
            url: 'xhr/userService/userLoginStatus'
        };
    },
    currentStage() {
        return {
            url: 'sapphire/v1/client-leads?sort=-clientActivity.phaseId&rows=1'
        };
    },
    siteVisits() {
        return {
            url: 'sapphire/v1/client/events?filters=clientActivity.phaseId==2' //'icrm/v1/client/events?filters=performTime=<t>=<now-date>'
        };
    },
    tempEnquiries({ enquiryId } = {}) {
        let url = "sapphire/v1/temp-enquiries";
        if (enquiryId) {
            url = `sapphire/v1/temp-enquiries/${enquiryId}`;
        }
        return {
            url: url
        };
    },
    callRating({ version } = {}) {
        version = version || 1;
        return {
            url: `/kira/data/v${version}/entity/call-rating`
        };
    },
    clientEvents(leadId) {
        return {
            url: leadId ? `sapphire/v1/client/events/${leadId}` : 'sapphire/v1/client/events?filters=clientActivity.phaseId==2'
        };
    },
    clientLeads(id) {
        return {
            url: id ? `sapphire/v1/client-leads?filters=companyId==${id}` : 'sapphire/v1/client-leads',
            mockUrl: 'mock/client-leads',
            dummy: false
        };
    },
    clientLeadsBySelectors(selector){
        return {
            url: `sapphire/v1/client-leads?selector=${selector}`
        };
    },
    multipleProjects() {
        return {
            url: 'petra/data/v1/entity/project'
        };
    },
    developerCampaigns() {
        return {
            url: 'jean/app/v1/campaigns'
        };
    },
    getProjects() {
        return {
            url: `petra/app/v1/project-listing-makaan`
        };
    },
   getSellerSpecificProjectDetail({selector}) {
        return {
            url: `petra/data/v2/entity/listing?selector=${selector}&sourceDomain=Makaan`,
            extra: {
                noClientIP: true
            }
        };
    },
    sellerImages(param) {
        return {
            url: `pixie/data/v2/entity/image?objectIds=${param}&objectType=user`
        };
    },
    companyDetails(param) {
        return {
            url: `madrox/data/v1/entity/companies?filters=${param}`
        };
    },
    sellersDealStatus() {
        return {
            url: 'petra/data/v1/entity/seller-deal-status',
            dummy: false,
            mockUrl: "mock/sellersDealStatusResponse"
        };
    },
    sellerRating() {
        return {
            url: 'petra/data/v1/entity/user/seller-rating-review',
            extra: {
                noClientIP: true
            },
            dummy: false,
            mockUrl: "mock/seller-rating-review"
        };
    },
    sellerRatingNonLoggedIn() {
        return {
            url: 'petra/data/v1/entity/seller-rating-review',
            dummy: false,
            mockUrl: "mock/seller-rating-review"
        };
    },
    sms({ contact }) {
        return {
            url: `madelyne/app/v1/sms?contact=${contact}&sourceDomain=Makaan`
        };
    },

    notification() {
        return {
            url: 'madelyne/data/v3/entity/notification/sender',
            extra: {
                noClientIP: true,
                excludeCookie: true
            },
            dummy: false,
            mockUrl: "mock/notification-mail-response"
        };
    },
    unsubscribe({ unsubscribeKey }) {
        return {
            url: `madelyne/data/v1/entity/notification/unsubscribe?unsubscribeKey=${unsubscribeKey}`
        };
    },
    trendingSearchLocality({ entityId, rows, category }) {
        return {
            url: `columbus/app/v1/popular/suggestions?entityId=${entityId}&rows=${rows}&category=${category}`
        };
    },
    articles(tag) {
        return {
            url: `iq/blog?tag=${tag}`
        };
    },
    configurationBuckets({ projectId, selector }) {
        return {
            url: `petra/app/v1/project-configuration/${projectId}?selector=${selector}`,
            dummy: 'mock/configurationBuckets'
        };
    },
    getImage() {
        return {
            url: 'pixie/data/v4/entity/image'
        };
    },
    topLocalitiesWithFilters({ listingCategory = 'buy', cityId, selector }) {
        return {
            url: `petra/data/v1/entity/locality/top-ranked?categoryType=${listingCategory}&cityId=${cityId}&selector=${selector || {}}`
        };
    },
    getProject({ selector }) {
        return {
            url: `petra/app/v2/project-listing?selector=${selector}&facets=projectStatus`
        };
    },
    getUserByCallId({ callId }) {
        return {
            url: `/kira/data/v3/entity/call-log?sourceDomain=Makaan&filters=id==${callId}&fields=communicationLog.virtualNumberMapping.userId`
        };
    },
    getVirtualNumber({ userId, cityId, listingType }) {
        return {
            url: `kira/data/v1/entity/virtual-number-mapping/details?userId=${userId}&cityId=${cityId}&category=${listingType}`
        };
    },
    trackerCookies() {
        return {
            url: 'petra/app/v1/cookies'
        };
    },
    getAllAllianceCategories({ cityId, fields, serviceId }) {
        return {
            url: `/petra/data/v1/entity/company-coupon/service/categories?${cityId ? 'cityId=' + cityId : ''}${serviceId ? '&serviceId=' + serviceId : ''}${fields ? '&fields=' + fields : ''}`
        };
    },
    getAliances({ cityId, serviceId, categoryId }) {
        return {
            url: ALLIANCE_APIS_END + `data/v1/entity/company-coupons?${cityId ? 'cityId=' + cityId : ''}${categoryId ? '&categoryId=' + categoryId : ''}${serviceId ? '&serviceId=' + serviceId : ''}`
        };
    },
    getServiceCategory({ cityId }) {
        return {
            url: ALLIANCE_APIS_END + `data/v1/entity/company-coupon/service/categories${cityId ? '?cityId=' + cityId : ''}`
        };
    },
    getServices({ cityId, categoryId }) {
        return {
            url: `/petra/data/v1/entity/company-coupon/services?${cityId ? 'cityId=' + cityId : ''}${categoryId ? '&categoryId=' + categoryId : ''}`
        };
    },
    getStageCards({ cityId, stageId, listingType }) {
        return {
            url: `/petra/data/v1/entity/company-coupon/services?${stageId ? 'stageId=' + stageId : ''}${listingType ? '&listingType=' + listingType : ''}${cityId ? '&cityId=' + cityId : ''}`
        };
    },
    postAlliance() {
        return {
            url: '/petra/data/v1/entity/user-offer'
        };
    },
    patchAlliance({ userEmail, userOfferLeadId }) {
        return {
            url: `/petra/data/v1/entity/user-offer/populate-user?${userEmail ? 'userEmail=' + userEmail : ''}${userOfferLeadId ? '&userOfferLeadId=' + userOfferLeadId : ''}&debug=true`
        };
    },
    getUserCityByIp() {
        return {
            url: 'petra/app/v1/mylocation?selector={"fields":["id","authorized","label","status","url","geo"]}'
        };
    },
    getAllianceCities() {
        return {
            url: "/petra/data/v1/entity/company-coupon/cities?fields=id,label,displayPriority"
        };
    },
    userDetails() {
        return {
            url: 'madrox/app/v1/user/details'
        };
    },
    getCallDetails({ callId }) {
        return {
            url: `kira/data/v3/entity/call-log?filters=id==${callId}&fields=communicationLog.virtualNumberMapping.userId,callDuration,extraData,callerUserId,callerNumber,id,cityId,callStatus,listingCategory&sourceDomain=Makaan`
        };
    },
    postChatEvent() {
        return {
            url: 'api/expose-session'
        };
    },
    feedbackToChat() {
        return {
            url: 'api/call-feedback'
        };
    },
    getVideoForListing({ listingId }) {
        return {
            url: `/pixie/data/v1/entity/video?objectType=listing&objectId=${listingId}&documentType=PropertyVideoWalkthrough&domainId=${globalConfig.makaanDomainId}`
        };
    },
    topAgentsAllIndia() {
        return {
            url: 'petra/data/v1/entity/agent/top-rated'
        };
    },
    getUsersFromContactNumber({contactNumber}) {
        return {
            url: `madrox/data/v2/entity/user-details?contactNumbers=${contactNumber}&domainId=${globalConfig.makaanDomainId}`,
            mockUrl: "mock/user-details-by-phone",
            dummy: false
        };
    },
    sendOtp({userId,userNumber}) {
        return {
            url: `madrox/app/v1/otp-for-user?userId=${userId}&number=${userNumber}&medium=sms`
        };
    },
    verifyOtp(userId) {
        return {
            url: `madrox/app/v1/otp/validate-for-user?userId=${userId}`
        };
    },
    getUserDetailsFromUserId({ userId, selector }) {
        return {
            url: `madrox/data/v2/entity/user-details?userIds=${userId}&selector=${selector}`,
            mockUrl: "mock/user-details-from-userid",
            dummy: false
        };
    },
    postUserDetails() {
        return {
            url: `madrox/data/v1/entity/user`,
        };
    },
    postRippleCase() {
        return {
            url: `mystique/v1/ripple/case-post`,
            extra: {
                noClientIP: true
            }
        };
    },
    putRippleCase(rippleCaseId) {
        return {
            url: `/mystique/v1/ripple/${rippleCaseId}?isLoginRequired=false`,
            extra: {
                noClientIP: true
            }
        };
    },
    getMultipleUserLeads(filter) {
        return {
            url: `sapphire/v1/client-leads?filters=${filter}`,
            mockUrl: "mock/multiple-leads-from-userid",
            dummy: false
        };
    },
    getShortUrl() {
        return {
            url: 'urlshortener/v1/url?key=AIzaSyCuFvJjKFoO7ckFt3yXTK0JKDc_-vt_qaA'
        };
    },
    getAgents() {
        return {
            url: 'petra/data/v1/entity/agent/listing-derived'
        };
    },
    getBankDetails() {
        return {
            url: 'petra/data/v1/entity/banklist?filters=isPartnered==true'
        };
    },

    getApprovedBankDetails(projectId) {
        return {
            url: `petra/data/v1/entity/approved-list?projectId=${projectId}&sourceDomain=Makaan`
        };
    },
    getEntityUrl({ urlParams }) {
        return {
            url: `dawnstar/data/v2/fetch-urls?urlParam=${urlParams}`
        };
    },
    getBudgetUrls({ objectTypeId, cityId, pageNumber, pageSize }) {
        var queryKeys = ["objectTypeId", "pageNumber", "pageSize"];
        var queryValues = [objectTypeId, pageNumber, pageSize];
        if(cityId) {
            queryKeys.push("cityId");
            queryValues.push(cityId);
        }
        return {
            url: utils.updateQueryStringParameter(
                    'dawnstar/data/v5/fetch-budget-price-urls',
                    queryKeys,
                    queryValues
                    )
        };
    },
    getAllSeoUrls({ objectId, objectType}) {
        return {
            url: `dawnstar/data/v1/get-all-seo-urls?objectId=${objectId}&objectType=${objectType}`
        };
    },
    futureSpaceLinks() {
        return {
            url: 'petra/data/v1/entity/links',
            extra: {
                noClientIP: true
            }
        };
    },
    rawSellerDetail({ sellerUserId }) {
        return {
            url: `petra/data/v1/entity/seller?filters=sellerUserId==${sellerUserId}`
        };
    },
    generateCouponNotification({buyerUserId,makaanCouponId}) {
        return {
            url: `petra/data/v1/entity/coupon-code/notification?buyerUserId=${buyerUserId}&makaanCouponId=${makaanCouponId}&sourceDomain=Makaan`,
        };
    },
    generateCoupon() {
        return {
            url: `flash/v1/coupon/buyer`,
            extra: {
                noClientIP: true
            }
        };
    },
    shareCouponDetails(buyerId) {
        return {
            url: `flash/v1/coupon-share?buyerId=${buyerId}`,
            extra: {
                noClientIP: true
            }
        };
    },
    getCoupondetails({buyerId,expiryDate,rows}){
        return {
            url: `flash/v1/coupon/buyer?buyerId=${buyerId}&filters=expiryDate=gt=${expiryDate};active==true&sort=-id&rows=${rows}`,
            extra: {
                noClientIP: true
            }
        };
    },
    jarvisFilterWebhook({ type }) {
        let url,
            baseUrl = 'petra/app/v1/mixpanel/callback/';
        switch (type) {
            case 'bhk':
            case 'budget':
            case 'property_type':
                url = `serp_scroll/${type}`;
                break;
            case 'enquiry_dropped':
                url = 'enquiry_dropped';
                break;
            case 'bj_sitevisit':
                url = 'page_visits/bj_sitevisit';
                break;
            case 'bj_register':
                url = 'page_visits/bj_register';
                break;
            case 'bj_book':
                url = 'page_visits/bj_book';
                break;
            case 'bj_posses':
                url = 'page_visits/bj_posses';
                break;
            case 'content_shortlist':
                url = 'content_shortlist';
                break;
            case 'fast_shortlist':
                url = 'fast_shortlist';
                break;
            case 'child_serp':
                url = 'child_serp';
                break;
            case 'content_pyr':
                url = 'content_pyr';
                break;
        }
        return {
            url: (baseUrl + url)
        };
    },
    reportErrorApi() {
        return {
            url: 'petra/data/v1/entity/error/report?sourceDomain=Makaan'
        };
    },
    getSellerReviews({ companyUserId }){
        return {
            url: `themis/data/v1/entity/feedbacks?entityDataId=${companyUserId}&entityTypeId=16&feedbackTypeId=5`
        };
    },
    getBuyerPendingFeedbacks({ buyerId }) {
        return {
            url: `themis/data/v1/entity/feedback/seller-connect/eligible-sellers/${buyerId}`
        };
    },
    updateCallFeedbackRatingReason({ ratingSubmitId }) {
        return {
            url: `themis/data/v1/entity/feedback/seller-connect/${ratingSubmitId}`
        };
    },
    getSiteVisitFeedbacks() {
        return {
            url: `petra/marketforce/data/v2/entity/seller/feedback`
        };
    },
    callNow() {
        return {
            url: 'kira/app/v2/callNow?sourceDomain=Makaan'
        };
    },
    buyerOptInStatus() {
        return {
            url: 'cyclops/data/v1/entity/temp-lead-factors?sourceDomain=Makaan'
        };
    },
    user() {
        return {
            url: `madrox/data/v1/entity/user`,
            dummy: 'mock/userCreatedDetail'
        };
    },
    getCityPriceDetails() {
        return {
            url: 'cyclops/data/v1/lead-price-list'
        };
    },
    postHomeloanAgentDetails() {
        return {
            url: 'cyclops/data/v1/pnb/homeloan'
        };
    },
    getCountries(){
        return {
            url: `/petra/data/v1/entity/country`
        };
    },
    recentDeals({start,rows,fields,filters}){
        return {
            url: `petra/app/v1/deal?start=${start}&rows=${rows}&filters=${filters}&group=sellerCompanyId&fields=${fields}&sort=-leadPaymentStatus.createdAt`
        };
    },
    popularLocalitiesOnRelevance({cityId,selector}){
        return {
            url: `petra/data/v4/entity/locality/popular?cityId=${cityId}&selector=${selector}`
        };
    },
    listingsData({selector}){
        return {
            url: `petra/data/v2/entity/listing?selector=${selector}`,
            extra: {
                noClientIP: true
            }
        };
    },
    sellerFeedbackTemplate(){
        return {
            url: `themis/data/v1/entity/feedback/template/5?sourceDomain=Makaan`
        };
    },
    uploadUserReviewImage(userId){
        return {
            url: `/pixie/data/v1/entity/image/internal?objectType=user&objectId=${userId}&imageType=feedbackProfilePicture&sourceDomain=Makaan`
        };
    },
    getFeedbackTokenData(){
        return {
            url: `themis/auth/v1/feedback/token/validate`
        };
    },
    sellerRatingsMap({fields, filters}){
        return {
            url: `themis/data/v2/entity/rating?filters=${filters}&fields=${fields}&group=value,entityDataId`,
            mockUrl: 'mock/sellerRatingsMap',
            dummy: false
        };
    },
    sellersReviewsMap(sellerCompanyUserIds){
        return {
            url: `/themis/data/v1/entity/feedback/deal-closure/latest?sellerIds=${sellerCompanyUserIds}`
        };
    },
    submitCallFeedback({sellerId}){
        return {
            url: `themis/data/v2/entity/feedback/seller-connect/${sellerId}`
        };
    },
    getFeedbackRating({filters}){
        return {
            url: `themis/data/v2/entity/rating?filters=${filters}`
        };
    },
    getDecryptedToken(){
        return {
            url: `/themis/auth/v1/feedback/token/validate`
        }
    },
    getSellerScoreBreakUp({companyUserId}){
        return {
            url: `petra/data/v2/entity/seller/${companyUserId}/score`
        }
    },
    getSponsoredProject(){
        return {
            url: `/petra/getSponsoredProjectAds`,
            mockUrl: 'mock/sponsoredProjectAds',
            dummy: true
        }
    },
    userProfileQuestions({method}) {
        return {
            url: `/midora/profile/${method}`
        };
    },
    getlistingDerivedProject(){
        return {
            url: `/petra/data/v1/entity/projects/listing-derived`,
            mockUrl: 'mock/leadDeficitSellerProjects',
            dummy:false
        };
    },
    getdeficitNearbyListings(){
        return{
            url:`petra/app/v1/nearby-listings?sourceDomain=Makaan`,
            extra: {
                noClientIP: true
            }
        }
    },
    listingPhraseDetection(){
        return {
            url: `/petra/app/v1/listing/special-features`,
            mockUrl: 'mock/listingPhraseDetection',
            dummy: false
        }
    },
    abExperiments(){
        return {
            url: 'experiments?domain=Makaan'
        }
    },
    postCustomerQuestion(){
        return {
            url: 'madelyne/data/v3/entity/notification/sender',
            extra: {
                noClientIP: true
            }
        }
    },
    getPlacesFromOSM({query}){
        return {
            url: `search?q=${query}&countrycodes=in&format=json`
        }
    },
    getPlaceDetailsFromOSM({placeId, placeType}){
        return {
            url: `reverse?osm_id=${placeId}&osm_type=${placeType}&format=json`
        }
    }
};

apis = utils._.mapValues(apis, func => function(options) {
    let urlDetails = func(options);

    if (options && options.query) {
        return getURL(urlDetails, options.query);
    }
    return urlDetails;
});

module.exports = apis;
