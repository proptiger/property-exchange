"use strict";

let benefitTagConfig = {},
    sellerMapping = {
        "9284168": {
            "sellerName": "Nestaway",
            "listingTypes": ["rental"],
            "displayTags": ["No Brokerage", "Fully Furnished Homes"]
        },
        "11038935": {
            "sellerName": "Stayabode",
            "listingTypes": ["rental"],
            "displayTags": ["No Brokerage", "Shared Homes"]
        },
        "6569351": {
            "sellerName": "Bank NPA Property",
            "listingTypes": ["primary", "resale", "rental"],
            "displayTags": ["Bank NPA Property"]
        },
        "12941239": {
            "sellerName": "Zolo",
            "listingTypes": ["rental"],
            "displayTags": ["No Brokerage", "Shared Homes"]
        }
    };

benefitTagConfig.getSellerBenefitTags = function (sellerId, listingCategory) {
    let tags = [];
    if (sellerId && sellerMapping[sellerId] && sellerMapping[sellerId].listingTypes.indexOf(listingCategory.toLowerCase()) > -1) {
        tags = sellerMapping[sellerId].displayTags;
    }
    return tags;
};

module.exports = benefitTagConfig;