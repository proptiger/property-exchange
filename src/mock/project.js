module.exports = {
    "statusCode": "2XX",
    "version": "A",
    "data": {
        "projectId": 500773,
        "isHotProject": true,
        "authorized": true,
        "localityId": 50082,
        "locality": {
            "localityId": 50082,
            "authorized": true,
            "suburbId": 10053,
            "suburb": {
                "id": 10053,
                "cityId": 20,
                "label": "Near City Center",
                "city": {
                    "id": 20,
                    "authorized": true,
                    "label": "Noida",
                    "northEastLatitude": 28.62696457,
                    "northEastLongitude": 77.61610413,
                    "southWestLatitude": 28.24383354,
                    "southWestLongitude": 77.31108856,
                    "centerLatitude": 28.53549957,
                    "centerLongitude": 77.39099884,
                    "displayPriority": 1,
                    "displayOrder": 4,
                    "url": "noida/property-sale",
                    "description": "<p style=\"text-align: justify;\">If there is one place in the National Capital Region (NCR) that offers something to everyone, we could safely say that it is the New Okhla Industrial Development Authority. Popularly known as NOIDA, the bustling city on the periphery of <span itemscope itemtype=\"https://schema.org/City\"><span itemprop=\"name\">Delhi</span></span> is one of the largest planned industrial townships in Asia. &nbsp;Fourteen kilometers away from Central , <span itemscope itemtype=\"https://schema.org/City\"><span itemprop=\"name\">Noida</span></span> was established in 1976 with the goal of building a township that would be a perfect nest of business establishments and housing projects coexisting and lending to the NCR a distinct social and cultural fabric. <span itemscope itemtype=\"https://schema.org/City\"><span itemprop=\"name\">Noida</span></span> has come far in the four decades since it was set up. Even as high rises touch its skyline and the  Metro cruises through the city to make commutes easier for its populace, <span itemscope itemtype=\"https://schema.org/City\"><span itemprop=\"name\">Noida</span></span> also offers what big cities usually offer at a premium: affordable property.</p>\n<p style=\"text-align: justify;\">Why property in <span itemscope itemtype=\"https://schema.org/City\"><span itemprop=\"name\">Noida</span></span> consistently plays on the imagination of home buyers and industrial houses when it comes to real estate investments, is a question that has multiple answers. We explore the many facets of <span itemscope itemtype=\"https://schema.org/City\"><span itemprop=\"name\">Noida</span></span> that put it in an enviable position among all the cities in the NCR.&nbsp;</p>\n<p style=\"text-align: justify;\"><strong>Infrastructure: Enabling Business and Living &nbsp;</strong></p>\n<p style=\"text-align: justify;\">The infrastructure in the city, we could say, is excellent, if we go by the sheer number of industrial houses, educational institutions and healthcare facilities that exist here. We all know that this wouldn&rsquo;t be possible without good roads and rapidly emerging highways, as these are the basics that provide the industrial houses supportive environment for business. <span itemscope itemtype=\"https://schema.org/City\"><span itemprop=\"name\">Noida</span></span> already has plenty of them, including large software companies such as the TCS, WIPRO, IBM, Accenture and HCL Technologies. Many other industrial houses such as the GAIL, ICMR, BHEL, NTPC, IBP, and FDDI have also chosen <span itemscope itemtype=\"https://schema.org/City\"><span itemprop=\"name\">Noida</span></span> as their preferred destination to do business.&nbsp;</p>\n<p style=\"text-align: justify;\">Clearly, the NCR township has been a fast mover in terms of its growth as a favored destination of the multinationals looking to set up business or invest in property in <span itemscope itemtype=\"https://schema.org/City\"><span itemprop=\"name\">Noida</span></span>. This growth story has also brought a sizeable number of working professionals to buy or rent homes here. The demand for housing has shot up in recent years, spurring real estate investments in the region and the rapid construction of high rises corresponds to the growing needs of its population. Another positive consequence of this has been the growth in supporting infrastructure such as the mushrooming of schools and hospitals. <span itemscope itemtype=\"https://schema.org/City\"><span itemprop=\"name\">Noida</span></span> today has a number of reputed educational institutions including the Genesis International, Step by Step, Lotus Valley, Pathways, Kothari International, Khaitan Public School and Galgotias.&nbsp;</p>\n<p style=\"text-align: justify;\">If you have children looking to enter college, <span itemscope itemtype=\"https://schema.org/City\"><span itemprop=\"name\">Noida</span></span> also offers plenty of good options in terms of higher education. Institutes such as the Amity University, Galgotias, ia Institute of Management, IMS, Jaypee Institute of Information Technology, and JSS  offer a range of courses to provide students a wholesome education. Your health needs are also just a few steps away in <span itemscope itemtype=\"https://schema.org/City\"><span itemprop=\"name\">Noida</span></span> with reputed healthcare chains operating their branches in <span itemscope itemtype=\"https://schema.org/City\"><span itemprop=\"name\">Noida</span></span>. Among the reputed one include the Apollo Hospital, Fortis hospital, Max hospital,  Medicare Centre and Kailash hospital.</p>\n<p style=\"text-align: justify;\">Cities are also a lot about what their neighborhoods and shopping hubs are. <span itemscope itemtype=\"https://schema.org/City\"><span itemprop=\"name\">Noida</span></span> sure has plenty in number, making it a prominent afterhours destination for people across the length and breadth of the NCR, including <span itemscope itemtype=\"https://schema.org/City\"><span itemprop=\"name\">Delhi</span></span>. Sector 18, also known for the hip `Atta Market&rsquo;, is a bustling hub of restaurants, pubs, and retail outlets catering to varied tastes and preferences of consumers. Much of the buzz in the market is also fed by the adjoining `Film City&rsquo; in Sector 16A, which is home to many media establishments such as the NDTV, India TV, T-Series and Network 18. Another landmark in the area is the Great India Place, a prominent mall with stores of all major national and international brands. For leisure and business needs, <span itemscope itemtype=\"https://schema.org/City\"><span itemprop=\"name\">Noida</span></span> also offers a host of budget and boutique hotels as well as service apartments. Some of the luxury hotels in <span itemscope itemtype=\"https://schema.org/City\"><span itemprop=\"name\">Noida</span></span> include the Radisson Blu, the Park Plaza and the Hyatt, among others. For people interested in sports, <span itemscope itemtype=\"https://schema.org/City\"><span itemprop=\"name\">Noida</span></span> is often the preferred hangout for a number of sporting venues such as the  Cricket Stadium and the Buddh International Circuit where regular sporting and recreational events are held. &nbsp;&nbsp;</p>\n<p style=\"text-align: justify;\">Besides the glamour of its malls and shopping complexes, <span itemscope itemtype=\"https://schema.org/City\"><span itemprop=\"name\">Noida</span></span> is also the hub of affordable housing. Owing to its proximity to <span itemscope itemtype=\"https://schema.org/City\"><span itemprop=\"name\">Delhi</span></span>, a number of people have chosen to settle down in <span itemscope itemtype=\"https://schema.org/City\"><span itemprop=\"name\">Noida</span></span> and commute to different parts of the capital and even within <span itemscope itemtype=\"https://schema.org/City\"><span itemprop=\"name\">Noida</span></span> for work. While greater demand for housing has pushed the prices up for property in <span itemscope itemtype=\"https://schema.org/City\"><span itemprop=\"name\">Noida</span></span>, the appreciation however has been steady, thus keeping it within the budget of the middle classes. Most houses on offer here come in the sizes of 540 sq. ft. to 9000 sq. ft. The prominent builders coming up with attractive residential projects in the area include  Greens, Jaypee Group,  Limited and  Group.</p>\n<p style=\"text-align: justify;\">However, for those with even more limited financial means, property in <span itemscope itemtype=\"https://schema.org/City\"><span itemprop=\"name\">Noida</span></span> comes with an affordable tag. Many parts of <span itemscope itemtype=\"https://schema.org/City\"><span itemprop=\"name\">Noida</span></span> are close to villages like Harola, Chaura, Nithari, Barola and Sultanpur, where people from weaker economic sections can afford homes. Prices for property in <span itemscope itemtype=\"https://schema.org/City\"><span itemprop=\"name\">Noida</span></span> can vary significantly depending on location, quality and size of project and amenities, thus offering something for everyone.</p>\n<p style=\"text-align: justify;\"><strong> Metro, the lifeline of <span itemscope itemtype=\"https://schema.org/City\"><span itemprop=\"name\">Noida</span></span>&nbsp;</strong></p>\n<p style=\"text-align: justify;\"> Metro is the lifeline of thousands of office goers in <span itemscope itemtype=\"https://schema.org/City\"><span itemprop=\"name\">Delhi</span></span> and this by no means is an exaggeration. The excellent connectivity that the service offers is now also linking various sectors within <span itemscope itemtype=\"https://schema.org/City\"><span itemprop=\"name\">Noida</span></span> and to various parts of <span itemscope itemtype=\"https://schema.org/City\"><span itemprop=\"name\">Delhi</span></span> and the NCR. For the record, <span itemscope itemtype=\"https://schema.org/City\"><span itemprop=\"name\">Noida</span></span> is the first suburb of <span itemscope itemtype=\"https://schema.org/City\"><span itemprop=\"name\">Delhi</span></span> to be connected to the Metro line. Currently, <span itemscope itemtype=\"https://schema.org/City\"><span itemprop=\"name\">Noida</span></span> has six Metro stations covering <span itemscope itemtype=\"http://schema.org/PostalAddress\"><span itemprop=\"addressLocality\">Sector 15</span></span>, Sector 16, Sector 18, Botanical Garden, Golf Course and City Centre. The Metro line also connects <span itemscope itemtype=\"https://schema.org/City\"><span itemprop=\"name\">Noida</span></span> to Connaught Place in <span itemscope itemtype=\"https://schema.org/City\"><span itemprop=\"name\">Delhi</span></span> through Barakhambha Road, IP Estate and Akshardham Complex. The Metro line also connects <span itemscope itemtype=\"https://schema.org/City\"><span itemprop=\"name\">Noida</span></span> to areas in the NCR including Vaishali through the Yamuna Bank interchange station and parts of East  such as the  Enclave, Mayur Vihar, and New Ashok Nagar. &nbsp;As is the case with excellent transport networks, the daily operations of  Metro rail has raised real estate prices in <span itemscope itemtype=\"https://schema.org/City\"><span itemprop=\"name\">Noida</span></span> and the same is expected for <span itemscope itemtype=\"http://schema.org/PostalAddress\"><span itemprop=\"addressLocality\">Greater Noida</span></span>, which is soon likely to have Metro connectivity. If this happens, the commute between <span itemscope itemtype=\"https://schema.org/City\"><span itemprop=\"name\">Noida</span></span> and <span itemscope itemtype=\"http://schema.org/PostalAddress\"><span itemprop=\"addressLocality\">Greater Noida</span></span> would not only be shorter but also more convenient, further raising the price of property in <span itemscope itemtype=\"https://schema.org/City\"><span itemprop=\"name\">Noida</span></span>.</p>\n<p style=\"text-align: justify;\"><strong>Highways: Connecting Cities</strong></p>\n<p style=\"text-align: justify;\">Highways lend life to cities. Imagine getting stuck on a congested road for hours while returning home from work and missing the movie date with your family? The number of highways that connect <span itemscope itemtype=\"https://schema.org/City\"><span itemprop=\"name\">Noida</span></span> to various parts of the NCR mitigate such worries, thus making work-life balance an achievable target for office goers in <span itemscope itemtype=\"https://schema.org/City\"><span itemprop=\"name\">Noida</span></span>. If you live in South  and come to work in <span itemscope itemtype=\"https://schema.org/City\"><span itemprop=\"name\">Noida</span></span>, the DND Flyway is the route you would prefer. The highway connects Maharani Bagh in South  to <span itemscope itemtype=\"https://schema.org/City\"><span itemprop=\"name\">Noida</span></span> and cuts down the average commute time to <span itemscope itemtype=\"https://schema.org/City\"><span itemprop=\"name\">Delhi</span></span> by 15 minutes.&nbsp;</p>\n<p style=\"text-align: justify;\"><span itemscope itemtype=\"http://schema.org/PostalAddress\"><span itemprop=\"addressLocality\">Sector 62</span></span> in <span itemscope itemtype=\"https://schema.org/City\"><span itemprop=\"name\">Noida</span></span>, which is the hub of several IT companies, is connected to  Vihar ISBT in <span itemscope itemtype=\"https://schema.org/City\"><span itemprop=\"name\">Delhi</span></span> through National Highway (NH 24) and is about 24 minutes away. While  is located further up the Expressway in <span itemscope itemtype=\"https://schema.org/City\"><span itemprop=\"name\">Noida</span></span>, it is very well connected to <span itemscope itemtype=\"https://schema.org/City\"><span itemprop=\"name\">Noida</span></span> through the - Expressway. This Expressway is also linked to the 150-kilometre long <span itemscope itemtype=\"http://schema.org/PostalAddress\"><span itemprop=\"addressLocality\">Yamuna Expressway</span></span> that connects <span itemscope itemtype=\"https://schema.org/City\"><span itemprop=\"name\">Noida</span></span> to <span itemscope itemtype=\"https://schema.org/City\"><span itemprop=\"name\">Agra</span></span>. <span itemscope itemtype=\"http://schema.org/PostalAddress\"><span itemprop=\"addressLocality\">Yamuna Expressway</span></span>, a six-lane access controlled Expressway, is popular of late for developments in real estate.&nbsp;</p>\n<p style=\"text-align: justify;\"><strong>Property Trends &nbsp;</strong></p>\n<p style=\"text-align: justify;\">As  is connected to <span itemscope itemtype=\"https://schema.org/City\"><span itemprop=\"name\">Delhi</span></span> and offers more affordable real estate, it continues to be a prominent residential and commercial area. Though the price of property in <span itemscope itemtype=\"https://schema.org/City\"><span itemprop=\"name\">Noida</span></span> has been appreciating over a period of time, it is still cheaper when compared to other parts of the NCR. According to a report from the PropTiger Data Labs, the weighted average BSP (Rs /sq. ft.) of apartment units in <span itemscope itemtype=\"https://schema.org/City\"><span itemprop=\"name\">Noida</span></span> in December 2014 was 4,475 and in <span itemscope itemtype=\"http://schema.org/PostalAddress\"><span itemprop=\"addressLocality\">Greater Noida</span></span>, it was 3,485.&nbsp;</p>\n<p style=\"text-align: justify;\">So, what are you waiting for? Make a move to <span itemscope itemtype=\"https://schema.org/City\"><span itemprop=\"name\">Noida</span></span> today.</p>\n<p><span>Property rates in <span itemscope itemtype=\"https://schema.org/City\"><span itemprop=\"name\">Noida</span></span> have risen by upto 80.0% in last 6 months.</span></p>\n<p><span>Some of the recently launched projects in <span itemscope itemtype=\"https://schema.org/City\"><span itemprop=\"name\">Noida</span></span>, that you might be interested in, are Galactic Homes, Orchid, Parkway.</span></p>",
                    "isServing": false,
                    "isServingResale": false,
                    "avgPricePerUnitArea": 4162.1343,
                    "overviewUrl": "noida-real-estate",
                    "localityMaxSafetyScore": 8.2,
                    "localityMinSafetyScore": 0.6,
                    "localityMaxLivabilityScore": 9.4,
                    "localityMinLivabilityScore": 4.5,
                    "cityLocalityCount": 123,
                    "cityTaxonomyUrls": {
                        "apartments": "noida/apartments-flats-sale",
                        "propertysale": "noida/property-sale",
                        "plots": "noida/sites-plots-sale",
                        "villas": "noida/villas-sale",
                        "newLaunch": "new-launch-in-noida",
                        "resaleApartmentUrl": "noida/resale-apartments",
                        "resalePropertyUrl": "noida/resale-property",
                        "readyToMoveFlatsUrl": "noida/ready-to-move-flats",
                        "luxuryProjectsUrl": "noida/luxury-projects",
                        "affordableFlatsUrl": "noida/affordable-flats",
                        "underConstructionPropertyUrl": "noida/under-construction-property",
                        "upcomingFlatsForSaleUrl": "noida/upcoming-flats-for-sale",
                        "upcomingPropertyUrl": "noida/upcoming-property",
                        "newAppartmentsForSaleUrl": "noida/new-apartments-for-sale"
                    },
                    "cityHeroshotImageUrl": "https://im.proptiger-ws.com/6/20/92/noida-heroshot-image-659708.jpeg",
                    "showPolygon": false
                },
                "url": "noida/property-sale-near-city-center-10053",
                "description": "",
                "priority": 100,
                "overviewUrl": "noida-real-estate/near-city-center-overview-10053",
                "suburbTaxonomyUrls": {
                    "apartments": "noida/apartments-flats-sale-near-city-center-10053",
                    "propertysale": "noida/property-sale-near-city-center-10053",
                    "plots": "noida/sites-plots-sale-near-city-center-10053",
                    "villas": "noida/villas-sale-near-city-center-10053",
                    "resaleApartmentUrl": "noida/resale-apartments-in-near-city-center-10053",
                    "resalePropertyUrl": "noida/resale-property-in-near-city-center-10053",
                    "readyToMoveFlatsUrl": "noida/ready-to-move-flats-in-near-city-center-10053",
                    "luxuryProjectsUrl": "noida/luxury-projects-in-near-city-center-10053",
                    "affordableFlatsUrl": "noida/affordable-flats-in-near-city-center-10053",
                    "underConstructionPropertyUrl": "noida/under-construction-property-in-near-city-center-10053",
                    "upcomingFlatsForSaleUrl": "noida/upcoming-flats-for-sale-in-near-city-center-10053",
                    "upcomingPropertyUrl": "noida/upcoming-property-in-near-city-center-10053",
                    "newAppartmentsForSaleUrl": "noida/new-apartments-for-sale-in-near-city-center-10053"
                },
                "isDescriptionVerified": false
            },
            "cityId": 20,
            "label": "Sector 74",
            "url": "noida/property-sale-sector-74-50082",
            "description": "Sector 74 is one of the top most locality in Noida located in suburb Near City Center. Sector 74 is surrounded by areas Sector 117, Sector 76, Sector 77, Sector 78, Delhi Meerut Highway, Sector 100, Sector 104, Sector 107, Sector 110 and Sector 113. There are at least 2 different builders constructing their projects in Sector 74. Some of the key builders are Supertech and Ajnara in Sector 74. Property in Sector 74 has built up areas ranging from 520 sq ft to 5,972 sq ft. Projects offer Apartment and Villa with 1 BHK, 2 BHK, 3 BHK, 4 BHK and 6 BHK configurations. Sector 74 has 37 Apartment and 2 Villa projects. The prices of Apartment and Villa in Sector 74 have risen by 9% in past 26 months. Sector 74 has many well known schools and Hospitals. Popular schools in Sector 74 are Manav Rachna International School, Raghav Global School, Nilgiri Hills Public School, Ramagya School, Global Indian International School, Heritage Public School, Kothari International School, Sapphire International School, Adarsh Public School, City Public School and Sri Sri Ravi Shankar Vidya Mandir, Sector 48, Noid. Popular hospitals in Sector 74 are Srijan Clinic, Shivalik Medical Center, Ashwini Hospital, Prayag Hospital and Research Centre, CardiaGreen Advanced Heart Care Center, Dhanwantri Ayurveda Care Clinic, Eye Health Clinic, Samvedana Multispeciality Hospital and Samvedana Multispeciality Hospital and Pain Manage. Find popular Apartment and Villa for sale in Sector 74 at attractive rates. You can find 5 under construction projects in Sector 74 from different builders. Sector 74 in Noida have an average rating of 8 based on user inputs, with highest rating of 10 for location. Grand Heritage by Ajnara is available at lowest rate of Rs. 4,925 per sq ft in Sector 74, whereas Ritz Chateaux by Supertech is available at highest rate of Rs. 16,990 per sq ft.",
            "priority": 100,
            "latitude": 28.57482719,
            "longitude": 77.38872528,
            "overviewUrl": "noida-real-estate/sector-74-overview-50082",
            "projectStatusCount": {
                "Under Construction": 5
            },
            "amenityTypeCount": {
                "market": 69,
                "bank": 20,
                "road": 1,
                "school": 209,
                "restaurant": 56,
                "play_school": 8,
                "petrol_pump": 7,
                "higher_education": 2,
                "atm": 328,
                "bus_stop": 80,
                "hospital": 163,
                "park": 26
            },
            "totalReviews": 0,
            "averageRating": 8.25,
            "ratingsCount": 4,
            "projectCount": 5,
            "maxRadius": 0.395,
            "avgPriceRisePercentage": 8.6,
            "avgPriceRisePercentageApartment": 8.6,
            "avgPriceRisePercentageVilla": 5.3,
            "avgPriceRiseMonths": 26,
            "avgPricePerUnitArea": 6567.4419,
            "avgPricePerUnitAreaApartment": 6567.4419,
            "avgPricePerUnitAreaVilla": 15990.0,
            "dominantUnitType": "Apartment",
            "numberOfUsersByRating": {
                "9.0": 2,
                "8.0": 1,
                "7.0": 1
            },
            "avgRatingsByCategory": {
                "overallRating": 8.25,
                "location": 10.0,
                "safety": 10.0,
                "pubTrans": 10.0,
                "restShop": 10.0,
                "schools": 9.0,
                "parks": 8.0,
                "traffic": 10.0,
                "hospitals": 9.0,
                "civic": 7.0
            },
            "localityUnitsSold6Months": 347,
            "safetyScore": 5.2,
            "livabilityScore": 7.8,
            "localitySafetyRank": 57,
            "localityLivabilityRank": 30,
            "projectMaxSafetyScore": 6.2,
            "projectMinSafetyScore": 3.8,
            "projectMaxLivabilityScore": 8.0,
            "projectMinLivabilityScore": 7.2,
            "localityTagLine": "An Overview",
            "landmarkTypes": [{
                "id": 1,
                "name": "school",
                "displayName": "school",
                "description": "School",
                "localityLandmarkTypeUrl": "noida/sector-74-50082/schools"
            }, {
                "id": 2,
                "name": "hospital",
                "displayName": "hospital",
                "description": "Hospital",
                "localityLandmarkTypeUrl": "noida/sector-74-50082/hospitals"
            }, {
                "id": 3,
                "name": "bank",
                "displayName": "bank",
                "description": "Bank",
                "localityLandmarkTypeUrl": "noida/sector-74-50082/banks"
            }, {
                "id": 4,
                "name": "atm",
                "displayName": "atm",
                "description": "Atm",
                "localityLandmarkTypeUrl": "noida/sector-74-50082/atms"
            }, {
                "id": 5,
                "name": "restaurant",
                "displayName": "restaurant",
                "description": "Restaurant",
                "localityLandmarkTypeUrl": "noida/sector-74-50082/restaurants"
            }, {
                "id": 6,
                "name": "gas_station",
                "displayName": "petrol_pump",
                "description": "Petrol Pump",
                "localityLandmarkTypeUrl": "noida/sector-74-50082/petrol-pumps"
            }],
            "newsTag": "Near City Center Sector 74",
            "localityTaxonomyUrls": {
                "apartments": "noida/apartments-flats-sale-sector-74-50082",
                "propertysale": "noida/property-sale-sector-74-50082",
                "plots": "noida/sites-plots-sale-sector-74-50082",
                "villas": "noida/villas-sale-sector-74-50082",
                "resaleApartmentUrl": "noida/resale-apartments-in-sector-74-50082",
                "readyToMoveFlatsUrl": "noida/ready-to-move-flats-in-sector-74-50082",
                "luxuryProjectsUrl": "noida/luxury-projects-in-sector-74-50082",
                "affordableFlatsUrl": "noida/affordable-flats-in-sector-74-50082",
                "underConstructionPropertyUrl": "noida/under-construction-property-in-sector-74-50082",
                "upcomingFlatsForSaleUrl": "noida/upcoming-flats-for-sale-in-sector-74-50082",
                "upcomingPropertyUrl": "noida/upcoming-property-in-sector-74-50082",
                "newAppartmentsForSaleUrl": "noida/new-apartments-for-sale-in-sector-74-50082",
                "resalePropertyUrl": "noida/resale-property-in-sector-74-50082"
            },
            "priceRiseRank": 7,
            "priceRiseRankPercentage": 6,
            "livabilityScoreRankPercentage": 25,
            "isDescriptionVerified": false,
            "localitySourceId": [101, 1],
            "localitySourceDomain": ["Proptiger", "Makaan"],
            "minAffordablePrice": 4000000,
            "maxAffordablePrice": 8000000,
            "minLuxuryPrice": 15000000,
            "maxBudgetPrice": 5000000
        },
        "builderId": 100039,
        "builder": {
            "id": 100039,
            "name": "Supertech",
            "imageURL": "https://im.proptiger-ws.com/3/39/13/supertech-70.jpg",
            "description": "<p style=\"text-align: justify;\">Founded in 1988, Supertech Group is Indias leading real estate developer company in National Capital Region. R K Arora is the chairman and Managing Director of company.</p>\n<br />\n<p style=\"text-align: justify;\">The company possesses over 2 decades of experience across the real estate, hospitality and construction sector. It develops properties in diversified verticals of real estate like townships, hospitality, residential, office spaces, commercial and retail.</p>\n<br />\n<p style=\"text-align: justify;\">Supertech has developed both residential and commercial complexes in National Capital Region (NCR), <span><span><span><span>Meerut</span></span></span></span>, <span><span>Rudrapur</span></span> and <span><span>Moradabad</span></span>. The company also has its presence in Haryana, Uttarakhand, Uttar Pradesh and Bengaluru in South.</p>\n<br />\n<p style=\"text-align: justify;\">It has pioneered in introducing the concept of mixed-use development in India and present high rise constructions in North India. More than 33 million sq. ft. of space has been developed by Supertech for residential and commercial sector. Presently, 40 projects are ongoing worth Rs. 14,000 Crores and 75 million sq. ft. is under construction.</p>\n<br />\n<p style=\"text-align: justify;\"><strong> Top Projects till Date:</strong></p>\n<br />\n<ul style=\"padding-left: 20px;\">\n<li>Developed more than 500 operational hotel rooms in different cities across India.</li>\n<li>Emerald Court in Sector 93A, <span><span>Noida</span></span> comprising of 660 units of 3, 4 and 5 BHK apartments with sizes ranging from 1,750 sq. ft. to 5,431 sq. ft.</li>\n<li>Romano in Sector 118, <span><span>Noida</span></span> comprising of 2,544 units of 2 and 3 BHK apartments with sizes ranging from 1,020 sq. ft. to 2,170 sq. ft.</li>\n</ul>\n<br />\n<p style=\"text-align: justify;\"><strong>Few of its key under construction residential projects are:</strong></p>\n<br />\n<ul style=\"padding-left: 20px;\">\n<li>Pavilion in Sector 34, <span><span>Noida</span></span> comprising of 362 units of 2 and 3 BHK apartments of 1,075 sq. ft. to 3,776 sq. ft.</li>\n<li>Ecociti in Sector 137, <span><span><span><span>Noida</span></span></span></span> comprising of 1732 units of 2, 3 and 4 BHK apartments with sizes ranging from 890 sq. ft. to 2,275 sq. ft.</li>\n<li>Micasa in <span><span>Thanisandra</span></span>, <span><span><span><span>Bangalore</span></span></span></span> comprising of 224 units of 2 and 3 BHK apartments with sizes ranging from 1,100 sq. ft. to 1,735 sq. ft.</li>\n</ul>\n<br />\n<p style=\"text-align: justify;\">Supertech Group has been honoured with Udyog Ratan Award for its quality standard. Not only this, the company ISO 9001:2001 certified.</p>",
            "establishedDate": 567973800000,
            "priority": 8,
            "url": "supertech-100039",
            "projectStatusCount": {
                "pre launch": 7,
                "under construction": 32,
                "cancelled": 0,
                "launch": 0,
                "completed": 9,
                "on hold": 3,
                "not launched": 2
            },
            "projectCount": 53,
            "builderWebsite": "www.supertechlimited.com",
            "builderAddress": "Supertech House\nB 28-29, Sec-58,\nNoida - 201307, India",
            "builderLocalityCount": 30,
            "images": [{
                "id": 70,
                "imageTypeId": 13,
                "objectId": 100039,
                "path": "3/39/13/",
                "pageUrl": "gallery/supertech-100039-70",
                "statusId": 1,
                "createdAt": 1450927153000,
                "sizeInBytes": 19459,
                "width": 148,
                "height": 61,
                "altText": "Supertech",
                "waterMarkName": "70.jpg",
                "active": true,
                "activeStatus": 1,
                "seoName": "supertech-70.jpg",
                "absolutePath": "https://im.proptiger-ws.com/3/39/13/supertech-70.jpg",
                "imageType": {
                    "id": 13,
                    "objectType": {
                        "id": 3,
                        "type": "builder"
                    },
                    "mediaType": {
                        "id": 1,
                        "name": "Image"
                    },
                    "objectTypeId": 3,
                    "mediaTypeId": 1,
                    "type": "logo",
                    "domainId": 2,
                    "priority": 2,
                    "imageSitemapEnabled": 1,
                    "displayName": "Logo"
                },
                "masterImageStatuses": {
                    "id": 1,
                    "status": "PROCESSED"
                }
            }],
            "builderCities": ["Noida", "Udham Singh Nagar", "Bangalore", "Dehradun", "Ghaziabad", "Gurgaon", "Meerut"],
            "mainImage": {
                "id": 0,
                "imageTypeId": 0,
                "objectId": 0,
                "statusId": 0,
                "sizeInBytes": 0,
                "width": 0,
                "height": 0,
                "altText": "Supertech",
                "active": false,
                "activeStatus": 0,
                "absolutePath": "3/39/13/supertech-70.jpg"
            },
            "builderScore": 7.703390945832,
            "isBuilderListed": false,
            "builderSourceId": [1, 101],
            "builderSourceDomain": ["Makaan", "Proptiger"]
        },
        "properties": [{
            "propertyId": 5000168,
            "projectId": 500773,
            "bedrooms": 2,
            "bathrooms": 2,
            "unitType": "Apartment",
            "unitName": "2BHK+2T",
            "pricePerUnitArea": 5350,
            "size": 930,
            "measure": "sq ft",
            "URL": "noida/supertech-capetown-sector-74-5000168/2bhk-2t-930-sqft-apartment",
            "budget": 4975500.0,
            "images": [{
                "id": 565085,
                "imageTypeId": 12,
                "objectId": 5000168,
                "path": "2/5000168/12/",
                "pageUrl": "gallery/supertech-capetown-floor-plan-2bhk-2t-930-sq-ft-5000168-565085",
                "statusId": 1,
                "createdAt": 1450927153000,
                "sizeInBytes": 334634,
                "width": 1088,
                "height": 1200,
                "altText": "Supertech CapeTown Floor Plan (2BHK+2T (930 sq ft) 930 sq ft)",
                "title": "Floor Plan (2BHK+2T (930 sq ft) 930 sq ft)",
                "description": "",
                "waterMarkName": "565085.jpeg",
                "active": true,
                "activeStatus": 1,
                "seoName": "supertech-capetown-floor-plan-2bhk-2t-930-sq-ft-565085.jpeg",
                "absolutePath": "https://im.proptiger-ws.com/2/5000168/12/supertech-capetown-floor-plan-2bhk-2t-930-sq-ft-565085.jpeg",
                "imageType": {
                    "id": 12,
                    "objectType": {
                        "id": 2,
                        "type": "property"
                    },
                    "mediaType": {
                        "id": 1,
                        "name": "Image"
                    },
                    "objectTypeId": 2,
                    "mediaTypeId": 1,
                    "type": "floorPlan",
                    "domainId": 2,
                    "priority": 2,
                    "imageSitemapEnabled": 1,
                    "displayName": "Floor Plan"
                },
                "masterImageStatuses": {
                    "id": 1,
                    "status": "PROCESSED"
                }
            }],
            "minResaleOrPrimaryPrice": 4975500.0,
            "maxResaleOrPrimaryPrice": 4975500.0,
            "studyRoom": 0,
            "balcony": 3,
            "displayCarpetArea": 0,
            "createdAt": 1384366064000,
            "updatedAt": 1431903044000,
            "penthouse": false,
            "studio": false,
            "imageTypeCount": {
                "paymentPlan": 2,
                "constructionStatus": 41,
                "masterPlan": 1,
                "locationPlan": 1,
                "main": 2,
                "mainOther": 7
            },
            "media": [{
                "id": 252871,
                "objectMediaTypeId": 102,
                "objectMediaType": {
                    "id": 102,
                    "objectType": {
                        "id": 2,
                        "type": "property"
                    },
                    "mediaType": {
                        "id": 2,
                        "name": "Document"
                    },
                    "objectTypeId": 2,
                    "mediaTypeId": 2,
                    "type": "Panoramic",
                    "domainId": 2,
                    "priority": 1,
                    "imageSitemapEnabled": 0,
                    "displayName": "Tour Zip"
                },
                "objectId": 5000168,
                "priority": 1,
                "sizeInBytes": 12665170,
                "description": "Panorama",
                "active": true,
                "createdAt": 1434549468000,
                "updatedAt": 1434549468000,
                "mediaExtraAttributes": {
                    "indexFile": "vtour/tour.html",
                    "extractPath": "2/2/5000168/102/252871/"
                },
                "absoluteUrl": "https://im.proptiger-ws.com/2/2/5000168/102/252871/252871.zip"
            }, {
                "id": 251183,
                "objectMediaTypeId": 89,
                "objectMediaType": {
                    "id": 89,
                    "objectType": {
                        "id": 2,
                        "type": "property"
                    },
                    "mediaType": {
                        "id": 2,
                        "name": "Document"
                    },
                    "objectTypeId": 2,
                    "mediaTypeId": 2,
                    "type": "3DFloorPlan",
                    "domainId": 2,
                    "priority": 0,
                    "imageSitemapEnabled": 0,
                    "displayName": "3D Floor Plan"
                },
                "objectId": 5000168,
                "priority": 1,
                "sizeInBytes": 130042,
                "description": "Floor Plan",
                "active": true,
                "createdAt": 1434368653000,
                "updatedAt": 1434368653000,
                "mediaExtraAttributes": {
                    "width": 1200.0,
                    "height": 900.0,
                    "svg": "[{\"polygon_class\":\"BR1\",\"name\":\"Bedroom\",\"points\":\"837 742 805 408 525 408 514 689 691 687 692 743 836 744 \",\"per_points\":\"93 82.44 89.44 45.33 58.33 45.33 57.11 76.56 76.78 76.33 76.89 82.56 92.89 82.67 \",\"tooltip_pos\":\"Top\",\"tooltip_left\":\"58\",\"tooltip_top\":\"26\",\"length\":\"10\",\"width\":\"11\"},{\"polygon_class\":\"BR2\",\"name\":\"Bedroom\",\"points\":\"670 25 678 243 688 242 682 317 783 315 951 317 984 217 1048 216 1024 114 972 114 955 24 671 24 \",\"per_points\":\"74.44 2.78 75.33 27 76.44 26.89 75.78 35.22 87 35 105.67 35.22 109.33 24.11 116.44 24 113.78 12.67 108 12.67 106.11 2.67 74.56 2.67 \",\"tooltip_pos\":\"Bottom\",\"tooltip_left\":\"47\",\"tooltip_top\":\"37\",\"length\":\"11\",\"width\":\"10\"},{\"polygon_class\":\"KITCHEN\",\"name\":\"Kitchen\",\"points\":\"533 507 374 508 395 306 403 305 404 300 441 301 445 306 478 309 535 407 533 506 \",\"per_points\":\"59.22 56.33 41.56 56.44 43.89 34 44.78 33.89 44.89 33.33 49 33.44 49.44 34 53.11 34.33 59.44 45.22 59.22 56.22 \",\"tooltip_pos\":\"Top\",\"tooltip_left\":\"31\",\"tooltip_top\":\"15\",\"length\":\"5\",\"width\":\"8.75\"},{\"polygon_class\":\"LR\",\"name\":\"Living/Dinning room\",\"points\":\"218 218 258 24 681 24 687 242 684 315 447 315 441 301 405 300 406 307 396 307 248 309 217 216 \",\"per_points\":\"24.22 24.22 28.67 2.67 75.67 2.67 76.33 26.89 76 35 49.67 35 49 33.44 45 33.33 45.11 34.11 44 34.11 27.56 34.33 24.11 24 \",\"tooltip_pos\":\"Bottom\",\"tooltip_left\":\"24\",\"tooltip_top\":\"35\",\"length\":\"17.08\",\"width\":\"10\"},{\"polygon_class\":\"BATHROOM1\",\"name\":\"Bathroom\",\"points\":\"525 691 532 501 374 498 356 691 524 690 \",\"per_points\":\"58.33 76.78 59.11 55.67 41.56 55.33 39.56 76.78 58.22 76.67 \",\"tooltip_pos\":\"Top\",\"tooltip_left\":\"35\",\"tooltip_top\":\"36\",\"length\":\"5\",\"width\":\"7\"},{\"polygon_class\":\"BATHROOM2 \",\"name\":\"Bathroom\",\"points\":\"612 417 803 417 793 309 611 309 610 416 \",\"per_points\":\"68 46.33 89.22 46.33 88.11 34.33 67.89 34.33 67.78 46.22 \",\"tooltip_pos\":\"Top\",\"tooltip_left\":\"43\",\"tooltip_top\":\"15\",\"length\":\"4.75\",\"width\":\"6.67\"}]"
                },
                "absoluteUrl": "https://im.proptiger-ws.com/2/2/5000168/89/251183.jpg"
            }],
            "video": [{
                "id": 2,
                "objectMediaTypeId": 106,
                "objectMediaType": {
                    "id": 106,
                    "objectType": {
                        "id": 2,
                        "type": "property"
                    },
                    "mediaType": {
                        "id": 4,
                        "name": "Video"
                    },
                    "objectTypeId": 2,
                    "mediaTypeId": 4,
                    "type": "VideoWalkthrough",
                    "domainId": 2,
                    "priority": 1,
                    "imageSitemapEnabled": 0,
                    "displayName": "Video Walkthrough"
                },
                "objectId": 5000168,
                "priority": 1,
                "sizeInBytes": 8357161,
                "active": true,
                "createdAt": 1433242098000,
                "updatedAt": 1440567578000,
                "mediaExtraAttributes": {
                    "videoUrlDetails": [{
                        "resolution": 270,
                        "bitRate": "300",
                        "url": "http://d10fypcdh7kbf3.cloudfront.net/4/2/5000168/106/2/supertech-capetown-floor-plan-2bhk-2t-930-sq-ft-5000168.mp4"
                    }]
                },
                "imageUrl": "https://im.proptiger-ws.com/4/2/5000168/106/2/supertech-capetown-floor-plan-2bhk-2t-930-sq-ft-5000168.jpg",
                "fps": 30,
                "resolution": "960×540",
                "bitRate": 500,
                "duration": 33.9339,
                "image": {
                    "id": 0,
                    "imageTypeId": 0,
                    "objectId": 0,
                    "statusId": 0,
                    "sizeInBytes": 0,
                    "width": 0,
                    "height": 0,
                    "active": false,
                    "activeStatus": 0,
                    "absolutePath": "https://im.proptiger-ws.com/https://im.proptiger-ws.com/4/2/5000168/106/2/supertech-capetown-floor-plan-2bhk-2t-930-sq-ft-5000168.jpg"
                },
                "status": "Processed",
                "url": "http://d10fypcdh7kbf3.cloudfront.net/4/2/5000168/106/2/supertech-capetown-floor-plan-2bhk-2t-930-sq-ft-5000168.mp4"
            }],
            "isPropertySoldOut": false,
            "propertySourceDomain": ["Proptiger"]
        }, {
            "propertyId": 5000169,
            "projectId": 500773,
            "bedrooms": 2,
            "bathrooms": 2,
            "unitType": "Apartment",
            "unitName": "2BHK+2T",
            "pricePerUnitArea": 5350,
            "size": 1082,
            "measure": "sq ft",
            "URL": "noida/supertech-capetown-sector-74-5000169/2bhk-2t-1082-sqft-apartment",
            "budget": 5788700.0,
            "images": [{
                "id": 565086,
                "imageTypeId": 12,
                "objectId": 5000169,
                "path": "2/5000169/12/",
                "pageUrl": "gallery/supertech-capetown-floor-plan-2bhk-2t-1082-sq-ft-5000169-565086",
                "statusId": 1,
                "createdAt": 1450927153000,
                "sizeInBytes": 350552,
                "width": 1040,
                "height": 1192,
                "altText": "Supertech CapeTown Floor Plan (2BHK+2T (1,082 sq ft) 1082 sq ft)",
                "title": "Floor Plan (2BHK+2T (1,082 sq ft) 1082 sq ft)",
                "description": "",
                "waterMarkName": "565086.jpeg",
                "active": true,
                "activeStatus": 1,
                "seoName": "supertech-capetown-floor-plan-2bhk-2t-1082-sq-ft-565086.jpeg",
                "absolutePath": "https://im.proptiger-ws.com/2/5000169/12/supertech-capetown-floor-plan-2bhk-2t-1082-sq-ft-565086.jpeg",
                "imageType": {
                    "id": 12,
                    "objectType": {
                        "id": 2,
                        "type": "property"
                    },
                    "mediaType": {
                        "id": 1,
                        "name": "Image"
                    },
                    "objectTypeId": 2,
                    "mediaTypeId": 1,
                    "type": "floorPlan",
                    "domainId": 2,
                    "priority": 2,
                    "imageSitemapEnabled": 1,
                    "displayName": "Floor Plan"
                },
                "masterImageStatuses": {
                    "id": 1,
                    "status": "PROCESSED"
                }
            }],
            "minResaleOrPrimaryPrice": 5788700.0,
            "maxResaleOrPrimaryPrice": 5788700.0,
            "studyRoom": 0,
            "balcony": 3,
            "displayCarpetArea": 0,
            "createdAt": 1384366064000,
            "updatedAt": 1431903047000,
            "penthouse": false,
            "studio": false,
            "imageTypeCount": {
                "paymentPlan": 2,
                "constructionStatus": 41,
                "masterPlan": 1,
                "locationPlan": 1,
                "main": 2,
                "mainOther": 7
            },
            "media": [{
                "id": 252882,
                "objectMediaTypeId": 102,
                "objectMediaType": {
                    "id": 102,
                    "objectType": {
                        "id": 2,
                        "type": "property"
                    },
                    "mediaType": {
                        "id": 2,
                        "name": "Document"
                    },
                    "objectTypeId": 2,
                    "mediaTypeId": 2,
                    "type": "Panoramic",
                    "domainId": 2,
                    "priority": 1,
                    "imageSitemapEnabled": 0,
                    "displayName": "Tour Zip"
                },
                "objectId": 5000169,
                "priority": 1,
                "sizeInBytes": 12660955,
                "description": "Panorama",
                "active": true,
                "createdAt": 1434550332000,
                "updatedAt": 1434550332000,
                "mediaExtraAttributes": {
                    "indexFile": "vtour/tour.html",
                    "extractPath": "2/2/5000169/102/252882/"
                },
                "absoluteUrl": "https://im.proptiger-ws.com/2/2/5000169/102/252882/252882.zip"
            }, {
                "id": 251186,
                "objectMediaTypeId": 89,
                "objectMediaType": {
                    "id": 89,
                    "objectType": {
                        "id": 2,
                        "type": "property"
                    },
                    "mediaType": {
                        "id": 2,
                        "name": "Document"
                    },
                    "objectTypeId": 2,
                    "mediaTypeId": 2,
                    "type": "3DFloorPlan",
                    "domainId": 2,
                    "priority": 0,
                    "imageSitemapEnabled": 0,
                    "displayName": "3D Floor Plan"
                },
                "objectId": 5000169,
                "priority": 1,
                "sizeInBytes": 132872,
                "description": "Floor Plan",
                "active": true,
                "createdAt": 1434368653000,
                "updatedAt": 1434368653000,
                "mediaExtraAttributes": {
                    "width": 1200.0,
                    "height": 900.0,
                    "svg": "[{\"polygon_class\":\"BR1\",\"name\":\"Bedroom\",\"points\":\"684 740 824 738 796 405 517 404 506 684 514 684 686 740 \",\"per_points\":\"76 82.22 91.56 82 88.44 45 57.44 44.89 56.22 76 57.11 76 76.22 82.22 \",\"tooltip_pos\":\"Top\",\"tooltip_left\":\"58\",\"tooltip_top\":\"26\",\"length\":\"10.5\",\"width\":\"13\"},{\"polygon_class\":\"BR2\",\"name\":\"Bedroom\",\"points\":\"663 311 669 237 663 22 947 21 964 110 1016 110 1039 213 978 212 944 312 664 311 \",\"per_points\":\"73.67 34.56 74.33 26.33 73.67 2.44 105.22 2.33 107.11 12.22 112.89 12.22 115.44 23.67 108.67 23.56 104.89 34.67 73.78 34.56 \",\"tooltip_pos\":\"Bottom\",\"tooltip_left\":\"46\",\"tooltip_top\":\"36\",\"length\":\"11\",\"width\":\"10.5\"},{\"polygon_class\":\"LR\",\"name\":\"Living/Dinning room\",\"points\":\"675 309 678 240 672 21 249 21 209 211 240 312 394 309 395 297 429 296 437 309 675 308 \",\"per_points\":\"75 34.33 75.33 26.67 74.67 2.33 27.67 2.33 23.22 23.44 26.67 34.67 43.78 34.33 43.89 33 47.67 32.89 48.56 34.33 75 34.22 \",\"tooltip_pos\":\"Bottom\",\"tooltip_left\":\"11\",\"tooltip_top\":\"36\",\"length\":\"18.25\",\"width\":\"11\"},{\"polygon_class\":\"KITCHEN\",\"name\":\"Kitchen\",\"points\":\"523 500 367 504 365 503 388 304 396 303 397 296 432 296 434 304 470 302 469 309 527 402 524 499 \",\"per_points\":\"58.11 55.56 40.78 56 40.56 55.89 43.11 33.78 44 33.67 44.11 32.89 48 32.89 48.22 33.78 52.22 33.56 52.11 34.33 58.56 44.67 58.22 55.44 \",\"tooltip_pos\":\"Top\",\"tooltip_left\":\"30\",\"tooltip_top\":\"14\",\"length\":\"6\",\"width\":\"9.75\"},{\"polygon_class\":\"BATHROOM1\",\"name\":\"Bathroom\",\"points\":\"516 686 523 496 366 496 347 684 516 686 \",\"per_points\":\"57.33 76.22 58.11 55.11 40.67 55.11 38.56 76 57.33 76.22 \",\"tooltip_pos\":\"Top\",\"tooltip_left\":\"35\",\"tooltip_top\":\"35\",\"length\":\"6\",\"width\":\"7.5\"},{\"polygon_class\":\"BATHROOM2\",\"name\":\"Bathroom\",\"points\":\"603 412 795 411 784 305 604 304 602 411 \",\"per_points\":\"67 45.78 88.33 45.67 87.11 33.89 67.11 33.78 66.89 45.67 \",\"tooltip_pos\":\"Top\",\"tooltip_left\":\"43\",\"tooltip_top\":\"14\",\"length\":\"4.75\",\"width\":\"7.17\"}]"
                },
                "absoluteUrl": "https://im.proptiger-ws.com/2/2/5000169/89/251186.jpg"
            }],
            "video": [{
                "id": 28,
                "objectMediaTypeId": 106,
                "objectMediaType": {
                    "id": 106,
                    "objectType": {
                        "id": 2,
                        "type": "property"
                    },
                    "mediaType": {
                        "id": 4,
                        "name": "Video"
                    },
                    "objectTypeId": 2,
                    "mediaTypeId": 4,
                    "type": "VideoWalkthrough",
                    "domainId": 2,
                    "priority": 1,
                    "imageSitemapEnabled": 0,
                    "displayName": "Video Walkthrough"
                },
                "objectId": 5000169,
                "priority": 1,
                "sizeInBytes": 8344692,
                "active": true,
                "createdAt": 1433245346000,
                "updatedAt": 1440567577000,
                "mediaExtraAttributes": {
                    "videoUrlDetails": [{
                        "resolution": 270,
                        "bitRate": "300",
                        "url": "http://d10fypcdh7kbf3.cloudfront.net/4/2/5000169/106/28/supertech-capetown-floor-plan-2bhk-2t-1082-sq-ft-5000169.mp4"
                    }]
                },
                "imageUrl": "https://im.proptiger-ws.com/4/2/5000169/106/28/supertech-capetown-floor-plan-2bhk-2t-1082-sq-ft-5000169.jpg",
                "fps": 30,
                "resolution": "960×540",
                "bitRate": 500,
                "duration": 33.9339,
                "image": {
                    "id": 0,
                    "imageTypeId": 0,
                    "objectId": 0,
                    "statusId": 0,
                    "sizeInBytes": 0,
                    "width": 0,
                    "height": 0,
                    "active": false,
                    "activeStatus": 0,
                    "absolutePath": "https://im.proptiger-ws.com/https://im.proptiger-ws.com/4/2/5000169/106/28/supertech-capetown-floor-plan-2bhk-2t-1082-sq-ft-5000169.jpg"
                },
                "status": "Processed",
                "url": "http://d10fypcdh7kbf3.cloudfront.net/4/2/5000169/106/28/supertech-capetown-floor-plan-2bhk-2t-1082-sq-ft-5000169.mp4"
            }],
            "isPropertySoldOut": false,
            "propertySourceDomain": ["Proptiger"]
        }, {
            "propertyId": 5262116,
            "projectId": 500773,
            "bedrooms": 2,
            "bathrooms": 2,
            "unitType": "Apartment",
            "unitName": "2BHK+2T Study Room",
            "pricePerUnitArea": 5350,
            "size": 1150,
            "measure": "sq ft",
            "URL": "noida/supertech-capetown-sector-74-5262116/2bhk-2t-1150-sqft-apartment",
            "budget": 6152500.0,
            "images": [{
                "id": 616046,
                "imageTypeId": 12,
                "objectId": 5262116,
                "path": "2/5262116/12/",
                "pageUrl": "gallery/supertech-capetown-floor-plan-floor-plan-5262116-616046",
                "statusId": 1,
                "createdAt": 1450927153000,
                "sizeInBytes": 153521,
                "width": 1584,
                "height": 968,
                "altText": "Supertech CapeTown Floor Plan (2BHK+2T (1,150 sq ft) + Study Room 1150 sq ft)",
                "title": "Floor Plan (2BHK+2T (1,150 sq ft) + Study Room 1150 sq ft)",
                "description": "",
                "waterMarkName": "616046.jpeg",
                "active": true,
                "activeStatus": 1,
                "seoName": "supertech-capetown-floor-plan-floor-plan-616046.jpeg",
                "absolutePath": "https://im.proptiger-ws.com/2/5262116/12/supertech-capetown-floor-plan-floor-plan-616046.jpeg",
                "imageType": {
                    "id": 12,
                    "objectType": {
                        "id": 2,
                        "type": "property"
                    },
                    "mediaType": {
                        "id": 1,
                        "name": "Image"
                    },
                    "objectTypeId": 2,
                    "mediaTypeId": 1,
                    "type": "floorPlan",
                    "domainId": 2,
                    "priority": 2,
                    "imageSitemapEnabled": 1,
                    "displayName": "Floor Plan"
                },
                "masterImageStatuses": {
                    "id": 1,
                    "status": "PROCESSED"
                }
            }],
            "minResaleOrPrimaryPrice": 6152500.0,
            "maxResaleOrPrimaryPrice": 6152500.0,
            "studyRoom": 1,
            "balcony": 4,
            "displayCarpetArea": 0,
            "createdAt": 1423477332000,
            "updatedAt": 1431903051000,
            "penthouse": false,
            "studio": false,
            "imageTypeCount": {
                "paymentPlan": 2,
                "constructionStatus": 41,
                "masterPlan": 1,
                "locationPlan": 1,
                "main": 2,
                "mainOther": 7
            },
            "media": [{
                "id": 252885,
                "objectMediaTypeId": 102,
                "objectMediaType": {
                    "id": 102,
                    "objectType": {
                        "id": 2,
                        "type": "property"
                    },
                    "mediaType": {
                        "id": 2,
                        "name": "Document"
                    },
                    "objectTypeId": 2,
                    "mediaTypeId": 2,
                    "type": "Panoramic",
                    "domainId": 2,
                    "priority": 1,
                    "imageSitemapEnabled": 0,
                    "displayName": "Tour Zip"
                },
                "objectId": 5262116,
                "priority": 1,
                "sizeInBytes": 9659031,
                "description": "Panorama",
                "active": true,
                "createdAt": 1434550480000,
                "updatedAt": 1434550480000,
                "mediaExtraAttributes": {
                    "indexFile": "vtour/tour.html",
                    "extractPath": "2/2/5262116/102/252885/"
                },
                "absoluteUrl": "https://im.proptiger-ws.com/2/2/5262116/102/252885/252885.zip"
            }, {
                "id": 251190,
                "objectMediaTypeId": 89,
                "objectMediaType": {
                    "id": 89,
                    "objectType": {
                        "id": 2,
                        "type": "property"
                    },
                    "mediaType": {
                        "id": 2,
                        "name": "Document"
                    },
                    "objectTypeId": 2,
                    "mediaTypeId": 2,
                    "type": "3DFloorPlan",
                    "domainId": 2,
                    "priority": 0,
                    "imageSitemapEnabled": 0,
                    "displayName": "3D Floor Plan"
                },
                "objectId": 5262116,
                "priority": 1,
                "sizeInBytes": 129220,
                "description": "Floor Plan",
                "active": true,
                "createdAt": 1434368653000,
                "updatedAt": 1434368653000,
                "mediaExtraAttributes": {
                    "width": 1200.0,
                    "height": 900.0,
                    "svg": "[{\"polygon_class\":\"BR1\",\"name\":\"Bedroom\",\"points\":\"680 102 1028 102 1048 162 1039 167 1011 257 974 258 1001 389 684 387 680 250 680 141 681 101 \",\"per_points\":\"75.56 11.33 114.22 11.33 116.44 18 115.44 18.56 112.33 28.56 108.22 28.67 111.22 43.22 76 43 75.56 27.78 75.56 15.67 75.67 11.22 \",\"tooltip_pos\":\"Bottom\",\"tooltip_left\":\"48\",\"tooltip_top\":\"44\",\"length\":\"12\",\"width\":\"11\"},{\"polygon_class\":\"BR2\",\"name\":\"Bedroom\",\"points\":\"900 714 851 385 580 385 576 719 899 715 \",\"per_points\":\"100 79.33 94.56 42.78 64.44 42.78 64 79.89 99.89 79.44 \",\"tooltip_pos\":\"Top\",\"tooltip_left\":\"40\",\"tooltip_top\":\"23\",\"length\":\"10\",\"width\":\"12\"},{\"polygon_class\":\"LR\",\"name\":\"Living/dinning room\",\"points\":\"52 389 91 257 64 182 88 126 78 106 80 100 507 101 498 224 507 314 499 321 492 389 449 389 444 366 375 363 369 393 252 387 240 357 204 356 195 387 192 392 50 387 \",\"per_points\":\"5.78 43.22 10.11 28.56 7.11 20.22 9.78 14 8.67 11.78 8.89 11.11 56.33 11.22 55.33 24.89 56.33 34.89 55.44 35.67 54.67 43.22 49.89 43.22 49.33 40.67 41.67 40.33 41 43.67 28 43 26.67 39.67 22.67 39.56 21.67 43 21.33 43.56 5.56 43 \",\"tooltip_pos\":\"Bottom\",\"tooltip_left\":\"21\",\"tooltip_top\":\"45\",\"length\":\"17\",\"width\":\"11\"},{\"polygon_class\":\"SR1\",\"name\":\"Study room\",\"points\":\"588 593 588 385 446 384 443 367 374 368 341 588 587 592 \",\"per_points\":\"65.33 65.89 65.33 42.78 49.56 42.67 49.22 40.78 41.56 40.89 37.89 65.33 65.22 65.78 \",\"tooltip_pos\":\"Top\",\"tooltip_left\":\"28\",\"tooltip_top\":\"22\",\"length\":\"8\",\"width\":\"9\"},{\"polygon_class\":\"KITCHEN\",\"name\":\"Kitchen\",\"points\":\"167 566 170 541 162 523 202 357 240 355 250 385 372 384 351 588 342 587 262 565 167 566 \",\"per_points\":\"18.56 62.89 18.89 60.11 18 58.11 22.44 39.67 26.67 39.44 27.78 42.78 41.33 42.67 39 65.33 38 65.22 29.11 62.78 18.56 62.89 \",\"tooltip_pos\":\"Top\",\"tooltip_left\":\"11\",\"tooltip_top\":\"20\",\"length\":\"6\",\"width\":\"8\"},{\"polygon_class\":\"BATHROOM1\",\"name\":\"Bathroom\",\"points\":\"996 391 1140 390 1181 299 1132 161 1039 159 1039 167 1016 260 968 258 996 391 \",\"per_points\":\"110.67 43.44 126.67 43.33 131.22 33.22 125.78 17.89 115.44 17.67 115.44 18.56 112.89 28.89 107.56 28.67 110.67 43.44 \",\"tooltip_pos\":\"Top\",\"tooltip_left\":\"78\",\"tooltip_top\":\"-2\",\"length\":\"5\",\"width\":\"7\"},{\"polygon_class\":\"BATHROOM2\",\"name\":\"Bathroom\",\"points\":\"550 320 499 321 491 229 497 138 682 138 688 141 688 320 549 320 \",\"per_points\":\"61.11 35.56 55.44 35.67 54.56 25.44 55.22 15.33 75.78 15.33 76.44 15.67 76.44 35.56 61 35.56 \",\"tooltip_pos\":\"Top\",\"tooltip_left\":\"48\",\"tooltip_top\":\"-5\",\"length\":\"7\",\"width\":\"5\"}]"
                },
                "absoluteUrl": "https://im.proptiger-ws.com/2/2/5262116/89/251190.jpg"
            }],
            "isPropertySoldOut": false,
            "propertySourceDomain": ["Proptiger"]
        }, {
            "propertyId": 5000171,
            "projectId": 500773,
            "bedrooms": 3,
            "bathrooms": 2,
            "unitType": "Apartment",
            "unitName": "3BHK+2T",
            "pricePerUnitArea": 5350,
            "size": 1295,
            "measure": "sq ft",
            "URL": "noida/supertech-capetown-sector-74-5000171/3bhk-2t-1295-sqft-apartment",
            "budget": 6928250.0,
            "images": [{
                "id": 565082,
                "imageTypeId": 12,
                "objectId": 5000171,
                "path": "2/5000171/12/",
                "pageUrl": "gallery/supertech-capetown-floor-plan-3bhk-3t-1295-sq-ft-5000171-565082",
                "statusId": 1,
                "createdAt": 1450927153000,
                "sizeInBytes": 316583,
                "width": 1072,
                "height": 1184,
                "altText": "Supertech CapeTown Floor Plan (3BHK+2T (1,295 sq ft) 1295 sq ft)",
                "title": "Floor Plan (3BHK+2T (1,295 sq ft) 1295 sq ft)",
                "description": "",
                "waterMarkName": "565082.jpeg",
                "active": true,
                "activeStatus": 1,
                "seoName": "supertech-capetown-floor-plan-3bhk-3t-1295-sq-ft-565082.jpeg",
                "absolutePath": "https://im.proptiger-ws.com/2/5000171/12/supertech-capetown-floor-plan-3bhk-3t-1295-sq-ft-565082.jpeg",
                "imageType": {
                    "id": 12,
                    "objectType": {
                        "id": 2,
                        "type": "property"
                    },
                    "mediaType": {
                        "id": 1,
                        "name": "Image"
                    },
                    "objectTypeId": 2,
                    "mediaTypeId": 1,
                    "type": "floorPlan",
                    "domainId": 2,
                    "priority": 2,
                    "imageSitemapEnabled": 1,
                    "displayName": "Floor Plan"
                },
                "masterImageStatuses": {
                    "id": 1,
                    "status": "PROCESSED"
                }
            }],
            "minResaleOrPrimaryPrice": 6928250.0,
            "maxResaleOrPrimaryPrice": 6928250.0,
            "studyRoom": 0,
            "balcony": 4,
            "displayCarpetArea": 0,
            "createdAt": 1384366064000,
            "updatedAt": 1431903055000,
            "penthouse": false,
            "studio": false,
            "imageTypeCount": {
                "paymentPlan": 2,
                "constructionStatus": 41,
                "masterPlan": 1,
                "locationPlan": 1,
                "main": 2,
                "mainOther": 7
            },
            "media": [{
                "id": 252886,
                "objectMediaTypeId": 102,
                "objectMediaType": {
                    "id": 102,
                    "objectType": {
                        "id": 2,
                        "type": "property"
                    },
                    "mediaType": {
                        "id": 2,
                        "name": "Document"
                    },
                    "objectTypeId": 2,
                    "mediaTypeId": 2,
                    "type": "Panoramic",
                    "domainId": 2,
                    "priority": 1,
                    "imageSitemapEnabled": 0,
                    "displayName": "Tour Zip"
                },
                "objectId": 5000171,
                "priority": 1,
                "sizeInBytes": 11472273,
                "description": "Panorama",
                "active": true,
                "createdAt": 1434550538000,
                "updatedAt": 1434550538000,
                "mediaExtraAttributes": {
                    "indexFile": "vtour/tour.html",
                    "extractPath": "2/2/5000171/102/252886/"
                },
                "absoluteUrl": "https://im.proptiger-ws.com/2/2/5000171/102/252886/252886.zip"
            }, {
                "id": 251185,
                "objectMediaTypeId": 89,
                "objectMediaType": {
                    "id": 89,
                    "objectType": {
                        "id": 2,
                        "type": "property"
                    },
                    "mediaType": {
                        "id": 2,
                        "name": "Document"
                    },
                    "objectTypeId": 2,
                    "mediaTypeId": 2,
                    "type": "3DFloorPlan",
                    "domainId": 2,
                    "priority": 0,
                    "imageSitemapEnabled": 0,
                    "displayName": "3D Floor Plan"
                },
                "objectId": 5000171,
                "priority": 1,
                "sizeInBytes": 121903,
                "description": "Floor Plan",
                "active": true,
                "createdAt": 1434368653000,
                "updatedAt": 1434368653000,
                "mediaExtraAttributes": {
                    "width": 1200.0,
                    "height": 900.0,
                    "svg": "[{\"polygon_class\":\"BR1\",\"name\":\"Bedroom\",\"points\":\"520 131 512 286 275 283 256 213 235 210 251 126 212 126 229 47 518 47 516 53 519 131 \",\"per_points\":\"57.78 14.56 56.89 31.78 30.56 31.44 28.44 23.67 26.11 23.33 27.89 14 23.56 14 25.44 5.22 57.56 5.22 57.33 5.89 57.67 14.56 \",\"tooltip_pos\":\"Bottom\",\"tooltip_left\":\"33\",\"tooltip_top\":\"33\",\"length\":\"12\",\"width\":\"10.5\"},{\"polygon_class\":\"BR2\",\"name\":\"Bedroom\",\"points\":\"908 510 876 287 657 286 664 490 796 490 798 511 908 510 \",\"per_points\":\"100.89 56.67 97.33 31.89 73 31.78 73.78 54.44 88.44 54.44 88.67 56.78 100.89 56.67 \",\"tooltip_pos\":\"Top\",\"tooltip_left\":\"46\",\"tooltip_top\":\"12\",\"length\":\"10\",\"width\":\"10\"},{\"polygon_class\":\"BR3\",\"name\":\"Bedroom\",\"points\":\"475 779 326 780 360 482 672 484 681 763 475 768 474 778 \",\"per_points\":\"52.78 86.56 36.22 86.67 40 53.56 74.67 53.78 75.67 84.78 52.78 85.33 52.67 86.44 \",\"tooltip_pos\":\"Top\",\"tooltip_left\":\"21\",\"tooltip_top\":\"34\",\"length\":\"13\",\"width\":\"11\"},{\"polygon_class\":\"LR\",\"name\":\"Living/Dinning Room\",\"points\":\"659 293 623 234 622 132 622 54 622 48 988 47 991 53 968 130 1001 290 1002 293 660 292 \",\"per_points\":\"73.22 32.56 69.22 26 69.11 14.67 69.11 6 69.11 5.33 109.78 5.22 110.11 5.89 107.56 14.44 111.22 32.22 111.33 32.56 73.33 32.44 \",\"tooltip_pos\":\"Bottom\",\"tooltip_left\":\"74\",\"tooltip_top\":\"34\",\"length\":\"17.5\",\"width\":\"11\"},{\"polygon_class\":\"KITCHEN\",\"name\":\"Kitchen\",\"points\":\"585 394 587 281 382 279 371 390 584 394 \",\"per_points\":\"65 43.78 65.22 31.22 42.44 31 41.22 43.33 64.89 43.78 \",\"tooltip_pos\":\"Top\",\"tooltip_left\":\"40\",\"tooltip_top\":\"12\",\"length\":\"9.5\",\"width\":\"6\"},{\"polygon_class\":\"BATHROOM1\",\"name\":\"Bathroom\",\"points\":\"629 234 629 130 630 53 628 47 520 49 518 53 519 130 515 234 629 232 \",\"per_points\":\"69.89 26 69.89 14.44 70 5.89 69.78 5.22 57.78 5.44 57.56 5.89 57.67 14.44 57.22 26 69.89 25.78 \",\"tooltip_pos\":\"Bottom\",\"tooltip_left\":\"34\",\"tooltip_top\":\"27\",\"length\":\"5\",\"width\":\"7\"},{\"polygon_class\":\"BATHROOM2\",\"name\":\"Bathroom\",\"points\":\"584 490 585 386 407 385 396 490 582 490 \",\"per_points\":\"64.89 54.44 65 42.89 45.22 42.78 44 54.44 64.67 54.44 \",\"tooltip_pos\":\"Top\",\"tooltip_left\":\"40\",\"tooltip_top\":\"23\",\"length\":\"7.5\",\"width\":\"5\"}]"
                },
                "absoluteUrl": "https://im.proptiger-ws.com/2/2/5000171/89/251185.jpg"
            }],
            "video": [{
                "id": 39,
                "objectMediaTypeId": 106,
                "objectMediaType": {
                    "id": 106,
                    "objectType": {
                        "id": 2,
                        "type": "property"
                    },
                    "mediaType": {
                        "id": 4,
                        "name": "Video"
                    },
                    "objectTypeId": 2,
                    "mediaTypeId": 4,
                    "type": "VideoWalkthrough",
                    "domainId": 2,
                    "priority": 1,
                    "imageSitemapEnabled": 0,
                    "displayName": "Video Walkthrough"
                },
                "objectId": 5000171,
                "priority": 1,
                "sizeInBytes": 16429111,
                "active": true,
                "createdAt": 1433245366000,
                "updatedAt": 1440567578000,
                "mediaExtraAttributes": {
                    "videoUrlDetails": [{
                        "resolution": 270,
                        "bitRate": "300",
                        "url": "http://d10fypcdh7kbf3.cloudfront.net/4/2/5000171/106/39/supertech-capetown-floor-plan-3bhk-3t-1295-sq-ft-5000171.mp4"
                    }]
                },
                "imageUrl": "https://im.proptiger-ws.com/4/2/5000171/106/39/supertech-capetown-floor-plan-3bhk-3t-1295-sq-ft-5000171.jpg",
                "fps": 30,
                "resolution": "960×540",
                "bitRate": 500,
                "duration": 37.7377,
                "image": {
                    "id": 0,
                    "imageTypeId": 0,
                    "objectId": 0,
                    "statusId": 0,
                    "sizeInBytes": 0,
                    "width": 0,
                    "height": 0,
                    "active": false,
                    "activeStatus": 0,
                    "absolutePath": "https://im.proptiger-ws.com/https://im.proptiger-ws.com/4/2/5000171/106/39/supertech-capetown-floor-plan-3bhk-3t-1295-sq-ft-5000171.jpg"
                },
                "status": "Processed",
                "url": "http://d10fypcdh7kbf3.cloudfront.net/4/2/5000171/106/39/supertech-capetown-floor-plan-3bhk-3t-1295-sq-ft-5000171.mp4"
            }],
            "isPropertySoldOut": false,
            "propertySourceDomain": ["Proptiger"]
        }, {
            "propertyId": 5000172,
            "projectId": 500773,
            "bedrooms": 3,
            "bathrooms": 3,
            "unitType": "Apartment",
            "unitName": "3BHK+3T",
            "pricePerUnitArea": 5350,
            "size": 1505,
            "measure": "sq ft",
            "URL": "noida/supertech-capetown-sector-74-5000172/3bhk-3t-1505-sqft-apartment",
            "budget": 8051750.0,
            "images": [{
                "id": 565088,
                "imageTypeId": 12,
                "objectId": 5000172,
                "path": "2/5000172/12/",
                "pageUrl": "gallery/supertech-capetown-floor-plan-3bhk-3t-1505-sq-ft-5000172-565088",
                "statusId": 1,
                "createdAt": 1450927153000,
                "sizeInBytes": 353073,
                "width": 1248,
                "height": 1168,
                "altText": "Supertech CapeTown Floor Plan (3BHK+3T (1,505 sq ft) 1505 sq ft)",
                "title": "Floor Plan (3BHK+3T (1,505 sq ft) 1505 sq ft)",
                "description": "",
                "waterMarkName": "565088.jpeg",
                "active": true,
                "activeStatus": 1,
                "seoName": "supertech-capetown-floor-plan-3bhk-3t-1505-sq-ft-565088.jpeg",
                "absolutePath": "https://im.proptiger-ws.com/2/5000172/12/supertech-capetown-floor-plan-3bhk-3t-1505-sq-ft-565088.jpeg",
                "imageType": {
                    "id": 12,
                    "objectType": {
                        "id": 2,
                        "type": "property"
                    },
                    "mediaType": {
                        "id": 1,
                        "name": "Image"
                    },
                    "objectTypeId": 2,
                    "mediaTypeId": 1,
                    "type": "floorPlan",
                    "domainId": 2,
                    "priority": 2,
                    "imageSitemapEnabled": 1,
                    "displayName": "Floor Plan"
                },
                "masterImageStatuses": {
                    "id": 1,
                    "status": "PROCESSED"
                }
            }],
            "minResaleOrPrimaryPrice": 8051750.0,
            "maxResaleOrPrimaryPrice": 8051750.0,
            "studyRoom": 0,
            "balcony": 4,
            "displayCarpetArea": 0,
            "createdAt": 1384366064000,
            "updatedAt": 1431903063000,
            "penthouse": false,
            "studio": false,
            "imageTypeCount": {
                "paymentPlan": 2,
                "constructionStatus": 41,
                "masterPlan": 1,
                "locationPlan": 1,
                "main": 2,
                "mainOther": 7
            },
            "media": [{
                "id": 253330,
                "objectMediaTypeId": 102,
                "objectMediaType": {
                    "id": 102,
                    "objectType": {
                        "id": 2,
                        "type": "property"
                    },
                    "mediaType": {
                        "id": 2,
                        "name": "Document"
                    },
                    "objectTypeId": 2,
                    "mediaTypeId": 2,
                    "type": "Panoramic",
                    "domainId": 2,
                    "priority": 1,
                    "imageSitemapEnabled": 0,
                    "displayName": "Tour Zip"
                },
                "objectId": 5000172,
                "priority": 1,
                "sizeInBytes": 11790850,
                "description": "Panorama",
                "active": true,
                "createdAt": 1434616874000,
                "updatedAt": 1434616874000,
                "mediaExtraAttributes": {
                    "indexFile": "vtour/tour.html",
                    "extractPath": "2/2/5000172/102/253330/"
                },
                "absoluteUrl": "https://im.proptiger-ws.com/2/2/5000172/102/253330/253330.zip"
            }, {
                "id": 251188,
                "objectMediaTypeId": 89,
                "objectMediaType": {
                    "id": 89,
                    "objectType": {
                        "id": 2,
                        "type": "property"
                    },
                    "mediaType": {
                        "id": 2,
                        "name": "Document"
                    },
                    "objectTypeId": 2,
                    "mediaTypeId": 2,
                    "type": "3DFloorPlan",
                    "domainId": 2,
                    "priority": 0,
                    "imageSitemapEnabled": 0,
                    "displayName": "3D Floor Plan"
                },
                "objectId": 5000172,
                "priority": 1,
                "sizeInBytes": 131748,
                "description": "Floor Plan",
                "active": true,
                "createdAt": 1434368653000,
                "updatedAt": 1434368653000,
                "mediaExtraAttributes": {
                    "width": 1200.0,
                    "height": 900.0,
                    "svg": "[{\"polygon_class\":\"BR1\",\"name\":\"Bedroom\",\"points\":\"684 253 676 104 676 22 676 15 976 17 996 88 989 95 967 177 936 178 954 254 790 254 787 218 748 217 744 253 684 255 \",\"per_points\":\"76 28.11 75.11 11.56 75.11 2.44 75.11 1.67 108.44 1.89 110.67 9.78 109.89 10.56 107.44 19.67 104 19.78 106 28.22 87.78 28.22 87.44 24.22 83.11 24.11 82.67 28.11 76 28.33 \",\"tooltip_pos\":\"Bottom\",\"tooltip_left\":\"48\",\"tooltip_top\":\"30\",\"length\":\"12\",\"width\":\"10.5\"},{\"polygon_class\":\"BR2\",\"name\":\"Bedroom\",\"points\":\"849 754 813 450 492 450 479 707 688 708 692 756 849 755 \",\"per_points\":\"94.33 83.78 90.33 50 54.67 50 53.22 78.56 76.44 78.67 76.89 84 94.33 83.89 \",\"tooltip_pos\":\"Top\",\"tooltip_left\":\"33\",\"tooltip_top\":\"30\",\"length\":\"13\",\"width\":\"11\"},{\"polygon_class\":\"BR3\",\"name\":\"Bedroom\",\"points\":\"340 611 220 612 272 329 508 329 495 568 347 570 338 611 \",\"per_points\":\"37.78 67.89 24.44 68 30.22 36.56 56.44 36.56 55 63.11 38.56 63.33 37.56 67.89 \",\"tooltip_pos\":\"Top\",\"tooltip_left\":\"14\",\"tooltip_top\":\"17\",\"length\":\"10\",\"width\":\"12\"},{\"polygon_class\":\"DR\",\"name\":\"Dinning room\",\"points\":\"581 79 582 121 581 199 586 254 497 334 282 333 273 327 356 233 364 168 352 84 364 78 581 78 \",\"per_points\":\"64.56 8.78 64.67 13.44 64.56 22.11 65.11 28.22 55.22 37.11 31.33 37 30.33 36.33 39.56 25.89 40.44 18.67 39.11 9.33 40.44 8.67 64.56 8.67 \",\"tooltip_pos\":\"Bottom\",\"tooltip_left\":\"32\",\"tooltip_top\":\"38\",\"length\":\"10\",\"width\":\"10.5\"},{\"polygon_class\":\"LR\",\"name\":\"Living room\",\"points\":\"364 234 273 327 214 333 111 333 80 251 70 253 144 17 373 15 362 78 363 84 375 167 364 233 \",\"per_points\":\"40.44 26 30.33 36.33 23.78 37 12.33 37 8.89 27.89 7.78 28.11 16 1.89 41.44 1.67 40.22 8.67 40.33 9.33 41.67 18.56 40.44 25.89 \",\"tooltip_pos\":\"Bottom\",\"tooltip_left\":\"8\",\"tooltip_top\":\"39\",\"length\":\"10.5\",\"width\":\"15.5\"},{\"polygon_class\":\"KITCHEN\",\"name\":\"Kitchen\",\"points\":\"804 357 792 256 788 221 747 220 744 247 585 247 584 360 804 359 \",\"per_points\":\"89.33 39.67 88 28.44 87.56 24.56 83 24.44 82.67 27.44 65 27.44 64.89 40 89.33 39.89 \",\"tooltip_pos\":\"Top\",\"tooltip_left\":\"54\",\"tooltip_top\":\"5\",\"length\":\"9.5\",\"width\":\"6\"},{\"polygon_class\":\"BATHROOM1\",\"name\":\"Bathroom\",\"points\":\"777 457 764 351 584 351 582 456 776 457 \",\"per_points\":\"86.33 50.78 84.89 39 64.89 39 64.67 50.67 86.22 50.78 \",\"tooltip_pos\":\"Top\",\"tooltip_left\":\"55\",\"tooltip_top\":\"19\",\"length\":\"7.5\",\"width\":\"5\"},{\"polygon_class\":\"BATHROOM2\",\"name\":\"Bathroom\",\"points\":\"684 103 689 201 574 203 572 121 574 17 686 17 685 22 684 103 \",\"per_points\":\"76 11.44 76.56 22.33 63.78 22.56 63.56 13.44 63.78 1.89 76.22 1.89 76.11 2.44 76 11.44 \",\"tooltip_pos\":\"Bottom\",\"tooltip_left\":\"39\",\"tooltip_top\":\"24\",\"length\":\"5\",\"width\":\"7\"}]"
                },
                "absoluteUrl": "https://im.proptiger-ws.com/2/2/5000172/89/251188.jpg"
            }],
            "video": [{
                "id": 13,
                "objectMediaTypeId": 106,
                "objectMediaType": {
                    "id": 106,
                    "objectType": {
                        "id": 2,
                        "type": "property"
                    },
                    "mediaType": {
                        "id": 4,
                        "name": "Video"
                    },
                    "objectTypeId": 2,
                    "mediaTypeId": 4,
                    "type": "VideoWalkthrough",
                    "domainId": 2,
                    "priority": 1,
                    "imageSitemapEnabled": 0,
                    "displayName": "Video Walkthrough"
                },
                "objectId": 5000172,
                "priority": 1,
                "sizeInBytes": 3227553,
                "active": true,
                "createdAt": 1433245313000,
                "updatedAt": 1440567577000,
                "mediaExtraAttributes": {
                    "videoUrlDetails": [{
                        "resolution": 270,
                        "bitRate": "300",
                        "url": "http://d10fypcdh7kbf3.cloudfront.net/4/2/5000172/106/13/supertech-capetown-floor-plan-3bhk-3t-1505-sq-ft-5000172.mp4"
                    }]
                },
                "imageUrl": "https://im.proptiger-ws.com/4/2/5000172/106/13/supertech-capetown-floor-plan-3bhk-3t-1505-sq-ft-5000172.jpg",
                "fps": 30,
                "resolution": "960×540",
                "bitRate": 500,
                "duration": 27.093733,
                "image": {
                    "id": 0,
                    "imageTypeId": 0,
                    "objectId": 0,
                    "statusId": 0,
                    "sizeInBytes": 0,
                    "width": 0,
                    "height": 0,
                    "active": false,
                    "activeStatus": 0,
                    "absolutePath": "https://im.proptiger-ws.com/https://im.proptiger-ws.com/4/2/5000172/106/13/supertech-capetown-floor-plan-3bhk-3t-1505-sq-ft-5000172.jpg"
                },
                "status": "Processed",
                "url": "http://d10fypcdh7kbf3.cloudfront.net/4/2/5000172/106/13/supertech-capetown-floor-plan-3bhk-3t-1505-sq-ft-5000172.mp4"
            }],
            "isPropertySoldOut": false,
            "propertySourceDomain": ["Proptiger"]
        }, {
            "propertyId": 5000173,
            "projectId": 500773,
            "bedrooms": 3,
            "bathrooms": 3,
            "unitType": "Apartment",
            "unitName": "3BHK+3T",
            "pricePerUnitArea": 5350,
            "size": 1625,
            "measure": "sq ft",
            "URL": "noida/supertech-capetown-sector-74-5000173/3bhk-3t-1625-sqft-apartment",
            "budget": 8693750.0,
            "images": [{
                "id": 565087,
                "imageTypeId": 12,
                "objectId": 5000173,
                "path": "2/5000173/12/",
                "pageUrl": "gallery/supertech-capetown-floor-plan-3bhk-3t-1625-sq-ft-5000173-565087",
                "statusId": 1,
                "createdAt": 1450927153000,
                "sizeInBytes": 382184,
                "width": 1584,
                "height": 888,
                "altText": "Supertech CapeTown Floor Plan (3BHK+3T (1,625 sq ft) 1625 sq ft)",
                "title": "Floor Plan (3BHK+3T (1,625 sq ft) 1625 sq ft)",
                "description": "",
                "waterMarkName": "565087.jpeg",
                "active": true,
                "activeStatus": 1,
                "seoName": "supertech-capetown-floor-plan-3bhk-3t-1625-sq-ft-565087.jpeg",
                "absolutePath": "https://im.proptiger-ws.com/2/5000173/12/supertech-capetown-floor-plan-3bhk-3t-1625-sq-ft-565087.jpeg",
                "imageType": {
                    "id": 12,
                    "objectType": {
                        "id": 2,
                        "type": "property"
                    },
                    "mediaType": {
                        "id": 1,
                        "name": "Image"
                    },
                    "objectTypeId": 2,
                    "mediaTypeId": 1,
                    "type": "floorPlan",
                    "domainId": 2,
                    "priority": 2,
                    "imageSitemapEnabled": 1,
                    "displayName": "Floor Plan"
                },
                "masterImageStatuses": {
                    "id": 1,
                    "status": "PROCESSED"
                }
            }],
            "minResaleOrPrimaryPrice": 8693750.0,
            "maxResaleOrPrimaryPrice": 8693750.0,
            "studyRoom": 0,
            "balcony": 3,
            "displayCarpetArea": 0,
            "createdAt": 1384366064000,
            "updatedAt": 1431903060000,
            "penthouse": false,
            "studio": false,
            "imageTypeCount": {
                "paymentPlan": 2,
                "constructionStatus": 41,
                "masterPlan": 1,
                "locationPlan": 1,
                "main": 2,
                "mainOther": 7
            },
            "media": [{
                "id": 252889,
                "objectMediaTypeId": 102,
                "objectMediaType": {
                    "id": 102,
                    "objectType": {
                        "id": 2,
                        "type": "property"
                    },
                    "mediaType": {
                        "id": 2,
                        "name": "Document"
                    },
                    "objectTypeId": 2,
                    "mediaTypeId": 2,
                    "type": "Panoramic",
                    "domainId": 2,
                    "priority": 1,
                    "imageSitemapEnabled": 0,
                    "displayName": "Tour Zip"
                },
                "objectId": 5000173,
                "priority": 1,
                "sizeInBytes": 10391063,
                "description": "Panorama",
                "active": true,
                "createdAt": 1434550690000,
                "updatedAt": 1434550690000,
                "mediaExtraAttributes": {
                    "indexFile": "vtour/tour.html",
                    "extractPath": "2/2/5000173/102/252889/"
                },
                "absoluteUrl": "https://im.proptiger-ws.com/2/2/5000173/102/252889/252889.zip"
            }, {
                "id": 251184,
                "objectMediaTypeId": 89,
                "objectMediaType": {
                    "id": 89,
                    "objectType": {
                        "id": 2,
                        "type": "property"
                    },
                    "mediaType": {
                        "id": 2,
                        "name": "Document"
                    },
                    "objectTypeId": 2,
                    "mediaTypeId": 2,
                    "type": "3DFloorPlan",
                    "domainId": 2,
                    "priority": 0,
                    "imageSitemapEnabled": 0,
                    "displayName": "3D Floor Plan"
                },
                "objectId": 5000173,
                "priority": 1,
                "sizeInBytes": 98948,
                "description": "Floor Plan",
                "active": true,
                "createdAt": 1434368653000,
                "updatedAt": 1434368653000,
                "mediaExtraAttributes": {
                    "width": 1200.0,
                    "height": 900.0,
                    "svg": "[{\"polygon_class\":\"BR1\",\"name\":\"Bedroom\",\"points\":\"421 612 449 382 644 381 651 574 532 576 528 613 421 611 \",\"per_points\":\"46.78 68 49.89 42.44 71.56 42.33 72.33 63.78 59.11 64 58.67 68.11 46.78 67.89 \",\"tooltip_pos\":\"Top\",\"tooltip_left\":\"29\",\"tooltip_top\":\"23\",\"length\":\"10\",\"width\":\"12\"},{\"polygon_class\":\"BR2\",\"name\":\"Bedroom\",\"points\":\"645 612 637 381 836 381 884 653 772 653 767 612 645 611 \",\"per_points\":\"71.67 68 70.78 42.33 92.89 42.33 98.22 72.56 85.78 72.56 85.22 68 71.67 67.89 \",\"tooltip_pos\":\"Top\",\"tooltip_left\":\"61\",\"tooltip_top\":\"23\",\"length\":\"10\",\"width\":\"13\"},{\"polygon_class\":\"BR3\",\"name\":\"Bedroom\",\"points\":\"764 166 780 268 765 333 773 386 1032 385 1075 321 1084 321 1048 228 1078 226 1051 164 764 165 \",\"per_points\":\"84.89 18.44 86.67 29.78 85 37 85.89 42.89 114.67 42.78 119.44 35.67 120.44 35.67 116.44 25.33 119.78 25.11 116.78 18.22 84.89 18.33 \",\"tooltip_pos\":\"Top\",\"tooltip_left\":\"55\",\"tooltip_top\":\"-1\",\"length\":\"14\",\"width\":\"11.17\"},{\"polygon_class\":\"LR\",\"name\":\"Living room\",\"points\":\"164 165 462 165 461 170 448 264 461 331 456 386 201 388 196 379 150 373 144 371 180 277 155 230 170 191 163 174 163 164 \",\"per_points\":\"18.22 18.33 51.33 18.33 51.22 18.89 49.78 29.33 51.22 36.78 50.67 42.89 22.33 43.11 21.78 42.11 16.67 41.44 16 41.22 20 30.78 17.22 25.56 18.89 21.22 18.11 19.33 18.11 18.22 \",\"tooltip_pos\":\"Top\",\"tooltip_left\":\"10\",\"tooltip_top\":\"-1\",\"length\":\"16.33\",\"width\":\"11.17\"},{\"polygon_class\":\"DR\",\"name\":\"Dinning room\",\"points\":\"435 553 234 553 265 435 450 379 456 385 435 549 \",\"per_points\":\"48.33 61.44 26 61.44 29.44 48.33 50 42.11 50.67 42.78 48.33 61 \",\"tooltip_pos\":\"Top\",\"tooltip_left\":\"29\",\"tooltip_top\":\"23\",\"length\":\"9\",\"width\":\"10.42\"},{\"polygon_class\":\"KITCHEN\",\"name\":\"Kitchen\",\"points\":\"241 553 285 379 195 379 143 371 133 396 136 401 81 552 241 552 \",\"per_points\":\"26.78 61.44 31.67 42.11 21.67 42.11 15.89 41.22 14.78 44 15.11 44.56 9 61.33 26.78 61.33 \",\"tooltip_pos\":\"Top\",\"tooltip_left\":\"15\",\"tooltip_top\":\"23\",\"length\":\"7\",\"width\":\"10\"},{\"polygon_class\":\"BATHROOM1\",\"name\":\"Bathroom\",\"points\":\"556 330 557 305 553 240 555 165 456 166 441 265 456 333 556 328 \",\"per_points\":\"61.78 36.67 61.89 33.89 61.44 26.67 61.67 18.33 50.67 18.44 49 29.44 50.67 37 61.78 36.44 \",\"tooltip_pos\":\"Top\",\"tooltip_left\":\"37\",\"tooltip_top\":\"-1\",\"length\":\"5\",\"width\":\"7.33\"},{\"polygon_class\":\"BATHROOM2\",\"name\":\"Bathroom\",\"points\":\"671 165 676 239 672 328 772 329 785 269 769 165 671 165 \",\"per_points\":\"74.56 18.33 75.11 26.56 74.67 36.44 85.78 36.56 87.22 29.89 85.44 18.33 74.56 18.33 \",\"tooltip_pos\":\"Top\",\"tooltip_left\":\"47\",\"tooltip_top\":\"-1\",\"length\":\"5\",\"width\":\"7.33\"},{\"polygon_class\":\"BATHROOM3\",\"name\":\"Bathroom\",\"points\":\"851 505 962 505 930 380 828 381 852 503 \",\"per_points\":\"94.56 56.11 106.89 56.11 103.33 42.22 92 42.33 94.67 55.89 \",\"tooltip_pos\":\"Top\",\"tooltip_left\":\"68\",\"tooltip_top\":\"23\",\"length\":\"5\",\"width\":\"7.33\"}]"
                },
                "absoluteUrl": "https://im.proptiger-ws.com/2/2/5000173/89/251184.jpg"
            }],
            "video": [{
                "id": 5,
                "objectMediaTypeId": 106,
                "objectMediaType": {
                    "id": 106,
                    "objectType": {
                        "id": 2,
                        "type": "property"
                    },
                    "mediaType": {
                        "id": 4,
                        "name": "Video"
                    },
                    "objectTypeId": 2,
                    "mediaTypeId": 4,
                    "type": "VideoWalkthrough",
                    "domainId": 2,
                    "priority": 1,
                    "imageSitemapEnabled": 0,
                    "displayName": "Video Walkthrough"
                },
                "objectId": 5000173,
                "priority": 1,
                "sizeInBytes": 8299505,
                "active": true,
                "createdAt": 1433242109000,
                "updatedAt": 1440567578000,
                "mediaExtraAttributes": {
                    "videoUrlDetails": [{
                        "resolution": 270,
                        "bitRate": "300",
                        "url": "http://d10fypcdh7kbf3.cloudfront.net/4/2/5000173/106/5/supertech-capetown-floor-plan-3bhk-3t-1625-sq-ft-5000173.mp4"
                    }]
                },
                "imageUrl": "https://im.proptiger-ws.com/4/2/5000173/106/5/supertech-capetown-floor-plan-3bhk-3t-1625-sq-ft-5000173.jpg",
                "fps": 30,
                "resolution": "960×540",
                "bitRate": 500,
                "duration": 39.8398,
                "image": {
                    "id": 0,
                    "imageTypeId": 0,
                    "objectId": 0,
                    "statusId": 0,
                    "sizeInBytes": 0,
                    "width": 0,
                    "height": 0,
                    "active": false,
                    "activeStatus": 0,
                    "absolutePath": "https://im.proptiger-ws.com/https://im.proptiger-ws.com/4/2/5000173/106/5/supertech-capetown-floor-plan-3bhk-3t-1625-sq-ft-5000173.jpg"
                },
                "status": "Processed",
                "url": "http://d10fypcdh7kbf3.cloudfront.net/4/2/5000173/106/5/supertech-capetown-floor-plan-3bhk-3t-1625-sq-ft-5000173.mp4"
            }],
            "isPropertySoldOut": false,
            "propertySourceDomain": ["Proptiger"]
        }, {
            "propertyId": 5000174,
            "projectId": 500773,
            "bedrooms": 3,
            "bathrooms": 3,
            "unitType": "Apartment",
            "unitName": "3BHK+3T Study Room",
            "pricePerUnitArea": 5350,
            "size": 1945,
            "measure": "sq ft",
            "URL": "noida/supertech-capetown-sector-74-5000174/3bhk-3t-1945-sqft-apartment",
            "budget": 1.040575E7,
            "images": [{
                "id": 565083,
                "imageTypeId": 12,
                "objectId": 5000174,
                "path": "2/5000174/12/",
                "pageUrl": "gallery/supertech-capetown-floor-plan-3bhk-3t-1945-sq-ft-5000174-565083",
                "statusId": 1,
                "createdAt": 1450927153000,
                "sizeInBytes": 370592,
                "width": 1584,
                "height": 800,
                "altText": "Supertech CapeTown Floor Plan (3BHK+3T (1,945 sq ft) + Study Room 1945 sq ft)",
                "title": "Floor Plan (3BHK+3T (1,945 sq ft) + Study Room 1945 sq ft)",
                "description": "",
                "waterMarkName": "565083.jpeg",
                "active": true,
                "activeStatus": 1,
                "seoName": "supertech-capetown-floor-plan-3bhk-3t-1945-sq-ft-565083.jpeg",
                "absolutePath": "https://im.proptiger-ws.com/2/5000174/12/supertech-capetown-floor-plan-3bhk-3t-1945-sq-ft-565083.jpeg",
                "imageType": {
                    "id": 12,
                    "objectType": {
                        "id": 2,
                        "type": "property"
                    },
                    "mediaType": {
                        "id": 1,
                        "name": "Image"
                    },
                    "objectTypeId": 2,
                    "mediaTypeId": 1,
                    "type": "floorPlan",
                    "domainId": 2,
                    "priority": 2,
                    "imageSitemapEnabled": 1,
                    "displayName": "Floor Plan"
                },
                "masterImageStatuses": {
                    "id": 1,
                    "status": "PROCESSED"
                }
            }],
            "minResaleOrPrimaryPrice": 1.040575E7,
            "maxResaleOrPrimaryPrice": 1.040575E7,
            "studyRoom": 1,
            "balcony": 4,
            "displayCarpetArea": 0,
            "createdAt": 1384366064000,
            "updatedAt": 1431903066000,
            "penthouse": false,
            "studio": false,
            "imageTypeCount": {
                "paymentPlan": 2,
                "constructionStatus": 41,
                "masterPlan": 1,
                "locationPlan": 1,
                "main": 2,
                "mainOther": 7
            },
            "media": [{
                "id": 252893,
                "objectMediaTypeId": 102,
                "objectMediaType": {
                    "id": 102,
                    "objectType": {
                        "id": 2,
                        "type": "property"
                    },
                    "mediaType": {
                        "id": 2,
                        "name": "Document"
                    },
                    "objectTypeId": 2,
                    "mediaTypeId": 2,
                    "type": "Panoramic",
                    "domainId": 2,
                    "priority": 1,
                    "imageSitemapEnabled": 0,
                    "displayName": "Tour Zip"
                },
                "objectId": 5000174,
                "priority": 1,
                "sizeInBytes": 18826457,
                "description": "Panorama",
                "active": true,
                "createdAt": 1434550899000,
                "updatedAt": 1434550899000,
                "mediaExtraAttributes": {
                    "indexFile": "vtour/tour.html",
                    "extractPath": "2/2/5000174/102/252893/"
                },
                "absoluteUrl": "https://im.proptiger-ws.com/2/2/5000174/102/252893/252893.zip"
            }, {
                "id": 251187,
                "objectMediaTypeId": 89,
                "objectMediaType": {
                    "id": 89,
                    "objectType": {
                        "id": 2,
                        "type": "property"
                    },
                    "mediaType": {
                        "id": 2,
                        "name": "Document"
                    },
                    "objectTypeId": 2,
                    "mediaTypeId": 2,
                    "type": "3DFloorPlan",
                    "domainId": 2,
                    "priority": 0,
                    "imageSitemapEnabled": 0,
                    "displayName": "3D Floor Plan"
                },
                "objectId": 5000174,
                "priority": 1,
                "sizeInBytes": 120594,
                "description": "Floor Plan",
                "active": true,
                "createdAt": 1434368653000,
                "updatedAt": 1434368653000,
                "mediaExtraAttributes": {
                    "width": 1200.0,
                    "height": 900.0,
                    "svg": "[{\"polygon_class\":\"BR1\",\"name\":\"Bedroom\",\"points\":\"743 617 728 418 543 420 534 633 632 633 742 616 \",\"per_points\":\"82.56 68.56 80.89 46.44 60.33 46.67 59.33 70.33 70.22 70.33 82.44 68.44 \",\"tooltip_pos\":\"Top\",\"tooltip_left\":\"52\",\"tooltip_top\":\"27\",\"length\":\"10\",\"width\":\"12\"},{\"polygon_class\":\"BR2\",\"name\":\"Bedroom\",\"points\":\"736 617 722 418 909 419 950 648 852 652 736 616 \",\"per_points\":\"81.78 68.56 80.22 46.44 101 46.56 105.56 72 94.67 72.44 81.78 68.44 \",\"tooltip_pos\":\"Top\",\"tooltip_left\":\"66\",\"tooltip_top\":\"27\",\"length\":\"10\",\"width\":\"13\"},{\"polygon_class\":\"BR3\",\"name\":\"Bedroom\",\"points\":\"794 423 790 368 856 367 844 285 845 219 844 213 1102 212 1126 282 1159 282 1185 356 1158 421 996 423 792 424 \",\"per_points\":\"88.22 47 87.78 40.89 95.11 40.78 93.78 31.67 93.89 24.33 93.78 23.67 122.44 23.56 125.11 31.33 128.78 31.33 131.67 39.56 128.67 46.78 110.67 47 88 47.11 \",\"tooltip_pos\":\"Top\",\"tooltip_left\":\"62\",\"tooltip_top\":\"4\",\"length\":\"14\",\"width\":\"11.17\"},{\"polygon_class\":\"LR\",\"name\":\"Living room\",\"points\":\"548 424 560 366 560 309 561 245 560 213 276 214 276 217 284 300 268 325 248 423 547 421 \",\"per_points\":\"60.89 47.11 62.22 40.67 62.22 34.33 62.33 27.22 62.22 23.67 30.67 23.78 30.67 24.11 31.56 33.33 29.78 36.11 27.56 47 60.78 46.78 \",\"tooltip_pos\":\"Top\",\"tooltip_left\":\"38\",\"tooltip_top\":\"4\",\"length\":\"17\",\"width\":\"11.17\"},{\"polygon_class\":\"DR\",\"name\":\"Dinning room\",\"points\":\"542 633 549 420 549 418 377 418 355 576 534 632 541 633 \",\"per_points\":\"60.22 70.33 61 46.67 61 46.44 41.89 46.44 39.44 64 59.33 70.22 60.11 70.33 \",\"tooltip_pos\":\"Top\",\"tooltip_left\":\"24\",\"tooltip_top\":\"27\",\"length\":\"9\",\"width\":\"11\"},{\"polygon_class\":\"KITCHEN\",\"name\":\"Kitchen\",\"points\":\"364 576 383 416 249 417 231 505 224 504 215 542 220 561 216 576 362 576 \",\"per_points\":\"40.44 64 42.56 46.22 27.67 46.33 25.67 56.11 24.89 56 23.89 60.22 24.44 62.33 24 64 40.22 64 \",\"tooltip_pos\":\"Top\",\"tooltip_left\":\"13\",\"tooltip_top\":\"27\",\"length\":\"7\",\"width\":\"10.5\"},{\"polygon_class\":\"SR1\",\"name\":\"Study room\",\"points\":\"243 480 113 480 133 400 116 342 112 338 129 275 136 277 157 347 269 353 243 480 \",\"per_points\":\"27 53.33 12.56 53.33 14.78 44.44 12.89 38 12.44 37.56 14.33 30.56 15.11 30.78 17.44 38.56 29.89 39.22 27 53.33 \",\"tooltip_pos\":\"Top\",\"tooltip_left\":\"14\",\"tooltip_top\":\"19\",\"length\":\"6.5\",\"width\":\"9\"},{\"polygon_class\":\"EL\",\"name\":\"Entracne Lobby\",\"points\":\"268 356 284 297 286 219 288 215 152 213 148 219 126 288 132 289 149 346 162 353 204 357 268 357 \",\"per_points\":\"29.78 39.56 31.56 33 31.78 24.33 32 23.89 16.89 23.67 16.44 24.33 14 32 14.67 32.11 16.56 38.44 18 39.22 22.67 39.67 29.78 39.67 \",\"tooltip_pos\":\"Top\",\"tooltip_left\":\"4\",\"tooltip_top\":\"4\",\"length\":\"6.5\",\"width\":\"6\"},{\"polygon_class\":\"BATHROOM1\",\"name\":\"Bathroom\",\"points\":\"119 480 138 407 115 341 111 337 33 339 4 417 36 483 119 481 \",\"per_points\":\"13.22 53.33 15.33 45.22 12.78 37.89 12.33 37.44 3.67 37.67 0.44 46.33 4 53.67 13.22 53.44 \",\"tooltip_pos\":\"Top\",\"tooltip_left\":\"1\",\"tooltip_top\":\"18\",\"length\":\"4\",\"width\":\"5\"},{\"polygon_class\":\"BATHROOM2\",\"name\":\"Bathroom\",\"points\":\"925 539 1029 537 1002 417 901 417 924 540 \",\"per_points\":\"102.78 59.89 114.33 59.67 111.33 46.33 100.11 46.33 102.67 60 \",\"tooltip_pos\":\"Top\",\"tooltip_left\":\"75\",\"tooltip_top\":\"27\",\"length\":\"5\",\"width\":\"8\"},{\"polygon_class\":\"BATHROOM3\",\"name\":\"bathroom\",\"points\":\"692 373 689 309 688 242 687 239 752 238 751 212 850 213 851 219 849 285 864 373 692 373 \",\"per_points\":\"76.89 41.44 76.56 34.33 76.44 26.89 76.33 26.56 83.56 26.44 83.44 23.56 94.44 23.67 94.56 24.33 94.33 31.67 96 41.44 76.89 41.44 \",\"tooltip_pos\":\"Top\",\"tooltip_left\":\"62\",\"tooltip_top\":\"4\",\"length\":\"9.5\",\"width\":\"7.33\"},{\"polygon_class\":\"BATHROOM4\",\"name\":\"Bathroom\",\"points\":\"699 373 695 310 694 243 694 239 563 237 552 242 556 306 554 374 697 373 \",\"per_points\":\"77.67 41.44 77.22 34.44 77.11 27 77.11 26.56 62.56 26.33 61.33 26.89 61.78 34 61.56 41.56 77.44 41.44 \",\"tooltip_pos\":\"Top\",\"tooltip_left\":\"48\",\"tooltip_top\":\"7\",\"length\":\"8\",\"width\":\"5\"}]"
                },
                "absoluteUrl": "https://im.proptiger-ws.com/2/2/5000174/89/251187.jpg"
            }],
            "video": [{
                "id": 41,
                "objectMediaTypeId": 106,
                "objectMediaType": {
                    "id": 106,
                    "objectType": {
                        "id": 2,
                        "type": "property"
                    },
                    "mediaType": {
                        "id": 4,
                        "name": "Video"
                    },
                    "objectTypeId": 2,
                    "mediaTypeId": 4,
                    "type": "VideoWalkthrough",
                    "domainId": 2,
                    "priority": 1,
                    "imageSitemapEnabled": 0,
                    "displayName": "Video Walkthrough"
                },
                "objectId": 5000174,
                "priority": 1,
                "sizeInBytes": 10464506,
                "active": true,
                "createdAt": 1433245371000,
                "updatedAt": 1440567578000,
                "mediaExtraAttributes": {
                    "videoUrlDetails": [{
                        "resolution": 270,
                        "bitRate": "300",
                        "url": "http://d10fypcdh7kbf3.cloudfront.net/4/2/5000174/106/41/supertech-capetown-floor-plan-3bhk-3t-1945-sq-ft-5000174.mp4"
                    }]
                },
                "imageUrl": "https://im.proptiger-ws.com/4/2/5000174/106/41/supertech-capetown-floor-plan-3bhk-3t-1945-sq-ft-5000174.jpg",
                "fps": 30,
                "resolution": "960×540",
                "bitRate": 500,
                "duration": 39.272567,
                "image": {
                    "id": 0,
                    "imageTypeId": 0,
                    "objectId": 0,
                    "statusId": 0,
                    "sizeInBytes": 0,
                    "width": 0,
                    "height": 0,
                    "active": false,
                    "activeStatus": 0,
                    "absolutePath": "https://im.proptiger-ws.com/https://im.proptiger-ws.com/4/2/5000174/106/41/supertech-capetown-floor-plan-3bhk-3t-1945-sq-ft-5000174.jpg"
                },
                "status": "Processed",
                "url": "http://d10fypcdh7kbf3.cloudfront.net/4/2/5000174/106/41/supertech-capetown-floor-plan-3bhk-3t-1945-sq-ft-5000174.mp4"
            }],
            "isPropertySoldOut": false,
            "propertySourceDomain": ["Proptiger"]
        }, {
            "propertyId": 5000175,
            "projectId": 500773,
            "bedrooms": 4,
            "bathrooms": 4,
            "unitType": "Apartment",
            "unitName": "4BHK+4T Servant Room",
            "pricePerUnitArea": 5350,
            "size": 2385,
            "measure": "sq ft",
            "URL": "noida/supertech-capetown-sector-74-5000175/4bhk-4t-2385-sqft-apartment",
            "budget": 1.275975E7,
            "images": [{
                "id": 565084,
                "imageTypeId": 12,
                "objectId": 5000175,
                "path": "2/5000175/12/",
                "pageUrl": "gallery/supertech-capetown-floor-plan-4bhk-4t-2385-sq-ft-5000175-565084",
                "statusId": 1,
                "createdAt": 1450927153000,
                "sizeInBytes": 319403,
                "width": 1168,
                "height": 1200,
                "altText": "Supertech CapeTown Floor Plan (4BHK+4T (2,385 sq ft) + Servant Room 2385 sq ft)",
                "title": "Floor Plan (4BHK+4T (2,385 sq ft) + Servant Room 2385 sq ft)",
                "description": "",
                "waterMarkName": "565084.jpeg",
                "active": true,
                "activeStatus": 1,
                "seoName": "supertech-capetown-floor-plan-4bhk-4t-2385-sq-ft-565084.jpeg",
                "absolutePath": "https://im.proptiger-ws.com/2/5000175/12/supertech-capetown-floor-plan-4bhk-4t-2385-sq-ft-565084.jpeg",
                "imageType": {
                    "id": 12,
                    "objectType": {
                        "id": 2,
                        "type": "property"
                    },
                    "mediaType": {
                        "id": 1,
                        "name": "Image"
                    },
                    "objectTypeId": 2,
                    "mediaTypeId": 1,
                    "type": "floorPlan",
                    "domainId": 2,
                    "priority": 2,
                    "imageSitemapEnabled": 1,
                    "displayName": "Floor Plan"
                },
                "masterImageStatuses": {
                    "id": 1,
                    "status": "PROCESSED"
                }
            }],
            "servantRoom": 1,
            "minResaleOrPrimaryPrice": 1.275975E7,
            "maxResaleOrPrimaryPrice": 1.275975E7,
            "studyRoom": 0,
            "balcony": 4,
            "displayCarpetArea": 0,
            "createdAt": 1384366064000,
            "updatedAt": 1431903072000,
            "penthouse": false,
            "studio": false,
            "imageTypeCount": {
                "paymentPlan": 2,
                "constructionStatus": 41,
                "masterPlan": 1,
                "locationPlan": 1,
                "main": 2,
                "mainOther": 7
            },
            "media": [{
                "id": 252895,
                "objectMediaTypeId": 102,
                "objectMediaType": {
                    "id": 102,
                    "objectType": {
                        "id": 2,
                        "type": "property"
                    },
                    "mediaType": {
                        "id": 2,
                        "name": "Document"
                    },
                    "objectTypeId": 2,
                    "mediaTypeId": 2,
                    "type": "Panoramic",
                    "domainId": 2,
                    "priority": 1,
                    "imageSitemapEnabled": 0,
                    "displayName": "Tour Zip"
                },
                "objectId": 5000175,
                "priority": 1,
                "sizeInBytes": 23841842,
                "description": "Panorama",
                "active": true,
                "createdAt": 1434550978000,
                "updatedAt": 1434550978000,
                "mediaExtraAttributes": {
                    "indexFile": "vtour/tour.html",
                    "extractPath": "2/2/5000175/102/252895/"
                },
                "absoluteUrl": "https://im.proptiger-ws.com/2/2/5000175/102/252895/252895.zip"
            }, {
                "id": 251189,
                "objectMediaTypeId": 89,
                "objectMediaType": {
                    "id": 89,
                    "objectType": {
                        "id": 2,
                        "type": "property"
                    },
                    "mediaType": {
                        "id": 2,
                        "name": "Document"
                    },
                    "objectTypeId": 2,
                    "mediaTypeId": 2,
                    "type": "3DFloorPlan",
                    "domainId": 2,
                    "priority": 0,
                    "imageSitemapEnabled": 0,
                    "displayName": "3D Floor Plan"
                },
                "objectId": 5000175,
                "priority": 1,
                "sizeInBytes": 123706,
                "description": "Floor Plan",
                "active": true,
                "createdAt": 1434368653000,
                "updatedAt": 1434368653000,
                "mediaExtraAttributes": {
                    "width": 1200.0,
                    "height": 900.0,
                    "svg": "[{\"polygon_class\":\"BR1\",\"name\":\"Bedroom\",\"points\":\"168 346 212 221 184 164 188 160 290 159 299 160 323 197 323 199 379 200 348 353 346 355 209 355 168 355 168 345 \",\"per_points\":\"18.67 38.44 23.56 24.56 20.44 18.22 20.89 17.78 32.22 17.67 33.22 17.78 35.89 21.89 35.89 22.11 42.11 22.22 38.67 39.22 38.44 39.44 23.22 39.44 18.67 39.44 18.67 38.33 \",\"tooltip_pos\":\"Top\",\"tooltip_left\":\"23\",\"tooltip_top\":\"3\",\"length\":\"10.5\",\"width\":\"12\"},{\"polygon_class\":\"BR2\",\"name\":\"Bedroom\",\"points\":\"681 422 671 271 745 271 758 222 922 220 930 220 918 275 961 422 682 423 679 422 \",\"per_points\":\"75.67 46.89 74.56 30.11 82.78 30.11 84.22 24.67 102.44 24.44 103.33 24.44 102 30.56 106.78 46.89 75.78 47 75.44 46.89 \",\"tooltip_pos\":\"Top\",\"tooltip_left\":\"55\",\"tooltip_top\":\"5\",\"length\":\"16.5\",\"width\":\"12\"},{\"polygon_class\":\"BR3\",\"name\":\"Bedroom\",\"points\":\"519 781 417 781 451 494 661 494 671 740 522 739 520 781 \",\"per_points\":\"57.67 86.78 46.33 86.78 50.11 54.89 73.44 54.89 74.56 82.22 58 82.11 57.78 86.78 \",\"tooltip_pos\":\"Top\",\"tooltip_left\":\"46\",\"tooltip_top\":\"35\",\"length\":\"11.5\",\"width\":\"13\"},{\"polygon_class\":\"BR4\",\"name\":\"Bedroom\",\"points\":\"665 740 655 493 753 495 758 530 854 530 901 781 803 780 797 739 664 739 \",\"per_points\":\"73.89 82.22 72.78 54.78 83.67 55 84.22 58.89 94.89 58.89 100.11 86.78 89.22 86.67 88.56 82.11 73.78 82.11 \",\"tooltip_pos\":\"Top\",\"tooltip_left\":\"46\",\"tooltip_top\":\"36\",\"length\":\"10.5\",\"width\":\"11\"},{\"polygon_class\":\"DR\",\"name\":\"Dressing room\",\"points\":\"1037 219 917 222 914 274 933 339 1026 334 1067 294 1037 219 \",\"per_points\":\"115.22 24.33 101.89 24.67 101.56 30.44 103.67 37.67 114 37.11 118.56 32.67 115.22 24.33 \",\"tooltip_pos\":\"Top\",\"tooltip_left\":\"68\",\"tooltip_top\":\"5\",\"length\":\"6\",\"width\":\"5\"},{\"polygon_class\":\"LR\",\"name\":\"Dinning room\",\"points\":\"687 422 675 270 524 271 522 294 464 373 459 423 685 422 \",\"per_points\":\"76.33 46.89 75 30 58.22 30.11 58 32.67 51.56 41.44 51 47 76.11 46.89 \",\"tooltip_pos\":\"Top\",\"tooltip_left\":\"35\",\"tooltip_top\":\"10\",\"length\":\"13.33\",\"width\":\"12\"},{\"polygon_class\":\"LR2\",\"name\":\"Living room\",\"points\":\"339 375 379 189 535 191 528 293 469 374 418 379 340 377 \",\"per_points\":\"37.67 41.67 42.11 21 59.44 21.22 58.67 32.56 52.11 41.56 46.44 42.11 37.78 41.89 \",\"tooltip_pos\":\"Top\",\"tooltip_left\":\"36\",\"tooltip_top\":\"2\",\"length\":\"11\",\"width\":\"17\"},{\"polygon_class\":\"EL\",\"name\":\"Entrance Lobby\",\"points\":\"379 204 323 202 301 150 297 150 324 64 448 66 444 69 444 123 439 160 435 192 377 203 \",\"per_points\":\"42.11 22.67 35.89 22.44 33.44 16.67 33 16.67 36 7.11 49.78 7.33 49.33 7.67 49.33 13.67 48.78 17.78 48.33 21.33 41.89 22.56 \",\"tooltip_pos\":\"Bottom\",\"tooltip_left\":\"25\",\"tooltip_top\":\"23\",\"length\":\"8\",\"width\":\"7.75\"},{\"polygon_class\":\"KITCHEN\",\"name\":\"Kitchen\",\"points\":\"676 273 671 190 677 134 591 134 592 172 532 169 523 275 675 274 675 272 \",\"per_points\":\"75.11 30.33 74.56 21.11 75.22 14.89 65.67 14.89 65.78 19.11 59.11 18.78 58.11 30.56 75 30.44 75 30.22 \",\"tooltip_pos\":\"Bottom\",\"tooltip_left\":\"34\",\"tooltip_top\":\"32\",\"length\":\"10\",\"width\":\"8\"},{\"polygon_class\":\"SR1\",\"name\":\"Servant room\",\"points\":\"533 191 538 128 537 67 536 64 440 64 440 69 443 123 427 192 532 191 \",\"per_points\":\"59.22 21.22 59.78 14.22 59.67 7.44 59.56 7.11 48.89 7.11 48.89 7.67 49.22 13.67 47.44 21.33 59.11 21.22 \",\"tooltip_pos\":\"Bottom\",\"tooltip_left\":\"34\",\"tooltip_top\":\"22\",\"length\":\"7\",\"width\":\"7.75\"},{\"polygon_class\":\"BATHROOM1\",\"name\":\"Bathroom\",\"points\":\"967 455 930 331 1023 330 1065 292 1071 290 1121 408 1077 455 967 455 \",\"per_points\":\"107.44 50.56 103.33 36.78 113.67 36.67 118.33 32.44 119 32.22 124.56 45.33 119.67 50.56 107.44 50.56 \",\"tooltip_pos\":\"Top\",\"tooltip_left\":\"69\",\"tooltip_top\":\"18\",\"length\":\"6\",\"width\":\"9\"},{\"polygon_class\":\"BATHROOM2\",\"name\":\"Bathroom\",\"points\":\"855 530 833 418 741 418 752 529 854 530 \",\"per_points\":\"95 58.89 92.56 46.44 82.33 46.44 83.56 58.78 94.89 58.89 \",\"tooltip_pos\":\"Top\",\"tooltip_left\":\"54\",\"tooltip_top\":\"27\",\"length\":\"5\",\"width\":\"7\"},{\"polygon_class\":\"BATHROOM3\",\"name\":\"Bathroom\",\"points\":\"598 498 598 420 460 420 451 498 597 498 \",\"per_points\":\"66.44 55.33 66.44 46.67 51.11 46.67 50.11 55.33 66.33 55.33 \",\"tooltip_pos\":\"Top\",\"tooltip_left\":\"41\",\"tooltip_top\":\"28\",\"length\":\"8\",\"width\":\"5\"},{\"polygon_class\":\"BATHROOM4\",\"name\":\"Bathroom\",\"points\":\"534 124 532 161 593 160 591 134 607 135 606 65 530 63 531 68 533 123 \",\"per_points\":\"59.33 13.78 59.11 17.89 65.89 17.78 65.67 14.89 67.44 15 67.33 7.22 58.89 7 59 7.56 59.22 13.67 \",\"tooltip_pos\":\"Bottom\",\"tooltip_left\":\"35\",\"tooltip_top\":\"19\",\"length\":\"4\",\"width\":\"5\"},{\"polygon_class\":\"BATHROOM5\",\"name\":\"Bathroom\",\"points\":\"228 440 255 352 168 350 134 442 226 442 \",\"per_points\":\"25.33 48.89 28.33 39.11 18.67 38.89 14.89 49.11 25.11 49.11 \",\"tooltip_pos\":\"Top\",\"tooltip_left\":\"13\",\"tooltip_top\":\"20\",\"length\":\"5\",\"width\":\"7\"}]"
                },
                "absoluteUrl": "https://im.proptiger-ws.com/2/2/5000175/89/251189.jpg"
            }],
            "video": [{
                "id": 18,
                "objectMediaTypeId": 106,
                "objectMediaType": {
                    "id": 106,
                    "objectType": {
                        "id": 2,
                        "type": "property"
                    },
                    "mediaType": {
                        "id": 4,
                        "name": "Video"
                    },
                    "objectTypeId": 2,
                    "mediaTypeId": 4,
                    "type": "VideoWalkthrough",
                    "domainId": 2,
                    "priority": 1,
                    "imageSitemapEnabled": 0,
                    "displayName": "Video Walkthrough"
                },
                "objectId": 5000175,
                "priority": 1,
                "sizeInBytes": 10362613,
                "active": true,
                "createdAt": 1433245321000,
                "updatedAt": 1440567578000,
                "mediaExtraAttributes": {
                    "videoUrlDetails": [{
                        "resolution": 270,
                        "bitRate": "300",
                        "url": "http://d10fypcdh7kbf3.cloudfront.net/4/2/5000175/106/18/supertech-capetown-floor-plan-4bhk-4t-2385-sq-ft-5000175.mp4"
                    }]
                },
                "imageUrl": "https://im.proptiger-ws.com/4/2/5000175/106/18/supertech-capetown-floor-plan-4bhk-4t-2385-sq-ft-5000175.jpg",
                "fps": 30,
                "resolution": "960×540",
                "bitRate": 500,
                "duration": 36.002633,
                "image": {
                    "id": 0,
                    "imageTypeId": 0,
                    "objectId": 0,
                    "statusId": 0,
                    "sizeInBytes": 0,
                    "width": 0,
                    "height": 0,
                    "active": false,
                    "activeStatus": 0,
                    "absolutePath": "https://im.proptiger-ws.com/https://im.proptiger-ws.com/4/2/5000175/106/18/supertech-capetown-floor-plan-4bhk-4t-2385-sq-ft-5000175.jpg"
                },
                "status": "Processed",
                "url": "http://d10fypcdh7kbf3.cloudfront.net/4/2/5000175/106/18/supertech-capetown-floor-plan-4bhk-4t-2385-sq-ft-5000175.mp4"
            }],
            "isPropertySoldOut": false,
            "propertySourceDomain": ["Proptiger"]
        }],
        "name": "CapeTown",
        "projectTypeId": 0,
        "launchDate": 1262284200000,
        "address": "Sector 74, Noida",
        "computedPriority": 62.605769230769,
        "assignedPriority": 9,
        "assignedLocalityPriority": 9,
        "assignedSuburbPriority": 9,
        "possessionDate": 1512066600000,
        "createdDate": 1425956052000,
        "submittedDate": 1382553000000,
        "imageURL": "https://im.proptiger-ws.com/1/500773/6/supertech-capetown-elevation-699489.jpeg",
        "URL": "noida/sector-74/supertech-capetown-500773",
        "latitude": 28.57572174,
        "longitude": 77.38767242,
        "forceResale": 0,
        "minPricePerUnitArea": 5350,
        "maxPricePerUnitArea": 5350,
        "minSize": 930,
        "maxSize": 2385,
        "minPrice": 4975500,
        "maxPrice": 12759750,
        "minBedrooms": 2,
        "maxBedrooms": 4,
        "projectStatus": "Under Construction",
        "isResale": false,
        "isPrimary": true,
        "isSoldOut": false,
        "description": "Supertech Cape Town is an initiative by the Supertech Group and comes with 2, 3 and 4 BHK apartments for buyers. Supertech Cape Town Sector 74 spreads over a total area of 50 acres and offers several amenities and other facilities to residents. Supertech Cape Town offers apartments that are open from four sides and are amply ventilated. These spacious apartments are also Vaastu complaint as far as their designs and architectural layouts are concerned. There are several locational advantages enjoyed by residents including easy access to several hospitals, educational institutions, banks and markets in addition to several other prominent areas in Noida including Sectors 77, 110, 117, 104, 113 and 110.The Highway linking Meerut and Delhi is also located near the project, thereby ensuring great connectivity to several prominent areas in the city. The project consists of a total of 4893 units with average home sizes ranging between 930 and 2385 sq. ft. Some of the amenities on offer include a gymnasium, sports facilities, swimming pool, intercom facilities, playing zone for kids, 24 hour security services, club house, jogging track, multipurpose room, landscaped gardens, rainwater harvesting systems, indoor game facilities and an area for senior citizens.",
        "totalUnits": 4893,
        "sizeInAcres": 34.0,
        "propertySizeMeasure": "sq ft",
        "dominantUnitType": "Apartment",
        "propertyUnitTypes": ["Apartment"],
        "images": [{
            "id": 860944,
            "imageTypeId": 6,
            "objectId": 500773,
            "path": "1/500773/6/",
            "pageUrl": "gallery/capetown-elevation-500773-860944",
            "statusId": 1,
            "createdAt": 1450927153000,
            "sizeInBytes": 1679838,
            "width": 2024,
            "height": 920,
            "altText": " capetown Elevation",
            "title": "Elevation",
            "priority": 1,
            "waterMarkName": "860944.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "capetown-elevation-860944.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/500773/6/capetown-elevation-860944.jpeg",
            "imageType": {
                "id": 6,
                "objectType": {
                    "id": 1,
                    "type": "project"
                },
                "mediaType": {
                    "id": 1,
                    "name": "Image"
                },
                "objectTypeId": 1,
                "mediaTypeId": 1,
                "type": "main",
                "domainId": 2,
                "priority": 1,
                "imageSitemapEnabled": 1,
                "displayName": "Elevation"
            },
            "masterImageStatuses": {
                "id": 1,
                "status": "PROCESSED"
            }
        }, {
            "id": 670429,
            "imageTypeId": 6,
            "objectId": 500773,
            "path": "1/500773/6/",
            "pageUrl": "gallery/capetown-elevation-500773-670429",
            "statusId": 1,
            "createdAt": 1450927153000,
            "sizeInBytes": 593683,
            "width": 1994,
            "height": 900,
            "altText": " capetown Elevation",
            "title": "Elevation",
            "description": "",
            "priority": 2,
            "waterMarkName": "670429.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "capetown-elevation-670429.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/500773/6/capetown-elevation-670429.jpeg",
            "imageType": {
                "id": 6,
                "objectType": {
                    "id": 1,
                    "type": "project"
                },
                "mediaType": {
                    "id": 1,
                    "name": "Image"
                },
                "objectTypeId": 1,
                "mediaTypeId": 1,
                "type": "main",
                "domainId": 2,
                "priority": 1,
                "imageSitemapEnabled": 1,
                "displayName": "Elevation"
            },
            "masterImageStatuses": {
                "id": 1,
                "status": "PROCESSED"
            }
        }, {
            "id": 670082,
            "imageTypeId": 81,
            "objectId": 500773,
            "path": "1/500773/81/",
            "pageUrl": "gallery/supertech-capetown-main-other-500773-670082",
            "statusId": 1,
            "createdAt": 1450927153000,
            "sizeInBytes": 138024,
            "width": 947,
            "height": 1280,
            "altText": "supertech capetown Main Other",
            "title": "Main Other",
            "priority": 5,
            "waterMarkName": "670082.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "supertech-capetown-main-other-670082.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/500773/81/supertech-capetown-main-other-670082.jpeg",
            "imageType": {
                "id": 81,
                "objectType": {
                    "id": 1,
                    "type": "project"
                },
                "mediaType": {
                    "id": 1,
                    "name": "Image"
                },
                "objectTypeId": 1,
                "mediaTypeId": 1,
                "type": "mainOther",
                "domainId": 2,
                "priority": 3,
                "imageSitemapEnabled": 0,
                "displayName": "Main Other"
            },
            "masterImageStatuses": {
                "id": 1,
                "status": "PROCESSED"
            }
        }, {
            "id": 670081,
            "imageTypeId": 81,
            "objectId": 500773,
            "path": "1/500773/81/",
            "pageUrl": "gallery/supertech-capetown-main-other-500773-670081",
            "statusId": 1,
            "createdAt": 1450927153000,
            "sizeInBytes": 213094,
            "width": 1280,
            "height": 960,
            "altText": "supertech capetown Main Other",
            "title": "Main Other",
            "priority": 5,
            "waterMarkName": "670081.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "supertech-capetown-main-other-670081.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/500773/81/supertech-capetown-main-other-670081.jpeg",
            "imageType": {
                "id": 81,
                "objectType": {
                    "id": 1,
                    "type": "project"
                },
                "mediaType": {
                    "id": 1,
                    "name": "Image"
                },
                "objectTypeId": 1,
                "mediaTypeId": 1,
                "type": "mainOther",
                "domainId": 2,
                "priority": 3,
                "imageSitemapEnabled": 0,
                "displayName": "Main Other"
            },
            "masterImageStatuses": {
                "id": 1,
                "status": "PROCESSED"
            }
        }, {
            "id": 670080,
            "imageTypeId": 81,
            "objectId": 500773,
            "path": "1/500773/81/",
            "pageUrl": "gallery/supertech-capetown-main-other-500773-670080",
            "statusId": 1,
            "createdAt": 1450927153000,
            "sizeInBytes": 147062,
            "width": 947,
            "height": 1280,
            "altText": "supertech capetown Main Other",
            "title": "Main Other",
            "priority": 5,
            "waterMarkName": "670080.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "supertech-capetown-main-other-670080.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/500773/81/supertech-capetown-main-other-670080.jpeg",
            "imageType": {
                "id": 81,
                "objectType": {
                    "id": 1,
                    "type": "project"
                },
                "mediaType": {
                    "id": 1,
                    "name": "Image"
                },
                "objectTypeId": 1,
                "mediaTypeId": 1,
                "type": "mainOther",
                "domainId": 2,
                "priority": 3,
                "imageSitemapEnabled": 0,
                "displayName": "Main Other"
            },
            "masterImageStatuses": {
                "id": 1,
                "status": "PROCESSED"
            }
        }, {
            "id": 670079,
            "imageTypeId": 81,
            "objectId": 500773,
            "path": "1/500773/81/",
            "pageUrl": "gallery/supertech-capetown-main-other-500773-670079",
            "statusId": 1,
            "createdAt": 1450927153000,
            "sizeInBytes": 130835,
            "width": 1280,
            "height": 720,
            "altText": "supertech capetown Main Other",
            "title": "Main Other",
            "priority": 5,
            "waterMarkName": "670079.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "supertech-capetown-main-other-670079.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/500773/81/supertech-capetown-main-other-670079.jpeg",
            "imageType": {
                "id": 81,
                "objectType": {
                    "id": 1,
                    "type": "project"
                },
                "mediaType": {
                    "id": 1,
                    "name": "Image"
                },
                "objectTypeId": 1,
                "mediaTypeId": 1,
                "type": "mainOther",
                "domainId": 2,
                "priority": 3,
                "imageSitemapEnabled": 0,
                "displayName": "Main Other"
            },
            "masterImageStatuses": {
                "id": 1,
                "status": "PROCESSED"
            }
        }, {
            "id": 670078,
            "imageTypeId": 81,
            "objectId": 500773,
            "path": "1/500773/81/",
            "pageUrl": "gallery/supertech-capetown-main-other-500773-670078",
            "statusId": 1,
            "createdAt": 1450927153000,
            "sizeInBytes": 187087,
            "width": 720,
            "height": 1280,
            "altText": "supertech capetown Main Other",
            "title": "Main Other",
            "priority": 5,
            "waterMarkName": "670078.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "supertech-capetown-main-other-670078.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/500773/81/supertech-capetown-main-other-670078.jpeg",
            "imageType": {
                "id": 81,
                "objectType": {
                    "id": 1,
                    "type": "project"
                },
                "mediaType": {
                    "id": 1,
                    "name": "Image"
                },
                "objectTypeId": 1,
                "mediaTypeId": 1,
                "type": "mainOther",
                "domainId": 2,
                "priority": 3,
                "imageSitemapEnabled": 0,
                "displayName": "Main Other"
            },
            "masterImageStatuses": {
                "id": 1,
                "status": "PROCESSED"
            }
        }, {
            "id": 670077,
            "imageTypeId": 81,
            "objectId": 500773,
            "path": "1/500773/81/",
            "pageUrl": "gallery/supertech-capetown-main-other-500773-670077",
            "statusId": 1,
            "createdAt": 1450927153000,
            "sizeInBytes": 68528,
            "width": 1280,
            "height": 720,
            "altText": "supertech capetown Main Other",
            "title": "Main Other",
            "priority": 5,
            "waterMarkName": "670077.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "supertech-capetown-main-other-670077.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/500773/81/supertech-capetown-main-other-670077.jpeg",
            "imageType": {
                "id": 81,
                "objectType": {
                    "id": 1,
                    "type": "project"
                },
                "mediaType": {
                    "id": 1,
                    "name": "Image"
                },
                "objectTypeId": 1,
                "mediaTypeId": 1,
                "type": "mainOther",
                "domainId": 2,
                "priority": 3,
                "imageSitemapEnabled": 0,
                "displayName": "Main Other"
            },
            "masterImageStatuses": {
                "id": 1,
                "status": "PROCESSED"
            }
        }, {
            "id": 670076,
            "imageTypeId": 81,
            "objectId": 500773,
            "path": "1/500773/81/",
            "pageUrl": "gallery/supertech-capetown-main-other-500773-670076",
            "statusId": 1,
            "createdAt": 1450927153000,
            "sizeInBytes": 76974,
            "width": 1040,
            "height": 780,
            "altText": "supertech capetown Main Other",
            "title": "Main Other",
            "priority": 5,
            "waterMarkName": "670076.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "supertech-capetown-main-other-670076.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/500773/81/supertech-capetown-main-other-670076.jpeg",
            "imageType": {
                "id": 81,
                "objectType": {
                    "id": 1,
                    "type": "project"
                },
                "mediaType": {
                    "id": 1,
                    "name": "Image"
                },
                "objectTypeId": 1,
                "mediaTypeId": 1,
                "type": "mainOther",
                "domainId": 2,
                "priority": 3,
                "imageSitemapEnabled": 0,
                "displayName": "Main Other"
            },
            "masterImageStatuses": {
                "id": 1,
                "status": "PROCESSED"
            }
        }, {
            "id": 505343,
            "imageTypeId": 5,
            "objectId": 500773,
            "path": "1/500773/5/",
            "pageUrl": "gallery/supertech-capetown-location-plan-500773-505343",
            "statusId": 1,
            "createdAt": 1450927153000,
            "sizeInBytes": 194512,
            "width": 1043,
            "height": 555,
            "altText": "supertech capetown Location Plan",
            "title": "Location Plan",
            "description": "",
            "waterMarkName": "505343.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "supertech-capetown-location-plan-505343.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/500773/5/supertech-capetown-location-plan-505343.jpeg",
            "imageType": {
                "id": 5,
                "objectType": {
                    "id": 1,
                    "type": "project"
                },
                "mediaType": {
                    "id": 1,
                    "name": "Image"
                },
                "objectTypeId": 1,
                "mediaTypeId": 1,
                "type": "locationPlan",
                "domainId": 2,
                "priority": 4,
                "imageSitemapEnabled": 1,
                "displayName": "Location Plan"
            },
            "masterImageStatuses": {
                "id": 1,
                "status": "PROCESSED"
            }
        }, {
            "id": 587764,
            "imageTypeId": 7,
            "objectId": 500773,
            "path": "1/500773/7/",
            "pageUrl": "gallery/supertech-capetown-master-plan-500773-587764",
            "statusId": 1,
            "createdAt": 1450927153000,
            "sizeInBytes": 6475187,
            "width": 4455,
            "height": 3242,
            "altText": "supertech capetown Master Plan",
            "title": "Master Plan",
            "description": "",
            "waterMarkName": "587764.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "supertech-capetown-master-plan-587764.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/500773/7/supertech-capetown-master-plan-587764.jpeg",
            "imageType": {
                "id": 7,
                "objectType": {
                    "id": 1,
                    "type": "project"
                },
                "mediaType": {
                    "id": 1,
                    "name": "Image"
                },
                "objectTypeId": 1,
                "mediaTypeId": 1,
                "type": "masterPlan",
                "domainId": 2,
                "priority": 7,
                "imageSitemapEnabled": 1,
                "displayName": "Master Plan"
            },
            "masterImageStatuses": {
                "id": 1,
                "status": "PROCESSED"
            }
        }, {
            "id": 799086,
            "imageTypeId": 3,
            "objectId": 500773,
            "path": "1/500773/3/",
            "pageUrl": "gallery/capetown-tower-cs3-construction-status-sept-15-500773-799086",
            "statusId": 1,
            "createdAt": 1450927153000,
            "takenAt": 1441065600000,
            "sizeInBytes": 400033,
            "width": 810,
            "height": 1080,
            "altText": " capetown Tower CS3 Construction Status Sept-15",
            "title": "Tower CS3 Construction Status Sept-15",
            "jsonDump": "{\"tower_id\":\"11945\"}",
            "waterMarkName": "799086.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "capetown-tower-cs3-construction-status-sept-15-799086.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/500773/3/capetown-tower-cs3-construction-status-sept-15-799086.jpeg",
            "imageType": {
                "id": 3,
                "objectType": {
                    "id": 1,
                    "type": "project"
                },
                "mediaType": {
                    "id": 1,
                    "name": "Image"
                },
                "objectTypeId": 1,
                "mediaTypeId": 1,
                "type": "constructionStatus",
                "domainId": 2,
                "priority": 9,
                "imageSitemapEnabled": 0,
                "displayName": "Construction Status"
            },
            "masterImageStatuses": {
                "id": 1,
                "status": "PROCESSED"
            }
        }, {
            "id": 799085,
            "imageTypeId": 3,
            "objectId": 500773,
            "path": "1/500773/3/",
            "pageUrl": "gallery/capetown-tower-cs9-construction-status-sept-15-500773-799085",
            "statusId": 1,
            "createdAt": 1450927153000,
            "takenAt": 1441065600000,
            "sizeInBytes": 414032,
            "width": 810,
            "height": 1080,
            "altText": " capetown Tower CS9 Construction Status Sept-15",
            "title": "Tower CS9 Construction Status Sept-15",
            "jsonDump": "{\"tower_id\":\"11951\"}",
            "waterMarkName": "799085.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "capetown-tower-cs9-construction-status-sept-15-799085.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/500773/3/capetown-tower-cs9-construction-status-sept-15-799085.jpeg",
            "imageType": {
                "id": 3,
                "objectType": {
                    "id": 1,
                    "type": "project"
                },
                "mediaType": {
                    "id": 1,
                    "name": "Image"
                },
                "objectTypeId": 1,
                "mediaTypeId": 1,
                "type": "constructionStatus",
                "domainId": 2,
                "priority": 9,
                "imageSitemapEnabled": 0,
                "displayName": "Construction Status"
            },
            "masterImageStatuses": {
                "id": 1,
                "status": "PROCESSED"
            }
        }, {
            "id": 799084,
            "imageTypeId": 3,
            "objectId": 500773,
            "path": "1/500773/3/",
            "pageUrl": "gallery/capetown-tower-cs8-construction-status-sept-15-500773-799084",
            "statusId": 1,
            "createdAt": 1450927153000,
            "takenAt": 1441065600000,
            "sizeInBytes": 432952,
            "width": 810,
            "height": 1080,
            "altText": " capetown Tower CS8 Construction Status Sept-15",
            "title": "Tower CS8 Construction Status Sept-15",
            "jsonDump": "{\"tower_id\":\"11950\"}",
            "waterMarkName": "799084.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "capetown-tower-cs8-construction-status-sept-15-799084.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/500773/3/capetown-tower-cs8-construction-status-sept-15-799084.jpeg",
            "imageType": {
                "id": 3,
                "objectType": {
                    "id": 1,
                    "type": "project"
                },
                "mediaType": {
                    "id": 1,
                    "name": "Image"
                },
                "objectTypeId": 1,
                "mediaTypeId": 1,
                "type": "constructionStatus",
                "domainId": 2,
                "priority": 9,
                "imageSitemapEnabled": 0,
                "displayName": "Construction Status"
            },
            "masterImageStatuses": {
                "id": 1,
                "status": "PROCESSED"
            }
        }, {
            "id": 799083,
            "imageTypeId": 3,
            "objectId": 500773,
            "path": "1/500773/3/",
            "pageUrl": "gallery/capetown-tower-cs2-construction-status-sept-15-500773-799083",
            "statusId": 1,
            "createdAt": 1450927153000,
            "takenAt": 1441065600000,
            "sizeInBytes": 720978,
            "width": 1440,
            "height": 1080,
            "altText": " capetown Tower CS2 Construction Status Sept-15",
            "title": "Tower CS2 Construction Status Sept-15",
            "jsonDump": "{\"tower_id\":\"11944\"}",
            "waterMarkName": "799083.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "capetown-tower-cs2-construction-status-sept-15-799083.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/500773/3/capetown-tower-cs2-construction-status-sept-15-799083.jpeg",
            "imageType": {
                "id": 3,
                "objectType": {
                    "id": 1,
                    "type": "project"
                },
                "mediaType": {
                    "id": 1,
                    "name": "Image"
                },
                "objectTypeId": 1,
                "mediaTypeId": 1,
                "type": "constructionStatus",
                "domainId": 2,
                "priority": 9,
                "imageSitemapEnabled": 0,
                "displayName": "Construction Status"
            },
            "masterImageStatuses": {
                "id": 1,
                "status": "PROCESSED"
            }
        }, {
            "id": 799082,
            "imageTypeId": 3,
            "objectId": 500773,
            "path": "1/500773/3/",
            "pageUrl": "gallery/capetown-construction-status-sept-15-500773-799082",
            "statusId": 1,
            "createdAt": 1450927153000,
            "takenAt": 1441065600000,
            "sizeInBytes": 665026,
            "width": 1440,
            "height": 1080,
            "altText": " capetown Construction Status Sept-15",
            "title": "Construction Status Sept-15",
            "jsonDump": "{\"tower_id\":null}",
            "waterMarkName": "799082.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "capetown-construction-status-sept-15-799082.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/500773/3/capetown-construction-status-sept-15-799082.jpeg",
            "imageType": {
                "id": 3,
                "objectType": {
                    "id": 1,
                    "type": "project"
                },
                "mediaType": {
                    "id": 1,
                    "name": "Image"
                },
                "objectTypeId": 1,
                "mediaTypeId": 1,
                "type": "constructionStatus",
                "domainId": 2,
                "priority": 9,
                "imageSitemapEnabled": 0,
                "displayName": "Construction Status"
            },
            "masterImageStatuses": {
                "id": 1,
                "status": "PROCESSED"
            }
        }, {
            "id": 799081,
            "imageTypeId": 3,
            "objectId": 500773,
            "path": "1/500773/3/",
            "pageUrl": "gallery/capetown-tower-cv9-construction-status-sept-15-500773-799081",
            "statusId": 1,
            "createdAt": 1450927153000,
            "takenAt": 1441065600000,
            "sizeInBytes": 716615,
            "width": 1440,
            "height": 844,
            "altText": " capetown Tower CV9 Construction Status Sept-15",
            "title": "Tower CV9 Construction Status Sept-15",
            "jsonDump": "{\"tower_id\":\"11937\"}",
            "waterMarkName": "799081.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "capetown-tower-cv9-construction-status-sept-15-799081.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/500773/3/capetown-tower-cv9-construction-status-sept-15-799081.jpeg",
            "imageType": {
                "id": 3,
                "objectType": {
                    "id": 1,
                    "type": "project"
                },
                "mediaType": {
                    "id": 1,
                    "name": "Image"
                },
                "objectTypeId": 1,
                "mediaTypeId": 1,
                "type": "constructionStatus",
                "domainId": 2,
                "priority": 9,
                "imageSitemapEnabled": 0,
                "displayName": "Construction Status"
            },
            "masterImageStatuses": {
                "id": 1,
                "status": "PROCESSED"
            }
        }, {
            "id": 799080,
            "imageTypeId": 3,
            "objectId": 500773,
            "path": "1/500773/3/",
            "pageUrl": "gallery/capetown-tower-cs6-construction-status-sept-15-500773-799080",
            "statusId": 1,
            "createdAt": 1450927153000,
            "takenAt": 1441065600000,
            "sizeInBytes": 420299,
            "width": 810,
            "height": 1080,
            "altText": " capetown Tower CS6 Construction Status Sept-15",
            "title": "Tower CS6 Construction Status Sept-15",
            "jsonDump": "{\"tower_id\":\"11948\"}",
            "waterMarkName": "799080.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "capetown-tower-cs6-construction-status-sept-15-799080.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/500773/3/capetown-tower-cs6-construction-status-sept-15-799080.jpeg",
            "imageType": {
                "id": 3,
                "objectType": {
                    "id": 1,
                    "type": "project"
                },
                "mediaType": {
                    "id": 1,
                    "name": "Image"
                },
                "objectTypeId": 1,
                "mediaTypeId": 1,
                "type": "constructionStatus",
                "domainId": 2,
                "priority": 9,
                "imageSitemapEnabled": 0,
                "displayName": "Construction Status"
            },
            "masterImageStatuses": {
                "id": 1,
                "status": "PROCESSED"
            }
        }, {
            "id": 799079,
            "imageTypeId": 3,
            "objectId": 500773,
            "path": "1/500773/3/",
            "pageUrl": "gallery/capetown-tower-cs7-construction-status-sept-15-500773-799079",
            "statusId": 1,
            "createdAt": 1450927153000,
            "takenAt": 1441065600000,
            "sizeInBytes": 439383,
            "width": 810,
            "height": 1080,
            "altText": " capetown Tower CS7 Construction Status Sept-15",
            "title": "Tower CS7 Construction Status Sept-15",
            "jsonDump": "{\"tower_id\":\"11949\"}",
            "waterMarkName": "799079.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "capetown-tower-cs7-construction-status-sept-15-799079.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/500773/3/capetown-tower-cs7-construction-status-sept-15-799079.jpeg",
            "imageType": {
                "id": 3,
                "objectType": {
                    "id": 1,
                    "type": "project"
                },
                "mediaType": {
                    "id": 1,
                    "name": "Image"
                },
                "objectTypeId": 1,
                "mediaTypeId": 1,
                "type": "constructionStatus",
                "domainId": 2,
                "priority": 9,
                "imageSitemapEnabled": 0,
                "displayName": "Construction Status"
            },
            "masterImageStatuses": {
                "id": 1,
                "status": "PROCESSED"
            }
        }, {
            "id": 799078,
            "imageTypeId": 3,
            "objectId": 500773,
            "path": "1/500773/3/",
            "pageUrl": "gallery/capetown-tower-cv3-construction-status-sept-15-500773-799078",
            "statusId": 1,
            "createdAt": 1450927153000,
            "takenAt": 1441065600000,
            "sizeInBytes": 447990,
            "width": 810,
            "height": 1080,
            "altText": " capetown Tower CV3 Construction Status Sept-15",
            "title": "Tower CV3 Construction Status Sept-15",
            "jsonDump": "{\"tower_id\":\"11932\"}",
            "waterMarkName": "799078.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "capetown-tower-cv3-construction-status-sept-15-799078.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/500773/3/capetown-tower-cv3-construction-status-sept-15-799078.jpeg",
            "imageType": {
                "id": 3,
                "objectType": {
                    "id": 1,
                    "type": "project"
                },
                "mediaType": {
                    "id": 1,
                    "name": "Image"
                },
                "objectTypeId": 1,
                "mediaTypeId": 1,
                "type": "constructionStatus",
                "domainId": 2,
                "priority": 9,
                "imageSitemapEnabled": 0,
                "displayName": "Construction Status"
            },
            "masterImageStatuses": {
                "id": 1,
                "status": "PROCESSED"
            }
        }, {
            "id": 799077,
            "imageTypeId": 3,
            "objectId": 500773,
            "path": "1/500773/3/",
            "pageUrl": "gallery/capetown-tower-cmc4-construction-status-sept-15-500773-799077",
            "statusId": 1,
            "createdAt": 1450927153000,
            "takenAt": 1441065600000,
            "sizeInBytes": 475401,
            "width": 810,
            "height": 1080,
            "altText": " capetown Tower CMC4 Construction Status Sept-15",
            "title": "Tower CMC4 Construction Status Sept-15",
            "jsonDump": "{\"tower_id\":\"11963\"}",
            "waterMarkName": "799077.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "capetown-tower-cmc4-construction-status-sept-15-799077.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/500773/3/capetown-tower-cmc4-construction-status-sept-15-799077.jpeg",
            "imageType": {
                "id": 3,
                "objectType": {
                    "id": 1,
                    "type": "project"
                },
                "mediaType": {
                    "id": 1,
                    "name": "Image"
                },
                "objectTypeId": 1,
                "mediaTypeId": 1,
                "type": "constructionStatus",
                "domainId": 2,
                "priority": 9,
                "imageSitemapEnabled": 0,
                "displayName": "Construction Status"
            },
            "masterImageStatuses": {
                "id": 1,
                "status": "PROCESSED"
            }
        }, {
            "id": 799076,
            "imageTypeId": 3,
            "objectId": 500773,
            "path": "1/500773/3/",
            "pageUrl": "gallery/capetown-tower-cv7-construction-status-sept-15-500773-799076",
            "statusId": 1,
            "createdAt": 1450927153000,
            "takenAt": 1441065600000,
            "sizeInBytes": 396939,
            "width": 810,
            "height": 1080,
            "altText": " capetown Tower CV7 Construction Status Sept-15",
            "title": "Tower CV7 Construction Status Sept-15",
            "jsonDump": "{\"tower_id\":\"11936\"}",
            "waterMarkName": "799076.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "capetown-tower-cv7-construction-status-sept-15-799076.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/500773/3/capetown-tower-cv7-construction-status-sept-15-799076.jpeg",
            "imageType": {
                "id": 3,
                "objectType": {
                    "id": 1,
                    "type": "project"
                },
                "mediaType": {
                    "id": 1,
                    "name": "Image"
                },
                "objectTypeId": 1,
                "mediaTypeId": 1,
                "type": "constructionStatus",
                "domainId": 2,
                "priority": 9,
                "imageSitemapEnabled": 0,
                "displayName": "Construction Status"
            },
            "masterImageStatuses": {
                "id": 1,
                "status": "PROCESSED"
            }
        }, {
            "id": 799075,
            "imageTypeId": 3,
            "objectId": 500773,
            "path": "1/500773/3/",
            "pageUrl": "gallery/capetown-tower-cv4-construction-status-sept-15-500773-799075",
            "statusId": 1,
            "createdAt": 1450927153000,
            "takenAt": 1441065600000,
            "sizeInBytes": 400053,
            "width": 810,
            "height": 1080,
            "altText": " capetown Tower CV4 Construction Status Sept-15",
            "title": "Tower CV4 Construction Status Sept-15",
            "jsonDump": "{\"tower_id\":\"11933\"}",
            "waterMarkName": "799075.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "capetown-tower-cv4-construction-status-sept-15-799075.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/500773/3/capetown-tower-cv4-construction-status-sept-15-799075.jpeg",
            "imageType": {
                "id": 3,
                "objectType": {
                    "id": 1,
                    "type": "project"
                },
                "mediaType": {
                    "id": 1,
                    "name": "Image"
                },
                "objectTypeId": 1,
                "mediaTypeId": 1,
                "type": "constructionStatus",
                "domainId": 2,
                "priority": 9,
                "imageSitemapEnabled": 0,
                "displayName": "Construction Status"
            },
            "masterImageStatuses": {
                "id": 1,
                "status": "PROCESSED"
            }
        }, {
            "id": 799074,
            "imageTypeId": 3,
            "objectId": 500773,
            "path": "1/500773/3/",
            "pageUrl": "gallery/capetown-construction-status-sept-15-500773-799074",
            "statusId": 1,
            "createdAt": 1450927153000,
            "takenAt": 1441065600000,
            "sizeInBytes": 485294,
            "width": 810,
            "height": 1080,
            "altText": " capetown Construction Status Sept-15",
            "title": "Construction Status Sept-15",
            "jsonDump": "{\"tower_id\":null}",
            "waterMarkName": "799074.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "capetown-construction-status-sept-15-799074.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/500773/3/capetown-construction-status-sept-15-799074.jpeg",
            "imageType": {
                "id": 3,
                "objectType": {
                    "id": 1,
                    "type": "project"
                },
                "mediaType": {
                    "id": 1,
                    "name": "Image"
                },
                "objectTypeId": 1,
                "mediaTypeId": 1,
                "type": "constructionStatus",
                "domainId": 2,
                "priority": 9,
                "imageSitemapEnabled": 0,
                "displayName": "Construction Status"
            },
            "masterImageStatuses": {
                "id": 1,
                "status": "PROCESSED"
            }
        }, {
            "id": 799073,
            "imageTypeId": 3,
            "objectId": 500773,
            "path": "1/500773/3/",
            "pageUrl": "gallery/capetown-tower-cs1-construction-status-sept-15-500773-799073",
            "statusId": 1,
            "createdAt": 1450927153000,
            "takenAt": 1441065600000,
            "sizeInBytes": 497484,
            "width": 810,
            "height": 1080,
            "altText": " capetown Tower CS1 Construction Status Sept-15",
            "title": "Tower CS1 Construction Status Sept-15",
            "jsonDump": "{\"tower_id\":\"11943\"}",
            "waterMarkName": "799073.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "capetown-tower-cs1-construction-status-sept-15-799073.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/500773/3/capetown-tower-cs1-construction-status-sept-15-799073.jpeg",
            "imageType": {
                "id": 3,
                "objectType": {
                    "id": 1,
                    "type": "project"
                },
                "mediaType": {
                    "id": 1,
                    "name": "Image"
                },
                "objectTypeId": 1,
                "mediaTypeId": 1,
                "type": "constructionStatus",
                "domainId": 2,
                "priority": 9,
                "imageSitemapEnabled": 0,
                "displayName": "Construction Status"
            },
            "masterImageStatuses": {
                "id": 1,
                "status": "PROCESSED"
            }
        }, {
            "id": 799072,
            "imageTypeId": 3,
            "objectId": 500773,
            "path": "1/500773/3/",
            "pageUrl": "gallery/capetown-tower-cv1-construction-status-sept-15-500773-799072",
            "statusId": 1,
            "createdAt": 1450927153000,
            "takenAt": 1441065600000,
            "sizeInBytes": 439175,
            "width": 810,
            "height": 1080,
            "altText": " capetown Tower CV1 Construction Status Sept-15",
            "title": "Tower CV1 Construction Status Sept-15",
            "jsonDump": "{\"tower_id\":\"11930\"}",
            "waterMarkName": "799072.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "capetown-tower-cv1-construction-status-sept-15-799072.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/500773/3/capetown-tower-cv1-construction-status-sept-15-799072.jpeg",
            "imageType": {
                "id": 3,
                "objectType": {
                    "id": 1,
                    "type": "project"
                },
                "mediaType": {
                    "id": 1,
                    "name": "Image"
                },
                "objectTypeId": 1,
                "mediaTypeId": 1,
                "type": "constructionStatus",
                "domainId": 2,
                "priority": 9,
                "imageSitemapEnabled": 0,
                "displayName": "Construction Status"
            },
            "masterImageStatuses": {
                "id": 1,
                "status": "PROCESSED"
            }
        }, {
            "id": 799071,
            "imageTypeId": 3,
            "objectId": 500773,
            "path": "1/500773/3/",
            "pageUrl": "gallery/capetown-tower-cs4-construction-status-sept-15-500773-799071",
            "statusId": 1,
            "createdAt": 1450927153000,
            "takenAt": 1441065600000,
            "sizeInBytes": 395084,
            "width": 810,
            "height": 1080,
            "altText": " capetown Tower CS4 Construction Status Sept-15",
            "title": "Tower CS4 Construction Status Sept-15",
            "jsonDump": "{\"tower_id\":\"11946\"}",
            "waterMarkName": "799071.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "capetown-tower-cs4-construction-status-sept-15-799071.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/500773/3/capetown-tower-cs4-construction-status-sept-15-799071.jpeg",
            "imageType": {
                "id": 3,
                "objectType": {
                    "id": 1,
                    "type": "project"
                },
                "mediaType": {
                    "id": 1,
                    "name": "Image"
                },
                "objectTypeId": 1,
                "mediaTypeId": 1,
                "type": "constructionStatus",
                "domainId": 2,
                "priority": 9,
                "imageSitemapEnabled": 0,
                "displayName": "Construction Status"
            },
            "masterImageStatuses": {
                "id": 1,
                "status": "PROCESSED"
            }
        }, {
            "id": 799070,
            "imageTypeId": 3,
            "objectId": 500773,
            "path": "1/500773/3/",
            "pageUrl": "gallery/capetown-tower-cs5-construction-status-sept-15-500773-799070",
            "statusId": 1,
            "createdAt": 1450927153000,
            "takenAt": 1441065600000,
            "sizeInBytes": 447413,
            "width": 810,
            "height": 1080,
            "altText": " capetown Tower CS5 Construction Status Sept-15",
            "title": "Tower CS5 Construction Status Sept-15",
            "jsonDump": "{\"tower_id\":\"11947\"}",
            "waterMarkName": "799070.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "capetown-tower-cs5-construction-status-sept-15-799070.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/500773/3/capetown-tower-cs5-construction-status-sept-15-799070.jpeg",
            "imageType": {
                "id": 3,
                "objectType": {
                    "id": 1,
                    "type": "project"
                },
                "mediaType": {
                    "id": 1,
                    "name": "Image"
                },
                "objectTypeId": 1,
                "mediaTypeId": 1,
                "type": "constructionStatus",
                "domainId": 2,
                "priority": 9,
                "imageSitemapEnabled": 0,
                "displayName": "Construction Status"
            },
            "masterImageStatuses": {
                "id": 1,
                "status": "PROCESSED"
            }
        }, {
            "id": 799069,
            "imageTypeId": 3,
            "objectId": 500773,
            "path": "1/500773/3/",
            "pageUrl": "gallery/capetown-tower-cv2-construction-status-sept-15-500773-799069",
            "statusId": 1,
            "createdAt": 1450927153000,
            "takenAt": 1441065600000,
            "sizeInBytes": 418640,
            "width": 810,
            "height": 1080,
            "altText": " capetown Tower CV2 Construction Status Sept-15",
            "title": "Tower CV2 Construction Status Sept-15",
            "jsonDump": "{\"tower_id\":\"11931\"}",
            "waterMarkName": "799069.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "capetown-tower-cv2-construction-status-sept-15-799069.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/500773/3/capetown-tower-cv2-construction-status-sept-15-799069.jpeg",
            "imageType": {
                "id": 3,
                "objectType": {
                    "id": 1,
                    "type": "project"
                },
                "mediaType": {
                    "id": 1,
                    "name": "Image"
                },
                "objectTypeId": 1,
                "mediaTypeId": 1,
                "type": "constructionStatus",
                "domainId": 2,
                "priority": 9,
                "imageSitemapEnabled": 0,
                "displayName": "Construction Status"
            },
            "masterImageStatuses": {
                "id": 1,
                "status": "PROCESSED"
            }
        }, {
            "id": 799068,
            "imageTypeId": 3,
            "objectId": 500773,
            "path": "1/500773/3/",
            "pageUrl": "gallery/capetown-tower-cs10-construction-status-sept-15-500773-799068",
            "statusId": 1,
            "createdAt": 1450927153000,
            "takenAt": 1441065600000,
            "sizeInBytes": 448316,
            "width": 810,
            "height": 1080,
            "altText": " capetown Tower CS10 Construction Status Sept-15",
            "title": "Tower CS10 Construction Status Sept-15",
            "jsonDump": "{\"tower_id\":\"11952\"}",
            "waterMarkName": "799068.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "capetown-tower-cs10-construction-status-sept-15-799068.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/500773/3/capetown-tower-cs10-construction-status-sept-15-799068.jpeg",
            "imageType": {
                "id": 3,
                "objectType": {
                    "id": 1,
                    "type": "project"
                },
                "mediaType": {
                    "id": 1,
                    "name": "Image"
                },
                "objectTypeId": 1,
                "mediaTypeId": 1,
                "type": "constructionStatus",
                "domainId": 2,
                "priority": 9,
                "imageSitemapEnabled": 0,
                "displayName": "Construction Status"
            },
            "masterImageStatuses": {
                "id": 1,
                "status": "PROCESSED"
            }
        }, {
            "id": 799067,
            "imageTypeId": 3,
            "objectId": 500773,
            "path": "1/500773/3/",
            "pageUrl": "gallery/capetown-tower-cv5-construction-status-sept-15-500773-799067",
            "statusId": 1,
            "createdAt": 1450927153000,
            "takenAt": 1441065600000,
            "sizeInBytes": 422639,
            "width": 810,
            "height": 1080,
            "altText": " capetown Tower CV5 Construction Status Sept-15",
            "title": "Tower CV5 Construction Status Sept-15",
            "jsonDump": "{\"tower_id\":\"11934\"}",
            "waterMarkName": "799067.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "capetown-tower-cv5-construction-status-sept-15-799067.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/500773/3/capetown-tower-cv5-construction-status-sept-15-799067.jpeg",
            "imageType": {
                "id": 3,
                "objectType": {
                    "id": 1,
                    "type": "project"
                },
                "mediaType": {
                    "id": 1,
                    "name": "Image"
                },
                "objectTypeId": 1,
                "mediaTypeId": 1,
                "type": "constructionStatus",
                "domainId": 2,
                "priority": 9,
                "imageSitemapEnabled": 0,
                "displayName": "Construction Status"
            },
            "masterImageStatuses": {
                "id": 1,
                "status": "PROCESSED"
            }
        }, {
            "id": 799064,
            "imageTypeId": 3,
            "objectId": 500773,
            "path": "1/500773/3/",
            "pageUrl": "gallery/capetown-tower-cb3-construction-status-sept-15-500773-799064",
            "statusId": 1,
            "createdAt": 1450927153000,
            "takenAt": 1441065600000,
            "sizeInBytes": 936179,
            "width": 1440,
            "height": 1286,
            "altText": " capetown Tower CB3 Construction Status Sept-15",
            "title": "Tower CB3 Construction Status Sept-15",
            "jsonDump": "{\"tower_id\":\"11941\"}",
            "waterMarkName": "799064.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "capetown-tower-cb3-construction-status-sept-15-799064.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/500773/3/capetown-tower-cb3-construction-status-sept-15-799064.jpeg",
            "imageType": {
                "id": 3,
                "objectType": {
                    "id": 1,
                    "type": "project"
                },
                "mediaType": {
                    "id": 1,
                    "name": "Image"
                },
                "objectTypeId": 1,
                "mediaTypeId": 1,
                "type": "constructionStatus",
                "domainId": 2,
                "priority": 9,
                "imageSitemapEnabled": 0,
                "displayName": "Construction Status"
            },
            "masterImageStatuses": {
                "id": 1,
                "status": "PROCESSED"
            }
        }, {
            "id": 799063,
            "imageTypeId": 3,
            "objectId": 500773,
            "path": "1/500773/3/",
            "pageUrl": "gallery/capetown-tower-cm3-construction-status-sept-15-500773-799063",
            "statusId": 1,
            "createdAt": 1450927153000,
            "takenAt": 1441065600000,
            "sizeInBytes": 695264,
            "width": 1440,
            "height": 1080,
            "altText": " capetown Tower CM3 Construction Status Sept-15",
            "title": "Tower CM3 Construction Status Sept-15",
            "jsonDump": "{\"tower_id\":\"11966\"}",
            "waterMarkName": "799063.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "capetown-tower-cm3-construction-status-sept-15-799063.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/500773/3/capetown-tower-cm3-construction-status-sept-15-799063.jpeg",
            "imageType": {
                "id": 3,
                "objectType": {
                    "id": 1,
                    "type": "project"
                },
                "mediaType": {
                    "id": 1,
                    "name": "Image"
                },
                "objectTypeId": 1,
                "mediaTypeId": 1,
                "type": "constructionStatus",
                "domainId": 2,
                "priority": 9,
                "imageSitemapEnabled": 0,
                "displayName": "Construction Status"
            },
            "masterImageStatuses": {
                "id": 1,
                "status": "PROCESSED"
            }
        }, {
            "id": 799062,
            "imageTypeId": 3,
            "objectId": 500773,
            "path": "1/500773/3/",
            "pageUrl": "gallery/capetown-tower-cm1-construction-status-sept-15-500773-799062",
            "statusId": 1,
            "createdAt": 1450927153000,
            "takenAt": 1441065600000,
            "sizeInBytes": 757873,
            "width": 1440,
            "height": 1080,
            "altText": " capetown Tower CM1 Construction Status Sept-15",
            "title": "Tower CM1 Construction Status Sept-15",
            "jsonDump": "{\"tower_id\":\"11964\"}",
            "waterMarkName": "799062.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "capetown-tower-cm1-construction-status-sept-15-799062.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/500773/3/capetown-tower-cm1-construction-status-sept-15-799062.jpeg",
            "imageType": {
                "id": 3,
                "objectType": {
                    "id": 1,
                    "type": "project"
                },
                "mediaType": {
                    "id": 1,
                    "name": "Image"
                },
                "objectTypeId": 1,
                "mediaTypeId": 1,
                "type": "constructionStatus",
                "domainId": 2,
                "priority": 9,
                "imageSitemapEnabled": 0,
                "displayName": "Construction Status"
            },
            "masterImageStatuses": {
                "id": 1,
                "status": "PROCESSED"
            }
        }, {
            "id": 799061,
            "imageTypeId": 3,
            "objectId": 500773,
            "path": "1/500773/3/",
            "pageUrl": "gallery/capetown-tower-cgd1-construction-status-sept-15-500773-799061",
            "statusId": 1,
            "createdAt": 1450927153000,
            "takenAt": 1441065600000,
            "sizeInBytes": 452532,
            "width": 932,
            "height": 1080,
            "altText": " capetown Tower CGD1 Construction Status Sept-15",
            "title": "Tower CGD1 Construction Status Sept-15",
            "jsonDump": "{\"tower_id\":\"11968\"}",
            "waterMarkName": "799061.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "capetown-tower-cgd1-construction-status-sept-15-799061.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/500773/3/capetown-tower-cgd1-construction-status-sept-15-799061.jpeg",
            "imageType": {
                "id": 3,
                "objectType": {
                    "id": 1,
                    "type": "project"
                },
                "mediaType": {
                    "id": 1,
                    "name": "Image"
                },
                "objectTypeId": 1,
                "mediaTypeId": 1,
                "type": "constructionStatus",
                "domainId": 2,
                "priority": 9,
                "imageSitemapEnabled": 0,
                "displayName": "Construction Status"
            },
            "masterImageStatuses": {
                "id": 1,
                "status": "PROCESSED"
            }
        }, {
            "id": 799060,
            "imageTypeId": 3,
            "objectId": 500773,
            "path": "1/500773/3/",
            "pageUrl": "gallery/capetown-tower-cg6-construction-status-sept-15-500773-799060",
            "statusId": 1,
            "createdAt": 1450927153000,
            "takenAt": 1441065600000,
            "sizeInBytes": 450621,
            "width": 810,
            "height": 1080,
            "altText": " capetown Tower CG6 Construction Status Sept-15",
            "title": "Tower CG6 Construction Status Sept-15",
            "jsonDump": "{\"tower_id\":\"11959\"}",
            "waterMarkName": "799060.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "capetown-tower-cg6-construction-status-sept-15-799060.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/500773/3/capetown-tower-cg6-construction-status-sept-15-799060.jpeg",
            "imageType": {
                "id": 3,
                "objectType": {
                    "id": 1,
                    "type": "project"
                },
                "mediaType": {
                    "id": 1,
                    "name": "Image"
                },
                "objectTypeId": 1,
                "mediaTypeId": 1,
                "type": "constructionStatus",
                "domainId": 2,
                "priority": 9,
                "imageSitemapEnabled": 0,
                "displayName": "Construction Status"
            },
            "masterImageStatuses": {
                "id": 1,
                "status": "PROCESSED"
            }
        }, {
            "id": 799059,
            "imageTypeId": 3,
            "objectId": 500773,
            "path": "1/500773/3/",
            "pageUrl": "gallery/capetown-tower-cg1-construction-status-sept-15-500773-799059",
            "statusId": 1,
            "createdAt": 1450927153000,
            "takenAt": 1441065600000,
            "sizeInBytes": 429424,
            "width": 810,
            "height": 1080,
            "altText": " capetown Tower CG1 Construction Status Sept-15",
            "title": "Tower CG1 Construction Status Sept-15",
            "jsonDump": "{\"tower_id\":\"11954\"}",
            "waterMarkName": "799059.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "capetown-tower-cg1-construction-status-sept-15-799059.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/500773/3/capetown-tower-cg1-construction-status-sept-15-799059.jpeg",
            "imageType": {
                "id": 3,
                "objectType": {
                    "id": 1,
                    "type": "project"
                },
                "mediaType": {
                    "id": 1,
                    "name": "Image"
                },
                "objectTypeId": 1,
                "mediaTypeId": 1,
                "type": "constructionStatus",
                "domainId": 2,
                "priority": 9,
                "imageSitemapEnabled": 0,
                "displayName": "Construction Status"
            },
            "masterImageStatuses": {
                "id": 1,
                "status": "PROCESSED"
            }
        }, {
            "id": 799058,
            "imageTypeId": 3,
            "objectId": 500773,
            "path": "1/500773/3/",
            "pageUrl": "gallery/capetown-tower-cm2-construction-status-sept-15-500773-799058",
            "statusId": 1,
            "createdAt": 1450927153000,
            "takenAt": 1441065600000,
            "sizeInBytes": 444796,
            "width": 810,
            "height": 1080,
            "altText": " capetown Tower CM2 Construction Status Sept-15",
            "title": "Tower CM2 Construction Status Sept-15",
            "jsonDump": "{\"tower_id\":\"11965\"}",
            "waterMarkName": "799058.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "capetown-tower-cm2-construction-status-sept-15-799058.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/500773/3/capetown-tower-cm2-construction-status-sept-15-799058.jpeg",
            "imageType": {
                "id": 3,
                "objectType": {
                    "id": 1,
                    "type": "project"
                },
                "mediaType": {
                    "id": 1,
                    "name": "Image"
                },
                "objectTypeId": 1,
                "mediaTypeId": 1,
                "type": "constructionStatus",
                "domainId": 2,
                "priority": 9,
                "imageSitemapEnabled": 0,
                "displayName": "Construction Status"
            },
            "masterImageStatuses": {
                "id": 1,
                "status": "PROCESSED"
            }
        }, {
            "id": 799057,
            "imageTypeId": 3,
            "objectId": 500773,
            "path": "1/500773/3/",
            "pageUrl": "gallery/capetown-tower-cc2-construction-status-sept-15-500773-799057",
            "statusId": 1,
            "createdAt": 1450927153000,
            "takenAt": 1441065600000,
            "sizeInBytes": 469765,
            "width": 810,
            "height": 1080,
            "altText": " capetown Tower CC2 Construction Status Sept-15",
            "title": "Tower CC2 Construction Status Sept-15",
            "jsonDump": "{\"tower_id\":\"11970\"}",
            "waterMarkName": "799057.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "capetown-tower-cc2-construction-status-sept-15-799057.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/500773/3/capetown-tower-cc2-construction-status-sept-15-799057.jpeg",
            "imageType": {
                "id": 3,
                "objectType": {
                    "id": 1,
                    "type": "project"
                },
                "mediaType": {
                    "id": 1,
                    "name": "Image"
                },
                "objectTypeId": 1,
                "mediaTypeId": 1,
                "type": "constructionStatus",
                "domainId": 2,
                "priority": 9,
                "imageSitemapEnabled": 0,
                "displayName": "Construction Status"
            },
            "masterImageStatuses": {
                "id": 1,
                "status": "PROCESSED"
            }
        }, {
            "id": 799056,
            "imageTypeId": 3,
            "objectId": 500773,
            "path": "1/500773/3/",
            "pageUrl": "gallery/capetown-tower-cg5-construction-status-sept-15-500773-799056",
            "statusId": 1,
            "createdAt": 1450927153000,
            "takenAt": 1441065600000,
            "sizeInBytes": 451875,
            "width": 810,
            "height": 1080,
            "altText": " capetown Tower CG5 Construction Status Sept-15",
            "title": "Tower CG5 Construction Status Sept-15",
            "jsonDump": "{\"tower_id\":\"11958\"}",
            "waterMarkName": "799056.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "capetown-tower-cg5-construction-status-sept-15-799056.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/500773/3/capetown-tower-cg5-construction-status-sept-15-799056.jpeg",
            "imageType": {
                "id": 3,
                "objectType": {
                    "id": 1,
                    "type": "project"
                },
                "mediaType": {
                    "id": 1,
                    "name": "Image"
                },
                "objectTypeId": 1,
                "mediaTypeId": 1,
                "type": "constructionStatus",
                "domainId": 2,
                "priority": 9,
                "imageSitemapEnabled": 0,
                "displayName": "Construction Status"
            },
            "masterImageStatuses": {
                "id": 1,
                "status": "PROCESSED"
            }
        }, {
            "id": 799055,
            "imageTypeId": 3,
            "objectId": 500773,
            "path": "1/500773/3/",
            "pageUrl": "gallery/capetown-tower-cb1-construction-status-sept-15-500773-799055",
            "statusId": 1,
            "createdAt": 1450927153000,
            "takenAt": 1441065600000,
            "sizeInBytes": 431588,
            "width": 810,
            "height": 1080,
            "altText": " capetown Tower CB1 Construction Status Sept-15",
            "title": "Tower CB1 Construction Status Sept-15",
            "jsonDump": "{\"tower_id\":\"11939\"}",
            "waterMarkName": "799055.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "capetown-tower-cb1-construction-status-sept-15-799055.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/500773/3/capetown-tower-cb1-construction-status-sept-15-799055.jpeg",
            "imageType": {
                "id": 3,
                "objectType": {
                    "id": 1,
                    "type": "project"
                },
                "mediaType": {
                    "id": 1,
                    "name": "Image"
                },
                "objectTypeId": 1,
                "mediaTypeId": 1,
                "type": "constructionStatus",
                "domainId": 2,
                "priority": 9,
                "imageSitemapEnabled": 0,
                "displayName": "Construction Status"
            },
            "masterImageStatuses": {
                "id": 1,
                "status": "PROCESSED"
            }
        }, {
            "id": 799054,
            "imageTypeId": 3,
            "objectId": 500773,
            "path": "1/500773/3/",
            "pageUrl": "gallery/capetown-tower-cm4-construction-status-sept-15-500773-799054",
            "statusId": 1,
            "createdAt": 1450927153000,
            "takenAt": 1441065600000,
            "sizeInBytes": 439531,
            "width": 810,
            "height": 1080,
            "altText": " capetown Tower CM4 Construction Status Sept-15",
            "title": "Tower CM4 Construction Status Sept-15",
            "jsonDump": "{\"tower_id\":\"11967\"}",
            "waterMarkName": "799054.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "capetown-tower-cm4-construction-status-sept-15-799054.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/500773/3/capetown-tower-cm4-construction-status-sept-15-799054.jpeg",
            "imageType": {
                "id": 3,
                "objectType": {
                    "id": 1,
                    "type": "project"
                },
                "mediaType": {
                    "id": 1,
                    "name": "Image"
                },
                "objectTypeId": 1,
                "mediaTypeId": 1,
                "type": "constructionStatus",
                "domainId": 2,
                "priority": 9,
                "imageSitemapEnabled": 0,
                "displayName": "Construction Status"
            },
            "masterImageStatuses": {
                "id": 1,
                "status": "PROCESSED"
            }
        }, {
            "id": 799053,
            "imageTypeId": 3,
            "objectId": 500773,
            "path": "1/500773/3/",
            "pageUrl": "gallery/capetown-tower-cmc1-construction-status-sept-15-500773-799053",
            "statusId": 1,
            "createdAt": 1450927153000,
            "takenAt": 1441065600000,
            "sizeInBytes": 446465,
            "width": 810,
            "height": 1080,
            "altText": " capetown Tower CMC1 Construction Status Sept-15",
            "title": "Tower CMC1 Construction Status Sept-15",
            "jsonDump": "{\"tower_id\":\"11960\"}",
            "waterMarkName": "799053.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "capetown-tower-cmc1-construction-status-sept-15-799053.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/500773/3/capetown-tower-cmc1-construction-status-sept-15-799053.jpeg",
            "imageType": {
                "id": 3,
                "objectType": {
                    "id": 1,
                    "type": "project"
                },
                "mediaType": {
                    "id": 1,
                    "name": "Image"
                },
                "objectTypeId": 1,
                "mediaTypeId": 1,
                "type": "constructionStatus",
                "domainId": 2,
                "priority": 9,
                "imageSitemapEnabled": 0,
                "displayName": "Construction Status"
            },
            "masterImageStatuses": {
                "id": 1,
                "status": "PROCESSED"
            }
        }, {
            "id": 799052,
            "imageTypeId": 3,
            "objectId": 500773,
            "path": "1/500773/3/",
            "pageUrl": "gallery/capetown-tower-cb4-construction-status-sept-15-500773-799052",
            "statusId": 1,
            "createdAt": 1450927153000,
            "takenAt": 1441065600000,
            "sizeInBytes": 554327,
            "width": 1440,
            "height": 684,
            "altText": " capetown Tower CB4 Construction Status Sept-15",
            "title": "Tower CB4 Construction Status Sept-15",
            "jsonDump": "{\"tower_id\":\"11942\"}",
            "waterMarkName": "799052.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "capetown-tower-cb4-construction-status-sept-15-799052.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/500773/3/capetown-tower-cb4-construction-status-sept-15-799052.jpeg",
            "imageType": {
                "id": 3,
                "objectType": {
                    "id": 1,
                    "type": "project"
                },
                "mediaType": {
                    "id": 1,
                    "name": "Image"
                },
                "objectTypeId": 1,
                "mediaTypeId": 1,
                "type": "constructionStatus",
                "domainId": 2,
                "priority": 9,
                "imageSitemapEnabled": 0,
                "displayName": "Construction Status"
            },
            "masterImageStatuses": {
                "id": 1,
                "status": "PROCESSED"
            }
        }, {
            "id": 799051,
            "imageTypeId": 3,
            "objectId": 500773,
            "path": "1/500773/3/",
            "pageUrl": "gallery/capetown-tower-cb2-construction-status-sept-15-500773-799051",
            "statusId": 1,
            "createdAt": 1450927153000,
            "takenAt": 1441065600000,
            "sizeInBytes": 447865,
            "width": 810,
            "height": 1080,
            "altText": " capetown Tower CB2 Construction Status Sept-15",
            "title": "Tower CB2 Construction Status Sept-15",
            "jsonDump": "{\"tower_id\":\"11940\"}",
            "waterMarkName": "799051.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "capetown-tower-cb2-construction-status-sept-15-799051.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/500773/3/capetown-tower-cb2-construction-status-sept-15-799051.jpeg",
            "imageType": {
                "id": 3,
                "objectType": {
                    "id": 1,
                    "type": "project"
                },
                "mediaType": {
                    "id": 1,
                    "name": "Image"
                },
                "objectTypeId": 1,
                "mediaTypeId": 1,
                "type": "constructionStatus",
                "domainId": 2,
                "priority": 9,
                "imageSitemapEnabled": 0,
                "displayName": "Construction Status"
            },
            "masterImageStatuses": {
                "id": 1,
                "status": "PROCESSED"
            }
        }, {
            "id": 799050,
            "imageTypeId": 3,
            "objectId": 500773,
            "path": "1/500773/3/",
            "pageUrl": "gallery/capetown-tower-cg3-construction-status-sept-15-500773-799050",
            "statusId": 1,
            "createdAt": 1450927153000,
            "takenAt": 1441065600000,
            "sizeInBytes": 455555,
            "width": 810,
            "height": 1080,
            "altText": " capetown Tower CG3 Construction Status Sept-15",
            "title": "Tower CG3 Construction Status Sept-15",
            "jsonDump": "{\"tower_id\":\"11956\"}",
            "waterMarkName": "799050.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "capetown-tower-cg3-construction-status-sept-15-799050.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/500773/3/capetown-tower-cg3-construction-status-sept-15-799050.jpeg",
            "imageType": {
                "id": 3,
                "objectType": {
                    "id": 1,
                    "type": "project"
                },
                "mediaType": {
                    "id": 1,
                    "name": "Image"
                },
                "objectTypeId": 1,
                "mediaTypeId": 1,
                "type": "constructionStatus",
                "domainId": 2,
                "priority": 9,
                "imageSitemapEnabled": 0,
                "displayName": "Construction Status"
            },
            "masterImageStatuses": {
                "id": 1,
                "status": "PROCESSED"
            }
        }, {
            "id": 799049,
            "imageTypeId": 3,
            "objectId": 500773,
            "path": "1/500773/3/",
            "pageUrl": "gallery/capetown-tower-cmc3-construction-status-sept-15-500773-799049",
            "statusId": 1,
            "createdAt": 1450927153000,
            "takenAt": 1441065600000,
            "sizeInBytes": 464432,
            "width": 810,
            "height": 1080,
            "altText": " capetown Tower CMC3 Construction Status Sept-15",
            "title": "Tower CMC3 Construction Status Sept-15",
            "jsonDump": "{\"tower_id\":\"11962\"}",
            "waterMarkName": "799049.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "capetown-tower-cmc3-construction-status-sept-15-799049.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/500773/3/capetown-tower-cmc3-construction-status-sept-15-799049.jpeg",
            "imageType": {
                "id": 3,
                "objectType": {
                    "id": 1,
                    "type": "project"
                },
                "mediaType": {
                    "id": 1,
                    "name": "Image"
                },
                "objectTypeId": 1,
                "mediaTypeId": 1,
                "type": "constructionStatus",
                "domainId": 2,
                "priority": 9,
                "imageSitemapEnabled": 0,
                "displayName": "Construction Status"
            },
            "masterImageStatuses": {
                "id": 1,
                "status": "PROCESSED"
            }
        }, {
            "id": 799048,
            "imageTypeId": 3,
            "objectId": 500773,
            "path": "1/500773/3/",
            "pageUrl": "gallery/capetown-tower-cmc2-construction-status-sept-15-500773-799048",
            "statusId": 1,
            "createdAt": 1450927153000,
            "takenAt": 1441065600000,
            "sizeInBytes": 434054,
            "width": 810,
            "height": 1080,
            "altText": " capetown Tower CMC2 Construction Status Sept-15",
            "title": "Tower CMC2 Construction Status Sept-15",
            "jsonDump": "{\"tower_id\":\"11961\"}",
            "waterMarkName": "799048.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "capetown-tower-cmc2-construction-status-sept-15-799048.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/500773/3/capetown-tower-cmc2-construction-status-sept-15-799048.jpeg",
            "imageType": {
                "id": 3,
                "objectType": {
                    "id": 1,
                    "type": "project"
                },
                "mediaType": {
                    "id": 1,
                    "name": "Image"
                },
                "objectTypeId": 1,
                "mediaTypeId": 1,
                "type": "constructionStatus",
                "domainId": 2,
                "priority": 9,
                "imageSitemapEnabled": 0,
                "displayName": "Construction Status"
            },
            "masterImageStatuses": {
                "id": 1,
                "status": "PROCESSED"
            }
        }, {
            "id": 799047,
            "imageTypeId": 3,
            "objectId": 500773,
            "path": "1/500773/3/",
            "pageUrl": "gallery/capetown-tower-cg4-construction-status-sept-15-500773-799047",
            "statusId": 1,
            "createdAt": 1450927153000,
            "takenAt": 1441065600000,
            "sizeInBytes": 457422,
            "width": 810,
            "height": 1080,
            "altText": " capetown Tower CG4 Construction Status Sept-15",
            "title": "Tower CG4 Construction Status Sept-15",
            "jsonDump": "{\"tower_id\":\"11957\"}",
            "waterMarkName": "799047.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "capetown-tower-cg4-construction-status-sept-15-799047.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/500773/3/capetown-tower-cg4-construction-status-sept-15-799047.jpeg",
            "imageType": {
                "id": 3,
                "objectType": {
                    "id": 1,
                    "type": "project"
                },
                "mediaType": {
                    "id": 1,
                    "name": "Image"
                },
                "objectTypeId": 1,
                "mediaTypeId": 1,
                "type": "constructionStatus",
                "domainId": 2,
                "priority": 9,
                "imageSitemapEnabled": 0,
                "displayName": "Construction Status"
            },
            "masterImageStatuses": {
                "id": 1,
                "status": "PROCESSED"
            }
        }, {
            "id": 799046,
            "imageTypeId": 3,
            "objectId": 500773,
            "path": "1/500773/3/",
            "pageUrl": "gallery/capetown-tower-cb2-construction-status-sept-15-500773-799046",
            "statusId": 1,
            "createdAt": 1450927153000,
            "takenAt": 1441065600000,
            "sizeInBytes": 441502,
            "width": 810,
            "height": 1080,
            "altText": " capetown Tower CB2 Construction Status Sept-15",
            "title": "Tower CB2 Construction Status Sept-15",
            "jsonDump": "{\"tower_id\":\"11940\"}",
            "waterMarkName": "799046.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "capetown-tower-cb2-construction-status-sept-15-799046.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/500773/3/capetown-tower-cb2-construction-status-sept-15-799046.jpeg",
            "imageType": {
                "id": 3,
                "objectType": {
                    "id": 1,
                    "type": "project"
                },
                "mediaType": {
                    "id": 1,
                    "name": "Image"
                },
                "objectTypeId": 1,
                "mediaTypeId": 1,
                "type": "constructionStatus",
                "domainId": 2,
                "priority": 9,
                "imageSitemapEnabled": 0,
                "displayName": "Construction Status"
            },
            "masterImageStatuses": {
                "id": 1,
                "status": "PROCESSED"
            }
        }, {
            "id": 799045,
            "imageTypeId": 3,
            "objectId": 500773,
            "path": "1/500773/3/",
            "pageUrl": "gallery/capetown-tower-cc1-construction-status-sept-15-500773-799045",
            "statusId": 1,
            "createdAt": 1450927153000,
            "takenAt": 1441065600000,
            "sizeInBytes": 499739,
            "width": 922,
            "height": 1080,
            "altText": " capetown Tower CC1 Construction Status Sept-15",
            "title": "Tower CC1 Construction Status Sept-15",
            "jsonDump": "{\"tower_id\":\"11969\"}",
            "waterMarkName": "799045.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "capetown-tower-cc1-construction-status-sept-15-799045.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/500773/3/capetown-tower-cc1-construction-status-sept-15-799045.jpeg",
            "imageType": {
                "id": 3,
                "objectType": {
                    "id": 1,
                    "type": "project"
                },
                "mediaType": {
                    "id": 1,
                    "name": "Image"
                },
                "objectTypeId": 1,
                "mediaTypeId": 1,
                "type": "constructionStatus",
                "domainId": 2,
                "priority": 9,
                "imageSitemapEnabled": 0,
                "displayName": "Construction Status"
            },
            "masterImageStatuses": {
                "id": 1,
                "status": "PROCESSED"
            }
        }, {
            "id": 738911,
            "imageTypeId": 3,
            "objectId": 500773,
            "path": "1/500773/3/",
            "pageUrl": "gallery/supertech-capetown-tower-cv9-construction-status-july-15-500773-738911",
            "statusId": 1,
            "createdAt": 1450927153000,
            "takenAt": 1435708800000,
            "sizeInBytes": 172597,
            "width": 1570,
            "height": 932,
            "altText": "supertech capetown Tower CV9 Construction Status July-15",
            "title": "Tower CV9 Construction Status July-15",
            "jsonDump": "{\"tower_id\":\"11937\"}",
            "waterMarkName": "738911.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "supertech-capetown-tower-cv9-construction-status-july-15-738911.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/500773/3/supertech-capetown-tower-cv9-construction-status-july-15-738911.jpeg",
            "imageType": {
                "id": 3,
                "objectType": {
                    "id": 1,
                    "type": "project"
                },
                "mediaType": {
                    "id": 1,
                    "name": "Image"
                },
                "objectTypeId": 1,
                "mediaTypeId": 1,
                "type": "constructionStatus",
                "domainId": 2,
                "priority": 9,
                "imageSitemapEnabled": 0,
                "displayName": "Construction Status"
            },
            "masterImageStatuses": {
                "id": 1,
                "status": "PROCESSED"
            }
        }, {
            "id": 754804,
            "imageTypeId": 8,
            "objectId": 500773,
            "path": "1/500773/8/",
            "pageUrl": "gallery/supertech-capetown-payment-plan-500773-754804",
            "statusId": 1,
            "createdAt": 1450927153000,
            "takenAt": 1377683863000,
            "sizeInBytes": 770451,
            "width": 1842,
            "height": 1000,
            "altText": "supertech capetown Payment Plan",
            "title": "Payment Plan",
            "description": "",
            "waterMarkName": "754804.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "supertech-capetown-payment-plan-754804.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/500773/8/supertech-capetown-payment-plan-754804.jpeg",
            "imageType": {
                "id": 8,
                "objectType": {
                    "id": 1,
                    "type": "project"
                },
                "mediaType": {
                    "id": 1,
                    "name": "Image"
                },
                "objectTypeId": 1,
                "mediaTypeId": 1,
                "type": "paymentPlan",
                "domainId": 2,
                "priority": 9,
                "imageSitemapEnabled": 1,
                "displayName": "Payment Plan"
            },
            "masterImageStatuses": {
                "id": 1,
                "status": "PROCESSED"
            }
        }, {
            "id": 587765,
            "imageTypeId": 8,
            "objectId": 500773,
            "path": "1/500773/8/",
            "pageUrl": "gallery/supertech-capetown-payment-plan-500773-587765",
            "statusId": 1,
            "createdAt": 1450927153000,
            "sizeInBytes": 238008,
            "width": 883,
            "height": 943,
            "altText": "supertech capetown Payment Plan",
            "title": "Payment Plan",
            "description": "",
            "waterMarkName": "587765.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "supertech-capetown-payment-plan-587765.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/500773/8/supertech-capetown-payment-plan-587765.jpeg",
            "imageType": {
                "id": 8,
                "objectType": {
                    "id": 1,
                    "type": "project"
                },
                "mediaType": {
                    "id": 1,
                    "name": "Image"
                },
                "objectTypeId": 1,
                "mediaTypeId": 1,
                "type": "paymentPlan",
                "domainId": 2,
                "priority": 9,
                "imageSitemapEnabled": 1,
                "displayName": "Payment Plan"
            },
            "masterImageStatuses": {
                "id": 1,
                "status": "PROCESSED"
            }
        }],
        "mainImage": {
            "id": 0,
            "imageTypeId": 0,
            "objectId": 0,
            "statusId": 0,
            "sizeInBytes": 0,
            "width": 0,
            "height": 0,
            "altText": "supertech capetown Elevation",
            "title": "Elevation",
            "active": false,
            "activeStatus": 0,
            "absolutePath": "1/500773/6/supertech-capetown-elevation-699489.jpeg"
        },
        "localityLabelPriority": "sector 74:100",
        "suburbLabelPriority": "near city center:100",
        "unitTypeString": "2,3,4 BHK Apartment",
        "distinctBedrooms": [2, 3, 4],
        "avgPriceRisePercentage": 9.7,
        "avgPriceRiseMonths": 26,
        "derivedAvailability": 504,
        "numberOfTowers": 41,
        "supply": 4893,
        "projectSourceId": [101],
        "projectSourceDomain": ["Proptiger"],
        "totalProjectDiscussion": 50,
        "neighborhood": [{
            "id": 49694,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 5,
            "name": "Vindu Roshan",
            "latitude": 28.5744858,
            "longitude": 77.3826828,
            "geoDistance": 0.5062536001205444,
            "vicinity": "Sector 75, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 5,
                "name": "restaurant",
                "description": "Restaurant"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 90141,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 20,
            "name": "Noida Sector 117",
            "latitude": 28.5779781,
            "longitude": 77.3978119,
            "geoDistance": 1.0213899612426758,
            "vicinity": "Noida Sector 117 UP",
            "priority": 1,
            "localityAmenityTypes": {
                "id": 20,
                "name": "road",
                "description": "Road"
            },
            "rating": 5,
            "futureInfrastructure": false
        }, {
            "id": 143763,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Power Ayurveda Clinic Dr.Reyaz Khan",
            "latitude": 28.5857277,
            "longitude": 77.3860626,
            "geoDistance": 1.12366783618927,
            "vicinity": "Sarfabad Village, Sector 73, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 143764,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Power Ayurveda Clinic Dr.Reyaz Khan",
            "latitude": 28.5857277,
            "longitude": 77.3860626,
            "geoDistance": 1.12366783618927,
            "vicinity": "Sarfabad Village, Sector 73, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 72301,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Manav Rachna International School",
            "latitude": 28.5800896,
            "longitude": 77.3764954,
            "geoDistance": 1.1946355104446411,
            "vicinity": "D-196, Sector 51, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 2,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 4,
            "futureInfrastructure": false
        }, {
            "id": 143765,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Shai Hospital",
            "latitude": 28.5845795,
            "longitude": 77.3799896,
            "geoDistance": 1.2381014823913574,
            "vicinity": "Sarfabad Village, Block A, Sector 72, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 143752,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Gyandeep Public School",
            "latitude": 28.5850449,
            "longitude": 77.3792725,
            "geoDistance": 1.321940541267395,
            "vicinity": "Sarfabad Village, Block A, Sector 72, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 83828,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 16,
            "name": "Meghdootam Park",
            "latitude": 28.575367,
            "longitude": 77.3729935,
            "geoDistance": 1.433946967124939,
            "vicinity": "F Block, Block F, Sector 50, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":4.5}",
            "priority": 1,
            "localityAmenityTypes": {
                "id": 16,
                "name": "park",
                "description": "Park"
            },
            "rating": 5,
            "futureInfrastructure": false
        }, {
            "id": 143742,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Yadu Public School",
            "latitude": 28.5884647,
            "longitude": 77.3901215,
            "geoDistance": 1.4370263814926147,
            "vicinity": "Sector 73, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 143743,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Yadu Public School",
            "latitude": 28.5884647,
            "longitude": 77.3901215,
            "geoDistance": 1.4370263814926147,
            "vicinity": "Sector 73, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 87191,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 16,
            "name": "Meghdootam Park",
            "latitude": 28.5754681,
            "longitude": 77.3729401,
            "geoDistance": 1.4389160871505737,
            "vicinity": "F Block Sector 20 Noida",
            "priority": 1,
            "localityAmenityTypes": {
                "id": 16,
                "name": "park",
                "description": "Park"
            },
            "rating": 5,
            "futureInfrastructure": false
        }, {
            "id": 28571,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 6,
            "name": "CNG Gas Station",
            "latitude": 28.5850067,
            "longitude": 77.3754196,
            "geoDistance": 1.5802854299545288,
            "vicinity": "Sector 51, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 6,
                "name": "gas_station",
                "description": "Petrol Pump"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 83826,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 16,
            "name": "Park",
            "latitude": 28.5833244,
            "longitude": 77.3737106,
            "geoDistance": 1.6041299104690552,
            "vicinity": "Block E, Sector 51, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 1,
            "localityAmenityTypes": {
                "id": 16,
                "name": "park",
                "description": "Park"
            },
            "rating": 5,
            "futureInfrastructure": false
        }, {
            "id": 23010,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 5,
            "name": "Lahori Restaurant",
            "latitude": 28.5734043,
            "longitude": 77.3712006,
            "geoDistance": 1.6290194988250732,
            "vicinity": "B 1/28, Central Market, Sector 50, Noida, 201301",
            "restDetails": "{\"price_level\":null,\"rating\":3.5}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 5,
                "name": "restaurant",
                "description": "Restaurant"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 185319,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Sheikh Primary School",
            "latitude": 28.5731792,
            "longitude": 77.4051743,
            "geoDistance": 1.7323360443115234,
            "vicinity": "Sorkha, Sorkha Jahidabad",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 185324,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Sheikh Primary School",
            "latitude": 28.5731792,
            "longitude": 77.4051743,
            "geoDistance": 1.7323360443115234,
            "vicinity": "Sorkha, Sorkha Jahidabad",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 185325,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Sheikh Primary School",
            "latitude": 28.5731792,
            "longitude": 77.4051743,
            "geoDistance": 1.7323360443115234,
            "vicinity": "Sorkha, Sorkha Jahidabad",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 147981,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Jigsaw Abacus Center",
            "latitude": 28.5725098,
            "longitude": 77.3702011,
            "geoDistance": 1.743064284324646,
            "vicinity": "d-160, Sec-50, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 147982,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Jigsaw Abacus Center",
            "latitude": 28.5725098,
            "longitude": 77.3702011,
            "geoDistance": 1.743064284324646,
            "vicinity": "d-160, Sec-50, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 157476,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Jigsaw Abacus Center",
            "latitude": 28.5725098,
            "longitude": 77.3702011,
            "geoDistance": 1.743064284324646,
            "vicinity": "d-160, Sec-50, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 109082,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 1,
            "name": "Jigsaw Abacus Center",
            "latitude": 28.5725098,
            "longitude": 77.3702011,
            "geoDistance": 1.743064284324646,
            "vicinity": "d-160, Sec-50, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 111913,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Jigsaw Abacus Center",
            "latitude": 28.5725098,
            "longitude": 77.3702011,
            "geoDistance": 1.743064284324646,
            "vicinity": "d-160, Sec-50, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 111914,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Jigsaw Abacus Center",
            "latitude": 28.5725098,
            "longitude": 77.3702011,
            "geoDistance": 1.743064284324646,
            "vicinity": "d-160, Sec-50, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 113763,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Jigsaw Abacus Center",
            "latitude": 28.5725098,
            "longitude": 77.3702011,
            "geoDistance": 1.743064284324646,
            "vicinity": "d-160, Sec-50, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 113770,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Jigsaw Abacus Center",
            "latitude": 28.5725098,
            "longitude": 77.3702011,
            "geoDistance": 1.743064284324646,
            "vicinity": "d-160, Sec-50, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 106399,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Jigsaw Abacus Center",
            "latitude": 28.5725098,
            "longitude": 77.3702011,
            "geoDistance": 1.743064284324646,
            "vicinity": "d-160, Sec-50, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 116634,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Jigsaw Abacus Center",
            "latitude": 28.5725098,
            "longitude": 77.3702011,
            "geoDistance": 1.743064284324646,
            "vicinity": "d-160, Sec-50, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 175567,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Jigsaw Abacus Center",
            "latitude": 28.5725098,
            "longitude": 77.3702011,
            "geoDistance": 1.743064284324646,
            "vicinity": "d-160, Sec-50, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 158343,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 1,
            "name": "Jigsaw Abacus Center",
            "latitude": 28.5725098,
            "longitude": 77.3702011,
            "geoDistance": 1.743064284324646,
            "vicinity": "d-160, Sec-50, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 164636,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 1,
            "name": "Jigsaw Abacus Center",
            "latitude": 28.5725098,
            "longitude": 77.3702011,
            "geoDistance": 1.743064284324646,
            "vicinity": "d-160, Sec-50, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 165454,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Jigsaw Abacus Center",
            "latitude": 28.5725098,
            "longitude": 77.3702011,
            "geoDistance": 1.743064284324646,
            "vicinity": "d-160, Sec-50, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 135924,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Jigsaw Abacus Center",
            "latitude": 28.5725098,
            "longitude": 77.3702011,
            "geoDistance": 1.743064284324646,
            "vicinity": "d-160, Sec-50, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 125785,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Jigsaw Abacus Center",
            "latitude": 28.5725098,
            "longitude": 77.3702011,
            "geoDistance": 1.743064284324646,
            "vicinity": "d-160, Sec-50, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 125816,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Jigsaw Abacus Center",
            "latitude": 28.5725098,
            "longitude": 77.3702011,
            "geoDistance": 1.743064284324646,
            "vicinity": "d-160, Sec-50, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 133500,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 1,
            "name": "Jigsaw Abacus Center",
            "latitude": 28.5725098,
            "longitude": 77.3702011,
            "geoDistance": 1.743064284324646,
            "vicinity": "d-160, Sec-50, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 134222,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 1,
            "name": "Jigsaw Abacus Center",
            "latitude": 28.5725098,
            "longitude": 77.3702011,
            "geoDistance": 1.743064284324646,
            "vicinity": "d-160, Sec-50, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 123706,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 1,
            "name": "Jigsaw Abacus Center",
            "latitude": 28.5725098,
            "longitude": 77.3702011,
            "geoDistance": 1.743064284324646,
            "vicinity": "d-160, Sec-50, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 124428,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Jigsaw Abacus Center",
            "latitude": 28.5725098,
            "longitude": 77.3702011,
            "geoDistance": 1.743064284324646,
            "vicinity": "d-160, Sec-50, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 23575,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 14,
            "name": "JIGSAW ABACUS Center",
            "latitude": 28.5725594,
            "longitude": 77.3701477,
            "geoDistance": 1.747072696685791,
            "vicinity": "D-160, Sec-50, Sector 50, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 3,
            "localityAmenityTypes": {
                "id": 14,
                "name": "play_school",
                "description": "Play School"
            },
            "rating": 3,
            "futureInfrastructure": false
        }, {
            "id": 185316,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Udgam Play School",
            "latitude": 28.587122,
            "longitude": 77.4001007,
            "geoDistance": 1.754968523979187,
            "vicinity": "Sector 119, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 185321,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Udgam Play School",
            "latitude": 28.587122,
            "longitude": 77.4001007,
            "geoDistance": 1.754968523979187,
            "vicinity": "Sector 119, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 143753,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Udgam Play School",
            "latitude": 28.587122,
            "longitude": 77.4001007,
            "geoDistance": 1.754968523979187,
            "vicinity": "Sector 119, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 143739,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Raghav Global School",
            "latitude": 28.5922337,
            "longitude": 77.389595,
            "geoDistance": 1.8455910682678223,
            "vicinity": "SS 1 Block-A, Sector 122, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 3160,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 15,
            "name": "Rajiv Gandhi Institute of Petroleum Technology",
            "latitude": 28.5925674,
            "longitude": 77.3851013,
            "geoDistance": 1.889904260635376,
            "vicinity": "Sector 73, Basi Bahuddin Nagar",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 15,
                "name": "higher_education",
                "description": "Higher Education"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 185315,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Rajiv Gandhi Institute of Petroleum Technology",
            "latitude": 28.5925674,
            "longitude": 77.3851013,
            "geoDistance": 1.889904260635376,
            "vicinity": "Sector 73, Basi Bahuddin Nagar",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 185320,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Rajiv Gandhi Institute of Petroleum Technology",
            "latitude": 28.5925674,
            "longitude": 77.3851013,
            "geoDistance": 1.889904260635376,
            "vicinity": "Sector 73, Basi Bahuddin Nagar",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 143741,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Rajiv Gandhi Institute of Petroleum Technology",
            "latitude": 28.5925674,
            "longitude": 77.3851013,
            "geoDistance": 1.889904260635376,
            "vicinity": "Sector 73, Basi Bahuddin Nagar",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 25598,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 5,
            "name": "Slice of Italy",
            "latitude": 28.585804,
            "longitude": 77.3720779,
            "geoDistance": 1.8909624814987183,
            "vicinity": "VDS Market, Gali Number 9, Hoshiyarpur, Block F, Sector 51, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 5,
                "name": "restaurant",
                "description": "Restaurant"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 76491,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Raghav Global School",
            "latitude": 28.5926991,
            "longitude": 77.3895035,
            "geoDistance": 1.8962156772613525,
            "website": "http://www.raghavglobalschool.in/home1.html",
            "vicinity": "SS 1 Block-A, Sector 122, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 3,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 3,
            "futureInfrastructure": false
        }, {
            "id": 8073,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 5,
            "name": "Food Factory",
            "latitude": 28.5860424,
            "longitude": 77.3721161,
            "geoDistance": 1.9037870168685913,
            "vicinity": "Sector 51, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 5,
                "name": "restaurant",
                "description": "Restaurant"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 57657,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 5,
            "name": "Nawabs - The Taste of Lucknow",
            "latitude": 28.5858669,
            "longitude": 77.3718567,
            "geoDistance": 1.9125308990478516,
            "vicinity": "Shop No 12, VDS Market, Gali Number 9, Hoshiyarpur, Sector 51, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 5,
                "name": "restaurant",
                "description": "Restaurant"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 83819,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 16,
            "name": "Park",
            "latitude": 28.5922298,
            "longitude": 77.3933258,
            "geoDistance": 1.9167819023132324,
            "vicinity": "Block A, Sector 122, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 1,
            "localityAmenityTypes": {
                "id": 16,
                "name": "park",
                "description": "Park"
            },
            "rating": 5,
            "futureInfrastructure": false
        }, {
            "id": 87208,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Nilgiri Hills Public School",
            "latitude": 28.5765705,
            "longitude": 77.3680573,
            "geoDistance": 1.917776107788086,
            "vicinity": "Nilgiri Hills Public School F1 F Block Sector 50 Noida",
            "priority": 1,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 5,
            "futureInfrastructure": false
        }, {
            "id": 2757,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 5,
            "name": "Raj Tadka Pure Veg Rajasthani Restaurant",
            "latitude": 28.5859661,
            "longitude": 77.3718719,
            "geoDistance": 1.9177958965301514,
            "vicinity": "Gali Number 9, Sector 51, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 5,
                "name": "restaurant",
                "description": "Restaurant"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 76737,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Srijan Clinic",
            "latitude": 28.5790005,
            "longitude": 77.3683167,
            "geoDistance": 1.924871563911438,
            "website": "http://www.gynaecologistinnoida.com/",
            "vicinity": "C-85, Sector-51, Noida, Uttar Pradesh 201307, Block D, Sector 51, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":4.7}",
            "priority": 4,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 2,
            "futureInfrastructure": false
        }, {
            "id": 83820,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 16,
            "name": "Park",
            "latitude": 28.5926399,
            "longitude": 77.3922272,
            "geoDistance": 1.9330681562423706,
            "vicinity": "Block A, Sector 122, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 1,
            "localityAmenityTypes": {
                "id": 16,
                "name": "park",
                "description": "Park"
            },
            "rating": 5,
            "futureInfrastructure": false
        }, {
            "id": 80205,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 16,
            "name": "Central Park",
            "latitude": 28.5879612,
            "longitude": 77.4017487,
            "geoDistance": 1.934184193611145,
            "vicinity": "Sector 119, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 1,
            "localityAmenityTypes": {
                "id": 16,
                "name": "park",
                "description": "Park"
            },
            "rating": 5,
            "futureInfrastructure": false
        }, {
            "id": 9326,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 6,
            "name": "Petrol Pump",
            "latitude": 28.5916538,
            "longitude": 77.3794937,
            "geoDistance": 1.9432682991027832,
            "vicinity": "Sector 71, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 6,
                "name": "gas_station",
                "description": "Petrol Pump"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 83825,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 16,
            "name": "Triangular Park",
            "latitude": 28.5892143,
            "longitude": 77.3748398,
            "geoDistance": 1.9547388553619385,
            "vicinity": "Block A, Sector 71, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 1,
            "localityAmenityTypes": {
                "id": 16,
                "name": "park",
                "description": "Park"
            },
            "rating": 5,
            "futureInfrastructure": false
        }, {
            "id": 83821,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 16,
            "name": "Block A Park",
            "latitude": 28.5933952,
            "longitude": 77.3905716,
            "geoDistance": 1.9854408502578735,
            "vicinity": "Block A, Sector 122, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 1,
            "localityAmenityTypes": {
                "id": 16,
                "name": "park",
                "description": "Park"
            },
            "rating": 5,
            "futureInfrastructure": false
        }, {
            "id": 143747,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Shambhu Dayal School",
            "latitude": 28.5764389,
            "longitude": 77.4079971,
            "geoDistance": 1.9863508939743042,
            "vicinity": "Pusta Road, Khanda, SONEPAT",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 143748,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Shambhu Dayal School",
            "latitude": 28.5764389,
            "longitude": 77.4079971,
            "geoDistance": 1.9863508939743042,
            "vicinity": "Pusta Road, Khanda, SONEPAT",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 143738,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "SD Girls High School",
            "latitude": 28.5765419,
            "longitude": 77.4080429,
            "geoDistance": 1.991292119026184,
            "vicinity": "Sorkha, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 39097,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 5,
            "name": "32 Desi Canteen",
            "latitude": 28.5849094,
            "longitude": 77.3701401,
            "geoDistance": 1.9936738014221191,
            "vicinity": "Hoshiyarpur, Hoshiarpur Village, Sector 51, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 5,
                "name": "restaurant",
                "description": "Restaurant"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 38962,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 5,
            "name": "Yadav Hotel",
            "latitude": 28.5848656,
            "longitude": 77.3700943,
            "geoDistance": 1.9949862957000732,
            "vicinity": "Hoshiyarpur, Hoshiarpur Village, Sector 51, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 5,
                "name": "restaurant",
                "description": "Restaurant"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 83822,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 16,
            "name": "Noida Green Belt",
            "latitude": 28.5928364,
            "longitude": 77.3814316,
            "geoDistance": 1.9982998371124268,
            "vicinity": "Sector 70, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 1,
            "localityAmenityTypes": {
                "id": 16,
                "name": "park",
                "description": "Park"
            },
            "rating": 5,
            "futureInfrastructure": false
        }, {
            "id": 30198,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Shivalik Medical Center",
            "latitude": 28.5843716,
            "longitude": 77.3697052,
            "geoDistance": 2.0008039474487305,
            "vicinity": "Hoshiyarpur, Hoshiarpur Village, Sector 51, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 4,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 2,
            "futureInfrastructure": false
        }, {
            "id": 67683,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 5,
            "name": "Yadav Hotel & Resturant",
            "latitude": 28.5845852,
            "longitude": 77.369812,
            "geoDistance": 2.003181219100952,
            "vicinity": "Hoshiyarpur, Hoshiarpur Village, Sector 51, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 5,
                "name": "restaurant",
                "description": "Restaurant"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 4832,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 14,
            "name": "Windows Play School",
            "latitude": 28.5721207,
            "longitude": 77.367569,
            "geoDistance": 2.00357723236084,
            "vicinity": "Sector 50, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 1,
            "localityAmenityTypes": {
                "id": 14,
                "name": "play_school",
                "description": "Play School"
            },
            "rating": 5,
            "futureInfrastructure": false
        }, {
            "id": 157481,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Windows Play School",
            "latitude": 28.5721207,
            "longitude": 77.367569,
            "geoDistance": 2.00357723236084,
            "vicinity": "E-6/A, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 157482,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Windows Play School",
            "latitude": 28.5721207,
            "longitude": 77.367569,
            "geoDistance": 2.00357723236084,
            "vicinity": "E-6/A, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 157483,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Windows Play School",
            "latitude": 28.5721207,
            "longitude": 77.367569,
            "geoDistance": 2.00357723236084,
            "vicinity": "E-6/A, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 157484,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Windows Play School",
            "latitude": 28.5721207,
            "longitude": 77.367569,
            "geoDistance": 2.00357723236084,
            "vicinity": "E-6/A, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 157485,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Windows Play School",
            "latitude": 28.5721207,
            "longitude": 77.367569,
            "geoDistance": 2.00357723236084,
            "vicinity": "E-6/A, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 157486,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Windows Play School",
            "latitude": 28.5721207,
            "longitude": 77.367569,
            "geoDistance": 2.00357723236084,
            "vicinity": "E-6/A, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 109087,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 1,
            "name": "Windows Play School",
            "latitude": 28.5721207,
            "longitude": 77.367569,
            "geoDistance": 2.00357723236084,
            "vicinity": "E-6/A, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 109088,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 1,
            "name": "Windows Play School",
            "latitude": 28.5721207,
            "longitude": 77.367569,
            "geoDistance": 2.00357723236084,
            "vicinity": "E-6/A, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 109089,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 1,
            "name": "Windows Play School",
            "latitude": 28.5721207,
            "longitude": 77.367569,
            "geoDistance": 2.00357723236084,
            "vicinity": "E-6/A, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 109090,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 1,
            "name": "Windows Play School",
            "latitude": 28.5721207,
            "longitude": 77.367569,
            "geoDistance": 2.00357723236084,
            "vicinity": "E-6/A, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 106404,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Windows Play School",
            "latitude": 28.5721207,
            "longitude": 77.367569,
            "geoDistance": 2.00357723236084,
            "vicinity": "E-6/A, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 106405,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Windows Play School",
            "latitude": 28.5721207,
            "longitude": 77.367569,
            "geoDistance": 2.00357723236084,
            "vicinity": "E-6/A, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 106406,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Windows Play School",
            "latitude": 28.5721207,
            "longitude": 77.367569,
            "geoDistance": 2.00357723236084,
            "vicinity": "E-6/A, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 106407,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Windows Play School",
            "latitude": 28.5721207,
            "longitude": 77.367569,
            "geoDistance": 2.00357723236084,
            "vicinity": "E-6/A, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 116639,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Windows Play School",
            "latitude": 28.5721207,
            "longitude": 77.367569,
            "geoDistance": 2.00357723236084,
            "vicinity": "E-6/A, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 116640,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Windows Play School",
            "latitude": 28.5721207,
            "longitude": 77.367569,
            "geoDistance": 2.00357723236084,
            "vicinity": "E-6/A, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 116641,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Windows Play School",
            "latitude": 28.5721207,
            "longitude": 77.367569,
            "geoDistance": 2.00357723236084,
            "vicinity": "E-6/A, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 116642,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Windows Play School",
            "latitude": 28.5721207,
            "longitude": 77.367569,
            "geoDistance": 2.00357723236084,
            "vicinity": "E-6/A, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 116643,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Windows Play School",
            "latitude": 28.5721207,
            "longitude": 77.367569,
            "geoDistance": 2.00357723236084,
            "vicinity": "E-6/A, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 116644,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Windows Play School",
            "latitude": 28.5721207,
            "longitude": 77.367569,
            "geoDistance": 2.00357723236084,
            "vicinity": "E-6/A, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 175571,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Windows Play School",
            "latitude": 28.5721207,
            "longitude": 77.367569,
            "geoDistance": 2.00357723236084,
            "vicinity": "E-6/A, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 158348,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 1,
            "name": "Windows Play School",
            "latitude": 28.5721207,
            "longitude": 77.367569,
            "geoDistance": 2.00357723236084,
            "vicinity": "E-6/A, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 158349,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 1,
            "name": "Windows Play School",
            "latitude": 28.5721207,
            "longitude": 77.367569,
            "geoDistance": 2.00357723236084,
            "vicinity": "E-6/A, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 158350,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 1,
            "name": "Windows Play School",
            "latitude": 28.5721207,
            "longitude": 77.367569,
            "geoDistance": 2.00357723236084,
            "vicinity": "E-6/A, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 158351,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 1,
            "name": "Windows Play School",
            "latitude": 28.5721207,
            "longitude": 77.367569,
            "geoDistance": 2.00357723236084,
            "vicinity": "E-6/A, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 164641,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 1,
            "name": "Windows Play School",
            "latitude": 28.5721207,
            "longitude": 77.367569,
            "geoDistance": 2.00357723236084,
            "vicinity": "E-6/A, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 164642,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 1,
            "name": "Windows Play School",
            "latitude": 28.5721207,
            "longitude": 77.367569,
            "geoDistance": 2.00357723236084,
            "vicinity": "E-6/A, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 164643,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 1,
            "name": "Windows Play School",
            "latitude": 28.5721207,
            "longitude": 77.367569,
            "geoDistance": 2.00357723236084,
            "vicinity": "E-6/A, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 164644,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 1,
            "name": "Windows Play School",
            "latitude": 28.5721207,
            "longitude": 77.367569,
            "geoDistance": 2.00357723236084,
            "vicinity": "E-6/A, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 135929,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Windows Play School",
            "latitude": 28.5721207,
            "longitude": 77.367569,
            "geoDistance": 2.00357723236084,
            "vicinity": "E-6/A, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 135930,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Windows Play School",
            "latitude": 28.5721207,
            "longitude": 77.367569,
            "geoDistance": 2.00357723236084,
            "vicinity": "E-6/A, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 135931,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Windows Play School",
            "latitude": 28.5721207,
            "longitude": 77.367569,
            "geoDistance": 2.00357723236084,
            "vicinity": "E-6/A, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 135932,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Windows Play School",
            "latitude": 28.5721207,
            "longitude": 77.367569,
            "geoDistance": 2.00357723236084,
            "vicinity": "E-6/A, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 133505,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 1,
            "name": "Windows Play School",
            "latitude": 28.5721207,
            "longitude": 77.367569,
            "geoDistance": 2.00357723236084,
            "vicinity": "E-6/A, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 133506,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 1,
            "name": "Windows Play School",
            "latitude": 28.5721207,
            "longitude": 77.367569,
            "geoDistance": 2.00357723236084,
            "vicinity": "E-6/A, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 133507,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 1,
            "name": "Windows Play School",
            "latitude": 28.5721207,
            "longitude": 77.367569,
            "geoDistance": 2.00357723236084,
            "vicinity": "E-6/A, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 133508,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 1,
            "name": "Windows Play School",
            "latitude": 28.5721207,
            "longitude": 77.367569,
            "geoDistance": 2.00357723236084,
            "vicinity": "E-6/A, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 134227,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 1,
            "name": "Windows Play School",
            "latitude": 28.5721207,
            "longitude": 77.367569,
            "geoDistance": 2.00357723236084,
            "vicinity": "E-6/A, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 134228,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 1,
            "name": "Windows Play School",
            "latitude": 28.5721207,
            "longitude": 77.367569,
            "geoDistance": 2.00357723236084,
            "vicinity": "E-6/A, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 134229,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 1,
            "name": "Windows Play School",
            "latitude": 28.5721207,
            "longitude": 77.367569,
            "geoDistance": 2.00357723236084,
            "vicinity": "E-6/A, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 134230,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 1,
            "name": "Windows Play School",
            "latitude": 28.5721207,
            "longitude": 77.367569,
            "geoDistance": 2.00357723236084,
            "vicinity": "E-6/A, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 123711,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 1,
            "name": "Windows Play School",
            "latitude": 28.5721207,
            "longitude": 77.367569,
            "geoDistance": 2.00357723236084,
            "vicinity": "E-6/A, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 123712,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 1,
            "name": "Windows Play School",
            "latitude": 28.5721207,
            "longitude": 77.367569,
            "geoDistance": 2.00357723236084,
            "vicinity": "E-6/A, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 123713,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 1,
            "name": "Windows Play School",
            "latitude": 28.5721207,
            "longitude": 77.367569,
            "geoDistance": 2.00357723236084,
            "vicinity": "E-6/A, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 123714,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 1,
            "name": "Windows Play School",
            "latitude": 28.5721207,
            "longitude": 77.367569,
            "geoDistance": 2.00357723236084,
            "vicinity": "E-6/A, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 124433,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Windows Play School",
            "latitude": 28.5721207,
            "longitude": 77.367569,
            "geoDistance": 2.00357723236084,
            "vicinity": "E-6/A, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 124434,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Windows Play School",
            "latitude": 28.5721207,
            "longitude": 77.367569,
            "geoDistance": 2.00357723236084,
            "vicinity": "E-6/A, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 124435,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Windows Play School",
            "latitude": 28.5721207,
            "longitude": 77.367569,
            "geoDistance": 2.00357723236084,
            "vicinity": "E-6/A, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 124436,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Windows Play School",
            "latitude": 28.5721207,
            "longitude": 77.367569,
            "geoDistance": 2.00357723236084,
            "vicinity": "E-6/A, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 227298,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 17,
            "name": "Propplanner.com",
            "latitude": 28.5846329,
            "longitude": 77.3698196,
            "geoDistance": 2.005258321762085,
            "vicinity": "Gali Number 7, Hoshiyarpur, Hoshiarpur Village, Sector 51, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 17,
                "name": "shopping_mall",
                "description": "Shopping Mall"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 227467,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 17,
            "name": "Propplanner.com",
            "latitude": 28.5846329,
            "longitude": 77.3698196,
            "geoDistance": 2.005258321762085,
            "vicinity": "Gali Number 7, Hoshiyarpur, Hoshiarpur Village, Sector 51, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 17,
                "name": "shopping_mall",
                "description": "Shopping Mall"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 227468,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 17,
            "name": "Propplanner.com",
            "latitude": 28.5846329,
            "longitude": 77.3698196,
            "geoDistance": 2.005258321762085,
            "vicinity": "Gali Number 7, Hoshiyarpur, Hoshiarpur Village, Sector 51, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 17,
                "name": "shopping_mall",
                "description": "Shopping Mall"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 227469,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 17,
            "name": "Propplanner.com",
            "latitude": 28.5846329,
            "longitude": 77.3698196,
            "geoDistance": 2.005258321762085,
            "vicinity": "Gali Number 7, Hoshiyarpur, Hoshiarpur Village, Sector 51, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 17,
                "name": "shopping_mall",
                "description": "Shopping Mall"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 229341,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 17,
            "name": "Propplanner.com",
            "latitude": 28.5846329,
            "longitude": 77.3698196,
            "geoDistance": 2.005258321762085,
            "vicinity": "Gali Number 7, Hoshiyarpur, Hoshiarpur Village, Sector 51, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 17,
                "name": "shopping_mall",
                "description": "Shopping Mall"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 231224,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 17,
            "name": "Propplanner.com",
            "latitude": 28.5846329,
            "longitude": 77.3698196,
            "geoDistance": 2.005258321762085,
            "vicinity": "Gali Number 7, Hoshiyarpur, Hoshiarpur Village, Sector 51, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 17,
                "name": "shopping_mall",
                "description": "Shopping Mall"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 231838,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 17,
            "name": "Propplanner.com",
            "latitude": 28.5846329,
            "longitude": 77.3698196,
            "geoDistance": 2.005258321762085,
            "vicinity": "Gali Number 7, Hoshiyarpur, Hoshiarpur Village, Sector 51, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 17,
                "name": "shopping_mall",
                "description": "Shopping Mall"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 231839,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 17,
            "name": "Propplanner.com",
            "latitude": 28.5846329,
            "longitude": 77.3698196,
            "geoDistance": 2.005258321762085,
            "vicinity": "Gali Number 7, Hoshiyarpur, Hoshiarpur Village, Sector 51, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 17,
                "name": "shopping_mall",
                "description": "Shopping Mall"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 76822,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Ramagya School",
            "latitude": 28.5738297,
            "longitude": 77.3669662,
            "geoDistance": 2.032865524291992,
            "vicinity": "E-7 Sector 50 Noida",
            "priority": 3,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 3,
            "futureInfrastructure": false
        }, {
            "id": 185318,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Swami Vivekanand School",
            "latitude": 28.5904045,
            "longitude": 77.4000931,
            "geoDistance": 2.0337917804718018,
            "vicinity": "Parthala Khanjarpur, Sector 122, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 185323,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Swami Vivekanand School",
            "latitude": 28.5904045,
            "longitude": 77.4000931,
            "geoDistance": 2.0337917804718018,
            "vicinity": "Parthala Khanjarpur, Sector 122, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 1227,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Global Indian International School",
            "latitude": 28.5922718,
            "longitude": 77.3785629,
            "geoDistance": 2.044015884399414,
            "vicinity": "D-5,Sector 71, Noida,Uttar Pradesh, Sector 71, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 1,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 5,
            "futureInfrastructure": false
        }, {
            "id": 69928,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 14,
            "name": "S D Public School",
            "latitude": 28.5669422,
            "longitude": 77.3692322,
            "geoDistance": 2.0484440326690674,
            "website": "http://sdvidyanoida.com/",
            "vicinity": "Jain Road, Baraula, Sector 49, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 4,
            "localityAmenityTypes": {
                "id": 14,
                "name": "play_school",
                "description": "Play School"
            },
            "rating": 2,
            "futureInfrastructure": false
        }, {
            "id": 157479,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "S D Public School",
            "latitude": 28.5669422,
            "longitude": 77.3692322,
            "geoDistance": 2.0484440326690674,
            "vicinity": "Jain Rd, Baraula, Sector 49, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 109085,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 1,
            "name": "S D Public School",
            "latitude": 28.5669422,
            "longitude": 77.3692322,
            "geoDistance": 2.0484440326690674,
            "vicinity": "Jain Rd, Baraula, Sector 49, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 113772,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "S D Public School",
            "latitude": 28.5669422,
            "longitude": 77.3692322,
            "geoDistance": 2.0484440326690674,
            "vicinity": "Jain Rd, Baraula, Sector 49, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 113765,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "S D Public School",
            "latitude": 28.5669422,
            "longitude": 77.3692322,
            "geoDistance": 2.0484440326690674,
            "vicinity": "Jain Rd, Baraula, Sector 49, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 106402,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "S D Public School",
            "latitude": 28.5669422,
            "longitude": 77.3692322,
            "geoDistance": 2.0484440326690674,
            "vicinity": "Jain Rd, Baraula, Sector 49, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 116637,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "S D Public School",
            "latitude": 28.5669422,
            "longitude": 77.3692322,
            "geoDistance": 2.0484440326690674,
            "vicinity": "Jain Rd, Baraula, Sector 49, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 175569,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "S D Public School",
            "latitude": 28.5669422,
            "longitude": 77.3692322,
            "geoDistance": 2.0484440326690674,
            "vicinity": "Jain Rd, Baraula, Sector 49, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 158346,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 1,
            "name": "S D Public School",
            "latitude": 28.5669422,
            "longitude": 77.3692322,
            "geoDistance": 2.0484440326690674,
            "vicinity": "Jain Rd, Baraula, Sector 49, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 164639,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 1,
            "name": "S D Public School",
            "latitude": 28.5669422,
            "longitude": 77.3692322,
            "geoDistance": 2.0484440326690674,
            "vicinity": "Jain Rd, Baraula, Sector 49, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 135927,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "S D Public School",
            "latitude": 28.5669422,
            "longitude": 77.3692322,
            "geoDistance": 2.0484440326690674,
            "vicinity": "Jain Rd, Baraula, Sector 49, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 133503,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 1,
            "name": "S D Public School",
            "latitude": 28.5669422,
            "longitude": 77.3692322,
            "geoDistance": 2.0484440326690674,
            "vicinity": "Jain Rd, Baraula, Sector 49, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 134225,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 1,
            "name": "S D Public School",
            "latitude": 28.5669422,
            "longitude": 77.3692322,
            "geoDistance": 2.0484440326690674,
            "vicinity": "Jain Rd, Baraula, Sector 49, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 123709,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 1,
            "name": "S D Public School",
            "latitude": 28.5669422,
            "longitude": 77.3692322,
            "geoDistance": 2.0484440326690674,
            "vicinity": "Jain Rd, Baraula, Sector 49, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 124431,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "S D Public School",
            "latitude": 28.5669422,
            "longitude": 77.3692322,
            "geoDistance": 2.0484440326690674,
            "vicinity": "Jain Rd, Baraula, Sector 49, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 215659,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 17,
            "name": "PAN Oasis Shopping Complex",
            "latitude": 28.5932541,
            "longitude": 77.3811646,
            "geoDistance": 2.0504422187805176,
            "vicinity": "Sector 70, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 17,
                "name": "shopping_mall",
                "description": "Shopping Mall"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 215660,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 17,
            "name": "PAN Oasis Shopping Complex",
            "latitude": 28.5932541,
            "longitude": 77.3811646,
            "geoDistance": 2.0504422187805176,
            "vicinity": "Sector 70, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 17,
                "name": "shopping_mall",
                "description": "Shopping Mall"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 67844,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Ashwini Hospital",
            "latitude": 28.5789871,
            "longitude": 77.3669662,
            "geoDistance": 2.0542454719543457,
            "website": "http://www.ashwinihealthcare.com/contact.html",
            "vicinity": "Hoshiarpur Village, Sector 51, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 4,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 2,
            "futureInfrastructure": false
        }, {
            "id": 147976,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Ramagya School",
            "latitude": 28.5738792,
            "longitude": 77.3667145,
            "geoDistance": 2.056797981262207,
            "vicinity": "E-7, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 157473,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Ramagya School",
            "latitude": 28.5738792,
            "longitude": 77.3667145,
            "geoDistance": 2.056797981262207,
            "vicinity": "E-7, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 109079,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 1,
            "name": "Ramagya School",
            "latitude": 28.5738792,
            "longitude": 77.3667145,
            "geoDistance": 2.056797981262207,
            "vicinity": "E-7, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 111910,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Ramagya School",
            "latitude": 28.5738792,
            "longitude": 77.3667145,
            "geoDistance": 2.056797981262207,
            "vicinity": "E-7, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 113760,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Ramagya School",
            "latitude": 28.5738792,
            "longitude": 77.3667145,
            "geoDistance": 2.056797981262207,
            "vicinity": "E-7, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 113767,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Ramagya School",
            "latitude": 28.5738792,
            "longitude": 77.3667145,
            "geoDistance": 2.056797981262207,
            "vicinity": "E-7, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 106396,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Ramagya School",
            "latitude": 28.5738792,
            "longitude": 77.3667145,
            "geoDistance": 2.056797981262207,
            "vicinity": "E-7, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 116631,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Ramagya School",
            "latitude": 28.5738792,
            "longitude": 77.3667145,
            "geoDistance": 2.056797981262207,
            "vicinity": "E-7, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 175564,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Ramagya School",
            "latitude": 28.5738792,
            "longitude": 77.3667145,
            "geoDistance": 2.056797981262207,
            "vicinity": "E-7, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 158340,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 1,
            "name": "Ramagya School",
            "latitude": 28.5738792,
            "longitude": 77.3667145,
            "geoDistance": 2.056797981262207,
            "vicinity": "E-7, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 164633,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 1,
            "name": "Ramagya School",
            "latitude": 28.5738792,
            "longitude": 77.3667145,
            "geoDistance": 2.056797981262207,
            "vicinity": "E-7, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 165452,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Ramagya School",
            "latitude": 28.5738792,
            "longitude": 77.3667145,
            "geoDistance": 2.056797981262207,
            "vicinity": "E-7, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 135921,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Ramagya School",
            "latitude": 28.5738792,
            "longitude": 77.3667145,
            "geoDistance": 2.056797981262207,
            "vicinity": "E-7, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 125782,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Ramagya School",
            "latitude": 28.5738792,
            "longitude": 77.3667145,
            "geoDistance": 2.056797981262207,
            "vicinity": "E-7, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 125813,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Ramagya School",
            "latitude": 28.5738792,
            "longitude": 77.3667145,
            "geoDistance": 2.056797981262207,
            "vicinity": "E-7, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 133497,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 1,
            "name": "Ramagya School",
            "latitude": 28.5738792,
            "longitude": 77.3667145,
            "geoDistance": 2.056797981262207,
            "vicinity": "E-7, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 134219,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 1,
            "name": "Ramagya School",
            "latitude": 28.5738792,
            "longitude": 77.3667145,
            "geoDistance": 2.056797981262207,
            "vicinity": "E-7, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 123703,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 1,
            "name": "Ramagya School",
            "latitude": 28.5738792,
            "longitude": 77.3667145,
            "geoDistance": 2.056797981262207,
            "vicinity": "E-7, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 124425,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Ramagya School",
            "latitude": 28.5738792,
            "longitude": 77.3667145,
            "geoDistance": 2.056797981262207,
            "vicinity": "E-7, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 25273,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 5,
            "name": "Aggarwal Sweets",
            "latitude": 28.5837231,
            "longitude": 77.3685989,
            "geoDistance": 2.0640640258789062,
            "vicinity": "Gali Number 6, Hoshiyarpur, Hoshiarpur Village, Sector 51, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 5,
                "name": "restaurant",
                "description": "Restaurant"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 49577,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 5,
            "name": "Nawabi Biryani",
            "latitude": 28.5696888,
            "longitude": 77.367569,
            "geoDistance": 2.074610948562622,
            "vicinity": "Sector 50, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 5,
                "name": "restaurant",
                "description": "Restaurant"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 83824,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 16,
            "name": "Central Park",
            "latitude": 28.5915394,
            "longitude": 77.3763657,
            "geoDistance": 2.07653546333313,
            "vicinity": "Sector 71, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 1,
            "localityAmenityTypes": {
                "id": 16,
                "name": "park",
                "description": "Park"
            },
            "rating": 5,
            "futureInfrastructure": false
        }, {
            "id": 10193,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "Axis Bank ATM",
            "latitude": 28.5743561,
            "longitude": 77.3663864,
            "geoDistance": 2.0841832160949707,
            "vicinity": "Sector 50",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 147988,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "Axis Bank ATM",
            "latitude": 28.5743561,
            "longitude": 77.3663864,
            "geoDistance": 2.0841832160949707,
            "vicinity": "Sector 50",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 147989,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "Axis Bank ATM",
            "latitude": 28.5743561,
            "longitude": 77.3663864,
            "geoDistance": 2.0841832160949707,
            "vicinity": "Sector 50",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 157501,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "Axis Bank ATM",
            "latitude": 28.5743561,
            "longitude": 77.3663864,
            "geoDistance": 2.0841832160949707,
            "vicinity": "Sector 50",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 109107,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Axis Bank ATM",
            "latitude": 28.5743561,
            "longitude": 77.3663864,
            "geoDistance": 2.0841832160949707,
            "vicinity": "Sector 50",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 113780,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "Axis Bank ATM",
            "latitude": 28.5743561,
            "longitude": 77.3663864,
            "geoDistance": 2.0841832160949707,
            "vicinity": "Sector 50",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 111931,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "Axis Bank ATM",
            "latitude": 28.5743561,
            "longitude": 77.3663864,
            "geoDistance": 2.0841832160949707,
            "vicinity": "Sector 50",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 111932,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "Axis Bank ATM",
            "latitude": 28.5743561,
            "longitude": 77.3663864,
            "geoDistance": 2.0841832160949707,
            "vicinity": "Sector 50",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 111933,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "Axis Bank ATM",
            "latitude": 28.5743561,
            "longitude": 77.3663864,
            "geoDistance": 2.0841832160949707,
            "vicinity": "Sector 50",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 113748,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "Axis Bank ATM",
            "latitude": 28.5743561,
            "longitude": 77.3663864,
            "geoDistance": 2.0841832160949707,
            "vicinity": "Sector 50",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 106411,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "Axis Bank ATM",
            "latitude": 28.5743561,
            "longitude": 77.3663864,
            "geoDistance": 2.0841832160949707,
            "vicinity": "Sector 50",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 116616,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "Axis Bank ATM",
            "latitude": 28.5743561,
            "longitude": 77.3663864,
            "geoDistance": 2.0841832160949707,
            "vicinity": "Sector 50",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 175591,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "Axis Bank ATM",
            "latitude": 28.5743561,
            "longitude": 77.3663864,
            "geoDistance": 2.0841832160949707,
            "vicinity": "Sector 50",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 158355,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Axis Bank ATM",
            "latitude": 28.5743561,
            "longitude": 77.3663864,
            "geoDistance": 2.0841832160949707,
            "vicinity": "Sector 50",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 164648,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Axis Bank ATM",
            "latitude": 28.5743561,
            "longitude": 77.3663864,
            "geoDistance": 2.0841832160949707,
            "vicinity": "Sector 50",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 165450,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "Axis Bank ATM",
            "latitude": 28.5743561,
            "longitude": 77.3663864,
            "geoDistance": 2.0841832160949707,
            "vicinity": "Sector 50",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 135936,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "Axis Bank ATM",
            "latitude": 28.5743561,
            "longitude": 77.3663864,
            "geoDistance": 2.0841832160949707,
            "vicinity": "Sector 50",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 125793,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "Axis Bank ATM",
            "latitude": 28.5743561,
            "longitude": 77.3663864,
            "geoDistance": 2.0841832160949707,
            "vicinity": "Sector 50",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 125824,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "Axis Bank ATM",
            "latitude": 28.5743561,
            "longitude": 77.3663864,
            "geoDistance": 2.0841832160949707,
            "vicinity": "Sector 50",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 133480,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Axis Bank ATM",
            "latitude": 28.5743561,
            "longitude": 77.3663864,
            "geoDistance": 2.0841832160949707,
            "vicinity": "Sector 50",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 134180,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Axis Bank ATM",
            "latitude": 28.5743561,
            "longitude": 77.3663864,
            "geoDistance": 2.0841832160949707,
            "vicinity": "Sector 50",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 123688,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Axis Bank ATM",
            "latitude": 28.5743561,
            "longitude": 77.3663864,
            "geoDistance": 2.0841832160949707,
            "vicinity": "Sector 50",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 124397,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "Axis Bank ATM",
            "latitude": 28.5743561,
            "longitude": 77.3663864,
            "geoDistance": 2.0841832160949707,
            "vicinity": "Sector 50",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 83834,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 16,
            "name": "Sector 50 Park",
            "latitude": 28.5713692,
            "longitude": 77.3667145,
            "geoDistance": 2.1030561923980713,
            "vicinity": "E-2, E Block, Sector 50, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":3.7}",
            "priority": 1,
            "localityAmenityTypes": {
                "id": 16,
                "name": "park",
                "description": "Park"
            },
            "rating": 5,
            "futureInfrastructure": false
        }, {
            "id": 157478,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Karate Asia - Humbo Dojo",
            "latitude": 28.5681705,
            "longitude": 77.3676682,
            "geoDistance": 2.126338481903076,
            "vicinity": "C-150, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 109084,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 1,
            "name": "Karate Asia - Humbo Dojo",
            "latitude": 28.5681705,
            "longitude": 77.3676682,
            "geoDistance": 2.126338481903076,
            "vicinity": "C-150, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 113764,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Karate Asia - Humbo Dojo",
            "latitude": 28.5681705,
            "longitude": 77.3676682,
            "geoDistance": 2.126338481903076,
            "vicinity": "C-150, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 113771,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Karate Asia - Humbo Dojo",
            "latitude": 28.5681705,
            "longitude": 77.3676682,
            "geoDistance": 2.126338481903076,
            "vicinity": "C-150, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 106401,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Karate Asia - Humbo Dojo",
            "latitude": 28.5681705,
            "longitude": 77.3676682,
            "geoDistance": 2.126338481903076,
            "vicinity": "C-150, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 116636,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Karate Asia - Humbo Dojo",
            "latitude": 28.5681705,
            "longitude": 77.3676682,
            "geoDistance": 2.126338481903076,
            "vicinity": "C-150, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 175568,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Karate Asia - Humbo Dojo",
            "latitude": 28.5681705,
            "longitude": 77.3676682,
            "geoDistance": 2.126338481903076,
            "vicinity": "C-150, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 158345,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 1,
            "name": "Karate Asia - Humbo Dojo",
            "latitude": 28.5681705,
            "longitude": 77.3676682,
            "geoDistance": 2.126338481903076,
            "vicinity": "C-150, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 164638,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 1,
            "name": "Karate Asia - Humbo Dojo",
            "latitude": 28.5681705,
            "longitude": 77.3676682,
            "geoDistance": 2.126338481903076,
            "vicinity": "C-150, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 135926,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Karate Asia - Humbo Dojo",
            "latitude": 28.5681705,
            "longitude": 77.3676682,
            "geoDistance": 2.126338481903076,
            "vicinity": "C-150, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 133502,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 1,
            "name": "Karate Asia - Humbo Dojo",
            "latitude": 28.5681705,
            "longitude": 77.3676682,
            "geoDistance": 2.126338481903076,
            "vicinity": "C-150, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 134224,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 1,
            "name": "Karate Asia - Humbo Dojo",
            "latitude": 28.5681705,
            "longitude": 77.3676682,
            "geoDistance": 2.126338481903076,
            "vicinity": "C-150, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 123708,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 1,
            "name": "Karate Asia - Humbo Dojo",
            "latitude": 28.5681705,
            "longitude": 77.3676682,
            "geoDistance": 2.126338481903076,
            "vicinity": "C-150, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 124430,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Karate Asia - Humbo Dojo",
            "latitude": 28.5681705,
            "longitude": 77.3676682,
            "geoDistance": 2.126338481903076,
            "vicinity": "C-150, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 66488,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 5,
            "name": "Viranda Garden",
            "latitude": 28.5826111,
            "longitude": 77.3671036,
            "geoDistance": 2.149634838104248,
            "vicinity": "Hoshiyarpur, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 5,
                "name": "restaurant",
                "description": "Restaurant"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 37936,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 5,
            "name": "Chawhan Hotel",
            "latitude": 28.5833054,
            "longitude": 77.3672943,
            "geoDistance": 2.1612191200256348,
            "vicinity": "Captain Shashi Kant Marg, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 5,
                "name": "restaurant",
                "description": "Restaurant"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 143744,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Parthala Primary School",
            "latitude": 28.5913105,
            "longitude": 77.4009552,
            "geoDistance": 2.1649351119995117,
            "vicinity": "Parthala Khanjarpur, Sector 122, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 143740,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "ZTPL Academy (a unit of Zusammen Technologies Pvt Ltd)",
            "latitude": 28.5932713,
            "longitude": 77.3973999,
            "geoDistance": 2.1703267097473145,
            "vicinity": "c 59, Sector 122, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 61981,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 3,
            "name": "Kotak Mahindra Bank – Branch/ATM",
            "latitude": 28.5786037,
            "longitude": 77.3654633,
            "geoDistance": 2.192253828048706,
            "vicinity": "Ground Floor & Rear Basement, B - 1/8 & B - 1/9, Sector - 51, Noida,, Uttar Pradesh 201301, New Delh",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 3,
                "name": "bank",
                "description": "Bank"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 143749,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Indian National Public School",
            "latitude": 28.5770264,
            "longitude": 77.4102783,
            "geoDistance": 2.2122654914855957,
            "vicinity": "Sorkha, Sorkha Jahidabad",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 80237,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 18,
            "name": "AaramShop C/o Store Next Door",
            "latitude": 28.5901184,
            "longitude": 77.3718796,
            "geoDistance": 2.222804307937622,
            "vicinity": "Shop No. 6,B Block, Captain Shashi Kant Marg, Sector 52, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 18,
                "name": "grocery_or_supermarket",
                "description": "Grocery or Supermarket"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 20140,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "Oriental Bank of Commerce Atm",
            "latitude": 28.5645275,
            "longitude": 77.3683319,
            "geoDistance": 2.2619926929473877,
            "vicinity": "Barola, Sector - 49, Noida, Tehsil: Dadri, Distt. Gautam Budh Nagar, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 80234,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 18,
            "name": "Store Next Door",
            "latitude": 28.5864944,
            "longitude": 77.367981,
            "geoDistance": 2.2654495239257812,
            "vicinity": "B192, Block B, Sector 52, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 18,
                "name": "grocery_or_supermarket",
                "description": "Grocery or Supermarket"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 157497,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Dr Usha Bhatnagar- Best Eye Specialist, Eye Surgeon, Eye Doctor, Eye Clinic, Lasik, Catarac Surgeon",
            "latitude": 28.5874424,
            "longitude": 77.3682022,
            "geoDistance": 2.3050625324249268,
            "vicinity": "Sector 52, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 109103,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 2,
            "name": "Dr Usha Bhatnagar- Best Eye Specialist, Eye Surgeon, Eye Doctor, Eye Clinic, Lasik, Catarac Surgeon",
            "latitude": 28.5874424,
            "longitude": 77.3682022,
            "geoDistance": 2.3050625324249268,
            "vicinity": "Sector 52, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 106395,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Dr Usha Bhatnagar- Best Eye Specialist, Eye Surgeon, Eye Doctor, Eye Clinic, Lasik, Catarac Surgeon",
            "latitude": 28.5874424,
            "longitude": 77.3682022,
            "geoDistance": 2.3050625324249268,
            "vicinity": "Sector 52, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 158380,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 2,
            "name": "Dr Usha Bhatnagar- Best Eye Specialist, Eye Surgeon, Eye Doctor, Eye Clinic, Lasik, Catarac Surgeon",
            "latitude": 28.5874424,
            "longitude": 77.3682022,
            "geoDistance": 2.3050625324249268,
            "vicinity": "Sector 52, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 164675,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 2,
            "name": "Dr Usha Bhatnagar- Best Eye Specialist, Eye Surgeon, Eye Doctor, Eye Clinic, Lasik, Catarac Surgeon",
            "latitude": 28.5874424,
            "longitude": 77.3682022,
            "geoDistance": 2.3050625324249268,
            "vicinity": "Sector 52, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 165460,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Dr Usha Bhatnagar- Best Eye Specialist, Eye Surgeon, Eye Doctor, Eye Clinic, Lasik, Catarac Surgeon",
            "latitude": 28.5874424,
            "longitude": 77.3682022,
            "geoDistance": 2.3050625324249268,
            "vicinity": "Sector 52, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 135963,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Dr Usha Bhatnagar- Best Eye Specialist, Eye Surgeon, Eye Doctor, Eye Clinic, Lasik, Catarac Surgeon",
            "latitude": 28.5874424,
            "longitude": 77.3682022,
            "geoDistance": 2.3050625324249268,
            "vicinity": "Sector 52, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 125803,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Dr Usha Bhatnagar- Best Eye Specialist, Eye Surgeon, Eye Doctor, Eye Clinic, Lasik, Catarac Surgeon",
            "latitude": 28.5874424,
            "longitude": 77.3682022,
            "geoDistance": 2.3050625324249268,
            "vicinity": "Sector 52, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 125834,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Dr Usha Bhatnagar- Best Eye Specialist, Eye Surgeon, Eye Doctor, Eye Clinic, Lasik, Catarac Surgeon",
            "latitude": 28.5874424,
            "longitude": 77.3682022,
            "geoDistance": 2.3050625324249268,
            "vicinity": "Sector 52, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 133534,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 2,
            "name": "Dr Usha Bhatnagar- Best Eye Specialist, Eye Surgeon, Eye Doctor, Eye Clinic, Lasik, Catarac Surgeon",
            "latitude": 28.5874424,
            "longitude": 77.3682022,
            "geoDistance": 2.3050625324249268,
            "vicinity": "Sector 52, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 134205,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 2,
            "name": "Dr Usha Bhatnagar- Best Eye Specialist, Eye Surgeon, Eye Doctor, Eye Clinic, Lasik, Catarac Surgeon",
            "latitude": 28.5874424,
            "longitude": 77.3682022,
            "geoDistance": 2.3050625324249268,
            "vicinity": "Sector 52, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 123727,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 2,
            "name": "Dr Usha Bhatnagar- Best Eye Specialist, Eye Surgeon, Eye Doctor, Eye Clinic, Lasik, Catarac Surgeon",
            "latitude": 28.5874424,
            "longitude": 77.3682022,
            "geoDistance": 2.3050625324249268,
            "vicinity": "Sector 52, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 124449,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Dr Usha Bhatnagar- Best Eye Specialist, Eye Surgeon, Eye Doctor, Eye Clinic, Lasik, Catarac Surgeon",
            "latitude": 28.5874424,
            "longitude": 77.3682022,
            "geoDistance": 2.3050625324249268,
            "vicinity": "Sector 52, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 138241,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Dr Usha Bhatnagar- Best Eye Specialist, Eye Surgeon, Eye Doctor, Eye Clinic, Lasik, Catarac Surgeon",
            "latitude": 28.5874424,
            "longitude": 77.3682022,
            "geoDistance": 2.3050625324249268,
            "vicinity": "Sector 52, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 138242,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Dr Usha Bhatnagar- Best Eye Specialist, Eye Surgeon, Eye Doctor, Eye Clinic, Lasik, Catarac Surgeon",
            "latitude": 28.5874424,
            "longitude": 77.3682022,
            "geoDistance": 2.3050625324249268,
            "vicinity": "Sector 52, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 138243,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Dr Usha Bhatnagar- Best Eye Specialist, Eye Surgeon, Eye Doctor, Eye Clinic, Lasik, Catarac Surgeon",
            "latitude": 28.5874424,
            "longitude": 77.3682022,
            "geoDistance": 2.3050625324249268,
            "vicinity": "Sector 52, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 138244,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Dr Usha Bhatnagar- Best Eye Specialist, Eye Surgeon, Eye Doctor, Eye Clinic, Lasik, Catarac Surgeon",
            "latitude": 28.5874424,
            "longitude": 77.3682022,
            "geoDistance": 2.3050625324249268,
            "vicinity": "Sector 52, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 138245,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Dr Usha Bhatnagar- Best Eye Specialist, Eye Surgeon, Eye Doctor, Eye Clinic, Lasik, Catarac Surgeon",
            "latitude": 28.5874424,
            "longitude": 77.3682022,
            "geoDistance": 2.3050625324249268,
            "vicinity": "Sector 52, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 138246,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Dr Usha Bhatnagar- Best Eye Specialist, Eye Surgeon, Eye Doctor, Eye Clinic, Lasik, Catarac Surgeon",
            "latitude": 28.5874424,
            "longitude": 77.3682022,
            "geoDistance": 2.3050625324249268,
            "vicinity": "Sector 52, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 138247,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Dr Usha Bhatnagar- Best Eye Specialist, Eye Surgeon, Eye Doctor, Eye Clinic, Lasik, Catarac Surgeon",
            "latitude": 28.5874424,
            "longitude": 77.3682022,
            "geoDistance": 2.3050625324249268,
            "vicinity": "Sector 52, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 138248,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Dr Usha Bhatnagar- Best Eye Specialist, Eye Surgeon, Eye Doctor, Eye Clinic, Lasik, Catarac Surgeon",
            "latitude": 28.5874424,
            "longitude": 77.3682022,
            "geoDistance": 2.3050625324249268,
            "vicinity": "Sector 52, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 138249,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Dr Usha Bhatnagar- Best Eye Specialist, Eye Surgeon, Eye Doctor, Eye Clinic, Lasik, Catarac Surgeon",
            "latitude": 28.5874424,
            "longitude": 77.3682022,
            "geoDistance": 2.3050625324249268,
            "vicinity": "Sector 52, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 147980,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Kothari International School",
            "latitude": 28.5675182,
            "longitude": 77.3658218,
            "geoDistance": 2.3206489086151123,
            "vicinity": "B-279, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 157475,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Kothari International School",
            "latitude": 28.5675182,
            "longitude": 77.3658218,
            "geoDistance": 2.3206489086151123,
            "vicinity": "B-279, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 109081,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 1,
            "name": "Kothari International School",
            "latitude": 28.5675182,
            "longitude": 77.3658218,
            "geoDistance": 2.3206489086151123,
            "vicinity": "B-279, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 111912,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Kothari International School",
            "latitude": 28.5675182,
            "longitude": 77.3658218,
            "geoDistance": 2.3206489086151123,
            "vicinity": "B-279, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 113762,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Kothari International School",
            "latitude": 28.5675182,
            "longitude": 77.3658218,
            "geoDistance": 2.3206489086151123,
            "vicinity": "B-279, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 113769,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Kothari International School",
            "latitude": 28.5675182,
            "longitude": 77.3658218,
            "geoDistance": 2.3206489086151123,
            "vicinity": "B-279, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 106398,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Kothari International School",
            "latitude": 28.5675182,
            "longitude": 77.3658218,
            "geoDistance": 2.3206489086151123,
            "vicinity": "B-279, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 116633,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Kothari International School",
            "latitude": 28.5675182,
            "longitude": 77.3658218,
            "geoDistance": 2.3206489086151123,
            "vicinity": "B-279, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 175566,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Kothari International School",
            "latitude": 28.5675182,
            "longitude": 77.3658218,
            "geoDistance": 2.3206489086151123,
            "vicinity": "B-279, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 158342,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 1,
            "name": "Kothari International School",
            "latitude": 28.5675182,
            "longitude": 77.3658218,
            "geoDistance": 2.3206489086151123,
            "vicinity": "B-279, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 164635,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 1,
            "name": "Kothari International School",
            "latitude": 28.5675182,
            "longitude": 77.3658218,
            "geoDistance": 2.3206489086151123,
            "vicinity": "B-279, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 135923,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Kothari International School",
            "latitude": 28.5675182,
            "longitude": 77.3658218,
            "geoDistance": 2.3206489086151123,
            "vicinity": "B-279, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 125784,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Kothari International School",
            "latitude": 28.5675182,
            "longitude": 77.3658218,
            "geoDistance": 2.3206489086151123,
            "vicinity": "B-279, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 125815,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Kothari International School",
            "latitude": 28.5675182,
            "longitude": 77.3658218,
            "geoDistance": 2.3206489086151123,
            "vicinity": "B-279, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 133499,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 1,
            "name": "Kothari International School",
            "latitude": 28.5675182,
            "longitude": 77.3658218,
            "geoDistance": 2.3206489086151123,
            "vicinity": "B-279, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 134221,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 1,
            "name": "Kothari International School",
            "latitude": 28.5675182,
            "longitude": 77.3658218,
            "geoDistance": 2.3206489086151123,
            "vicinity": "B-279, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 123705,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 1,
            "name": "Kothari International School",
            "latitude": 28.5675182,
            "longitude": 77.3658218,
            "geoDistance": 2.3206489086151123,
            "vicinity": "B-279, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 124427,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Kothari International School",
            "latitude": 28.5675182,
            "longitude": 77.3658218,
            "geoDistance": 2.3206489086151123,
            "vicinity": "B-279, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 5056,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Heritage Public School",
            "latitude": 28.5604286,
            "longitude": 77.3714752,
            "geoDistance": 2.3223986625671387,
            "vicinity": "Baraula, Sector 49, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 83829,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 16,
            "name": "Noida sector 52 PG park",
            "latitude": 28.5875282,
            "longitude": 77.3678818,
            "geoDistance": 2.3362929821014404,
            "vicinity": "B-191, Sector 52, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 1,
            "localityAmenityTypes": {
                "id": 16,
                "name": "park",
                "description": "Park"
            },
            "rating": 5,
            "futureInfrastructure": false
        }, {
            "id": 177831,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Wellbeing Counselling & Psycometric Assessment Centre",
            "latitude": 28.5645657,
            "longitude": 77.3673935,
            "geoDistance": 2.336864471435547,
            "vicinity": "218, D Block, Sector 49, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 1341,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Adarsh Public School",
            "latitude": 28.5869389,
            "longitude": 77.3673477,
            "geoDistance": 2.3440260887145996,
            "vicinity": "B -193, Sector 52, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 3,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 3,
            "futureInfrastructure": false
        }, {
            "id": 100096,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Sapphire International School",
            "latitude": 28.5959454,
            "longitude": 77.3807831,
            "geoDistance": 2.347262382507324,
            "vicinity": "SS-1, Sector-70, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 100097,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Sapphire International School",
            "latitude": 28.5959454,
            "longitude": 77.3807831,
            "geoDistance": 2.347262382507324,
            "vicinity": "SS-1, Sector-70, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 100252,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Sapphire International School",
            "latitude": 28.5959454,
            "longitude": 77.3807831,
            "geoDistance": 2.347262382507324,
            "vicinity": "SS-1, Sector-70, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 100253,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Sapphire International School",
            "latitude": 28.5959454,
            "longitude": 77.3807831,
            "geoDistance": 2.347262382507324,
            "vicinity": "SS-1, Sector-70, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 63733,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Sapphire International School",
            "latitude": 28.5960331,
            "longitude": 77.3809586,
            "geoDistance": 2.351710796356201,
            "website": "http://www.sapphireschool.in/sis/index.html",
            "vicinity": "SS-1, Sector-70, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":4.3}",
            "priority": 3,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 3,
            "futureInfrastructure": false
        }, {
            "id": 83836,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 16,
            "name": "Durga Puja Maidan",
            "latitude": 28.5729408,
            "longitude": 77.3637543,
            "geoDistance": 2.355994462966919,
            "vicinity": "A-57, A Block, Block A, Sector 50, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 1,
            "localityAmenityTypes": {
                "id": 16,
                "name": "park",
                "description": "Park"
            },
            "rating": 5,
            "futureInfrastructure": false
        }, {
            "id": 73167,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 8,
            "name": "Sector 34",
            "latitude": 28.5809574,
            "longitude": 77.3642349,
            "geoDistance": 2.3615219593048096,
            "vicinity": "India",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 109063,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 8,
            "name": "Sector 34",
            "latitude": 28.5809574,
            "longitude": 77.3642349,
            "geoDistance": 2.3615219593048096,
            "vicinity": "India",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 106381,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 8,
            "name": "Sector 34",
            "latitude": 28.5809574,
            "longitude": 77.3642349,
            "geoDistance": 2.3615219593048096,
            "vicinity": "India",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 116608,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 8,
            "name": "Sector 34",
            "latitude": 28.5809574,
            "longitude": 77.3642349,
            "geoDistance": 2.3615219593048096,
            "vicinity": "India",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 158390,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 8,
            "name": "Sector 34",
            "latitude": 28.5809574,
            "longitude": 77.3642349,
            "geoDistance": 2.3615219593048096,
            "vicinity": "India",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 164629,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 8,
            "name": "Sector 34",
            "latitude": 28.5809574,
            "longitude": 77.3642349,
            "geoDistance": 2.3615219593048096,
            "vicinity": "India",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 135917,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 8,
            "name": "Sector 34",
            "latitude": 28.5809574,
            "longitude": 77.3642349,
            "geoDistance": 2.3615219593048096,
            "vicinity": "India",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 133518,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 8,
            "name": "Sector 34",
            "latitude": 28.5809574,
            "longitude": 77.3642349,
            "geoDistance": 2.3615219593048096,
            "vicinity": "India",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 134215,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 8,
            "name": "Sector 34",
            "latitude": 28.5809574,
            "longitude": 77.3642349,
            "geoDistance": 2.3615219593048096,
            "vicinity": "India",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 123669,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 8,
            "name": "Sector 34",
            "latitude": 28.5809574,
            "longitude": 77.3642349,
            "geoDistance": 2.3615219593048096,
            "vicinity": "India",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 124421,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 8,
            "name": "Sector 34",
            "latitude": 28.5809574,
            "longitude": 77.3642349,
            "geoDistance": 2.3615219593048096,
            "vicinity": "India",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 83805,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 16,
            "name": "Yamaha Vihar park",
            "latitude": 28.5577793,
            "longitude": 77.3742981,
            "geoDistance": 2.3846206665039062,
            "vicinity": "Yamaha Vihar, Natthu Colony, Baraula, Sector 49, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 1,
            "localityAmenityTypes": {
                "id": 16,
                "name": "park",
                "description": "Park"
            },
            "rating": 5,
            "futureInfrastructure": false
        }, {
            "id": 36988,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Kothari International School",
            "latitude": 28.5670338,
            "longitude": 77.3652725,
            "geoDistance": 2.3912360668182373,
            "vicinity": "B-279, Sector 50, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 2,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 4,
            "futureInfrastructure": false
        }, {
            "id": 177830,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Om Hospital",
            "latitude": 28.5601997,
            "longitude": 77.3704834,
            "geoDistance": 2.4076766967773438,
            "vicinity": "Main Dadri Road, Opposite Radha Swami Satsang Vyas, Sector 49, Main Dadri Road, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 157494,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Om Hospital",
            "latitude": 28.5601997,
            "longitude": 77.3704834,
            "geoDistance": 2.4076766967773438,
            "vicinity": "Main Dadri Road, Opposite Radha Swami Satsang Vyas, Sector 49, Main Dadri Road, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 157495,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Om Hospital",
            "latitude": 28.5601997,
            "longitude": 77.3704834,
            "geoDistance": 2.4076766967773438,
            "vicinity": "Main Dadri Road, Opposite Radha Swami Satsang Vyas, Sector 49, Main Dadri Road, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 157496,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Om Hospital",
            "latitude": 28.5601997,
            "longitude": 77.3704834,
            "geoDistance": 2.4076766967773438,
            "vicinity": "Main Dadri Road, Opposite Radha Swami Satsang Vyas, Sector 49, Main Dadri Road, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 109100,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 2,
            "name": "Om Hospital",
            "latitude": 28.5601997,
            "longitude": 77.3704834,
            "geoDistance": 2.4076766967773438,
            "vicinity": "Main Dadri Road, Opposite Radha Swami Satsang Vyas, Sector 49, Main Dadri Road, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 109101,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 2,
            "name": "Om Hospital",
            "latitude": 28.5601997,
            "longitude": 77.3704834,
            "geoDistance": 2.4076766967773438,
            "vicinity": "Main Dadri Road, Opposite Radha Swami Satsang Vyas, Sector 49, Main Dadri Road, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 109102,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 2,
            "name": "Om Hospital",
            "latitude": 28.5601997,
            "longitude": 77.3704834,
            "geoDistance": 2.4076766967773438,
            "vicinity": "Main Dadri Road, Opposite Radha Swami Satsang Vyas, Sector 49, Main Dadri Road, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 106392,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Om Hospital",
            "latitude": 28.5601997,
            "longitude": 77.3704834,
            "geoDistance": 2.4076766967773438,
            "vicinity": "Main Dadri Road, Opposite Radha Swami Satsang Vyas, Sector 49, Main Dadri Road, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 106393,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Om Hospital",
            "latitude": 28.5601997,
            "longitude": 77.3704834,
            "geoDistance": 2.4076766967773438,
            "vicinity": "Main Dadri Road, Opposite Radha Swami Satsang Vyas, Sector 49, Main Dadri Road, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 106394,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Om Hospital",
            "latitude": 28.5601997,
            "longitude": 77.3704834,
            "geoDistance": 2.4076766967773438,
            "vicinity": "Main Dadri Road, Opposite Radha Swami Satsang Vyas, Sector 49, Main Dadri Road, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 116653,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Om Hospital",
            "latitude": 28.5601997,
            "longitude": 77.3704834,
            "geoDistance": 2.4076766967773438,
            "vicinity": "Main Dadri Road, Opposite Radha Swami Satsang Vyas, Sector 49, Main Dadri Road, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 116654,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Om Hospital",
            "latitude": 28.5601997,
            "longitude": 77.3704834,
            "geoDistance": 2.4076766967773438,
            "vicinity": "Main Dadri Road, Opposite Radha Swami Satsang Vyas, Sector 49, Main Dadri Road, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 158377,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 2,
            "name": "Om Hospital",
            "latitude": 28.5601997,
            "longitude": 77.3704834,
            "geoDistance": 2.4076766967773438,
            "vicinity": "Main Dadri Road, Opposite Radha Swami Satsang Vyas, Sector 49, Main Dadri Road, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 158378,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 2,
            "name": "Om Hospital",
            "latitude": 28.5601997,
            "longitude": 77.3704834,
            "geoDistance": 2.4076766967773438,
            "vicinity": "Main Dadri Road, Opposite Radha Swami Satsang Vyas, Sector 49, Main Dadri Road, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 158379,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 2,
            "name": "Om Hospital",
            "latitude": 28.5601997,
            "longitude": 77.3704834,
            "geoDistance": 2.4076766967773438,
            "vicinity": "Main Dadri Road, Opposite Radha Swami Satsang Vyas, Sector 49, Main Dadri Road, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 164672,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 2,
            "name": "Om Hospital",
            "latitude": 28.5601997,
            "longitude": 77.3704834,
            "geoDistance": 2.4076766967773438,
            "vicinity": "Main Dadri Road, Opposite Radha Swami Satsang Vyas, Sector 49, Main Dadri Road, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 164673,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 2,
            "name": "Om Hospital",
            "latitude": 28.5601997,
            "longitude": 77.3704834,
            "geoDistance": 2.4076766967773438,
            "vicinity": "Main Dadri Road, Opposite Radha Swami Satsang Vyas, Sector 49, Main Dadri Road, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 164674,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 2,
            "name": "Om Hospital",
            "latitude": 28.5601997,
            "longitude": 77.3704834,
            "geoDistance": 2.4076766967773438,
            "vicinity": "Main Dadri Road, Opposite Radha Swami Satsang Vyas, Sector 49, Main Dadri Road, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 135960,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Om Hospital",
            "latitude": 28.5601997,
            "longitude": 77.3704834,
            "geoDistance": 2.4076766967773438,
            "vicinity": "Main Dadri Road, Opposite Radha Swami Satsang Vyas, Sector 49, Main Dadri Road, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 135961,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Om Hospital",
            "latitude": 28.5601997,
            "longitude": 77.3704834,
            "geoDistance": 2.4076766967773438,
            "vicinity": "Main Dadri Road, Opposite Radha Swami Satsang Vyas, Sector 49, Main Dadri Road, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 135962,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Om Hospital",
            "latitude": 28.5601997,
            "longitude": 77.3704834,
            "geoDistance": 2.4076766967773438,
            "vicinity": "Main Dadri Road, Opposite Radha Swami Satsang Vyas, Sector 49, Main Dadri Road, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 133531,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 2,
            "name": "Om Hospital",
            "latitude": 28.5601997,
            "longitude": 77.3704834,
            "geoDistance": 2.4076766967773438,
            "vicinity": "Main Dadri Road, Opposite Radha Swami Satsang Vyas, Sector 49, Main Dadri Road, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 133532,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 2,
            "name": "Om Hospital",
            "latitude": 28.5601997,
            "longitude": 77.3704834,
            "geoDistance": 2.4076766967773438,
            "vicinity": "Main Dadri Road, Opposite Radha Swami Satsang Vyas, Sector 49, Main Dadri Road, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 133533,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 2,
            "name": "Om Hospital",
            "latitude": 28.5601997,
            "longitude": 77.3704834,
            "geoDistance": 2.4076766967773438,
            "vicinity": "Main Dadri Road, Opposite Radha Swami Satsang Vyas, Sector 49, Main Dadri Road, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 134202,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 2,
            "name": "Om Hospital",
            "latitude": 28.5601997,
            "longitude": 77.3704834,
            "geoDistance": 2.4076766967773438,
            "vicinity": "Main Dadri Road, Opposite Radha Swami Satsang Vyas, Sector 49, Main Dadri Road, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 134203,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 2,
            "name": "Om Hospital",
            "latitude": 28.5601997,
            "longitude": 77.3704834,
            "geoDistance": 2.4076766967773438,
            "vicinity": "Main Dadri Road, Opposite Radha Swami Satsang Vyas, Sector 49, Main Dadri Road, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 134204,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 2,
            "name": "Om Hospital",
            "latitude": 28.5601997,
            "longitude": 77.3704834,
            "geoDistance": 2.4076766967773438,
            "vicinity": "Main Dadri Road, Opposite Radha Swami Satsang Vyas, Sector 49, Main Dadri Road, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 123724,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 2,
            "name": "Om Hospital",
            "latitude": 28.5601997,
            "longitude": 77.3704834,
            "geoDistance": 2.4076766967773438,
            "vicinity": "Main Dadri Road, Opposite Radha Swami Satsang Vyas, Sector 49, Main Dadri Road, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 123725,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 2,
            "name": "Om Hospital",
            "latitude": 28.5601997,
            "longitude": 77.3704834,
            "geoDistance": 2.4076766967773438,
            "vicinity": "Main Dadri Road, Opposite Radha Swami Satsang Vyas, Sector 49, Main Dadri Road, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 123726,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 2,
            "name": "Om Hospital",
            "latitude": 28.5601997,
            "longitude": 77.3704834,
            "geoDistance": 2.4076766967773438,
            "vicinity": "Main Dadri Road, Opposite Radha Swami Satsang Vyas, Sector 49, Main Dadri Road, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 124446,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Om Hospital",
            "latitude": 28.5601997,
            "longitude": 77.3704834,
            "geoDistance": 2.4076766967773438,
            "vicinity": "Main Dadri Road, Opposite Radha Swami Satsang Vyas, Sector 49, Main Dadri Road, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 124447,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Om Hospital",
            "latitude": 28.5601997,
            "longitude": 77.3704834,
            "geoDistance": 2.4076766967773438,
            "vicinity": "Main Dadri Road, Opposite Radha Swami Satsang Vyas, Sector 49, Main Dadri Road, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 124448,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Om Hospital",
            "latitude": 28.5601997,
            "longitude": 77.3704834,
            "geoDistance": 2.4076766967773438,
            "vicinity": "Main Dadri Road, Opposite Radha Swami Satsang Vyas, Sector 49, Main Dadri Road, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 12133,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 8,
            "name": "Sector 34",
            "latitude": 28.5799179,
            "longitude": 77.3634415,
            "geoDistance": 2.4116129875183105,
            "vicinity": "India",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 177833,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "Oriental Bank Of Commerce ATM",
            "latitude": 28.5598087,
            "longitude": 77.3704987,
            "geoDistance": 2.4380013942718506,
            "vicinity": "Dadri Main Rd, Natthu Colony, Barola, Sector 49, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 157499,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "Oriental Bank Of Commerce ATM",
            "latitude": 28.5598087,
            "longitude": 77.3704987,
            "geoDistance": 2.4380013942718506,
            "vicinity": "Dadri Main Rd, Natthu Colony, Barola, Sector 49, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 109105,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Oriental Bank Of Commerce ATM",
            "latitude": 28.5598087,
            "longitude": 77.3704987,
            "geoDistance": 2.4380013942718506,
            "vicinity": "Dadri Main Rd, Natthu Colony, Barola, Sector 49, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 113778,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "Oriental Bank Of Commerce ATM",
            "latitude": 28.5598087,
            "longitude": 77.3704987,
            "geoDistance": 2.4380013942718506,
            "vicinity": "Dadri Main Rd, Natthu Colony, Barola, Sector 49, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 111929,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "Oriental Bank Of Commerce ATM",
            "latitude": 28.5598087,
            "longitude": 77.3704987,
            "geoDistance": 2.4380013942718506,
            "vicinity": "Dadri Main Rd, Natthu Colony, Barola, Sector 49, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 113746,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "Oriental Bank Of Commerce ATM",
            "latitude": 28.5598087,
            "longitude": 77.3704987,
            "geoDistance": 2.4380013942718506,
            "vicinity": "Dadri Main Rd, Natthu Colony, Barola, Sector 49, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 106409,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "Oriental Bank Of Commerce ATM",
            "latitude": 28.5598087,
            "longitude": 77.3704987,
            "geoDistance": 2.4380013942718506,
            "vicinity": "Dadri Main Rd, Natthu Colony, Barola, Sector 49, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 116613,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "Oriental Bank Of Commerce ATM",
            "latitude": 28.5598087,
            "longitude": 77.3704987,
            "geoDistance": 2.4380013942718506,
            "vicinity": "Dadri Main Rd, Natthu Colony, Barola, Sector 49, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 175589,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "Oriental Bank Of Commerce ATM",
            "latitude": 28.5598087,
            "longitude": 77.3704987,
            "geoDistance": 2.4380013942718506,
            "vicinity": "Dadri Main Rd, Natthu Colony, Barola, Sector 49, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 158353,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Oriental Bank Of Commerce ATM",
            "latitude": 28.5598087,
            "longitude": 77.3704987,
            "geoDistance": 2.4380013942718506,
            "vicinity": "Dadri Main Rd, Natthu Colony, Barola, Sector 49, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 164646,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Oriental Bank Of Commerce ATM",
            "latitude": 28.5598087,
            "longitude": 77.3704987,
            "geoDistance": 2.4380013942718506,
            "vicinity": "Dadri Main Rd, Natthu Colony, Barola, Sector 49, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 135934,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "Oriental Bank Of Commerce ATM",
            "latitude": 28.5598087,
            "longitude": 77.3704987,
            "geoDistance": 2.4380013942718506,
            "vicinity": "Dadri Main Rd, Natthu Colony, Barola, Sector 49, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 133478,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Oriental Bank Of Commerce ATM",
            "latitude": 28.5598087,
            "longitude": 77.3704987,
            "geoDistance": 2.4380013942718506,
            "vicinity": "Dadri Main Rd, Natthu Colony, Barola, Sector 49, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 134178,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Oriental Bank Of Commerce ATM",
            "latitude": 28.5598087,
            "longitude": 77.3704987,
            "geoDistance": 2.4380013942718506,
            "vicinity": "Dadri Main Rd, Natthu Colony, Barola, Sector 49, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 123686,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Oriental Bank Of Commerce ATM",
            "latitude": 28.5598087,
            "longitude": 77.3704987,
            "geoDistance": 2.4380013942718506,
            "vicinity": "Dadri Main Rd, Natthu Colony, Barola, Sector 49, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 124395,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "Oriental Bank Of Commerce ATM",
            "latitude": 28.5598087,
            "longitude": 77.3704987,
            "geoDistance": 2.4380013942718506,
            "vicinity": "Dadri Main Rd, Natthu Colony, Barola, Sector 49, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 177832,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Naveen Dental Clinic & Maternity Centre",
            "latitude": 28.5598946,
            "longitude": 77.370369,
            "geoDistance": 2.439864158630371,
            "vicinity": "Main Market, Near Bus Stand, Dadri Main Rd, Natthu Colony, Barola, Sector 49, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 48426,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 6,
            "name": "Indian Oil Petrol pump",
            "latitude": 28.5932369,
            "longitude": 77.3726044,
            "geoDistance": 2.440819263458252,
            "vicinity": "E-block, Sector 52, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 6,
                "name": "gas_station",
                "description": "Petrol Pump"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 177827,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 8,
            "name": "Barola Market",
            "latitude": 28.5599308,
            "longitude": 77.3702164,
            "geoDistance": 2.4473376274108887,
            "vicinity": "India",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 109055,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 8,
            "name": "Barola Market",
            "latitude": 28.5599308,
            "longitude": 77.3702164,
            "geoDistance": 2.4473376274108887,
            "vicinity": "India",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 113775,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 8,
            "name": "Barola Market",
            "latitude": 28.5599308,
            "longitude": 77.3702164,
            "geoDistance": 2.4473376274108887,
            "vicinity": "India",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 111919,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 8,
            "name": "Barola Market",
            "latitude": 28.5599308,
            "longitude": 77.3702164,
            "geoDistance": 2.4473376274108887,
            "vicinity": "India",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 113758,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 8,
            "name": "Barola Market",
            "latitude": 28.5599308,
            "longitude": 77.3702164,
            "geoDistance": 2.4473376274108887,
            "vicinity": "India",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 106373,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 8,
            "name": "Barola Market",
            "latitude": 28.5599308,
            "longitude": 77.3702164,
            "geoDistance": 2.4473376274108887,
            "vicinity": "India",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 116600,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 8,
            "name": "Barola Market",
            "latitude": 28.5599308,
            "longitude": 77.3702164,
            "geoDistance": 2.4473376274108887,
            "vicinity": "India",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 175581,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 8,
            "name": "Barola Market",
            "latitude": 28.5599308,
            "longitude": 77.3702164,
            "geoDistance": 2.4473376274108887,
            "vicinity": "India",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 158382,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 8,
            "name": "Barola Market",
            "latitude": 28.5599308,
            "longitude": 77.3702164,
            "geoDistance": 2.4473376274108887,
            "vicinity": "India",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 164621,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 8,
            "name": "Barola Market",
            "latitude": 28.5599308,
            "longitude": 77.3702164,
            "geoDistance": 2.4473376274108887,
            "vicinity": "India",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 67315,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 8,
            "name": "Barola Market",
            "latitude": 28.5599308,
            "longitude": 77.3702164,
            "geoDistance": 2.4473376274108887,
            "vicinity": "India",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 135909,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 8,
            "name": "Barola Market",
            "latitude": 28.5599308,
            "longitude": 77.3702164,
            "geoDistance": 2.4473376274108887,
            "vicinity": "India",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 133510,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 8,
            "name": "Barola Market",
            "latitude": 28.5599308,
            "longitude": 77.3702164,
            "geoDistance": 2.4473376274108887,
            "vicinity": "India",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 134207,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 8,
            "name": "Barola Market",
            "latitude": 28.5599308,
            "longitude": 77.3702164,
            "geoDistance": 2.4473376274108887,
            "vicinity": "India",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 123661,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 8,
            "name": "Barola Market",
            "latitude": 28.5599308,
            "longitude": 77.3702164,
            "geoDistance": 2.4473376274108887,
            "vicinity": "India",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 124413,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 8,
            "name": "Barola Market",
            "latitude": 28.5599308,
            "longitude": 77.3702164,
            "geoDistance": 2.4473376274108887,
            "vicinity": "India",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 147993,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Dhama Clinic And Medical Centre",
            "latitude": 28.5809326,
            "longitude": 77.3633194,
            "geoDistance": 2.4475040435791016,
            "vicinity": "Sector 34, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 148008,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Dhama Clinic And Medical Centre",
            "latitude": 28.5809326,
            "longitude": 77.3633194,
            "geoDistance": 2.4475040435791016,
            "vicinity": "Sector 34, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 157489,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Dhama Clinic And Medical Centre",
            "latitude": 28.5809326,
            "longitude": 77.3633194,
            "geoDistance": 2.4475040435791016,
            "vicinity": "Sector 34, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 109095,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 2,
            "name": "Dhama Clinic And Medical Centre",
            "latitude": 28.5809326,
            "longitude": 77.3633194,
            "geoDistance": 2.4475040435791016,
            "vicinity": "Sector 34, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 113785,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Dhama Clinic And Medical Centre",
            "latitude": 28.5809326,
            "longitude": 77.3633194,
            "geoDistance": 2.4475040435791016,
            "vicinity": "Sector 34, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 111924,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Dhama Clinic And Medical Centre",
            "latitude": 28.5809326,
            "longitude": 77.3633194,
            "geoDistance": 2.4475040435791016,
            "vicinity": "Sector 34, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 113753,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Dhama Clinic And Medical Centre",
            "latitude": 28.5809326,
            "longitude": 77.3633194,
            "geoDistance": 2.4475040435791016,
            "vicinity": "Sector 34, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 106387,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Dhama Clinic And Medical Centre",
            "latitude": 28.5809326,
            "longitude": 77.3633194,
            "geoDistance": 2.4475040435791016,
            "vicinity": "Sector 34, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 116646,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Dhama Clinic And Medical Centre",
            "latitude": 28.5809326,
            "longitude": 77.3633194,
            "geoDistance": 2.4475040435791016,
            "vicinity": "Sector 34, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 175575,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Dhama Clinic And Medical Centre",
            "latitude": 28.5809326,
            "longitude": 77.3633194,
            "geoDistance": 2.4475040435791016,
            "vicinity": "Sector 34, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 158372,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 2,
            "name": "Dhama Clinic And Medical Centre",
            "latitude": 28.5809326,
            "longitude": 77.3633194,
            "geoDistance": 2.4475040435791016,
            "vicinity": "Sector 34, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 164667,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 2,
            "name": "Dhama Clinic And Medical Centre",
            "latitude": 28.5809326,
            "longitude": 77.3633194,
            "geoDistance": 2.4475040435791016,
            "vicinity": "Sector 34, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 165459,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Dhama Clinic And Medical Centre",
            "latitude": 28.5809326,
            "longitude": 77.3633194,
            "geoDistance": 2.4475040435791016,
            "vicinity": "Sector 34, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 135955,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Dhama Clinic And Medical Centre",
            "latitude": 28.5809326,
            "longitude": 77.3633194,
            "geoDistance": 2.4475040435791016,
            "vicinity": "Sector 34, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 125801,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Dhama Clinic And Medical Centre",
            "latitude": 28.5809326,
            "longitude": 77.3633194,
            "geoDistance": 2.4475040435791016,
            "vicinity": "Sector 34, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 125832,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Dhama Clinic And Medical Centre",
            "latitude": 28.5809326,
            "longitude": 77.3633194,
            "geoDistance": 2.4475040435791016,
            "vicinity": "Sector 34, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 133526,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 2,
            "name": "Dhama Clinic And Medical Centre",
            "latitude": 28.5809326,
            "longitude": 77.3633194,
            "geoDistance": 2.4475040435791016,
            "vicinity": "Sector 34, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 134197,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 2,
            "name": "Dhama Clinic And Medical Centre",
            "latitude": 28.5809326,
            "longitude": 77.3633194,
            "geoDistance": 2.4475040435791016,
            "vicinity": "Sector 34, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 123719,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 2,
            "name": "Dhama Clinic And Medical Centre",
            "latitude": 28.5809326,
            "longitude": 77.3633194,
            "geoDistance": 2.4475040435791016,
            "vicinity": "Sector 34, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 124441,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Dhama Clinic And Medical Centre",
            "latitude": 28.5809326,
            "longitude": 77.3633194,
            "geoDistance": 2.4475040435791016,
            "vicinity": "Sector 34, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 109065,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 8,
            "name": "New Holland Fiat India Bus Stop",
            "latitude": 28.5653667,
            "longitude": 77.365387,
            "geoDistance": 2.4621119499206543,
            "vicinity": "Block C, Sector 49, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 106383,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 8,
            "name": "New Holland Fiat India Bus Stop",
            "latitude": 28.5653667,
            "longitude": 77.365387,
            "geoDistance": 2.4621119499206543,
            "vicinity": "Block C, Sector 49, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 116610,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 8,
            "name": "New Holland Fiat India Bus Stop",
            "latitude": 28.5653667,
            "longitude": 77.365387,
            "geoDistance": 2.4621119499206543,
            "vicinity": "Block C, Sector 49, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 158392,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 8,
            "name": "New Holland Fiat India Bus Stop",
            "latitude": 28.5653667,
            "longitude": 77.365387,
            "geoDistance": 2.4621119499206543,
            "vicinity": "Block C, Sector 49, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 164631,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 8,
            "name": "New Holland Fiat India Bus Stop",
            "latitude": 28.5653667,
            "longitude": 77.365387,
            "geoDistance": 2.4621119499206543,
            "vicinity": "Block C, Sector 49, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 67144,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 8,
            "name": "New Holland Fiat India Bus Stop",
            "latitude": 28.5653667,
            "longitude": 77.365387,
            "geoDistance": 2.4621119499206543,
            "vicinity": "Block C, Sector 49, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 135919,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 8,
            "name": "New Holland Fiat India Bus Stop",
            "latitude": 28.5653667,
            "longitude": 77.365387,
            "geoDistance": 2.4621119499206543,
            "vicinity": "Block C, Sector 49, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 133520,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 8,
            "name": "New Holland Fiat India Bus Stop",
            "latitude": 28.5653667,
            "longitude": 77.365387,
            "geoDistance": 2.4621119499206543,
            "vicinity": "Block C, Sector 49, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 134217,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 8,
            "name": "New Holland Fiat India Bus Stop",
            "latitude": 28.5653667,
            "longitude": 77.365387,
            "geoDistance": 2.4621119499206543,
            "vicinity": "Block C, Sector 49, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 123671,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 8,
            "name": "New Holland Fiat India Bus Stop",
            "latitude": 28.5653667,
            "longitude": 77.365387,
            "geoDistance": 2.4621119499206543,
            "vicinity": "Block C, Sector 49, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 124423,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 8,
            "name": "New Holland Fiat India Bus Stop",
            "latitude": 28.5653667,
            "longitude": 77.365387,
            "geoDistance": 2.4621119499206543,
            "vicinity": "Block C, Sector 49, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 80186,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 17,
            "name": "INDIA GLASS COMPANY",
            "latitude": 28.5597401,
            "longitude": 77.3701401,
            "geoDistance": 2.467749834060669,
            "vicinity": "Dadri Main Rd, D Block, Sector 49, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 17,
                "name": "shopping_mall",
                "description": "Shopping Mall"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 55090,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 14,
            "name": "Noddys Play School",
            "latitude": 28.5719032,
            "longitude": 77.3626938,
            "geoDistance": 2.475879669189453,
            "vicinity": "A-56/1, Kailash Dham Road, A Block, Block A, Sector 50, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 1,
            "localityAmenityTypes": {
                "id": 14,
                "name": "play_school",
                "description": "Play School"
            },
            "rating": 5,
            "futureInfrastructure": false
        }, {
            "id": 75894,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 8,
            "name": "Sector 49 Barola Market",
            "latitude": 28.5599899,
            "longitude": 77.3697052,
            "geoDistance": 2.4777166843414307,
            "vicinity": "India",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 177826,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 8,
            "name": "Sector 49 Barola Market",
            "latitude": 28.5599899,
            "longitude": 77.3697052,
            "geoDistance": 2.4777166843414307,
            "vicinity": "India",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 109054,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 8,
            "name": "Sector 49 Barola Market",
            "latitude": 28.5599899,
            "longitude": 77.3697052,
            "geoDistance": 2.4777166843414307,
            "vicinity": "India",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 113774,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 8,
            "name": "Sector 49 Barola Market",
            "latitude": 28.5599899,
            "longitude": 77.3697052,
            "geoDistance": 2.4777166843414307,
            "vicinity": "India",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 111918,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 8,
            "name": "Sector 49 Barola Market",
            "latitude": 28.5599899,
            "longitude": 77.3697052,
            "geoDistance": 2.4777166843414307,
            "vicinity": "India",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 113757,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 8,
            "name": "Sector 49 Barola Market",
            "latitude": 28.5599899,
            "longitude": 77.3697052,
            "geoDistance": 2.4777166843414307,
            "vicinity": "India",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 106372,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 8,
            "name": "Sector 49 Barola Market",
            "latitude": 28.5599899,
            "longitude": 77.3697052,
            "geoDistance": 2.4777166843414307,
            "vicinity": "India",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 116599,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 8,
            "name": "Sector 49 Barola Market",
            "latitude": 28.5599899,
            "longitude": 77.3697052,
            "geoDistance": 2.4777166843414307,
            "vicinity": "India",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 175580,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 8,
            "name": "Sector 49 Barola Market",
            "latitude": 28.5599899,
            "longitude": 77.3697052,
            "geoDistance": 2.4777166843414307,
            "vicinity": "India",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 158381,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 8,
            "name": "Sector 49 Barola Market",
            "latitude": 28.5599899,
            "longitude": 77.3697052,
            "geoDistance": 2.4777166843414307,
            "vicinity": "India",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 164620,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 8,
            "name": "Sector 49 Barola Market",
            "latitude": 28.5599899,
            "longitude": 77.3697052,
            "geoDistance": 2.4777166843414307,
            "vicinity": "India",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 135908,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 8,
            "name": "Sector 49 Barola Market",
            "latitude": 28.5599899,
            "longitude": 77.3697052,
            "geoDistance": 2.4777166843414307,
            "vicinity": "India",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 133509,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 8,
            "name": "Sector 49 Barola Market",
            "latitude": 28.5599899,
            "longitude": 77.3697052,
            "geoDistance": 2.4777166843414307,
            "vicinity": "India",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 134206,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 8,
            "name": "Sector 49 Barola Market",
            "latitude": 28.5599899,
            "longitude": 77.3697052,
            "geoDistance": 2.4777166843414307,
            "vicinity": "India",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 123660,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 8,
            "name": "Sector 49 Barola Market",
            "latitude": 28.5599899,
            "longitude": 77.3697052,
            "geoDistance": 2.4777166843414307,
            "vicinity": "India",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 124412,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 8,
            "name": "Sector 49 Barola Market",
            "latitude": 28.5599899,
            "longitude": 77.3697052,
            "geoDistance": 2.4777166843414307,
            "vicinity": "India",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 33008,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 5,
            "name": "Kebabplus",
            "latitude": 28.5832176,
            "longitude": 77.3637161,
            "geoDistance": 2.4833295345306396,
            "vicinity": "Pocket C, Sector 34, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 5,
                "name": "restaurant",
                "description": "Restaurant"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 147977,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Wonder Years Daycare",
            "latitude": 28.5766258,
            "longitude": 77.3622437,
            "geoDistance": 2.4851882457733154,
            "vicinity": "A-67, Sector 51, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 147978,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Wonder Years Daycare",
            "latitude": 28.5766258,
            "longitude": 77.3622437,
            "geoDistance": 2.4851882457733154,
            "vicinity": "A-67, Sector 51, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 147979,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Wonder Years Daycare",
            "latitude": 28.5766258,
            "longitude": 77.3622437,
            "geoDistance": 2.4851882457733154,
            "vicinity": "A-67, Sector 51, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 147998,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Wonder Years Daycare",
            "latitude": 28.5766258,
            "longitude": 77.3622437,
            "geoDistance": 2.4851882457733154,
            "vicinity": "A-67, Sector 51, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 157474,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Wonder Years Daycare",
            "latitude": 28.5766258,
            "longitude": 77.3622437,
            "geoDistance": 2.4851882457733154,
            "vicinity": "A-67, Sector 51, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 109080,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 1,
            "name": "Wonder Years Daycare",
            "latitude": 28.5766258,
            "longitude": 77.3622437,
            "geoDistance": 2.4851882457733154,
            "vicinity": "A-67, Sector 51, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 111911,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Wonder Years Daycare",
            "latitude": 28.5766258,
            "longitude": 77.3622437,
            "geoDistance": 2.4851882457733154,
            "vicinity": "A-67, Sector 51, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 113761,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Wonder Years Daycare",
            "latitude": 28.5766258,
            "longitude": 77.3622437,
            "geoDistance": 2.4851882457733154,
            "vicinity": "A-67, Sector 51, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 113768,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Wonder Years Daycare",
            "latitude": 28.5766258,
            "longitude": 77.3622437,
            "geoDistance": 2.4851882457733154,
            "vicinity": "A-67, Sector 51, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 106397,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Wonder Years Daycare",
            "latitude": 28.5766258,
            "longitude": 77.3622437,
            "geoDistance": 2.4851882457733154,
            "vicinity": "A-67, Sector 51, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 116632,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Wonder Years Daycare",
            "latitude": 28.5766258,
            "longitude": 77.3622437,
            "geoDistance": 2.4851882457733154,
            "vicinity": "A-67, Sector 51, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 175565,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Wonder Years Daycare",
            "latitude": 28.5766258,
            "longitude": 77.3622437,
            "geoDistance": 2.4851882457733154,
            "vicinity": "A-67, Sector 51, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 158341,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 1,
            "name": "Wonder Years Daycare",
            "latitude": 28.5766258,
            "longitude": 77.3622437,
            "geoDistance": 2.4851882457733154,
            "vicinity": "A-67, Sector 51, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 164634,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 1,
            "name": "Wonder Years Daycare",
            "latitude": 28.5766258,
            "longitude": 77.3622437,
            "geoDistance": 2.4851882457733154,
            "vicinity": "A-67, Sector 51, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 165453,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Wonder Years Daycare",
            "latitude": 28.5766258,
            "longitude": 77.3622437,
            "geoDistance": 2.4851882457733154,
            "vicinity": "A-67, Sector 51, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 135922,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Wonder Years Daycare",
            "latitude": 28.5766258,
            "longitude": 77.3622437,
            "geoDistance": 2.4851882457733154,
            "vicinity": "A-67, Sector 51, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 125783,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Wonder Years Daycare",
            "latitude": 28.5766258,
            "longitude": 77.3622437,
            "geoDistance": 2.4851882457733154,
            "vicinity": "A-67, Sector 51, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 125814,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Wonder Years Daycare",
            "latitude": 28.5766258,
            "longitude": 77.3622437,
            "geoDistance": 2.4851882457733154,
            "vicinity": "A-67, Sector 51, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 133498,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 1,
            "name": "Wonder Years Daycare",
            "latitude": 28.5766258,
            "longitude": 77.3622437,
            "geoDistance": 2.4851882457733154,
            "vicinity": "A-67, Sector 51, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 134220,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 1,
            "name": "Wonder Years Daycare",
            "latitude": 28.5766258,
            "longitude": 77.3622437,
            "geoDistance": 2.4851882457733154,
            "vicinity": "A-67, Sector 51, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 123704,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 1,
            "name": "Wonder Years Daycare",
            "latitude": 28.5766258,
            "longitude": 77.3622437,
            "geoDistance": 2.4851882457733154,
            "vicinity": "A-67, Sector 51, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 124426,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Wonder Years Daycare",
            "latitude": 28.5766258,
            "longitude": 77.3622437,
            "geoDistance": 2.4851882457733154,
            "vicinity": "A-67, Sector 51, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 59290,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "HDFC Bank ATM",
            "latitude": 28.5580616,
            "longitude": 77.3719177,
            "geoDistance": 2.4946231842041016,
            "vicinity": "Dadri Road, Baraula, Sector 49, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 177834,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "HDFC Bank ATM",
            "latitude": 28.5580616,
            "longitude": 77.3719177,
            "geoDistance": 2.4946231842041016,
            "vicinity": "Baraula, Sector 49, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 157502,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "HDFC Bank ATM",
            "latitude": 28.5580616,
            "longitude": 77.3719177,
            "geoDistance": 2.4946231842041016,
            "vicinity": "Baraula, Sector 49, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 109108,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "HDFC Bank ATM",
            "latitude": 28.5580616,
            "longitude": 77.3719177,
            "geoDistance": 2.4946231842041016,
            "vicinity": "Baraula, Sector 49, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 106412,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "HDFC Bank ATM",
            "latitude": 28.5580616,
            "longitude": 77.3719177,
            "geoDistance": 2.4946231842041016,
            "vicinity": "Baraula, Sector 49, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 116617,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "HDFC Bank ATM",
            "latitude": 28.5580616,
            "longitude": 77.3719177,
            "geoDistance": 2.4946231842041016,
            "vicinity": "Baraula, Sector 49, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 175592,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "HDFC Bank ATM",
            "latitude": 28.5580616,
            "longitude": 77.3719177,
            "geoDistance": 2.4946231842041016,
            "vicinity": "Baraula, Sector 49, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 158356,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "HDFC Bank ATM",
            "latitude": 28.5580616,
            "longitude": 77.3719177,
            "geoDistance": 2.4946231842041016,
            "vicinity": "Baraula, Sector 49, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 164649,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "HDFC Bank ATM",
            "latitude": 28.5580616,
            "longitude": 77.3719177,
            "geoDistance": 2.4946231842041016,
            "vicinity": "Baraula, Sector 49, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 135937,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "HDFC Bank ATM",
            "latitude": 28.5580616,
            "longitude": 77.3719177,
            "geoDistance": 2.4946231842041016,
            "vicinity": "Baraula, Sector 49, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 133481,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "HDFC Bank ATM",
            "latitude": 28.5580616,
            "longitude": 77.3719177,
            "geoDistance": 2.4946231842041016,
            "vicinity": "Baraula, Sector 49, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 134181,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "HDFC Bank ATM",
            "latitude": 28.5580616,
            "longitude": 77.3719177,
            "geoDistance": 2.4946231842041016,
            "vicinity": "Baraula, Sector 49, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 123689,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "HDFC Bank ATM",
            "latitude": 28.5580616,
            "longitude": 77.3719177,
            "geoDistance": 2.4946231842041016,
            "vicinity": "Baraula, Sector 49, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 124398,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "HDFC Bank ATM",
            "latitude": 28.5580616,
            "longitude": 77.3719177,
            "geoDistance": 2.4946231842041016,
            "vicinity": "Baraula, Sector 49, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 45830,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 3,
            "name": "HDFC Bank",
            "latitude": 28.5782566,
            "longitude": 77.3622742,
            "geoDistance": 2.496087074279785,
            "vicinity": "B-1A, 26 & 27, B-Block Market, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 3,
                "name": "bank",
                "description": "Bank"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 55194,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 3,
            "name": "Allahabad Bank",
            "latitude": 28.5784187,
            "longitude": 77.3622818,
            "geoDistance": 2.4974067211151123,
            "vicinity": "Sector 51, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 3,
                "name": "bank",
                "description": "Bank"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 69731,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 14,
            "name": "Noddys International",
            "latitude": 28.571909,
            "longitude": 77.3624573,
            "geoDistance": 2.4984893798828125,
            "vicinity": "Sector 50, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 4,
            "localityAmenityTypes": {
                "id": 14,
                "name": "play_school",
                "description": "Play School"
            },
            "rating": 2,
            "futureInfrastructure": false
        }, {
            "id": 83823,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 16,
            "name": "Sector 70 Park",
            "latitude": 28.5978317,
            "longitude": 77.3830414,
            "geoDistance": 2.4997074604034424,
            "vicinity": "Sector 70, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 1,
            "localityAmenityTypes": {
                "id": 16,
                "name": "park",
                "description": "Park"
            },
            "rating": 5,
            "futureInfrastructure": false
        }, {
            "id": 55117,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "State Bank of Patiala ATM",
            "latitude": 28.5833549,
            "longitude": 77.3635864,
            "geoDistance": 2.5003464221954346,
            "vicinity": "Sector 34, Sector 34, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 48272,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 5,
            "name": "Khidmat restaurant",
            "latitude": 28.5833073,
            "longitude": 77.363533,
            "geoDistance": 2.5034608840942383,
            "vicinity": "Sector-50, Market Noida, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 5,
                "name": "restaurant",
                "description": "Restaurant"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 39040,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 3,
            "name": "ICICI Lombard",
            "latitude": 28.5779057,
            "longitude": 77.3621445,
            "geoDistance": 2.504624605178833,
            "vicinity": "Block B, Sector 51, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 3,
                "name": "bank",
                "description": "Bank"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 10408,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 5,
            "name": "Evergreen Sweets",
            "latitude": 28.5832844,
            "longitude": 77.3634567,
            "geoDistance": 2.5096397399902344,
            "vicinity": "Sector 34, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":2.7}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 5,
                "name": "restaurant",
                "description": "Restaurant"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 206479,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Shri Ram Hospital",
            "latitude": 28.5578766,
            "longitude": 77.3718643,
            "geoDistance": 2.514068841934204,
            "vicinity": "Dadri Main Rd, Natthu Colony, Baraula, Sector 49, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 19716,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 14,
            "name": "Little Munchkins Daycare",
            "latitude": 28.5696354,
            "longitude": 77.3628235,
            "geoDistance": 2.51912784576416,
            "vicinity": "Opposite Dominoes, Sector 50 Market, B-260, Sector 50, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 2,
            "localityAmenityTypes": {
                "id": 14,
                "name": "play_school",
                "description": "Play School"
            },
            "rating": 4,
            "futureInfrastructure": false
        }, {
            "id": 20390,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 3,
            "name": "ICICI Bank",
            "latitude": 28.5702991,
            "longitude": 77.3626022,
            "geoDistance": 2.5212786197662354,
            "vicinity": "Khasra 50, Bhawani Paradise Hotel, Jammu Road, Katra",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 3,
                "name": "bank",
                "description": "Bank"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 28617,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 3,
            "name": "Bank of Maharashtra",
            "latitude": 28.5781994,
            "longitude": 77.361969,
            "geoDistance": 2.5249195098876953,
            "vicinity": "Sector 51, Noida, Delhi, Block B, Sector 51, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 3,
                "name": "bank",
                "description": "Bank"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 50334,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 5,
            "name": "Moti Mahal Delux Tandoori Trail",
            "latitude": 28.5702744,
            "longitude": 77.3625412,
            "geoDistance": 2.527806282043457,
            "vicinity": "B 1/54 Central Market, Sector 50, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 5,
                "name": "restaurant",
                "description": "Restaurant"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 49358,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 3,
            "name": "Indian Bank",
            "latitude": 28.5698566,
            "longitude": 77.3626251,
            "geoDistance": 2.531350612640381,
            "vicinity": "B-258, Sector 50, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 3,
                "name": "bank",
                "description": "Bank"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 57101,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 5,
            "name": "Pindori - The Village Kitchen",
            "latitude": 28.5702095,
            "longitude": 77.3625031,
            "geoDistance": 2.5330896377563477,
            "vicinity": "First Floor, B-1/52, Central Market,, Sector 50, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 5,
                "name": "restaurant",
                "description": "Restaurant"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 36677,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 5,
            "name": "Joe's Pasta",
            "latitude": 28.5703278,
            "longitude": 77.3624496,
            "geoDistance": 2.535034656524658,
            "vicinity": "Central Market, Sector 50, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 5,
                "name": "restaurant",
                "description": "Restaurant"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 20218,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 5,
            "name": "Qjin Restaurant",
            "latitude": 28.5701962,
            "longitude": 77.3624802,
            "geoDistance": 2.5356109142303467,
            "vicinity": "B 1/23, Central Market, Sector 50, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 5,
                "name": "restaurant",
                "description": "Restaurant"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 20395,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "CardiaGreen Advanced Heart Care Center",
            "latitude": 28.5970211,
            "longitude": 77.3783798,
            "geoDistance": 2.5362606048583984,
            "website": "http://www.cardiagreen.in/",
            "vicinity": "C Block, Block C, Sector 71, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 4,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 2,
            "futureInfrastructure": false
        }, {
            "id": 65329,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 5,
            "name": "Papa John's",
            "latitude": 28.5702629,
            "longitude": 77.362442,
            "geoDistance": 2.537501335144043,
            "vicinity": "B 1/50, Central Market, Sector 50, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 5,
                "name": "restaurant",
                "description": "Restaurant"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 34479,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 5,
            "name": "Granny's Kitchen",
            "latitude": 28.5703182,
            "longitude": 77.3624191,
            "geoDistance": 2.538208246231079,
            "vicinity": "B 1/47, Central Market, Sector 50, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 5,
                "name": "restaurant",
                "description": "Restaurant"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 60785,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 5,
            "name": "Hot & Spicy",
            "latitude": 28.5701485,
            "longitude": 77.3624496,
            "geoDistance": 2.5398385524749756,
            "vicinity": "B Block Market,, Sector 50, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 5,
                "name": "restaurant",
                "description": "Restaurant"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 7844,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 5,
            "name": "nathu sweets",
            "latitude": 28.5608273,
            "longitude": 77.3679428,
            "geoDistance": 2.5407052040100098,
            "vicinity": "Dadri Road, D Block, Block D, Sector 49, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 5,
                "name": "restaurant",
                "description": "Restaurant"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 997,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 204892,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 204893,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 205011,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 205012,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 197484,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 197485,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 197486,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 198590,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 198591,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 198592,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 198780,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 198781,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 198782,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 199378,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 199379,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 199380,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 199762,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 199763,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 199764,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 203080,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 203081,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 203532,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 203533,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 195534,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 195535,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 195536,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 195739,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 195740,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 195741,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 206484,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 206485,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 206486,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 206487,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 206488,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 206797,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 206798,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 207193,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 207194,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 179252,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 179253,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 179254,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 179519,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 179520,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 179521,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 179765,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 179766,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 179767,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 180021,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 180022,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 180023,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 187053,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 187054,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 187055,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 188612,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 188613,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 188614,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 188623,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 188624,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 188625,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 182879,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 182880,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 182881,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 183251,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 183252,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 183253,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 180048,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 180049,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 180050,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 181934,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 181935,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 181936,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 185129,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 185130,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 185131,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 191151,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 191152,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 191153,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 191162,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 191163,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 191164,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 177840,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 157511,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 157512,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 157513,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 109117,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 109118,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 109119,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 106421,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 106422,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 106423,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 116626,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 116627,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 116628,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 212327,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 212328,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 212397,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 212398,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 214027,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 214028,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 227301,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 227302,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 227472,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 227473,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 229344,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 229345,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 158365,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 158366,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 158367,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 164658,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 164659,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 164660,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 135946,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 135947,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 135948,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 133490,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 133491,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 133492,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 134190,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 134191,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 134192,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 123698,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 123699,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 123700,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 124407,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 124408,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 124409,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 231227,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 231228,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 231842,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "Punjab National Bank ATM",
            "latitude": 28.5636997,
            "longitude": 77.3655319,
            "geoDistance": 2.5420284271240234,
            "vicinity": "Aghapur, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 41612,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 3,
            "name": "Indian Overseas Bank (IOB)",
            "latitude": 28.5703259,
            "longitude": 77.3623581,
            "geoDistance": 2.5437233448028564,
            "vicinity": "B 1/27, Central Market, Sector 50, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 3,
                "name": "bank",
                "description": "Bank"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 76200,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 5,
            "name": "The Caspian",
            "latitude": 28.570837,
            "longitude": 77.3622208,
            "geoDistance": 2.5440962314605713,
            "vicinity": "B-1/14, Central Market,, Sector 50, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 5,
                "name": "restaurant",
                "description": "Restaurant"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 109057,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 8,
            "name": "Sector-34 Bustand",
            "latitude": 28.5792103,
            "longitude": 77.361908,
            "geoDistance": 2.5455873012542725,
            "vicinity": "Captain Shashi Kant Marg, Block B, Sector 34, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 106375,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 8,
            "name": "Sector-34 Bustand",
            "latitude": 28.5792103,
            "longitude": 77.361908,
            "geoDistance": 2.5455873012542725,
            "vicinity": "Captain Shashi Kant Marg, Block B, Sector 34, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 116602,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 8,
            "name": "Sector-34 Bustand",
            "latitude": 28.5792103,
            "longitude": 77.361908,
            "geoDistance": 2.5455873012542725,
            "vicinity": "Captain Shashi Kant Marg, Block B, Sector 34, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 175584,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 8,
            "name": "Sector-34 Bustand",
            "latitude": 28.5792103,
            "longitude": 77.361908,
            "geoDistance": 2.5455873012542725,
            "vicinity": "Captain Shashi Kant Marg, Block B, Sector 34, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 158384,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 8,
            "name": "Sector-34 Bustand",
            "latitude": 28.5792103,
            "longitude": 77.361908,
            "geoDistance": 2.5455873012542725,
            "vicinity": "Captain Shashi Kant Marg, Block B, Sector 34, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 164623,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 8,
            "name": "Sector-34 Bustand",
            "latitude": 28.5792103,
            "longitude": 77.361908,
            "geoDistance": 2.5455873012542725,
            "vicinity": "Captain Shashi Kant Marg, Block B, Sector 34, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 135911,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 8,
            "name": "Sector-34 Bustand",
            "latitude": 28.5792103,
            "longitude": 77.361908,
            "geoDistance": 2.5455873012542725,
            "vicinity": "Captain Shashi Kant Marg, Block B, Sector 34, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 125805,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 8,
            "name": "Sector-34 Bustand",
            "latitude": 28.5792103,
            "longitude": 77.361908,
            "geoDistance": 2.5455873012542725,
            "vicinity": "Captain Shashi Kant Marg, Block B, Sector 34, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 125836,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 8,
            "name": "Sector-34 Bustand",
            "latitude": 28.5792103,
            "longitude": 77.361908,
            "geoDistance": 2.5455873012542725,
            "vicinity": "Captain Shashi Kant Marg, Block B, Sector 34, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 133512,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 8,
            "name": "Sector-34 Bustand",
            "latitude": 28.5792103,
            "longitude": 77.361908,
            "geoDistance": 2.5455873012542725,
            "vicinity": "Captain Shashi Kant Marg, Block B, Sector 34, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 134209,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 8,
            "name": "Sector-34 Bustand",
            "latitude": 28.5792103,
            "longitude": 77.361908,
            "geoDistance": 2.5455873012542725,
            "vicinity": "Captain Shashi Kant Marg, Block B, Sector 34, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 123663,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 8,
            "name": "Sector-34 Bustand",
            "latitude": 28.5792103,
            "longitude": 77.361908,
            "geoDistance": 2.5455873012542725,
            "vicinity": "Captain Shashi Kant Marg, Block B, Sector 34, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 124415,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 8,
            "name": "Sector-34 Bustand",
            "latitude": 28.5792103,
            "longitude": 77.361908,
            "geoDistance": 2.5455873012542725,
            "vicinity": "Captain Shashi Kant Marg, Block B, Sector 34, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 71366,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 5,
            "name": "American Fast Food",
            "latitude": 28.5704384,
            "longitude": 77.3622971,
            "geoDistance": 2.5466926097869873,
            "vicinity": "B 1/24, Central Market, Sector 50, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 5,
                "name": "restaurant",
                "description": "Restaurant"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 27149,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 5,
            "name": "Khidmat",
            "latitude": 28.5704823,
            "longitude": 77.3622742,
            "geoDistance": 2.5477285385131836,
            "vicinity": "B 1 58/59, Central Market, Sector 50, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 5,
                "name": "restaurant",
                "description": "Restaurant"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 80196,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 18,
            "name": "Empire Super Market",
            "latitude": 28.5702133,
            "longitude": 77.3623123,
            "geoDistance": 2.551030158996582,
            "vicinity": "Shop No. B-1/48, Central Market, Jain Rd, B Block, Sector 50, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 18,
                "name": "grocery_or_supermarket",
                "description": "Grocery or Supermarket"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 59905,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 5,
            "name": "Shaan E Awadh Hind Biryani",
            "latitude": 28.5700912,
            "longitude": 77.3623352,
            "geoDistance": 2.5521650314331055,
            "vicinity": "Central Market, Sector 50, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 5,
                "name": "restaurant",
                "description": "Restaurant"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 177829,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Prayag Hospital and Research Centre Private Limited",
            "latitude": 28.5636253,
            "longitude": 77.3654556,
            "geoDistance": 2.552772045135498,
            "vicinity": "J-206/A-1, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 147990,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Prayag Hospital and Research Centre Private Limited",
            "latitude": 28.5636253,
            "longitude": 77.3654556,
            "geoDistance": 2.552772045135498,
            "vicinity": "J-206/A-1, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 157487,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Prayag Hospital and Research Centre Private Limited",
            "latitude": 28.5636253,
            "longitude": 77.3654556,
            "geoDistance": 2.552772045135498,
            "vicinity": "J-206/A-1, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 109093,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 2,
            "name": "Prayag Hospital and Research Centre Private Limited",
            "latitude": 28.5636253,
            "longitude": 77.3654556,
            "geoDistance": 2.552772045135498,
            "vicinity": "J-206/A-1, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 113783,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Prayag Hospital and Research Centre Private Limited",
            "latitude": 28.5636253,
            "longitude": 77.3654556,
            "geoDistance": 2.552772045135498,
            "vicinity": "J-206/A-1, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 111922,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Prayag Hospital and Research Centre Private Limited",
            "latitude": 28.5636253,
            "longitude": 77.3654556,
            "geoDistance": 2.552772045135498,
            "vicinity": "J-206/A-1, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 113751,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Prayag Hospital and Research Centre Private Limited",
            "latitude": 28.5636253,
            "longitude": 77.3654556,
            "geoDistance": 2.552772045135498,
            "vicinity": "J-206/A-1, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 106385,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Prayag Hospital and Research Centre Private Limited",
            "latitude": 28.5636253,
            "longitude": 77.3654556,
            "geoDistance": 2.552772045135498,
            "vicinity": "J-206/A-1, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 116645,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Prayag Hospital and Research Centre Private Limited",
            "latitude": 28.5636253,
            "longitude": 77.3654556,
            "geoDistance": 2.552772045135498,
            "vicinity": "J-206/A-1, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 175572,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Prayag Hospital and Research Centre Private Limited",
            "latitude": 28.5636253,
            "longitude": 77.3654556,
            "geoDistance": 2.552772045135498,
            "vicinity": "J-206/A-1, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 175573,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Prayag Hospital and Research Centre Private Limited",
            "latitude": 28.5636253,
            "longitude": 77.3654556,
            "geoDistance": 2.552772045135498,
            "vicinity": "J-206/A-1, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 158370,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 2,
            "name": "Prayag Hospital and Research Centre Private Limited",
            "latitude": 28.5636253,
            "longitude": 77.3654556,
            "geoDistance": 2.552772045135498,
            "vicinity": "J-206/A-1, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 164665,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 2,
            "name": "Prayag Hospital and Research Centre Private Limited",
            "latitude": 28.5636253,
            "longitude": 77.3654556,
            "geoDistance": 2.552772045135498,
            "vicinity": "J-206/A-1, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 135953,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Prayag Hospital and Research Centre Private Limited",
            "latitude": 28.5636253,
            "longitude": 77.3654556,
            "geoDistance": 2.552772045135498,
            "vicinity": "J-206/A-1, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 133524,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 2,
            "name": "Prayag Hospital and Research Centre Private Limited",
            "latitude": 28.5636253,
            "longitude": 77.3654556,
            "geoDistance": 2.552772045135498,
            "vicinity": "J-206/A-1, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 134195,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 2,
            "name": "Prayag Hospital and Research Centre Private Limited",
            "latitude": 28.5636253,
            "longitude": 77.3654556,
            "geoDistance": 2.552772045135498,
            "vicinity": "J-206/A-1, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 123717,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 2,
            "name": "Prayag Hospital and Research Centre Private Limited",
            "latitude": 28.5636253,
            "longitude": 77.3654556,
            "geoDistance": 2.552772045135498,
            "vicinity": "J-206/A-1, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 124439,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Prayag Hospital and Research Centre Private Limited",
            "latitude": 28.5636253,
            "longitude": 77.3654556,
            "geoDistance": 2.552772045135498,
            "vicinity": "J-206/A-1, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 87206,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 17,
            "name": "Main Market Sector 50",
            "latitude": 28.5706158,
            "longitude": 77.3621597,
            "geoDistance": 2.555210590362549,
            "vicinity": "Noida Sector 50",
            "priority": 1,
            "localityAmenityTypes": {
                "id": 17,
                "name": "shopping_mall",
                "description": "Shopping Mall"
            },
            "rating": 5,
            "futureInfrastructure": false
        }, {
            "id": 87207,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 18,
            "name": "Main Market Sector 50",
            "latitude": 28.5706158,
            "longitude": 77.3621597,
            "geoDistance": 2.555210590362549,
            "vicinity": "Noida Sector 50",
            "priority": 1,
            "localityAmenityTypes": {
                "id": 18,
                "name": "grocery_or_supermarket",
                "description": "Grocery or Supermarket"
            },
            "rating": 5,
            "futureInfrastructure": false
        }, {
            "id": 20482,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Prayag Hospital and Research Centre",
            "latitude": 28.5636425,
            "longitude": 77.3653946,
            "geoDistance": 2.5567638874053955,
            "vicinity": "J-206/A-1, J Block, Sector 41, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 3,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 3,
            "futureInfrastructure": false
        }, {
            "id": 49477,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 5,
            "name": "Saffron Grill",
            "latitude": 28.5701504,
            "longitude": 77.3622665,
            "geoDistance": 2.5571696758270264,
            "vicinity": "B-1/17, First Floor, Central Market,, Sector 50, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 5,
                "name": "restaurant",
                "description": "Restaurant"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 58678,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 5,
            "name": "Vijeta Food And Beverages",
            "latitude": 28.5705452,
            "longitude": 77.3621521,
            "geoDistance": 2.5577447414398193,
            "vicinity": "B 1/23, Central Market, Sector 50, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 5,
                "name": "restaurant",
                "description": "Restaurant"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 20141,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 5,
            "name": "nathu sweets and family restaurant",
            "latitude": 28.5608768,
            "longitude": 77.3676453,
            "geoDistance": 2.5592615604400635,
            "vicinity": "Dadri Road, A block, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 5,
                "name": "restaurant",
                "description": "Restaurant"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 27851,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "SBI ATM",
            "latitude": 28.5700607,
            "longitude": 77.3622589,
            "geoDistance": 2.560244083404541,
            "vicinity": "Sector 50, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":3.5}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 147983,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "SBI ATM",
            "latitude": 28.5700607,
            "longitude": 77.3622589,
            "geoDistance": 2.560244083404541,
            "vicinity": "Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 147984,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "SBI ATM",
            "latitude": 28.5700607,
            "longitude": 77.3622589,
            "geoDistance": 2.560244083404541,
            "vicinity": "Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 157498,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "SBI ATM",
            "latitude": 28.5700607,
            "longitude": 77.3622589,
            "geoDistance": 2.560244083404541,
            "vicinity": "Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 109104,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "SBI ATM",
            "latitude": 28.5700607,
            "longitude": 77.3622589,
            "geoDistance": 2.560244083404541,
            "vicinity": "Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 113777,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "SBI ATM",
            "latitude": 28.5700607,
            "longitude": 77.3622589,
            "geoDistance": 2.560244083404541,
            "vicinity": "Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 111928,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "SBI ATM",
            "latitude": 28.5700607,
            "longitude": 77.3622589,
            "geoDistance": 2.560244083404541,
            "vicinity": "Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 113745,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "SBI ATM",
            "latitude": 28.5700607,
            "longitude": 77.3622589,
            "geoDistance": 2.560244083404541,
            "vicinity": "Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 106408,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "SBI ATM",
            "latitude": 28.5700607,
            "longitude": 77.3622589,
            "geoDistance": 2.560244083404541,
            "vicinity": "Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 116612,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "SBI ATM",
            "latitude": 28.5700607,
            "longitude": 77.3622589,
            "geoDistance": 2.560244083404541,
            "vicinity": "Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 175588,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "SBI ATM",
            "latitude": 28.5700607,
            "longitude": 77.3622589,
            "geoDistance": 2.560244083404541,
            "vicinity": "Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 158352,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "SBI ATM",
            "latitude": 28.5700607,
            "longitude": 77.3622589,
            "geoDistance": 2.560244083404541,
            "vicinity": "Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 164645,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "SBI ATM",
            "latitude": 28.5700607,
            "longitude": 77.3622589,
            "geoDistance": 2.560244083404541,
            "vicinity": "Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 165448,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "SBI ATM",
            "latitude": 28.5700607,
            "longitude": 77.3622589,
            "geoDistance": 2.560244083404541,
            "vicinity": "Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 135933,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "SBI ATM",
            "latitude": 28.5700607,
            "longitude": 77.3622589,
            "geoDistance": 2.560244083404541,
            "vicinity": "Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 125791,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "SBI ATM",
            "latitude": 28.5700607,
            "longitude": 77.3622589,
            "geoDistance": 2.560244083404541,
            "vicinity": "Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 125822,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "SBI ATM",
            "latitude": 28.5700607,
            "longitude": 77.3622589,
            "geoDistance": 2.560244083404541,
            "vicinity": "Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 133477,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "SBI ATM",
            "latitude": 28.5700607,
            "longitude": 77.3622589,
            "geoDistance": 2.560244083404541,
            "vicinity": "Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 134177,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "SBI ATM",
            "latitude": 28.5700607,
            "longitude": 77.3622589,
            "geoDistance": 2.560244083404541,
            "vicinity": "Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 123685,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "SBI ATM",
            "latitude": 28.5700607,
            "longitude": 77.3622589,
            "geoDistance": 2.560244083404541,
            "vicinity": "Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 124394,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "SBI ATM",
            "latitude": 28.5700607,
            "longitude": 77.3622589,
            "geoDistance": 2.560244083404541,
            "vicinity": "Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 6730,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 5,
            "name": "The Golden Puff",
            "latitude": 28.5707283,
            "longitude": 77.3620758,
            "geoDistance": 2.560508966445923,
            "vicinity": "Sector 50, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 5,
                "name": "restaurant",
                "description": "Restaurant"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 23375,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 5,
            "name": "South Indian Madrashi Dosa",
            "latitude": 28.5701008,
            "longitude": 77.3622437,
            "geoDistance": 2.560671329498291,
            "vicinity": "near Central Market, Sector 50, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 5,
                "name": "restaurant",
                "description": "Restaurant"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 53543,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 3,
            "name": "Nainital Bank Ltd.",
            "latitude": 28.5706291,
            "longitude": 77.3620987,
            "geoDistance": 2.5607542991638184,
            "vicinity": "B-1/31, B Block, Block A, Sector 50, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 3,
                "name": "bank",
                "description": "Bank"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 50766,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 5,
            "name": "Pizza Hut",
            "latitude": 28.5703468,
            "longitude": 77.3621521,
            "geoDistance": 2.5628039836883545,
            "vicinity": "Sector 50, Noida",
            "restDetails": "{\"price_level\":1,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 5,
                "name": "restaurant",
                "description": "Restaurant"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 44216,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 5,
            "name": "Sweat Zone",
            "latitude": 28.5704594,
            "longitude": 77.3620987,
            "geoDistance": 2.5650105476379395,
            "vicinity": "Central Market, Sector 50, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 5,
                "name": "restaurant",
                "description": "Restaurant"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 52827,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 5,
            "name": "Pizza Hut Delivery",
            "latitude": 28.5703964,
            "longitude": 77.3620911,
            "geoDistance": 2.567256212234497,
            "vicinity": "B1/32, Central Market, Sector 50, Noida",
            "restDetails": "{\"price_level\":1,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 5,
                "name": "restaurant",
                "description": "Restaurant"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 83808,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 16,
            "name": "E Block Park",
            "latitude": 28.5661488,
            "longitude": 77.3637466,
            "geoDistance": 2.5675134658813477,
            "vicinity": "Jain Rd, Block E, Sector 41, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":4}",
            "priority": 1,
            "localityAmenityTypes": {
                "id": 16,
                "name": "park",
                "description": "Park"
            },
            "rating": 5,
            "futureInfrastructure": false
        }, {
            "id": 203078,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 2,
            "name": "Prayag Hospital and Research Centre Private Limited",
            "latitude": 28.5634956,
            "longitude": 77.3653641,
            "geoDistance": 2.567896604537964,
            "vicinity": "J-206/A-1, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 203534,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 2,
            "name": "Prayag Hospital and Research Centre Private Limited",
            "latitude": 28.5634956,
            "longitude": 77.3653641,
            "geoDistance": 2.567896604537964,
            "vicinity": "J-206/A-1, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 67888,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 5,
            "name": "Yo China",
            "latitude": 28.5704823,
            "longitude": 77.3620605,
            "geoDistance": 2.5680041313171387,
            "vicinity": "Sector 50, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 5,
                "name": "restaurant",
                "description": "Restaurant"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 80197,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 18,
            "name": "Yakult Danone India Private Limited",
            "latitude": 28.5706291,
            "longitude": 77.3620148,
            "geoDistance": 2.568673610687256,
            "vicinity": "Shop Number. B-1/22, Commercial Complex, Sector 55 ATS Greens 2 Rd, A Block, Sector 50, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 18,
                "name": "grocery_or_supermarket",
                "description": "Grocery or Supermarket"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 44790,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 5,
            "name": "Theo's Bakery",
            "latitude": 28.5706596,
            "longitude": 77.3619766,
            "geoDistance": 2.571657657623291,
            "vicinity": "B-1/20, Sector 50, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":4.6}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 5,
                "name": "restaurant",
                "description": "Restaurant"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 227296,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 17,
            "name": "Om Gallery",
            "latitude": 28.5701046,
            "longitude": 77.3621216,
            "geoDistance": 2.5721213817596436,
            "vicinity": "b ground floor b, 50, Sector 50 Block D Road, D Block, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 17,
                "name": "shopping_mall",
                "description": "Shopping Mall"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 227297,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 17,
            "name": "Om Gallery",
            "latitude": 28.5701046,
            "longitude": 77.3621216,
            "geoDistance": 2.5721213817596436,
            "vicinity": "b ground floor b, 50, Sector 50 Block D Road, D Block, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 17,
                "name": "shopping_mall",
                "description": "Shopping Mall"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 227465,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 17,
            "name": "Om Gallery",
            "latitude": 28.5701046,
            "longitude": 77.3621216,
            "geoDistance": 2.5721213817596436,
            "vicinity": "b ground floor b, 50, Sector 50 Block D Road, D Block, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 17,
                "name": "shopping_mall",
                "description": "Shopping Mall"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 227466,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 17,
            "name": "Om Gallery",
            "latitude": 28.5701046,
            "longitude": 77.3621216,
            "geoDistance": 2.5721213817596436,
            "vicinity": "b ground floor b, 50, Sector 50 Block D Road, D Block, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 17,
                "name": "shopping_mall",
                "description": "Shopping Mall"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 229339,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 17,
            "name": "Om Gallery",
            "latitude": 28.5701046,
            "longitude": 77.3621216,
            "geoDistance": 2.5721213817596436,
            "vicinity": "b ground floor b, 50, Sector 50 Block D Road, D Block, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 17,
                "name": "shopping_mall",
                "description": "Shopping Mall"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 229340,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 17,
            "name": "Om Gallery",
            "latitude": 28.5701046,
            "longitude": 77.3621216,
            "geoDistance": 2.5721213817596436,
            "vicinity": "b ground floor b, 50, Sector 50 Block D Road, D Block, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 17,
                "name": "shopping_mall",
                "description": "Shopping Mall"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 231222,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 17,
            "name": "Om Gallery",
            "latitude": 28.5701046,
            "longitude": 77.3621216,
            "geoDistance": 2.5721213817596436,
            "vicinity": "b ground floor b, 50, Sector 50 Block D Road, D Block, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 17,
                "name": "shopping_mall",
                "description": "Shopping Mall"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 231223,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 17,
            "name": "Om Gallery",
            "latitude": 28.5701046,
            "longitude": 77.3621216,
            "geoDistance": 2.5721213817596436,
            "vicinity": "b ground floor b, 50, Sector 50 Block D Road, D Block, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 17,
                "name": "shopping_mall",
                "description": "Shopping Mall"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 231837,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 17,
            "name": "Om Gallery",
            "latitude": 28.5701046,
            "longitude": 77.3621216,
            "geoDistance": 2.5721213817596436,
            "vicinity": "b ground floor b, 50, Sector 50 Block D Road, D Block, Sector 50, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 17,
                "name": "shopping_mall",
                "description": "Shopping Mall"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 16011,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 5,
            "name": "Moti Mahal Deluxe",
            "latitude": 28.5700188,
            "longitude": 77.3621445,
            "geoDistance": 2.572298765182495,
            "vicinity": "Sector 50, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 5,
                "name": "restaurant",
                "description": "Restaurant"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 10547,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 3,
            "name": "Bank of India",
            "latitude": 28.5700665,
            "longitude": 77.3621292,
            "geoDistance": 2.572413444519043,
            "vicinity": "B-1/2, Central market, Sector 50, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 3,
                "name": "bank",
                "description": "Bank"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 16774,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 5,
            "name": "Southern Treat",
            "latitude": 28.5704708,
            "longitude": 77.3620148,
            "geoDistance": 2.5726115703582764,
            "vicinity": "B1/32, Commercial Complex, Central Market,, Sector 50, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 5,
                "name": "restaurant",
                "description": "Restaurant"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 3095,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 5,
            "name": "Domino's Pizza",
            "latitude": 28.5699978,
            "longitude": 77.3621368,
            "geoDistance": 2.5735225677490234,
            "vicinity": "B-1/55, Jain Rd, Near Central Market, Sector 50, Noida, Uttar Pradesh 201301, B Block, Block A, Sect",
            "restDetails": "{\"price_level\":1,\"rating\":3.3}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 5,
                "name": "restaurant",
                "description": "Restaurant"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 43818,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 5,
            "name": "Lahori Restaurant",
            "latitude": 28.5704727,
            "longitude": 77.3619995,
            "geoDistance": 2.574108362197876,
            "vicinity": "B-1/28,Central Market,Above Reebok Showroom, B Block, Block A, Sector 50, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 5,
                "name": "restaurant",
                "description": "Restaurant"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 198653,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Goodluck Medicose",
            "latitude": 28.5905704,
            "longitude": 77.3674011,
            "geoDistance": 2.577597141265869,
            "vicinity": "Gijhore, Sector 53, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 32156,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 5,
            "name": "Q'gin Restaurant",
            "latitude": 28.5705585,
            "longitude": 77.3619232,
            "geoDistance": 2.5792369842529297,
            "vicinity": "B-1, 23, Central Market, Sector 50, Noida, Uttar Pradesh, 201304, B Block, Block A, Sector 50, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 5,
                "name": "restaurant",
                "description": "Restaurant"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 206795,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 2,
            "name": "ULTIMATE LIFE",
            "latitude": 28.5773106,
            "longitude": 77.36129,
            "geoDistance": 2.5823111534118652,
            "vicinity": "B Block, Sector 10, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 207195,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 2,
            "name": "ULTIMATE LIFE",
            "latitude": 28.5773106,
            "longitude": 77.36129,
            "geoDistance": 2.5823111534118652,
            "vicinity": "B Block, Sector 10, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 26172,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 3,
            "name": "HDFC Bank",
            "latitude": 28.5700436,
            "longitude": 77.3620148,
            "geoDistance": 2.583827495574951,
            "vicinity": "B 1/51 & 1/60, Jain Road, B Block, Block A, Sector 50, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 3,
                "name": "bank",
                "description": "Bank"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 10400,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 5,
            "name": "Subway",
            "latitude": 28.5705109,
            "longitude": 77.3618469,
            "geoDistance": 2.5875515937805176,
            "vicinity": "B1/24, Commercial Market, Sector 50, Noida, Uttar Pradesh, 201304, B Block, Block A, Sector 50, Noid",
            "restDetails": "{\"price_level\":1,\"rating\":3.2}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 5,
                "name": "restaurant",
                "description": "Restaurant"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 37722,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 3,
            "name": "State Bank of India",
            "latitude": 28.5689392,
            "longitude": 77.362236,
            "geoDistance": 2.595853805541992,
            "vicinity": "Sector 41, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 3,
                "name": "bank",
                "description": "Bank"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 19073,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "Kotak Mahindra Bank ATM",
            "latitude": 28.5775986,
            "longitude": 77.3611603,
            "geoDistance": 2.5972187519073486,
            "vicinity": "Block B, Sector 51, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 19739,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "Indian Bank ATM",
            "latitude": 28.5691032,
            "longitude": 77.3621521,
            "geoDistance": 2.598536968231201,
            "vicinity": "C-block Market, Sector 41, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 41503,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "City Public School",
            "latitude": 28.5943184,
            "longitude": 77.3714066,
            "geoDistance": 2.6074063777923584,
            "website": "http://www.cpsnoida.com/",
            "vicinity": "E-3, Sector 61, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 4,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 2,
            "futureInfrastructure": false
        }, {
            "id": 60367,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 5,
            "name": "Wah Ji Wah",
            "latitude": 28.5691624,
            "longitude": 77.3619843,
            "geoDistance": 2.612401008605957,
            "vicinity": "C-Block Market, Sector 41, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 5,
                "name": "restaurant",
                "description": "Restaurant"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 53528,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Eye Health Clinic",
            "latitude": 28.5935001,
            "longitude": 77.3701248,
            "geoDistance": 2.6159827709198,
            "website": "http://eyehealthcentre.org/eye-health-clinic.php",
            "vicinity": "Life Care Hospital, Near Sai Mandir, Sector 61, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 4,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 2,
            "futureInfrastructure": false
        }, {
            "id": 143746,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Parivartan Mission School",
            "latitude": 28.5985203,
            "longitude": 77.3943405,
            "geoDistance": 2.6173980236053467,
            "vicinity": "Garhi Chaukhandi, Chaukhandi, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 23687,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "IDBI Bank",
            "latitude": 28.5694008,
            "longitude": 77.361824,
            "geoDistance": 2.620248317718506,
            "vicinity": "C-Block Market, Sector 41, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 157504,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "IDBI Bank",
            "latitude": 28.5694008,
            "longitude": 77.361824,
            "geoDistance": 2.620248317718506,
            "vicinity": "C-Block Market, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 109110,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "IDBI Bank",
            "latitude": 28.5694008,
            "longitude": 77.361824,
            "geoDistance": 2.620248317718506,
            "vicinity": "C-Block Market, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 113782,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "IDBI Bank",
            "latitude": 28.5694008,
            "longitude": 77.361824,
            "geoDistance": 2.620248317718506,
            "vicinity": "C-Block Market, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 111935,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "IDBI Bank",
            "latitude": 28.5694008,
            "longitude": 77.361824,
            "geoDistance": 2.620248317718506,
            "vicinity": "C-Block Market, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 113750,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "IDBI Bank",
            "latitude": 28.5694008,
            "longitude": 77.361824,
            "geoDistance": 2.620248317718506,
            "vicinity": "C-Block Market, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 106414,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "IDBI Bank",
            "latitude": 28.5694008,
            "longitude": 77.361824,
            "geoDistance": 2.620248317718506,
            "vicinity": "C-Block Market, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 116619,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "IDBI Bank",
            "latitude": 28.5694008,
            "longitude": 77.361824,
            "geoDistance": 2.620248317718506,
            "vicinity": "C-Block Market, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 175594,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "IDBI Bank",
            "latitude": 28.5694008,
            "longitude": 77.361824,
            "geoDistance": 2.620248317718506,
            "vicinity": "C-Block Market, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 175595,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "IDBI Bank",
            "latitude": 28.5694008,
            "longitude": 77.361824,
            "geoDistance": 2.620248317718506,
            "vicinity": "C-Block Market, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 158358,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "IDBI Bank",
            "latitude": 28.5694008,
            "longitude": 77.361824,
            "geoDistance": 2.620248317718506,
            "vicinity": "C-Block Market, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 164651,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "IDBI Bank",
            "latitude": 28.5694008,
            "longitude": 77.361824,
            "geoDistance": 2.620248317718506,
            "vicinity": "C-Block Market, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 135939,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "IDBI Bank",
            "latitude": 28.5694008,
            "longitude": 77.361824,
            "geoDistance": 2.620248317718506,
            "vicinity": "C-Block Market, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 125794,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "IDBI Bank",
            "latitude": 28.5694008,
            "longitude": 77.361824,
            "geoDistance": 2.620248317718506,
            "vicinity": "C-Block Market, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 125825,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "IDBI Bank",
            "latitude": 28.5694008,
            "longitude": 77.361824,
            "geoDistance": 2.620248317718506,
            "vicinity": "C-Block Market, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 133483,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "IDBI Bank",
            "latitude": 28.5694008,
            "longitude": 77.361824,
            "geoDistance": 2.620248317718506,
            "vicinity": "C-Block Market, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 134183,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "IDBI Bank",
            "latitude": 28.5694008,
            "longitude": 77.361824,
            "geoDistance": 2.620248317718506,
            "vicinity": "C-Block Market, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 123691,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "IDBI Bank",
            "latitude": 28.5694008,
            "longitude": 77.361824,
            "geoDistance": 2.620248317718506,
            "vicinity": "C-Block Market, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 124400,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "IDBI Bank",
            "latitude": 28.5694008,
            "longitude": 77.361824,
            "geoDistance": 2.620248317718506,
            "vicinity": "C-Block Market, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 80195,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 18,
            "name": "Nine O Nine Store 41",
            "latitude": 28.5693703,
            "longitude": 77.3618088,
            "geoDistance": 2.622558116912842,
            "vicinity": "C-98/15, Sector 41 Market, Jain Rd, Block C, Sector 41, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 18,
                "name": "grocery_or_supermarket",
                "description": "Grocery or Supermarket"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 50851,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 6,
            "name": "HP Petrol Pump",
            "latitude": 28.5953865,
            "longitude": 77.3727264,
            "geoDistance": 2.6288576126098633,
            "vicinity": "Sector 71, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 6,
                "name": "gas_station",
                "description": "Petrol Pump"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 2344,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 5,
            "name": "Peshawari Dlx Restaurant",
            "latitude": 28.5692005,
            "longitude": 77.3617859,
            "geoDistance": 2.6298635005950928,
            "vicinity": "C Block Market, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 5,
                "name": "restaurant",
                "description": "Restaurant"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 50658,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 5,
            "name": "Home Needs/ YUMMY TUMMY",
            "latitude": 28.5691452,
            "longitude": 77.361763,
            "geoDistance": 2.633695363998413,
            "vicinity": "C Block Market, Sector 41, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 5,
                "name": "restaurant",
                "description": "Restaurant"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 30958,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 5,
            "name": "Peshawari Delux",
            "latitude": 28.569313,
            "longitude": 77.3616562,
            "geoDistance": 2.638664722442627,
            "vicinity": "C-Block Market, Sector 41, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 5,
                "name": "restaurant",
                "description": "Restaurant"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 1580,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 5,
            "name": "D Hung-er Over",
            "latitude": 28.5691376,
            "longitude": 77.3616943,
            "geoDistance": 2.6403605937957764,
            "vicinity": "C-Block Market, Sector 41, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 5,
                "name": "restaurant",
                "description": "Restaurant"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 83837,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 16,
            "name": "A block park",
            "latitude": 28.5758152,
            "longitude": 77.3606186,
            "geoDistance": 2.6417760848999023,
            "vicinity": "A-100/4, Block A, Sector 51, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 1,
            "localityAmenityTypes": {
                "id": 16,
                "name": "park",
                "description": "Park"
            },
            "rating": 5,
            "futureInfrastructure": false
        }, {
            "id": 66382,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 5,
            "name": "JBS Ever Green Cool Point",
            "latitude": 28.5691681,
            "longitude": 77.3616257,
            "geoDistance": 2.645831346511841,
            "vicinity": "25, C-Block Market, Sector 41, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 5,
                "name": "restaurant",
                "description": "Restaurant"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 64459,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 5,
            "name": "China Hot",
            "latitude": 28.5692616,
            "longitude": 77.3615799,
            "geoDistance": 2.647385358810425,
            "vicinity": "C-Block Market, Sector 41, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 5,
                "name": "restaurant",
                "description": "Restaurant"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 49008,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 14,
            "name": "Bubbles Play School",
            "latitude": 28.5942116,
            "longitude": 77.3705521,
            "geoDistance": 2.6497702598571777,
            "vicinity": "Gol Chakkar, Block E, Sector 61, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 2,
            "localityAmenityTypes": {
                "id": 14,
                "name": "play_school",
                "description": "Play School"
            },
            "rating": 4,
            "futureInfrastructure": false
        }, {
            "id": 157480,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Vinod Pawar Mission Chemistry",
            "latitude": 28.5691738,
            "longitude": 77.361496,
            "geoDistance": 2.65788197517395,
            "vicinity": "9, Second Floor, C-Block Market,, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 109086,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 1,
            "name": "Vinod Pawar Mission Chemistry",
            "latitude": 28.5691738,
            "longitude": 77.361496,
            "geoDistance": 2.65788197517395,
            "vicinity": "9, Second Floor, C-Block Market,, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 113773,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Vinod Pawar Mission Chemistry",
            "latitude": 28.5691738,
            "longitude": 77.361496,
            "geoDistance": 2.65788197517395,
            "vicinity": "9, Second Floor, C-Block Market,, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 113766,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Vinod Pawar Mission Chemistry",
            "latitude": 28.5691738,
            "longitude": 77.361496,
            "geoDistance": 2.65788197517395,
            "vicinity": "9, Second Floor, C-Block Market,, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 106403,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Vinod Pawar Mission Chemistry",
            "latitude": 28.5691738,
            "longitude": 77.361496,
            "geoDistance": 2.65788197517395,
            "vicinity": "9, Second Floor, C-Block Market,, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 116638,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Vinod Pawar Mission Chemistry",
            "latitude": 28.5691738,
            "longitude": 77.361496,
            "geoDistance": 2.65788197517395,
            "vicinity": "9, Second Floor, C-Block Market,, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 175570,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Vinod Pawar Mission Chemistry",
            "latitude": 28.5691738,
            "longitude": 77.361496,
            "geoDistance": 2.65788197517395,
            "vicinity": "9, Second Floor, C-Block Market,, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 158347,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 1,
            "name": "Vinod Pawar Mission Chemistry",
            "latitude": 28.5691738,
            "longitude": 77.361496,
            "geoDistance": 2.65788197517395,
            "vicinity": "9, Second Floor, C-Block Market,, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 164640,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 1,
            "name": "Vinod Pawar Mission Chemistry",
            "latitude": 28.5691738,
            "longitude": 77.361496,
            "geoDistance": 2.65788197517395,
            "vicinity": "9, Second Floor, C-Block Market,, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 135928,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Vinod Pawar Mission Chemistry",
            "latitude": 28.5691738,
            "longitude": 77.361496,
            "geoDistance": 2.65788197517395,
            "vicinity": "9, Second Floor, C-Block Market,, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 133504,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 1,
            "name": "Vinod Pawar Mission Chemistry",
            "latitude": 28.5691738,
            "longitude": 77.361496,
            "geoDistance": 2.65788197517395,
            "vicinity": "9, Second Floor, C-Block Market,, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 134226,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 1,
            "name": "Vinod Pawar Mission Chemistry",
            "latitude": 28.5691738,
            "longitude": 77.361496,
            "geoDistance": 2.65788197517395,
            "vicinity": "9, Second Floor, C-Block Market,, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 123710,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 1,
            "name": "Vinod Pawar Mission Chemistry",
            "latitude": 28.5691738,
            "longitude": 77.361496,
            "geoDistance": 2.65788197517395,
            "vicinity": "9, Second Floor, C-Block Market,, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 124432,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Vinod Pawar Mission Chemistry",
            "latitude": 28.5691738,
            "longitude": 77.361496,
            "geoDistance": 2.65788197517395,
            "vicinity": "9, Second Floor, C-Block Market,, Sector 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 4225,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Dhanwantri Ayurveda Care Clinic",
            "latitude": 28.5700951,
            "longitude": 77.3611374,
            "geoDistance": 2.665753126144409,
            "website": "http://www.dhanwantriayurveda.com/",
            "vicinity": "A-142, besides Veracity Dental Care, Near C Block Market, Jain Road, Sector 41, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 4,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 2,
            "futureInfrastructure": false
        }, {
            "id": 83810,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 16,
            "name": "C Block Park",
            "latitude": 28.5689869,
            "longitude": 77.3614273,
            "geoDistance": 2.6700479984283447,
            "vicinity": "Block C, Sector 41, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 1,
            "localityAmenityTypes": {
                "id": 16,
                "name": "park",
                "description": "Park"
            },
            "rating": 5,
            "futureInfrastructure": false
        }, {
            "id": 16150,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "PNB ATM",
            "latitude": 28.5816555,
            "longitude": 77.3610992,
            "geoDistance": 2.677391290664673,
            "vicinity": "Sector 34, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 42417,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 3,
            "name": "Citizen Coop Bank",
            "latitude": 28.5864525,
            "longitude": 77.3631134,
            "geoDistance": 2.6784775257110596,
            "vicinity": "Shop No-17-18-19, Ashirwad Complex, Village Tijore, Near Coast Guard, Sector 53, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 3,
                "name": "bank",
                "description": "Bank"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 24214,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 3,
            "name": "Noble Bank",
            "latitude": 28.5864735,
            "longitude": 77.3631058,
            "geoDistance": 2.680244207382202,
            "vicinity": "B-19 & 20, Asliirwad Complex, Sector 53, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 3,
                "name": "bank",
                "description": "Bank"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 14192,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 5,
            "name": "Mithaas",
            "latitude": 28.5864296,
            "longitude": 77.3630753,
            "geoDistance": 2.6807515621185303,
            "vicinity": "G 20-21, Ashirwad Complex, Sector 53, Block F, Sector 52, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 5,
                "name": "restaurant",
                "description": "Restaurant"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 76436,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "PNB ATM",
            "latitude": 28.5773029,
            "longitude": 77.3602676,
            "geoDistance": 2.681817054748535,
            "vicinity": "Block B, Sector 51, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 157514,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "PNB ATM",
            "latitude": 28.5773029,
            "longitude": 77.3602676,
            "geoDistance": 2.681817054748535,
            "vicinity": "Block B, Sector 51, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 157515,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "PNB ATM",
            "latitude": 28.5773029,
            "longitude": 77.3602676,
            "geoDistance": 2.681817054748535,
            "vicinity": "Block B, Sector 51, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 109120,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "PNB ATM",
            "latitude": 28.5773029,
            "longitude": 77.3602676,
            "geoDistance": 2.681817054748535,
            "vicinity": "Block B, Sector 51, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 109121,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "PNB ATM",
            "latitude": 28.5773029,
            "longitude": 77.3602676,
            "geoDistance": 2.681817054748535,
            "vicinity": "Block B, Sector 51, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 106424,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "PNB ATM",
            "latitude": 28.5773029,
            "longitude": 77.3602676,
            "geoDistance": 2.681817054748535,
            "vicinity": "Block B, Sector 51, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 106425,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "PNB ATM",
            "latitude": 28.5773029,
            "longitude": 77.3602676,
            "geoDistance": 2.681817054748535,
            "vicinity": "Block B, Sector 51, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 116629,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "PNB ATM",
            "latitude": 28.5773029,
            "longitude": 77.3602676,
            "geoDistance": 2.681817054748535,
            "vicinity": "Block B, Sector 51, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 158368,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "PNB ATM",
            "latitude": 28.5773029,
            "longitude": 77.3602676,
            "geoDistance": 2.681817054748535,
            "vicinity": "Block B, Sector 51, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 158369,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "PNB ATM",
            "latitude": 28.5773029,
            "longitude": 77.3602676,
            "geoDistance": 2.681817054748535,
            "vicinity": "Block B, Sector 51, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 164661,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "PNB ATM",
            "latitude": 28.5773029,
            "longitude": 77.3602676,
            "geoDistance": 2.681817054748535,
            "vicinity": "Block B, Sector 51, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 164662,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "PNB ATM",
            "latitude": 28.5773029,
            "longitude": 77.3602676,
            "geoDistance": 2.681817054748535,
            "vicinity": "Block B, Sector 51, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 164663,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "PNB ATM",
            "latitude": 28.5773029,
            "longitude": 77.3602676,
            "geoDistance": 2.681817054748535,
            "vicinity": "Block B, Sector 51, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 164664,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "PNB ATM",
            "latitude": 28.5773029,
            "longitude": 77.3602676,
            "geoDistance": 2.681817054748535,
            "vicinity": "Block B, Sector 51, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 135949,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "PNB ATM",
            "latitude": 28.5773029,
            "longitude": 77.3602676,
            "geoDistance": 2.681817054748535,
            "vicinity": "Block B, Sector 51, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 135950,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "PNB ATM",
            "latitude": 28.5773029,
            "longitude": 77.3602676,
            "geoDistance": 2.681817054748535,
            "vicinity": "Block B, Sector 51, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 133493,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "PNB ATM",
            "latitude": 28.5773029,
            "longitude": 77.3602676,
            "geoDistance": 2.681817054748535,
            "vicinity": "Block B, Sector 51, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 133494,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "PNB ATM",
            "latitude": 28.5773029,
            "longitude": 77.3602676,
            "geoDistance": 2.681817054748535,
            "vicinity": "Block B, Sector 51, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 133522,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "PNB ATM",
            "latitude": 28.5773029,
            "longitude": 77.3602676,
            "geoDistance": 2.681817054748535,
            "vicinity": "Block B, Sector 51, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 133523,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "PNB ATM",
            "latitude": 28.5773029,
            "longitude": 77.3602676,
            "geoDistance": 2.681817054748535,
            "vicinity": "Block B, Sector 51, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 134193,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "PNB ATM",
            "latitude": 28.5773029,
            "longitude": 77.3602676,
            "geoDistance": 2.681817054748535,
            "vicinity": "Block B, Sector 51, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 134194,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "PNB ATM",
            "latitude": 28.5773029,
            "longitude": 77.3602676,
            "geoDistance": 2.681817054748535,
            "vicinity": "Block B, Sector 51, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 123701,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "PNB ATM",
            "latitude": 28.5773029,
            "longitude": 77.3602676,
            "geoDistance": 2.681817054748535,
            "vicinity": "Block B, Sector 51, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 123702,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "PNB ATM",
            "latitude": 28.5773029,
            "longitude": 77.3602676,
            "geoDistance": 2.681817054748535,
            "vicinity": "Block B, Sector 51, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 123715,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "PNB ATM",
            "latitude": 28.5773029,
            "longitude": 77.3602676,
            "geoDistance": 2.681817054748535,
            "vicinity": "Block B, Sector 51, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 123716,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "PNB ATM",
            "latitude": 28.5773029,
            "longitude": 77.3602676,
            "geoDistance": 2.681817054748535,
            "vicinity": "Block B, Sector 51, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 124410,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "PNB ATM",
            "latitude": 28.5773029,
            "longitude": 77.3602676,
            "geoDistance": 2.681817054748535,
            "vicinity": "Block B, Sector 51, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 124411,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "PNB ATM",
            "latitude": 28.5773029,
            "longitude": 77.3602676,
            "geoDistance": 2.681817054748535,
            "vicinity": "Block B, Sector 51, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 124437,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "PNB ATM",
            "latitude": 28.5773029,
            "longitude": 77.3602676,
            "geoDistance": 2.681817054748535,
            "vicinity": "Block B, Sector 51, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 124438,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "PNB ATM",
            "latitude": 28.5773029,
            "longitude": 77.3602676,
            "geoDistance": 2.681817054748535,
            "vicinity": "Block B, Sector 51, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 19881,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "SBI ATM",
            "latitude": 28.5773106,
            "longitude": 77.3602676,
            "geoDistance": 2.6818759441375732,
            "vicinity": "Block B, Sector 51, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 83807,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 16,
            "name": "Park and Jogging Track",
            "latitude": 28.5633221,
            "longitude": 77.3641129,
            "geoDistance": 2.6822597980499268,
            "vicinity": "J Block, Sector 41, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 1,
            "localityAmenityTypes": {
                "id": 16,
                "name": "park",
                "description": "Park"
            },
            "rating": 5,
            "futureInfrastructure": false
        }, {
            "id": 29327,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 3,
            "name": "Corporation Bank",
            "latitude": 28.5864353,
            "longitude": 77.3630295,
            "geoDistance": 2.6850719451904297,
            "vicinity": "Sector 53, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 3,
                "name": "bank",
                "description": "Bank"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 19267,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 3,
            "name": "Indian Overseas Bank",
            "latitude": 28.5883808,
            "longitude": 77.3642197,
            "geoDistance": 2.688004970550537,
            "vicinity": "Sector 53, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 3,
                "name": "bank",
                "description": "Bank"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 177842,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 17,
            "name": "Shopping Planet",
            "latitude": 28.5553303,
            "longitude": 77.3728714,
            "geoDistance": 2.688976526260376,
            "vicinity": "Dadri Main Rd, Sector 48, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 17,
                "name": "shopping_mall",
                "description": "Shopping Mall"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 177843,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 17,
            "name": "Shopping Planet",
            "latitude": 28.5553303,
            "longitude": 77.3728714,
            "geoDistance": 2.688976526260376,
            "vicinity": "Dadri Main Rd, Sector 48, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 17,
                "name": "shopping_mall",
                "description": "Shopping Mall"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 157462,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 17,
            "name": "Shopping Planet",
            "latitude": 28.5553303,
            "longitude": 77.3728714,
            "geoDistance": 2.688976526260376,
            "vicinity": "Dadri Main Rd, Sector 48, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 17,
                "name": "shopping_mall",
                "description": "Shopping Mall"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 157463,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 17,
            "name": "Shopping Planet",
            "latitude": 28.5553303,
            "longitude": 77.3728714,
            "geoDistance": 2.688976526260376,
            "vicinity": "Dadri Main Rd, Sector 48, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 17,
                "name": "shopping_mall",
                "description": "Shopping Mall"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 157464,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 17,
            "name": "Shopping Planet",
            "latitude": 28.5553303,
            "longitude": 77.3728714,
            "geoDistance": 2.688976526260376,
            "vicinity": "Dadri Main Rd, Sector 48, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 17,
                "name": "shopping_mall",
                "description": "Shopping Mall"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 109068,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 17,
            "name": "Shopping Planet",
            "latitude": 28.5553303,
            "longitude": 77.3728714,
            "geoDistance": 2.688976526260376,
            "vicinity": "Dadri Main Rd, Sector 48, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 17,
                "name": "shopping_mall",
                "description": "Shopping Mall"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 109069,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 17,
            "name": "Shopping Planet",
            "latitude": 28.5553303,
            "longitude": 77.3728714,
            "geoDistance": 2.688976526260376,
            "vicinity": "Dadri Main Rd, Sector 48, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 17,
                "name": "shopping_mall",
                "description": "Shopping Mall"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 109070,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 17,
            "name": "Shopping Planet",
            "latitude": 28.5553303,
            "longitude": 77.3728714,
            "geoDistance": 2.688976526260376,
            "vicinity": "Dadri Main Rd, Sector 48, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 17,
                "name": "shopping_mall",
                "description": "Shopping Mall"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 106361,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 17,
            "name": "Shopping Planet",
            "latitude": 28.5553303,
            "longitude": 77.3728714,
            "geoDistance": 2.688976526260376,
            "vicinity": "Dadri Main Rd, Sector 48, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 17,
                "name": "shopping_mall",
                "description": "Shopping Mall"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 106362,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 17,
            "name": "Shopping Planet",
            "latitude": 28.5553303,
            "longitude": 77.3728714,
            "geoDistance": 2.688976526260376,
            "vicinity": "Dadri Main Rd, Sector 48, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 17,
                "name": "shopping_mall",
                "description": "Shopping Mall"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 106363,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 17,
            "name": "Shopping Planet",
            "latitude": 28.5553303,
            "longitude": 77.3728714,
            "geoDistance": 2.688976526260376,
            "vicinity": "Dadri Main Rd, Sector 48, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 17,
                "name": "shopping_mall",
                "description": "Shopping Mall"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 116589,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 17,
            "name": "Shopping Planet",
            "latitude": 28.5553303,
            "longitude": 77.3728714,
            "geoDistance": 2.688976526260376,
            "vicinity": "Dadri Main Rd, Sector 48, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 17,
                "name": "shopping_mall",
                "description": "Shopping Mall"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 175602,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 17,
            "name": "Shopping Planet",
            "latitude": 28.5553303,
            "longitude": 77.3728714,
            "geoDistance": 2.688976526260376,
            "vicinity": "Dadri Main Rd, Sector 48, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 17,
                "name": "shopping_mall",
                "description": "Shopping Mall"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 175603,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 17,
            "name": "Shopping Planet",
            "latitude": 28.5553303,
            "longitude": 77.3728714,
            "geoDistance": 2.688976526260376,
            "vicinity": "Dadri Main Rd, Sector 48, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 17,
                "name": "shopping_mall",
                "description": "Shopping Mall"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 158329,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 17,
            "name": "Shopping Planet",
            "latitude": 28.5553303,
            "longitude": 77.3728714,
            "geoDistance": 2.688976526260376,
            "vicinity": "Dadri Main Rd, Sector 48, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 17,
                "name": "shopping_mall",
                "description": "Shopping Mall"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 158330,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 17,
            "name": "Shopping Planet",
            "latitude": 28.5553303,
            "longitude": 77.3728714,
            "geoDistance": 2.688976526260376,
            "vicinity": "Dadri Main Rd, Sector 48, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 17,
                "name": "shopping_mall",
                "description": "Shopping Mall"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 158331,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 17,
            "name": "Shopping Planet",
            "latitude": 28.5553303,
            "longitude": 77.3728714,
            "geoDistance": 2.688976526260376,
            "vicinity": "Dadri Main Rd, Sector 48, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 17,
                "name": "shopping_mall",
                "description": "Shopping Mall"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 164609,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 17,
            "name": "Shopping Planet",
            "latitude": 28.5553303,
            "longitude": 77.3728714,
            "geoDistance": 2.688976526260376,
            "vicinity": "Dadri Main Rd, Sector 48, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 17,
                "name": "shopping_mall",
                "description": "Shopping Mall"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 164610,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 17,
            "name": "Shopping Planet",
            "latitude": 28.5553303,
            "longitude": 77.3728714,
            "geoDistance": 2.688976526260376,
            "vicinity": "Dadri Main Rd, Sector 48, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 17,
                "name": "shopping_mall",
                "description": "Shopping Mall"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 164611,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 17,
            "name": "Shopping Planet",
            "latitude": 28.5553303,
            "longitude": 77.3728714,
            "geoDistance": 2.688976526260376,
            "vicinity": "Dadri Main Rd, Sector 48, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 17,
                "name": "shopping_mall",
                "description": "Shopping Mall"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 164677,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 17,
            "name": "Shopping Planet",
            "latitude": 28.5553303,
            "longitude": 77.3728714,
            "geoDistance": 2.688976526260376,
            "vicinity": "Dadri Main Rd, Sector 48, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 17,
                "name": "shopping_mall",
                "description": "Shopping Mall"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 164678,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 17,
            "name": "Shopping Planet",
            "latitude": 28.5553303,
            "longitude": 77.3728714,
            "geoDistance": 2.688976526260376,
            "vicinity": "Dadri Main Rd, Sector 48, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 17,
                "name": "shopping_mall",
                "description": "Shopping Mall"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 164679,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 17,
            "name": "Shopping Planet",
            "latitude": 28.5553303,
            "longitude": 77.3728714,
            "geoDistance": 2.688976526260376,
            "vicinity": "Dadri Main Rd, Sector 48, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 17,
                "name": "shopping_mall",
                "description": "Shopping Mall"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 135891,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 17,
            "name": "Shopping Planet",
            "latitude": 28.5553303,
            "longitude": 77.3728714,
            "geoDistance": 2.688976526260376,
            "vicinity": "Dadri Main Rd, Sector 48, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 17,
                "name": "shopping_mall",
                "description": "Shopping Mall"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 135897,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 17,
            "name": "Shopping Planet",
            "latitude": 28.5553303,
            "longitude": 77.3728714,
            "geoDistance": 2.688976526260376,
            "vicinity": "Dadri Main Rd, Sector 48, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 17,
                "name": "shopping_mall",
                "description": "Shopping Mall"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 135898,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 17,
            "name": "Shopping Planet",
            "latitude": 28.5553303,
            "longitude": 77.3728714,
            "geoDistance": 2.688976526260376,
            "vicinity": "Dadri Main Rd, Sector 48, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 17,
                "name": "shopping_mall",
                "description": "Shopping Mall"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 135899,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 17,
            "name": "Shopping Planet",
            "latitude": 28.5553303,
            "longitude": 77.3728714,
            "geoDistance": 2.688976526260376,
            "vicinity": "Dadri Main Rd, Sector 48, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 17,
                "name": "shopping_mall",
                "description": "Shopping Mall"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 133466,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 17,
            "name": "Shopping Planet",
            "latitude": 28.5553303,
            "longitude": 77.3728714,
            "geoDistance": 2.688976526260376,
            "vicinity": "Dadri Main Rd, Sector 48, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 17,
                "name": "shopping_mall",
                "description": "Shopping Mall"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 133467,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 17,
            "name": "Shopping Planet",
            "latitude": 28.5553303,
            "longitude": 77.3728714,
            "geoDistance": 2.688976526260376,
            "vicinity": "Dadri Main Rd, Sector 48, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 17,
                "name": "shopping_mall",
                "description": "Shopping Mall"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 133468,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 17,
            "name": "Shopping Planet",
            "latitude": 28.5553303,
            "longitude": 77.3728714,
            "geoDistance": 2.688976526260376,
            "vicinity": "Dadri Main Rd, Sector 48, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 17,
                "name": "shopping_mall",
                "description": "Shopping Mall"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 134233,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 17,
            "name": "Shopping Planet",
            "latitude": 28.5553303,
            "longitude": 77.3728714,
            "geoDistance": 2.688976526260376,
            "vicinity": "Dadri Main Rd, Sector 48, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 17,
                "name": "shopping_mall",
                "description": "Shopping Mall"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 134234,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 17,
            "name": "Shopping Planet",
            "latitude": 28.5553303,
            "longitude": 77.3728714,
            "geoDistance": 2.688976526260376,
            "vicinity": "Dadri Main Rd, Sector 48, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 17,
                "name": "shopping_mall",
                "description": "Shopping Mall"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 134235,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 17,
            "name": "Shopping Planet",
            "latitude": 28.5553303,
            "longitude": 77.3728714,
            "geoDistance": 2.688976526260376,
            "vicinity": "Dadri Main Rd, Sector 48, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 17,
                "name": "shopping_mall",
                "description": "Shopping Mall"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 123674,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 17,
            "name": "Shopping Planet",
            "latitude": 28.5553303,
            "longitude": 77.3728714,
            "geoDistance": 2.688976526260376,
            "vicinity": "Dadri Main Rd, Sector 48, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 17,
                "name": "shopping_mall",
                "description": "Shopping Mall"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 123675,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 17,
            "name": "Shopping Planet",
            "latitude": 28.5553303,
            "longitude": 77.3728714,
            "geoDistance": 2.688976526260376,
            "vicinity": "Dadri Main Rd, Sector 48, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 17,
                "name": "shopping_mall",
                "description": "Shopping Mall"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 123676,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 17,
            "name": "Shopping Planet",
            "latitude": 28.5553303,
            "longitude": 77.3728714,
            "geoDistance": 2.688976526260376,
            "vicinity": "Dadri Main Rd, Sector 48, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 17,
                "name": "shopping_mall",
                "description": "Shopping Mall"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 124383,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 17,
            "name": "Shopping Planet",
            "latitude": 28.5553303,
            "longitude": 77.3728714,
            "geoDistance": 2.688976526260376,
            "vicinity": "Dadri Main Rd, Sector 48, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 17,
                "name": "shopping_mall",
                "description": "Shopping Mall"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 124384,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 17,
            "name": "Shopping Planet",
            "latitude": 28.5553303,
            "longitude": 77.3728714,
            "geoDistance": 2.688976526260376,
            "vicinity": "Dadri Main Rd, Sector 48, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 17,
                "name": "shopping_mall",
                "description": "Shopping Mall"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 124385,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 17,
            "name": "Shopping Planet",
            "latitude": 28.5553303,
            "longitude": 77.3728714,
            "geoDistance": 2.688976526260376,
            "vicinity": "Dadri Main Rd, Sector 48, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 17,
                "name": "shopping_mall",
                "description": "Shopping Mall"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 291,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 6,
            "name": "Indraprastha Gas Limited",
            "latitude": 28.5960522,
            "longitude": 77.3727264,
            "geoDistance": 2.690690755844116,
            "vicinity": "Sector 71, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 6,
                "name": "gas_station",
                "description": "Petrol Pump"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 83806,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 16,
            "name": "Sector 41 Park",
            "latitude": 28.5621986,
            "longitude": 77.364563,
            "geoDistance": 2.7118709087371826,
            "vicinity": "Block J Aghapur Rd, J Block, Sector 41, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 1,
            "localityAmenityTypes": {
                "id": 16,
                "name": "park",
                "description": "Park"
            },
            "rating": 5,
            "futureInfrastructure": false
        }, {
            "id": 83858,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 16,
            "name": "Park of Serenity",
            "latitude": 28.5754089,
            "longitude": 77.3598022,
            "geoDistance": 2.721736192703247,
            "vicinity": "Block A, Sector 51, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 1,
            "localityAmenityTypes": {
                "id": 16,
                "name": "park",
                "description": "Park"
            },
            "rating": 5,
            "futureInfrastructure": false
        }, {
            "id": 143745,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Noida International Public School",
            "latitude": 28.5989151,
            "longitude": 77.3968811,
            "geoDistance": 2.7312018871307373,
            "vicinity": "Garhi Chaukhandi, Sector 121, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 177828,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 8,
            "name": "Sector 49 Bus Stop",
            "latitude": 28.5607014,
            "longitude": 77.3655319,
            "geoDistance": 2.732156753540039,
            "vicinity": "Dadri Main Rd, A block, Sector 49, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 52204,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 8,
            "name": "Sector 49 Bus Stop",
            "latitude": 28.5607014,
            "longitude": 77.3655319,
            "geoDistance": 2.732156753540039,
            "vicinity": "Dadri Road, Block A, Sector 49, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 109059,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 8,
            "name": "Sector 49 Bus Stop",
            "latitude": 28.5607014,
            "longitude": 77.3655319,
            "geoDistance": 2.732156753540039,
            "vicinity": "Dadri Main Rd, A block, Sector 49, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 106377,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 8,
            "name": "Sector 49 Bus Stop",
            "latitude": 28.5607014,
            "longitude": 77.3655319,
            "geoDistance": 2.732156753540039,
            "vicinity": "Dadri Main Rd, A block, Sector 49, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 116604,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 8,
            "name": "Sector 49 Bus Stop",
            "latitude": 28.5607014,
            "longitude": 77.3655319,
            "geoDistance": 2.732156753540039,
            "vicinity": "Dadri Main Rd, A block, Sector 49, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 158386,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 8,
            "name": "Sector 49 Bus Stop",
            "latitude": 28.5607014,
            "longitude": 77.3655319,
            "geoDistance": 2.732156753540039,
            "vicinity": "Dadri Main Rd, A block, Sector 49, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 164625,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 8,
            "name": "Sector 49 Bus Stop",
            "latitude": 28.5607014,
            "longitude": 77.3655319,
            "geoDistance": 2.732156753540039,
            "vicinity": "Dadri Main Rd, A block, Sector 49, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 135913,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 8,
            "name": "Sector 49 Bus Stop",
            "latitude": 28.5607014,
            "longitude": 77.3655319,
            "geoDistance": 2.732156753540039,
            "vicinity": "Dadri Main Rd, A block, Sector 49, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 133514,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 8,
            "name": "Sector 49 Bus Stop",
            "latitude": 28.5607014,
            "longitude": 77.3655319,
            "geoDistance": 2.732156753540039,
            "vicinity": "Dadri Main Rd, A block, Sector 49, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 134211,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 8,
            "name": "Sector 49 Bus Stop",
            "latitude": 28.5607014,
            "longitude": 77.3655319,
            "geoDistance": 2.732156753540039,
            "vicinity": "Dadri Main Rd, A block, Sector 49, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 123665,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 8,
            "name": "Sector 49 Bus Stop",
            "latitude": 28.5607014,
            "longitude": 77.3655319,
            "geoDistance": 2.732156753540039,
            "vicinity": "Dadri Main Rd, A block, Sector 49, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 124417,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 8,
            "name": "Sector 49 Bus Stop",
            "latitude": 28.5607014,
            "longitude": 77.3655319,
            "geoDistance": 2.732156753540039,
            "vicinity": "Dadri Main Rd, A block, Sector 49, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 83859,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 16,
            "name": "HIMGIRI SOCIETY PARK",
            "latitude": 28.5831032,
            "longitude": 77.3608856,
            "geoDistance": 2.7413620948791504,
            "vicinity": "HIMGIRI, Sector 34, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 1,
            "localityAmenityTypes": {
                "id": 16,
                "name": "park",
                "description": "Park"
            },
            "rating": 5,
            "futureInfrastructure": false
        }, {
            "id": 64506,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 5,
            "name": "Om Bikaner Sweets & Catters",
            "latitude": 28.5867825,
            "longitude": 77.3625488,
            "geoDistance": 2.7441868782043457,
            "vicinity": "Sector 53, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 5,
                "name": "restaurant",
                "description": "Restaurant"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 80236,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 18,
            "name": "Indraj & Sons",
            "latitude": 28.5872421,
            "longitude": 77.3628082,
            "geoDistance": 2.7451045513153076,
            "vicinity": "Main Road, Gijhor, Block E, Sector 53, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 18,
                "name": "grocery_or_supermarket",
                "description": "Grocery or Supermarket"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 16836,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 14,
            "name": "Peach Blossom School",
            "latitude": 28.5959187,
            "longitude": 77.3712921,
            "geoDistance": 2.7571470737457275,
            "vicinity": "E-6/1, Sector 61, Noida , Uttar Pradesh, 201301, Block E, Sector 61, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 2,
            "localityAmenityTypes": {
                "id": 14,
                "name": "play_school",
                "description": "Play School"
            },
            "rating": 4,
            "futureInfrastructure": false
        }, {
            "id": 72497,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 5,
            "name": "Noodle Point",
            "latitude": 28.5916882,
            "longitude": 77.3659058,
            "geoDistance": 2.7692811489105225,
            "vicinity": "Gijhore, Sector 53, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 5,
                "name": "restaurant",
                "description": "Restaurant"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 147995,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Holistic Homeopathy",
            "latitude": 28.5639858,
            "longitude": 77.362587,
            "geoDistance": 2.7756781578063965,
            "vicinity": "B- 157, Sector- 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 157491,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Holistic Homeopathy",
            "latitude": 28.5639858,
            "longitude": 77.362587,
            "geoDistance": 2.7756781578063965,
            "vicinity": "B- 157, Sector- 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 109097,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 2,
            "name": "Holistic Homeopathy",
            "latitude": 28.5639858,
            "longitude": 77.362587,
            "geoDistance": 2.7756781578063965,
            "vicinity": "B- 157, Sector- 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 113787,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Holistic Homeopathy",
            "latitude": 28.5639858,
            "longitude": 77.362587,
            "geoDistance": 2.7756781578063965,
            "vicinity": "B- 157, Sector- 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 115771,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Holistic Homeopathy",
            "latitude": 28.5639858,
            "longitude": 77.362587,
            "geoDistance": 2.7756781578063965,
            "vicinity": "B- 157, Sector- 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 111926,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Holistic Homeopathy",
            "latitude": 28.5639858,
            "longitude": 77.362587,
            "geoDistance": 2.7756781578063965,
            "vicinity": "B- 157, Sector- 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 113755,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Holistic Homeopathy",
            "latitude": 28.5639858,
            "longitude": 77.362587,
            "geoDistance": 2.7756781578063965,
            "vicinity": "B- 157, Sector- 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 106389,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Holistic Homeopathy",
            "latitude": 28.5639858,
            "longitude": 77.362587,
            "geoDistance": 2.7756781578063965,
            "vicinity": "B- 157, Sector- 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 115795,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Holistic Homeopathy",
            "latitude": 28.5639858,
            "longitude": 77.362587,
            "geoDistance": 2.7756781578063965,
            "vicinity": "B- 157, Sector- 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 116648,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Holistic Homeopathy",
            "latitude": 28.5639858,
            "longitude": 77.362587,
            "geoDistance": 2.7756781578063965,
            "vicinity": "B- 157, Sector- 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 175578,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Holistic Homeopathy",
            "latitude": 28.5639858,
            "longitude": 77.362587,
            "geoDistance": 2.7756781578063965,
            "vicinity": "B- 157, Sector- 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 158374,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 2,
            "name": "Holistic Homeopathy",
            "latitude": 28.5639858,
            "longitude": 77.362587,
            "geoDistance": 2.7756781578063965,
            "vicinity": "B- 157, Sector- 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 164669,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 2,
            "name": "Holistic Homeopathy",
            "latitude": 28.5639858,
            "longitude": 77.362587,
            "geoDistance": 2.7756781578063965,
            "vicinity": "B- 157, Sector- 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 135957,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Holistic Homeopathy",
            "latitude": 28.5639858,
            "longitude": 77.362587,
            "geoDistance": 2.7756781578063965,
            "vicinity": "B- 157, Sector- 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 133528,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 2,
            "name": "Holistic Homeopathy",
            "latitude": 28.5639858,
            "longitude": 77.362587,
            "geoDistance": 2.7756781578063965,
            "vicinity": "B- 157, Sector- 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 134199,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 2,
            "name": "Holistic Homeopathy",
            "latitude": 28.5639858,
            "longitude": 77.362587,
            "geoDistance": 2.7756781578063965,
            "vicinity": "B- 157, Sector- 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 123721,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 2,
            "name": "Holistic Homeopathy",
            "latitude": 28.5639858,
            "longitude": 77.362587,
            "geoDistance": 2.7756781578063965,
            "vicinity": "B- 157, Sector- 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 124443,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Holistic Homeopathy",
            "latitude": 28.5639858,
            "longitude": 77.362587,
            "geoDistance": 2.7756781578063965,
            "vicinity": "B- 157, Sector- 41, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 47658,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Sri Sri Ravi Shankar Vidya Mandir, Sector 48, Noid",
            "latitude": 28.5579224,
            "longitude": 77.3677063,
            "geoDistance": 2.778285503387451,
            "website": "http://ssrvm.org/",
            "vicinity": "B-182/A, Near Samvedna Hospital,, Sector 48,, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":4.7}",
            "priority": 3,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 3,
            "futureInfrastructure": false
        }, {
            "id": 143754,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Addi Charitable Poly Clinic",
            "latitude": 28.5999718,
            "longitude": 77.3945847,
            "geoDistance": 2.7796578407287598,
            "vicinity": "Garhi Chaukhandi, Chaukhandi, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 143755,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Addi Charitable Poly Clinic",
            "latitude": 28.5999718,
            "longitude": 77.3945847,
            "geoDistance": 2.7796578407287598,
            "vicinity": "Garhi Chaukhandi, Chaukhandi, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 143756,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Addi Charitable Poly Clinic",
            "latitude": 28.5999718,
            "longitude": 77.3945847,
            "geoDistance": 2.7796578407287598,
            "vicinity": "Garhi Chaukhandi, Chaukhandi, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 143757,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Addi Charitable Poly Clinic",
            "latitude": 28.5999718,
            "longitude": 77.3945847,
            "geoDistance": 2.7796578407287598,
            "vicinity": "Garhi Chaukhandi, Chaukhandi, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 143758,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Addi Charitable Poly Clinic",
            "latitude": 28.5999718,
            "longitude": 77.3945847,
            "geoDistance": 2.7796578407287598,
            "vicinity": "Garhi Chaukhandi, Chaukhandi, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 143759,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Addi Charitable Poly Clinic",
            "latitude": 28.5999718,
            "longitude": 77.3945847,
            "geoDistance": 2.7796578407287598,
            "vicinity": "Garhi Chaukhandi, Chaukhandi, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 143760,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Addi Charitable Poly Clinic",
            "latitude": 28.5999718,
            "longitude": 77.3945847,
            "geoDistance": 2.7796578407287598,
            "vicinity": "Garhi Chaukhandi, Chaukhandi, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 143761,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Addi Charitable Poly Clinic",
            "latitude": 28.5999718,
            "longitude": 77.3945847,
            "geoDistance": 2.7796578407287598,
            "vicinity": "Garhi Chaukhandi, Chaukhandi, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 143762,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Addi Charitable Poly Clinic",
            "latitude": 28.5999718,
            "longitude": 77.3945847,
            "geoDistance": 2.7796578407287598,
            "vicinity": "Garhi Chaukhandi, Chaukhandi, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 143768,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Addi Charitable Poly Clinic",
            "latitude": 28.5999718,
            "longitude": 77.3945847,
            "geoDistance": 2.7796578407287598,
            "vicinity": "Garhi Chaukhandi, Chaukhandi, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 83827,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 16,
            "name": "Local Public Park",
            "latitude": 28.5979347,
            "longitude": 77.3746109,
            "geoDistance": 2.7797658443450928,
            "vicinity": "B Block, Sector 71, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 1,
            "localityAmenityTypes": {
                "id": 16,
                "name": "park",
                "description": "Park"
            },
            "rating": 5,
            "futureInfrastructure": false
        }, {
            "id": 55253,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 3,
            "name": "Mahamedha urban bank",
            "latitude": 28.561306,
            "longitude": 77.3643951,
            "geoDistance": 2.781461715698242,
            "vicinity": "J Block, Sector 41, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 3,
                "name": "bank",
                "description": "Bank"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 231843,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Samvedana MultiSpeciality Hospital",
            "latitude": 28.5573578,
            "longitude": 77.3682785,
            "geoDistance": 2.7851181030273438,
            "vicinity": "Block B, Sector 48, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 31759,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "Axis Bank ATM",
            "latitude": 28.5572948,
            "longitude": 77.3682556,
            "geoDistance": 2.7917730808258057,
            "vicinity": "Sector 48, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 177838,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "Axis Bank ATM",
            "latitude": 28.5572948,
            "longitude": 77.3682556,
            "geoDistance": 2.7917730808258057,
            "vicinity": "Sector 48, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 157508,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "Axis Bank ATM",
            "latitude": 28.5572948,
            "longitude": 77.3682556,
            "geoDistance": 2.7917730808258057,
            "vicinity": "Sector 48, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 109114,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Axis Bank ATM",
            "latitude": 28.5572948,
            "longitude": 77.3682556,
            "geoDistance": 2.7917730808258057,
            "vicinity": "Sector 48, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 106418,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "Axis Bank ATM",
            "latitude": 28.5572948,
            "longitude": 77.3682556,
            "geoDistance": 2.7917730808258057,
            "vicinity": "Sector 48, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 116623,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "Axis Bank ATM",
            "latitude": 28.5572948,
            "longitude": 77.3682556,
            "geoDistance": 2.7917730808258057,
            "vicinity": "Sector 48, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 175599,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "Axis Bank ATM",
            "latitude": 28.5572948,
            "longitude": 77.3682556,
            "geoDistance": 2.7917730808258057,
            "vicinity": "Sector 48, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 158362,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Axis Bank ATM",
            "latitude": 28.5572948,
            "longitude": 77.3682556,
            "geoDistance": 2.7917730808258057,
            "vicinity": "Sector 48, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 164655,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Axis Bank ATM",
            "latitude": 28.5572948,
            "longitude": 77.3682556,
            "geoDistance": 2.7917730808258057,
            "vicinity": "Sector 48, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 135943,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "Axis Bank ATM",
            "latitude": 28.5572948,
            "longitude": 77.3682556,
            "geoDistance": 2.7917730808258057,
            "vicinity": "Sector 48, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 133487,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Axis Bank ATM",
            "latitude": 28.5572948,
            "longitude": 77.3682556,
            "geoDistance": 2.7917730808258057,
            "vicinity": "Sector 48, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 134187,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Axis Bank ATM",
            "latitude": 28.5572948,
            "longitude": 77.3682556,
            "geoDistance": 2.7917730808258057,
            "vicinity": "Sector 48, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 123695,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "Axis Bank ATM",
            "latitude": 28.5572948,
            "longitude": 77.3682556,
            "geoDistance": 2.7917730808258057,
            "vicinity": "Sector 48, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 124404,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "Axis Bank ATM",
            "latitude": 28.5572948,
            "longitude": 77.3682556,
            "geoDistance": 2.7917730808258057,
            "vicinity": "Sector 48, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 38006,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Samvedana Multispeciality Hospital",
            "latitude": 28.5573444,
            "longitude": 77.368187,
            "geoDistance": 2.79227352142334,
            "website": "http://samvedana.org/",
            "vicinity": "B-206 A, Block B, Sector 48, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 4,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 2,
            "futureInfrastructure": false
        }, {
            "id": 80233,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 18,
            "name": "Ambey General Store",
            "latitude": 28.5837116,
            "longitude": 77.3605499,
            "geoDistance": 2.7935237884521484,
            "vicinity": "21, B-13 Market, Captain Shashi Kant Marg, Block B, Sector 34, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 18,
                "name": "grocery_or_supermarket",
                "description": "Grocery or Supermarket"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 58000,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "HDFC Bank ATM",
            "latitude": 28.5838947,
            "longitude": 77.360611,
            "geoDistance": 2.7943408489227295,
            "vicinity": "B-13 MARKET,NEAR MOTHER DAIRY, Sector 34, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 704,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "IDBI ATM",
            "latitude": 28.5838909,
            "longitude": 77.3605804,
            "geoDistance": 2.797043800354004,
            "vicinity": "Sector 34, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 41202,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 5,
            "name": "Touch Of China",
            "latitude": 28.5837879,
            "longitude": 77.360527,
            "geoDistance": 2.798346519470215,
            "vicinity": "A-Block Market, B-13, Sector 34, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 5,
                "name": "restaurant",
                "description": "Restaurant"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 40314,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "ICICI Bank ATM",
            "latitude": 28.5838814,
            "longitude": 77.3605042,
            "geoDistance": 2.803766965866089,
            "vicinity": "Sector 34, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 80207,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 16,
            "name": "E-Block Park",
            "latitude": 28.5954075,
            "longitude": 77.3696747,
            "geoDistance": 2.807136058807373,
            "vicinity": "Besides Parsvnath Gardenia and Krishna Apra Residency, Gol Chakkar, Block E, Sector 61, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 1,
            "localityAmenityTypes": {
                "id": 16,
                "name": "park",
                "description": "Park"
            },
            "rating": 5,
            "futureInfrastructure": false
        }, {
            "id": 212325,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 2,
            "name": "Manas Hospital - A Unit of Sharma Medical Centre Pvt. Ltd.",
            "latitude": 28.5864754,
            "longitude": 77.3616409,
            "geoDistance": 2.8090176582336426,
            "vicinity": "Block A, Sector 34, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 177839,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "HDFC Bank ATM",
            "latitude": 28.5578861,
            "longitude": 77.3672562,
            "geoDistance": 2.8121821880340576,
            "vicinity": "Dadri Main Rd, Block A, Sector 48, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 157510,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "HDFC Bank ATM",
            "latitude": 28.5578861,
            "longitude": 77.3672562,
            "geoDistance": 2.8121821880340576,
            "vicinity": "Dadri Main Rd, Block A, Sector 48, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 109116,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "HDFC Bank ATM",
            "latitude": 28.5578861,
            "longitude": 77.3672562,
            "geoDistance": 2.8121821880340576,
            "vicinity": "Dadri Main Rd, Block A, Sector 48, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 106420,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "HDFC Bank ATM",
            "latitude": 28.5578861,
            "longitude": 77.3672562,
            "geoDistance": 2.8121821880340576,
            "vicinity": "Dadri Main Rd, Block A, Sector 48, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 116625,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "HDFC Bank ATM",
            "latitude": 28.5578861,
            "longitude": 77.3672562,
            "geoDistance": 2.8121821880340576,
            "vicinity": "Dadri Main Rd, Block A, Sector 48, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 158364,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "HDFC Bank ATM",
            "latitude": 28.5578861,
            "longitude": 77.3672562,
            "geoDistance": 2.8121821880340576,
            "vicinity": "Dadri Main Rd, Block A, Sector 48, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 164657,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "HDFC Bank ATM",
            "latitude": 28.5578861,
            "longitude": 77.3672562,
            "geoDistance": 2.8121821880340576,
            "vicinity": "Dadri Main Rd, Block A, Sector 48, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 135945,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "HDFC Bank ATM",
            "latitude": 28.5578861,
            "longitude": 77.3672562,
            "geoDistance": 2.8121821880340576,
            "vicinity": "Dadri Main Rd, Block A, Sector 48, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 133489,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "HDFC Bank ATM",
            "latitude": 28.5578861,
            "longitude": 77.3672562,
            "geoDistance": 2.8121821880340576,
            "vicinity": "Dadri Main Rd, Block A, Sector 48, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 134189,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "HDFC Bank ATM",
            "latitude": 28.5578861,
            "longitude": 77.3672562,
            "geoDistance": 2.8121821880340576,
            "vicinity": "Dadri Main Rd, Block A, Sector 48, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 123697,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "HDFC Bank ATM",
            "latitude": 28.5578861,
            "longitude": 77.3672562,
            "geoDistance": 2.8121821880340576,
            "vicinity": "Dadri Main Rd, Block A, Sector 48, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 124406,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "HDFC Bank ATM",
            "latitude": 28.5578861,
            "longitude": 77.3672562,
            "geoDistance": 2.8121821880340576,
            "vicinity": "Dadri Main Rd, Block A, Sector 48, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 76403,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "HDFC Bank ATM",
            "latitude": 28.557888,
            "longitude": 77.3672409,
            "geoDistance": 2.813098430633545,
            "vicinity": "Dadri Road, Block A, Sector 48, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 220,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 3,
            "name": "HDFC Bank",
            "latitude": 28.5578899,
            "longitude": 77.3672104,
            "geoDistance": 2.815025806427002,
            "vicinity": "A-182/2, Sector 48, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 3,
                "name": "bank",
                "description": "Bank"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 147985,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "HDFC Bank ATM",
            "latitude": 28.5838547,
            "longitude": 77.3603134,
            "geoDistance": 2.820410966873169,
            "vicinity": "Noida, Sector 52, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 147986,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "HDFC Bank ATM",
            "latitude": 28.5838547,
            "longitude": 77.3603134,
            "geoDistance": 2.820410966873169,
            "vicinity": "Noida, Sector 52, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 147987,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "HDFC Bank ATM",
            "latitude": 28.5838547,
            "longitude": 77.3603134,
            "geoDistance": 2.820410966873169,
            "vicinity": "Noida, Sector 52, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 148012,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "HDFC Bank ATM",
            "latitude": 28.5838547,
            "longitude": 77.3603134,
            "geoDistance": 2.820410966873169,
            "vicinity": "Noida, Sector 52, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 157500,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "HDFC Bank ATM",
            "latitude": 28.5838547,
            "longitude": 77.3603134,
            "geoDistance": 2.820410966873169,
            "vicinity": "Noida, Sector 52, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 109106,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "HDFC Bank ATM",
            "latitude": 28.5838547,
            "longitude": 77.3603134,
            "geoDistance": 2.820410966873169,
            "vicinity": "Noida, Sector 52, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 113779,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "HDFC Bank ATM",
            "latitude": 28.5838547,
            "longitude": 77.3603134,
            "geoDistance": 2.820410966873169,
            "vicinity": "Noida, Sector 52, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 111930,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "HDFC Bank ATM",
            "latitude": 28.5838547,
            "longitude": 77.3603134,
            "geoDistance": 2.820410966873169,
            "vicinity": "Noida, Sector 52, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 113747,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "HDFC Bank ATM",
            "latitude": 28.5838547,
            "longitude": 77.3603134,
            "geoDistance": 2.820410966873169,
            "vicinity": "Noida, Sector 52, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 106410,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "HDFC Bank ATM",
            "latitude": 28.5838547,
            "longitude": 77.3603134,
            "geoDistance": 2.820410966873169,
            "vicinity": "Noida, Sector 52, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 116614,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "HDFC Bank ATM",
            "latitude": 28.5838547,
            "longitude": 77.3603134,
            "geoDistance": 2.820410966873169,
            "vicinity": "Noida, Sector 52, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 116615,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "HDFC Bank ATM",
            "latitude": 28.5838547,
            "longitude": 77.3603134,
            "geoDistance": 2.820410966873169,
            "vicinity": "Noida, Sector 52, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 175590,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "HDFC Bank ATM",
            "latitude": 28.5838547,
            "longitude": 77.3603134,
            "geoDistance": 2.820410966873169,
            "vicinity": "Noida, Sector 52, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 158354,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "HDFC Bank ATM",
            "latitude": 28.5838547,
            "longitude": 77.3603134,
            "geoDistance": 2.820410966873169,
            "vicinity": "Noida, Sector 52, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 164647,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "HDFC Bank ATM",
            "latitude": 28.5838547,
            "longitude": 77.3603134,
            "geoDistance": 2.820410966873169,
            "vicinity": "Noida, Sector 52, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 165449,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "HDFC Bank ATM",
            "latitude": 28.5838547,
            "longitude": 77.3603134,
            "geoDistance": 2.820410966873169,
            "vicinity": "Noida, Sector 52, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 135935,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "HDFC Bank ATM",
            "latitude": 28.5838547,
            "longitude": 77.3603134,
            "geoDistance": 2.820410966873169,
            "vicinity": "Noida, Sector 52, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 125792,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "HDFC Bank ATM",
            "latitude": 28.5838547,
            "longitude": 77.3603134,
            "geoDistance": 2.820410966873169,
            "vicinity": "Noida, Sector 52, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 125823,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "HDFC Bank ATM",
            "latitude": 28.5838547,
            "longitude": 77.3603134,
            "geoDistance": 2.820410966873169,
            "vicinity": "Noida, Sector 52, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 133479,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "HDFC Bank ATM",
            "latitude": 28.5838547,
            "longitude": 77.3603134,
            "geoDistance": 2.820410966873169,
            "vicinity": "Noida, Sector 52, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 134179,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "HDFC Bank ATM",
            "latitude": 28.5838547,
            "longitude": 77.3603134,
            "geoDistance": 2.820410966873169,
            "vicinity": "Noida, Sector 52, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 123687,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "HDFC Bank ATM",
            "latitude": 28.5838547,
            "longitude": 77.3603134,
            "geoDistance": 2.820410966873169,
            "vicinity": "Noida, Sector 52, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 124396,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "HDFC Bank ATM",
            "latitude": 28.5838547,
            "longitude": 77.3603134,
            "geoDistance": 2.820410966873169,
            "vicinity": "Noida, Sector 52, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 147991,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Shivalik Medical Center",
            "latitude": 28.5865097,
            "longitude": 77.3613586,
            "geoDistance": 2.8356823921203613,
            "vicinity": "Block A, Sector 34, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 147992,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Shivalik Medical Center",
            "latitude": 28.5865097,
            "longitude": 77.3613586,
            "geoDistance": 2.8356823921203613,
            "vicinity": "Block A, Sector 34, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 148007,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Shivalik Medical Center",
            "latitude": 28.5865097,
            "longitude": 77.3613586,
            "geoDistance": 2.8356823921203613,
            "vicinity": "Block A, Sector 34, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 157488,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Shivalik Medical Center",
            "latitude": 28.5865097,
            "longitude": 77.3613586,
            "geoDistance": 2.8356823921203613,
            "vicinity": "Block A, Sector 34, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 109094,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 2,
            "name": "Shivalik Medical Center",
            "latitude": 28.5865097,
            "longitude": 77.3613586,
            "geoDistance": 2.8356823921203613,
            "vicinity": "Block A, Sector 34, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 113784,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Shivalik Medical Center",
            "latitude": 28.5865097,
            "longitude": 77.3613586,
            "geoDistance": 2.8356823921203613,
            "vicinity": "Block A, Sector 34, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 111923,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Shivalik Medical Center",
            "latitude": 28.5865097,
            "longitude": 77.3613586,
            "geoDistance": 2.8356823921203613,
            "vicinity": "Block A, Sector 34, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 113752,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Shivalik Medical Center",
            "latitude": 28.5865097,
            "longitude": 77.3613586,
            "geoDistance": 2.8356823921203613,
            "vicinity": "Block A, Sector 34, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 106386,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Shivalik Medical Center",
            "latitude": 28.5865097,
            "longitude": 77.3613586,
            "geoDistance": 2.8356823921203613,
            "vicinity": "Block A, Sector 34, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 175574,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Shivalik Medical Center",
            "latitude": 28.5865097,
            "longitude": 77.3613586,
            "geoDistance": 2.8356823921203613,
            "vicinity": "Block A, Sector 34, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 158371,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 2,
            "name": "Shivalik Medical Center",
            "latitude": 28.5865097,
            "longitude": 77.3613586,
            "geoDistance": 2.8356823921203613,
            "vicinity": "Block A, Sector 34, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 164666,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 2,
            "name": "Shivalik Medical Center",
            "latitude": 28.5865097,
            "longitude": 77.3613586,
            "geoDistance": 2.8356823921203613,
            "vicinity": "Block A, Sector 34, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 165458,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Shivalik Medical Center",
            "latitude": 28.5865097,
            "longitude": 77.3613586,
            "geoDistance": 2.8356823921203613,
            "vicinity": "Block A, Sector 34, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 135954,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Shivalik Medical Center",
            "latitude": 28.5865097,
            "longitude": 77.3613586,
            "geoDistance": 2.8356823921203613,
            "vicinity": "Block A, Sector 34, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 125800,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Shivalik Medical Center",
            "latitude": 28.5865097,
            "longitude": 77.3613586,
            "geoDistance": 2.8356823921203613,
            "vicinity": "Block A, Sector 34, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 125831,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Shivalik Medical Center",
            "latitude": 28.5865097,
            "longitude": 77.3613586,
            "geoDistance": 2.8356823921203613,
            "vicinity": "Block A, Sector 34, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 133525,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 2,
            "name": "Shivalik Medical Center",
            "latitude": 28.5865097,
            "longitude": 77.3613586,
            "geoDistance": 2.8356823921203613,
            "vicinity": "Block A, Sector 34, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 134196,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 2,
            "name": "Shivalik Medical Center",
            "latitude": 28.5865097,
            "longitude": 77.3613586,
            "geoDistance": 2.8356823921203613,
            "vicinity": "Block A, Sector 34, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 123718,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 2,
            "name": "Shivalik Medical Center",
            "latitude": 28.5865097,
            "longitude": 77.3613586,
            "geoDistance": 2.8356823921203613,
            "vicinity": "Block A, Sector 34, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 124440,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Shivalik Medical Center",
            "latitude": 28.5865097,
            "longitude": 77.3613586,
            "geoDistance": 2.8356823921203613,
            "vicinity": "Block A, Sector 34, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 83830,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 16,
            "name": "Triphala Park",
            "latitude": 28.5950451,
            "longitude": 77.3686905,
            "geoDistance": 2.837679862976074,
            "vicinity": "Block E, Sector 61, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 1,
            "localityAmenityTypes": {
                "id": 16,
                "name": "park",
                "description": "Park"
            },
            "rating": 5,
            "futureInfrastructure": false
        }, {
            "id": 33043,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "PNB ATM",
            "latitude": 28.5576115,
            "longitude": 77.3668518,
            "geoDistance": 2.8617920875549316,
            "vicinity": "Sector 48, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 177837,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "PNB ATM",
            "latitude": 28.5576115,
            "longitude": 77.3668518,
            "geoDistance": 2.8617920875549316,
            "vicinity": "Sector 48, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 157505,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "PNB ATM",
            "latitude": 28.5576115,
            "longitude": 77.3668518,
            "geoDistance": 2.8617920875549316,
            "vicinity": "Sector 48, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 109111,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "PNB ATM",
            "latitude": 28.5576115,
            "longitude": 77.3668518,
            "geoDistance": 2.8617920875549316,
            "vicinity": "Sector 48, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 111936,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "PNB ATM",
            "latitude": 28.5576115,
            "longitude": 77.3668518,
            "geoDistance": 2.8617920875549316,
            "vicinity": "Sector 48, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 106415,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "PNB ATM",
            "latitude": 28.5576115,
            "longitude": 77.3668518,
            "geoDistance": 2.8617920875549316,
            "vicinity": "Sector 48, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 116620,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "PNB ATM",
            "latitude": 28.5576115,
            "longitude": 77.3668518,
            "geoDistance": 2.8617920875549316,
            "vicinity": "Sector 48, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 116621,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "PNB ATM",
            "latitude": 28.5576115,
            "longitude": 77.3668518,
            "geoDistance": 2.8617920875549316,
            "vicinity": "Sector 48, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 175596,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "PNB ATM",
            "latitude": 28.5576115,
            "longitude": 77.3668518,
            "geoDistance": 2.8617920875549316,
            "vicinity": "Sector 48, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 158359,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "PNB ATM",
            "latitude": 28.5576115,
            "longitude": 77.3668518,
            "geoDistance": 2.8617920875549316,
            "vicinity": "Sector 48, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 164652,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "PNB ATM",
            "latitude": 28.5576115,
            "longitude": 77.3668518,
            "geoDistance": 2.8617920875549316,
            "vicinity": "Sector 48, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 135940,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "PNB ATM",
            "latitude": 28.5576115,
            "longitude": 77.3668518,
            "geoDistance": 2.8617920875549316,
            "vicinity": "Sector 48, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 133484,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "PNB ATM",
            "latitude": 28.5576115,
            "longitude": 77.3668518,
            "geoDistance": 2.8617920875549316,
            "vicinity": "Sector 48, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 134184,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "PNB ATM",
            "latitude": 28.5576115,
            "longitude": 77.3668518,
            "geoDistance": 2.8617920875549316,
            "vicinity": "Sector 48, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 123692,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 4,
            "name": "PNB ATM",
            "latitude": 28.5576115,
            "longitude": 77.3668518,
            "geoDistance": 2.8617920875549316,
            "vicinity": "Sector 48, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 124401,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "PNB ATM",
            "latitude": 28.5576115,
            "longitude": 77.3668518,
            "geoDistance": 2.8617920875549316,
            "vicinity": "Sector 48, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 33510,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 6,
            "name": "Bharat Petroleum Petrol Pump",
            "latitude": 28.5769482,
            "longitude": 77.3583755,
            "geoDistance": 2.864086151123047,
            "vicinity": "Sector 35",
            "restDetails": "{\"price_level\":null,\"rating\":4.2}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 6,
                "name": "gas_station",
                "description": "Petrol Pump"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 16551,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 2,
            "name": "Samvedana Multispeciality Hospital and Pain Manage",
            "latitude": 28.5533428,
            "longitude": 77.3731384,
            "geoDistance": 2.8647899627685547,
            "website": "http://samvedana.org/",
            "vicinity": "Sector 48, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 4,
            "localityAmenityTypes": {
                "id": 2,
                "name": "hospital",
                "description": "Hospital"
            },
            "rating": 2,
            "futureInfrastructure": false
        }, {
            "id": 80194,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 18,
            "name": "City Mega Store",
            "latitude": 28.5533428,
            "longitude": 77.3731384,
            "geoDistance": 2.8647899627685547,
            "vicinity": "Block D, Sector 48, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 18,
                "name": "grocery_or_supermarket",
                "description": "Grocery or Supermarket"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 46977,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "Billabong High International School",
            "latitude": 28.5837994,
            "longitude": 77.3597183,
            "geoDistance": 2.873629093170166,
            "vicinity": "A-73, Sector 34, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 3,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 3,
            "futureInfrastructure": false
        }, {
            "id": 34615,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 3,
            "name": "State Bank of India",
            "latitude": 28.5534019,
            "longitude": 77.3725281,
            "geoDistance": 2.8891403675079346,
            "vicinity": "Sector 48, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 3,
                "name": "bank",
                "description": "Bank"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 83860,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 16,
            "name": "DHAWALGIRI SOCIETY PARK",
            "latitude": 28.5841999,
            "longitude": 77.3596725,
            "geoDistance": 2.8920798301696777,
            "vicinity": "Sector 34, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 1,
            "localityAmenityTypes": {
                "id": 16,
                "name": "park",
                "description": "Park"
            },
            "rating": 5,
            "futureInfrastructure": false
        }, {
            "id": 65684,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 4,
            "name": "Oriental Bank of Commerce Atm",
            "latitude": 28.5929489,
            "longitude": 77.3654633,
            "geoDistance": 2.8934035301208496,
            "vicinity": "C-1, Sector-61, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 4,
                "name": "atm",
                "description": "Atm"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 214031,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 8,
            "name": "Noida City Center Sector 32",
            "latitude": 28.5749874,
            "longitude": 77.3578873,
            "geoDistance": 2.909677505493164,
            "vicinity": "Block A, Sector 51, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 147966,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 8,
            "name": "Morna Bus Station",
            "latitude": 28.5766659,
            "longitude": 77.3575211,
            "geoDistance": 2.946129322052002,
            "vicinity": "Golf Marg, Morna, Sector 35, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 147967,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 8,
            "name": "Morna Bus Station",
            "latitude": 28.5766659,
            "longitude": 77.3575211,
            "geoDistance": 2.946129322052002,
            "vicinity": "Golf Marg, Morna, Sector 35, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 147974,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 8,
            "name": "Morna Bus Station",
            "latitude": 28.5766659,
            "longitude": 77.3575211,
            "geoDistance": 2.946129322052002,
            "vicinity": "Golf Marg, Morna, Sector 35, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 109056,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 8,
            "name": "Morna Bus Station",
            "latitude": 28.5766659,
            "longitude": 77.3575211,
            "geoDistance": 2.946129322052002,
            "vicinity": "Golf Marg, Morna, Sector 35, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 113776,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 8,
            "name": "Morna Bus Station",
            "latitude": 28.5766659,
            "longitude": 77.3575211,
            "geoDistance": 2.946129322052002,
            "vicinity": "Golf Marg, Morna, Sector 35, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 111920,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 8,
            "name": "Morna Bus Station",
            "latitude": 28.5766659,
            "longitude": 77.3575211,
            "geoDistance": 2.946129322052002,
            "vicinity": "Golf Marg, Morna, Sector 35, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 111921,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 8,
            "name": "Morna Bus Station",
            "latitude": 28.5766659,
            "longitude": 77.3575211,
            "geoDistance": 2.946129322052002,
            "vicinity": "Golf Marg, Morna, Sector 35, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 113759,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 8,
            "name": "Morna Bus Station",
            "latitude": 28.5766659,
            "longitude": 77.3575211,
            "geoDistance": 2.946129322052002,
            "vicinity": "Golf Marg, Morna, Sector 35, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 106374,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 8,
            "name": "Morna Bus Station",
            "latitude": 28.5766659,
            "longitude": 77.3575211,
            "geoDistance": 2.946129322052002,
            "vicinity": "Golf Marg, Morna, Sector 35, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 116601,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 8,
            "name": "Morna Bus Station",
            "latitude": 28.5766659,
            "longitude": 77.3575211,
            "geoDistance": 2.946129322052002,
            "vicinity": "Golf Marg, Morna, Sector 35, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 175582,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 8,
            "name": "Morna Bus Station",
            "latitude": 28.5766659,
            "longitude": 77.3575211,
            "geoDistance": 2.946129322052002,
            "vicinity": "Golf Marg, Morna, Sector 35, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 158383,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 8,
            "name": "Morna Bus Station",
            "latitude": 28.5766659,
            "longitude": 77.3575211,
            "geoDistance": 2.946129322052002,
            "vicinity": "Golf Marg, Morna, Sector 35, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 164622,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 8,
            "name": "Morna Bus Station",
            "latitude": 28.5766659,
            "longitude": 77.3575211,
            "geoDistance": 2.946129322052002,
            "vicinity": "Golf Marg, Morna, Sector 35, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 165469,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 8,
            "name": "Morna Bus Station",
            "latitude": 28.5766659,
            "longitude": 77.3575211,
            "geoDistance": 2.946129322052002,
            "vicinity": "Golf Marg, Morna, Sector 35, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 165470,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 8,
            "name": "Morna Bus Station",
            "latitude": 28.5766659,
            "longitude": 77.3575211,
            "geoDistance": 2.946129322052002,
            "vicinity": "Golf Marg, Morna, Sector 35, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 135910,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 8,
            "name": "Morna Bus Station",
            "latitude": 28.5766659,
            "longitude": 77.3575211,
            "geoDistance": 2.946129322052002,
            "vicinity": "Golf Marg, Morna, Sector 35, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 125804,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 8,
            "name": "Morna Bus Station",
            "latitude": 28.5766659,
            "longitude": 77.3575211,
            "geoDistance": 2.946129322052002,
            "vicinity": "Golf Marg, Morna, Sector 35, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 125835,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 8,
            "name": "Morna Bus Station",
            "latitude": 28.5766659,
            "longitude": 77.3575211,
            "geoDistance": 2.946129322052002,
            "vicinity": "Golf Marg, Morna, Sector 35, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 133511,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 8,
            "name": "Morna Bus Station",
            "latitude": 28.5766659,
            "longitude": 77.3575211,
            "geoDistance": 2.946129322052002,
            "vicinity": "Golf Marg, Morna, Sector 35, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 134208,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 8,
            "name": "Morna Bus Station",
            "latitude": 28.5766659,
            "longitude": 77.3575211,
            "geoDistance": 2.946129322052002,
            "vicinity": "Golf Marg, Morna, Sector 35, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 123662,
            "localityId": 0,
            "cityId": 88,
            "placeTypeId": 8,
            "name": "Morna Bus Station",
            "latitude": 28.5766659,
            "longitude": 77.3575211,
            "geoDistance": 2.946129322052002,
            "vicinity": "Golf Marg, Morna, Sector 35, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 124414,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 8,
            "name": "Morna Bus Station",
            "latitude": 28.5766659,
            "longitude": 77.3575211,
            "geoDistance": 2.946129322052002,
            "vicinity": "Golf Marg, Morna, Sector 35, Noida",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 8,
                "name": "bus_station",
                "description": "Bus Station"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 90111,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 17,
            "name": "Shopprix Mall",
            "latitude": 28.5960884,
            "longitude": 77.3683319,
            "geoDistance": 2.9486589431762695,
            "vicinity": "Sector 61 Noida Uttar Pradesh",
            "priority": 1,
            "localityAmenityTypes": {
                "id": 17,
                "name": "shopping_mall",
                "description": "Shopping Mall"
            },
            "rating": 5,
            "futureInfrastructure": false
        }, {
            "id": 24565,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 5,
            "name": "Abhay Modi",
            "latitude": 28.5960884,
            "longitude": 77.3683319,
            "geoDistance": 2.9486589431762695,
            "vicinity": "Sector 61, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 5,
                "name": "restaurant",
                "description": "Restaurant"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 5085,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 5,
            "name": "Chinese Bowl",
            "latitude": 28.5960884,
            "longitude": 77.3683319,
            "geoDistance": 2.9486589431762695,
            "vicinity": "Shoprix Mall,Basement,Shop No.104,Block C, Sector 61, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 5,
                "name": "restaurant",
                "description": "Restaurant"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 74823,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 5,
            "name": "Lemonz",
            "latitude": 28.5960884,
            "longitude": 77.3683319,
            "geoDistance": 2.9486589431762695,
            "vicinity": "151, Market, Greater Noida, Sector 61, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 5,
                "name": "restaurant",
                "description": "Restaurant"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 80481,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 18,
            "name": "Wherspa Ashwin",
            "latitude": 28.5960884,
            "longitude": 77.3683319,
            "geoDistance": 2.9486589431762695,
            "vicinity": "Block E, Sector 61, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 18,
                "name": "grocery_or_supermarket",
                "description": "Grocery or Supermarket"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 6733,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 6,
            "name": "CNG Filling Station",
            "latitude": 28.5979767,
            "longitude": 77.4042664,
            "geoDistance": 2.9578237533569336,
            "vicinity": "Sector 122, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 5,
            "localityAmenityTypes": {
                "id": 6,
                "name": "gas_station",
                "description": "Petrol Pump"
            },
            "rating": 1,
            "futureInfrastructure": false
        }, {
            "id": 83861,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 16,
            "name": "ARAVALI PARK",
            "latitude": 28.5836182,
            "longitude": 77.3587341,
            "geoDistance": 2.9590518474578857,
            "vicinity": "Sector 34, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 1,
            "localityAmenityTypes": {
                "id": 16,
                "name": "park",
                "description": "Park"
            },
            "rating": 5,
            "futureInfrastructure": false
        }, {
            "id": 45752,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 1,
            "name": "The Millennium School",
            "latitude": 28.5671368,
            "longitude": 77.3589096,
            "geoDistance": 2.966606378555298,
            "vicinity": "D-108, Sector 41, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 2,
            "localityAmenityTypes": {
                "id": 1,
                "name": "school",
                "description": "School"
            },
            "rating": 4,
            "futureInfrastructure": false
        }, {
            "id": 83802,
            "localityId": 0,
            "cityId": 20,
            "placeTypeId": 16,
            "name": "Park",
            "latitude": 28.5538387,
            "longitude": 77.3702393,
            "geoDistance": 2.9697604179382324,
            "vicinity": "Sector 48, Noida",
            "restDetails": "{\"price_level\":null,\"rating\":null}",
            "priority": 1,
            "localityAmenityTypes": {
                "id": 16,
                "name": "park",
                "description": "Park"
            },
            "rating": 5,
            "futureInfrastructure": false
        }],
        "resiProjectSpecifications": [{
            "id": 718,
            "masterSpecification": {
                "masterSpecId": 30,
                "masterSpecificationCategory": {
                    "masterSpecCatId": 6,
                    "masterSpecCatName": "FLOORING_BALCONY",
                    "masterSpecCatDisplayName": "Balcony",
                    "masterSpecParentCat": {
                        "masterSpecParentCatId": 1,
                        "masterSpecParentDisplayName": "Flooring"
                    }
                },
                "masterSpecClassName": "Ceramic Tiles"
            },
            "masterSpecificationId": 30,
            "objectTypeId": 1,
            "objectId": 500773,
            "projectId": 500773
        }, {
            "id": 721,
            "masterSpecification": {
                "masterSpecId": 43,
                "masterSpecificationCategory": {
                    "masterSpecCatId": 7,
                    "masterSpecCatName": "FLOORING_KITCHEN",
                    "masterSpecCatDisplayName": "Kitchen",
                    "masterSpecParentCat": {
                        "masterSpecParentCatId": 1,
                        "masterSpecParentDisplayName": "Flooring"
                    }
                },
                "masterSpecClassName": "Vitrified Tiles"
            },
            "masterSpecificationId": 43,
            "objectTypeId": 1,
            "objectId": 500773,
            "projectId": 500773
        }, {
            "id": 722,
            "masterSpecification": {
                "masterSpecId": 57,
                "masterSpecificationCategory": {
                    "masterSpecCatId": 8,
                    "masterSpecCatName": "FLOORING_LIVING_DINING",
                    "masterSpecCatDisplayName": "Living/Dining",
                    "masterSpecParentCat": {
                        "masterSpecParentCatId": 1,
                        "masterSpecParentDisplayName": "Flooring"
                    }
                },
                "masterSpecClassName": "Vitrified Tiles"
            },
            "masterSpecificationId": 57,
            "objectTypeId": 1,
            "objectId": 500773,
            "projectId": 500773
        }, {
            "id": 724,
            "masterSpecification": {
                "masterSpecId": 71,
                "masterSpecificationCategory": {
                    "masterSpecCatId": 9,
                    "masterSpecCatName": "FLOORING_MASTER_BEDROOM",
                    "masterSpecCatDisplayName": "Master Bedroom",
                    "masterSpecParentCat": {
                        "masterSpecParentCatId": 1,
                        "masterSpecParentDisplayName": "Flooring"
                    }
                },
                "masterSpecClassName": "Vitrified Tiles"
            },
            "masterSpecificationId": 71,
            "objectTypeId": 1,
            "objectId": 500773,
            "projectId": 500773
        }, {
            "id": 723,
            "masterSpecification": {
                "masterSpecId": 85,
                "masterSpecificationCategory": {
                    "masterSpecCatId": 10,
                    "masterSpecCatName": "FLOORING_OTHER_BEDROOM",
                    "masterSpecCatDisplayName": "Other Bedroom",
                    "masterSpecParentCat": {
                        "masterSpecParentCatId": 1,
                        "masterSpecParentDisplayName": "Flooring"
                    }
                },
                "masterSpecClassName": "Vitrified Tiles"
            },
            "masterSpecificationId": 85,
            "objectTypeId": 1,
            "objectId": 500773,
            "projectId": 500773
        }, {
            "id": 720,
            "masterSpecification": {
                "masterSpecId": 100,
                "masterSpecificationCategory": {
                    "masterSpecCatId": 11,
                    "masterSpecCatName": "FLOORING_TOILETS",
                    "masterSpecCatDisplayName": "Toilets",
                    "masterSpecParentCat": {
                        "masterSpecParentCatId": 1,
                        "masterSpecParentDisplayName": "Flooring"
                    }
                },
                "masterSpecClassName": "Ceramic Tiles"
            },
            "masterSpecificationId": 100,
            "objectTypeId": 1,
            "objectId": 500773,
            "projectId": 500773
        }, {
            "id": 711,
            "masterSpecification": {
                "masterSpecId": 119,
                "masterSpecificationCategory": {
                    "masterSpecCatId": 13,
                    "masterSpecCatName": "WALLS_EXTERIOR",
                    "masterSpecCatDisplayName": "Exterior",
                    "masterSpecParentCat": {
                        "masterSpecParentCatId": 2,
                        "masterSpecParentDisplayName": "Walls"
                    }
                },
                "masterSpecClassName": "Texture Paint"
            },
            "masterSpecificationId": 119,
            "objectTypeId": 1,
            "objectId": 500773,
            "projectId": 500773
        }, {
            "id": 717,
            "masterSpecification": {
                "masterSpecId": 137,
                "masterSpecificationCategory": {
                    "masterSpecCatId": 14,
                    "masterSpecCatName": "WALLS_INTERIOR",
                    "masterSpecCatDisplayName": "Interior",
                    "masterSpecParentCat": {
                        "masterSpecParentCatId": 2,
                        "masterSpecParentDisplayName": "Walls"
                    }
                },
                "masterSpecClassName": "Oil Bound Distemper"
            },
            "masterSpecificationId": 137,
            "objectTypeId": 1,
            "objectId": 500773,
            "projectId": 500773
        }, {
            "id": 714,
            "masterSpecification": {
                "masterSpecId": 144,
                "masterSpecificationCategory": {
                    "masterSpecCatId": 15,
                    "masterSpecCatName": "WALLS_KITCHEN",
                    "masterSpecCatDisplayName": "Kitchen",
                    "masterSpecParentCat": {
                        "masterSpecParentCatId": 2,
                        "masterSpecParentDisplayName": "Walls"
                    }
                },
                "masterSpecClassName": "Ceramic Tiles Dado"
            },
            "masterSpecificationId": 144,
            "objectTypeId": 1,
            "objectId": 500773,
            "projectId": 500773
        }, {
            "id": 712,
            "masterSpecification": {
                "masterSpecId": 158,
                "masterSpecificationCategory": {
                    "masterSpecCatId": 16,
                    "masterSpecCatName": "WALLS_TOILETS",
                    "masterSpecCatDisplayName": "Toilets",
                    "masterSpecParentCat": {
                        "masterSpecParentCatId": 2,
                        "masterSpecParentDisplayName": "Walls"
                    }
                },
                "masterSpecClassName": "Ceramic Tiles Dado"
            },
            "masterSpecificationId": 158,
            "objectTypeId": 1,
            "objectId": 500773,
            "projectId": 500773
        }, {
            "id": 713,
            "masterSpecification": {
                "masterSpecId": 8,
                "masterSpecificationCategory": {
                    "masterSpecCatId": 1,
                    "masterSpecCatName": "DOORS_INTERNAL",
                    "masterSpecCatDisplayName": "Internal",
                    "masterSpecParentCat": {
                        "masterSpecParentCatId": 3,
                        "masterSpecParentDisplayName": "Doors"
                    }
                },
                "masterSpecClassName": "Flush Shutters"
            },
            "masterSpecificationId": 8,
            "objectTypeId": 1,
            "objectId": 500773,
            "projectId": 500773
        }, {
            "id": 354451,
            "masterSpecification": {
                "masterSpecId": 185,
                "masterSpecificationCategory": {
                    "masterSpecCatId": 4,
                    "masterSpecCatName": "FITTINGS_AND_FIXTURES_KITCHEN",
                    "masterSpecCatDisplayName": "Kitchen",
                    "masterSpecParentCat": {
                        "masterSpecParentCatId": 4,
                        "masterSpecParentDisplayName": "Fittings"
                    }
                },
                "masterSpecClassName": "Modular Kitchen with Stainless Steel Sink"
            },
            "masterSpecificationId": 185,
            "objectTypeId": 1,
            "objectId": 500773,
            "projectId": 500773
        }, {
            "id": 354452,
            "masterSpecification": {
                "masterSpecId": 208,
                "masterSpecificationCategory": {
                    "masterSpecCatId": 5,
                    "masterSpecCatName": "FITTINGS_AND_FIXTURES_TOILETS",
                    "masterSpecCatDisplayName": "Toilets",
                    "masterSpecParentCat": {
                        "masterSpecParentCatId": 4,
                        "masterSpecParentDisplayName": "Fittings"
                    }
                },
                "masterSpecClassName": "CP Fittings"
            },
            "masterSpecificationId": 208,
            "objectTypeId": 1,
            "objectId": 500773,
            "projectId": 500773
        }, {
            "id": 715,
            "masterSpecification": {
                "masterSpecId": 174,
                "masterSpecificationCategory": {
                    "masterSpecCatId": 17,
                    "masterSpecCatName": "WINDOWS",
                    "masterSpecCatDisplayName": "Windows",
                    "masterSpecParentCat": {
                        "masterSpecParentCatId": 5,
                        "masterSpecParentDisplayName": "Others"
                    }
                },
                "masterSpecClassName": "Powder Coated Aluminium Sliding"
            },
            "masterSpecificationId": 174,
            "objectTypeId": 1,
            "objectId": 500773,
            "projectId": 500773
        }],
        "projectAmenities": [{
            "id": 654589,
            "amenityDisplayName": "Gymnasium",
            "amenityMaster": {
                "amenityId": 1,
                "amenityName": "Gymnasium",
                "abbreviation": "Gym"
            },
            "verified": true
        }, {
            "id": 654590,
            "amenityDisplayName": "Swimming Pool",
            "amenityMaster": {
                "amenityId": 2,
                "amenityName": "Swimming Pool",
                "abbreviation": "Swi"
            },
            "verified": true
        }, {
            "id": 654591,
            "amenityDisplayName": "Children's play area",
            "amenityMaster": {
                "amenityId": 3,
                "amenityName": "Children's play area",
                "abbreviation": "Chi"
            },
            "verified": true
        }, {
            "id": 654592,
            "amenityDisplayName": "Club House",
            "amenityMaster": {
                "amenityId": 4,
                "amenityName": "Club House",
                "abbreviation": "Clu"
            },
            "verified": true
        }, {
            "id": 654593,
            "amenityDisplayName": "Multipurpose Room",
            "amenityMaster": {
                "amenityId": 7,
                "amenityName": "Multipurpose Room",
                "abbreviation": "Mul"
            },
            "verified": true
        }, {
            "id": 654594,
            "amenityDisplayName": "Sports Facility",
            "amenityMaster": {
                "amenityId": 8,
                "amenityName": "Sports Facility",
                "abbreviation": "Spo"
            },
            "verified": true
        }, {
            "id": 654595,
            "amenityDisplayName": "Rain Water Harvesting",
            "amenityMaster": {
                "amenityId": 9,
                "amenityName": "Rain Water Harvesting",
                "abbreviation": "Rai"
            },
            "verified": true
        }, {
            "id": 654596,
            "amenityDisplayName": "Intercom",
            "amenityMaster": {
                "amenityId": 10,
                "amenityName": "Intercom",
                "abbreviation": "Int"
            },
            "verified": true
        }, {
            "id": 654597,
            "amenityDisplayName": "24 X 7 Security",
            "amenityMaster": {
                "amenityId": 11,
                "amenityName": "24 X 7 Security",
                "abbreviation": "Sec"
            },
            "verified": true
        }, {
            "id": 654598,
            "amenityDisplayName": "Jogging Track",
            "amenityMaster": {
                "amenityId": 12,
                "amenityName": "Jogging Track",
                "abbreviation": "Jog"
            },
            "verified": true
        }, {
            "id": 654599,
            "amenityDisplayName": "Power Backup",
            "amenityMaster": {
                "amenityId": 13,
                "amenityName": "Power Backup",
                "abbreviation": "Pow"
            },
            "verified": true
        }, {
            "id": 654600,
            "amenityDisplayName": "Landscaped Gardens",
            "amenityMaster": {
                "amenityId": 14,
                "amenityName": "Landscaped Gardens",
                "abbreviation": "Lan"
            },
            "verified": true
        }, {
            "id": 654601,
            "amenityDisplayName": "Indoor Games",
            "amenityMaster": {
                "amenityId": 15,
                "amenityName": "Indoor Games",
                "abbreviation": "Ind"
            },
            "verified": true
        }, {
            "id": 1052741,
            "amenityDisplayName": "4 Sided Open Vastu Friendly",
            "amenityMaster": {
                "amenityId": 99,
                "amenityName": "Others",
                "abbreviation": "Oth"
            },
            "verified": true
        }, {
            "id": 1052742,
            "amenityDisplayName": "24 Hours Water Supply",
            "amenityMaster": {
                "amenityId": 99,
                "amenityName": "Others",
                "abbreviation": "Oth"
            },
            "verified": true
        }, {
            "id": 1052743,
            "amenityDisplayName": "Herbal and Medicine Plantation",
            "amenityMaster": {
                "amenityId": 99,
                "amenityName": "Others",
                "abbreviation": "Oth"
            },
            "verified": true
        }, {
            "id": 1052744,
            "amenityDisplayName": "Massage Therapy",
            "amenityMaster": {
                "amenityId": 99,
                "amenityName": "Others",
                "abbreviation": "Oth"
            },
            "verified": true
        }, {
            "id": 1052745,
            "amenityDisplayName": "Basketball Court",
            "amenityMaster": {
                "amenityId": 99,
                "amenityName": "Others",
                "abbreviation": "Oth"
            },
            "verified": true
        }, {
            "id": 1052746,
            "amenityDisplayName": "Volley Ball",
            "amenityMaster": {
                "amenityId": 99,
                "amenityName": "Others",
                "abbreviation": "Oth"
            },
            "verified": true
        }, {
            "id": 1052747,
            "amenityDisplayName": "Badminton Court",
            "amenityMaster": {
                "amenityId": 99,
                "amenityName": "Others",
                "abbreviation": "Oth"
            },
            "verified": true
        }, {
            "id": 1052748,
            "amenityDisplayName": "Tennis Court",
            "amenityMaster": {
                "amenityId": 99,
                "amenityName": "Others",
                "abbreviation": "Oth"
            },
            "verified": true
        }, {
            "id": 1052749,
            "amenityDisplayName": "Senior Citizen Area",
            "amenityMaster": {
                "amenityId": 99,
                "amenityName": "Others",
                "abbreviation": "Oth"
            },
            "verified": true
        }, {
            "id": 1052750,
            "amenityDisplayName": "Hospital",
            "amenityMaster": {
                "amenityId": 99,
                "amenityName": "Others",
                "abbreviation": "Oth"
            },
            "verified": true
        }, {
            "id": 1052751,
            "amenityDisplayName": "Party Lawn",
            "amenityMaster": {
                "amenityId": 99,
                "amenityName": "Others",
                "abbreviation": "Oth"
            },
            "verified": true
        }, {
            "id": 1052752,
            "amenityDisplayName": "Squash Court",
            "amenityMaster": {
                "amenityId": 99,
                "amenityName": "Others",
                "abbreviation": "Oth"
            },
            "verified": true
        }, {
            "id": 1052753,
            "amenityDisplayName": "Palm Court",
            "amenityMaster": {
                "amenityId": 99,
                "amenityName": "Others",
                "abbreviation": "Oth"
            },
            "verified": true
        }, {
            "id": 1052754,
            "amenityDisplayName": "Yoga and Meditation Garden",
            "amenityMaster": {
                "amenityId": 99,
                "amenityName": "Others",
                "abbreviation": "Oth"
            },
            "verified": true
        }, {
            "id": 1052755,
            "amenityDisplayName": "Water Body with Musical Fountain",
            "amenityMaster": {
                "amenityId": 99,
                "amenityName": "Others",
                "abbreviation": "Oth"
            },
            "verified": true
        }, {
            "id": 1052756,
            "amenityDisplayName": "Jacuzzi, Spa",
            "amenityMaster": {
                "amenityId": 99,
                "amenityName": "Others",
                "abbreviation": "Oth"
            },
            "verified": true
        }],
        "paymentPlanUrl": "app_docs/CapeTown/1409144969_Supertech.pdf",
        "videoUrls": [{
            "videoId": 2954,
            "category": "Walkthrough",
            "videoUrl": "3eQIGJxgLpQ"
        }, {
            "videoId": 2953,
            "category": "Walkthrough",
            "videoUrl": "QbX3hCLqzMg"
        }, {
            "videoId": 2133,
            "category": "Presentation",
            "videoUrl": "RHyEHsL63FY"
        }],
        "loanProviderBanks": [],
        "lastUpdatedDate": 1439836200000,
        "imagesCount": 60,
        "imageCountByType": {
            "paymentPlan": 2,
            "constructionStatus": 41,
            "masterPlan": 1,
            "main": 2,
            "locationPlan": 1,
            "mainOther": 7
        },
        "videosCount": 3,
        "avgPricePerUnitArea": 5539.2957,
        "minResaleOrPrimaryPrice": 4975500.0,
        "maxResaleOrPrimaryPrice": 1.275975E7,
        "safetyScore": 5.2,
        "livabilityScore": 8.0,
        "projectLocalityScore": 8.0,
        "primaryScore": 0.0,
        "projectSocietyScore": 8.1,
        "projectSafetyRank": 3,
        "projectLivabilityRank": 1,
        "newsTag": "Supertech CapeTown",
        "has3DImages": true,
        "landmarkImages": [{
            "id": 467300,
            "imageTypeId": 65,
            "objectId": 45752,
            "path": "10/45752/65/",
            "pageUrl": "gallery/school-the-millennium-school-45752-467300",
            "statusId": 1,
            "createdAt": 1450927153000,
            "takenAt": 1398377843000,
            "sizeInBytes": 174254,
            "width": 1600,
            "height": 1200,
            "altText": "school-The Millennium School",
            "title": "school-The Millennium School",
            "description": "",
            "priority": 999,
            "waterMarkName": "467300.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "school-the-millennium-school-467300.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/10/45752/65/school-the-millennium-school-467300.jpeg",
            "imageType": {
                "id": 65,
                "objectType": {
                    "id": 10,
                    "type": "landmark"
                },
                "mediaType": {
                    "id": 1,
                    "name": "Image"
                },
                "objectTypeId": 10,
                "mediaTypeId": 1,
                "type": "school",
                "domainId": 2,
                "priority": 1,
                "imageSitemapEnabled": 0,
                "displayName": "School"
            },
            "masterImageStatuses": {
                "id": 1,
                "status": "PROCESSED"
            }
        }, {
            "id": 463852,
            "imageTypeId": 65,
            "objectId": 87208,
            "path": "10/87208/65/",
            "pageUrl": "gallery/school-nilgiri-hills-public-school-87208-463852",
            "statusId": 1,
            "createdAt": 1450927153000,
            "takenAt": 1398377119000,
            "sizeInBytes": 12372615,
            "width": 3872,
            "height": 2592,
            "altText": "school-Nilgiri Hills Public School",
            "title": "school-Nilgiri Hills Public School",
            "description": "",
            "priority": 999,
            "waterMarkName": "463852.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "school-nilgiri-hills-public-school-463852.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/10/87208/65/school-nilgiri-hills-public-school-463852.jpeg",
            "imageType": {
                "id": 65,
                "objectType": {
                    "id": 10,
                    "type": "landmark"
                },
                "mediaType": {
                    "id": 1,
                    "name": "Image"
                },
                "objectTypeId": 10,
                "mediaTypeId": 1,
                "type": "school",
                "domainId": 2,
                "priority": 1,
                "imageSitemapEnabled": 0,
                "displayName": "School"
            },
            "masterImageStatuses": {
                "id": 1,
                "status": "PROCESSED"
            }
        }, {
            "id": 463154,
            "imageTypeId": 65,
            "objectId": 72301,
            "path": "10/72301/65/",
            "pageUrl": "gallery/school-manav-rachna-international-school-72301-463154",
            "statusId": 1,
            "createdAt": 1450927153000,
            "takenAt": 1398376765000,
            "sizeInBytes": 6773521,
            "width": 3872,
            "height": 2592,
            "altText": "school-Manav Rachna International School",
            "title": "school-Manav Rachna International School",
            "description": "",
            "priority": 999,
            "waterMarkName": "463154.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "school-manav-rachna-international-school-463154.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/10/72301/65/school-manav-rachna-international-school-463154.jpeg",
            "imageType": {
                "id": 65,
                "objectType": {
                    "id": 10,
                    "type": "landmark"
                },
                "mediaType": {
                    "id": 1,
                    "name": "Image"
                },
                "objectTypeId": 10,
                "mediaTypeId": 1,
                "type": "school",
                "domainId": 2,
                "priority": 1,
                "imageSitemapEnabled": 0,
                "displayName": "School"
            },
            "masterImageStatuses": {
                "id": 1,
                "status": "PROCESSED"
            }
        }, {
            "id": 467299,
            "imageTypeId": 65,
            "objectId": 72301,
            "path": "10/72301/65/",
            "pageUrl": "gallery/school-manav-rachna-international-school-72301-467299",
            "statusId": 1,
            "createdAt": 1450927153000,
            "takenAt": 1398376749000,
            "sizeInBytes": 196330,
            "width": 1600,
            "height": 1200,
            "altText": "school-Manav Rachna International School",
            "title": "school-Manav Rachna International School",
            "description": "",
            "priority": 999,
            "waterMarkName": "467299.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "school-manav-rachna-international-school-467299.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/10/72301/65/school-manav-rachna-international-school-467299.jpeg",
            "imageType": {
                "id": 65,
                "objectType": {
                    "id": 10,
                    "type": "landmark"
                },
                "mediaType": {
                    "id": 1,
                    "name": "Image"
                },
                "objectTypeId": 10,
                "mediaTypeId": 1,
                "type": "school",
                "domainId": 2,
                "priority": 1,
                "imageSitemapEnabled": 0,
                "displayName": "School"
            },
            "masterImageStatuses": {
                "id": 1,
                "status": "PROCESSED"
            }
        }, {
            "id": 463882,
            "imageTypeId": 65,
            "objectId": 76822,
            "path": "10/76822/65/",
            "pageUrl": "gallery/school-ramagya-school-76822-463882",
            "statusId": 1,
            "createdAt": 1450927153000,
            "takenAt": 1398375187000,
            "sizeInBytes": 7745450,
            "width": 3449,
            "height": 2404,
            "altText": "school-Ramagya School",
            "title": "school-Ramagya School",
            "description": "",
            "priority": 999,
            "waterMarkName": "463882.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "school-ramagya-school-463882.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/10/76822/65/school-ramagya-school-463882.jpeg",
            "imageType": {
                "id": 65,
                "objectType": {
                    "id": 10,
                    "type": "landmark"
                },
                "mediaType": {
                    "id": 1,
                    "name": "Image"
                },
                "objectTypeId": 10,
                "mediaTypeId": 1,
                "type": "school",
                "domainId": 2,
                "priority": 1,
                "imageSitemapEnabled": 0,
                "displayName": "School"
            },
            "masterImageStatuses": {
                "id": 1,
                "status": "PROCESSED"
            }
        }, {
            "id": 467298,
            "imageTypeId": 65,
            "objectId": 36988,
            "path": "10/36988/65/",
            "pageUrl": "gallery/school-kothari-international-school-36988-467298",
            "statusId": 1,
            "createdAt": 1450927153000,
            "takenAt": 1398374260000,
            "sizeInBytes": 252202,
            "width": 1600,
            "height": 1200,
            "altText": "school-Kothari International School",
            "title": "school-Kothari International School",
            "description": "",
            "priority": 999,
            "waterMarkName": "467298.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "school-kothari-international-school-467298.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/10/36988/65/school-kothari-international-school-467298.jpeg",
            "imageType": {
                "id": 65,
                "objectType": {
                    "id": 10,
                    "type": "landmark"
                },
                "mediaType": {
                    "id": 1,
                    "name": "Image"
                },
                "objectTypeId": 10,
                "mediaTypeId": 1,
                "type": "school",
                "domainId": 2,
                "priority": 1,
                "imageSitemapEnabled": 0,
                "displayName": "School"
            },
            "masterImageStatuses": {
                "id": 1,
                "status": "PROCESSED"
            }
        }, {
            "id": 463744,
            "imageTypeId": 65,
            "objectId": 36988,
            "path": "10/36988/65/",
            "pageUrl": "gallery/school-kothari-international-school-36988-463744",
            "statusId": 1,
            "createdAt": 1450927153000,
            "takenAt": 1398373674000,
            "sizeInBytes": 9791095,
            "width": 3872,
            "height": 2592,
            "altText": "school-Kothari International School",
            "title": "school-Kothari International School",
            "description": "",
            "priority": 999,
            "waterMarkName": "463744.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "school-kothari-international-school-463744.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/10/36988/65/school-kothari-international-school-463744.jpeg",
            "imageType": {
                "id": 65,
                "objectType": {
                    "id": 10,
                    "type": "landmark"
                },
                "mediaType": {
                    "id": 1,
                    "name": "Image"
                },
                "objectTypeId": 10,
                "mediaTypeId": 1,
                "type": "school",
                "domainId": 2,
                "priority": 1,
                "imageSitemapEnabled": 0,
                "displayName": "School"
            },
            "masterImageStatuses": {
                "id": 1,
                "status": "PROCESSED"
            }
        }, {
            "id": 573510,
            "imageTypeId": 66,
            "objectId": 20482,
            "path": "10/20482/66/",
            "pageUrl": "gallery/hospital-prayag-hospital-and-research-centre-20482-573510",
            "statusId": 1,
            "createdAt": 1450927153000,
            "takenAt": 1416794868000,
            "sizeInBytes": 863538,
            "width": 3165,
            "height": 2374,
            "altText": "hospital-Prayag Hospital and Research Centre",
            "title": "hospital-Prayag Hospital and Research Centre",
            "description": "",
            "priority": 999,
            "waterMarkName": "573510.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "hospital-prayag-hospital-and-research-centre-573510.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/10/20482/66/hospital-prayag-hospital-and-research-centre-573510.jpeg",
            "imageType": {
                "id": 66,
                "objectType": {
                    "id": 10,
                    "type": "landmark"
                },
                "mediaType": {
                    "id": 1,
                    "name": "Image"
                },
                "objectTypeId": 10,
                "mediaTypeId": 1,
                "type": "hospital",
                "domainId": 2,
                "priority": 2,
                "imageSitemapEnabled": 0,
                "displayName": "Hospital"
            },
            "masterImageStatuses": {
                "id": 1,
                "status": "PROCESSED"
            }
        }, {
            "id": 463195,
            "imageTypeId": 69,
            "objectId": 87191,
            "path": "10/87191/65/",
            "pageUrl": "gallery/park-meghdootam-park-87191-463195",
            "statusId": 1,
            "createdAt": 1450927153000,
            "takenAt": 1398376464000,
            "sizeInBytes": 8178923,
            "width": 3700,
            "height": 2364,
            "altText": "park-Meghdootam Park",
            "title": "park-Meghdootam Park",
            "description": "",
            "priority": 999,
            "waterMarkName": "463195.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "park-Meghdootam-Park-463195.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/10/87191/65/park-Meghdootam-Park-463195.jpeg",
            "imageType": {
                "id": 69,
                "objectType": {
                    "id": 10,
                    "type": "landmark"
                },
                "mediaType": {
                    "id": 1,
                    "name": "Image"
                },
                "objectTypeId": 10,
                "mediaTypeId": 1,
                "type": "park",
                "domainId": 2,
                "priority": 3,
                "imageSitemapEnabled": 0,
                "displayName": "Park"
            },
            "masterImageStatuses": {
                "id": 1,
                "status": "PROCESSED"
            }
        }, {
            "id": 471508,
            "imageTypeId": 77,
            "objectId": 90111,
            "path": "10/90111/77/",
            "pageUrl": "gallery/shopping-mall-shopprix-mall-90111-471508",
            "statusId": 1,
            "createdAt": 1450927153000,
            "takenAt": 1399433640000,
            "sizeInBytes": 7306248,
            "width": 3456,
            "height": 2592,
            "altText": "shopping_mall-Shopprix Mall",
            "title": "shopping_mall-Shopprix Mall",
            "description": "",
            "priority": 999,
            "waterMarkName": "471508.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "shopping-mall-shopprix-mall-471508.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/10/90111/77/shopping-mall-shopprix-mall-471508.jpeg",
            "imageType": {
                "id": 77,
                "objectType": {
                    "id": 10,
                    "type": "landmark"
                },
                "mediaType": {
                    "id": 1,
                    "name": "Image"
                },
                "objectTypeId": 10,
                "mediaTypeId": 1,
                "type": "shopping_mall",
                "domainId": 2,
                "priority": 4,
                "imageSitemapEnabled": 0,
                "displayName": "Shopping Mall"
            },
            "masterImageStatuses": {
                "id": 1,
                "status": "PROCESSED"
            }
        }, {
            "id": 463155,
            "imageTypeId": 78,
            "objectId": 80196,
            "path": "10/80196/65/",
            "pageUrl": "gallery/grocery-or-supermarket-empire-super-market-80196-463155",
            "statusId": 1,
            "createdAt": 1450927153000,
            "takenAt": 1398379092000,
            "sizeInBytes": 6828079,
            "width": 3563,
            "height": 2340,
            "altText": "grocery_or_supermarket-Empire Super Market",
            "title": "grocery_or_supermarket-Empire Super Market",
            "description": "",
            "priority": 999,
            "waterMarkName": "463155.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "grocery_or_supermarket-Empire-Super-Market-463155.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/10/80196/65/grocery_or_supermarket-Empire-Super-Market-463155.jpeg",
            "imageType": {
                "id": 78,
                "objectType": {
                    "id": 10,
                    "type": "landmark"
                },
                "mediaType": {
                    "id": 1,
                    "name": "Image"
                },
                "objectTypeId": 10,
                "mediaTypeId": 1,
                "type": "grocery_or_supermarket",
                "domainId": 2,
                "priority": 4,
                "imageSitemapEnabled": 0,
                "displayName": "Grocery Or Supermarket"
            },
            "masterImageStatuses": {
                "id": 1,
                "status": "PROCESSED"
            }
        }, {
            "id": 463160,
            "imageTypeId": 78,
            "objectId": 80196,
            "path": "10/80196/65/",
            "pageUrl": "gallery/grocery-or-supermarket-empire-super-market-80196-463160",
            "statusId": 1,
            "createdAt": 1450927153000,
            "takenAt": 1398379022000,
            "sizeInBytes": 10841749,
            "width": 3640,
            "height": 2418,
            "altText": "grocery_or_supermarket-Empire Super Market",
            "title": "grocery_or_supermarket-Empire Super Market",
            "description": "",
            "priority": 999,
            "waterMarkName": "463160.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "grocery_or_supermarket-Empire-Super-Market-463160.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/10/80196/65/grocery_or_supermarket-Empire-Super-Market-463160.jpeg",
            "imageType": {
                "id": 78,
                "objectType": {
                    "id": 10,
                    "type": "landmark"
                },
                "mediaType": {
                    "id": 1,
                    "name": "Image"
                },
                "objectTypeId": 10,
                "mediaTypeId": 1,
                "type": "grocery_or_supermarket",
                "domainId": 2,
                "priority": 4,
                "imageSitemapEnabled": 0,
                "displayName": "Grocery Or Supermarket"
            },
            "masterImageStatuses": {
                "id": 1,
                "status": "PROCESSED"
            }
        }, {
            "id": 471464,
            "imageTypeId": 78,
            "objectId": 87207,
            "path": "10/87207/78/",
            "pageUrl": "gallery/grocery-or-supermarket-main-market-sector-50-87207-471464",
            "statusId": 1,
            "createdAt": 1450927153000,
            "takenAt": 1398378992000,
            "sizeInBytes": 303048,
            "width": 1600,
            "height": 1071,
            "altText": "grocery_or_supermarket-Main Market Sector 50",
            "title": "grocery_or_supermarket-Main Market Sector 50",
            "description": "",
            "priority": 999,
            "waterMarkName": "471464.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "grocery-or-supermarket-main-market-sector-50-471464.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/10/87207/78/grocery-or-supermarket-main-market-sector-50-471464.jpeg",
            "imageType": {
                "id": 78,
                "objectType": {
                    "id": 10,
                    "type": "landmark"
                },
                "mediaType": {
                    "id": 1,
                    "name": "Image"
                },
                "objectTypeId": 10,
                "mediaTypeId": 1,
                "type": "grocery_or_supermarket",
                "domainId": 2,
                "priority": 4,
                "imageSitemapEnabled": 0,
                "displayName": "Grocery Or Supermarket"
            },
            "masterImageStatuses": {
                "id": 1,
                "status": "PROCESSED"
            }
        }, {
            "id": 485340,
            "imageTypeId": 84,
            "objectId": 90141,
            "path": "10/90141/84/",
            "pageUrl": "gallery/road-noida-sector-117-90141-485340",
            "statusId": 1,
            "createdAt": 1450927153000,
            "takenAt": 1403265132000,
            "sizeInBytes": 5088608,
            "width": 5000,
            "height": 1111,
            "altText": "road-Noida Sector 117",
            "title": "road-Noida Sector 117",
            "description": "",
            "priority": 999,
            "waterMarkName": "485340.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "road-noida-sector-117-485340.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/10/90141/84/road-noida-sector-117-485340.jpeg",
            "imageType": {
                "id": 84,
                "objectType": {
                    "id": 10,
                    "type": "landmark"
                },
                "mediaType": {
                    "id": 1,
                    "name": "Image"
                },
                "objectTypeId": 10,
                "mediaTypeId": 1,
                "type": "road",
                "domainId": 2,
                "priority": 6,
                "imageSitemapEnabled": 0,
                "displayName": "Road"
            },
            "masterImageStatuses": {
                "id": 1,
                "status": "PROCESSED"
            }
        }],
        "imageTypeCount": {
            "paymentPlan": 2,
            "constructionStatus": 41,
            "masterPlan": 1,
            "locationPlan": 1,
            "main": 2,
            "mainOther": 7
        },
        "landmarkImageTypeCount": {
            "school": 7,
            "road": 1,
            "shopping_mall": 1,
            "hospital": 1,
            "park": 1,
            "grocery_or_supermarket": 3
        },
        "minDiscountPrice": 4975500.0,
        "maxDiscountPrice": 1.275975E7,
        "minResaleOrDiscountPrice": 4975500.0,
        "maxResaleOrDiscountPrice": 1.275975E7,
        "resaleEnquiry": false,
        "reachabilityInfo": "%7B%220-10%22%3A%22aeomD%7DwiwMng%40%7D%5EqWkiBznAit%40vnB_Ay%5BtpCnkD%7Cn%40ea%40v~Bm%7CBde%40y~AgE%7Dn%40wkAab%40ab%40yK%7Bn%40%22%2C%2210-20%22%3A%22aeomD%7DwiwMng%40%7D%5EqWkiBznAit%40vnB_Ay%5BtpCnkD%7Cn%40ea%40v~Bm%7CBde%40y~AgE%7Dn%40wkAab%40ab%40yK%7Bn%40%7DqF%3F%3F%3FhRptA%7CRjvAvm%40xnA%60i%40boBvmBbG%7CbBho%40fcCdgAdzB_r%40vZi~Cfm%40_%7CAdrBafAhaAcvBkPgcCqlEaa%40zSqmCmt%40sdCgiEzrFet%40acDcuAjh%40gbBFgYllBurAbh%40tTd%60Bc%60AhhA%22%2C%2220-30%22%3A%22_xvmD%7DwiwMb%60AihAuTe%60BtrAch%40fYmlBfbBGbuAkh%40dt%40%60cDfiE%7BrFlt%40rdC%7BSpmCplE%60a%40jPfcCiaAbvBerB%60fAgm%40~%7BAwZh~CezB~q%40gcCegA%7DbBio%40wmBcGai%40coBwm%40ynA%7DRkvAiRqtA%3F%3F%7BzE%3F%3F%3Fnx%40feCeGv~Cb%7DAbxBf%7DBftApyA%7CnFjeE~xAfxD%7BiC%7CdEuZsJecHvfBc%60BrxC%7DhB%7DGogDuJ_bDj%7BD_hHc%7DHqQk~AexDkhE%7BMytDuL_%7BCdmBayBncBccFqW%60~%40hyF%7BfBhlBsJpqC%22%7D",
        "hasTownship": true,
        "avgFlatsPerFloor": 6.05,
        "delayInMonths": 36,
        "hasProjectInsightReport": true,
        "approvers": [],
        "specifications": {
            "Fittings": {
                "Kitchen": "Modular Kitchen with Stainless Steel Sink",
                "Toilets": "CP Fittings"
            },
            "Doors": {
                "Internal": "Flush Shutters"
            },
            "Flooring": {
                "Master Bedroom": "Vitrified Tiles",
                "Balcony": "Ceramic Tiles",
                "Kitchen": "Vitrified Tiles",
                "Other Bedroom": "Vitrified Tiles",
                "Living/Dining": "Vitrified Tiles",
                "Toilets": "Ceramic Tiles"
            },
            "Windows": "Powder Coated Aluminium Sliding",
            "Walls": {
                "Kitchen": "Ceramic Tiles Dado",
                "Toilets": "Ceramic Tiles Dado",
                "Interior": "Oil Bound Distemper",
                "Exterior": "Texture Paint"
            }
        }
    }
}
