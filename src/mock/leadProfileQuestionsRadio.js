module.exports = {
    "statusCode": "2XX",
    "version": "A",
    "data": {
        "entityId": null,
        "profileCompleteness": {
            "completenessScore": 30
        },
        "questionnaireEntityDetail": {
            "id": 63,
            "entityId": 8405,
            "profilingQuestionnaireId": 1,
            "createdAt": 1531470000000,
            "updatedAt": 1531470000000
        },
        "profilingQuestions": [{
            "id": 5,
            "profilingQuestionnaireId": 1,
            "questionId": 2,
            "masterQuestion": {
                "id": 2,
                "question": "How are you moving in?",
                "questionTypeId": 3,
                "masterQuestionType": {
                    "id": 3,
                    "type": "radio"
                },
                "answerOptions": [{
                        "id": 5,
                        "questionId": 2,
                        "displayName": "Alone",
                        "displayOrder": 1,
                        "createdAt": 1529557382000,
                        "updatedAt": 1529557382000,
                        "imageUrl": "https://cdn.makaan.com/images/selected-alone.png"
                    },
                    {
                        "id": 6,
                        "questionId": 2,
                        "displayName": "With Family",
                        "displayOrder": 2,
                        "createdAt": 1529557393000,
                        "updatedAt": 1529557393000,
                        "imageUrl": "https://cdn.makaan.com/images/selected-family.png"
                    },
                    {
                        "id": 7,
                        "questionId": 2,
                        "displayName": "With Friends",
                        "displayOrder": 3,
                        "createdAt": 1529557406000,
                        "updatedAt": 1529557406000,
                        "imageUrl": "https://cdn.makaan.com/images/selected-friends.png"
                    }
                ],
                "createdAt": 1529575979000,
                "updatedAt": 1529575982000
            },
            "profilingQuestionnaire": null,
            "previousQuestionId": 1,
            "status": "Active",
            "createdAt": 1529558425000,
            "updatedAt": 1529558425000,
            "allowSkip": false
        }],
        "profilingResponse": null
    }
}