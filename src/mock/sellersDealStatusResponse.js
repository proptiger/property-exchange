module.exports = {
  "statusCode": "2XX",
  "version": "A",
  "data": [
    {
      "sellerId": 4783923,
      "companyId": 403825,
      "sellerDealMakingStatusForBuy": "NA",
      "sellerDealMakingStatusForRent": "Normal"
    },
    {
      "sellerId": 4464309,
      "companyId": 363782,
      "sellerDealMakingStatusForBuy": "NA",
      "sellerDealMakingStatusForRent": "NA"
    },
    {
      "sellerId": 7392692,
      "companyId": 588618,
      "sellerDealMakingStatusForBuy": "NA",
      "sellerDealMakingStatusForRent": "NA"
    },
    {
      "sellerId": 7557474,
      "companyId": 602773,
      "sellerDealMakingStatusForBuy": "NA",
      "sellerDealMakingStatusForRent": "NA"
    },
    {
      "sellerId": 7509587,
      "companyId": 595997,
      "sellerDealMakingStatusForBuy": "NA",
      "sellerDealMakingStatusForRent": "Normal"
    },
    {
      "sellerId": 5658763,
      "companyId": 596535,
      "sellerDealMakingStatusForBuy": "NA",
      "sellerDealMakingStatusForRent": "Expert"
    },
    {
      "sellerId": 7458033,
      "companyId": 592837,
      "sellerDealMakingStatusForBuy": "NA",
      "sellerDealMakingStatusForRent": "Normal"
    },
    {
      "sellerId": 5251747,
      "companyId": 592875,
      "sellerDealMakingStatusForBuy": "NA",
      "sellerDealMakingStatusForRent": "Expert"
    },
    {
      "sellerId": 7290576,
      "companyId": 579661,
      "sellerDealMakingStatusForBuy": "NA",
      "sellerDealMakingStatusForRent": "Normal"
    },
    {
      "sellerId": 7424236,
      "companyId": 590880,
      "sellerDealMakingStatusForBuy": "NA",
      "sellerDealMakingStatusForRent": "Expert"
    }
  ]
};
