module.exports = {
 	"totalCount":8,
 	"statusCode":"2XX",
 	"version":"A",
 	"data":
 		{
 			"totalCount":2,
 			"results":[
 			{
 				"id":1,
 				"name":"HDFC (5244)",
 				"bankLogo":"https://content.makaan-ws.com/5/1/369/1013008.jpeg",
 				"detail":"",
 				"isPartnered":true,
 				"minInterestRate":9.35,
 				"maxInterestRate":9.35
 			},
 			{
 				"id":2,
 				"name":"SBI (MUM95026)",
 				"bankLogo":"https://content.makaan-ws.com/5/2/369/1013009.jpeg",
 				"detail":"",
 				"isPartnered":true,
 				"minInterestRate":10.35,
 				"maxInterestRate":11.35
 			}]
 		}
};
