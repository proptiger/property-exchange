module.exports = {
    statusCode: "2XX",
    version: "A",
    data: {
        name: "city_expert",
        title: "You are now a City Expert",
        image:
            "https://s3-ap-southeast-1.amazonaws.com/cdn.makaan.com/images/success.png",
        body: "Congratulations! Your benefits have been activated",
        type: "Dashboard",
        context: "success",
        color: "21A657",
        gaId: 1003
    }
};
