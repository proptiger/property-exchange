module.exports = {
"totalCount": 86,
"statusCode": "2XX",
"version": "A",
"data": [
{
"locality": {
"suburbId": 10003,
"suburb": {
"label": "SG Highway"
},
"label": "Near Vaishno Devi Circle On SG Highway"
},
"name": "The Meadows",
"URL": "ahmedabad/near-vaishno-devi-circle-on-sg-highway/adani-the-meadows-501497"
},
{
"locality": {
"suburbId": 10003,
"suburb": {
"label": "SG Highway"
},
"label": "Makarba"
},
"name": "Strata",
"URL": "ahmedabad/makarba/savvy-strata-666493"
},
{
"locality": {
"suburbId": 10003,
"suburb": {
"label": "SG Highway"
},
"label": "Near Nirma University On SG Highway"
},
"name": "Garden City",
"URL": "ahmedabad/near-nirma-university-on-sg-highway/godrej-garden-city-501448"
},
{
"locality": {
"suburbId": 10003,
"suburb": {
"label": "SG Highway"
},
"label": "Near Nirma University On SG Highway"
},
"name": "Reflections",
"URL": "ahmedabad/near-nirma-university-on-sg-highway/pacifica-reflections-506138"
},
{
"locality": {
"suburbId": 10003,
"suburb": {
"label": "SG Highway"
},
"label": "Near Vaishno Devi Circle On SG Highway"
},
"name": "North Enclave",
"URL": "ahmedabad/near-vaishno-devi-circle-on-sg-highway/pacifica-north-enclave-667561"
},
{
"locality": {
"suburbId": 10003,
"suburb": {
"label": "SG Highway"
},
"label": "Makarba"
},
"name": "Palm Meadows",
"URL": "ahmedabad/makarba/poddar-palm-meadows-645494"
},
{
"locality": {
"suburbId": 10003,
"suburb": {
"label": "SG Highway"
},
"label": "Near Vaishno Devi Circle On SG Highway"
},
"name": "Elysium",
"URL": "ahmedabad/near-vaishno-devi-circle-on-sg-highway/adani-elysium-647819"
},
{
"locality": {
"suburbId": 10003,
"suburb": {
"label": "SG Highway"
},
"label": "Near Vaishno Devi Circle On SG Highway"
},
"name": "Shantigram Water Lily",
"URL": "ahmedabad/near-vaishno-devi-circle-on-sg-highway/adani-shantigram-water-lily-501682"
},
{
"locality": {
"suburbId": 10003,
"suburb": {
"label": "SG Highway"
},
"label": "Chandkheda"
},
"name": "Greens",
"URL": "ahmedabad/chandkheda/swati-procon-greens-667055"
},
{
"locality": {
"suburbId": 10003,
"suburb": {
"label": "SG Highway"
},
"label": "Chandkheda"
},
"name": "Divine II",
"URL": "ahmedabad/chandkheda/sparsh-divine-ii-667051"
},
{
"locality": {
"suburbId": 10226,
"suburb": {
"label": "Ahmedabad North"
},
"label": "Ranip"
},
"name": "Ashraya 9",
"URL": "ahmedabad/ranip/keval-vision-corp-ashraya-9-670157"
},
{
"locality": {
"suburbId": 10226,
"suburb": {
"label": "Ahmedabad North"
},
"label": "Sanand"
},
"name": "Edens",
"URL": "ahmedabad/sanand/addor-edens-667901"
},
{
"locality": {
"suburbId": 10226,
"suburb": {
"label": "Ahmedabad North"
},
"label": "Sanand"
},
"name": "Sumukh Villa",
"URL": "ahmedabad/sanand/whistling-group-sumukh-villa-652092"
},
{
"locality": {
"suburbId": 10226,
"suburb": {
"label": "Ahmedabad North"
},
"label": "Sanand"
},
"name": "Kalhaar Blues and Greens",
"URL": "ahmedabad/sanand/navratna-kalhaar-blues-and-greens-652479"
},
{
"locality": {
"suburbId": 10226,
"suburb": {
"label": "Ahmedabad North"
},
"label": "Sanand"
},
"name": "Hari Om Elegance",
"URL": "ahmedabad/sanand/hari-om-group-hari-om-elegance-512530"
},
{
"locality": {
"suburbId": 10226,
"suburb": {
"label": "Ahmedabad North"
},
"label": "Sanand"
},
"name": "Sanand Green Residency",
"URL": "ahmedabad/sanand/swastik-developers-ahmedabad-sanand-green-residency-647784"
},
{
"locality": {
"suburbId": 10226,
"suburb": {
"label": "Ahmedabad North"
},
"label": "Sanand"
},
"name": "Sagar",
"URL": "ahmedabad/sanand/popular-infra-sagar-652269"
},
{
"locality": {
"suburbId": 10226,
"suburb": {
"label": "Ahmedabad North"
},
"label": "Sanand"
},
"name": "Abhishree Orchard",
"URL": "ahmedabad/sanand/sarthav-abhishree-orchard-664527"
},
{
"locality": {
"suburbId": 10226,
"suburb": {
"label": "Ahmedabad North"
},
"label": "Sanand"
},
"name": "Beyond Five Villas",
"URL": "ahmedabad/sanand/arvind-infra-beyond-five-villas-668120"
},
{
"locality": {
"suburbId": 10226,
"suburb": {
"label": "Ahmedabad North"
},
"label": "Nana Chiloda"
},
"name": "Sun Gold",
"URL": "ahmedabad/nana-chiloda/savaliya-builders-sun-gold-665117"
},
{
"locality": {
"suburbId": 10221,
"suburb": {
"label": "Ahmedabad East"
},
"label": "Nikol"
},
"name": "Harmony",
"URL": "ahmedabad/nikol/shayona-land-corporation-harmony-652562"
},
{
"locality": {
"suburbId": 10221,
"suburb": {
"label": "Ahmedabad East"
},
"label": "Bopal"
},
"name": "Sorrel",
"URL": "ahmedabad/bopal/applewood-estates-sorrel-501854"
},
{
"locality": {
"suburbId": 10221,
"suburb": {
"label": "Ahmedabad East"
},
"label": "Nikol"
},
"name": "Devkrupa Flora",
"URL": "ahmedabad/nikol/sahjanand-devkrupa-flora-661634"
},
{
"locality": {
"suburbId": 10221,
"suburb": {
"label": "Ahmedabad East"
},
"label": "Nikol"
},
"name": "Nandanvan",
"URL": "ahmedabad/nikol/armaan-developers-nandanvan-661624"
},
{
"locality": {
"suburbId": 10221,
"suburb": {
"label": "Ahmedabad East"
},
"label": "Nikol"
},
"name": "Galaxy Height",
"URL": "ahmedabad/nikol/galaxy-group-galaxy-height-512925"
},
{
"locality": {
"suburbId": 10221,
"suburb": {
"label": "Ahmedabad East"
},
"label": "Nikol"
},
"name": "Ashtamangal Orchid",
"URL": "ahmedabad/nikol/mitsumi-infrastructure-ashtamangal-orchid-511011"
},
{
"locality": {
"suburbId": 10221,
"suburb": {
"label": "Ahmedabad East"
},
"label": "Nikol"
},
"name": "Devnandan Park",
"URL": "ahmedabad/nikol/devnandan-builders-devnandan-park-650632"
},
{
"locality": {
"suburbId": 10221,
"suburb": {
"label": "Ahmedabad East"
},
"label": "Nikol"
},
"name": "Elegance",
"URL": "ahmedabad/nikol/aayu-buildcon-elegance-669837"
},
{
"locality": {
"suburbId": 10221,
"suburb": {
"label": "Ahmedabad East"
},
"label": "Nikol"
},
"name": "Krish Villa",
"URL": "ahmedabad/nikol/savaliya-builders-krish-villa-653075"
},
{
"locality": {
"suburbId": 10221,
"suburb": {
"label": "Ahmedabad East"
},
"label": "Nikol"
},
"name": "Kartavya Residency",
"URL": "ahmedabad/nikol/vivek-developer-kartavya-residency-667827"
},
{
"locality": {
"suburbId": 10073,
"suburb": {
"label": "Rajpath Club"
},
"label": "Shilaj"
},
"name": "Samprat Residence",
"URL": "ahmedabad/shilaj/bsafal-samprat-residence-502425"
},
{
"locality": {
"suburbId": 10073,
"suburb": {
"label": "Rajpath Club"
},
"label": "Shilaj"
},
"name": "Kaveri Sangam",
"URL": "ahmedabad/shilaj/a-shridhar-kaveri-sangam-666879"
},
{
"locality": {
"suburbId": 10073,
"suburb": {
"label": "Rajpath Club"
},
"label": "Shilaj"
},
"name": "Nisarg Dreams",
"URL": "ahmedabad/shilaj/krish-developer-nisarg-dreams-661657"
},
{
"locality": {
"suburbId": 10073,
"suburb": {
"label": "Rajpath Club"
},
"label": "Shilaj"
},
"name": "Kadamb Residency",
"URL": "ahmedabad/shilaj/agarwal-group-kadamb-residency-644167"
},
{
"locality": {
"suburbId": 10073,
"suburb": {
"label": "Rajpath Club"
},
"label": "Shilaj"
},
"name": "Kaveri",
"URL": "ahmedabad/shilaj/a-shridhar-kaveri-666868"
},
{
"locality": {
"suburbId": 10073,
"suburb": {
"label": "Rajpath Club"
},
"label": "Shilaj"
},
"name": "Ayodhya I and II",
"URL": "ahmedabad/shilaj/art-nirman-ayodhya-i-and-ii-507927"
},
{
"locality": {
"suburbId": 10073,
"suburb": {
"label": "Rajpath Club"
},
"label": "Shilaj"
},
"name": "Optima",
"URL": "ahmedabad/shilaj/sun-builders-optima-647600"
},
{
"locality": {
"suburbId": 10073,
"suburb": {
"label": "Rajpath Club"
},
"label": "Shilaj"
},
"name": "Hariom Residency",
"URL": "ahmedabad/shilaj/hari-om-group-hariom-residency-655076"
},
{
"locality": {
"suburbId": 10073,
"suburb": {
"label": "Rajpath Club"
},
"label": "Shilaj"
},
"name": "Suramya Three",
"URL": "ahmedabad/shilaj/synthesis-spacelinks-suramya-three-652726"
},
{
"locality": {
"suburbId": 10073,
"suburb": {
"label": "Rajpath Club"
},
"label": "Shilaj"
},
"name": "Vrundavan Bunglows",
"URL": "ahmedabad/shilaj/gopi-vrundavan-bunglows-667657"
},
{
"locality": {
"suburbId": 10229,
"suburb": {
"label": "Ahmedabad West"
},
"label": "Vejalpur Gam"
},
"name": "Paarijat Eclat",
"URL": "ahmedabad/vejalpur-gam/bsafal-paarijat-eclat-664432"
},
{
"locality": {
"suburbId": 10229,
"suburb": {
"label": "Ahmedabad West"
},
"label": "Vejalpur Gam"
},
"name": "Jaldeep Vertex",
"URL": "ahmedabad/vejalpur-gam/shree-radha-krishna-jaldeep-vertex-667184"
},
{
"locality": {
"suburbId": 10229,
"suburb": {
"label": "Ahmedabad West"
},
"label": "Naranpura"
},
"name": "Raj Homes",
"URL": "ahmedabad/naranpura/uma-procon-raj-homes-666895"
},
{
"locality": {
"suburbId": 10229,
"suburb": {
"label": "Ahmedabad West"
},
"label": "Naranpura"
},
"name": "Sagar Sangeet Heights",
"URL": "ahmedabad/naranpura/suramya-upvan-developers-sagar-sangeet-heights-659685"
},
{
"locality": {
"suburbId": 10229,
"suburb": {
"label": "Ahmedabad West"
},
"label": "Naranpura"
},
"name": "Icon",
"URL": "ahmedabad/naranpura/arise-developers-icon-656528"
},
{
"locality": {
"suburbId": 10229,
"suburb": {
"label": "Ahmedabad West"
},
"label": "Naranpura"
},
"name": "Gulab Tower",
"URL": "ahmedabad/naranpura/avirat-gulab-tower-667798"
},
{
"locality": {
"suburbId": 10229,
"suburb": {
"label": "Ahmedabad West"
},
"label": "Vejalpur Gam"
},
"name": "Opulence",
"URL": "ahmedabad/vejalpur-gam/aaryan-developers-opulence-659878"
},
{
"locality": {
"suburbId": 10229,
"suburb": {
"label": "Ahmedabad West"
},
"label": "Sachana"
},
"name": "99 Residency",
"URL": "ahmedabad/sachana/aagam-99-residency-647707"
},
{
"locality": {
"suburbId": 10229,
"suburb": {
"label": "Ahmedabad West"
},
"label": "Chekhla"
},
"name": "129 Weekend Villas",
"URL": "ahmedabad/chekhla/amaya-129-weekend-villas-653308"
},
{
"locality": {
"suburbId": 10229,
"suburb": {
"label": "Ahmedabad West"
},
"label": "Vejalpur Gam"
},
"name": "Grace II",
"URL": "ahmedabad/vejalpur-gam/sankalp-organisers-grace-ii-508032"
},
{
"locality": {
"suburbId": 10075,
"suburb": {
"label": "Khokhra Circle"
},
"label": "Maninagar"
},
"name": "Shakuntal Apartment",
"URL": "ahmedabad/maninagar/aaryan-group-ahmedabad-shakuntal-apartment-653625"
},
{
"locality": {
"suburbId": 10075,
"suburb": {
"label": "Khokhra Circle"
},
"label": "Maninagar"
},
"name": "Prestige Bunglows",
"URL": "ahmedabad/maninagar/mahadev-prestige-bunglows-652304"
},
{
"locality": {
"suburbId": 10075,
"suburb": {
"label": "Khokhra Circle"
},
"label": "Maninagar"
},
"name": "Dev Kesal",
"URL": "ahmedabad/maninagar/dev-group-dev-kesal-512930"
},
{
"locality": {
"suburbId": 10075,
"suburb": {
"label": "Khokhra Circle"
},
"label": "Maninagar"
},
"name": "Courtyard 2",
"URL": "ahmedabad/maninagar/avalon-construction-courtyard-2-655136"
},
{
"locality": {
"suburbId": 10075,
"suburb": {
"label": "Khokhra Circle"
},
"label": "Maninagar"
},
"name": "Dev Castle",
"URL": "ahmedabad/maninagar/dev-group-dev-castle-511513"
},
{
"locality": {
"suburbId": 10075,
"suburb": {
"label": "Khokhra Circle"
},
"label": "Maninagar"
},
"name": "Colonials",
"URL": "ahmedabad/maninagar/takshashila-colonials-507655"
},
{
"locality": {
"suburbId": 10237,
"suburb": {
"label": "Ahmedabad Central"
},
"label": "Ellisbridge"
},
"name": "Tilak IV",
"URL": "ahmedabad/ellisbridge/shayona-land-corporation-tilak-iv-656426"
},
{
"locality": {
"suburbId": 10237,
"suburb": {
"label": "Ahmedabad Central"
},
"label": "Ambavadi"
},
"name": "Paradise",
"URL": "ahmedabad/ambavadi/shivalik-projects-paradise-668725"
},
{
"locality": {
"suburbId": 10237,
"suburb": {
"label": "Ahmedabad Central"
},
"label": "Ambavadi"
},
"name": "Eternity",
"URL": "ahmedabad/ambavadi/suvidha-projects-eternity-667211"
},
{
"locality": {
"suburbId": 10237,
"suburb": {
"label": "Ahmedabad Central"
},
"label": "Ambavadi"
},
"name": "Cloud 9",
"URL": "ahmedabad/ambavadi/addor-cloud-9-666899"
},
{
"locality": {
"suburbId": 10237,
"suburb": {
"label": "Ahmedabad Central"
},
"label": "Gulbai Tekra"
},
"name": "Gulmohar Auris",
"URL": "ahmedabad/gulbai-tekra/trilokesh-gulmohar-auris-667095"
},
{
"locality": {
"suburbId": 10237,
"suburb": {
"label": "Ahmedabad Central"
},
"label": "Gulbai Tekra"
},
"name": "Shalin Bellevue",
"URL": "ahmedabad/gulbai-tekra/sai-krupa-project-shalin-bellevue-667082"
},
{
"locality": {
"suburbId": 10237,
"suburb": {
"label": "Ahmedabad Central"
},
"label": "Gulbai Tekra"
},
"name": "Anushri Pratistha",
"URL": "ahmedabad/gulbai-tekra/samruddhi-anushri-pratistha-667083"
},
{
"locality": {
"suburbId": 10237,
"suburb": {
"label": "Ahmedabad Central"
},
"label": "Hatkeshwar"
},
"name": "Shree Rang Heights",
"URL": "ahmedabad/hatkeshwar/azad-shree-rang-heights-667355"
},
{
"locality": {
"suburbId": 10237,
"suburb": {
"label": "Ahmedabad Central"
},
"label": "Gulbai Tekra"
},
"name": "14 Crowns",
"URL": "ahmedabad/gulbai-tekra/addor-14-crowns-666907"
},
{
"locality": {
"suburbId": 10237,
"suburb": {
"label": "Ahmedabad Central"
},
"label": "Gulbai Tekra"
},
"name": "Opal",
"URL": "ahmedabad/gulbai-tekra/merlin-group-opal-654617"
},
{
"locality": {
"suburbId": 10228,
"suburb": {
"label": "Ahmedabad South"
},
"label": "Fedara"
},
"name": "Greentech Residency",
"URL": "ahmedabad/fedara/jaska-greentech-residency-671148"
},
{
"locality": {
"suburbId": 10228,
"suburb": {
"label": "Ahmedabad South"
},
"label": "Sanathal"
},
"name": "The Meadows",
"URL": "ahmedabad/sanathal/pacifica-the-meadows-512905"
},
{
"locality": {
"suburbId": 10228,
"suburb": {
"label": "Ahmedabad South"
},
"label": "Changodar"
},
"name": "Aavaas",
"URL": "ahmedabad/changodar/pacifica-aavaas-663872"
},
{
"locality": {
"suburbId": 10228,
"suburb": {
"label": "Ahmedabad South"
},
"label": "Vasna"
},
"name": "Heights",
"URL": "ahmedabad/vasna/classic-build-heights-667255"
},
{
"locality": {
"suburbId": 10228,
"suburb": {
"label": "Ahmedabad South"
},
"label": "Vasna"
},
"name": "Saransh Courtyard",
"URL": "ahmedabad/vasna/chanchal-saransh-courtyard-667199"
},
{
"locality": {
"suburbId": 10228,
"suburb": {
"label": "Ahmedabad South"
},
"label": "Hathijan"
},
"name": "Homes",
"URL": "ahmedabad/hathijan/balleshwar-construction-company-homes-652489"
},
{
"locality": {
"suburbId": 10228,
"suburb": {
"label": "Ahmedabad South"
},
"label": "Sarkhej"
},
"name": "Homes",
"URL": "ahmedabad/sarkhej/classic-build-homes-667258"
},
{
"locality": {
"suburbId": 10228,
"suburb": {
"label": "Ahmedabad South"
},
"label": "Vasna"
},
"name": "Reevera",
"URL": "ahmedabad/vasna/rajyash-reevera-668191"
},
{
"locality": {
"suburbId": 10228,
"suburb": {
"label": "Ahmedabad South"
},
"label": "Vatva"
},
"name": "Shraddha Pioneer",
"URL": "ahmedabad/vatva/jbl-buildcon-shraddha-pioneer-647759"
},
{
"locality": {
"suburbId": 10228,
"suburb": {
"label": "Ahmedabad South"
},
"label": "Narolgam"
},
"name": "Swastik Platinum",
"URL": "ahmedabad/narolgam/shapers-swastik-platinum-667157"
},
{
"locality": {
"suburbId": 10236,
"suburb": {
"label": "Gandhinagar"
},
"label": "Gift City"
},
"name": "Gift City",
"URL": "ahmedabad/gift-city/brigade-gift-city-666854"
},
{
"locality": {
"suburbId": 10236,
"suburb": {
"label": "Gandhinagar"
},
"label": "Urjanagar"
},
"name": "Luvkush 4",
"URL": "ahmedabad/urjanagar/shree-real-estate-luvkush-4-666865"
},
{
"locality": {
"suburbId": 10236,
"suburb": {
"label": "Gandhinagar"
},
"label": "Sector 11 Gandhinagar"
},
"name": "Shivesh 195",
"URL": "ahmedabad/sector-11-gandhinagar/dharmaja-shivesh-195-667026"
},
{
"locality": {
"suburbId": 10236,
"suburb": {
"label": "Gandhinagar"
},
"label": "Dahegam"
},
"name": "Shree Rang Vishamo",
"URL": "ahmedabad/dahegam/shree-rang-shree-rang-vishamo-652661"
},
{
"locality": {
"suburbId": 10236,
"suburb": {
"label": "Gandhinagar"
},
"label": "Urjanagar"
},
"name": "Shree Rang Palace",
"URL": "ahmedabad/urjanagar/shree-rang-shree-rang-palace-652624"
},
{
"locality": {
"suburbId": 10236,
"suburb": {
"label": "Gandhinagar"
},
"label": "Sargaasan"
},
"name": "Santoor Grace",
"URL": "ahmedabad/sargaasan/sanskar-infracon-santoor-grace-512219"
},
{
"locality": {
"suburbId": 10236,
"suburb": {
"label": "Gandhinagar"
},
"label": "Sargaasan"
},
"name": "Blossom",
"URL": "ahmedabad/sargaasan/swagat-blossom-644436"
},
{
"locality": {
"suburbId": 10236,
"suburb": {
"label": "Gandhinagar"
},
"label": "Urjanagar"
},
"name": "Shree Rang Nagar",
"URL": "ahmedabad/urjanagar/shree-rang-shree-rang-nagar-652644"
},
{
"locality": {
"suburbId": 10236,
"suburb": {
"label": "Gandhinagar"
},
"label": "Urjanagar"
},
"name": "Strawberry",
"URL": "ahmedabad/urjanagar/aawaass-buildcon-pvt-ltd-strawberry-658227"
},
{
"locality": {
"suburbId": 10236,
"suburb": {
"label": "Gandhinagar"
},
"label": "Sargaasan"
},
"name": "Nano City 1 And 2",
"URL": "ahmedabad/sargaasan/shree-rang-nano-city-1-and-2-652650"
}
]
};
