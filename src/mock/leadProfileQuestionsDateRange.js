module.exports = {
    "statusCode": "2XX",
    "version": "A",
    "data": {
        "entityId": null,
        "profileCompleteness": {
            "completenessScore": 30
        },
        "questionnaireEntityDetail": null,
        "profilingQuestions": [{
            "id": 1,
            "profilingQuestionnaireId": 1,
            "questionId": 1,
            "masterQuestion": {
                "id": 1,
                "question": "When do you expect to move in",
                "questionTypeId": 4,
                "masterQuestionType": {
                    "id": 4,
                    "type": "dateRange"
                },
                "answerOptions": [{
                        "id": 1,
                        "questionId": 1,
                        "displayName": "1st Week",
                        "displayOrder": 1,
                        "createdAt": 1529557259000,
                        "updatedAt": 1529557259000,
                        "tag": "Moving in 1st week"
                    },
                    {
                        "id": 2,
                        "questionId": 1,
                        "displayName": "2nd Week",
                        "displayOrder": 2,
                        "createdAt": 1529557291000,
                        "updatedAt": 1529557291000,
                        "tag": "Moving in 2nd week"
                    },
                    {
                        "id": 3,
                        "questionId": 1,
                        "displayName": "3rd Week",
                        "displayOrder": 3,
                        "createdAt": 1529557299000,
                        "updatedAt": 1529557299000,
                        "tag": "Moving in 3rd week"
                    },
                    {
                        "id": 4,
                        "questionId": 1,
                        "displayName": "4th Week",
                        "displayOrder": 3,
                        "createdAt": 1529557305000,
                        "updatedAt": 1529557305000
                    }
                ],
                "createdAt": 1529575902000,
                "updatedAt": 1529575906000
            },
            "profilingQuestionnaire": null,
            "status": "Active",
            "createdAt": 1529558333000,
            "updatedAt": 1529558333000,
            "allowSkip": false
        }],
        "profilingResponse": {
            "skipQuestion": false
        }
    }
}