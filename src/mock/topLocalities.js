module.exports = {
	"totalCount": 10662,
	"statusCode": "2XX",
	"version": "A",
	"data": [{
		"localityId": 50375,
		"label": 'Gachibowli',
		"avgPriceRisePercentage": 3.1,
		"avgPricePerUnitArea": 4471.9693,
		"livabilityScore": 7.1
	}, {
		"localityId": 50376,
		"label": 'Kukatpally',
		"avgPriceRisePercentage": 7.2,
		"avgPricePerUnitArea": 3844.267,
		"livabilityScore": 8.1
	}, {
		"localityId": 50393,
		"label": 'Miyapur',
		"avgPriceRisePercentage": 12,
		"avgPricePerUnitArea": 3340.5044,
		"livabilityScore": 7.1
	}, {
		"localityId": 51628,
		"label": 'Banjara Hills',
		"avgPricePerUnitArea": 8910.9984,
		"livabilityScore": 9
	}]
};
