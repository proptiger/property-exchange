module.exports = {
    "statusCode": "2XX",
    "version": "A",
    "data": [
    {
        "sellerUserId": 3508859,
        "callRatingMap": {
            "1": 783,
            "2": 221,
            "3": 376,
            "4": 508,
            "5": 654
        }
    },
    {
        "sellerUserId": 6264240,
        "callRatingMap": {
            "1": 2,
            "3": 2,
            "5": 3
        }
    }]
}
