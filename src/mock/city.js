module.exports = {
"statusCode": "2XX",
"version": "A",
"data": {
"id": 1,
"authorized": true,
"label": "Ahmedabad",
"northEastLatitude": 23.13780022,
"northEastLongitude": 72.70259857,
"southWestLatitude": 22.9029007,
"southWestLongitude": 72.45410156,
"centerLatitude": 23.03960037,
"centerLongitude": 72.56600189,
"displayPriority": 1,
"displayOrder": 7,
"url": "ahmedabad/property-sale",
"description": "<p style=\"text-align: justify;\">Gujarat&rsquo;s most important real estate hub and one of the biggest industrial cities in India, here is a summary of <span><span><span itemscope itemtype=\"https://schema.org/City\"><span itemprop=\"name\">Ahmedabad</span></span></span></span>, which is also known as the Manchester of the East:</p>\n<p style=\"text-align: justify;\"><span><span><span itemscope itemtype=\"https://schema.org/City\"><span itemprop=\"name\">Ahmedabad</span></span></span></span> is the largest city in Gujarat and finds its place among the most preferred real estate investment destinations in the state. It is among the well-developed regions in the western part of India, and with several employment opportunities across the city which attract people from all over the state, it is also one of the most populous cities in Gujarat.</p>\n<p style=\"text-align: justify;\">Located on the bank of the River Sabarmati, <span><span><span itemscope itemtype=\"https://schema.org/City\"><span itemprop=\"name\">Ahmedabad</span></span></span></span> is said to have been inhabited ever since the 11th century, and has been ruled by several dynasties including the Muzaffarid, Mughal and Maratha Dynasties and it was also under British Raj from 1858 until Independence. Though the city had been a centre for commerce since a long time, it witnessed most of the rapid urbanization during the post-independence era, qualifying it as a metropolitan city in the second half of the 20th century.</p>\n<p style=\"text-align: justify;\">Being the centre of the textile and fabric industry in Gujarat, <span><span><span itemscope itemtype=\"https://schema.org/City\"><span itemprop=\"name\">Ahmedabad</span></span></span></span> is home to some of the oldest textile mills in the nation, which manufacture various types of garments. <span><span><span itemscope itemtype=\"https://schema.org/City\"><span itemprop=\"name\">Ahmedabad</span></span></span></span> also has its own stock exchange, which is the second oldest one in the country. Car manufacturing is another upcoming industry that is developing in and around <span><span><span itemscope itemtype=\"https://schema.org/City\"><span itemprop=\"name\">Ahmedabad</span></span></span></span>, with several big names like Ford,  Suzuki and Honda looking to set up plants here.</p>\n<p style=\"text-align: justify;\"><strong>More about the city</strong></p>\n<p style=\"text-align: justify;\"><span><span><span itemscope itemtype=\"https://schema.org/City\"><span itemprop=\"name\">Ahmedabad</span></span></span></span> today spans itself across 464 square kilometres of land, with a population of about 6 million residing within it, making it a densely populated city. Enjoying exclusive benefits of being centrally located in the heart of Gujarat, <span><span><span itemscope itemtype=\"https://schema.org/City\"><span itemprop=\"name\">Ahmedabad</span></span></span></span> enjoys close proximity to all major cities in the state, including the capital city of <span><span><span itemscope itemtype=\"http://schema.org/PostalAddress\"><span itemprop=\"addressLocality\">Gandhinagar</span></span></span></span>, which is just 30 kilometres from <span><span><span itemscope itemtype=\"https://schema.org/City\"><span itemprop=\"name\">Ahmedabad</span></span></span></span>.</p>\n<p style=\"text-align: justify;\"><span><span><span itemscope itemtype=\"https://schema.org/City\"><span itemprop=\"name\">Ahmedabad</span></span></span></span> is among the most easily accessible cities in Gujarat. It has one major railway station which connects it to all important cities across India, along with other smaller railway stations in various parts of the city. By road, the National Highway No. 8 connects <span><span><span itemscope itemtype=\"https://schema.org/City\"><span itemprop=\"name\">Ahmedabad</span></span></span></span> to <span><span><span itemscope itemtype=\"https://schema.org/City\"><span itemprop=\"name\">Mumbai</span></span></span></span> and <span><span><span itemscope itemtype=\"https://schema.org/City\"><span itemprop=\"name\">Delhi</span></span></span></span>, and several other important cities in between. It is also linked to <span><span><span itemscope itemtype=\"https://schema.org/City\"><span itemprop=\"name\">Vadodara</span></span></span></span> via the National Expressway No. 1, which is a part of the Golden Quadrilateral of India. <span><span><span itemscope itemtype=\"https://schema.org/City\"><span itemprop=\"name\">Ahmedabad</span></span></span></span> has well-built internal roads, which make it easy to transit within the city. It is also located just 15 kilometres away from the domestic and international airport, which makes it easily accessible by air.</p>\n<p style=\"text-align: justify;\"><strong>Villages in <span><span><span itemscope itemtype=\"https://schema.org/City\"><span itemprop=\"name\">Ahmedabad</span></span></span></span></strong></p>\n<p style=\"text-align: justify;\">Being the central district of Gujarat, <span><span><span itemscope itemtype=\"https://schema.org/City\"><span itemprop=\"name\">Ahmedabad</span></span></span></span> encompasses several important towns and villages within it. There are 10 taluks within the district, which include more than 500 villages. <span><span><span itemscope itemtype=\"http://schema.org/PostalAddress\"><span itemprop=\"addressLocality\">Wadaj</span></span></span></span>, <span><span><span itemscope itemtype=\"http://schema.org/PostalAddress\"><span itemprop=\"addressLocality\">Paldi</span></span></span></span>, <span><span><span itemscope itemtype=\"http://schema.org/PostalAddress\"><span itemprop=\"addressLocality\">Memnagar</span></span></span></span>, Narol, Aslali, Dascroi, <span><span><span itemscope itemtype=\"http://schema.org/PostalAddress\"><span itemprop=\"addressLocality\">Dholka</span></span></span></span> and <span itemscope itemtype=\"http://schema.org/PostalAddress\"><span itemprop=\"addressLocality\"><a href=\"/ahmedabad-real-estate/sanand-overview-52097\">Sanand</a></span></span> are among the important areas within <span><span><span itemscope itemtype=\"https://schema.org/City\"><span itemprop=\"name\">Ahmedabad</span></span></span></span>.</p>\n<p style=\"text-align: justify;\"><strong>Schools and Colleges</strong></p>\n<p style=\"text-align: justify;\"><span><span><span itemscope itemtype=\"https://schema.org/City\"><span itemprop=\"name\">Ahmedabad</span></span></span></span> has been an important centre of education in Gujarat for almost a century. Home to some of the most prestigious educational facilities in Gujarat, <span><span><span itemscope itemtype=\"https://schema.org/City\"><span itemprop=\"name\">Ahmedabad</span></span></span></span> continues to be one of the most preferred educational hubs in Western India.</p>\n<p style=\"text-align: justify;\"><span><span><span itemscope itemtype=\"https://schema.org/City\"><span itemprop=\"name\">Ahmedabad</span></span></span></span> has a number of educational institutions which are spread out across various parts of the city. Most of the schools and colleges in <span><span><span itemscope itemtype=\"https://schema.org/City\"><span itemprop=\"name\">Ahmedabad</span></span></span></span> operate under the affiliation of the Gujarat Secondary and Higher Secondary Education Board and Gujarat University, which are the major educational facilities in the state. Apart from basic education, <span><span><span itemscope itemtype=\"https://schema.org/City\"><span itemprop=\"name\">Ahmedabad</span></span></span></span> has several institutions offering specialized education in fields like law, journalism, engineering, medicine, business administration and communications.</p>\n<p style=\"text-align: justify;\">Some of the important educational institutions in <span><span><span itemscope itemtype=\"https://schema.org/City\"><span itemprop=\"name\">Ahmedabad</span></span></span></span> include Indian Institute of Management , Vishwakarma Engineering College, Mudra Institute of Communications, Gujarat National Law University, National Institute of Design and B.J. Medical College.</p>\n<p style=\"text-align: justify;\"><strong>Shopping Malls and Recreation</strong></p>\n<p style=\"text-align: justify;\"><span><span><span itemscope itemtype=\"https://schema.org/City\"><span itemprop=\"name\">Ahmedabad</span></span></span></span> offers plenty of avenues for entertainment and recreation, thanks to it being a metropolitan city with well-developed infrastructure. Ranging from shopping complexes and malls which offer ample shopping opportunities to multiscreen cinemas, cultural events, sports, musical events and the several picnic spots peppered across the city, there is more than just enough of entertainment in <span><span><span itemscope itemtype=\"https://schema.org/City\"><span itemprop=\"name\">Ahmedabad</span></span></span></span>. Nightlife, however, is quite dull in the city, as is the case with most other cities in Gujarat, and there are not many avenues for staying out till late.</p>\n<h3 style=\"font-size: 12px; line-height: 17px; text-align: justify;\"><strong>Real Estate in <span><span><span itemscope itemtype=\"https://schema.org/City\"><span itemprop=\"name\">Ahmedabad</span></span></span></span></strong></h3>\n<p style=\"text-align: justify;\">Real Estate in <span><span><span itemscope itemtype=\"https://schema.org/City\"><span itemprop=\"name\">Ahmedabad</span></span></span></span> has witnessed rapid development ever since it emerged as a metropolitan city. Property in <span><span><span itemscope itemtype=\"https://schema.org/City\"><span itemprop=\"name\">Ahmedabad</span></span></span></span> is one of the ideal options for investment in Gujarat, and there are several avenues in the commercial as well as residential sector. Major real estate developers in India have launched several real estate projects in and around <span><span><span itemscope itemtype=\"https://schema.org/City\"><span itemprop=\"name\">Ahmedabad</span></span></span></span> over the past decade, and it is one of the most sought-after real estate investment destinations in Gujarat.</p>  ",
"isServing": true,
"isServingResale": false,
"avgPricePerUnitArea": 2767.7411,
"avgPricePerUnitAreaApartment": 2767.7411,
"avgPricePerUnitAreaPlot": 416.7325,
"avgPricePerUnitAreaVilla": 4768.6163,
"avgPriceRisePercentage": 5,
"avgPriceRisePercentageApartment": 5,
"avgPriceRisePercentagePlot": 13,
"avgPriceRisePercentageVilla": 4.1,
"avgPriceRiseMonths": 26,
"minZoomLevel": 11,
"maxZoomLevel": 13,
"dominantUnitType": "Apartment",
"overviewUrl": "ahmedabad-real-estate",
"localityMaxLivabilityScore": 9.7,
"localityMinLivabilityScore": 4,
"cityLocalityCount": 87,
"cityPopulation": 6352254,
"cityTagLine": "Manchester of the East",
"cityPropertyCount": 2816,
"populationSurveyDate": 1298917800000,
"cityTaxonomyUrls": {
"apartments": "ahmedabad/apartments-flats-sale",
"propertysale": "ahmedabad/property-sale",
"plots": "ahmedabad/sites-plots-sale",
"villas": "ahmedabad/villas-sale",
"newLaunch": "new-launch-in-ahmedabad",
"resaleApartmentUrl": "ahmedabad/resale-apartments",
"resalePropertyUrl": "ahmedabad/resale-property",
"readyToMoveFlatsUrl": "ahmedabad/ready-to-move-flats",
"luxuryProjectsUrl": "ahmedabad/luxury-projects",
"affordableFlatsUrl": "ahmedabad/affordable-flats",
"underConstructionPropertyUrl": "ahmedabad/under-construction-property",
"upcomingFlatsForSaleUrl": "ahmedabad/upcoming-flats-for-sale",
"upcomingPropertyUrl": "ahmedabad/upcoming-property",
"newAppartmentsForSaleUrl": "ahmedabad/new-apartments-for-sale"
},
"cityHeroshotImageUrl": "https://im.proptiger-ws.com/6/1/92/ahmedabad-heroshot-image-659698.jpeg",
"showPolygon": false,
"cityProjectCount": 1209,
"citySourceId": [
1,
101
],
"citySourceDomain": [
"Makaan",
"Proptiger"
],
"annualGrowth": 7.307752633726574,
"rentalYield": 2.856675589298842,
"demandRate": 5.2544745669660635,
"supplyRate": 5.6635605809102,
"entityDescriptions": [{
    id: 7,
    tableId: 50001,
    tableName: "locality",
    descriptionTypeId: 5,
    masterDescriptionTypes: {
        id: 5,
        type: "Makaan"
    },
    description: "This is lifestyle .Yooo",
    isReviewed: 1,
    entityDescriptionCategories: {
        id: 1,
        objectTypeId: 4,
        masterDescriptionCategory: {
            id: 3,
            name: "Schools",
            masterDescriptionParentCategories: {
                id: 1,
                parentCategory: "LifeStyle"
            }
        }
    }
}]
}
};
