module.exports = {
  "statusCode": "2XX",
  "version": "A",
  "data": {
    "id": 1,
    "sellerId": 1263326,
    "userId": 5711549,
    "rating": 3,
    "ratingScale": 5,
    "reviewComment": "anyReviewComment",
    "createdAt": 1452260657000,
    "sellerRatingParameters": [
      {
        "id": 1,
        "name": "Lack of Knowledge"
      },
      {
        "id": 2,
        "name": "Offensive Language"
      }
    ]
  }
}
