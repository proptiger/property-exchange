module.exports = {
    "statusCode": "2XX",
    "version": "A",
    "data": {
        "urlStatus": {
            "httpStatus": 200,
            "redirectUrl": "gurgaon/vipul-floors-in-sector-48-2678133/3bhk-3t-2160-sqft-builderfloor",
            "pageType": null,
            "urlType": "MAKAAN_PROPERTY_BUY",
            "masterUrlType": "property_urls",
            "urlDetail": {
                "url": "gurgaon/vipul-floors-in-sector-48-2678133/3bhk-3t-2160-sqft-builderfloor",
                "templateId": "MAKAAN_PROPERTY_BUY",
                "sourceDomain": 1,
                "cityName": "Gurgaon",
                "cityId": 11,
                "localityId": 50395,
                "projectId": 504566,
                "builderId": 100331,
                "suburbId": 10024,
                "propertyId": 7026795,
                "listingId": 2678133,
                "bedrooms": 3,
                "bathrooms": 3,
                "size": 2160,
                "projectName": "Floors",
                "localityName": "Sector 48",
                "suburbName": "Sohna Road",
                "builderName": "Vipul",
                "listingConfig": "3bhk-3t-2160-sqft-builderfloor",
                "urlTaxonomyGroupType": "Default",
                "bedroomString": "3",
                "priceString": "",
                "propertyType": "",
                "listingFilter": "",
                "urlType": 8
            }
        },
        "footer-content": "",
        "navigation": {
            "All Facing Houses/Villas": [{
                "label": "East Facing House/Villas in ahmedabad",
                "url": "ahmedabad-residential-property/buy-east-facing-independent-house-villas-in-ahmedabad-city"
            }, {
                "label": "West Facing House/Villas in ahmedabad",
                "url": "ahmedabad-residential-property/buy-west-facing-independent-house-villas-in-ahmedabad-city"
            }, {
                "label": "North Facing House/Villas in ahmedabad",
                "url": "ahmedabad-residential-property/buy-north-facing-independent-house-villas-in-ahmedabad-city"
            }, {
                "label": "South Facing House/Villas in ahmedabad",
                "url": "ahmedabad-residential-property/buy-south-facing-independent-house-villas-in-ahmedabad-city"
            }, {
                "label": "North East Facing House/Villas in ahmedabad",
                "url": "ahmedabad-residential-property/buy-north-east-facing-independent-house-villas-in-ahmedabad-city"
            }],
            "Rent Property in ahmedabad": [{
                "label": "Ready to Move Flats for rent in ahmedabad",
                "url": "ahmedabad-residential-property/ready-to-move-flats-rent"
            }, {
                "label": "Rental Villas in ahmedabad",
                "url": "ahmedabad-residential-property/villas-rent"
            }, {
                "label": "Under Construction Property for rent in ahmedabad",
                "url": "ahmedabad-residential-property/under-construction-property-rent"
            }, {
                "label": "Rental Apartment In ahmedabad",
                "url": "ahmedabad-residential-property/rent-apartments-in-ahmedabad-city"
            }, {
                "label": "Budget Flats for Rent in ahmedabad",
                "url": "ahmedabad-residential-property/rent-budget-property-in-ahmedabad-city"
            }, {
                "label": "Upcoming Flats for Rent in ahmedabad",
                "url": "ahmedabad-residential-property/upcoming-flats-for-rent"
            }],
            "All Unit Type Facing Apartments": [{
                "label": "1 BHK East Facing apartments in ahmedabad",
                "url": "ahmedabad-residential-property/buy-1bhk-east-facing-apartments-in-ahmedabad-city"
            }, {
                "label": "2 BHK East Facing apartments in ahmedabad",
                "url": "ahmedabad-residential-property/buy-2bhk-east-facing-apartments-in-ahmedabad-city"
            }, {
                "label": "1 BHK South West Facing apartments in ahmedabad",
                "url": "ahmedabad-residential-property/buy-1bhk-south-west-facing-apartments-in-ahmedabad-city"
            }, {
                "label": "2 BHK South West Facing apartments in ahmedabad",
                "url": "ahmedabad-residential-property/buy-2bhk-south-west-facing-apartments-in-ahmedabad-city"
            }, {
                "label": "3 BHK North East Facing apartments in ahmedabad",
                "url": "ahmedabad-residential-property/buy-3bhk-north-east-facing-apartments-in-ahmedabad-city"
            }],
            "House for Sale in ahmedabad": [{
                "label": "List of Budget Independent Houses/Villas for sale in ahmedabad",
                "url": "ahmedabad-residential-property/buy-budget-independent-house-villa"
            }, {
                "label": "List of Houses/Villas for sale from Owners in ahmedabad",
                "url": "ahmedabad-residential-property/buy-independent-house-villa-from-owners"
            }, {
                "label": "List of Luxury House/Villas for sale in ahmedabad",
                "url": "ahmedabad-residential-property/buy-luxury-independent-house"
            }, {
                "label": "List of Under Construction Houses/Villas for Sale in ahmedabad",
                "url": "ahmedabad-residential-property/buy-under-construction-independent-house-villa"
            }, {
                "label": "List of Ready to Move Houses/Villas for sale in ahmedabad",
                "url": "ahmedabad-residential-property/buy-ready-to-move-in-independent-house"
            }],
            "BHK Apartments for rent in ahmedabad": [{
                "label": "1 BHK Apartments for Rent in ahmedabad",
                "url": "ahmedabad-residential-property/rent-1bhk-apartments-in-ahmedabad-city"
            }, {
                "label": "2 BHK Apartments for Rent in ahmedabad",
                "url": "ahmedabad-residential-property/rent-2bhk-apartments-in-ahmedabad-city"
            }, {
                "label": "3 BHK Apartments for Rent in ahmedabad",
                "url": "ahmedabad-residential-property/rent-3bhk-apartments-in-ahmedabad-city"
            }],
            "Rent BHK Property in ahmedabad": [{
                "label": "1 BHK Rental Properties in ahmedabad",
                "url": "ahmedabad-residential-property/rent-1bhk-property-in-ahmedabad-city"
            }, {
                "label": "2 BHK Rental Properties in ahmedabad",
                "url": "ahmedabad-residential-property/rent-2bhk-property-in-ahmedabad-city"
            }, {
                "label": "3 BHK Rental Properties in ahmedabad",
                "url": "ahmedabad-residential-property/rent-3bhk-property-in-ahmedabad-city"
            }]
        },
        "bread-crum": {
            "1": [{
                "label": "Property in Bangalore Central",
                "url": "/real-estate-bangalore-property",
            }, {
                "label": "Commercial Land in",
                "url": "bangalore-residential-property/buy-land-in-bangalore-central-10006"
            }, {
                "label": "Ready to move Apartments for sale in",
                "url": "bangalore-residential-property/buy-ready-to-move-in-apartments-in-bangalore-central-10006"
            }],
            "2": [{
                "label": "kr puram properties",
                "url": "/bangalore-property/kr-puram-flats-for-sale-50167"
            }, {
                "label": "Commercial Land in",
                "url": "bangalore-residential-property/buy-land-in-bangalore-central-10006"
            }, {
                "label": "Ready to move Apartments for sale in",
                "url": "bangalore-residential-property/buy-ready-to-move-in-apartments-in-bangalore-central-10006"
            }, {
                "label": "2",
                "url": "bangalore-residential-property/buy-ready-to-move-in-apartments-in-bangalore-central-10006"
            }],
            "3": [{
                "url": "/bangalore/skylark-ithaca-in-kr-puram-642535",
                "label": "skylark ithaca",
            }, {
                "label": "Commercial Land in",
                "url": "bangalore-residential-property/buy-land-in-bangalore-central-10006"
            }, {
                "label": "Ready to move Apartments for sale in",
                "url": "bangalore-residential-property/buy-ready-to-move-in-apartments-in-bangalore-central-10006"
            }, {
                "label": "2",
                "url": "bangalore-residential-property/buy-ready-to-move-in-apartments-in-bangalore-central-10006"
            }],
            "4": [{
                "name": "1011 sq ft 2 bhk 2t apartments",
                "url": "#",
                "title": "1011 sq ft 2 bhk 2t apartments",
                "label": "Commercial Land in",
                "category": "city"
            }, {
                "label": "Commercial Land in",
                "url": "bangalore-residential-property/buy-land-in-bangalore-central-10006"
            }, {
                "label": "Ready to move Apartments for sale in",
                "url": "bangalore-residential-property/buy-ready-to-move-in-apartments-in-bangalore-central-10006"
            }, {
                "label": "2",
                "url": "bangalore-residential-property/buy-ready-to-move-in-apartments-in-bangalore-central-10006"
            }]
        },
        "footer": {
            "footer-label": {
                "builders-city": "Builders in City",
                "suburb-listing-buy": "Buy Property in Popular Suburbs",
                "builders": "Builders in (other)city",
                "brokers": "Brokers in City",
                "suburb-taxonomy": "Suburb taxonomy",
                "locality": "Popular Localities",
                "price-trend-suburb": "Property Rates/trends in Suburb",
                "suburb-listing-rent": "Rental Property in Popular Suburbs",
                "city-listing-rent": "Rental Property",
                "city-listing-buy": "Buy Property",
                "brokers-locality": "Brokers in Locality",
                "locality-listing-buy": "Buy Property in Popular Localities",
                "builder-taxonomy": "Builder taxonomy",
                "locality-listing-rent": "Rental Property in Popular Localities",
                "city-taxonomy": "Property Type",
                "locality-taxonomy": "Locality taxonomy",
                "brokers-suburb": "Brokers in Suburb",
                "suburb": "Popular Suburbs",
                "price-trend-city": "Property Rates/trends in City",
                "city-overview": "Real Estate in India",
                "price-trend-locality": "Property Rates/trends in Locality"
            },
            "global": {
                "city-taxonomy": [{
                    "label": "Ready to move flats in Greater Noida",
                    "url": "greater-noida-residential-property/buy-ready-to-move-flats"
                }, {
                    "label": "Upcoming property in Greater Noida",
                    "url": "greater-noida-residential-property/buy-upcoming-property"
                }, {
                    "label": "Villas for sale in Greater Noida",
                    "url": "greater-noida-residential-property/buy-villas-sale"
                }, {
                    "label": "Affordable flats in Greater Noida",
                    "url": "greater-noida-residential-property/buy-affordable-flats"
                }, {
                    "label": "Under construction property in Greater Noida",
                    "url": "greater-noida-residential-property/buy-under-construction-property"
                }, {
                    "label": "Rent Ready To move flats in Greater Noida",
                    "url": "greater-noida-residential-property/ready-to-move-flats-rent"
                }, {
                    "label": "Rent Upcoming property in Greater Noida",
                    "url": "greater-noida-residential-property/upcoming-property-rent"
                }, {
                    "label": "Rent villas in Greater Noida",
                    "url": "greater-noida-residential-property/villas-rent"
                }, {
                    "label": "Rent Affordable flats in Greater Noida",
                    "url": "greater-noida-residential-property/affordable-flats-rent"
                }, {
                    "label": "Rent upcoming flats for sale in Greater Noida",
                    "url": "greater-noida-residential-property/upcoming-flats-for-rent"
                }, {
                    "label": "Rent under construction property in Greater Noida",
                    "url": "greater-noida-residential-property/under-construction-property-rent"
                }, {
                    "label": "",
                    "url": "greater-noida-residential-project/luxury-projects-in-greater-noida-city"
                }, {
                    "label": "",
                    "url": "greater-noida-residential-project/affordable-projects-in-greater-noida-city"
                }, {
                    "label": "",
                    "url": "greater-noida-residential-project/new-launch-projects-in-greater-noida-city"
                }, {
                    "label": "East facing apartments for rent in Greater Noida",
                    "url": "greater-noida-residential-property/rent-east-facing-apartments-in-greater-noida-city"
                }, {
                    "label": "West facing apartments for rent in Greater Noida",
                    "url": "greater-noida-residential-property/rent-west-facing-apartments-in-greater-noida-city"
                }],
                "city-overview": [{
                    "label": "Property in Mumbai",
                    "url": "real-estate-mumbai-property"
                }, {
                    "label": "Property in Bangalore",
                    "url": "real-estate-bangalore-property"
                }, {
                    "label": "Property in Pune",
                    "url": "real-estate-pune-property"
                }, {
                    "label": "Property in Delhi",
                    "url": "real-estate-delhi-property"
                }, {
                    "label": "Property in Noida",
                    "url": "real-estate-noida-property"
                }, {
                    "label": "Property in Ghaziabad",
                    "url": "real-estate-ghaziabad-property"
                }, {
                    "label": "Property in Gurgaon",
                    "url": "real-estate-gurgaon-property"
                }, {
                    "label": "Property in Mohali",
                    "url": "real-estate-mohali-property"
                }, {
                    "label": "Property in Hyderabad",
                    "url": "real-estate-hyderabad-property"
                }, {
                    "label": "Property in Chennai",
                    "url": "real-estate-chennai-property"
                }, {
                    "label": "Property in Kolkata",
                    "url": "real-estate-kolkata-property"
                }, {
                    "label": "Property in Chandigarh",
                    "url": "real-estate-chandigarh-property"
                }, {
                    "label": "Property in Lucknow",
                    "url": "real-estate-lucknow-property"
                }, {
                    "label": "Property in Faridabad",
                    "url": "real-estate-faridabad-property"
                }, {
                    "label": "Property in Ahmedabad",
                    "url": "real-estate-ahmedabad-property"
                }, {
                    "label": "Property in Nagpur",
                    "url": "real-estate-nagpur-property"
                }, {
                    "label": "Property in Greater Noida",
                    "url": "real-estate-greater-noida-property"
                }, {
                    "label": "Property in Jaipur",
                    "url": "real-estate-jaipur-property"
                }, {
                    "label": "Property in Bhopal",
                    "url": "real-estate-bhopal-property"
                }, {
                    "label": "Property in Indore",
                    "url": "real-estate-indore-property"
                }, {
                    "label": "all cities",
                    "url": "all-cities-overview"
                }],
                "city-listing-rent": [{
                    "label": "Rental Properties in Greater Noida",
                    "url": "greater-noida-residential-property/rent-property-in-greater-noida-city"
                }],
                "city-listing-buy": [{
                    "label": "Buy Greater Noida Properties",
                    "url": "greater-noida-residential-property/buy-property-in-greater-noida-city"
                }]
            },
            "seo": {
                "suburb-listing-buy": [{
                    "label": "Property in Greater Noida Central",
                    "url": "greater-noida/greater-noida-central-property-11076"
                }, {
                    "label": "Property in Other",
                    "url": "greater-noida/other-property-11073"
                }, {
                    "label": "Property in Yamuna Expressway",
                    "url": "greater-noida/yamuna-expressway-property-11080"
                }, {
                    "label": "Property in Greater Noida North",
                    "url": "greater-noida/greater-noida-north-property-11078"
                }, {
                    "label": "Property in Dadri Surajpur Chhalera Road",
                    "url": "greater-noida/dadri-surajpur-chhalera-road-property-11075"
                }, {
                    "label": "Property in Air Force Station Dadri",
                    "url": "greater-noida/air-force-station-dadri-property-11074"
                }, {
                    "label": "Property in Greater Noida East",
                    "url": "greater-noida/greater-noida-east-property-11077"
                }, {
                    "label": "Property in NH 91",
                    "url": "greater-noida/nh-91-property-11079"
                }],
                "locality-listing-buy": [{
                    "label": "Property in Greater Noida West Greater Noida",
                    "url": "greater-noida-property/greater-noida-west-flats-for-sale-51768"
                }, {
                    "label": "Property in Omicron Greater Noida",
                    "url": "greater-noida-property/omicron-flats-for-sale-51093"
                }, {
                    "label": "Property in Pari Chowk Greater Noida",
                    "url": "greater-noida-property/pari-chowk-flats-for-sale-51264"
                }, {
                    "label": "Property in Zeta Greater Noida",
                    "url": "greater-noida-property/zeta-flats-for-sale-51372"
                }, {
                    "label": "Property in Knowledge Park Greater Noida",
                    "url": "greater-noida-property/knowledge-park-flats-for-sale-51366"
                }, {
                    "label": "Property in Knowledge Park II Greater Noida",
                    "url": "greater-noida-property/knowledge-park-ii-flats-for-sale-54093"
                }, {
                    "label": "Property in ETA 2 Greater Noida",
                    "url": "greater-noida-property/eta-2-flats-for-sale-50401"
                }, {
                    "label": "Property in PI Greater Noida",
                    "url": "greater-noida-property/pi-flats-for-sale-51246"
                }, {
                    "label": "Property in CHI 5 Greater Noida",
                    "url": "greater-noida-property/chi-5-flats-for-sale-51221"
                }, {
                    "label": "Property in Taj Expressway Greater Noida",
                    "url": "greater-noida-property/taj-expressway-flats-for-sale-54149"
                }, {
                    "label": "Property in Sector 4 Greater Noida",
                    "url": "greater-noida-property/sector-4-flats-for-sale-85871"
                }, {
                    "label": "Property in Shahberi Greater Noida",
                    "url": "greater-noida-property/shahberi-flats-for-sale-84143"
                }, {
                    "label": "Property in Ansal Plaza Greater Noida",
                    "url": "greater-noida-property/ansal-plaza-flats-for-sale-54047"
                }, {
                    "label": "Property in Swarn Nagri Greater Noida",
                    "url": "greater-noida-property/swarn-nagri-flats-for-sale-51365"
                }, {
                    "label": "Property in Omicron 3 Greater Noida",
                    "url": "greater-noida-property/omicron-3-flats-for-sale-51596"
                }, {
                    "label": "all localities Rent for Greater Noida",
                    "url": "greater-noida/all-localities-rent"
                }],
                "locality-listing-rent": [{
                    "label": "Flats for rent in Shahberi Greater Noida",
                    "url": "greater-noida-property/shahberi-rental-flats-84143"
                }, {
                    "label": "Flats for rent in PI Greater Noida",
                    "url": "greater-noida-property/pi-rental-flats-51246"
                }, {
                    "label": "Flats for rent in Alpha 2 Greater Noida",
                    "url": "greater-noida-property/alpha-2-rental-flats-51371"
                }, {
                    "label": "Flats for rent in Swarn Nagri Greater Noida",
                    "url": "greater-noida-property/swarn-nagri-rental-flats-51365"
                }, {
                    "label": "Flats for rent in Sector 55 Greater Noida",
                    "url": "greater-noida-property/sector-55-rental-flats-54131"
                }, {
                    "label": "Flats for rent in Omega Greater Noida",
                    "url": "greater-noida-property/omega-rental-flats-51421"
                }, {
                    "label": "Flats for rent in Zeta Greater Noida",
                    "url": "greater-noida-property/zeta-rental-flats-51372"
                }, {
                    "label": "Flats for rent in CHI 5 Greater Noida",
                    "url": "greater-noida-property/chi-5-rental-flats-51221"
                }, {
                    "label": "Flats for rent in Beta 2 Greater Noida",
                    "url": "greater-noida-property/beta-2-rental-flats-51369"
                }, {
                    "label": "Flats for rent in Omicron Greater Noida",
                    "url": "greater-noida-property/omicron-rental-flats-51093"
                }, {
                    "label": "Flats for rent in Greater Noida West Greater Noida",
                    "url": "greater-noida-property/greater-noida-west-rental-flats-51768"
                }, {
                    "label": "Flats for rent in Gamma 2 Greater Noida",
                    "url": "greater-noida-property/gamma-2-rental-flats-51363"
                }, {
                    "label": "Flats for rent in UPSIDC Surajpur Site Greater Noida",
                    "url": "greater-noida-property/upsidc-surajpur-site-rental-flats-52326"
                }, {
                    "label": "Flats for rent in Knowledge Park Greater Noida",
                    "url": "greater-noida-property/knowledge-park-rental-flats-51366"
                }, {
                    "label": "Flats for rent in Jalvayu Vihar Greater Noida",
                    "url": "greater-noida-property/jalvayu-vihar-rental-flats-54088"
                }, {
                    "label": "all localities Rent for Greater Noida",
                    "url": "greater-noida/all-localities-rent"
                }],
                "suburb-listing-rent": [{
                    "label": "Rental Apartments in Greater Noida Central",
                    "url": "greater-noida/greater-noida-central-flats-for-rent-11076"
                }, {
                    "label": "Rental Apartments in Other",
                    "url": "greater-noida/other-flats-for-rent-11073"
                }, {
                    "label": "Rental Apartments in Yamuna Expressway",
                    "url": "greater-noida/yamuna-expressway-flats-for-rent-11080"
                }, {
                    "label": "Rental Apartments in Greater Noida North",
                    "url": "greater-noida/greater-noida-north-flats-for-rent-11078"
                }, {
                    "label": "Rental Apartments in NH 91",
                    "url": "greater-noida/nh-91-flats-for-rent-11079"
                }, {
                    "label": "Rental Apartments in Air Force Station Dadri",
                    "url": "greater-noida/air-force-station-dadri-flats-for-rent-11074"
                }, {
                    "label": "Rental Apartments in Dadri Surajpur Chhalera Road",
                    "url": "greater-noida/dadri-surajpur-chhalera-road-flats-for-rent-11075"
                }, {
                    "label": "Rental Apartments in Greater Noida East",
                    "url": "greater-noida/greater-noida-east-flats-for-rent-11077"
                }]
            }
        },
        "meta": {
            "templateId": "MAKAAN_PROJECT_OVERVIEW",
            "title": "Primrose Ryne in CHI 5, Greater Noida - Flats for Sale in Primrose Ryne",
            "description": "Primrose Ryne, a new residential apartments/flats available for sale in CHI 5 Greater Noida. Get detailed project Information like Floor Plan, Amenities, Location Map etc. online only on Makaan.com.",
            "keywords": "Primrose Ryne, Primrose Ryne CHI 5, Primrose Ryne Greater Noida, Primrose Ryne Price, Primrose Ryne Review",
            "h1": "Primrose Ryne",
            "h2": "",
            "h3": "",
            "h4": "",
            "urlLabel": "Primrose Ryne",
            "domainId": 1,
            "pageDescription": "",
            "robots": "index, follow",
            "country": "India",
            "Author": "Makaan.com",
            "googlebot": "all",
            "og:url": "https://www.makaan.com/greater-noida/primrose-ryne-in-chi-5-507062",
            "og:description": "Primrose Ryne, a new residential apartments/flats available for sale in CHI 5 Greater Noida. Get detailed project Information like Floor Plan, Amenities, Location Map etc. online only on Makaan.com.",
            "og:site_name": "Makaan.com",
            "og:title": "Primrose Ryne in CHI 5, Greater Noida - Flats for Sale in Primrose Ryne",
            "og:type": "website",
            "og:image:url": "",
            "og:image:secure_url": "",
            "viewport": "width=device-width, initial-scale=1.0, maximum-scale=5.0, user-scalable=no",
            "metaTags": {
                "country": "India",
                "og:type": "website",
                "og:site_name": "Makaan.com",
                "og:image:url": "",
                "og:title": "Primrose Ryne in CHI 5, Greater Noida - Flats for Sale in Primrose Ryne",
                "googlebot": "all",
                "og:description": "Primrose Ryne, a new residential apartments/flats available for sale in CHI 5 Greater Noida. Get detailed project Information like Floor Plan, Amenities, Location Map etc. online only on Makaan.com.",
                "og:image:secure_url": "",
                "viewport": "width=device-width, initial-scale=1.0, maximum-scale=5.0, user-scalable=no",
                "Author": "Makaan.com",
                "robots": "index, follow",
                "og:url": "https://www.makaan.com/greater-noida/primrose-ryne-in-chi-5-507062"
            }
        },
        "urlDetail": {
            "url": "greater-noida/primrose-ryne-in-chi-5-507062",
            "templateId": "MAKAAN_PROJECT_OVERVIEW",
            "sourceDomain": 1,
            "cityName": "greater noida",
            "cityId": 352,
            "projectId": 507062,
            "fallBackUrl": "greater-noida/primrose-ryne-in-chi-5-507062",
            "projectName": "ryne",
            "localityName": "chi 5",
            "builderName": "primrose",
            "urlPattern": "([^-][\\-\\w,\\.()]*[^-]?)/([^-][\\-\\w,\\.()]*[^-]?)-([^-][\\-\\w,\\.()]*[^-]?)-in-([^-][\\-\\w,\\.()]*[^-]?)-([1-9]\\d{5,7})",
            "bedroomString": "",
            "priceString": "",
            "listingFilter": "",
            "urlType": 15
        },
        "templateId": "MAKAAN_PROJECT_OVERVIEW",
        "statusCode": 200
    }
};
