module.exports = {
  statusCode: "2XX",
  version: "A",
  data: {
    id: 2030,
    status: "Active",
    rating: 0,
    companyId: 499,
    userId: 1277755,
    email: "paul.clarence@proptiger.com",
    company: {
      id: 499,
      type: "Broker",
      name: "Proptiger",
      cityId: 11,
      status: "Inactive",
      ptManagerId: 43,
      employeeNo: 0,
      makaanOnly: false,
      primaryEmail: "campaign.proptiger@gmail.com",
      createdAt: 1430122489000,
      updatedAt: 1449879963000,
      website: "www.proptiger.com"
    },
    companyCoverages: [
      
    ],
    user: {
      id: 1277755,
      fullName: "Paul J Clarence",
      registered: true,
      contactNumbers: [
        {
          id: 1306009,
          userId: 1277755,
          contactNumber: "01206799915",
          priority: 2,
          createdBy: 1277755,
          createdAt: 1438876416000,
          isVerified: false,
          isActive: true,
          isMobile: false
        },
        {
          id: 1306008,
          userId: 1277755,
          contactNumber: "9945530714",
          priority: 2,
          createdBy: 1277755,
          createdAt: 1438876416000,
          isVerified: false,
          isActive: true,
          isMobile: true
        },
        {
          id: 1561836,
          userId: 1277755,
          contactNumber: "919945530714",
          priority: 1,
          createdBy: 1277755,
          createdAt: 1442149063000,
          isVerified: false,
          isActive: true,
          isMobile: true
        }
      ],
      createdAt: 1438876416000,
      updatedAt: 1451013416000,
      email: "paul.clarence@proptiger.com",
      verified: false,
      userRoles: [
        {
          id: 187,
          userId: 1277755,
          roleId: 13,
          createdBy: 1223006,
          createdAt: 1438856616000,
          role: {
            id: 13,
            name: "Agent Sales",
            createdBy: 1,
            createdAt: 1430381225000
          }
        },
        {
          id: 2962,
          userId: 1277755,
          roleId: 19,
          createdBy: 1,
          createdAt: 1441185934000,
          role: {
            id: 19,
            name: "Recon Report Viewer-Bangalore",
            createdBy: 1,
            createdAt: 1438322625000
          }
        },
        {
          id: 3059,
          userId: 1277755,
          roleId: 17,
          createdBy: 1,
          createdAt: 1441202694000,
          role: {
            id: 17,
            name: "ListingAdmin",
            createdBy: 1,
            createdAt: 1432100607000
          }
        },
        {
          id: 188,
          userId: 1277755,
          roleId: 33,
          createdBy: 1223006,
          createdAt: 1438856616000,
          role: {
            id: 33,
            name: "CRM USER",
            createdBy: 1,
            createdAt: 1438322627000
          }
        },
        {
          id: 186,
          userId: 1277755,
          roleId: 10,
          createdBy: 1223006,
          createdAt: 1438856616000,
          role: {
            id: 10,
            name: "Manager Sales",
            createdBy: 1,
            createdAt: 1430381225000
          }
        }
      ],
      domainId: 2
    },
    parentId: 1212052,
    left: 1708,
    right: 2017,
    employeeCode: "080060",
    branchId: 11,
    departmentId: 3,
    joiningDate: 1304274600000,
    rootParentId: 1277716,
    updatedBy: 1223006,
    createdAt: 1398191400000,
    checkAddress: "off",
    rateOption: "auto",
    name: "Paul J Clarence",
    cityId: 2
  }
}