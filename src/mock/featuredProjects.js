module.exports = {
  "totalCount": 1,
  "statusCode": "2XX",
  "version": "A",
  "data": [
    {
      "projectId": 501682,
      "localityId": 50256,
      "locality": {
        "localityId": 50256,
        "suburb": {
          "id": 10003,
          "label": "SG Highway",
          "city": {
            "id": 1,
            "label": "Ahmedabad",
            "overviewUrl": "real-estate-ahmedabad-property"
          },
          "overviewUrl": "ahmedabad/sg-highway-real-estate-10003"
        },
        "label": "Near Vaishno Devi Circle On SG Highway",
        "overviewUrl": "ahmedabad/near-vaishno-devi-circle-on-sg-highway-real-estate-50256"
      },
      "name": "Water Lily",
      "imageURL": "https://content.makaan-ws.com/1/501682/297/adani-water-lily-elevation-1032021.jpeg",
      "overviewUrl": "ahmedabad/adani-water-lily-in-near-vaishno-devi-circle-on-sg-highway-501682",
      "minPrice": 8150000,
      "maxPrice": 25296320,
      "propertyUnitTypes": [
        "Apartment"
      ],
      "distinctBedrooms": [
        3,
        4,
        5
      ],
      "minResaleOrPrimaryPrice": 8150000.0,
      "maxResaleOrPrimaryPrice": 2.529632E7
  },{
    "projectId": 501682,
    "localityId": 50256,
    "locality": {
      "localityId": 50256,
      "suburb": {
        "id": 10003,
        "label": "SG Highway",
        "city": {
          "id": 1,
          "label": "Ahmedabad",
          "overviewUrl": "real-estate-ahmedabad-property"
        },
        "overviewUrl": "ahmedabad/sg-highway-real-estate-10003"
      },
      "label": "Near Vaishno Devi Circle On SG Highway",
      "overviewUrl": "ahmedabad/near-vaishno-devi-circle-on-sg-highway-real-estate-50256"
    },
    "name": "Water Lily",
    "imageURL": "https://content.makaan-ws.com/1/501682/297/adani-water-lily-elevation-1032021.jpeg",
    "overviewUrl": "ahmedabad/adani-water-lily-in-near-vaishno-devi-circle-on-sg-highway-501682",
    "minPrice": 8150000,
    "maxPrice": 25296320,
    "propertyUnitTypes": [
      "Apartment"
    ],
    "distinctBedrooms": [
      3,
      4,
      5
    ],
    "minResaleOrPrimaryPrice": 8150000.0,
    "maxResaleOrPrimaryPrice": 2.529632E7
    },{
      "projectId": 501682,
      "localityId": 50256,
      "locality": {
        "localityId": 50256,
        "suburb": {
          "id": 10003,
          "label": "SG Highway",
          "city": {
            "id": 1,
            "label": "Ahmedabad",
            "overviewUrl": "real-estate-ahmedabad-property"
          },
          "overviewUrl": "ahmedabad/sg-highway-real-estate-10003"
        },
        "label": "Near Vaishno Devi Circle On SG Highway",
        "overviewUrl": "ahmedabad/near-vaishno-devi-circle-on-sg-highway-real-estate-50256"
      },
      "name": "Water Lily",
      "imageURL": "https://content.makaan-ws.com/1/501682/297/adani-water-lily-elevation-1032021.jpeg",
      "overviewUrl": "ahmedabad/adani-water-lily-in-near-vaishno-devi-circle-on-sg-highway-501682",
      "minPrice": 8150000,
      "maxPrice": 25296320,
      "propertyUnitTypes": [
        "Apartment"
      ],
      "distinctBedrooms": [
        3,
        4,
        5
      ],
      "minResaleOrPrimaryPrice": 8150000.0,
      "maxResaleOrPrimaryPrice": 2.529632E7
    },{
      "projectId": 501682,
      "localityId": 50256,
      "locality": {
        "localityId": 50256,
        "suburb": {
          "id": 10003,
          "label": "SG Highway",
          "city": {
            "id": 1,
            "label": "Ahmedabad",
            "overviewUrl": "real-estate-ahmedabad-property"
          },
          "overviewUrl": "ahmedabad/sg-highway-real-estate-10003"
        },
        "label": "Near Vaishno Devi Circle On SG Highway",
        "overviewUrl": "ahmedabad/near-vaishno-devi-circle-on-sg-highway-real-estate-50256"
      },
      "name": "Water Lily",
      "imageURL": "https://content.makaan-ws.com/1/501682/297/adani-water-lily-elevation-1032021.jpeg",
      "overviewUrl": "ahmedabad/adani-water-lily-in-near-vaishno-devi-circle-on-sg-highway-501682",
      "minPrice": 8150000,
      "maxPrice": 25296320,
      "propertyUnitTypes": [
        "Apartment"
      ],
      "distinctBedrooms": [
        3,
        4,
        5
      ],
      "minResaleOrPrimaryPrice": 8150000.0,
      "maxResaleOrPrimaryPrice": 2.529632E7
    },{
      "projectId": 501682,
      "localityId": 50256,
      "locality": {
        "localityId": 50256,
        "suburb": {
          "id": 10003,
          "label": "SG Highway",
          "city": {
            "id": 1,
            "label": "Ahmedabad",
            "overviewUrl": "real-estate-ahmedabad-property"
          },
          "overviewUrl": "ahmedabad/sg-highway-real-estate-10003"
        },
        "label": "Near Vaishno Devi Circle On SG Highway",
        "overviewUrl": "ahmedabad/near-vaishno-devi-circle-on-sg-highway-real-estate-50256"
      },
      "name": "Water Lily",
      "imageURL": "https://content.makaan-ws.com/1/501682/297/adani-water-lily-elevation-1032021.jpeg",
      "overviewUrl": "ahmedabad/adani-water-lily-in-near-vaishno-devi-circle-on-sg-highway-501682",
      "minPrice": 8150000,
      "maxPrice": 25296320,
      "propertyUnitTypes": [
        "Apartment"
      ],
      "distinctBedrooms": [
        3,
        4,
        5
      ],
      "minResaleOrPrimaryPrice": 8150000.0,
      "maxResaleOrPrimaryPrice": 2.529632E7
    }]
};
