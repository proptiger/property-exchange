module.exports = {
  "totalCount": 2,
  "statusCode": "2XX",
  "version": "A",
  "data": [
    {
      "id": 2150,
      "userId": 5711549,
      "searchQuery": "{\"filters\":{\"and\":[{\"geoDistance\":{\"geo\":{\"distance\":3,\"lat\":18.9723,\"lon\":73.0237}}},{\"equal\":{\"bedrooms\":[\"2\",\"3\"]}},{\"range\":{\"primaryOrResaleBudget\":{\"from\":\"7500000\",\"to\":\"12000000\"}}},{\"range\":{\"possessionDate\":{\"from\":1444826380323,\"to\":1460378380323}}}]},\"paging\":{\"start\":0,\"rows\":9999},\"fields\":[\"name\",\"avgPriceRisePercentage\",\"avgPriceRiseMonths\",\"imageURL\",\"projectStatus\",\"address\",\"distinctBedrooms\",\"propertyUnitTypes\",\"minPrice\",\"maxPrice\",\"offers\",\"offer\",\"offerHeading\",\"offerDesc\",\"minResalePrice\",\"maxResalePrice\",\"possessionDate\",\"minPricePerUnitArea\",\"propertySizeMeasure\",\"minSize\",\"maxSize\",\"latitude\",\"longitude\",\"projectId\",\"localityId\",\"locality\",\"cityId\",\"builder\",\"URL\",\"authorized\",\"avgPricePerUnitArea\",\"derivedAvailability\",\"totalUnits\",\"launchDate\",\"label\",\"dominantUnitType\",\"resalePricePerUnitArea\",\"minResaleOrPrimaryPrice\",\"maxResaleOrPrimaryPrice\",\"safetyScore\",\"livabilityScore\",\"resaleEnquiry\",\"isResale\"]}",
      "name": "abcd",
      "createdDate": 1452062807000
    },
    {
      "id": 2151,
      "userId": 5711549,
      "searchQuery": "{\"filters\":{\"and\":[{\"geoDistance\":{\"geo\":{\"distance\":4,\"lat\":18.9723,\"lon\":73.0237}}},{\"equal\":{\"bedrooms\":[\"2\",\"3\"]}},{\"range\":{\"primaryOrResaleBudget\":{\"from\":\"7500000\",\"to\":\"12000000\"}}},{\"range\":{\"possessionDate\":{\"from\":1444826380323,\"to\":1460378380323}}}]},\"paging\":{\"start\":0,\"rows\":9999},\"fields\":[\"name\",\"avgPriceRisePercentage\",\"avgPriceRiseMonths\",\"imageURL\",\"projectStatus\",\"address\",\"distinctBedrooms\",\"propertyUnitTypes\",\"minPrice\",\"maxPrice\",\"offers\",\"offer\",\"offerHeading\",\"offerDesc\",\"minResalePrice\",\"maxResalePrice\",\"possessionDate\",\"minPricePerUnitArea\",\"propertySizeMeasure\",\"minSize\",\"maxSize\",\"latitude\",\"longitude\",\"projectId\",\"localityId\",\"locality\",\"cityId\",\"builder\",\"URL\",\"authorized\",\"avgPricePerUnitArea\",\"derivedAvailability\",\"totalUnits\",\"launchDate\",\"label\",\"dominantUnitType\",\"resalePricePerUnitArea\",\"minResaleOrPrimaryPrice\",\"maxResaleOrPrimaryPrice\",\"safetyScore\",\"livabilityScore\",\"resaleEnquiry\",\"isResale\"]}",
      "name": "12345",
      "createdDate": 1452062827000
    }
  ]
};