module.exports = {
  "totalCount": 5,
  "statusCode": "2XX",
  "version": "A",
  "data": {
    "totalCount": 5,
    "results": [
      {
        "id": 1796467,
        "clientId": 4627545,
        "cityId": 18,
        "countryId": 1,
        "clientTypeId": 1,
        "clientType": {
          "id": 1,
          "clientType": "Investor"
        },
        "notes": "take",
        "updatedBy": 131108,
        "flexibleBudget": false,
        "saleTypeId": 1,
        "saleType": {
          "id": 1,
          "saleType": "Primary",
          "marketplaceOnly": false
        },
        "latestLeadAssigment": 1956710,
        "createdAt": 1451908639000,
        "updatedAt": 1452005887000,
        "propertyTypeId": 1,
        "budgetNotProvided": true,
        "clientFirstEnquiryTime": 1451908639000,
        "directlyAssignToSales": true,
        "companyId": 499,
        "leadAssignments": null,
        "enquiries": null,
        "propertyRequirements": [
          {
            "id": 110359712,
            "leadId": 1796467,
            "minBudget": 123,
            "maxBudget": 123,
            "minSize": 123,
            "maxSize": 123,
            "areaUnitTypeId": 1,
            "bedroom": 2,
            "projectId": 500001,
            "localityId": 50280,
            "optionId": 1,
            "listingId": 1234,
            "latitude": 123.0,
            "longitude": 1234.0,
            "radiusKm": 123,
            "updatedBy": 131108,
            "createdAt": 1451908639000,
            "updatedAt": 1452005071000
          },
          {
            "id": 110359722,
            "leadId": 1796467,
            "areaUnitTypeId": 1,
            "projectId": 668662,
            "localityId": 50063,
            "updatedBy": 131108,
            "createdAt": 1452005677000,
            "updatedAt": 1452006186000
          }
        ],
        "homeloanRequirements": null,
        "leadAssignment": {
          "id": 1956710,
          "leadId": 1796467,
          "agentId": 1277699,
          "statusId": 14,
          "status": {
            "id": 14,
            "status": "Site Visit Scheduled",
            "open": true,
            "index": true,
            "assignmentPriority": 11,
            "displayOrder": 8,
            "isPresalesStatus": false,
            "isSalesStatus": true,
            "isHomeloanStatus": false
          },
          "followUpTime": 1452265450000,
          "clientPreferredTime": true,
          "lastActionId": 17870188,
          "lastActionHistory": null,
          "createdAt": 1451908640000,
          "updatedAt": 1452006236000,
          "leadSellerAttributeId": 1,
          "leadSellerAttribute": null
        },
        "requirements": null
      },
      {
        "id": 1796468,
        "clientId": 4627545,
        "cityId": 20,
        "countryId": 1,
        "clientTypeId": 1,
        "clientType": {
          "id": 1,
          "clientType": "Investor"
        },
        "notes": "take",
        "flexibleBudget": true,
        "saleTypeId": 1,
        "saleType": {
          "id": 1,
          "saleType": "Primary",
          "marketplaceOnly": false
        },
        "latestLeadAssigment": 1956711,
        "createdAt": 1451909751000,
        "updatedAt": 1451909752000,
        "propertyTypeId": 1,
        "clientFirstEnquiryTime": 1451909751000,
        "directlyAssignToSales": true,
        "companyId": 499,
        "leadAssignments": null,
        "enquiries": null,
        "propertyRequirements": [
          {
            "id": 110359713,
            "leadId": 1796468,
            "minBudget": 123,
            "maxBudget": 123,
            "minSize": 123,
            "maxSize": 123,
            "bedroom": 2,
            "projectId": 500001,
            "localityId": 50280,
            "optionId": 1,
            "listingId": 1234,
            "latitude": 123.0,
            "longitude": 1234.0,
            "radiusKm": 123,
            "createdAt": 1451909752000,
            "updatedAt": 1451909752000
          }
        ],
        "homeloanRequirements": null,
        "leadAssignment": {
          "id": 1956711,
          "leadId": 1796468,
          "agentId": 1277699,
          "statusId": 2,
          "status": {
            "id": 2,
            "status": "New",
            "open": true,
            "index": true,
            "assignmentPriority": 15,
            "displayOrder": 1,
            "isPresalesStatus": false,
            "isSalesStatus": true,
            "isHomeloanStatus": true
          },
          "followUpTime": 1451909752000,
          "createdAt": 1451909752000,
          "updatedAt": 1451909752000,
          "leadSellerAttributeId": 1,
          "leadSellerAttribute": null
        },
        "requirements": null
      },
      {
        "id": 1796469,
        "clientId": 4627545,
        "cityId": 12,
        "countryId": 1,
        "clientTypeId": 1,
        "clientType": {
          "id": 1,
          "clientType": "Investor"
        },
        "notes": "take",
        "flexibleBudget": true,
        "saleTypeId": 1,
        "saleType": {
          "id": 1,
          "saleType": "Primary",
          "marketplaceOnly": false
        },
        "latestLeadAssigment": 1956712,
        "createdAt": 1451909761000,
        "updatedAt": 1451909762000,
        "propertyTypeId": 1,
        "clientFirstEnquiryTime": 1451909761000,
        "directlyAssignToSales": true,
        "companyId": 499,
        "leadAssignments": null,
        "enquiries": null,
        "propertyRequirements": [
          {
            "id": 110359714,
            "leadId": 1796469,
            "minBudget": 123,
            "maxBudget": 123,
            "minSize": 123,
            "maxSize": 123,
            "bedroom": 2,
            "projectId": 500001,
            "localityId": 50280,
            "optionId": 1,
            "listingId": 1234,
            "latitude": 123.0,
            "longitude": 1234.0,
            "radiusKm": 123,
            "createdAt": 1451909761000,
            "updatedAt": 1451909762000
          }
        ],
        "homeloanRequirements": null,
        "leadAssignment": {
          "id": 1956712,
          "leadId": 1796469,
          "agentId": 1277699,
          "statusId": 2,
          "status": {
            "id": 2,
            "status": "New",
            "open": true,
            "index": true,
            "assignmentPriority": 15,
            "displayOrder": 1,
            "isPresalesStatus": false,
            "isSalesStatus": true,
            "isHomeloanStatus": true
          },
          "followUpTime": 1451909762000,
          "createdAt": 1451909762000,
          "updatedAt": 1451909762000,
          "leadSellerAttributeId": 1,
          "leadSellerAttribute": null
        },
        "requirements": null
      },
      {
        "id": 1796470,
        "clientId": 4627545,
        "cityId": 2,
        "countryId": 1,
        "clientTypeId": 1,
        "clientType": {
          "id": 1,
          "clientType": "Investor"
        },
        "notes": "take",
        "flexibleBudget": true,
        "saleTypeId": 1,
        "saleType": {
          "id": 1,
          "saleType": "Primary",
          "marketplaceOnly": false
        },
        "latestLeadAssigment": 1956713,
        "createdAt": 1451909792000,
        "updatedAt": 1451909792000,
        "propertyTypeId": 1,
        "clientFirstEnquiryTime": 1451909792000,
        "directlyAssignToSales": true,
        "companyId": 499,
        "leadAssignments": null,
        "enquiries": null,
        "propertyRequirements": [
          {
            "id": 110359715,
            "leadId": 1796470,
            "minBudget": 123,
            "maxBudget": 123,
            "minSize": 123,
            "maxSize": 123,
            "bedroom": 2,
            "projectId": 500001,
            "localityId": 50280,
            "optionId": 1,
            "listingId": 1234,
            "latitude": 123.0,
            "longitude": 1234.0,
            "radiusKm": 123,
            "createdAt": 1451909792000,
            "updatedAt": 1451909792000
          }
        ],
        "homeloanRequirements": null,
        "leadAssignment": {
          "id": 1956713,
          "leadId": 1796470,
          "agentId": 1277699,
          "statusId": 2,
          "status": {
            "id": 2,
            "status": "New",
            "open": true,
            "index": true,
            "assignmentPriority": 15,
            "displayOrder": 1,
            "isPresalesStatus": false,
            "isSalesStatus": true,
            "isHomeloanStatus": true
          },
          "followUpTime": 1451909792000,
          "createdAt": 1451909792000,
          "updatedAt": 1451909792000,
          "leadSellerAttributeId": 1,
          "leadSellerAttribute": null
        },
        "requirements": null
      },
      {
        "id": 1796471,
        "clientId": 4627545,
        "cityId": 11,
        "countryId": 1,
        "clientTypeId": 1,
        "clientType": {
          "id": 1,
          "clientType": "Investor"
        },
        "notes": "take",
        "flexibleBudget": true,
        "saleTypeId": 1,
        "saleType": {
          "id": 1,
          "saleType": "Primary",
          "marketplaceOnly": false
        },
        "latestLeadAssigment": 1956714,
        "createdAt": 1451909926000,
        "updatedAt": 1451909926000,
        "propertyTypeId": 1,
        "clientFirstEnquiryTime": 1451909926000,
        "directlyAssignToSales": true,
        "companyId": 200,
        "leadAssignments": null,
        "enquiries": null,
        "propertyRequirements": [
          {
            "id": 110359716,
            "leadId": 1796471,
            "minBudget": 123,
            "maxBudget": 123,
            "minSize": 123,
            "maxSize": 123,
            "bedroom": 2,
            "projectId": 500001,
            "localityId": 50280,
            "optionId": 1,
            "listingId": 1234,
            "latitude": 123.0,
            "longitude": 1234.0,
            "radiusKm": 123,
            "createdAt": 1451909926000,
            "updatedAt": 1451909926000
          }
        ],
        "homeloanRequirements": null,
        "leadAssignment": {
          "id": 1956714,
          "leadId": 1796471,
          "agentId": 1277699,
          "statusId": 2,
          "status": {
            "id": 2,
            "status": "New",
            "open": true,
            "index": true,
            "assignmentPriority": 15,
            "displayOrder": 1,
            "isPresalesStatus": false,
            "isSalesStatus": true,
            "isHomeloanStatus": true
          },
          "followUpTime": 1451909926000,
          "createdAt": 1451909926000,
          "updatedAt": 1451909926000,
          "leadSellerAttributeId": 1,
          "leadSellerAttribute": null
        },
        "requirements": null
      }
    ]
  }
}