module.exports = {
    "statusCode": "2XX",
    "version": "A",
    "data": {
        "configuration": {
            "Primary": {
                "Apartment": {
                    "0": [],
                    "7500000": [],
                    "15000000": [],
                    "2500000": [{
                        "company": {
                            "id": 0,
                            "makaanOnly": false
                        },
                        "companySeller": {
                            "id": 0,
                            "company": {
                                "id": 132535,
                                "makaanOnly": false,
                                "name": "PropTiger.com",
                                "type": "Broker"
                            },
                            "left": 0,
                            "right": 0,
                            "checkAddress": "off",
                            "rateOption": "auto"
                        },
                        "expiryAt": 1452946289037,
                        "id": 566930,
                        "property": {
                            "propertyId": 0,
                            "bedrooms": 1,
                            "bathrooms": 0,
                            "halls": 0,
                            "storeRoom": 0,
                            "measure": "sq ft",
                            "studyRoom": 0,
                            "balcony": 0,
                            "displayCarpetArea": 0,
                            "penthouse": false,
                            "studio": false,
                            "isPropertySoldOut": false
                        },
                        "relevanceScore": 0.0
                    }, {
                        "company": {
                            "id": 0,
                            "makaanOnly": false
                        },
                        "companySeller": {
                            "id": 0,
                            "company": {
                                "id": 132535,
                                "makaanOnly": false,
                                "name": "PropTiger.com",
                                "type": "Broker"
                            },
                            "left": 0,
                            "right": 0,
                            "checkAddress": "off",
                            "rateOption": "auto"
                        },
                        "expiryAt": 1452946289037,
                        "id": 566929,
                        "property": {
                            "propertyId": 0,
                            "bedrooms": 1,
                            "bathrooms": 0,
                            "halls": 0,
                            "storeRoom": 0,
                            "measure": "sq ft",
                            "studyRoom": 0,
                            "balcony": 0,
                            "displayCarpetArea": 0,
                            "penthouse": false,
                            "studio": false,
                            "isPropertySoldOut": false
                        },
                        "relevanceScore": 0.0
                    }],
                    "10000000": [],
                    "5000000": [],
                    "12500000": []
                }
            }
        },
        "sellerPropertyCount": {
            "132535": 2
        }
    }
};
