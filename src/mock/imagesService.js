module.exports = {
    "statusCode": "2XX",
    "version": "A",
    "data": [{
        "id": 902982,
        "imageTypeId": 136,
        "objectId": 443784,
        "path": "17/443784/136/",
        "pageUrl": "gallery/443784-902982",
        "statusId": 2,
        "createdAt": 1452214672000,
        "sizeInBytes": 116362,
        "width": 800,
        "height": 558,
        "waterMarkName": "902982.jpeg",
        "active": true,
        "activeStatus": 1,
        "seoName": "902982.jpeg",
        "absolutePath": "http://content.makaan-ws.com/17/443784/136/902982.jpeg",
        "imageType": {
            "id": 136,
            "objectType": {
                "id": 17,
                "type": "listing"
            },
            "mediaType": {
                "id": 1,
                "name": "Image"
            },
            "objectTypeId": 17,
            "mediaTypeId": 1,
            "type": "Bad Image Wrong Image",
            "domainId": 1,
            "priority": 0,
            "imageSitemapEnabled": 0,
            "displayName": "Bad Image Wrong Nishtha"
        },
        "masterImageStatuses": {
            "id": 2,
            "status": "UNPROCESSED"
        }
    }]
};
