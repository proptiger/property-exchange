module.exports = {
  "statusCode": "2XX",
  "version": "A",
  "data": {
    "id": 100002,
    "name": "DLF",
    "imageURL": "https://im.proptiger-ws.com/3/100002/13/dlf-570745.gif",
    "description": "<p style=\"text-align: justify;\">In business since 1946, DLF is one of the most prominent commercial real estate developers in India.<br /> <br /> The company&lsquo;s primary business is the development of residential, commercial and retail properties. The operations of the company include all aspects of the real estate industry from execution, construction to even marketing of the projects. In addition to this the company is also involved in the business of power generation, maintenance services, hospitality, recreational activities and life insurance.<br /> <br /> When it comes to residential housing the company has segmented it into three parts i.e. super luxury, luxury and premium. This includes apartments, condominiums, row houses and duplexes of varied sizes.</p>\n<br />\n<p style=\"text-align: justify;\">In the commercial segment the company had worked for some of the top Indian and International corporate giants Citibank, Microsoft, IBM, CSC, Cognizant etc.</p>\n<br />\n<p style=\"text-align: justify;\">Presently the company has over 406msf of planned projects and 52 msf of under-construction projects. Such long term and wide exposure in the industry has made DLF the leaders in the real estate business and today the company enjoys their presence across 15 states and 24 cities.</p>\n<br />\n<p style=\"text-align: justify;\"><strong> Top Projects till Date:</strong></p>\n<br />\n<ul style=\"padding-left: 20px;\">\n<li>Development of 22 major urban colonies in <span><span>Delhi</span></span>.</li>\n<li>Developed 3000 acre DLF city in <span><span>Gurgaon</span></span>.</li>\n<li>Developed DLF Emporio which is India&rsquo;s first luxury mall.</li>\n<li>Delivered Delhi&rsquo;s first automated car parking system.</li>\n</ul>\n<br />\n<p style=\"text-align: justify;\"><strong> Projects under Construction:</strong></p>\n<br />\n<p style=\"text-align: justify;\">Few of its key under construction residential projects are:</p>\n<br />\n<ul style=\"padding-left: 20px;\">\n<li>DLF Ultima in Sector 81, <span><span>Gurgaon</span></span> comprising of 500 units of 3 and 4 BHK</li>\n<li>Bella Greens in <span><span>Begur</span></span>, <span><span><span><span>Bangalore</span></span></span></span> comprising of 106 units of 4 and 5 BHK villas with sizes ranging from 3,222 sq. ft. to 4,598 sq. ft.</li>\n<li>Hyde Park in Mullanpur, <span><span><span><span>Chandigarh</span></span></span></span> comprising of 654 units of 3 and 4 BHK apartments with sizes ranging from 1,880 sq. ft. to 2,000 sq. ft. and plot area ranging from 2,250 sq. ft. to 4,500 sq. ft.</li>\n<li>The Primus in Sector 82A, <span><span><span><span>Gurgaon</span></span></span></span> comprising of 566 units of 3 and 4 BHK apartments with sizes ranging from 1,799 sq. ft. to 2,576 sq. ft.</li>\n</ul>",
    "establishedDate": -757402200000,
    "projectStatusCount": {
      "pre launch": 0,
      "under construction": 23,
      "cancelled": 0,
      "launch": 1,
      "completed": 43,
      "on hold": 2,
      "not launched": 1
    },
    "mainImage": {
      "id": 0,
      "imageTypeId": 0,
      "objectId": 0,
      "statusId": 0,
      "sizeInBytes": 0,
      "width": 0,
      "height": 0,
      "active": false,
      "activeStatus": 0
    },
    "isBuilderListed": false
  }
};
