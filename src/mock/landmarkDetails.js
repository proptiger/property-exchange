module.exports = {
  "statusCode": "2XX",
  "version": "A",
  "data": {
    "placeId": "ChIJAQAA8UjkDDkRmdprFuRlLbo",
    "placeName": "The Great India Place",
    "formattedAddress": "Plot No A-2, Sector 38-A, Noida, Uttar Pradesh 201301, India",
    "latitude": 28.567624,
    "longitude": 77.3261979,
    "cityid": 20,
    "url": "nearby/the-great-india-place?id=ChIJAQAA8UjkDDkRmdprFuRlLbo"
  }
};
