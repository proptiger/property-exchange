module.exports = {
    "statusCode": "2XX",
    "version": "A",
    "totalCount" : 1,
    "data": [{
        totalCount: 4,
        isOriginalSearchResult: true,
        facetedResponse: {
            items: [{
                listing: {
                    "bookingAmount": 200000,
                    "bookingStatusId": 1,
                    "brokerConsent": false,
                    "company": {
                        "id": 0,
                        "makaanOnly": false
                    },
                    "companySeller": {
                        "id": 0,
                        "userId": 6603338,
                        "company": {
                            "assist": false,
                            "id": 524862,
                            "makaanOnly": false,
                            "micrositeEnabled": false,
                            "companyUrl": "growmax-wealth-management-services-profile-524862",
                            "name": "Growmax Wealth Management Services",
                            "score": 4.9,
                            "type": "Broker",
                            "paidLeadCount": 6
                        },
                        "user": {
                            "id": 6603338,
                            "fullName": "Jay Prakash",
                            "registered": false,
                            "contactNumbers": [{
                                "id": 0,
                                "userId": 0,
                                "contactNumber": "01130506840",
                                "priority": 2,
                                "createdBy": 0,
                                "createdAt": 1539241892577,
                                "isVerified": false,
                                "isActive": true
                            }],
                            "verified": false,
                            "profilePictureURL": "https://content.makaan.com/16/6603338/266/23243610.jpeg"
                        },
                        "left": 0,
                        "right": 0,
                        "checkAddress": "off",
                        "rateOption": "auto",
                        "isDefault": false
                    },
                    "computedPriority": 0.0196078431372549,
                    "constructionStatusId": 1,
                    "createdAt": 1531389642000,
                    "currentListingPrice": {
                        "id": 20081573,
                        "listingId": 5332825,
                        "version": "Website",
                        "effectiveDate": 1530383400000,
                        "pricePerUnitArea": 6000,
                        "plotCostPerUnitArea": 0,
                        "constructionCostPerUnitArea": 0,
                        "price": 9900000,
                        "status": "Active",
                        "updatedBy": 8432614,
                        "createdAt": 1531389642000,
                        "updatedAt": 1531456008000,
                        "hasPricePerUnitArea": false
                    },
                    "description": "The house is furnished. It has children's play area as well as lift available. The project also has car parking, landscaped gardens, jogging track, gymnasium, swimming pool and club house. The property is available on freehold. It is an under-construction property and is expected to get completed by 01/12/2018. It is well connected to the city areas. It is made in way to provide a comfortable living. Contact us for more details. ",
                    "descriptionPhrases" : ["children's play area", "car parking, landscaped gardens, jogging track", "well connected to the city", "children's play area", "children's play area"],
                    "expiryAt": 1546885801000,
                    "floor": 11,
                    "furnished": "Furnished",
                    "furnishings": [{
                            "id": 0,
                            "masterFurnishingId": 0,
                            "masterFurnishing": {
                                "id": 12,
                                "minValue": 0,
                                "maxValue": 0
                            },
                            "listingId": 0,
                            "value": 0,
                            "statusId": 0,
                            "updatedBy": 0
                        },
                        {
                            "id": 0,
                            "masterFurnishingId": 0,
                            "masterFurnishing": {
                                "id": 4,
                                "minValue": 0,
                                "maxValue": 0
                            },
                            "listingId": 0,
                            "value": 0,
                            "statusId": 0,
                            "updatedBy": 0
                        },
                        {
                            "id": 0,
                            "masterFurnishingId": 0,
                            "masterFurnishing": {
                                "id": 6,
                                "minValue": 0,
                                "maxValue": 0
                            },
                            "listingId": 0,
                            "value": 0,
                            "statusId": 0,
                            "updatedBy": 0
                        },
                        {
                            "id": 0,
                            "masterFurnishingId": 0,
                            "masterFurnishing": {
                                "id": 6,
                                "minValue": 0,
                                "maxValue": 0
                            },
                            "listingId": 0,
                            "value": 0,
                            "statusId": 0,
                            "updatedBy": 0
                        },
                        {
                            "id": 0,
                            "masterFurnishingId": 0,
                            "masterFurnishing": {
                                "id": 9,
                                "minValue": 0,
                                "maxValue": 0
                            },
                            "listingId": 0,
                            "value": 0,
                            "statusId": 0,
                            "updatedBy": 0
                        },
                        {
                            "id": 0,
                            "masterFurnishingId": 0,
                            "masterFurnishing": {
                                "id": 12,
                                "minValue": 0,
                                "maxValue": 0
                            },
                            "listingId": 0,
                            "value": 0,
                            "statusId": 0,
                            "updatedBy": 0
                        },
                        {
                            "id": 0,
                            "masterFurnishingId": 0,
                            "masterFurnishing": {
                                "id": 9,
                                "minValue": 0,
                                "maxValue": 0
                            },
                            "listingId": 0,
                            "value": 0,
                            "statusId": 0,
                            "updatedBy": 0
                        },
                        {
                            "id": 0,
                            "masterFurnishingId": 0,
                            "masterFurnishing": {
                                "id": 4,
                                "minValue": 0,
                                "maxValue": 0
                            },
                            "listingId": 0,
                            "value": 0,
                            "statusId": 0,
                            "updatedBy": 0
                        }
                    ],
                    "id": 5332825,
                    "imageCount": 51,
                    "listingImageCount": 5,
                    "allocation": {
                        "allocationHistory": [{
                            "masterAllocationStatus": {
                                "status": "ContentAuditingRequired"
                            }
                        }]
                    },
                    "isDeleted": false,
                    "isSponsored": false,
                    "throughCampaign": false,
                    "addedByPromoter": false,
                    "jsonDump": "{\"mapDragged\":\"N\",\"mediaMappings\":[{\"inactiveMediaIds\":[],\"deleteMediaIds\":[23865037,23865038,23865048,23865044,23865040,23865041],\"mediaModifiedTypeMap\":{\"23865037\":286,\"23865038\":286,\"23865040\":286,\"23865041\":286,\"23865044\":286,\"23865045\":286,\"23865046\":276,\"23865047\":286,\"23865048\":286,\"23865050\":277,\"23884299\":286},\"mediaTypeId\":1,\"newMediaIds\":[23865050,23884299,23865045,23865046,23865047],\"deleteMediaRejectionMap\":{\"23865037\":8,\"23865038\":2,\"23865040\":8,\"23865041\":8,\"23865044\":8,\"23865048\":8}}]}",
                    "jsonObject": {
                        "mapDragged": "N",
                        "mediaMappings": [{
                            "inactiveMediaIds": [],
                            "deleteMediaIds": [
                                23865044,
                                23865037,
                                23865038,
                                23865048,
                                23865040,
                                23865041
                            ],
                            "mediaModifiedTypeMap": {
                                "23865037": 286,
                                "23865038": 286,
                                "23865040": 286,
                                "23865041": 286,
                                "23865044": 286,
                                "23865045": 286,
                                "23865046": 276,
                                "23865047": 286,
                                "23865048": 286,
                                "23865050": 277,
                                "23884299": 286
                            },
                            "mediaTypeId": 1,
                            "newMediaIds": [
                                23865045,
                                23865046,
                                23865047,
                                23865050,
                                23884299
                            ],
                            "deleteMediaRejectionMap": {
                                "23865037": 8,
                                "23865038": 2,
                                "23865040": 8,
                                "23865041": 8,
                                "23865044": 8,
                                "23865048": 8
                            }
                        }]
                    },
                    "latitude": 28.47252083,
                    "listingAmenities": [{
                            "amenity": {
                                "id": 0,
                                "amenityMaster": {
                                    "amenityId": 1
                                },
                                "verified": false
                            }
                        },
                        {
                            "amenity": {
                                "id": 0,
                                "amenityMaster": {
                                    "amenityId": 2
                                },
                                "verified": false
                            }
                        },
                        {
                            "amenity": {
                                "id": 0,
                                "amenityMaster": {
                                    "amenityId": 3
                                },
                                "verified": false
                            }
                        },
                        {
                            "amenity": {
                                "id": 0,
                                "amenityMaster": {
                                    "amenityId": 4
                                },
                                "verified": false
                            }
                        },
                        {
                            "amenity": {
                                "id": 0,
                                "amenityMaster": {
                                    "amenityId": 12
                                },
                                "verified": false
                            }
                        },
                        {
                            "amenity": {
                                "id": 0,
                                "amenityMaster": {
                                    "amenityId": 18
                                },
                                "verified": false
                            }
                        },
                        {
                            "amenity": {
                                "id": 0,
                                "amenityMaster": {
                                    "amenityId": 17
                                },
                                "verified": false
                            }
                        },
                        {
                            "amenity": {
                                "id": 0,
                                "amenityMaster": {
                                    "amenityId": 20
                                },
                                "verified": false
                            }
                        },
                        {
                            "amenity": {
                                "id": 0,
                                "amenityMaster": {
                                    "amenityId": 14
                                },
                                "verified": false
                            }
                        },
                        {
                            "amenity": {
                                "id": 0,
                                "amenityMaster": {
                                    "amenityId": 102
                                },
                                "verified": false
                            }
                        },
                        {
                            "amenity": {
                                "id": 0,
                                "amenityMaster": {
                                    "amenityId": 100
                                },
                                "verified": false
                            }
                        },
                        {
                            "amenity": {
                                "id": 0,
                                "amenityMaster": {
                                    "amenityId": 1
                                },
                                "verified": false
                            }
                        },
                        {
                            "amenity": {
                                "id": 0,
                                "amenityMaster": {
                                    "amenityId": 2
                                },
                                "verified": false
                            }
                        },
                        {
                            "amenity": {
                                "id": 0,
                                "amenityMaster": {
                                    "amenityId": 3
                                },
                                "verified": false
                            }
                        },
                        {
                            "amenity": {
                                "id": 0,
                                "amenityMaster": {
                                    "amenityId": 4
                                },
                                "verified": false
                            }
                        },
                        {
                            "amenity": {
                                "id": 0,
                                "amenityMaster": {
                                    "amenityId": 12
                                },
                                "verified": false
                            }
                        },
                        {
                            "amenity": {
                                "id": 0,
                                "amenityMaster": {
                                    "amenityId": 18
                                },
                                "verified": false
                            }
                        },
                        {
                            "amenity": {
                                "id": 0,
                                "amenityMaster": {
                                    "amenityId": 17
                                },
                                "verified": false
                            }
                        },
                        {
                            "amenity": {
                                "id": 0,
                                "amenityMaster": {
                                    "amenityId": 20
                                },
                                "verified": false
                            }
                        },
                        {
                            "amenity": {
                                "id": 0,
                                "amenityMaster": {
                                    "amenityId": 14
                                },
                                "verified": false
                            }
                        },
                        {
                            "amenity": {
                                "id": 0,
                                "amenityMaster": {
                                    "amenityId": 102
                                },
                                "verified": false
                            }
                        },
                        {
                            "amenity": {
                                "id": 0,
                                "amenityMaster": {
                                    "amenityId": 100
                                },
                                "verified": false
                            }
                        }
                    ],
                    "listingCategory": "Primary",
                    "listingLatitude": 28.47252083,
                    "listingLongitude": 76.96575928,
                    "livabilityScore": 8.5,
                    "longitude": 76.96575928,
                    "mainImageURL": "https://dp5zphk8udxg9.cloudfront.net/wp-content/uploads/2017/12/shutterstock_715863751-e1514311727881-250x150.jpg",
                    "mainImageHeight": 644,
                    "mainImageWidth": 778,
                    "mainImage": {
                        "id": 0,
                        "imageTypeId": 0,
                        "objectId": 0,
                        "statusId": 0,
                        "sizeInBytes": 0,
                        "width": 778,
                        "height": 644,
                        "altText": "1650 sqft, 3 bhk Apartment in Emaar Gurgaon Greens Sector 102, Gurgaon at Rs. 99.0000 Lacs",
                        "title": "1650 sqft 3 bhk Apartment Emaar Gurgaon Greens Exterior",
                        "activeStatus": 0,
                        "absolutePath": "https://dp5zphk8udxg9.cloudfront.net/wp-content/uploads/2017/12/shutterstock_715863751-e1514311727881-250x150.jpg",
                        "active": false
                    },
                    "masterAmenityIds": [
                        1,
                        2,
                        3,
                        4,
                        12,
                        18,
                        17,
                        20,
                        14,
                        102,
                        100,
                        1,
                        2,
                        3,
                        4,
                        12,
                        18,
                        17,
                        20,
                        14,
                        102,
                        100
                    ],
                    "mediaMappings": [{
                        "inactiveMediaIds": [],
                        "deleteMediaIds": [
                            23865044,
                            23865037,
                            23865038,
                            23865048,
                            23865040,
                            23865041
                        ],
                        "mediaModifiedTypeMap": {
                            "23865037": 286,
                            "23865038": 286,
                            "23865040": 286,
                            "23865041": 286,
                            "23865044": 286,
                            "23865045": 286,
                            "23865046": 276,
                            "23865047": 286,
                            "23865048": 286,
                            "23865050": 277,
                            "23884299": 286
                        },
                        "mediaTypeId": 1,
                        "newMediaIds": [
                            23865045,
                            23865046,
                            23865047,
                            23865050,
                            23884299
                        ],
                        "deleteMediaRejectionMap": {
                            "23865037": 8,
                            "23865038": 2,
                            "23865040": 8,
                            "23865041": 8,
                            "23865044": 8,
                            "23865048": 8
                        }
                    }],
                    "negotiable": true,
                    "ownershipTypeId": 1,
                    "phaseId": 1006509,
                    "possessionDate": 1543602600000,
                    "postedDate": 1531389642000,
                    "priceVerified": false,
                    "property": {
                        "propertyId": 5004580,
                        "projectId": 502137,
                        "bedrooms": 3,
                        "bathrooms": 3,
                        "halls": 0,
                        "storeRoom": 0,
                        "unitType": "Apartment",
                        "unitTypeId": 1,
                        "unitName": "3BHK+3T (1,650 sq ft) + Servant Room",
                        "size": 1650,
                        "measure": "sq ft",
                        "project": {
                            "isHotProject": false,
                            "isFeaturedOnHomePage": false,
                            "isFeaturedOnCityPage": false,
                            "isFeaturedOnLocalityPage": false,
                            "isFeaturedOnMakaanIq": false,
                            "isFeatured": false,
                            "isProjectFeaturedBuy": false,
                            "isProjectFeaturedRent": false,
                            "builderPackageMagicNumber": 1,
                            "authorized": true,
                            "localityId": 50146,
                            "locality": {
                                "localityId": 50146,
                                "authorized": true,
                                "suburbId": 10028,
                                "suburb": {
                                    "id": 10028,
                                    "cityId": 11,
                                    "label": "Dwarka Expressway",
                                    "status": "Active",
                                    "city": {
                                        "id": 11,
                                        "authorized": true,
                                        "label": "Gurgaon",
                                        "status": "Active",
                                        "isServing": false,
                                        "isServingResale": false,
                                        "overviewUrl": "real-estate-gurgaon-property",
                                        "buyUrl": "gurgaon-residential-property/buy-property-in-gurgaon-city",
                                        "rentUrl": "gurgaon-residential-property/rent-property-in-gurgaon-city",
                                        "showPolygon": false,
                                        "geo": [
                                            "28.47252083,76.96575928"
                                        ],
                                        "stateId": 9,
                                        "state": {
                                            "stateId": 9,
                                            "countryId": 1,
                                            "label": "Haryana",
                                            "country": {
                                                "displayName": "India"
                                            }
                                        }
                                    },
                                    "priority": 0,
                                    "buyUrl": "gurgaon/dwarka-expressway-property-10028",
                                    "rentUrl": "gurgaon/dwarka-expressway-flats-for-rent-10028",
                                    "isDescriptionVerified": false
                                },
                                "cityId": 11,
                                "label": "Sector 102",
                                "localityServing": false,
                                "status": "Active",
                                "priority": 0,
                                "overviewUrl": "gurgaon/sector-102-real-estate-50146",
                                "buyUrl": "gurgaon-property/sector-102-flats-for-sale-50146",
                                "rentUrl": "gurgaon-property/sector-102-rental-flats-50146",
                                "relevanceScore": 0,
                                "isDescriptionVerified": false,
                                "localityHeroshotImageUrl": "https://im.proptiger.com/6/1/92/ahmedabad-heroshot-image-659698.jpeg",
                                "minAffordablePrice": 4000000,
                                "maxAffordablePrice": 8000000,
                                "minLuxuryPrice": 15000000,
                                "maxBudgetPrice": 5000000,
                                "constructionStatusId": 0
                            },
                            "builderId": 100026,
                            "builder": {
                                "id": 100026,
                                "name": "Emaar India",
                                "displayName": "Emaar",
                                "buyUrl": "emaar-india-100026",
                                "countryId": 1,
                                "mainImage": {
                                    "id": 0,
                                    "imageTypeId": 0,
                                    "objectId": 0,
                                    "statusId": 0,
                                    "sizeInBytes": 0,
                                    "width": 0,
                                    "height": 0,
                                    "activeStatus": 0,
                                    "active": false
                                },
                                "builderScore": 0.53813316375164,
                                "isBuilderListed": false,
                                "averageDelay": 1,
                                "activeStatus": "Active"
                            },
                            "name": "Gurgaon Greens",
                            "projectTypeId": 0,
                            "computedPriority": 0,
                            "assignedPriority": 0,
                            "imageURL": "https://im.proptiger.com/",
                            "overviewUrl": "gurgaon/emaar-india-gurgaon-greens-in-sector-102-502137",
                            "buyUrl": "gurgaon/property-for-buy-in-emaar-india-gurgaon-greens-in-sector-102-502137",
                            "rentUrl": "gurgaon/property-for-rent-in-emaar-india-gurgaon-greens-in-sector-102-502137",
                            "latitude": 28.47252083,
                            "longitude": 76.96575928,
                            "forceResale": 0,
                            "minBedrooms": 0,
                            "maxBedrooms": 0,
                            "isResale": false,
                            "isPrimary": false,
                            "isSoldOut": false,
                            "propertySizeMeasure": "sq ft",
                            "propertyUnitTypes": [],
                            "mainImage": {
                                "id": 0,
                                "imageTypeId": 0,
                                "objectId": 0,
                                "statusId": 0,
                                "sizeInBytes": 0,
                                "width": 0,
                                "height": 0,
                                "activeStatus": 0,
                                "active": false
                            },
                            "distinctBedrooms": [],
                            "livabilityScore": 8.5,
                            "activeStatus": "Active",
                            "has3DImages": false,
                            "resaleEnquiry": false,
                            "hasTownship": false,
                            "hasProjectInsightReport": false,
                            "projectId": 502137
                        },
                        "servantRoom": 1,
                        "poojaRoom": 0,
                        "optionCategory": "Actual",
                        "studyRoom": 0,
                        "balcony": 3,
                        "displayCarpetArea": 0,
                        "updatedBy": 1010,
                        "createdAt": 1384366064000,
                        "updatedAt": 1539172400000,
                        "penthouse": false,
                        "studio": false,
                        "rk": false,
                        "isPropertySoldOut": false,
                        "reraProject": {
                            "isHotProject": false,
                            "isFeaturedOnHomePage": false,
                            "isFeaturedOnCityPage": false,
                            "isFeaturedOnLocalityPage": false,
                            "isFeaturedOnMakaanIq": false,
                            "isFeatured": false,
                            "isProjectFeaturedBuy": false,
                            "isProjectFeaturedRent": false,
                            "builderPackageMagicNumber": 1,
                            "authorized": true,
                            "localityId": 0,
                            "builderId": 0,
                            "projectTypeId": 0,
                            "computedPriority": 0,
                            "assignedPriority": 0,
                            "forceResale": 0,
                            "minBedrooms": 0,
                            "maxBedrooms": 0,
                            "isResale": false,
                            "isPrimary": false,
                            "isSoldOut": false,
                            "propertySizeMeasure": "sq ft",
                            "has3DImages": false,
                            "resaleEnquiry": false,
                            "hasProjectInsightReport": false
                        }
                    },
                    "propertyId": 5004580,
                    "qualityScore": 9.17857,
                    "relevanceScore": 3.3623984,
                    "popularityScore": 0,
                    "leadStarvationScore": 2.3765512,
                    "sellerResponseRate": 5.020118,
                    "sellerTransactionRevealScore": 0,
                    "topSellerPriority": 0,
                    "listingSellerTransactionStatuses": [
                        "ACCOUNT_LOCKED"
                    ],
                    "sellerLeadDeficitScore": 0,
                    "relevanceScore2": 2.2500105,
                    "unstableRelevanceScoreAfterCompanyProjectLocalityRanking": 0,
                    "unstableRelevanceScoreAfterSellerRanking": 0,
                    "unstableRelevanceScoreAfterBudgetRanking": 0,
                    "resaleURL": "gurgaon/emaar-india-gurgaon-greens-in-sector-102-5332825/3bhk-3t-1650-sqft-apartment",
                    "sellerId": 6603338,
                    "similarListingIds": [
                        5314630,
                        1468776,
                        448597,
                        5901755,
                        5884964,
                        5889367,
                        5827615,
                        5804328,
                        5900242,
                        5839567,
                        5751786,
                        5900804,
                        2340309,
                        5814989,
                        4546582,
                        5757117,
                        5816987,
                        5757176
                    ],
                    "sourceDomainId": 1,
                    "status": "Active",
                    "depositRequired": false,
                    "totalFloors": 14,
                    "tower": {
                        "towerId": 0,
                        "projectId": 0
                    },
                    "updatedAt": 1531478189000,
                    "updatedBy": 8432614,
                    "createdBy": 6603338,
                    "verificationDate": 1531420200000,
                    "displayDate": 1531420200000,
                    "viewDirections": [{
                            "viewType": "Corner"
                        },
                        {
                            "viewType": "Garden View"
                        },
                        {
                            "viewType": "Pool View"
                        },
                        {
                            "viewType": "Road View"
                        }
                    ],
                    "isSellerEditable": true,
                    "listingSourceDomain": "Makaan",
                    "imageQualityTagId": 1,
                    "defaultImageId": 23865046,
                    "recencyScore": 0,
                    "versionNumber": "1531478186799",
                    "sellerCallRatingCount": 2,
                    "isTemplatized": false
                }
            },{
                listing: {
                    "bookingAmount": 200000,
                    "bookingStatusId": 1,
                    "brokerConsent": false,
                    "company": {
                        "id": 0,
                        "makaanOnly": false
                    },
                    "companySeller": {
                        "id": 0,
                        "userId": 6603338,
                        "company": {
                            "assist": false,
                            "id": 524862,
                            "makaanOnly": false,
                            "micrositeEnabled": false,
                            "companyUrl": "growmax-wealth-management-services-profile-524862",
                            "name": "Growmax Wealth Management Services",
                            "score": 4.9,
                            "type": "Broker",
                            "paidLeadCount": 6
                        },
                        "user": {
                            "id": 6603338,
                            "fullName": "Jay Prakash",
                            "registered": false,
                            "contactNumbers": [{
                                "id": 0,
                                "userId": 0,
                                "contactNumber": "01130506840",
                                "priority": 2,
                                "createdBy": 0,
                                "createdAt": 1539241892577,
                                "isVerified": false,
                                "isActive": true
                            }],
                            "verified": false,
                            "profilePictureURL": "https://content.makaan.com/16/6603338/266/23243610.jpeg"
                        },
                        "left": 0,
                        "right": 0,
                        "checkAddress": "off",
                        "rateOption": "auto",
                        "isDefault": false
                    },
                    "computedPriority": 0.0196078431372549,
                    "constructionStatusId": 1,
                    "createdAt": 1531389642000,
                    "currentListingPrice": {
                        "id": 20081573,
                        "listingId": 5332825,
                        "version": "Website",
                        "effectiveDate": 1530383400000,
                        "pricePerUnitArea": 6000,
                        "plotCostPerUnitArea": 0,
                        "constructionCostPerUnitArea": 0,
                        "price": 9900000,
                        "status": "Active",
                        "updatedBy": 8432614,
                        "createdAt": 1531389642000,
                        "updatedAt": 1531456008000,
                        "hasPricePerUnitArea": false
                    },
                    "description": "The house is furnished. It has children's play area as well as lift available. The project also has car parking, landscaped gardens, jogging track, gymnasium, swimming pool and club house. The property is available on freehold. It is an under-construction property and is expected to get completed by 01/12/2018. It is well connected to the city areas. It is made in way to provide a comfortable living. Contact us for more details. ",
                    "descriptionPhrases" : ["children's play area", "car parking, landscaped gardens, jogging track", "well connected to the city"],
                    "expiryAt": 1546885801000,
                    "floor": 11,
                    "furnished": "Furnished",
                    "furnishings": [{
                            "id": 0,
                            "masterFurnishingId": 0,
                            "masterFurnishing": {
                                "id": 12,
                                "minValue": 0,
                                "maxValue": 0
                            },
                            "listingId": 0,
                            "value": 0,
                            "statusId": 0,
                            "updatedBy": 0
                        },
                        {
                            "id": 0,
                            "masterFurnishingId": 0,
                            "masterFurnishing": {
                                "id": 4,
                                "minValue": 0,
                                "maxValue": 0
                            },
                            "listingId": 0,
                            "value": 0,
                            "statusId": 0,
                            "updatedBy": 0
                        },
                        {
                            "id": 0,
                            "masterFurnishingId": 0,
                            "masterFurnishing": {
                                "id": 6,
                                "minValue": 0,
                                "maxValue": 0
                            },
                            "listingId": 0,
                            "value": 0,
                            "statusId": 0,
                            "updatedBy": 0
                        },
                        {
                            "id": 0,
                            "masterFurnishingId": 0,
                            "masterFurnishing": {
                                "id": 6,
                                "minValue": 0,
                                "maxValue": 0
                            },
                            "listingId": 0,
                            "value": 0,
                            "statusId": 0,
                            "updatedBy": 0
                        },
                        {
                            "id": 0,
                            "masterFurnishingId": 0,
                            "masterFurnishing": {
                                "id": 9,
                                "minValue": 0,
                                "maxValue": 0
                            },
                            "listingId": 0,
                            "value": 0,
                            "statusId": 0,
                            "updatedBy": 0
                        },
                        {
                            "id": 0,
                            "masterFurnishingId": 0,
                            "masterFurnishing": {
                                "id": 12,
                                "minValue": 0,
                                "maxValue": 0
                            },
                            "listingId": 0,
                            "value": 0,
                            "statusId": 0,
                            "updatedBy": 0
                        },
                        {
                            "id": 0,
                            "masterFurnishingId": 0,
                            "masterFurnishing": {
                                "id": 9,
                                "minValue": 0,
                                "maxValue": 0
                            },
                            "listingId": 0,
                            "value": 0,
                            "statusId": 0,
                            "updatedBy": 0
                        },
                        {
                            "id": 0,
                            "masterFurnishingId": 0,
                            "masterFurnishing": {
                                "id": 4,
                                "minValue": 0,
                                "maxValue": 0
                            },
                            "listingId": 0,
                            "value": 0,
                            "statusId": 0,
                            "updatedBy": 0
                        }
                    ],
                    "id": 5332825,
                    "imageCount": 51,
                    "listingImageCount": 5,
                    "allocation": {
                        "allocationHistory": [{
                            "masterAllocationStatus": {
                                "status": "ContentAuditingRequired"
                            }
                        }]
                    },
                    "isDeleted": false,
                    "isSponsored": false,
                    "throughCampaign": false,
                    "addedByPromoter": false,
                    "jsonDump": "{\"mapDragged\":\"N\",\"mediaMappings\":[{\"inactiveMediaIds\":[],\"deleteMediaIds\":[23865037,23865038,23865048,23865044,23865040,23865041],\"mediaModifiedTypeMap\":{\"23865037\":286,\"23865038\":286,\"23865040\":286,\"23865041\":286,\"23865044\":286,\"23865045\":286,\"23865046\":276,\"23865047\":286,\"23865048\":286,\"23865050\":277,\"23884299\":286},\"mediaTypeId\":1,\"newMediaIds\":[23865050,23884299,23865045,23865046,23865047],\"deleteMediaRejectionMap\":{\"23865037\":8,\"23865038\":2,\"23865040\":8,\"23865041\":8,\"23865044\":8,\"23865048\":8}}]}",
                    "jsonObject": {
                        "mapDragged": "N",
                        "mediaMappings": [{
                            "inactiveMediaIds": [],
                            "deleteMediaIds": [
                                23865044,
                                23865037,
                                23865038,
                                23865048,
                                23865040,
                                23865041
                            ],
                            "mediaModifiedTypeMap": {
                                "23865037": 286,
                                "23865038": 286,
                                "23865040": 286,
                                "23865041": 286,
                                "23865044": 286,
                                "23865045": 286,
                                "23865046": 276,
                                "23865047": 286,
                                "23865048": 286,
                                "23865050": 277,
                                "23884299": 286
                            },
                            "mediaTypeId": 1,
                            "newMediaIds": [
                                23865045,
                                23865046,
                                23865047,
                                23865050,
                                23884299
                            ],
                            "deleteMediaRejectionMap": {
                                "23865037": 8,
                                "23865038": 2,
                                "23865040": 8,
                                "23865041": 8,
                                "23865044": 8,
                                "23865048": 8
                            }
                        }]
                    },
                    "latitude": 28.47252083,
                    "listingAmenities": [{
                            "amenity": {
                                "id": 0,
                                "amenityMaster": {
                                    "amenityId": 1
                                },
                                "verified": false
                            }
                        },
                        {
                            "amenity": {
                                "id": 0,
                                "amenityMaster": {
                                    "amenityId": 2
                                },
                                "verified": false
                            }
                        },
                        {
                            "amenity": {
                                "id": 0,
                                "amenityMaster": {
                                    "amenityId": 3
                                },
                                "verified": false
                            }
                        },
                        {
                            "amenity": {
                                "id": 0,
                                "amenityMaster": {
                                    "amenityId": 4
                                },
                                "verified": false
                            }
                        },
                        {
                            "amenity": {
                                "id": 0,
                                "amenityMaster": {
                                    "amenityId": 12
                                },
                                "verified": false
                            }
                        },
                        {
                            "amenity": {
                                "id": 0,
                                "amenityMaster": {
                                    "amenityId": 18
                                },
                                "verified": false
                            }
                        },
                        {
                            "amenity": {
                                "id": 0,
                                "amenityMaster": {
                                    "amenityId": 17
                                },
                                "verified": false
                            }
                        },
                        {
                            "amenity": {
                                "id": 0,
                                "amenityMaster": {
                                    "amenityId": 20
                                },
                                "verified": false
                            }
                        },
                        {
                            "amenity": {
                                "id": 0,
                                "amenityMaster": {
                                    "amenityId": 14
                                },
                                "verified": false
                            }
                        },
                        {
                            "amenity": {
                                "id": 0,
                                "amenityMaster": {
                                    "amenityId": 102
                                },
                                "verified": false
                            }
                        },
                        {
                            "amenity": {
                                "id": 0,
                                "amenityMaster": {
                                    "amenityId": 100
                                },
                                "verified": false
                            }
                        },
                        {
                            "amenity": {
                                "id": 0,
                                "amenityMaster": {
                                    "amenityId": 1
                                },
                                "verified": false
                            }
                        },
                        {
                            "amenity": {
                                "id": 0,
                                "amenityMaster": {
                                    "amenityId": 2
                                },
                                "verified": false
                            }
                        },
                        {
                            "amenity": {
                                "id": 0,
                                "amenityMaster": {
                                    "amenityId": 3
                                },
                                "verified": false
                            }
                        },
                        {
                            "amenity": {
                                "id": 0,
                                "amenityMaster": {
                                    "amenityId": 4
                                },
                                "verified": false
                            }
                        },
                        {
                            "amenity": {
                                "id": 0,
                                "amenityMaster": {
                                    "amenityId": 12
                                },
                                "verified": false
                            }
                        },
                        {
                            "amenity": {
                                "id": 0,
                                "amenityMaster": {
                                    "amenityId": 18
                                },
                                "verified": false
                            }
                        },
                        {
                            "amenity": {
                                "id": 0,
                                "amenityMaster": {
                                    "amenityId": 17
                                },
                                "verified": false
                            }
                        },
                        {
                            "amenity": {
                                "id": 0,
                                "amenityMaster": {
                                    "amenityId": 20
                                },
                                "verified": false
                            }
                        },
                        {
                            "amenity": {
                                "id": 0,
                                "amenityMaster": {
                                    "amenityId": 14
                                },
                                "verified": false
                            }
                        },
                        {
                            "amenity": {
                                "id": 0,
                                "amenityMaster": {
                                    "amenityId": 102
                                },
                                "verified": false
                            }
                        },
                        {
                            "amenity": {
                                "id": 0,
                                "amenityMaster": {
                                    "amenityId": 100
                                },
                                "verified": false
                            }
                        }
                    ],
                    "listingCategory": "Primary",
                    "listingLatitude": 28.47252083,
                    "listingLongitude": 76.96575928,
                    "livabilityScore": 8.5,
                    "longitude": 76.96575928,
                    "mainImageURL": "https://dp5zphk8udxg9.cloudfront.net/wp-content/uploads/2017/06/shutterstock_587077805-e1496728729750-250x150.jpg",
                    "mainImageHeight": 644,
                    "mainImageWidth": 778,
                    "mainImage": {
                        "id": 0,
                        "imageTypeId": 0,
                        "objectId": 0,
                        "statusId": 0,
                        "sizeInBytes": 0,
                        "width": 778,
                        "height": 644,
                        "altText": "1650 sqft, 3 bhk Apartment in Emaar Gurgaon Greens Sector 102, Gurgaon at Rs. 99.0000 Lacs",
                        "title": "1650 sqft 3 bhk Apartment Emaar Gurgaon Greens Exterior",
                        "activeStatus": 0,
                        "absolutePath": "https://dp5zphk8udxg9.cloudfront.net/wp-content/uploads/2017/06/shutterstock_587077805-e1496728729750-250x150.jpg",
                        "active": false
                    },
                    "masterAmenityIds": [
                        1,
                        2,
                        3,
                        4,
                        12,
                        18,
                        17,
                        20,
                        14,
                        102,
                        100,
                        1,
                        2,
                        3,
                        4,
                        12,
                        18,
                        17,
                        20,
                        14,
                        102,
                        100
                    ],
                    "mediaMappings": [{
                        "inactiveMediaIds": [],
                        "deleteMediaIds": [
                            23865044,
                            23865037,
                            23865038,
                            23865048,
                            23865040,
                            23865041
                        ],
                        "mediaModifiedTypeMap": {
                            "23865037": 286,
                            "23865038": 286,
                            "23865040": 286,
                            "23865041": 286,
                            "23865044": 286,
                            "23865045": 286,
                            "23865046": 276,
                            "23865047": 286,
                            "23865048": 286,
                            "23865050": 277,
                            "23884299": 286
                        },
                        "mediaTypeId": 1,
                        "newMediaIds": [
                            23865045,
                            23865046,
                            23865047,
                            23865050,
                            23884299
                        ],
                        "deleteMediaRejectionMap": {
                            "23865037": 8,
                            "23865038": 2,
                            "23865040": 8,
                            "23865041": 8,
                            "23865044": 8,
                            "23865048": 8
                        }
                    }],
                    "negotiable": true,
                    "ownershipTypeId": 1,
                    "phaseId": 1006509,
                    "possessionDate": 1543602600000,
                    "postedDate": 1531389642000,
                    "priceVerified": false,
                    "property": {
                        "propertyId": 5004580,
                        "projectId": 502137,
                        "bedrooms": 3,
                        "bathrooms": 3,
                        "halls": 0,
                        "storeRoom": 0,
                        "unitType": "Apartment",
                        "unitTypeId": 1,
                        "unitName": "3BHK+3T (1,650 sq ft) + Servant Room",
                        "size": 1650,
                        "measure": "sq ft",
                        "project": {
                            "isHotProject": false,
                            "isFeaturedOnHomePage": false,
                            "isFeaturedOnCityPage": false,
                            "isFeaturedOnLocalityPage": false,
                            "isFeaturedOnMakaanIq": false,
                            "isFeatured": false,
                            "isProjectFeaturedBuy": false,
                            "isProjectFeaturedRent": false,
                            "builderPackageMagicNumber": 1,
                            "authorized": true,
                            "localityId": 50146,
                            "locality": {
                                "localityId": 50146,
                                "authorized": true,
                                "suburbId": 10028,
                                "suburb": {
                                    "id": 10028,
                                    "cityId": 11,
                                    "label": "Dwarka Expressway",
                                    "status": "Active",
                                    "city": {
                                        "id": 11,
                                        "authorized": true,
                                        "label": "Gurgaon",
                                        "status": "Active",
                                        "isServing": false,
                                        "isServingResale": false,
                                        "overviewUrl": "real-estate-gurgaon-property",
                                        "buyUrl": "gurgaon-residential-property/buy-property-in-gurgaon-city",
                                        "rentUrl": "gurgaon-residential-property/rent-property-in-gurgaon-city",
                                        "showPolygon": false,
                                        "geo": [
                                            "28.47252083,76.96575928"
                                        ],
                                        "stateId": 9,
                                        "state": {
                                            "stateId": 9,
                                            "countryId": 1,
                                            "label": "Haryana",
                                            "country": {
                                                "displayName": "India"
                                            }
                                        }
                                    },
                                    "priority": 0,
                                    "buyUrl": "gurgaon/dwarka-expressway-property-10028",
                                    "rentUrl": "gurgaon/dwarka-expressway-flats-for-rent-10028",
                                    "isDescriptionVerified": false
                                },
                                "cityId": 11,
                                "label": "Sector 102",
                                "localityServing": false,
                                "status": "Active",
                                "priority": 0,
                                "overviewUrl": "gurgaon/sector-102-real-estate-50146",
                                "buyUrl": "gurgaon-property/sector-102-flats-for-sale-50146",
                                "rentUrl": "gurgaon-property/sector-102-rental-flats-50146",
                                "relevanceScore": 0,
                                "isDescriptionVerified": false,
                                "localityHeroshotImageUrl": "https://im.proptiger.com/6/1/92/ahmedabad-heroshot-image-659698.jpeg",
                                "minAffordablePrice": 4000000,
                                "maxAffordablePrice": 8000000,
                                "minLuxuryPrice": 15000000,
                                "maxBudgetPrice": 5000000,
                                "constructionStatusId": 0
                            },
                            "builderId": 100026,
                            "builder": {
                                "id": 100026,
                                "name": "Emaar India",
                                "displayName": "Emaar",
                                "buyUrl": "emaar-india-100026",
                                "countryId": 1,
                                "mainImage": {
                                    "id": 0,
                                    "imageTypeId": 0,
                                    "objectId": 0,
                                    "statusId": 0,
                                    "sizeInBytes": 0,
                                    "width": 0,
                                    "height": 0,
                                    "activeStatus": 0,
                                    "active": false
                                },
                                "builderScore": 0.53813316375164,
                                "isBuilderListed": false,
                                "averageDelay": 1,
                                "activeStatus": "Active"
                            },
                            "name": "Gurgaon Greens",
                            "projectTypeId": 0,
                            "computedPriority": 0,
                            "assignedPriority": 0,
                            "imageURL": "https://im.proptiger.com/",
                            "overviewUrl": "gurgaon/emaar-india-gurgaon-greens-in-sector-102-502137",
                            "buyUrl": "gurgaon/property-for-buy-in-emaar-india-gurgaon-greens-in-sector-102-502137",
                            "rentUrl": "gurgaon/property-for-rent-in-emaar-india-gurgaon-greens-in-sector-102-502137",
                            "latitude": 28.47252083,
                            "longitude": 76.96575928,
                            "forceResale": 0,
                            "minBedrooms": 0,
                            "maxBedrooms": 0,
                            "isResale": false,
                            "isPrimary": false,
                            "isSoldOut": false,
                            "propertySizeMeasure": "sq ft",
                            "propertyUnitTypes": [],
                            "mainImage": {
                                "id": 0,
                                "imageTypeId": 0,
                                "objectId": 0,
                                "statusId": 0,
                                "sizeInBytes": 0,
                                "width": 0,
                                "height": 0,
                                "activeStatus": 0,
                                "active": false
                            },
                            "distinctBedrooms": [],
                            "livabilityScore": 8.5,
                            "activeStatus": "Active",
                            "has3DImages": false,
                            "resaleEnquiry": false,
                            "hasTownship": false,
                            "hasProjectInsightReport": false,
                            "projectId": 502137
                        },
                        "servantRoom": 1,
                        "poojaRoom": 0,
                        "optionCategory": "Actual",
                        "studyRoom": 0,
                        "balcony": 3,
                        "displayCarpetArea": 0,
                        "updatedBy": 1010,
                        "createdAt": 1384366064000,
                        "updatedAt": 1539172400000,
                        "penthouse": false,
                        "studio": false,
                        "rk": false,
                        "isPropertySoldOut": false,
                        "reraProject": {
                            "isHotProject": false,
                            "isFeaturedOnHomePage": false,
                            "isFeaturedOnCityPage": false,
                            "isFeaturedOnLocalityPage": false,
                            "isFeaturedOnMakaanIq": false,
                            "isFeatured": false,
                            "isProjectFeaturedBuy": false,
                            "isProjectFeaturedRent": false,
                            "builderPackageMagicNumber": 1,
                            "authorized": true,
                            "localityId": 0,
                            "builderId": 0,
                            "projectTypeId": 0,
                            "computedPriority": 0,
                            "assignedPriority": 0,
                            "forceResale": 0,
                            "minBedrooms": 0,
                            "maxBedrooms": 0,
                            "isResale": false,
                            "isPrimary": false,
                            "isSoldOut": false,
                            "propertySizeMeasure": "sq ft",
                            "has3DImages": false,
                            "resaleEnquiry": false,
                            "hasProjectInsightReport": false
                        }
                    },
                    "propertyId": 5004580,
                    "qualityScore": 9.17857,
                    "relevanceScore": 3.3623984,
                    "popularityScore": 0,
                    "leadStarvationScore": 2.3765512,
                    "sellerResponseRate": 5.020118,
                    "sellerTransactionRevealScore": 0,
                    "topSellerPriority": 0,
                    "listingSellerTransactionStatuses": [
                        "ACCOUNT_LOCKED"
                    ],
                    "sellerLeadDeficitScore": 0,
                    "relevanceScore2": 2.2500105,
                    "unstableRelevanceScoreAfterCompanyProjectLocalityRanking": 0,
                    "unstableRelevanceScoreAfterSellerRanking": 0,
                    "unstableRelevanceScoreAfterBudgetRanking": 0,
                    "resaleURL": "gurgaon/emaar-india-gurgaon-greens-in-sector-102-5332825/3bhk-3t-1650-sqft-apartment",
                    "sellerId": 6603338,
                    "similarListingIds": [
                        5314630,
                        1468776,
                        448597,
                        5901755,
                        5884964,
                        5889367,
                        5827615,
                        5804328,
                        5900242,
                        5839567,
                        5751786,
                        5900804,
                        2340309,
                        5814989,
                        4546582,
                        5757117,
                        5816987,
                        5757176
                    ],
                    "sourceDomainId": 1,
                    "status": "Active",
                    "depositRequired": false,
                    "totalFloors": 14,
                    "tower": {
                        "towerId": 0,
                        "projectId": 0
                    },
                    "updatedAt": 1531478189000,
                    "updatedBy": 8432614,
                    "createdBy": 6603338,
                    "verificationDate": 1531420200000,
                    "displayDate": 1531420200000,
                    "viewDirections": [{
                            "viewType": "Corner"
                        },
                        {
                            "viewType": "Garden View"
                        },
                        {
                            "viewType": "Pool View"
                        },
                        {
                            "viewType": "Road View"
                        }
                    ],
                    "isSellerEditable": true,
                    "listingSourceDomain": "Makaan",
                    "imageQualityTagId": 1,
                    "defaultImageId": 23865046,
                    "recencyScore": 0,
                    "versionNumber": "1531478186799",
                    "sellerCallRatingCount": 2,
                    "isTemplatized": false
                }
            },{
                listing: {
                    "bookingAmount": 200000,
                    "bookingStatusId": 1,
                    "brokerConsent": false,
                    "company": {
                        "id": 0,
                        "makaanOnly": false
                    },
                    "companySeller": {
                        "id": 0,
                        "userId": 6603338,
                        "company": {
                            "assist": false,
                            "id": 524862,
                            "makaanOnly": false,
                            "micrositeEnabled": false,
                            "companyUrl": "growmax-wealth-management-services-profile-524862",
                            "name": "Growmax Wealth Management Services",
                            "score": 4.9,
                            "type": "Broker",
                            "paidLeadCount": 6
                        },
                        "user": {
                            "id": 6603338,
                            "fullName": "Jay Prakash",
                            "registered": false,
                            "contactNumbers": [{
                                "id": 0,
                                "userId": 0,
                                "contactNumber": "01130506840",
                                "priority": 2,
                                "createdBy": 0,
                                "createdAt": 1539241892577,
                                "isVerified": false,
                                "isActive": true
                            }],
                            "verified": false,
                            "profilePictureURL": "https://content.makaan.com/16/6603338/266/23243610.jpeg"
                        },
                        "left": 0,
                        "right": 0,
                        "checkAddress": "off",
                        "rateOption": "auto",
                        "isDefault": false
                    },
                    "computedPriority": 0.0196078431372549,
                    "constructionStatusId": 1,
                    "createdAt": 1531389642000,
                    "currentListingPrice": {
                        "id": 20081573,
                        "listingId": 5332825,
                        "version": "Website",
                        "effectiveDate": 1530383400000,
                        "pricePerUnitArea": 6000,
                        "plotCostPerUnitArea": 0,
                        "constructionCostPerUnitArea": 0,
                        "price": 9900000,
                        "status": "Active",
                        "updatedBy": 8432614,
                        "createdAt": 1531389642000,
                        "updatedAt": 1531456008000,
                        "hasPricePerUnitArea": false
                    },
                    "description": "The house is furnished. It has children's play area as well as lift available. The project also has car parking, landscaped gardens, jogging track, gymnasium, swimming pool and club house. The property is available on freehold. It is an under-construction property and is expected to get completed by 01/12/2018. It is well connected to the city areas. It is made in way to provide a comfortable living. Contact us for more details. ",
                    "descriptionPhrases" : ["children's play area", "car parking, landscaped gardens, jogging track", "well connected to the city"],
                    "expiryAt": 1546885801000,
                    "floor": 11,
                    "furnished": "Furnished",
                    "furnishings": [{
                            "id": 0,
                            "masterFurnishingId": 0,
                            "masterFurnishing": {
                                "id": 12,
                                "minValue": 0,
                                "maxValue": 0
                            },
                            "listingId": 0,
                            "value": 0,
                            "statusId": 0,
                            "updatedBy": 0
                        },
                        {
                            "id": 0,
                            "masterFurnishingId": 0,
                            "masterFurnishing": {
                                "id": 4,
                                "minValue": 0,
                                "maxValue": 0
                            },
                            "listingId": 0,
                            "value": 0,
                            "statusId": 0,
                            "updatedBy": 0
                        },
                        {
                            "id": 0,
                            "masterFurnishingId": 0,
                            "masterFurnishing": {
                                "id": 6,
                                "minValue": 0,
                                "maxValue": 0
                            },
                            "listingId": 0,
                            "value": 0,
                            "statusId": 0,
                            "updatedBy": 0
                        },
                        {
                            "id": 0,
                            "masterFurnishingId": 0,
                            "masterFurnishing": {
                                "id": 6,
                                "minValue": 0,
                                "maxValue": 0
                            },
                            "listingId": 0,
                            "value": 0,
                            "statusId": 0,
                            "updatedBy": 0
                        },
                        {
                            "id": 0,
                            "masterFurnishingId": 0,
                            "masterFurnishing": {
                                "id": 9,
                                "minValue": 0,
                                "maxValue": 0
                            },
                            "listingId": 0,
                            "value": 0,
                            "statusId": 0,
                            "updatedBy": 0
                        },
                        {
                            "id": 0,
                            "masterFurnishingId": 0,
                            "masterFurnishing": {
                                "id": 12,
                                "minValue": 0,
                                "maxValue": 0
                            },
                            "listingId": 0,
                            "value": 0,
                            "statusId": 0,
                            "updatedBy": 0
                        },
                        {
                            "id": 0,
                            "masterFurnishingId": 0,
                            "masterFurnishing": {
                                "id": 9,
                                "minValue": 0,
                                "maxValue": 0
                            },
                            "listingId": 0,
                            "value": 0,
                            "statusId": 0,
                            "updatedBy": 0
                        },
                        {
                            "id": 0,
                            "masterFurnishingId": 0,
                            "masterFurnishing": {
                                "id": 4,
                                "minValue": 0,
                                "maxValue": 0
                            },
                            "listingId": 0,
                            "value": 0,
                            "statusId": 0,
                            "updatedBy": 0
                        }
                    ],
                    "id": 5332825,
                    "imageCount": 51,
                    "listingImageCount": 5,
                    "allocation": {
                        "allocationHistory": [{
                            "masterAllocationStatus": {
                                "status": "ContentAuditingRequired"
                            }
                        }]
                    },
                    "isDeleted": false,
                    "isSponsored": false,
                    "throughCampaign": false,
                    "addedByPromoter": false,
                    "jsonDump": "{\"mapDragged\":\"N\",\"mediaMappings\":[{\"inactiveMediaIds\":[],\"deleteMediaIds\":[23865037,23865038,23865048,23865044,23865040,23865041],\"mediaModifiedTypeMap\":{\"23865037\":286,\"23865038\":286,\"23865040\":286,\"23865041\":286,\"23865044\":286,\"23865045\":286,\"23865046\":276,\"23865047\":286,\"23865048\":286,\"23865050\":277,\"23884299\":286},\"mediaTypeId\":1,\"newMediaIds\":[23865050,23884299,23865045,23865046,23865047],\"deleteMediaRejectionMap\":{\"23865037\":8,\"23865038\":2,\"23865040\":8,\"23865041\":8,\"23865044\":8,\"23865048\":8}}]}",
                    "jsonObject": {
                        "mapDragged": "N",
                        "mediaMappings": [{
                            "inactiveMediaIds": [],
                            "deleteMediaIds": [
                                23865044,
                                23865037,
                                23865038,
                                23865048,
                                23865040,
                                23865041
                            ],
                            "mediaModifiedTypeMap": {
                                "23865037": 286,
                                "23865038": 286,
                                "23865040": 286,
                                "23865041": 286,
                                "23865044": 286,
                                "23865045": 286,
                                "23865046": 276,
                                "23865047": 286,
                                "23865048": 286,
                                "23865050": 277,
                                "23884299": 286
                            },
                            "mediaTypeId": 1,
                            "newMediaIds": [
                                23865045,
                                23865046,
                                23865047,
                                23865050,
                                23884299
                            ],
                            "deleteMediaRejectionMap": {
                                "23865037": 8,
                                "23865038": 2,
                                "23865040": 8,
                                "23865041": 8,
                                "23865044": 8,
                                "23865048": 8
                            }
                        }]
                    },
                    "latitude": 28.47252083,
                    "listingAmenities": [{
                            "amenity": {
                                "id": 0,
                                "amenityMaster": {
                                    "amenityId": 1
                                },
                                "verified": false
                            }
                        },
                        {
                            "amenity": {
                                "id": 0,
                                "amenityMaster": {
                                    "amenityId": 2
                                },
                                "verified": false
                            }
                        },
                        {
                            "amenity": {
                                "id": 0,
                                "amenityMaster": {
                                    "amenityId": 3
                                },
                                "verified": false
                            }
                        },
                        {
                            "amenity": {
                                "id": 0,
                                "amenityMaster": {
                                    "amenityId": 4
                                },
                                "verified": false
                            }
                        },
                        {
                            "amenity": {
                                "id": 0,
                                "amenityMaster": {
                                    "amenityId": 12
                                },
                                "verified": false
                            }
                        },
                        {
                            "amenity": {
                                "id": 0,
                                "amenityMaster": {
                                    "amenityId": 18
                                },
                                "verified": false
                            }
                        },
                        {
                            "amenity": {
                                "id": 0,
                                "amenityMaster": {
                                    "amenityId": 17
                                },
                                "verified": false
                            }
                        },
                        {
                            "amenity": {
                                "id": 0,
                                "amenityMaster": {
                                    "amenityId": 20
                                },
                                "verified": false
                            }
                        },
                        {
                            "amenity": {
                                "id": 0,
                                "amenityMaster": {
                                    "amenityId": 14
                                },
                                "verified": false
                            }
                        },
                        {
                            "amenity": {
                                "id": 0,
                                "amenityMaster": {
                                    "amenityId": 102
                                },
                                "verified": false
                            }
                        },
                        {
                            "amenity": {
                                "id": 0,
                                "amenityMaster": {
                                    "amenityId": 100
                                },
                                "verified": false
                            }
                        },
                        {
                            "amenity": {
                                "id": 0,
                                "amenityMaster": {
                                    "amenityId": 1
                                },
                                "verified": false
                            }
                        },
                        {
                            "amenity": {
                                "id": 0,
                                "amenityMaster": {
                                    "amenityId": 2
                                },
                                "verified": false
                            }
                        },
                        {
                            "amenity": {
                                "id": 0,
                                "amenityMaster": {
                                    "amenityId": 3
                                },
                                "verified": false
                            }
                        },
                        {
                            "amenity": {
                                "id": 0,
                                "amenityMaster": {
                                    "amenityId": 4
                                },
                                "verified": false
                            }
                        },
                        {
                            "amenity": {
                                "id": 0,
                                "amenityMaster": {
                                    "amenityId": 12
                                },
                                "verified": false
                            }
                        },
                        {
                            "amenity": {
                                "id": 0,
                                "amenityMaster": {
                                    "amenityId": 18
                                },
                                "verified": false
                            }
                        },
                        {
                            "amenity": {
                                "id": 0,
                                "amenityMaster": {
                                    "amenityId": 17
                                },
                                "verified": false
                            }
                        },
                        {
                            "amenity": {
                                "id": 0,
                                "amenityMaster": {
                                    "amenityId": 20
                                },
                                "verified": false
                            }
                        },
                        {
                            "amenity": {
                                "id": 0,
                                "amenityMaster": {
                                    "amenityId": 14
                                },
                                "verified": false
                            }
                        },
                        {
                            "amenity": {
                                "id": 0,
                                "amenityMaster": {
                                    "amenityId": 102
                                },
                                "verified": false
                            }
                        },
                        {
                            "amenity": {
                                "id": 0,
                                "amenityMaster": {
                                    "amenityId": 100
                                },
                                "verified": false
                            }
                        }
                    ],
                    "listingCategory": "Primary",
                    "listingLatitude": 28.47252083,
                    "listingLongitude": 76.96575928,
                    "livabilityScore": 8.5,
                    "longitude": 76.96575928,
                    "mainImageURL": "https://dp5zphk8udxg9.cloudfront.net/wp-content/uploads/2017/03/Mumbai-Gateway-of-India-e1490892947983-250x150.jpg",
                    "mainImageHeight": 644,
                    "mainImageWidth": 778,
                    "mainImage": {
                        "id": 0,
                        "imageTypeId": 0,
                        "objectId": 0,
                        "statusId": 0,
                        "sizeInBytes": 0,
                        "width": 778,
                        "height": 644,
                        "altText": "1650 sqft, 3 bhk Apartment in Emaar Gurgaon Greens Sector 102, Gurgaon at Rs. 99.0000 Lacs",
                        "title": "1650 sqft 3 bhk Apartment Emaar Gurgaon Greens Exterior",
                        "activeStatus": 0,
                        "absolutePath": "https://dp5zphk8udxg9.cloudfront.net/wp-content/uploads/2017/03/Mumbai-Gateway-of-India-e1490892947983-250x150.jpg",
                        "active": false
                    },
                    "masterAmenityIds": [
                        1,
                        2,
                        3,
                        4,
                        12,
                        18,
                        17,
                        20,
                        14,
                        102,
                        100,
                        1,
                        2,
                        3,
                        4,
                        12,
                        18,
                        17,
                        20,
                        14,
                        102,
                        100
                    ],
                    "mediaMappings": [{
                        "inactiveMediaIds": [],
                        "deleteMediaIds": [
                            23865044,
                            23865037,
                            23865038,
                            23865048,
                            23865040,
                            23865041
                        ],
                        "mediaModifiedTypeMap": {
                            "23865037": 286,
                            "23865038": 286,
                            "23865040": 286,
                            "23865041": 286,
                            "23865044": 286,
                            "23865045": 286,
                            "23865046": 276,
                            "23865047": 286,
                            "23865048": 286,
                            "23865050": 277,
                            "23884299": 286
                        },
                        "mediaTypeId": 1,
                        "newMediaIds": [
                            23865045,
                            23865046,
                            23865047,
                            23865050,
                            23884299
                        ],
                        "deleteMediaRejectionMap": {
                            "23865037": 8,
                            "23865038": 2,
                            "23865040": 8,
                            "23865041": 8,
                            "23865044": 8,
                            "23865048": 8
                        }
                    }],
                    "negotiable": true,
                    "ownershipTypeId": 1,
                    "phaseId": 1006509,
                    "possessionDate": 1543602600000,
                    "postedDate": 1531389642000,
                    "priceVerified": false,
                    "property": {
                        "propertyId": 5004580,
                        "projectId": 502137,
                        "bedrooms": 3,
                        "bathrooms": 3,
                        "halls": 0,
                        "storeRoom": 0,
                        "unitType": "Apartment",
                        "unitTypeId": 1,
                        "unitName": "3BHK+3T (1,650 sq ft) + Servant Room",
                        "size": 1650,
                        "measure": "sq ft",
                        "project": {
                            "isHotProject": false,
                            "isFeaturedOnHomePage": false,
                            "isFeaturedOnCityPage": false,
                            "isFeaturedOnLocalityPage": false,
                            "isFeaturedOnMakaanIq": false,
                            "isFeatured": false,
                            "isProjectFeaturedBuy": false,
                            "isProjectFeaturedRent": false,
                            "builderPackageMagicNumber": 1,
                            "authorized": true,
                            "localityId": 50146,
                            "locality": {
                                "localityId": 50146,
                                "authorized": true,
                                "suburbId": 10028,
                                "suburb": {
                                    "id": 10028,
                                    "cityId": 11,
                                    "label": "Dwarka Expressway",
                                    "status": "Active",
                                    "city": {
                                        "id": 11,
                                        "authorized": true,
                                        "label": "Gurgaon",
                                        "status": "Active",
                                        "isServing": false,
                                        "isServingResale": false,
                                        "overviewUrl": "real-estate-gurgaon-property",
                                        "buyUrl": "gurgaon-residential-property/buy-property-in-gurgaon-city",
                                        "rentUrl": "gurgaon-residential-property/rent-property-in-gurgaon-city",
                                        "showPolygon": false,
                                        "geo": [
                                            "28.47252083,76.96575928"
                                        ],
                                        "stateId": 9,
                                        "state": {
                                            "stateId": 9,
                                            "countryId": 1,
                                            "label": "Haryana",
                                            "country": {
                                                "displayName": "India"
                                            }
                                        }
                                    },
                                    "priority": 0,
                                    "buyUrl": "gurgaon/dwarka-expressway-property-10028",
                                    "rentUrl": "gurgaon/dwarka-expressway-flats-for-rent-10028",
                                    "isDescriptionVerified": false
                                },
                                "cityId": 11,
                                "label": "Sector 102",
                                "localityServing": false,
                                "status": "Active",
                                "priority": 0,
                                "overviewUrl": "gurgaon/sector-102-real-estate-50146",
                                "buyUrl": "gurgaon-property/sector-102-flats-for-sale-50146",
                                "rentUrl": "gurgaon-property/sector-102-rental-flats-50146",
                                "relevanceScore": 0,
                                "isDescriptionVerified": false,
                                "localityHeroshotImageUrl": "https://im.proptiger.com/6/1/92/ahmedabad-heroshot-image-659698.jpeg",
                                "minAffordablePrice": 4000000,
                                "maxAffordablePrice": 8000000,
                                "minLuxuryPrice": 15000000,
                                "maxBudgetPrice": 5000000,
                                "constructionStatusId": 0
                            },
                            "builderId": 100026,
                            "builder": {
                                "id": 100026,
                                "name": "Emaar India",
                                "displayName": "Emaar",
                                "buyUrl": "emaar-india-100026",
                                "countryId": 1,
                                "mainImage": {
                                    "id": 0,
                                    "imageTypeId": 0,
                                    "objectId": 0,
                                    "statusId": 0,
                                    "sizeInBytes": 0,
                                    "width": 0,
                                    "height": 0,
                                    "activeStatus": 0,
                                    "active": false
                                },
                                "builderScore": 0.53813316375164,
                                "isBuilderListed": false,
                                "averageDelay": 1,
                                "activeStatus": "Active"
                            },
                            "name": "Gurgaon Greens",
                            "projectTypeId": 0,
                            "computedPriority": 0,
                            "assignedPriority": 0,
                            "imageURL": "https://im.proptiger.com/",
                            "overviewUrl": "gurgaon/emaar-india-gurgaon-greens-in-sector-102-502137",
                            "buyUrl": "gurgaon/property-for-buy-in-emaar-india-gurgaon-greens-in-sector-102-502137",
                            "rentUrl": "gurgaon/property-for-rent-in-emaar-india-gurgaon-greens-in-sector-102-502137",
                            "latitude": 28.47252083,
                            "longitude": 76.96575928,
                            "forceResale": 0,
                            "minBedrooms": 0,
                            "maxBedrooms": 0,
                            "isResale": false,
                            "isPrimary": false,
                            "isSoldOut": false,
                            "propertySizeMeasure": "sq ft",
                            "propertyUnitTypes": [],
                            "mainImage": {
                                "id": 0,
                                "imageTypeId": 0,
                                "objectId": 0,
                                "statusId": 0,
                                "sizeInBytes": 0,
                                "width": 0,
                                "height": 0,
                                "activeStatus": 0,
                                "active": false
                            },
                            "distinctBedrooms": [],
                            "livabilityScore": 8.5,
                            "activeStatus": "Active",
                            "has3DImages": false,
                            "resaleEnquiry": false,
                            "hasTownship": false,
                            "hasProjectInsightReport": false,
                            "projectId": 502137
                        },
                        "servantRoom": 1,
                        "poojaRoom": 0,
                        "optionCategory": "Actual",
                        "studyRoom": 0,
                        "balcony": 3,
                        "displayCarpetArea": 0,
                        "updatedBy": 1010,
                        "createdAt": 1384366064000,
                        "updatedAt": 1539172400000,
                        "penthouse": false,
                        "studio": false,
                        "rk": false,
                        "isPropertySoldOut": false,
                        "reraProject": {
                            "isHotProject": false,
                            "isFeaturedOnHomePage": false,
                            "isFeaturedOnCityPage": false,
                            "isFeaturedOnLocalityPage": false,
                            "isFeaturedOnMakaanIq": false,
                            "isFeatured": false,
                            "isProjectFeaturedBuy": false,
                            "isProjectFeaturedRent": false,
                            "builderPackageMagicNumber": 1,
                            "authorized": true,
                            "localityId": 0,
                            "builderId": 0,
                            "projectTypeId": 0,
                            "computedPriority": 0,
                            "assignedPriority": 0,
                            "forceResale": 0,
                            "minBedrooms": 0,
                            "maxBedrooms": 0,
                            "isResale": false,
                            "isPrimary": false,
                            "isSoldOut": false,
                            "propertySizeMeasure": "sq ft",
                            "has3DImages": false,
                            "resaleEnquiry": false,
                            "hasProjectInsightReport": false
                        }
                    },
                    "propertyId": 5004580,
                    "qualityScore": 9.17857,
                    "relevanceScore": 3.3623984,
                    "popularityScore": 0,
                    "leadStarvationScore": 2.3765512,
                    "sellerResponseRate": 5.020118,
                    "sellerTransactionRevealScore": 0,
                    "topSellerPriority": 0,
                    "listingSellerTransactionStatuses": [
                        "ACCOUNT_LOCKED"
                    ],
                    "sellerLeadDeficitScore": 0,
                    "relevanceScore2": 2.2500105,
                    "unstableRelevanceScoreAfterCompanyProjectLocalityRanking": 0,
                    "unstableRelevanceScoreAfterSellerRanking": 0,
                    "unstableRelevanceScoreAfterBudgetRanking": 0,
                    "resaleURL": "gurgaon/emaar-india-gurgaon-greens-in-sector-102-5332825/3bhk-3t-1650-sqft-apartment",
                    "sellerId": 6603338,
                    "similarListingIds": [
                        5314630,
                        1468776,
                        448597,
                        5901755,
                        5884964,
                        5889367,
                        5827615,
                        5804328,
                        5900242,
                        5839567,
                        5751786,
                        5900804,
                        2340309,
                        5814989,
                        4546582,
                        5757117,
                        5816987,
                        5757176
                    ],
                    "sourceDomainId": 1,
                    "status": "Active",
                    "depositRequired": false,
                    "totalFloors": 14,
                    "tower": {
                        "towerId": 0,
                        "projectId": 0
                    },
                    "updatedAt": 1531478189000,
                    "updatedBy": 8432614,
                    "createdBy": 6603338,
                    "verificationDate": 1531420200000,
                    "displayDate": 1531420200000,
                    "viewDirections": [{
                            "viewType": "Corner"
                        },
                        {
                            "viewType": "Garden View"
                        },
                        {
                            "viewType": "Pool View"
                        },
                        {
                            "viewType": "Road View"
                        }
                    ],
                    "isSellerEditable": true,
                    "listingSourceDomain": "Makaan",
                    "imageQualityTagId": 1,
                    "defaultImageId": 23865046,
                    "recencyScore": 0,
                    "versionNumber": "1531478186799",
                    "sellerCallRatingCount": 2,
                    "isTemplatized": false
                }
            },{
                listing: {
                    "bookingAmount": 200000,
                    "bookingStatusId": 1,
                    "brokerConsent": false,
                    "company": {
                        "id": 0,
                        "makaanOnly": false
                    },
                    "companySeller": {
                        "id": 0,
                        "userId": 6603338,
                        "company": {
                            "assist": false,
                            "id": 524862,
                            "makaanOnly": false,
                            "micrositeEnabled": false,
                            "companyUrl": "growmax-wealth-management-services-profile-524862",
                            "name": "Growmax Wealth Management Services",
                            "score": 4.9,
                            "type": "Broker",
                            "paidLeadCount": 6
                        },
                        "user": {
                            "id": 6603338,
                            "fullName": "Jay Prakash",
                            "registered": false,
                            "contactNumbers": [{
                                "id": 0,
                                "userId": 0,
                                "contactNumber": "01130506840",
                                "priority": 2,
                                "createdBy": 0,
                                "createdAt": 1539241892577,
                                "isVerified": false,
                                "isActive": true
                            }],
                            "verified": false,
                            "profilePictureURL": "https://content.makaan.com/16/6603338/266/23243610.jpeg"
                        },
                        "left": 0,
                        "right": 0,
                        "checkAddress": "off",
                        "rateOption": "auto",
                        "isDefault": false
                    },
                    "computedPriority": 0.0196078431372549,
                    "constructionStatusId": 1,
                    "createdAt": 1531389642000,
                    "currentListingPrice": {
                        "id": 20081573,
                        "listingId": 5332825,
                        "version": "Website",
                        "effectiveDate": 1530383400000,
                        "pricePerUnitArea": 6000,
                        "plotCostPerUnitArea": 0,
                        "constructionCostPerUnitArea": 0,
                        "price": 9900000,
                        "status": "Active",
                        "updatedBy": 8432614,
                        "createdAt": 1531389642000,
                        "updatedAt": 1531456008000,
                        "hasPricePerUnitArea": false
                    },
                    "description": "The house is furnished. It has children's play area as well as lift available. The project also has car parking, landscaped gardens, jogging track, gymnasium, swimming pool and club house. The property is available on freehold. It is an under-construction property and is expected to get completed by 01/12/2018. It is well connected to the city areas. It is made in way to provide a comfortable living. Contact us for more details. ",
                    "descriptionPhrases" : ["children's play area", "car parking, landscaped gardens, jogging track", "well connected to the city"],
                    "expiryAt": 1546885801000,
                    "floor": 11,
                    "furnished": "Furnished",
                    "furnishings": [{
                            "id": 0,
                            "masterFurnishingId": 0,
                            "masterFurnishing": {
                                "id": 12,
                                "minValue": 0,
                                "maxValue": 0
                            },
                            "listingId": 0,
                            "value": 0,
                            "statusId": 0,
                            "updatedBy": 0
                        },
                        {
                            "id": 0,
                            "masterFurnishingId": 0,
                            "masterFurnishing": {
                                "id": 4,
                                "minValue": 0,
                                "maxValue": 0
                            },
                            "listingId": 0,
                            "value": 0,
                            "statusId": 0,
                            "updatedBy": 0
                        },
                        {
                            "id": 0,
                            "masterFurnishingId": 0,
                            "masterFurnishing": {
                                "id": 6,
                                "minValue": 0,
                                "maxValue": 0
                            },
                            "listingId": 0,
                            "value": 0,
                            "statusId": 0,
                            "updatedBy": 0
                        },
                        {
                            "id": 0,
                            "masterFurnishingId": 0,
                            "masterFurnishing": {
                                "id": 6,
                                "minValue": 0,
                                "maxValue": 0
                            },
                            "listingId": 0,
                            "value": 0,
                            "statusId": 0,
                            "updatedBy": 0
                        },
                        {
                            "id": 0,
                            "masterFurnishingId": 0,
                            "masterFurnishing": {
                                "id": 9,
                                "minValue": 0,
                                "maxValue": 0
                            },
                            "listingId": 0,
                            "value": 0,
                            "statusId": 0,
                            "updatedBy": 0
                        },
                        {
                            "id": 0,
                            "masterFurnishingId": 0,
                            "masterFurnishing": {
                                "id": 12,
                                "minValue": 0,
                                "maxValue": 0
                            },
                            "listingId": 0,
                            "value": 0,
                            "statusId": 0,
                            "updatedBy": 0
                        },
                        {
                            "id": 0,
                            "masterFurnishingId": 0,
                            "masterFurnishing": {
                                "id": 9,
                                "minValue": 0,
                                "maxValue": 0
                            },
                            "listingId": 0,
                            "value": 0,
                            "statusId": 0,
                            "updatedBy": 0
                        },
                        {
                            "id": 0,
                            "masterFurnishingId": 0,
                            "masterFurnishing": {
                                "id": 4,
                                "minValue": 0,
                                "maxValue": 0
                            },
                            "listingId": 0,
                            "value": 0,
                            "statusId": 0,
                            "updatedBy": 0
                        }
                    ],
                    "id": 5332825,
                    "imageCount": 51,
                    "listingImageCount": 5,
                    "allocation": {
                        "allocationHistory": [{
                            "masterAllocationStatus": {
                                "status": "ContentAuditingRequired"
                            }
                        }]
                    },
                    "isDeleted": false,
                    "isSponsored": false,
                    "throughCampaign": false,
                    "addedByPromoter": false,
                    "jsonDump": "{\"mapDragged\":\"N\",\"mediaMappings\":[{\"inactiveMediaIds\":[],\"deleteMediaIds\":[23865037,23865038,23865048,23865044,23865040,23865041],\"mediaModifiedTypeMap\":{\"23865037\":286,\"23865038\":286,\"23865040\":286,\"23865041\":286,\"23865044\":286,\"23865045\":286,\"23865046\":276,\"23865047\":286,\"23865048\":286,\"23865050\":277,\"23884299\":286},\"mediaTypeId\":1,\"newMediaIds\":[23865050,23884299,23865045,23865046,23865047],\"deleteMediaRejectionMap\":{\"23865037\":8,\"23865038\":2,\"23865040\":8,\"23865041\":8,\"23865044\":8,\"23865048\":8}}]}",
                    "jsonObject": {
                        "mapDragged": "N",
                        "mediaMappings": [{
                            "inactiveMediaIds": [],
                            "deleteMediaIds": [
                                23865044,
                                23865037,
                                23865038,
                                23865048,
                                23865040,
                                23865041
                            ],
                            "mediaModifiedTypeMap": {
                                "23865037": 286,
                                "23865038": 286,
                                "23865040": 286,
                                "23865041": 286,
                                "23865044": 286,
                                "23865045": 286,
                                "23865046": 276,
                                "23865047": 286,
                                "23865048": 286,
                                "23865050": 277,
                                "23884299": 286
                            },
                            "mediaTypeId": 1,
                            "newMediaIds": [
                                23865045,
                                23865046,
                                23865047,
                                23865050,
                                23884299
                            ],
                            "deleteMediaRejectionMap": {
                                "23865037": 8,
                                "23865038": 2,
                                "23865040": 8,
                                "23865041": 8,
                                "23865044": 8,
                                "23865048": 8
                            }
                        }]
                    },
                    "latitude": 28.47252083,
                    "listingAmenities": [{
                            "amenity": {
                                "id": 0,
                                "amenityMaster": {
                                    "amenityId": 1
                                },
                                "verified": false
                            }
                        },
                        {
                            "amenity": {
                                "id": 0,
                                "amenityMaster": {
                                    "amenityId": 2
                                },
                                "verified": false
                            }
                        },
                        {
                            "amenity": {
                                "id": 0,
                                "amenityMaster": {
                                    "amenityId": 3
                                },
                                "verified": false
                            }
                        },
                        {
                            "amenity": {
                                "id": 0,
                                "amenityMaster": {
                                    "amenityId": 4
                                },
                                "verified": false
                            }
                        },
                        {
                            "amenity": {
                                "id": 0,
                                "amenityMaster": {
                                    "amenityId": 12
                                },
                                "verified": false
                            }
                        },
                        {
                            "amenity": {
                                "id": 0,
                                "amenityMaster": {
                                    "amenityId": 18
                                },
                                "verified": false
                            }
                        },
                        {
                            "amenity": {
                                "id": 0,
                                "amenityMaster": {
                                    "amenityId": 17
                                },
                                "verified": false
                            }
                        },
                        {
                            "amenity": {
                                "id": 0,
                                "amenityMaster": {
                                    "amenityId": 20
                                },
                                "verified": false
                            }
                        },
                        {
                            "amenity": {
                                "id": 0,
                                "amenityMaster": {
                                    "amenityId": 14
                                },
                                "verified": false
                            }
                        },
                        {
                            "amenity": {
                                "id": 0,
                                "amenityMaster": {
                                    "amenityId": 102
                                },
                                "verified": false
                            }
                        },
                        {
                            "amenity": {
                                "id": 0,
                                "amenityMaster": {
                                    "amenityId": 100
                                },
                                "verified": false
                            }
                        },
                        {
                            "amenity": {
                                "id": 0,
                                "amenityMaster": {
                                    "amenityId": 1
                                },
                                "verified": false
                            }
                        },
                        {
                            "amenity": {
                                "id": 0,
                                "amenityMaster": {
                                    "amenityId": 2
                                },
                                "verified": false
                            }
                        },
                        {
                            "amenity": {
                                "id": 0,
                                "amenityMaster": {
                                    "amenityId": 3
                                },
                                "verified": false
                            }
                        },
                        {
                            "amenity": {
                                "id": 0,
                                "amenityMaster": {
                                    "amenityId": 4
                                },
                                "verified": false
                            }
                        },
                        {
                            "amenity": {
                                "id": 0,
                                "amenityMaster": {
                                    "amenityId": 12
                                },
                                "verified": false
                            }
                        },
                        {
                            "amenity": {
                                "id": 0,
                                "amenityMaster": {
                                    "amenityId": 18
                                },
                                "verified": false
                            }
                        },
                        {
                            "amenity": {
                                "id": 0,
                                "amenityMaster": {
                                    "amenityId": 17
                                },
                                "verified": false
                            }
                        },
                        {
                            "amenity": {
                                "id": 0,
                                "amenityMaster": {
                                    "amenityId": 20
                                },
                                "verified": false
                            }
                        },
                        {
                            "amenity": {
                                "id": 0,
                                "amenityMaster": {
                                    "amenityId": 14
                                },
                                "verified": false
                            }
                        },
                        {
                            "amenity": {
                                "id": 0,
                                "amenityMaster": {
                                    "amenityId": 102
                                },
                                "verified": false
                            }
                        },
                        {
                            "amenity": {
                                "id": 0,
                                "amenityMaster": {
                                    "amenityId": 100
                                },
                                "verified": false
                            }
                        }
                    ],
                    "listingCategory": "Primary",
                    "listingLatitude": 28.47252083,
                    "listingLongitude": 76.96575928,
                    "livabilityScore": 8.5,
                    "longitude": 76.96575928,
                    "mainImageURL": "https://dp5zphk8udxg9.cloudfront.net/wp-content/uploads/2017/12/shutterstock_715863751-e1514311727881-250x150.jpg",
                    "mainImageHeight": 644,
                    "mainImageWidth": 778,
                    "mainImage": {
                        "id": 0,
                        "imageTypeId": 0,
                        "objectId": 0,
                        "statusId": 0,
                        "sizeInBytes": 0,
                        "width": 778,
                        "height": 644,
                        "altText": "1650 sqft, 3 bhk Apartment in Emaar Gurgaon Greens Sector 102, Gurgaon at Rs. 99.0000 Lacs",
                        "title": "1650 sqft 3 bhk Apartment Emaar Gurgaon Greens Exterior",
                        "activeStatus": 0,
                        "absolutePath": "https://dp5zphk8udxg9.cloudfront.net/wp-content/uploads/2017/12/shutterstock_715863751-e1514311727881-250x150.jpg",
                        "active": false
                    },
                    "masterAmenityIds": [
                        1,
                        2,
                        3,
                        4,
                        12,
                        18,
                        17,
                        20,
                        14,
                        102,
                        100,
                        1,
                        2,
                        3,
                        4,
                        12,
                        18,
                        17,
                        20,
                        14,
                        102,
                        100
                    ],
                    "mediaMappings": [{
                        "inactiveMediaIds": [],
                        "deleteMediaIds": [
                            23865044,
                            23865037,
                            23865038,
                            23865048,
                            23865040,
                            23865041
                        ],
                        "mediaModifiedTypeMap": {
                            "23865037": 286,
                            "23865038": 286,
                            "23865040": 286,
                            "23865041": 286,
                            "23865044": 286,
                            "23865045": 286,
                            "23865046": 276,
                            "23865047": 286,
                            "23865048": 286,
                            "23865050": 277,
                            "23884299": 286
                        },
                        "mediaTypeId": 1,
                        "newMediaIds": [
                            23865045,
                            23865046,
                            23865047,
                            23865050,
                            23884299
                        ],
                        "deleteMediaRejectionMap": {
                            "23865037": 8,
                            "23865038": 2,
                            "23865040": 8,
                            "23865041": 8,
                            "23865044": 8,
                            "23865048": 8
                        }
                    }],
                    "negotiable": true,
                    "ownershipTypeId": 1,
                    "phaseId": 1006509,
                    "possessionDate": 1543602600000,
                    "postedDate": 1531389642000,
                    "priceVerified": false,
                    "property": {
                        "propertyId": 5004580,
                        "projectId": 502137,
                        "bedrooms": 3,
                        "bathrooms": 3,
                        "halls": 0,
                        "storeRoom": 0,
                        "unitType": "Apartment",
                        "unitTypeId": 1,
                        "unitName": "3BHK+3T (1,650 sq ft) + Servant Room",
                        "size": 1650,
                        "measure": "sq ft",
                        "project": {
                            "isHotProject": false,
                            "isFeaturedOnHomePage": false,
                            "isFeaturedOnCityPage": false,
                            "isFeaturedOnLocalityPage": false,
                            "isFeaturedOnMakaanIq": false,
                            "isFeatured": false,
                            "isProjectFeaturedBuy": false,
                            "isProjectFeaturedRent": false,
                            "builderPackageMagicNumber": 1,
                            "authorized": true,
                            "localityId": 50146,
                            "locality": {
                                "localityId": 50146,
                                "authorized": true,
                                "suburbId": 10028,
                                "suburb": {
                                    "id": 10028,
                                    "cityId": 11,
                                    "label": "Dwarka Expressway",
                                    "status": "Active",
                                    "city": {
                                        "id": 11,
                                        "authorized": true,
                                        "label": "Gurgaon",
                                        "status": "Active",
                                        "isServing": false,
                                        "isServingResale": false,
                                        "overviewUrl": "real-estate-gurgaon-property",
                                        "buyUrl": "gurgaon-residential-property/buy-property-in-gurgaon-city",
                                        "rentUrl": "gurgaon-residential-property/rent-property-in-gurgaon-city",
                                        "showPolygon": false,
                                        "geo": [
                                            "28.47252083,76.96575928"
                                        ],
                                        "stateId": 9,
                                        "state": {
                                            "stateId": 9,
                                            "countryId": 1,
                                            "label": "Haryana",
                                            "country": {
                                                "displayName": "India"
                                            }
                                        }
                                    },
                                    "priority": 0,
                                    "buyUrl": "gurgaon/dwarka-expressway-property-10028",
                                    "rentUrl": "gurgaon/dwarka-expressway-flats-for-rent-10028",
                                    "isDescriptionVerified": false
                                },
                                "cityId": 11,
                                "label": "Sector 102",
                                "localityServing": false,
                                "status": "Active",
                                "priority": 0,
                                "overviewUrl": "gurgaon/sector-102-real-estate-50146",
                                "buyUrl": "gurgaon-property/sector-102-flats-for-sale-50146",
                                "rentUrl": "gurgaon-property/sector-102-rental-flats-50146",
                                "relevanceScore": 0,
                                "isDescriptionVerified": false,
                                "localityHeroshotImageUrl": "https://im.proptiger.com/6/1/92/ahmedabad-heroshot-image-659698.jpeg",
                                "minAffordablePrice": 4000000,
                                "maxAffordablePrice": 8000000,
                                "minLuxuryPrice": 15000000,
                                "maxBudgetPrice": 5000000,
                                "constructionStatusId": 0
                            },
                            "builderId": 100026,
                            "builder": {
                                "id": 100026,
                                "name": "Emaar India",
                                "displayName": "Emaar",
                                "buyUrl": "emaar-india-100026",
                                "countryId": 1,
                                "mainImage": {
                                    "id": 0,
                                    "imageTypeId": 0,
                                    "objectId": 0,
                                    "statusId": 0,
                                    "sizeInBytes": 0,
                                    "width": 0,
                                    "height": 0,
                                    "activeStatus": 0,
                                    "active": false
                                },
                                "builderScore": 0.53813316375164,
                                "isBuilderListed": false,
                                "averageDelay": 1,
                                "activeStatus": "Active"
                            },
                            "name": "Gurgaon Greens",
                            "projectTypeId": 0,
                            "computedPriority": 0,
                            "assignedPriority": 0,
                            "imageURL": "https://im.proptiger.com/",
                            "overviewUrl": "gurgaon/emaar-india-gurgaon-greens-in-sector-102-502137",
                            "buyUrl": "gurgaon/property-for-buy-in-emaar-india-gurgaon-greens-in-sector-102-502137",
                            "rentUrl": "gurgaon/property-for-rent-in-emaar-india-gurgaon-greens-in-sector-102-502137",
                            "latitude": 28.47252083,
                            "longitude": 76.96575928,
                            "forceResale": 0,
                            "minBedrooms": 0,
                            "maxBedrooms": 0,
                            "isResale": false,
                            "isPrimary": false,
                            "isSoldOut": false,
                            "propertySizeMeasure": "sq ft",
                            "propertyUnitTypes": [],
                            "mainImage": {
                                "id": 0,
                                "imageTypeId": 0,
                                "objectId": 0,
                                "statusId": 0,
                                "sizeInBytes": 0,
                                "width": 0,
                                "height": 0,
                                "activeStatus": 0,
                                "active": false
                            },
                            "distinctBedrooms": [],
                            "livabilityScore": 8.5,
                            "activeStatus": "Active",
                            "has3DImages": false,
                            "resaleEnquiry": false,
                            "hasTownship": false,
                            "hasProjectInsightReport": false,
                            "projectId": 502137
                        },
                        "servantRoom": 1,
                        "poojaRoom": 0,
                        "optionCategory": "Actual",
                        "studyRoom": 0,
                        "balcony": 3,
                        "displayCarpetArea": 0,
                        "updatedBy": 1010,
                        "createdAt": 1384366064000,
                        "updatedAt": 1539172400000,
                        "penthouse": false,
                        "studio": false,
                        "rk": false,
                        "isPropertySoldOut": false,
                        "reraProject": {
                            "isHotProject": false,
                            "isFeaturedOnHomePage": false,
                            "isFeaturedOnCityPage": false,
                            "isFeaturedOnLocalityPage": false,
                            "isFeaturedOnMakaanIq": false,
                            "isFeatured": false,
                            "isProjectFeaturedBuy": false,
                            "isProjectFeaturedRent": false,
                            "builderPackageMagicNumber": 1,
                            "authorized": true,
                            "localityId": 0,
                            "builderId": 0,
                            "projectTypeId": 0,
                            "computedPriority": 0,
                            "assignedPriority": 0,
                            "forceResale": 0,
                            "minBedrooms": 0,
                            "maxBedrooms": 0,
                            "isResale": false,
                            "isPrimary": false,
                            "isSoldOut": false,
                            "propertySizeMeasure": "sq ft",
                            "has3DImages": false,
                            "resaleEnquiry": false,
                            "hasProjectInsightReport": false
                        }
                    },
                    "propertyId": 5004580,
                    "qualityScore": 9.17857,
                    "relevanceScore": 3.3623984,
                    "popularityScore": 0,
                    "leadStarvationScore": 2.3765512,
                    "sellerResponseRate": 5.020118,
                    "sellerTransactionRevealScore": 0,
                    "topSellerPriority": 0,
                    "listingSellerTransactionStatuses": [
                        "ACCOUNT_LOCKED"
                    ],
                    "sellerLeadDeficitScore": 0,
                    "relevanceScore2": 2.2500105,
                    "unstableRelevanceScoreAfterCompanyProjectLocalityRanking": 0,
                    "unstableRelevanceScoreAfterSellerRanking": 0,
                    "unstableRelevanceScoreAfterBudgetRanking": 0,
                    "resaleURL": "gurgaon/emaar-india-gurgaon-greens-in-sector-102-5332825/3bhk-3t-1650-sqft-apartment",
                    "sellerId": 6603338,
                    "similarListingIds": [
                        5314630,
                        1468776,
                        448597,
                        5901755,
                        5884964,
                        5889367,
                        5827615,
                        5804328,
                        5900242,
                        5839567,
                        5751786,
                        5900804,
                        2340309,
                        5814989,
                        4546582,
                        5757117,
                        5816987,
                        5757176
                    ],
                    "sourceDomainId": 1,
                    "status": "Active",
                    "depositRequired": false,
                    "totalFloors": 14,
                    "tower": {
                        "towerId": 0,
                        "projectId": 0
                    },
                    "updatedAt": 1531478189000,
                    "updatedBy": 8432614,
                    "createdBy": 6603338,
                    "verificationDate": 1531420200000,
                    "displayDate": 1531420200000,
                    "viewDirections": [{
                            "viewType": "Corner"
                        },
                        {
                            "viewType": "Garden View"
                        },
                        {
                            "viewType": "Pool View"
                        },
                        {
                            "viewType": "Road View"
                        }
                    ],
                    "isSellerEditable": true,
                    "listingSourceDomain": "Makaan",
                    "imageQualityTagId": 1,
                    "defaultImageId": 23865046,
                    "recencyScore": 0,
                    "versionNumber": "1531478186799",
                    "sellerCallRatingCount": 2,
                    "isTemplatized": false
                }
            }]
        }
    }]
}

