module.exports = {
    "totalCount": 1,
    "statusCode": "2XX",
    "version": "A",
    "data": {
        "items": [{
            "company": {
                "id": 0
            },
            "companySeller": {
                "id": 0,
                "company": {
                    "id": 0,
                    "type": "Broker"
                },
                "user": {
                    "id": 3508859
                }
            },
            "currentListingPrice": {
                "id": 5805469,
                "price": 2772000
            },
            "id": 569971,
            "listingCategory": "Primary",
            "property": {
                "projectId": 664448,
                "bedrooms": 2,
                "bathrooms": 0,
                "unitTypeId": 1,
                "size": 840,
                "project": {
                    "projectId": 664448
                },
                "servantRoom": 0,
                "poojaRoom": 0,
                "studyRoom": 0
            },
            "sellerId": 3508859
        },{

                "company": {
                    "id": 0
                },
                "companySeller": {
                    "id": 0,
                    "company": {
                        "id": 0,
                        "type": "Broker"
                    },
                    "user": {
                        "id": 3508859
                    }
                },
                "currentListingPrice": {
                    "id": 5805469,
                    "price": 2772000
                },
                "id": 569971,
                "listingCategory": "Primary",
                "property": {
                    "projectId": 664448,
                    "bedrooms": 2,
                    "bathrooms": 0,
                    "unitTypeId": 1,
                    "size": 840,
                    "project": {
                        "projectId": 664448
                    },
                    "servantRoom": 0,
                    "poojaRoom": 0,
                    "studyRoom": 0
                },
                "sellerId": 3508850
        }],
        "facets": {
            "bedrooms": [{
                "1": 3
            }, {
                "2": 4
            }],
            "sellerId": [{
                "3508859": 17,
                "3508850":1
            }]
        }
    }
};
