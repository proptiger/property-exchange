module.exports = {
  "statusCode": "2XX",
  "version": "A",
  "data": [
    {
      "listingCount": 108,
      "agent": {
        "id": 0,
        "userId": 3555590,
        "company": {
          "assist": false,
          "id": 166829,
          "makaanOnly": false,
          "micrositeEnabled": false,
          "micrositeUrl": "realty-shopee-company-profile-166829",
          "name": "Realty Shopee company",
          "score": 9.0,
          "type": "Broker",
          "paidLeadCount": 6
        },
        "user": {
          "id": 3555590,
          "fullName": "Realty Shopee",
          "registered": false,
          "contactNumbers": [
            {
              "id": 0,
              "userId": 0,
              "contactNumber": "01166765394",
              "priority": 2,
              "createdBy": 0,
              "createdAt": 1501585776694,
              "isVerified": false,
              "isActive": true
            }
          ],
          "verified": false
        },
        "left": 0,
        "right": 0,
        "checkAddress": "off",
        "rateOption": "auto"
      },
      "projectId": 504382,
      "localityId": 51518,
      "suburbId": 10024,
      "cityId": 11,
      "builderId": 100316,
      "sellerTransactionStatuses": [
        "DEAL_MAKER"
      ]
    },
    {
      "listingCount": 107,
      "agent": {
        "id": 0,
        "userId": 8251310,
        "company": {
          "assist": false,
          "id": 637904,
          "makaanOnly": false,
          "micrositeEnabled": false,
          "micrositeUrl": "signature-global-profile-637904",
          "name": "signature global",
          "score": 9.0,
          "type": "Broker",
          "paidLeadCount": 1
        },
        "user": {
          "id": 8251310,
          "fullName": "nikhat kalim",
          "registered": false,
          "verified": false
        },
        "left": 0,
        "right": 0,
        "checkAddress": "off",
        "rateOption": "auto"
      },
      "projectId": 1616982,
      "localityId": 50152,
      "suburbId": 10028,
      "cityId": 11,
      "builderId": 263514,
      "sellerTransactionStatuses": [
        "DEAL_MAKER"
      ]
    },
    {
      "listingCount": 197,
      "agent": {
        "id": 0,
        "userId": 8194721,
        "company": {
          "assist": false,
          "id": 634974,
          "makaanOnly": false,
          "micrositeEnabled": false,
          "micrositeUrl": "99estate-profile-634974",
          "name": "99estate",
          "score": 9.0,
          "type": "Broker",
          "paidLeadCount": 1
        },
        "user": {
          "id": 8194721,
          "fullName": "Vijender",
          "registered": false,
          "verified": false,
          "profilePictureURL": "https://content.makaan-ws.com/16/8194721/266/12206411.jpeg"
        },
        "left": 0,
        "right": 0,
        "checkAddress": "off",
        "rateOption": "auto"
      },
      "projectId": 513521,
      "localityId": 50147,
      "suburbId": 10028,
      "cityId": 11,
      "builderId": 100904,
      "sellerTransactionStatuses": [
        "DEAL_MAKER"
      ]
    },
    {
      "listingCount": 5,
      "agent": {
        "id": 0,
        "userId": 6382980,
        "company": {
          "assist": false,
          "id": 352636,
          "makaanOnly": false,
          "micrositeEnabled": false,
          "micrositeUrl": "resale-experts-profile-352636",
          "name": "RESALE EXPERTS",
          "score": 9.0,
          "type": "Broker"
        },
        "user": {
          "id": 6382980,
          "fullName": "mohit",
          "registered": false,
          "contactNumbers": [
            {
              "id": 0,
              "userId": 0,
              "contactNumber": "01130506579",
              "priority": 2,
              "createdBy": 0,
              "createdAt": 1501585776699,
              "isVerified": false,
              "isActive": true
            }
          ],
          "verified": false
        },
        "left": 0,
        "right": 0,
        "checkAddress": "off",
        "rateOption": "auto"
      },
      "projectId": 504430,
      "localityId": 50138,
      "suburbId": 10028,
      "cityId": 11,
      "builderId": 100821
    },
    {
      "listingCount": 6,
      "agent": {
        "id": 0,
        "userId": 7035390,
        "company": {
          "assist": false,
          "id": 557354,
          "makaanOnly": false,
          "micrositeEnabled": false,
          "micrositeUrl": "property-linkers-profile-557354",
          "name": "Property Linkers",
          "score": 9.0,
          "type": "Broker",
          "paidLeadCount": 3
        },
        "user": {
          "id": 7035390,
          "fullName": "Naresh Bajaj",
          "registered": false,
          "verified": false,
          "profilePictureURL": "https://content.makaan-ws.com/16/7035390/266/12083918.png"
        },
        "left": 0,
        "right": 0,
        "checkAddress": "off",
        "rateOption": "auto"
      },
      "projectId": 502060,
      "localityId": 50217,
      "suburbId": 10028,
      "cityId": 11,
      "builderId": 100606,
      "sellerTransactionStatuses": [
        "DEAL_MAKER"
      ]
    }
  ]
};