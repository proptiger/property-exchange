module.exports = {
    "statusCode": "2XX",
    "version": "A",
    "data": {
        "entityId": null,
        "profileCompleteness": {
            "completenessScore": 30
        },
        "questionnaireEntityDetail": null,
        "profilingQuestions": [{
            "id": 8,
            "profilingQuestionnaireId": 1,
            "questionId": 5,
            "masterQuestion": {
                "id": 5,
                "question": "Remarks (if any)",
                "questionTypeId": 2,
                "masterQuestionType": {
                    "id": 2,
                    "type": "textArea"
                },
                "answerOptions": []
            },
            "profilingQuestionnaire": null,
            "previousQuestionId": 6,
            "status": "Active",
            "createdAt": 1529559565000,
            "updatedAt": 1529559565000,
            "allowSkip": false
        }],
        "profilingResponse": {
            "skipQuestion": false
        }
    }
}