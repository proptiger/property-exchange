module.exports = {
  "totalCount": 1,
  "statusCode": "2XX",
  "version": "A",
  "data": {
    "totalCount": 1,
    "results": [
      {
        "id": 447541,
        "clientId": 131108,
        "cityId": 18,
        "countryId": 1,
        "clientTypeId": 3,
        "clientType": {
          "id": 3,
          "clientType": "Not Known"
        },
        "notes": "I am interested in this Property. Please Contact",
        "updatedBy": 1278124,
        "flexibleBudget": false,
        "showSimilar": false,
        "saleTypeId": 1,
        "saleType": {
          "id": 1,
          "saleType": "Primary",
          "marketplaceOnly": false
        },
        "loanRequired": false,
        "clientFurious": false,
        "leadHot": false,
        "latestLeadAssigment": 602712,
        "createdAt": 1363762770000,
        "updatedAt": 1449639947000,
        "timeFrameId": 5,
        "budgetNotProvided": false,
        "saleTypeNotSure": false,
        "clientFirstEnquiryTime": 1363762770000,
        "companyId": 499,
        "leadAssignments": null,
        "enquiries": null,
        "propertyRequirements": [
          {
            "id": 105716253,
            "leadId": 447541,
            "minBudget": 3500000,
            "maxBudget": 3500000,
            "areaUnitTypeId": 1,
            "projectId": 501291,
            "localityId": 51177,
            "updatedAt": 1442201325000
          }
        ],
        "homeloanRequirements": null,
        "leadAssignment": {
          "id": 602712,
          "leadId": 447541,
          "agentId": 1278124,
          "statusId": 7,
          "status": {
            "id": 7,
            "status": "Dead",
            "open": false,
            "index": false,
            "assignmentPriority": 1,
            "displayOrder": 19,
            "isPresalesStatus": true,
            "isSalesStatus": true,
            "isHomeloanStatus": true
          },
          "followUpTime": 1363784400000,
          "clientPreferredTime": false,
          "assingBy": 1277726,
          "createdAt": 1363822837000,
          "updatedAt": 1363842847000,
          "leadSellerAttributeId": 1,
          "leadSellerAttribute": null
        },
        "requirements": null
      }
    ]
  }
};