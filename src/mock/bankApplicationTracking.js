module.exports = {
    statusCode: '2XX',
    data: {
        leadId: '123',
        requirements: [
            {
                projectName: 'Skylark Ithaca',
                projectAddress : 'Sector 349, Gurgaon',
                projectId : 12343214,
                requirementId: 13232,
                creationDate: 'timestamp',
                banks: [
                    {
                        bankId: 2,
                        bankStatusId: '2',
                        status: 'A',
                        text: 'application submitted',
                        appReferenceId: 'ADASS12',
                        jsondump: "string"
                    },
                    {
                        bankId: 3,
                        bankStatusId: '4',
                        status: 'B',
                        appReferenceId: 'AD12S12',
                        text: 'application processesd',
                        jsondump: "string"
                    }
                ]
            },
            {
                requirementId: 132344,
                projectName: 'Skylark Ithaca',
                projectAddress : 'Sector 349, Gurgaon',
                projectId : 12343214,
                creationDate: 'timestamp',
                banks: [
                    {
                        bankId: 3,
                        bankStatusId: '2',
                        status: 'C',
                        appReferenceId: 'ADASS12',
                        text: 'application approved',
                        jsondump: "string"
                    },
                    {
                        bankId: 4,
                        bankStatusId: '4',
                        status: 'D',
                        appReferenceId: 'AD12S12',
                        text: 'application closed',
                        jsondump: "string"
                    }
                ]
            }
        ]
    }
}
