module.exports = [{
    "redirectUrl": "https://goo.gl/forms/NLmbiMsImS3QLZnq1",
    "image": "/images/un-versioned/sponsoredAds/spon-img.jpg",
    "alt": "jms",
    "logo": "/images/un-versioned/sponsoredAds/spon-logo.jpg",
    "content": {
        "default": {
            "title": "Assured Returns with Bank Guarantee",
            "info": "Investment starts @ 20 Lac onwards"
        },
        "rhs": {
            "title": "Book Commercial Property on Dwarka Expressway",
            "info": "12% assured return with bank guarantee"
        }
    },
    "cta": "Enquire Now",
    "visiblityInCities": [ 11 ],
    "pagesToShowOn": ["serp", "property"],
    "saleTypeSupported": ["buy"],
    "id": "ADID_A001",
    "expiresOn": '2018-05-01'
},{
    "redirectUrl": "https://docs.google.com/forms/d/e/1FAIpQLSfb8bV7Mqo0dLFLfdh29ZnKLkBis7bFMaIHvlu62Jd95OnK-g/viewform",
    "image": "/images/un-versioned/sponsoredAds/spon-img-nhr.jpg",
    "alt": "nhr",
    "logo": "/images/un-versioned/sponsoredAds/spon-logo-nhr.png",
    "content": {
        "default": {
            "title": "Lucrative commercial investments starting from 21.51 Lacs",
            "info": "Invest in Signature Global's retail space"
        },
        "rhs": {
            "title": "Lucrative commercial investments starting from 21.51 Lacs",
            "info": "Invest in Signature Global's retail space"
        }
    },
    "cta": "Connect Now",
    "visiblityInCities": [ 11 ],
    "pagesToShowOn": ["serp", "property"],
    "saleTypeSupported": ["buy"],
    "id": "ADID_A002",
    "expiresOn": '2018-06-30'
},{
    "redirectUrl": "https://docs.google.com/forms/d/1Tt_wilC0yWrgr8XhyCgmXrVAjjXE4XttCnq7M2P41rI/edit",
    "image": "/images/un-versioned/sponsoredAds/spon-img-global.jpeg",
    "alt": "global",
    "logo": "/images/un-versioned/sponsoredAds/spon-logo-global.png",
    "content": {
        "default": {
            "title": "Invest in proven commercials jewel starting from 22 Lacs",
            "info": "The Roselia, Sector-95A"
        },
        "rhs": {
            "title": "Invest in proven commercials jewel starting from 22 Lacs",
            "info": "The Roselia, Sector-95A"
        }
    },
    "cta": "Know More",
    "visiblityInCities": [ 11 ],
    "pagesToShowOn": ["serp", "property"],
    "saleTypeSupported": ["buy"],
    "id": "ADID_A003",
    "expiresOn": '2018-07-06'
},{
    "redirectUrl": "http://www.alphathumsector90noida.in",
    "image": "/images/un-versioned/sponsoredAds/spon-img-alphathum.jpg",
    "alt": "nhr",
    "logo": "/images/un-versioned/sponsoredAds/spon-logo-alphathum.jpg",
    "content": {
        "default": {
            "title": "Commercial Investment Starts From Rs. 15.93 Lacs",
            "info": "Bhutani Alphathum, Sector 90 - Noida"
        },
        "rhs": {
            "title": "Commercial Investment Starts From Rs. 15.93 Lacs",
            "info": "Bhutani Alphathum, Sector 90 - Noida"
        }
    },
    "cta": "Enquire Now",
    "visiblityInCities": [ 20 ],
    "pagesToShowOn": ["serp", "property"],
    "saleTypeSupported": ["buy"],
    "id": "ADID_A004",
    "expiresOn": '2018-07-11'
},{
    "redirectUrl": "https://docs.google.com/forms/d/e/1FAIpQLSdclewBh0-Y2-5wmaNAQ1Lt2I6oatKdVdiyhn7cRKpO4dG5Vg/viewform?c=0&w=1",
    "image": "/images/un-versioned/sponsoredAds/spon-img-smc.jpg",
    "alt": "smc",
    "logo": "/images/un-versioned/sponsoredAds/spon-logo-smc.jpg",
    "content": {
        "default": {
            "title": "High-end Commercial Investment Starting from 27 Lacs",
            "info": "Signum 93 by Signature Global"
        },
        "rhs": {
            "title": "High-end Commercial Investment Starting from 27 Lacs",
            "info": "Signum 93 by Signature Global"
        }
    },
    "cta": "Know More",
    "visiblityInCities": [ 11 ],
    "pagesToShowOn": ["serp", "property"],
    "saleTypeSupported": ["buy"],
    "id": "ADID_A005",
    "expiresOn": '2018-07-23'
},{
    "redirectUrl": "http://www.gohappybuying.com/commercial-projects/chandigarh-citi-center",
    "image": "/images/un-versioned/sponsoredAds/spon-img-ccc.png",
    "alt": "ccc",
    "logo": "/images/un-versioned/sponsoredAds/spon-logo-ccc.png",
    "content": {
        "default": {
            "title": "Pillars of Chandigarh",
            "info": "Chandigarh Citi Center, Zirakpur"
        },
        "rhs": {
            "title": "Pillars of Chandigarh",
            "info": "Chandigarh Citi Center, Zirakpur"
        }
    },
    "cta": "Know More",
    "visiblityInCities": [24,6,185,102,129],
    "pagesToShowOn": ["serp", "property"],
    "saleTypeSupported": ["buy"],
    "id": "ADID_A006",
    "expiresOn": '2018-10-11'
}];