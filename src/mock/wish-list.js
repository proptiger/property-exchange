module.exports = {
  "totalCount": 2,
  "statusCode": "2XX",
  "version": "A",
  "data": [
    {
      "projectId": 501660,
      "projectName": "Winter Hills",
      "projectUrl": "gurgaon/sector-77/umang-realtech-winter-hills-501660",
      "typeId": null,
      "listingId": null,
      "bedrooms": null,
      "wishListId": 39763,
      "cityLabel": "Gurgaon",
      "unitName": null,
      "builderName": "Umang Realtech",
      "datetime": 1452043086000,
      "project": {
        "projectId": 501660,
        "isHotProject": false,
        "authorized": true,
        "localityId": 50245,
        "locality": {
          "localityId": 50245,
          "authorized": true,
          "suburbId": 10027,
          "suburb": {
            "id": 10027,
            "cityId": 11,
            "label": "NH8",
            "city": {
              "id": 11,
              "authorized": true,
              "label": "Gurgaon",
              "isServing": false,
              "isServingResale": false,
              "overviewUrl": "gurgaon-real-estate",
              "localityMaxSafetyScore": 8.2,
              "localityMinSafetyScore": 0.6,
              "localityMaxLivabilityScore": 9.7,
              "localityMinLivabilityScore": 4.0,
              "showPolygon": false
            },
            "priority": 0,
            "overviewUrl": "gurgaon-real-estate/nh8-overview-10027",
            "isDescriptionVerified": false
          },
          "cityId": 11,
          "label": "Sector 77",
          "url": "gurgaon/property-sale-sector-77-50245",
          "priority": 0,
          "overviewUrl": "gurgaon-real-estate/sector-77-overview-50245",
          "localitySafetyRank": 62,
          "localityLivabilityRank": 93,
          "projectMaxSafetyScore": 6.4,
          "projectMinSafetyScore": 6.0,
          "projectMaxLivabilityScore": 6.7,
          "projectMinLivabilityScore": 6.3,
          "newsTag": "Umang Realtech Winter Hills",
          "isDescriptionVerified": false,
          "localityHeroshotImageUrl": "https://im.proptiger-ws.com/6/1/92/ahmedabad-heroshot-image-659698.jpeg",
          "minAffordablePrice": 4000000,
          "maxAffordablePrice": 8000000,
          "minLuxuryPrice": 15000000,
          "maxBudgetPrice": 5000000,
          "constructionStatusId": 0
        },
        "builderId": 102867,
        "builder": {
          "id": 102867,
          "name": "Umang Realtech",
          "imageURL": "https://im.proptiger-ws.com/3/2867/13/umang-realtech-1384.jpg",
          "mainImage": {
            "id": 0,
            "imageTypeId": 0,
            "objectId": 0,
            "statusId": 0,
            "sizeInBytes": 0,
            "width": 0,
            "height": 0,
            "altText": "Umang Realtech",
            "active": false,
            "activeStatus": 0,
            "absolutePath": "3/2867/13/umang-realtech-1384.jpg"
          },
          "builderScore": 6.414840517125,
          "isBuilderListed": false,
          "averageDelay": 1
        },
        "name": "Winter Hills",
        "projectTypeId": 0,
        "launchDate": 1343759400000,
        "address": "Sector 77, Gurgaon",
        "completeProjectAddress": "Sector 77, Gurgaon",
        "projectBuilderContact": "9812681368",
        "computedPriority": 57.913461538462,
        "projectEnquiryCount": 2,
        "assignedPriority": 999,
        "assignedLocalityPriority": 999,
        "assignedSuburbPriority": 999,
        "possessionDate": 1480530600000,
        "createdDate": 1425956052000,
        "submittedDate": 1376245800000,
        "imageURL": "https://im.proptiger-ws.com/1/501660/6/pr-builders-winter-hills-elevation-717813.jpeg",
        "offer": "Other",
        "offerHeading": "Offer",
        "offerDesc": "Pay 10% Now & No EMI for 18 Months",
        "URL": "gurgaon/sector-77/umang-realtech-winter-hills-501660",
        "latitude": 28.38174438,
        "longitude": 76.97941589,
        "forceResale": 0,
        "minPricePerUnitArea": 6350,
        "maxPricePerUnitArea": 6550,
        "minSize": 1342,
        "maxSize": 2077,
        "minPrice": 8790100,
        "maxPrice": 13188950,
        "minBedrooms": 0,
        "maxBedrooms": 0,
        "projectStatus": "Under Construction",
        "isResale": false,
        "isPrimary": true,
        "isSoldOut": false,
        "description": "Umang Realtech Winter Hills is a residential complex in Sector 77 in Gurgaon. The complex has 2 and 3BHK apartments on offer that cost between Rs87.2 lakh and 1.3 crore. The complex is awaiting completion shortly and has 724 units available through resale and the developer. The size of the flats range from 1342 to 2077 sq. ft. Spread over 16 acres, the community shall have numerous amenities on offer which include swimming pool, gymnasium, sports facilities, car parking, jogging track and landscaped gardens. Umang Realtech is a reputed property developer of Gurgaon, having worked on six properties so far. Five of their projects are under construction. Sector 77 is positioned quite strategically as the Hero Honda and IFFCO Chowk are 7 and 14 kilometres from here respectively.",
        "totalUnits": 724,
        "sizeInAcres": 16.5,
        "propertySizeMeasure": "sq ft",
        "dominantUnitType": "Apartment",
        "propertyUnitTypes": [
          "Apartment"
        ],
        "images": [
          {
            "id": 717813,
            "imageTypeId": 6,
            "objectId": 501660,
            "path": "1/501660/6/",
            "pageUrl": "gallery/pr-builders-winter-hills-elevation-501660-717813",
            "statusId": 1,
            "createdAt": 1451993197000,
            "sizeInBytes": 958095,
            "width": 1200,
            "height": 900,
            "altText": "pr-builders winter-hills Elevation",
            "title": "Elevation",
            "description": "",
            "priority": 1,
            "waterMarkName": "717813.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "pr-builders-winter-hills-elevation-717813.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/501660/6/pr-builders-winter-hills-elevation-717813.jpeg",
            "imageType": {
              "id": 6,
              "objectType": {
                "id": 1,
                "type": "project"
              },
              "mediaType": {
                "id": 1,
                "name": "Image"
              },
              "objectTypeId": 1,
              "mediaTypeId": 1,
              "type": "main",
              "domainId": 2,
              "priority": 1,
              "imageSitemapEnabled": 1,
              "displayName": "Elevation"
            },
            "masterImageStatuses": {
              "id": 1,
              "status": "PROCESSED"
            }
          },
          {
            "id": 533993,
            "imageTypeId": 6,
            "objectId": 501660,
            "path": "1/501660/6/",
            "pageUrl": "gallery/pr-builders-winter-hills-elevation-501660-533993",
            "statusId": 1,
            "createdAt": 1451993197000,
            "sizeInBytes": 1468775,
            "width": 1982,
            "height": 800,
            "altText": "pr-builders winter-hills Elevation",
            "title": "Elevation",
            "description": "",
            "priority": 2,
            "waterMarkName": "533993.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "pr-builders-winter-hills-elevation-533993.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/501660/6/pr-builders-winter-hills-elevation-533993.jpeg",
            "imageType": {
              "id": 6,
              "objectType": {
                "id": 1,
                "type": "project"
              },
              "mediaType": {
                "id": 1,
                "name": "Image"
              },
              "objectTypeId": 1,
              "mediaTypeId": 1,
              "type": "main",
              "domainId": 2,
              "priority": 1,
              "imageSitemapEnabled": 1,
              "displayName": "Elevation"
            },
            "masterImageStatuses": {
              "id": 1,
              "status": "PROCESSED"
            }
          },
          {
            "id": 630642,
            "imageTypeId": 6,
            "objectId": 501660,
            "path": "1/501660/6/",
            "pageUrl": "gallery/pr-builders-winter-hills-elevation-501660-630642",
            "statusId": 1,
            "createdAt": 1451993197000,
            "sizeInBytes": 716779,
            "width": 1299,
            "height": 1299,
            "altText": "pr-builders winter-hills Elevation",
            "title": "Elevation",
            "description": "",
            "priority": 3,
            "waterMarkName": "630642.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "pr-builders-winter-hills-elevation-630642.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/501660/6/pr-builders-winter-hills-elevation-630642.jpeg",
            "imageType": {
              "id": 6,
              "objectType": {
                "id": 1,
                "type": "project"
              },
              "mediaType": {
                "id": 1,
                "name": "Image"
              },
              "objectTypeId": 1,
              "mediaTypeId": 1,
              "type": "main",
              "domainId": 2,
              "priority": 1,
              "imageSitemapEnabled": 1,
              "displayName": "Elevation"
            },
            "masterImageStatuses": {
              "id": 1,
              "status": "PROCESSED"
            }
          },
          {
            "id": 533992,
            "imageTypeId": 6,
            "objectId": 501660,
            "path": "1/501660/6/",
            "pageUrl": "gallery/pr-builders-winter-hills-elevation-501660-533992",
            "statusId": 1,
            "createdAt": 1451993197000,
            "sizeInBytes": 1726489,
            "width": 1000,
            "height": 1373,
            "altText": "pr-builders winter-hills Elevation",
            "title": "Elevation",
            "description": "",
            "priority": 4,
            "waterMarkName": "533992.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "pr-builders-winter-hills-elevation-533992.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/501660/6/pr-builders-winter-hills-elevation-533992.jpeg",
            "imageType": {
              "id": 6,
              "objectType": {
                "id": 1,
                "type": "project"
              },
              "mediaType": {
                "id": 1,
                "name": "Image"
              },
              "objectTypeId": 1,
              "mediaTypeId": 1,
              "type": "main",
              "domainId": 2,
              "priority": 1,
              "imageSitemapEnabled": 1,
              "displayName": "Elevation"
            },
            "masterImageStatuses": {
              "id": 1,
              "status": "PROCESSED"
            }
          },
          {
            "id": 777405,
            "imageTypeId": 6,
            "objectId": 501660,
            "path": "1/501660/6/",
            "pageUrl": "gallery/pr-builders-winter-hills-elevation-501660-777405",
            "statusId": 1,
            "createdAt": 1451993197000,
            "takenAt": 1406538506000,
            "sizeInBytes": 3584458,
            "width": 5000,
            "height": 3588,
            "altText": "pr-builders winter-hills Elevation",
            "title": "Elevation",
            "priority": 5,
            "waterMarkName": "777405.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "pr-builders-winter-hills-elevation-777405.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/501660/6/pr-builders-winter-hills-elevation-777405.jpeg",
            "imageType": {
              "id": 6,
              "objectType": {
                "id": 1,
                "type": "project"
              },
              "mediaType": {
                "id": 1,
                "name": "Image"
              },
              "objectTypeId": 1,
              "mediaTypeId": 1,
              "type": "main",
              "domainId": 2,
              "priority": 1,
              "imageSitemapEnabled": 1,
              "displayName": "Elevation"
            },
            "masterImageStatuses": {
              "id": 1,
              "status": "PROCESSED"
            }
          },
          {
            "id": 630643,
            "imageTypeId": 6,
            "objectId": 501660,
            "path": "1/501660/6/",
            "pageUrl": "gallery/pr-builders-winter-hills-elevation-501660-630643",
            "statusId": 1,
            "createdAt": 1451993197000,
            "sizeInBytes": 849528,
            "width": 1299,
            "height": 1299,
            "altText": "pr-builders winter-hills Elevation",
            "title": "Elevation",
            "description": "",
            "priority": 5,
            "waterMarkName": "630643.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "pr-builders-winter-hills-elevation-630643.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/501660/6/pr-builders-winter-hills-elevation-630643.jpeg",
            "imageType": {
              "id": 6,
              "objectType": {
                "id": 1,
                "type": "project"
              },
              "mediaType": {
                "id": 1,
                "name": "Image"
              },
              "objectTypeId": 1,
              "mediaTypeId": 1,
              "type": "main",
              "domainId": 2,
              "priority": 1,
              "imageSitemapEnabled": 1,
              "displayName": "Elevation"
            },
            "masterImageStatuses": {
              "id": 1,
              "status": "PROCESSED"
            }
          },
          {
            "id": 776942,
            "imageTypeId": 81,
            "objectId": 501660,
            "path": "1/501660/81/",
            "pageUrl": "gallery/pr-builders-winter-hills-main-other-501660-776942",
            "statusId": 1,
            "createdAt": 1451993197000,
            "takenAt": 1416312965000,
            "sizeInBytes": 5625048,
            "width": 3840,
            "height": 5760,
            "altText": "pr-builders winter-hills Main Other",
            "title": "Main Other",
            "priority": 5,
            "waterMarkName": "776942.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "pr-builders-winter-hills-main-other-776942.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/501660/81/pr-builders-winter-hills-main-other-776942.jpeg",
            "imageType": {
              "id": 81,
              "objectType": {
                "id": 1,
                "type": "project"
              },
              "mediaType": {
                "id": 1,
                "name": "Image"
              },
              "objectTypeId": 1,
              "mediaTypeId": 1,
              "type": "mainOther",
              "domainId": 2,
              "priority": 3,
              "imageSitemapEnabled": 0,
              "displayName": "Main Other"
            },
            "masterImageStatuses": {
              "id": 1,
              "status": "PROCESSED"
            }
          },
          {
            "id": 776940,
            "imageTypeId": 81,
            "objectId": 501660,
            "path": "1/501660/81/",
            "pageUrl": "gallery/pr-builders-winter-hills-main-other-501660-776940",
            "statusId": 1,
            "createdAt": 1451993197000,
            "takenAt": 1416311003000,
            "sizeInBytes": 5484938,
            "width": 5760,
            "height": 3840,
            "altText": "pr-builders winter-hills Main Other",
            "title": "Main Other",
            "priority": 5,
            "waterMarkName": "776940.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "pr-builders-winter-hills-main-other-776940.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/501660/81/pr-builders-winter-hills-main-other-776940.jpeg",
            "imageType": {
              "id": 81,
              "objectType": {
                "id": 1,
                "type": "project"
              },
              "mediaType": {
                "id": 1,
                "name": "Image"
              },
              "objectTypeId": 1,
              "mediaTypeId": 1,
              "type": "mainOther",
              "domainId": 2,
              "priority": 3,
              "imageSitemapEnabled": 0,
              "displayName": "Main Other"
            },
            "masterImageStatuses": {
              "id": 1,
              "status": "PROCESSED"
            }
          },
          {
            "id": 776967,
            "imageTypeId": 81,
            "objectId": 501660,
            "path": "1/501660/81/",
            "pageUrl": "gallery/pr-builders-winter-hills-main-other-501660-776967",
            "statusId": 1,
            "createdAt": 1451993197000,
            "takenAt": 1416308144000,
            "sizeInBytes": 7123800,
            "width": 5760,
            "height": 3840,
            "altText": "pr-builders winter-hills Main Other",
            "title": "Main Other",
            "priority": 5,
            "waterMarkName": "776967.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "pr-builders-winter-hills-main-other-776967.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/501660/81/pr-builders-winter-hills-main-other-776967.jpeg",
            "imageType": {
              "id": 81,
              "objectType": {
                "id": 1,
                "type": "project"
              },
              "mediaType": {
                "id": 1,
                "name": "Image"
              },
              "objectTypeId": 1,
              "mediaTypeId": 1,
              "type": "mainOther",
              "domainId": 2,
              "priority": 3,
              "imageSitemapEnabled": 0,
              "displayName": "Main Other"
            },
            "masterImageStatuses": {
              "id": 1,
              "status": "PROCESSED"
            }
          },
          {
            "id": 776941,
            "imageTypeId": 81,
            "objectId": 501660,
            "path": "1/501660/81/",
            "pageUrl": "gallery/pr-builders-winter-hills-main-other-501660-776941",
            "statusId": 1,
            "createdAt": 1451993197000,
            "takenAt": 1416296098000,
            "sizeInBytes": 6077161,
            "width": 5760,
            "height": 3840,
            "altText": "pr-builders winter-hills Main Other",
            "title": "Main Other",
            "priority": 5,
            "waterMarkName": "776941.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "pr-builders-winter-hills-main-other-776941.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/501660/81/pr-builders-winter-hills-main-other-776941.jpeg",
            "imageType": {
              "id": 81,
              "objectType": {
                "id": 1,
                "type": "project"
              },
              "mediaType": {
                "id": 1,
                "name": "Image"
              },
              "objectTypeId": 1,
              "mediaTypeId": 1,
              "type": "mainOther",
              "domainId": 2,
              "priority": 3,
              "imageSitemapEnabled": 0,
              "displayName": "Main Other"
            },
            "masterImageStatuses": {
              "id": 1,
              "status": "PROCESSED"
            }
          },
          {
            "id": 776938,
            "imageTypeId": 81,
            "objectId": 501660,
            "path": "1/501660/81/",
            "pageUrl": "gallery/pr-builders-winter-hills-main-other-501660-776938",
            "statusId": 1,
            "createdAt": 1451993197000,
            "takenAt": 1416293073000,
            "sizeInBytes": 5793131,
            "width": 5760,
            "height": 3840,
            "altText": "pr-builders winter-hills Main Other",
            "title": "Main Other",
            "priority": 5,
            "waterMarkName": "776938.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "pr-builders-winter-hills-main-other-776938.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/501660/81/pr-builders-winter-hills-main-other-776938.jpeg",
            "imageType": {
              "id": 81,
              "objectType": {
                "id": 1,
                "type": "project"
              },
              "mediaType": {
                "id": 1,
                "name": "Image"
              },
              "objectTypeId": 1,
              "mediaTypeId": 1,
              "type": "mainOther",
              "domainId": 2,
              "priority": 3,
              "imageSitemapEnabled": 0,
              "displayName": "Main Other"
            },
            "masterImageStatuses": {
              "id": 1,
              "status": "PROCESSED"
            }
          },
          {
            "id": 776939,
            "imageTypeId": 81,
            "objectId": 501660,
            "path": "1/501660/81/",
            "pageUrl": "gallery/pr-builders-winter-hills-main-other-501660-776939",
            "statusId": 1,
            "createdAt": 1451993197000,
            "takenAt": 1416289052000,
            "sizeInBytes": 5896543,
            "width": 5760,
            "height": 3840,
            "altText": "pr-builders winter-hills Main Other",
            "title": "Main Other",
            "priority": 5,
            "waterMarkName": "776939.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "pr-builders-winter-hills-main-other-776939.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/501660/81/pr-builders-winter-hills-main-other-776939.jpeg",
            "imageType": {
              "id": 81,
              "objectType": {
                "id": 1,
                "type": "project"
              },
              "mediaType": {
                "id": 1,
                "name": "Image"
              },
              "objectTypeId": 1,
              "mediaTypeId": 1,
              "type": "mainOther",
              "domainId": 2,
              "priority": 3,
              "imageSitemapEnabled": 0,
              "displayName": "Main Other"
            },
            "masterImageStatuses": {
              "id": 1,
              "status": "PROCESSED"
            }
          },
          {
            "id": 777397,
            "imageTypeId": 5,
            "objectId": 501660,
            "path": "1/501660/5/",
            "pageUrl": "gallery/pr-builders-winter-hills-location-plan-501660-777397",
            "statusId": 1,
            "createdAt": 1451993197000,
            "sizeInBytes": 3791764,
            "width": 2370,
            "height": 1660,
            "altText": "pr-builders winter-hills Location Plan",
            "title": "Location Plan",
            "waterMarkName": "777397.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "pr-builders-winter-hills-location-plan-777397.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/501660/5/pr-builders-winter-hills-location-plan-777397.jpeg",
            "imageType": {
              "id": 5,
              "objectType": {
                "id": 1,
                "type": "project"
              },
              "mediaType": {
                "id": 1,
                "name": "Image"
              },
              "objectTypeId": 1,
              "mediaTypeId": 1,
              "type": "locationPlan",
              "domainId": 2,
              "priority": 4,
              "imageSitemapEnabled": 1,
              "displayName": "Location Plan"
            },
            "masterImageStatuses": {
              "id": 1,
              "status": "PROCESSED"
            }
          },
          {
            "id": 777080,
            "imageTypeId": 5,
            "objectId": 501660,
            "path": "1/501660/5/",
            "pageUrl": "gallery/pr-builders-winter-hills-location-plan-501660-777080",
            "statusId": 1,
            "createdAt": 1451993197000,
            "sizeInBytes": 196268,
            "width": 1200,
            "height": 726,
            "altText": "pr-builders winter-hills Location Plan",
            "title": "Location Plan",
            "waterMarkName": "777080.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "pr-builders-winter-hills-location-plan-777080.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/501660/5/pr-builders-winter-hills-location-plan-777080.jpeg",
            "imageType": {
              "id": 5,
              "objectType": {
                "id": 1,
                "type": "project"
              },
              "mediaType": {
                "id": 1,
                "name": "Image"
              },
              "objectTypeId": 1,
              "mediaTypeId": 1,
              "type": "locationPlan",
              "domainId": 2,
              "priority": 4,
              "imageSitemapEnabled": 1,
              "displayName": "Location Plan"
            },
            "masterImageStatuses": {
              "id": 1,
              "status": "PROCESSED"
            }
          },
          {
            "id": 777444,
            "imageTypeId": 10,
            "objectId": 501660,
            "path": "1/501660/10/",
            "pageUrl": "gallery/pr-builders-winter-hills-site-plan-501660-777444",
            "statusId": 1,
            "createdAt": 1451993197000,
            "sizeInBytes": 4310535,
            "width": 1848,
            "height": 2232,
            "altText": "pr-builders winter-hills Site Plan",
            "title": "Site Plan",
            "waterMarkName": "777444.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "pr-builders-winter-hills-site-plan-777444.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/501660/10/pr-builders-winter-hills-site-plan-777444.jpeg",
            "imageType": {
              "id": 10,
              "objectType": {
                "id": 1,
                "type": "project"
              },
              "mediaType": {
                "id": 1,
                "name": "Image"
              },
              "objectTypeId": 1,
              "mediaTypeId": 1,
              "type": "sitePlan",
              "domainId": 2,
              "priority": 5,
              "imageSitemapEnabled": 1,
              "displayName": "Site Plan"
            },
            "masterImageStatuses": {
              "id": 1,
              "status": "PROCESSED"
            }
          },
          {
            "id": 559156,
            "imageTypeId": 2,
            "objectId": 501660,
            "path": "1/501660/2/",
            "pageUrl": "gallery/pr-builders-winter-hills-block-b-terrace-cluster-plan-501660-559156",
            "statusId": 1,
            "createdAt": 1451993197000,
            "sizeInBytes": 1701768,
            "width": 3413,
            "height": 1328,
            "altText": "pr-builders winter-hills Block B Terrace Cluster Plan",
            "title": "Block B Terrace Cluster Plan",
            "description": "",
            "jsonDump": "{\"tower_id\":\"1845\"}",
            "waterMarkName": "559156.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "pr-builders-winter-hills-block-b-terrace-cluster-plan-559156.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/501660/2/pr-builders-winter-hills-block-b-terrace-cluster-plan-559156.jpeg",
            "imageType": {
              "id": 2,
              "objectType": {
                "id": 1,
                "type": "project"
              },
              "mediaType": {
                "id": 1,
                "name": "Image"
              },
              "objectTypeId": 1,
              "mediaTypeId": 1,
              "type": "clusterPlan",
              "domainId": 2,
              "priority": 8,
              "imageSitemapEnabled": 1,
              "displayName": "Cluster Plan"
            },
            "masterImageStatuses": {
              "id": 1,
              "status": "PROCESSED"
            }
          },
          {
            "id": 554478,
            "imageTypeId": 2,
            "objectId": 501660,
            "path": "1/501660/2/",
            "pageUrl": "gallery/pr-builders-winter-hills-cluster-plan-for-11th-floor-501660-554478",
            "statusId": 1,
            "createdAt": 1451993197000,
            "sizeInBytes": 631253,
            "width": 2705,
            "height": 1200,
            "altText": "pr-builders winter-hills Cluster Plan for 11th Floor",
            "title": "Cluster Plan for 11th Floor",
            "description": "",
            "jsonDump": "{\"tower_id\":\"1844\"}",
            "waterMarkName": "554478.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "pr-builders-winter-hills-cluster-plan-for-11th-floor-554478.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/501660/2/pr-builders-winter-hills-cluster-plan-for-11th-floor-554478.jpeg",
            "imageType": {
              "id": 2,
              "objectType": {
                "id": 1,
                "type": "project"
              },
              "mediaType": {
                "id": 1,
                "name": "Image"
              },
              "objectTypeId": 1,
              "mediaTypeId": 1,
              "type": "clusterPlan",
              "domainId": 2,
              "priority": 8,
              "imageSitemapEnabled": 1,
              "displayName": "Cluster Plan"
            },
            "masterImageStatuses": {
              "id": 1,
              "status": "PROCESSED"
            }
          },
          {
            "id": 809624,
            "imageTypeId": 3,
            "objectId": 501660,
            "path": "1/501660/3/",
            "pageUrl": "gallery/winter-hills-tower-l-construction-status-sept-15-501660-809624",
            "statusId": 1,
            "createdAt": 1451993197000,
            "takenAt": 1441065600000,
            "sizeInBytes": 665334,
            "width": 900,
            "height": 1214,
            "altText": " winter-hills Tower L Construction Status Sept-15",
            "title": "Tower L Construction Status Sept-15",
            "jsonDump": "{\"tower_id\":\"1855\"}",
            "waterMarkName": "809624.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "winter-hills-tower-l-construction-status-sept-15-809624.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/501660/3/winter-hills-tower-l-construction-status-sept-15-809624.jpeg",
            "imageType": {
              "id": 3,
              "objectType": {
                "id": 1,
                "type": "project"
              },
              "mediaType": {
                "id": 1,
                "name": "Image"
              },
              "objectTypeId": 1,
              "mediaTypeId": 1,
              "type": "constructionStatus",
              "domainId": 2,
              "priority": 9,
              "imageSitemapEnabled": 0,
              "displayName": "Construction Status"
            },
            "masterImageStatuses": {
              "id": 1,
              "status": "PROCESSED"
            }
          },
          {
            "id": 809623,
            "imageTypeId": 3,
            "objectId": 501660,
            "path": "1/501660/3/",
            "pageUrl": "gallery/winter-hills-tower-k-construction-status-sept-15-501660-809623",
            "statusId": 1,
            "createdAt": 1451993197000,
            "takenAt": 1441065600000,
            "sizeInBytes": 633035,
            "width": 900,
            "height": 1071,
            "altText": " winter-hills Tower K Construction Status Sept-15",
            "title": "Tower K Construction Status Sept-15",
            "jsonDump": "{\"tower_id\":\"1854\"}",
            "waterMarkName": "809623.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "winter-hills-tower-k-construction-status-sept-15-809623.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/501660/3/winter-hills-tower-k-construction-status-sept-15-809623.jpeg",
            "imageType": {
              "id": 3,
              "objectType": {
                "id": 1,
                "type": "project"
              },
              "mediaType": {
                "id": 1,
                "name": "Image"
              },
              "objectTypeId": 1,
              "mediaTypeId": 1,
              "type": "constructionStatus",
              "domainId": 2,
              "priority": 9,
              "imageSitemapEnabled": 0,
              "displayName": "Construction Status"
            },
            "masterImageStatuses": {
              "id": 1,
              "status": "PROCESSED"
            }
          },
          {
            "id": 809622,
            "imageTypeId": 3,
            "objectId": 501660,
            "path": "1/501660/3/",
            "pageUrl": "gallery/winter-hills-tower-d-construction-status-sept-15-501660-809622",
            "statusId": 1,
            "createdAt": 1451993197000,
            "takenAt": 1441065600000,
            "sizeInBytes": 651129,
            "width": 900,
            "height": 1151,
            "altText": " winter-hills Tower D Construction Status Sept-15",
            "title": "Tower D Construction Status Sept-15",
            "jsonDump": "{\"tower_id\":\"1847\"}",
            "waterMarkName": "809622.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "winter-hills-tower-d-construction-status-sept-15-809622.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/501660/3/winter-hills-tower-d-construction-status-sept-15-809622.jpeg",
            "imageType": {
              "id": 3,
              "objectType": {
                "id": 1,
                "type": "project"
              },
              "mediaType": {
                "id": 1,
                "name": "Image"
              },
              "objectTypeId": 1,
              "mediaTypeId": 1,
              "type": "constructionStatus",
              "domainId": 2,
              "priority": 9,
              "imageSitemapEnabled": 0,
              "displayName": "Construction Status"
            },
            "masterImageStatuses": {
              "id": 1,
              "status": "PROCESSED"
            }
          },
          {
            "id": 809621,
            "imageTypeId": 3,
            "objectId": 501660,
            "path": "1/501660/3/",
            "pageUrl": "gallery/winter-hills-tower-f-construction-status-sept-15-501660-809621",
            "statusId": 1,
            "createdAt": 1451993197000,
            "takenAt": 1441065600000,
            "sizeInBytes": 767865,
            "width": 980,
            "height": 1086,
            "altText": " winter-hills Tower F Construction Status Sept-15",
            "title": "Tower F Construction Status Sept-15",
            "jsonDump": "{\"tower_id\":\"1849\"}",
            "waterMarkName": "809621.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "winter-hills-tower-f-construction-status-sept-15-809621.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/501660/3/winter-hills-tower-f-construction-status-sept-15-809621.jpeg",
            "imageType": {
              "id": 3,
              "objectType": {
                "id": 1,
                "type": "project"
              },
              "mediaType": {
                "id": 1,
                "name": "Image"
              },
              "objectTypeId": 1,
              "mediaTypeId": 1,
              "type": "constructionStatus",
              "domainId": 2,
              "priority": 9,
              "imageSitemapEnabled": 0,
              "displayName": "Construction Status"
            },
            "masterImageStatuses": {
              "id": 1,
              "status": "PROCESSED"
            }
          },
          {
            "id": 809619,
            "imageTypeId": 3,
            "objectId": 501660,
            "path": "1/501660/3/",
            "pageUrl": "gallery/winter-hills-tower-e-construction-status-sept-15-501660-809619",
            "statusId": 1,
            "createdAt": 1451993197000,
            "takenAt": 1441065600000,
            "sizeInBytes": 709004,
            "width": 900,
            "height": 1168,
            "altText": " winter-hills Tower E Construction Status Sept-15",
            "title": "Tower E Construction Status Sept-15",
            "jsonDump": "{\"tower_id\":\"1848\"}",
            "waterMarkName": "809619.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "winter-hills-tower-e-construction-status-sept-15-809619.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/501660/3/winter-hills-tower-e-construction-status-sept-15-809619.jpeg",
            "imageType": {
              "id": 3,
              "objectType": {
                "id": 1,
                "type": "project"
              },
              "mediaType": {
                "id": 1,
                "name": "Image"
              },
              "objectTypeId": 1,
              "mediaTypeId": 1,
              "type": "constructionStatus",
              "domainId": 2,
              "priority": 9,
              "imageSitemapEnabled": 0,
              "displayName": "Construction Status"
            },
            "masterImageStatuses": {
              "id": 1,
              "status": "PROCESSED"
            }
          },
          {
            "id": 809618,
            "imageTypeId": 3,
            "objectId": 501660,
            "path": "1/501660/3/",
            "pageUrl": "gallery/winter-hills-construction-status-sept-15-501660-809618",
            "statusId": 1,
            "createdAt": 1451993197000,
            "takenAt": 1441065600000,
            "sizeInBytes": 531810,
            "width": 1000,
            "height": 744,
            "altText": " winter-hills Construction Status Sept-15",
            "title": "Construction Status Sept-15",
            "jsonDump": "{\"tower_id\":null}",
            "waterMarkName": "809618.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "winter-hills-construction-status-sept-15-809618.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/501660/3/winter-hills-construction-status-sept-15-809618.jpeg",
            "imageType": {
              "id": 3,
              "objectType": {
                "id": 1,
                "type": "project"
              },
              "mediaType": {
                "id": 1,
                "name": "Image"
              },
              "objectTypeId": 1,
              "mediaTypeId": 1,
              "type": "constructionStatus",
              "domainId": 2,
              "priority": 9,
              "imageSitemapEnabled": 0,
              "displayName": "Construction Status"
            },
            "masterImageStatuses": {
              "id": 1,
              "status": "PROCESSED"
            }
          },
          {
            "id": 809617,
            "imageTypeId": 3,
            "objectId": 501660,
            "path": "1/501660/3/",
            "pageUrl": "gallery/winter-hills-tower-h-construction-status-sept-15-501660-809617",
            "statusId": 1,
            "createdAt": 1451993197000,
            "takenAt": 1441065600000,
            "sizeInBytes": 492537,
            "width": 900,
            "height": 971,
            "altText": " winter-hills Tower H Construction Status Sept-15",
            "title": "Tower H Construction Status Sept-15",
            "jsonDump": "{\"tower_id\":\"1851\"}",
            "waterMarkName": "809617.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "winter-hills-tower-h-construction-status-sept-15-809617.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/501660/3/winter-hills-tower-h-construction-status-sept-15-809617.jpeg",
            "imageType": {
              "id": 3,
              "objectType": {
                "id": 1,
                "type": "project"
              },
              "mediaType": {
                "id": 1,
                "name": "Image"
              },
              "objectTypeId": 1,
              "mediaTypeId": 1,
              "type": "constructionStatus",
              "domainId": 2,
              "priority": 9,
              "imageSitemapEnabled": 0,
              "displayName": "Construction Status"
            },
            "masterImageStatuses": {
              "id": 1,
              "status": "PROCESSED"
            }
          },
          {
            "id": 809616,
            "imageTypeId": 3,
            "objectId": 501660,
            "path": "1/501660/3/",
            "pageUrl": "gallery/winter-hills-tower-j-construction-status-sept-15-501660-809616",
            "statusId": 1,
            "createdAt": 1451993197000,
            "takenAt": 1441065600000,
            "sizeInBytes": 407048,
            "width": 650,
            "height": 864,
            "altText": " winter-hills Tower J Construction Status Sept-15",
            "title": "Tower J Construction Status Sept-15",
            "jsonDump": "{\"tower_id\":\"1853\"}",
            "waterMarkName": "809616.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "winter-hills-tower-j-construction-status-sept-15-809616.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/501660/3/winter-hills-tower-j-construction-status-sept-15-809616.jpeg",
            "imageType": {
              "id": 3,
              "objectType": {
                "id": 1,
                "type": "project"
              },
              "mediaType": {
                "id": 1,
                "name": "Image"
              },
              "objectTypeId": 1,
              "mediaTypeId": 1,
              "type": "constructionStatus",
              "domainId": 2,
              "priority": 9,
              "imageSitemapEnabled": 0,
              "displayName": "Construction Status"
            },
            "masterImageStatuses": {
              "id": 1,
              "status": "PROCESSED"
            }
          },
          {
            "id": 809615,
            "imageTypeId": 3,
            "objectId": 501660,
            "path": "1/501660/3/",
            "pageUrl": "gallery/winter-hills-tower-l-construction-status-sept-15-501660-809615",
            "statusId": 1,
            "createdAt": 1451993197000,
            "takenAt": 1441065600000,
            "sizeInBytes": 430178,
            "width": 600,
            "height": 875,
            "altText": " winter-hills Tower L Construction Status Sept-15",
            "title": "Tower L Construction Status Sept-15",
            "jsonDump": "{\"tower_id\":\"1855\"}",
            "waterMarkName": "809615.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "winter-hills-tower-l-construction-status-sept-15-809615.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/501660/3/winter-hills-tower-l-construction-status-sept-15-809615.jpeg",
            "imageType": {
              "id": 3,
              "objectType": {
                "id": 1,
                "type": "project"
              },
              "mediaType": {
                "id": 1,
                "name": "Image"
              },
              "objectTypeId": 1,
              "mediaTypeId": 1,
              "type": "constructionStatus",
              "domainId": 2,
              "priority": 9,
              "imageSitemapEnabled": 0,
              "displayName": "Construction Status"
            },
            "masterImageStatuses": {
              "id": 1,
              "status": "PROCESSED"
            }
          },
          {
            "id": 809614,
            "imageTypeId": 3,
            "objectId": 501660,
            "path": "1/501660/3/",
            "pageUrl": "gallery/winter-hills-tower-b-construction-status-sept-15-501660-809614",
            "statusId": 1,
            "createdAt": 1451993197000,
            "takenAt": 1441065600000,
            "sizeInBytes": 434888,
            "width": 600,
            "height": 842,
            "altText": " winter-hills Tower B Construction Status Sept-15",
            "title": "Tower B Construction Status Sept-15",
            "jsonDump": "{\"tower_id\":\"1845\"}",
            "waterMarkName": "809614.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "winter-hills-tower-b-construction-status-sept-15-809614.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/501660/3/winter-hills-tower-b-construction-status-sept-15-809614.jpeg",
            "imageType": {
              "id": 3,
              "objectType": {
                "id": 1,
                "type": "project"
              },
              "mediaType": {
                "id": 1,
                "name": "Image"
              },
              "objectTypeId": 1,
              "mediaTypeId": 1,
              "type": "constructionStatus",
              "domainId": 2,
              "priority": 9,
              "imageSitemapEnabled": 0,
              "displayName": "Construction Status"
            },
            "masterImageStatuses": {
              "id": 1,
              "status": "PROCESSED"
            }
          },
          {
            "id": 809613,
            "imageTypeId": 3,
            "objectId": 501660,
            "path": "1/501660/3/",
            "pageUrl": "gallery/winter-hills-tower-c-construction-status-sept-15-501660-809613",
            "statusId": 1,
            "createdAt": 1451993197000,
            "takenAt": 1441065600000,
            "sizeInBytes": 468944,
            "width": 600,
            "height": 872,
            "altText": " winter-hills Tower C Construction Status Sept-15",
            "title": "Tower C Construction Status Sept-15",
            "jsonDump": "{\"tower_id\":\"1846\"}",
            "waterMarkName": "809613.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "winter-hills-tower-c-construction-status-sept-15-809613.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/501660/3/winter-hills-tower-c-construction-status-sept-15-809613.jpeg",
            "imageType": {
              "id": 3,
              "objectType": {
                "id": 1,
                "type": "project"
              },
              "mediaType": {
                "id": 1,
                "name": "Image"
              },
              "objectTypeId": 1,
              "mediaTypeId": 1,
              "type": "constructionStatus",
              "domainId": 2,
              "priority": 9,
              "imageSitemapEnabled": 0,
              "displayName": "Construction Status"
            },
            "masterImageStatuses": {
              "id": 1,
              "status": "PROCESSED"
            }
          },
          {
            "id": 809612,
            "imageTypeId": 3,
            "objectId": 501660,
            "path": "1/501660/3/",
            "pageUrl": "gallery/winter-hills-tower-a-construction-status-sept-15-501660-809612",
            "statusId": 1,
            "createdAt": 1451993197000,
            "takenAt": 1441065600000,
            "sizeInBytes": 396126,
            "width": 600,
            "height": 714,
            "altText": " winter-hills Tower A Construction Status Sept-15",
            "title": "Tower A Construction Status Sept-15",
            "jsonDump": "{\"tower_id\":\"1844\"}",
            "waterMarkName": "809612.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "winter-hills-tower-a-construction-status-sept-15-809612.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/501660/3/winter-hills-tower-a-construction-status-sept-15-809612.jpeg",
            "imageType": {
              "id": 3,
              "objectType": {
                "id": 1,
                "type": "project"
              },
              "mediaType": {
                "id": 1,
                "name": "Image"
              },
              "objectTypeId": 1,
              "mediaTypeId": 1,
              "type": "constructionStatus",
              "domainId": 2,
              "priority": 9,
              "imageSitemapEnabled": 0,
              "displayName": "Construction Status"
            },
            "masterImageStatuses": {
              "id": 1,
              "status": "PROCESSED"
            }
          },
          {
            "id": 809611,
            "imageTypeId": 3,
            "objectId": 501660,
            "path": "1/501660/3/",
            "pageUrl": "gallery/winter-hills-tower-g-construction-status-sept-15-501660-809611",
            "statusId": 1,
            "createdAt": 1451993197000,
            "takenAt": 1441065600000,
            "sizeInBytes": 322790,
            "width": 600,
            "height": 761,
            "altText": " winter-hills Tower G Construction Status Sept-15",
            "title": "Tower G Construction Status Sept-15",
            "jsonDump": "{\"tower_id\":\"1850\"}",
            "waterMarkName": "809611.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "winter-hills-tower-g-construction-status-sept-15-809611.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/501660/3/winter-hills-tower-g-construction-status-sept-15-809611.jpeg",
            "imageType": {
              "id": 3,
              "objectType": {
                "id": 1,
                "type": "project"
              },
              "mediaType": {
                "id": 1,
                "name": "Image"
              },
              "objectTypeId": 1,
              "mediaTypeId": 1,
              "type": "constructionStatus",
              "domainId": 2,
              "priority": 9,
              "imageSitemapEnabled": 0,
              "displayName": "Construction Status"
            },
            "masterImageStatuses": {
              "id": 1,
              "status": "PROCESSED"
            }
          },
          {
            "id": 809620,
            "imageTypeId": 3,
            "objectId": 501660,
            "path": "1/501660/3/",
            "pageUrl": "gallery/winter-hills-construction-status-aug-15-501660-809620",
            "statusId": 1,
            "createdAt": 1451993197000,
            "takenAt": 1438387200000,
            "sizeInBytes": 860993,
            "width": 1200,
            "height": 731,
            "altText": " winter-hills Construction Status Aug-15",
            "title": "Construction Status Aug-15",
            "jsonDump": "{\"tower_id\":null}",
            "waterMarkName": "809620.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "winter-hills-construction-status-aug-15-809620.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/501660/3/winter-hills-construction-status-aug-15-809620.jpeg",
            "imageType": {
              "id": 3,
              "objectType": {
                "id": 1,
                "type": "project"
              },
              "mediaType": {
                "id": 1,
                "name": "Image"
              },
              "objectTypeId": 1,
              "mediaTypeId": 1,
              "type": "constructionStatus",
              "domainId": 2,
              "priority": 9,
              "imageSitemapEnabled": 0,
              "displayName": "Construction Status"
            },
            "masterImageStatuses": {
              "id": 1,
              "status": "PROCESSED"
            }
          },
          {
            "id": 787774,
            "imageTypeId": 8,
            "objectId": 501660,
            "path": "1/501660/8/",
            "pageUrl": "gallery/winter-hills-payment-plan-501660-787774",
            "statusId": 1,
            "createdAt": 1451993197000,
            "sizeInBytes": 12892,
            "width": 504,
            "height": 213,
            "altText": " winter-hills Payment Plan",
            "title": "Payment Plan",
            "waterMarkName": "787774.png",
            "active": true,
            "activeStatus": 1,
            "seoName": "winter-hills-payment-plan-787774.png",
            "absolutePath": "https://im.proptiger-ws.com/1/501660/8/winter-hills-payment-plan-787774.png",
            "imageType": {
              "id": 8,
              "objectType": {
                "id": 1,
                "type": "project"
              },
              "mediaType": {
                "id": 1,
                "name": "Image"
              },
              "objectTypeId": 1,
              "mediaTypeId": 1,
              "type": "paymentPlan",
              "domainId": 2,
              "priority": 9,
              "imageSitemapEnabled": 1,
              "displayName": "Payment Plan"
            },
            "masterImageStatuses": {
              "id": 1,
              "status": "PROCESSED"
            }
          }
        ],
        "mainImage": {
          "id": 0,
          "imageTypeId": 0,
          "objectId": 0,
          "statusId": 0,
          "sizeInBytes": 0,
          "width": 0,
          "height": 0,
          "altText": "pr-builders winter-hills Elevation",
          "title": "Elevation",
          "active": false,
          "activeStatus": 0,
          "absolutePath": "1/501660/6/pr-builders-winter-hills-elevation-717813.jpeg"
        },
        "localityLabelPriority": "sector 77:15",
        "suburbLabelPriority": "nh8:100",
        "unitTypeString": "2,3 BHK Apartment",
        "distinctBedrooms": [
          2,
          3
        ],
        "avgPriceRisePercentage": 13.5,
        "avgPriceRiseMonths": 31,
        "derivedAvailability": 50,
        "numberOfTowers": 12,
        "supply": 724,
        "totalProjectDiscussion": 11,
        "paymentPlanUrl": "",
        "offers": [
          {
            "offer": "Other",
            "offerHeading": "Offer",
            "offerDesc": "Pay 10% Now & No EMI for 18 Months"
          }
        ],
        "lastUpdatedDate": 1448821800000,
        "imagesCount": 35,
        "videosCount": 1,
        "avgPricePerUnitArea": 6472.2652,
        "minResaleOrPrimaryPrice": 8790100.0,
        "maxResaleOrPrimaryPrice": 1.318895E7,
        "safetyScore": 6.0,
        "livabilityScore": 6.7,
        "projectLocalityScore": 6.0,
        "primaryScore": 0.0,
        "projectSocietyScore": 7.9,
        "projectSafetyRank": 2,
        "projectLivabilityRank": 1,
        "newsTag": "Umang Realtech Winter Hills",
        "has3DImages": false,
        "imageTypeCount": {
          "clusterPlan": 2,
          "paymentPlan": 1,
          "constructionStatus": 14,
          "locationPlan": 2,
          "main": 6,
          "sitePlan": 1,
          "mainOther": 6
        },
        "resaleEnquiry": false,
        "reachabilityInfo": "%7B%220-10%22%3A%22qhhlDk%60ztMsm%40s~%40t%5DeiArnAiTpnAhTxtAji%40gEl~Ax%60%40lnBs%60AblBi~Bq%5C%5Ck~Bp%5C%5CoI%7DkCdiAsnA%22%2C%2210-20%22%3A%22qhhlDk%60ztMsm%40s~%40t%5DeiArnAiTpnAhTxtAji%40gEl~Ax%60%40lnBs%60AblBi~Bq%5C%5Ck~Bp%5C%5CoI%7DkCdiAsnAujH%3F%3F%3FbkDfr%40%7BxFljE%7CoAlhBthAbgCryCgCzbChPxbCiPfj%40w%7BD%60xDzw%40to%40a%7BBbrAggBim%40ybCs_Gsq%40neAebB%7D%60%40wjAcq%40gmAcaBfNilAkdAg%7BAxLu%5En%60CgkFopC%7DoAnhBpS%7ClCdx%40toB%22%2C%2220-30%22%3A%22gtqlDk%60ztMex%40uoBqS%7DlC%7CoAohBfkFnpCt%5Eo%60Cf%7BAyLhlAjdAbaBgNbq%40fmA%7C%60%40vjAoeAdbBr_Grq%40hm%40xbCcrAfgBuo%40%60%7BBaxD%7Bw%40gj%40v%7BDybChP%7BbCiPsyCfCuhAcgC%7DoAmhBzxFmjEckDgr%40%3F%3Fy%60F%3F%3F%3Fu%5EjeDwg%40rvEjxBf%7DC~cC%60dDnpEr_%40vdEnlFl%7DEgpCrlFi%60%40vaG_mAgx%40%7BoIrZidEviAuwDqiK%7DaB%7BBaoBiBevCwdCq%60%40keAiaEa%7DCv_%40uwD%7BbCqxD%7Cn%40mqJkbB%60nAteJyMtgEfvAftD%22%7D",
        "hasTownship": false,
        "avgFlatsPerFloor": 4.0,
        "delayInMonths": 11,
        "hasProjectInsightReport": false,
        "lastEnquiredDate": 1448724829000,
        "hasPrimaryExpandedListing": 1
      }
    },
    {
      "projectId": 640331,
      "projectName": "Winter Hills",
      "projectUrl": "delhi/shanti-park-dwarka/umang-realtech-winter-hills-640331",
      "typeId": null,
      "listingId": null,
      "bedrooms": null,
      "wishListId": 39764,
      "cityLabel": "Delhi",
      "unitName": null,
      "builderName": "Umang Realtech",
      "datetime": 1452043105000,
      "project": {
        "projectId": 640331,
        "isHotProject": false,
        "authorized": true,
        "localityId": 51800,
        "locality": {
          "localityId": 51800,
          "authorized": true,
          "suburbId": 10212,
          "suburb": {
            "id": 10212,
            "cityId": 6,
            "label": "Dwarka",
            "city": {
              "id": 6,
              "authorized": true,
              "label": "Delhi",
              "isServing": false,
              "isServingResale": false,
              "overviewUrl": "delhi-real-estate",
              "localityMaxSafetyScore": 8.8,
              "localityMinSafetyScore": 2.6,
              "localityMaxLivabilityScore": 9.7,
              "localityMinLivabilityScore": 4.0,
              "showPolygon": false
            },
            "priority": 0,
            "overviewUrl": "delhi-real-estate/dwarka-overview-10212",
            "isDescriptionVerified": false
          },
          "cityId": 6,
          "label": "Shanti Park Dwarka",
          "url": "delhi/property-sale-shanti-park-dwarka-51800",
          "priority": 0,
          "overviewUrl": "delhi-real-estate/shanti-park-dwarka-overview-51800",
          "localitySafetyRank": 37,
          "localityLivabilityRank": 42,
          "projectMaxSafetyScore": 5.2,
          "projectMinSafetyScore": 5.2,
          "projectMaxLivabilityScore": 7.4,
          "projectMinLivabilityScore": 7.4,
          "newsTag": "Umang Realtech Winter Hills",
          "isDescriptionVerified": false,
          "localityHeroshotImageUrl": "https://im.proptiger-ws.com/6/1/92/ahmedabad-heroshot-image-659698.jpeg",
          "minAffordablePrice": 4000000,
          "maxAffordablePrice": 8000000,
          "minLuxuryPrice": 15000000,
          "maxBudgetPrice": 5000000,
          "constructionStatusId": 0
        },
        "builderId": 102867,
        "builder": {
          "id": 102867,
          "name": "Umang Realtech",
          "imageURL": "https://im.proptiger-ws.com/3/2867/13/umang-realtech-1384.jpg",
          "mainImage": {
            "id": 0,
            "imageTypeId": 0,
            "objectId": 0,
            "statusId": 0,
            "sizeInBytes": 0,
            "width": 0,
            "height": 0,
            "altText": "Umang Realtech",
            "active": false,
            "activeStatus": 0,
            "absolutePath": "3/2867/13/umang-realtech-1384.jpg"
          },
          "builderScore": 6.414840517125,
          "isBuilderListed": false,
          "averageDelay": 1
        },
        "name": "Winter Hills",
        "projectTypeId": 0,
        "launchDate": 1317407400000,
        "address": "Shanti Park Dwarka, Delhi",
        "completeProjectAddress": "Uttam Nagar, Dwarka Morh, Delhi",
        "projectBuilderContact": "8010400500",
        "computedPriority": 61.798076923077,
        "projectEnquiryCount": 6,
        "assignedPriority": 999,
        "assignedLocalityPriority": 999,
        "assignedSuburbPriority": 999,
        "possessionDate": 1464719400000,
        "createdDate": 1425956052000,
        "submittedDate": 1381257000000,
        "imageURL": "https://im.proptiger-ws.com/1/640331/6/pr-builders-winter-hills-elevation-700571.jpeg",
        "URL": "delhi/shanti-park-dwarka/umang-realtech-winter-hills-640331",
        "latitude": 28.61820602,
        "longitude": 77.03482819,
        "forceResale": 0,
        "minPricePerUnitArea": 9350,
        "maxPricePerUnitArea": 9550,
        "minSize": 1245,
        "maxSize": 2192,
        "minPrice": 11889750,
        "maxPrice": 20495200,
        "minBedrooms": 0,
        "maxBedrooms": 0,
        "projectStatus": "Under Construction",
        "isResale": true,
        "isPrimary": true,
        "isSoldOut": false,
        "description": "Winter Hills is a latest residential project by Umang Realtech that offers 360 units of luxurious 2, 3 and 4 BHK apartments with sizes ranging between 1,245 sq ft to 2,192 sq ft Umang Realtech aims to deliver world-class apartments at affordable costs. Located in Shanti Park Dwarka, Delhi, Winter Hills comes equipped with modern amenities like children's play area, gymnasium, swimming pool, club house, power backup, landscaped gardens, party hall, car parking, 24x7 security, health club, commercial shops, restaurant and many more. Prominently located at a close proximity to many developed areas like Sectors 2, 3, 4, 5, 6, 10, 12, 13, 16 B and 19 Dwarka, this place is one of most popular residential destinations. Shanti Park Dwarka offers smooth connectivity to Dwarka Mor, Nangloi, Dwarka, Najafgarh, Uttam Nagar and other parts of Delhi. Some of reputed educational institutes located here ate KR Mangalam World School, Meditech Educational Institute &amp; Research Centre, Delhi Degree College and Guru Nanak Dev Polytechnic.",
        "totalUnits": 360,
        "sizeInAcres": 5.5,
        "propertySizeMeasure": "sq ft",
        "dominantUnitType": "Apartment",
        "propertyUnitTypes": [
          "Apartment"
        ],
        "images": [
          {
            "id": 700571,
            "imageTypeId": 6,
            "objectId": 640331,
            "path": "1/640331/6/",
            "pageUrl": "gallery/pr-builders-winter-hills-elevation-640331-700571",
            "statusId": 1,
            "createdAt": 1451993197000,
            "sizeInBytes": 1030409,
            "width": 1200,
            "height": 900,
            "altText": "pr-builders winter-hills Elevation",
            "title": "Elevation",
            "description": "",
            "priority": 1,
            "waterMarkName": "700571.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "pr-builders-winter-hills-elevation-700571.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/640331/6/pr-builders-winter-hills-elevation-700571.jpeg",
            "imageType": {
              "id": 6,
              "objectType": {
                "id": 1,
                "type": "project"
              },
              "mediaType": {
                "id": 1,
                "name": "Image"
              },
              "objectTypeId": 1,
              "mediaTypeId": 1,
              "type": "main",
              "domainId": 2,
              "priority": 1,
              "imageSitemapEnabled": 1,
              "displayName": "Elevation"
            },
            "masterImageStatuses": {
              "id": 1,
              "status": "PROCESSED"
            }
          },
          {
            "id": 777544,
            "imageTypeId": 6,
            "objectId": 640331,
            "path": "1/640331/6/",
            "pageUrl": "gallery/pr-builders-winter-hills-elevation-640331-777544",
            "statusId": 1,
            "createdAt": 1451993197000,
            "sizeInBytes": 2369321,
            "width": 2000,
            "height": 1500,
            "altText": "pr-builders winter-hills Elevation",
            "title": "Elevation",
            "description": "",
            "priority": 2,
            "waterMarkName": "777544.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "pr-builders-winter-hills-elevation-777544.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/640331/6/pr-builders-winter-hills-elevation-777544.jpeg",
            "imageType": {
              "id": 6,
              "objectType": {
                "id": 1,
                "type": "project"
              },
              "mediaType": {
                "id": 1,
                "name": "Image"
              },
              "objectTypeId": 1,
              "mediaTypeId": 1,
              "type": "main",
              "domainId": 2,
              "priority": 1,
              "imageSitemapEnabled": 1,
              "displayName": "Elevation"
            },
            "masterImageStatuses": {
              "id": 1,
              "status": "PROCESSED"
            }
          },
          {
            "id": 777055,
            "imageTypeId": 81,
            "objectId": 640331,
            "path": "1/640331/81/",
            "pageUrl": "gallery/pr-builders-winter-hills-main-other-640331-777055",
            "statusId": 1,
            "createdAt": 1451993197000,
            "takenAt": 1415970730000,
            "sizeInBytes": 739945,
            "width": 1200,
            "height": 800,
            "altText": "pr-builders winter-hills Main Other",
            "title": "Main Other",
            "priority": 5,
            "waterMarkName": "777055.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "pr-builders-winter-hills-main-other-777055.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/640331/81/pr-builders-winter-hills-main-other-777055.jpeg",
            "imageType": {
              "id": 81,
              "objectType": {
                "id": 1,
                "type": "project"
              },
              "mediaType": {
                "id": 1,
                "name": "Image"
              },
              "objectTypeId": 1,
              "mediaTypeId": 1,
              "type": "mainOther",
              "domainId": 2,
              "priority": 3,
              "imageSitemapEnabled": 0,
              "displayName": "Main Other"
            },
            "masterImageStatuses": {
              "id": 1,
              "status": "PROCESSED"
            }
          },
          {
            "id": 777059,
            "imageTypeId": 81,
            "objectId": 640331,
            "path": "1/640331/81/",
            "pageUrl": "gallery/pr-builders-winter-hills-main-other-640331-777059",
            "statusId": 1,
            "createdAt": 1451993197000,
            "takenAt": 1415967488000,
            "sizeInBytes": 9043226,
            "width": 5760,
            "height": 3840,
            "altText": "pr-builders winter-hills Main Other",
            "title": "Main Other",
            "priority": 5,
            "waterMarkName": "777059.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "pr-builders-winter-hills-main-other-777059.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/640331/81/pr-builders-winter-hills-main-other-777059.jpeg",
            "imageType": {
              "id": 81,
              "objectType": {
                "id": 1,
                "type": "project"
              },
              "mediaType": {
                "id": 1,
                "name": "Image"
              },
              "objectTypeId": 1,
              "mediaTypeId": 1,
              "type": "mainOther",
              "domainId": 2,
              "priority": 3,
              "imageSitemapEnabled": 0,
              "displayName": "Main Other"
            },
            "masterImageStatuses": {
              "id": 1,
              "status": "PROCESSED"
            }
          },
          {
            "id": 777058,
            "imageTypeId": 81,
            "objectId": 640331,
            "path": "1/640331/81/",
            "pageUrl": "gallery/pr-builders-winter-hills-main-other-640331-777058",
            "statusId": 1,
            "createdAt": 1451993197000,
            "takenAt": 1415963269000,
            "sizeInBytes": 9997558,
            "width": 5526,
            "height": 3108,
            "altText": "pr-builders winter-hills Main Other",
            "title": "Main Other",
            "priority": 5,
            "waterMarkName": "777058.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "pr-builders-winter-hills-main-other-777058.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/640331/81/pr-builders-winter-hills-main-other-777058.jpeg",
            "imageType": {
              "id": 81,
              "objectType": {
                "id": 1,
                "type": "project"
              },
              "mediaType": {
                "id": 1,
                "name": "Image"
              },
              "objectTypeId": 1,
              "mediaTypeId": 1,
              "type": "mainOther",
              "domainId": 2,
              "priority": 3,
              "imageSitemapEnabled": 0,
              "displayName": "Main Other"
            },
            "masterImageStatuses": {
              "id": 1,
              "status": "PROCESSED"
            }
          },
          {
            "id": 777057,
            "imageTypeId": 81,
            "objectId": 640331,
            "path": "1/640331/81/",
            "pageUrl": "gallery/pr-builders-winter-hills-main-other-640331-777057",
            "statusId": 1,
            "createdAt": 1451993197000,
            "takenAt": 1415953879000,
            "sizeInBytes": 836957,
            "width": 1348,
            "height": 900,
            "altText": "pr-builders winter-hills Main Other",
            "title": "Main Other",
            "priority": 5,
            "waterMarkName": "777057.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "pr-builders-winter-hills-main-other-777057.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/640331/81/pr-builders-winter-hills-main-other-777057.jpeg",
            "imageType": {
              "id": 81,
              "objectType": {
                "id": 1,
                "type": "project"
              },
              "mediaType": {
                "id": 1,
                "name": "Image"
              },
              "objectTypeId": 1,
              "mediaTypeId": 1,
              "type": "mainOther",
              "domainId": 2,
              "priority": 3,
              "imageSitemapEnabled": 0,
              "displayName": "Main Other"
            },
            "masterImageStatuses": {
              "id": 1,
              "status": "PROCESSED"
            }
          },
          {
            "id": 777056,
            "imageTypeId": 81,
            "objectId": 640331,
            "path": "1/640331/81/",
            "pageUrl": "gallery/pr-builders-winter-hills-main-other-640331-777056",
            "statusId": 1,
            "createdAt": 1451993197000,
            "takenAt": 1415946325000,
            "sizeInBytes": 889597,
            "width": 1348,
            "height": 900,
            "altText": "pr-builders winter-hills Main Other",
            "title": "Main Other",
            "priority": 5,
            "waterMarkName": "777056.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "pr-builders-winter-hills-main-other-777056.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/640331/81/pr-builders-winter-hills-main-other-777056.jpeg",
            "imageType": {
              "id": 81,
              "objectType": {
                "id": 1,
                "type": "project"
              },
              "mediaType": {
                "id": 1,
                "name": "Image"
              },
              "objectTypeId": 1,
              "mediaTypeId": 1,
              "type": "mainOther",
              "domainId": 2,
              "priority": 3,
              "imageSitemapEnabled": 0,
              "displayName": "Main Other"
            },
            "masterImageStatuses": {
              "id": 1,
              "status": "PROCESSED"
            }
          },
          {
            "id": 597845,
            "imageTypeId": 81,
            "objectId": 640331,
            "path": "1/640331/81/",
            "pageUrl": "gallery/pr-builders-winter-hills-main-other-640331-597845",
            "statusId": 1,
            "createdAt": 1451993197000,
            "sizeInBytes": 567380,
            "width": 1280,
            "height": 720,
            "altText": "pr-builders winter-hills Main Other",
            "title": "Main Other",
            "description": "",
            "priority": 5,
            "waterMarkName": "597845.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "pr-builders-winter-hills-main-other-597845.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/640331/81/pr-builders-winter-hills-main-other-597845.jpeg",
            "imageType": {
              "id": 81,
              "objectType": {
                "id": 1,
                "type": "project"
              },
              "mediaType": {
                "id": 1,
                "name": "Image"
              },
              "objectTypeId": 1,
              "mediaTypeId": 1,
              "type": "mainOther",
              "domainId": 2,
              "priority": 3,
              "imageSitemapEnabled": 0,
              "displayName": "Main Other"
            },
            "masterImageStatuses": {
              "id": 1,
              "status": "PROCESSED"
            }
          },
          {
            "id": 597844,
            "imageTypeId": 81,
            "objectId": 640331,
            "path": "1/640331/81/",
            "pageUrl": "gallery/pr-builders-winter-hills-main-other-640331-597844",
            "statusId": 1,
            "createdAt": 1451993197000,
            "sizeInBytes": 862334,
            "width": 1280,
            "height": 721,
            "altText": "pr-builders winter-hills Main Other",
            "title": "Main Other",
            "description": "",
            "priority": 5,
            "waterMarkName": "597844.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "pr-builders-winter-hills-main-other-597844.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/640331/81/pr-builders-winter-hills-main-other-597844.jpeg",
            "imageType": {
              "id": 81,
              "objectType": {
                "id": 1,
                "type": "project"
              },
              "mediaType": {
                "id": 1,
                "name": "Image"
              },
              "objectTypeId": 1,
              "mediaTypeId": 1,
              "type": "mainOther",
              "domainId": 2,
              "priority": 3,
              "imageSitemapEnabled": 0,
              "displayName": "Main Other"
            },
            "masterImageStatuses": {
              "id": 1,
              "status": "PROCESSED"
            }
          },
          {
            "id": 597843,
            "imageTypeId": 81,
            "objectId": 640331,
            "path": "1/640331/81/",
            "pageUrl": "gallery/pr-builders-winter-hills-main-other-640331-597843",
            "statusId": 1,
            "createdAt": 1451993197000,
            "sizeInBytes": 653264,
            "width": 1281,
            "height": 720,
            "altText": "pr-builders winter-hills Main Other",
            "title": "Main Other",
            "description": "",
            "priority": 5,
            "waterMarkName": "597843.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "pr-builders-winter-hills-main-other-597843.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/640331/81/pr-builders-winter-hills-main-other-597843.jpeg",
            "imageType": {
              "id": 81,
              "objectType": {
                "id": 1,
                "type": "project"
              },
              "mediaType": {
                "id": 1,
                "name": "Image"
              },
              "objectTypeId": 1,
              "mediaTypeId": 1,
              "type": "mainOther",
              "domainId": 2,
              "priority": 3,
              "imageSitemapEnabled": 0,
              "displayName": "Main Other"
            },
            "masterImageStatuses": {
              "id": 1,
              "status": "PROCESSED"
            }
          },
          {
            "id": 597842,
            "imageTypeId": 81,
            "objectId": 640331,
            "path": "1/640331/81/",
            "pageUrl": "gallery/pr-builders-winter-hills-main-other-640331-597842",
            "statusId": 1,
            "createdAt": 1451993197000,
            "sizeInBytes": 733770,
            "width": 1280,
            "height": 719,
            "altText": "pr-builders winter-hills Main Other",
            "title": "Main Other",
            "description": "",
            "priority": 5,
            "waterMarkName": "597842.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "pr-builders-winter-hills-main-other-597842.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/640331/81/pr-builders-winter-hills-main-other-597842.jpeg",
            "imageType": {
              "id": 81,
              "objectType": {
                "id": 1,
                "type": "project"
              },
              "mediaType": {
                "id": 1,
                "name": "Image"
              },
              "objectTypeId": 1,
              "mediaTypeId": 1,
              "type": "mainOther",
              "domainId": 2,
              "priority": 3,
              "imageSitemapEnabled": 0,
              "displayName": "Main Other"
            },
            "masterImageStatuses": {
              "id": 1,
              "status": "PROCESSED"
            }
          },
          {
            "id": 597841,
            "imageTypeId": 81,
            "objectId": 640331,
            "path": "1/640331/81/",
            "pageUrl": "gallery/pr-builders-winter-hills-main-other-640331-597841",
            "statusId": 1,
            "createdAt": 1451993197000,
            "sizeInBytes": 641022,
            "width": 1280,
            "height": 721,
            "altText": "pr-builders winter-hills Main Other",
            "title": "Main Other",
            "description": "",
            "priority": 5,
            "waterMarkName": "597841.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "pr-builders-winter-hills-main-other-597841.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/640331/81/pr-builders-winter-hills-main-other-597841.jpeg",
            "imageType": {
              "id": 81,
              "objectType": {
                "id": 1,
                "type": "project"
              },
              "mediaType": {
                "id": 1,
                "name": "Image"
              },
              "objectTypeId": 1,
              "mediaTypeId": 1,
              "type": "mainOther",
              "domainId": 2,
              "priority": 3,
              "imageSitemapEnabled": 0,
              "displayName": "Main Other"
            },
            "masterImageStatuses": {
              "id": 1,
              "status": "PROCESSED"
            }
          },
          {
            "id": 597840,
            "imageTypeId": 81,
            "objectId": 640331,
            "path": "1/640331/81/",
            "pageUrl": "gallery/pr-builders-winter-hills-main-other-640331-597840",
            "statusId": 1,
            "createdAt": 1451993197000,
            "sizeInBytes": 467451,
            "width": 1281,
            "height": 720,
            "altText": "pr-builders winter-hills Main Other",
            "title": "Main Other",
            "description": "",
            "priority": 5,
            "waterMarkName": "597840.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "pr-builders-winter-hills-main-other-597840.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/640331/81/pr-builders-winter-hills-main-other-597840.jpeg",
            "imageType": {
              "id": 81,
              "objectType": {
                "id": 1,
                "type": "project"
              },
              "mediaType": {
                "id": 1,
                "name": "Image"
              },
              "objectTypeId": 1,
              "mediaTypeId": 1,
              "type": "mainOther",
              "domainId": 2,
              "priority": 3,
              "imageSitemapEnabled": 0,
              "displayName": "Main Other"
            },
            "masterImageStatuses": {
              "id": 1,
              "status": "PROCESSED"
            }
          },
          {
            "id": 777532,
            "imageTypeId": 5,
            "objectId": 640331,
            "path": "1/640331/5/",
            "pageUrl": "gallery/pr-builders-winter-hills-location-plan-640331-777532",
            "statusId": 1,
            "createdAt": 1451993197000,
            "sizeInBytes": 577460,
            "width": 1500,
            "height": 1476,
            "altText": "pr-builders winter-hills Location Plan",
            "title": "Location Plan",
            "description": "",
            "waterMarkName": "777532.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "pr-builders-winter-hills-location-plan-777532.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/640331/5/pr-builders-winter-hills-location-plan-777532.jpeg",
            "imageType": {
              "id": 5,
              "objectType": {
                "id": 1,
                "type": "project"
              },
              "mediaType": {
                "id": 1,
                "name": "Image"
              },
              "objectTypeId": 1,
              "mediaTypeId": 1,
              "type": "locationPlan",
              "domainId": 2,
              "priority": 4,
              "imageSitemapEnabled": 1,
              "displayName": "Location Plan"
            },
            "masterImageStatuses": {
              "id": 1,
              "status": "PROCESSED"
            }
          },
          {
            "id": 777540,
            "imageTypeId": 7,
            "objectId": 640331,
            "path": "1/640331/7/",
            "pageUrl": "gallery/pr-builders-winter-hills-master-plan-640331-777540",
            "statusId": 1,
            "createdAt": 1451993197000,
            "sizeInBytes": 2247780,
            "width": 2000,
            "height": 1500,
            "altText": "pr-builders winter-hills Master Plan",
            "title": "Master Plan",
            "description": "",
            "waterMarkName": "777540.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "pr-builders-winter-hills-master-plan-777540.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/640331/7/pr-builders-winter-hills-master-plan-777540.jpeg",
            "imageType": {
              "id": 7,
              "objectType": {
                "id": 1,
                "type": "project"
              },
              "mediaType": {
                "id": 1,
                "name": "Image"
              },
              "objectTypeId": 1,
              "mediaTypeId": 1,
              "type": "masterPlan",
              "domainId": 2,
              "priority": 7,
              "imageSitemapEnabled": 1,
              "displayName": "Master Plan"
            },
            "masterImageStatuses": {
              "id": 1,
              "status": "PROCESSED"
            }
          },
          {
            "id": 819417,
            "imageTypeId": 3,
            "objectId": 640331,
            "path": "1/640331/3/",
            "pageUrl": "gallery/winter-hills-tower-f-construction-status-sept-15-640331-819417",
            "statusId": 1,
            "createdAt": 1451993197000,
            "takenAt": 1441065600000,
            "sizeInBytes": 582144,
            "width": 800,
            "height": 1073,
            "altText": " winter-hills Tower F Construction Status Sept-15",
            "title": "Tower F Construction Status Sept-15",
            "jsonDump": "{\"tower_id\":\"1830\"}",
            "waterMarkName": "819417.png",
            "active": true,
            "activeStatus": 1,
            "seoName": "winter-hills-tower-f-construction-status-sept-15-819417.png",
            "absolutePath": "https://im.proptiger-ws.com/1/640331/3/winter-hills-tower-f-construction-status-sept-15-819417.png",
            "imageType": {
              "id": 3,
              "objectType": {
                "id": 1,
                "type": "project"
              },
              "mediaType": {
                "id": 1,
                "name": "Image"
              },
              "objectTypeId": 1,
              "mediaTypeId": 1,
              "type": "constructionStatus",
              "domainId": 2,
              "priority": 9,
              "imageSitemapEnabled": 0,
              "displayName": "Construction Status"
            },
            "masterImageStatuses": {
              "id": 1,
              "status": "PROCESSED"
            }
          },
          {
            "id": 819416,
            "imageTypeId": 3,
            "objectId": 640331,
            "path": "1/640331/3/",
            "pageUrl": "gallery/winter-hills-tower-e-construction-status-sept-15-640331-819416",
            "statusId": 1,
            "createdAt": 1451993197000,
            "takenAt": 1441065600000,
            "sizeInBytes": 684471,
            "width": 800,
            "height": 1063,
            "altText": " winter-hills Tower E Construction Status Sept-15",
            "title": "Tower E Construction Status Sept-15",
            "jsonDump": "{\"tower_id\":\"1829\"}",
            "waterMarkName": "819416.png",
            "active": true,
            "activeStatus": 1,
            "seoName": "winter-hills-tower-e-construction-status-sept-15-819416.png",
            "absolutePath": "https://im.proptiger-ws.com/1/640331/3/winter-hills-tower-e-construction-status-sept-15-819416.png",
            "imageType": {
              "id": 3,
              "objectType": {
                "id": 1,
                "type": "project"
              },
              "mediaType": {
                "id": 1,
                "name": "Image"
              },
              "objectTypeId": 1,
              "mediaTypeId": 1,
              "type": "constructionStatus",
              "domainId": 2,
              "priority": 9,
              "imageSitemapEnabled": 0,
              "displayName": "Construction Status"
            },
            "masterImageStatuses": {
              "id": 1,
              "status": "PROCESSED"
            }
          },
          {
            "id": 819415,
            "imageTypeId": 3,
            "objectId": 640331,
            "path": "1/640331/3/",
            "pageUrl": "gallery/winter-hills-tower-b-construction-status-sept-15-640331-819415",
            "statusId": 1,
            "createdAt": 1451993197000,
            "takenAt": 1441065600000,
            "sizeInBytes": 791790,
            "width": 1070,
            "height": 800,
            "altText": " winter-hills Tower B Construction Status Sept-15",
            "title": "Tower B Construction Status Sept-15",
            "jsonDump": "{\"tower_id\":\"1826\"}",
            "waterMarkName": "819415.png",
            "active": true,
            "activeStatus": 1,
            "seoName": "winter-hills-tower-b-construction-status-sept-15-819415.png",
            "absolutePath": "https://im.proptiger-ws.com/1/640331/3/winter-hills-tower-b-construction-status-sept-15-819415.png",
            "imageType": {
              "id": 3,
              "objectType": {
                "id": 1,
                "type": "project"
              },
              "mediaType": {
                "id": 1,
                "name": "Image"
              },
              "objectTypeId": 1,
              "mediaTypeId": 1,
              "type": "constructionStatus",
              "domainId": 2,
              "priority": 9,
              "imageSitemapEnabled": 0,
              "displayName": "Construction Status"
            },
            "masterImageStatuses": {
              "id": 1,
              "status": "PROCESSED"
            }
          },
          {
            "id": 819414,
            "imageTypeId": 3,
            "objectId": 640331,
            "path": "1/640331/3/",
            "pageUrl": "gallery/winter-hills-tower-a-construction-status-sept-15-640331-819414",
            "statusId": 1,
            "createdAt": 1451993197000,
            "takenAt": 1441065600000,
            "sizeInBytes": 668476,
            "width": 800,
            "height": 1074,
            "altText": " winter-hills Tower A Construction Status Sept-15",
            "title": "Tower A Construction Status Sept-15",
            "jsonDump": "{\"tower_id\":\"1825\"}",
            "waterMarkName": "819414.png",
            "active": true,
            "activeStatus": 1,
            "seoName": "winter-hills-tower-a-construction-status-sept-15-819414.png",
            "absolutePath": "https://im.proptiger-ws.com/1/640331/3/winter-hills-tower-a-construction-status-sept-15-819414.png",
            "imageType": {
              "id": 3,
              "objectType": {
                "id": 1,
                "type": "project"
              },
              "mediaType": {
                "id": 1,
                "name": "Image"
              },
              "objectTypeId": 1,
              "mediaTypeId": 1,
              "type": "constructionStatus",
              "domainId": 2,
              "priority": 9,
              "imageSitemapEnabled": 0,
              "displayName": "Construction Status"
            },
            "masterImageStatuses": {
              "id": 1,
              "status": "PROCESSED"
            }
          },
          {
            "id": 819413,
            "imageTypeId": 3,
            "objectId": 640331,
            "path": "1/640331/3/",
            "pageUrl": "gallery/winter-hills-tower-d-construction-status-sept-15-640331-819413",
            "statusId": 1,
            "createdAt": 1451993197000,
            "takenAt": 1441065600000,
            "sizeInBytes": 813718,
            "width": 800,
            "height": 1075,
            "altText": " winter-hills Tower D Construction Status Sept-15",
            "title": "Tower D Construction Status Sept-15",
            "jsonDump": "{\"tower_id\":\"1828\"}",
            "waterMarkName": "819413.png",
            "active": true,
            "activeStatus": 1,
            "seoName": "winter-hills-tower-d-construction-status-sept-15-819413.png",
            "absolutePath": "https://im.proptiger-ws.com/1/640331/3/winter-hills-tower-d-construction-status-sept-15-819413.png",
            "imageType": {
              "id": 3,
              "objectType": {
                "id": 1,
                "type": "project"
              },
              "mediaType": {
                "id": 1,
                "name": "Image"
              },
              "objectTypeId": 1,
              "mediaTypeId": 1,
              "type": "constructionStatus",
              "domainId": 2,
              "priority": 9,
              "imageSitemapEnabled": 0,
              "displayName": "Construction Status"
            },
            "masterImageStatuses": {
              "id": 1,
              "status": "PROCESSED"
            }
          },
          {
            "id": 819412,
            "imageTypeId": 3,
            "objectId": 640331,
            "path": "1/640331/3/",
            "pageUrl": "gallery/winter-hills-construction-status-sept-15-640331-819412",
            "statusId": 1,
            "createdAt": 1451993197000,
            "takenAt": 1441065600000,
            "sizeInBytes": 633581,
            "width": 1065,
            "height": 800,
            "altText": " winter-hills Construction Status Sept-15",
            "title": "Construction Status Sept-15",
            "jsonDump": "{\"tower_id\":null}",
            "waterMarkName": "819412.png",
            "active": true,
            "activeStatus": 1,
            "seoName": "winter-hills-construction-status-sept-15-819412.png",
            "absolutePath": "https://im.proptiger-ws.com/1/640331/3/winter-hills-construction-status-sept-15-819412.png",
            "imageType": {
              "id": 3,
              "objectType": {
                "id": 1,
                "type": "project"
              },
              "mediaType": {
                "id": 1,
                "name": "Image"
              },
              "objectTypeId": 1,
              "mediaTypeId": 1,
              "type": "constructionStatus",
              "domainId": 2,
              "priority": 9,
              "imageSitemapEnabled": 0,
              "displayName": "Construction Status"
            },
            "masterImageStatuses": {
              "id": 1,
              "status": "PROCESSED"
            }
          },
          {
            "id": 819411,
            "imageTypeId": 3,
            "objectId": 640331,
            "path": "1/640331/3/",
            "pageUrl": "gallery/winter-hills-tower-c-construction-status-sept-15-640331-819411",
            "statusId": 1,
            "createdAt": 1451993197000,
            "takenAt": 1441065600000,
            "sizeInBytes": 680677,
            "width": 800,
            "height": 1064,
            "altText": " winter-hills Tower C Construction Status Sept-15",
            "title": "Tower C Construction Status Sept-15",
            "jsonDump": "{\"tower_id\":\"1827\"}",
            "waterMarkName": "819411.png",
            "active": true,
            "activeStatus": 1,
            "seoName": "winter-hills-tower-c-construction-status-sept-15-819411.png",
            "absolutePath": "https://im.proptiger-ws.com/1/640331/3/winter-hills-tower-c-construction-status-sept-15-819411.png",
            "imageType": {
              "id": 3,
              "objectType": {
                "id": 1,
                "type": "project"
              },
              "mediaType": {
                "id": 1,
                "name": "Image"
              },
              "objectTypeId": 1,
              "mediaTypeId": 1,
              "type": "constructionStatus",
              "domainId": 2,
              "priority": 9,
              "imageSitemapEnabled": 0,
              "displayName": "Construction Status"
            },
            "masterImageStatuses": {
              "id": 1,
              "status": "PROCESSED"
            }
          },
          {
            "id": 819410,
            "imageTypeId": 3,
            "objectId": 640331,
            "path": "1/640331/3/",
            "pageUrl": "gallery/winter-hills-tower-h-construction-status-sept-15-640331-819410",
            "statusId": 1,
            "createdAt": 1451993197000,
            "takenAt": 1441065600000,
            "sizeInBytes": 676714,
            "width": 800,
            "height": 1057,
            "altText": " winter-hills Tower H Construction Status Sept-15",
            "title": "Tower H Construction Status Sept-15",
            "jsonDump": "{\"tower_id\":\"1832\"}",
            "waterMarkName": "819410.png",
            "active": true,
            "activeStatus": 1,
            "seoName": "winter-hills-tower-h-construction-status-sept-15-819410.png",
            "absolutePath": "https://im.proptiger-ws.com/1/640331/3/winter-hills-tower-h-construction-status-sept-15-819410.png",
            "imageType": {
              "id": 3,
              "objectType": {
                "id": 1,
                "type": "project"
              },
              "mediaType": {
                "id": 1,
                "name": "Image"
              },
              "objectTypeId": 1,
              "mediaTypeId": 1,
              "type": "constructionStatus",
              "domainId": 2,
              "priority": 9,
              "imageSitemapEnabled": 0,
              "displayName": "Construction Status"
            },
            "masterImageStatuses": {
              "id": 1,
              "status": "PROCESSED"
            }
          },
          {
            "id": 819409,
            "imageTypeId": 3,
            "objectId": 640331,
            "path": "1/640331/3/",
            "pageUrl": "gallery/winter-hills-construction-status-sept-15-640331-819409",
            "statusId": 1,
            "createdAt": 1451993197000,
            "takenAt": 1441065600000,
            "sizeInBytes": 678298,
            "width": 1059,
            "height": 800,
            "altText": " winter-hills Construction Status Sept-15",
            "title": "Construction Status Sept-15",
            "jsonDump": "{\"tower_id\":null}",
            "waterMarkName": "819409.png",
            "active": true,
            "activeStatus": 1,
            "seoName": "winter-hills-construction-status-sept-15-819409.png",
            "absolutePath": "https://im.proptiger-ws.com/1/640331/3/winter-hills-construction-status-sept-15-819409.png",
            "imageType": {
              "id": 3,
              "objectType": {
                "id": 1,
                "type": "project"
              },
              "mediaType": {
                "id": 1,
                "name": "Image"
              },
              "objectTypeId": 1,
              "mediaTypeId": 1,
              "type": "constructionStatus",
              "domainId": 2,
              "priority": 9,
              "imageSitemapEnabled": 0,
              "displayName": "Construction Status"
            },
            "masterImageStatuses": {
              "id": 1,
              "status": "PROCESSED"
            }
          },
          {
            "id": 819408,
            "imageTypeId": 3,
            "objectId": 640331,
            "path": "1/640331/3/",
            "pageUrl": "gallery/winter-hills-construction-status-sept-15-640331-819408",
            "statusId": 1,
            "createdAt": 1451993197000,
            "takenAt": 1441065600000,
            "sizeInBytes": 782353,
            "width": 1068,
            "height": 800,
            "altText": " winter-hills Construction Status Sept-15",
            "title": "Construction Status Sept-15",
            "jsonDump": "{\"tower_id\":null}",
            "waterMarkName": "819408.png",
            "active": true,
            "activeStatus": 1,
            "seoName": "winter-hills-construction-status-sept-15-819408.png",
            "absolutePath": "https://im.proptiger-ws.com/1/640331/3/winter-hills-construction-status-sept-15-819408.png",
            "imageType": {
              "id": 3,
              "objectType": {
                "id": 1,
                "type": "project"
              },
              "mediaType": {
                "id": 1,
                "name": "Image"
              },
              "objectTypeId": 1,
              "mediaTypeId": 1,
              "type": "constructionStatus",
              "domainId": 2,
              "priority": 9,
              "imageSitemapEnabled": 0,
              "displayName": "Construction Status"
            },
            "masterImageStatuses": {
              "id": 1,
              "status": "PROCESSED"
            }
          },
          {
            "id": 726224,
            "imageTypeId": 3,
            "objectId": 640331,
            "path": "1/640331/3/",
            "pageUrl": "gallery/pr-builders-winter-hills-block-g-construction-status-june-15-640331-726224",
            "statusId": 1,
            "createdAt": 1451993197000,
            "takenAt": 1433116800000,
            "sizeInBytes": 5439424,
            "width": 3456,
            "height": 4608,
            "altText": "pr-builders winter-hills Block G Construction Status June-15",
            "title": "Block G Construction Status June-15",
            "jsonDump": "{\"tower_id\":\"1831\"}",
            "waterMarkName": "726224.jpeg",
            "active": true,
            "activeStatus": 1,
            "seoName": "pr-builders-winter-hills-block-g-construction-status-june-15-726224.jpeg",
            "absolutePath": "https://im.proptiger-ws.com/1/640331/3/pr-builders-winter-hills-block-g-construction-status-june-15-726224.jpeg",
            "imageType": {
              "id": 3,
              "objectType": {
                "id": 1,
                "type": "project"
              },
              "mediaType": {
                "id": 1,
                "name": "Image"
              },
              "objectTypeId": 1,
              "mediaTypeId": 1,
              "type": "constructionStatus",
              "domainId": 2,
              "priority": 9,
              "imageSitemapEnabled": 0,
              "displayName": "Construction Status"
            },
            "masterImageStatuses": {
              "id": 1,
              "status": "PROCESSED"
            }
          },
          {
            "id": 787773,
            "imageTypeId": 8,
            "objectId": 640331,
            "path": "1/640331/8/",
            "pageUrl": "gallery/winter-hills-subvention-scheme-640331-787773",
            "statusId": 1,
            "createdAt": 1451993197000,
            "sizeInBytes": 12862,
            "width": 509,
            "height": 229,
            "altText": " winter-hills Subvention Scheme",
            "title": "Subvention Scheme",
            "waterMarkName": "787773.png",
            "active": true,
            "activeStatus": 1,
            "seoName": "winter-hills-subvention-scheme-787773.png",
            "absolutePath": "https://im.proptiger-ws.com/1/640331/8/winter-hills-subvention-scheme-787773.png",
            "imageType": {
              "id": 8,
              "objectType": {
                "id": 1,
                "type": "project"
              },
              "mediaType": {
                "id": 1,
                "name": "Image"
              },
              "objectTypeId": 1,
              "mediaTypeId": 1,
              "type": "paymentPlan",
              "domainId": 2,
              "priority": 9,
              "imageSitemapEnabled": 1,
              "displayName": "Payment Plan"
            },
            "masterImageStatuses": {
              "id": 1,
              "status": "PROCESSED"
            }
          }
        ],
        "mainImage": {
          "id": 0,
          "imageTypeId": 0,
          "objectId": 0,
          "statusId": 0,
          "sizeInBytes": 0,
          "width": 0,
          "height": 0,
          "altText": "pr-builders winter-hills Elevation",
          "title": "Elevation",
          "active": false,
          "activeStatus": 0,
          "absolutePath": "1/640331/6/pr-builders-winter-hills-elevation-700571.jpeg"
        },
        "localityLabelPriority": "shanti park dwarka:100",
        "suburbLabelPriority": "dwarka:2",
        "unitTypeString": "2,3,4 BHK Apartment",
        "distinctBedrooms": [
          2,
          3,
          4
        ],
        "avgPriceRisePercentage": 7.9,
        "avgPriceRiseMonths": 31,
        "derivedAvailability": 53,
        "numberOfTowers": 8,
        "supply": 360,
        "paymentPlanUrl": "",
        "lastUpdatedDate": 1447353000000,
        "imagesCount": 31,
        "avgPricePerUnitArea": 9438.5333,
        "minResaleOrPrimaryPrice": 1.188975E7,
        "maxResaleOrPrimaryPrice": 2.04952E7,
        "safetyScore": 5.2,
        "livabilityScore": 7.4,
        "projectLocalityScore": 8.3,
        "primaryScore": 0.0,
        "projectSocietyScore": 8.5,
        "projectSafetyRank": 1,
        "projectLivabilityRank": 1,
        "newsTag": "Umang Realtech Winter Hills",
        "has3DImages": false,
        "imageTypeCount": {
          "paymentPlan": 1,
          "constructionStatus": 11,
          "masterPlan": 1,
          "locationPlan": 1,
          "main": 2,
          "mainOther": 11
        },
        "resaleEnquiry": false,
        "hasTownship": false,
        "avgFlatsPerFloor": 4.0,
        "delayInMonths": 15,
        "hasProjectInsightReport": false,
        "lastEnquiredDate": 1449299352000,
        "hasPrimaryExpandedListing": 1
      }
    }
  ]
};