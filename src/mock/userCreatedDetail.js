module.exports = {
    "statusCode": "2XX",
    "version": "A",
    "data": {
        "id": 7712488,
        "email": "kashish.gupta889898@proptiger.com",
        "firstName": null,
        "lastName": null,
        "contactNumber": "9999749722",
        "profileImageUrl": "",
        "createdDate": null,
        "countryId": 1,
        "companyIds": [603585],
        "dashboards": [],
        "appDetails": {
            "B2B": {
                "subscriptions": [{
                    "company": {
                        "assist": false,
                        "brokerContacts": null,
                        "coverages": null,
                        "createdAt": 1484738437000,
                        "id": 603585,
                        "makaanOnly": false,
                        "name": "Dummy",
                        "primaryEmail": "kashish.gupta889898@proptiger.com",
                        "status": "Active",
                        "type": "Other",
                        "updatedAt": 1484738437000
                    },
                    "sections": ["Dashboard", "Builder", "Catchment"],
                    "cities": [{"id": 2, "name": null, "localities": []}],
                    "cityCount": 0,
                    "localityCount": 0,
                    "projectCount": 0,
                    "dataUpdationDate": 1477938600000,
                    "expiryDate": 1484764200000,
                    "userType": "city",
                    "subscriptionStatus": "PendingForActivation",
                    "orderType": null,
                    "subscriptionSource": "Campaign"
                }],
                "preference": {
                    "id": 784,
                    "userId": 7712488,
                    "app": "B2B",
                    "preference": {
                        "areaUnit": "sqft",
                        "unitType": ["Apartment", "Villa"],
                        "budgetRange": {},
                        "majorMovementPercentage": {"monthly": 10, "quarterly": 10, "yearly": 10},
                        "lengthUnit": "m",
                        "catchmentSize": 5000,
                        "yearType": "calendar"
                    },
                    "createdAt": 1484738437000,
                    "updatedAt": 1484738437000
                },
                "now": 1484738438109
            }
        },
        "roles": null,
        "permissions": null,
        "passwordCreated": true,
        "acceptedTermAndConditions": null,
        "contactNumbers": [{
            "id": 5930422,
            "userId": 7712488,
            "contactNumber": "9999749722",
            "priority": 1,
            "createdBy": 7712488,
            "createdAt": 1484738436000,
            "isVerified": false,
            "isActive": true,
            "updatedAt": 1484738436000
        }],
        "gender": null,
        "domainId": 2,
        "attributes": [{"id": 299481, "attributeName": "OTP_DISABLE", "attributeValue": "TRUE"}]
    }
};

// Just for reference
var payload = {
    "contactNumbers": [
        {
            "contactNumber": "9999749722",
            "sendOTP": true,
            "priority": 1
        }
    ],
    "email": "kashish.gupta889898@proptiger.com",
    "isRegister": false
};
