module.exports = {
"totalCount": 16,
"statusCode": "2XX",
"version": "A",
"data": [
{
"suburbId": 10371,
"suburb": {
"label": "Kochi North",
"url": "kochi/property-sale-kochi-north-10371"
},
"label": "Aluva",
"url": "kochi/property-sale-aluva-50564",
"projectCount": 63
},
{
"suburbId": 10371,
"suburb": {
"label": "Kochi North",
"url": "kochi/property-sale-kochi-north-10371"
},
"label": "Pukkattupady",
"url": "kochi/property-sale-pukkattupady-53516",
"projectCount": 1
},
{
"suburbId": 11043,
"suburb": {
"label": "Other",
"url": ""
},
"label": "Dombivili (East)",
"url": "",
"projectCount": 1
},
{
"suburbId": 10082,
"suburb": {
"label": "Hyderabad West",
"url": "hyderabad/property-sale-hyderabad-west-10082"
},
"label": "Kukatpally",
"url": "hyderabad/property-sale-kukatpally-50376",
"projectCount": 128
},
{
"suburbId": 10082,
"suburb": {
"label": "Hyderabad West",
"url": "hyderabad/property-sale-hyderabad-west-10082"
},
"label": "Manikonda",
"url": "hyderabad/property-sale-manikonda-51943",
"projectCount": 127
},
{
"suburbId": 10082,
"suburb": {
"label": "Hyderabad West",
"url": "hyderabad/property-sale-hyderabad-west-10082"
},
"label": "Gachibowli",
"url": "hyderabad/property-sale-gachibowli-50375",
"projectCount": 82
},
{
"suburbId": 10082,
"suburb": {
"label": "Hyderabad West",
"url": "hyderabad/property-sale-hyderabad-west-10082"
},
"label": "Attapur",
"url": "hyderabad/property-sale-attapur-51240",
"projectCount": 36
},
{
"suburbId": 10082,
"suburb": {
"label": "Hyderabad West",
"url": "hyderabad/property-sale-hyderabad-west-10082"
},
"label": "Mokila",
"url": "hyderabad/property-sale-mokila-51354",
"projectCount": 29
},
{
"suburbId": 10082,
"suburb": {
"label": "Hyderabad West",
"url": "hyderabad/property-sale-hyderabad-west-10082"
},
"label": "Shankarpalli",
"url": "hyderabad/property-sale-shankarpalli-52280",
"projectCount": 24
},
{
"suburbId": 10082,
"suburb": {
"label": "Hyderabad West",
"url": "hyderabad/property-sale-hyderabad-west-10082"
},
"label": "Bandlaguda Jagir",
"url": "hyderabad/property-sale-bandlaguda-jagir-51937",
"projectCount": 21
},
{
"suburbId": 10082,
"suburb": {
"label": "Hyderabad West",
"url": "hyderabad/property-sale-hyderabad-west-10082"
},
"label": "Film Nagar",
"url": "hyderabad/property-sale-film-nagar-52015",
"projectCount": 17
},
{
"suburbId": 10082,
"suburb": {
"label": "Hyderabad West",
"url": "hyderabad/property-sale-hyderabad-west-10082"
},
"label": "Kavadiguda",
"url": "hyderabad/property-sale-kavadiguda-51090",
"projectCount": 14
},
{
"suburbId": 10082,
"suburb": {
"label": "Hyderabad West",
"url": "hyderabad/property-sale-hyderabad-west-10082"
},
"label": "Peerancheru",
"url": "hyderabad/property-sale-peerancheru-52092",
"projectCount": 11
},
{
"suburbId": 10242,
"suburb": {
"label": "Lucknow East",
"url": "lucknow/property-sale-lucknow-east-10242"
},
"label": "Gomti Nagar",
"url": "lucknow/property-sale-gomti-nagar-50378",
"projectCount": 44
},
{
"suburbId": 10242,
"suburb": {
"label": "Lucknow East",
"url": "lucknow/property-sale-lucknow-east-10242"
},
"label": "Uattardhona",
"url": "lucknow/property-sale-uattardhona-52124",
"projectCount": 18
},
{
"suburbId": 10242,
"suburb": {
"label": "Lucknow East",
"url": "lucknow/property-sale-lucknow-east-10242"
},
"label": "Gosainganj",
"url": "lucknow/property-sale-gosainganj-52914",
"projectCount": 1
}
]
};
