module.exports = {
    "statusCode": "2XX",
    "version": "A",
    "data": {
        "entityId": null,
        "profileCompleteness": {
            "completenessScore": 30
        },
        "questionnaireEntityDetail": {
            "id": 45,
            "entityId": 8888,
            "profilingQuestionnaireId": 1,
            "updatedAt": 1531206257000
        },
        "profilingQuestions": [{
            "id": 9,
            "profilingQuestionnaireId": 1,
            "questionId": 13,
            "masterQuestion": {
                "id": 13,
                "question": "Where do you work?",
                "questionTypeId": 7,
                "masterQuestionType": {
                    "id": 7,
                    "type": "multiple question"
                },
                "answerOptions": [],
                "createdAt": 1531099788000,
                "updatedAt": 1531099788000
            },
            "profilingQuestionnaire": null,
            "parentQuestionId": 8,
            "previousQuestionId": 0,
            "status": "Active",
            "createdAt": 1529559594000,
            "updatedAt": 1529559594000,
            "allowSkip": false,
            "childQuestions": [{
                    "id": 10,
                    "profilingQuestionnaireId": 1,
                    "questionId": 7,
                    "masterQuestion": {
                        "id": 7,
                        "question": "Office Location",
                        "questionTypeId": 1,
                        "masterQuestionType": {
                            "id": 1,
                            "type": "text"
                        },
                        "answerOptions": []
                    },
                    "profilingQuestionnaire": null,
                    "parentQuestionId": 8,
                    "previousQuestionId": 17,
                    "status": "Active",
                    "createdAt": 1529559619000,
                    "updatedAt": 1529559619000,
                    "allowSkip": false
                },
                {
                    "id": 17,
                    "profilingQuestionnaireId": 1,
                    "questionId": 6,
                    "masterQuestion": {
                        "id": 6,
                        "question": "Company Name",
                        "questionTypeId": 1,
                        "masterQuestionType": {
                            "id": 1,
                            "type": "text"
                        },
                        "answerOptions": []
                    },
                    "profilingQuestionnaire": null,
                    "parentQuestionId": 8,
                    "status": "Active",
                    "createdAt": 1531099911000,
                    "updatedAt": 1531099911000,
                    "allowSkip": false
                }
            ]
        }],
        "profilingResponse": {
            "skipQuestion": false
        }
    }
}