"use strict";

const template = require('services/templateLoader').loadTemplate(__dirname, 'view-coupon.marko'),
    apiService = require('services/apiService'),
    agentService = require('services/agentService'),
    enquiryService = require('services/enquiryService'),
    apiConfig = require('configs/apiConfig'),
    NoInfoTemplate = require('services/templateLoader').loadTemplate(__dirname, 'noInfo.marko'),
    encryptionService = require('services/encryptionService'),
    cityService=require('services/cityService');

function sellerFeedbackQuestions(req){
    let urlConfig = apiConfig.sellerFeedbackTemplate().url;
    return apiService.get(urlConfig, { req }).then((response) => {
        let questions = [], mcq_questions=[];
        if(response && response.data && response.data.ratingParameters){
            questions = response.data.ratingParameters.map((rating)=>{
                let question = {
                    id: rating.id,
                    category: rating.parameter,
                    quest: rating.displayText
                }
                let answerOptions, other;
                if(rating.answerOptions){
                    answerOptions = rating.answerOptions.map((option)=>{
                        return {
                            id: option.id,
                            ratingVal: option.weigtage,
                            rating: ratingValueMap(option.weigtage),
                            item: option.label
                        }
                    })
                }
                if(answerOptions){
                    question['options'] = answerOptions
                }
                if(rating.questionTypeId===2){
                    mcq_questions.push(question)
                }
                return question;
            })
        }
        return mcq_questions
    });
}
function ratingValueMap(value){
    if(value>4){
        return 'best'
    }
    if(value>3){
        return 'good'
    }
    if(value>2){
        return 'average'
    }
    if(value>1){
        return 'poor'
    } 
    return ''
}
function hashContactNumbers(phoneList){
    let phoneHash=''
    if(!phoneList){
        return phoneHash
    }
    phoneList.forEach(item=>{
        let n = item.contactNumber;
        phoneHash += '-' + n;
    })
    return phoneHash;
}
function getLeadData(buyerId,req){
    return new Promise((resolve,reject)=>{
        enquiryService.getLeadDetails({
            fields: 'id,companyId',
            filters: `clientId==${buyerId}`
        },{req}).then((response)=>{
            if(response && response.data && response.data.totalCount>0 && response.data.results && response.data.results.length>0){
                let companyLeadData = {}, 
                    companyIdList = [],
                    leads = response.data.results;
                for(let loop = leads.length-1; loop>=0;loop--){
                    let lead = leads[loop];
                    if(!companyLeadData[lead.companyId]){
                        companyIdList.push(lead.companyId);
                        companyLeadData[lead.companyId] = {
                            leadId: lead.id,
                            city: cityService.getCityNameById(lead.cityId)
                        }
                    }
                }
                if(companyIdList.length>0){
                    getSellerDetails(companyIdList,req).then((result)=>{
                        result.forEach((seller)=>{
                            let tempLeadId = companyLeadData[seller.id].leadId,
                                tempCityName = companyLeadData[seller.id].city;
                            let userName, img, contactNumbers;
                            try{
                                let companyUser = seller.sellers[0].companyUser.user;
                                userName = companyUser.username;
                                img = seller.logo||companyUser.profilePictureURL||seller.coverPicture;
                                contactNumbers=hashContactNumbers(companyUser.contactNumbers)
                            } catch(err){}
                            companyLeadData[seller.id] = {
                                img: img?img+'?width=44&height=44':'',
                                leadId: tempLeadId,
                                companyName: seller.name,
                                userName,
                                contactNumbers,
                                city: tempCityName
                            };
                        });
                        resolve(companyLeadData);
                    }).catch(()=>{
                        logError("Fetch companyInfo failed");
                        reject()
                    })
                }
            } else {
                logError("No info for the user leads");
                reject()
            }
        }).catch((err)=>{
            logError("Fetch LeadInfo failed");
            reject()
        })
    })
}

function getSellerDetails(companyIdList,req){
    return new Promise((resolve,reject)=>{
        agentService.getSellerDetailsByCompanyList(companyIdList, { req }).then((result)=>{
            if(result && result.data && result.data.length>0){
                resolve(result.data);
            } else {
                logError("Fetch sellerInfo failed");
                reject();
            }
        }).catch(()=>{
            logError("Fetch sellerInfo failed");
            reject();
        })  
    })
}

function getUserInfoFromToken(token,req){
    return new Promise((resolve,reject)=>{
        encryptionService.getDataViaToken('getFeedbackTokenData',token, { req }).then((result)=>{
            if(result){
                resolve(result);
            } else {
                logError("Fetch userinfo failed");
                reject();
            }
        }).catch(()=>{
            logError("Fetch userinfo failed");
            reject();
        })  
    })
}
function render(data,req,res){
    template.renderAsync(data, res, "", req);
}
function renderNoInfoTemplate(req,res){
    NoInfoTemplate.renderAsync({}, res, "", req);
}
function logError(msg){
    console.log("Feedback Error: ", msg);
}
module.exports.routeHandler = function(req, res) {
    let data = {
        amount: 1000,
        couponClass:''
    };
    if(!req.query){
        return renderNoInfoTemplate(req,res)
    }
    if(req.query.skipImageUpload){
        data['skipImageUpload'] = true;
    }
    if(req.query.token){
        getUserInfoFromToken(req.query.token,req).then(result=>{
            if(result){
                data.buyerName = result.name;
                data.type=result.saleTypeId==1?'buy':'rent';
                data.buyerId=result.userId;
                if(data.type==='rent'){
                    data.amount=500;
                    data.couponClass='rent';
                }
            }
            Promise.all([getLeadData(data.buyerId,req),sellerFeedbackQuestions(req)]).then(response=>{
                data.questions = response[1];
                data.companyList = response[0];
                render(data,req,res);
            }).catch((err)=>{
                renderNoInfoTemplate(req,res)
            })
        }).catch((err)=>{
            renderNoInfoTemplate(req,res)
        })
    } else {
        data.buyerName = req.query.bname;
        data.sellerId = req.query.sid;
        data.sellerName = req.query.sname;
        data.locality=req.query.cname;
        data.leadSubmitDate=req.query.sdate;
        data.type=req.query.type;
        data.buyerId=req.query.bid;
        data.city=req.query.cname;
        data.leadId = req.query.leadid;
        if(data.type==='rent'){
            data.amount=500;
            data.couponClass='rent';
        } 
        sellerFeedbackQuestions(req).then(questions=>{
            data.questions = questions;
            data.sellerContextFlow = true;
            render(data,req,res);
        }).catch((err)=>{
            logError("Fetch feedback questions failed");
            renderNoInfoTemplate(req,res)
        })
    }
    
};
