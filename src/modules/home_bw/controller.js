"use strict";

//const utils = require('services/utilService'),
  const  desktopTemplate = require('services/templateLoader').loadTemplate(__dirname),
    SharedConfig = require('public/scripts/common/sharedConfig'),
    ApiService = require('services/apiService'),
    BlogService = require('services/blogService'),
    masterDetailService = require('services/masterDetailsService'),
    localityService = require('services/localityService'),
    moment = require("moment");

function _getBusinessWorldLinks(){
    var bw_baseURL = 'http://businessworld.in/';
    return {
        bw_baseURL: bw_baseURL,
        webExclusive: bw_baseURL + 'online-exclusive/',
        columns: bw_baseURL + 'columns/',
        'bw_communities': bw_baseURL + 'bw-communities/',
        'bw_event': bw_baseURL + 'bw-event/',
        'bw_tv': bw_baseURL + 'all-videos/',
        'subscribe': 'http://subscribe.businessworld.in/',
        'magzine_issues': bw_baseURL + 'magazine-issues/' + moment().format('YYYY'),
        'advertize': bw_baseURL + 'advertise-with-BW-Businessworld/',
        'rss': bw_baseURL + 'rss-feed/',
        'contact_us': bw_baseURL + 'contact-us'

    };
}
function _getUserLocalition(req) {
    let urlConfig = SharedConfig.clientApis.getCityByLocation();
    if (req.connection && req.connection.remoteAddress) {
        return ApiService.get(urlConfig, { req }).then((response) => {
            if(response && response.data && response.data.city){
                return {
                    id:response.data.city.id,
                    name: response.data.city.label
                };
            }
        });
    } else {
        return new Promise((resolve) => {
            resolve('');
        });
    }
} 

function _getBlogData(callback, req,tag,count) {
    let config = {
        "count": 3,
        "website": "makaaniq"
    };
    if(tag){
        config['tag'] = tag;
    }
    else{
        config['days'] = 30;
    }
    if(count){
        config['count'] = count;
    }
    BlogService.getPostList(req, config).then((response) => {
        callback(null, response);
    }, err => {
        callback(err);
    });
}

function _getBlogDetails(callback, req,blogId) {
    BlogService.getPostDetail(req, blogId).then((response) => {
        callback(null, response);
    }, err => {
        callback(err);
    });
}


function _getTopLocalities(cityId, callback, req) {
    localityService.getTopLocalities(cityId, {req}).then((response) => {
        callback(null, response.data);
    }, err => {
        callback(err);
    });
}

module.exports.routeHandler = function(req, res) {
    var template = desktopTemplate;

    var data = {
        blogDataRecent: function(args, callback) {
            _getBlogData(callback, req,null,5);
        },
        topLocalities: function(args, callback) {
            _getTopLocalities(args.cityId, callback, req);
        },
        cityList: masterDetailService.getMasterDetails('cityList'),
        userLocation: _getUserLocalition(req),
        businessWorldLinks: _getBusinessWorldLinks()
    };
    if(req.query && req.query.blogId){
        data.blogDetail = function(args,callback){
            _getBlogDetails(callback,req,req.query.blogId);
        };
    } else {
        data.blogDataEditor = function(args, callback) {
            _getBlogData(callback, req,'editor',3);
        };
    }
    template.renderAsync(data, res, "", req);
};
