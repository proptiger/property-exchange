"use strict";

const logger = require('services/loggerService'),
    projectNormalService = require('services/projectDefaultService');


function routeHandler(req, res, next) {
    logger.info('project controller called');
    req.cacheControl = 3600; // 1 hour caching for project page
    projectNormalService.routeHandler(req, res, next, false);
}

module.exports = {
    routeHandler
};
