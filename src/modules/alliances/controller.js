"use strict";
const template = require('services/templateLoader').loadTemplate(__dirname);
const async = require('async');

const utilService = require('services/utilService');
   // _ = utilService._;
//const apiService = require('services/apiService'),
  const  logger = require('services/loggerService'),
    //apiConfig = require('configs/apiConfig'),
    cityService = require('services/cityService'),
    userDetailsService = require('services/userDetailsService'),
    allianceService = require('services/allianceService');

const CITY_COOKIE = 'alliancesCity';

module.exports.routeHandler = function(req, res, next) {
    let query = req.query,
        renderData = {},
        cityId;

    let cityCookie = req.cookies[CITY_COOKIE];

    function getCityQuery() {
        if (query.allianceCities) {
            let cityPromise = new Promise((resolve) => {
                query.cityId = cityId = query.allianceCities;
                resolve(query);
            });
            return cityPromise;
        } else if (cityCookie) {
            let cityPromise = new Promise((resolve) => {
                query.cityId = cityId = cityCookie;
                resolve(query);
            });
            return cityPromise;
        } else {
            return cityService.getUserCityByIp().then(city => {
                if (city) {
                    query.cityId = cityId = city.id;
                }
                return query;
            });
        }
    }
    let asyncCalls = [];
    asyncCalls.push(callback => {
        getCityQuery().then(response => {
            callback(null, response);
        }, error => {
            callback(error);
        });
    });

    asyncCalls.push(callback => {
        userDetailsService.getUserDetails(req, res, next).then(response => {
            response.isLoggedIn = true;
            callback(null, response);
        }, () => {
            let response = {
                isLoggedIn: false
            };
            callback(null, response);
        });
    });

    async.parallel(asyncCalls, (err, results) => {
        if (err) {
            logger.err("Error in alliances async calls", err);
            return next(err);
        }
        allianceService.getAllCategoryData(results[0]).then(categories => {
            renderData.userDetails = results[1] && results[1].isLoggedIn ? results[1] : undefined;
            renderData.categories = categories;
            renderData.cityId = cityId;
            renderData.cityName = cityService.getAllianceCityNameById(parseInt(cityId));
            renderData.isMobile = utilService.isMobileRequest(req);
            renderData.alliancePage = query.serviceId ? true : false;
            if (query.serviceId && categories && !categories.length) {
                allianceService.getServicesCount(results[0]).then(response=>{
                    renderData.servicesCount = response;
                    allianceService.getAllServices(results[0]).then(response=>{
                        renderData.selectedService = response.filter(service=>{
                            return service.id == query.serviceId;
                        });
                        renderData.selectedService = renderData.selectedService.length ? renderData.selectedService[0] : {};
                        renderData.noService = true;
                        template.renderSync(renderData, res, null, null, req);
                    });
                });
            } else {
                template.renderSync(renderData, res, null, null, req);
            }
        }).catch(error => {
            logger.error("Error in alliances rendering", error);
            next(error);
        });

    });
};
