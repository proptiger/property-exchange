"use strict";
const template = require('services/templateLoader').loadTemplate(__dirname),
    landmarkService = require('services/landmarkService'),
    cityService = require('services/cityService'),
    localityService = require('services/localityService'),
    projectService = require('services/projectService'),
    taxonomyService = require('services/taxonomyService'),
    //logger = require('services/loggerService'),
    utils = require('services/utilService'),
    seoService = require('services/seoService'),
    globalConfig = require('configs/globalConfig'),
    _ = utils._,
    builderService = require('services/builderService'),
    BlogService = require('services/blogService');

function _getTopProjects(cityId, callback, req) {
    let data = {
        cityId: cityId
    };
    projectService.getTopProjectsInGA(data, {req}).then((response) => {
        response=_parseSparseData(response);
        callback(null, response);
    }, err => {
        callback(err);
    });
}

function _getPopularLocalities(cityId, callback, req) {
    let data = {
            cityId: cityId
        },
        buySelector = ["buyUrl", "overviewUrl", "listingCountBuy", "suburbId", "url", "locality", "suburb", "label", "localityId"],
        buyGroupSort = [{
            field: "localityListingCountBuy",
            sortOrder: "DESC"
        }],
        rentSelector = ["rentUrl", "overviewUrl", "listingCountRent", "suburbId", "url", "locality", "suburb", "label", "localityId"],
        rentGroupSort = [{
            field: "localityListingCountRent",
            sortOrder: "DESC"
        }];
    Promise.all([localityService.getPopularLocalities(data, {
        fields: buySelector,
        groupSort: buyGroupSort,
        req
    }), localityService.getPopularLocalities(data, {
        fields: rentSelector,
        groupSort: rentGroupSort,
        req
    })]).then((response) => {
        function __removeLowListingUrl(responseObj) {
            var MAX_LINKS = 100,
                totalLinks = 0;
            return Object.values(_parseSparseData(responseObj)||{}).map(rObj=>{
                return Object.assign(rObj, {
                    topProjects: (rObj.topProjects||[]).filter(topProj=>(topProj.listingCount>=5))
                });
            }).sort((aObj, bObj)=>{
                return (bObj.topProjects.length - aObj.topProjects.length);
            }).reduce(function(finalArr, rObj){
                totalLinks += rObj.topProjects.length + 1; // there would be 1 extra suburb link
                if(rObj.topProjects.length && totalLinks < MAX_LINKS) {
                    finalArr.push(rObj);
                }
                return finalArr;
            }, []);
        }
        callback(null, {
            buyLinks: __removeLowListingUrl(response[0]),
            rentLinks: __removeLowListingUrl(response[1])
        });
    }, err => {
        callback(err);
    });
}

//handling new city for sparse content
function _parseSparseData(response ={}) {
    if (Object.keys(response).length < 3) {
        let topProjects = [],
            length, updatedLinks = [];
        for (var i in response) {
            topProjects = topProjects.concat(response[i].topProjects || []);
        }
        length = Math.ceil(topProjects.length / 4);
        for (let i = 0; i < 4 && topProjects.length>0; i++) {
            updatedLinks.push({ topProjects: topProjects.splice(0, length) });
        }
        return updatedLinks;
    } else {
        return response;
    }
}


function _getTaxonomyUrl(data, callback, req) {
    let selectorData = {
        cityId: data.cityId,
        cityName: data.cityName
    };

    if (data.minAffordablePrice && data.maxAffordablePrice) {
        selectorData.minAffordablePrice = data.minAffordablePrice;
        selectorData.maxAffordablePrice = data.maxAffordablePrice;
    }

    if (data.minLuxuryPrice) {
        selectorData.minLuxuryPrice = data.minLuxuryPrice;
    }

    if (data.maxBudgetPrice) {
        selectorData.maxBudgetPrice = data.maxBudgetPrice;
    }

    selectorData.isMobileRequest = data.mobileView;


    taxonomyService.getTaxonomyUrl(selectorData, {req}).then((response) => {
        let isEmpty = response[0]['Recent properties'] && response[0]['Recent properties'].listingUrls.length;
        callback(null, { data: response, isEmpty });

    }, err => {
        callback(err);
    });
}

function _getTopLocalities(cityId, callback, req) {
    localityService.getTopLocalities(cityId, {req}).then((response) => {
        callback(null, response);
    }, err => {
        callback(err);
    });
}

function _getBlogData(callback, req) {

    let blogIqUrl = process.env.BASE_URL_MAKAANIQ || '', 
        configCity = {
            "text": req.urlDetail.cityName,
            "count": 5,
            "sort": "views-desc",
            "website": "makaaniq"
        },
        configTop = {
            "count": 5,
            "days": 50,
            "sort": "views-desc",
            "website": "makaaniq"
        };
    if(req.locals.numberFormat && req.locals.numberFormat.code) {
      configCity.website = configTop.website = "iq" + req.locals.numberFormat.code;
      blogIqUrl = blogIqUrl.replace(/\/iq\//,'/'+req.locals.numberFormat.code+'/iq/');
    }
    Promise.all([BlogService.getPostList(req, configCity), BlogService.getPostList(req, configTop)]).then((response) => {
        let post = [];
        if (_.isArray(response[0].data) && _.isArray(response[1].data)) {
            post = response[0].data.concat(response[1].data).slice(0, 5);
        }
        callback(null, {
            post,
            makaaniqUrl: blogIqUrl
        });
    }, err => {
        callback(err);
    });
}

function _getTopBuilders(cityId, callback, req) {
    builderService.getTopBuilders({
        "cityId": cityId,
        "count": 30
    }, {req}).then(function(response) {
        callback(null, response);
    }, err => {
        callback(err);
    });
}

function _getProjectCount(pageData, data, callback, req) {
    let seoUrlParams = [];

    _.forEach(globalConfig.templateId.cityProjectSerp, (v) => {
        seoUrlParams.push({
            "urlDomain": "city",
            "domainIds": [pageData.cityId],
            "urlCategoryName": v
        });
    });
    Promise.all([projectService.getProject({ cityId: pageData.cityId, projectCount: data.projectCount }, req), seoService.getEntityUrls(seoUrlParams, req)]).then((response) => {
        _.forEach(globalConfig.templateId.cityProjectSerp, (v, k) => {
            response[1][k] = response[1][v] && response[1][v][pageData.cityId];
        });
        callback(null, response);
    }, err => {
        callback(err);
    });
}

function _getLandmarkData(cityId, callback, req){
    return landmarkService.getTopLandmarks({
        cityId, 
        "filters": [{"key": "cityId", "value": cityId}],
        "rows": 50
    }, {req}).then((response)=>{
        callback(null, response.results);
    },callback).catch(callback);
}

function routeHandler(req, res, next) {
    let cityId = req.urlDetail.cityId,
        urlDetail = req.urlDetail,
        pageData = {
            cityId: urlDetail.cityId,
            cityName: urlDetail.cityName,
            mobileView: utils.isMobileRequest(req) ? true : false
        };

    cityService.getCityOverview(cityId, req, {}).then((response) => {

        response.isMasterPlanSupported = utils.isMasterPlanSupported({
            name: req.urlDetail.cityName,
            id: req.urlDetail.cityId
        });
        response.mobileView = utils.isMobileRequest(req) ? true : false;
        template.renderAsync({
            data: response,
            pageData,
            schemaMap: globalConfig.schemaMap,
            topBuildersDataProvider: function(args, callback) {
                _getTopBuilders(cityId, callback, req);
            },
            projectCountInCityProvider: function(args, callback) {
                _getProjectCount(pageData, response, callback, req);
            },
            taxonomyUrlDataProvider: function(args, callback) {
                _getTaxonomyUrl(response, callback, req);
            },
            topLocalities: function(args, callback) {
                _getTopLocalities(cityId, callback, req);
            },
            topProjectDataProvider: function(args, callback) {
                _getTopProjects(cityId, callback, req);
            },
            popularLocalitiesDataProvider: function(args, callback) {
                _getPopularLocalities(cityId, callback, req);
            },
            topLandmarkDataProvider: function(args, callback) {
                _getLandmarkData(cityId, callback, req);
            },
            blogData: function(args, callback) {
                _getBlogData(callback, req);
            }
        }, res, "", req);
    }, err => {
        next(err);
    }).catch(err => {
        next(err);
    });
}

module.exports = {
    routeHandler
};
