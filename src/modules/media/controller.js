"use strict";
/**
 * controller setup for 500
 * @param  {Object} router
 */

const template = require('services/templateLoader').loadTemplate(__dirname),
    blogService = require('services/blogService');

module.exports.routeHandler = function(req, res) {
    let configPhoto = {
            "category": "A-Z Glossary",
            "count": 6
        },
        configVideo = {
            "category": "Video",
            "count": 6
        },
        data = {
            photo: blogService.getPostList(req, configPhoto),
            video: blogService.getPostList(req, configVideo),
            flag: 3
        };
    template.renderAsync(data, res, '', req);
};
