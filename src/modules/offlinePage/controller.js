"use strict";
/**
 * controller setup for offlie page
 * @param  {Object} router
 */

 const template = require('services/templateLoader').loadTemplate(__dirname);

module.exports.routeHandler = function(req,res) {
    req.originalUrl = '';
    req.skipAjaxyCaching = 'true';
    template.renderSync({}, res, null, null, req);
};
