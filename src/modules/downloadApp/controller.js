"use strict";
const logger = require('services/loggerService'),
	utils = require('services/utilService'),
	globalConfig = require('configs/globalConfig'),
	template = require('services/templateLoader').loadTemplate(__dirname);

module.exports.routeHandler = function(req, res) {
	logger.info('inside the download app route');
	let os = utils.getMobileOperatingSystem(req.header('user-agent'));
	if(os=='iOS' || os=='IEMobile' || os=='BlackBerry'){
		template.renderAsync({}, res, '', req);
	} else {
		logger.info('redirecting to play store');
		res.redirect(302, globalConfig.googlePlayLink);
		return;
	}

};
