"use strict";

/*
Description: This registers routes for all api wrappers and assigns respective handlers
*/

const logger = require('services/loggerService'),
    utilService = require('services/utilService'),
    _ = utilService._,
    apiHandlers = require('public/scripts/common/sharedConfig').apiHandlers;
module.exports.setup = function(router) {
    for (let apiUrl in apiHandlers) {
        let _apisHandler = apiHandlers[apiUrl]();
        var parser = require('./apiParsers/' + _apisHandler.controller);
        if (parser) {
            let methods = _apisHandler.method;
            if(typeof _apisHandler.route == "string"){
                _apisHandler.route = [_apisHandler.route];
            }
            _.forEach(_apisHandler.route, (v) => {     //jshint ignore:line
                let parserRouter = router.route(v);
                if(methods.indexOf("GET") > -1){
                    parserRouter.get(parser.apiHandler);
                }
                if(methods.indexOf("POST") > -1){
                    parserRouter.post(parser.apiHandler);
                }
                if(methods.indexOf("PUT") > -1){
                    parserRouter.put(parser.apiHandler);
                }
                if(methods.indexOf("PATCH") > -1){
                    parserRouter.patch(parser.apiHandler);
                }
                if(methods.indexOf("DELETE") > -1){
                    parserRouter.delete(parser.apiHandler);
                }
            });
        }
    }

    router.get('/apis/*', (req, res) => {
        logger.error('Error: Url ' + req.originalUrl + ' does not exit.');
        res.status(404).send({
            message: 'this url does not exist !!'
        });
    });
};
