"use strict";
const apiService = require('services/apiService'),
      apiConfig = require('configs/apiConfig'),
      utils = require('services/utilService'),
      imageParser = require('services/imageParser'),
      mappingService = require('services/mappingService');


function _parseSellerDetails (response,isMobile) {
    let imageSize = 'squareTile';
    if(isMobile){
        imageSize = 'smallThumbnail';
    }
    response = response.data;
    let result = {selectSellers:[],nonSelectSellers:[]},
        i, length = response.length;
    for (i = 0; i < length; i++) {
        let seller = response[i].sellerDetails || {},
            company = seller && seller.companyUser && seller.companyUser.company || {},
            user = seller && seller.companyUser && seller.companyUser.user || {},
            temp = {};

        temp.companyId = company.id;
        temp.companyName = company.name;
        temp.logo = imageParser.appendImageSize(company.logo || user.profilePictureURL, imageSize);
        temp.sellerUserId = seller.sellerUserId;
        temp.companyAvatar = utils.getAvatar(company.name);
        temp.contact = (user.contactNumbers && user.contactNumbers[0] && user.contactNumbers[0].contactNumber) || 'n.a.';
        temp.isMakaanSelectSeller = response[i].isMakaanSelectSeller || false;
        temp.sellerName = user.fullName;
        temp.sellerEmail = utils.getSellerValidEmail(user);
        if(temp.isMakaanSelectSeller){
            result.selectSellers.push(temp);
        }else{
            result.nonSelectSellers.push(temp);
        }
    }
    return result;
};
module.exports.apiHandler = function(req, res, next) {
    let requestMethod = req.method ||'';
    switch(requestMethod){
        case 'GET': 
            if(req.query.buyerUserId){
                let buyerUserId = req.query.buyerUserId||'';
                let saleType = req.query.saleType||'';
                let urlConfig = apiConfig.sellerDetailByBuyerUserId({buyerUserId,saleType});
                let isMobileRequest = utils.isMobileRequest(req);
                return apiService.get(urlConfig, {req}).then(response => {
                    if(response && response.data){
                        let sellerDetails = _parseSellerDetails (response,isMobileRequest)||{};
                        res.send(sellerDetails);
                    }
                },(err)=>{
                    next(err);
                });
            }else{
                next(new Error("no userId found"));
            }
            break;
        case 'POST': 
            if(req.query.buyerUserId){
                let buyerUserId = req.query.buyerUserId||'';
                let sellerUserId = req.query.sellerUserId||'';
                let body ={buyerUserId,sellerUserId};
                let urlConfig = apiConfig.storeBuyerSeller();
                return apiService.post(urlConfig, {body}).then(response => {
                    res.send(response);
                },(err)=>{
                    next(err);
                });
            }else{
                next(new Error("no userId found"));
            }
            break;
    }
    
};
