"use strict";
const logger = require('services/loggerService'),
    propertyService = require('services/propertyService'),
    utilService = require('services/utilService'),
    gridTemplate = require('services/templateLoader').loadTemplate(__dirname, '../../../tags/seller-data/template.marko');

module.exports.apiHandler = function(req, res, next) {
    logger.info('entered the similarConfigSeller Route');
    let params = req.query;

    if (params.projectId) {
        propertyService.getSellerDetails({ params }, { req }).then(response => {
            gridTemplate.renderAsync({
                griddata: response.sellerList
            }, res, '', req);
        }, err => {
            next(err);
        }).catch(err => {
            next(err);
        });
    } else {
        next(utilService.getError(400));
    }

};
