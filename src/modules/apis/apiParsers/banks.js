"use strict";
const bankService = require('services/bankService');

module.exports.apiHandler = function(req, res, next) {
    //let data = {};

    bankService.getBankDetails({req: req}).then((response) => {
        req.cacheControl = "691200";
        response = response || {};
        res.send(response);
    }, err => {
        next(err);
    }).catch(err => {
        next(err);
    });
};
