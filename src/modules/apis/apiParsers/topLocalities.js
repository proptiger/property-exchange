"use strict";
    const localityService = require('services/localityService');

module.exports.apiHandler = function(req, res, next) {
    let params = req.params;
    if(params && params.cityId){
        localityService.getTopLocalities(params.cityId, {req}).then((response) => {
            res.send(response);
        }).catch(err => {
            next(err);
        });    
    }
};
