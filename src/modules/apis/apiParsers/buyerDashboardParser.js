/*
 * Description: Parser to handle all ajax calls related to buyer journey
 * @author [Harvinder Singh]
 */

"use strict";

const async = require('async');
const moment = require('moment');
const reverseParser = require('services/reverseParseListingSelector');
const _ = require('services/utilService')._;

const apiService = require('services/apiService'),
    allianceService = require('services/allianceService'),
    logger = require('services/loggerService'),
    apiConfig = require('configs/apiConfig'),
    projectService = require('services/projectService'),
    utilFunctions = require('public/scripts/common/utilFunctions'),
    propertyService = require('services/propertyService'),
    agentService = require('services/agentService'),
    utils = require('services/utilService'),
    globalConfig = require('configs/globalConfig'),
    cityService = require('services/cityService');
   // masterService = require('services/masterDetailsService');

var requiredFields = ["userId", "contactNumber","type", "contactNumbers", "resaleURL", "overviewUrl", "mainImageURL", "user", "profilePictureURL", "currentListingPrice", "price", "latitude", "longitude", "activeStatus", "projectId", "companySeller", "company", "name", "score", "halls", "unitType", "unitTypeId", "unitName", "measure", "size", "bathrooms", "bedrooms", "minResaleOrPrimaryPrice", "maxResaleOrPrimaryPrice", "listing", "id", "possessionDate", "property", "project", "builder", "name", "minSize", "maxSize", "locality", "suburb", "label", "city", "imageURL"],
    stageUrlMap = {
        1: {
            currentStage: 1,
            currentUrl: "favourites",
            jarvisEvent: "bj_sitevisit",
        },
        2: {
            currentStage: 2,
            currentUrl: "sitevisits",
            jarvisEvent: "bj_book",
        },
        3: {
            currentStage: 3,
            currentUrl: "unitbook",
            jarvisEvent: "bj_posses",
        },
        4: {
            currentStage: 4,
            currentUrl: "possession",
            jarvisEvent: "bj_register",
        },
        5: {
            currentStage: 5,
            currentUrl: "registration",
            jarvisEvent: "",
        }
    };

const allianceStageIdMap = {
    savedsearches: 1,
    favourites: 2,
    recentlyviewed: 2,
    sitevisits: 3,
    booking: 4,
    homeloan: 5,
    possession: 6,
    registration: 7
};


function apiHandler(req, res, next) {
    var method = req.method;

    switch (method) {
        case 'GET':
            getRequest(req, res, next);
            break;

        case 'POST':
            postRequest(req, res, next);
            break;

        case 'PUT':
            break;

        case 'DELETE':
            deleteRequest(req, res, next);
            break;

        default:
            break;
    }
}

/*
 *   redirect get request to specific handlers based on query type
 */
function getRequest(req, res, next) {
    var query = req.query['query'],
        articles = req.query.articles;

    if (articles) {
        articlesHandler(req, res, next);
        return;
    }

    switch (query) {
        case 'currentstage':
            getCurrentPhase(req, res, next);
            break;

        case 'seller':
            getSellerInfo(req, res, next);
            break;

        case 'favourites':
            getCallbackCards(req, res, next);
            break;

        case 'enquired':
            getEnquiryCardDetails(req, res, next);
            break;

        case 'recentlyviewed':
            getCallbackCards(req, res, next);
            break;

        case 'savedsearches':
            // articlesHandler(req, res, next, query);
            savedSearchesHandler(req, res, next);
            break;

        case 'sitevisits':
            getSiteVisitCardDetails(req, res, next);
            break;

        case 'getcompany':
            getCompanyInfo(req, res, next);
            break;

        case 'homeloan':
            articlesHandler(req, res, next);
            break;

        case 'registration':
            articlesHandler(req, res, next);
            break;

        case 'possession':
            articlesHandler(req, res, next);
            break;

        case 'unitbook':
            articlesHandler(req, res, next);
            break;

        case 'wishList':
            wishlistHandler(req, res, next);
            break;

        default:
            initLoadHandler(req, res, next);
            break;
    }
}

// listingType: buy/rent/both
// stageId: 1to7 based upon buyerjourney stage
function getAlliancesCards(stageId, req = {}, listingType) {   //jshint ignore:line
    if (req.query && req.query.alliances === 'show') {
        let queryParams = {
            stageId,
            listingType
        };
        return allianceService.getStageCards(queryParams);
    } else {
        return new Promise((resolve) => {
            resolve([]);
        });
    }
}

function wishlistHandler(req, res,next) {
    var apiOptions = {
        req: req,
        res: res,
        body: req.body
    };
    apiService.get(apiConfig.wishList(), apiOptions).then((response) => {
        res.status(200).send(response);
    }, (error) => {
        next(error);
    });
}

function getSellerInfo(req, res, next) {
    logger.info("Getting seller info");
    var listingId = req.query.listingId,
        projectId = req.query.projectId;
    if (listingId) {
        getListingCard(listingId, (err, response) => {
            if (err) {
                logger.error("error in listing api in get seller info", err);
                return next(err);
            }
            res.send(response);
        }, {
            req
        });
    } else if (projectId) {
        let selector = {};
        selector.filters = [{
            key: "projectId",
            value: projectId
        }];
        agentService.getTopAgents(req.query.city, {
            selector
        }, {
            req
        }).then((response) => {
            var finalResponse = {};
            response = response[0];
            finalResponse.sellerId = response.id;
            finalResponse.sellerUserId = response.userId;
            finalResponse.sellerName = response.name;
            finalResponse.sellerRating = response.rating;
            finalResponse.sellerType = response.type;
            finalResponse.userId = response.userId;
            finalResponse.sellerImage = response.image;
            finalResponse.sellerAssist = response.assist;
            finalResponse.sellerContactNumber = response.contact;
            finalResponse.projectId = projectId;
            res.send(finalResponse);
        }, (err) => {
            next(err);
        });
    }
}

function getSiteVisitCardDetails(req, res, next) {
    logger.info("Getting site visit card details");
    // get all enquiries for the user
    getClientEvents(req).then((clientEvents) => {
        //{listingEvents: [], projectEvents: [], sellerEvents: []}
        let cardFunctionList = [];
        // get array of all listing card fetch promise
        cardFunctionList = cardFunctionList.concat(clientEvents.listingEvents.map(listingEvent => {
            return (callback) => {
                getListingCard(searchId(listingEvent.clientEventListings, 'listingId'), callback, {
                    req
                });
            };
        }));

        cardFunctionList = cardFunctionList.concat(clientEvents.projectEvents.map(projectEvent => {
            return (callback) => {
                getProjectCard(searchId(projectEvent.clientEventProjects, 'projectId'), callback, {
                    req,
                    companyId: projectEvent.agentId
                });
            };
        }));
        cardFunctionList = cardFunctionList.concat(clientEvents.sellerEvents.map(sellerEvent => {
            return (callback) => {
                getSellerCard(sellerEvent.agentId, callback, {
                    req
                });
            };
        }));

        async.parallel(cardFunctionList, (err, results) => {
            if (err) {
                return next(err);
            }

            for (var i = 0, len = results.length, x = clientEvents.listingEvents.length, y = clientEvents.projectEvents.length; i < len; i++) {
                if (i < x) {
                    results[i].updatedTime = clientEvents.listingEvents[i].performTime;
                    results[i].performTime = parseTime(clientEvents.listingEvents[i].performTime);
                } else if (i < x + y) {
                    results[i].updatedTime = clientEvents.projectEvents[i - x].performTime;
                    results[i].performTime = parseTime(clientEvents.projectEvents[i - x].performTime);
                } else {
                    results[i].updatedTime = clientEvents.sellerEvents[i - x - y].performTime;
                    results[i].performTime = parseTime(clientEvents.sellerEvents[i - x - y].performTime);
                }
            }
            results = _.sortBy(results, ['updatedTime']).reverse();

            getAlliancesCards(allianceStageIdMap[req.query.query], req).then(response => {
                res.send({
                    cards: results,
                    serviceCards: response
                });
            }, error => {
                next(error);
            });

        });
    }, e => {
        next(e);
    }).catch(e => {
        next(e);
    });
}

function parseTime(date) {
    return {
        date: moment(date).format("DD MMM, YYYY"),
        time: moment(date).format("h.mm a")
    };
}

function getClientEvents(req) {
    return apiService.get(apiConfig.clientEvents(), {
        req: req
    }).then(function(response) {
        var results = response.data.results,
            finalResponse = {};

        finalResponse.listingEvents = results.filter(result => {
            return result.clientEventListings &&
                result.clientEventListings[0] &&
                result.clientEventListings[0].listingId;
        });

        results = _.difference(results, finalResponse.listingEvents);

        finalResponse.projectEvents = results.filter(result => {
            return result.clientEventProjects &&
                result.clientEventProjects[0] &&
                result.clientEventProjects[0].projectId;
        });

        finalResponse.sellerEvents = _.difference(results, finalResponse.projectEvents);
        return finalResponse;
    }, err => {
        logger.error("error in get client events", err);
        throw err;
    });
}

//find type(listing or project) in lead or event
function searchId(searchArray, type) {
    for (let i = 0, len = searchArray.length; i < len; i++) {
        if (searchArray[i][type]) {
            return searchArray[i][type];
        }
    }
}

function getEnquiryCardDetails(req, res, next) {
    logger.info("Getting enquiry card details");
    // get all enquiries for the user
    apiService.get(apiConfig.clientLeads(), {
        req: req
    }).then((response) => {
        return parseClientLeads(response);
    }).then((clientLeads) => {
        //{listingLeads: [], projectLeads: [], sellerLeads: []}
        let cardFunctionList = [];
        // get array of all listing card fetch promise
        cardFunctionList = cardFunctionList.concat(clientLeads.listingLeads.map(listingLead => {
            return (callback) => {
                getListingCard(listingLead.listingId, callback, {
                    req,
                    updatedTime: listingLead.updatedAt,
                    leadId: listingLead.leadId
                });
            };
        }));

        cardFunctionList = cardFunctionList.concat(clientLeads.projectLeads.map(projectLead => {
            return (callback) => {
                getProjectCard(projectLead.projectId, callback, {
                    req,
                    companyId: projectLead.companyId,
                    updatedTime: projectLead.updatedAt,
                    leadId: projectLead.leadId
                });
            };
        }));
        cardFunctionList = cardFunctionList.concat(clientLeads.sellerLeads.map(sellerLead => {
            return (callback) => {
                getSellerCard(sellerLead.companyId, callback, {
                    req,
                    updatedTime: sellerLead.updatedAt,
                    cityId: sellerLead.cityId,
                    leadId: sellerLead.leadId
                });
            };
        }));

        // cardFunctionList.push(callback => {
        //     getFavouritesCount(req, callback);
        // });

        async.parallel(cardFunctionList, (err, results) => {
            var resultLength = results.length,
                finalResponse = {};
            if (err) {
                return next(err);
            }

            finalResponse.cards = results.splice(0, resultLength);
            // finalResponse.favourites = results[0];
            finalResponse.enquired = resultLength;

            finalResponse.cards = _.sortBy(finalResponse.cards, ['updatedAt']).reverse();
            res.send(finalResponse);
        });
    }, e => {
        next(e);
    }).catch(e => {
        next(e);
    });
}

function getCallbackCards(req, res, next) {
    logger.info("Getting callback card details");
    let queries = req.query;
    let listingIds = JSON.parse(queries.listings);
    let projectIds = JSON.parse(queries.projects);
    let leads = queries.leads;
    let cardFunctionList = [];
    // get array of all listing card fetch promise
    cardFunctionList = cardFunctionList.concat(listingIds.map(listingId => {
        return (callback) => {
            getListingCard(listingId.id, callback, {
                req,
                updatedTime: listingId.time
            });
        };
    }));

    cardFunctionList = cardFunctionList.concat(projectIds.map(projectId => {
        return (callback) => {
            getProjectCard(projectId.id, callback, {
                req,
                updatedTime: projectId.time
            });
        };
    }));

    if (leads) {
        cardFunctionList.push(callback => {
            getEnquiredCount(req, callback);
        });
    }

    async.parallel(cardFunctionList, (err, results) => {
        if (err) {
            logger.error("error in get callback cards parallel api call", err);
            return next(err);
        }
        results = results.filter(v => Object.keys(v).length > 0 );
        var resultLength = results.length,
            finalResponse = {};
        if (leads) {
            finalResponse.enquired = results[resultLength - 1];
            resultLength -= 1;
        }
        finalResponse.cards = results.splice(0, resultLength);

        finalResponse.cards = _.sortBy(finalResponse.cards, ['updatedAt']).reverse();
        getAlliancesCards(allianceStageIdMap[queries.query], req).then(response => {
            finalResponse.serviceCards = response;
            res.send(finalResponse);
        }, error => {
            next(error);
        });
    });
}

function getEnquiredCount(req, callback) {
    return apiService.get(apiConfig.clientLeads(), {
        req: req
    }).then(response => {
        response = parseClientLeads(response);
        response = response.sellerLeads.length + response.listingLeads.length + response.projectLeads.length;
        callback(null, response);
    }, err => {
        callback(err);
    });
}

function getListingCard(listingId, callback, properties = {}) {
    propertyService.getPropertyInfo(listingId, {
        fields: requiredFields
    }, {
        req: properties.req
    }).then((listingDetail) => {
        if(!listingDetail) {
            callback(null,{});
            return;
        }
        listingDetail = listingParser(listingDetail, properties.req);
        if (properties.updatedTime) {
            listingDetail.updatedAt = properties.updatedTime;
        }
        if (properties.leadId) {
            listingDetail.leadId = properties.leadId;
        }
        listingDetail.anchorCheck = true;
        callback(null, listingDetail);
    }, () => {
        callback(null,{});
    });
}

function getProjectCard(projectId, callback, properties = {}) {
    // companyId, callback, updatedTime, leadId
    let options = {
        selector: {
            "fields": requiredFields
        }
    };

    let callbackArray = [];
    callbackArray.push((cb) => {
        projectService.getProjectInfo(projectId, options, properties).then((response) => {
            response = projectParser(response.data, properties.req);
            cb(null, response);
        }, e => {
            cb(e);
        });
    });
    if (properties.companyId) {
        callbackArray.push((cb) => {
            getCompanyDetail(properties.companyId, properties).then(companyDetail => {
                cb(null, companyDetail);
            }, e => {
                cb(e);
            });
        });
    }

    async.parallel(callbackArray, function(err, results) {
        if (err) {
            callback(err);
        }
        // results[0].seller = results[1]
        let response = properties.companyId ? _.merge(results[0], results[1]) : results[0];
        if (properties.updatedTime) {
            response.updatedAt = properties.updatedTime;
        }
        if (properties.leadId) {
            response.leadId = properties.leadId;
        }
        response.anchorCheck = true;
        callback(err, response);
        // parse and send in callback
        // callback(parsedResult)
        //{projectName:'', projectAddress: '', image: '', seller: {image: '', rating: '', name: '', address: '', mob:''}}
    });

}

function getCompanyDetail(companyId, properties = {}) {
    return agentService.getSellerDetailsByCompanyId(companyId, {
        req: properties.req
    }).then(response => {
        let finalResponse = {};
        finalResponse.sellerId = response[0].id;
        finalResponse.sellerName = response[0].name;
        finalResponse.avatar = utils.getAvatar(finalResponse.sellerName);
        finalResponse.sellerRating = response[0].rating;
        finalResponse.sellerImage = response[0].image;
        finalResponse.sellerContactNumber = response[0].contact;
        return finalResponse;
    }, error => {
        logger.error("Error in agentService get agent by id api call", error);
        throw error;
    });
}

function getSellerCard(companyId, callback, properties = {}) {
    getCompanyDetail(companyId, properties).then(function(response) {
        if (properties.updatedTime) {
            response.updatedAt = properties.updatedTime;
        }
        if (properties.cityId) {
            response.cityName = cityService.getCityNameById(properties.cityId);
        }
        if (properties.leadId) {
            response.leadId = properties.leadId;
        }
        callback(null, response);
    }, error => {
        callback(error);
    });
}

function parseClientLeads(response) {
    var results = response.data.results,
        finalResponse = {
            sellerLeads: [],
            projectLeads: [],
            listingLeads: []
        };

    let withPropertyLeads = results.filter(result => {
        return result.propertyRequirements &&
            result.propertyRequirements.filter(requirement => {
                return requirement.listingId || requirement.projectId;
            }).length;
    });

    let withoutPropertyLeads = _.difference(results, withPropertyLeads);

    for (let i = 0, len = withPropertyLeads.length; i < len; i++) {
        withPropertyLeads[i].propertyRequirements.map(requirement => {  //jshint ignore:line
            if (requirement.listingId) {
                finalResponse.listingLeads.push({
                    listingId: requirement.listingId,
                    updatedAt: withPropertyLeads[i].updatedAt,
                    leadId: withPropertyLeads[i].id
                });
            } else if (requirement.projectId) {
                finalResponse.projectLeads.push({
                    projectId: requirement.projectId,
                    updatedAt: withPropertyLeads[i].updatedAt,
                    companyId: withPropertyLeads[i].companyId,
                    leadId: withPropertyLeads[i].id
                });
            }
            return;
        });
    }

    finalResponse.sellerLeads = withoutPropertyLeads.map(lead => {
        return {
            companyId: lead.companyId,
            updatedAt: lead.updatedAt,
            cityId: lead.cityId,
            leadId: lead.id
        };
    });

    return finalResponse;
}

function articlesHandler(req, res, next) {
    logger.info("Getting article cards");
    var queryMap = {
            'savedsearches': 'search',
            'favourites': 'shortlist',
            'enquired': 'shortlist',
            'recentlyviewed': 'shortlist',
            'sitevisits': 'sitevisit',
            'unitbook': 'unit booking',
            'possession': 'possession',
            'registration': 'registration',
            'homeloan': 'homeloan'
        },
        headingMap = {
            savedsearches: {
                bold: 'Find your saved searches here',
                small: 'To save a search, hit the "set alert" button on the search results page'
            },
            favourites: {
                bold: 'Find shortlists here',
                small: 'To shortlist a property, click <span class="icon-logo"><span class="path1"></span><span class="path2"></span><span class="path3"></span></span> on the search results page'
            },
            sitevisits: {
                bold: 'Track upcoming site visits here',
                small: 'To set up your site visits, get in touch with any seller'
            }
        },
        subHeadingMap = {
            savedsearches: 'Advice for the best home buying experience',
            favourites: 'Shortlisting the right properties',
            sitevisits: 'Making the most of your site visits',
            homeloan: 'Home loans guidance',
            unitbook: 'Home booking guidance',
            possession: 'Home possession guidance',
            registration: 'Home registration guidance'
        },
        query = req.query['query'];

    apiService.get(apiConfig.articles(queryMap[query]), {
        req
    }).then(function(response) {
        var finalResponse = {},
            temp = response.data,
            thumbnails;

        finalResponse.heading = headingMap[query];
        finalResponse.subHeading = subHeadingMap[query];
        finalResponse.articles = [];
        for (var i = 0, len = temp.length; i < len; i++) {
            finalResponse.articles[i] = {};
            finalResponse.articles[i].id = temp[i].id;
            finalResponse.articles[i].url = temp[i].guid;
            finalResponse.articles[i].postTitle = temp[i].postTitle;
            thumbnails = temp[i].imageSizeMap;
            finalResponse.articles[i].image = thumbnails && thumbnails.medium && thumbnails.medium.file;
        }
        getAlliancesCards(allianceStageIdMap[query], req).then(response => {
            finalResponse.serviceCards = response;
            res.status(200).send(finalResponse);
        }, () => {
            finalResponse.serviceCards = [];
            res.status(200).send(finalResponse);
        });
    }).catch(function(error) {
        logger.error('Error in buyer dashboard articles handler api call', error);
        next(error);
    });
}

function getCurrentPhase(req, res, next) {
    logger.info("Getting current phase of buyer");
    let checkLog = req && req.query && req.query.checklog;
    apiService.get(apiConfig.currentStage().url, {
        req: req
    }).then((response) => {
        var currentStage = response.data.results,
            finalResponse = {};
        if (currentStage.length && currentStage[0].clientActivity && currentStage[0].clientActivity.phaseId) {
            finalResponse = stageUrlMap[currentStage[0].clientActivity.phaseId];
            finalResponse.leadId = currentStage[0].id;
            finalResponse.agentId = currentStage[0].companyId;
        } else {
            finalResponse.currentStage = 0;
            finalResponse.currentUrl = "savedsearches";
        }
        res.status(200).send(finalResponse);
    }, (error) => {
        if (checkLog === 'addevent') {
            logger.error("Error in addevent currentstage api call");
        }
        logger.error("Error in buyerdashboard get current phase api call", error);
        next(error);
    });
}

function projectParser(project, req) {
    project = project || {};
    let finalResponse = {},
        locality = project.locality,
        suburb = locality && locality.suburb,
        city = suburb && suburb.city,
        builder = project.builder,
        numberFormat = req.locals.numberFormat;
    const mapUrl = "http://maps.google.com/maps?q=";

    finalResponse.projectId = project.projectId;
    finalResponse.name = project.name;
    finalResponse.projectStatus = project.activeStatus && project.activeStatus.toLowerCase();

    if (finalResponse.projectStatus && finalResponse.projectStatus !== 'dummy' && builder && builder.name && project.name) {
        finalResponse.projectName = builder.name + ' ' + project.name;
    }

    finalResponse.directions = {
        lat: project.latitude,
        lng: project.longitude
    };

    finalResponse.mapUrl = project.latitude && project.longitude ? mapUrl + project.latitude + "," + project.longitude : '';
    finalResponse.locality = locality && locality.label;
    finalResponse.localityId = locality && locality.localityId;
    finalResponse.suburb = suburb && suburb.label;
    finalResponse.suburbId = suburb && suburb.id;
    finalResponse.city = city && city.label;
    finalResponse.cityId = city && city.id;
    finalResponse.builder = builder && builder.name;
    finalResponse.builderId = builder && builder.id;
    // finalResponse.unitType = project.propertyUnitTypes && project.propertyUnitTypes.join();
    finalResponse.possessionDate = project.possessionDate;
    finalResponse.minSize = project.minSize;
    finalResponse.maxSize = project.maxSize;
    finalResponse.minPrice = utilFunctions.formatNumber(project.minResaleOrPrimaryPrice, { precision : 2, seperator : numberFormat });
    finalResponse.maxPrice = utilFunctions.formatNumber(project.maxResaleOrPrimaryPrice, { precision : 2, seperator : numberFormat });
    finalResponse.minPriceRaw = project.minResaleOrPrimaryPrice;
    finalResponse.maxPriceRaw = project.maxResaleOrPrimaryPrice;
    finalResponse.budget = project.minResaleOrPrimaryPrice + ',' + project.maxResaleOrPrimaryPrice;
    finalResponse.imageUrl = getCompressedImageUrl(project.imageURL);
    finalResponse.url = utils.prefixToUrl(project.overviewUrl);
    finalResponse.type = "project";

    return finalResponse;
}

//used to parse list api response
//argument data = compositeApiResponse.data
function listingParser(listing, req) {
    listing = listing || {};
    let finalResponse = {},
        property = listing.property,
        project = property && property.project,
        seller = listing.companySeller,
        company = seller && seller.company,
        sellerUser = seller && seller.user,
        currentListingPrice = listing.currentListingPrice;

    finalResponse = projectParser(project, req);
    if (listing.mainImageURL) {
        finalResponse.imageUrl = getCompressedImageUrl(listing.mainImageURL);
    }
    finalResponse.listingId = listing.id;
    if (property && property.bedrooms && property.unitType) {
        finalResponse.bedrooms = property.bedrooms;
        finalResponse.unitType = property.unitType;
        finalResponse.listingName = property.bedrooms + 'BHK ' + property.unitType;
    }
    finalResponse.bathrooms = property && property.bathrooms;
    finalResponse.size = property && property.size;
    finalResponse.measure = property && property.measure;
    finalResponse.unitName = property && property.unitName;
    finalResponse.unitTypeId = property && property.unitTypeId;
    finalResponse.halls = property && property.halls;
    finalResponse.url = listing.resaleURL;
    finalResponse.type = "listing";

    finalResponse.userId = seller && seller.userId;
    finalResponse.sellerId = company && company.id;
    finalResponse.sellerName = company && company.name;
    finalResponse.avatar = utils.getAvatar(finalResponse.sellerName);
    finalResponse.sellerRating = company && company.score;
    finalResponse.sellerType = company && company.type;

    finalResponse.sellerImage = sellerUser && sellerUser.profilePictureURL;
    finalResponse.sellerContactNumber = sellerUser && sellerUser.contactNumbers && sellerUser.contactNumbers.length && sellerUser.contactNumbers[0].contactNumber;

    if (currentListingPrice) {
        finalResponse.listingPrice = utilFunctions.formatNumber(currentListingPrice.price, { precision : 2, seperator : req.locals.numberFormat });
        finalResponse.budget = currentListingPrice.price;
    }
    return finalResponse;
}

function getCompressedImageUrl(imageUrl, type = "smallHeroshot") {
    let typeDimensions = globalConfig.imageSizes[type];
    return imageUrl + `?width=${typeDimensions.width}&height=${typeDimensions.height}`;
}

function makeNewMatchesApiUrl(searchId) {
    if (searchId) {
        return apiConfig.newMatches().url + "?savedSearchId=" + searchId;
    }
    return null;
}

//handles api call and send response for buyer dasboard page menu
function initLoadHandler(req, res, next) {
    logger.info("Entered init load handler");
    var compositeApiUrl = [apiConfig.savedSearches().url, apiConfig.newMatches().url, apiConfig.siteVisits().url, apiConfig.currentStage().url];

    apiService.composite(compositeApiUrl, {
        req: req
    }).then(function(response) {
        var data = response.data,
            finalResponse = {},
            savedSearches = data[apiConfig.savedSearches().url],
            newMatches = data[apiConfig.newMatches().url],
            // wishList = data[apiConfig.wishList().url],
            siteVisits = data[apiConfig.siteVisits().url],
            currentStage = data[apiConfig.currentStage().url];
        if (savedSearches.statusCode === '2XX') {
            finalResponse.savedSearches = savedSearches.totalCount;
        }
        if (newMatches.statusCode === '2XX') {
            finalResponse.newMatches = newMatches.data ? newMatches.data : 0;
        }
        // if(wishList.statusCode === '2XX'){
        //     finalResponse.wishList = wishList.totalCount;
        // }
        if (siteVisits.statusCode === '2XX') {
            finalResponse.siteVisits = siteVisits.totalCount;
        }
        if (currentStage.statusCode === '2XX') {
            currentStage = currentStage.data.results;
            if (currentStage.length && currentStage[0].clientActivity && currentStage[0].clientActivity.phaseId) {
                _.merge(finalResponse, stageUrlMap[currentStage[0].clientActivity.phaseId]);
            } else {
                finalResponse.currentStage = 0;
                finalResponse.currentUrl = "savedsearches";
            }
        }
        res.status(200).send(finalResponse);
    }, function(error) {
        logger.error('Error in buyer dashboard page initLoadHandler api call', error);
        next(error);
    });
}

//used to parse saved search api response
//argument data = compositeApiResponse.data
function savedSearchesParser(data, req) {
    var responseData = [],
        localityIds = [],
         temp;
    if (data[apiConfig.savedSearches().url].statusCode === '2XX') {
        data = data[apiConfig.savedSearches().url].data;
        for (var i = 0, len = data.length; i < len; i++) {
            responseData[i] = {};
            responseData[i].id = data[i].id;
            responseData[i].urlObject = reverseParser.reverseParseSelector(data[i].searchQuery);
            temp = responseData[i].urlObject.beds;
            if (temp) {
                temp = temp.split(',');
                if (temp.indexOf('4') > -1) {
                    temp.splice(temp.indexOf('4'), temp.length);
                    temp.push("3+");
                    temp = temp.join();
                }
                responseData[i].urlObject.beds = temp;
            }
            if (responseData[i].urlObject.budget) {
                temp = responseData[i].urlObject.budget.split(",");
                responseData[i].urlObject.budgetMin = utilFunctions.formatNumber(temp[0], { returnSeperate : true, seperator : req.locals.numberFormat });
                    responseData[i].urlObject.budgetMax = utilFunctions.formatNumber(temp[1], { returnSeperate : true, seperator : req.locals.numberFormat });
                // minPrice = minPrice.val+'<span style="text-transform: capitalize; display: inline;">'+minPrice.unit+'</span>';
                // maxPrice = maxPrice.val+'<span style="text-transform: capitalize; display: inline;">'+maxPrice.unit+'</span>';
                //  = temp[0] ? (minPrice +(temp[1] ? ' - ': ' ' )) : 'upto ';
                // responseData[i].urlObject.budgetMax = temp[1] ? maxPrice : '& above';
            }
            if (responseData[i].urlObject.localityId) {
                localityIds.push(responseData[i].urlObject.localityId);
            }
            responseData[i].name = data[i].name;
        }
    }
    return {
        cards: responseData,
        localityIds: localityIds
    };
}

//to get new matches for all saved searches
//finalResponse is the object containing the parsed response from initLoad
function savedSearchesHandler(req, res, next) {
    logger.info("Getting saved searches details");
    var compositeApiUrl = [],
        finalResponse = {},
        cards, localityUrl;

    apiService.composite([apiConfig.savedSearches().url], {
        req: req
    }).then(function(response) {
        finalResponse = savedSearchesParser(response.data, req);
        cards = finalResponse.cards;

        if (_.isEmpty(cards)) {
            res.status(200).send(finalResponse);
            return;
        }

        for (var i = 0, len = cards.length; i < len; i++) {
            compositeApiUrl[i] = makeNewMatchesApiUrl(cards[i].id);
        }
        if (finalResponse.localityIds && finalResponse.localityIds.length) {
            localityUrl = makeLocalitySelector(finalResponse.localityIds);
            compositeApiUrl.push(localityUrl);
        }
        return apiService.composite(compositeApiUrl, {
            req: req
        }).then(function(responseData) {
            for (var i = 0, len = cards.length; i < len; i++) {
                if (responseData.data[compositeApiUrl[i]].statusCode === '2XX') {
                    cards[i].newMatches = responseData.data[compositeApiUrl[i]].data;
                }
            }
            if (localityUrl && responseData.data[localityUrl.url].statusCode === '2XX') {
                finalResponse.localityIds = responseData.data[localityUrl.url].data.reduce(function(final, current) {
                    final[current.localityId] = {
                        locality: current.label,
                        city: current.suburb && current.suburb.city && current.suburb.city.label ? current.suburb.city.label : '',
                        image: getCompressedImageUrl(current.localityHeroshotImageUrl)
                    };
                    return final;
                }, {});
            }
            getAlliancesCards(allianceStageIdMap[req.query.query], req).then(response => {
                finalResponse.serviceCards = response;
                res.status(200).send(finalResponse);
            });
        }, error => {
            logger.error('Error in buyer dashboard page saved searches new matches api call', error);
            next(error);
        });
    }).catch(function(error) {
        logger.error('Error in buyer dashboard page saved searches api call', error);
        next(error);
    });
}

//localityIds: array of locality Ids
function makeLocalitySelector(localityIds) {
    var selector = {
        filters: {
            and: [{
                equal: {
                    localityId: localityIds
                }
            }]
        },
        fields: ['label', 'cityId', 'localityId', 'localityHeroshotImageUrl', 'suburb', 'city', '']
    };
    return apiConfig.localities(JSON.stringify(selector));
}

function getCompanyInfo(req, res, next) {
    logger.info("Getting company info");
    var companyInfo = [],
        sellerIds = [],
        leadIdsMap = {},
        temp;
    apiService.get(apiConfig.clientLeads().url, {
        req: req
    }).then(function(response) {
        temp = response.data.results;
        for (let i = 0, len = temp.length; i < len; i++) {
            sellerIds.push("id==" + temp[i].companyId);
            leadIdsMap[temp[i].companyId] = {
                leadId: temp[i].id,
                cityId: temp[i].cityId,
                leadPhase: temp[i].clientActivity && temp[i].clientActivity.phaseId
            };
        }
        if (!sellerIds.length) {
            res.status(200).send([]);
            return;
        }
        return apiService.get(apiConfig.companyDetails(sellerIds.join()).url, {
            req: req
        });
    }).then(function(responseData) {
        var leadInfo;
        if (!responseData) {
            return;
        }
        for (let i = 0, len = responseData.data.length; i < len; i++) {
            companyInfo[i] = {};
            companyInfo[i].id = responseData.data[i].id;
            leadInfo = leadIdsMap[companyInfo[i].id];
            companyInfo[i].leadId = leadInfo.leadId;
            companyInfo[i].cityId = leadInfo.cityId;
            companyInfo[i].leadPhase = leadInfo.leadPhase;
            companyInfo[i].name = responseData.data[i].name;
            companyInfo[i].avatar = utils.getAvatar(companyInfo[i].name);
            companyInfo[i].sortTimeParam = temp[sellerIds.indexOf("id==" + companyInfo[i].id)].createdAt;
            let performTime = companyInfo[i].performTime = moment(temp[sellerIds.indexOf("id==" + companyInfo[i].id)].createdAt).format("MMM DD, YYYY");
            let cityName = companyInfo[i].cityName = cityService.getCityNameById(leadInfo.cityId);
            if (cityName && performTime) {
                companyInfo[i].enquiredOn = performTime + ' | ' + cityName;
            } else if (cityName) {
                companyInfo[i].enquiredOn = cityName;
            } else if (performTime) {
                companyInfo[i].enquiredOn = performTime;
            }
        }
        companyInfo = _.sortBy(companyInfo, ['sortTimeParam']).reverse();
        res.status(200).send(companyInfo);
    }).catch(function(error) {
        logger.error('Error in buyer dashboard page get company info', error);
        next(error);
    });
}

function postRequest(req, res, next) {
    var query = req.query ? req.query : {},
        apiOptions;
    apiOptions = {
        req: req,
        res: res,
        body: req.body
    };
    if (query.savedSearches) {
        if (query.nonLoggedIn && query.email) {
            let urlConfig = apiConfig.savedSearchesNonLoggedIn({
                query: {
                    email: query.email,
                    sourceDomain: 'Makaan'
                }
            });
            apiService.post(urlConfig, apiOptions).then((response) => {
                res.send(response);
            }, (error) => {
                next(error);
            });
        } else {

            /* code commented for save search sync api not ready
            if(query.sync){
                let sync = JSON.parse(decodeURI(query.sync)),
                    apis = [];
                _.forEach(sync,(val)=>{
                    _.forEach(val.id,(id)=>{
                        let email = val.email;
                        apis.push(apiService.put(apiConfig.syncSavedSearches({id,email}), apiOptions));
                    });
                });
                Promise.all(apis).then((response) => {
                    res.send(response);
                }, (error) => {
                        res.status(500).send(error);
                });

            }else{
            // code end */
            apiService.post(apiConfig.savedSearches(), apiOptions).then((response) => {
                res.send(response);
            }, (error) => {
                next(error);
            });
        }
    } else if (query.wishList) {
        apiService.post(apiConfig.wishList(), apiOptions).then((response) => {
            res.status(200).send(response);
        }, (error) => {
            next(error);
        });
    } else if (query.sitevisit) {
        let stageChangeData = apiOptions.body && apiOptions.body.stageChangeData,
            postCalls = [],
            leadId = query.leadId;
        if (stageChangeData) {
            postCalls.push((callback) => {
                apiService.post(apiConfig.clientEvents(leadId), {
                    req,
                    res,
                    body: stageChangeData
                }).then(response => {
                    if (response) {
                        callback(null, response);
                    }
                    callback(response);
                }, error => {
                    callback(error);
                });
            });
        }

        if (postCalls.length) {
            async.parallel(postCalls, (err, results) => {
                if (err) {
                    return next(err);
                }

                res.status(200).send(results);
            });
        } else {
            res.status(200).send({});
        }
        // apiService.post(apiConfig.clientEvents(query.leadId), apiOptions).then((response) => {
        //     res.status(200).send(response);
        // }, (error) => {
        //     res.status(500).send(error);
        // });
    }
}

function deleteRequest(req, res, next) {
    var query = req.query ? req.query : {},
        apiOptions;
    apiOptions = {
        req: req,
        res: res,
        body: req.body
    };
    if (query.wishList) {
        apiService.delete(apiConfig.wishList({
            wishListId: apiOptions.body.wishListId
        }), apiOptions).then((response) => {
            res.status(200).send(response);
        }, (error) => {
            res.status(error.status).send(error.body);
        });
    }
    if (query.query && query.query === 'savedsearches') {
        apiService.delete(apiConfig.savedSearches(query.searchId).url, apiOptions).then(response => {
            res.status(200).send(response);
        }, error => {
            next(error);
        });
    }
}

module.exports = {
    apiHandler,
    postRequest
};
