"use strict";
const logger = require('services/loggerService'),
    utilService = require('services/utilService'),
    propertyService = require('services/propertyService');

module.exports.apiHandler = function(req, res, next) {
    logger.info('entered similar project route');
    let count = (req.query && req.query.count) || 5;
    let listingId = req.params.listingId,
        summary = req.query && req.query.summary,
        companyId = (req.query && req.query.companyId),
        distinctSeller = req.query.distinctSeller == 'true'? true : false,
        isCommercial = req.query && req.query.isCommercial== 'true'? true : false;

    if(!listingId){
        next(utilService.getError(400));
    }else{
        propertyService.getSimilarListings(listingId,{count,companyId, summary, distinctSeller, isCommercial}).then(response => {
            res.send(response);
        }, err => {
            next(err);
        }).catch(err => {
            next(err);
        });
    }

};