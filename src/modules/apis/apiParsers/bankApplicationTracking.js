"use strict";
const logger = require('services/loggerService'),
    bankService = require('services/bankService');

module.exports.apiHandler = function(req, res, next) {
    let userId = req.params.userId;
    bankService.getBankApplicationTracking(userId).then(response => {
        res.send(response);
    }, err => {
        next(err);
    }).catch(err => {
        next(err);
    });
};
