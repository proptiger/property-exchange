"use strict";
const apiService = require('services/apiService');
const apiConfig = require('configs/apiConfig');
const mappingService = require('services/mappingService');
const QUESTION_TYPE = mappingService.LEAD_PROFILE_QUESTION_TYPE||{};
const NEXT_BTN_QUESTION_TYPE = mappingService.LP_NEXT_BTN_QUESTION_TYPE || [];

module.exports.apiHandler = function(req, res, next) {
    let requestMethod = req.method || '',urlConfig;
    switch (requestMethod) {
        case 'GET':
            let requestQuery = req.query;
            if (requestQuery.entityId) {
                let queryObj = {
                    entityId: requestQuery.entityId,
                    entityType: requestQuery.entityType,
                    saleType: requestQuery.saleType,
                    questionOrder: requestQuery.questionOrder
                }
                if (requestQuery.profilingQuestionId) {
                    queryObj.profilingQuestionId = requestQuery.profilingQuestionId;
                }
                if (requestQuery.answerOptionId) {
                    queryObj.answerOptionId = requestQuery.answerOptionId;
                }
                let urlConfig = apiConfig.userProfileQuestions({method:'question',query:queryObj});
                return apiService.get(urlConfig, {req}).then(response => {
                    if (response && response.data) {
                        res.send(parseQuestionDetail(response.data));
                    } else {
                        next(new Error(`No questions available`));
                    }
                }, (err) => {
                    next(err);
                });
            } else {
                next(new Error("Internal server error, no user found"));
            }
            break;
        case 'POST': 
            urlConfig = apiConfig.userProfileQuestions({method:'create'});
            apiService.post(urlConfig, {
                body: req.body
            }).then((response) => {
                res.send(response.data);
            }, (err) => {
                next(err);
            });
            break;
        case 'PUT':
            urlConfig = apiConfig.userProfileQuestions({method:'update'});
            apiService.put(urlConfig, {
                body: req.body
            }).then((response) => {
                res.send(response.data);
            }, (err) => {
                next(err);
            });
            break;
    }
};

function parseQuestionDetail(result) {
    let questionDetail = {},
        responseDetail = {},
        profilingQuestion = result.profilingQuestions && result.profilingQuestions[0],
        profilingResponse = result.profilingResponse,
        profileDetail = {
            id: result.questionnaireEntityDetail && result.questionnaireEntityDetail.id,
            entityId: result.entityId,
            completeness: result.profileCompleteness && result.profileCompleteness.completenessScore || 30,
            profilingQuestionnaireId: profilingQuestion && profilingQuestion.profilingQuestionnaireId
        };
        if (profilingResponse && profilingResponse[0]) {
            for (let k = 0; k < profilingResponse.length; k++){
                var id = profilingResponse[k].id,
                    answerOptionId = profilingResponse[k].answerOptionId,
                    value =  profilingResponse[k].value;
                if (profilingQuestion.masterQuestion.questionTypeId == QUESTION_TYPE.CHECKBOX){
                    responseDetail[profilingResponse[k].profilingQuestionId] = responseDetail[profilingResponse[k].profilingQuestionId] || {};
                    responseDetail[profilingResponse[k].profilingQuestionId][answerOptionId] = {id, answerOptionId, value};
                } else {
                    responseDetail[profilingResponse[k].profilingQuestionId] = {id, answerOptionId, value};
                }
            }
        }
        if (profilingQuestion) {
            questionDetail = parseQuestion(profilingQuestion,responseDetail);
            let questionObj = profilingQuestion.masterQuestion;
            if (questionObj.masterQuestionType.id == QUESTION_TYPE.MULTIPLE){
                let childQuestions = [], childQuestionsArr = profilingQuestion.childQuestions || [];
                for (let j = 0; j < childQuestionsArr.length; j++) {
                    childQuestions.push(parseQuestion(childQuestionsArr[j]));
                }
                questionDetail.childQuestions = childQuestions;
            }
        }
    return {
        questionDetail,
        profileDetail,
        responseDetail
    };
}

function parseQuestion(questionObj, responseObj){
    let masterQuestion = questionObj.masterQuestion || {},
        answerObj = parseAnswerOptions(questionObj, responseObj),
        questionId = questionObj.id;
    return {
        questionId,
        question: masterQuestion.question,
        attributeQuestion: masterQuestion.question && masterQuestion.question.replace(/ /g, '_').toUpperCase(),
        placeholder: masterQuestion.placeholder || masterQuestion.question,
        questionTypeId: masterQuestion.masterQuestionType.id,
        questionType: masterQuestion.masterQuestionType.type,
        answerOptions: answerObj.answerOptions,
        answerHasImages: answerObj.hasImages,
        allowSkip: questionObj.allowSkip || false,
        allowNextBtn: NEXT_BTN_QUESTION_TYPE.indexOf(parseInt(masterQuestion.masterQuestionType.id)) !== -1 ? true : false,
        parentQuestionId: questionObj.parentQuestionId || '',
        previousQuestionId: questionObj.previousQuestionId || '',
        allowPrevious: (questionObj.parentQuestionId || questionObj.previousQuestionId) ? true : false,
        respValue: responseObj && responseObj[questionId] ? getResponseValue(responseObj[questionId].value, masterQuestion.masterQuestionType.id) : '',
        respId : responseObj && responseObj[questionId] ? responseObj[questionId].id :'',
        respAnswerOptionId : responseObj && responseObj[questionId] ? responseObj[questionId].answerOptionId :''
    };
}

function getResponseValue(response, questionType){
    if (questionType != QUESTION_TYPE.MULTIPLE_TYPEAHEAD){
        return response;
    }
    let respObj = JSON.parse(response),
        result = [];
        for (let i = 0; i < respObj.length; i++){
              result.push({
                  tagid: respObj[i].tagid,
                  val: respObj[i].val,
                  dataset: respObj[i]
              })
        }
    return result;
}

function parseAnswerOptions(questionObj, responseObj) {
    let hasImages = false, 
        answerOptions = [],
        questionId = questionObj.id,
        answerOptionsArr = questionObj && questionObj.masterQuestion && questionObj.masterQuestion.answerOptions,
        questionTypeId = questionObj && questionObj.masterQuestion && questionObj.masterQuestion.masterQuestionType.id;
    for (let i = 0; i < answerOptionsArr.length; i++) {
        if (answerOptionsArr[i].imageUrl){
            hasImages = true;
        }
        let obj = {
            answerId: answerOptionsArr[i].id,
            displayName: answerOptionsArr[i].displayName,
            imageUrl: answerOptionsArr[i].imageUrl || ''    
        }
        if (questionTypeId == QUESTION_TYPE.CHECKBOX && responseObj && responseObj[questionId]) {
           obj.checked = responseObj[questionId][answerOptionsArr[i].id] ? true : false
        }
        answerOptions.push(obj);
    }
    return {
        answerOptions,
        hasImages
    };
}