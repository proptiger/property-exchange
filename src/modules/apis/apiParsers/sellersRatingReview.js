"use strict";

const   sellersRatingReviewsService = require('services/sellersRatingReviewsService');

module.exports.apiHandler = function(req, res, next) {

    let listingUserCompanyUserId = req.query.listingUserCompanyUserId || '';

    let ratingsPromise = sellersRatingReviewsService.getSellerRatingsMap(listingUserCompanyUserId);
    ratingsPromise.then((data)=>{
        return res.send({
            statusCode: '2XX',
            data
        });
    }, (err)=>{
        next(err);
    });
};
