'use strict';
let trackerCookiesService = require('services/trackerCookiesService'),
logger = require('services/loggerService'),
experimentsService = require('services/experimentsService');

module.exports.apiHandler = function(req, res, next){
    trackerCookiesService.setTrackingCookies(req,res).then((response)=>{
    	experimentsService.setExperimentsCookie(req, res);
        return res.send(response);
    }, (e)=>{
        logger.info('trackerCookieParser catch callback');
    	experimentsService.setExperimentsCookie(req, res);
        next(e);
    });
}
