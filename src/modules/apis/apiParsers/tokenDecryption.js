"use strict";
const apiService = require('services/apiService');
const apiConfig = require('configs/apiConfig');


module.exports.apiHandler = function(req, res, next) {
    if(req.body.token){
        return apiService.post(apiConfig.getDecryptedToken().url, {
            res,
            req,
            body : req.body.token
        }).then(response => {
            res.send(response.data);
        },(err)=>{
            next(err);
        });
    }else{
        next(new Error("no token found"));
    }
};
