'use strict';

let propertyService = require("services/propertyService"),
	utilService = require("services/utilService"),
	//imageParser = require('services/imageParser'),
	_ = utilService._;

function _errorHandler(next){
	let error = new Error("Please provide relevant parameters");
	error.status = 400;
	next(error);
}

module.exports.apiHandler = function(req, res, next){
	if(req.query.listingIds){
		let listingIds;
		try{			
			listingIds = JSON.parse(req.query.listingIds);
		} catch(e){
			return _errorHandler(next);
		}

		let propertyPromise = [];
		_.forEach(listingIds, (v) => {
			propertyPromise.push(propertyService.getListingDetail(v, {req}));
		});
		Promise.all(propertyPromise).then((result) => {
			let finalData = {};
			_.forEach(result, (v) => {
				if(v && v.data){
					let parsedData = propertyService.parsePropertySummary(v.data, {req});
					finalData[parsedData.id] = parsedData.data;
				}
			});
			res.send(finalData);
		}, (err) => {
			next(err);
		}).catch((err) => {
			next(err);
		});		
	} else {
		return _errorHandler(next);
	}
};
