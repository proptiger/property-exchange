"use strict";
const bankService = require('services/bankService');

module.exports.apiHandler = function(req, res, next) {
    let data = {
        projectId: req.params && req.params.projectId
    };
    bankService.getApprovedBankDetails({req: req}, data).then((response) => {
        response = response || {};
        res.send(response);
    }, err => {
        next(err);
    }).catch(err => {
        next(err);
    });
};
