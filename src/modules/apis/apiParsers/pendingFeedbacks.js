'use strict';

const apiConfig = require('configs/apiConfig');
const apiService = require('services/apiService');
const utilService = require('services/utilService');
const logger = require('services/loggerService'); 

function timeLapsedSinceConnect(ms){
    let timeElapsedSinceConnectInHours, lastConnectedAt;
    timeElapsedSinceConnectInHours = ms/(1000*60*60);
    if(timeElapsedSinceConnectInHours <= 1){
        lastConnectedAt = `Connected just now`;
    } else if(timeElapsedSinceConnectInHours < 24){
        lastConnectedAt = `Connected ${Math.floor(timeElapsedSinceConnectInHours)} ${timeElapsedSinceConnectInHours > 1 ? 'hours' : 'hour'} back`;
    } else {
        lastConnectedAt = `Connected ${Math.floor(timeElapsedSinceConnectInHours/24)} ${(timeElapsedSinceConnectInHours/24) > 1 ? 'days' : 'day'} back`;
    }
    return {
        lastConnectedAt
    };
}


module.exports.apiHandler = function (req, res, next){
	let buyerId = req.params.buyerId;
	if(isNaN(buyerId) || !buyerId) {
		let error = new Error('input parameter not correct');
		error.status = 400;
        return next(error);
    } else{
    	let urlConfig = apiConfig.getBuyerPendingFeedbacks({buyerId});
        if(req.query.companyId){
            urlConfig.url += `?companyId=${req.query.companyId}`;
        }
    	apiService.get(urlConfig, {req}).then((response) => {
    		let result = response && response.data || [];
            if(result.length){
                let timelapsedData;
                result.forEach(item => {
                    timelapsedData = timeLapsedSinceConnect((new Date()).getTime() - item.lastConnectedAt);
                    item.ratingScreenText = item.displayText;
                    item.lastConnectedAt = timelapsedData.lastConnectedAt;
                    item.userId = buyerId;
                    item.companyAvatar = utilService.getAvatar(item.companyName) || {};
                });
            } else {
                logger.error("no seller to show");
            }
            return res.send(result);
    	}, (err) => {
    		next(err);
    	}).catch((err) => {
            next(err);
        });
    }
};
