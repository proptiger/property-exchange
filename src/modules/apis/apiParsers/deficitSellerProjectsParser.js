'use strict';

const apiConfig = require('configs/apiConfig'),
    apiService = require('services/apiService'),
    projectService = require('services/projectService'),
    agentService = require('services/agentService'),
    utilService = require('services/utilService'),
    _ = utilService._,
    masterDetailsService = require('services/masterDetailsService'),
    logger = require('services/loggerService');

var config = {
    filters: {
        budget: {
            type: "range",
            key: "price"
        },
        localityIds: {
            key: "localityId",
            value: 'integer'
        },
        cityId: {
            key: "cityId",
            value: 'integer'
        },
        unitType: {
            parserType: "unitType",
            key: "unitTypeId",
            value: 'Array'
        },
        listingCategory: {
            key: "listingCategory",
            value: 'Array'
        },
        listingSellerTransactionStatuses:{
            key: "listingSellerTransactionStatuses",
            value: 'Array'
        },
        projectDbStatus:{
            key: "projectDbStatus",
            value: 'Array'
        },
        bhk: {
            key: "bedrooms",
            value: 'Array'
        },
        suburbIds: {
            key: "suburbId",
            value: 'integer'
        },
        listingSellerDisplayAds: {
            type: "range",
            key: "listingSellerDisplayAds",
        },
        leadDeficitScore: {
            type: "range",
            key: "sellerLeadDeficitScore"
        }
    }
};
const BUDGET_PERCENTAGE = 0.1;


function _getMappedUnitType(id) {
    let unitTypes = masterDetailsService.getMasterDetails("unitTypes");
    return unitTypes[id];
}

function generateFilter(query) {
    let filters = [];
    let filterKeys = Object.keys(config.filters);
    _.forEach(filterKeys, (v) => {
        if (query[v]) {
            let filter = _.cloneDeep(config.filters[v]);
            if (filter.type === 'range' && query[v].indexOf(',') !== -1) {
                if (filter.key === 'price' || filter.key == 'leadDeficitScore') {
                    query[v] = query[v].split(',').map(function (i) {
                        return parseInt(i, 10);
                    }).join(",");
                }
                let range = query[v].split(',');
                if (range[0]) {
                    filter.from = range[0];
                }
                if (range[1]) {
                    filter.to = range[1];
                }
                filter.value = query[v].indexOf(',') === -1 ? query[v] : query[v].split(',');
            }else if(filter.key === 'price' && query[v].indexOf(',') == -1 ){
                let budget = query[v] && parseInt(query[v],10);
                if(budget  && !_.isNaN(budget)){
                    filter.from = Math.floor(budget - budget * BUDGET_PERCENTAGE);
                    filter.to = Math.ceil(budget + budget * BUDGET_PERCENTAGE);
                    filter.value = [filter.from.toString(),filter.to.toString()];
                }
            } else if (filter.value === 'integer') {
                filter.value = query[v].indexOf(',') === -1 ? parseInt(query[v], 10) : query[v].split(',').map(function (i) {
                    return parseInt(i, 10);
                });
            } else if (filter.value === 'Array') {
                filter.value = query[v].split(',');
            }
            if (!(filter.key === "localityId" && query.suburbIds)){
                filters.push(filter);
            }
        }
    });
    return filters;
}
function _getleadDeficitProjects(req, res){
    let query = req.query,
    filters = generateFilter(query),
    rows = query.rows ? query.rows : 10,
    selector = {
        filters,
        paging: {
            rows: rows,
            start: 0
        }
    };
    let urlObj = apiConfig.getlistingDerivedProject({
        query: {
            includeNearby: query.includeNearby || false,
            selector: apiService.createSelector(selector)
        }
    });
    return apiService.get(urlObj, {req}).then(response => {
        let parsedProjectData = projectService.parseProjectData(response,{},false);
        let sellerUserIds = [];
        let sellerDataMap = {};
        if (parsedProjectData.totalCount > 0){
            parsedProjectData.projects.forEach(item => {
                sellerUserIds.push(item.projectSeller);
            });
            let deficitSeller = agentService.getMultipleFeaturedSellers(sellerUserIds, { req, isMobile: true });
            return projectService.parseTopAgentForLead(deficitSeller, { cityId: query.cityId, req }).then(response => {
                response.forEach(sellerData => {
                    sellerDataMap[sellerData.companyUserId] = sellerData;
                });
                parsedProjectData.sellerDataMap = sellerDataMap;
                return parsedProjectData;
            }, (e) => {
                logger.error("error ocurred while getting seller data for lead deficit sellers", e);
                return(e);
            });
        } else {
            return (new Error('No projects available'));
        }
    }, (e) => {
        logger.error("error occurred while getting projects of lead deficit sellers", e);
        return(e);
    });
}     
function apiHandler(req, res, next) {
    return _getleadDeficitProjects(req,res).then(response => {
        if(response && response.totalCount > 0) {
            return res.status(200).send(response);
        }else{
            next(response);
        }
    }, (e) => {
        logger.error("error occurred while getting projects of lead deficit sellers", e);
        next(e);
    });
}
function _addProjectDetailkeys(project){
    let deficitProject = project;
    deficitProject.originalBackgroundImage = deficitProject.originalBackgroundImage ? `${deficitProject.originalBackgroundImage}?width=210&height=210`:'';
    deficitProject.overviewUrl = `${deficitProject.overviewUrl}?sellerUserIds=${JSON.stringify([deficitProject.projectSeller])}`;
    return deficitProject;

}
function getAmpleadDeficitProjects(req, res, next){
    return _getleadDeficitProjects(req,res).then(response => {
        let shuffledProjects = utilService.shuffleArray(response.projects);
        let deficitProject = {},similarDeficitProject={};
        if(shuffledProjects && shuffledProjects[0]){
            deficitProject =  _addProjectDetailkeys(shuffledProjects[0]);
        }
        if(shuffledProjects && shuffledProjects[1]){
            similarDeficitProject =  _addProjectDetailkeys(shuffledProjects[1]);
        }
        return {deficitProject,similarDeficitProject};
    }, (e) => {
        logger.error("error occurred while getting projects of lead deficit sellers", e);
        return(e);
    });
}

module.exports = {
    apiHandler,
    getAmpleadDeficitProjects
};
