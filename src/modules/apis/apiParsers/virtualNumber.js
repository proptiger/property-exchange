"use strict";
      const AgentService = require('services/agentService');
  


module.exports.apiHandler = function(req, res, next) {
    if(req.params.cityId && req.params.userId && req.params.listingType && !isNaN(req.params.userId) && !isNaN(req.params.cityId)){
        AgentService.getContactNumber(req.params.userId, req.params.cityId, req.params.listingType, {req}).then((response)=>{
            let callingNumber, userId,isVirtualNumber;
            if(response.length){
                callingNumber = (response[0].isVirtualNumber) ? response[0].contact : "01130507477";
                userId  = response[0].userId;
                isVirtualNumber = response[0].isVirtualNumber;
            }else{
                callingNumber = "01130507477";
                userId = req.params.userId;
                isVirtualNumber = false;
            }
            res.send({callingNumber,userId,isVirtualNumber});
        },(err)=>{
            next(err);
        });
    }else{
        next(new Error("provide relevant parameters"));
    }
};
