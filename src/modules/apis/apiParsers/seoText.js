"use strict";
const ApiService = require('services/apiService'),
    ApiConfig = require('configs/apiConfig'),
    _ = require('services/utilService')._;

module.exports.apiHandler = function(req, res, next) {
    //let data = {},
      let  query = req.query;
    if (query.url) {
        ApiService.get(ApiConfig.seoService({ url: query.url })).then((response) => {
            response = (response && response.data) || {};
            if (query.fields) {
                let fields = query.fields.split(',');
                let parsedResponse = {};
                _.forEach(fields, (v)=>{
                    parsedResponse[v] = response[v];
                });
                res.send(parsedResponse);
            } else {
                res.send(response);
            }
        }, err => {
            next(err);
        }).catch(err => {
            next(err);
        });
    } else {
        res.send({});
    }
};