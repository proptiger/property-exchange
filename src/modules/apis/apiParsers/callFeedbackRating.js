"use strict";

const ApiService    = require('services/apiService'),
      Apis          = require('configs/apiConfig'),
      utilService   = require('services/utilService');

function postRating(req, res, next){
    let sellerId = req.params.sellerId;
    let body = req.body || {};
    if(body.ratings && body.buyerId){
        let payload = {
            "createdBy": body.buyerId,
            "jsonDump": "",
            "ratings": body.ratings
        };
        return ApiService.post(Apis.submitCallFeedback({sellerId}), {
            res,
            req,
            body : payload
        }).then(response => {
            return res.send(response);
        },(err) => {
            next(err);
        });
    } else {
        next(utilService.getError(400));
    }
}

function getRating(req, res, next){
    let sellerId = req.params.sellerId;
    let buyerId = req.query.buyerId;
    let createdAt = utilService.dateAhead('', {days: -1}, true).toJSON();
    let selector = {
        filters: [{
            "key": "ratingParameterId",
            "value": 59
        },{
            "key": "createdBy",
            "value": buyerId
        },{
            "multiple": true,
            "filters": [{
                "key": "createdAt",
                "type": "ge",
                "value": createdAt
            },{
                "multiple": true,
                "filter": "or",
                "filters": [{
                    "key": "entityTypeId",
                    "value": 16
                },{
                    "key": "entityDataId",
                    "value": sellerId,
                    "filter": "and"
                }]
            }]
        }]
    };
    if(sellerId && buyerId){
        return ApiService.get(Apis.getFeedbackRating(ApiService.createFiqlSelector(selector)),{
            res,
            req
        }).then(response => {
            return res.send(response.data);
        },(err) => {
            next(err);
        });
    } else {
        next(new Error(`either sellerId or buyerId not provided`));
    }
}

module.exports.apiHandler = function(req, res, next) {
    let method = req.method;
    switch(method){
        case 'GET':
            getRating(req, res, next);
            break;

        case 'POST':
            postRating(req, res, next);
            break;

        default:
            break;
    }
    
};
