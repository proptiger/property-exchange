"use strict";
const apiService = require('services/apiService');
const apiConfig = require('configs/apiConfig');

function _errorHandler(next){
    let error = new Error("NOT A SUPPORTED REQUEST METHOD (rippleCase)");
    error.status = 400;
    next(error);
};

module.exports.apiHandler = function(req, res, next) {

    let requestMethod = req.method ||'';
    switch(requestMethod){
        case 'POST':
            let postData = [];
            postData.push(req.body);
            let urlConfig = apiConfig.postRippleCase();
            return apiService.post(urlConfig, {
                body: postData
            }).then((response)=>{
                return res.send(response.data);
            },(err)=>{
                next(err);
            });
            break;
        case 'PUT':
            if(req.query.rippleCaseId) {
                postData = req.body;
                let urlConfig = apiConfig.putRippleCase(req.query.rippleCaseId);
                return apiService.put(urlConfig, {
                    body: postData
                }).then((response)=>{
                    return res.send(response.data);
                },(err)=>{
                    next(err);
                });
            }
            break;
        default:
            return _errorHandler(next);
    }
    
};
