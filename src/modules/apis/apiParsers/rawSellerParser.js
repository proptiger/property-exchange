"use strict";
const agentService = require('services/agentService');
const utilService = require('services/utilService');

module.exports.apiHandler = function(req, res, next) {
    let sellerUserId = req.params.sellerUserId;
    agentService.getRawSeller(sellerUserId).then((response) => {
        response.sellerAvailable = true;
        response.allowCallInNight = false;
        var istOffset = 330; // IST offset UTC +5:30 
        var currentTime = new Date(),
            currentOffset = currentTime.getTimezoneOffset(),
            istTime = new Date(currentTime.getTime() + (istOffset + currentOffset) * 60000),
            sellerTime = utilService.getSellerCallingTime();
        if ((istTime.getHours() < sellerTime.startTime || istTime.getHours() >= sellerTime.endTime) &&
            response.allowCallInNight === false) {
            response.sellerAvailable = false;
        }
        res.send(response);
    }, err => {
        next(err);
    }).catch(err => {
        next(err);
    });
};