'use strict';

const   apiConfig      = require('configs/apiConfig'),
        apiService     = require('services/apiService');

module.exports.apiHandler = function(req, res, next){
	let filters = 'isSellerImagePresent==true',
		start=0,
		rows=5,
		fields='leadPaymentStatus,id,leadId,updatedAt,seller,sellerLogoUrl,buyer,locality,city,sellerScore,leadPaymentStatus,createdAt';
	if(req.query.entityName && req.query.entityValue){
		filters=req.query.entityName+'=='+req.query.entityValue;
	}
	if(req.query.start){
		start=req.query.start;
	}
	if(req.query.rows){
		rows=req.query.rows
	}
	apiService.get(apiConfig.recentDeals({
			start,
			rows,
			fields,
			filters
		}),{req}).then((result)=>{
			res.send(result)
		},(error)=>{
			next(error)
		}).catch(error=>{
			next(error)
		})
};
