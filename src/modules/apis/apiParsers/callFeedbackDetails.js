"use strict";
const PropertyService = require('services/propertyService');
const agentService = require('services/agentService');
const utilService = require('services/utilService');
const maps = require('services/mappingService');


module.exports.apiHandler = function(req, res, next){
    let listingId = req.query.listingId;
    let companyId = req.query.companyId;
    let postData = {};
    if(listingId){
        let fields = ["currentListingPrice","listingCategory","price","localityId","logo","profilePictureURL","companySeller","activeStatus","mainImageURL","company","user","imageURL","cityId","unitType","size","unitTypeId","projectStatus","id","label","name","project","property","bedrooms","locality","suburb","city","rk","penthouse","studio"];
        return PropertyService.getListingDetail(listingId, {
                req: {},
                fields
            }).then((response) => {
                response = response && response.data;
                if (!response) {
                    throw (new Error("data null in listing"));
                }
                let property = response.property || {};
                let project = property.project || {};
                postData.listingId = listingId;
                postData.propertyType = maps.getPropertyType(property.unitTypeId);
                var propertyTypeLowercase = postData.propertyType && postData.propertyType.toLowerCase();
                postData.isPlot = propertyTypeLowercase === 'residential plot' ? true : false;
                postData.rk = property.rk;
                postData.bhkInfo = property.bedrooms + ((property.rk) ? ' RK' : ' BHK');
                postData.listingTitle = postData.isPlot ? postData.propertyType : `${postData.bhkInfo}, ${postData.propertyType}`;
                postData.companyId = response.companySeller && response.companySeller.company && response.companySeller.company.id;
                postData.companyImage = response.companySeller && response.companySeller.user && response.companySeller.user.profilePictureURL;
                postData.companyName = response.companySeller && response.companySeller.company && response.companySeller.company.name;
                postData.logo = response.companySeller && response.companySeller.company && response.companySeller.company.logo;
                postData.beds = property.bedrooms;
                postData.image = response.mainImageURL;
                postData.locality = project.locality && project.locality.label;
                postData.city = project && project.locality.suburb.city.label;
                postData.cityId = project && project.locality.suburb.city.id;
                postData.listingSubtitle = project.activeStatus.toUpperCase() === "DUMMY" ? postData.locality : `${project.name}, ${postData.locality}`;
                postData.price = utilService.formatNumber(response.currentListingPrice ? response.currentListingPrice.price : '', { precision : 2, seperator : req.locals.numberFormat });
                postData.homeloanMinPrice = response.currentListingPrice ? response.currentListingPrice.price :'';
                postData.showHomeloan = response.listingCategory && response.listingCategory.toLowerCase()!="rental";
                return res.send(postData);
            }, (err) => {
                next(err);
            }).catch(err => {
                next(err);
            });
    } else if(companyId) {
        return agentService.getSellerDetailsByCompanyId(companyId, {req: {}}).then((company) => {
                    company = company[0];
                    postData.companyName = company.name;
                    postData.companyId = company.id;
                    postData.logo = company.logo;
                    postData.companyAvatar = utilService.getAvatar(company.name) || {};
                    postData.sellerType = company.sellerType;
                    postData.showHomeloan = true;
              return res.send(postData);
        },(err) => {
            next(err);
        }).catch(err => {
            next(err);
        });
    } else {
        next(new Error("no listingId or CompanyId present"));
    }
};
