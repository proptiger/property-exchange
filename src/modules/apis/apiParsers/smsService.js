"use strict";

const apiService = require('services/apiService'),
    apiConfig = require('configs/apiConfig'),
    smsService = require('services/smsService');

module.exports.apiHandler = function(req, res, next) {
    var phone = req.body.phone,
        smsString = req.body.smsString;

    smsService.sendMessage(phone, smsString, { req }).then(response=>{
        res.status(200).send(response);
    }, err =>{
        next(err);
    });
};

module.exports.sendSMS = function(phone, smsString) {
    if (phone && smsString) {
        let api = apiConfig.sms({
            contact: phone
        });
        return apiService.post(api, {
            body: smsString
        }).then((response) => {
            return response;
        });
    } else {
        return new Promise((resolve, reject)=>{
                let message = "";
                if(!phone){
                    message = "No Phone number present";
                }else if(!smsString){
                    message = "No Sms String present";
                }
                reject(new Error(message));
            });
    }
};
