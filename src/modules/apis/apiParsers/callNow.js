'use strict';
let apiService = require('services/apiService');
let apiConfig = require('configs/apiConfig');
let buyerOptInService = require('services/buyerOptInService');
let loggerService = require('services/loggerService');

module.exports.apiHandler = function(req, res, next){
	let callApi = apiConfig.callNow();
	let callingPayload = req.body.callerPayload;
	let data = req.body.extraPayload && req.body.extraPayload.data || {};
	if(data.isCommercial) {
		callingPayload.commercial = true;
	}
	return apiService.post(callApi, {
		body: callingPayload,
		req
	}).then((result) => {
		buyerOptInService.sendOptInStatus(data);
		return res.send(result);
	}, (err) => {
		next(err);
	})
}