/**
    This parser parses the list of sponsored project based on the city, saleType, page and project's ad expiry time.
    This is dedicated to the sponsoredProjectAds client side module to show sponsored projects paid ad.
*/
"use strict";

const apiService = require('services/apiService'),
    loggerService = require('services/loggerService'),
    apiConfig = require('configs/apiConfig'),
    utils = require('services/utilService');


module.exports.apiHandler = function(req, res, next){
    let cityId = req.query.cityId ? parseInt(req.query.cityId) : 0;
    let page = req.query.page || "";
    let listingType = req.query.listingType || "";
    let currentDate = (new Date()).getTime();
    let urlObject = apiConfig.getSponsoredProject();
    apiService.get(urlObject).then(response => {
        if(response && response.length){
            let data = response.filter(project => {
                let projectExpiryInMilliseconds = (project.expiresOn && new Date(project.expiresOn).getTime()) || '';
                return (project.pagesToShowOn.indexOf(page) > -1 && project.visiblityInCities.indexOf(cityId) > -1 && project.saleTypeSupported.indexOf(listingType) > -1 && (!projectExpiryInMilliseconds || currentDate < projectExpiryInMilliseconds));
            });
            return res.status(200).send(data);
        } else {
            return res.status(200).send([]);
        }
    }).catch(err => {
        next(err);
    });
}