"use strict";

const apiService = require('services/apiService'),
    utilService = require('services/utilService'),
    apiConfig = require('configs/apiConfig'),
    reportErrorParser = require('services/reportErrorParser');

module.exports.apiHandler = function(req, res, next) {

    let reportdata = req.body;
    if (reportdata) {
       let userId = reportdata["userId"],
            listingId = reportdata["listingId"],
            phone = [],
            errorDetailsIds = reportdata["errorDetailsId"],
            description = reportdata["description"];
          
        phone.push({ "contactNumber": reportdata["phone"] });

        reportErrorParser.sendMessage({
            userId,
            listingId,
            phone,
            errorDetailsIds,
            description
        }, {
            req
        }).then(response => {
            res.status(200).send(response);
        }, err => {
            next(err);
        });
    } else {
            let err = new Error("data incorrect");
            next(err);
    }

};
