"use strict";
const localityService = require('services/localityService'),
      utilService     = require('services/utilService');

module.exports.apiHandler = function(req, res, next) {
    let lat, lon, fields, count, radius, query,localityId;
    if (!req.query || req.query.lat==='undefined' || req.query.lon==='undefined') {
        next(utilService.getError(400));
    }else{
        query = req.query;
        lat = query.lat;
        lon = query.lon;
        localityId = query.localityId;
        fields = query.fields;
        count = query.rows || 5;
        radius = query.radius || 10;
        fields = fields == "false" ? "" : (fields && fields.split(',')) || 'all';
        localityService.getNearbyLocalities(lat, lon,localityId, fields, count, radius, {req}).then((response) => {
            response = response || {};
            res.send(response);
        }, err => {
            next(err);
        }).catch(err => {
            next(err);
        });
    }
};
