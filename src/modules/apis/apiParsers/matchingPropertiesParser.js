'use strict';

let listingsService = require("services/listingsService");
let similarPropertyService = require("services/similarPropertyService");

const   fields = [
            "localityId",
            "listing",
            "property",
            "project",
            "builder",
            "displayName",
            "locality",
            "suburb",
            "city",
            "state",
            "currentListingPrice",
            "companySeller",
            "company",
            "user",
            "id",
            "name",
            "label",
            "listingId",
            "propertyId",
            "projectId",
            "propertyTitle",
            "unitTypeId",
            "resaleURL",
            "size",
            "measure",
            "bedrooms",
            "pricePerUnitArea",
            "price",
            "localityAvgPrice",
            "rk",
            "buyUrl",
            "rentUrl",
            "overviewUrl",
            "listingCategory",
            "type",
            "logo",
            "profilePictureURL",
            "mainImageURL",
            "mainImage",
            "absolutePath",
            "altText",
            "title",
            "defaultImageId",
            "projectStatus",
            "videos",
            "imageUrl",
            "penthouse",
            "studio",
            "allocation",
            "allocationHistory",
            "masterAllocationStatus",
            "status"
        ];
module.exports.apiHandler = function(req, res, next){
	req.query.fields = fields.slice();
	let listingAPIPromise = listingsService.getApiListings(req.query, {pageType: req.query.pageType}, {req, itemsPerPage: 5});
	listingAPIPromise.then(response => {
		let selector = response.selector;
        response = response.response;
        let totalCount = response && response.data[0].totalCount || 0;
        let data = (response.data[0] && response.data[0].facetedResponse ? response.data[0].facetedResponse.items : response.data[0].items) || [];
        data = similarPropertyService.parseSimilarPropertyData({data: {items: data}});
        return res.status(200).send({totalCount, data: data.properties});
	}, (err) => {
		next(err);
	});
}
