'use strict';
/* Gives Popular localities based on locality score, listings in those localities and active agents*/
const   apiConfig      = require('configs/apiConfig'),
        apiService     = require('services/apiService'),
        listingsService = require("services/listingsService");

function errorHandler(res){
	res(500).send("error in api response");
}
module.exports.apiHandler = function(req, res, next){

	let cityId = 11,
		rows = 4,
		fields='"localityId","label"';
	if(req.query.cityId ){
		cityId=req.query.cityId;
	}
	if(req.query.rows){
		rows=req.query.rows;
	}
	if(req.query.fields){
		fields=req.query.fields;
	}
	let selector = `{"fields":[${fields}],"paging":{"rows":${rows}}}`;
	apiService.get(apiConfig.popularLocalitiesOnRelevance({
	    cityId,
	    selector
	}), { req }).then((result) => {
	    if (result && result.data && result.data.length > 0) {
	        let popularLocalityList = result.data;
	        let localityList = result.data.map((locality) => {
	            return locality.localityId;
	        });
	        let selector = { "filters": [{ key: 'localityId', value: localityList }, { key: 'status', value: 'Active' }], "jsonFacets": { "groupByField": 'localityId', "fields": ["uniqueFuncSellerId"], "paging": { "rows": 4 } }, "paging": { "rows": 0 } };
	        listingsService.getListingsData(selector ,{req}).then((response) => {
	            if (response.data && response.data.jsonFacetsGroup && response.data.jsonFacetsGroup.length > 0) {
	                let jsonFacet = response.data.jsonFacetsGroup[0];
	                if (jsonFacet.buckets && jsonFacet.buckets.length > 0) {
	                    let uniqueSellers = {},
	                        popularLocalitiesAndSellers = [];
	                    jsonFacet.buckets.map((bucket) => {
	                        let uniqueSellerCount = 0;
	                        if (bucket.facetValues) {
	                            bucket.facetValues.map((facet) => {
	                                if (facet.name == "SellerId") {
	                                    uniqueSellerCount = facet.aggregates.unique;
	                                }
	                            });
	                        }
	                        uniqueSellers[bucket.value] = {
	                            listingCount: bucket.count,
	                            uniqueSellerCount: uniqueSellerCount
	                        };
	                    });

	                    popularLocalityList.map((locality) => {
	                        if (uniqueSellers[locality.localityId]) {
	                            let localityId = locality.localityId,
	                                localityName = locality.label;
	                            popularLocalitiesAndSellers.push({
	                                localityName: locality.label,
	                                uniqueSellerCount: uniqueSellers[locality.localityId].uniqueSellerCount,
	                                listingCount: uniqueSellers[locality.localityId].listingCount,
	                                localitySERPUrl: `listings?listingType=buy&pageType=LOCALITY_URLS&localityName=${localityName}&localityId=${localityId}&templateId=MAKAAN_LOCALITY_LISTING_BUY`
	                            });
	                        }
	                    });
                      req.cacheControl = "691200";
	                    res.send({ popularLocalities: popularLocalitiesAndSellers });
	                } else {
	                    errorHandler(res);
	                }
	            } else {
	                errorHandler(res);
	            }
	        }, error => {
	            next(error);
	        });
	    } else {
	        errorHandler(res);
	    }
	}, (error) => {
	    next(error);
	}).catch(error => {
	    next(error);
	});

};
