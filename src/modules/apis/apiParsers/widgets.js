"use strict";
const widgetService = require('services/widgetService'),
    utilService = require('services/utilService');

module.exports.apiHandler = function(req, res, next) {
    let widget = req.params.widget;
    let query = req.query;
    let urlConfig = {
        localityId: query.localityId,
        cityId: query.cityId,
        bhk: query.bhk,
        affordableRent: query.rent
    };
    switch (widget) {
        case "buy-rent":
            widgetService.getBuyRentSuggestion({ 
            	cityId : urlConfig.cityId , 
            	localityId : urlConfig.localityId,
            	bhk: urlConfig.bhk && urlConfig.bhk.split(','),
            	affordableRent: urlConfig.affordableRent
            },{req}).then((response)=>{
            	res.send(response);
            },(error)=>{
            	next(error);
            });
            break;
        default:
            next(utilService.getError(400));
            break;
    }
};