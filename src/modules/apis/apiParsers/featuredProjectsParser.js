"use strict";

const   apiConfig      = require('configs/apiConfig'),
        apiService     = require('services/apiService'),
        projectService = require('services/projectService'),
        utils = require('services/utilService'),
        _isEmptyObject = utils.isEmptyObject;

module.exports.apiHandler = function(req, res, next) {

    let cityId = req.params.cityId,
        suburbId = req.query.suburbId,
        localityId = req.query.localityId,
        projectCount = req.query.rows || 20,
        fetchSponsoredProjects = req.query.fetchSponsoredProjects || false;
    //TODO XHR request
    //req.locals = req.locals || {};
    //req.locals.numberFormat = 3;

    let fetchUrlsPromise, featuredProjectsPromise,
        urlParamObj = {}, urlObj;

    if(suburbId && suburbId!="undefined"){
        urlParamObj.urlDomain = "suburb";
        urlParamObj.domainIds = [suburbId];
        urlParamObj.urlCategoryName = "MAKAAN_SUBURB_PROJECTS_SERP_BUY";
    }else if(localityId && localityId!="undefined"){
        urlParamObj.urlDomain = "locality";
        urlParamObj.domainIds = [localityId];
        urlParamObj.urlCategoryName = "MAKAAN_LOCALITY_PROJECTS_SERP_BUY";
    }else if(cityId && cityId!="undefined"){
        urlParamObj.urlDomain = "city";
        urlParamObj.domainIds = [cityId];
        urlParamObj.urlCategoryName = "MAKAAN_CITY_PROJECTS_SERP_BUY";
    }
    if(!_isEmptyObject(urlParamObj)){
        urlObj = apiConfig.fetchUrls(JSON.stringify([urlParamObj]));
        fetchUrlsPromise = apiService.get(urlObj, {req});

        featuredProjectsPromise = projectService.getFeaturedProjects({
            cityId,
            suburbId,
            localityId,
            projectCount,
            fetchSponsoredProjects
        }, {req});

        Promise.all([featuredProjectsPromise, fetchUrlsPromise]).then((results)=>{

            let projectsUrl = results[1] && results[1].data && results[1].data[urlParamObj.urlCategoryName] && results[1].data[urlParamObj.urlCategoryName][urlParamObj.domainIds[0]];

            let response = results[0];
            response.projectsUrl = projectsUrl;
            return res.send(response);
        }, (err) => {
            next(err);
        });
    }else{
        next(new Error("no location found for getting featured projects"));
    }

};
