"use strict";

var uuid = require("node-uuid"),
    mpAnalyticsService = require("services/mpAnalytics"),
    utils = require('services/utilService'),
    logger = require('services/loggerService'),
    _ = utils._;

const TRACKING_COOKIE_EXPIRE_TIME = (24*60*60*1000);

var _generateUuid = function() {
    return uuid.v4();
};

module.exports.apiHandler = function(req, res,next) {
    let body = req.body,
        traits = body && body.traits,
        type = body && body.type,
        visitor_id = body && body.visitor_id,
        response = {
            statusCode: '2XX',
            data:"success"
        };
    switch(type) {
        case "identify" :
            if(!visitor_id) {
               visitor_id =  _generateUuid();
               res.cookie('_mpa', visitor_id, { maxAge: TRACKING_COOKIE_EXPIRE_TIME, httpOnly: false});
            }
            let model = {
                visitor_id: visitor_id
            };
            _.extend(model,traits);
            mpAnalyticsService.findAndUpdate(model,true).then( () =>{
                res.status(200).send(response);
            }, (err) => {
                logger.info(err.message);
                res.status(500).send(err.message);
            })
            .catch((err) =>{
                logger.info(err.message);
                res.status(500).send(err.message);
            } );
            break;
        case "track":
            if(visitor_id) {
                let model = {
                   visitor_id: visitor_id
                };
                _.extend(model,traits);
                mpAnalyticsService.findAndUpdate(model).then( () => {
                    mpAnalyticsService.sendFiltersSuggestion(model,req);
                    res.status(200).send(response);
                },(err) => next(err))
                .catch((err)=> next(err));
            }
            break;
        default:
        res.status(400).send({
            message: "Provide relevant parameters"
        });
        break;
    }
};
