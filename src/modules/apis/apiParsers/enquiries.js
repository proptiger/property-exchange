"use strict";

const apiService = require('services/apiService'),
Logger = require('services/loggerService'),
masterDetailsService = require('services/masterDetailsService'),
apis = require('configs/apiConfig'),
buyerOptInService = require('services/buyerOptInService'),
utilService = require('services/utilService'),
_ = utilService._;

function _getMappedUnitType(id) {
    let unitTypes = masterDetailsService.getMasterDetails("unitTypes");
    return unitTypes[id];
}

module.exports.apiHandler = function(req, res, next) {
    var callType = req.query.callType;
    switch (callType) {
        case 'ampOtpOnCall':
            if(req.body.userId && req.body.userNumber){
                req.params.userId = req.body.userId;
                req.params.userNumber = req.body.userNumber;
                _otpOnCall(req, res, next);
                break;
            } else {
                next(utilService.getError(400,"Invalid UserId, userNumber"));
            }       
            break;
        case 'otpOnCall':
            _otpOnCall(req, res, next);
            break;
        case 'verifyTempEnquiry':
            _verifyTempEnquiry(req,res,next);
            break;
        default:
            _enquiryAndVerification(req, res, next);
            break;
    }
};

function _insertKeyForViewFeedback(obj) {
    if(!obj || utilService.isEmptyObject(obj)) {
        return;
    }
    // in case of VIEW_PHONE
    if(obj.enquiryType && obj.enquiryType.id === 7 && obj.sellerId && obj.buyerId && obj.deliveryId) {
        let api = apis.feedbackToChat(),
            payload = {
                feedbackKey: obj.buyerId + ":" + obj.sellerId ,
                deliveryId: obj.deliveryId
            };
        apiService.post(api, {
            body: payload
        }).then(()=>{
            Logger.info('view phone event posted');
        },()=>{
            Logger.info('Error: While posting event for view phone');
        });
    }
}

function _otpOnCall(req, res, next){
    apiService.get(apis.sendOtpOnCall({
        userId: req.params.userId,
        userNumber: req.params.userNumber
    }), {req}).then(response=>{
        res.send(response);
    }, err => {
        res.send(err);
    }).catch(err => {
        next(err);
    });
}

function _verifyTempEnquiry(req,res,next){
    let putData = req && req.body || {};
    let urlConfig = apis.tempEnquiries();
    apiService.put(urlConfig, {
        body: putData,
        req: req,
        res: res
    }).then((response) => {
        res.status(200).send(response);
    }, err => {
        console.log("payload--:",putData);
        next(err);
    }).catch(err => {
        console.log("payload--:",putData);
        next(err);
    });
}

function _enquiryAndVerification(req, res,next) {
    let api,
        isTempEnquiry = false;
    if(req.params.temp){
        api = apis.tempEnquiries;
        isTempEnquiry = true;
    }else{
        api = apis.enquiries;
    }
    if (req.method === "POST") {
        let urlConfig = api({
            query: {
                lastEnquiryRequired: true,
                debug:true
            }
        });
        let subject = process.env.WEBSITE_URL + '/' + urlConfig.url;
        let propertyTypes = [];
        if(req.body.propertyTypes && Array.isArray(req.body.propertyTypes) && req.body.propertyTypes.length ) {
            req.body.propertyTypes = req.body.propertyTypes.filter( n => n );
            _.forEach(req.body.propertyTypes,(v)=>{
                let value = _getMappedUnitType(v.toString());
                if(value){
                    propertyTypes.push(value);
                }
            });
        }
        if(propertyTypes.length > 0){
            req.body.propertyTypes = propertyTypes;
        } else {
            delete req.body.propertyTypes;
        }
        console.log("payload:------", req.body);
        let mailPayload = {
            body: req.body
        },
        optInData = _.cloneDeep(req.body),
        deliveryId = req.body && req.body.deliveryId,
        sellerId = req.body && req.body.companyUserId;
        delete req.body.deliveryId;
        delete req.body.companyUserId;
        delete req.body.otp;
        delete req.body.optIn;
        apiService.post(urlConfig, {
            body: req.body,
            req: req,
            res: res
        }).then((response) => {
            if (response && response.data && Object.keys(response.data).length) {
                 let obj = {
                    buyerId: response && response.data && response.data.userId,
                    sellerId,
                    enquiryType: req.body.enquiryType,
                    deliveryId
                 };

                !isTempEnquiry && _insertKeyForViewFeedback(obj); //jshint ignore:line
                buyerOptInService.sendOptInStatus(optInData);
                res.status(200).send(response.data);
            } else {
                throw new Error("error");
            }
        }, err => {
            mailPayload.err = err || {};
            utilService.sendMail(subject, JSON.stringify(mailPayload));
            next(err);
        }).catch(err => {
            mailPayload.err = err || {};
            utilService.sendMail(subject, JSON.stringify(mailPayload));
            next(err);
        });
    } else if (req.method === "PUT") {
        var enquiryId = req.body.userEnquiryId,
            otp = req.body.otp,
            putData = {};

        if (req.body.userId) {
            putData.userId = req.body.userId;
        }
        if (otp) {
            putData.phone = req.body.phone;
        } else {
            if (req.body.phone) {
                putData.phoneToVerify = req.body.phone;
            }
        }
        if (req.body.bhk) {
            putData.bhk = req.body.bhk;
        }
        if (req.body.localityIds) {
            putData.localityIds = req.body.localityIds;
        }

        let urlConfig = api({enquiryId,
            query: {
                otp: otp
            }
        });
        apiService.put(urlConfig, {
            body: putData,
            req: req,
            res: res
        }).then((response) => {
            res.status(200).send(response);
        }, err => {
            console.log("payload--:",putData);
            next(err);
        }).catch(err => {
            console.log("payload--:",putData);
            next(err);
        });
    }

}

