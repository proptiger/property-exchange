/*
 * Description: Parser to handle all ajax calls related to alliances
 * @author [Harvinder Singh]
 */

"use strict";

//const async = require('async');
const _ = require('services/utilService')._;
//const moment = require('moment');


const apiService = require('services/apiService'),
    //logger = require('services/loggerService'),
    apiConfig = require('configs/apiConfig');
    //allianceService = require('services/allianceService');

function apiHandler(req, res, next) {
    var method = req.method;

    switch (method) {
        // case 'GET':
        //     getRequest(req, res, next);
        //     break;

        case 'POST':
            postRequest(req, res, next);
            break;

        case 'PATCH':
            patchRequest(req, res, next);
            break;

        default:
            break;
    }
}

// function getRequest(req, res, next) {
//     var query = req.query;
//     if ("serviceId" in query) {
//         //api call for alliance
//         allianceService.getAlliances(query).then(response => {
//             res.send(response);
//         }, error => {
//             logger.error("Error in alliances apis xhr request", error);
//         });
//     } else if ("categoryId" in query) {
//         //api call for services
//         allianceService.getServices(query).then(response => {
//             res.send(response);
//         }, error => {
//             logger.error("Error in alliances apis xhr request", error);
//         });
//     } else if ("cityId" in query) {
//         //apicall for city list
//         allianceService.getServiceCategories(query).then(response=>{
//             res.send(response);
//         }).catch(error=>{
//             logger.error("error in get categories apis in alliance", error);
//             res.send(error);
//         });
//     }
// }

function postParser(data={}){
    let finalResponse = {},
        userOfferServiceRequest = _.isArray(data.userOfferServiceRequests) && data.userOfferServiceRequests[0],
        couponData = userOfferServiceRequest.coupon,
        couponInfo = couponData && couponData.couponCode,
        companyOffer = couponData && couponData.companyOffer,
        companyInfo = companyOffer && companyOffer.company,
        userInfo = data.user;

    finalResponse.leadId = data.id;
    finalResponse.offer = couponData.offer;
    finalResponse.emailId = userInfo && userInfo.email;
    finalResponse.hasVerifiedEmail = data.hasValidPrimaryEmail;
    finalResponse.couponCode = couponInfo && couponInfo.couponCode;
    finalResponse.couponType = couponInfo && couponInfo.couponCodeType;
    finalResponse.allianceName = companyInfo.name;
    finalResponse.allianceUrl = companyInfo.website;

    // data = data || {};
    // let offers = data.userOfferServiceRequests || [],
    //     finalResponse = {};
    // finalResponse.isOtpVerified = data.isOtpVerified;
    // finalResponse.otpSent = data.isOtpSent;
    // finalResponse.offers = offers.map(coupon=>{
    //     coupon = coupon.coupon;
    //     let code = coupon && coupon.couponCode;
    //     return {
    //         offer: coupon && coupon.offer,
    //         expiry: coupon && moment(coupon.expiryDate).format("Do MMMM, YYYY"),
    //         couponCode: code.couponCode,
    //         logo: coupon && coupon.companyOffer && coupon.companyOffer.company && coupon.companyOffer.company.logo
    //     };
    // });
    return finalResponse;
}

function postRequest(req, res) {
    var query = req.query ? req.query : {},
        apiOptions;
    apiOptions = {
        req: req,
        res: res,
        body: req.body
    };
    if(query.type === 'userOffer'){
        apiService.post(apiConfig.postAlliance(), apiOptions).then(response=> {
            res.status(200).send(postParser(response.data));
        }, error=> {
            res.status(500).send(error);
        });
    } else if (query.type === 'subscribe'){
        apiService.post(apiConfig.patchAlliance({
            userEmail: query.userEmail,
            userOfferLeadId: query.userOfferLeadId
        }), apiOptions).then(response=> {
            res.status(200).send(postParser(response.data));
        }, error=> {
            res.status(500).send(error);
        });
    }

}

function patchRequest(req, res){
    var query = req.query ? req.query : {},
        apiOptions;
    apiOptions = {
        req: req,
        res: res,
        body: req.body
    };
    apiService.patch(apiConfig.patchAlliance({
        userEmail: query.userEmail,
        userOfferLeadId: query.userOfferLeadId
    }), apiOptions).then(response=> {
        res.status(200).send(postParser(response.data));
    }, error=> {
        res.status(500).send(error);
    });
}

module.exports = {
    apiHandler
};
