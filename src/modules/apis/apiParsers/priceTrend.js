"use strict";
const utilService = require('services/utilService'),
    apiService = require('services/apiService'),
    apiConfig = require('configs/apiConfig'),
   // logger = require('services/loggerService'),
    moment = require('moment'),
    _ = utilService._;

const config = {
    locality: {
        required: {

        },
        optional: {
            'lat': null,
            'lon': null,
            'rows': 3,
            'duration': 6,
            'group': null,
            'compare': null,
            'prependName': '',
            'unitType': ''
        },
        groupingOffset: 12
    },
    city: {
        required: {
        },
        optional: {
            'rows': 4,
            'group': null,
            'duration': 6,
            'prependName': '',
            'unitType': '',
            'compare': null
        },
        groupingOffset: 12
    },
    project: {
        required: {},
        optional: {
            'rows': 3,
            'group': null,
            'duration': 6,
            'compare': null,
            'prependName': '',
            'unitType': ''
        },
        groupingOffset: 12
    }
};

function updateGrouping(group, duration, extra) {
    let result = [],
        defaultGrouping = duration > config.locality.groupingOffset ? 'quarter' : 'month';
    if (_.isArray(extra)) {
        result = extra;
    } else {
        result = [extra];
    }
    group = group ? group.indexOf(',') === -1 ? [group] : group.split(',') : [];
    if (group.length === 0) {
        result.push(defaultGrouping);
    } else if (group.length > 0 && group.indexOf('quarter') === -1 && group.indexOf('month') === -1) {
        result.push(group);
        result.push(defaultGrouping);
    } else {
        result.push(group);
    }
    return result;
}

function updateDetails(required, obj, query) {
    let temp = {};
    _.forEach(obj, (val, key) => {
        if (query[key]) {
            temp[key] = query[key];
        } else if (required) {
            let err = new Error("Provide relevant parameters. Missing "+ key);
            err.status = 500;
            throw err;
        } else {
            temp[key] = val;
        }
    });
    return temp;
}

function _parseTrend(response, type, trendField, prependName) {
    if (!response && !response.data) {
        return {};
    }
    response = response.data;
    let keys = Object.keys(response),
        length = keys.length,
        i, j, result = {};

    for (i = 0; i < length; i++) {
        let locality = response[keys[i]],
            month = Object.keys(locality),
            monthLength = month.length,
            key =  prependName + locality[month[0]][0][type];
        for (j = 0; j < monthLength; j++) {
            let price = locality[month[j]][0].extraAttributes[trendField];
            if (price) {
                result[key] = result[key] ? result[key] : {};
                let trend = locality[month[j]][0].extraAttributes[trendField];
                result[key][month[j]] = trend ? parseInt(trend) : null;
            }
        }
    }

    return result;
}

function _sendTrendResponse(output, details, {req, category}={}) {
    let selector, urlConfig,trendField, selectorObj = {},
        dateRange = _generateDateRange(details.duration);
        trendField =  (category=='buy')?'avgBuyPricePerUnitArea':'avgRentPricePerUnitArea';
    selectorObj.fields = [trendField];
    selectorObj.filters = [];
    selectorObj.fields.push(details.type);

    let filters = [];
    for (let i = 0; i < output.length; i++) {
        let key = Object.keys(output[i])[0];
        filters.push({
            'filter': 'or',
            'type': 'equal',
            'key': key,
            'value': output[i][key]
        });
    }
    selectorObj.filters.push({
        'multiple': true,
        'filter': 'or',
        'filters': filters
    });

    selectorObj.filters.push({
        'filter': 'and',
        'type': 'ge',
        'key': 'month',
        'value': dateRange.startDate
    }, {
        'filter': 'and',
        'type': 'le',
        'key': 'month',
        'value': dateRange.endDate
    });

    if(details.unitType){
        selectorObj.filters.push({
            'filter': 'and',
            'type': 'equal',
            'key': 'unitType',
            'value': details.unitType
        });
    }
    selectorObj.extra = {
        // monthDuration: details.duration,
        group: details.group
    };
    selector = apiService.createFiqlSelector(selectorObj);
    urlConfig = apiConfig.priceTrend({
        query: selector
    });
    return apiService.get(urlConfig, {req}).then((response) => {
        return _parseTrend(response, details.type, trendField, details.prependName);
    }, err => {
        throw err;
    });
}

function _generateDateRange(duration) {
    let days;
    let endDate = moment();
    let startDate = moment().subtract(duration, 'months');
    days = parseInt(endDate.format('D')) - 1;
    endDate = endDate.subtract(days, 'days').format('YYYY-MM-DD');
    days = parseInt(startDate.format('D')) - 1;
    startDate = startDate.subtract(days, 'days').format('YYYY-MM-DD');
    return {
        startDate,
        endDate
    };
}
module.exports.apiHandler = function(req, res, next) {
    let params = req.params,
        query = req.query,
        details = {},
        category;
    if (params.cityId) {
        let localityService = require('services/localityService'),
            cityDetails = {};
        details.id = params.cityId;
        details = _.assign(details, updateDetails(true, config.city.required, query));
        details = _.assign(details, updateDetails(false, config.city.optional, query));
        category = query.category || 'buy';
        cityDetails = _.assign(cityDetails, details);
        cityDetails.group = updateGrouping(details.group, details.duration, 'cityId');
        cityDetails.type = 'cityName';
        if (details.compare === 'topLocality') {
            details.group = updateGrouping(details.group, details.duration, 'localityId');
            details.type = 'localityName';
            localityService.getTopLocalities(details.id, {
                options: {
                    count: config.city.optional.rows,
                    fields: ['localityId'],
                    type: category
                },
                req: req
            }).then((response) => {
                if (!response || _.isEmpty(response)) {
                    res.send({});
                }else{
                    Promise.all([_sendTrendResponse(response, details, {req, category}), _sendTrendResponse([{
                        'cityId': cityDetails.id
                    }], cityDetails, {req, category})], {req}).then((response) => {
                        let parsedData = {};
                        if (response.length > 0) {
                            _.forEach(response, (val) => {
                                parsedData = _.assign(parsedData, val);
                            });
                        }
                        req.cacheControl = "691200";
                        res.send(parsedData);
                    }, err => {
                        next(err);
                    }).catch(err => {
                        next(err);
                    });
                }
            }, err => {
                next(err);
            }).catch(err => {
                next(err);
            });
        } else if (details.compare === 'topProjects') {
            details.type = 'projectName';
            details.group = updateGrouping(details.group, details.duration, 'projectId');
            // will implement when needed
            res.send("Not Implemented. Please do that.");
        } else {
            _sendTrendResponse([{'cityId': cityDetails.id}], cityDetails, {req, category}).then((parsedData) => {
                req.cacheControl = "691200";
                res.send(parsedData);
            }, err => {
                next(err);
            }).catch(err => {
                next(err);
            });
        }
    } else if (params.localityId) {
        let localityService = require('services/localityService');
        details.id = params.localityId;
        details = _.assign(details, updateDetails(true, config.locality.required, query));
        details = _.assign(details, updateDetails(false, config.locality.optional, query));
        category = query.category || 'buy';
        if (details.compare === 'nearByLocality') {
            details.group = updateGrouping(details.group, details.duration, 'localityId');
            details.type = 'localityName';
            if(details.lat!='undefined' && details.lon!='undefined')  {
                localityService.getNearbyLocalities(details.lat, details.lon, details.id, ['localityId'], 3, 10, {req}).then((response) => {
                    response = response && response.data;
                    response.push({
                        'localityId': details.id
                    });
                    if (!response || _.isEmpty(response)) {
                        res.send({});
                    }else{
                        _sendTrendResponse(response, details, {req, category}).then((parsedData) => {
                            req.cacheControl = "691200";
                            res.send(parsedData);
                        }, err => {
                            next(err);
                        }).catch(err => {
                            next(err);
                        });
                    }
                }, err => {
                    next(err);
                }).catch(err => {
                    next(err);
                });
            }else{
                _sendTrendResponse([{'localityId': details.id}], details, {req, category}).then((parsedData) => {
                    req.cacheControl = "691200";
                    res.send(parsedData);
                }, err => {
                    next(err);
                }).catch(err => {
                    next(err);
                });
            }
        } else if (details.compare === 'topProjects') {
            details.type = 'projectName';
            details.group = updateGrouping(details.group, details.duration, 'projectId');
            // will implement when needed
            res.send("Not Implemented. Please do that.");

        } else {
            details.group = updateGrouping(details.group, details.duration, 'localityId');
            details.type = 'localityName';
            _sendTrendResponse([{
                'localityId': details.id
            }], details, {req, category}).then((parsedData) => {
                req.cacheControl = "691200";
                res.send(parsedData);
            }, err => {
                next(err);
            }).catch(err => {
                next(err);
            });
        }
    } else if (params.projectId) {
        details.id = params.projectId;
        details = _.assign(details, updateDetails(true, config.project.required, query));
        details = _.assign(details, updateDetails(false, config.project.optional, query));
        details.group = updateGrouping(details.group, details.duration, 'projectId');
        details.type = 'projectName';
        category = query.category || 'buy';
        if (details.compare === 'similarProject') {
            // will implement when needed
            res.send("Not Implemented. Please do that.");
        } else {
            _sendTrendResponse([{
                'projectId': details.id
            }], details, {req, category}).then((parsedData) => {
                req.cacheControl = "691200";
                res.send(parsedData);
            }, err => {
                next(err);
            }).catch(err => {
                next(err);
            });
        }
    }
};
