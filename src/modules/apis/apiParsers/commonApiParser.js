"use strict";
const cityService = require('services/cityService'),
    utilService = require('services/utilService');

module.exports.apiHandler = function(req, res, next) {
    switch (req.params.dataType) {
        case 'cityLeadPrice':
            return cityService.cityPriceList(req, res).then((response) => {
                res.status(200).send(response);
            }, (err) => {
                next(err);
            }).catch((err) => {
                next(err);
            });
        default:
            next(utilService.getError(400));
    }
};
