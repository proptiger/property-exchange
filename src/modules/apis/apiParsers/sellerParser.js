"use strict";
const agentService = require('services/agentService'),
    projectService = require('services/projectService'),
    utilService = require('services/utilService'),
    _ = utilService._,
    masterDetailsService = require('services/masterDetailsService');

var config = {
    keys: {
        BUY_FILTER: "Primary,Resale",
        RENT_FILTER: "Rental"
    },
    filters: {
        budget: {
            type: "range",
            key: "price"
        },
        localityIds: {
            key: "localityId",
            value: 'integer'
        },
        unitType: {
            parserType: "unitType",
            key: "unitType"
        },
        listingCategory: {
            key: "listingCategory"
        },
        bhk: {
            key: "bedrooms"
        },
        rk: {
            key: "rk"
        },
        projectId: {
            key: "projectId",
            value: 'integer'

        },
        suburbIds: {
            key: "suburbId",
            value: 'integer'
        },
        localityOrSuburbIds: {
            key: "localityOrSuburbId",
            value: 'integer'
        },
        expertFilter: {
            key: "listingSellerBenefits",
            value: "array"
        },
        ignoreExpert: {
            key: "listingSellerBenefits",
            value: "array",
            type: "notEqual"
        }
    },
    optional: ["budget", "localityIds", "unitType", "listingCategory", "bhk", "projectId", "suburbId", "suburbIds", "rk", "expertFilter", "ignoreExpert", "localityOrSuburbIds"],
    topLocalityFields: ["label","localityId","localityHeroshotImageUrl"]
};


function _getMappedUnitType(id) {
    let unitTypes = masterDetailsService.getMasterDetails("unitTypes");
    return unitTypes[id];
}
function __removeAgentContacts(response){
    if(response && response.length){
        response.forEach((saleType) => {
            saleType.forEach((agent) => {
                if(agent.contact){
                    delete agent.contact ; 
                }
            });
        });
    }
    return response;
}

function generateFilter(query) {
    let filters = [];
    _.forEach(config.optional, (v) => {
        if (query[v]) {
            let filter = _.cloneDeep(config.filters[v]);
            if (filter.parserType === 'unitType') {
                let _unitTypes = query[v].split(',');
                _unitTypes = _unitTypes.map(function(propertyType) {
                    return _getMappedUnitType(propertyType) || '';
                }).filter(propertyType => {
                    return propertyType;
                });
                filter.value = _unitTypes;

            } else if (filter.type === 'range' && query[v].indexOf(',') !== -1) {
                if (filter.key === "price") {
                    query[v] = query[v].split(',').map(function(i) {
                        return parseInt(i, 10);
                    }).join(",");
                }
                let range = query[v].split(',');
                if (range[0]) {
                    filter.from = range[0];
                }
                if (range[1]) {
                    filter.to = range[1];
                }
                filter.value = query[v].indexOf(',') === -1 ? query[v] : query[v].split(',');
            } else if (filter.value === 'integer') {
                filter.value = query[v].indexOf(',') === -1 ? parseInt(query[v], 10) : query[v].split(',').map(function(i) {
                    return parseInt(i, 10);
                });
            } else {
                filter.value = query[v].indexOf(',') === -1 ? query[v] : query[v].split(',');
            }
            filters.push(filter);
        }
    });

    return filters;
}

module.exports.apiHandler = function(req, res, next) {
    let cityId,
        query = req.query,
        filters = generateFilter(query),
        rows = query.rows ? query.rows : 20,
        promise = [],
        listingCategory,
        buyFilter,
        rentFilter,

        options = {
            selector: {
                filters,
                paging: {
                    rows: rows,
                    start: 0
                }
            },
            limit: rows,
            utmSource: query.utmSource,
            utmMedium: query.utmMedium
        };
    if (query.contactNumber) {
        options.contactNumber = query.contactNumber;
    }
    if (query.giveBoostToExpertDealMakers == 'true') {
        options.giveBoostToExpertDealMakers = true;
    }
    if (query.listingCategory) {
        listingCategory = query.listingCategory;
        if(query.listingCategory == "All"){
            query.listingCategory = config.keys.BUY_FILTER;
            buyFilter = generateFilter(query);
            query.listingCategory = config.keys.RENT_FILTER;
            rentFilter = generateFilter(query);
            query.listingCategory = "All";
        }
    }

    if (req.params.cityId) {
        cityId = req.params.cityId;
        if(query.listingCategory == "All"){
            options.selector.filters = buyFilter;
            promise.push(_makeTopAgentsCall(cityId, options, {req, query}));
            options.selector.filters = rentFilter;
            promise.push(_makeTopAgentsCall(cityId, options, {req, query}));
        } else {
            promise.push(_makeTopAgentsCall(cityId, options, {req, query}));
        }
        Promise.all(promise).then((response) => {
            response = __removeAgentContacts(response);
            if(response && response.length > 1){
                res.send({
                    buy: response[0],
                    rent: response[1]
                });
            } else {
                res.send(response[0]);
            }
        }).catch((err) => {
            next(err);
        });
    } else {
        next(utilService.getError(400));
    }
};

function _makeTopAgentsCall(cityId, options, {req, query}){
    let promise, topAgents;
    topAgents = promise = agentService.getTopAgents(cityId, options, { req });

    if (query.parser == "lead") {
        promise = projectService.parseTopAgentForLead(topAgents, { cityId, req });
    }
    return promise.then(response => {
        let topSellerResponse = response;
        if (response && response.length && query.sellerType == "dealMakers") {
            if(query.metaInfo){
                let selector = {
                    fields: config.topLocalityFields
                };

                return agentService.getAgentLocalities(response, {selector}, {req}).then((result) => {
                    return agentService.parseAgentsForTopSellerStatus(topSellerResponse, query, result);
                }, () => {
                    return agentService.parseAgentsForTopSellerStatus(topSellerResponse, query);
                });
            } else {
                return agentService.parseAgentsForTopSellerStatus(topSellerResponse, query);
            }
        } else {
            return response;
        }
    }, (err) => {
        throw err;
    });
}
