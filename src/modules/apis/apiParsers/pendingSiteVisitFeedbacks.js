'use strict';

const apiConfig = require('configs/apiConfig');
const apiService = require('services/apiService'); 
const utilService = require('services/utilService');
const globalConfig = require("configs/globalConfig");
const ONE_DAY = 24 * 60 * 60 * 1000;
const siteVisitparameters = {
    ONE_DAY_LATER_SITE_VISIT_ID: 6,
    ONE_WEEK_LATER_SITE_VISIT_ID: 7,
    TWO_WEEK_LATER_SITE_VISIT_ID: 8,
    NO_SITE_VISIT_ID: 5 
};

function _hasTimePassed(dataObj) {
    let currentdate = new Date(),
        currentTime = currentdate.getTime(),
        timePassed;
    if(!dataObj.callRatingParameterId) {
        return false;
    }

    switch(parseInt(dataObj.callRatingParameterId)) {
        case siteVisitparameters.ONE_DAY_LATER_SITE_VISIT_ID:
            timePassed = ONE_DAY; 
            break;
        case siteVisitparameters.ONE_WEEK_LATER_SITE_VISIT_ID:
            timePassed = 7 * ONE_DAY;
            break;
        case siteVisitparameters.TWO_WEEK_LATER_SITE_VISIT_ID:
            timePassed = 14 * ONE_DAY;
            break;
    }
    if( (currentTime - parseInt(dataObj.createdAt)) >= timePassed ) {
        return true;
    }
    return false;
}

function _parseCallFeedbackDetails (callFeedbackArr,req) {
    var urlConfig,
    finalData = {
        reqData : [],
        callFeedbackPromises : []
    };

    if(!callFeedbackArr.length) {
        return finalData; 
    } 


    for(var i = 0; i< callFeedbackArr.length; i++) {
        if(callFeedbackArr[i].rating >= 4 && [siteVisitparameters.ONE_DAY_LATER_SITE_VISIT_ID,siteVisitparameters.ONE_WEEK_LATER_SITE_VISIT_ID,siteVisitparameters.TWO_WEEK_LATER_SITE_VISIT_ID,].indexOf(callFeedbackArr[i].callRatingParameterId) > -1 ) {
            let obj = {};
            obj.callRatingParameterId = callFeedbackArr[i].callRatingParameterId;
            obj.contactedDate = utilService.formatDate(callFeedbackArr[i].createdAt, "dd MM YY");
            obj.createdAt = callFeedbackArr[i].callLog.createdAt;

            if(callFeedbackArr[i].callLog && callFeedbackArr[i].callLog.communicationLog) {
                obj.callId = callFeedbackArr[i].callLog.id;
                obj.callerUserId = callFeedbackArr[i].callLog.communicationLog.callerUserId;
                obj.phone = callFeedbackArr[i].callLog.communicationLog.callerNumber;

                if(callFeedbackArr[i].callLog.communicationLog.virtualNumberMapping) {
                    obj.companyUserId = callFeedbackArr[i].callLog.communicationLog.virtualNumberMapping.userId;
                    obj.salesType = callFeedbackArr[i].callLog.communicationLog.virtualNumberMapping.listingCategory;
                    obj.cityId = callFeedbackArr[i].callLog.communicationLog.virtualNumberMapping.cityId;
                    obj.uniquenessId = `${callFeedbackArr[i].callLog.communicationLog.callerUserId}:${callFeedbackArr[i].callLog.communicationLog.virtualNumberMapping.userId}`;
                }
                if(callFeedbackArr[i].callLog.communicationLog.extraData) {
                    obj.listingId = JSON.parse(callFeedbackArr[i].callLog.communicationLog.extraData).listingId;
                }
                let query = {
                    entityDataId: obj.callId,
                    userId: obj.callerUserId,
                    entityTypeId: globalConfig.siteVisitConfigParameters.entityTypeId,
                    feedbackTypeId: globalConfig.siteVisitConfigParameters.feedbackTypeId
                };
                urlConfig = apiConfig.getSiteVisitFeedbacks({query}); 
                finalData.callFeedbackPromises.push(apiService.get(urlConfig, {req}));
            }

            finalData.reqData.push(obj);
        } 
    }
    return finalData;
}

module.exports.apiHandler = function (req, res, next){
	let buyerId = req.params.buyerId;
	if(isNaN(buyerId)) {
		let error = new Error('input parameter not correct');
		error.status = 400;
        return next(error);
    } else{
        let days = 30,
            date = new Date(),
            last = new Date(date.getTime() - (days * 24 * 60 * 60 * 1000)),
            urlConfig,
            object =  {
                fields: ['rating','callLog','callLog.communicationLog.virtualNumberMapping','id','userId','cityId','callerUserId','extraData','createdAt','callDuration','callStatus','callerNumber','listingCategory','callRatingParameterId'],
                filters: []
            },
            selector;

            last = last.toISOString();
            object.filters.push({
                type: 'equal',
                key: 'callLog.communicationLog.callerUserId',
                value: buyerId,
                filter: 'and'
            });
            object.filters.push({
                type: 'ge',
                key: 'createdAt',
                value: last,
                filter: 'and'
            });
            selector = apiService.createFiqlSelector(object);
            urlConfig = apiConfig.callRating(
                {
                query: selector
            }).url;
    	apiService.get(urlConfig, {req}).then((response) => {
    		let result = response && response.data && response.data.results || [];
    		return _parseCallFeedbackDetails(result, req);
    	}, (err) => {
    		next(err);
    	}).then((result) => {
    		Promise.all(result.callFeedbackPromises).then((response) => {
                var i,totalSiteVisitFeedbackLeft = 0,
                     isEligibleFeedbackFound = false;
                for(i = 0;i < response.length; i++) {
                    if(!response[i].data.length) {
                        totalSiteVisitFeedbackLeft++;
                        if(_hasTimePassed(result.reqData[i])) {
                            isEligibleFeedbackFound = true;
                            break;
                        }
                    }
                }
                let reqObject = isEligibleFeedbackFound && result.reqData[i];
                if(reqObject) {
                    let companyDetailConfig = apiConfig.sellerDetailByUserId({sellerId: reqObject.companyUserId});
                    apiService.get(companyDetailConfig, {req}).then((v) => {
                        reqObject.companyName = v && v.data && v.data.company && v.data.company.name;
                        reqObject.companyId = v && v.data && v.data.company && v.data.company.id;
                        reqObject.totalSiteVisitFeedbackLeft = totalSiteVisitFeedbackLeft;
                        res.send(reqObject);
                     });
                } else {
                    res.send({});
                }
    		});
    	}).catch((err) => {
    		next(err);
    	});
    }
};
