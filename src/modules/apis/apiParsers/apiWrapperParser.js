'use strict';
const apiService = require('services/apiService');

module.exports.apiHandler = function(req, res, next){
	let url = req.params[0];
	if(url){
		return apiService.get({url}, {req}).then(response => {
            res.status(200).send(response);
        }, err => {
            next(err);
        }).catch(err => {
            next(err);
        });
	} else {
        next(new Error("no url provided for call"));
    }
};