"use strict";

const apiService = require('services/apiService'),
logger = require('services/loggerService'),
apis = require('configs/apiConfig'),
selectorService = require('services/serpSelectorService');

let labelMap = {
    'builder' : {
        label: 'name',
        value: 'id'
    },
    'locality' : {
        label: 'label',
        value: 'localityId'
    }
};

module.exports.apiHandler = function(req, res) {
    let type = req.query.type,
    fields = labelMap[type],
    queryParams = {
        fields: fields.label+","+fields.value,
        city: req.query.city
    };

    let apiParam = type === 'locality' ? apis.locality() : apis.topBuilder();
    apiParam.qs = selectorService.getSelectorQuery(queryParams);

    apiService.get(apiParam).then((response) => {
        res.status(200).send(dataParser(type, response));
    }, (error) => {
        logger.error('Error while getting '+type+' from '+type+' API : ' + JSON.stringify(error));
        res.status(500).send({message: error.message});
    });
};

function dataParser(type, response){
	let data = [], i;
	for(i in response.data){
		data[i]={
			'label': response.data[i][labelMap[type].label],
			'value': response.data[i][labelMap[type].value]
		};
	}
	return data;
}
