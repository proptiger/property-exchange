"use strict";
const sellersRatingReviewsService = require('services/sellersRatingReviewsService')

module.exports.apiHandler = function(req, res, next) {
    let companyUserId = req.params && req.params.companyUserId;
    sellersRatingReviewsService.getSellerReviews(companyUserId, {req}).then(response => {
        return res.send(response);
    }).catch(err => {
        return res.send({});
    });
};
