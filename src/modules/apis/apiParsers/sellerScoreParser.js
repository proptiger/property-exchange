"use strict";

const   sellersRatingReviewsService = require('services/sellersRatingReviewsService'),
    apiService = require('services/apiService'),
    apiConfig = require('configs/apiConfig');;

module.exports.apiHandler = function(req, res, next) {

    let companyUserId = req.params.companyUserId;
    let urlConfig = apiConfig.getSellerScoreBreakUp({companyUserId});

    apiService.get(urlConfig, {req}).then(response => {
        let data = sellersRatingReviewsService.parseSellerScoreDetail(response.data);
        return res.send({
            statusCode: '2XX',
            data
        });
    }, (err) => {
        next(err);
    })
};
