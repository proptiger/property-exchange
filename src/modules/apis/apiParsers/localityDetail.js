"use strict";

const localityService = require('services/localityService'),
    _ = require('services/utilService')._;

const config = {
    parser: {
        'trend': localityService.getParsedDataWithPriceTrendUrl
    }
};

module.exports.apiHandler = function(req, res, next) {
    let parser = req.query.parser;
    if (req.params.localityId) {
        localityService.getLocalityOverview(req.params.localityId, req, {
            fields: ["label",
                "rentUrl",
                "buyUrl",
                "overviewUrl",
                "listingCountBuy",
                "listingCountRent",
                "listingAggregations",
                "listingCategory",
                "avgPricePerUnitArea",
                "avgPrice",
                "count",
                "localityId",
                "avgPriceRisePercentage",
                "avgPriceRisePercentage",
                "avgPriceRisePercentageApartment",
                "avgPriceRisePercentagePlot",
                "avgPriceRisePercentageVilla",
                "avgPriceRiseMonths",
                "avgPricePerUnitArea",
                "avgPricePerUnitArea",
                "avgPricePerUnitArea",
                "avgPricePerUnitArea",
                "avgPricePerUnitAreaApartment",
                "avgPricePerUnitAreaPlot",
                "avgPricePerUnitAreaVilla",
                "priceRiseRank",
                "priceRiseRankPercentage"
            ]
        }).then(response => {
            if (parser && config.parser[parser]) {
                config.parser[parser]([response], {
                    templates: {
                        buy: 'MAKAAN_LOCALITY_PRICE_TREND_BUY',
                        rent: 'MAKAAN_LOCALITY_PRICE_TREND_RENT'
                    }
                }, req).then((response) => {
                    response = response && response.data;
                    res.send(response[0] || {});
                }, (err) => {
                    next(err);
                });
            } else {
                res.send(_.assignIn(response, localityService.averagePricesFromAggrigation(response.listingAggregations, {req})));
            }
        }, err => {
            next(err);
        });

    } else {
        next(new Error("provide relevant parameters"));
    }
};
