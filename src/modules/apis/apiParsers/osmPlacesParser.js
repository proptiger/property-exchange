"use strict";

const apiService = require('services/apiService'),
apiConfig = require('configs/apiConfig');

function getPlaceDetails(req, res, next) {
    let placeId = req.query.placeId, placeType;
    if(req.query.placeType && req.query.placeType.toUpperCase) {
        placeType = req.query.placeType.toUpperCase()[0];
    }
    delete req.query.placeId;
    delete req.query.placeType;
    apiService.get(apiConfig.getPlaceDetailsFromOSM({placeId,placeType}), {req: req, 
        baseUrl: process.env.OSM_GEOCODER_URL,
        skipBaseUrl: true
    }).then((response) => {
        response = response || {};
        res.send(response);
    }, err => {
        next(err);
    }).catch(err => {
        next(err);
    });
}

function getSuggestedPlaces(req, res, next) {
    let query = req.query.query;
    delete req.query.query;
    apiService.get(apiConfig.getPlacesFromOSM({query}), {req: req, 
        baseUrl: process.env.OSM_GEOCODER_URL,
        skipBaseUrl: true
    }).then((response) => {
        response = response || {};
        res.send(response);
    }, err => {
        next(err);
    }).catch(err => {
        next(err);
    });
}
module.exports.apiHandler = function(req, res, next) {
    if(req.query.query) {
        return getSuggestedPlaces(req, res, next);
    } else if(req.query.placeId && req.query.placeType) {
        return getPlaceDetails(req, res, next);
    } else {
        res.send(400);
    }
};
