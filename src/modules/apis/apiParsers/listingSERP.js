'use strict';

let listingsService = require("services/listingsService"),
	seoService = require("services/seoService"),
	urlDataService = require('services/urlDataService'),
	utilFunctions = require('services/utilService');

let SEARCH_TYPE_ENUM = {1:"buy",2:"rent",3:"commercialBuy",4:"commercialLease"};

module.exports.apiHandler = function(req, res, next){
	if(req.query.serpURL){
		seoService.getSeoTags(req.query.serpURL, req, res).then((result)=>{
			let params={}, tempParams;

			if(req.query.serpParams){
				tempParams = '?'+req.query.serpParams.replace(/@@@@/g,'&')
				params = utilFunctions.getAllQueryStringParam(tempParams)
			}

			if(req.query && req.query.searchType){
				params.buyOrRent = SEARCH_TYPE_ENUM[req.query.searchType];
			}

			let urlStatus = result.data.urlStatus;
			let pageType = urlStatus.masterUrlType || urlStatus.pageType;
                pageType = pageType.toUpperCase();
			let urlDetail = urlDataService.getUrlData(pageType, urlStatus.urlDetail, req);
			listingsService.getApiListings(params,urlDetail,{req: req})
				.then((result) => {
					let data ={};
					if(result && result.response && result.response.data){
						if(req.query.onlyListingCount){	
							data = {
								totalCount: result.response.data[0].totalCount,
								isOriginalSearchResult: result.response.data[0].isOriginalSearchResult
							}
						} else {
							data=result
						}
					} 
					res.send(data)
			    }, err => {
			        next(err);
			    }).catch(err => {
			        next(err);
			    });				
		})

	} else if(req.query.serpUrl == "true"){
		listingsService.getApiListings(req.query, {"pageType": req.query.pageType,
			"serpType": 'listing', "listingType": req.query.listingType}, {req: req})
			.then((result) => {
					let data ={};
					if(result && result.response && result.response.data){
						data = {
							totalCount: result.response.data[0].totalCount,
							isOriginalSearchResult: result.response.data[0].isOriginalSearchResult
						}
					} 
					res.send(data)
			    }, err => {
			        next(err);
			    }).catch(err => {
			        next(err);
			    });
	} else {
		next();
	}
};
