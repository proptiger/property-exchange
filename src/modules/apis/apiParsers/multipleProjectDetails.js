"use strict";
const  projectService = require('services/projectService'),
    utilService = require("services/utilService"),
    _ = utilService._;

function _errorHandler(next){
    let error = new Error("Please provide relevant parameters");
    error.status = 400;
    next(error);
}
module.exports.apiHandler = function(req, res, next) {
    let projectIdList;
    try{
        projectIdList = JSON.parse(req.query.projectIdList);
    } catch(e){
        return _errorHandler(next);
    }

    let projectPromise = [];
    _.forEach(projectIdList, (v) => {
        projectPromise.push(projectService.getProjectDetailsWithoutImage(v, {req}));
    });
    Promise.all(projectPromise).then((result) => {
        let finalData = {};
        _.forEach(result, (v) => {
            if(v){
                let minPrice = utilService.formatNumber(v.minResaleOrPrimaryPrice, { precision : 2, returnSeperate : true, seperator : (req.locals && req.locals.numberFormat) }),
                maxPrice = utilService.formatNumber(v.maxResaleOrPrimaryPrice, { precision : 2, returnSeperate : true, seperator : (req.locals && req.locals.numberFormat) }),
                price = "";

                if(minPrice && minPrice.val){
                    price += minPrice.val + minPrice.unit;
                }
                if(maxPrice && maxPrice.val){
                    price += " - " + maxPrice.val + maxPrice.unit;
                }

                finalData[v.projectId] = {
                    url: v.URL ,
                    title: v.builder.displayName + ' ' + v.name, //builder name + project name
                    locality: v.locality.label,
                    imageUrl: v.imageURL + '?width=60&height=60',
                    price: price
                };
            }
        });
        req.noCache = true;
        res.send(finalData);
    }, (err) => {
        next(err);
    }).catch((err) => {
        next(err);
    });     
};
