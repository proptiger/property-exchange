"use strict";
const failedEnquiryPushService = require('services/failedEnquiryPushService');

module.exports.apiHandler = function(req, res, next) {
    if(!req.body || !req.body.payload) {
        return res.send("empty body");
    }
    failedEnquiryPushService.push({
        payload: req.body.payload,
        api: req.body.api
    }).then(() => {
        res.send("success");
    } ,(err) => {
        next(err);
    });
};
