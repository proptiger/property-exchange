"use strict";

  const  projectService = require('services/projectService');

const config = {
    parser: {
        'similar': projectService.parseSimilarProjects
    }
};
module.exports.apiHandler = function(req, res, next) {
    let projectId = req.params.projectId;
    let parser = req.query.parser;
    
    projectService.getProjectDetailsWithoutImage(projectId, {req}).then(response => {
        if (parser && config.parser[parser]) {
            res.send(config.parser[parser]([response], undefined, req.locals)[0] || {});
        } else {
            res.send(response);
        }
    }, err => {
        next(err);
    }).catch(err => {
        next(err);
    });
};
