"use strict";
const listingService = require('services/listingsService'),
    utilService = require('services/utilService');

var config = {
    groupCount: 20,
    groupCountMobile: 10,
    buy: ['Primary', 'Resale'],
    rent: ['Rental'],
    listingTypes: ['buy', 'rent']
};
module.exports.apiHandler = function(req, res, next) {
    let params = req.params,
        query = req.query,
        groupCount = utilService.isMobileRequest(req) ? config.groupCountMobile : config.groupCount;

    if (query && query.min && query.max && query.max > query.min) {
        let min = parseInt(query.min),
            max = parseInt(query.max),
            gap = parseInt((max - min) / groupCount),
            otherFilters = {};
        if (query.bhk) {
            otherFilters.bedrooms = query.bhk.split(",");
            let index = otherFilters.bedrooms.indexOf("3plus");
            if (index > 0) {
                otherFilters.bedrooms.splice(index, 1);
                Array.prototype.push.apply(otherFilters.bedrooms, ["4", "5", "6", "7", "8", "9", "10"]);
            }
        }
        if (query.propertyType) {
            otherFilters.unitType = query.propertyType.split(",");
        }
        if (query.listingType) {
            let listingTypes = query.listingType.split(",");
            otherFilters.listingCategory = [];
            listingTypes.forEach(function(type) {
                if (config.listingTypes.indexOf(type) !== -1) {
                    otherFilters.listingCategory = otherFilters.listingCategory.concat(config[type]);
                }
            });
            if (otherFilters.listingCategory.length === 0) {
                delete otherFilters.listingCategory;
            }
        }
        listingService.getListingInPriceRange(params.cityId, min, max, gap, otherFilters, {req}).then((response) => {
            let ratio = response.length / groupCount;
            let offsetCount = parseInt(response.length) / ratio;
              //  offset = offsetCount * gap;
            for (let i = 1; i <= offsetCount; i++) {
                response.unshift({
                    startPoint: (min - i * gap).toString(),
                    propertyCount: null
                });
            }
            for (let i = 1; i <= offsetCount; i++) {
                response.push({
                    startPoint: (max + i * gap).toString(),
                    propertyCount: null
                });
            }
            res.send(response);
        }, err => {
            next(err);
        }).catch(err => {
            next(err);
        });
    } else {
        next(utilService.getError(400));
    }

};