"use strict";
const buyerDashboardParser = require('modules/apis/apiParsers/buyerDashboardParser'),
      serpSelectorService = require('services/serpSelectorService'),
      utils = require('services/utilService'),
      _ = utils._;

module.exports.apiHandler = function(req, res, next) {
    let query = req.body && req.body.query || "",
        pageData = req.body && req.body.pageData;
    query = utils.getAllQueryStringParam(query) || {};
    _.merge(req,{
        query,
        urlDetail: pageData
    });
    let selector = serpSelectorService.getSelectorQuery(req.query, req.urlDetail);
    req.body = {
        searchQuery: selector.selector,
        name: req.query.name
    };
    buyerDashboardParser.postRequest(req, res, next);
};

