"use strict";
const apiService = require('services/apiService');
const apiConfig = require('configs/apiConfig');

function generatePostData(req){
    let postData={};
    if(req.body && req.body.isAmpLead){
        postData.id = req.body.userId;
            if(req.body.email){
                postData.emails = [{"email":req.body.email,"priority":1}];
            }
            if(req.body.name){
                postData.fullName = req.body.name;
            }
    }else{
        postData = req.body;
    }
    return postData;
}

module.exports.apiHandler = function(req, res, next) {
    let requestMethod = req.method ||'';
    let urlConfig;
    switch(requestMethod){
        case 'GET': 

            let selector = JSON.stringify({"fields": ["id","domainId","email","fullName","firstName","lastName","countryId","contactNumber","contactNumbers","email","priority","isVerified","emails"]});
            if(req.query.userId){
                let userId = req.query.userId;
                urlConfig = apiConfig.getUserDetailsFromUserId({userId, selector});
            }else if(req.query.contactNumber){
                let contactNumber = req.query.contactNumber;
                urlConfig = apiConfig.getUsersFromContactNumber({contactNumber});
            }else{
                next(new Error("no userId or ContactNumber found"));
            }

            return apiService.get(urlConfig, {req}).then(response => {
                if(response && response.data && response.data.length){
                    response.data[0].contactNumbers.sort((a, b) => {
                        return a.priority - b.priority;
                    });
                    res.send(response.data[0]);
                } else {
                    next (new Error ());
                }
            },(err)=>{
                next(err);
            });
            break;

        case 'POST':
        
            let postData = generatePostData(req);
            urlConfig = apiConfig.postUserDetails();
            if(!(req.body && req.body.isAmpLead) || (postData.emails && postData.fullName)){
                apiService.post(urlConfig, {
                    body: postData
                }).then((response)=>{
                   response.data = response.data || {};
                   response.data.screenToShow = req.body && req.body.showCouponScreen ? 'couponScreen':'endScreen';
                   res.send(response.data);
                },(err)=>{
                    if(req.body && req.body.isAmpLead){
                        res.send({screenToShow: req.body && req.body.showCouponScreen ? 'couponScreen':'endScreen'});
                    }else{
                        next(err);
                    }
                });
            }else{
                next(new Error ());
            }
            break;
    }
    
};
