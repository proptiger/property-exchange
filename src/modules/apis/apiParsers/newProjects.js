"use strict";
const projectService = require('services/projectService');

module.exports.apiHandler = function(req, res, next) {
    var selector = {
    	"fields":[
    		"projectId",
	    	"name",
	    	"completeProjectAddress",
	    	"builder",
	    	"mainImage",
			"absolutePath",
			"altText",
			"title",
			"maxPrice",
			"minPrice",
			"overviewUrl"
		],
		"filters":{
			"and":[{
				"equal":{
					"builderDbStatus":
					[
						"Active",
						"ActiveInMakaan"
					]
				}
			},{
				"equal":{
					"cityId":req.params.cityId
				}
			},{
				"equal":{
					"projectStatus":
						[
							"not launched",
							"pre launch"
						]
				}
			}
			]
		},
		"paging":{
			"start":0,
			"rows": (req.query&&req.query.rows)?req.query.rows:6
		}
	};
    var config = { selector : JSON.stringify(selector)};
    projectService.getProjectSerpWithFilters(config, { req }).then((response) => {
        response = response || {};
        res.send(response);
    }, err => {
        next(err);
    }).catch(err => {
        next(err);
    });
};