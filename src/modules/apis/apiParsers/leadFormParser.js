'use strict';

let utilService = require("services/utilService");
let masterDetailsService = require('services/masterDetailsService');
let enquiryService = require('services/enquiryService');
let globalConfig = require('configs/globalConfig');
let loggerService = require('services/loggerService');
let _ = utilService._;
let couponDetailParser = require('modules/apis/apiParsers/couponDetails');
let deficitProjectParser = require('modules/apis/apiParsers/deficitSellerProjectsParser');

function _getMappedUnitType(id) {
    let unitTypes = masterDetailsService.getMasterDetails("unitTypes");
    return unitTypes[id];
}

const leadDataConfig = {
	propertyTypes: {
		key: "propertyTypes",
		value: function(req, originalValue){
			if(!_.isArray(originalValue)){
				originalValue = [originalValue];
			}
			return originalValue.map((v) => {
				return _getMappedUnitType(v.toString());
			});
		}
	},
	companyId: {
		key: "multipleCompanyIds",
		value: function(req, originalValue){
			return [originalValue];
		}
	},
	jsonDump: {
		key: "jsonDump",
		value: function(req){
			let jsonDump = {userAgent: req.headers['user-agent']};
			return JSON.stringify(jsonDump);
		}
	},
	salesType: {
		key: "salesType",
		value: function(req, originalValue=""){
			originalValue = originalValue && originalValue.toLowerCase();
			if(["primary", "resale", "buy"].indexOf(originalValue) !== -1){
				return "buy";
			} else if(["rental", "rent"].indexOf(originalValue) !== -1){
				return "rent";
			}
		}
	},
	pass: ["cityId", "cityName", "minBudget", "maxBudget", "projectId", "bhk", "listingId", "pageName", "pageUrl", "multipleCompanyIds","multipleSellerIds", "localityIds", "phone", "countryId", "email", "name", "enquiryType"],
	requiredFields: ["cityId", "phone", "countryId", "enquiryType", "multipleCompanyIds"],
	dummyFields: {
		email: function(rawData){
			return `email_${rawData.phone}@email.com`;
		},
		name: function(rawData){
			return `name_${rawData.phone}`;
		}
	}
};

function generateSelectCoupon(response,req,res,next){
	let data = {
        cityId :req.body.cityId,
        entityId : 0,
        entityTypeId : 1,//denoting coupon is generated for rent
        generatorUserId : response && response.data.userId
    };
    let couponRequest = Object.assign({}, req);
    couponRequest.body = data;
    return couponDetailParser.generateCoupon(couponRequest,res,next).then((couponResponse) => {
    	response.data.couponDetails = couponResponse;
    	response.data.screenToShow = req.body && req.body.showNameEmailScreen ? "nameEmailScreen" : "couponScreen";
    	response.data.showCouponScreen = true;
		return res.send(response);
	}).catch(() => {
		response.data.screenToShow = req.body && req.body.showNameEmailScreen ? "nameEmailScreen" : "endScreen";
		return res.send(response);
	});
}

function _parseDataForLeadForm(rawData, req){
	let finalData = {};
	_.forEach(rawData, (v, k) => {
		if(leadDataConfig.pass.indexOf(k) > -1 && v){
			finalData[k] = v;
		} else{
			let key = leadDataConfig[k] && leadDataConfig[k].key;
			if(!key){
				return;
			}
			let value = leadDataConfig[k] && leadDataConfig[k].value || v;
			if(typeof value == "function"){
				value = value(req, v);
			}
			finalData[key] = value;
		}
	});
	finalData.domainId = globalConfig.makaanDomainId;
	finalData.applicationType = "Desktop Site";
	for(let key in leadDataConfig.requiredFields){
		let requiredKey = leadDataConfig.requiredFields[key];
		if(!finalData[requiredKey]){
			return false;
		}
	}
	for(let key in leadDataConfig.dummyFields){
		let dummyFn = leadDataConfig.dummyFields[key];
		if(finalData[key]){
			continue;
		} else if(typeof dummyFn == "function") {
			finalData[key] = dummyFn(rawData);
		}
	}
	return finalData;
}

function _getVerifiedStatus({contacted}, {phone}){
	if(contacted){
		return contacted.includes(phone);
	}
	return false;
}

function _getDeficitQueryObj(data){
	let deficitQueryObj = {};
    deficitQueryObj.budget = data.maxBudget == data.minBudget ? `${data.minBudget},` : `${data.minBudget},${data.maxBudget}`;
    deficitQueryObj.localityIds = data.localityIds && data.localityIds.toString();
    deficitQueryObj.cityId = data.cityId && data.cityId.toString();
    deficitQueryObj.bhk = data.bhk && data.bhk.toString();
    deficitQueryObj.listingCategory = "Primary";
    deficitQueryObj.listingSellerTransactionStatuses = "BUILDER_EXCLUSIVE_CAMPAIGN_SELLER,CAMPAIGN_SELLER";
    deficitQueryObj.leadDeficitScore = "5,";
    deficitQueryObj.projectDbStatus="Active,ActiveInMakaan";
    deficitQueryObj.rows = 10;
    deficitQueryObj.includeNearby = true;
    return deficitQueryObj;
}

module.exports.apiHandler = function (req, res, next){
	let action = req.params.action;
	if(req.isAmp){
		//optIn checkbox returns 'on' or 'off', converting it to required true & false value
		req.body.optIn = ((req.body.optIn && req.body.optIn==="on") ? true : false);
	}
	switch(action){
		case "submit":
			_submitEnquiry(req, res, next);
			break;
		case "verify":
			_verifyOtp(req, res, next);
			break;
		case "resend-otp":
			_resendOtp(req, res, next);
			break;
		case "reset": // only for testing purposes
			res.cookie("contacted", "").send({});
			break;
		case "otp-on-call":
			_sendOtpOnCall(req, res, next);
			break;
	}
};

function _sendOtpOnCall(req, res, next){
	enquiryService.sendOtpOnCall({
		userId: req.body.userId,
		phone: req.body.phone
	}, req).then((response) => {
		return res.send(response);
	}).catch((err) => {
		next(err);
	});
}

function _setVerifiedCookie(res, phone, {contacted = ""}){
    if(!contacted.includes(phone)){
        contacted += `,${phone}`;
    }
    res.cookie("contacted", contacted, {
        maxAge: 1000 * 60 * 60 * 24 * 30
    });
}

function _resendOtp(req, res, next){
	let putData = {
		userId: req.body.userId,
		phoneToVerify: req.body.phone
	};
	enquiryService.resendAndVerifyOtp({putData, enquiryId: req.body.userEnquiryId}, req, res).then((response) => {
		return res.send(response);
	}).catch((err) => {
		next(err);
	});
}

function _verifyOtp(req, res, next){
	let cookie = req.headers.cookie;
	cookie = utilService.parseCookies(cookie);
	var enquiryId = req.body.userEnquiryId,
	    otp = req.body.otp,
	    putData = {};
	if (req.body.userId) {
	    putData.userId = req.body.userId;
	}
	putData.phone = req.body.phone;

	enquiryService.resendAndVerifyOtp({putData, enquiryId, otp}, req, res).then((response) => {
		if(req.body.mProfileRedirectUrl){
			response.data.mProfileRedirectUrl = req.body.mProfileRedirectUrl + '&uid='+response.data.userId ;
		}
		if(response && response.data && response.data.otpValidationSuccess){
			_setVerifiedCookie(res, response.data.phone, cookie);
			response.data.showNameEmailScreen = true;
			response.data.screenToShow = "nameEmailScreen";
			return res.status(200).send(response);
		}else{
			response.data.screenToShow = "otpScreen";
			return res.status(200).send(response);
		}
	}).catch(err => {
	    loggerService.info("payload--:",putData);
	    next(err);
	});
}

function _submitEnquiry(req, res, next){
	let cookie = req.headers.cookie;
	let body = req.body
	if(!utilService.validatePhone(body.phone.toString(), (req.body.countryId===1?'india':null))){
		return res.status(400).json({isValidPhone:false,error:"Please provide a valid Phone No."});
	}
	cookie = utilService.parseCookies(cookie);
	let isVerified = _getVerifiedStatus(cookie, body);
	let leadData = _parseDataForLeadForm(body, req);
	if(!leadData){
		return next(utilService.getError(400));
	}
	if(isVerified){
		//_post main Enquiry
		leadData.sendOtp = false;
		enquiryService.postEnquiry(leadData, req, res).then((response) => {
			if(req.isAmp && req.body.mProfileRedirectUrl){
				response.data.mProfileRedirectUrl = req.body.mProfileRedirectUrl + '&uid='+response.data.userId ;
			}
			response.data.screenToShow = req.body && req.body.showNameEmailScreen ? "nameEmailScreen" : "endScreen";
			response.data.showNameEmailScreen = false;
			if(req.isAmp && req.body && req.body.isMakaanSelectSeller){
				if(response.data && response.data.couponDetails){
					response.data.couponDetails.displayCode = response.data.couponDetails.code && utilService.getMaskedCode(response.data.couponDetails.code,4);
					response.data.screenToShow = req.body && req.body.showNameEmailScreen ? "nameEmailScreen" : "couponScreen";
    				response.data.showCouponScreen = true;
					return res.send(response);
				}
				return generateSelectCoupon(response,req,res,next);
			}else if(req.isAmp && req.body && req.body.salesType=="buy"){
				let deficitQueryObj = _getDeficitQueryObj(req.body);
	            let deficitRequest = Object.assign({}, req);
	            deficitRequest.query = deficitQueryObj;
	            return deficitProjectParser.getAmpleadDeficitProjects(deficitRequest, res, next).then(function(deficitResponse) {
	                response.data.deficitProject = deficitResponse &&  deficitResponse.deficitProject||{};
	                return res.send(response);
	            }, function (){
	                return res.send(response);
	            });
			}else{
				return res.send(response);
			}
		}).catch((err) => {
			next(err);
		});
	} else {
		//_post temp Enquiry
		leadData.sendOtp = true;
		enquiryService.postTempEnquiry(leadData, req, res).then((response) => {
			if(req.isAmp && req.body.mProfileRedirectUrl){
				response.data.mProfileRedirectUrl = req.body.mProfileRedirectUrl + '&uid='+response.data.userId ;
				response.data.showNameEmailScreen = true;
				response.data.screenToShow = "otpScreen";
			}
			return res.send(response);
		}, (err) => {
			next(err);
		});
	}
}