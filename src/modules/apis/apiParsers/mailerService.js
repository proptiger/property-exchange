"use strict";

  const  mailerService = require('services/mailerService'),
         homeloanService = require ('services/homeloanService');

module.exports.apiHandler = function(req, res, next) {
    var { name ,email , phone ,city ,subject , message, branch }= req.body;
    var postData={
        countryCode:req.body.countryCode,
        phoneNumber:phone,
        email:email,
        name:name,
        city:city,
        branch:branch
    }

    let partnerWithMakaan = req.query.partnerWithMakaan;
    let homeloanAgent = req.query.homeloanAgent;
    if(partnerWithMakaan){
        return mailerService.partnerWithMakaanEnquiry({name, email, phone, message}, {req}).then((response) => {
            return res.send(response);
        }).catch((err) => {
            next(err);
        });
    }else if(homeloanAgent){
        let sendMailPromise = mailerService.homeloanAgent({name, email, phone,city,branch}, {req});
        let postDataPromise = homeloanService.postHomeloanAgentDetails(postData, {req});
        return Promise.all([sendMailPromise,postDataPromise]).then((response) => {
            return res.send(response);
        }).catch((err) => {
            next(err);
        });

    }else {
        mailerService.sendMessage({
            name,
            city,
            email,
            phone,
            subject,
            message
        }, {
            req
        }).then(response => {
            res.status(200).send(response);
        }, err => {
            next(err);
        });
    }
};
