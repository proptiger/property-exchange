"use strict";
const apiService = require('services/apiService');
const apiConfig = require('configs/apiConfig');
const EMAIL_ID_USER = process.env.ASK_QUESTION_EMAIL_USER_ID;

module.exports.apiHandler = function (req, res, next) {
    let body = {
        "notificationType": "makaan_ask_a_question",
        "users": [{ "id": EMAIL_ID_USER }],
        "mediumDetails": [{ "mediumType": "Email" }]
    };
    let url = apiConfig.postCustomerQuestion();
    let requestMethod = req.method || '';
    switch (requestMethod) {
        case 'POST':
            body["payloadMap"] = {
                "dateTime": req.body.currentDate,
                "listingId": req.body.listingId,
                "question": req.body.question,
                "mobile": req.body.phoneNumber
            }
            apiService.post(url, {req, res, body}).then((response) => {
                res.send(response.data);
            }, (err) => {
                next(err);
            });
            break;
        default:
            next(new Error("Method not supported"));
    }

};
