"use strict";

const   apiConfig      = require('configs/apiConfig'),
        apiService     = require('services/apiService'),
        projectService = require('services/projectService'),
        builderProducts = ['featuredDeveloper','featuredNationalProjects'],
        utils = require('services/utilService'),
        _isEmptyObject = utils.isEmptyObject;


function parseLocation(location){
    if(location){
        location = Array.isArray(location) ? location.filter(item => item != "undefined") : location.toString().split(',');
    } 
    return location;
}

module.exports.apiHandler = function(req, res, next) {
// variables used for fetching the details of all developer ads and for fetching url for fallback
    let cityId = req.query.cityId && JSON.parse(req.query.cityId),
        cityName = req.query.label||'',  
        suburbId = req.query.suburbId && JSON.parse(req.query.suburbId),
        localityId = req.query.localityId && JSON.parse(req.query.localityId),
        projectCount = req.query.rows || 20,
        productType = req.params.productType || '',
        saleType = req.query.saleType||'',
        pageSource = req.query.pageSource || '';

    let fetchUrlsPromise, developerEntityPromise,
        urlParamObj = {}, urlObj;

    localityId = parseLocation(localityId);
    suburbId = parseLocation(suburbId);
    cityId = parseLocation(cityId);
    

    if(suburbId && suburbId.length){
        urlParamObj.urlDomain = "suburb";
        urlParamObj.domainIds = suburbId;
        urlParamObj.urlCategoryName = "MAKAAN_SUBURB_PROJECTS_SERP_BUY";
    } else if (localityId && localityId.length){
        urlParamObj.urlDomain = "locality";
        urlParamObj.domainIds = localityId;
        urlParamObj.urlCategoryName = "MAKAAN_LOCALITY_PROJECTS_SERP_BUY";
    } else if(cityId && cityId.length){
        urlParamObj.urlDomain = "city";
        urlParamObj.domainIds = cityId;
        urlParamObj.urlCategoryName = "MAKAAN_CITY_PROJECTS_SERP_BUY";
    }
    if(!_isEmptyObject(urlParamObj)){
        urlObj = apiConfig.fetchUrls(JSON.stringify([urlParamObj]));
        fetchUrlsPromise = apiService.get(urlObj, {req}); // returns the url for fallback CTA
    
        if(builderProducts.indexOf(productType) == -1){
            developerEntityPromise = projectService.getDeveloperProjects({ // return the developer ads details
                cityId,
                suburbId,
                localityId,
                projectCount,
                productType,
                saleType,
                pageSource,
                cityName
            }, {req});
        }else{
            developerEntityPromise = projectService.getFeaturedDeveloper({
                cityId,
                productType,
                saleType,
                pageSource,
                cityName
            }, {req});
        }

        Promise.all([developerEntityPromise, fetchUrlsPromise]).then((results)=>{ 

            let projectsUrl = results[1] && results[1].data && results[1].data[urlParamObj.urlCategoryName] && results[1].data[urlParamObj.urlCategoryName][urlParamObj.domainIds[0]];

            let response = results[0];
            response.projectsUrl = projectsUrl;
            return res.send(response);
        }, (err) => {
            next(err);
        });
    } else {
        next(new Error("no location found for getting developer projects"));
    }
};
