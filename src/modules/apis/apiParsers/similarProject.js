"use strict";
const logger = require('services/loggerService'),
    projectService = require('services/projectService');

module.exports.apiHandler = function(req, res, next) {
    logger.info('entered similar project route');
    let count = (req.query && req.query.count) || 5;
    let projectId = req.params.projectId;
    //TODO- XHR request
    //req.locals = req.locals || {};
    //req.locals.numberFormat = 3;
    projectService.getSimilarProjectAPIData(projectId, count, {req}).then(response => {
        res.send(response);
    }, err => {
        next(err);
    }).catch(err => {
        next(err);
    });

};
