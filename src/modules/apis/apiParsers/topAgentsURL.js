'use strict';

const   apiConfig      = require('configs/apiConfig'),
        apiService     = require('services/apiService');

module.exports.apiHandler = function(req, res, next){
	if(req.query.entityType && req.query.entityValue){
		let entityType = req.query.entityType,
			entityValue=req.query.entityValue,
			entityTypeLowerCase = req.query.entityType.toLowerCase();
		let urlParams = `[{"urlDomain":${entityTypeLowerCase},"domainIds":[${entityValue}],"urlCategoryName":"MAKAAN_${entityType}_TOP_BROKERS"}]&sourceDomain=Makaan`
		apiService.get(apiConfig.getEntityUrl({urlParams: urlParams}),{req}).then((result)=>{
			res.send(result)
		},(error)=>{
			next(error)
		}).catch(error=>{
			next(error)
		})
	} else {
		next()
	}
};