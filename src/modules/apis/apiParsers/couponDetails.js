"use strict";
const apiService = require('services/apiService'),
      apiConfig = require('configs/apiConfig'),
      utils = require('services/utilService'),
      mappingService = require('services/mappingService'),
      agentService = require('services/agentService');

const CouponStatus = mappingService.getMakaanSelectCouponStatus();

function _shareCouponwithSeller(req,res,next){
    let postData = req.body;
    let urlConfig = apiConfig.shareCouponDetails(req.query.userId);
    apiService.post(urlConfig, {
        body: postData
    }).then((response)=>{
       res.send(response.data);
    },(err)=>{
        next(err);
    });
}

function _getCouponShareStatus(couponShareData){
    let couponShareIds = [];
    couponShareData.forEach(function(couponShare){
        couponShareIds.push(couponShare.statusId);
    });
    return couponShareIds;
}

function getCouponRedeemerUserId(couponShareData){
    let redeemerUserId = '';
    let latestShareTimeStamp =0;
    couponShareData.forEach(function(element) {
      if(element.updatedAt >latestShareTimeStamp){
        latestShareTimeStamp = element.updatedAt;
        redeemerUserId = element.redeemerUserId;
      }
    });
    return redeemerUserId;
}
function _parseCouponDetails(couponData){
    let coupons={underProcess:[],active:[]},couponInfo={};
    couponData && couponData.forEach(function(coupon){
        couponInfo = {};
        couponInfo.couponId = coupon.id;
        couponInfo.code = coupon.code;
        couponInfo.displayCode = couponInfo.code && utils.getMaskedCode(couponInfo.code,4);
        couponInfo.amount = coupon.amount;
        couponInfo.createdAt = coupon.createdAt; 
        couponInfo.shareIds = coupon.couponShares && coupon.couponShares.length && _getCouponShareStatus(coupon.couponShares)||[];
        couponInfo.redeemerUserId = coupon.couponShares && coupon.couponShares.length && getCouponRedeemerUserId(coupon.couponShares)||'';
        couponInfo.redeemerDetails = {};
        if(couponInfo.shareIds.indexOf(CouponStatus.SHARED_WITH_SELLER) > -1 || couponInfo.shareIds.indexOf(CouponStatus.SELLER_REDEEMED) > -1){
            coupons.underProcess.push(couponInfo);
        }else{
            coupons.active.push(couponInfo);
        }
    });
    return coupons;
}

function _parseRedeemerDetails(company){
       let  user = company && company.user || {},
        temp = {};
    temp.contact = (user.contactNumbers && user.contactNumbers[0] && user.contactNumbers[0].contactNumber) || 'n.a.';
    temp.sellerName = user.fullName;
    temp.sellerEmail = utils.getSellerValidEmail(user);
    return temp;
}

function generateCoupon (req,res,next){
    let postData = req.body;
    let urlConfig = apiConfig.generateCoupon(req.query.userId);
    return apiService.post(urlConfig, {
        body: postData
    }).then((response)=>{
        if(response && response.data){
            let couponData = response.data;
            couponData.expiryDate = couponData && utils.formatDate(couponData.expiryDate, 'MM dd,YY');
            couponData.displayCode = couponData.code && utils.getMaskedCode(couponData.code,4);
            let makaanCouponId = couponData.id,
                buyerUserId = couponData.generatorUserId,
                cuponNotificationUrl = apiConfig.generateCouponNotification({buyerUserId,makaanCouponId});
                return apiService.post(cuponNotificationUrl).then((notificationResponse)=>{
                    couponData.notificationSent = notificationResponse && notificationResponse.data && notificationResponse.data.couponCodeNotificationSent || false;
                    if(req.isAmp){
                        return couponData;
                    }else{
                        return res.send(couponData);
                    }
                },()=>{
                    couponData.notificationSent = false;
                    if(req.isAmp){
                        return couponData;
                    }else{
                        return res.send(couponData);
                    }
                });
        }else{
            return res.send({});
        }
    },(err)=>{
        next(err);
    });
}

function apiHandler (req, res, next) {
    let requestMethod = req.method ||'';
    switch(requestMethod){
        case 'GET': 
            if(req.query.userId){
                let buyerId = req.query.userId;
                let expiryDateTimeStamp = req.query.expiryDate;
                let expiryDate = expiryDateTimeStamp ? `${utils.formatDate(expiryDateTimeStamp,"YY-mm-dd")}T:00:00:00`:''; 
                let rows = 1;
                let urlConfig = apiConfig.getCoupondetails({buyerId,expiryDate,rows});
                return apiService.get(urlConfig, {req}).then(response => {
                    if(response && response.data){
                        let coupondetails = _parseCouponDetails (response.data);
                        if(coupondetails && coupondetails.underProcess && coupondetails.underProcess.length){
                            let sellerId = coupondetails.underProcess[0].redeemerUserId;
                            return agentService.getSellerDetailsByUserId(sellerId,{req}).then(response=>{
                                coupondetails.underProcess[0].redeemerDetails = response && _parseRedeemerDetails(response)||{};
                                res.send(coupondetails);
                            },(err)=>{
                                next(err);
                            });
                        }else{
                            res.send(coupondetails);
                        }
                    }
                },(err)=>{
                    next(err);
                });
            }else{
                next(new Error("no userId found"));
            }
            break;
        case 'POST':
            if(req.query.userId){
                _shareCouponwithSeller(req,res,next);
            }else{
                generateCoupon(req,res,next);
            }
            break;
    }
    
}
module.exports = {
    apiHandler,
    generateCoupon
};
