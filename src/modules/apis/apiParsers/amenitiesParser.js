'use strict';

const apiService = require('services/apiService'),
      apiConfig = require('configs/apiConfig'),
      utilService = require('services/utilService'),
        _ = require('services/utilService')._;

module.exports.apiHandler = function(req, res, next) {
    let cityId = req.params.cityId;
    if (cityId) {
        let selector = {
                "fields": ["amenities"]
            },
            query = {
                selector: JSON.stringify(selector)
            };


        return apiService.get(apiConfig.cityDetailsById({cityId, query}), {req}).then(response => {
            res.status(200).send(response);
        }, err => {
            next(err);
        }).catch(err => {
            next(err);
        });
    } else if(Object.keys(req.query).length) {
        let selector = JSON.stringify({
                "sort": [{
                    field: 'priority',
                    sortOrder: 'DESC'
                }, {
                    field: 'geoDistance',
                    sortOrder: 'ASC'
                }]
            });
        selector = _.assign({ selector }, req.query);


        return apiService.get(apiConfig.amenities({query:selector}), {req}).then(response => {
            req.cacheControl = "691200";
            response = _parseAmenitiesData(response);
            res.status(200).send(response);
        }, err => {
            next(err);
        }).catch(err => {
            next(err);
        });
    }else{
        next(utilService.getError(400));
    }
};

function _initAmenityData(data, holder) {
    for (let i = 0; i < data.length; i++) {
        if (!holder.hasOwnProperty(data[i].localityAmenityTypes.name)) {
            holder[data[i].localityAmenityTypes.name] = [];
        }
    }
}

function _parseAmenitiesData(response) {
    let [apiData, amenityData] = [response.data, {}];
    _initAmenityData(apiData, amenityData);
    for (let i = 0; i < apiData.length; i++) {
        amenityData[apiData[i].localityAmenityTypes.name].push(apiData[i]);
    }
    return amenityData;
}
