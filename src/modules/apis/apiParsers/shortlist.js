/*
 * Description: Parser to handle all ajax calls related to favourites/shortlist
 * @author [Harvinder Singh]
 */

"use strict";

const async = require('async');
//const _ = require('services/utilService')._;

const apiService = require('services/apiService'),
    //logger = require('services/loggerService'),
    apiConfig = require('configs/apiConfig');

function getRequest(req, res, next){
    apiService.get(apiConfig.wishList().url, {req}).then((response)=>{
        let results = response && response.data,
            finalResponse = {
                listing: [],
                project: []
            };
        if(results && results.length){
            results.map(result=>{
                if(result.listingId){
                    finalResponse.listing.push({
                        id: result.listingId,
                        projectId: result.projectId,
                        timestamp: result.datetime,
                        status: 1,
                        wishListId: result.wishListId
                    });
                } else if(result.projectId){
                    finalResponse.project.push({
                        projectId: result.projectId,
                        data: {
                            label: result.project && result.project.name,
                            builderName: result.project && result.project.builder && result.project.builder.name
                        },
                        timestamp: result.datetime,
                        status: 1,
                        wishListId: result.wishListId
                    });
                }
                return;
            });
        }
        res.status(200).send(finalResponse);
    }, (error)=>{
        next(error);
    });
}

function saveListing(listing, apiOptions, postListing, callback){
    apiOptions.body = listing;
    apiService.post(apiConfig.wishList().url, apiOptions).then((response) => {
        if(response && response.data && response.data.length){
            let result = response.data.filter(savedListing=>{
                if(savedListing && listing){
                    return savedListing.listingId === listing.listingId;
                }
            });
            if(result && result.length){
                result = result[0];
                postListing.push({
                    id: result.listingId,
                    projectId: result.projectId,
                    timestamp: result.datetime,
                    wishListId: result.wishListId,
                    status: 1
                });
            }
            callback(null, response);
            return;
        }
        callback(response);
    }, (err) => {
        callback(err);
    });
}

function saveProject(project, apiOptions, postProject, callback){
    apiOptions.body = project;
    apiService.post(apiConfig.wishList().url, apiOptions).then((response) => {
        if(response && response.data && response.data.length){
            let result = response.data.filter(savedProject=>{
                if(savedProject && project){
                    return savedProject.projectId === project.projectId;
                }
            });
            if(result && result.length){
                result = result[0];
                postProject.push({
                    projectId: result.projectId,
                    data: {
                        label: result.project && result.project.name,
                        builderName: result.project && result.project.builder && result.project.builder.name
                    },
                    timestamp: result.datetime,
                    wishListId: result.wishListId,
                    status: 1
                });
            }
            callback(null, response);
            return;
        }
        callback(response);
    }, (err) => {
        callback(err);
    });
}

function postRequest(req, res, next){
    let query = req.query;
    let listings = query.listings ? JSON.parse(query.listings) : [],
        projects = query.projects ? JSON.parse(query.projects) : [],
        postCalls = [],
        apiOptions = {
            req,
            res
        };

    let postListing = [],
        postProject = [];

    postCalls = postCalls.concat(listings.map(listing => {
        return (callback) => {
            saveListing(listing, apiOptions, postListing, callback);
        };
    }));

    postCalls = postCalls.concat(projects.map(project => {
        return (callback) => {
            saveProject(project, apiOptions, postProject, callback);
        };
    }));

    async.parallel(postCalls, (err) => {
        if (err) {
            return next(err);
        }

        let finalResponse = {
            project: postProject,
            listing: postListing
        };

        res.status(200).send(finalResponse);
    });
}

function deleteRequest(req, res){
    let wishListId = req.query && req.query.wishListId,
        apiOptions = {req, res};
    apiService.delete(apiConfig.wishList({wishListId}).url, apiOptions).then((response) => {
        res.status(200).send(response);
    }, (error) => {
        res.status(error.status).send(error.body);
    });
}

function apiHandler(req, res, next) {
    var method = req.method;

    switch(method){
        case 'GET':
            getRequest(req, res, next);
            break;

        case 'POST':
            postRequest(req, res, next);
            break;

        case 'DELETE':
            deleteRequest(req, res, next);
            break;

        default:
            break;
    }
}

module.exports = {
    apiHandler
};
