"use strict";

const ApiService    = require('services/apiService'),
      Apis          = require('configs/apiConfig'),
      utilService   = require('services/utilService'),
      logger = require('services/loggerService'),
      otherReasonsId = [53, 64];

function parseReasons(payload){
    let ratings = payload.ratings.map(reason => {
        let obj = {
            "answerOptionId": reason.id,
            "ratingParameterId": reason.parentId,
            "value": 1
        };
        if(otherReasonsId.indexOf(obj.answerOptionId) > -1){
            obj.answerValue = payload.comment || '';
        }
        return obj;
    }); 
    return ratings;
}

module.exports.apiHandler = function(req, res, next) {
    let ratingSubmitId = req.params.ratingSubmitId;
    let reasons = parseReasons(req.body);
    let body = req.body || {};
    req.headers["content-type"] = "application/json";
    if (body.buyerId && body.ratings) {
        let payload = {
            "createdBy": body.buyerId,
            "jsonDump": "",
            "ratings" : reasons
        };
        ApiService.put(Apis.updateCallFeedbackRatingReason({ratingSubmitId}), {
            res: res,
            req: req,
            body: payload
        }).then((response) => {
            res.send(response);
        }, (err) => {
            logger.log("error while posting reason for call feedback ..",err);
            next(err);
        });
    } else {
        next(utilService.getError(400));
    }
};
