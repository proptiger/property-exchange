"use strict";
const localityService = require('services/localityService');
//const cityService = require('services/cityService');

module.exports.apiHandler = function(req, res, next) {
    let data = {},
        cityId = req.params.cityId,
        query = req.query,
        listingCategory = query.listingCategory || "buy";

    data.fields = query.fields && query.fields.split(',') || 'all';
    let rows = query.rows || 4;

    localityService.getTopRankedLocalities(cityId, listingCategory, {paging: {rows}}, {options: data, req: req}).then((response) => {
        response = response || {};
        res.send(response);
    }, err => {
        next(err);
    }).catch(err => {
        next(err);
    });
};