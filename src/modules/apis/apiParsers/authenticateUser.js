"use strict";
const apiService = require('services/apiService');
const apiConfig = require('configs/apiConfig');


module.exports.apiHandler = function(req, res, next) {
    let requestMethod = req.method ||'';
    switch(requestMethod){
        case 'GET': 
            if(req.params.userId){
                apiService.get(apiConfig.sendOtp({userId: req.params.userId,userNumber: req.params.userNumber}), {req}).then(response=>{
                    res.send(response);
                }, err => {
                    res.send(err);
                }).catch(err => {
                    next(err);
                });
            }else{
                next(new Error("no userId found"));
            }
            break;
        case 'POST':
            let postData = req.body;
            let urlConfig = apiConfig.verifyOtp(req.params.userId);
            apiService.post(urlConfig, {
                body: postData.otp
            }).then((response)=>{
               res.send(response.data);
            },(err)=>{
                next(err);
            });
            break;
    }
    
};
