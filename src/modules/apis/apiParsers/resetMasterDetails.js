"use strict";

module.exports.apiHandler = function(req, res) {
    let query = req.query || {};
    if(query.reset=='true'){
        require('services/masterDetailsService').populateAllMasterData();
        res.send('<p>master detail services reset</p>');
    } else {
        res.status(500).send({
            message: "master detail service: invalid param"
        });
    }
};
