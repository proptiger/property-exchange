"use strict";

const apiService    = require('services/apiService'),
      apiConfig          = require('configs/apiConfig');
const request = require('request'),
    fs = require('fs'),
    multiparty=require('multiparty');      

module.exports.apiHandler = function(req, res, next) {
    new multiparty.Form().parse(req, function(err, fields, files) {
        try{
            if (!files || !files.image){
                return res.status(400).json({
                    success: false,
                    err: "No files found"
                });
            }
            let filePath = files.image[0].path;
            var apiOptions = {
                req: req,
                res: res,
                formData: {
                    image : fs.createReadStream(filePath)
                }
            };
            let userId=JSON.parse(req.query.userId)
            apiService.post(apiConfig.uploadUserReviewImage(userId), apiOptions).then(response=> {
                fs.unlink(filePath, function (err) {console.log(err)});
                res.status(200).send(response);
            }, error=> {
                fs.unlink(filePath, function (err) {console.log(err)});
                res.status(500).send(error);
            })
        } catch (ex) {
            console.log('Buyer image upload error ',ex);
            return res.status(500).json("Error in image upload");
        }

    });
};
