"use strict";

const logger = require('services/loggerService'),
    imageService = require('services/imageService'),
    videoService = require('services/videoService'),
    imageParser = require('services/imageParser');

module.exports.apiHandler = function(req, res, next) {

    if (!req.params.objectType || !req.params.objectId) {
        logger.error('Error while getting images from listing API : Either params objectType or objectId missing');
        return [];
    }

    let objectType = req.params.objectType,
        objectId = req.params.objectId.split(',');

    let apiObj = {
        [`${objectType}Id`]: objectId,
        'mainImageId':req.query['mainImageId']
    }, groupingArray = { [`${objectType}Id`]: objectId };

    for(let key in req.query){
        if(['projectId','listingId','propertyId'].indexOf(key) > -1 && req.query[key]){
            apiObj[key] = req.query[key];
            groupingArray[key] = req.query[key];
        }
    }

    let videoPromise;
    let listingId = req.query.listingId || req.params.objectType === 'listing' && req.params.objectId;
    videoPromise = listingId && videoService.getVideoForListing(listingId, {req});

    imageService.getImageDetails(objectType, apiObj, {
        req
    }).then((response) => {
        let returnObj = response;
        if (req.query.isGallery === 'true') {
            returnObj = imageParser.groupListingImages(response, groupingArray, req);

            if (videoPromise) {
                videoPromise.then(videoObject => {
                    if (videoObject !== null) {

                        let newGalleryGroup = {
                            Overview: 1
                        };
                        for (let param in returnObj.galleryGroup) {
                            newGalleryGroup[param] = returnObj.galleryGroup[param];
                        }
                        returnObj = {
                            data: [videoObject].concat(returnObj.galleryData),
                            group: newGalleryGroup
                        };
                    } else {
                        returnObj = {
                            data: returnObj.galleryData,
                            group: returnObj.galleryGroup
                        };
                    }

                    res.status(200).send(returnObj);
                }, err => {
                    next(err);
                }).catch(err => {
                    next(err);
                });
            } else {
                returnObj = {
                    data: returnObj.galleryData,
                    group: returnObj.galleryGroup
                };
                res.status(200).send(returnObj);
            }
        } else {
            videoPromise.then(videoObject => {
                // append video bucket if there is any video
                if (videoObject !== null) {
                    returnObj[listingId] = returnObj[listingId] || {};
                    returnObj[listingId].video = [videoObject];
                    returnObj.count = returnObj.count || {listing: 0};
                    returnObj.count.listing++; 
                    returnObj.buckets = returnObj.buckets || [];
                    returnObj.buckets = [{key: 'Video',buckets: ['video']}].concat(returnObj.buckets);
                    res.status(200).send(returnObj);
                } else {
                    res.status(200).send(returnObj);
                }
            },()=> {
                res.status(200).send(returnObj);
            });  
        }
    }, err => {
        next(err);
    }).catch(err => {
    next(err);
});
};
