"use strict";
const logger = require('services/loggerService'),
    apiService = require('services/apiService'),
    apiConfig = require('configs/apiConfig'),
    utilService = require('services/utilService');

module.exports.apiHandler = function(req, res, next) {
    logger.info('entered unsubscribe service');
    let query = req.query || {},
        unsubscribeKey = encodeURIComponent(query.unsubscribeKey),
        unsubscribeApi = apiConfig.unsubscribe({
            unsubscribeKey
        });

    if(unsubscribeKey){
        return apiService.post(unsubscribeApi, {
            body: {
                "notificationCategoryId":2,
                "mediumDetailsList":[{"mediumType":"Email"}],
                "unsubscribeFromAllMediums": false
            }, req}).then((response) => {
            res.status(200).send(response);
        }, err => {
            next(err);
        }).catch(err => {
            next(err);
        });
    } else {
        next(utilService.getError(400));
    }
};