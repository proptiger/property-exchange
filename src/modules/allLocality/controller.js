"use strict";
const logger = require('services/loggerService'),
    localityService = require('services/localityService'),
    apiService = require('services/apiService'),
    apiConfig = require('configs/apiConfig'),
    globalConfig = require('configs/globalConfig'),
    imageParser = require('services/imageParser'),
    utils = require('services/utilService'),
    amenityService = require('services/amenityService'),
    seoService = require('services/seoService'),
    async = require('async'),
    cityService = require('services/cityService'),
    template = require('services/templateLoader').loadTemplate(__dirname);

function _parseListingData(listingData, urlType, cityName) {
    listingData = listingData || [];
    for (let i = 0, length = listingData.length; i < length; i++) {
        let currObj = listingData[i];
        currObj.localityHeroshotImageUrl = imageParser.appendImageSize(currObj.localityHeroshotImageUrl, 'smallHeroshot');
        currObj.url = currObj[urlType];
        currObj.altText = currObj.title = _getAltText(urlType, cityName, currObj.label);
        currObj.label = _getLocalityLabel(currObj, cityName);
        listingData[i] = currObj;
    }
    let url = listingData[0].suburb && listingData[0].suburb.city && listingData[0].suburb.city[urlType];
    return { listingData, url };
}

function _getAltText(urlType, cityName, localityName) {
    if (cityName) {
        if (urlType == 'overviewUrl') {
            return `Properties in ${localityName}, ${cityName}`;
        } else if (urlType == 'buyUrl') {
            return `Property in ${localityName}, ${cityName}`;
        } else if (urlType == 'rentUrl') {
            return `Flats for rent in  ${localityName}, ${cityName}`;
        }
    } else { //cases for pan-india
        if (urlType == 'overviewUrl') {
            return `${localityName} Property`;
        } else if (urlType == 'buyUrl') {
            return `Apartments in ${localityName}`;
        } else if (urlType == 'rentUrl') {
            return `Flats for rent in  ${localityName}`;
        }
    }
}

function _getLocalityLabel(localityObj, cityName) {
    if (!cityName) { // append city name to locality name for pan india localities
        if (localityObj && localityObj.suburb && localityObj.suburb.city && localityObj.suburb.city.label) {
            return `${localityObj.label}, ${localityObj.suburb.city.label}`;
        }
    }
    return localityObj.label;
}

function _parseLandmark(urlType, data) {
    let landmarks = [];
    if(!data || !data.length) return null;
    for(var i=0; i<data.length; i++) {
        let value = data[i], vType = (value.localityAmenityTypes && value.localityAmenityTypes.name) || "-", vUrl;
        vType = vType.replace(/(_)+/g," ");
        switch(urlType){
            case 'buyUrl':
                vUrl = value.buyUrl;
                break;
            case 'rentUrl':
                vUrl = value.rentUrl;
                break;
            case 'overviewUrl':
            default:
                vUrl = null;
                break;
        }
        if(!vUrl) continue;
        landmarks.push({
            "id": value.id,
            "name": value.name,
            "latitude": value.latitude,
            "longitude": value.longitude,
            "url": utils.prefixToUrl(vUrl),
            "type": vType
        });
    }
    return landmarks.length>0?landmarks:null;
}

module.exports.routeHandler = function(req, res, next) {
    logger.info('entered allLocalityinCityOverview route');
    let query = req.query || {},
        currentPage = parseInt(query.page) || 1,
        currentUrl = req.urlDetail.url,
        templateId = req.urlDetail.templateId,
        currBaseUrl = req.urlDetail.currBaseUrl,
        rows = globalConfig.panIndiaPageRowCount,
        start = (currentPage - 1) * rows,
        fields = ['label', 'localityHeroshotImageUrl', 'suburb', 'city'],
        countryId = req.urlDetail.countryId || (req.locals.numberFormat && req.locals.numberFormat.code) || 1,
        urlType, category, cityId, cityName,landmarkTemplateId;

    //buy or rent url
    if (templateId.indexOf('RENT') > -1) {
        urlType = 'rentUrl';
        landmarkTemplateId = 'MAKAAN_LANDMARK_PROPERTY_RENT';
    } else if (templateId.indexOf('BUY') > -1) {
        urlType = 'buyUrl';
        landmarkTemplateId = 'MAKAAN_LANDMARK_PROPERTY_BUY';
    } else if (templateId.indexOf('OVERVIEW') > -1) {
        urlType = 'overviewUrl';
        landmarkTemplateId = 'MAKAAN_LANDMARK_PROPERTY_BUY';
    }

    //city specific of pan india url
    if (templateId.indexOf('CITY') > -1) {
        cityId = req.urlDetail.cityId;
        cityName = req.urlDetail.cityName;
    }

    // let error = {};
    async.auto({
        locality: function(callback) {
            fields.push(urlType);
            localityService.getAllLocalities({countryId, cityId, rows, start, fields}, { req }).then(function(response) {
                let localityData = [], totalPages = Math.ceil(response.totalCount / rows);
                if (response.data.length) {
                    let parsedData = _parseListingData(response.data, urlType, cityName),
                        url = utils.prefixToUrl(parsedData.url);
                    localityData = parsedData.listingData;
                }
                callback(null, {localityData, totalPages});
            }, function(err) {
                // error.locality = err;
                callback(err);
            });
        },
        landmark: function(callback) {
            let urlConfig,selector, selectorObj = {
                "filters": [{"type":"wildcard","key":"buyUrl","value":"*"},{"key":"landmarkPagesLiveStatus","value":"Active"},{"key": "countryId","value": countryId}]
            };
            if(urlType=='overviewUrl'){
                callback(null, {"landmarkData":null, "totalPages":0});
            } else {
                selectorObj.fields = ["id","cityId","name","latitude","longitude","buyUrl","rentUrl","rating","localityAmenityTypes"];
                selectorObj.paging = {
                    "start": start,
                    "rows": rows
                };
                selectorObj.sort = [{
                    key: 'rating',
                    order: 'DESC'
                }];
                if(cityId){
                    selectorObj.filters.push({
                        key: "cityId",
                        value: cityId,
                    });
                }
                selector = apiService.createSelector(selectorObj);
                urlConfig = apiConfig.allLandmarks(selector);
                return apiService.get(urlConfig,{req}).then((response) => {
                    let landmarkResponse = response && response.data && response.data.results;
                    let totalPages = Math.ceil(response.totalCount / rows) || 0,
                        landmarkData = _parseLandmark(urlType, landmarkResponse);
                    callback(null, {landmarkData, totalPages});
                }, err => {
                    callback(err);
                });
            }
        },
        budget: function(callback) {
            const { mapTypeIdToPageType } = seoService;
            if(urlType == 'overviewUrl' && cityId) {
                let pageObjectIds = [
                    mapTypeIdToPageType["suburb"],
                    mapTypeIdToPageType["locality"]
                ];
                let urlConfig = apiConfig.getBudgetUrls({
                    cityId,
                    objectTypeId: pageObjectIds.join(','), 
                    pageNumber: currentPage, 
                    pageSize: rows
                });
                return apiService.get(urlConfig,{req}).then((response) => {
                    let budgetUrls = response && response.data;
                    let totalPages = Math.ceil(response.totalCount / rows) || 0;
                    callback(null, {budgetUrls, totalPages});
                }, err => {
                    callback(err);
                });
            } else {
                callback(null, {"budgetUrls":null, "totalPages": 0})
            }
        }
    }, function(err, results) {
        // if (!utils.isEmptyObject(error.locality) && !utils.isEmptyObject(error.landmark)) {
        if(err){
            return next(err);
        }
        // }
        let budgetUrls = utils.isEmptyObject(results.budget.budgetUrls)?null:results.budget.budgetUrls,
            localityData = utils.isEmptyObject(results.locality.localityData)?null:results.locality.localityData,
            landmarkData = utils.isEmptyObject(results.landmark.landmarkData)?null:results.landmark.landmarkData,
            totalPages = Math.max(results.locality.totalPages,results.landmark.totalPages,results.budget.totalPages),
            paginationData = { "totalPages": totalPages, "currentUrl": currentUrl, "currentPage": currentPage, "changeQueryParam": utils.changeQueryParam, "filter": true };
        req = utils.createSeoPrevNext(currentPage, totalPages, currBaseUrl, req);
        if (localityData || landmarkData || budgetUrls) {
            template.renderAsync({
                budgetUrls: budgetUrls,
                localityData: localityData,
                landmarkData: landmarkData,
                paginationData: paginationData,
                cityName
            }, res, '', req);
        } else if (currentPage>1) {
            res.redirect(302, currBaseUrl);
        } else {
            res.status(301);
            next();
        }
    })
};
