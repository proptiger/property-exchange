"use strict";
//const logger = require('services/loggerService'),
    const agentService = require('services/agentService'),
    utils = require('services/utilService'),
    listingsService = require("services/listingsService"),
    globalConfig = require('configs/globalConfig'),
    async = require('async'),
    MasterDetailsService = require('services/masterDetailsService'),
    template = require('services/templateLoader').loadTemplate(__dirname);

module.exports.routeHandler = function(req, res, next) {

    let pageLevel = req.urlDetail.pageLevel,
        rows = (pageLevel.indexOf('top') == -1) ? globalConfig.panIndiaPageRowCount : globalConfig.topAgentPageCount,
        currentPage = (req.query && req.query.page) ? parseInt(req.query.page) : 1,
        start = (currentPage - 1) * rows,
        currentUrl = req.urlDetail.url,
        currBaseUrl = req.urlDetail.currBaseUrl,
        totalPages,

        stateId = req.urlDetail.stateId,
        cityId = req.urlDetail.cityId,
        countryId = req.urlDetail.filters && req.urlDetail.filters.countryId || req.urlDetail.countryId || 1,
        localityId = req.urlDetail.localityId,
        suburbId = req.urlDetail.suburbId,
        suburbName = req.urlDetail.suburbName && req.urlDetail.suburbName.toLowerCase(),
        localityName = req.urlDetail.localityName && req.urlDetail.localityName.toLowerCase(),
        cityName = req.urlDetail.cityName && req.urlDetail.cityName.toLowerCase(),
        urlGroupType = req.urlDetail.urlGroupType && req.urlDetail.urlGroupType.toLowerCase(),
        unitTypesNameMap = MasterDetailsService.getMasterDetails('unitTypesNameMap');

    function _getEntityFilter(){
        let filters=[{
            key: 'countryId',
            value: countryId
        }];
        switch (pageLevel) {
            case "stateBroker":
                filters.push({
                    key: 'stateId',
                    value: stateId
                });
                break;
            case "cityallBroker":
            case "cityallBrokerBuy":
            case "cityallBrokerRent":
            case "citytopBroker":
            case "citytopBrokerBuy":
            case "citytopBrokerRent":
                filters.push({
                    key: 'cityId',
                    value: cityId
                });
                break;
            case "localityallBroker":
            case "localityallBrokerBuy":
            case "localityallBrokerRent":
            case "localitytopBroker":
            case "localitytopBrokerBuy":
            case "localitytopBrokerRent":
                filters.push({
                    key: 'localityId',
                    value: localityId
                });
                break;
            case "suburballBroker":
            case "suburballBrokerBuy":
            case "suburballBrokerRent":
            case "suburbtopBroker":
            case "suburbtopBrokerBuy":
            case "suburbtopBrokerRent":
                filters.push({
                    key: 'suburbId',
                    value: suburbId
                });
                break;
        }
        return filters;
    }
    function _getApiConfig(config) {
        let selector = {
                filters: _getEntityFilter(),
                paging: {
                    rows: rows,
                    start: start
                }
            };
        switch(urlGroupType){
            case 'buy':
                selector.filters.push({
                    key: "listingCategory",
                    value: ["Primary", "Resale"]
                });
                break;
            case 'rent':
                selector.filters.push({
                    key: "listingCategory",
                    value: ["Rental"]
                });
                break;
            default:
                selector.filters.push({
                    key: "listingCategory",
                    value: ["Primary", "Resale", "Rental"]
                });
        }

        if (!config.isTopPage) {
            selector.sort = {
                key: 'listingSellerCompanyName',
                order: 'ASC'
            };
        } else {
            selector.sort = {
                key: 'sellerTransactionRevealScore',
                order: 'DESC'
            };
        }


        return { selector };
    }

    function _sendResponse() {
        let isTopPage = pageLevel.indexOf('top') > -1;
        let options = _getApiConfig({ isTopPage });
        let pageType = _parsePageType();

        // let error = {};
        async.auto({
            brokersData: function(callback) {
                agentService.getAgents(options, { req }).then(function(response) {
                    callback(null, response);
                }, function(err) {
                    callback(err);
                });
            },
            count:['brokersData' ,function(callback,results) {
                let companyIds = [];
                if(!(results.brokersData && results.brokersData.data && results.brokersData.data.length)){
                    callback(null,{});
                    return;
                }
                results.brokersData.data.forEach(item => {
                    if(item && item.id ){
                        companyIds.push(item.id);
                    }
                });

                agentService.getSellerDetailsByCompanyList(companyIds, { req }).then((result)=>{
                    if(result && result.data && result.data.length>0){
                        callback(null, result.data);
                    } else {
                    callback("Fetch sellerInfo failed");
                    }
                }).catch((err)=>{
                    callback(err);
                });
            }],
            serplistingCount: function(callback) {
                let filter =_getEntityFilter();
                if(!filter.length){
                    callback(null,{});
                    return;
                }
                let selector = {
                    "filters": filter,
                    "jsonFacets": {
                        "groupByField": utils._.get(filter, '0.key'),
                        "fields": [
                            "listingCategory"
                        ],
                        "paging": {
                            "rows": 0
                        }
                    },
                    "paging": {
                        "rows": 0
                    }
                };
                listingsService.getListingsData(selector, { req: req })
                    .then((result) => {
                        let data = {};
                        result = utils._.get(result, 'data.jsonFacetsGroup[0]buckets[0]facetValues[0]values');
                        if(!result) {
                            return callback(null, {});
                        }
                        result.map(function(a) {
                            data[a.value] = parseInt(a.count || 0);
                        });
                        data.Primary = data.Primary || 0;
                        data.Resale = data.Resale || 0;
                        data.Rental = data.Rental || 0;
                        callback(null, data);
                    }, err => {
                        callback(err);
                    }).catch(err => {
                        callback(err);
                    });
                }
        }, function(err, results) {
            if(err){
                return next(err);
            }
            if (results.brokersData && results.brokersData.data && results.brokersData.data.length) {
                var mergedData = utils._.intersectionWith(
                  utils._.cloneDeep(results.brokersData.data),
                  results.count,
                  function(x, y) {
                    return x.id === y.id && utils._.assign(x, y);
                  }
                );
                let otherData = _parseOtherData(pageLevel, { "req":req, "response": mergedData ,totalCount:results.brokersData.totalCount});

                let agentSubLabel = "";
                if(pageLevel.indexOf('top') > -1){ agentSubLabel+="Top ";}
                else{ agentSubLabel+="A ";}
                agentSubLabel+="real estate agent (Broker or Dealer)";
                if(localityName){
                    agentSubLabel+=" in "+localityName;
                } else if(suburbName){
                    agentSubLabel+=" in "+suburbName;
                } else if(cityName){
                    agentSubLabel+=" in "+cityName;
                }
                if(pageType && pageType.for){
                    agentSubLabel+=" for "+pageType.for;
                }
                
                let data = {
                    agentData: mergedData,
                    agentSubLabel: agentSubLabel,
                    serplistingCount:results.serplistingCount,
                    cityId,
                    localityId,
                    suburbId,
                    isTopPage,
                    localityName,
                    cityName,
                    suburbName,
                    paginationData: otherData.paginationData,
                    pageType: pageType,
                    getH3
                };

                template.renderAsync({
                    data: data,
                    // asyncData: function(args, callback) {
                    //     _getAsyncData(callback);
                    // },
                    asycSeoData: function(args, callback) {
                        let seoPrevNext ={};
                        callback(null, {
                            seoPrevNext: utils.createSeoPrevNext(currentPage, totalPages, currBaseUrl, seoPrevNext)
                        });
                    }
                }, res, "", req);
            } else if(currentPage > 1){
                res.redirect(302, utils.prefixToUrl(currentUrl));
            } else{
                template.renderAsync({data:{pageType: pageType}}, res, "", req);
            }
        });
    }

    function _parsePageType(){

        var buyUrl, rentUrl, allUrl, isTop,
         allLabel,
         buyLabel='Agents for Buy',
         rentLabel='Agents for Rent',
         pageType = {
            "current": '',
            "items":[]
        };

        if(pageLevel.indexOf('top') > -1){
            isTop = 'top';
            allLabel = 'Top Agents';
        } else {
            isTop = 'all';
            allLabel = 'All Agents';
        }

        if(pageLevel.indexOf('locality')>-1){
            buyUrl = '/'+cityName.replace(/\s/g, "-")+'/'+isTop+'-real-estate-agent-broker-for-buy-in-'+localityName.replace(/\s/g, "-")+'-'+localityId;
            rentUrl = '/'+cityName.replace(/\s/g, "-")+'/'+isTop+'-real-estate-agent-broker-for-rent-in-'+localityName.replace(/\s/g, "-")+'-'+localityId;
            allUrl = '/'+cityName.replace(/\s/g, "-")+'/'+isTop+'-real-estate-agent-broker-in-'+localityName.replace(/\s/g, "-")+'-'+localityId;
        }
        else if(pageLevel.indexOf('suburb')>-1){
            buyUrl = '/'+cityName.replace(/\s/g, "-")+'/'+isTop+'-real-estate-agent-broker-for-buy-in-'+suburbName.replace(/\s/g, "-")+'-'+suburbId;
            rentUrl = '/'+cityName.replace(/\s/g, "-")+'/'+isTop+'-real-estate-agent-broker-for-rent-in-'+suburbName.replace(/\s/g, "-")+'-'+suburbId;
            allUrl = '/'+cityName.replace(/\s/g, "-")+'/'+isTop+'-real-estate-agent-broker-in-'+suburbName.replace(/\s/g, "-")+'-'+suburbId;
        }
        else if(pageLevel.indexOf('city')>-1){
            buyUrl = '/'+cityName.replace(/\s/g, "-")+'/'+isTop+'-real-estate-agent-broker-for-buy';
            rentUrl = '/'+cityName.replace(/\s/g, "-")+'/'+isTop+'-real-estate-agent-broker-for-rent';
            allUrl = '/'+cityName.replace(/\s/g, "-")+'/'+isTop+'-real-estate-agent-broker';
        }
        else{
            return;
        }

        switch(urlGroupType){
            //all india page - no links
            case 'default':
                return;
            //for both buy and rent
            case 'overview':
                pageType.current = allLabel;
                pageType.items.push({
                    "name":rentLabel,
                    "isLink":true,
                    "url":rentUrl
                });
                pageType.items.push({
                    "name":buyLabel,
                    "isLink":false,
                    "url":buyUrl
                });
                break;
            case 'buy':
                pageType.for = 'buy';
                pageType.current = buyLabel;
                pageType.items.push({
                    "name":rentLabel,
                    "isLink":true,
                    "url":rentUrl
                });
                pageType.items.push({
                    "name":allLabel,
                    "isLink":true,
                    "url":allUrl
                });
                break;
            case 'rent':
                pageType.for = 'rent';
                pageType.current = rentLabel;
                pageType.items.push({
                    "name":buyLabel,
                    "isLink":false,
                    "url":buyUrl
                });
                pageType.items.push({
                    "name":allLabel,
                    "isLink":true,
                    "url":allUrl
                });
                break;
            default:
                return;
        }

        return pageType;
    }

    function _parseOtherData(type, { req, response ,totalCount}) {
        let data = {};
        totalPages = Math.ceil(totalCount / rows);

        data.paginationData = {
            "totalPages": totalPages,
            "currentUrl": currentUrl,
            "filter": true,
            "currentPage": currentPage
        };
        data.paginationData.changeQueryParam = utils.changeQueryParam;
        data.isCityPage = (req.urlDetail.cityId) ? true : false;
        return data;
    }

    function getH3(unitType, agentType, data) {
        let resPageType='', area = data.localityName || data.suburbName || data.cityName || '';
        area = area ? 'in '+area.toLowerCase() : '';
        unitType = unitType && unitType.toLowerCase();
        let unitTypeDisplayName = (unitTypesNameMap[unitType] && unitTypesNameMap[unitType].label) || unitType;
        unitTypeDisplayName = unitTypeDisplayName.toLowerCase();
        if(data.pageType && data.pageType.for){
            resPageType = ' for '+data.pageType.for;
        }
        return data.isTopPage ? `Top ${unitTypeDisplayName} property ${agentType}${resPageType} ${area}` : `${unitTypeDisplayName} property ${agentType}${resPageType} ${area}`;
    }
    _sendResponse();
};
