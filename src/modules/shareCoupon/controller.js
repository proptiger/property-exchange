"use strict";


 const template = require('services/templateLoader').loadTemplate(__dirname),
 		utils = require('services/utilService');

module.exports.routeHandler = function(req,res) {
	let isMobileRequest = utils.isMobileRequest(req) ? true : false,
		isIphone = utils.isIphoneRequest(req);
    template.renderAsync({isMobileRequest,isIphone}, res, '', req);
};
