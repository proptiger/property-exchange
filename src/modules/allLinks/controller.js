"use strict";
const template = require('services/templateLoader').loadTemplate(__dirname);
const seoService = require('services/seoService'),
    globalConfig = require('configs/globalConfig'),
    utils = require('services/utilService');

function _getConfiguration(req) {
    req.urlDetail = req.urlDetail || {};
    let cityId = req.urlDetail.cityId,
        cityName = req.urlDetail.cityName,
        localityId = req.urlDetail.localityId,
        localityName = req.urlDetail.localityName,
        suburbId = req.urlDetail.suburbId,
        suburbName = req.urlDetail.suburbName;
    return {
        cityId,
        localityId,
        suburbId,
        cityName,
        localityName,
        suburbName
    };
}

module.exports.routeHandler = function(req, res, next) {

    let rows = globalConfig.projectSerpItemsPerPage,
        currBaseUrl = req.urlDetail.currBaseUrl,
        currentUrl = utils.prefixToUrl(req.urlDetail.url);

    if(req.query && req.query.page){
        res.redirect(301, currentUrl.split('?')[0]); // removed pagination redirecting to first page
        return;
    }

    let config = _getConfiguration(req);
    let objectId, objectType;
    if (config.localityId) {
        objectId = config.localityId;
        objectType = 'locality';
    } else if (config.suburbId) {
        objectId = config.suburbId;
        objectType = 'suburb';
    } else if (config.cityId) {
        objectId = config.cityId;
        objectType = 'city';
    }
    if (objectType && objectId) {
        seoService.getAllSeoUrls(objectId, objectType, req).then((data = {}) => {

            template.renderAsync({
                groupUrls: data.data,
            }, res, "", req);
        }, (err) => {
            logger.error('inside allLinks controller error: ', err);
            next(err);
        }).catch(err => {
            logger.error('inside allLinks controller catch: ', err);
            next(err);
        });
    } else {
        res.status(301);
        next();
    }


};
