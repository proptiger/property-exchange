"use strict";
const propertyService = require('services/propertyService'),
    imageParser = require('services/imageParser'),
    utils = require('services/utilService'),
    globalConfig = require('configs/globalConfig'),
    benefitTagConfig = require('configs/benefitTagConfig'),
    maps = require('services/propertyMappingService'),
    mappingService = require('services/mappingService'),
    logger = require('services/loggerService'),
    desktopTemplate = require('services/templateLoader').loadTemplate(__dirname, 'view.marko'),
    mobileTemplate = require('services/templateLoader').loadTemplate(__dirname, 'view-mobile.marko'),
    _isEmptyObject = utils.isEmptyObject,
    utilService = require('services/utilService'),
    moment = require('moment'),
    _ = utilService._,
    BlogService = require('services/blogService'),
    config = {
        validActiveStatus: ['active','activeinmakaan'],
        dummyStatus:['dummy'],
        unverifiedStatus:['Unverified'],
        underScreeningStatus : ['ImageScreeningRequired', 'AllocatedToImageScreener']
    },
    similarPropertyService = require('services/similarPropertyService'),
    POSTED_BY_BROKER = "BROKER",
    sharedConfig = require('public/scripts/common/sharedConfig'),
    BUDGET_PERCENTAGE = .30;

var makaanSelectCities = sharedConfig.makaanSelectCities;
var wordLimit,
    _getAmenities = propertyService.getAmenities,

    _getAdditionalRooms = propertyService.getAdditionalRooms,

    _getViewDirections = propertyService.getViewDirections,

    _getTenantType = propertyService.getTenantType,


    _getOverviewKeys = function(data) {
        var keyArray, length, i,
            resultObj = {};
        keyArray = Object.keys(data);
        length = keyArray.length;
        for (i = 0; i < length; i++) {
            let val = data[keyArray[i]];
            resultObj[keyArray[i]] = {
                key: maps.getLanguageText(keyArray[i]),
                val: val,
                title: val && val.toString().replace(/<.*>/g, '')
            };
        }
        return resultObj;
    },
    _getDisclaimer = function(projectStatusId) {
        return ["New Launch", "Under Construction", "Launching Soon"].indexOf(mappingService.getPropertyStatus(projectStatusId)) > -1;
    },

    _getPlc = function(data) {
        let plc = [];
        for (var i = 0; i < data.length; i++) {
            plc.push(maps.getPlc(data[i].otherPricingSubcategoryId));
        }
        if (plc.length) {
            return plc.toString();
        }
        return null;
    },

    _getOverViewDetails = function(data, type, category, format) {
        let strippedDescription = utils.stripHtmlContent(data.description, wordLimit, true, true);
        let result = {
                "unitTypeId": data.area.unitTypeId,
                "unitType": maps.getPropertyType(data.area.unitTypeId),
                "localityId": data.locality.localityId,
                "cityId": data.city.id,
                'listingCategory': data.listingCategory,
                'isRk': data.flags.rk,
                'isStudio': data.flags.studio,
                'isPentHouse': data.flags.penthouse,
                'priceNum': data.priceDetails.price,
                'beds': data.rooms.bedrooms,
                'projectStatus': mappingService.getPropertyStatus(data.constructionStatusId),
                'showDisclaimer': _getDisclaimer(data.constructionStatusId)
            },
            details = {
                'status': mappingService.getPropertyStatus(data.constructionStatusId),
                'area': data.area.size + ' ' + data.area.units,
                'dateAdded': utils.formatDate(data.creationDate, 'SS YY'),
                'parking': data.details.parking,
                'category': maps.getListingCategory(data.listingCategory),
                'floor': data.tower.floor ? utils.ordinalSuffix(data.tower.floor) + (data.tower.totalFloors ? ` <span class="grey">of ${data.tower.totalFloors}</span>` : '') : undefined,
                'facing': maps.getFacing(data.details.facing),
                'bath': data.rooms.bathrooms,
                'balcony': data.rooms.balcony,
                'furnishStat': data.furnishStat,
                'totalFloor': data.tower.totalFloors,
                'bookamt': data.priceDetails.bookingAmount ? `<span class="currency"></span> ${utils.formatNumber(data.priceDetails.bookingAmount, { type : 'number', seperator : format})}` : undefined,
                'openSides': data.details.openSides,
                'securityDeposit': data.priceDetails.securityDeposit ? `<span class="currency"></span> ${utils.formatNumber(data.priceDetails.securityDeposit, { type : 'number', seperator : format})}` : "No Deposit",
                'tenantPref': _getTenantType(data.details.tenantTypes),
                'entryRoadWd': data.details.mainEntryRoadWidth ? data.details.mainEntryRoadWidth + ' feet' : undefined,
                'priceNeg': data.flags.negotiable ? 'Yes' : 'No',
                'overlook': _getViewDirections(data.details.viewDirections),
                'addRoom': _getAdditionalRooms(data.rooms),
                'ownerType': maps.getOwnerType(data.details.ownershipTypeId),
                'beds': data.rooms.bedrooms,
                'price': data.priceDetails.price,
                'unitType': data.area.unitType,
                'carpetArea': data.details.carpetArea,
                'noOfSeats': data.noOfSeats,
                'noOfRooms': data.noOfRooms,
                'lockInPeriod': data.lockInPeriod ? (data.lockInPeriod + ' Years') : undefined,
                'maintenanceCharges': (data.maintenanceCharges && data.maintenanceFrequencyType) ? (data.maintenanceCharges + ' ' + data.maintenanceFrequencyType.toLowerCase()) : null,
                'willingToModifyInteriors': data.willingToModifyInteriors ? 'Yes' : undefined,
                'isCornerShop': data.isCornerShop ? 'Yes' : undefined,
                'isBoundryWallPresent': data.isBoundryWallPresent ? 'Yes' : undefined,
                'isMainRoadFacing': data.isMainRoadFacing ? 'Yes' : undefined,
                'pantryType': data.pantryType ? data.pantryType.toLowerCase() : undefined,
                'assureReturnPercentage': data.assureReturnPercentage,
                'currentlyLeasedOut': data.currentlyLeasedOut ? 'Yes' : undefined,
            };
        let plc = _getPlc(data.plc);
        if (plc) {
            details.plc = 'other costs ' + plc;
        }
        details.posession = (data.constructionStatusId == 1) ? utils.formatDate(data.possessionDate, 'SS YY') : undefined;
        details.availability = data.possessionDate ? (data.possessionDate > Date.now() ? `from ${utils.formatDate(data.possessionDate, 'SS YY')}` : 'immediate') : undefined;
        if (data.constructionStatusId == 2 && (data.details.minAge || data.details.maxAge)) {
            let minAge = data.details.minAge || data.details.maxAge, //incase minAge is missing
                totallMonths = moment().diff(minAge, 'months'),
                years = Math.floor(totallMonths / 12),
                months = totallMonths % 12;
            details.age = years;
            if (months || (years === 0)) { // years == 0 is the case if property is some days old
                details.age += ` - ${years+1}`;
            }
            details.age += (years === 0 || (years == 1 && !months)) ? ' year' : ' years';
        }
        //details.age = (data.constructionStatusId == 2) ? utils.getRange(moment().diff(data.details.minAge, 'years'), (moment().diff(data.details.maxAge, 'years')), 'years') : undefined;
        let rawDetails = details;
        details = _getOverviewKeys(details);
        result.size = data.area.size ? `${utils.formatNumber(data.area.size, 
            { type : 'number', seperator : format}
        )} ${data.area.units}` : undefined;
        let _bhkString = (data.flags.rk) ? "RK" : "BHK",
            _unitTypeString = data.type;

        if (data.flags.studio) {
            _unitTypeString = "studio apartment";
        }

        if (data.flags.penthouse) {
            _unitTypeString = "penthouse";
        }

        if(data.isCommercialListing) {
            result.title = data.area.unitType;
        } else{
            result.title = (data.area.unitType !== 'Plot' ? (`${data.rooms.bedrooms} ${_bhkString}`) : '') + ` ${_unitTypeString}`;
        }

        result.longTitle = data.area.unitType !== 'Plot' ? (data.rooms.bedrooms + ' BHK ' + (data.rooms.bathrooms ? ` + ${data.rooms.bathrooms} T ` : '') + data.area.unitType) :(`${data.area.unitType} ${result.size||''}`);
        result.description = {
            fullDescription: data.description,
            smallDescription: strippedDescription.description,
            hideViewMore: strippedDescription.error
        };
        result.isNegotiable = data.flags.negotiable;
        result.price = utils.formatNumber(data.priceDetails.price, { 
            precision : 2, 
            returnSeperate : true,
            seperator : format
        });
        result.pricePerArea = (category != 'Rental') ? utils.formatNumber(data.priceDetails.pricePerUnitArea, 
            {type : 'number', seperator : format}
        ) : undefined;
        result.priceInclusive = data.priceAllInclusive;
        result.details = {};
        if(data.isCommercialListing){
            result.details.thirdFold = propertyService.getThirdFolddata(details);
        }
        details = propertyService.sortByMap(details, type, category, 'overview');
        result.details.firstFold = details.splice(0, globalConfig.visibleKeyCount);
        result.details.secondFold = details;
        result.propertyId = data.propertyId;
        result.listingId = data.listingId;
        result.projectId = data.project.id;
        result.trendIncreasing = (category != 'Rental') ? data.project.avgPriceRisePercentage > 0 : undefined;
        result.emi = (category != 'Rental') ? utils.formatNumber(
            utils.calculateEMI(data.priceDetails.price, globalConfig.emi_rate, globalConfig.emi_tenure, globalConfig.downpayment_percent),
            { type : 'number', seperator : format }
        ) : undefined;
        return { parsed: result, raw: rawDetails };
    },


    _groupData = function(data) {
        var result = {},
            priceDetails = data.currentListingPrice,
            property = data.property,
            galleryImages = [],
            companySeller = data.companySeller,
            project = property ? property.project : {},
            locality = project ? project.locality : {},
            builder = project ? project.builder : {},
            propertyOtherPricingSubcategoryMappings = data.propertyOtherPricingSubcategoryMappings ? data.propertyOtherPricingSubcategoryMappings : null;
        result.flags = {};
        result.priceDetails = {};
        result.rooms = {};
        result.area = {};
        result.tower = {};
        result.project = {};
        result.locality = {};
        result.city = {};
        result.builder = {};
        result.floorPlan = {};
        result.details = {};
        result.sellerDetails = {};
        result.plc = {};
        if (propertyOtherPricingSubcategoryMappings) {
            result.plc = propertyOtherPricingSubcategoryMappings;
        }
        if (!_isEmptyObject(priceDetails)) {
            result.priceDetails.price = priceDetails.price;
            result.priceDetails.pricePerUnitArea = priceDetails.pricePerUnitArea;
        }
        if (companySeller) {
            let seller = companySeller.company || {},
                user = companySeller.user || {};
            result.sellerDetails.name = seller.name;
            result.sellerDetails.paidLeadCount = seller.paidLeadCount;
            let sellerNumbers = user.contactNumbers;
            if (sellerNumbers && sellerNumbers.length) {
                result.sellerDetails.phone = sellerNumbers[0].contactNumber;
            } else {
                result.sellerDetails.phone = 'n.a.';
            }
            result.sellerDetails.image = imageParser.appendImageSize(seller.logo || user.profilePictureURL, 'thumbnail');
            result.sellerDetails.assist = seller.assist;
            result.sellerDetails.sellerId = seller.id;
            result.sellerDetails.userId = user.id;
            result.sellerDetails.type = seller.type;
            if (seller.type === POSTED_BY_BROKER) {
                result.sellerDetails.type = 'AGENT';
            }
            if (seller.score) {
                result.sellerDetails.rating = (seller.score);
                result.sellerDetails.ratingClass = utils.getRatingClass(seller.score);
            }
            result.sellerDetails.assist = seller.assist;
            result.sellerDetails.avatar = utils.getAvatar(seller.name);
        }
        if (property) {
            result.flags.penthouse = property.penthouse;
            result.flags.studio = property.studio;
            result.flags.rk = property.rk;
            result.flags.isPropertySoldOut = property.isPropertySoldOut;
            result.rooms.bedrooms = property.bedrooms;
            result.rooms.bathrooms = property.bathrooms || undefined;
            result.rooms.study = property.studyRoom || undefined;
            result.rooms.servant = property.servantRoom || undefined;
            result.rooms.pooja = property.poojaRoom || undefined;
            result.rooms.balcony = property.balcony || undefined;
            result.area.unitType = property.unitType;
            result.area.unitTypeId = property.unitTypeId;
            result.area.unitName = property.unitName;
            result.area.size = property.size;
            result.area.units = property.measure;
            result.details.carpetArea = property.carpetArea ? property.carpetArea + ' ' + property.measure : null;
            // result.sourceDomain = property.propertySourceDomain;
            galleryImages = galleryImages.concat(property.images);
        }
        if (!_isEmptyObject(project)) {
            result.project.show = (config.dummyStatus.indexOf((project.activeStatus || '').toLowerCase()) === -1);
            result.project.id = project.projectId;
            result.project.name = project.name;
            result.project.possessionDate = project.possessionDate;
            result.project.url = (config.validActiveStatus.indexOf((project.activeStatus || '').toLowerCase()) > -1) ? utils.prefixToUrl(project.overviewUrl) : undefined;
            result.project.description = project.description;
            result.project.livabilityScore = project.livabilityScore > 10 ? 10 : project.livabilityScore;
            result.project.availability = project.derivedAvailability;
            result.project.projectStatus = project.projectStatus;
            result.project.avgPriceRisePercentage = project.avgPriceRisePercentage;
            result.project.projectImage = project.imageURL;
            result.project.sizeInAcres = project.sizeInAcres;
            result.project.launchDate = project.launchDate;
            result.project.hideLaunchDate = project.hideLaunchDate;
            galleryImages = galleryImages.concat(project.images);
        }
        if (!_isEmptyObject(locality)) {
            result.locality.localityId = locality.localityId;
            result.locality.name = locality.label;
            result.locality.url = utils.prefixToUrl(locality.overviewUrl);
            result.locality.buyUrl = utils.prefixToUrl(locality.buyUrl);
            result.locality.rentUrl = utils.prefixToUrl(locality.rentUrl);
            result.locality.livabilityScore = locality.livabilityScore > 10 ? 10 : locality.livabilityScore;
            result.locality.description = utils.stripHtmlContent(locality.description, globalConfig.wordLimit.smallLength, true, true).description;
            result.locality.localityImage = imageParser.appendImageSize(locality.localityHeroshotImageUrl, 'small');
            if (locality.suburb && locality.suburb.city) {
                result.city.label = locality.suburb.city.label;
                result.city.id = locality.suburb.city.id;
                result.city.url = utils.prefixToUrl(locality.suburb.city.overviewUrl);
            }
            galleryImages = galleryImages.concat(locality.images);
        }
        if (!_isEmptyObject(builder)) {
            result.builder.show = (config.dummyStatus.indexOf((builder.activeStatus || '').toLowerCase()) === -1);
            result.builder.id = builder.id;
            result.builder.name = builder.name;
            result.builder.displayName = builder.displayName ? builder.displayName : '';
            result.builder.logo = imageParser.appendImageSize(builder.imageURL, 'smallSquare');
            result.builder.altText = builder.mainImage && builder.mainImage.altText;
            result.builder.description = utils.stripHtmlContent(project.builder.description, globalConfig.wordLimit.longLength, true, true).description; //modifying builder description to show three line(approx 500 letter i.e longlength)
            result.builder.originalDescription = project.builder.description;
            result.builder.url = (config.validActiveStatus.indexOf((builder.activeStatus || '').toLowerCase()) > -1) ? utils.prefixToUrl(builder.buyUrl) : undefined;
            result.builder.age = builder.establishedDate ? utils.timeFromDate(builder.establishedDate) + ' years' : undefined;
            result.builder.timeCompletion = builder.percentageCompletionOnTime ? builder.percentageCompletionOnTime + '%' : undefined;
            var projectStatus = builder.projectStatusCount;
            if (projectStatus) {
                result.builder.ongoing = projectStatus['under construction'];
                result.builder.completed = projectStatus.completed;
            }

        }
        galleryImages = galleryImages.concat(data.images);
        result.details.facing = data.facingId;
        result.details.mainEntryRoadWidth = data.mainEntryRoadWidth;
        result.details.tenantTypes = data.tenantTypes;
        result.details.viewDirections = data.viewDirections;
        result.details.parking = data.noOfCarParks;
        result.details.openSides = data.noOfOpenSides || undefined;
        result.details.ownershipTypeId = data.ownershipTypeId;
        result.details.minAge = data.minConstructionCompletionDate;
        result.details.maxAge = data.maxConstructionCompletionDate;
        result.furnishStat = mappingService.getFurnishingStatus(data.furnished) || data.furnished;
        result.tower.floor = data.floor === 0 ? 'Gr' : data.floor;
        result.tower.totalFloors = data.totalFloors;
        result.description = data.description;
        result.latitude = data.listingLatitude;
        result.longitude = data.listingLongitude;
        result.creationDate = data.postedDate;
        result.priceDetails.bookingAmount = data.bookingAmount || undefined;
        result.priceDetails.securityDeposit = data.securityDeposit || undefined;
        result.flags.negotiable = data.negotiable;
        result.galleryImages = galleryImages;
        result.listingCategory = data.listingCategory;
        result.status = data.status;
        result.type = maps.getPropertyType(data.property.unitTypeId) || '';
        result.furnishing = data.furnishings;
        result.amenities = _getAmenities(data.masterAmenityIds, data.property.project.projectAmenities);
        result.propertyId = data.propertyId;
        result.specifications = data.specifications;
        result.listingId = data.id;
        result.constructionStatusId = data.constructionStatusId;
        result.possessionDate = data.possessionDate;
        result.updatedAt = utils.formatDate(data.updatedAt, 'dd-mm-YY');
        result.relativeVerifiedDate = data.displayDate && moment(data.displayDate).fromNow();
        if (data.mainImage) {
            result.mainImage = [{
                url: data.mainImage.absolutePath,
                alt: data.mainImage.altText,
                title: data.mainImage.title

            }];
        } else {
            result.mainImage = [{
                url: data.mainImageURL
            }];
        }
        if(data.allocation && data.allocation.allocationHistory && data.allocation.allocationHistory.length && data.allocation.allocationHistory[0].masterAllocationStatus && data.allocation.allocationHistory[0].masterAllocationStatus.status && config.underScreeningStatus.indexOf(data.allocation.allocationHistory[0].masterAllocationStatus.status)>-1){
            result.underScreening = true;
        } else {
            result.underScreening = false;
        }

        result.isCommercialListing = data.isCommercialListing;
        result.noOfSeats = data.noOfSeats;
        result.noOfRooms = data.noOfRooms;
        result.maintenanceCharges = data.maintenanceCharges;
        result.lockInPeriod = data.lockInPeriod;
        result.maintenanceFrequencyType = data.maintenanceFrequencyType;
        result.willingToModifyInteriors = data.willingToModifyInteriors;
        result.isBoundryWallPresent = data.isBoundryWallPresent;
        result.isMainRoadFacing = data.isMainRoadFacing;
        result.isCornerShop = data.isCornerShop;
        result.assureReturnPercentage = data.assureReturnPercentage;
        result.currentlyLeasedOut = data.currentlyLeasedOut;
        result.pantryType = data.pantryType;

        return result;
    },

    _getProjectDetails = function(data, sellerDetails = {}) { //jshint ignore:line
        var result = {};
        result.projectId = data.id;
        result.show = data.show;
        result.name = data.name;
        result.description = utils.stripHtmlContent(data.description, globalConfig.wordLimit.midLength, true, true).description;
        result.url = utils.prefixToUrl(data.url);
        result.livabilityScore = data.livabilityScore > 10 ? 10 : data.livabilityScore;
        result.projectImage = imageParser.appendImageSize(data.projectImage, 'small');
        result.totalUnits = data.availability;
        result.launchDate = utils.formatDate(data.launchDate, 'SS YY');
        result.hideLaunchDate = data.hideLaunchDate;
        result.area = data.sizeInAcres;
        return result;
    },

    _getBuilderDetails = function(data) {
        var result = {};
        result.show = data.builder.show;
        result.logo = data.builder.logo;
        result.altText = data.builder.altText;
        result.name = data.builder.name;
        result.displayName = data.builder.displayName ? data.builder.displayName : '';
        result.description = data.builder.description;
        result.originalDescription = data.builder.originalDescription;
        result.url = utils.prefixToUrl(data.builder.url);
        result.details = [{
            key: 'Experience',
            val: data.builder.age
        }, {
            key: 'Completion on time',
            val: data.builder.timeCompletion
        }, {
            key: 'Ongoing Projects',
            val: data.builder.ongoing,
            "url": result.url ? `${result.url}?possession=any` : undefined
        }, {
            key: 'Past Projects',
            val: data.builder.completed,
            url: result.url ? `${result.url}?ageOfProperty=any` : undefined
        }];
        return result;
    },
    _getHomeloanData = function(data, format) {
        var result = {};
        if (data.currentListingPrice) {
            result.rawPrice = data.currentListingPrice.price;
            result.readablePrice = utils.formatNumber(data.currentListingPrice.price, {
                returnSeperate : true, 
                seperator : format
            });
        }

        return result;
    },
    getMaxPrice = function(price){
        price += (price*0.1) //taking 10% +
        return price;
    },
    propertyParser = function(data, req) {
        wordLimit = utils.isMobileRequest(req) ? globalConfig.wordLimit.mobile : globalConfig.wordLimit.desktop;
        var groupData = _groupData(data),
            numberFormat = req.locals.numberFormat,
            result = mappingService.getSellerStatuses(data);
        

        result.isMakaanSelectSeller = result.isMakaanSelectSeller && 
                                    groupData.city && makaanSelectCities.indexOf(groupData.city.id) > -1 ? true : false ;
        result.isNonPaidSeller = data && ((data.listingSellerDisplayAds && data.listingSellerDisplayAds.length) || (data.listingSellerBenefits && data.listingSellerBenefits.length)) ? false : true;
        result.reviewsCount = data.sellerCompanyFeedbackCount>0?data.sellerCompanyFeedbackCount:0;
        result.ratingsCount = data.sellerCallRatingCount>0?data.sellerCallRatingCount:0;
        result.amenities = propertyService.parseAmenities(groupData.amenities, groupData.type, groupData.listingCategory, groupData.isCommercialListing);
        if (groupData.furnishing && groupData.furnishing.length && !groupData.isCommercialListing) {
            result.furnishings = propertyService.parseFurnishing(groupData.furnishing, groupData.type, groupData.listingCategory);
        }
        result.specifications = propertyService.parseSpecifications(groupData.specifications, groupData.type, groupData.listingCategory);
        let overviewData = _getOverViewDetails(groupData, groupData.type, groupData.listingCategory, numberFormat);
        result.overview = overviewData.parsed;
        result.raw = overviewData.raw;
        result.builder = _getBuilderDetails(groupData);
        result.project = _getProjectDetails(groupData.project, groupData.sellerDetails);
        result.localityDetails = groupData.locality;
        result.city = groupData.city;
        result.listingScore = data.qualityScore;
        result.coordinates = {
            lat: groupData.latitude,
            lng: groupData.longitude
        };
        result.homeloan = _getHomeloanData(data, numberFormat);
        // let {
        //     formattedImages, groupObj
        // } = imageParser.parseGalleryImages(groupData.galleryImages);
        // result.galleryGroup = JSON.stringify(groupObj);
        // result.galleryImages = JSON.stringify(formattedImages);
        // result.galleryData = JSON.stringify({
        //     data: formattedImages,
        //     group: groupObj
        // });
        result.baseLength = globalConfig.visibleKeyCount;
        result.smallLength = wordLimit;

        result.showMasterPlan = utils.isMasterPlanSupported({
            name: groupData.city.label,
            id: groupData.city.id
        });
        result.tabs = _.extend([], maps.getNeighbourhoodTabs());
        // if (!result.showMasterPlan) {
        //     result.tabs.splice(0, 1);
        // }
        result.sellerDetails = groupData.sellerDetails;
        result.sellerSimilarProp = {
            bed: (groupData.rooms.bedrooms || 0).toString(),
            bath: (groupData.rooms.bathrooms || 0).toString(),
            study: (groupData.rooms.study || 0).toString(),
            pooja: (groupData.rooms.pooja || 0).toString(),
            servant: (groupData.rooms.servant || 0).toString(),
            projectId: (groupData.project.id).toString(),
            listingCompanyId: (groupData.sellerDetails.sellerId).toString(),
            pricemin:0,
            pricemax: getMaxPrice(groupData.priceDetails.price),
            listingCategory: (groupData.listingCategory == 'Rental' ? ['Rental'] : ['Primary', 'Resale']).toString()
        };
        result.mainImage = groupData.mainImage;
        result.updatedAt = groupData.updatedAt;
        result.relativeVerifiedDate = groupData.relativeVerifiedDate;
        result.possessionDateFormatted = moment(groupData.possessionDate).format('DD-MM-YYYY');
        result.underScreening = groupData.underScreening;
        result.tenantPreferences = data.tenantPreference && Object.keys(data.tenantPreference).map(function(key){
            return {
                "key": key,
                "val": data.tenantPreference[key]
            }
        });
        result.listing = getPropertyListingData(result, data);
        result.isCommercialListing = groupData.isCommercialListing;
        return result;
    },
    getOtherSellersTitle = function(parsedData) {
        let title = parsedData.overview.longTitle;
        if (parsedData.builder.show || parsedData.project.show) {
            title += " in ";
            title += parsedData.builder.show ? (parsedData.builder.displayName||'') + ' ' : ' ';
            title += parsedData.project.show ? parsedData.project.name + ' ' : ' ';
        }
        return title;
    },
    getLeadBoxData = function(pageAllData) {
        var leadBoxData = {};
        var leadFormData = {};
        leadBoxData.projectTitle = pageAllData.project.name;
        leadBoxData.localityDetails = pageAllData.localityDetails.name + ', ' + pageAllData.city.label;
        leadBoxData.price = pageAllData.overview.price.val + ' ' + pageAllData.overview.price.unit;
        leadBoxData.type = pageAllData.overview.title;
        leadBoxData.listingId = pageAllData.overview.listingId;
        leadBoxData.projectId = pageAllData.overview.projectId;
        leadBoxData.builderName = pageAllData.builder.name;
        leadFormData.city = {};
        leadFormData.city.id = pageAllData.city.id;
        leadFormData.listingId = pageAllData.listingId;
        leadFormData.listingCategory = pageAllData.listingCategory;
        leadFormData.sellerDetails = {};
        leadFormData.sellerDetails.sellerId = pageAllData.sellerDetails.sellerId;
        leadFormData.sellerDetails.name = pageAllData.sellerDetails.name;
        leadFormData.sellerDetails.phone = pageAllData.sellerDetails.phone;
        leadFormData.sellerDetails.rating = pageAllData.sellerDetails.rating;
        leadFormData.sellerDetails.type = pageAllData.sellerDetails.type;
        leadFormData.sellerDetails.userId = pageAllData.sellerDetails.userId;
        leadFormData.sellerDetails.image = pageAllData.sellerDetails.image;
        leadFormData.sellerDetails.assist = pageAllData.sellerDetails.assist;
        leadFormData.sellerDetails.isMakaanSelectSeller = pageAllData.isMakaanSelectSeller;
        leadFormData.isMakaanSelectSeller = pageAllData.isMakaanSelectSeller;
        leadBoxData.leadFormData = leadFormData;
        return leadBoxData;
    };

function getPropertyListingData(result, data) {
    return {
        allocation: data.allocation,
        hasVideo: false,
        isPlot: data.property.unitType && data.property.unitType.toLowerCase() === 'residential plot',
        isApartment: data.property.unitType && data.property.unitType.toLowerCase() === 'apartment',
        listingId: result.overview.listingId,
        imagesCount: data.imageCount,
        mainImageURL: data.mainImage.absolutePath,
        mainImageTitle: data.mainImage.title,
        mainImageAlt: data.mainImage.altText,
        mainImageWidth: data.mainImageWidth,
        mainImageHeight: data.mainImageHeight,
        defaultImageId: data.defaultImageId
    }
}

module.exports.routeHandler = function(req, res, next) {
    logger.info('the route handler of property controller');
    let listingId = req.urlDetail.listingId,
        projectId = req.urlDetail.projectId,
        propertyId = req.urlDetail.propertyId,
        // query = req.query,
        isMobileRequest = utils.isMobileRequest(req),
        similarPropertiesPromise,
        template = isMobileRequest ? mobileTemplate : desktopTemplate,
        isCommercial = req && req.pageData && req.pageData.isCommercial;

    similarPropertiesPromise = propertyService.getSimilarListings(listingId, { req , isCommercial});

    propertyService.getPropertyDetail({ listingId, projectId, propertyId, isCommercial}, { req }).then((data) => {
        // if (!data.data) {
        //     logger.error('empty response recieved');
        //     return next();
        // }
        var parsedData = propertyParser(data.propertyDetail, req);
        // Adding additional items in page data required for tracking
        req.pageData.listingCategory = parsedData.listingCategory = data.propertyDetail.listingCategory;
        req.pageData.unitType = data.propertyDetail.property.unitType;
        req.pageData.budget = data.propertyDetail.currentListingPrice.price;
        req.pageData.bhk = data.propertyDetail.property.bedrooms;
        req.pageData.projectStatus = (config.dummyStatus.indexOf((data.propertyDetail.property.project.activeStatus||'').toLowerCase())==-1) ? data.propertyDetail.property.project.projectStatus : "N/A";
        req.pageData.sellerId = parsedData.sellerDetails.sellerId;
        req.pageData.furnished = data.propertyDetail.furnished;

        parsedData.sellerDealStatus = {};

        req.pageData.sellerScore = parsedData.sellerDetails.rating;
        req.pageData.listingScore = data.propertyDetail.qualityScore;
        parsedData.galleryContainerData = data.galleryContainerData;

        if (!_isEmptyObject(data.galleryData.data)) {
            parsedData.galleryData = data.galleryData;
        } else {
            parsedData.galleryData = { data: imageParser.parseImages(parsedData.mainImage, isMobileRequest) }; //data.imageDetails.data[0] && data.imageDetails.data[0].src;
        }
        parsedData.mainImage = parsedData.galleryData.data[0];
        parsedData.floorPlanObj = data.floorPlanObj;
        parsedData.listingId = listingId;
        parsedData.isMobileRequest = isMobileRequest;
        parsedData.propertyTypeId = data.propertyDetail.property.unitTypeId;
        parsedData.similarSeller = function(args, callback) {
            propertyService.getSellerDetails({
                params: parsedData.sellerSimilarProp
            }, { req }).then((response) => {
                callback(null, response);
            }, error => {
                callback(error);
            });
        };
        parsedData.similarProperties = function(args, callback) {
            similarPropertiesPromise.then((response) => {
                callback(null, response);
            }, error => {
                callback(error);
            });
        };

        if (parsedData.project.show) {
            parsedData.similarPropertyProj = function(args, callback) {
                similarPropertyService.getApiListings(parsedData.layoutData.pageData, 'project', {req, isCommercial}).then((response) => {
                    callback(null, response);
                }, error => {
                    callback(error);
                });
            };
        }
        parsedData.similarPropertySell = function(args, callback) {
            similarPropertyService.getApiListings(parsedData.layoutData.pageData, 'seller', {req, isCommercial}).then((response) => {
                callback(null, response);
            }, error => {
                callback(error);
            });
        };
        if (isMobileRequest && req.pageData.budget && parsedData.localityDetails && parsedData.city) {
            let budget = req.pageData.budget,
                query = {
                    minBudget: (budget - budget * BUDGET_PERCENTAGE),
                    maxBudget: (budget + budget * BUDGET_PERCENTAGE),
                    localityId: parsedData.localityDetails.localityId
                };
                parsedData.similarLocalityProp = function (args, callback) {
                    propertyService.getSimilarLocalityListings(query, {req}).then((response) => {
                        callback(null, response);
                    }, error => {
                        callback(error);
                    });
                };
                parsedData.carouselImage = Math.floor(Math.random() * 3) + 1;
        }
        parsedData.schemaMap = globalConfig.schemaMap;
        parsedData.longOtherSellersTitle = getOtherSellersTitle(parsedData);
        parsedData.isMobileRequest = isMobileRequest;
        parsedData.homeLoanBlogs = function(args, callback) {
            BlogService.getHomeLoanBlogs(req, callback);
        };
        parsedData.sellerBenefitTags = benefitTagConfig.getSellerBenefitTags(parsedData.sellerDetails.userId, parsedData.overview.listingCategory);
        parsedData.leadBoxData = getLeadBoxData(parsedData);
        template.renderAsync(parsedData, res, "", req);
    }, err => {
        logger.error('inside property controller error: ', err);
        next(err);
    }).catch(err => {
        logger.error('inside property controller catch: ', err);
        next(err);
    });
};
