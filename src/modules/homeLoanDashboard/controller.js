"use strict";

const utils = require('services/utilService'),
    _ = utils._,
    globalConfig = require('configs/globalConfig'),
    viewTemplate = require('services/templateLoader').loadTemplate(__dirname),
    BlogService = require('services/blogService'),
    BankService = require('services/bankService');

function _getBlogData(callback, req, tag) {
    let blogIqUrl = process.env.BASE_URL_MAKAANIQ || '';
    let config = {
            "count": 8,
            "website": "makaaniq"
        };
    if(tag){
        config['tag'] = tag;
    }
    else{
        config['days'] = 30;
    }
    BlogService.getPostList(req, config).then((response) => {
        callback(null, {
            post:response.data,
            makaaniqUrl: blogIqUrl
        });
    }, err => {
        callback(err);
    });
}

function _getBankLists(callback, req){
    BankService.getBankDetails(req).then(function(success){
        callback(null, success);
    },function(err){
        callback(err)
    });
}

module.exports.routeHandler = function(req, res) {
    console.log('inside routeHandler')
    var template =  viewTemplate;
    let data = {
    };
    template.renderAsync({
        data,
        blogDataHomeLoan: function(args, callback) {
            _getBlogData(callback, req,'homeloan');
        },
        getBankDetailList : function(args, callback){
            _getBankLists(callback, req);
        }
    }, res, "", req);
};
