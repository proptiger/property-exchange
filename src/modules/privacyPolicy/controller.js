"use strict";
/**
 * controller setup for 500
 * @param  {Object} router
 */

 const template = require('services/templateLoader').loadTemplate(__dirname);

module.exports.routeHandler = function(req,res) {
    template.renderAsync({}, res, '', req);
};
