"use strict";
//const logger = require('services/loggerService'),
  const  stateService = require('services/stateService'),
    utils = require('services/utilService'),
    imageParser = require('services/imageParser'),
    template = require('services/templateLoader').loadTemplate(__dirname);

function _parseStateData(stateData) {
    stateData = stateData || [];
    for (let i = 0, length = stateData.length; i < length; i++) {
        stateData[i].heroshotMakaanImageUrl = imageParser.appendImageSize(stateData[i].heroshotMakaanImageUrl, 'smallHeroshot');
        stateData[i].url = utils.prefixToUrl(stateData[i].makaanUrl);
    }
    return stateData;
}

module.exports.routeHandler = function(req, res, next) {
    stateService.getAllStateList({needRaw:true},{req}).then(response => {
        if(response && response.data && response.data.length){
            var stateData = _parseStateData(response.data);
            template.renderAsync({stateData}, res , "", req);
        } else{
            next();
        }
    }, err => {
        next(err);
    }).catch(err => {
        next(err);
    });
};
