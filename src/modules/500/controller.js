"use strict";
/**
 * controller setup for 500
 * @param  {Object} router
 */
function _500Handler(req, res){
	let template = require('./view.marko');
	template.render({}, (error, output) => {
		res.send(output);
	});
}

module.exports.setup = function(router) {
    router.get('/500', _500Handler);
};
