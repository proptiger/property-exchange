"use strict";
/**
 * controller setup for tracking leads
 * @param  {Object} router
 */
const utilService = require('services/utilService'),
    apiService = require('services/apiService'),
    apis = require('configs/apiConfig'),
    cityService = require('services/cityService'),
    masterDetailsService = require('services/masterDetailsService'),
    _ = utilService._;

    const config = {
    	required:{
    		'msurl': null,
    		'ctyNm': null,
    		'companyId': null,
    		'nM': null,
    		'eMl': null,
    		'phN': null,
    		'sType': null
    	},
    	optional:{
    		'localityId': null,
    		'projectId': null,
    		'propertyType': null,
    		'budgetFrom': null,
    		'budgetTo': null,
    		'bhk': null,
            'tm': null,
            'mmid':null,
            'mid': null,
            'memid': null
    	},
        debug: {
            'noredirect': false,
            'nolead': false ,
            'showerror': false
        },
    	defaultValue:{
    		'countryId': 1,
    		'domainId': 1,
            'enquiryType': {
                'id': 6
            },
            'sendOtp': false
    	},
    	keyMap:{
    		'companyId': 'multipleCompanyIds',
    		'localityId': 'localityIds',
            'propertyType': 'propertyTypes',
            'budgetFrom': 'minBudget',
            'budgetTo': 'maxBudget',
            'msurl': 'redirectUrl',
            'tm': 'testmode',
            'nM': 'name',
            'eMl': 'email',
            'phN': 'phone',
            'sType': 'salesType',
            'ctyNm': 'cityName',
            'mmid': {
                'name': 'mailerResponseMasterId',
                'type': 'object',
                'typeName': 'jsonDump'
            },
            'mid': {
                'name': 'mailerID',
                'type': 'object',
                'typeName': 'jsonDump'
            },
            'memid': {
                'name': 'memberId',
                'type': 'object',
                'typeName': 'jsonDump'
            }
    	},
    	allowedMultipleValue:['companyId', 'localityIds', 'propertyTypes'],
    	manualFields: ['cityId', 'propertyTypes', 'jsonDump', 'multipleCompanyIds', 'cityName', 'salesType'],
        skipCasing: ['msurl'],
        salesTypeMap:{
            "rent": [2,3]
        }
    };

function getPropertyTypeByString(str){
    let unitTypes = masterDetailsService.getMasterDetails("unitTypes"),
    result;
    _.forEach(unitTypes, (v, k) => {
        if(v.toLowerCase() === str.toLowerCase()){
            result = {
                name: v,
                id: k
            };
            return;
        }
    });
    return result;
}

function _prune(obj, val){
    if(!val){
    	for (let i in obj) {
    		if (obj[i] === null || obj[i] === undefined) {
    			delete obj[i];
    		}
    	}
    }else if(_.isString(val)){
        delete obj[val];
    }else if(_.isArray(val)){
        _.forEach(val, (v)=>{
            delete obj[v];
        });
    }else if(_.isPlainObject(val)){
        _.forEach(val, (v, k)=>{
            delete obj[k];
        });
    }
}

function _updateDetailsFromUrl(required, obj, query) {
    let temp = {};
    _.forEach(obj, (val, key) => {
        if (query[key]) {
            temp[key] = query[key].indexOf(',') === -1 ? query[key] : query[key].split(',');
        } else if (required) {
            let err = new Error("Provide relevant parameters. Missing "+ key);
            err.status = 500;
            throw err;
        } else {
            temp[key] = val;
        }
    });
    return temp;
}

function _updateApiKeys(details, map){
    for (let i in map) {
        if (details[i]) {
            let key = map[i];
            if(_.isPlainObject(key)){
                if(key.type === 'array' && key.typeName){
                    details[key.typeName] = details[key.typeName] || [];
                    details[key.typeName].push(details[i]);
                }else if(key.type === 'object' && key.typeName){
                    details[key.typeName] = details[key.typeName] || {};
                    details[key.typeName][key.name] = details[i];
                }else{
                    details[key.name] = details[i];
                }
            }else{
                details[key] = details[i];
            }
			delete details[i];
		}
	}

}

function _updateManualFields(details, fields){
	for(let i=0; i<fields.length; i++){
               if(fields[i] == 'cityId'){
			details.cityId = cityService.getCityIdByName(details.cityName);
		}else if(fields[i] == 'cityName'){
            details.cityName = utilService.toTitleCase(details.cityName);
        }else if(fields[i] == 'propertyTypes'){
            let types = [],type;
            if(_.isArray(details[fields[i]])){
                _.forEach(details[fields[i]], (v)=>{
                    if(type = getPropertyTypeByString(v)){
                        types.push(type.name);
                    }
                });
                details.propertyTypes = types;
            }else{
                if(type = getPropertyTypeByString(details[fields[i]])){
                    details.propertyTypes = [type.name];
                }else{
                    details.propertyTypes = [];
                }
            }
            if(!details.propertyTypes.length) {delete details.propertyTypes;}
        }else if(fields[i] == 'jsonDump'){
            details[fields[i]] = JSON.stringify(details[fields[i]]);
        }else if(fields[i] == 'multipleCompanyIds'){
            if(!_.isArray(details[fields[i]])){
                details[fields[i]] = [details[fields[i]]];
            }
        }else if(fields[i] == 'salesType'){
            // if(['rent','rental'].indexOf(details[fields[i]]) !== -1)
            if(config.salesTypeMap["rent"].indexOf(details[fields[i]]) !== -1){
                details[fields[i]] = 'rent';
            }else{
                details[fields[i]] = 'buy';
            }
        }
	}
}

function _decode(details){
    _.forEach(details, (v, k)=>{
        //let z = new Buffer('apartment').toString('base64');
        if(config.optional[k] !== undefined|| config.required[k] !== undefined){
            details[k] = new Buffer(v, 'base64').toString('ascii');
            details[k] = config.skipCasing.indexOf(k) !== -1 ? details[k] : details[k].toLowerCase();
        }
    });
}

function _updateTestModeDetails(details){
    details.multipleCompanyIds = ['508578'];
    delete details.testmode;
}

function _setDebugFlags(details){
    let result = {};
    _.forEach(config.debug, (v, k)=>{
        result[k] = details[k];
    });
    return result;
}

function _redirect({debug, response, res, payload, redirectUrl}){
    if(debug.noredirect){
        let data = {
            response,
            payload,
            redirectUrl
        };
        res.send(data);
    }else{
        res.redirect(301, redirectUrl);
    }
}

module.exports.routeHandler = function(req, res, next) {
	let query = req.query,
		urlDetails = {},
		urlConfig, redirectUrl, debug;
		req.headers['content-type'] = 'application/json';

        //for tracker cookie
            req.headers['cookie'] =res.getHeader('Set-Cookie');
        // end
        try{
            urlDetails = _.assign(urlDetails, _updateDetailsFromUrl(true, config.required, query, next));
            urlDetails = _.assign(urlDetails, _updateDetailsFromUrl(false, config.optional, query, next));
            urlDetails = _.assign(urlDetails, config.defaultValue);
            urlDetails = _.assign(urlDetails, _updateDetailsFromUrl(false, config.debug, query, next));
            _prune(urlDetails);
            _decode(urlDetails);
            _updateApiKeys(urlDetails, config.keyMap);
            _updateManualFields(urlDetails, config.manualFields);
            if(urlDetails.testmode && (urlDetails.testmode.toLowerCase() == 'on' || urlDetails.testmode.toLowerCase() == 'true')){
                _updateTestModeDetails(urlDetails);
            }else{
                delete urlDetails.testmode;
            }

            redirectUrl = urlDetails.redirectUrl;

            debug = _setDebugFlags(urlDetails);

            _prune(urlDetails, 'redirectUrl');
            _prune(urlDetails, config.debug);

            urlConfig = apis.enquiries({
                    query: {
                        debug: debug.showerror
                    }
                });
            if(debug.nolead){
                res.send({payload:urlDetails,redirectUrl:redirectUrl});
            }else{
                apiService.post(urlConfig, {
                    body: urlDetails,
                    req: req,
                    res: res
                }).then((response) => {
                    _redirect({debug, response, res, payload:urlDetails, redirectUrl});
                }, err => {
                    _redirect({debug, response:err, res, payload:urlDetails, redirectUrl});
                }).catch(err => {
                    _redirect({debug, response:err, res, payload:urlDetails, redirectUrl});
                });
            }
        }catch(ex){
            next(ex);
        }
};
