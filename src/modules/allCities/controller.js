"use strict";
//const logger = require('services/loggerService'),
const async = require('async'),
    globalConfig = require('configs/globalConfig'),
    apiConfig = require('configs/apiConfig'),
    apiService = require('services/apiService'),
    seoService = require('services/seoService'),
    cityService = require('services/cityService'),
    utils = require('services/utilService'),
    imageParser = require('services/imageParser'),
    template = require('services/templateLoader').loadTemplate(__dirname);

function _parseCityData(cityData) {
    cityData = cityData || [];
    for (let i = 0, length = cityData.length; i < length; i++) {
        cityData[i].cityHeroshotImageUrl = imageParser.appendImageSize(cityData[i].cityHeroshotImageUrl, 'smallHeroshot');
    }
    return cityData;
}

module.exports.routeHandler = function(req, res, next) {
    let { mapTypeIdToPageType } = seoService,
        query = req.query || {},
        currentPage = parseInt(query.page) || 1,
        rows = globalConfig.panIndiaPageRowCount,
        start = (currentPage - 1) * rows,
        currentUrl = req.urlDetail.url,
        currBaseUrl = req.urlDetail.currBaseUrl,
        templateId = req.urlDetail.templateId,
        countryId = req.urlDetail.countryId || (req.locals.numberFormat && req.locals.numberFormat.code) || 1,
        totalPages = 0,
        urlType, cityId, cityName;

    //buy or rent url
    if (templateId.indexOf('RENT') > -1) {
        urlType = 'rentUrl';
    } else if (templateId.indexOf('BUY') > -1) {
        urlType = 'buyUrl';
    } else if (templateId.indexOf('OVERVIEW') > -1) {
        urlType = 'overviewUrl';
    }

    //city specific of pan india url
    if (templateId.indexOf('CITY') > -1) {
        cityId = req.urlDetail.cityId;
        cityName = req.urlDetail.cityName;
    }

    async.auto({
        city: function (callback){
            cityService.getAllCityList({
                rows, start, countryId, needRaw:true
            },{req}).then(response => {
                if(response && response.data && response.data.length){
                    callback(null, {
                        cityData: _parseCityData(response.data),
                        totalPages: Math.ceil(response.totalCount / rows)
                    })
                } else {
                    callback(null, {cityData: null, totalPages: 0})
                }
            }, err => {
                callback(err);
            }).catch(err => {
                callback(err);
            });
        },
        budget: function (callback) {
            if(urlType == 'overviewUrl') {
                let pageObjectIds = [];
                pageObjectIds.push(mapTypeIdToPageType["city"]);
                let urlConfig = apiConfig.getBudgetUrls({
                    objectTypeId: pageObjectIds.join(','), 
                    pageNumber: currentPage, 
                    pageSize: rows
                });
                return apiService.get(urlConfig,{req}).then((response) => {
                    callback(null, {
                        budgetUrls: response && response.data,
                        totalPages: Math.ceil(response.totalCount / rows) || 0
                    });
                }, err => {
                    callback(err);
                });
            } else {
                callback(null, {budgetUrls:null, totalPages: 0})
            }
        }
    }, function(err, results) {
        if(err){
            return next(err);
        }
        let budgetUrls = utils.isEmptyObject(results.budget.budgetUrls)?null:results.budget.budgetUrls,
            cityData = utils.isEmptyObject(results.city.cityData)?null:results.city.cityData,
            totalPages = Math.max(results.city.totalPages,results.budget.totalPages),
            paginationData = { "totalPages": totalPages, "currentUrl": currentUrl, "currentPage": currentPage, "changeQueryParam": utils.changeQueryParam, "filter": true };
        req = utils.createSeoPrevNext(currentPage, totalPages, currBaseUrl, req);
        if (cityData || budgetUrls) {
            template.renderAsync({
                paginationData,
                cityData,
                budgetUrls,
                cityName
            }, res, '', req);
        } else if (currentPage>1) {
            res.redirect(302, '/'+currentUrl);
        } else {
            res.status(301);
            next();
        }
    })

    
};
