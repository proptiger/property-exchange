"use strict";
const template = require('services/templateLoader').loadTemplate(__dirname),
    utilService = require('services/utilService');

module.exports.routeHandler = function(req, res, next) {
    let widgetName = req.params.widgetName;
    if (widgetName) {
        template.renderAsync({
            widgetName
        }, res, '', req);
    }else{
    	next(utilService.getError(400));
    }
};
