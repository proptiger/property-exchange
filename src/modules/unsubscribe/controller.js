"use strict";
const logger = require('services/loggerService'),
	template = require('services/templateLoader').loadTemplate(__dirname);

module.exports.routeHandler = function(req, res) {
	logger.info('inside the un-subscribe route');

	let query = req.query || {},
		unsubscribeKey = query.unsubscribeKey;

	template.renderAsync({unsubscribeKey}, res, '', req);
};
