"use strict";
const template = require('services/templateLoader').loadTemplate(__dirname);
const url = require('url'),
    apiService = require('services/apiService'),
    apiConfig = require('configs/apiConfig');

module.exports.routeHandler = function(req,res) {
    apiService.get(apiConfig.checkLogin(), {req, res})
    .then(response => {
        //checking if user if seller, then redirect, else render
        var upperCaseRoles = [];
        if(response && response.data){
            upperCaseRoles = response.data.roles.map(function(value) {
              return value.toUpperCase();
            });
        }
        if(upperCaseRoles.indexOf("CRM USER") > -1 && upperCaseRoles.indexOf("SELLER") > -1) {
            var urlHref = '/postProperty';
            if (req.query) {
                let i = req.url.indexOf('?');
                let queryStr = i > -1 ? req.url.substr(i + 1) : '';
                if (queryStr) {
                    urlHref += '?' + queryStr;
                }
            }
            return res.redirect(302, url.resolve(process.env.BASE_URL_SELLER,urlHref));
        } else {
            throw new Error("Non-Seller User.");
        }
    }, reject => {
        //show page
        var data = {
            overviewData: `As Makaan.com grows day by day as a property listing website and marketplace, we are here to help our partner sellers to make the most of this opportunity. Have you been thinking about how to sell a house or the best website for house rentals? Your confusion ends here. All you need to do is to join Makaan.com and list on this free property listing site. It wouldn’t take much of your time either.  All you need to do is list property.  If you are keen on selling a home, upload all the details of the properties online on the  perfect home listing website to get maximum leads. Post free property ads online on Makaan.com if you are looking to post house for rent.  You can post a complete property list that you may have because yes, it is unlimited.<br/><br/>If you have posted high-quality listings with full information of your property, you are sure to attract buyers and tenants because there are prospective customers browsing real estate listing websites and property advertisements to arrive at their dream home. Ensuring maximum visibility is therefore in your own hands. Anyone, whether you are a broker, builder, owner looking to sell house online, Makaan.com seller platform is for you to benefit the most.<br/><br/>We also have unique rewards for you! You could be highlighted as a city expert or locality expert on the basis of deals that you are able to close on Makaan.com. This is also your chance to get your property listing focussed to ensure maximum visibility among genuine buyers and tenants. So start posting listings for renting out a property. If you are looking forward for home sale online then Makaan.com is the only marketplace where you get to gain more than you think.<br/><br/>Makaan.com is the easiest way you can opt to sell property or rent a house. If you were looking at free property listing sites, worry no more as Makaan.com ensures that all you need to do is sit back comfortably while we help you reach your audience and sell property online. Enjoy a whole new experience because Makaan.com is not just any other property listing site! In your success, lies our success.`,
            featuredData: `We have also introduced Seller Score.  Seller score is dependent on crucial parameters based on your performance like Property options and their quality, Response to buyers, Deals closed by you and the customer feedback received in past. Seller score unites the buyers and sellers int he marketplace.<br/><br/>Get more leads on properties by improving your seller score. High seller score attracts more customers to finalize property with you as high seller score denotes your high credibility.<br/><br/>Sellers should improve their seller score by improving your listings and adding more listings, responding to more leads quickly, disclosing the deals closed and getting good feedback from buyers.`,
            listproperty: `If you are selling a house or keen to rent a house, Makaan.com is where you should be.  When you open our site, continue to ‘list property’ option to start with property listings. If you are an existing user, you could just login with your credentials. Select your city where you wish to sell flat or post ad for rent. Select your profile as a ‘owner’, ‘agent’ or ‘builder’. You could log in with your Facebook profile, Google Plus or email id.  You will be asked to register your mobile number. Proceed with OTP verification for a safe and reliable way of buying and selling property online. Post properties with complete details and pictures to get maximum responses. On the basis of your listing quality, a rating will be generated which only you can see. A good quality property listing will help you rent or sell property fast. Upon verification, your property listing will be visible to all potential buyers and tenants. You can keep a tab on all your listings at any given point of time.`,
            genuinebuyers: `Go to the enquiries icon to see your enquiry management page.  You will be able to see how many genuine customers are interested in your property listings. You could get in touch with interested home buyers or tenants looking at your property advertisement. Basis your conversation with them, you could also categorise them. Makaan.com seller features also lets you access audio recordings of your conversation with buyers/tenants for your reference. We are sure you would be able to sell your house fast or rent your property in no time!`,
            sales: `The new and improved Makaan seller features allow you to post unlimited property listings. Yes, you can avail of free property listing at any given point of time!<br/><br/>We have also introduced the Seller Score so that sellers like you who are selling property and looking to sell quick or looking to post property for rent are able to win buyers or tenants and be rewarded! It is a performance based score given to each seller on makaan. Seller score is dependent on crucial parameters based on your performance like Property options and their quality, Response to buyers, Deals closed by you and the customer feedback received in past. Seller score unites the buyers and sellers in the marketplace. Buyers will then be able to see your high-quality listings of sellers with best seller score on top which increases your prospects of closing deals. In short, faster sales!<br/><br/>Be assured of the superior quality of leads. The volume of quality real estate listings, your quick response to a potential property buyer, the number of deals closed and the customer’s feedback about their interaction with you is also considered to come up with the Seller Score.`,
            payearn: `Selling real estate is no easy job so now you also have the benefit of paying as you earn. Makaan.com is your true partner because we succeed only if you do. We told you, we are not just any property selling website.`
        }
        template.renderAsync(data, res, '', req);
    }).catch(error => {
        template.renderAsync(error, res, '', req);
    });
};
