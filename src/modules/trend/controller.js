"use strict";
/**
 * controller setup for tracking leads
 * @param  {Object} router
 */
const utilService = require('services/utilService'),
    logger = require('services/loggerService'),
    localityService = require('services/localityService'),
    cityService = require('services/cityService'),
    projectService = require('services/projectService'),
    globalConfig = require('configs/globalConfig').templateId,
    template = require('services/templateLoader').loadTemplate(__dirname);

function _getConfiguration(req) {
    req.urlDetail = req.urlDetail || {};
     let countryId = req.urlDetail.filters && req.urlDetail.filters.countryId || req.urlDetail.countryId || 1,
        cityId = req.urlDetail.cityId,
        cityName = req.urlDetail.cityName,
        localityId = req.urlDetail.localityId,
        localityName = req.urlDetail.localityName,
        suburbId = req.urlDetail.suburbId,
        suburbName = req.urlDetail.suburbName,
        pageLevel = req.urlDetail.pageLevel,
        isRent = req.urlDetail.listingType == 'rent' ? true : false,
        currBaseUrl = req.urlDetail.canonicalUrl && req.urlDetail.canonicalUrl.replace(/(\?.*)|(#.*)/g, ""),
        currentPage = parseInt(req.query.page) || 1,
        currentUrl = req.urlDetail.url,
        rows = cityId ? 60 : 1000,
        start = (currentPage - 1) * rows,
        urlLabel = req.meta.urlLabel;
    return {
        countryId,
        cityId,
        localityId,
        suburbId,
        cityName,
        localityName,
        suburbName,
        pageLevel,
        isRent,
        currBaseUrl,
        currentUrl,
        currentPage,
        rows,
        start,
        urlLabel
    };
}

module.exports.routeHandler = function(req, res, next) {
    var config = _getConfiguration(req);
    let paginationData = {},
        currentPage = config.currentPage,
        currentUrl = config.currentUrl,
        currBaseUrl = config.currBaseUrl,
        totalPages;

    function _getPaginationData() {
        paginationData = {
            "totalPages": totalPages,
            "currentUrl": currentUrl,
            "filter": true,
            "currentPage": currentPage
        };
        paginationData.changeQueryParam = utilService.changeQueryParam;
        
        return paginationData;
    }

    function _getProjectTrend(callback) {
        projectService.getTopProjectPriceRange(config, { req }).then((response) => {
            response = response || {};
            callback(null, {
                data: response.data || [],
                config
            });
        }, err => {
            logger.error('inside trend controller error: ', err);
            callback(err);
        }).catch(err => {
            logger.error('inside trend controller catch: ', err);
            callback(err);
        });
    }

    function _sendResponse(){
        let getData;
        if (config.cityId) {
            getData = localityService.getAllLocalityWithPriceTrendUrl;
            config.templates = {
                buy: globalConfig.localityPriceTrend.buy,
                rent: globalConfig.localityPriceTrend.rent
            };
        } else {
            getData = cityService.getAllCityWithPriceTrendUrl;
            config.templates = {
                buy: globalConfig.cityPriceTrend.buy,
                rent: globalConfig.cityPriceTrend.rent
            };
        }

        getData(config, { req }).then((response) => {
            response = response || {};
            totalPages = Math.ceil(response.totalCount / config.rows);
            template.renderAsync({
                data: response.data || [],
                totalListingCountRent: response.totalListingCountRent || 0,
                totalListingCountBuy: response.totalListingCountBuy || 0,
                paginationData: _getPaginationData(),
                config,
                asyncProjectData: function(args, callback) {
                    _getProjectTrend(callback);
                },
                asycSeoData: function(args, callback) {
                    let seoPrevNext ={};
                    callback(null, {
                        seoPrevNext: utilService.createSeoPrevNext(currentPage, totalPages, currBaseUrl, seoPrevNext)
                    });
                },
                formatPrice: function(val){
                    return utilService.formatNumber(val, {
                        type : 'number', 
                        seperator : req.locals.numberFormat
                    });
                }
            }, res, "", req);
        }, err => {
            logger.error('inside trend controller error: ', err);
            next(err);
        }).catch(err => {
            logger.error('inside trend controller catch: ', err);
            next(err);
        });
    }

    _sendResponse();
};
