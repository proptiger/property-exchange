"use strict";

let template = require('services/templateLoader').loadTemplate(__dirname, 'gurgaon-signature-signum.marko');

function routeHandler(req, res, next) {
    template.renderSync({}, res, null, null, req);
}

module.exports = {
    routeHandler
};
