"use strict";

const utils = require('services/utilService'),
    desktopTemplate = require('services/templateLoader').loadTemplate(__dirname, 'view-v2.marko'),
    mobileTemplate = require('services/templateLoader').loadTemplate(__dirname, 'view-mobile-v2.marko'),
    BlogService = require('services/blogService'),
    cityService = require('services/cityService');

function _getBlogData(callback, req) {

    let blogIqUrl = process.env.BASE_URL_MAKAANIQ || '', 
        configHome = {
            "tag": "home",
            "count": 3,
            "website": "makaaniq"
        },
        configRecent = {
            "count": 6,
            "days": 20,
            "website": "makaaniq"
        };
    if(req.locals.numberFormat && req.locals.numberFormat.code) {
      configHome.website = configRecent.website = "iq" + req.locals.numberFormat.code;
      blogIqUrl = blogIqUrl.replace(/\/iq\//,'/'+req.locals.numberFormat.code+'/iq/');
    }
    Promise.all([BlogService.getPostList(req, configHome), BlogService.getPostList(req, configRecent)]).then((response) => {
        callback(null, {
            home: response[0].data.concat(response[1].data),
            makaaniqUrl: blogIqUrl
        });
    }, err => {
        callback(err);
    });
}

module.exports.routeHandler = function(req, res) {

    var template =  utils.isMobileRequest(req) ? mobileTemplate : desktopTemplate;

    let data = {
        blogData: function(args, callback) {
            _getBlogData(callback, req);
        },
        defaultCity: {
            id:18,
            name:'Mumbai'
        },
    };

    template.renderAsync(data, res, "", req);    
};
