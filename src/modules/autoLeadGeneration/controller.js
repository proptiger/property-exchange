"use strict";

const utils = require('services/utilService'),
    _ = utils._,
    globalConfig = require('configs/globalConfig'),
    viewTemplate = require('services/templateLoader').loadTemplate(__dirname),
    projectService = require('services/projectService'),
    propertyService = require('services/propertyService'),
    agentService = require('services/agentService'),
    mappingService = require('services/mappingService'),
    maps = require('services/propertyMappingService'),
    logger = require('services/loggerService'),
    moment = require('moment'),
    _isEmptyObject = utils.isEmptyObject;


function parseTopAgent(result){
    let data = {};
    data.companyId = result.id;
    data.companyName = result.name;
    data.companyPhone = result.contact;
    data.companyUserId = result.userId;
    data.companyType = result.type;
    return data;
}

function getSellersForLead(data,req){
    if(data.sellerId){
        return agentService.getMultipleFeaturedSellers([data.sellerId], { req }).then(topAgents => {
            return topAgents.length ? parseTopAgent(topAgents[0]) : {};
        }, (err) => {
            return {};
        });;
    }else{
       return projectService.getPaidSellers({
            cityId: data.city.id,
            projectId: data.projectId,
            listingCategory: 'Primary,Resale',
        }, { req }).then(topAgents => {
            return topAgents.length ? parseTopAgent(topAgents[0]) : {};
        }, (err) => {
            return {};
        });
    }
}
 
function _getOverviewKeys(data) {
    var keyArray, length, i,
        resultObj = {};
    keyArray = Object.keys(data);
    length = keyArray.length;
    for (i = 0; i < length; i++) {
        let val = data[keyArray[i]];
        resultObj[keyArray[i]] = {
            key: maps.getLanguageText(keyArray[i]),
            val: val,
            title: val && val.toString().replace(/<.*>/g, '')
        };
    }
    return resultObj;
}

function _getOverViewDetails(data){
    let propertyDetails = {
        'status': mappingService.getPropertyStatus(data.constructionStatusId),
        'area': data.area.size + ' ' + data.area.units,
        'category': maps.getListingCategory(data.listingCategory),
        'floor': data.tower.floor ? utils.ordinalSuffix(data.tower.floor) + (data.tower.totalFloors ? ` <span class="grey">of ${data.tower.totalFloors}</span>` : '') : undefined,
        'bath': data.rooms.bathrooms,
        'furnishStat': data.furnishStat,
        'totalFloor': data.tower.totalFloors,
        'unitType': data.area.unitType,
        'carpetArea': data.propertyDetails.carpetArea
    }
    propertyDetails.posession = (data.constructionStatusId == 1) ? utils.formatDate(data.possessionDate, 'SS YY') : undefined;
    propertyDetails.availability = data.possessionDate ? (data.possessionDate > Date.now() ? `from ${utils.formatDate(data.possessionDate, 'SS YY')}` : 'immediate') : undefined;
    if (data.constructionStatusId == 2 && (data.propertyDetails.minAge || data.propertyDetails.maxAge)) {
        let minAge = data.propertyDetails.minAge || data.propertyDetails.maxAge, //incase minAge is missing
            totallMonths = moment().diff(minAge, 'months'),
            years = Math.floor(totallMonths / 12),
            months = totallMonths % 12;
        propertyDetails.age = years;
        if (months || (years === 0)) { // years == 0 is the case if property is some days old
            propertyDetails.age += ` - ${years+1}`;
        }
        propertyDetails.age += (years === 0 || (years == 1 && !months)) ? ' year' : ' years';
    }
    propertyDetails = _getOverviewKeys(propertyDetails);
    propertyDetails = propertyService.sortByMap(propertyDetails, data.type, data.category, 'overview');
    propertyDetails.firstFold = propertyDetails.splice(0, 3);
    return propertyDetails;
}

function propertyParser(data, req){
    let result = {};
    let companySeller = data.companySeller,
        priceDetails = data.currentListingPrice,
        property = data.property,
        project = property ? property.project : {},
        locality = project ? project.locality : {},
        builder = project ? project.builder : {};
    if (companySeller){
        let company = companySeller.company || {},
            user = companySeller.user || {};

            result.propertyUrl = data.resaleURL;
            result.listingCategory = data.listingCategory;
            result.furnished = data.furnished;
            result.floor = data.floor;
            result.status = data.status;
            result.possessionDate = data.possessionDate;

            result.sellerDetails = {};
            result.sellerDetails.companyName = company.name;
            let sellerNumbers = user.contactNumbers;
            if (sellerNumbers && sellerNumbers.length) {
                result.sellerDetails.companyPhone = sellerNumbers[0].contactNumber;
            } else {
                result.sellerDetails.companyPhone = 'n.a.';
            }
            result.sellerDetails.companyId = company.id;
            result.sellerDetails.companyUserId = user.id;
            result.sellerDetails.companyType = company.type;
            if (company.type === "BROKER") {
                result.sellerDetails.companyType = 'AGENT';
            }

            if (!_isEmptyObject(locality)) {
                result.locality = {};
                result.locality.id = locality.localityId;
                result.locality.name = locality.label;
                if (locality.suburb && locality.suburb.city) {
                    result.city = {};
                    result.city.name = locality.suburb.city.label;
                    result.city.id = locality.suburb.city.id;
                }
            }
      
            if(!_isEmptyObject(builder)){
                result.builder = {};
                result.builder.name = builder.name;
                result.builder.displayName = builder.displayName;
            }

            if (!_isEmptyObject(priceDetails)) {
                result.priceDetails = {};
                result.priceDetails.price = priceDetails.price;
                result.priceDetails.pricePerUnitArea = priceDetails.pricePerUnitArea;
            }

            result.area = {};
            result.propertyDetails = {};
            result.rooms = {};
            result.tower = {};

            if (!_isEmptyObject(property)) {
                result.rooms.bathrooms = property.bathrooms || undefined;
                result.propertyDetails.carpetArea = property.carpetArea ? property.carpetArea + ' ' + property.measure : null;
                result.area.unitType = property.unitType;
            }

            result.area.unitTypeId = property.unitTypeId;
            result.area.unitName = property.unitName;
            result.area.size = property.size;
            result.area.units = property.measure;
            result.status = data.status;
            result.propertyDetails.minAge = data.minConstructionCompletionDate;
            result.propertyDetails.maxAge = data.maxConstructionCompletionDate;
            result.tower.floor = data.floor === 0 ? 'Gr' : data.floor;
            result.tower.totalFloors = data.totalFloors;
            result.furnishStat = mappingService.getFurnishingStatus(data.furnished) || data.furnished;
            result.type = maps.getPropertyType(data.property.unitTypeId) || '';
            result.constructionStatusId = data.constructionStatusId;

            result.listingId = data.id;
            result.propertyType = maps.getPropertyType(property.unitTypeId) || '';
            result.mainImage = data.mainImage ? data.mainImage.absolutePath : data.mainImageURL;
            result.propertyTypeId = property.unitTypeId
            result.bhk = property.bedrooms;
            result.size = result.area.size + ' ' + result.area.units;
            result.priceDetails.price = utils.formatNumber(result.priceDetails.price, { 
                precision : 2, 
                returnSeperate : true,
                seperator : req.locals.numberFormat
            });

            let overviewData = _getOverViewDetails(result);

            if(!_isEmptyObject(project)){
                result.projectDetail = {};
                result.projectId = project.projectId;
                result.projectDetail.id = project.projectId;
                result.projectDetail.title = project.name;
                result.projectDetail.possessionDate = project.possessionDate;
                result.projectDetail.keyDetails = overviewData.firstFold;
            }

            let _bhkString = (property.rk) ? "RK" : "BHK",
            _unitTypeString = result.propertyType;

            if (property.studio) {
                _unitTypeString = "studio apartment";
            }

            if (property.penthouse) {
                _unitTypeString = "penthouse";
            }
            result.title = (property.unitType !== 'Plot' ? (`${property.bedrooms} ${_bhkString}`) : '') + ` ${_unitTypeString}`;
            result.propertyUrl = data.resaleURL;
            return result;
    }
}

module.exports.routeHandler = function(req, res, next) {
    var template =  viewTemplate;
    let isMobileRequest = utils.isMobileRequest(req);
    if(req.query.projectId){
        let projectId = req.query.projectId, projectDetail;
        projectDetail = projectService.getProjectDetails(projectId, undefined, {req});
        projectDetail.then(parsedData => {
            var data = parsedData.projectDetail;
            data.showbutton = req.query.showbutton === "true" ;
            data.projectId = projectId;
            data.sellerId = req.query.sellerid;
            data.isMobileRequest = isMobileRequest;
            let isProjectFeatured = data.projectDetail && (data.projectDetail.isProjectFeaturedBuy || data.projectDetail.isProjectFeaturedRent);
            data.priceDetails = {};
            data.priceDetails.minPrice = data.projectDetail.minPriceNum;
            data.priceDetails.maxPrice = data.projectDetail.maxPriceNum;
            data.projectUrl = data.projectDetail.projectUrl;
            getSellersForLead(data,req).then(topAgent=>{
                data.topAgent = topAgent;
                template.renderAsync(data, res, "", req);
            });        
        }, err => {
            next(err);
        }).catch(err => {
            next(err);
        });
    } else if(req.query.listingId) {
        let listingId = req.query.listingId;
        propertyService.getPropertyDetail({listingId}, {req}).then(data => {
            var parsedData = propertyParser(data.propertyDetail, req);
            parsedData.priceDetails.minPrice = 0;
            parsedData.showbutton = req.query.showbutton === "true" ;
            parsedData.priceDetails.maxPrice = parsedData.priceDetails.price;
            parsedData.isMobileRequest = isMobileRequest;
            template.renderAsync(parsedData, res, "", req);
        },(err) => {
            next(err);
        }).catch(err => {
            next(err);
        });

    } else {
        next(new Error());
    }
};
