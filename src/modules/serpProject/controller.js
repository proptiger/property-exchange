"use strict";

const projectSerpService = require('services/projectSerpService');
const utils = require('services/utilService');
const localityService = require('services/localityService');
const builderService = require('services/builderService');
const globalConfig = require('configs/globalConfig');
const logger = require('services/loggerService');


function _checkListingAvailable(data) {
    let available = false,
        field = "propertyForSale";

    utils._.forEach(data, (v) => {
        if (v[field] > 0) {
            available = true;
            return;
        }
    });
    return available;
}


function setPaginationData(req, totalCount) {
    let paginationData = {},
        projectPerPage = globalConfig.projectSerpItemsPerPage;
    if (totalCount && totalCount > 0) {
        paginationData.totalPages = Math.ceil(totalCount / projectPerPage);
    } else {
        paginationData.totalPages = 1;
    }
    paginationData.currentUrl = req.url;

    paginationData.filter = true;

    var currentPage = req.query.page;
    if (!currentPage) {
        currentPage = 1;
    }
    paginationData.currentPage = currentPage;
    paginationData.changeQueryParam = utils.changeQueryParam;
    return paginationData;
}

function routeHandler(req, res, next) {

    let showBackButtonInFilter = req.showBackButtonInFilter || false;

    let clientXHR = (req.xhr && req.query.format === 'json') ? true : false,
        projectData, snippetData, projectCount, totalCount, seoPrevNext = {},
        //paginationData,
        projectAPIPromise = projectSerpService.getProjectsData(req.query, req.urlDetail, { req });

    function _getRenderingData(callback) {
        projectAPIPromise.then((response) => {
            let selector = response.selector;
            let paginationData;
            projectData = response.data || [];
            totalCount = response.totalCount || 0;

            let conditionalObj = {
                h1Title: req.urlDetail.h1Title || '',
                h2Title: req.urlDetail.h2Title || '',
                mobileView: utils.isMobileRequest(req) ? true : false,
                showTopAgentCard: req.urlDetail.cityId ? true : false
            };

            if (!req.urlDetail.cityId) {
                conditionalObj.hidePyrCard = true;
            }
            //not showing map on project serp
            conditionalObj.mapCard = false;

            if ( req.urlDetail.templateId && req.urlDetail.templateId.indexOf('BUILDER') != -1) {
                conditionalObj.builderView = true;
                conditionalObj.builderData = builderService.parseBuilderData(req.data.builder, 100);
            }

            let pageLevel = req.urlDetail && req.urlDetail.pageLevel;

            if (pageLevel && projectData && projectData.length) {
                logger.info('pageLevel', pageLevel);
                let project = projectData[0] || {};
                let city = project.city || {};
                let locality = project.locality || {};
                let suburb = project.suburb || {};
                if (['city', 'citytaxonomy'].indexOf(pageLevel.toLowerCase()) !== -1) {
                    conditionalObj.knowMoreUrl = city.overviewUrl;
                    conditionalObj.knowMoreName = req.urlDetail.cityName || city.name;
                } else if (['suburb', 'suburbtaxonomy'].indexOf(pageLevel.toLowerCase()) !== -1) {
                    conditionalObj.knowMoreUrl = suburb.overviewUrl;
                    conditionalObj.knowMoreName = req.urlDetail.suburbName || suburb.name;
                    conditionalObj.knowMoreName += req.urlDetail.cityName || city.name ? `, ${req.urlDetail.cityName || city.name}` : '';
                } else if (['locality', 'localitytaxonomy'].indexOf(pageLevel.toLowerCase()) !== -1) {
                    conditionalObj.knowMoreUrl = utils.prefixToUrl(locality.overviewUrl);
                    conditionalObj.knowMoreName = req.urlDetail.localityName || locality.name;
                    conditionalObj.knowMoreName += req.urlDetail.cityName || city.name ? `, ${req.urlDetail.cityName || city.name}` : '';
                }
            }
            projectCount = response.totalCount;
            paginationData = setPaginationData(req, totalCount);

            if (req.seoAPI && paginationData.currentPage && paginationData.totalPages && paginationData.currentPage <= paginationData.totalPages) {
                let currentPage = parseInt(paginationData.currentPage),
                    totalPages = parseInt(paginationData.totalPages),
                    currBaseUrl = req.urlDetail.currBaseUrl;
                
                seoPrevNext = utils.createSeoPrevNext(currentPage, totalPages, currBaseUrl, seoPrevNext)
            }

            let similarCard = {
                similarProvider: undefined,
                similarTitle: undefined
            };
            if (!conditionalObj.mobileView && pageLevel && pageLevel.toLowerCase() === "city") {
                let project = projectData[0] || {};
                let cityName = project.city && project.city.name || '';
                similarCard.similarProvider = (function() {
                    return localityService.getTopRankedLocalities(req.urlDetail.cityId, "buy", { filters: selector.filters, paging: { rows: 4 } }, { req }).then((response) => {
                        return {
                            data: response,
                            showSection: _checkListingAvailable(response),
                            similar: "topLocality",
                            schema: "Place"
                        };
                    });
                })();
                similarCard.similarTitle = "Top localities in " + cityName;
            } else if (!conditionalObj.mobileView && pageLevel && pageLevel.toLowerCase() === "locality") {
                let localityId = req.urlDetail && req.urlDetail.localityId;
                if (localityId && conditionalObj.latitude && conditionalObj.longitude) {
                    similarCard.similarProvider = (function() {
                        return localityService.getNearbyLocalities(conditionalObj.latitude, conditionalObj.longitude, localityId, [], 4, 15, { key: 'localityListingCount', order: 'DESC' }, { req }).then((response) => {
                            return {
                                data: response,
                                showSection: _checkListingAvailable(response),
                                similar: "similarLocality",
                                schema: "Place"
                            };
                        });
                    })();
                    similarCard.similarTitle = "Similar localities to " + (req.urlDetail.localityName || projectData[0].localityName);
                }
            } else if (pageLevel && ["buildertaxonomy", "buildercitytaxonomy"].indexOf(pageLevel.toLowerCase()) >= 0) {

                let builderId = req.urlDetail && req.urlDetail.builderId;
                let builderName = req.urlDetail && req.urlDetail.builderName;
                let cityId = req.urlDetail && req.urlDetail.cityId;
                let cityName = req.urlDetail && req.urlDetail.cityName;
                if (builderId) {
                    similarCard.similarProvider = (function() {
                        return builderService.getSimilarBuilder(builderId, cityId, { count: 4, req, selector: { filters: selector.filters } }, { req }).then((response) => {
                            return {
                                data: response,
                                showSection: _checkListingAvailable(response),
                                similar: "",
                                schema: "Brand",
                                cityUrl: utils.toUrlCase(cityName)
                            };
                        });
                    })();
                    similarCard.similarTitle = "Similar builder to " + builderName;
                }
            }
            projectData.isMobile = conditionalObj.mobileView;

            // Featured Snippet
            snippetData = projectData.map(function(pData){
                    return pData.project && {
                        name: pData.project.name || '',
                        minFormattedPrice: utils.formatNumber(pData.project.minPriceNum, {"type": 'number', "seperator" : req.locals.numberFormat}),
                        maxFormattedPrice: utils.formatNumber(pData.project.maxPriceNum, {"type": 'number', "seperator" : req.locals.numberFormat}),
                        address: pData.project.address || ''
                    }
            }).filter(p=>p);

            callback(null, {
                projectData,
                snippetData,
                projectCount,
                readableTotalCount: utils.formatNumber(projectCount, { type : 'number'} ),
                conditionalObj,
                showBackButtonInFilter,
                similarCard,
                paginationData,
                seoPrevNext,
                displayAreaLabel: req.urlDetail.displayAreaLabel || ''
            });


        }, (error) => {
            next(error);  //jshint ignore:line
        }).catch((e) => {
            next(e);  //jshint ignore:line
        });
    }

    let template, templateName, contentType;

    templateName = 'view.marko';

    if (clientXHR) {
        contentType = 'application/json';
    }

    template = require('services/templateLoader').loadTemplate(__dirname, templateName);

    if (clientXHR) {
        _getRenderingData(function(err, data) {
            if (err) {
                next(err);
            } else {
                data = { asyncData: data, asycSeoData: data.seoPrevNext };
                template.renderSync(data, res, function(error, output) {
                    res.send({
                        propcardHtml: output,
                        totalCount,
                        projectCount
                    });
                }, contentType, req);
            }
        });
    } else {
        _getRenderingData(function(err, data){
            template.renderSync(
                Object.assign(data,{
                    isMobileRequest: utils.isMobileRequest(req)
                }), 
                res, null, null, req);
        });
    }
}

module.exports = {
    routeHandler
};
