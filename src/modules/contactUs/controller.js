"use strict";
const template = require('services/templateLoader').loadTemplate(__dirname),
masterDetailService = require('services/masterDetailsService');

module.exports.routeHandler = function(req,res) {
    //console.log(masterDetailService)
    let cityList = masterDetailService.getMasterDetails('cityList');
    //console.log(cityList)
    template.renderAsync({cityList:cityList}, res, '', req);
};
