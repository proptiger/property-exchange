"use strict";
const template = require('services/templateLoader').loadTemplate(__dirname);
const apiService = require('services/apiService'),
    logger = require('services/loggerService'),
    apiConfig = require('configs/apiConfig');

module.exports.routeHandler = function(req, res) {
    apiService.get(apiConfig.checkLogin(), {
        req,
        res
    }).then(response => {
        let config = {
            loggedIn: true,
            userInfo: response.data
        };
        /*renderPage({
            config: JSON.stringify(config)
        }, res);*/

        template.renderAsync({
            config: JSON.stringify(config)
        }, res, '', req);
    }).catch(error => {
        logger.error("buyer not logged in", error);
        let config = {
            loggedIn: false
        };
        /*renderPage({
            config: JSON.stringify(config)
        }, res);*/
    
        template.renderAsync({
            config: JSON.stringify(config)
        }, res, '', req);
    });

};
