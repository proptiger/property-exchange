"use strict";

const serpService = require('services/serpService');

function routeHandler(req, res, next) {
    serpService.routeHandler(req, res, next, false);
}

module.exports = {
    routeHandler
};
