"use strict";
/**
 * controller setup for 500
 * @param  {Object} router
 */

const template = require('services/templateLoader').loadTemplate(__dirname),
    blogService = require('services/blogService');

module.exports.routeHandler = function(req, res, next) {
    let postName = req.params.postName;
    blogService.getPostDetail(req, postName).then(response => {
        template.renderAsync(response, res, '', req);
    }, err => next(err)).catch(err => {
        next(err);
    });

};
