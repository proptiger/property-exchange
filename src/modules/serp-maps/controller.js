"use strict";

const serpService = require('services/serpService');

function routeHandler(req, res, next) {
    serpService.routeHandler(req, res, next, true);
}

module.exports = {
    routeHandler: routeHandler
};
