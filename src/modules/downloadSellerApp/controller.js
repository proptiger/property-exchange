"use strict";
    const template = require('services/templateLoader').loadTemplate(__dirname);

module.exports.routeHandler = function(req, res) {
    if (res.req) {
        res.req.pageData = req.pageData;
    }
    let data = {};
    data.query = req.query;
    template.renderAsync(data, res, '', req);
};
