"use strict";
/**
 * controller setup for 500
 * @param  {Object} router
 */

const template = require('services/templateLoader').loadTemplate(__dirname),
    blogService = require('services/blogService');

module.exports.routeHandler = function(req, res, next) {
    let config = {
        "category": "Trends",
        "count": 5
    };
    blogService.getPostList(req, config).then(response => {
        response.flag = 2;
        template.renderAsync(response, res, '', req);
    }, err => next(err)).catch(err => {
        next(err);
    });
};
