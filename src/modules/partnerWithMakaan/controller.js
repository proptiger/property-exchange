"use strict";
/**
 * controller setup for 500
 * @param  {Object} router
 */

const utils = require('services/utilService'),
    desktopTemplate = require('services/templateLoader').loadTemplate(__dirname, 'view.marko'),
    mobileTemplate = require('services/templateLoader').loadTemplate(__dirname, 'view-mobile.marko'),
    cityService = require('services/cityService');

function _getCityPriceList(callback, req, res) {
    cityService.cityPriceList(req, res).then((response) => {
        callback(null, response);
    }, (err) => {
        callback(err, null);
    }).catch((err) => {
        callback(err, null);
    });
}

module.exports.routeHandler = function(req, res) {
    let template = utils.isMobileRequest(req) ? mobileTemplate : desktopTemplate;
    template.renderAsync({
        showOnly: req.query && req.query.showOnly,
        asyncCityPriceList: function(args, callback) {
            _getCityPriceList(callback, req, res);
        }
    }, res, "", req);
};
