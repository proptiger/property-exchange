"use strict";
/**
 * controller setup for 404
 * @param  {Object} router
 */
function _404Handler(req, res){
	let template = require('./view.marko');
	template.render({},(error, output) => {
		res.send(output);
	});
}

module.exports.setup = function(router) {
    router.get('/404', _404Handler);
};
