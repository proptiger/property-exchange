"use strict";
// const logger = require('services/loggerService'),
// 	utils = require('services/utilService'),
// 	globalConfig = require('configs/globalConfig'),
	const template = require('services/templateLoader').loadTemplate(__dirname);

module.exports.routeHandler = function(req, res) {
	template.renderAsync({}, res, '', req);
};
