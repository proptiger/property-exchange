"use strict";
const logger = require('services/loggerService'),
    template = require('services/templateLoader').loadTemplate(__dirname),
    projectService = require('services/projectService'),
    imageParser = require('services/imageParser'),
    utils = require('services/utilService'),
    agentService = require('services/agentService'),
    globalConfig = require('configs/globalConfig'),
    listings = require('services/listingsService'),
    utilService = require('services/utilService'),
    async = require('async');


module.exports.routeHandler = function(req, res, next) {
    logger.info('project controller called');
    let projectId = req.urlDetail.projectId,
        localityId = req.urlDetail.localityId,
        isMobileRequest = utils.isMobileRequest(req),
        query = req.query || {},
        proptigerCompanyId = globalConfig.proptigerCompanyId,
        selector = { "fields": ["name", "address", "imageURL", "masterSpecParentDisplayName", "masterSpecParentCatId", "masterSpecParentCat", "masterSpecCatDisplayName", "masterSpecCatName", "masterSpecClassName", "masterSpecificationCategory", "masterSpecification", "masterSpecificationId", "resiProjectSpecifications", "minPrice", "maxPrice", "images", "distinctBedrooms", "dominantUnitType", "possessionDate", "sizeInAcres", "minSize", "maxSize", "supply", "locality", "cityId", "absolutePath", "imageType", "type", "resalePrice", "pricePerUnitArea", "resalePricePerUnitArea", "budget", "unitName", "measure", "size", "offers", "projectAmenities", "amenityDisplayName", "offerDesc", "id", "path", "imageTypeId", "waterMarkName", "localityId", "amenityMaster", "abbreviation", "launchDate", "hideLaunchDate", "projectStatus", "totalUnits", "URL", "propertyUnitTypes", "suburb", "city", "buyUrl", "label", "mainImage", "altText", "title", "isPrimary", "isResale", "isSoldOut", "unitType", "couponsInventoryLeft", "totalCouponsInventory", "minDiscountPrice", "maxDiscountPrice", "isCouponAvailable", "maxDiscount", "propertyId", "couponPrice", "totalInventory", "couponCatalogue", "purchaseExpiryAt", "inventoryLeft", "maxCouponExpiryAt", "discountPricePerUnitArea", "resaleEnquiry", "description", "minResalePrice", "isProperty", "propertyType", "propertySizeMeasure", "has3DImages", "livabilityScore", "offerHeading", "bedrooms", "minResaleOrPrimaryPrice", "media", "absoluteUrl", "bathrooms", "balcony", "displayCarpetArea", "amenityName", "specifications", "avgPriceRisePercentage", "avgPriceRiseMonths", "averageRating", "ratingsCount", "numberOfUsersByRating", "establishedDate", "projectCount", "projectStatusCount", "lastEnquiredDate", "url", "overviewUrl", "objectMediaType", "objectMediaTypeId", "totalProjectDiscussion", "resaleURL", "latitude", "longitude", "hasProjectInsightReport", "localityAmenityTypes", "geoDistance", "website", "vicinity", "restDetails", "rating", "projectLocalityScore", "projectSocietyScore", "safetyScore", "builder", "builderScore", "videoUrl", "category", "videoId", "videoUrls", "panoramaViewPath", "towers", "towerId", "towerName", "flatNumber", "videoUrlDetails", "video", "mediaExtraAttributes", "imageUrl", "bitRate", "resolution", "videos", "unitTypeString", "maxResalePrice", "neighborhood", "listings", "hasPrimaryExpandedListing", "discount", "discountDescription", "time", "salientFeatures", "nearbyProjectDistances", "loanProviderBanks", "maxResaleOrPrimaryPrice", "projectId", "isPropertySoldOut", "amenityId", "verified", "avgPricePerUnitArea"] },
        listingSelector = { "fields": ["listing", "property", "unitName", "unitType", "size", "measure", "currentListingPrice", "pricePerUnitArea", "price", "hasPricePerUnitArea"], "paging": { "start": 0, "rows": 100 } };
    listingSelector.sellerId = proptigerCompanyId;

    async.parallel({
        projectDetail: function(callback) {
            projectService.getProjectDetails(projectId, localityId, { req, selector }).then(response => {
                callback(null, projectDetailParser(response));
            }, error => {
                callback(error, {});
            });
        },
        listingDetail: function(callback) {
            listings.getApiListings(listingSelector, req.urlDetail, { req }).then(response => {
                response = (response.response.totalCount) ? listingDetailParser(response, req) : {};
                callback(null, response);
            }, error => {
                callback(error, {});
            });
        },
        sellerDetail: function(callback) {
            agentService.getSellerDetailsByCompanyId(proptigerCompanyId, { req }).then(response => {
                response = (response && response.length) ? response[0] : {};
                callback(null, response);
            }, error => {
                callback(error, {});
            });
        }

    }, function(err, results) {
        if (err) {
            next();
        }
        // results array
        template.renderAsync({
            asyncData: results
        }, res, "", req);
    });

    function projectDetailParser(parsedData) {
        var data = parsedData.projectDetail;
        data.isMobileRequest = isMobileRequest;
        if (!utils.isEmptyObject(parsedData.imageDetails.data)) {
            data.galleryData = parsedData.imageDetails;
        } else {
            data.galleryData = {
                data: imageParser.parseImages(data.mainImage, isMobileRequest)
            };
        }
        data.projectId = projectId;
        data.specification = parseSpecifications(data.specifications["specificationList"]);
        data.salientFeatures = data.salientFeatures && data.salientFeatures.length && data.salientFeatures[0].description && JSON.parse(data.salientFeatures[0].description) || [];
        data.constructionImages = parsedData.constructionImages;
        if (!utils.isEmptyObject(data.loans)) {
            data.homeLoanDetails = function(args, callback) {
                projectService.getHomeLoans(data.loans, callback, { req });
            };
        }
        data.chartData = {
            id: 'chart-price-trend',
            category: 'category',
            yAxisLegendText: 'Price / sqft',
            chartType: 'spline',
            extraconfig: {}
        };
        data.isMobileRequest = isMobileRequest;
        return data;
    }

    function listingDetailParser(parsedData, req) {
        parsedData = parsedData.response;
        var numberFormat = req.locals.numberFormat;
        parsedData.properties = parseProperties(parsedData.data, numberformat);
        parsedData.properties.sort(function(a, b) {
            return a.priceInt - b.priceInt;
        });
        parsedData.minPrice = parsedData.properties[0].price;
        parsedData.sizeRange = utilService.getRange(parsedData.properties[0].size, parsedData.properties[parsedData.properties.length - 1].size, parsedData.properties[0].measure);
        return parsedData;
    }

    function parseSpecifications(data) {
        var parsedData = {};
        for (var i = 0; i < data.length; i++) {
            parsedData[data[i].key] = data[i].value;
        }
        return parsedData;
    }

    function parseProperties(data, numberFormat) {
        var properties = [];
        data = ((data && data.facetedResponse) ? data.facetedResponse.items : data.items) || [];
        data.map(function(obj) {
            let flag = true;
            obj = obj.listing;
            for (var i = 0; i < properties.length; i++) {
                if (properties[i].unitName == obj.property.unitName) {
                    flag = false;
                    return;
                }
            }
            if (flag) {
                properties.push({
                    unitName: obj.property.unitName,
                    size: utilService.formatNumber(obj.property.size, { type : 'number', seperator : numberFormat} ),
                    priceInt: obj.currentListingPrice.price,
                    price: utilService.formatNumber((obj.currentListingPrice.price, { precision : 2, seperator : numberFormat} )),
                    avgPricePerUnitArea: utilService.formatNumber(obj.currentListingPrice.pricePerUnitArea, { precision : 2, seperator : numberFormat}),
                    measure: obj.property.measure
                });
            }
        });
        return properties;
    }
};
