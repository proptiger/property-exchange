var mongoose = require('mongoose');

var mpanalyticsSchema = new mongoose.Schema({
  visitor_id: String,
  page_type: String,
  event_name:String,
  delivery_id: String,
  serp_filter_bhk:Boolean,
  serp_filter_budget:Boolean,
  serp_filter_property_type:Boolean,
  notification_sent: Boolean
});

module.exports = mongoose.model('mpanalytics', mpanalyticsSchema);
