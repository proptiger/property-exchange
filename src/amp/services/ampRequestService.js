"use strict";

const apiService = require('services/apiService');
const apiConfig = require('configs/apiConfig');

var multiparty = require('multiparty');

var ampRequestService = {};

ampRequestService.setup = function(req, res, next) {
	var ourUrl = (process.env.WEBSITE_URL[process.env.WEBSITE_URL.length-1]==="/") ? process.env.WEBSITE_URL.slice(0,-1) : process.env.WEBSITE_URL;
    return new Promise((resolve,reject)=>{
    	if(req.headers["content-type"] && req.headers["content-type"].indexOf('multipart/form-data')>-1){
			var form = new multiparty.Form();
		    form.parse(req, function(err, fields, files) {
		    	if(err){
		    		reject(err);
		    	}
		    	let body = {};
		    	// converting to a single JSON
		    	if(fields){
					try{
				    	Object.keys(fields).forEach(function(name) {
				    		//multiparty converts all form-data vlaues to individual lists
				    		if(typeof(fields[name])==='string'){
								body[name] = fields[name];
				    		}else{
				    			//extracting the first element, as multiparty form-data elements are encapsulated in a list
				    			var tempField;
				    			try{
				    				tempField = JSON.parse(fields[name][0]);
				    			} catch(err) {
				    				tempField = fields[name][0];
				    			}
				    			body[name] = tempField;
				    		}
						});
					} catch(err){
						reject(err);
					}
		    	}

				req.body = body;
	            req.headers["content-type"]="application/json";

	            /* updating headers for google-amp CORS*/
	            res.setHeader('AMP-Access-Control-Allow-Source-Origin', ourUrl);
				res.setHeader('Access-Control-Expose-Headers', 'AMP-Access-Control-Allow-Source-Origin');
				res.setHeader('Access-Control-Allow-Credentials', true);
				if (req.headers.origin && (req.headers.origin.indexOf('ampproject.org') > -1 || req.headers.origin.indexOf('amp.cloudflare.com') > -1) && req.query['__amp_source_origin'] === ourUrl) {
					res.setHeader('Access-Control-Allow-Origin', req.headers.origin);
				} else if (!req.headers.origin && req.headers['amp-same-origin']) {
					res.setHeader('Access-Control-Allow-Origin', ourUrl);
				}
		      	resolve();
		    });
		} else {
			resolve();
		}
    });
};

module.exports = ampRequestService;
