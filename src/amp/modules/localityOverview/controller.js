"use strict";
const template = require('services/templateLoader').loadTemplate(__dirname, 'view.marko'),
    logger = require('services/loggerService'),
    ampTemplate = require('configs/ampTemplate'),
    localityService = require('services/localityService'),
    projectService = require('services/projectService'),
    builderService = require('services/builderService'),
    taxonomyService = require('services/taxonomyService'),
    utils = require('services/utilService'),
    seoService = require('services/seoService'),
    globalConfig = require('configs/globalConfig'),
    BlogService = require('services/blogService'),
    _ = utils._;

module.exports.routeHandler = function(req, res, next) {
    var urlDetail = req.urlDetail,
        pageData = {
            localityId: urlDetail.localityId || urlDetail.suburbId,
            localityName: urlDetail.localityName,
            cityId: urlDetail.cityId,
            cityName: urlDetail.cityName,
            suburbName: urlDetail.suburbName,
            mobileView: utils.isMobileRequest(req) ? true : false
        };

    function _getTemplate(urlDetail) {
        let analytics = JSON.stringify(ampTemplate.getAnalytics(urlDetail)),
            schema = null;
        return { analytics, schema };
    }

    function _getTopBuilders(localityId, callback, req) {
        builderService.getTopBuilders({
            "localityId": localityId,
            "count": 30
        }, {req}).then(function(response) {
            callback(null, response);
        }, err => {
            callback(err);
        });
    }

    function _getNearByLocalities(data, callback, req) {
        localityService.getNearbyLocalities(data.latitude, data.longitude, data.localityId, [], 15, 10, {req}).then((response) => {
            let isEmpty = _checkEmpty(response, ['propertyForSale', 'propertyForRent']);
            callback(null, {data:response,isEmpty});
        }, err => {
            callback(err);
        });
    }

    function _getTaxonomyUrl(taxonomyData, pageData, callback, req) {
        var selectorData = {
            localityId: pageData.localityId,
            localityName: pageData.localityName
        };
        if (taxonomyData.minAffordablePrice && taxonomyData.maxAffordablePrice) {
            selectorData.minAffordablePrice = taxonomyData.minAffordablePrice;
            selectorData.maxAffordablePrice = taxonomyData.maxAffordablePrice;
        }

        if (taxonomyData.minLuxuryPrice) {
            selectorData.minLuxuryPrice = taxonomyData.minLuxuryPrice;
        }

        if (taxonomyData.maxBudgetPrice) {
            selectorData.maxBudgetPrice = taxonomyData.maxBudgetPrice;
        }

        taxonomyService.getTaxonomyUrl(selectorData, {req}).then((response) => {
            var taxonomyPresent = false;
            for(let i=0; i<response.length; i++){
                let currTaxonomy = response[i];
                for(let key in currTaxonomy) {
                    if(currTaxonomy[key].rawStats){
                        taxonomyPresent = true;
                        break;
                    }
                }
            }
            callback(null, {response, taxonomyPresent});
        }, err => {
            callback(err);
        });
    }

    function _getTopLocality(pageData, localityDetails, callback, req) {
        //_.assignIn(localityDetails.cityDetails,{label:pageData.cityName})
        let localityData = {label:pageData.localityName, avgPrice_Rent:localityDetails.avgPrice_Rent, avgPriceUnitArea_Buy:localityDetails.avgPriceUnitArea_Buy, listingCount_Buy:localityDetails.listingCount_Buy, listingCount_Rent:localityDetails.listingCount_Rent,  buyUrl:localityDetails.buyUrl,rentUrl:localityDetails.rentUrl,overviewUrl:localityDetails.overviewUrl, maxPrice_Rent: localityDetails.maxPrice_Rent, minPrice_Rent: localityDetails.minPrice_Rent, minPriceUnitArea_Buy: localityDetails.minPriceUnitArea_Buy, maxPriceUnitArea_Buy: localityDetails.maxPriceUnitArea_Buy, avgPriceRisePercentage: localityDetails.avgPriceRisePercentage};
        Promise.all([localityService.getTopRankedLocalityInCity({cityId:pageData.cityId,localityId:pageData.localityId},{options:{category:'buy'}}, {req}),localityService.getTopRankedLocalityInCity({cityId:pageData.cityId, localityId:pageData.localityId},{options:{category:'rent'}}, {req})]).then(response=>{
            let isEmpty = _checkEmpty(response[0].topLocalities, ['listingcount_buy', 'listingcount_rent']);
            // response[0].unshift(localityDetails.cityDetails,localityData)
            // response[1].unshift(localityDetails.cityDetails,localityData)
            response[0].topLocalities.unshift(localityData);
            response[1].topLocalities.unshift(localityData);
            response[0].localityIdArray.push(pageData.localityId);
            response[1].localityIdArray.push(pageData.localityId);
            callback(null, { data: { buy: response[0], rent: response[1] }, isEmpty });
        }, err=>{
            callback(err);
        });
    }

    //check empty response for a key in object
    function _checkEmpty(response = {}, checks) {  //jshint ignore:line
        for (var i in response) {
            for (var j in checks) {
                if (response[i][checks[j]] !== 0)
                    {return false;}
            }
        }
        return true;
    }

    function _getProjectCountInLocality(pageData, localityDetails, callback, req) {
        let seoUrlParams = [];
        _.forEach(globalConfig.templateId.localityProjectSerp, (v) => {
            seoUrlParams.push({
                "urlDomain": "locality",
                "domainIds": [pageData.localityId],
                "urlCategoryName": v
            });
        });
        Promise.all([projectService.getProject({ localityId: pageData.localityId, projectCount: localityDetails.projectCount }, req), seoService.getEntityUrls(seoUrlParams, req)]).then((response) => {
            _.forEach(globalConfig.templateId.localityProjectSerp, (v, k) => {
                response[1][k] = response[1][v] && response[1][v][pageData.localityId];
            });
            callback(null, response);
        }, err => {
            callback(err);
        });
    }

    function _getLifestyleData(pageData, callback, req) {
        localityService.getLifestyleData(pageData.localityId, {req}).then(response=>{
            callback(null, response);
        }, err => {
            callback(err);
        });
    }

    function _getBlogData(callback, req) {

        let blogIqUrl = process.env.BASE_URL_MAKAANIQ || '',
            configLocality = {
                "text": pageData.localityName,
                "count": 5,
                "sort": "views-desc",
                "website": "makaaniq"
            },
            configTop = {
                "count": 5,
                "days": 50,
                "sort": "views-desc",
                "website": "makaaniq"
            };
        if(req.locals.numberFormat && req.locals.numberFormat.code) {
          configLocality.website = configTop.website = "iq" + req.locals.numberFormat.code;
          blogIqUrl = blogIqUrl.replace(/\/iq\//,'/'+req.locals.numberFormat.code+'/iq/');
        }
        
        Promise.all([BlogService.getPostList(req, configLocality), BlogService.getPostList(req, configTop)]).then((response) => {
            let post = [];
            if (_.isArray(response[0].data) && _.isArray(response[1].data)) {
                post = response[0].data.concat(response[1].data).slice(0, 5);
            }
            callback(null, {
                post,
                makaaniqUrl: blogIqUrl
            });
        }, err => {
            callback(err);
        });
    }

    localityService.getLocalityOverview(pageData.localityId, req).then((response) => {
        let localityDetails = localityService.parseLocalityData(response, pageData, { req });
        let tempData = _getTemplate(req.urlDetail);
        pageData.latitude = localityDetails.latitude;
        pageData.longitude = localityDetails.longitude;
        pageData.dominantUnit = localityDetails.dominantUnit;
        template.renderAsync({
            pageData,
            localityDetails,
            analytics: tempData.analytics,
            schema: tempData.schema,
            topBuildersDataProvider: function(args, callback) {
                _getTopBuilders(pageData.localityId, callback, req);
            },
            nearbyLocalitiesDataProvider: function(args, callback) {
                _getNearByLocalities({ latitude: localityDetails.latitude, longitude: localityDetails.longitude, localityId: pageData.localityId }, callback, req);
            },
            taxonomyUrlDataProvider: function(args, callback) {
                _getTaxonomyUrl(localityDetails.taxonomyData, pageData, callback, req);
            },
            topLocalityDataProvider: function(args, callback) {
                _getTopLocality(pageData, localityDetails, callback, req);
            },
            projectCountInLocalityProvider: function(args, callback) {
                _getProjectCountInLocality(pageData, localityDetails, callback, req);
            },
            lifestyleDataProvider: function(args, callback) {
                _getLifestyleData(pageData, callback, req);
            },
            blogData: function(args, callback) {
                _getBlogData(callback, req);
            }
        }, res, "", req);
    }, (err) => {
        logger.error('inside localityOverview controller error: ', err);
        next(err);
    }).catch(err => {
        logger.error('inside localityOverview controller catch: ', err);
        next(err);
    });

};
