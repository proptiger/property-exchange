"use strict";
    const async = require('async'),
    builderService = require('services/builderService'),
    utils = require('services/utilService'),
    imageParser = require('services/imageParser.js'),
    globalConfig = require('configs/globalConfig'),
    utilService = require('services/utilService'),
    ampTemplate = require('configs/ampTemplate'),
    template = require('services/templateLoader').loadTemplate(__dirname);

module.exports.routeHandler = function(req, res, next) {
    let pageLevel = req.urlDetail.pageLevel,
        apiType = (pageLevel.indexOf('top') > -1) ? "getTopBuilders" : "getAllBuilders",
        rows = apiType == 'getAllBuilders' ? globalConfig.panIndiaPageRowCount : globalConfig.topAgentPageCount,
        currentPage = (req.query && req.query.page) ? parseInt(req.query.page) : 1,
        start = (currentPage - 1) * rows,
        currBaseUrl = req.urlDetail.currBaseUrl,
        currentUrl = req.url,
        cityId = req.urlDetail.cityId || 0,
        countryId = req.urlDetail.countryId || 1,
        config = {
            fields: ["id", "name", "buyUrl", "imageURL", "establishedDate", "projectCount"],
            start: start,
            rows: rows,
            count: rows,
            cityId: cityId,
            countryId: countryId,
            pageType: req.urlDetail.templateId,
            needRaw: true
        },
        totalPages;

    function _sendResponse() {
        let isTopPage = pageLevel.indexOf('top') !== -1 ? true : false;
        let customH2;
        if(isTopPage) {
          customH2 = 'Top 10 Real estate Companies/ Developers in ' + (req.urlDetail.localityName || req.urlDetail.suburbName || req.urlDetail.cityName || 'India');
        } 
        else {
          customH2 = 'Real estate Companies/ Developers in ' + (req.urlDetail.ocalityName || req.urlDetail.suburbName || req.urlDetail.cityName || 'India');
        }
        builderService[apiType](config, { req }).then(response => {
            let tempData = _getTemplate(req.urlDetail);
            if (response && response.data && response.data.length) {
                let otherData = _parseOtherData(pageLevel, { req, response });
                let data = {
                    data: {
                        builderData: otherData.builderList,
                        cityId: cityId,
                        cityName: req.urlDetail.cityName,
                        paginationData: otherData.paginationData,
                        isTopPage: isTopPage,
                        h1: req.urlDetail.h1Title,
                        h2: req.urlDetail.h2Title || customH2
                    }
                };
                async.auto({
                    pageSeoData: function(callback) {
                        let seoPrevNext ={};
                        callback(null, {
                            seoPrevNext: utils.createSeoPrevNext(currentPage, totalPages, currBaseUrl, seoPrevNext)
                        });
                    },
                    pageTopData: function(callback) {
                        if (["topBuilder", "citytopBuilder"].indexOf(pageLevel) !== -1) {
                            _getTopData(callback);
                        } else {
                            callback(null);
                        }
                    }
                },function(err,results){
                    if (err) {
                        return next(err);
                    }
                    template.renderAsync({
                        data: data,
                        pageSeoData: results.pageSeoData,
                        pageTopData: results.pageTopData,
                        analytics: tempData.analytics,
                        schema: tempData.schema
                    }, res, "", req);
                });
            } else if(currentPage > 1){
                res.redirect(302, utils.prefixToUrl((currentUrl||'').replace(/[\?#](.*)$/,'')));
            } else{
                template.renderAsync({
                    data: {
                        data:{
                            cityId: cityId,
                            cityName: req.urlDetail.cityName,
                            isTopPage: isTopPage,
                            h1: req.urlDetail.h1Title,
                            h2: req.urlDetail.h2Title || customH2
                        }
                    },
                    analytics: tempData.analytics,
                    schema: tempData.schema
                }, res, "", req);
            }
        }, err => {
            return next(err);
        }).catch(err => {
            return next(err);
        });
    }

    function _getTopData(callback) {
        let topDataConfig = utilService._.assign({}, config);
        topDataConfig.start = currentPage * 5;
        builderService.getTopBuildersCards(topDataConfig, { req }).then(response => {
            let url, temp = {},
                cardData,
                parsedData = [];

            for (var res in response) {
                temp = {};
                temp.data = [];

                for (var i = 0; i < response[res].data.length; i++) {
                    cardData = response[res].data;
                    url = ((req.urlDetail.cityName) ? utils.toUrlCase(req.urlDetail.cityName) + '/' : "") + cardData[i].buyUrl;
                    temp.label = res;
                    temp.data.push({
                        logo: cardData[i].imageURL,
                        name: cardData[i].name,
                        count: cardData[i].projectCount,
                        listingCountBuy: cardData[i].listingCountBuy,
                        listingCountRent: cardData[i].listingCountRent,
                        url: url,
                        redirectUrl: utilService.prefixToUrl(url)

                    });
                }
                parsedData.push(temp);
            }
            callback(null, parsedData);
        }, err => {
            callback(err);
        });
    }

    function _parseOtherData(type, { req, response }) {
        let data = {};
        totalPages = Math.ceil(response.totalCount / rows);
        data.paginationData = {
            "totalPages": totalPages,
            "currentUrl": currentUrl,
            "filter": true,
            "currentPage": currentPage
        };
        data.paginationData.changeQueryParam = utils.changeQueryParam;
        data.builderList = response.data || [];
        data.isCityPage = (req.urlDetail.cityId) ? true : false;
        for (let i = 0, length = data.builderList.length; i < length; i++) {
            let currBuilder = data.builderList[i],
                url = ((data.isCityPage) ? utils.toUrlCase(req.urlDetail.cityName) + '/' : "") + currBuilder.buyUrl;
            currBuilder.imageURL = currBuilder.mainImage ? currBuilder.mainImage.absolutePath : undefined;
            currBuilder.url = url;
            currBuilder.redirectUrl = utilService.prefixToUrl(url);
            currBuilder.imageURL = imageParser.appendImageSize(currBuilder.imageURL, 'thumbnail');
            currBuilder.experience = currBuilder.establishedDate ? `${utils.timeFromDate(currBuilder.establishedDate)} years` : undefined;
        }

        return data;
    }

    function _getTemplate(urlDetail) {
        let analytics = JSON.stringify(ampTemplate.getAnalytics(urlDetail)),
            schema = null;
        return { analytics, schema };
    }

    _sendResponse();

};
