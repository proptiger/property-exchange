"use strict";
    const async = require('async'),
    agentService = require('services/agentService'),
    utils = require('services/utilService'),
    globalConfig = require('configs/globalConfig'),
    ampTemplate = require('configs/ampTemplate'),
    MasterDetailsService = require('services/masterDetailsService'),
    template = require('services/templateLoader').loadTemplate(__dirname);

module.exports.routeHandler = function(req, res, next) {

    let pageLevel = req.urlDetail.pageLevel,
        rows = (pageLevel.indexOf('top') == -1) ? globalConfig.panIndiaPageRowCount : globalConfig.topAgentPageCount,
        currentPage = (req.query && req.query.page) ? parseInt(req.query.page) : 1,
        start = (currentPage - 1) * rows,
        currentUrl = req.url,
        currBaseUrl = req.urlDetail.currBaseUrl,
        totalPages,

        countryId = req.urlDetail.countryId || 1,
        stateId = req.urlDetail.stateId,
        cityId = req.urlDetail.cityId,
        localityId = req.urlDetail.localityId,
        suburbId = req.urlDetail.suburbId,
        suburbName = req.urlDetail.suburbName && req.urlDetail.suburbName.toLowerCase(),
        localityName = req.urlDetail.localityName && req.urlDetail.localityName.toLowerCase(),
        cityName = req.urlDetail.cityName && req.urlDetail.cityName.toLowerCase(),
        stateName = req.urlDetail.stateName && req.urlDetail.stateName.toLowerCase(),
        urlGroupType = req.urlDetail.urlGroupType && req.urlDetail.urlGroupType.toLowerCase(),
        unitTypesNameMap = MasterDetailsService.getMasterDetails('unitTypesNameMap');

        function _getEntityFilter(){
            let filters=[{
                        key: 'countryId',
                        value: countryId
                    }];
            switch (pageLevel) {
                case "stateBroker":
                    filters.push({
                        key: 'stateId',
                        value: stateId
                    });
                    break;
                case "cityallBroker":
                case "cityallBrokerBuy":
                case "cityallBrokerRent":
                case "citytopBroker":
                case "citytopBrokerBuy":
                case "citytopBrokerRent":
                    filters.push({
                        key: 'cityId',
                        value: cityId
                    });
                    break;
                case "localityallBroker":
                case "localityallBrokerBuy":
                case "localityallBrokerRent":
                case "localitytopBroker":
                case "localitytopBrokerBuy":
                case "localitytopBrokerRent":
                    filters.push({
                        key: 'localityId',
                        value: localityId
                    });
                    break;
                case "suburballBroker":
                case "suburballBrokerBuy":
                case "suburballBrokerRent":
                case "suburbtopBroker":
                case "suburbtopBrokerBuy":
                case "suburbtopBrokerRent":
                    filters.push({
                        key: 'suburbId',
                        value: suburbId
                    });
                    break;
            }
            return filters;
        }

    function _getApiConfig(config) {
        let selector = {
                filters: _getEntityFilter(),
                paging: {
                    rows: rows,
                    start: start
                }
            };
        switch(urlGroupType){
            case 'buy':
                selector.filters.push({
                    key: "listingCategory",
                    value: ["Primary", "Resale"]
                });
                break;
            case 'rent':
                selector.filters.push({
                    key: "listingCategory",
                    value: ["Rental"]
                });
                break;
            default:
                selector.filters.push({
                    key: "listingCategory",
                    value: ["Primary", "Resale", "Rental"]
                });
        }

        if (!config.isTopPage) {
            selector.sort = {
                key: 'listingSellerCompanyName',
                order: 'ASC'
            };
        } else {
            selector.sort = {
                key: 'sellerTransactionRevealScore',
                order: 'DESC'
            };
        }
        return { selector };
    }

    function _sendResponse() {
        let isTopPage = pageLevel.indexOf('top') > -1;
        let options = _getApiConfig({ isTopPage });


        async.auto({
            brokersData: function(callback) {
                agentService.getAgents(options, { req }).then(function(response) {
                    callback(null, response);
                }, function(err) {
                    callback(err);
                });
            },
            count:['brokersData' ,function(callback,results) {
                let companyIds = [];
                if(!(results.brokersData && results.brokersData.data && results.brokersData.data.length)){
                    callback(null,{});
                    return;
                }
                results.brokersData.data.forEach(item => {
                    if(item && item.id ){
                        companyIds.push(item.id);
                    }
                });

                agentService.getSellerDetailsByCompanyList(companyIds, { req }).then((result)=>{
                    if(result && result.data && result.data.length>0){
                        callback(null, result.data);
                    } else {
                    callback("Fetch sellerInfo failed");
                    }
                }).catch((err)=>{
                    callback(err);
                });
            }]
        }, function(err, results) {
            if(err){
                return next(err);
            }
            if (results.brokersData && results.brokersData.data && results.brokersData.data.length) {
                var mergedData = utils._.intersectionWith(
                  utils._.cloneDeep(results.brokersData.data),
                  results.count,
                  function(x, y) {
                    return x.id === y.id && utils._.assign(x, y);
                  }
                );
                let otherData = _parseOtherData(pageLevel, { "req":req, "response": mergedData , "totalCount":results.brokersData.totalCount});
                let tempData = _getTemplate(req.urlDetail, mergedData)

                let agentSubLabel = "";
                if(pageLevel.indexOf('top') > -1){ agentSubLabel+="Top ";}
                else{ agentSubLabel+="A ";}
                agentSubLabel+="real estate agent (Broker or Dealer)";
                if(localityName){
                    agentSubLabel+=" in "+localityName;
                } else if(suburbName){
                    agentSubLabel+=" in "+suburbName;
                } else if(cityName){
                    agentSubLabel+=" in "+cityName;
                }
                switch(urlGroupType){
                    case 'buy':
                        agentSubLabel+=" for buy";
                        break;
                    case 'rent':
                        agentSubLabel+=" for buy";
                        break;
                }
                
                let data = {
                    analytics: tempData.analytics,
                    schema: tempData.schema,
                    agentData: mergedData,
                    agentSubLabel: agentSubLabel,
                    cityId,
                    localityId,
                    suburbId,
                    isTopPage,
                    localityName,
                    cityName,
                    suburbName,
                    paginationData: otherData.paginationData,
                    pageType: urlGroupType,
                    getH3
                };

                template.renderAsync({
                    data: data,
                    pageSeoData: otherData.pageSeoData
                }, res, "", req);
            } else if(currentPage > 1){
                res.redirect(302, utils.prefixToUrl((currentUrl||'').replace(/[\?#](.*)$/,'')));
            } else{
                let tempData = _getTemplate(req.urlDetail);
                template.renderAsync({data:{
                    analytics: tempData.analytics,
                    schema: tempData.schema
                }}, res, "", req);
            }
        });

    }

    function _parseOtherData(type, { req, response, totalCount }) {
        let data = {};
        totalPages = Math.ceil(totalCount / rows);

        data.paginationData = {
            "totalPages": totalPages,
            "currentUrl": currentUrl,
            "filter": true,
            "currentPage": currentPage
        };
        data.paginationData.changeQueryParam = utils.changeQueryParam;
        data.pageSeoData = {
                    seoPrevNext: utils.createSeoPrevNext(currentPage, totalPages, utils.prefixToUrl(currentUrl), {})
                };
        data.isCityPage = (req.urlDetail.cityId) ? true : false;
        return data;
    }

    function _getTemplate(urlDetail, data) {
        let analytics = JSON.stringify(ampTemplate.getAnalytics(urlDetail)),
            schema = JSON.stringify(ampTemplate.getSchema(urlDetail, data));
        return { analytics, schema }
    }

    function getH3(unitType, agentType, data) {
        let resPageType='', area = data.localityName || data.suburbName || data.cityName || '';
        area = area.toLowerCase();
        unitType = unitType && unitType.toLowerCase();
        let unitTypeDisplayName = (unitTypesNameMap[unitType] && unitTypesNameMap[unitType].label) || unitType;
        unitTypeDisplayName = unitTypeDisplayName.toLowerCase();
        if(data.pageType && data.pageType.for){
            resPageType = ' for '+data.pageType.for;
        }
        return data.isTopPage ? `top ${unitTypeDisplayName} property ${agentType}${resPageType} in ${area}` : `${unitTypeDisplayName} property ${agentType}${resPageType} in ${area}`;
    }
    _sendResponse();
};
