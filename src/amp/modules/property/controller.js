"use strict";

const async = require('async'),
    propertyService = require('services/propertyService'),
    imageParser = require('services/imageParser'),
    globalConfig = require('configs/globalConfig'),
    masterDetails = require('services/masterDetailsService'),
    maps = require('services/propertyMappingService'),
    mappingService = require('services/mappingService'),
    logger = require('services/loggerService'),
    similarPropertyService = require('services/similarPropertyService'),
    template = require('services/templateLoader').loadTemplate(__dirname, 'view.marko'),
    ampTemplate = require('configs/ampTemplate'),
    utils = require('services/utilService'),
    sharedConfig = require('public/scripts/common/sharedConfig'),
    moment = require('moment'),
    _ = utils._,
    _isEmptyObject = utils.isEmptyObject,
    config = {
        validActiveStatus: ['active', 'activeinmakaan'],
        dummyStatus: ['dummy'],
        unverifiedStatus: ['Unverified']
    },
    POSTED_BY_BROKER = "BROKER",
    makaanSelectCities = sharedConfig.makaanSelectCities;
/************ my functions ************/
function _parseSellerDetails(companySeller = {}) {
    let result = {};

    if (_isEmptyObject(companySeller)) {
        return result;
    }

    let seller = companySeller.company || {},
        user = companySeller.user || {},
        sellerNumbers = user.contactNumbers;

    result.name = seller.name;
    result.paidLeadCount = seller.paidLeadCount;
    result.assist = seller.assist;
    result.sellerId = seller.id;
    result.userId = user.id;
    result.type = seller.type;
    result.avatar = utils.getAvatar(seller.name);

    result.type = seller.type === POSTED_BY_BROKER ? 'AGENT' : seller.type;
    result.image = imageParser.appendImageSize(seller.logo || user.profilePictureURL, 'thumbnail');


    if (sellerNumbers && sellerNumbers.length) {
        result.phone = sellerNumbers[0].contactNumber;
        result.virtualPhone = utils.getMaskedPhoneNumber(sellerNumbers[0].contactNumber);
    } else {
        result.phone = false;
    }

    if (seller.score) {
        result.rating = seller.score;
        result.ratingClass = utils.getRatingBadgeClass(seller.score);
    }
    //TODO: Append the Reward Label for Seller here.
    return result;
}

function _parseFlags(data) {
    let result = {};
    let property = data.property || {};

    result.penthouse = property.penthouse;
    result.studio = property.studio;
    result.rk = property.rk;
    result.isPropertySoldOut = property.isPropertySoldOut;
    result.negotiable = data.negotiable;
    result.priceAllInclusive = data.priceAllInclusive;
    result.unitType = property.unitType;
    result.isCommercialListing = data.isCommercialListing;

    return result;
}

function _parseLocalityDetails(data = {}, req) {
    let project = data.property && data.property.project ? data.property.project : {};
    let locality = project ? project.locality : {};

    let result = {};
    result.localityId = locality.localityId;
    result.name = locality.label;
    result.url = utils.prefixToUrl(locality.overviewUrl);
    result.buyUrl = utils.prefixToUrl(locality.buyUrl);
    result.rentUrl = utils.prefixToUrl(locality.rentUrl);
    result.livabilityScore = locality.livabilityScore > 10 ? 10 : locality.livabilityScore;
    result.description = utils.stripHtmlContent(locality.description, globalConfig.wordLimit.midLength, true, true).description;
    result.localityImage = imageParser.appendImageSize(locality.localityHeroshotImageUrl, 'medium');
    return result;
}

function _parseProjectDetails(data = {}) {
    let result = {};
    let project = data.property && data.property.project ? data.property.project : {};

    result.show = (config.dummyStatus.indexOf((project.activeStatus || '').toLowerCase()) === -1);
    result.id = project.projectId;
    result.name = project.name;
    result.possessionDate = project.possessionDate;
    result.url = (config.validActiveStatus.indexOf((project.activeStatus || '').toLowerCase()) > -1) ? utils.prefixToUrl(project.overviewUrl) : undefined;
    result.description = utils.stripHtmlContent(project.description, globalConfig.wordLimit.midLength, true, true).description;
    result.livabilityScore = project.livabilityScore > 10 ? 10 : project.livabilityScore;
    result.availability = project.derivedAvailability;
    result.projectStatus = project.projectStatus;
    result.avgPriceRisePercentage = project.avgPriceRisePercentage;
    result.projectImage = project.imageURL && imageParser.appendImageSize(project.imageURL, 'medium');
    result.sizeInAcres = project.sizeInAcres;
    result.launchDate = project.launchDate && utils.formatDate(project.launchDate, 'SS YY');
    result.hideLaunchDate = project.hideLaunchDate;
    result.totalFloors = data.totalFloors;
    result.specifications = project.specifications;

    return result;
}

function _parseBuilderDetails(data) {
    let result = {};
    let property = data.property ? data.property : {};
    let project = property ? property.project : {};
    let builder = project ? project.builder : {};

    result.show = (config.dummyStatus.indexOf((builder.activeStatus || '').toLowerCase()) === -1);
    result.id = builder.id;
    result.name = builder.name;
    result.logo = imageParser.appendImageSize(builder.imageURL, 'smallSquare');
    result.altText = builder.mainImage && builder.mainImage.altText;
    result.description = utils.stripHtmlContent(project.description, globalConfig.wordLimit.midLength, true, true).description;
    result.url = (config.validActiveStatus.indexOf((builder.activeStatus || '').toLowerCase()) > -1) ? utils.prefixToUrl(builder.buyUrl) : undefined;
    result.age = builder.establishedDate ? utils.timeFromDate(builder.establishedDate) + ' years' : undefined;
    result.timeCompletion = builder.percentageCompletionOnTime ? builder.percentageCompletionOnTime + '%' : undefined;
    var projectStatus = builder.projectStatusCount;
    if (projectStatus) {
        result.ongoing = projectStatus['under construction'];
        result.completed = projectStatus.completed;
    }
    result.details = [{
        key: 'Experience',
        val: result.age
    }, {
        key: 'Completion on time',
        val: result.timeCompletion
    }, {
        key: 'Ongoing Projects',
        val: result.ongoing,
        "url": result.url ? `${result.url}?possession=any` : undefined
    }, {
        key: 'Past Projects',
        val: result.completed,
        url: result.url ? `${result.url}?ageOfProperty=any` : undefined
    }];
    return result;
}

function _getHomeloanData(data, req) {
    var result = {};
    if (data.property) {
        result.bedrooms = data.property.bedrooms;
        result.size = utils.formatNumber(data.property.size, {
            type : 'number',
            seperator : req.locals.numberFormat
        });
    }
    if (data.currentListingPrice) {
        result.rawPrice = data.currentListingPrice.price;
        result.readablePrice = utils.formatNumber(data.currentListingPrice.price, {
            returnSeperate : true,
            seperator : req.locals.numberFormat
        });
    }

    return result;
}


function _parseListingDetails(data = {}, req) {
    let result = {};
    let property = data.property ? data.property : {};
    let priceDetails = data.currentListingPrice || {};

    result.bedrooms = property.bedrooms;
    result.bathrooms = property.bathrooms || undefined;
    result.rooms = {
        study: property.studyRoom || undefined,
        servant: property.servantRoom || undefined,
        pooja: property.poojaRoom || undefined
    };

    result.unitType = property.unitType;
    result.unitTypeId = property.unitTypeId;
    result.unitName = property.unitName;
    result.units = property.measure;

    result.minAge = data.minConstructionCompletionDate;
    result.maxAge = data.maxConstructionCompletionDate;

    result.description = utils.stripHtmlContent(data.description, 100000, true, true).description;
    result.latitude = data.listingLatitude;
    result.longitude = data.listingLongitude;

    result.propertyId = data.propertyId;
    result.id = data.id;

    let displayDetails = {};
    displayDetails.bath = property.bathrooms || undefined;
    displayDetails.balcony = property.balcony || undefined;
    displayDetails.openSides = data.noOfOpenSides || undefined;
    displayDetails.parking = data.noOfCarParks;
    displayDetails.entryRoadWd = data.mainEntryRoadWidth ? data.mainEntryRoadWidth + ' feet' : undefined;
    displayDetails.tenantPref = propertyService.getTenantType(data.tenantTypes);
    displayDetails.facing = maps.getFacing(data.facingId);
    displayDetails.status = data.status;
    displayDetails.furnishStat = mappingService.getFurnishingStatus(data.furnished) || data.furnished;
    displayDetails.priceNeg = data.negotiable ? 'Yes' : 'No';
    displayDetails.overlook = propertyService.getViewDirections(data.viewDirections);
    displayDetails.ownerType = maps.getOwnerType(data.ownershipTypeId);
    displayDetails.type = maps.getPropertyType(property.unitTypeId) || '';
    displayDetails.carpetArea = (property && property.carpetArea && property.measure) ? property.carpetArea + ' ' + property.measure : null;
    displayDetails.dateAdded = data.creationDate ? utils.formatDate(data.creationDate, 'SS YY') : undefined;
    displayDetails.size = property.size ? `${utils.formatNumber(property.size, { 
        type : 'number', 
        sepertor : req.locals.numberFormat
    })} ${property.measure}` : undefined;
    displayDetails.status = mappingService.getPropertyStatus(data.constructionStatusId);
    displayDetails.category = maps.getListingCategory(data.listingCategory);
    displayDetails.posession = (data.constructionStatusId == 1) ? utils.formatDate(data.possessionDate, 'SS YY') : undefined;
    displayDetails.availability = data.possessionDate ? (data.possessionDate > Date.now() ? `from ${utils.formatDate(data.possessionDate, 'SS YY')}` : 'immediate') : undefined;
    displayDetails.addRoom = propertyService.getAdditionalRooms(result.rooms);
    displayDetails.floor = data.floor === 0 ? 'Gr' : data.floor;
    displayDetails.floor = data.floor ? utils.ordinalSuffix(data.floor) + (data.totalFloors ? ` <span class="grey">of ${data.totalFloors}</span>` : '') : undefined;
    displayDetails.bookamt = data.bookingAmount ? `<span class="currency"></span> ${utils.formatNumber(data.bookingAmount, { precision : 2, seperator : req.locals.numberFormat })}` : undefined;
    displayDetails.securityDeposit = data.securityDeposit ? `<span class="currency"></span> ${utils.formatNumber(data.securityDeposit, { precision : 2, seperator : req.locals.numberFormat })}` : undefined;
    displayDetails.displayPrice = utils.formatNumber(priceDetails.price, {
        precision : 2, 
        returnSeperate : true,
        seperator : req.locals.numberFormat
    });
    displayDetails.displayPricePerArea = utils.formatNumber(priceDetails.pricePerUnitArea, {
        type : 'number',
        seperator : req.locals.numberFormat
    });
    displayDetails.emi = (data.listingCategory != 'Rental') ? utils.formatNumber( utils.calculateEMI(priceDetails.price, globalConfig.emi_rate, globalConfig.emi_tenure, globalConfig.downpayment_percent), {
        type : 'number',
        seperator : req.locals.numberFormat
    }) : undefined;
    displayDetails.verifiedDate = data.displayDate ? utils.formatDate(data.displayDate, 'dd SS YY') : undefined;

    displayDetails.noOfSeats = data.noOfSeats;
    displayDetails.noOfRooms = data.noOfRooms;
    displayDetails.lockInPeriod = data.lockInPeriod ? (data.lockInPeriod + ' Years') : undefined;
    displayDetails.willingToModifyInteriors = data.willingToModifyInteriors ? 'Yes' : undefined;
    displayDetails.isCornerShop = data.isCornerShop ? 'Yes' : undefined;
    displayDetails.isBoundryWallPresent = data.isBoundryWallPresent ? 'Yes' : undefined;
    displayDetails.isMainRoadFacing = data.isMainRoadFacing ? 'Yes' : undefined;
    displayDetails.pantryType = data.pantryType ? data.pantryType.toLowerCase() : undefined;
    displayDetails.assureReturnPercentage = data.assureReturnPercentage;
    displayDetails.currentlyLeasedOut = data.currentlyLeasedOut ? 'Yes' : undefined;

    let priceBreakUpDetails = {};
    priceBreakUpDetails.bookamt = displayDetails.bookamt;
    priceBreakUpDetails.securityDeposit = displayDetails.securityDeposit;
    priceBreakUpDetails.maintenanceCharges = (data.maintenanceCharges && data.maintenanceFrequencyType) ? (data.maintenanceCharges + ' ' + data.maintenanceFrequencyType.toLowerCase()) : null;
    
    result.details =  displayDetails;
    result.overviewDetails = {};

    let overviewDetailsCount = globalConfig.visibleKeyCount;
    _.forEach(displayDetails, (value,key)=>{
        if(overviewDetailsCount>0 && value && maps.getLanguageText(key)){
            result.overviewDetails[key] = value;
            overviewDetailsCount--;
        }
    });

    if(data.isCommercialListing) {
        result.thirdFoldDetails = {};
        _.forEach(priceBreakUpDetails, (value,key)=>{
            if( value && maps.getLanguageText(key)){
                result.thirdFoldDetails[key] = value;
            }
        });
    }

    result.price = priceDetails.price;
    result.listingScore = data.qualityScore;

    result.furnishing = (data.furnishings && data.furnishings.length > 0 && !data.isCommercialListing) ? propertyService.parseFurnishing(data.furnishings, displayDetails.type, data.listingCategory) : [];


    result.amenities = propertyService.getAmenities(data.masterAmenityIds, property.project.projectAmenities);
    result.amenities = propertyService.parseAmenities(result.amenities, displayDetails.type, data.listingCategory);

    if (data.mainImage) {
        result.mainImage = [{
            absolutePath: data.mainImage.absolutePath,
            altText: data.mainImage.altText,
            title: data.mainImage.title

        }];
    } else {
        result.mainImage = [{
            absolutePath: data.mainImageURL
        }];
    }
    return result;
}

function _updatePageData(req, data) {
    req.pageData.listingCategory = data.listing.details.category;
    req.pageData.unitType = data.listing.unitType;
    req.pageData.budget = data.listing.price;
    req.pageData.bhk = data.listing.bedrooms;
    req.pageData.projectStatus = data.project.projectStatus;
    req.pageData.sellerId = data.sellerDetails.sellerId;
    req.pageData.sellerScore = data.sellerDetails.rating;
    req.pageData.listingScore = data.listing.listingScore;
    req.pageData.projectId = data.project.id;
    req.pageData.listingId = data.listing.id;
    req.pageData.cityId = data.city.id;

}

/************ my functions end *********/



function propertyParser(data, req) {
    var groupData = _groupData(data, req),
        result = {};

    if (data.constructionStatusId == 2 && (groupData.listing.minAge || groupData.listing.maxAge)) {
        let minAge = groupData.listing.minAge || groupData.listing.maxAge, //incase minAge is missing
            totallMonths = moment().diff(minAge, 'months'),
            years = Math.floor(totallMonths / 12),
            months = totallMonths % 12;
        result.age = years;
        if (months || (years === 0)) { // years == 0 is the case if property is some days old
            result.age += ` - ${years+1}`;
        }
        result.age += (years === 0 || (years == 1 && !months)) ? ' year' : ' years';
    }

    let _bhkString = (groupData.flags.rk) ? "RK" : "BHK",
        _unitTypeString = groupData.listing.details.type;

    if (groupData.flags.studio) {
        _unitTypeString = "studio apartment";
    }

    if (groupData.flags.penthouse) {
        _unitTypeString = "penthouse";
    }

    if(groupData.flags.isCommercialListing) {
        result.title = groupData.flags.unitType;
    } else{
        result.title = (groupData.listing.unitType !== 'Plot' ? (`${groupData.listing.bedrooms} ${_bhkString}`) : '') + ` ${_unitTypeString}`;
    }

    result.sellerSimilarProp = {
        bed: (groupData.listing.bedrooms || 0).toString(),
        bath: (groupData.listing.bathrooms || 0).toString(),
        study: (groupData.listing.rooms.study || 0).toString(),
        pooja: (groupData.listing.rooms.pooja || 0).toString(),
        servant: (groupData.listing.rooms.servant || 0).toString(),
        projectId: (groupData.project.id).toString(),
        listingCompanyId: (groupData.sellerDetails.sellerId).toString(),
        listingCategory: (groupData.listing.details.category == 'Rental' ? ['Rental'] : ['Primary', 'Resale']).toString()
    };
    groupData.listing = _.assign(groupData.listing, result);

    return groupData;
}

function _groupData(data, req) {
    var result = {},
    property = data.property ? data.property : {},
    galleryImages = [],
    companySeller = data.companySeller,
    project = property ? property.project : {},
    locality = project ? project.locality : {};

    result.flags = {};
    result.priceDetails = {};
    result.project = {};
    result.locality = {};
    result.city = {};
    result.builder = {};
    result.details = {};
    result.sellerDetails = {};


    if (locality.suburb && locality.suburb.city) {
        result.city.name = locality.suburb.city.label;
        result.city.id = locality.suburb.city.id;
        result.city.url = utils.prefixToUrl(locality.suburb.city.overviewUrl);
    }

    result.sellerDetails = _.assign({}, _parseSellerDetails(companySeller));
    result.sellerDetails.isMakaanSelectSeller = data.listingSellerTransactionStatuses && data.listingSellerTransactionStatuses.indexOf('MakaanSelect')>=0 && makaanSelectCities.indexOf(result.city.id) > -1 && data.listingCategory == 'Rental' ;
    result.flags = _.assign({}, _parseFlags(data));
    result.locality = _.assign({}, _parseLocalityDetails(data));
    result.project = _.assign({}, _parseProjectDetails(data));
    result.builder = _.assign({}, _parseBuilderDetails(data));
    result.listing = _.assign({}, _parseListingDetails(data, req));
    result.homeloan = _.assign({}, _getHomeloanData(data, req));

    galleryImages = result.listing && result.listing.mainImage ? galleryImages.concat(result.listing.mainImage) : galleryImages;
    galleryImages = property && property.images ? galleryImages.concat(property.images) : galleryImages;
    galleryImages = project && project.images ? galleryImages.concat(project.images) : galleryImages;
    galleryImages = locality && locality.images ? galleryImages.concat(locality.images) : galleryImages;
    galleryImages = galleryImages.concat(data.images || []);
    for(var i in galleryImages){
        galleryImages[i].absolutePath = imageParser.appendImageSize(galleryImages[i].absolutePath, "medium");
    }
    result.galleryImages = galleryImages;
    return result;
}

module.exports.routeHandler = function(req, res, next) {
    logger.info('property controller  called');
    let listingId = req.urlDetail.listingId,
        propertyId = req.urlDetail.propertyId,
        projectId = req.urlDetail.projectId,
        isCommercial = req && req.pageData && req.pageData.isCommercial;
    async.auto({
        propertyData: function(callback) {
            propertyService.getPropertyDetail({ listingId, projectId, propertyId, isCommercial}, { req }).then((data) => {
                var parsedData = propertyParser(data.propertyDetail, req);
                _updatePageData(req, parsedData);
                callback(null, parsedData);
            }, err => {
                logger.error('inside property controller error: ', err);
                callback(err);
            }).catch(err => {
                logger.error('inside property controller catch: ', err);
                callback(err);
            });
        },
        similarPropertyProj: ['propertyData', function(callback, result) {
            if (result.propertyData.project.show) {
                similarPropertyService.getApiListings(res.req.pageData, 'project', {req}).then((response) => {
                    if(response.seeAllUrl){
                        response.seeAllUrl = utils.prefixToUrl(response.seeAllUrl);
                    }
                    callback(null, response);
                }, () => {
                    callback();
                });
            } else {
                callback();
            }
        }],
        similarPropertySell: ['propertyData', function(callback) {
            similarPropertyService.getApiListings(res.req.pageData, 'seller', {req}).then((response) => {
                callback(null, response);
            }, () => {
                callback();
            });
        }],
        // similarSeller: ['propertyData', function(callback, result) {
        //     if (result.propertyData && result.propertyData.listing.sellerSimilarProp) {
        //         propertyService.getSellerDetails({
        //             params: result.propertyData.listing.sellerSimilarProp
        //         }, { req }).then((response) => {
        //             callback(null, response);
        //         }, () => {
        //             callback();
        //         });
        //     } else {
        //         callback();
        //     }
        // }],
        similarProperties: ['propertyData', function(callback, result) {
            propertyService.getSimilarListings(result.propertyData.listing.id, { req }).then((response) => {
                callback(null, response);
            }, () => {
                callback();
            });
        }]
    }, function(err, results) {
        if (err) {
            return next(err);
        }
        let tempData = _getTemplate(req.urlDetail, results);
        let tempCountries = masterDetails.getMasterDetails("countriesList");
        template.renderAsync({
            propertyData: results.propertyData,
            label: maps.getLanguageText,
            similarSeller: results.similarSeller,
            similarProperties: results.similarProperties,
            similarPropertyProj: results.similarPropertyProj,
            similarPropertySell: results.similarPropertySell,
            analytics: tempData.analytics,
            schema: tempData.schema,
            enquiryType: mappingService.enquiryTypeMapping.CALL_NOW,
            countriesList: tempCountries
        }, res, "", req);
    });
};

function _getTemplate(urlDetail, data) {

    let analytics = JSON.stringify(ampTemplate.getAnalytics(urlDetail, data)),
        schema = JSON.stringify(ampTemplate.getSchema(urlDetail, data.propertyData));
    return { analytics, schema };
}
