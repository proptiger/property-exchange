"use strict";


const async = require('async'),
    utils = require('services/utilService'),
    _ = utils._,
    masterDetails = require('services/masterDetailsService'),
    mappingService = require('services/mappingService'),
    listingService = require('services/listingsService'),
    builderService = require('services/builderService'),
    agentService = require('services/agentService'),
    globalConfig = require('configs/globalConfig'),
    template = require('services/templateLoader').loadTemplate(__dirname),
    ampTemplate = require('configs/ampTemplate'),
    sharedConfig = require('public/scripts/common/sharedConfig'),
    deficitProjectParser = require('modules/apis/apiParsers/deficitSellerProjectsParser'),
    makaanSelectCities = sharedConfig.makaanSelectCities,
    //filterConfigService.getUrlWithUpdatedParams - Removing listingType & sortBy, to be set by Filter
    validFilterKeys = ['pageType', 'stateName', 'cityName', 'localityName', 'suburbName',  'builderName', 'companyName', 'projectName', 'placeName','stateId', 'cityId', 'localityId', 'localityOrSuburbId', 'suburbId', 'builderId', 'projectId', 'placeId', 'sellerId', 'companyId', 'templateId'],
    fields = [
            "localityId",
            "displayDate",
            "listing",
            "property",
            "project",
            "builder",
            "displayName",
            "locality",
            "suburb",
            "city",
            "state",
            "currentListingPrice",
            "companySeller",
            "company",
            "user",
            "id",
            "name",
            "label",
            "listingId",
            "propertyId",
            "projectId",
            "propertyTitle",
            "unitTypeId",
            "resaleURL",
            "description",
            "postedDate",
            "verificationDate",
            "size",
            "measure",
            "bedrooms",
            "bathrooms",
            "listingLatitude",
            "listingLongitude",
            "studyRoom",
            "servantRoom",
            "pricePerUnitArea",
            "price",
            "localityAvgPrice",
            "negotiable",
            "rk",
            "buyUrl",
            "rentUrl",
            "overviewUrl",
            "minConstructionCompletionDate",
            "maxConstructionCompletionDate",
            "halls",
            "facingId",
            "noOfOpenSides",
            "bookingAmount",
            "securityDeposit",
            "ownershipTypeId",
            "furnished",
            "constructionStatusId",
            "tenantTypes",
            "bedrooms",
            "balcony",
            "floor",
            "totalFloors",
            "listingCategory",
            "possessionDate",
            "activeStatus",
            "type",
            "logo",
            "profilePictureURL",
            "score",
            "assist",
            "contactNumbers",
            "contactNumber",
            "isOffered",
            "mainImageURL",
            "mainImage",
            "absolutePath",
            "altText",
            "title",
            "imageCount",
            "defaultImageId",
            "updatedAt",
            "qualityScore",
            "projectStatus",
            "throughCampaign",
            "addedByPromoter",
            "listingDebugInfo",
            "videos",
            "imageUrl",
            "rk",
            "penthouse",
            "studio",
            "paidLeadCount",
            "allocation",
            "allocationHistory",
            "masterAllocationStatus",
            "status",
            "sellerCompanyFeedbackCount",
            "companyStateReraRegistrationId"
    ];
const URL_DETAIL_DEPENDENT_FIELDS = {
    "placeId": "geoDistance"
};

function _getDeficitBHK(queryObj,urlDetail){
    let bhkArray = queryObj.beds && queryObj.beds.toString() && queryObj.beds.split(",")||[];
    if(urlDetail.filter && urlDetail.filter.beds){
        bhkArray.concat(urlDetail.filter.beds.split(","));
    }
    return utils.processBHK(bhkArray);
}
function _getDeficitUnitType(queryObj,urlDetail){
    let unitTypeArray = queryObj.propertyType && queryObj.propertyType.toString() && queryObj.propertyType.split(",")||[];
    if(urlDetail.filter && urlDetail.filter.propertyType){
        unitTypeArray.concat(urlDetail.filter.propertyType.split(","));
    }
    return utils.getBuyUnitTypeId(unitTypeArray);
}
function _getDeficitQueryObj(queryObj,urlDetail){
    let urlFilters = urlDetail.filter || {},deficitQueryObj = {};
    deficitQueryObj.budget = queryObj.budget || urlFilters.budget 
    deficitQueryObj.localityIds = queryObj.localityId || urlDetail.localityId;
    deficitQueryObj.suburbIds = queryObj.suburbId || urlDetail.suburbId;
    deficitQueryObj.cityId = queryObj.cityId || urlDetail.cityId;
    deficitQueryObj.unitType = _getDeficitUnitType(queryObj,urlDetail);
    deficitQueryObj.bhk = _getDeficitBHK(queryObj,urlDetail);
    deficitQueryObj.listingCategory = "Primary";
    deficitQueryObj.listingSellerTransactionStatuses = "BUILDER_EXCLUSIVE_CAMPAIGN_SELLER,CAMPAIGN_SELLER";
    deficitQueryObj.leadDeficitScore = "5,";
    deficitQueryObj.projectDbStatus="Active,ActiveInMakaan";
    deficitQueryObj.rows = 10;
    deficitQueryObj.includeNearby = true;
    if(deficitQueryObj.cityId){
        deficitQueryObj.cityId = deficitQueryObj.cityId.toString();
    }
    if(deficitQueryObj.localityIds){
        deficitQueryObj.localityIds = deficitQueryObj.localityIds.toString();
    }
    if(deficitQueryObj.suburbIds){
        deficitQueryObj.suburbIds = deficitQueryObj.suburbIds.toString();
    }
    return deficitQueryObj;
}


module.exports.routeHandler = function(req, res, next) {
    async.auto({
        listing: function(callback) {
            req.query.fields = fields.slice(); // to copy fields by value
            for(let key in URL_DETAIL_DEPENDENT_FIELDS) {
                if(req.urlDetail[key]) {
                    req.query.fields.push(URL_DETAIL_DEPENDENT_FIELDS[key]);
                }
            }
            req.query.forceFieldFiltering = true;
            
            // MAKAAN-5384 : Replicating MAKAAN-5170(Change Default Sort) on AMP
            if (req.query && sharedConfig.recencySortedCities.indexOf(req.query.cityId) > -1 || req.urlDetail && sharedConfig.recencySortedCities.indexOf(req.urlDetail.cityId) > -1) {
                req.query = req.query || {};
                if (!req.query.sortBy) {
                    req.query.sortBy = "date-desc"; // listing to be sorted on recency
                }
            }
            
            listingService.getApiListings(req.query, req.urlDetail, { req }).then(function(response) {

                let listingData, listingCount, totalCount, seoPrevNext = {},
                    seoPrevNextFinal = {},
                    paginationData,
                    indexthreshold, conditionalObj = {
                        h1Title: req.urlDetail.h1Title,
                        h2Title: req.urlDetail.h2Title,
                        showNearbyLocalitiesCheckbox: req.showNearbyLocalitiesCheckbox,
                        trackingObject: {}
                    },
                    mobileView = utils.isMobileRequest(req) ? true : false,
                    missingQuickFilters = response.missingQuickFilters||[];
                    response = response.response;
                    totalCount = response && response.data[0].totalCount || 0;
                    paginationData = setPaginationData(req, response.data[0].totalCount);

                if (req.seoAPI && paginationData.currentPage && paginationData.totalPages && paginationData.currentPage <= paginationData.totalPages) {
                    let currentPage = parseInt(paginationData.currentPage),
                        totalPages = parseInt(paginationData.totalPages),
                        currBaseUrl = req.urlDetail.currBaseUrl;
                    seoPrevNextFinal = utils.createSeoPrevNext(currentPage, totalPages, currBaseUrl, seoPrevNext);
                }


                conditionalObj.trackingObject = _getTrackingFilters(req.query);

                if (!req.urlDetail.cityId) {
                    conditionalObj.hidePyrCard = true;
                }else{
                    conditionalObj.cityId = req.urlDetail.cityId;
                }

                if (req.data.boundaryEncodedPolygon) {
                    conditionalObj.boundaryEncodedPolygon = window.escape(req.data.boundaryEncodedPolygon);
                }

                if(req.urlDetail.pageType.indexOf('TAXONOMY') > -1 || req.urlDetail.cityId) {
                  conditionalObj.isTaxonomy = true;
                }
                else {
                  conditionalObj.isTaxonomy = false;
                }
                if(req.data.builder && ((['BUILDER_TAXONOMY_URLS','BUILDER_TAXONOMY_URLS_MAPS','BUILDER_URLS','BUILDER_URLS_MAPS'].indexOf(req.urlDetail.pageType) > -1) || (req.query.builderId && ['BUILDER_TAXONOMY_URLS','BUILDER_TAXONOMY_URLS_MAPS','LISTINGS_PROPERTY_URLS','LISTINGS_PROPERTY_URLS_MAPS','BUILDER_URLS','BUILDER_URLS_MAPS'].indexOf(req.query.pageType) > -1))){
                    conditionalObj.builderView = true;
                    conditionalObj.builderData = builderService.parseBuilderData(req.data.builder, 100);
                }else if((req.data.seller && ((['COMPANY_TAXONOMY_URLS','COMPANY_TAXONOMY_URLS_MAPS','COMPANY_URLS','COMPANY_URLS_MAPS'].indexOf(req.urlDetail.pageType) > -1) || (req.query.companyId && ['COMPANY_TAXONOMY_URLS','COMPANY_TAXONOMY_URLS_MAPS','LISTINGS_PROPERTY_URLS','LISTINGS_PROPERTY_URLS_MAPS','COMPANY_URLS','COMPANY_URLS_MAPS'].indexOf(req.query.pageType) > -1))) || (['SELLER_PROPERTY_URLS', 'SELLER_PROPERTY_URLS_MAPS', ].indexOf(req.query.pageType) > -1)  || (req.query.sellerId && ['SELLER_PROPERTY_URLS','SELLER_PROPERTY_URLS_MAPS','LISTINGS_PROPERTY_URLS','LISTINGS_PROPERTY_URLS_MAPS'].indexOf(req.query.pageType) > -1)) {
                    if(req.data.seller){
                        conditionalObj.sellerView = true;
                        conditionalObj.sellerData = agentService.parseCompanyData(req.data.seller, {req});
                        conditionalObj.sellerReviews = req.data.sellerReviews;
                        conditionalObj.feedbackCount = req.data.seller.feedbackCount;

                        // Adding additional items in page data required for tracking
                        req.pageData.sellerScore = conditionalObj.sellerData.rating;

                    }else if(req.data.builder){
                        conditionalObj.builderView = true;
                        conditionalObj.builderData = builderService.parseBuilderData(req.data.builder, 100);

                    }
                }

                if (req.urlDetail.listingType && req.urlDetail.listingType.toUpperCase() === 'RENT') {
                    conditionalObj.isRental = true;
                }

                listingData = listingService.parseListingsApiResponse(response, { mobileView, query: req.query, mapsView: false, sellerView: conditionalObj.sellerView, urlDetail: req.urlDetail, locals: req.locals });
                if ((req.query && sharedConfig.recencySortedCities.indexOf(req.query.cityId) > -1) || (req.urlDetail && sharedConfig.recencySortedCities.indexOf(req.urlDetail.cityId) > -1)) {
                    if (req.query && req.query.sortBy == "date-desc") {
                        listingData = listingService.shuffleListingData(listingData);
                    }
                }

                conditionalObj.isZeroListingPage = listingData.isZeroListingPage;
                indexthreshold = (req.query && req.query.indexthreshold && parseInt(req.query.indexthreshold)) || 0;
                listingCount = listingData.data ? listingData.data.length : 0;
                totalCount = listingData.totalCount;

                // to show seller rating as first listing rating and seller india level rating in case of zero result
                if (listingCount && conditionalObj.sellerData) {
                    conditionalObj.sellerData.rating = listingData.data[0].postedBy.rating || conditionalObj.sellerData.rating;
                }

                listingData.displayAreaLabel = req.urlDetail.displayAreaLabel || '';

                listingData.isMobile = mobileView;
                listingData.isMaps = false;

                listingData.landmarkName = req.urlDetail.placeName;
                if (["NEARBY_LISTING_URLS_MAPS", "NEARBY_URLS_MAPS", "NEARBY_LISTING_TAXONOMY_URLS"].indexOf(req.urlDetail.pageType) != -1 || (req.urlDetail.pageType == 'LISTINGS_PROPERTY_URLS_MAPS' && req.urlDetail.placeId)) {
                    listingData.pageType = 'landmark';
                    // override latitude & longitude of first listing for maps landing center
                    listingData.latitude = req.urlDetail.latitude;
                    listingData.longitude = req.urlDetail.longitude;
                }

                callback(null, {
                    listingData,
                    seoPrevNextFinal,
                    listingCount,
                    indexthreshold,
                    conditionalObj,
                    paginationData,
                    missingQuickFilters
                });


            }, function(err) {
                callback(err);
            });
        },
        nearbyListingResponse:function(callback){
            listingService.getdeficitNearbyListings(req.query, req.urlDetail,{req,itemsPerPage:10,isMobileRequest:true}).then(function(response) {
                let sellerView = req.data.seller ? true : false;
                let nearbyListings = listingService.parseDeficitNearbyListings(response.response,{mobileView:true, query: req.query, mapsView: false, sellerView, urlDetail: req.urlDetail, locals: req.locals });
                callback(null,[nearbyListings]);
            }, function (){
                callback(null,[]);
            });
        },
        leadDeficitProjectResponse:function(callback){
            if(req.urlDetail && req.urlDetail.listingType=="buy"){
                let deficitQueryObj = _getDeficitQueryObj(req.query,req.urlDetail);
                let deficitRequest = Object.assign({}, req);
                deficitRequest.query = deficitQueryObj;
                deficitProjectParser.getAmpleadDeficitProjects(deficitRequest, res, next).then(function(response) {
                    callback(null,response);
                }, function (){
                    callback(null,{});
                });
            }else{
                callback(null,{});
            }
        }


    }, function(err, results) {
        if (err) {
            return next(err);
        }

        let tempData = _getTemplate(req.urlDetail, results);
        let tempCountries = masterDetails.getMasterDetails("countriesList");
        let serpAmpLeadConfig = _getAmpSerpLeadConfig(results.listing.listingData.listings);
        if(results.nearbyListingResponse && results.nearbyListingResponse[0] && results.nearbyListingResponse[0].length){
            let nearbyListingLeadConfig = _getAmpSerpLeadConfig(results.nearbyListingResponse);
            _.assign(serpAmpLeadConfig, nearbyListingLeadConfig);
        }
        template.renderAsync({
            asyncData: results.listing,
            analytics: tempData.analytics,
            schema: tempData.schema,
            listingsUrl: tempData.listingsUrl,
            listingsKeys: tempData.listingsKeys,
            countriesList: tempCountries,
            enquiryType: mappingService.enquiryTypeMapping.CALL_NOW,
            nearbyListings:results.nearbyListingResponse && results.nearbyListingResponse[0]||[],
            deficitProjectResponse:results.leadDeficitProjectResponse,
            serpAmpLeadConfig: serpAmpLeadConfig
        }, res, "", req);
    });
};


function _getTemplate(urlDetail, data) {
    let analytics = JSON.stringify(ampTemplate.getAnalytics(urlDetail, data)),
        schema = JSON.stringify(ampTemplate.getSchema(urlDetail, data)),
        listingsUrl = "/listings?", listingsKeys={};

    for (let key in urlDetail) {
        if (validFilterKeys.indexOf(key) > -1 && urlDetail[key]) {
            listingsUrl += "&"+key+"="+urlDetail[key];
            listingsKeys[key] = urlDetail[key];
        }
    }
    listingsUrl = utils.prefixToUrl(listingsUrl);
    return { analytics, schema, listingsUrl, listingsKeys };
}

function _getAmpSerpLeadConfig(listings) {
    var serpAmpConfig = {};
    _.forEach(listings, (listingType,key)=>{
        _.forEach(listingType, (listing,key)=>{
            serpAmpConfig[listing.listingId] = {
                "cityId": listing.cityId,
                "cityName": listing.cityName,
                "minBudget": listing.price,
                "maxBudget": listing.price,
                "projectId": listing.projectId,
                "bhk": listing.bedrooms,
                "listingId": listing.listingId,
                "multipleCompanyIds": [listing.postedBy.id.toString()],
                "multipleSellerIds": [listing.postedBy.userId.toString()],
                "localityIds": listing.localityId,
                "propertyTypes": listing.propertyType,
                "companyUserId": listing.postedBy.userId,
            };
            serpAmpConfig[listing.listingId].sellerDetails = {
                "type": listing.postedBy.type,
                "name": listing.postedBy.name,
                "phone": listing.postedBy.phone,
                "rating": utils.roundNumber(listing.postedBy.rating,1),
                "ratingClass": listing.postedBy.rating ? utils.getRatingBadgeClass(listing.postedBy.rating) : 'none',
                "avatar": utils.getAvatar(listing.postedBy.nameText),
                "virtualPhone": utils.getMaskedPhoneNumber(listing.postedBy.phone),
                "isMakaanSelectSeller":listing.isMakaanSelectSeller && makaanSelectCities.indexOf(listing.cityId) > -1 && listing.isRent || false
            }
        });
    });

    return Object.keys(serpAmpConfig).length > 0 ? serpAmpConfig : null;
} 

function setPaginationData(req, totalCount) {

    let paginationData = {},
        listingPerPage = globalConfig.itemsPerPage;
    if (totalCount && totalCount > 0) {
        paginationData.totalPages = Math.ceil(totalCount / listingPerPage);
    } else {
        paginationData.totalPages = 1;
    }
    paginationData.currentUrl = req.url;

    paginationData.filter = true;

    var currentPage = req.query.page;
    if (!currentPage) {
        currentPage = 1;
    }
    paginationData.currentPage = currentPage;
    paginationData.changeQueryParam = utils.changeQueryParam;
    return paginationData;
}

function _getTrackingFilters(query) {
    let trackingObject = {};
    trackingObject.beds = query.beds;
    trackingObject.budget = query.budget && query.budget.replace(/^,|,$/g, '');
    return trackingObject;
}
