"use strict";

const async = require('async'),
    utils = require('services/utilService'),
    _ = utils._,
    mappingService = require('services/mappingService'),
    masterDetails = require('services/masterDetailsService'),
    amphtmlValidator = require('amphtml-validator'),
    ampTemplate = require('configs/ampTemplate'),
    logger = require('services/loggerService'),
    projectService = require('services/projectService'),
    imageParser = require('services/imageParser'),
    ampTrackingService = require('services/ampTrackingService'),
    template = require('services/templateLoader').loadTemplate(__dirname);

module.exports.routeHandler = function(req, res, next) {
    logger.info('projectDefaultService  called');
    let projectId = req.urlDetail.projectId,
        localityId = req.urlDetail.localityId,
        projectName = req.urlDetail.projectName,
        builderName = req.urlDetail.builderName,
        cityId = req.urlDetail.cityId,
        isMobileRequest = utils.isMobileRequest(req),
        query = req.query || {},
        sellerId = query.sellerId;

    async.auto({
        projectData: function(callback) {
            projectService.getProjectDetails(projectId, localityId, { req }).then(function(parsedData) {
                var data = parsedData.projectDetail;
                data.raw = parsedData.raw;
                data.projectId = projectId;
                data.isMobileRequest = isMobileRequest;
                data.galleryContainerData = parsedData.galleryContainerData;
                if (!utils.isEmptyObject(parsedData.imageDetails.data)) {
                    data.galleryData = parsedData.imageDetails;
                } else {
                    data.galleryData = {
                        data: imageParser.parseImages(data.mainImage, isMobileRequest, true)
                    };
                }
                data.mobileConfigurationBucket = projectService.prepareMobileConfigurationBucket(parsedData.projectDetail.properties, data.galleryData, {req});
                data.mainImage = data.galleryData.data.length && data.galleryData.data[0];
                data.constructionImages = parsedData.constructionImages;
                data.floorPlanImages = parsedData.floorPlanImages;
                data.formatPriceWithComma = utils.formatPriceWithComma;
                data.isMobileRequest = isMobileRequest;
                callback(null, data);
            }, function(err) {
                callback(err);
            })
        },
        similar: function(callback) {
            projectService.getSimilarProjectAPIData(projectId, 10, { req, isMobileRequest }).then(function(response) {
                callback(null, response);
            }, function(err) {
                callback(err);
            })
        },
        configurationBuckets: ['projectData', function(callback, results) {
            let projectProperties = results.projectData.properties,
                projectStatus = results.projectData.raw.status,
                possessionTimestamp = results.projectData.raw && results.projectData.raw.possessionTimestamp;
            projectService.configurationBuckets({ projectId, projectName, builderName, projectProperties, projectStatus, possessionTimestamp, isMobileRequest }, { req }).then(function(response) {
                callback(null, {"listingBucket": response, "leadConfigBucket": _getAmpListingLeadConfig(response)});
            }, function(err) {
                callback(err);
            })
        }],
        agentService: function(callback) {
            projectService.parseTopAgentForLead(projectService.getTopAgent({ cityId, projectId }, { req, isMobileRequest}), { builderName, cityId, req }).then(function(response) {
                response = response || [];
                _.forEach(response, (v, k)=>{
                    v.companyRating = utils.roundNumber(v.companyRating,1);
                    v.companyRatingClass = utils.getRatingBadgeClass(v.companyRating);
                });
                callback(null, response);
            }, function(err) {
                callback();
            });
        }

    }, function(err, results) {
        if (err) {
            return next(err);
        }
        let tempData = _getTemplate(req.urlDetail, results);
        let tempCountries = masterDetails.getMasterDetails("countriesList");
        let topAgentLeadConfig = {};
        let multipleCompanyIds=[];
        if(results.agentService){
            topAgentLeadConfig =_parseTopAgentLeadConfig(results.agentService);
        } else {
            topAgentLeadConfig = null;
        }
        template.renderAsync({
            projectData: results.projectData,
            similar: results.similar,
            configurationBuckets: results.configurationBuckets.listingBucket,
            agentService: results.agentService,
            analytics: tempData.analytics,
            schema: tempData.schema,
            countriesList: tempCountries,
            enquiryType: mappingService.enquiryTypeMapping.CALL_NOW,
            multipleCompanyIds: multipleCompanyIds,
            topAgentLeadConfig: topAgentLeadConfig,
            listingLeadConfig: results.configurationBuckets.leadConfigBucket,
            prefixToUrl: utils.prefixToUrl
        }, res, "", req);
    })
}

function _getTemplate(urlDetail, data) {

    let analytics = JSON.stringify(ampTemplate.getAnalytics(urlDetail, data)),
        schema = JSON.stringify(ampTemplate.getSchema(urlDetail, data.projectData))
    return { analytics, schema }
}

function _parseTopAgentLeadConfig(data){
    let tempConfig = {};

    _.forEach(data, (agent,key)=>{
        tempConfig[agent.id] = {
            "id": agent.id,
            "userid": agent.companyUserId,
            "type": agent.companyType,
            "name": agent.companyName,
            "phone": agent.companyPhone,
            "rating": agent.companyRating,
            "ratingClass": agent.companyRatingClass,
            "avatar": agent.companyAvatar,
            "virtualPhone": utils.getMaskedPhoneNumber(agent.companyPhone),
            "multipleCompanyIds": [agent.id.toString()],
            "multipleSellerIds": [agent.companyUserId.toString()]
        }
    });

    return Object.keys(tempConfig).length>0?tempConfig:null;
}

function _getAmpListingLeadConfig(listings) {
    var serpAmpConfig = {};

    function __leadConfigGen(bucket, salesType){
        if(!bucket) return;
        _.forEach(bucket, (listingGroup,key)=>{
            if(listingGroup.count>0) {
                _.forEach(listingGroup.listings, (listing,key)=>{
                    serpAmpConfig[listing.listingId] = {
                        "cityId": listing.cityId,
                        "minBudget": listing.fullPrice && listing.fullPrice.replace(/,/g,''),
                        "maxBudget": listing.fullPrice && listing.fullPrice.replace(/,/g,''),
                        "projectId": listing.projectId,
                        "bhk": listing.beds,
                        "listingId": listing.listingId,
                        "multipleCompanyIds": [listing.companyId && listing.companyId.toString()],
                        "localityIds": listing.localityId,
                        "propertyTypes": listing.unitTypeId || "",
                        "companyUserId": listing.companyUserId,
                        "multipleSellerIds": [listing.companyUserId && listing.companyUserId.toString()],
                        "salesType": salesType
                    };
                    serpAmpConfig[listing.listingId].sellerDetails = {
                        "type": "",
                        "name": listing.companyName,
                        "phone": listing.companyPhone,
                        "rating": utils.roundNumber(listing.companyRating,1),
                        "ratingClass": listing.companyRating ? utils.getRatingBadgeClass(listing.companyRating) : 'none',
                        "avatar": utils.getAvatar(listing.companyName),
                        "virtualPhone": utils.getMaskedPhoneNumber(listing.companyPhone)
                    };
                });
            }
        });
    }

    if(listings.buy && listings.mobileConfigurationBuckets.buyBucket){
        __leadConfigGen(listings.mobileConfigurationBuckets.buyBucket, "buy");
    }
    if(listings.rent && listings.mobileConfigurationBuckets.rentalBucket){
        __leadConfigGen(listings.mobileConfigurationBuckets.rentalBucket, "rent");
    }

    return Object.keys(serpAmpConfig).length > 0 ? serpAmpConfig : null;
}
