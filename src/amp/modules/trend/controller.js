"use strict";

const async = require('async'),
    utils = require('services/utilService'),
    utilService = require('services/utilService'),
    _ = utilService._,
    mappingService = require('services/mappingService'),
    masterDetails = require('services/masterDetailsService'),
    amphtmlValidator = require('amphtml-validator'),
    ampTemplate = require('configs/ampTemplate'),
    logger = require('services/loggerService'),
    localityService = require('services/localityService'),
    cityService = require('services/cityService'),
    projectService = require('services/projectService'),
    globalConfig = require('configs/globalConfig').templateId,
    imageParser = require('services/imageParser'),
    ampTrackingService = require('services/ampTrackingService'),
    template = require('services/templateLoader').loadTemplate(__dirname);



function _getTemplate(urlDetail, data) {

    let analytics = JSON.stringify(ampTemplate.getAnalytics(urlDetail, data)),
        schema = JSON.stringify(ampTemplate.getSchema(urlDetail, data.projectData))
    return { analytics, schema }
}

function _getPaginationData(pData) {
    pData.filter = true;
    pData.changeQueryParam = utilService.changeQueryParam;
    return pData;
}

function _getConfiguration(req) {
    req.urlDetail = req.urlDetail || {};
     let countryId = req.urlDetail.filters && req.urlDetail.filters.countryId || req.urlDetail.countryId || 1,
        cityId = req.urlDetail.cityId,
        cityName = req.urlDetail.cityName,
        localityId = req.urlDetail.localityId,
        localityName = req.urlDetail.localityName,
        suburbId = req.urlDetail.suburbId,
        suburbName = req.urlDetail.suburbName,
        pageLevel = req.urlDetail.pageLevel,
        isRent = req.urlDetail.listingType == 'rent' ? true : false,
        currBaseUrl = req.urlDetail.canonicalUrl && req.urlDetail.canonicalUrl.replace(/(\?.*)|(#.*)/g, ""),
        currentPage = parseInt(req.query.page) || 1,
        currentUrl = req.url,
        rows = cityId ? 60 : 1000,
        start = (currentPage - 1) * rows,
        urlLabel = req.meta.urlLabel;
    return {
        countryId,
        cityId,
        localityId,
        suburbId,
        cityName,
        localityName,
        suburbName,
        pageLevel,
        isRent,
        currBaseUrl,
        currentUrl,
        currentPage,
        rows,
        start,
        urlLabel
    };
}

module.exports.routeHandler = function(req, res, next) {
    logger.info('ampTrend controller called');

    var config = _getConfiguration(req);
    let paginationData = {},
        currentPage = config.currentPage,
        currentUrl = config.currentUrl,
        currBaseUrl = config.currBaseUrl,
        totalPages,
        getData;

    async.auto({
        trendData: function(callback) {
            if (config.cityId) {
                getData = localityService.getAllLocalityWithPriceTrendUrl;
                config.templates = {
                    buy: globalConfig.localityPriceTrend.buy,
                    rent: globalConfig.localityPriceTrend.rent
                };
            } else {
                getData = cityService.getAllCityWithPriceTrendUrl;
                config.templates = {
                    buy: globalConfig.cityPriceTrend.buy,
                    rent: globalConfig.cityPriceTrend.rent
                };
            }
            getData(config, { req }).then((response)=>{
                response = response || {} ;
                callback(null, response);
            },(err)=>{
                callback(err);
            }).catch((err)=>{
                callback(err);
            });
        },
        asyncProjectData: function(callback) {
            projectService.getTopProjectPriceRange(config, { req }).then((response) => {
                response = response || {};
                callback(null, {
                    data: response.data || [],
                    config
                });
            }, err => {
                callback(err);
            }).catch(err => {
                callback(err);
            });
        }
    }, function(err, results) {
        if (err) {
            return next(err);
        }
        let tempData = _getTemplate(req.urlDetail, results);
        let seoPrevNext = {}
        totalPages = Math.ceil(results.trendData.totalCount / config.rows);
        seoPrevNext = utilService.createSeoPrevNext(currentPage, totalPages, currBaseUrl, seoPrevNext);
        
        template.renderAsync({
            data: results.trendData.data || [],
            totalListingCountRent: results.trendData.totalListingCountRent || 0,
            totalListingCountBuy: results.trendData.totalListingCountBuy || 0,
            paginationData: _getPaginationData({totalPages,currentUrl,currentPage}),
            config,
            asyncProjectData: results.asyncProjectData,
            asycSeoData: seoPrevNext,
            formatPrice: function(val){
                //TODO- why we are not using price formator
                return utilService.formatNumber(val, { type : 'number', seperator : req.locals.numberFormat} );
            },            
            analytics: tempData.analytics,
            schema: tempData.schema,
        }, res, "", req);
    })
}
