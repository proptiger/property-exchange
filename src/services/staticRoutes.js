"use strict";

module.exports.getStaticUrls = function() {
    return [{
        url: '/partner-with-makaan',
        moduleName: 'partnerWithMakaan',
        req: {
            skipSeo: true,
            "bread-crum": null,
            meta: {
                title: 'Makaan USP & Business Model',
                metaTags: {
                    viewport: 'minimum-scale=1.0, width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no'
                }
            },
            pageData: {
                pageType: "PARTNER_WITH_MAKAAN"
            }
        }
    }, {
        url: '/rent-sell-property-online',
        moduleName: 'sellerHomePage',
        req: {
            skipSeo: true,
            "bread-crum": null,
            meta: {
                title: 'Rent & Sell Property | Post Your Property Ads For Free Online',
                description: 'Want to Sell your Property or Rent a House with easy step? ✔Unlimited free listings on Makaan.com. ✔Post your Property for Sale & Rent. ✔Download Our Seller App.',
                metaTags: {
                    viewport: 'minimum-scale=1.0, width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no',
                    "og:type": "website",
                    "og:site_name": "Makaan.com",
                    "og:title": "Rent & Sell Property | Post Your Property Ads For Free Online",
                    "og:description": 'Want to Sell your Property or Rent a House with easy step? ✔Unlimited free listings on Makaan.com. ✔Post your Property for Sale & Rent. ✔Download Our Seller App.',
                    "og:url": "https://www.makaan.com/rent-sell-property-online"
                },
                aboutUs: "Makaan.com is a marketplace where sellers can sell property or cater to those wish to rent a house. We know that buying and sellingproperty is not easy. So, this platform is made to ensure that verified listings attract genuine buyers and customers. Partner with Makaan.com, post property for sale or post rent ad for free and enjoy the benefits of partnering with us. We help you find joy and more. Makaan.com is the most preferred property website among buyers. When you join us, you get connected with only genuine buyers. Start today."
            },
            pageData: {
                pageType: "PARTNER_WITH_MAKAAN",
                canonicalUrl: "https://www.makaan.com/rent-sell-property-online"
            }
        }
    }, {
        url: '/download-app',
        moduleName: 'downloadApp',
        req: {
            skipSeo: true,
            "bread-crum": null,
            meta: {
                metaTags: {
                    viewport: 'minimum-scale=1.0, width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no'
                }
            }
        }
    }, {
       url: '/download-mobileapp',
       moduleName: 'downloadBuyerApp',
       req: {
           skipSeo: true,
           "bread-crum": [
               [{
                   "label": "home",
                   "url": "/"
               }],
               [{
                   "label": "Real Estate App",
                   "url": "#"
               }]
           ],
           meta: {
               title: 'Makaan Buyer App for Mobiles and iPhone – Best Android and iOS App for Real Estate Property Search: Makaan.com',
               description: 'Makaan Buyer App for Mobile and iPhone – Download Real Estate Property Search App for your Android Mobiles, Tablets and iOS App for iPhone and iPad. Find best property on the go.',
               keywords: 'Makaan Buyer App, Makaan Android App for Property, Makaan App for Real Estate Search, Makaan iOS App, Makaan iOS App for Property',
               h1Tag: 'Makaan App',
               metaTags: {
                   viewport: 'minimum-scale=1.0, width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no'
               }
           },
           pageData: {
               pageType: 'DOWNLOAD_MOBILE_APP',
               canonicalUrl: 'https://www.makaan.com/download-mobileapp'
           }
       }
    }, {
       url: '/app-for-sell-rent-property-online',
       moduleName: 'downloadSellerApp',
       req: {
           skipSeo: true,
           "bread-crum": [
               [{
                   "label": "home",
                   "url": "/"
               }],
               [{
                   "label": "Sell/Rent House App",
                   "url": "#"
               }]
           ],
           meta: {
               title: 'Home Rental Apps | Sell, Rent House/Apartments Apps - Makaan.com',
               description: 'Sell, Rent House App - Find best house rental app for android and iOS. Download India\'s best home selling app online. Visit Makaan.com',
               keywords: 'Makaan Seller App, Makaan Android App for Property, Makaan App for Real Estate Search, Makaan iOS App, Makaan iOS App for Property',
               h1Tag: 'Sell/Rent House App',
               metaTags: {
                   viewport: 'minimum-scale=1.0, width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no'
               }
           },
           pageData: {
               pageType: 'DOWNLOAD_MOBILE_APP',
               canonicalUrl: 'https://www.makaan.com/app-for-sell-rent-property-online'
           }
       }
    }, {
        url: '/widgets/:widgetName',
        moduleName: 'widgets',
        req: {
            skipSeo: true,
            "bread-crum": null,
            meta: {
                title: '',
                description: '',
                keywords: '',
                metaTags: {
                    viewport: 'minimum-scale=1.0, width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no'
                }
            },
            pageData: {
                pageType: 'widgets'
            }
        }
    }, {
        url: '/reset-password',
        moduleName: 'resetPassword',
        req: {
            skipSeo: true,
            "bread-crum": null,
            meta: {
                metaTags: {
                    viewport: 'minimum-scale=1.0, width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no'
                }
            }
        }

    }, {
        url: '/unsubscribe',
        moduleName: 'unsubscribe',
        req: {
            skipSeo: true,
            "bread-crum": null,
            meta: {
                metaTags: {
                    viewport: 'minimum-scale=1.0, width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no'
                }
            }
        }
    },
    {
        url: '/home-loan',
        moduleName: 'homeLoanDashboard',
        req: {
            skipSeo: true,
            "bread-crum": null,
            meta: {
                title: 'Makaan Home Loan',
                description: 'Need a loan for your new home? Connect with us and get a range of home loan providers and immediate assistance',
                keywords: '',
                metaTags: {
                    viewport: 'width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no',
                    "og:type": "website",
                    "og:site_name": "Makaan.com",
                    "og:title": "Makaan Home Loan",
                    "og:description": 'Need a loan for your new home? Connect with us and get a range of home loan providers and immediate assistance',
                    "og:url": "https://www.makaan.com/home-loan"
                }
            },
            pageData: {
                pageType: 'homeloan',
                trackingKeyword:'HOME LOAN',
                canonicalUrl: "https://www.makaan.com/home-loan"
            }
        }
    },
    {
        url: '/about-us',
        moduleName: 'aboutUs',
        req: {
            skipSeo: true,
            "bread-crum": [
                [{
                    "label": "home",
                    "url": "/"
                }],
                [{
                    "label": "about us",
                    "url": "#"
                }]
            ],
            meta: {
                title: 'About Us - To know About Makaan.com',
                description: 'Searching about property for sale in India? Get details for all real estate property for sale in India at reasonable cost.',
                keywords: 'About us, about Makaan.com, about us real estate sites, about Makaan',
                metaTags: {
                    viewport: 'minimum-scale=1.0, width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no'
                }
            }
        }

    }, {
        url: '/payment',
        moduleName: 'payment',
        req: {
            skipSeo: true,
            meta: {
                title: 'Makaan.com - Payment',
                description: 'Searching about property for sale in India? Get details for all real estate property for sale in India at reasonable cost.',
                keywords: 'About us, about Makaan.com',
                metaTags: {
                    viewport: 'minimum-scale=1.0, width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no'
                }
            }
        }

    }, {
        url: '/contact-us$/',
        moduleName: 'contactUs',
        req: {
            skipSeo: true,
            "bread-crum": [
                [{
                    "label": "home",
                    "url": "/"
                }],
                [{
                    "label": "contact us",
                    "url": "#"
                }]
            ],
            meta: {
                title: 'Find Makaan.com Office Addresses & Contact Details Across India',
                description: 'Get Makaan.com contact details and Office address across India. Find India fastest growing property site offices details.',
                keywords: 'contact us, Makaan office address, Makaan office in India, Makaan Office address',
                metaTags: {
                    viewport: 'minimum-scale=1.0, width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no'
                }

            }
        }
    }, {
        url: ['/privacy-policy/buyer$/','/privacy-policy/seller$/','/privacy-policy$/'],
        moduleName: 'privacyPolicy',
        req: {
            skipSeo: true,
            "bread-crum": [
                [{
                    "label": "home",
                    "url": "/"
                }],
                [{
                    "label": "privacy policy",
                    "url": "#"
                }]
            ],
            meta: {
                title: 'Privacy Policy for Property, Real Estate Policy at Makaan.com',
                description: 'Makaan.com provides privacy policy for buyer and users. Searching privacy policy for property in India? Get properties policy in details at real estate site.',
                keywords: 'Property privacy policy, real estate policy, site policy, Makaan policy, Makaan, Makaan.com',
                metaTags: {
                    viewport: 'minimum-scale=1.0, width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no'
                }

            }
        }
    }, {
        url: '/terms$/',
        moduleName: 'termsNconditions',
        req: {
            skipSeo: true,
            "bread-crum": [
                [{
                    "label": "home",
                    "url": "/"
                }],
                [{
                    "label": "terms & conditions",
                    "url": "#"
                }]
            ],
            meta: {
                title: 'Terms and Condition to Use Makaan.com Services',
                description: 'Makaan.com provides term and condition to use the services. Find term and condition for property in India at real estate website.',
                keywords: 'Terms and condition, real estate site, property site, Makaan, Makaan.com',
                metaTags: {
                    viewport: 'minimum-scale=1.0, width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no'
                }
            }
        }
    }, {
        url: '/offline-page',
        moduleName: 'offlinePage',
        req: {
            skipSeo: true,
            "bread-crum": null,
            meta: {
                metaTags: {
                    viewport: 'minimum-scale=1.0, width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no'
                }
            }
        }
    }, {
        url: '/news-bytes/:postName([a-zA-Z0-9-]+)',
        moduleName: 'inTheNewsPostDetail',
        req: {
            skipSeo: true,
            "bread-crum": [
                [{
                    "label": "home",
                    "url": "/"
                }],
                [{
                    "label": "news bytes",
                    "url": "/news-bytes"
                }]
            ],
            meta: {
                title: 'Makaan in Media News &amp; Press Release: Makaan.com',
                description: 'Find news about Makaan.com – Makaan Latest News, Media press release coverage by Makaan online.',
                keywords: '',
                metaTags: {
                    viewport: 'minimum-scale=1.0, width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no'
                }
            }
        }
    }, {
        url: '/press-releases/:postName([a-zA-Z0-9-]+)',
        moduleName: 'inTheNewsPostDetail',
        req: {
            skipSeo: true,
            "bread-crum": [
                [{
                    "label": "home",
                    "url": "/"
                }],
                [{
                    "label": "press-releases",
                    "url": "/press-releases"
                }]
            ],
            meta: {
                title: 'Makaan in Media News &amp; Press Release: Makaan.com',
                description: 'Find news about Makaan.com – Makaan Latest News, Media press release coverage by Makaan online.',
                keywords: '',
                metaTags: {
                    viewport: 'minimum-scale=1.0, width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no'
                }
            }
        }
    }, {
        url: '/media-resources',
        moduleName: 'media',
        req: {
            skipSeo: true,
            "bread-crum": [
                [{
                    "label": "home",
                    "url": "/"
                }],
                [{
                    "label": "photos & video gallery",
                    "url": "#"
                }]
            ],
            meta: {
                title: 'Makaan Media News Photos &amp; Videos: Makaan.com',
                description: 'Makaan in News &amp; Media – Find latest photos and Videos news gallery on Makaan online.',
                keywords: '',
                metaTags: {
                    viewport: 'minimum-scale=1.0, width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no'
                }
            }
        }
    }, {
        url: '/press-releases',
        moduleName: 'pressReleases',
        req: {
            skipSeo: true,
            "bread-crum": [
                [{
                    "label": "home",
                    "url": "/"
                }],
                [{
                    "label": "press releases",
                    "url": "#"
                }]
            ],
            meta: {
                title: 'Makaan in top media press release: Makaan.com',
                description: 'Find top press release coverage on Makaan.com, Makaan in Press Release covered by Makaan online.',
                keywords: '',
                metaTags: {
                    viewport: 'minimum-scale=1.0, width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no'
                }
            }
        }
    }, {
        url: '/news-bytes',
        moduleName: 'newsBytes',
        req: {
            skipSeo: true,
            "bread-crum": [
                [{
                    "label": "home",
                    "url": "/"
                }],
                [{
                    "label": "makaan news",
                    "url": "#"
                }]
            ],
            meta: {
                title: 'Makaan News, Makaan in Newspaper &amp; Magazine: Makaan.com',
                description: 'Find news about Makaan.com – Makaan Latest News, Newspaper &amp; Magazine coverage by Makaan online.',
                keywords: '',
                metaTags: {
                    viewport: 'minimum-scale=1.0, width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no'
                }
            }
        }
    }, {
        url: '/business-world',
        moduleName: 'home_bw',
        req: {
            skipSeo: true,
            meta: {
                title: 'BW Businessworld real-estate',
                description: 'TBD',
                keywords: 'TBD',
                metaTags: {
                    viewport: 'minimum-scale=1.0, width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no'
                }

            }
        }
    }, {
        url: '/feedback',
        moduleName: 'feedback',
        req: {
            skipSeo: true,
            meta: {
                title: 'Feedback',
                description: '',
                keywords: '',
                metaTags: {
                    viewport: 'minimum-scale=1.0, width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no'
                }

            }
        }
    }, {
        url: '/_Gurgaon-Signature-Signum-93.html',
        moduleName: 'campaigns',
        req: {
            skipSeo: true,
            meta: {
                title: 'Gurgaon-Signature Campaign',
                description: '',
                keywords: '',
                metaTags: {
                    viewport: 'minimum-scale=1.0, width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no'
                }

            }
        }
    },{
        url: '/promotions',
        moduleName: 'autoLeadGeneration',
        req: {
            skipSeo: true,
            meta: {
                title: 'Makaan.com | Find Joy',
                description: '',
                keywords: '',
                metaTags: {
                    viewport: 'width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no',
                }
            }
        }
    },{
        url: '/makaan-select',
        moduleName: 'shareCoupon',
        req: {
            skipSeo: true,
            meta: {
                title: 'Makaan.com | Makaan Select',
                description: '',
                keywords: '',
                metaTags: {
                    viewport: 'width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no',
                }
            },
            pageData: {
                pageType: "MAKAAN_SELECT"
            }
        }
    },{
        url: '/sell-on-makaan',
        moduleName: 'sellOnMakaan',
        req: {
            skipSeo: true,
            meta: {
                title: 'Rent & Sell your Property on Makaan',
                description: 'Want to Sell your Property or Rent a House with easy step? Unlimited free listings on Makaan.com. Post your Property for Sale & Rent. Download Our Seller App',
                keywords: 'Post Property, Residential Property, Commercial Property, Sell Property, Real Estate online, Rent Property',
                metaTags: {
                    viewport: 'width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no',
                }
            }
        }
    }];


};
