"use strict";

var apiService = require('services/apiService'),
    apiConfig = require('configs/apiConfig'),
    globalConfig = require('configs/globalConfig'),
    utilService = require('services/utilService'),
    _ = utilService._;

// city url categories
const menuDrawerCityUrls = ['nearByCities','dynamicCity','city','otherTopCities'];
// config used in header Parser to give custom label to some keys
const menuDrawerRenames = {
    "project#": 'Similar Projects',
    "projectFromBuilder#": 'Projects from Same Builder',
    "nearByCities#": "Nearby Cities",
    "dynamicCity#": "Top Cities",
    "city#": "Top Cities",
    "otherTopCities#": "Other Top Cities"
}
// myinfo -- Properties in %pageLevel%
// myinfoCustom -- custom label setup for showing a level under than current
// %pageLevel% -- replacing annotated string with value
var config = {
    "_common": {
        "Recent Projects": "recentProject",
        "Top Cities": "aggroCity"
    },
    "broker": {
        //"Top Brokers": "brokers"
    },
    "builder": {
    },
    "project_overview": {
    },
    "state": {
        "myInfoCustom Buy in Top Cities":"state",
        "myInfoCustom Rent in Top Cities":"state",
        "Current": "-"
    },
    "city": {
        "myInfo":"current",
        "Top Localities": "locality",
        "Builders%pageLevel%": "builders",
        "Brokers%pageLevel%": "brokers",
        "Current": "-"
    },
    "city_overview": {
        "myInfoBuy":"current",
        "myInfoRent":"current",
        "Buy In Top Localities": "locality",
        "Rent In Top Localities":"locality",
        "Builders%pageLevel%": "builders",
        "Brokers%pageLevel%": "brokers",
        "Current": "city"
    },
    "locality_overview": {
        "myInfoBuy":"current",
        "myInfoRent":"current",
        "Buy In Nearby Localities": "locality",
        "Rent In Nearby Localities":"locality",
        "Builders%pageLevel%": "builders",
        "Brokers%pageLevel%": "brokers",
        "Current": "-"
    },
    "locality": {
        "myInfo":"current",
        "Nearby Localities": "locality",
        "Builders%pageLevel%": "builders",
        "Brokers%pageLevel%": "brokers",
        "Current": "-" 
    },
    "nearby": {
        "myInfo":"current",
        "Nearby Localities": "locality",
        "Nearby Landmarks": "landmarks",
        "Builders%pageLevel%": "builders",
        "Brokers%pageLevel%": "brokers",
        "Current": "-" 
    },
    "suburb": {
        "myInfo":"current",
        "Nearby Localities": "locality",
        "Builders%pageLevel%": "builders",
        "Brokers%pageLevel%": "brokers",
        "Current": "-" 
    },
    "home": {
        "Top Builders": "builders",
        "Top Brokers": "brokers",
        "Current": "home"
    }
};
const CURRENT = "Current";
const OTHER = "other"
const STEP = "Step";
const RENT = "rent";
const BUY = "buy";


var seoService = {};
seoService.mapTypeIdToPageType={
    'project':1,
    'builder':3,
    "locality": 4,
    'home':5,
    'city':6,
    'suburb':7,
    'nearby':9,
    'broker':15,
    'state':16
};

seoService.getSeoTags = function(url, req, res) {
    url = url || '';
    if (url.slice(0, 1) === '/') {
        url = url.slice(1);
    }
    //  removeing the forward slash
    if (url.slice(-1) === "/") {
        url = url.slice(0, -1);
    }
    url = encodeURIComponent(url);

    if(req.query && !isNaN(req.query.page)){
        url += '&page='+req.query.page;
    }
        
    if(!req.locals) req.locals = {};
    var api = apiConfig.seoService({ url });
    return apiService.get(api, { req }).then(response => {
        var format = {}, urlDetail = response && response.data && response.data.urlStatus && response.data.urlStatus.urlDetail || {};
        format.code = urlDetail.countryDisplayName && urlDetail.countryDisplayName.toLowerCase() || "";
        format.id = urlDetail.countryId;
        switch(format.code) {
            case 'ae' :
                format.format = 3;
                format.isConnectNow = false;
                format.seoLinks = false;
                break;
            default : 
                format.format = 2;
                format.isConnectNow = true;
                format.seoLinks = true;
                break;
        }
        res.cookie('numberFormat', format);
        req.locals.numberFormat = format;
        return response;
    });
};

seoService.getEntityUrls = function(entities = [], req) {  //jshint ignore:line
    if (entities.length) {
        var api = apiConfig.getEntityUrl({ urlParams: JSON.stringify(entities) });
        return apiService.get(api, { req }).then((response) => {
            response = (response && response.data) || [];
            let result = {},
                category, tempArr, id;
            _.forEach(response, (v, k) => {
                tempArr = k.split('-');
                category = tempArr[0];
                id = tempArr[1];
                result[category] = result[category] || {};
                result[category][id] = utilService.prefixToUrl(v);
            });
            return result;
        }, (err) => {
            throw err;
        });
    } else {
        return new Promise((resolve, reject) => {
            reject('provide relevant parameters');
        });
    }
};

seoService.parseBreadcrum = function(data, layoutData) {
    let breadcrumLinks = [];
    _.forEach(data, (v) => {
        if(v && v.length){
            let temp = v.map(function(val) {
                val.url = (val.url === '#') ? val.url : utilService.prefixToUrl(val.url);
                return val;
            });
            breadcrumLinks.push({
                "main": temp.shift(),
                "links": temp
            });
        }
    });

    if (breadcrumLinks[breadcrumLinks.length-1] && breadcrumLinks[breadcrumLinks.length-1].links && breadcrumLinks[breadcrumLinks.length-1].links.length>1){
        let labelName = _getLastLabel(layoutData);
        if (labelName) {
            breadcrumLinks.push({
                "main": { "label": labelName, "url": "#" },
                "links": []
            });
        }
    }
    else if(breadcrumLinks[breadcrumLinks.length-1] && breadcrumLinks[breadcrumLinks.length-1].main && breadcrumLinks[breadcrumLinks.length-1].main.url){
        breadcrumLinks[breadcrumLinks.length-1].main.url = "#";
        breadcrumLinks[breadcrumLinks.length-1].links = [];
    }
    return breadcrumLinks;
};

function _getLastLabel(layoutData) {
    let pageData = layoutData.pageData,
        moduleName = pageData && pageData.modulePath && pageData.modulePath.split('/')[1],
        pageLevel = pageData && pageData.pageLevel,
        labelName;
    switch (moduleName) {
        case 'serp':
            if (pageLevel && ["builder", "buildercity"].indexOf(pageLevel.toLowerCase()) >= 0) {
                labelName = 'properties by ' + pageData.builderName;
            } else {
                labelName = 'All Residential';
            }
            break;
        case 'cityOverview':
            labelName = pageData.cityName + " real estate";
            break;
        case 'localityOverview':
            labelName = pageData.localityName + " real estate";
            break;
    }
    return labelName;
}

seoService.parseNavigationLink = function(data) {
    let taxonomy = [],
        taxonomyLinks = [],
        iconPriority = globalConfig.navigationIconPriority.slice(),
        parsedData = {},
        iconDetails;
    _.forEach(data, (categories) => {
        _.forEach(categories, (element) => {
            let label = element.label && element.label.toLowerCase(),
                iconType = label && label.split(" ").find(function(element) {
                    return (iconPriority.indexOf(element) > -1);
                });

            iconPriority.splice(iconPriority.indexOf(iconType), 1);
            iconDetails = _getIconDetails(iconType);
            taxonomy.push({
                label: label.substring(0, label.lastIndexOf(' in ')),
                address: label.substring(label.lastIndexOf(' in ')),
                url: utilService.prefixToUrl(element.url),
                iconType: iconDetails.iconType,
                iconPath: iconDetails.iconPath
            });
        });
    });
    parsedData = { allData: taxonomy.slice(), parsedData: taxonomyLinks };
    while (taxonomy.length > 0) {
        taxonomyLinks.push(taxonomy.splice(0, 4));
    }
    return parsedData;
};

seoService.getAllSeoUrls = function(objectId, objectType, req) {
    let urlObj = apiConfig.getAllSeoUrls({ objectId, objectType });
    return apiService.get(urlObj, { req }).then((response) => {
        return response;
    }, (err) => {
        throw err;
    });
};

function _getIconDetails(iconType) {
    let iconPath;
    iconType = ((typeof iconType != 'undefined') ? iconType : "default" + Math.ceil(Math.random() * 8));
    iconPath = "/images/taxonomy-icon/" + iconType + ".png";
    return {
        iconType,
        iconPath
    };
}

function _getHeaderEntityName(val, pageLevel, req) {
    const obj = req || {};
    // Appending correct label based on the 'myInfo'
    var entity = val, 
        pageLevelName = pageLevel,
        verbLocation = (pageLevelName=='nearby'?' near ':' in '),
        locName = obj.pageData && obj.pageData[pageLevelName+'Name'] || '';
    if(entity=='myInfoBuy'){
      entity = "myInfoCustom Property for Sale"+verbLocation+locName;
    } else if(entity=='myInfoRent'){
      entity = "myInfoCustom Property for Rent"+verbLocation+locName;
    } else if(entity=='myInfo'){
      entity = "myInfoCustom Property"+verbLocation+locName;
    }

    // replacing step-1 annotated string with value
    let toReplaceText = entity.match(/%(.*)%/);
    switch (toReplaceText && toReplaceText[1]){
        case 'pageLevel':
            if(locName && locName!=""){
                entity = entity.replace(/%.*%/,verbLocation+locName);
            } else {
                entity = 'Top '+entity.replace(/%.*%/,'');
            }
            break;
    }

    return entity;
}

function ampDrawerParser(hcSections, pLevel, req) {
    const   data=req["header-data"] || {},
            seo_page_type=req["header-seo-pageType"],
            isRental=req["header-seo-isRent"],
            pageLevelName = pLevel;
    let headerContentSections = [];

    // Defaults
    if(!hcSections["Top Cities"] || !data[hcSections["Top Cities"]] || (typeof data[hcSections["Top Cities"]] == "object" && utilService.isEmptyObject(data[hcSections["Top Cities"]])) || (Array.isArray(data[hcSections["Top Cities"]]) && data[hcSections["Top Cities"]].length == 0)){
        hcSections["Top Cities"] = (req.locals && req.locals.numberFormat && req.locals.numberFormat.code == "ae") ? [
            {label:"Property in Dubai", url:"ae/real-estate-dubai-property"}
        ] : [
            {label:"Property in Bangalore",url:"real-estate-bangalore-property"},
            {label:"Property in Mumbai",url:"real-estate-mumbai-property"},
            {label:"Property in Pune",url:"real-estate-pune-property"},
            {label:"Property in Noida",url:"real-estate-noida-property"},
            {label:"Property in Gurgaon",url:"real-estate-gurgaon-property"},
            {label:"Property in Hyderabad",url:"real-estate-hyderabad-property"},
            {label:"Property in Ahmedabad",url:"real-estate-ahmedabad-property"},
            {label:"Property in Chennai",url:"real-estate-chennai-property"},
            {label:"Property in Kolkata",url:"real-estate-kolkata-property"},
            {label:"Property in Delhi",url:"real-estate-delhi-property"},
            {label:"All Cities in India",url:"all-cities-overview"}
        ];
    }
    if(!hcSections["Top Builders"] || !data[hcSections["Top Builders"]] || (typeof data[hcSections["Top Builders"]] == "object" && utilService.isEmptyObject(data[hcSections["Top Builders"]])) || (Array.isArray(data[hcSections["Top Builders"]]) && data[hcSections["Top Builders"]].length == 0)){
        hcSections["Top Builders"] = (req.locals && req.locals.numberFormat && req.locals.numberFormat.code == "ae") ? undefined : [
            {label:"Prestige Group", url: "/prestige-group-100365"},
            {label:"TATA Housing Development", url: "/tata-housing-development-100025"},
            {label:"Central Park", url: "/central-park-106084"},
            {label:"DLF", url: "/dlf-100002"},
            {label:"Aparna Group", url: "/aparna-101406"},
            {label:"Lodha Group", url: "/lodha-group-100089"},
            {label:"Xrbia", url: "/xrbia-102595"},
            {label:"Puravankara", url: "/puravankara-100046"},
            {label:"Omaxe", url: "/omaxe-100361"},
            {label:"Godrej Properties", url: "/godrej-properties-100111"},
            {label:"Shapoorji Pallonji Real Estate", url: "/shapoorji-pallonji-real-estate-103027"},
            {label:"Ajmera", url: "/ajmera-100309"},
            {label:"Ambuja Neotia", url: "/ambuja-neotia-105312"},
            {label:"Bakeri", url: "/bakeri-100550"},
            {label:"Ceebros", url: "/ceebros-100978"},
            {label:"Paranjape Schemes", url: "/paranjape-schemes-100696"},
            {label:"Manju", url: "/manju-102226"},
            {label:"Sobha Limited", url: "/sobha-limited-100052"},
            {label:"Shine City", url: "/shine-city-109277"},
            {label:"PS Group", url: "/ps-group-101690"},
            {label:"Total Environment", url: "/total-environment-100825"},
            {label:"Mohan", url: "/mohan-100357"},
            {label:"Hiranandani Developers", url: "/hiranandani-developers-100059"},
            {label:"Unitech", url: "/unitech-100027"},
            {label:"Brigade", url: "/brigade-100376"},
            {label:"Amrapali", url: "/amrapali-100029"},
            {label:"Kolte Patil", url: "/kolte-patil-100168"},
            {label:"Ansal API", url: "/ansal-api-100316"},
            {label:"ATS Green", url: "/ats-green-100806"},
            {label:"Reliable", url: "/reliable-101946"},
            {label:"Akshar", url: "/akshar-100310"},
            {label:"All Builders In India", url: "/all-builders"}
        ];
    }
    // Temporarily show an overriding static list of Brokers
    // if(!hcSections["Top Brokers"] || !data[hcSections["Top Brokers"]] || (typeof data[hcSections["Top Brokers"]] == "object" && utilService.isEmptyObject(data[hcSections["Top Brokers"]])) || (Array.isArray(data[hcSections["Top Brokers"]]) && data[hcSections["Top Brokers"]].length == 0)){
    //     hcSections["Top Brokers"] = (req.locals && req.locals.numberFormat && req.locals.numberFormat.code == "ae") ? hcSections["Top Brokers"] : [
    //         {label:"Proptiger",url:"/proptiger-profile-499"},
    //         {label:"Square Yards",url:"/square-yards-profile-308603"},
    //         {label:"Sweet homes",url:"/sweet-homes-profile-690235"},
    //         {label:"Zenify",url:"/zenify-profile-167239"},
    //         {label:"profuse",url:"/profuse-profile-545681"},
    //         {label:"SHRI RAM PROPERTIES",url:"/shri-ram-properties-profile-700921"},
    //         {label:"subramanyam",url:"/subramanyam-profile-129565"},
    //         {label:"Home India",url:"/home-india-profile-732774"},
    //         {label:"Square Properties",url:"/square-properties-profile-270258"},
    //         {label:"DREAM HOME",url:"/dream-home-profile-255996"},
    //         {label:"Swastik Enterprises",url:"/swastik-enterprises-profile-622222"},
    //         {label:"CBRE SOUTH ASIA PVT LTD",url:"/cbre-south-asia-pvt-ltd-profile-743649"},
    //         {label:"NBR",url:"/nbr-profile-606722"},
    //         {label:"Aadhar Homes",url:"/aadhar-homes-profile-267352"},
    //         {label:"mw group",url:"/mw-group-profile-729491"},
    //         {label:"Shiv Shakti Enterprises",url:"/shiv-shakti-enterprises-profile-656018"},
    //         {label:"Squareone",url:"/squareone-profile-128090"},
    //         {label:"Home Planners",url:"/home-planners-profile-250252"},
    //         {label:"Shine Group",url:"/shine-group-profile-714242"},
    //         {label:"Balaji Group",url:"/balaji-group-profile-685888"}
    //     ];
    // }

    // Initial First-List Fill up
    var initMenuList = [];
    headerContentSections.push({id:0,prev: -1,content:initMenuList});
    Object.keys(hcSections).forEach((hKey)=>{
        const newKey = _getHeaderEntityName(hKey, pageLevelName, req).replace(/myInfo(?:Custom)?\s?/,'');
        var initMenuItem = {
            id: 0,
            label: newKey
        }
        initMenuList.push(initMenuItem);
        if(Array.isArray(hcSections[hKey]) || typeof hcSections[hKey] == "object"){
            _sectionParser(hcSections[hKey], initMenuItem);
        } else if (data[hcSections[hKey]]) {
            _sectionParser(data[hcSections[hKey]], initMenuItem);
        } else {
            return;
        }
    });

    // return Finalized Output
    return headerContentSections;

    // Recursive function to parse data through DFS-traversal into linear lists
    function _sectionParser(base, parent = {}) {
        // If content is array, then it is contains list of links [skip to subGroup if present]
        if (Array.isArray(base)) {
            const deepBase = (base.length == 1 && base[0].subGroup)
            headerContentSections.push({
                id: headerContentSections.length,
                prev: parent.id,
                // Remove initial 'Top' from top label
                label: deepBase ? base[0].label || parent.label : parent.label.replace(/^(top\s)((?:.*?)for\s(?:sale|rent)\sin\s(?:.*))/i,'$2'),
                labelUrl: deepBase ? base[0].url && utilService.prefixToUrl(base[0].url) : undefined,
                content: (deepBase ? base[0].subGroup : base).map(lElem => { return {url: utilService.prefixToUrl(lElem.url), label: lElem.label, bottom: lElem.prefferedUrlLabel == "more" ? true : undefined}})
            });

            // When done, mark the recently entered object in list as child.
            parent["next"] = headerContentSections.length-1;

        // expected an Object or Array [Recursion-End condition]
        } else if(typeof base != "object") {
            return;

        // If entity is an Object
        // - If entity subGroup => Make a list of all entites and iterate deeper for all subGroups
        // - If entity does not contain subGroup => Do not push this entity and keep its values as duplicatem, and iterate deeper
        } else {

            // [Special Case - If page is rental and entity has 'rent' object => dive deeper into rent. vis-a-vis for buy.]
            if (typeof isRental == "undefined") {
                base = (~(parent.label||'').search(/rent/i) ?  base.rent : base.buy) || base;
            } else if(isRental && base.rent) {
                base = base.rent;
            } else if(!isRental && base.buy){
                base = base.buy;
            }

            // Wrapper
            // -- Note: If a subGroup key is not present in an object, list them with heading of parent and dive-deeper
            var memberObj = {
                id: headerContentSections.length,
                prev: parent.id,
                labelUrl: base.url && utilService.prefixToUrl(base.url),
                label: base.subGroup ? base.label : parent.label,
                content: []
            }, overrideBase = base.subGroup ? base.subGroup : base; // [skip to subGroup if present]

            // [Special Case - An Entity got misindentified]
            if(Array.isArray(overrideBase)) {
                return _sectionParser(overrideBase, parent);
            }
            // [Special Case - An Entity with 1 Key is useless]
            else if(Object.keys(base).length == 1) {
                return _sectionParser(base[Object.keys(base)[0]], parent);
            // [Special case - 'prefferedUrlLabel' with 'more' denotes final link ]
            } else if(base.prefferedUrlLabel == "more") {
                return;
            }
            // Push Wrapper into the list [fill the wrapper-entries below and iterate deeper]
            headerContentSections.push(memberObj);
            // When done, mark the entered object in list as child.
            parent["next"] = memberObj.id;
            
            // For all keys in object
            // => to fill into wrapper
            // => dive deeper if "prefferedUrlLabel is not more"
            Object.keys(overrideBase).forEach((bEntity)=>{
                var objListElem = {
                    id: memberObj.id,
                    url: base.subGroup ? undefined : utilService.prefixToUrl(base[bEntity].url),
                    labelUrl: base.subGroup ? base.url && utilService.prefixToUrl(base.url) : undefined,
                    // Need to create custom-Labels for subGroup keys
                    label: (()=>{
                        // If it is a city overview|aggregation url
                        if (isNaN(bEntity[bEntity.length-1]) && overrideBase[bEntity].urlType) {
                            return overrideBase[bEntity].label || '';
                        // Create label from key if subgroup exists
                        } else if (base.subGroup) {
                            let pluralString = utilService.getPluralWord(bEntity,{breakCase:true}),
                                serviceString = ((isRental || ~(parent.label||'').search(/rent/i) || ~bEntity.search(/rent/i))?" Rent":" Sale");
                            return `Top ${pluralString} for ${serviceString} in ${base.objectLabel}`;
                        } else {
                            return base[bEntity].label||'';
                        }
                    })(),
                    bottom: overrideBase[bEntity].prefferedUrlLabel == "more" ? true : undefined
                }
                // Fill Wrapper
                memberObj.content.push(objListElem);
                // Dive Deeper for more entries
                _sectionParser(overrideBase[bEntity], objListElem);
            });
            
        }
        return;
    }
}

seoService.headerParser = function(obj) {
    let data=obj["header-data"] || {},
        seo_page_type=obj["header-seo-pageType"],
        isRental=obj["header-seo-isRent"],
        headerContentSections = _.cloneDeep(config[seo_page_type] || {});

    _.forEach(config["_common"], (v,k)=>{
        if(!headerContentSections[k]){
            headerContentSections[k] = v;
        }
    });

    let isSeoLinks = obj.locals && obj.locals.numberFormat && obj.locals.numberFormat.seoLinks;
    if(!isSeoLinks) {
      delete headerContentSections["Top Builders"];
    }

    let pageLevelName = obj.urlDetail && obj.urlDetail.pageLevel || "";
    let result = {};
    if (headerContentSections) {
      _.forEach(data[headerContentSections[CURRENT]], (v, k)=>{
          data[k] = v;
          // headerContentSections[k] = k;
      });
      delete data[headerContentSections[CURRENT]];
      delete headerContentSections[CURRENT];

      // getting stuff out of other to header-data
      // Set label if there is none, and set step-1 string from menuDrawerRenames
      _.forEach(data[OTHER], (v, k)=>{
        if(isNaN(k.slice(0,-1))){
            headerContentSections[menuDrawerRenames[k]] = k;
        } else {
            headerContentSections[v.label] = k;
        }
        data[k] = v.subGroup;
      });
      delete data[OTHER];

      // aggregating city urls together
      data['aggroCity'] = {};
      menuDrawerCityUrls.forEach(city=>{
        if(!data[city]) return;
        let tempAggroCity = {};
        tempAggroCity[city+'#'] = {
            label: menuDrawerRenames[city+'#'] || city,
            urlType: 'cityAggregation',
            subGroup: data[city]
        };
        Object.assign(data['aggroCity'], tempAggroCity);
        delete data[city];
      });
      // remove if empty
      let aggroKeys = Object.keys(data['aggroCity']);
      switch(aggroKeys.length) {
        case 1:
            data['aggroCity'] = data[aggroKeys[0]];
            break; 
        case 0:
            delete data['aggroCity'];
            break;
      }
      aggroKeys = null;

      //for finding the most appropriate pageLevel for header
      if(pageLevelName.indexOf("state")>=0){
        pageLevelName = 'state';
      } else if(pageLevelName.indexOf("city")>=0){
        pageLevelName = 'city';
      } else if(pageLevelName.indexOf("suburb")>=0){
        pageLevelName = 'suburb';
      } else if(pageLevelName.indexOf("locality")>=0){
        pageLevelName = 'locality';
      } else if(pageLevelName.indexOf("nearby")>=0){
        pageLevelName = 'nearby';
      } else {
        pageLevelName = 'locality';
      }

      // Using a common MenuDrawer-Parser to convert `header` to staged <ul> with <li> elements
      return ampDrawerParser(headerContentSections, pageLevelName, obj);
    }
    return result;
};

module.exports = seoService;
