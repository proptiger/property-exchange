"use strict";
const apiService = require('services/apiService'),
    apiConfig = require('configs/apiConfig'),
    utilService = require('services/utilService'),
    seoService = require('services/seoService'),
    masterDetailService = require('services/masterDetailsService'),
    globalConfig = require('configs/globalConfig'),
    logger = require('services/loggerService'),
    localityService = require('services/localityService'),
    imageParser = require('services/imageParser'),
    _ = utilService._;

var cityService = {};

var config = {
    "apiKeyMap": {
        "cityName": "label",
        "cityId": "id",
        "latitude": "centerLatitude",
        "longitude": "centerLongitude",
        "fullDescription": "description",
        "smallDescription": "description",
        "imagery": "cityHeroshotImageUrl",
        "lifestyleDescription": "entityDescriptions",
        "cityTagLine": "cityTagLine",
        "buyUrl": "buyUrl",
        "rentUrl": "rentUrl"
    },
    "propertyRangeDropdowns": [{
        "id": "cityPageListingDropdown",
        "type": "buyOrRent",
        "config": {
            "grouping": 5
        }
    }, {
        "id": "cityPagePropertyTypeDropdown",
        "type": "propertyType",
        "multiselect": true,
        "config": {
            "grouping": 1
        }
    }, {
        "id": "cityPagebedsDropdown",
        "type": "beds",
        "multiselect": true,
        "config": {
            "grouping": 5,
            "suffix": "bhk"
        }
    }],
    "neighbourhoodTabs": [{
        "label": 'Master Plan',
        "targetId": 'masterPlanContent'
    }, {
        "label": 'Neighbourhood',
        "targetId": 'neighbourhoodContent',
        "selected": true
    }],
    "allowedRanges": {
        "cityMaxPriceAllowed": 20000000,
        "cityMinPriceAllowed": 1500000,
        "cityMinRentAllowed": 2000,
        "cityMaxRentAllowed": 200000,
    }
};

cityService.getMinMaxListingTypePrices = function(data = {}) {
    let buyMinPrice = 0,
        buyMaxPrice = 0,
        rentMinPrice = 0,
        rentMaxPrice = 0;
    for (let i = 0; i < data.length; i++) {
        if (["Primary", "Resale"].indexOf(data[i].listingCategory) >= 0) {
            buyMinPrice = (buyMinPrice === 0 || data[i].minPrice < buyMinPrice) ? data[i].minPrice : buyMinPrice;
            buyMaxPrice = (buyMaxPrice === 0 || data[i].maxPrice > buyMaxPrice) ? data[i].maxPrice : buyMaxPrice;
        }
        if (["Rental"].indexOf(data[i].listingCategory) >= 0) {
            rentMinPrice = (rentMinPrice === 0 || data[i].minPrice < rentMinPrice) ? data[i].minPrice : rentMinPrice;
            rentMaxPrice = (rentMaxPrice === 0 || data[i].maxPrice > rentMaxPrice) ? data[i].maxPrice : rentMaxPrice;
        }
    }
    return {
        buyMinPrice,
        buyMaxPrice,
        rentMinPrice,
        rentMaxPrice
    };
};

function _parseCityData(data, req) {

    let parsedData = {},
        keys = Object.keys(config.apiKeyMap),
        length = keys.length,
        i,
        shortLength,
        listingAggregations,
        numberFormat = req.locals.numberFormat;

    for (i = 0; i < length; i++) {
        parsedData[keys[i]] = data[config.apiKeyMap[keys[i]]];
    }

    parsedData.buyUrl = utilService.prefixToUrl(parsedData.buyUrl);
    parsedData.rentUrl = utilService.prefixToUrl(parsedData.rentUrl);

    parsedData.annualGrowth = utilService.roundNumber(data.annualGrowth, 2);
    parsedData.rentalYield = utilService.roundNumber(data.rentalYield, 2);
    parsedData.demandRate = utilService.roundNumber(data.demandRate, 2);
    parsedData.supplyRate = utilService.roundNumber(data.supplyRate, 2);
    shortLength = utilService.isMobileRequest(req) ? globalConfig.wordLimit.mobile : globalConfig.wordLimit.desktop;

    let strippedDescription = utilService.stripHtmlContent(parsedData.smallDescription, shortLength, true);

    parsedData.smallDescription = strippedDescription.description;
    parsedData.hideViewMore = strippedDescription.error;
    parsedData.textShortLength = shortLength;
    parsedData.propertyRangeDropdowns = config.propertyRangeDropdowns;
    let isMasterPlanSupported = utilService.isMasterPlanSupported({
        id: parsedData.cityId
    });
    parsedData.tabs = _.extend([], config.neighbourhoodTabs);
    listingAggregations = cityService.getMinMaxListingTypePrices(data.listingAggregations || {});
    parsedData.buyMinPrice = listingAggregations.buyMinPrice || config.allowedRanges.cityMinPriceAllowed;
    parsedData.buyMaxPrice = listingAggregations.buyMaxPrice || config.allowedRanges.cityMaxPriceAllowed;
    parsedData.rentMinPrice = listingAggregations.rentMinPrice || config.allowedRanges.cityMinRentAllowed;
    parsedData.rentMaxPrice = listingAggregations.rentMaxPrice || config.allowedRanges.cityMaxRentAllowed;
    parsedData.maxAllowedPrice = parsedData.buyMaxPrice > config.allowedRanges.cityMaxPriceAllowed ? config.allowedRanges.cityMaxPriceAllowed : parsedData.buyMaxPrice;
    parsedData.minAllowedPrice = parsedData.buyMinPrice < config.allowedRanges.cityMinPriceAllowed ? config.allowedRanges.cityMinPriceAllowed : parsedData.buyMinPrice;
    parsedData.maxAllowedRent = parsedData.rentMaxPrice > config.allowedRanges.cityMaxRentAllowed ? config.allowedRanges.cityMaxRentAllowed : parsedData.rentMaxPrice;
    parsedData.minAllowedRent = parsedData.rentMinPrice < config.allowedRanges.cityMinRentAllowed ? config.allowedRanges.cityMinRentAllowed : parsedData.rentMinPrice;
    parsedData.formatted = {
        buyMinPrice: utilService.formatNumber(parsedData.buyMinPrice, { returnSeperate : true, seperator : numberFormat }),
        buyMaxPrice: utilService.formatNumber(parsedData.buyMaxPrice, { returnSeperate : true, seperator : numberFormat }),
        rentMinPrice: utilService.formatNumber(parsedData.rentMinPrice, { returnSeperate : true, seperator : numberFormat }),
        rentMaxPrice: utilService.formatNumber(parsedData.rentMaxPrice, { returnSeperate : true, seperator : numberFormat }),
        maxAllowedPrice: utilService.formatNumber(parsedData.maxAllowedPrice, { returnSeperate : true, seperator : numberFormat }),
        minAllowedPrice: utilService.formatNumber(parsedData.minAllowedPrice, { returnSeperate : true, seperator : numberFormat }),
        maxAllowedRent: utilService.formatNumber(parsedData.maxAllowedRent, { returnSeperate : true, seperator : numberFormat }),
        minAllowedRent: utilService.formatNumber(parsedData.minAllowedRent, { returnSeperate : true, seperator : numberFormat })
    };

    if (!isMasterPlanSupported) {
        parsedData.tabs.splice(0, 1); // since masterplan is 0th element
    }

    parsedData.isMasterPlanSupported = isMasterPlanSupported;
    parsedData.lifestyleDescription = localityService.parseLifeStyleContent(parsedData.lifestyleDescription, data.images);
    parsedData.imagery = imageParser.appendImageSize(parsedData.imagery, 'large');
    parsedData.projectCount = data.projectCount || false;

    return parsedData;
}

cityService.getCityOverview = function(cityId, req, { selectorFields, needRaw }) {
    let selector,
        defaultFields,
        querySelector = {},
        urlConfig;
    defaultFields = [
        "id",
        "centerLatitude",
        "centerLongitude",
        "description",
        "cityHeroshotImageUrl",
        "annualGrowth",
        "rentalYield",
        "demandRate",
        "supplyRate",
        "label",
        "listingAggregations",
        "buyUrl",
        "rentUrl",
        "entityDescriptions",
        "description",
        "entityDescriptionCategories",
        "masterDescriptionCategory",
        "name",
        "images",
        "imageType",
        "absolutePath",
        "displayName",
        "masterDescriptionParentCategories",
        "parentCategory",
        "cityTagLine",
        "projectCount"
    ];

    querySelector.fields = defaultFields;
    if (selectorFields && (typeof selectorFields === 'object')) {
        querySelector.fields = selectorFields;
    }
    selector = apiService.createSelector(querySelector);
    urlConfig = apiConfig.cityOverview({
        cityId,
        selector
    });

    return apiService.get(urlConfig, { req: req, isPrimary: true }).then(function(response) {
        if (needRaw) {
            return response.data;
        } else {
            return _parseCityData(response.data, req);
        }
    }, err => {
        throw err;
    });
};

cityService.getCityIdByName = function(name) {
    if (!name) {
        return null;
    }
    let cityList = masterDetailService.getMasterDetails('cityList');
    let cityId;
    _.forEach(cityList, (v) => {
        if (v.label.toLowerCase() === name.toLowerCase()) {
            cityId = v.id;
            return;
        }
    });
    return cityId;
};
cityService.getCityNameById = function(id) {
    if (!parseInt(id)) {
        return;
    }
    id = parseInt(id);
    let cityList = masterDetailService.getMasterDetails('cityList');
    let cityName;
    _.forEach(cityList, (v) => {
        if (v.id === id) {
            cityName = v.label.toLowerCase();
            return;
        }
    });
    return cityName;
};

cityService.getAllianceCityNameById = function(id) {
    if (!parseInt(id)) {
        return;
    }
    let cityList = masterDetailService.getMasterDetails('allianceCityList');
    let cityName;
    _.forEach(cityList, (v) => {
        if (v.id === id) {
            cityName = v.label.toLowerCase();
            return;
        }
    });
    return cityName;
};

cityService.getUserCityByIp = function(req) {
    return apiService.get(apiConfig.getUserCityByIp(), { req }).then(response => {
        let finalResponse = {};
        if (response && response.data && response.data.city) {
            let city = response.data.city;
            finalResponse = {
                id: city.id,
                label: city.label,
                country: response.data.userCountry
            };
            return finalResponse;
        }
        return;
    });
};

function parseAllCity(data, {req, config = {}}) {
    let parsedData = [];
    let extraFields = ["id",
        "label",
        "overviewUrl",
        "buyUrl",
        "rentUrl",
        "buyTrendUrl",
        "rentTrendUrl",
        "projectCount",
        "avgPriceRisePercentage",
        "avgPriceRisePercentageApartment",
        "avgPriceRisePercentagePlot",
        "avgPriceRisePercentageVilla",
        "avgPriceRiseMonths",
        "avgPricePerUnitArea",
        "avgPricePerUnitArea",
        "avgPricePerUnitArea",
        "avgPricePerUnitArea",
        "avgPricePerUnitAreaApartment",
        "avgPricePerUnitAreaPlot",
        "avgPricePerUnitAreaVilla",
        "priceRiseRank",
        "priceRiseRankPercentage",
    ];
    let trendUrls = config.trendUrls || {};
    _.forEach(data, (v) => {
        let cityData = {};
        cityData = localityService.parseLocalityListingAgg(v.listingAggregations, _.assign({
            cityId: v.id
        }), {req});
        _.forEach(extraFields, (val) => {
            cityData[val] = v[val];
        });
        cityData['overviewUrl'] = utilService.prefixToUrl(cityData['overviewUrl']);
        cityData['buyTrendUrl'] = utilService.prefixToUrl(trendUrls.buy && trendUrls.buy[v.id]);
        cityData['rentTrendUrl'] = utilService.prefixToUrl(trendUrls.rent && trendUrls.rent[v.id]);
        cityData['buyUrl'] = utilService.prefixToUrl(cityData['buyUrl']);
        cityData['rentUrl'] = utilService.prefixToUrl(cityData['rentUrl']);
        cityData.city = {
            buyUrl: utilService.prefixToUrl(v.buyUrl),
            overviewUrl: utilService.prefixToUrl(v.overviewUrl),
            rentUrl: utilService.prefixToUrl(v.rentUrl)
        };
        parsedData.push(cityData);
    });
    return parsedData;
}

cityService.getAllCityList = function(config = {}, { req }) { //jshint ignore:line

    var selector = {
        "paging": {
            "start": config.start || 0,
            "rows": config.rows
        }
    };
    if (config.fields){
        selector.fields = config.fields;
    }
    if(config.countryId) {
        selector.filters = {"and": [{ 
            "equal" : {
                'countryId': config.countryId
            }
        }]};
    }
    
    let urlConfig = apiConfig.masterCityList({ query: { selector: JSON.stringify(selector) } });

    return apiService.get(urlConfig, { req }).then(response => {
        if (config.needRaw) {
            return response || {};
        } else {
            let totalCount = response.totalCount;
            response = (response && response.data) || [];
            return {
                data: parseAllCity(response, {config, req}),
                totalCount
            };
        }
    }, error => {
        throw error;
    });
};

cityService.getAllCityWithPriceTrendUrl = function(config = {}, { req }) { //jshint  ignore:line
    config.needRaw = true;
    let cityDetailsList = cityService.getAllCityList(config, { req });
    let cityIds = [];
    let seoUrlParams = [];
    config.templates = config.templates || {};
    config.fields = ["id", "label", "url", "isServing", "isServingResale", "isServing", "isServingResale", "avgPricePerUnitArea", "avgPricePerUnitAreaApartment", "avgPricePerUnitAreaPlot", "avgPricePerUnitAreaVilla", "avgPriceRisePercentage", "avgPriceRisePercentageApartment", "avgPriceRisePercentagePlot", "avgPriceRisePercentageVilla", "avgPriceRisePercentageBuilderFloor", "avgPriceRiseMonths", "dominantUnitType", "overviewUrl", "buyUrl", "rentUrl", "cityLocalityCount", "cityPropertyCount", "cityProjectCount", "listingCountBuy", "listingCountRent", "annualGrowth", "rentalYield", "supplyRate", "averageListingQualityScore","listingAggregations"];
    if (!utilService._.isEmpty(config.templates)) {
        utilService._.forEach(masterDetailService.getMasterDetails('cityList'), (v) => {
            cityIds.push(v.id);
        });
        utilService._.forEach(config.templates, (v) => {
            seoUrlParams.push({
                "urlDomain": "city",
                "domainIds": cityIds,
                "urlCategoryName": v
            });
        });
        let entityUrls = seoService.getEntityUrls(seoUrlParams, req);
        return Promise.all([cityDetailsList, entityUrls]).then((response) => {
            let totalCount = response[0].totalCount;
            config.trendUrls = {
                buy: response[1][config.templates.buy],
                rent: response[1][config.templates.rent]
            };
            return {
                data: parseAllCity(response[0].data || [], {config, req}),
                totalCount
            };
        });
    }
    return cityDetailsList;
};


cityService.getCityWisePrice = function(req, res) {

    let urlConfig = apiConfig.getCityPriceDetails();
    return apiService.get(urlConfig, { req, res }).then((response) => {
        return response.data;
    });
};

cityService.getCityListMap = function() {
    let cityList = masterDetailService.getMasterDetails('cityList');
    let cityMap = {};

    for (var i = 0; i < cityList.length; i++) {
        cityMap[cityList[i].id] = cityList[i].label;
    }

    return cityMap;
};

cityService.cityPriceList = function(req, res) {
    function _parseLeadPriceData(priceData, cityMap) {
        return _.forEach(priceData, (v) => {
            if (v.cityId === -1) {
                v.val = "Rest of India";
                v.key = "Rest of India";
            } else {
                v.val = cityMap[v.cityId];
            }
            v.key = v.cityId;
            return v;
        });
    }

    let price = this.getCityWisePrice(req, res);
    let cityMap = this.getCityListMap(req, res);

    return Promise.all([price, cityMap]).then((response) => {
        let finalData = _parseLeadPriceData(response[0], response[1]);
        return finalData;
    }).catch((e) => {
        logger.error(e);
        throw e;
    });
};

module.exports = cityService;
