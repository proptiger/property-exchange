'use strict';
var globalConfig = require('configs/globalConfig'),
    utils = require('services/utilService'),
    _=utils._,
    imageSize = globalConfig.imageSizes;

// function _parseGroupedImages(groupedImages, order) {
//     let result = [],
//         groupObj = {};

//     for (let i = 0, length = order.length; i < length; i++) {
//         let currentGroup = groupedImages[order[i].key];
//         if (currentGroup && currentGroup.length) {
//             result = result.concat(currentGroup);
//             groupObj[order[i].key] = currentGroup.length;
//         }
//     }
//     return {
//         parsedImages: result,
//         groupObj: groupObj
//     };

// }

function formatImages(imageArray) {
    let result = [];
    for (let i = 0, length = imageArray.length; i < length; i++) {
        if (imageArray[i]) {
            result.push({
                "src": imageArray[i].absolutePath,
                'thumb': appendImageSize(imageArray[i].absolutePath, thumbnail),
                'subHtml': imageArray[i].imageType?'<p>' + imageArray[i].imageType.displayName + '</p>':`<p>${imageArray[i].title}</p>`
            });
        }
    }
    return result;
}

function _prepareGalleryContainerData(galleryRawData,config,req) {
    let mainImage = config.mainImage,
        isMobile = utils.isMobileRequest(req),
        titleVideo = config.titleVideo,
        groupObj = config.groupObj,
        resultArray = config.resultArray,
        indexOffsetForEachImage = 0, // get incremented on addition of any new buckets like Video,Title Image
        __count = 0;
    if (_.isEmpty(galleryRawData)) {
        return {};
    }
    let bucketKeys = Object.keys(galleryRawData),
        bucketLength = bucketKeys.length,
        buckets = [],
        totalImageCount = 0,
        entityCount = 0,
        galleryContainerData = {},
        more = { image: {},count: 0,startIndex: 0};

    for (let i = 0; i < bucketLength; i++) {
        let bucketImages = galleryRawData[bucketKeys[i]];
        buckets.push(bucketKeys[i]);
        totalImageCount += bucketImages.length;
        entityCount = totalImageCount;
    }

    if(titleVideo) {
        let _groupObj = {Overview: 1};
        for(let key in groupObj) {
            _groupObj[key] = groupObj[key];
        }
        groupObj = _groupObj;
        galleryContainerData.titleVideo = titleVideo;
        resultArray = [titleVideo].concat(resultArray);
        galleryContainerData.titleVideo.startIndex = 0;
        indexOffsetForEachImage++;
        entityCount++;
    }
    galleryContainerData.titleImage = (mainImage && parseImages([mainImage],isMobile)[0]) || (galleryRawData[bucketKeys[0]] && galleryRawData[bucketKeys[0]][0]);
    var titleImagefoundIndex = -1;
    if(galleryContainerData.titleImage) {
        var _count = 0;

        for (let i = 0; i < bucketLength && titleImagefoundIndex < 0; i++) {
            let bucketImages = galleryRawData[bucketKeys[i]];
            /* if titleimage is found in any of the bucket ,move that image to last */
            for (let j = 0; j < bucketImages.length; j++) {
                let _image = bucketImages[j];
                if (_image.id === galleryContainerData.titleImage.id) {
                    titleImagefoundIndex = j;
                    break;
                }
            }
            if (!(titleImagefoundIndex === -1) && !titleVideo ) {   //jshint ignore:line
                let poppedImage = galleryRawData[bucketKeys[i]].splice(titleImagefoundIndex, 1)[0];
                galleryContainerData.titleImage = poppedImage;
                galleryRawData[bucketKeys[i]].push(poppedImage);
                galleryRawData[bucketKeys[i]].startIndex = _count + indexOffsetForEachImage;
                galleryContainerData.titleImage.startIndex = _count + titleImagefoundIndex + indexOffsetForEachImage;
                totalImageCount--;
                //special case : When bucket is 1 of length 1  and title image is from it .We go back to title image only case
                if(titleImagefoundIndex === 0 && i === 0 && bucketImages.length === 1 && !titleVideo){
                    delete galleryRawData[bucketKeys[i]];
                    bucketKeys.splice(i,1);
                    buckets.splice(i,1);
                    indexOffsetForEachImage++;
                    bucketLength--;
                    //titleImagefoundIndex = -1;
                    totalImageCount++;
                }
                break;
            }
            _count += bucketImages.length;
        }
    }
    // If title image doesn't lie in any of the bucket make a new bucket
    if(titleImagefoundIndex === -1 && galleryContainerData.titleImage ) {
        let _groupObj = {};
        if(groupObj.Overview) {
            _groupObj.Overview = 1;
            delete groupObj.Overview;
        }
        _groupObj['Title Image'] =  1;
        for(let key in groupObj) {
            _groupObj[key] = groupObj[key];
        }
        groupObj = _groupObj;
        if(titleVideo){
            resultArray.splice(1, 0, galleryContainerData.titleImage);
        } else {
            resultArray = [galleryContainerData.titleImage].concat(resultArray);
        }
        galleryContainerData.titleImage.startIndex = (titleVideo)? 1: 0;
        indexOffsetForEachImage++;
        entityCount++;
    }

    /* selection criteria for more image */
    if (bucketLength > 2) {
        let _nextImage = (galleryRawData[bucketKeys[3]] && galleryRawData[bucketKeys[3]][0]);
        if (_nextImage) {
            more.image = _nextImage;
            more.startIndex = galleryRawData[bucketKeys[2]].length + galleryRawData[bucketKeys[1]].length + galleryRawData[bucketKeys[0]].length + indexOffsetForEachImage;
        }
        if (_.isEmpty(more.image)) {
            let _count = 0;
            for (let j = 2; j >= 0; j--) {
                let _currentGalleryData = galleryRawData[bucketKeys[j]];
                if (!_currentGalleryData) {
                    continue;
                }
                if (_currentGalleryData[1]) {
                    more.image = _currentGalleryData[1];
                    more.startIndex = _count +  (_currentGalleryData.length - 1) + indexOffsetForEachImage;
                    more.startIndex = entityCount - more.startIndex;
                    break;
                }
                _count += _currentGalleryData.length;
            }
        }

        /* Special case : when there are three buckets and one image per bucket , in that case convert it into bucket 2 case */
        if(bucketLength === 3 && _.isEmpty(more.image) ){
            more.image = galleryRawData[bucketKeys[2]] && galleryRawData[bucketKeys[2]][0];
            more.startIndex = galleryRawData[bucketKeys[1]].length + galleryRawData[bucketKeys[0]].length + indexOffsetForEachImage;
            bucketLength--;
        }
        more.count = (totalImageCount - more.startIndex + 1) >= 0 ? (totalImageCount - more.startIndex +1) : 0;
    } else if (bucketLength == 2) {
        more.image = galleryRawData[bucketKeys[1]][0];
        more.startIndex = galleryRawData[bucketKeys[0]].length + indexOffsetForEachImage;
        more.count = (totalImageCount - more.startIndex + 1) >= 0 ? (totalImageCount - more.startIndex +1) : 0;
    } else if (bucketLength == 1) {
        let _nextImage = galleryRawData[bucketKeys[0]] && galleryRawData[bucketKeys[0]][1],
            _theOnlyImage = galleryRawData[bucketKeys[0]] && galleryRawData[bucketKeys[0]][0];

        /* Special case handling :
           when then there is one bucket containing two images in it and title image is also from that image
        */
        if(galleryRawData[bucketKeys[0]].length === 2 && titleImagefoundIndex > -1) {
            _nextImage = _theOnlyImage;
        }
        if (_nextImage ) {
            more.image = _nextImage;
            more.startIndex = 1 + indexOffsetForEachImage;
        } else {
            more.image = _theOnlyImage;
            more.startIndex = 0 + indexOffsetForEachImage;
        }
        more.count = (totalImageCount - more.startIndex + 1) >= 0 ? (totalImageCount - more.startIndex +1) : 0;
    }

   /* Mapping Index for first image of each bucket for new gallery container data with original gallery data */
   __count = 0;
   for (let i = 0; i < bucketLength; i++) {
        let bucketImages = galleryRawData[bucketKeys[i]];
        // if index already set , no need to set it again
        if(galleryRawData[bucketKeys[i]].startIndex) {
            __count += bucketImages.length;
            continue;
        }
        if( i === 0) {
            galleryRawData[bucketKeys[i]].startIndex = ( 0 + indexOffsetForEachImage);
            __count += bucketImages.length;
            continue;
        }

        galleryRawData[bucketKeys[i]].startIndex = (__count + indexOffsetForEachImage );
        __count += bucketImages.length;
    }

    galleryContainerData = _.extend(galleryContainerData,{
        images : galleryRawData,
        bucketCount : bucketLength,
        buckets : buckets,
        totalImageCount : totalImageCount,
        entityCount : entityCount,
        more : more
    });

    return {
        galleryContainerData,
        groupObj,
        resultArray
    };
}


function groupListingImages(imageArray, config, req){
    let listingId = config.listingId,
        propertyId = config.propertyId,
        projectId = config.projectId,
        //mainImage = config.mainImage,
        resultArray=[],
        groupObj={},
        galleryRawData = {},
        images = {},
        floorPlanKeys = globalConfig.floorPlanKeys,
        floorPlanObj={},
        isMobile = utils.isMobileRequest(req),
        propertyImages,
        imageBucketMap=imageArray.buckets || [],//globalConfig.listingImageBucketMap;
        _galleryContainerData;

    if(listingId && imageArray[listingId.toString()]){
        images = imageArray[listingId.toString()];
    }

    if( projectId && imageArray[projectId.toString()]){
        images = _.assign(imageArray[projectId.toString()], images);
    }

    if(propertyId && imageArray[propertyId.toString()]){
        propertyImages = imageArray[propertyId.toString()];
    }

    if(images){
        images = _.assign(propertyImages, images);
        for(let i=0, length=imageBucketMap.length;i<length;i++){
            let currMap = imageBucketMap[i],
                currKey = currMap.key,
                currBucket = currMap.buckets,
                currBucketImages = [],
                _containerCurrBucketImages = [];
            for(let key of currBucket){
                let currImages = images[key];
                if(currImages && currImages.length){
                    currBucketImages = currBucketImages.concat(parseImages(currImages, isMobile));
                    _containerCurrBucketImages = _containerCurrBucketImages.concat(parseImages(currImages, isMobile,true));
                }
            }
            if (currBucketImages && currBucketImages.length){
                resultArray = resultArray.concat(currBucketImages);
                groupObj[currKey] = currBucketImages.length;
                galleryRawData[currKey] = _containerCurrBucketImages;
            }
        }
        for(let i=0, length=floorPlanKeys.length;i<length;i++) {
            let currKey = floorPlanKeys[i];
            if(images[currKey]){
                let currImages = images[currKey];
                    //currBucketImages = parseImages(currImages, isMobile);
                for(let j=0,length=currImages.length;j<length;j++){
                    currImages[j].url = appendImageSize(currImages[j].url, 'medium');
                }
                floorPlanObj[currKey] = currImages;
            }
        }
    }
    config.groupObj    = groupObj;
    config.resultArray = resultArray;
    _galleryContainerData = _prepareGalleryContainerData(galleryRawData,config,req);
    if(floorPlanObj){
        floorPlanObj.floorPlan = addGalleryImageIndex(floorPlanObj.floorPlan, _galleryContainerData.resultArray);
    }
    return {
        galleryData: _galleryContainerData.resultArray,
        galleryContainerData: _galleryContainerData.galleryContainerData,
        galleryGroup: _galleryContainerData.groupObj,
        floorPlanObj: floorPlanObj
    };
}

function addGalleryImageIndex(targetImageArray = [], galleryData = []){
    let resultArray = [];
    _.forEach(targetImageArray, imageData=>{
        let index = _.findIndex(galleryData, {id: imageData.id});
        imageData.galleryImageIndex = index;
        resultArray.push(imageData);
    });
    return resultArray;
}

function groupProjectImages(imageArray, options, req){
    let resultArray=[],
        groupObj={},
        constructionObj,
        projectId = options.projectId,
        //mainImage = options.mainImage,
        localityId = options.localityId,
        propertyIds = options.propertyIds,
        imageBucketMap = imageArray.buckets || [],//globalConfig.projectImageBucketMap,
        isMobile = utils.isMobileRequest(req),
        floorPlanImages = {},
        _galleryContainerData,
        galleryRawData = {};

    let projectImages = projectId && imageArray[projectId.toString()],
        localityImages = localityId && imageArray[localityId.toString()];
    if(projectImages){
        let constructionImages = projectImages['constructionStatus'];

        if(constructionImages && constructionImages.length) {
            constructionObj = _parseConstructionImages(constructionImages, isMobile);
        }

        for(let i=0, length=imageBucketMap.length;i<length;i++){
            let currMap = imageBucketMap[i],
                currKey = currMap.key,
                currBucket = currMap.buckets,
                currBucketImages = [],
                _containerCurrBucketImages = [];
            for(let key of currBucket){
                let currImages = projectImages[key];
                if(currImages && currImages.length){
                    currBucketImages = currBucketImages.concat(parseImages(currImages, isMobile));
                    _containerCurrBucketImages = _containerCurrBucketImages.concat(parseImages(currImages, isMobile, true));
                }
            }
            if (currBucketImages && currBucketImages.length){
                resultArray = resultArray.concat(currBucketImages);
                groupObj[currKey] = currBucketImages.length;
                galleryRawData[currKey] = _containerCurrBucketImages;
            }
        }
    }
    if(localityImages){
        let localityKeys = Object.keys(localityImages);
        for(let i=0, length = localityKeys.length; i<length; i++){
            let currKey = localityKeys[i],
              currCategoryImages = localityImages[currKey],
                //currBucketImages = parseImages(currCategoryImages, isMobile),
                _containerCurrBucketImages = parseImages(currCategoryImages, isMobile, true);
            resultArray = resultArray.concat(parseImages(currCategoryImages, isMobile));
            galleryRawData[currKey] = _containerCurrBucketImages;
            groupObj['Locality Images'] = (groupObj['Locality Images'] || 0) + currCategoryImages.length;
        }
    }

      if(propertyIds.length) {
         for(let index in propertyIds){
             let propertyImages = imageArray[propertyIds[index].toString()];
             if(propertyImages){
                 let propertyKeys = Object.keys(propertyImages);
                 for(let i = 0, length = propertyKeys.length; i<length; i++){
                     let currKey = propertyKeys[i],
                         currPropertyImages = propertyImages[currKey],
                          _parsedImageObject = parseImages(currPropertyImages, isMobile),
                          _containerParsedImageObject = parseImages(currPropertyImages, isMobile, true) ;
                     resultArray = resultArray.concat(_parsedImageObject);
                     floorPlanImages[propertyIds[index]] = floorPlanImages[propertyIds[index]]  || {};
                     floorPlanImages[propertyIds[index]].data  = [_parsedImageObject[0]];// return 1 floor plan image per property as of now
                     groupObj['Floor Plan'] = (groupObj['Floor Plan'] || 0) +  currPropertyImages.length;
                     galleryRawData[currKey] = galleryRawData[currKey] || [];
                     if(_containerParsedImageObject && _containerParsedImageObject[0]) {
                        galleryRawData[currKey].push(_containerParsedImageObject[0]);
                     }
                 }
             }
         }
     }

    options.groupObj      = groupObj;
    options.resultArray   = resultArray;
    _galleryContainerData = _prepareGalleryContainerData(galleryRawData,options,req);
    return {
        resultArray: _galleryContainerData.resultArray,
        galleryContainerData: _galleryContainerData.galleryContainerData,
        groupObj: _galleryContainerData.groupObj,
        constructionObj,
        floorPlanImages
    };
}

function _parseConstructionImages(constructionArray){
    //reverse sorting on the basis of timestamp
    constructionArray.sort(function (a, b) {
      if (a.takenAt < b.takenAt) {
        return 1;
      }
      if (a.takenAt > b.takenAt) {
        return -1;
      }
      return 0;
    });

    let groupObj={},parsedArray=[], orderedGroup=[];
    for (let i = 0, length = constructionArray.length; i < length; i++) {
        let currImage = constructionArray[i],
            takenAt = utils.formatDate(currImage.takenAt,'SS YY');
        if ((takenAt) && (groupObj.hasOwnProperty(takenAt) || Object.keys(groupObj).length < 3)) {
            parsedArray = parsedArray.concat(parseImages([currImage], true));
            groupObj[takenAt] = (groupObj[takenAt] || 0)+1;
        }
    }
    let dateArray = Object.keys(groupObj);
    for(let i=0,length = dateArray.length;i<length;i++){
        let currDate = dateArray[i],
            prevBucketCount = (dateArray[i - 1] && groupObj[dateArray[i - 1]]) + (dateArray[i - 2] && groupObj[dateArray[i - 2]] || 0) || 0;
        orderedGroup.push({
            val:currDate,
            count: prevBucketCount+1
        });
    }
    return {
        constructionImages: parsedArray,
        constructionGroup: groupObj,
        orderedGroup:orderedGroup
    };
}

function parseImages (imageArray, isMobile, isContainerImage = false){
    let resultArray = [],
        imageConfig;
    if(isMobile ) {
        imageConfig = isContainerImage ? "mobile" : "medium";
    } else {
        imageConfig = isContainerImage ? 'medium': 'galleryLarge';
    }
    for (let i = 0, length = imageArray.length; i < length; i++) {
        let currImage = imageArray[i],
            title = gallerySupportedTitle(currImage.title);
        resultArray[i] = {
            src: appendImageSize(currImage.url || currImage.absolutePath,imageConfig),
            thumb: appendImageSize(currImage.url || currImage.absolutePath,'tile'),
            subHtml: title,
            alt:currImage.alt|| currImage.altText || '',
            title: title,
            category: currImage.category || '',
            id: currImage.id,
            width: currImage.width,
            height: currImage.height
        };
    }
    return resultArray;
}

function gallerySupportedTitle(title = ''){
    _.forEach(globalConfig.supportedImagesType,(v)=>{
        if(title.indexOf('.'+v) !== -1){
            title = '';
            return title;
        }
    });
    return title;
}

function appendImageSize(imageURL, size) {
    var dimension, newImageURL;
    if (imageURL && size && imageSize[size]) {
        dimension = imageSize[size];
        newImageURL = imageURL + '?width=' + dimension.width + '&height=' + dimension.height;
        return newImageURL;
    }
    return imageURL;
}

function getImageSizeString(size) {
    var dimension, imageSizeString = '';
    if (size && imageSize[size]) {
        dimension = imageSize[size];
        imageSizeString = `?width=${dimension.width}&height=${dimension.height}`;
    }
    return imageSizeString;
}



module.exports = {
    formatImages,
    groupListingImages,
    groupProjectImages,
    parseImages,
    appendImageSize,
    getImageSizeString
};
