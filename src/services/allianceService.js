"use strict";
const moment = require('moment');
const apiService = require('services/apiService'),
    apiConfig = require('configs/apiConfig'),
    utilService = require('./utilService'),
    cityService = require('services/cityService'),
    _ = utilService._;
var allianceService = {},
    fields = 'id,expiryDate,couponType,category,title,bestOffer,services,service,description,companyOffers,companyId,company,logo,name,serviceId,companyCoupons,offer,termsAndConditions,preferenceOrder';

function allianceParser(alliance, locations) {
    alliance = alliance || {};
    let companyOffer = alliance.companyOffer,
        company = companyOffer && companyOffer.company,
        service = companyOffer && companyOffer.service,
        serviceCities = companyOffer && companyOffer.companyServiceCity,
        finalResponse = serviceParser(service) || {};

    finalResponse.couponId = alliance.id;
    finalResponse.allianceName = company && company.name;
    finalResponse.allianceDescription = company && company.description;
    finalResponse.allianceIcon = company && company.logo;
    finalResponse.allianceId = company && company.id;
    finalResponse.bestOffer = alliance.offer;
    if(locations){
        finalResponse.serviceCities = serviceCities.map(serviceCity=>{
            serviceCity.city = cityService.getCityNameById(serviceCity.cityId);
            return serviceCity.city;
        });
    }

    return finalResponse;
}

function serviceParser(service) {
    service = service || {};
    let category = service.serviceCategory,
        //buyerStage = service.buyerStage,
        finalResponse = categoryParser(category) || {};

    finalResponse.serviceName = service.service;
    finalResponse.serviceDescription = service.description;
    finalResponse.serviceTitle = service.title;
    finalResponse.serviceBestOffer = service.bestOffer;
    finalResponse.serviceId = service.id;

    return finalResponse;
}

function categoryParser(category) {
    category = category || {};
    let finalResponse = {};

    finalResponse.categoryName = category.category;
    finalResponse.categoryId = category.id;
    finalResponse.categoryTitle = category.title;
    finalResponse.categoryBestOffer = category.bestOffer;

    return finalResponse;
}


function changeDataElementsOrder(data=[]){
    var reOrderElementId = 9,
        placeAfterElementId = 1,
        newIndex,
        oldIndex,
        reOrderElement,
        i;
    for ( i=0;i<data.length;i++) {
        if(data[i] && data[i].id===reOrderElementId) {
            oldIndex=i;
            break;
        }
    }

    for (i=0;i<data.length;i++) {
        if(data[i] && data[i].id===placeAfterElementId) {
            newIndex=i+1;
            break;
        }
    }
    reOrderElement = data.splice(oldIndex,1);
    data.splice(newIndex,0,reOrderElement[0]);

    return data;
}

function parseAllCategoryData(data={}){
    data = data.results;
    let finalResponse = [];
    if(_.isArray(data)){
        data = changeDataElementsOrder(data);
        finalResponse = data.map(category=>{
            let categoryData = {},
                services = category.services;
            categoryData.id = category.id;
            categoryData.categoryName = category.category;
            if(_.isArray(services)){
                categoryData.services = services.map(service=>{
                    let serviceData = {},
                        companyOffers = service.companyOffers;
                    serviceData.serviceId = service.id;
                    serviceData.serviceName = service.service;
                    serviceData.serviceTitle = service.title;
                    if(_.isArray(companyOffers)){
                        serviceData.allianceCount = companyOffers.length;
                        serviceData.alliances = companyOffers.map(alliance=>{
                            let allianceData = {},
                                companyData = alliance.company,
                                couponData = _.isArray(alliance.companyCoupons) && alliance.companyCoupons[0];
                            allianceData.allianceId = companyData && companyData.id;
                            allianceData.allianceName = companyData && companyData.name;
                            allianceData.allianceIcon = companyData && companyData.logo;
                            allianceData.allianceDescription = companyData && companyData.description;
                            allianceData.allianceOffer = couponData && couponData.offer;
                            allianceData.allianceCouponId = couponData && couponData.id;
                            allianceData.allianceTermsAndConditions = couponData && couponData.termsAndConditions;
                            allianceData.allianceCouponExpiry = couponData && moment(couponData.expiryDate).format('Do MMM');
                            allianceData.allianceCouponType = couponData && couponData.couponType;
                            allianceData.allianceButton = allianceData.allianceCouponType === 'NoType' ? 'deal' : 'coupon';
                            return allianceData;
                        });
                    }
                    return serviceData;
                });
            }
            return categoryData;
        });
    return finalResponse;
    }
}

allianceService.getServiceCategories = function(queryParams) {
    let apiUrl = apiConfig.getServiceCategory(queryParams);
    return apiService.get(apiUrl).then((response) => {
        response = response && response.data || [];
        response = response.map(category => {
            return categoryParser(category);
        });
        return response;
    });
};

allianceService.getServicesCount = function(queryParams) {
    queryParams.fields = fields;
    let apiUrl = apiConfig.getServices(queryParams);
    return apiService.get(apiUrl).then((response) => {
        return response.totalCount;
    });
};

allianceService.getAllServices = function(queryParams){
    delete queryParams.cityId;
    queryParams.fields = fields;
    let apiUrl = apiConfig.getServices(queryParams);
    if(allianceService.allServices){
        let response = new Promise((resolve)=>{
            resolve(allianceService.allServices);
        });
        return response;
    } else {
        return apiService.get(apiUrl).then(response=>{
            allianceService.allServices = response.data;
            return response.data;
        });
    }
};

allianceService.getAlliances = function(queryParams) {
    let apiUrl = apiConfig.getAliances(queryParams);
    return apiService.get(apiUrl).then((response) => {
        response = response && response.data || [];
        response = response.map(alliance => {
            return allianceParser(alliance, queryParams.locations);
        });
        return response;
    });
};

allianceService.getStageCards = function(queryParams) {
    let apiUrl = apiConfig.getStageCards(queryParams);
    return apiService.get(apiUrl).then(response=>{
        response = response && response.data || [];
        response = response.map(service => {
            return serviceParser(service);
        });
        return response;
    });
};

//queryParams object contains cityId
allianceService.getAllCategoryData = function(queryParams){
    queryParams.fields = fields;
    let apiUrl = apiConfig.getAllAllianceCategories(queryParams);
    return apiService.get(apiUrl).then(response=>{
        return parseAllCategoryData(response.data);
    });
};

//queryParams object contains cityId, serviceId
allianceService.getAllianceData = function(queryParams){
    queryParams.fields = fields;
    let apiUrl = apiConfig.getAllAllianceCategories(queryParams);
    return apiService.get(apiUrl).then(response=>{
        return parseAllCategoryData(response.data);
    });
};

allianceService.getAllianceCities = function() {
    let apiUrl = apiConfig.getAllianceCities();
    return apiService.get(apiUrl).then((response) => {
        return response.data;
    });
};

module.exports = allianceService;
