'use strict';

const fs = require("fs"),
    cmd = require("node-cmd"),
    utils = require('public/scripts/common/utilFunctions'),
    Zip = require('adm-zip'),
    path = require('path'),
    urlParser = require('url'),
    _ = require('lodash'),
    sharedConfig = require('public/scripts/common/sharedConfig'),
    moment = require('moment'),
    cookie = require('cookie'),
    logger = require('services/loggerService');

/**
 * Defining util functions that are specific to node
 */
utils.getAllFilesFromFolder = function(dir) {
    var results = [];
    fs.readdirSync(dir).forEach(function(file) {
        file = dir + '/' + file;
        var stat = fs.statSync(file);
        if (stat && stat.isDirectory()) {
            results = results.concat(this.getAllFilesFromFolder(file));
        } else {
            results.push(file);
        }
    });
    return results;
};

utils.getFolders = function(dir) {
    let results = [];
    fs.readdirSync(path.resolve(__dirname, dir)).forEach(function(subdir) {
        subdir = path.resolve(__dirname, dir, subdir);
        let stat = fs.statSync(subdir);
        if (stat && stat.isDirectory()) {
            results.push(subdir + '/');
        }
    });
    return results;
};

utils.calculateSpeed = function(output) {
    let AdmZip = new Zip();
    AdmZip.addFile("test.txt", output);
    var zippedContent = AdmZip.toBuffer();
    var sizeOfHTML = (zippedContent.length - 1) / 1024;
    output = output.replace('{{__CONTENT__SIZE__}}', sizeOfHTML).replace('{{__RESPONSE__TIMESTAMP__}}', new Date().getTime());
    return output;
};

/**
 * update URL query string
 * @param  {String} uri   valid url, valid querystring parameter
 * @param  {String} key   querystring parameter
 * @param  {String} value querystring value
 * @return {String} updated uri
 */
utils.updateQueryStringParameter = function(uri, keys, values) {
    var re, separator;
    var i;
    var key, value;

    if (!(keys instanceof Array)) {
        keys = [keys];
        values = [values];
    }

    for (i = 0; i < keys.length; i++) {
        key = keys[i];
        value = values[i];
        re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
        separator = uri.indexOf('?') !== -1 ? "&" : "?";

        if (uri.match(re)) {
            uri = uri.replace(re, '$1' + key + "=" + value + '$2');
        } else {
            uri = uri + separator + key + "=" + value;
        }
    }

    return uri;
};

utils.generateSerpFilter = function(data) {
    let filterKeyMap = {
            beds: ["beds"],
            propertyType: ["propertyType"],
            area: ["minSize", "maxSize"],
            budget: ["minPrice", "maxPrice"],
            sellerId: ["sellerId"],
            projectId: ["projectId"],
            listingType: ["listingType"],
            projectName: ["projectName"],
            builderName: ["builderName"]
        },
        filter = {},
        key,
        k,
        urlObj,
        getURL = sharedConfig.getURL;

    for (key in filterKeyMap) {
        filter[key] = [];
        let mappedKeys = filterKeyMap[key];
        for (k = 0; k < mappedKeys.length; k++) {
            if (data[mappedKeys[k]]) {
                filter[key].push(data[mappedKeys[k]]);
            }
        }
        let length = filter[key].length;
        if (length) {
            // if (length === 2 && filter[key][0] === filter[key][1]) {
            //     filter[key].splice(1, 1);
            // }
            filter[key] = filter[key].join(",");
        } else {
            delete filter[key];
        }
    }

    urlObj = {
        url: ""
    };
    return Object.keys(filter).length ? getURL(urlObj, filter).url : null;
};

/**
 * remove the query search parameter from the url
 * @param {String} uri valid url, valid querystring parameter
 * @param {String} keyToRemove key to be removed
 * @return {String} filtered uri
 */
utils.removeQueryStringParameter = function(uri, keyToRemove) {
    let tempUri = uri.split('?');
    var searchIndex = tempUri.length > 1 ? 1 : 0;

    if (tempUri.length > 1) {
        uri = [tempUri[0], tempUri.slice(1).join('?')];
    } else {
        uri = tempUri;
    }

    uri[searchIndex] = uri[searchIndex].split('&').filter(function(pair) {
        pair = pair.split('=');
        if (pair[0] === keyToRemove) {
            return false;
        }

        return true;
    }).join('&');

    if (uri[searchIndex]) {
        return uri.join('?');
    } else {
        return uri.join('');
    }
};

utils.getError = function(status, message){
    message = message || "provide relevant parameter";
    let error = new Error(message);
    error.status = status || 500;
    return error;
};

utils.setCacheHeader = function(res){
    if(!res){
        return;
    }
    let cacheControl = {};
    let regex = new RegExp("\/nc\/");
    if (res.statusCode >= 400 || (res.req && res.req.noCache) || (res.req && res.req.bot) || (regex.test(res.req.url))) {
        cacheControl["Cache-Control"] = "no-cache, max-age=0";
    } else if (res.req.cacheControl){
        cacheControl["Cache-Control"] = "public, max-age="+res.req.cacheControl;
    } else if(res.req.isMarko){
        cacheControl["Cache-Control"] = "s-maxage=7200, max-age=0";
    } else {
        cacheControl["Cache-Control"] = "public, max-age=7200";
    }
    res.header(cacheControl);
};

utils.sendMail = function(subject="", body="", to){   //jshint ignore:line
    if(!to){
        to = process.env.LEAD_FAIL_EMAIL;
    }
    let command = "echo '"+body+"' | sudo mail -s '"+subject+"' "+to;
    console.log('Sending mail .....',command);
    cmd.run(command);
};

utils.getMobileOperatingSystem = function(userAgent) {
    //var userAgent = navigator.userAgent || navigator.vendor || window.opera;
    if (userAgent.match(/iPad/i) || userAgent.match(/iPhone/i) || userAgent.match(/iPod/i)) {
        return 'iOS';
    } else if (userAgent.match(/Android/i)) {
        return 'Android';
    } else if (userAgent.match(/IEMobile/i)) {
        return 'IEMobile';
    } else if (userAgent.match(/BlackBerry/i)) {
        return 'BlackBerry';
    } else {
        return 'other';
    }
};

//for serializing the query parameter of input object having key value pair
utils.serialize = function(obj) {
    var str = [];
    for (var p in obj){
        if (obj.hasOwnProperty(p)) {
            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
        }
    }
    return str.join("&");
};

//append seoPrevUrl and seoNextUrl to the object
utils.createSeoPrevNext = function(currentPage, totalPages, currBaseUrl, obj) {
    if (obj && currentPage && totalPages && currentPage <= totalPages) {
        if (currentPage == 2) {
            obj.seoPrevUrl = currBaseUrl;
        } else if (currentPage > 2) {
            obj.seoPrevUrl = currBaseUrl + '?page=' + (currentPage - 1);
        }
        if ((currentPage + 1) <= totalPages) {
            obj.seoNextUrl = currBaseUrl + '?page=' + (currentPage + 1);
        }
    }
    return obj;
};
var makeAbsoluteUrl = utils.prefixToUrl;

utils.prefixToUrl = function(url, prefix){
    let domain = process.env["WEBSITE_URL"];
    if(url && url.indexOf(domain) == -1){
        url = makeAbsoluteUrl(url, prefix);
    }
    if(domain && url){
        url = urlParser.resolve(domain, url);
    }
    return url;
};

utils.getRecency = function(timestamp, recencies) {
    const DEFAULT_RECENCY = "2 months ago";
    var date = moment(timestamp);
    if(date && date.isValid() && recencies && recencies.length > 0) {
        for( let i = 0 ; i < recencies.length ; i ++) {
            if(date.isAfter(moment().subtract(recencies[i].typeVal, recencies[i].type))) {
                return recencies[i].displayText;
            }    
        }
    }
    //By default, return the last recency or default one
    return recencies[recencies.length-1] && recencies[recencies.length-1].displayText || DEFAULT_RECENCY;
}

utils.getParamFromCookie = function(cookies, cookieName, key) {
    try {
        var cookieString = cookies[cookieName]
        if (!cookieString) {
          return false
        }
        var cookieObject = cookie.parse(cookieString)
        return cookieObject[key]
    } catch (error) {
        error.message = 'getting cookie failed for ' + cookieName + ' &key= ' + key
        logger.error(error);
        return null
    }
}
utils.processBHK = function (bhkArray){
    if (bhkArray.length) {
        let index = bhkArray.indexOf("3plus");
        if (index >= 0) {
            bhkArray.splice(index, 1);
            Array.prototype.push.apply(bhkArray, ["4", "5", "6", "7", "8", "9", "10"]);
        }
        bhkArray = bhkArray.filter(n => n);
    }
    return bhkArray.join(",");
}
utils.getBuyUnitTypeId = function (unitTypeArray){
    let propertyTypeIds = [];
    unitTypeArray && unitTypeArray.forEach(elem => {
        let id = sharedConfig.unitTypeToIdMapping[elem];
        if (id) {
            propertyTypeIds.push(id);
        }
    });
    return propertyTypeIds.join(",");
}

utils._ = _;
module.exports = utils;
