"use strict";


  const   utils = require('./utilService'),
    localityService = require('./localityService'),
    cityService = require('./cityService');


function getBuyRentSuggestion({ cityId, localityId, affordableRent, bhk }, { req }) {
    //var selector = {},
       var promise;
    if (cityId) {
        promise = cityService.getCityOverview(cityId, req, { needRaw: true });
    } else if (localityId) {
        promise = localityService.getLocalityOverview(localityId, req);
    } else {
        promise = new Promise((resolve, reject) => {
            reject("provide relevant parameters");
        });
    }

    return promise.then((response) => {
        response = response || {};

        let data = localityService.parseLocalityListingAgg(response.listingAggregations, { cityId, localityId });
        // return data;
        return makeBuyRentDecision(data, { affordableRent, bhk, req });
    }, (error) => {
        throw error;
    });

}

function _calculateEmi(loanAmount, config) {
    return (loanAmount * config.rate) * (Math.pow(config.rate + 1, config.month) / Math.pow(config.rate + 1, config.month - 1));
}

function makeBuyRentDecision(data = {}, { affordableRent, bhk, req }) {  //jshint ignore:line
    let result = {
    };
    let numberFormat = req.locals.numberFormat;
    if (!affordableRent) {
        return result;
    }
    let price = {
        'buy': {
            'avgPrice': 0
        },
        'rent': {
            'avgPrice': 0
        }
    };
    let emi = {
        'buy': {},
        'rent': {}
    };
    let count = {};
    let calculatedData = {};
    let config = {
        month: 240,
        rate: 0.00875,
    };
    if (bhk) {
        let buyAgg = data.parsedAggrigations && data.parsedAggrigations.buy || {};
        let rentAgg = data.parsedAggrigations && data.parsedAggrigations.rent || {};
        let availableBhkCount = {
            rent: 0,
            buy: 0
        };
        utils._.forEach(bhk, (v) => {
            buyAgg[v] = buyAgg[v] || {};
            rentAgg[v] = rentAgg[v] || {};
            price.buy.minPrice = utils.findMin(price.buy.minPrice, buyAgg[v].minPriceUnformatted);
            price.buy.maxPrice = utils.findMax(price.buy.maxPrice, buyAgg[v].maxPriceUnformatted);
            if(buyAgg[v].weigthedAvgPrice){
                price.buy.avgPrice += buyAgg[v].weigthedAvgPrice;
                availableBhkCount.buy += buyAgg[v].count;
            }

            price.rent.minPrice = utils.findMin(price.rent.minPrice, rentAgg[v].minPriceUnformatted);
            price.rent.maxPrice = utils.findMax(price.rent.maxPrice, rentAgg[v].maxPriceUnformatted);
            if(rentAgg[v].weigthedAvgPrice){
                price.rent.avgPrice += rentAgg[v].weigthedAvgPrice;
                availableBhkCount.rent += rentAgg[v].count;
            }
        });
        price.buy.avgPrice = price.buy.avgPrice / availableBhkCount.buy;
        price.rent.avgPrice = price.rent.avgPrice / availableBhkCount.rent;
    } else {
        price.buy.minPrice = data.minPrice_Buy;
        price.buy.maxPrice = data.maxPrice_Buy;
        price.buy.avgPrice = data.avgPrice_Buy_Unformatted;

        price.rent.minPrice = data.minPrice_Rent;
        price.rent.maxPrice = data.maxPrice_Rent;
        price.rent.avgPrice = data.avgPrice_Rent_Unformatted;
    }

    calculatedData.minLoan = 0.85 * price.buy.minPrice;
    calculatedData.avgLoan = 0.85 * price.buy.avgPrice;
    calculatedData.maxLoan = 0.85 * price.buy.maxPrice;

    calculatedData.minEmi = _calculateEmi(calculatedData.minLoan, config);
    calculatedData.avgEmi = _calculateEmi(calculatedData.avgLoan, config);
    calculatedData.maxEmi = _calculateEmi(calculatedData.maxLoan, config);
    calculatedData.affordableEmi = affordableRent * 5 * 0.45;

    price.buy.formattedMinPrice = utils.formatNumber(price.buy.minPrice, { seperator : numberFormat });
    price.buy.formattedMaxPrice = utils.formatNumber(price.buy.maxPrice, { seperator : numberFormat });
    price.buy.formattedAvgPrice = utils.formatNumber(price.buy.avgPrice, { seperator : numberFormat });

    price.rent.formattedMinPrice = utils.formatNumber(price.rent.minPrice, { seperator : numberFormat });
    price.rent.formattedMaxPrice = utils.formatNumber(price.rent.maxPrice, { seperator : numberFormat });
    price.rent.formattedAvgPrice = utils.formatNumber(price.rent.avgPrice, { seperator : numberFormat });


    count.buy = data.listingCount_Buy;
    count.rent = data.listingCount_Rent;

    emi.buy.minEmi = calculatedData.minEmi;
    emi.buy.avgEmi = calculatedData.avgEmi;
    emi.buy.maxEmi = calculatedData.maxEmi;

    emi.buy.affordableEmi = emi.rent.affordableEmi = calculatedData.affordableEmi;

    emi.buy.formattedMinEmi = utils.formatNumber(calculatedData.minEmi, { seperator : numberFormat });
    emi.buy.formattedAvgEmi = utils.formatNumber(calculatedData.avgEmi, { seperator : numberFormat });
    emi.buy.formattedMaxEmi = utils.formatNumber(calculatedData.maxEmi, { seperator : numberFormat });

    emi.rent.formattedAffordableEmi = emi.buy.formattedAffordableEmi = utils.formatNumber(calculatedData.affordableEmi, { seperator : numberFormat });

    if (calculatedData.affordableEmi >= calculatedData.avgEmi) {
        result.suggestion = 'buy';
    } else if(calculatedData.affordableEmi < calculatedData.avgEmi){
        result.suggestion = 'rent';
    }

    result.price = price;
    result.emi = emi;
    result.count = count;
    result.affordableEmi = calculatedData.affordableEmi;
    result.affordableRent = affordableRent;

    return result;
}


module.exports = {
    getBuyRentSuggestion
};
