"use strict";

const slash = require('express-slash'),
    path = require('path'),
    fs = require("fs"),
    find = require('find'),
    AdmZip = require('adm-zip'),
    logger = require('services/loggerService'),
    globalConfig = require('configs/globalConfig'),
    seoService = require('services/seoService'),
    urlDataService = require('services/urlDataService'),
    utilService = require('services/utilService'),
    _ = utilService._,
    routeConfig = require('configs/routeConfig'),
    staticRoutes = require('services/staticRoutes'),
    debuggerService = require('services/debuggerService'),
    apiProxyService = require('services/apiProxyService'),
    trackerCookiesService = require('services/trackerCookiesService'),
    serpSelectorService = require('services/serpSelectorService'),
    //buyerDashboardParser = require('modules/apis/apiParsers/buyerDashboardParser'),
    url = require('url');
var routeService = {},
    zip = new AdmZip(),
    appjsSize;
const template404 = require('services/templateLoader').loadTemplate(path.join(__dirname, '../modules/404'));
const template500 = require('services/templateLoader').loadTemplate(path.join(__dirname, '../modules/500'));
const datadog = require('datadog');

routeService.setup = function(app, router) {

    if (app.get('env') == 'development' || app.get('env') == 'localhost') {
        find.file(/app\.js/, path.join(__dirname, '../public/scripts'), function(files) {
            let filePath = files[0];
            fs.readFile(filePath, (err, data) => {
                zip.addFile("test.txt", data);
                appjsSize = zip.toBuffer().length;
            });
        });
    } else {
        find.file(/app.*\.js/, app.get('staticPath'), function(files) {
            let filePath = files[0];
            fs.readFile(filePath, (err, data) => {
                zip.addFile("test.txt", data);
                appjsSize = zip.toBuffer().length;
            });
        });
    }

    //setting proper headers for amp-api calls
    app.use((req, res, next)=>{
        if(!req.locals) req.locals = {};
        if(!req.locals.numberFormat) req.locals.numberFormat = req.cookies.numberFormat;
        if(req.path && (req.path.indexOf(globalConfig.ampPageUrl) > -1 || req.path.indexOf('amp/') > -1)){
           req.isAmp = true;
           require(`../amp/services/ampRequestService`).setup(req,res).then(()=>{
                next();
           },(err)=>{
                next(err);
           }).catch((err)=>{
                next(err);
           });
        }else{
            next();
        }
    });

    // register routes & decide based on seo api

    app.use(router);
    app.use(slash());
    app.use((req, res, next) => {
        res.setHeader('Vary', 'Accept-Encoding,X-Mobile');
        return next();
    });

    // setup reverse proxy for API URLs
    if (app.get('env') == 'development' || app.get('env') == 'localhost') {
        apiProxyService.setup(router);
    }

    // register apis urls
    require(`../modules/apis/controller`).setup(router);

    // set cookie
    app.use((req, res, next) => {
        if (!req.cookies.websiteversion) {
            res.cookie('websiteversion', 'new', { domain: '.makaan.com', httpOnly: true, path: '/', maxAge: 1000 * 60 * 60 * 24 * 30 * 365 });
        }
        next();
    });


    // Register redirects for map links
    app.use('/maps*',function(req,res){
        return res.redirect(301,req.originalUrl.replace(/^\/?maps/,''));
    });

    // Register redirects for outdated links
    app.use('/list-property',function(req,res){
        var urlHref = '/rent-sell-property-online';
        if (req.query) {
            let i = req.url.indexOf('?');
            let queryStr = i > -1 ? req.url.substr(i + 1) : '';
            if (queryStr) {
                urlHref += '?' + queryStr;
            }
        }
        return res.redirect(301, urlHref);
    });
    app.use('/seller-journey',function(req,res){
        var urlHref = '/rent-sell-property-online';
        if (req.query) {
            let i = req.url.indexOf('?');
            let queryStr = i > -1 ? req.url.substr(i + 1) : '';
            if (queryStr) {
                urlHref += '?' + queryStr;
            }
        }
        return res.redirect(301, urlHref);
    });

    app.use('/tracking-lead', function(req, res, next) {
        req.skipSeo = true;
        trackerCookiesService.setTrackingCookies(req, res).then(() => {
            require(`../modules/trackingLead/controller`).routeHandler(req, res, next);
        }, (error) => {
            logger.error('trackerCookies error callback for url /tracking-lead');
            next(error);
        }).catch((error) => {
            logger.error('trackerCookies catch callback for url /tracking-lead');
            next(error);
        });
    });

    // registering static routes
    let staticUrls = staticRoutes.getStaticUrls();
    if (staticUrls && staticUrls.length) {
        let staticUrlsCount = staticUrls.length,
            routeHandlerPath;
        for (let i = 0; i < staticUrlsCount; i++) {
            (function(index) { //jshint ignore:line
                app.use(staticUrls[index].url, function(req, res, next) {
                    let staticReq = staticUrls[index].req;
                    if (staticReq) {
                        for (let key in staticReq) {
                            req[key] = _.cloneDeep(staticReq[key]);
                        }
                        req.urlDetail = req.urlDetail || {};
                        req.urlDetail.url = staticUrls[index].url;
                    }
                    routeHandlerPath = `../modules/${staticUrls[index].moduleName}/controller`;
                    require(routeHandlerPath).routeHandler(req, res, next);
                });
            })(i);
        }
    }

    app.use('/my-journey/*', function(req, res, next) {
        req.urlDetail = { pageType: 'BUYER_DASHBOARD' };
        next();
    });

    app.use('/services/all', function(req, res, next) {
        req.urlDetail = { pageType: 'SERVICES' };
        next();
    });

    app.use('(?:/maps)?/similar-property/:listingId([1-9][0-9]{5,10})', function(req, res, next) {
        req.urlDetail = { pageType: 'SIMILAR_PROPERTY_URLS', listingId: req.params.listingId };
        next();
    });

    let residentialPropertyPath = utilService.listingsPropertyPathName();
    app.use(`(?:(/maps))?${residentialPropertyPath}`, function(req, res, next) {
        req.urlDetail = { pageType: 'LISTINGS_PROPERTY_URLS' };
        next();
    });

    let residentialProjectPath = utilService.projectPropertyPathName();
    app.use(`${residentialProjectPath}`, function(req, res, next) {
        req.urlDetail = { pageType: 'PROJECT_SERP_DYNAMIC_URLS' };
        next();
    });

    app.use('(?:(/maps))?/seller-property', function(req, res, next) {
        req.urlDetail = { pageType: 'SELLER_PROPERTY_URLS' };
        next();
    });

    app.use(function(req, res, next) {
        if (req.url && req.path && req.url.length > 1) {
            req.url = req.url.replace(/([^:]\/)\/+/g, "$1");
            if (req.path == "/") {
                // skipping further logic for home page
            } else if (req.url.indexOf("/?") > -1) {
                req.customRedirectUrl = req.url.replace("/?", "?");
                res.status(301);
            } else if (req.url.slice(-1) === "/") {
                req.customRedirectUrl = req.url.slice(0, -1);
                res.status(301);
            }
        }
        next();
    });

    app.use(function(req, res, next) {
        // handles routes using seo api
        if (res.statusCode != "301") {
            _routeHandler(req, res, next);
        } else {
            next();
        }
    });

    //debug mode error handler
    app.use(function(err, req, res, next) {
        if (req.query && req.query.debug) {
            let finalObj = {
                error: err,
                primaryApis: req.apiMap,
                apiTime: `${Date.now() - req.apiTime} ms`
            };
            res.send(debuggerService.parse(finalObj));
        } else {
            next(err);
        }
    });

    //handle conditional 301 based on data in api
    app.use(function(req, res, next) {

        if (res.statusCode === 301) {
            res.redirect(301, req.customRedirectUrl || req.path);
        } else {
            next();
        }
    });

    // catch 404 and forward to error handler
    app.use(function(req, res, next) {
        let err = new Error('This wild page has escaped from our servers.');
        err.status = 404;
        next(err);
    });

    app.use(function(err, req, res, next) {
        _errorHandler(err, req, res, next);
    });
};

// @assigns null to empty string values in the object
function _parseObj(Obj) {
    for (let key in Obj) {
        if (Obj.hasOwnProperty(key)) {
            Obj[key] = (typeof Obj[key] === "string" && Obj[key] === '') ? undefined : Obj[key];
        }
    }
    return Obj;
}
function _setData(groupType,objectTypeId){
    var result={};
    result['isRent']=groupType && groupType.toLowerCase()=='rent'? true : false;
    if(objectTypeId == seoService.mapTypeIdToPageType['home']){
        result['headerPageType'] = 'home';
    } else if(objectTypeId == seoService.mapTypeIdToPageType['broker']){
        result['headerPageType'] = 'broker';
    } else if(objectTypeId == seoService.mapTypeIdToPageType['builder']){
        result['headerPageType'] = 'builder';
    } else if(objectTypeId ==seoService.mapTypeIdToPageType['project']){
        result['headerPageType'] = 'project';
        if(groupType=='Overview'){
           result['headerPageType'] = 'project_overview';
           result['isRent']=undefined;
        }
    } else if(objectTypeId ==seoService.mapTypeIdToPageType['city']){
        result['headerPageType']= 'city';
        if(groupType=='Overview'){
           result['headerPageType'] = 'city_overview';
           result['isRent']=undefined;
        }
    } else if(objectTypeId ==seoService.mapTypeIdToPageType['nearby']){
        result['headerPageType'] = 'nearby';
    } else if(objectTypeId ==seoService.mapTypeIdToPageType['suburb']){
        result['headerPageType'] = 'suburb';
    } else if(objectTypeId ==seoService.mapTypeIdToPageType['state']){
        result['headerPageType'] = 'state';
        result['isRent']=undefined;
    } else{
        result['headerPageType'] = 'locality';
        if(groupType=='Overview'){
           result['headerPageType']= 'locality_overview';
           result['isRent']=undefined;
        }
    }
    return result;
}

function _decideRouteHandler(req, res, next, response, extraParam = {}) {
    let found, data = response.data,
        urlDetail, urlData,
        mapUrl = extraParam && extraParam.mapUrl,
        isFakeSeo = extraParam && extraParam.isFakeSeo,
        isSEMLightPage = req.isSEMLightPage,
        isAmp = req.isAmp;
    if (data && data.urlStatus) {

        let urlStatus = data.urlStatus,
            httpStatus = urlStatus.httpStatus;

            if(urlStatus.urlDetail){
                let result = _setData(urlStatus.urlDetail.urlGroupType,urlStatus.urlDetail.objectTypeId);
                req["header-seo-pageType"]=result ? result['headerPageType'] : '';
                req["header-seo-isRent"]=result ? result['isRent']: '';
                if(!urlStatus.urlDetail.cachePage) {
                    req.noCache = true;
                }
            }

            req.footer = data.footer || {};
            req.meta    = data.meta || {};
            req["navigation"] = data["navigation"] || null;
            req["bread-crum"] = data["bread-crum"] || null;
            req['header-data'] = data['header'] || null;
            req.footerDescription = data['footer-content'] || null;
            req["qna-data"] = data['qna'] || null;
            req.meta.metaTags = req.meta.metaTags || {};
            
        switch(httpStatus){
            case 200: // assign handler for url
                logger.info('valid url');
                let pageType = urlStatus.masterUrlType || urlStatus.pageType;
                pageType = pageType.toUpperCase();
                urlDetail = urlStatus.urlDetail;
                data.urlDetail = data.urlDetail || {};
                urlDetail.listingFilter = data.urlDetail.listingFilter || {};
                urlDetail.selector = data.urlDetail.selector;
                let isSource_google=false,isMedium_cpc=false;

                if (mapUrl) {
                    pageType = pageType + '_MAPS';
                } else if (urlDetail.templateId && urlDetail.templateId.indexOf('OTHER_POPULAR_SEARCHES') > -1) {
                    pageType = 'ALL_LINKS';
                } else if (urlDetail.templateId && urlDetail.templateId.indexOf('ALL_LOCALITIES') > -1 && ['CITY_URLS', 'STATIC_URLS'].indexOf(pageType) > -1) {
                    pageType = 'ALL_LOCALITIES'; //pageType + '_ALL_LOCALITIES_OVERVIEW';
                } else if (urlDetail.templateId && urlDetail.templateId.indexOf('CITY_ALL_OVERVIEW') > -1 && ['STATIC_URLS'].indexOf(pageType) > -1) {
                    pageType = pageType + '_CITY_ALL_OVERVIEW';
                } else if (urlDetail.templateId && urlDetail.templateId.indexOf('ALL_STATE') > -1 && ['STATIC_URLS'].indexOf(pageType) > -1) {
                    pageType = pageType + '_STATE_ALL_OVERVIEW';
                } else if (urlDetail.templateId && ['MAKAAN_ALL_BUILDERS', 'MAKAAN_CITY_ALL_BUILDERS'].indexOf(urlDetail.templateId) > -1) {
                    pageType = 'ALL_BUILDERS';
                } else if (urlDetail.templateId && ['MAKAAN_TOP_BUILDERS', 'MAKAAN_CITY_TOP_BUILDERS'].indexOf(urlDetail.templateId) > -1) {
                    pageType = 'TOP_BUILDERS';
                } else if (urlDetail.templateId && ['MAKAAN_STATE_REAL_ESTATE_AGENTS','MAKAAN_ALL_BROKERS', 'MAKAAN_CITY_ALL_BROKERS', 'MAKAAN_LOCALITY_ALL_BROKERS', 'MAKAAN_SUBURB_ALL_BROKERS'].indexOf(urlDetail.templateId) > -1) {
                    pageType = 'ALL_BROKERS';
                } else if (urlDetail.templateId && ['MAKAAN_TOP_BROKERS', 'MAKAAN_CITY_TOP_BROKERS', 'MAKAAN_LOCALITY_TOP_BROKERS', 'MAKAAN_SUBURB_TOP_BROKERS', 'MAKAAN_CITY_TOP_BROKERS_BUY', 'MAKAAN_CITY_TOP_BROKERS_RENT', 'MAKAAN_CITY_ALL_BROKERS_BUY', 'MAKAAN_CITY_ALL_BROKERS_RENT', 'MAKAAN_LOCALITY_TOP_BROKERS_BUY', 'MAKAAN_LOCALITY_TOP_BROKERS_RENT', 'MAKAAN_LOCALITY_ALL_BROKERS_BUY', 'MAKAAN_LOCALITY_ALL_BROKERS_RENT', 'MAKAAN_SUBURB_TOP_BROKERS_BUY', 'MAKAAN_SUBURB_TOP_BROKERS_RENT', 'MAKAAN_SUBURB_ALL_BROKERS_BUY', 'MAKAAN_SUBURB_ALL_BROKERS_RENT'].indexOf(urlDetail.templateId) > -1) {
                    pageType = 'TOP_BROKERS';
                } else if (urlDetail.templateId && urlDetail.templateId.indexOf('OVERVIEW') > -1 && ['CITY_URLS', 'LOCALITY_URLS', 'SUBURB_URLS', 'PROJECT_URLS', 'PROJECT_URLS_COMMERCIAL'].indexOf(pageType) > -1) {
                    pageType = pageType + '_OVERVIEW';
                } else if (urlDetail.templateId && urlDetail.templateId.indexOf('PRICE_TREND') > -1 && ['MAKAAN_CITY_PRICE_TREND_BUY', 'MAKAAN_CITY_PRICE_TREND_RENT', 'MAKAAN_SUBURB_PRICE_TREND_BUY', 'MAKAAN_SUBURB_PRICE_TREND_RENT', 'MAKAAN_LOCALITY_PRICE_TREND_BUY', 'MAKAAN_LOCALITY_PRICE_TREND_RENT', 'MAKAAN_INDIA_PRICE_TREND'].indexOf(urlDetail.templateId) > -1) {
                    pageType = 'PRICE_TREND';
                } else if (urlDetail.templateId && urlDetail.templateId.indexOf('PROJECTS_SERP') > -1) {
                    pageType = 'PROJECT_SERP';
                }
                urlData = urlDataService.getUrlData(pageType, urlDetail, req, mapUrl);
                urlData.isMap = mapUrl;
                urlData.hideFooter = mapUrl ? true : false;
                urlData.h1Title = data.meta && data.meta.h1;
                urlData.h2Title = data.meta && data.meta.h2;
                urlData.h3Title = data.meta && data.meta.h3;
                urlData.h4Title = data.meta && data.meta.h4;
                urlData.urlGroupType = data.urlDetail.urlGroupType || '';
                req.urlDetail = urlData;
                _parseObj(req.urlDetail); // to assign null to empty string values

                // redirecting only paid google traffic to locality serp else normal flow
                if(req && req.query && req.query.utm_source && req.query.utm_medium){
                    isSource_google = (Object.prototype.toString.call(req.query.utm_source) === '[object Array]' && req.query.utm_source.some(e => e.toLowerCase() == 'google')) || req.query.utm_source.toString().toLowerCase() == 'google';
                    isMedium_cpc = (Object.prototype.toString.call(req.query.utm_medium) === '[object Array]' && req.query.utm_medium.some(e => e.toLowerCase() == 'cpc')) || req.query.utm_medium.toString().toLowerCase() == 'cpc';
                }

                if (urlData.pageType.toLowerCase() == 'property_urls' && isSource_google && isMedium_cpc) {
                    let keys=['listingType',
                                'cityName',
                                'cityId',
                                'localityName',
                                'localityId',
                                'gclid',
                                'utm_source',
                                'utm_medium',
                                'utm_term',
                                'utm_campaign',
                                'utm_adgroup',
                                'utm_content',
                                'Network',
                                'SiteTarget',
                                'ef_id'];
                   let values =[urlData.listingType||'',
                                urlData.cityName||'',
                                urlData.cityId||'',
                                urlData.localityName||'',
                                urlData.localityId||'',
                                req.query.gclid||'',
                                req.query.utm_source||'',
                                req.query.utm_medium||'',
                                req.query.utm_term||'',
                                req.query.utm_campaign||'',
                                req.query.utm_adgroup||'',
                                req.query.utm_content||'',
                                req.query.Network||'',
                                req.query.SiteTarget||'',
                                req.query.ef_id||'' ];

                    _redirectingToSerp(keys,values,res);
                    return;
                }

                let routeControllerObj = routeConfig.getRouteHandler(pageType, { isSEMLightPage, isAmp, globalConfig });
                if (globalConfig.servingAmpPages[routeControllerObj.moduleName]){
                    // 301 old amp url to new amp url
                    if (req.path && (req.path.indexOf('/amp/') > -1)){
                        return res.redirect(301, req.path.replace(new RegExp('/amp'), globalConfig.ampPageUrl));
                    }
                    // generate amp-url for amphtml tag
                    if (!isFakeSeo){
                        if(urlData.countryId === undefined || urlData.countryId === 1) {
                            urlData.ampUrl = url.resolve(urlData.websiteUrl, globalConfig.ampPageUrl + urlData.relativeUrl);
                        } else {
                            urlData.ampUrl = url.resolve(urlData.websiteUrl, urlData.relativeUrl.replace(/(.*?)(\?.*)?$/,'$1'+globalConfig.ampPageUrl+'$2'));
                        }
                    }
                } else if (req.isAmp) {
                    // 302 to non-amp for non-existing amp
                    return res.redirect(302, urlData.relativeUrl.replace(/(\/lite|\/amp)([\/?]?)/g, '$2'));
                }

                if (routeControllerObj && routeControllerObj.modulePath) {
                    found = true;
                    Promise.all(urlData.promises).then(function() {
                        req.data = urlData.promisesData;
                        delete urlData.promises;
                        delete urlData.promisesData;
                        req.pageData = Object.assign(urlData,routeControllerObj,{
                            isSEMLightPage: req.isSEMLightPage
                        })
                        req.pageData.sortBy = serpSelectorService.getClientSortParameter(req.pageData.sortBy && req.pageData.sortBy.field);
                        require(routeControllerObj.modulePath).routeHandler(req, res, next);
                    }, function(err) {
                        logger.error('inside routeService Promise error: ', err);
                        next(err);
                    }).catch(function(err) {
                        logger.error('inside routeService Promise catch : ', err.stack);
                        next(err);
                    });
                } else {
                    console.log("route controller object not found");
                    next();
                }
                break;
            case 301: // redirect if seo api provides 301 or 302 url
            case 302:
                let prunedQuery = {};
                utilService._.forEach(req.query, (v, k) => {
                    if (globalConfig.excludeQueryParameters.indexOf(k) == -1) {
                        prunedQuery[k] = v;
                    }
                });
                let searchQueryString = utilService.serialize(prunedQuery);
                searchQueryString = searchQueryString && '?' + searchQueryString;
                urlStatus.redirectUrl = urlStatus.redirectUrl ? '/' + urlStatus.redirectUrl : '/';
                if (isAmp && urlStatus.redirectUrl!=='/') {
                    if(req.url.match(/^\/(lite|amp)\//)) {
                        urlStatus.redirectUrl = globalConfig.ampPageUrl + urlStatus.redirectUrl;
                    } else {
                        urlStatus.redirectUrl = urlStatus.redirectUrl + globalConfig.ampPageUrl;
                    }
                }
                logger.info('redirecting to', urlStatus.redirectUrl + searchQueryString);
                res.redirect(httpStatus, urlStatus.redirectUrl + searchQueryString);
                return;
            default:
                logger.info('invalid url');
                let err = new Error('Error from SEO API');
                err.status = httpStatus;

                return next(err);
        }
    }

    if (!found) {
        req.pageData = {}; // set url data to share at client side
        next();
    }
}


function _redirectingToSerp(keys,values,res) {
    // setting url to locality SERP page with given listing_type , city and locality
    let url = utilService.updateQueryStringParameter('/listings',keys,values);
    logger.info('redirecting to', url);
    res.redirect(302, url); // temporary redirect
}

function _routeHandler(req, res, next) {
    let urlHref = req.path;
    if (req.query) {
        let i = req.url.indexOf('?');
        let queryStr = i > -1 ? req.url.substr(i + 1) : '';
        if (queryStr) {
            urlHref += '?' + queryStr;
        }
    }

    let mapUrl, fakeSeoResponse = {
        "data": {
            "urlStatus": {
                "httpStatus": 200,
                "urlType": null,
                "masterUrlType": null,
                "urlDetail": {
                    "templateId": ''
                }
            },
            "meta": {
                "metaTags": {
                    "viewport": 'minimum-scale=1.0, width=device-width, initial-scale=1.0, maximum-scale=5.0, user-scalable=no'
                }
            }

        }
    };

    let isFakeSeo;

    if (urlHref && urlHref.match(/^\/project/i)) {
        req.isSEMLightPage = true;
        urlHref = urlHref.replace(new RegExp('/project/v1/'), '/');
    }


    // replace rent/buy for seo as we just need to validate nearby placeId irrespective of category
    if (urlHref && (urlHref.indexOf('nearby/') > -1)) {
        urlHref = urlHref.replace(/(buy\/|rent\/|commercialBuy\/|commercialLease\/)/, '');
    }

    if (urlHref && urlHref.indexOf('maps/') > -1) {
        urlHref = urlHref.replace('/maps/', '/');
        mapUrl = true;
    }

    if(urlHref) {
        // extracting non-amp url for parsing
        // ['/lite|/amp' can be at start or at end(-for dubai)]
        (function(){
            var ampPaths = [globalConfig.ampLegacyUrl, globalConfig.ampPageUrl];
            var urlHrefPaths = url.parse(urlHref).pathname.match(/\/[^/]*/gi);
            var {0: ampStart, length: ampLength, [ampLength-1]: ampEnd} = urlHrefPaths;

            if(ampPaths.includes(ampStart)) {
                urlHref = urlHrefPaths.slice(1).join('');
            } else if(ampPaths.includes(ampEnd)) {
                urlHref = urlHrefPaths.slice(0, ampLength-1).join('');
            }
        })();
    }

    if (urlHref && (/\.(gif|jpg|jpeg|tiff|png|woff|woff2|ico|js|css)$/i).test(urlHref)) {
        next();
        return;
    } else if (req.skipSeo) {
        next();
        return;
    } else if (req.urlDetail && ['SIMILAR_PROPERTY_URLS', 'SIMILAR_PROPERTY_URLS_MAPS', 'BUYER_DASHBOARD', 'SELLER_HOME_PAGE', 'SERVICES'].indexOf(req.urlDetail.pageType) > -1) {
        fakeSeoResponse.data.urlStatus.masterUrlType = req.urlDetail.pageType;
        fakeSeoResponse.data.urlStatus.urlDetail.listingId = req.urlDetail.listingId;
        req.showBackButtonInFilter = true;
        isFakeSeo = true;
    } else if (req.urlDetail && ['LISTINGS_PROPERTY_URLS', 'SELLER_PROPERTY_URLS', 'PROJECT_SERP_DYNAMIC_URLS'].indexOf(req.urlDetail.pageType) > -1) {
        fakeSeoResponse.data.urlStatus.masterUrlType = req.urlDetail.pageType;
        isFakeSeo = true;
    }

    if (isFakeSeo) {
        _decideRouteHandler(req, res, next, fakeSeoResponse, { mapUrl, isFakeSeo });
        return;
    }

    let seoTags = seoService.getSeoTags(urlHref, req, res);

    function handleSEOFail(urlHref){

        let response = false;
        switch (urlHref) {
            case '/':
                response = utilService._.assign({}, fakeSeoResponse);
                response["data"]["urlStatus"]["urlType"] = "MAKAAN_HOME_PAGE";
                response["data"]["urlStatus"]["masterUrlType"] = "home_page_urls";
                break;
        }
        return response;
    }

    function doSEOHandling(seoPromise) {
        seoPromise.then((response) => {
            req.seoAPI = true;
            _decideRouteHandler(req, res, next, response, { mapUrl }); // response[0] represents seoTags response
        }, (error) => {
            logger.error('within error of getSeoTags inside routeService for url:  ', urlHref);
            let response = handleSEOFail(urlHref);
            if (response) {
                _decideRouteHandler(req, res, next, response, { mapUrl });
            } else {
                next(error);
            }
        }).catch((err) => {
            logger.error('within catch of getSeoTags inside routeService for url:  ', urlHref);
            next(err);
        });
    }

    doSEOHandling(seoTags);
}

function _errorHandler(err = {}, req, res) { //jshint ignore:line
    let backendStatusCode = err.body && err.body.statusCode || '5XX';
    let networkStatusCode = err.status || 500;
    let statusCode = networkStatusCode;

    if (backendStatusCode != '5XX' && (networkStatusCode < 400 || networkStatusCode > 499)) {
        statusCode = backendStatusCode.indexOf('X') == -1 ? backendStatusCode : 400;
    }
    statusCode = globalConfig.validStatusCodes.indexOf(statusCode) > -1 ? statusCode : globalConfig.nonCacheableStatusCode;
    res.status(statusCode);

    req.meta = {
        metaTags: {
            viewport: 'minimum-scale=1.0, width=device-width, initial-scale=1.0, maximum-scale=5.0, user-scalable=no'
        }
    };

    if (err.status === 404) {
        req.pageData = {
            pageType: '404'
        };
        datadog.tracker.event(`404:${req.url}`, {
            headers: req.headers
        }, {
            alert_type: 'warning',
            tags: ['404']
        });
        datadog.tracker.increment("404_PAGE");
        return template404.renderSync({}, res, null, null, req);
    } else {
        logger.error(err.message);
        logger.info(err.stack);
        if ((req.xhr && req.query.format === 'json') || req.query.showerror) {
            if (!err.body) {
                err.body = err.message;
            }
            return res.send(err);
        }

        req.pageData = {
            pageType: '500'
        };
        datadog.tracker.event(`500:${req.url}`, {
            apiMap: req.apiMap,
            headers: req.headers
        }, {
            alert_type: 'error',
            tags: ['500', req.url]
        });
        datadog.tracker.increment("500_PAGE");
        return template500.renderSync({}, res, null, null, req);
    }
}

module.exports = routeService;
