"use strict";

var t = require('public/scripts/common/trackingConfigService'),
    sharedConfig = require('public/scripts/common/sharedConfig');
var ampTrackingService = {};

ampTrackingService.getAnalyticsData = function(urlDetail, data) {

    data = data || {};
    data["cd14"] = data[t.URL_KEY] || urlDetail.url;
    data["cd1"] = data[t.PAGE_KEY] || t.PAGE_TYPE_MAP[urlDetail.pageType] || t.DEFAULT_PAGE_TYPE;
    data[t.PROJECT_NAME_KEY] = urlDetail.projectName;
    data["cm1"] = data[t.LOCALITY_ID_KEY] || urlDetail.localityId;
    data["cm2"] = data[t.CITY_ID_KEY] || urlDetail.cityId;
    data["cm5"] = data[t.LISTING_ID_KEY] || urlDetail.listingId;
    data["cm6"] = data[t.PROJECT_ID_KEY] || urlDetail.projectId;
    data["cm7"] = data[t.BUILDER_ID_KEY] || urlDetail.builderId;
    data["cm8"] = data[t.LANDMARK_ID_KEY] || urlDetail.placeId;
    data["cm12"] = data[t.SUBURB_ID_KEY] || urlDetail.suburbId;
    data[t.NON_INTERACTION_KEY] = data[t.NON_INTERACTION_KEY] || 0;
    data["title"] = sharedConfig.PAGE_TYPE_MAP[urlDetail.pageType];
    data["cd19"] = urlDetail.trackingKeyword;
    data["cd16"] = "amp";
    data["cd26"] = Array.isArray(urlDetail.cityId) ? [...new Set(urlDetail.cityId)] : urlDetail.cityId;

    if (urlDetail.listingType) {
        data["cd5"] = t.LISTING_CATEGORY_MAP[urlDetail.listingType.toUpperCase()];
    }
    if (urlDetail.listingCategory) {
        data["cd5"] = t.LISTING_CATEGORY_MAP[urlDetail.listingCategory.toUpperCase()];
    }

    //currently not coming
    data["cd6"] = data[t.PROJECT_STATUS_KEY] || urlDetail.projectStatus;
    data["cd8"] = data[t.UNIT_TYPE_KEY] || urlDetail.unitType;
    data["cd7"] = data[t.BUDGET_KEY] || urlDetail.budget;
    data["cd9"] = data[t.BHK_KEY] || urlDetail.bhk;
    data["cd10"] = data[t.SELLER_ID_KEY] || urlDetail.sellerId;
    data["cm10"] = data[t.SELLER_SCORE_KEY] || urlDetail.sellerScore;
    data["cm11"] = data[t.LISTING_SCORE_KEY] || urlDetail.listingScore;

    return data;
};


module.exports = ampTrackingService;
