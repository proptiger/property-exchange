"use strict";

const apiService = require('services/apiService'),
    logger = require('services/loggerService'),
    listingService = require('services/listingsService'),
    utilService = require('services/utilService'),
    getURL = require('public/scripts/common/sharedConfig').getURL,
    apiConfig = require('configs/apiConfig');

var taxonomyService = {};
var defaultConfig = {
    mockUrl: 'mock/taxonomyUrl',
    rows: 5,
    fields: ["id", "name", "establishedDate", "projectCount"],
    taxoLocalityMap: {
        // "Recent properties": {
        //     "api": apiConfig.listings().url,
        //     "selector": {},
        //     "redirectURL": {}
        // },
        "Luxury Properties": {
            "api": apiConfig.listings({version:1}).url,
            "selector": {},
            "redirectURL": {},
            "rows": 6
        },
        "Budget Homes": {
            "api": apiConfig.listings({version:1}).url,
            "selector": {},
            "redirectURL": {},
            "rows": 2
        },
        "Trending Properties": {
            "api": apiConfig.listings({version:1}).url,
            "selector": {},
            "redirectURL": {}
        },
        "Rental Properties": {
            "api": apiConfig.listings({version:1}).url,
            "selector": {},
            "redirectURL": {}
        }
    },
    taxoCityMap: {
        "Recent properties": {
            "api": apiConfig.listings({version:1}).url,
            "selector": {},
            "redirectURL": {}
        },
        "Luxury Properties": {
            "api": apiConfig.listings({version:1}).url,
            "selector": {},
            "redirectURL": {},
            "rows": 6
        },
        "Budget Homes": {
            "api": apiConfig.listings({version:1}).url,
            "selector": {},
            "redirectURL": {},
            "rows": 2
        },
        "Popular Properties": {
            "api": apiConfig.listings({version:1}).url,
            "selector": {},
            "redirectURL": {}
        },
        "New Rental Properties": {
            "api": apiConfig.listings({version:1}).url,
            "selector": {},
            "redirectURL": {}
        }
    },
    minAffodablePrice: 4000000,
    maxAffodablePrice: 8000000,
    minLuxuryPrice: 15000000,
    maxBudgetPrice: 5000000

};

taxonomyService.getTaxonomyUrl = function(data, {req}) {
    logger.log(data);

    if (!data || (!data.localityId && !data.cityId)) {
        return new Promise(function(resolve, reject) {
            reject(new Error("localityId or CityId is not present"));
        });
    }
    var temp = {
        minAffodablePrice: data.minAffodablePrice || defaultConfig.minAffodablePrice,
        maxAffodablePrice: data.maxAffodablePrice || defaultConfig.maxAffodablePrice,
        minLuxuryPrice: data.minLuxuryPrice || defaultConfig.minLuxuryPrice,
        maxBudgetPrice: data.maxBudgetPrice || defaultConfig.maxBudgetPrice,
        id: data.localityId || data.cityId,
        name: data.localityName || data.cityName,
        isMobileRequest: data.isMobileRequest || false,
        type: data.localityId ?  'locality' : 'city'
    };

    return apiService.composite(_getSelector(temp.type, temp), {req}).then(function(response) {
            return _parseTaxonomyUrl(response, temp.type, {req});
        }, err => {
            throw err;
        });
};

function _getSelector(type, data) {
    var i, result = [],
        map, keys,
        length, __data;
    if (type == 'locality') {
        map = defaultConfig.taxoLocalityMap;
    } else if (type == 'city') {
        map = defaultConfig.taxoCityMap;
    }
    keys = Object.keys(map);
    length = keys.length;

    for (i = 0; i < length; i++) {
        map[keys[i]].selector = _createSelectorObject(keys[i], type, data);
        map[keys[i]].redirectURL = _getRedirectURL(keys[i], type, data);
        __data = {
            url: [map[keys[i]].api, '?selector=', map[keys[i]].selector].join(''),
            mockUrl: defaultConfig.mockUrl + (i + 1)
        };
        result.push(__data);
    }
    return result;
}

function _createSelectorObject(block, type, data) {
    var temp, commonFilter, result, fields, map, rows;
    if (type == 'locality') {
        map = defaultConfig.taxoLocalityMap;
    } else if (type == 'city') {
        map = defaultConfig.taxoCityMap;
    } else {
        return {};
    }
    rows = data.isMobileRequest ? defaultConfig.rows : (map[block].rows || defaultConfig.rows);
    commonFilter = [{
        key: type + 'Id',
        value: data.id
    }];
    fields = ['resaleUrl'];
    switch (block) {
        case "Recent properties":
            temp = {};
            temp.filters = [{
                key: 'listingCategory',
                value: ['Primary', 'Resale']
            }];
            temp.sort = [{
                key: 'listingVerificationDate',
                order: 'DESC'
            }];
            temp.filters = temp.filters.concat(commonFilter);
            //temp.fields = fields;
            break;
        case "Luxury Properties":
            temp = {};
            temp.filters = [{
                key: 'price',
                type: 'range',
                from: data.minLuxuryPrice
            }, {
                key: 'listingCategory',
                value: ['Primary', 'Resale']
            }];
            temp.filters = temp.filters.concat(commonFilter);

            // temp.fields = fields;
            break;
        case "Budget Homes":
            temp = {};
            temp.filters = [{
                key: 'price',
                type: 'range',
                to: data.maxBudgetPrice
            }, {
                key: 'listingCategory',
                value: ['Primary', 'Resale']
            }];
            temp.filters = temp.filters.concat(commonFilter);
            //temp.fields = fields;
            break;
        case "Trending Properties":
        case "Popular Properties":
            temp = {};
            temp.filters = [{
                key: 'listingCategory',
                value: ['Primary', 'Resale']
            }];
            temp.sort = [{
                key: 'listingQualityScore',
                order: 'DESC'
            }];
            temp.filters = temp.filters.concat(commonFilter);
            //temp.fields = fields;
            break;
        case "Rental Properties":
        case "New Rental Properties":
            temp = {};
            temp.filters = [{
                key: 'listingCategory',
                value: 'Rental'
            }];
            temp.sort = [{
                key: 'listingVerificationDate',
                order: 'DESC'
            }];
            temp.filters = temp.filters.concat(commonFilter);
            //temp.fields = fields;
            break;
    }
    temp.paging = {};
    temp.paging.rows = rows;
    result = apiService.createSelector(temp);
    return result;

}

function _parseTaxonomyUrl(response, type, {req}) {
    if (!(response && response.data)) {
        return {};
    }
    response = response.data;
    var result = {},
        i, j, length,
        temp, keys, map, listingData, listing,
        url, resultArr = [];

    if (type == 'locality') {
        map = defaultConfig.taxoLocalityMap;
    } else if (type == 'city') {
        map = defaultConfig.taxoCityMap;
    }
    if (type === 'locality' || type === 'city') {
        keys = Object.keys(map);
        length = keys.length;
        for (i = 0; i < length; i++) {
            url = map[keys[i]].api + '?selector=' + map[keys[i]].selector;

            result[keys[i]] = result[keys[i]] ? result[keys[i]] : {
                redirectURL: map[keys[i]].redirectURL,
                listingUrls: []
            };
            if (response[url] && response[url].data)  {
                for (j = 0; j < response[url].data.items.length; j++) {
                    temp = response[url].data.items[j].listing.resaleURL ? response[url].data.items[j].listing.resaleURL : '';
                    listing = response[url].data.items[j].listing;
                    listingData = listing && listing.property;
                    let listingUrl = listingService.getListingTitle(response[url].data.items[j].listing.property, response[url].data.items[j].listing.currentListingPrice.price, req.locals);
                    result[keys[i]].listingUrls.push({
                        'url': utilService.prefixToUrl(temp),
                        'label': listingUrl,
                        'bhk': listingData && listingData.bedrooms,
                        'unitType': listingData && listingData.unitType,
                        'listingType': listing && listing.listingCategory,
                        'budget': listing && listing.currentListingPrice && listing.currentListingPrice.price
                    });
                }
                if(!utilService.isEmptyObject(response[url].data.rawStats)){
                    response[url].data.rawStats.price.min = utilService.formatNumber(response[url].data.rawStats.price.min, { seperator : req.locals.numberFormat});
                    response[url].data.rawStats.price.max = utilService.formatNumber(response[url].data.rawStats.price.max, { seperator : req.locals.numberFormat});
                    result[keys[i]].rawStats = response[url].data.rawStats.price;
                }

            } else {
                result[keys[i]].listingUrls.push('error');
            }
            var obj = {};
            obj[keys[i]] = result[keys[i]];
            resultArr.push(obj);
        }
    }
    return resultArr;
}

function _getRedirectURL(block, type, data) {
    var filter,
        urlObj;

    if (type === 'locality' || type === "city") {
        let keyId = type + "Id",
            keyName = type + "Name";

        filter = {
            [`${keyId}`]: data.id, [`${keyName}`]: data.name
        };
        switch (block) {
            case "Recent properties":
                // filter.budget = [data.minAffodablePrice, data.maxAffodablePrice].join(",");
                filter.sortBy = "date-desc";
                break;
            case "Luxury Properties":
                filter.budget = [data.minLuxuryPrice, ""].join(",");
                filter.sortBy = "price-asc";
                break;
            case "Budget Homes":
                filter.budget = ["", data.maxBudgetPrice].join(",");
                break;
            case "Trending Properties":
                filter.sortBy = "price-asc";
                break;
            case "Popular Properties":
                filter.sortBy = "rating-desc";
                break;
            case "Rental Properties":
                filter.listingType = "rent";
                filter.sortBy = "price-asc";
                break;
            case "New Rental Properties":
                filter.listingType = "rent";
                filter.sortBy = "date-desc";
                break;
        }
        urlObj = {
            url: utilService.listingsPropertyPathName(),
        };
        return getURL(urlObj, filter).url;
    }
    return null;
}

module.exports = taxonomyService;
