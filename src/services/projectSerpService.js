"use strict";
const projectService 	= require('services/projectService');
const propertyService 	= require('services/propertyService');
const utils 			= require('services/utilService');
const maps 				= require('services/propertyMappingService');
const selectorService 	= require('services/serpSelectorService');
const globalConfig 		= require('configs/globalConfig');
const _ 				= utils._;
//const masterDetailsService = require('services/masterDetailsService');


var config = {
	validActiveStatus: ['active', 'activeinmakaan']
};

var _getProjectData = function(data = {}, params){
	let result 		= {}, 
		mainImage 	= data.mainImage || {},
		minPrice = Math.min(data.minResaleOrPrimaryPrice, data.minPrice),
		maxPrice = Math.min(data.maxResaleOrPrimaryPrice, data.maxPrice),
		minPricePerUnitArea = data.minPricePerUnitArea,
		maxPricePerUnitArea = data.maxPricePerUnitArea;

	result.isFeatured           = data.isProjectFeaturedBuy ? true : false;
	result.id 					= data.projectId;
	result.name					= data.name;
	result.overviewUrl  		= utils.prefixToUrl(data.overviewUrl);
	result.rentUrl 				= utils.prefixToUrl(data.rentUrl);
	result.buyUrl 				= utils.prefixToUrl(data.buyUrl);
	result.latitude 			= data.latitude;
	result.longitude 			= data.longitude;
	result.minSize 				= data.minSize;
	result.maxSize 				= data.maxSize;
	result.size 				= utils.getRange(utils.formatNumber(data.minSize, { type : 'number', seperator : params.numberFormat }), utils.formatNumber(data.maxSize, { type : 'number', seperator : params.numberFormat }), data.propertySizeMeasure);
	result.size 				= result.size && result.size + ' sq ft';
	result.beds					= data.distinctBedrooms;
	result.status				= data.projectStatus && maps.getProjectStatusKey(data.projectStatus);
	result.address				= data.completeProjectAddress;
	result.launchDate			= utils.formatDate(data.launchDate);
	result.launchDateFormatted 	= utils.formatDate(data.launchDate, 'YY-mm-dd');
	result.hideLaunchDate		= data.hideLaunchDate;
	result.totalTowers			= data.numberOfTowers;
	result.totalUnits			= utils.formatNumber(data.totalUnits, { type : 'number', seperator : params.numberFormat});
	result.totalArea			= data.sizeInAcres ? `${data.sizeInAcres} acres` : null;
	result.score 				= data.livabilityScore > 10 ? 10 : data.livabilityScore;
	result.activeStatus 		= data.activeStatus;
	result.unitTypes			= data.propertyUnitTypes;
	result.unitTypeId           = data.propertyUnitTypes && projectService.getMappedUnitTypeIds(data.propertyUnitTypes);
	result.minPriceNum          = minPrice;
	result.maxPriceNum          = maxPrice; 
	result.minPrice 			= utils.formatNumber(minPrice, { precision : 2, seperator : params.numberFormat });
	result.maxPrice 			= utils.formatNumber(maxPrice, { precision : 2, seperator : params.numberFormat });
	result.minPricePerUnitArea 	= utils.formatNumber(minPricePerUnitArea, { precision : 2, seperator : params.numberFormat });
	result.maxPricePerUnitArea 	= utils.formatNumber(maxPricePerUnitArea, { precision : 2, seperator : params.numberFormat });
	result.avgPricePerUnitArea 	= data.avgPricePerUnitArea ? utils.formatNumber(data.avgPricePerUnitArea, { precision : 2, seperator : params.numberFormat }) + '/ sq ft' : null ;
	result.pricePerUnitArea 	= minPricePerUnitArea && maxPricePerUnitArea ? minPricePerUnitArea == maxPricePerUnitArea ? result.minPricePerUnitArea+ '/sq ft' : result.minPricePerUnitArea + ' - ' + result.maxPricePerUnitArea+ '/sq ft' : null;
	result.price 				= minPrice && maxPrice ? minPrice == maxPrice ? result.minPrice : result.minPrice + ' - ' + result.maxPrice : null;
	result.image 				= mainImage.absolutePath;
	result.altText 				= mainImage.altText;
	result.imageTitle 			= mainImage.title;
	result.totalImages 			= 1;//data.imagesCount //|| 2
	result.possession			= utils.formatDate(data.possessionDate, 'SS YY');
	result.availability 		= projectService.getAvailability(data.listingAggregations);
	result.amenities			= propertyService.parseAmenities(projectService.getAmenityIds(data.projectAmenities), 'Apartment', 'Primary').amenityList;
	result.emi					= utils.formatNumber(
        utils.calculateEMI(data.minResaleOrPrimaryPrice, globalConfig.emi_rate, globalConfig.emi_tenure, globalConfig.downpayment_percent),
        { type : 'number', seperator : params.numberFormat}
    );
	result.priceRise 			= data.avgPriceRisePercentage ? data.avgPriceRisePercentage+' %' : null;
	result.priceRiseDuration    = data.avgPriceRiseMonths;
	result.priceRiseClass 		= data.avgPriceRisePercentage > 0 ? 'rise' : data.avgPriceRisePercentage < 0 ? 'fall' : '';
	result.properties 			= data.properties && data.properties.map(function(val){
									return val.propertyId;
								}) || [];
	result.listingFurnished = data.extraAttributes && data.extraAttributes.listingFurnished;
	result.hasTownShip = data.extraAttributes && data.extraAttributes.hasTownShip;
	result.showLeadForm = data.hasOwnProperty('listingAggregations');
	
	return result;
};	
var _getCityData = function(data = {}){
	let result = {};

	result.id 			= data.id;
	result.name			= data.label;
	result.overviewUrl  = utils.prefixToUrl(data.overviewUrl);
	result.rentUrl 		= utils.prefixToUrl(data.rentUrl);
	result.buyUrl 		= utils.prefixToUrl(data.buyUrl);
	return result;

};
var _getLocalityData = function(data = {}){
	let result = {};

	result.id 			= data.localityId;
	result.name			= data.label;
	result.overviewUrl  = utils.prefixToUrl(data.overviewUrl);
	result.rentUrl 		= utils.prefixToUrl(data.rentUrl);
	result.buyUrl 		= utils.prefixToUrl(data.buyUrl);
	return result;

};

var _getSuburbData = function(data = {}){
	let result = {};

	result.id 			= data.id;
	result.name			= data.label;
	result.overviewUrl  = utils.prefixToUrl(data.overviewUrl);
	result.rentUrl 		= utils.prefixToUrl(data.rentUrl);
	result.buyUrl 		= utils.prefixToUrl(data.buyUrl);
	return result;
};
var _getBuilderData = function(data = {}){
	let result = {}, mainImage = data.mainImage || {};

	result.id 			= data.id;
	result.name			= data.name;
	result.displayName	= data.displayName ? data.displayName : data.name;
	result.image 		= mainImage.absolutePath;
	result.altText 		= mainImage.altText;
	result.imageTitle 	= mainImage.title;
	result.score 		= data.builderScore;
	result.activeStatus = data.activeStatus;
	result.valid		= config.validActiveStatus.indexOf(result.activeStatus) > -1 ? true : false;
	return result;
};

var _parseProjectSerpData = function(response = [], selector, {req}){   //jshint ignore:line
	let result = {
		totalCount: response.totalCount,
		readableTotalCount: utils.formatNumber(response.totalCount, { type : 'number', seperator : req.locals.numberFormat}),
		data: [],
		selector
	};
	let projects = (response.data) || [];
	_.forEach(projects,(project)=>{
		project = project || {};

		let parsedProjectDetails = {};
		
		let locality = project.locality || {};
		let builder  = project.builder || {};
		let suburb 	 = locality.suburb || {};
		let city 	 = suburb.city || {};

		parsedProjectDetails.project 	= _getProjectData(project, req.locals);
		parsedProjectDetails.city 		= _getCityData(city);
		parsedProjectDetails.locality 	= _getLocalityData(locality);
		parsedProjectDetails.suburb 	= _getSuburbData(suburb);
		parsedProjectDetails.builder 	= _getBuilderData(builder);

		result.data.push(parsedProjectDetails);
	});
	return result;
};

var getProjectsData = function(query, urlDetail, { req }) {
	query.fields = ["isProjectFeaturedBuy",
					"builderScore",
					"builder",
					"displayName",
					"name",
					"completeProjectAddress",
					"launchDate",
					"hideLaunchDate",
					"totalTowers",
					"totalUnits",
					"sizeInAcres",
					"listingAggregations",
					"propertySizeMeasure",
					"activeStatus",
					"projectStatus",
					"mainImage",
					"absolutePath",
					"altText",
					"title",
					"propertyUnitTypes",
					"maxPrice",
					"minPrice",
					"livabilityScore",
					"maxResaleOrPrimaryPrice",
					"minResaleOrPrimaryPrice",
					"avgPricePerUnitArea",
					"minPricePerUnitArea",
					"maxPricePerUnitArea",
					"maxSize",
					"minSize",
					"abbreviation",
					"amenityName",
					"amenityId",
					"amenityDisplayName",
					"amenityMaster",
					"distinctBedrooms",
					"propertyUnitTypes",
					"localityId",
					"projectId",
					"id",
					"label",
					"overviewUrl",
					"rentUrl",
					"buyUrl",
					"city",
					"locality",
					"suburb",
					"projectAmenities",
					"numberOfTowers",
					"avgPriceRiseMonths",
					"avgPriceRisePercentage",
					// "properties",
					"imagesCount",
					"latitude",
					"propertyId",
					"longitude",
					"extraAttributes"];
    let queryObj = selectorService.getProjectSerpSelectorQuery(query, urlDetail, globalConfig.projectSerpItemsPerPage);
    return projectService.getProjectSerpWithFilters({ selector: queryObj.selector }, { req }).then((response)=>{
    	return _parseProjectSerpData(response, queryObj.selector, {req});
    },(err)=>{
    	throw err;
    });
};



module.exports = {
    getProjectsData
};
