'use strict'

var logger = require('services/loggerService'),
utils = require('services/utilService'),
masterDetailsService = require('services/masterDetailsService'),
experimentsCookieName = require('public/scripts/common/sharedConfig').EXPERIMENTS_COOKIE_NAME,
experimentCurrentVariationCounts = {},
experimentRatios,
ExperimentsList = [];

function initialize () {
  experimentRatios = setExperimentRatios()
  setExperimentCounts()
  ExperimentsList[0].refreshed = false
}

// On boot set experiment variation counts in experimentCurrentVariationCounts object
function setExperimentCounts () {
  for (var i in ExperimentsList) {
    if (!experimentCurrentVariationCounts[ExperimentsList[i].name]) {
      experimentCurrentVariationCounts[ExperimentsList[i].name] = {variations: {}, sum: 0}
    }
    var curr = experimentCurrentVariationCounts[ExperimentsList[i].name]
    for (var j in ExperimentsList[i].variations) {
      var variation = ExperimentsList[i].variations[j]
      if (!curr.variations[variation.name]) {
        curr.variations[variation.name] = 1
        curr.sum++
      }
    }
  }
}

// On boot store experiment variation ratios in experimentRatios object
function setExperimentRatios () {
  var obj = {}
  for (var i in ExperimentsList) {
    var curr = ExperimentsList[i]
    obj[curr.name] = {}
    obj[curr.name]['ratios'] = {}
    obj[curr.name]['sum'] = 0
    for (var j in curr.variations) {
      obj[curr.name]['sum'] += parseInt(curr.variations[j].weightage)
      obj[curr.name]['ratios'][curr.variations[j].name] = curr.variations[j].weightage
    }
  }
  return obj
}

/**
* @params
*   experiment{Object} : Config for specified experiment
*   res{Object} : response object used for dropping cookies
**/
function Experiments (experiment, res) {
  this.name = experiment.name
  this.variations = experiment.variations
  this.res = res
  return this
}

/**
* Sets cookie of selected variation for current experiment
**/
Experiments.prototype.select = function () {
  var specifiedRatios = experimentRatios[this.name]

  var variationIndex = this.plan(specifiedRatios, experimentCurrentVariationCounts[this.name])
  var selectedVariation = this.variations[variationIndex]

  experimentCurrentVariationCounts[this.name].variations[selectedVariation.name]++
  experimentCurrentVariationCounts[this.name].sum++

  return {name: this.name, value: selectedVariation.cookie}
}

/**
* @params
*  ratiosData{Object} : Varition ratios to be maintained for current experiment
*  counts{Object} : current variation counts for current experiment
*
* @returns
*   variationIndex{Number} : index of the variation chosen for current request
**/
Experiments.prototype.plan = function (ratiosData, current) {
  var relation = current.sum / ratiosData.sum
  var variationIndex = 0
  for (var i in ratiosData.ratios) {
    (variationIndex === 0) ? variationIndex = i : ''; //jshint ignore:line
    if (current.variations[i] < ratiosData.ratios[i] * relation) {
      variationIndex = i
      break
    }
  }
  return variationIndex
}

/**
* Init experiment cookies on each request
*/

function InitExperiments (req, res) {
  const experimentsToAdd = []
  const experimentsToRemove = []
  const expObj = {}
  // Drop cookies for each experiment if cookie doesn't exist
  try {
    if (ExperimentsList[0].refreshed) {
      initialize()
    }
    if (req.cookies[experimentsCookieName]) {
      var allExperiments = req.cookies[experimentsCookieName];
      var splitedExps = allExperiments.split(';')
      splitedExps.forEach(function (exp) {
        if (!!exp) { // eslint-disable-line
          var nowExp = exp.split('=')
          expObj[nowExp[0]] = nowExp[1]
        }
      })
      var ccExp = {}
      ExperimentsList.forEach(function (expList) {
        ccExp[expList.name] = expList
      })

      Object.keys(expObj).forEach(function (exp) {
        if (!ccExp[exp] || (ccExp[exp] && !ccExp[exp].live)) {
          delete expObj[exp]
        }
      })
    }
  } catch (e) {
    console.log(e)
  }

  for (var i in ExperimentsList) {
    try {
      var cookieExists = utils.getParamFromCookie(req.cookies, experimentsCookieName, ExperimentsList[i].name)
      var affiliatedUser = utils.getParamFromCookie(req.cookies, 'traffic', 'affiliate') && utils.getParamFromCookie(req.cookies, 'traffic', 'affiliate') == 'true' //eslint-disable-line
      var returningUser = utils.getParamFromCookie(req.cookies, 'uuid', 'id')
      var referralUser = utils.getParamFromCookie(req.cookies, 'traffic', 'referral') && utils.getParamFromCookie(req.cookies, 'traffic', 'referral') == 'true' //eslint-disable-line

      var dropCookie = (
       (!cookieExists && ExperimentsList[i].live) &&
        !(ExperimentsList[i].userSpecific === 'new' && returningUser) &&
        !(ExperimentsList[i].trafficType === 'affiliate' && !affiliatedUser) &&
        !(ExperimentsList[i].trafficType === 'referral' && !referralUser) &&
        !(ExperimentsList[i].trafficType === 'organic' && (affiliatedUser || referralUser))
        )

      if (dropCookie &&
          ExperimentsList[i].live
      ) {
        var exe = new Experiments(ExperimentsList[i], res)
        experimentsToAdd.push(exe.select())
      } else if (!ExperimentsList[i].live) {
        experimentsToRemove.push(ExperimentsList[i].name)
      }
    } catch (error) {
      error.message += ' :: setting experiment failed for ' + ExperimentsList[i].name
      error.original_url = req.url
      error.referrer = req.header('Referrer')
      error.tags = ['setting-experiment-failed']
      error.aggregation_key = 'setting-experiment-failed'
      logger.error(error)
    }
  }
  // for custom testing/UAT instead of having to create experiments or screwing arnd with ratios
  // pass the experiment as a queryParam obj
  // to get the actual cookie data back just clear the experiments cookie
  try {
    const experimentQueryParam = req.query && req.query.experiments && JSON.parse(req.query.experiments.replace(/(['"])?([a-zA-Z0-9_]+)(['"])?:/g, '"$2": ')) || {}
    if (experimentQueryParam && Object.keys(experimentQueryParam).length) {
      Object.keys(experimentQueryParam).map(experimentName => {
        experimentsToAdd.push({
          name: experimentName,
          value: experimentQueryParam[experimentName]
        })
        // to override the current experiment value delete current value
        // not needed particularly as we add this at the end of cookie str
        delete expObj[experimentName]
      })
    }
  } catch (error) {
    error.message += ' JSON parse failed for custom experiments provided in query'
    error.original_url = req.url
    error.referrer = req.header('Referrer')
    error.tags = ['setting-experiment-failed']
    error.aggregation_key = 'setting-experiment-failed'
    logger.error(error)
  }

  return {experimentsToAdd, experimentsToRemove, expObj}
}
if (ExperimentsList.length !== 0) {
  initialize()
}

module.exports.setExperimentsCookie = function (req, res) {
  ExperimentsList = masterDetailsService.getMasterDetails('experimentList');
  if (ExperimentsList.length === 0) {
    return;
  }
  const { experimentsToAdd, expObj } = InitExperiments(req, res); //jshint ignore:line
  let experimentsToAddStr = ''
  if (Object.keys(expObj).length) {
    Object.keys(expObj).forEach(function (exp) {
      experimentsToAddStr += `${exp}=${expObj[exp]};`
    })
  }
  if (experimentsToAdd.length) {
    experimentsToAdd.forEach(function (item) {
      experimentsToAddStr += `${item.name}=${item.value};`
    })
  }
  // if () { // eslint-disable-line
  const options = {path: '/', http: false}
  options.expires = new Date((Date.now()) + (30 * 86400000))
  res.cookie(experimentsCookieName, experimentsToAddStr, options)
  req.cookies[experimentsCookieName] = experimentsToAddStr
}
