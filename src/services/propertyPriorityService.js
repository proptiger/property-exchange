'use strict';

var _propertyPriority = {
    "Primary": {
        "Apartment": {
            "overview": ['status', 'age', 'posession', 'bath', 'floor', 'balcony', 'totalFloor', 'category', 'bookamt', 'openSides', 'entryRoadWd', 'facing', 'parking', 'overlook', 'addRoom', 'ownerType', 'plc', 'carpetArea'],
            "amenity": ['18', '17', '13', '11', '3', '20', '4', '1', '2', '8', '15', '12', '16', '10', '6', '5', '9', '19', '7', '14', '21', '22', '23', '24'],
            "furnish": ['11', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '12']
        },
        "BuilderFloor": {
            "overview": ['status', 'age', 'posession', 'bath', 'floor', 'category', 'entryRoadWd', 'balcony', 'totalFloor', 'bookamt', 'openSides', 'tenantPref', 'facing', 'parking', 'overlook', 'addRoom', 'ownerType', 'plc', 'carpetArea'],
            "amenity": ['18', '17', '13', '11', '3', '20', '4', '1', '2', '8', '15', '12', '16', '10', '6', '5', '9', '19', '7', '14', '21', '22', '23', '24'],
            "furnish": ['11', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '12']
        },
        "IndependentHouse": {
            "overview": ['status', 'age', 'posession', 'bath', 'floor', 'category', 'entryRoadWd', 'balcony', 'totalFloor', 'bookamt', 'openSides', 'facing', 'parking', 'overlook', 'addRoom', 'ownerType', 'plc', 'carpetArea'],
            "amenity": ['18', '17', '13', '11', '3', '20', '4', '1', '2', '8', '15', '12', '16', '10', '6', '5', '9', '19', '7', '14', '21', '22', '23', '24'],
            "furnish": ['11', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '12']
        },
        "Villa": {
            "overview": ['status', 'age', 'posession', 'bath', 'floor', 'category', 'entryRoadWd', 'balcony', 'totalFloor', 'bookamt', 'openSides', 'facing', 'parking', 'overlook', 'addRoom', 'ownerType', 'plc', 'carpetArea'],
            "amenity": ['18', '17', '13', '11', '3', '20', '4', '1', '2', '8', '15', '12', '16', '10', '6', '5', '9', '19', '7', '14', '21', '22', '23', '24'],
            "furnish": ['11', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '12']
        },
        "Plot": {
            "overview": ['age', 'posession', 'openSides', 'bookamt', 'facing', 'ownerType', 'category', 'entryRoadWd', 'parking', 'overlook', 'plc'],
            "amenity": [],//['1', '3', '10', '11', '2', '13', '8', '15', '4', '5', '12', '6', '9', '16','18', '17'],
            "furnish": []//['11', 'War', '2', '3', '4', '5', '6', '7', '8', '9', '10']
        },
        "Penthouse": {
            "overview": ['status', 'age', 'posession', 'bath', 'floor', 'category', 'balcony', 'openSides', 'bookamt', 'facing', 'entryRoadWd', 'totalFloor', 'parking', 'overlook', 'addRoom', 'ownerType', 'plc', 'carpetArea'],
            "amenity": ['18', '17', '13', '11', '3', '20', '4', '1', '2', '8', '15', '12', '16', '10', '6', '5', '9', '19', '7', '14', '21', '22', '23', '24'],
            "furnish": ['11', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '12']
        },
        "StudioApartment": {
            "overview": ['status', 'age', 'posession', 'bath', 'floor', 'category', 'balcony', 'openSides', 'bookamt', 'facing', 'entryRoadWd', 'totalFloor', 'parking', 'overlook', 'addRoom', 'ownerType', 'plc', 'carpetArea'],
            "amenity": ['18', '17', '13', '11', '3', '20', '4', '1', '2', '8', '15', '12', '16', '10', '6', '5', '9', '19', '7', '14', '21', '22', '23', '24'],
            "furnish": ['11', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '12']
        },
        "Farmhouse": {
            "overview": ['status', 'age', 'posession', 'bath', 'balcony', 'floor', 'category', 'totalFloor', 'openSides', 'bookamt', 'facing', 'entryRoadWd', 'parking', 'overlook', 'addRoom', 'ownerType', 'plc', 'carpetArea'],
            "amenity": ['18', '17', '13', '11', '3', '20', '4', '1', '2', '8', '15', '12', '16', '10', '6', '5', '9', '19', '7', '14', '21', '22', '23', '24'],
            "furnish": ['11', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '12']
        },
        "Office": {
            "overview": ['status', 'availability', 'posession', 'floor', 'category', 'age', 'furnishStat', 'bath', 'assureReturnPercentage', 'currentlyLeasedOut', 'pantryType', 'parking'],
            "amenity": ['17', '151', '110', '5', '152', '153', '10', '120', '18', '13', '154', '155', '11', '127', '156', '157', '149'],
        },
        "Shop": {
            "overview": ['status', 'availability', 'posession', 'isCornerShop', 'category', 'age', 'furnishStat', 'floor', 'bath', 'assureReturnPercentage', 'currentlyLeasedOut', 'isMainRoadFacing', 'entryRoadWd', 'openSides', 'isBoundryWallPresent', 'parking'],
            "amenity": ['17', '151', '110', '5', '152', '153', '10', '120', '18', '13', '154', '155', '11', '127', '156', '157', '149'],
        }
    },
    "Rental": {
        "Apartment": {
            "overview": ['furnishStat', 'securityDeposit', 'availability', 'tenantPref', 'bath', 'floor', 'balcony', 'totalFloor', 'openSides', 'facing', 'parking', 'overlook', 'addRoom', 'plc', 'carpetArea'],
            "amenity": ['18', '17', '13', '11', '3', '20', '4', '1', '2', '8', '15', '12', '16', '10', '6', '5', '9', '19', '7', '14', '21', '22', '23', '24'],
            "furnish": ['11', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '12']
        },
        "BuilderFloor": {
            "overview": ['furnishStat', 'securityDeposit', 'availability', 'tenantPref', 'bath', 'floor', 'balcony', 'totalFloor', 'openSides', 'facing', 'parking', 'overlook', 'addRoom', 'plc', 'carpetArea'],
            "amenity": ['18', '17', '13', '11', '3', '20', '4', '1', '2', '8', '15', '12', '16', '10', '6', '5', '9', '19', '7', '14', '21', '22', '23', '24'],
            "furnish": ['11', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '12']
        },
        "IndependentHouse": {
            "overview": ['furnishStat', 'securityDeposit', 'availability', 'tenantPref', 'bath', 'floor', 'balcony', 'totalFloor', 'openSides', 'facing', 'parking', 'overlook', 'addRoom', 'plc', 'carpetArea'],
            "amenity": ['18', '17', '13', '11', '3', '20', '4', '1', '2', '8', '15', '12', '16', '10', '6', '5', '9', '19', '7', '14', '21', '22', '23', '24'],
            "furnish": ['11', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '12']
        },
        "Villa": {
            "overview": ['furnishStat', 'securityDeposit', 'availability', 'tenantPref', 'bath', 'floor', 'balcony', 'totalFloor', 'openSides', 'facing', 'parking', 'overlook', 'addRoom', 'plc', 'carpetArea'],
            "amenity": ['18', '17', '13', '11', '3', '20', '4', '1', '2', '8', '15', '12', '16', '10', '6', '5', '9', '19', '7', '14', '21', '22', '23', '24'],
            "furnish": ['11', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '12']
        },
        "Plot": {
            "overview": ['age', 'posession', 'openSides', 'bookamt', 'facing', 'ownerType', 'category', 'entryRoadWd', 'parking', 'overlook', 'plc', 'carpetArea'],
            "amenity": [],//['1', '3', '10', '11', '2', '13', '8', '15', '4', '5', '12', '6', '9', '16','18', '17'],
            "furnish": []//['11', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10']
        },
        "Penthouse": {
            "overview": ['furnishStat', 'securityDeposit', 'availability', 'tenantPref', 'bath', 'floor', 'balcony', 'totalFloor', 'openSides', 'facing', 'parking', 'overlook', 'addRoom', 'plc', 'carpetArea'],
            "amenity": ['18', '17', '13', '11', '3', '20', '4', '1', '2', '8', '15', '12', '16', '10', '6', '5', '9', '19', '7', '14', '21', '22', '23', '24'],
            "furnish": ['11', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '12']
        },
        "StudioApartment": {
            "overview": ['furnishStat', 'securityDeposit', 'availability', 'tenantPref', 'bath', 'floor', 'balcony', 'totalFloor', 'openSides', 'facing', 'parking', 'overlook', 'addRoom', 'plc', 'carpetArea'],
            "amenity": ['18', '17', '13', '11', '3', '20', '4', '1', '2', '8', '15', '12', '16', '10', '6', '5', '9', '19', '7', '14', '21', '22', '23', '24'],
            "furnish": ['11', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '12']
        },
        "Farmhouse": {
            "overview": ['furnishStat', 'securityDeposit', 'availability', 'tenantPref', 'bath', 'floor', 'balcony', 'totalFloor', 'openSides', 'facing', 'parking', 'overlook', 'addRoom', 'plc', 'carpetArea'],
            "amenity": ['18', '17', '13', '11', '3', '20', '4', '1', '2', '8', '15', '12', '16', '10', '6', '5', '9', '19', '7', '14', '21', '22', '23', '24'],
            "furnish": ['11', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '12']
        },
        "Office": {
            "overview": ['furnishStat','availability','posession','floor', 'age', 'noOfSeats', 'noOfRooms', 'bath', 'lockInPeriod', 'parking', 'pantryType' ,'willingToModifyInteriors'],
            "amenity": ['17', '151', '110', '5', '152', '153', '10', '120', '18', '13', '154', '155', '11', '127', '156', '157', '149'],
        },
        "Shop": {
            "overview": ['furnishStat','availability','posession', 'isCornerShop', 'age', 'floor', 'bath', 'lockInPeriod', 'isMainRoadFacing', 'entryRoadWd', 'openSides', 'isBoundryWallPresent', 'parking'],
            "amenity": ['17', '151', '110', '5', '152', '153', '10', '120', '18', '13', '154', '155', '11', '127', '156', '157', '149'],
        }
    },
    "Resale": {
        "Apartment": {
            "overview": ['status', 'age', 'posession', 'bath', 'floor', 'balcony', 'furnishStat', 'totalFloor', 'category', 'bookamt', 'openSides', 'securityDeposit', 'tenantPref', 'entryRoadWd', 'facing', 'priceNeg', 'parking', 'overlook', 'addRoom', 'ownerType', 'plc', 'carpetArea'],
            "amenity": ['18', '17', '13', '11', '3', '20', '4', '1', '2', '8', '15', '12', '16', '10', '6', '5', '9', '19', '7', '14', '21', '22', '23', '24'],
            "furnish": ['11', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '12']
        },
        "BuilderFloor": {
            "overview": ['status', 'age', 'posession', 'bath', 'floor', 'category', 'entryRoadWd', 'balcony', 'furnishStat', 'totalFloor', 'bookamt', 'openSides', 'securityDeposit', 'tenantPref', 'facing', 'priceNeg', 'parking', 'overlook', 'addRoom', 'ownerType', 'plc', 'carpetArea'],
            "amenity": ['18', '17', '13', '11', '3', '20', '4', '1', '2', '8', '15', '12', '16', '10', '6', '5', '9', '19', '7', '14', '21', '22', '23', '24'],
            "furnish": ['11', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '12']
        },
        "IndependentHouse": {
            "overview": ['status', 'age', 'posession', 'bath', 'floor', 'category', 'entryRoadWd', 'balcony', 'furnishStat', 'totalFloor', 'bookamt', 'openSides', 'securityDeposit', 'tenantPref', 'facing', 'priceNeg', 'parking', 'overlook', 'addRoom', 'ownerType', 'plc', 'carpetArea'],
            "amenity": ['18', '17', '13', '11', '3', '20', '4', '1', '2', '8', '15', '12', '16', '10', '6', '5', '9', '19', '7', '14', '21', '22', '23', '24'],
            "furnish": ['11', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '12']
        },
        "Villa": {
            "overview": ['status', 'age', 'posession', 'bath', 'floor', 'category', 'entryRoadWd', 'balcony', 'furnishStat', 'totalFloor', 'bookamt', 'openSides', 'securityDeposit', 'tenantPref', 'facing', 'priceNeg', 'parking', 'overlook', 'addRoom', 'ownerType', 'plc', 'carpetArea'],
            "amenity": ['18', '17', '13', '11', '3', '20', '4', '1', '2', '8', '15', '12', '16', '10', '6', '5', '9', '19', '7', '14', '21', '22', '23', '24'],
            "furnish": ['11', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '12']
        },
        "Plot": {
            "overview": ['age', 'posession', 'openSides', 'bookamt', 'facing', 'ownerType', 'category', 'entryRoadWd', 'parking', 'overlook', 'plc', 'carpetArea'],
            "amenity": [],//['1', '3', '10', '11', '2', '13', '8', '15', '4', '5', '12', '6', '9', '16','18', '17'],
            "furnish": []//['11', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10']
        },
        "Penthouse": {
            "overview": ['status', 'age', 'posession', 'bath', 'floor', 'category', 'balcony', 'openSides', 'bookamt', 'facing', 'entryRoadWd', 'furnishStat', 'totalFloor', 'securityDeposit', 'tenantPref', 'priceNeg', 'parking', 'overlook', 'addRoom', 'ownerType', 'plc', 'carpetArea'],
            "amenity": ['18', '17', '13', '11', '3', '20', '4', '1', '2', '8', '15', '12', '16', '10', '6', '5', '9', '19', '7', '14', '21', '22', '23', '24'],
            "furnish": ['11', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '12']
        },
        "StudioApartment": {
            "overview": ['status', 'age', 'posession', 'bath', 'floor', 'category', 'balcony', 'furnishStat', 'openSides', 'bookamt', 'facing', 'entryRoadWd', 'totalFloor', 'securityDeposit', 'tenantPref', 'priceNeg', 'parking', 'overlook', 'addRoom', 'ownerType', 'plc', 'carpetArea'],
            "amenity": ['18', '17', '13', '11', '3', '20', '4', '1', '2', '8', '15', '12', '16', '10', '6', '5', '9', '19', '7', '14', '21', '22', '23', '24'],
            "furnish": ['11', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '12']
        },
        "Farmhouse": {
            "overview": ['status', 'age', 'posession', 'bath', 'balcony', 'furnishStat', 'floor', 'category', 'totalFloor', 'openSides', 'bookamt', 'facing', 'entryRoadWd', 'securityDeposit', 'tenantPref', 'priceNeg', 'parking', 'overlook', 'addRoom', 'ownerType', 'plc', 'carpetArea'],
            "amenity": ['18', '17', '13', '11', '3', '20', '4', '1', '2', '8', '15', '12', '16', '10', '6', '5', '9', '19', '7', '14', '21', '22', '23', '24'],
            "furnish": ['11', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '12']
        },
        "Office": {
            "overview": ['status', 'availability', 'posession', 'floor', 'category', 'age', 'furnishStat', 'bath', 'assureReturnPercentage', 'currentlyLeasedOut', 'pantryType', 'parking'],
            "amenity": ['17', '151', '110', '5', '152', '153', '10', '120', '18', '13', '154', '155', '11', '127', '156', '157', '149'],
        },
        "Shop": {
            "overview": ['status', 'availability', 'posession', 'isCornerShop', 'category', 'age', 'furnishStat', 'floor', 'bath', 'assureReturnPercentage', 'currentlyLeasedOut', 'isMainRoadFacing', 'entryRoadWd', 'openSides', 'isBoundryWallPresent', 'parking'],
            "amenity": ['17', '151', '110', '5', '152', '153', '10', '120', '18', '13', '154', '155', '11', '127', '156', '157', '149'],
        }
    }
};

var projectPriority = ['propertyType', 'size', 'status', 'availability', 'posession', 'totalArea', 'launchDate', 'reraRegistrationNumber', 'allowedFloor', 'numberOfTowers', 'onlineBooking', 'bookingStat', 'complDate', 'expComplDate', 'preLaunchDate', 'expPosDate', 'township', 'totalUnits', 'totalUnitsSale', 'totalUnitsRent'];

var propertyThirdFold = ['bookamt', 'securityDeposit', 'maintenanceCharges'];

module.exports.getPriorityArray = function(type='Apartment', category='Primary', card) {  //jshint ignore:line
    let categoryData=_propertyPriority[category] || _propertyPriority['Primary'],
        typeData = categoryData[type] || categoryData['Apartment'];
    return typeData[card];
};
module.exports.projectPriority = function(){
    return projectPriority;
};
module.exports.propertyThirdFold = function(){
    return propertyThirdFold;
}
