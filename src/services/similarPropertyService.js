"use strict";
//const similarService = require("services/similarPropertyService"),
  const    utils = require("services/utilService"),
     // maps = require('services/mappingService'),
      _ = require('lodash'),
      selectorService = require('services/serpSelectorService'),
      apiService = require('services/apiService'),
      apiConfig = require('configs/apiConfig'),
      imageParser = require('services/imageParser');

var   SELLER_TYPE = 'seller',
      PROJECT_TYPE = 'project',
      DUMMY_STATUS = 'DUMMY',
      ACTIVE_PROJECT_STATUSES = ["ACTIVE", "ACTIVEINMAKAAN"],
      RESIDENTIAL_PLOT = 'residential-plot';

function getApiListings(pageData,cardType, {req, isCommercial}) {
    var fields = [
        "listing",
        "companySeller",
        "currentListingPrice",
        "property",
        "bedrooms",
        "mainImageURL",
        "mainImage",
        "altText",
        "title",
        "resaleURL",
        "price",
        "company",
        "id",
        "rk",
        "penthouse",
        "studio",
        "listingCategory",
        "project",
        "projectStatus",
        "city",
        "locality",
        "suburb",
        "propertyId",
        "project",
        "projectId",
        "locality",
        "localityId",
        "builder",
        "displayName",
        "listingId",
        "buyUrl",
        "rentUrl",
        "name",
        "label",
        "activeStatus",
        "unitType",
        "qualityScore"
    ],
    minItems = 2,
    maxItems = 6,
    urlDetail = {},
    query,
    sellerId = pageData.sellerId;

    if(cardType == PROJECT_TYPE) {
        query = _getAllFieldsProject(pageData, isCommercial);
    } else if(cardType == SELLER_TYPE) {
        query = _getAllFieldsSeller(pageData, isCommercial);
    }   

        urlDetail.pageType = 'SIMILAR_PROPERTY_URLS';
        query.fields = fields.slice();
    return _getApiListings(query,urlDetail,query.itemsPerPage, {req}).then((response)=>{
        response = _parseResponse(cardType,response, minItems, maxItems, sellerId, pageData, req);
        return response;
    });
};

function _getAllFieldsProject(pageData, isCommercial) {
    var fieldsObj = {},
        size,
        minSize,
        maxSize;

    size = pageData.size;
    minSize = (size - Math.ceil(0.05*size));
    maxSize = (size + Math.ceil(0.05*size));
    fieldsObj.area = [minSize,maxSize].join();
    if(pageData.bhk === 0){
        if(isCommercial) {
            fieldsObj.propertyType = pageData.unitType.toLowerCase();
        } else{
            fieldsObj.propertyType = RESIDENTIAL_PLOT;
        }
    } else if(pageData.bhk) {
        fieldsObj.beds = pageData.bhk.toString();
    }
    if(pageData.projectId) {
        fieldsObj.projectId = pageData.projectId.toString();
    }
    _.merge(fieldsObj,_getCommonFields(pageData));
    return fieldsObj;
}

function _getAllFieldsSeller(pageData, isCommercial) {
    var fieldsObj = {},
        bhk,
        minBhk,
        maxBhk,
        budget,
        minBudget,
        maxBudget;
//        bhkArr = [];

    bhk = pageData.bhk;
    if(bhk === 0){
        if(isCommercial) {
            fieldsObj.propertyType = pageData.unitType.toLowerCase();
        } else{
            fieldsObj.propertyType = RESIDENTIAL_PLOT;
        }
    } else {
        minBhk = (bhk-1)>0?(bhk-1):bhk;
        maxBhk = bhk+1;
        fieldsObj.beds = [minBhk,bhk,maxBhk].join();
    }

    budget = pageData.budget;
    minBudget = (budget - Math.ceil(0.15*budget));
    maxBudget = (budget + Math.ceil(0.15*budget));
    fieldsObj.budget = [minBudget,maxBudget].join();
    fieldsObj.sellerId = pageData.sellerId && pageData.sellerId.toString();
    _.merge(fieldsObj,_getCommonFields(pageData));
    return fieldsObj;
}

function _getCommonFields(pageData) {
    var fieldsObj = {},
        tempCategory;

    fieldsObj.urlDetailpageType = 'SIMILAR_PROPERTY_URLS';
    fieldsObj.notListingId = pageData.listingId && pageData.listingId.toString();
    fieldsObj.cityId = pageData.cityId && pageData.cityId.toString();
    fieldsObj.itemsPerPage = 6;
    fieldsObj.minItems = 2;
    fieldsObj.maxItems = 6;
    tempCategory = pageData.listingCategory && pageData.listingCategory.toLowerCase();
    if(tempCategory.indexOf("rental")>=0) {
        fieldsObj.buyOrRent = 'rent';
    }else {
        fieldsObj.buyOrRent = 'buy';
    }
    return fieldsObj;
}

function _getApiListings(queryParams, urlDetail, itemsPerPage, {req}) {
    let queryObj = selectorService.getSelectorQuery(queryParams, urlDetail, itemsPerPage),
        listingApi;
    //  explicitely added these to ensure that nearby and sponsored results donot get added
    queryObj.includeNearbyResults = false;
    queryObj.includeSponsoredResults = false;

    listingApi = apiConfig.listings({
       version: 1,	
       query: queryObj
    });
    return apiService.get(listingApi, {req});
}

function _getSeeAllUrl(cardType,pageData,parsedListingArray) {
    let seeAllUrl,
        pageType = 'SELLER_PROPERTY_URLS',
        sellerName = pageData.sellerName,
        sellerId = pageData.sellerId,
        cityId = pageData.cityId,
        cityName = pageData.cityName,
        listingType;
    if(pageData.listingCategory && pageData.listingCategory.toLowerCase() == "rental") {
        seeAllUrl = parsedListingArray[0].rentUrl;
        if(cardType == 'seller') {
            seeAllUrl = utils.listingsPropertyPathName();
            listingType = pageData.listingType;
            sellerName = parsedListingArray[0].sellerName;
            seeAllUrl = `${seeAllUrl}?cityId=${cityId}&cityName=${cityName}&sellerId=${sellerId}&sellerName=${sellerName}&pageType=${pageType}&listingType=${listingType}`;
        }
    } else {
        seeAllUrl = parsedListingArray[0].buyUrl;
        if(cardType == 'seller') {
            seeAllUrl = utils.listingsPropertyPathName();
            listingType = pageData.listingType;
            sellerName = parsedListingArray[0].sellerName;
            seeAllUrl = `${seeAllUrl}?cityId=${cityId}&cityName=${cityName}&sellerId=${sellerId}&sellerName=${sellerName}&pageType=${pageType}&listingType=${listingType}`;
        }
    }
    return seeAllUrl;
}

function _parseResponse(cardType, response, minItems, maxItems, sellerId, pageData, req) {
    var parsedListingArray = [],
        seller,
        currentSellerPropertyArray = [],
        differentSellerPropertyArray = [],
        projectInfo,
        seeAllUrl;

    if(!response || !response.data || !response.data.items || response.data.items.length<minItems) {
        return { 'properties' : parsedListingArray , 'seeAllUrl': seeAllUrl};
    }
    var listingArray = response.data.items;

    listingArray.forEach(function(value){
        var parsedObject = {},
            priceWordObj;

        if(value && value.listing) {
            value = value.listing;
        }
        if(value.companySeller &&  value.companySeller.company) {
            seller = value.companySeller.company;
            parsedObject.sellerName = seller.name;
            parsedObject.sellerId = seller.id;
            parsedObject.sellerScore = seller.score;
        }
        parsedObject.listingScore = value.qualityScore;
        parsedObject.bhk = value.property && value.property.bedrooms;
        parsedObject.propertyId = value.property && value.property.propertyId; 
        if(value.property && value.property.project) {
            projectInfo = value.property.project;
            parsedObject.projectStatus = projectInfo.projectStatus;
            parsedObject.projectId = projectInfo.projectId;
            if(projectInfo.activeStatus && projectInfo.activeStatus.toUpperCase() != DUMMY_STATUS) {
                parsedObject.projectName = projectInfo.name;
                parsedObject.fullprojectName = (projectInfo.builder.displayName || projectInfo.builder.name)+' '+projectInfo.name;
            }
            parsedObject.localityId = projectInfo.localityId;
            parsedObject.localityName = projectInfo.locality && projectInfo.locality.label;

            let builderStatus = projectInfo.builder && projectInfo.builder.activeStatus ? projectInfo.builder.activeStatus.toUpperCase() : null;
            if(builderStatus != DUMMY_STATUS) {
                if(ACTIVE_PROJECT_STATUSES.indexOf(builderStatus) > -1){
                    parsedObject.builderName = projectInfo.builder.name;
                    parsedObject.fullprojectName = (projectInfo.builder.displayName || projectInfo.builder.name)+' '+projectInfo.name;
                }
            }
            parsedObject.builderId =  projectInfo.builder && projectInfo.builder.id;
            parsedObject.buyUrl =  utils.prefixToUrl(projectInfo.buyUrl);
            parsedObject.rentUrl = utils.prefixToUrl(projectInfo.rentUrl);
            parsedObject.suburbId = projectInfo.locality && projectInfo.locality.suburb && projectInfo.locality.suburb.id;
            parsedObject.cityId = projectInfo.locality && projectInfo.locality.suburb && projectInfo.locality.suburb.city && projectInfo.locality.suburb.city.id;
            parsedObject.cityName = projectInfo.locality && projectInfo.locality.suburb && projectInfo.locality.suburb.city && projectInfo.locality.suburb.city.label;
        }
        parsedObject.rk = value.property && value.property.rk;
        parsedObject.penthouse = value.property && value.property.penthouse;
        parsedObject.studio = value.property && value.property.studio;
        parsedObject.listingId = value.currentListingPrice && value.currentListingPrice.listingId;
        parsedObject.budget = value.currentListingPrice && value.currentListingPrice.price;
        priceWordObj = value.currentListingPrice && value.currentListingPrice.price && utils.formatNumber(value.currentListingPrice.price, { returnSeperate : true, seperator : req && req.locals && req.locals.numberFormat });
        parsedObject.price = priceWordObj && priceWordObj.val;
        parsedObject.unit = priceWordObj && priceWordObj.unit;
        parsedObject.resaleURL = utils.prefixToUrl(value.resaleURL);
        parsedObject.listingCategory = value.listingCategory;
        parsedObject.mainImageURL = imageParser.appendImageSize(value.mainImageURL,"small");
        parsedObject.altText = value.mainImage && value.mainImage.altText;
        parsedObject.title = value.mainImage && value.mainImage.title;
        parsedObject.propertyType = value.property.unitType;
        parsedObject.cardType = cardType;
        parsedListingArray.push(parsedObject);
    });
        if(cardType == PROJECT_TYPE) {
            parsedListingArray.forEach(function(value){
               (value.sellerId == sellerId) && currentSellerPropertyArray.push(value);  //jshint ignore:line
               (value.sellerId != sellerId) && differentSellerPropertyArray.push(value); //jshint ignore:line
            });

            currentSellerPropertyArray.sort(function(a,b){ 
               return (a.sellerId - b.sellerId);
            });

            differentSellerPropertyArray.sort(function(a,b){ 
               return (a.sellerId - b.sellerId);
            });
            parsedListingArray = differentSellerPropertyArray.concat(currentSellerPropertyArray);
       }
        seeAllUrl = _getSeeAllUrl(cardType,pageData,parsedListingArray); 
        return { 'properties' : parsedListingArray , 'seeAllUrl': seeAllUrl};
}


function parseSimilarPropertyData(response){
    return _parseResponse(undefined, response, 0, undefined, undefined, {});
}

module.exports = {
    getApiListings,
    parseSimilarPropertyData
}

