"use strict";


const apiService = require('services/apiService'),
    apiConfig = require('configs/apiConfig'),
    globalConfig = require('configs/globalConfig'),
    sharedConfig = require('public/scripts/common/sharedConfig'),
    reverseParser = require('services/reverseParseListingSelector'),
    cityService = require('services/cityService'),
    ratingReviewService = require('services/sellersRatingReviewsService'),
    url = require('url');

function _getDisplayAreaLabel(urlData) {
    let displayName = '';

    if (urlData.builderName) {
        displayName = `by ${urlData.builderName} `;
    }

    if (urlData.projectName) {
        let fullName = urlData.builderName ? `${urlData.builderName} ${urlData.projectName}` : `${urlData.projectName}`;
        displayName = `in ${fullName} `;
    }

    if (urlData.sellerName) {
        displayName = `by ${urlData.sellerName} `;
    }

    if(urlData.templateId && urlData.templateId.indexOf('MULTIPLE_LOCALITY')>-1){
        displayName += urlData.localityName ? `in ${urlData.localityName.split(',').join(', ')}` : '';
        displayName += urlData.suburbName ? `${!displayName ? `in`: ', '}${urlData.suburbName.split(',').join(', ')}` : '';
        displayName += ` - ${urlData.cityName}`;
    } else if (urlData.localityName) {
        displayName += `in ${urlData.localityName}`;
        displayName += urlData.cityName ? ` - ${urlData.cityName}` : ``;
    } else if (urlData.suburbName) {
        displayName += `in ${urlData.suburbName}, ${urlData.cityName}`;
    } else if (urlData.cityName) {
        displayName += `in ${urlData.cityName} `;
    }

    return displayName;
}


function _getPropertyTitle(urlData) {
    let propertyName;
    if(urlData.propertyId && urlData.bedrooms && urlData.size) {
        let propertyName = urlData.propertyType !== 'Plot' ? (urlData.bedrooms+ ' BHK ' + urlData.propertyType ) : (`${urlData.propertyType} ${urlData.size||''}`);
        propertyName = (urlData.builderName) ?  propertyName  + " " + urlData.builderName : propertyName;
        propertyName = (urlData.projectName) ?  propertyName  + " " + urlData.projectName : propertyName;
        propertyName = (urlData.localityName) ?  propertyName  + " " +  urlData.localityName : propertyName;
        propertyName = (urlData.cityName) ?  propertyName  + " " +  urlData.cityName : propertyName;
        return propertyName.toLowerCase();
    }
    return propertyName;
}


// tell if page belongs to serp builder, project, city, locality, suburb etc
function _getPageLevel(urlData) {
    let type = 'other',
        pageLevelApiURL;

    if (urlData.templateId && urlData.templateId.indexOf("PROJECTS_SERP_BUY") != -1) {
        if (urlData.templateId.indexOf("MAKAAN_BUILDER") != -1) {
            type = 'builderTaxonomy';
        } else if (urlData.templateId.indexOf("MAKAAN_CITY_BUILDER") != -1) {
            type = 'builderCityTaxonomy';
        } else if (urlData.templateId.indexOf("MAKAAN_COMPANY_URL") != -1) {
            type = 'brokerTaxonomy';
        } else if (urlData.templateId.indexOf("LOCALITY") != -1) {
            type = 'locality';
        } else if (urlData.templateId.indexOf("SUBURB") != -1) {
            type = 'suburb';
        } else if (urlData.templateId.indexOf("CITY") != -1) {
            type = 'city';
        }
    } else if (['MAKAAN_ALL_BUILDERS',
            'MAKAAN_CITY_ALL_BUILDERS',
            'MAKAAN_TOP_BUILDERS',
            'MAKAAN_CITY_TOP_BUILDERS'
        ].indexOf(urlData.templateId) !== -1) {
        switch (urlData.templateId) {
            case "MAKAAN_ALL_BUILDERS":
                type = 'allBuilder';
                break;
            case "MAKAAN_TOP_BUILDERS":
                type = 'topBuilder';
                break;
            case "MAKAAN_CITY_ALL_BUILDERS":
                type = 'cityallBuilder';
                break;
            case "MAKAAN_CITY_TOP_BUILDERS":
                type = 'citytopBuilder';
                break;
        }
    } else if (['MAKAAN_ALL_BROKERS',
            'MAKAAN_STATE_REAL_ESTATE_AGENTS',
            'MAKAAN_CITY_ALL_BROKERS',
            'MAKAAN_LOCALITY_ALL_BROKERS',
            'MAKAAN_SUBURB_ALL_BROKERS',
            'MAKAAN_TOP_BROKERS',
            'MAKAAN_CITY_TOP_BROKERS',
            'MAKAAN_LOCALITY_TOP_BROKERS',
            'MAKAAN_SUBURB_TOP_BROKERS',
            'MAKAAN_CITY_TOP_BROKERS_BUY',
            'MAKAAN_CITY_TOP_BROKERS_RENT',
            'MAKAAN_CITY_ALL_BROKERS_BUY',
            'MAKAAN_CITY_ALL_BROKERS_RENT',
            'MAKAAN_LOCALITY_TOP_BROKERS_BUY',
            'MAKAAN_LOCALITY_TOP_BROKERS_RENT',
            'MAKAAN_LOCALITY_ALL_BROKERS_BUY',
            'MAKAAN_LOCALITY_ALL_BROKERS_RENT',
            'MAKAAN_SUBURB_TOP_BROKERS_BUY',
            'MAKAAN_SUBURB_TOP_BROKERS_RENT',
            'MAKAAN_SUBURB_ALL_BROKERS_BUY',
            'MAKAAN_SUBURB_ALL_BROKERS_RENT'
        ].indexOf(urlData.templateId) !== -1) {
        switch (urlData.templateId) {
            case "MAKAAN_STATE_REAL_ESTATE_AGENTS":
                type = 'stateBroker';
                break;
            case "MAKAAN_CITY_TOP_BROKERS_BUY":
                type = 'citytopBrokerBuy';
                break;
            case "MAKAAN_CITY_TOP_BROKERS_RENT":
                type = 'citytopBrokerRent';
                break;
            case "MAKAAN_CITY_ALL_BROKERS_BUY":
                type = 'cityallBrokerBuy';
                break;
            case "MAKAAN_CITY_ALL_BROKERS_RENT":
                type = 'cityallBrokerRent';
                break;
            case "MAKAAN_LOCALITY_TOP_BROKERS_BUY":
                type = 'localitytopBrokerBuy';
                break;
            case "MAKAAN_LOCALITY_TOP_BROKERS_RENT":
                type = 'localitytopBrokerRent';
                break;
            case "MAKAAN_LOCALITY_ALL_BROKERS_BUY":
                type = 'localityallBrokerBuy';
                break;
            case "MAKAAN_LOCALITY_ALL_BROKERS_RENT":
                type = 'localityallBrokerRent';
                break;
            case "MAKAAN_SUBURB_TOP_BROKERS_BUY":
                type = 'suburbtopBrokerBuy';
                break;
            case "MAKAAN_SUBURB_TOP_BROKERS_RENT":
                type = 'suburbtopBrokerRent';
                break;
            case "MAKAAN_SUBURB_ALL_BROKERS_BUY":
                type = 'suburballBrokerBuy';
                break;
            case "MAKAAN_SUBURB_ALL_BROKERS_RENT":
                type = 'suburballBrokerRent';
                break;
            case "MAKAAN_ALL_BROKERS":
                type = 'allBroker';
                break;
            case "MAKAAN_TOP_BROKERS":
                type = 'topBroker';
                break;
            case "MAKAAN_CITY_ALL_BROKERS":
                type = 'cityallBroker';
                break;
            case "MAKAAN_CITY_TOP_BROKERS":
                type = 'citytopBroker';
                break;
            case "MAKAAN_LOCALITY_ALL_BROKERS":
                type = 'localityallBroker';
                break;
            case "MAKAAN_LOCALITY_TOP_BROKERS":
                type = 'localitytopBroker';
                break;
            case "MAKAAN_SUBURB_ALL_BROKERS":
                type = 'suburballBroker';
                break;
            case "MAKAAN_SUBURB_TOP_BROKERS":
                type = 'suburbtopBroker';
                break;
        }
    } else if (['MAKAAN_CITY_PRICE_TREND_BUY',
            'MAKAAN_CITY_PRICE_TREND_RENT',
            'MAKAAN_SUBURB_PRICE_TREND_BUY',
            'MAKAAN_SUBURB_PRICE_TREND_RENT',
            'MAKAAN_LOCALITY_PRICE_TREND_BUY',
            'MAKAAN_LOCALITY_PRICE_TREND_RENT',
            'MAKAAN_INDIA_PRICE_TREND'
        ].indexOf(urlData.templateId) !== -1) {
        switch (urlData.templateId) {
            case "MAKAAN_CITY_PRICE_TREND_RENT":
            case "MAKAAN_CITY_PRICE_TREND_BUY":
                type = 'cityPriceTrend';
                break;
            case "MAKAAN_SUBURB_PRICE_TREND_BUY":
            case "MAKAAN_SUBURB_PRICE_TREND_RENT":
                type = 'suburbPriceTrend';
                break;
            case "MAKAAN_LOCALITY_PRICE_TREND_BUY":
            case "MAKAAN_LOCALITY_PRICE_TREND_RENT":
                type = 'localityPriceTrend';
                break;
            case "MAKAAN_INDIA_PRICE_TREND":
                type = 'allIndiaPriceTrend';
                break;
        }
    } else if(urlData.templateId && (urlData.templateId.indexOf('LANDMARK') > -1 || urlData.templateId.indexOf('NEARBY') > -1)){
        type = 'nearby';
    } else if (urlData.propertyId && ['MAKAAN_PROPERTY_RENT', 'MAKAAN_PROPERTY_BUY'].indexOf(urlData.templateId) !== -1) {
        type = 'property';
    } else if (urlData.projectId) {
        type = 'project';
        pageLevelApiURL = apiConfig.project({
            projectId: urlData.projectId,
            query: { "selector": JSON.stringify({ "fields": ["projectId", "builder", "name", "overviewUrl", "latitude", "longitude"] }) }
        });
    } else if (urlData.builderId && urlData.templateId !== 'MAKAAN_BUILDER' && urlData.templateId !== 'MAKAAN_CITY_BUILDER') {
        type = 'builderTaxonomy';
    } else if (urlData.builderId && urlData.cityName && urlData.cityName.length) {
        type = 'builderCity';
    } else if (urlData.builderId) {
        type = 'builder';
    } else if (urlData.companyId && urlData.templateId !== 'MAKAAN_COMPANY_URL') {
        type = 'brokerTaxonomy';
    } else if (urlData.companyId) {
        type = 'broker';
    } else if(urlData.localityOrSuburbId && urlData.templateId && (urlData.templateId.indexOf('MULTIPLE_LOCALITY') > -1)){
        type = 'multipleSuburbLocality';
    } else if (urlData.localityId && urlData.templateId && (urlData.templateId.indexOf('LISTING_BUY') < 0 && urlData.templateId.indexOf('LISTING_RENT') < 0) && (urlData.templateId.indexOf('OVERVIEW_BUY') < 0 && urlData.templateId.indexOf('OVERVIEW_RENT') < 0)) {
        type = 'localityTaxonomy';
        pageLevelApiURL = apiConfig.localityDetailsById({
            localityId: urlData.localityId,
            selector: JSON.stringify({ "fields": ["localityId", "label", "overviewUrl", "latitude", "longitude"] })
        });
    } else if (urlData.localityId) {
        type = 'locality';
        pageLevelApiURL = apiConfig.localityDetailsById({
            localityId: urlData.localityId,
            selector: JSON.stringify({ "fields": ["localityId", "label", "overviewUrl", "latitude", "longitude"] })
        });
    } else if (urlData.suburbId && urlData.templateId && (urlData.templateId.indexOf('LISTING_BUY') < 0 && urlData.templateId.indexOf('LISTING_RENT') < 0) && (urlData.templateId.indexOf('OVERVIEW_BUY') < 0 && urlData.templateId.indexOf('OVERVIEW_RENT') < 0)) {
        type = 'suburbTaxonomy';
    } else if (urlData.suburbId) {
        type = 'suburb';
    }  else if (urlData.cityId) {
        type = 'city';
        pageLevelApiURL = apiConfig.cityOverview({
            cityId: urlData.cityId,
            selector: JSON.stringify({ "fields": ["id", "label", "overviewUrl", "centerLatitude", "centerLongitude"] })
        });
    } else if (urlData.stateId) {
        type = 'state';
    }
    urlData.pageLevelApiURL = pageLevelApiURL;
    return type;
}

module.exports.getUrlData = function(pageType, urlDetail, req, isMapUrl ) {
    let query = req.query,
        isAmp = req.isAmp,
        urlData = {
            listingType: 'buy',
            propertyType: undefined,
            pageType: pageType,
            countryDisplayName: urlDetail.countryDisplayName && urlDetail.countryDisplayName.toLowerCase() || '',
            stateName: urlDetail.stateName || query.stateName,
            cityName: urlDetail.cityName || query.cityName,
            localityName: urlDetail.localityName || query.localityName,
            suburbName: urlDetail.suburbName || query.suburbName,
            builderName: urlDetail.builderName || query.builderName,
            companyName: urlDetail.companyName || query.companyName,
            projectName: urlDetail.projectName || query.projectName,
            placeName: urlDetail.placeName || query.placeName,
            nearbyName : urlDetail.placeName || query.placeName,
            nearbyId : urlDetail.landMarkId,
            countryId: urlDetail.countryId,
            stateId: urlDetail.stateId,
            cityId: urlDetail.cityId,
            builderId: urlDetail.builderId,
            companyId: urlDetail.companyId,
            localityId: urlDetail.localityId,
            suburbId: urlDetail.suburbId,
            projectId: urlDetail.projectId,
            propertyId: urlDetail.propertyId,
            listingId: urlDetail.listingId,
            url: urlDetail.url,
            trackingKeyword: urlDetail.trackingKeyword,
            templateId: urlDetail.templateId || query.templateId,
            isCommercial: urlDetail.isCommercial || false,
            placeId: undefined,
            latitude: undefined,
            longitude: undefined,
            displayAreaLabel: undefined,
            filters: {}
        };

    switch (pageType) {
        case 'SELLER_PROPERTY_URLS':
        case 'SELLER_PROPERTY_URLS_MAPS':
        case 'SIMILAR_PROPERTY_URLS':
        case 'SIMILAR_PROPERTY_URLS_MAPS':
        case 'LISTINGS_PROPERTY_URLS':
        case 'LISTINGS_PROPERTY_URLS_MAPS':
        case 'PROJECT_SERP_DYNAMIC_URLS':
            urlData.countryId = query.countryId ? parseInt(query.countryId) : null;
            urlData.stateId = query.stateId ? parseInt(query.stateId) : null;
            urlData.cityId = query.cityId ? parseInt(query.cityId) : null;
            urlData.builderId = query.builderId ? parseInt(query.builderId) : null;
            urlData.companyId = query.companyId ? parseInt(query.companyId) : null;
            urlData.nearbyId = query.nearbyId ? parseInt(query.nearbyId) : null;
            urlData.localityId = query.localityId ? query.localityId : null;
            urlData.localityOrSuburbId = query.localityOrSuburbId || null;
            urlData.suburbId = query.suburbId ? query.suburbId : null;
            urlData.projectId = query.projectId ? parseInt(query.projectId) : null;
            urlData.listingId = urlDetail.listingId || (query.listingId ? parseInt(query.listingId) : null);
            urlData.sellerId = query.sellerId ? query.sellerId.split(',') : null;
            urlData.sellerUserId = query.sellerUserId ? query.sellerUserId : null;
            urlData.placeId = query.placeId;
            urlData.listingType = query.listingType || 'buy';
            urlData.sellerName = query.sellerName;
            if (req.query.pageType === "MULTIPLE_SEARCH_URLS") {
                urlData.subPageType = 'MULTIPLE_SEARCH_URLS';
                urlData.localityName = req.query.localityOrSuburbName || urlData.localityName;
            }
            urlData.dummySeoText = true;
            break;
        case 'PROPERTY_URLS':
            urlData.size =  urlDetail.size;
            urlData.propertyName = _getPropertyTitle(urlDetail);
            break;

        default:
            break;

    }
    urlData.websiteUrl = process.env.WEBSITE_URL || 'http://mp-qa2.makaan-ws.com/';
    if (urlData.websiteUrl.charAt(urlData.websiteUrl.length - 1) === '/') {
        urlData.websiteUrl = urlData.websiteUrl.slice(0,-1);
    }
    urlData[globalConfig.vernacHeader] = (req.headers[globalConfig.vernacHeader] || '').toLowerCase();
    urlData.language = globalConfig.vernacURI[urlData[globalConfig.vernacHeader]];
    
    (function(){
        var currUrl = url.parse(req.url, true);
        urlData.relativeUrl = currUrl.pathname + (currUrl.query && currUrl.query.page > 1 ? '?page=' + currUrl.query.page : '');
        // Respect Canonical from SEO API if not amp, otherwise keep currentUrl without /lite as canonical 
        if (isAmp) {
            urlData.currBaseUrl = urlData.canonicalUrl = url.resolve(urlData.websiteUrl, urlData.relativeUrl.replace(/(\/lite)([\/?]?)/g, '$2'));
        } else {
            urlData.canonicalUrl = url.resolve(urlData.websiteUrl, (urlDetail.canonicalUrl || urlData.relativeUrl) );
            urlData.currBaseUrl = url.resolve(urlData.websiteUrl, urlData.relativeUrl);
        }
        urlData.currBaseUrl = urlData.currBaseUrl.replace(/(\?.*)|(#.*)/g, "");

        var reverseParsedData = reverseParser.reverseParseSelector(urlDetail.selector || urlDetail.listingFilter, {sort:true});
        urlData.filters = reverseParsedData.filters;
        urlData.sortBy = reverseParsedData.sortBy;
    })();

    urlData.pageLevel = _getPageLevel(urlData);
    // if templateId has rent word that mean url belongs to rent urls and rent/nearby means nearby url for rent
    if ((urlDetail.templateId && urlDetail.templateId.indexOf('RENT') > -1) || (req.path && req.path.indexOf('rent/nearby/') > -1)) {
        urlData.listingType = 'rent';
    }

    if (req.path && req.path.indexOf('commercialBuy/nearby/') > -1) {
        urlData.listingType = 'commercialBuy';
    }

    if (req.path && req.path.indexOf('commercialLease/nearby/') > -1) {
        urlData.listingType = 'commercialLease';
    }

    if(urlDetail.templateId && urlDetail.templateId.indexOf('COMMERCIAL') > -1) {
        urlData.isCommercial = true;
        urlData.listingType = (urlData.listingType === 'buy') ? 'commercialBuy' : 'commercialLease';
    }

    urlData.promises = [];
    urlData.promisesData = {};

    function _builderCardHandling() {
        let builderId = urlData.builderId || req.query.builderId,
            selector = { "fields": ["id", "name", "description", "imageURL", "isDescriptionCrawlable", "projectStatusCount", "establishedDate", "percentageCompletionOnTime","sellerType"] },
            query = { selector: JSON.stringify(selector) };
        urlData.promises.push(apiService.get(apiConfig.builderDetail({ builderId, query: query }), { req }).then((response) => {
            urlData.promisesData.builder = response.data || {};
        }));
    }

    function _sellerCardHandling(listingCompanyId, listingCompanyUserId) {
        let companyId = listingCompanyId,
            sellerSelector = { "fields": [  "id", "name", "address", "score", "activeSince", "logo", "coverPicture", "micrositeEnabled", "micrositeUrl", "companyDescription", "sellers", "cities", "label", "companyUser", "user", "profilePictureURL", "sellerListingData", "localityCount", "projectCount", "categoryWiseCount","company","stateRegistrations","stateId","companyId","registrationNumber","feedbackCount","callRatingCount","callRatingMap","paidLeadCount","paidLeadCountMap", "averageResponseTimeInHours","sellerType","sellerMiscellaneousProducts", "sellerHighestBadge"] };
        urlData.promises.push(apiService.get(apiConfig.sellerDetailById({ companyId, query: { selector: JSON.stringify(sellerSelector) } }), { req }).then((response) => {
            urlData.promisesData.seller = response.data || {};
        }));
        if(listingCompanyUserId) {
            urlData.promises.push(ratingReviewService.getSellerReviews(listingCompanyUserId, {req}).then((response) => {
                urlData.promisesData.sellerReviews = response || {};
            }).catch(error => {
                urlData.promisesData.sellerReviews = {};
            }));
        } else {
            urlData.promises.push(ratingReviewService.getSellerReviewsByComapnyId(companyId, {req}).then((response) => {
                urlData.promisesData.sellerReviews = response || {};
            }).catch(error => {
                urlData.promisesData.sellerReviews = {};
            }));
        } 
    }


    if ((['NEARBY_LISTING_URLS','NEARBY_URLS', 'NEARBY_LISTING_TAXONOMY_URLS', 'NEARBY_LISTING_URLS_MAPS', 'NEARBY_URLS_MAPS', 'NEARBY_LISTING_TAXONOMY_URLS_MAPS'].indexOf(pageType) > -1) || (req.query.placeId && ['NEARBY_LISTING_URLS', 'NEARBY_URLS','NEARBY_LISTING_TAXONOMY_URLS', 'NEARBY_LISTING_URLS_MAPS', 'NEARBY_URLS_MAPS','NEARBY_LISTING_TAXONOMY_URLS_MAPS'].indexOf(req.query.pageType) > -1)) {
        urlData.placeId = urlDetail.placeId || req.query.placeId;
        urlData.placeName = urlDetail.placeName;
        let apiUrl = apiConfig.gpKeyDetail({ googlePlaceKey: urlData.placeId });
        urlData.promises.push(apiService.get(apiUrl, { req }).then((response) => {
            urlData.promisesData.landmark = response.data || {};
            urlData.latitude = response.data.latitude;
            urlData.longitude = response.data.longitude;
            urlData.placeName = response.data.placeName;
            urlData.displayAreaLabel = 'near ' + urlData.placeName;
            return response;
        }));
    } else if (!req.query.projectId && ((['BUILDER_TAXONOMY_URLS', 'BUILDER_TAXONOMY_URLS_MAPS','BUILDER_URLS','BUILDER_URLS_MAPS'].indexOf(pageType) > -1) || (req.query.builderId && ['BUILDER_TAXONOMY_URLS', 'BUILDER_TAXONOMY_URLS_MAPS', 'LISTINGS_PROPERTY_URLS', 'LISTINGS_PROPERTY_URLS_MAPS','BUILDER_URLS','BUILDER_URLS_MAPS'].indexOf(req.query.pageType) > -1))) {
        _builderCardHandling();
    } else if (!req.query.projectId && ((['COMPANY_TAXONOMY_URLS', 'COMPANY_TAXONOMY_URLS_MAPS','COMPANY_URLS','COMPANY_URLS_MAPS'].indexOf(pageType) > -1) || (req.query.companyId && ['COMPANY_TAXONOMY_URLS', 'COMPANY_TAXONOMY_URLS_MAPS', 'LISTINGS_PROPERTY_URLS', 'LISTINGS_PROPERTY_URLS_MAPS','COMPANY_URLS','COMPANY_URLS_MAPS'].indexOf(req.query.pageType) > -1))) {
        _sellerCardHandling(urlData.companyId || req.query.companyId, null);
    // generic case of /seller-property page which serves both broker and builder results
    } else if ((['SELLER_PROPERTY_URLS', 'SELLER_PROPERTY_URLS_MAPS'].indexOf(pageType) > -1) || (urlData.sellerId && ['SELLER_PROPERTY_URLS', 'SELLER_PROPERTY_URLS_MAPS', 'LISTINGS_PROPERTY_URLS', 'LISTINGS_PROPERTY_URLS_MAPS'].indexOf(req.query.pageType) > -1)) {
        if (urlData.sellerId && urlData.sellerId.length == 1) {
            _sellerCardHandling(urlData.sellerId[0], urlData.sellerUserId);
        } else if (!urlData.sellerId && req.query.builderId && !req.query.projectId) {
            _builderCardHandling();
        }
    }

    //to fetch builder detail for builder serp
    if ((urlData.pageType == 'PROJECT_SERP' || urlData.pageType == 'PROJECT_SERP_DYNAMIC_URLS') && urlData.templateId && urlData.templateId.indexOf('BUILDER') != -1) {
        _builderCardHandling();
    }

    // to show maps boundary for locality
    if (isMapUrl && globalConfig.enableBoundaryOnMap && urlData.localityId) {
        let selector = JSON.stringify({ "fields": ["encodedPolygon"] });
        urlData.promises.push(apiService.get(apiConfig.localityDetailsById({ localityId: urlData.localityId, selector })).then((response) => {
            urlData.promisesData.boundaryEncodedPolygon = response.data && response.data.encodedPolygon ? response.data.encodedPolygon : null;
        }));
    }

    if (urlDetail.cityName && !urlDetail.cityId) {
        urlData.cityId = cityService.getCityIdByName(urlDetail.cityName);
    } else if (!urlDetail.cityName && urlDetail.cityId) {
        urlData.cityName = cityService.getCityNameById(urlDetail.cityId);
    }

    urlData.displayAreaLabel = urlData.displayAreaLabel || _getDisplayAreaLabel(urlData);

    return urlData;
};