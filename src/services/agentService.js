"use strict";

const apiService = require('services/apiService'),
    utils = require('services/utilService'),
    apiConfig = require('configs/apiConfig'),
    cityService = require('services/cityService'),
    imageParser = require('services/imageParser'),
    mappingService = require('services/mappingService'),
    sharedConfig = require('public/scripts/common/sharedConfig'),
    dealStatusMapping = require('services/mappingService').dealStatusMapping,
    POSTED_BY_BROKER = "BROKER";

var agentService = {};
let sellerFields = ["id","name","score","logo","micrositeEnabled","micrositeUrl","sellers","companyUser","contactNumber","user","username","profilePictureURL","contactNumbers","feedbackCount","callRatingCount","callRatingMap","paidLeadCount","paidLeadCountMap","sellerType","averageResponseTimeInHours", "sellerHighestBadge"];
let paidStatuses = ["ExpertDealMaker","DealMaker","PrepaidSeller"];

agentService.getTopAgents = function(cityId, options = {}, { req, isMobile },returnPaidUnpaid=false) {  //jshint ignore:line
    let selectorStr = options.selector ? apiService.createSelector(options.selector) : '{}',
        api,
        query = {
            selector: selectorStr
        };

    if(options.limit){
        query.limit = options.limit;
    }

    if(options.contactNumber) {
        query.contactNumber = options.contactNumber;
    }
    if (options.utmSource) {
        query.utm_source = options.utmSource;
    }
    if (options.utmMedium) {
        query.utm_medium = options.utmMedium;
    }
    if (options.giveBoostToExpertDealMakers){
        query.giveBoostToExpertDealMakers = options.giveBoostToExpertDealMakers;
    }
    if(options.relaxProjectFilter){
        query.relaxProjectFilter = options.relaxProjectFilter;
    }

    if (cityId) {
        api = apiConfig.topAgents({
            cityId,
            query: query
        });
    } else {
        api = apiConfig.topAgentsAllIndia({
            query: query
        });
    }
    return apiService.get(api, { req }).then(function(response) {
        if (options.getTotalCount) {
            return {
                data: _parseTopAgents(response,isMobile,returnPaidUnpaid),
                totalCount: response.totalCount
            };
        } else {
            return _parseTopAgents(response,isMobile,returnPaidUnpaid);
        }
    }, err => {
        throw err;
    });
};

agentService.getAgents = function(config, { req }) {
    let selectorStr = config.selector ? apiService.createSelector(config.selector) : '{}',
        api;

    api = apiConfig.getAgents({
        query: {
            selector: selectorStr
        }
    });

    return apiService.get(api, { req }).then(function(response) {
        return {
            data: _parseTopAgents(response),
            totalCount: response.totalCount
        };
    }, err => {
        throw err;
    });
};

agentService.getSellerDetailsByCompanyId = function(companyId, {req}) {
    let sellerSelector = {
        "fields": sellerFields
    };
    let url = apiConfig.sellerDetailById({
        companyId,
        query: {
            selector: JSON.stringify(sellerSelector)
        }
    }).url;
    return apiService.get(url, {req}).then(function(response) {
        return [agentService.parseCompanyData(response.data , {req})];
    }, err => {
        throw err;
    });
};
agentService.getSellerDetailsByCompanyList = function(companyIdList, {req}) {
    let sellerSelector = {
        "fields": sellerFields,
        "paging": {
            "rows": companyIdList.length
        },
        "filters": {"and":[{
            "equal":{
                "companyId":companyIdList
            }}]}
    };
    let url = apiConfig.sellerDetails({
        selector: JSON.stringify(sellerSelector)
    }).url;
    return apiService.get(url, {req}).then(function(response) {
        return response;
    }, err => {
        throw err;
    });
};
agentService.getSellerDetailsByUserId = function(sellerId, {req}) {
    let url = apiConfig.sellerDetailByUserId({sellerId}).url;
    return apiService.get(url, {req}).then(function(response) {
        return response.data;
    }, err => {
        throw err;
    });
};
agentService.getMultipleFeaturedSellers = function(sellerIds,{ req , isMobile }){
    let selectorObj = {
        filters: [],
    }, selector;
    selectorObj.filters.push({
        key:"sellerUserId",
        value:sellerIds,
    });
    selector = apiService.createSelector(selectorObj);
    let url = apiConfig.sellerDetails({selector}).url; 

    return apiService.get(url, {req}).then(function(response) {
        return _parseDeveloperSeller(response,isMobile);
    }, err => {
        throw err;
    });
};


agentService.getFeaturedTopAgent=function (projectId, { req , isMobile }){
    return _fetchProjectSellerIds(projectId, {req}).then(function(response) {
        if(response.data && response.data.length) {
            let projectUserIds = _extractDeveloperAdSellerId(response.data);//function call to extract sellerIds and builderIds from the above api response  
            return agentService.getMultipleFeaturedSellers(projectUserIds.sellerIds,{ req , isMobile });
        } else {
            return [];
        }
    },(err)=> {
            throw err;
    });
};
//function to get sellerIds for the given project using jean get api
function _fetchProjectSellerIds(projectId, {req} = {}){
    let query ={
        promotedType:'PROJECT',
        productEntityIds:projectId,
        saleType:'BUY',
        status:'Active'
    };
    let urlObj = apiConfig.developerCampaigns({ query});

    return apiService.get(urlObj, {req});  
}

//function to extract sellerIds from the jean get Api response

function _extractDeveloperAdSellerId(data){
    let projectUserIds={sellerIds:[]};
    data.forEach(function(element) {
        let productDetails = element.productDetails ||[];
        productDetails.forEach(function(product) {
            let scopeDetails = product.scopeDetails ||[];
            scopeDetails.forEach(function(scope) {
                let userScopeDetails = scope.userScopeDetails ||[];
                userScopeDetails.forEach(function(scopeInfo) {
                    projectUserIds.sellerIds.push(scopeInfo.userId);
                });
            });
        });
    });
    return projectUserIds;
}

//parsing sellerInfo from the company API response

function _parseDeveloperSeller (response,isMobile) {

    if (!(response && response.data)) {
        return [];
    }
    let imageSize = 'squareTile';
    if(isMobile){
        imageSize = 'smallThumbnail';
    }
    response = response.data;
    let result = [],
        i, length = response.length;
    for (i = 0; i < length; i++) {
        let company = response[i]||{},
            seller = company && company.sellers && company.sellers.length && company.sellers[0] ||{},
            user = seller && seller.companyUser && seller.companyUser.user || {},
            temp = {};

        temp.id = company.id;
        temp.name = company.name;
        temp.properties = seller.listingCount;
        temp.rating = company.score;
        temp.ratingBadge = utils.roundNumber(temp.rating,1);
        temp.ratingBadgeClass = utils.getRatingBadgeClass(temp.rating);
        temp.ratingClass = utils.getRatingClass(temp.rating);
        temp.sellerUrl = utils.prefixToUrl(company.micrositeUrl);
        temp.assist = company.assist ? true : false;
        temp.dealMaker = company.paidLeadCount ? true : false;
        temp.paidLeadCount = company.paidLeadCount || 0;
        temp.image = imageParser.appendImageSize(company.logo || user.profilePictureURL, imageSize);
        temp.userId = user.id;
        temp.type = seller.sellerType;
        temp.avatarDetails = utils.getAvatar(company.name);
        temp.avatar = temp.avatarDetails;
        temp.contact = (user.contactNumbers && user.contactNumbers[0] && user.contactNumbers[0].contactNumber) || 'n.a.';
        temp.listingCount = seller.listingCount;
        temp.listingCountBuy = seller.buyListingCount;
        temp.listingCountRent = seller.rentListingCount;
        temp.localities = company.localities;   //field not in company API
        temp.cityId = company.addressCityId;
        temp.cityName = cityService.getCityNameById(temp.cityId) || '';
        temp.unitTypes = company.unitTypes;  //field not in company API
        temp.address = company.address || 'Currently not Available'; // field not in acompany API
        temp.sellerStatuses = seller.sellerHighestBadge && [seller.sellerHighestBadge] || []; 
        temp.expertStatus = getSellerStatusType(temp.sellerStatuses) || {};
        temp.sellerType = temp.expertStatus.label;
        temp.sellerCallRatingCount = company.sellerCallRatingCount || 0;
        temp.sellerFeedbackCount = company.sellerFeedbackCount || 0;
        temp.isFeaturedSeller = true;

        result.push(temp);
    }
    return result;
}

function _parseResponseTimeText(time){
    if(time < 1){
        return'Responds immediately';
    }
    else if(time < 12){
        return 'Responds within a few hours';
    }
    else if(time <= 24 ){
        return 'Responds within a day';
    }
    else if(time < 168){
        // 7 days = 168 hrs
        return 'Responds within a few days';
    }
    else {
        return '';
    }
}

agentService.parseCompanyData = function(companyData, {req}) {

    companyData = companyData || {};

    let company = {},
        seller = companyData.sellers ? companyData.sellers[0] : {},
        sellerListingData = seller.companyUser && seller.companyUser.sellerListingData ? seller.companyUser.sellerListingData : {},
        user = seller.companyUser && seller.companyUser.user  ? seller.companyUser.user : {},
        userProfilePictureURL = user.profilePictureURL;

    company.id = companyData.id;
    company.name = companyData.name;
    company.logo = company.image = companyData.logo || userProfilePictureURL;
    company.assist = companyData.assist || false;
    company.coverPicture = companyData.coverPicture;
    company.coverPictureSize = companyData.coverPicture ? imageParser.getImageSizeString('large') : '';
    company.micrositeEnabled = companyData.micrositeEnabled ? true : false;
    company.micrositeUrl = company.sellerUrl = utils.prefixToUrl(companyData.micrositeUrl);
    company.fullDescription = companyData.companyDescription;
    if (!company.coverPicture) {
        let randomImageIndex = (company.id % 4) + 1;
        company.coverPictureIndex = randomImageIndex;
        // company.coverPicture = `/images/seller-heroshot/img_${randomImageIndex}.jpg`;
    }
    
    company.isDealMaker = dealStatusMapping.Normal.indexOf(seller.sellerHighestBadge) > -1;
    company.isExpertDealMaker = dealStatusMapping.Expert.indexOf(seller.sellerHighestBadge) > -1;
    company.highestBadge = sharedConfig.listingSellerTransactionMapping[seller.sellerHighestBadge];

    company.address = companyData.address;
    //company.rating = companyData.score ? Math.round(companyData.score * 10) / (10 * 2) : 0;
    company.rating = companyData.score ? companyData.score : 0;
    company.ratingClass = utils.getRatingClass(company.rating);
    company.ratingBadge = utils.roundNumber(company.rating,1);
    company.ratingBadgeClass = utils.getRatingBadgeClass(company.rating);
    company.age = companyData.activeSince ? utils.timeFromDate(companyData.activeSince) + ' years' : undefined;
    company.projectCount = utils.formatNumber(sellerListingData.projectCount, { type : 'number', seperator : req && req.locals && req.locals.numberFormat });
    let avatarDetails = utils.getAvatar(company.name);
    company.avatar = avatarDetails;
    company.backgroundColor = avatarDetails.backgroundColor;
    company.nameText = avatarDetails.text;
    company.textColor = avatarDetails.textColor;
    company.contact = (user.contactNumbers && user.contactNumbers[0] && user.contactNumbers[0].contactNumber) || 'n.a.';
    company.userId = user.id;
    company.averageResponseTimeText = _parseResponseTimeText(companyData.averageResponseTimeInHours);

    if(companyData.stateRegistrations && companyData.stateRegistrations.length){
        company.stateRegistrations =  companyData.stateRegistrations;
    }

    let listingCount = 0,
        listingCategoryType = {},
        categoryWiseCount = sellerListingData.categoryWiseCount || [];

    for (let i = 0; i < categoryWiseCount.length; i++) {
        listingCount += categoryWiseCount[i].listingCount;
        listingCategoryType[categoryWiseCount[i].listingCategoryType] = categoryWiseCount[i].listingCount;
    }

    company.localitiesCount = utils.formatNumber(sellerListingData.localityCount, { type : 'number', seperator : req && req.locals && req.locals.numberFormat});
    let citiesCount = seller.cities ? seller.cities.length : 0,
        cityName = seller.cities && seller.cities[0] ? seller.cities[0].label : null;

    if (citiesCount) {
        company.cityCountText = citiesCount == 1 ? `${cityName}` : `${citiesCount} cities`;
    }

    company.areaOfService = Object.keys(listingCategoryType).join(' & ');
    company.listingCount = utils.formatNumber(listingCount, { type : 'number', seperator : req && req.locals && req.locals.numberFormat });
    company.categoryWiseCount = listingCategoryType;
    company.categoryWiseCount.buy=0;
    if(listingCategoryType.Primary>0){
        company.categoryWiseCount.buy = listingCategoryType.Primary;
    }
    if(listingCategoryType.Resale>0){
        company.categoryWiseCount.buy += listingCategoryType.Resale;
    }
    company.feedbackCount = companyData.feedbackCount || 0;
    company.callRatingCount = companyData.callRatingCount;
    company.paidLeadCount = companyData.paidLeadCount;
    company.sellerType = (seller.sellerType || '').toUpperCase();
    company.isMakaanSelectSeller = seller.sellerMiscellaneousProducts && seller.sellerMiscellaneousProducts.indexOf('MakaanSelect') > -1;
    if(company.sellerType == POSTED_BY_BROKER){
        company.sellerType = 'AGENT';
    }
    return company;
};

agentService.getContactNumber = function(userId,cityId,listingType="Sell", {req}){  //jshint  ignore:line

    if(!userId || !cityId) {
        return new Promise((resolve, reject)=> {
            let message = "userId or cityId not present";
            reject((message));
        });
    }

    if(utils._.isArray(userId)){
        userId.join(',');
    }
    listingType = utils.toTitleCase(listingType);
    return apiService.get(apiConfig.getVirtualNumber({userId,cityId,listingType}),{req}).then((response)=>{
        response = response && response.data;
        let result = [];
        utils._.forEach(response,(v)=>{
            let contact,
                isVirtualNumber = true;
            if(v.virtualNumber && v.virtualNumber.number){
                contact = v.virtualNumber.number;
            }else if(v.userPrimaryContactNumber){
                contact = v.userPrimaryContactNumber;
                isVirtualNumber = false;
            }
            result.push({contact, userId: v.userId,isVirtualNumber: isVirtualNumber});
        });
        return result;
    },(err)=>{
        throw err;
    });
};

agentService.getSellerProfilePic = function(sellerUserId){
    return apiService.get(apiConfig.sellerImages(sellerUserId)).then((response) => {
        if (response && response.data && response.data[0]) {
            return response.data[0].absolutePath;
        }
        return {};
    }, (err) => {
        throw err;
    });
};

agentService.getRawSeller = function(sellerUserId) {
    return apiService.get(apiConfig.rawSellerDetail({ sellerUserId })).then((response) => {
        if (response && response.data && response.data[0]) {
            return response.data[0];
        }
        return {};
    }, (err) => {
        throw err;
    });
};

agentService.getAgentLocalities = function(sellers, {selector}, {req}){
    let agentIds = [];
    selector = apiService.createSelector(selector) || "{}";
    sellers.forEach(item => {
        if(item.userId){
            agentIds.push(item.userId);
        }
    });
    let cityId = sellers[0].cityId;
    agentIds = agentIds.join(",");
    let api = apiConfig.getAgentLocalities({cityId, query: {agentIds, selector}});
    return apiService.get(api, {req}).then((response) => {
        return _parseAgentLocalities(response.data);
    });
};

function _parseAgentLocalities(data){
    let finalObj = {};
    for(var key in data){
        let localitiesArray = data[key];
        finalObj[key] = localitiesArray.map(locality => {
            return {
                localityId: locality.localityId,
                label: locality.label,
                mainImage: locality.localityHeroshotImageUrl
            };
        });
    }
    return finalObj;
}

function _trimBhks (arr) {
    var index = arr.indexOf(4);
    if(index != -1) {
        arr[index] = '3+';
        arr = arr.slice(0,index+1);
    }
    return arr;
}


function getSellerStatusType(sellerStatuses){
    let sellerType;
    sellerStatuses = sellerStatuses || [];
    if(sellerStatuses.indexOf(mappingService.dealMakersMapping.LOCALITY_EXPERT) > -1 || 
        sellerStatuses.indexOf(mappingService.dealMakersMapping.SUBURB_EXPERT) > -1){
        sellerType = mappingService.listingSellerTransactionMapping[mappingService.dealMakersMapping.LOCALITY_EXPERT];
    }else if (sellerStatuses.indexOf(mappingService.dealMakersMapping.CITY_EXPERT) > -1) {
        sellerType = mappingService.listingSellerTransactionMapping[mappingService.dealMakersMapping.CITY_EXPERT];
    }else if (sellerStatuses.indexOf(mappingService.dealMakersMapping.EXPERT) > -1) {
        sellerType = mappingService.listingSellerTransactionMapping[mappingService.dealMakersMapping.EXPERT];
    }else if (sellerStatuses.indexOf(mappingService.dealMakersMapping.DEAL_MAKER) > -1) {
        sellerType = mappingService.listingSellerTransactionMapping[mappingService.dealMakersMapping.DEAL_MAKER];
    }
    return sellerType;
}
function checkIfPaid(sellerStatuses){
    let paid = false;
    sellerStatuses && sellerStatuses.forEach(function(status){
        if(paidStatuses.indexOf(status) > -1){
            paid = true;
        }
    });
    return paid;
}

function _parseTopAgents(response,isMobile=false,returnPaidUnpaid=false) {
    if (!(response && response.data)) {
        return [];
    }
    let imageSize = 'squareTile';
    if(isMobile){
        imageSize = 'smallThumbnail';
    }
    response = response.data;
    let result = [],
        i, length = response.length;
    let paidUnpaid={
        paid:[],
        unpaid:[]
    };
    for (i = 0; i < length; i++) {
        let agent = response[i].agent || {},
            company = agent.company || {},
            user = agent.user || {},
            temp = {};

        temp.id = company.id;
        temp.name = company.name;
        temp.properties = response[i].listingCount;
        temp.rating = company.score;
        temp.ratingBadge = utils.roundNumber(temp.rating,1);
        temp.ratingBadgeClass = utils.getRatingBadgeClass(temp.rating);
        temp.ratingClass = utils.getRatingClass(temp.rating);
        temp.sellerUrl = utils.prefixToUrl(company.micrositeUrl);
        temp.assist = company.assist ? true : false;
        temp.dealMaker = company.paidLeadCount ? true : false;
        temp.paidLeadCount = company.paidLeadCount || 0;
        temp.image = imageParser.appendImageSize(company.logo || user.profilePictureURL, imageSize);
        temp.userId = user.id;
        temp.type = company.type;
        temp.avatarDetails = utils.getAvatar(company.name);
        temp.avatar = temp.avatarDetails;
        temp.contact = (user.contactNumbers && user.contactNumbers[0] && user.contactNumbers[0].contactNumber) || 'n.a.';
        temp.listingCount = response[i].listingCount;
        temp.listingCountBuy = response[i].buyListingCount;
        temp.listingCountRent = response[i].rentListingCount;
        temp.localities = response[i].localities;
        temp.cityId = response[i].cityId;
        temp.cityName = cityService.getCityNameById(temp.cityId) || '';
        temp.unitTypes = response[i].unitTypes;
        temp.address = response[i].address || 'Currently not Available';
        temp.isRelaxed = response[i].isRelaxedRequirementForAgent;
        temp.sellerBhk = response[i].bhk && _trimBhks(response[i].bhk.sort());
        temp.sellerStatuses = response[i].sellerTransactionStatuses;
        temp.expertStatus = getSellerStatusType(temp.sellerStatuses) || {};
        temp.sellerType = temp.expertStatus.label;
        temp.sellerCallRatingCount = response[i].sellerCallRatingCount || 0;
        temp.sellerFeedbackCount = response[i].sellerFeedbackCount || 0;
        temp.isPaid = checkIfPaid(temp.sellerStatuses);
        temp.localityId = response[i].localityId;
        temp.suburbId = response[i].suburbId;

        if(temp.isPaid){
            paidUnpaid.paid.push(temp);
        }else{
            paidUnpaid.unpaid.push(temp);
        }

        result.push(temp);
    }
    if(returnPaidUnpaid){
        return paidUnpaid;
    }
    if(result){
        result.sort(function(topAgent1,topAgent2){
            return topAgent2.rating - topAgent1.rating;
        });
    }
    return result;
}

agentService.parseAgentsForTopSellerStatus = function(topSellers, {hideSpecificExperts, localityOrSuburbIds, expertFilter} = {}, localities = {}){
    let dealMakerCount = 0,
        expertDealMakerCount = 0,
        cityExpertCount = 0,
        localityExpertCount = 0;
    topSellers = topSellers || [];

    let expertSellers = [],
        cityExpert = [],
        localityExpert = [],
        dealMakers = [],
        isSpecificExpert = false,
        otherSellers = [];
    if(localityOrSuburbIds && expertFilter.indexOf(mappingService.dealMakersMapping.SUBURB_EXPERT) > -1){
        localityOrSuburbIds = localityOrSuburbIds.split(',').map(id => parseInt(id, 10));
        topSellers = topSellers.filter(seller => {
            if((localityOrSuburbIds.indexOf(seller.suburbId) > -1) && (localityOrSuburbIds.indexOf(seller.localityId) == -1) && seller.sellerStatuses.indexOf(mappingService.dealMakersMapping.SUBURB_EXPERT) == -1){
                return false;
            } else {
                return true;
            }
        });
    }

    topSellers.forEach(seller => {
        delete(seller.dealMaker); //is an ancient field, not in use anymore
        seller.sellerStatuses = seller.sellerStatuses || [];
        seller.localities = localities[seller.userId];
        if(!hideSpecificExperts && seller.sellerStatuses.indexOf(mappingService.dealMakersMapping.CITY_EXPERT) > -1){
            seller.isCityExpert = true;
            seller.sellerType = mappingService.dealMakersMapping.CITY_EXPERT;
            isSpecificExpert = true;
            cityExpert.push(seller);
            cityExpertCount++;
        } else if(!hideSpecificExperts && (seller.sellerStatuses.indexOf(mappingService.dealMakersMapping.LOCALITY_EXPERT) > -1 || seller.sellerStatuses.indexOf(mappingService.dealMakersMapping.SUBURB_EXPERT) > -1)){
            seller.isLocalityExpert = true;
            seller.sellerType = mappingService.dealMakersMapping.LOCALITY_EXPERT;
            isSpecificExpert = true;
            localityExpert.push(seller);
            localityExpertCount++;
        } else if(seller.sellerStatuses.indexOf(mappingService.dealMakersMapping.EXPERT) > -1){
            seller.isExpertDealMaker = true;
            seller.sellerType = mappingService.dealMakersMapping.EXPERT;
            expertSellers.push(seller);
            expertDealMakerCount++;
        } else if(seller.sellerStatuses.indexOf(mappingService.dealMakersMapping.DEAL_MAKER) > -1){
            seller.isDealMaker = true;
            seller.sellerType = mappingService.dealMakersMapping.DEAL_MAKER;
            dealMakers.push(seller);
            dealMakerCount++;
        } else {
            otherSellers.push(seller);
        }
    });
    topSellers = [].concat(cityExpert, localityExpert, expertSellers, dealMakers, otherSellers); //Sorted array in order of expert, dealmaker and topSellers

    return [{
        topSellers,
        dealMakerCount,
        expertDealMakerCount,
        localityExpertCount,
        cityExpertCount,
        isSpecificExpert
    }];
};
module.exports = agentService;
