"use strict";

const apiService = require('services/apiService'),
    apiConfig = require('configs/apiConfig'),
    utils = require('services/utilService'),
    priorityService = require('services/propertyPriorityService'),
    globalConfig = require('configs/globalConfig'),
    masterDetailsService = require('services/masterDetailsService'),
    maps = require('services/propertyMappingService'),
    mappingService = require('services/mappingService'),
    imageParser = require('services/imageParser'),
    imageService = require('services/imageService'),
    videoService = require('services/videoService'),
    logger = require('services/loggerService'),
    utilService = require('services/utilService'),
    _ = utilService._,
    _createSelector = apiService.createSelector,
    _isEmptyObject = utils.isEmptyObject;

var sortByMap = function(dataArray, type, category, card) {
        var priorityArray = priorityService.getPriorityArray(type, category, card);
        var resultArray = [];
        for (var i = 0; i < priorityArray.length; i++) {
            if (dataArray[priorityArray[i]] && dataArray[priorityArray[i]].val !== undefined) {
                resultArray.push(dataArray[priorityArray[i]]);
            }
        }
        return resultArray;
    },
    getThirdFolddata = function(dataArray) {
        var priorityArray = priorityService.propertyThirdFold();
        var resultArray = [];
        for (var i = 0; i < priorityArray.length; i++) {
            if (dataArray[priorityArray[i]] && dataArray[priorityArray[i]].val !== undefined) {
                resultArray.push(dataArray[priorityArray[i]]);
            }
        }
        return resultArray;
    },
    _getDefaultList = function(category, masterMap, isCommercialListing) {
        var keysArray = isCommercialListing ? globalConfig.defaultListCommercial[category] : globalConfig.defaultList[category],
            result = {},
            length = keysArray.length;
        for (let i = 0; i < length; i++) {
            var key = keysArray[i];
            result[key] = {
                val: masterMap[keysArray[i]] && masterMap[keysArray[i]].name,
                icon: maps.getIconMap(key),
                id: keysArray[i]
            };
        }
        return result;
    },

    parseAmenities = function(amenityList = [], type, category, isCommercialListing) { //jshint ignore:line
        let result = {},
            amenityMap = masterDetailsService.getMasterDetails('amenities');
        for (let i = 0, length = amenityList.length; i < length; i++) {
            let currentKey = amenityList[i],
                currentAmenity = amenityMap[currentKey.toString()];
            if (currentAmenity) {
                result[currentKey] = {
                    icon: 'icon-gym',
                    val: currentAmenity.name,
                    present: true,
                    id: currentKey
                };
            }
        }
        result = utils.deepCopy(result, _getDefaultList('amenity', amenityMap, isCommercialListing));
        let parsedAmenties = sortByMap(result, type, category, 'amenity');
        let parsedFinalAmenties = sortListOnKeyBasis(parsedAmenties, 'present');
        return {
            amenityList: parsedFinalAmenties
        };
    },

    parseFurnishing = function(furnishingList = [], type, category) { //jshint ignore:line
        let result = {},
            furnishingMap = masterDetailsService.getMasterDetails('furnishings');
            
        for (let i = 0, length = furnishingList.length; i < length; i++) {
            let currentKey = furnishingList[i].masterFurnishingId,
                currentFurnish = furnishingMap[currentKey];
            if (currentFurnish) {
                result[currentKey] = {
                    icon: 'icon-gym',
                    val: currentFurnish.name,
                    present: true,
                    id: currentKey
                };
            }
        }
        
        result = utils.deepCopy(result, _getDefaultList('furnish', furnishingMap));
        let parsedFurnishingListMap = sortByMap(result, type, category, 'furnish');
        let finalFurnishedList = sortListOnKeyBasis(parsedFurnishingListMap, 'present');
        return {
            furnishingList: finalFurnishedList
            
        };
    },
    sortListOnKeyBasis = function(list, key){
        return list.sort((a,b)=>{ 
            return b[key] || false - a[key] || false ;
           });
    },

    parseSpecifications = function(specifications = [], type, category) { //jshint ignore:line
        let length = specifications.length,
            result = {},
            masterSpecification;
        for (let i = 0; i < length; i++) {
            let currentObj = specifications[i];
            masterSpecification = currentObj.masterSpecification || {};
            category = masterSpecification.masterSpecificationCategory || {};
            let parent = category.masterSpecParentCat || {};
            if (!_isEmptyObject(masterSpecification) && !_isEmptyObject(category) && !_isEmptyObject(parent)) {
                var displayName = parent.masterSpecParentDisplayName;
                result[displayName] = result[displayName] || {};
                result[displayName][category.masterSpecCatDisplayName] = masterSpecification.masterSpecClassName;
            }
        }

        specifications = result;
        let specSorted = [],
            keyArray = Object.keys(specifications);

        for (let i = 0, length = keyArray.length; i < length; i++) {
            specSorted.push({
                key: keyArray[i],
                value: specifications[keyArray[i]]
            });
        }
        specSorted.sort(function(a, b) {
            if (a.value.length > b.value.length) {
                return -1;
            } else if (a.value.length < b.value.length) {
                return 1;
            }
        });
        return {
            specificationList: specSorted
        };
    },


    _getPropertyCount = function(sellerId, list) {
        let count = list.filter((facet) => {
            return facet[sellerId.toString()];
        });

        return count[0] && count[0][sellerId];
    },

    _parseSellerDetails = function(sellerList = [], req) {
        let items = sellerList.items || [],
            sellerData = [];
        for (let i = 0, length = items.length; i < length; i++) {
            let currentObject = items[i],
                companySeller = currentObject.companySeller || {},
                company = companySeller.company || {},
                user = companySeller.user || {};
            sellerData.push({
                name: company.name || '',
                rating: (company.score),
                price: utils.formatNumber(currentObject.currentListingPrice ? currentObject.currentListingPrice.price : '', { precision : 2, seperator : req.locals.numberFormat }),
                propertyCount: _getPropertyCount(company.id, sellerList.facets.listingCompanyId),
                sellerURL: '#',
                phone: (user.contactNumbers && user.contactNumbers.length) ? user.contactNumbers[0].contactNumber : 'N.A.',
                id: company.id,
                userId: user.id,
                ratingClass: utils.getRatingClass((company.score)),
                displayImage: imageParser.appendImageSize(company.logo || user.profilePictureURL, 'thumbnail'),
                avatar: utils.getAvatar(company.name)
            });
        }
        return sellerData;
    },

    _parseSimilarListings = function(list = [], listingId, {req}) { //jshint ignore:line
        var i, length = list && list.length,
            similarArray = [], numberFormat = req && req.locals && req.locals.numberFormat;
        for (i = 0; i < length; i++) {
            var currentObject = list[i] || {}; //.listing;
            if (currentObject.id !== listingId) {
                let size = currentObject.property.size ? utils.formatNumber(currentObject.property.size, { type : 'number', seperator : numberFormat}) + " sq ft" : '',
                    bedrooms = currentObject.property.bedrooms,
                    bathrooms = currentObject.property.bathrooms,
                    title = bedrooms ? `${bedrooms} BHK + ${bathrooms}T` : '',
                    unitName = (mappingService.getUnitTypes().filter(obj => { //jshint ignore:line
                        if (obj.id == currentObject.property.unitTypeId) {
                            return true;
                        }
                    })[0] || {}).displayText || '',
                    company = currentObject.companySeller && currentObject.companySeller.company && currentObject.companySeller.company || {},
                    locality = currentObject.property.project.locality,
                    project = currentObject.property.project,
                    companyUser = currentObject.companySeller && currentObject.companySeller.user && currentObject.companySeller.user || {},
                    companyAvatarDetails = utils.getAvatar(company.name),
                    tempObject = {
                        "listingId": currentObject.id,
                        "price": utils.formatNumber(currentObject.currentListingPrice.price, { precision : 2, returnSeperate : true, seperator : numberFormat }),
                        "unitName": (globalConfig.commercailUnitTypes.indexOf(currentObject.property.unitTypeId) > -1) ? currentObject.property.unitType : ((currentObject.property.unitTypeId != 3) ? (currentObject.property.bedrooms + ' BHK' + (currentObject.property.study ? ' + study' : '')) : ('Plot')),
                        "projectName": project.name,
                        "projectId": project.projectId,
                        "builder":currentObject.property.project.builder,
                        "localityName": locality.label,
                        "localityId": locality.localityId,
                        "imageUrl": imageParser.appendImageSize(currentObject.mainImageURL, 'tile'),
                        "pageURL": utils.prefixToUrl(currentObject.resaleURL),
                        "altText": `${size} ${title} ${unitName} in ${currentObject.property.project.builder.name} ${currentObject.property.project.name}`,
                        "title": `${size} ${title} ${currentObject.property.project.builder.name} ${currentObject.property.project.name}`,
                        "size": currentObject.property.size ? utils.formatNumber(currentObject.property.size, {type : 'number', seperator : numberFormat}) + " sq ft" : undefined,
                        "isAssist": company.assist,
                        "companyName": company.name,
                        "anchorTitle": `${size} ${title} ${unitName} in ${currentObject.property.project.builder.name} ${currentObject.property.project.name}`,
                        "companyId": company.id,
                        "sellerId": companyUser.id,
                        "companyImage": imageParser.appendImageSize(companyUser.profilePictureURL, 'tile'),
                        "cityId": locality.cityId,
                        "companyAvatar": companyAvatarDetails,
                        "propertyType": currentObject.property.unitType,
                        "salesType": currentObject.listingCategory,
                        "sellerType": "", // key not coming in api
                        "sellerRating": (company.score && utils.numberToDecimalPrecision(company.score/2)) || ''
                    };
                tempObject.builder.displayName = tempObject.builder.displayName ? tempObject.builder.displayName : tempObject.builder.name;
                similarArray.push(tempObject);
            }
        }
        return similarArray;
    },

    parsePropertySummary = function(data = {}, {req}) {
        let parsedData = {};
        let id = data.id;
        let property = data.property || {};
        let project = property.project || {};
        let companySeller = data.companySeller || {};
        let price = data.currentListingPrice && data.currentListingPrice.price;
        let rating = (companySeller.company && companySeller.company.score && utils.numberToDecimalPrecision(companySeller.company.score/2)) || '';
        parsedData = {
            leadFormData: {
                id: data.id,
                cityId: project.locality && project.locality.cityId,
                salesType: (data.listingCategory == "Rental") ? "rent" : "buy",
                listingId: data.id,
                localityId: project.locality && project.locality.localityId,
                projectId: project.projectId,
                companyId: companySeller.company && companySeller.company.id,
                companyName: companySeller.company && companySeller.company.name,
                companyPhone: companySeller.user && companySeller.user.contactNumbers && companySeller.user.contactNumbers[0] && companySeller.user.contactNumbers[0].contactNumber,
                companyRating: rating,
                companyImage: data.sellerimage,
                companyAssist: companySeller.company && companySeller.company.assist,
                companyUserId: companySeller.user && companySeller.user.id,
                companyType: companySeller.company && companySeller.company.type,
                projectName: project.name,
                projectOverviewUrl: utils.prefixToUrl(project.overviewUrl),
                propertyType: [property.unitTypeId],
                propertyTypeLabel: property.unitType,
                builderId: project.builder && project.builder.id,
                imageUrl: data.mainImageURL,
                bhk: property.bedrooms,
                projectStatus: project.projectStatus,
                minBudget: price,
                maxBudget: price,
                budget: price,
                isSponsored: data.isSponsored
            },
            viewData: {
                imageUrl: imageParser.appendImageSize(data.mainImageURL, 'smallThumbnail'),
                price: utilService.formatNumber(price, { precision : 2, returnSeperate : true, seperator : req.locals.numberFormat }),
                locality: project.locality && project.locality.label,
                city: project.locality && project.locality.suburb && project.locality.suburb.city && project.locality.suburb.city.label,
                title: (globalConfig.commercailUnitTypes.indexOf(property.unitTypeId) > -1) ? property.unitType : ((property.unitTypeId != 3) ? (property.bedrooms + ' BHK' + (property.study ? ' + study' : '')) : ('Plot')),
                url: utils.prefixToUrl(data.resaleURL)
            }
        };
        return {
            id: id,
            data: parsedData
        };
    },


    getSimilarListings = function(propertyId, { req, count, companyId, summary, distinctSeller, isCommercial }) {

        if (typeof propertyId == 'undefined') {
            return new Promise(function(resolve, reject) {
                reject(new Error("Provide Property Id parameter in property service"));
            });
        }

        var selector = {
            "fields": ["carpetArea", "updatedAt", "displayDate", "launchDate", "hideLaunchDate", "sizeInAcres", "resaleURL", "qualityScore", "listingLongitude", "listingLatitude", "localityId", "buyUrl", "rentUrl", "logo", "profilePictureURL", "score", "masterAmenityIds", "companySeller", "overviewUrl", "constructionStatusId", "mainImage", "activeStatus", "mainImageURL", "avgPriceRisePercentage", "amenitiesIds", "contactNumber", "registered", "contactNumbers", "seller", "company", "user", "imageURL", "cityId", "latitude", "longitude", "percentageCompletionOnTime", "priceAllInclusive", "type", "altText", "title", "imageType", "absolutePath", "imageTypeId", "URL", "percentageCompletionOnTime", "projectStatusCount", "tenantType", "noOfOpenSides", "studyRoom", "poojaRoom", "servantRoom", "balcony", "bedrooms", "bathrooms", "possessionDate", "facingId", "maxConstructionCompletionDate", "minConstructionCompletionDate", "unitName", "unitType", "livabilityScore", "url", "availability", "size", "unitTypeId", "percentageCompletionOnTime", "establishedDate", "description", "poojaRoom", "servantRoom", "mainEntryRoadWidth", "tenantTypes", "derivedAvailability", "projectStatus", "listingCategory", "id", "label", "name", "floor", "securityDeposit", "propertyId", "images", "furnished", "ownershipTypeId", "viewDirections", "viewType", "bookingAmount", "pricePerUnitArea", "price", "currentListingPrice", "totalFloors", "project", "property", "builder", "displayName", "locality", "suburb", "city", "specifications", "furnishings", "amenity", "projectAmenityId", "projectId", "amenityDisplayName", "listingAmenities", "amenityMaster", "verified", "amenityId", "amenityName", "abbreviation", "masterSpecification", "masterSpecificationCategory", "masterSpecClassName", "masterSpecId", "masterSpecCatId", "masterSpecCatDisplayName", "masterSpecParentCat", "masterSpecParentCatId", "masterSpecParentDisplayName", "masterFurnishingId", "masterFurnishing", "statusId", "sellerId", "ownerId", "seller", "rating", "assist", "fullName", "carParkingType", "noOfCarParks", "negotiable", "defaultImageId", "projectAmenities", "rk", "penthouse", "studio"],
            "paging": {
                "rows": count || 10
            },
            "filters": []
        };

        if (companyId) {
            selector.filters.push({
                "key": "listingCompanyId",
                "value": companyId,
                "type": "notEqual"
            });
        }

        var api = apiConfig.similarListings({
            propId: propertyId,
            selector: _createSelector(selector),
            query: {
                distinctSeller:distinctSeller||false,
                isCommercial:isCommercial||false
            }
        });

        return apiService.get(api, { req }).then(function(data) {
            data = (data && data.data) || [];
            if (summary == "true") {
                let finalData = {};
                _.forEach(data, (v) => {
                    let parsedData = parsePropertySummary(v, {req});
                    finalData[parsedData.id] = parsedData.data;
                });
                return finalData;
            } else {
                return _parseSimilarListings(data, propertyId, {req});
            }
        }, err => {
            throw err;
        });

    },

    getSellerDetails = function(config, { req }) {
        let _sellerAPIDefaultConfig = {
                key: 'listingSellerCompanyScore',
                order: 'true',
                baseIndex: '0',
                rowsToLoad: '5',
                format: 'json'
            },
            params = config && config.params || {};
        params = utils.deepCopy(params, _sellerAPIDefaultConfig);

        let selector = {
            "fields": ["assist", "companyImage", "score", "contactNumber", "contactNumbers", "user", "name", "id", "profilePictureURL", "label", "sellerId", "property", "currentListingPrice", "price", "bedrooms", "bathrooms", "size", "unitTypeId", "project", "projectId", "studyRoom", "servantRoom", "poojaRoom", "companySeller", "company", "companyScore"],
            "paging": {
                start: params.baseIndex,
                rows: params.rowsToLoad
            },
            "filters": []
        };


        if (params.bed) {
            selector.filters.push({
                "key": "bedrooms",
                "value": params.bed.split(',')
            });
        }
        if (params.bath) {
            selector.filters.push({
                "key": "bathrooms",
                "value": params.bath.split(',')
            });
        }
        if (params.pricemin || params.pricemax) {
            selector.filters.push({
                "key": "price",
                "type": "range",
                "from": params.pricemin,
                "to": params.pricemax
            });
        }
        if (params.unitTypeId) {
            selector.filters.push({
                "key": "unitTypeId",
                "value": params.unitTypeId.split(',')
            });
        }
        if (params.listingCategory) {
            selector.filters.push({
                "key": "listingCategory",
                "value": params.listingCategory.split(',')
            });
        }
        if (params.listingCompanyId) {
            selector.filters.push({
                "key": "listingCompanyId",
                "value": params.listingCompanyId.split(','),
                "type": "notEqual"
            });
        }

        let api = apiConfig.sellerFiltering({
            projectId: params.projectId,
            query: {
                selector: _createSelector(selector)
            }
        });

        return apiService.get(api, { req }).then(function(data) {
            logger.info('success recieved from get seller details api', data.totalCount);

            return {
                totalCount: utils.formatNumber(data.totalCount, { type : 'number', seperator : req.locals.numberFormat }),
                sellerList: _parseSellerDetails(data.data, req),
                params: config.params
            };
        }, err => {
            throw err;
        });

    },

    getListingDetail = function(listingId, { fields, req, isCommercial }) {
        if (!fields) {
            fields = ["carpetArea", "paidLeadCount", "propertyOtherPricingSubcategoryMappings", "otherPricingSubcategoryId", 
            "localityHeroshotImageUrl", "updatedAt", "displayDate", "launchDate", "hideLaunchDate", "sizeInAcres", "resaleURL", "qualityScore", 
            "listingLongitude", "listingLatitude", "localityId", "buyUrl", "rentUrl", "logo", "profilePictureURL", "score", 
            "masterAmenityIds", "companySeller", "overviewUrl", "constructionStatusId", "mainImage", "activeStatus", "mainImageURL", 
            "avgPriceRisePercentage", "amenitiesIds", "contactNumber", "registered", "contactNumbers", "seller", "company", "user", 
            "imageURL", "cityId", "latitude", "longitude", "percentageCompletionOnTime", "priceAllInclusive", "type", "altText", 
            "title", "imageType", "absolutePath", "imageTypeId", "URL", "percentageCompletionOnTime", "projectStatusCount", 
            "tenantType", "noOfOpenSides", "studyRoom", "poojaRoom", "servantRoom", "balcony", "bedrooms", "bathrooms", "possessionDate", 
            "facingId", "maxConstructionCompletionDate", "minConstructionCompletionDate", "unitName", "unitType", "livabilityScore", "url", 
            "availability", "size", "unitTypeId", "percentageCompletionOnTime", "establishedDate", "description", "poojaRoom", "servantRoom", 
            "mainEntryRoadWidth", "tenantTypes", "derivedAvailability", "projectStatus", "listingCategory", "id", "label", "name", "floor", 
            "securityDeposit", "propertyId", "images", "furnished", "ownershipTypeId", "viewDirections", "viewType", "bookingAmount", 
            "pricePerUnitArea", "price", "currentListingPrice", "totalFloors", "project", "property", "builder", "displayName", "locality", 
            "suburb", "city", "specifications", "furnishings", "amenity", "projectAmenityId", "projectId", "amenityDisplayName", "listingAmenities", 
            "amenityMaster", "verified", "amenityId", "amenityName", "abbreviation", "masterSpecification", "masterSpecificationCategory", 
            "masterSpecClassName", "masterSpecId", "masterSpecCatId", "masterSpecCatDisplayName", "masterSpecParentCat", "masterSpecParentCatId", 
            "masterSpecParentDisplayName", "masterFurnishingId", "masterFurnishing", "statusId", "sellerId", "ownerId", "seller", "rating", "assist", 
            "fullName", "carParkingType", "noOfCarParks", "negotiable", "defaultImageId", "projectAmenities", "rk", "penthouse", "studio" , 
            "allocation", "allocationHistory","masterAllocationStatus","status","listingSellerTransactionStatuses","listingSellerDisplayAds",
            "listingSellerBenefits","listingSellerPrivileges","listingSellerMiscellaneousProducts","sellerCompanyFeedbackCount","sellerCallRatingCount", 
            "tenantPreference", "imageCount", "mainImageWidth", "mainImageHeight"];
            if(isCommercial) {
                fields.push("isCommercialListing","maintenanceFrequencyType", "maintenanceCharges", "isStampDutyIncluded", "isWaterChargesIncluded", "isElectricityChargesIncluded", "brokeragePercentage", "lockInPeriod", "assuredReturn", "assureReturnPercentage", "isMainRoadFacing", "willingToModifyInteriors", "isBoundryWallPresent", "personalWashroom", "pantryAvailable", "cafeteriaAvailable", "noOfSeats", "noOfRooms", "currentlyLeasedOut", "currentlyLeasedOutBy", "isCornerShop", "otherCharges", "pantryType", "currentBusinessSector");
            }
        }
        let propertyDetail = apiService.get(apiConfig.property({
            propId: listingId,
            selector: _createSelector({
                fields: fields
            })
        }), { req, isPrimary: true });
        return propertyDetail;
    },

    getPropertyDetail = function(config, { req }) {

        let listingId = config.listingId,
            projectId = config.projectId,
            propertyId = config.propertyId,
            isCommercial = config.isCommercial,
            propertyDetail = getListingDetail(listingId, { req, isCommercial });

        return propertyDetail.then(response => {
            logger.info('success from main api property page');
            let mainImage = response && response.data && response.data.mainImage,
                defaultImageId = response && response.data && response.data.defaultImageId;
            if (mainImage && defaultImageId) {
                mainImage.id = defaultImageId;
            }
            let videoPromise = videoService.getVideoForListing(
                listingId, { req }
            );
            let imagePromise = imageService.getImageDetails('listing', {
                projectId: projectId,
                listingId: listingId,
                propertyId: propertyId,
                mainImageId: defaultImageId
            }, { req: req, isPrimary: true });
            return imagePromise.then(images => {
                return videoPromise.then(videoObject => {
                    let imageDetails = imageParser.groupListingImages(images, { projectId, listingId, propertyId, mainImage, titleVideo: videoObject }, req); //property
                    return {
                        propertyDetail: response.data,
                        galleryContainerData: imageDetails.galleryContainerData,
                        galleryData: {
                            data: imageDetails.galleryData,
                            group: imageDetails.galleryGroup
                        },
                        floorPlanObj: imageDetails.floorPlanObj
                    };
                }, err => {
                    throw err;
                });
            }, err => {
                throw err;
            });
        }, err => {
            throw err;
        });
    },


    //options: {fields:["",""]}
    getPropertyInfo = function(listingId, options, { req }) {
        return apiService.get(apiConfig.property({
            propId: listingId,
            selector: _createSelector(options)
        }), { req }).then(response => {
            logger.info('success from listing api in getPropertyInfo');
            return response.data;
        }, err => {
            throw err;
        });
    },

    // common function for amp and normal property pages
    getAdditionalRooms = function(rooms) {
        let count = 0,
            displayText = [],
            roomArray = globalConfig.roomArray,
            length = roomArray.length,
            i;
        for (i = 0; i < length; i++) {
            let roomCount = rooms[roomArray[i]];
            if (roomCount) {
                count = count + roomCount;
                displayText.push(roomArray[i] + ' room');
            }
        }
        return count ? count + ' rooms' + (displayText.length ? '( ' + displayText.join(', ') + ' )' : '') : undefined;
    },

    getViewDirections = function(directions) {
        var direction = [],
            length = directions.length;

        for (let directon of directions) {
            direction.push(directon.viewType);
        }

        return length ? direction.join(', ') : undefined;
    },

    getTenantType = function(tenantArray = []) {
        let tenantMap = mappingService.getTenantTypes,
            result = [],
            length = tenantArray.length;
        for (let type of tenantArray) {
            let category = tenantMap(type.id);
            if (category) {
                result.push(category);
            }
        }
        return length ? result.join(', ') : undefined;
    },

    getAmenities = function(listingAmenities, projectAmenities) {
        if (!projectAmenities || !projectAmenities.length) {
            return listingAmenities;
        }

        listingAmenities = listingAmenities || [];

        let projectAmenityIds = projectAmenities.map(function(projectAmenity) {
            return projectAmenity.amenityMaster.amenityId;
        });
        return _.union(listingAmenities, projectAmenityIds);
    },

    getSimilarLocalityListings = function (queryObj, {req}) {
        if (typeof queryObj.localityId == 'undefined') {
            return new Promise(function(resolve, reject) {
                reject(new Error("Provide Locality Id parameter in property service"));
            });
        }

        var selector = {
            fields: ["id"],
            paging: {
                "rows": 10
            },
            filters: [{
                key: "localityId",
                value: queryObj.localityId,
                type: "equal"
            },{
                key: 'price',
                type: 'range',
                from: queryObj.minBudget,
                to: queryObj.maxBudget
            }]
        };

        var api = apiConfig.similarLocalityListings({
            selector: _createSelector(selector)
        });
        return apiService.get(api, { req }).then(function(data) {
            return data;
        }, err => {
            throw err;
        });
    };

module.exports = {
    getPropertyDetail,
    getListingDetail,
    getPropertyInfo,
    getSellerDetails,
    getSimilarListings,
    getSimilarLocalityListings,
    parseAmenities,
    parseFurnishing,
    parseSpecifications,
    sortByMap,
    parsePropertySummary,
    getAdditionalRooms,
    getViewDirections,
    getTenantType,
    getAmenities,
    getThirdFolddata
};
