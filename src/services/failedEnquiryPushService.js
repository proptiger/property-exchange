"use strict";

const AWS = require('aws-sdk'),
      utilService = require('services/utilService');

var failedEnquiryPushService = {};

AWS.config.update({
    accessKeyId: process.env.AWS_CALL_QUEUE_ACCESS_KEY,
    secretAccessKey: process.env.AWS_CALL_QUEUE_SECRET_KEY
});

var sqs = new AWS.SQS({
    region: process.env.AWS_CALL_QUEUE_REGION
});

failedEnquiryPushService.push = function({payload, api}) {
    if (!payload || !api) {
        return;
    }
    delete payload.otp;
    let separator = api.indexOf('?') > -1 ?  '&' : "?";
    api += separator + "format=json&showerror=true" ;
    let _payload = {
        payload,
        api
    },
    sqsParams = {
        MessageBody: JSON.stringify(_payload),
        QueueUrl: process.env.SQS_FAILED_ENQUIRIES_URL
    };
    return new Promise(function(resolve, reject) {
        sqs.sendMessage(sqsParams, (err, data) => {
            if (err) {
                console.log('Error while pushing failed enquiries ... ', err);
                return reject(err);
            }
            console.log("pushed failed enquiries ...", _payload);
            let subject = process.env.WEBSITE_URL + " failed enquiries posted";
            utilService.sendMail(subject, JSON.stringify(payload));
            resolve(data);
        });
    });
};

module.exports = failedEnquiryPushService;
