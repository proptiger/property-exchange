"use strict";

const apiService = require('services/apiService'),
    apiConfig = require('configs/apiConfig'),
    utils = require('services/utilService'),
    _ = utils._,
    moment = require('moment');

var blogService = {};

blogService.getPostList = function(req, config) {
    let urlConfig, queryParam, internationalKey,
        query = req.query || {},
        currentPage = parseInt(query.page) || 1,
        currentUrl = req.urlDetail.url,
        lang = (utils.getVernacLanguage({req})||'').replace(/-.*/, ''),
        defaultConfig = Object.assign({
            "page": currentPage,
            "count": "9",
            "sort": "date-desc",
            "website": "makaan"
        }, lang?{lang}:{}, config);
    internationalKey = (req.pageData && req.pageData.countryDisplayName) || (req.locals.numberFormat && req.locals.numberFormat.code);
    queryParam = utils.serialize(defaultConfig);
    urlConfig = apiConfig.inTheNewsPostList(queryParam);
    urlConfig.url = (internationalKey ? internationalKey + '/' : '') + urlConfig.url;
    return apiService.get(urlConfig, { req }).then(response => {
        let paginationData = {
            "totalPages": Math.ceil(response.totalCount / defaultConfig.count),
            "currentUrl": currentUrl,
            "filter": true,
            "currentPage": currentPage,
            "changeQueryParam": utils.changeQueryParam
        };
        _.forEach(response.data, function(iteratee) {
            iteratee.postDateIso = iteratee.postDate;
            iteratee.postDate = moment(iteratee.postDate).format("Do MMM YY");
        });
        return {
            paginationData: paginationData,
            data: response.data
        };
    }, err => {
        throw err;
    }).catch((err)=>{
        throw err;
    });
};

blogService.getPostDetail = function(req, postName) {
    let urlConfig = apiConfig.inTheNewsPostDetail(postName);
    return apiService.get(urlConfig, { req }).then(response => {
        return response;
    }, err => {
        throw err;
    });
};

blogService.getHomeLoanBlogs = function(req,callback){
    let blogIqUrl = process.env.BASE_URL_MAKAANIQ || '';
    let configLoan = {
            "text": "homeloan",
            "count": 2,
            "sort": "views-desc",
            "website": "makaaniq"
        },
        configTop = {
            "count": 2,
            "days": 50,
            "sort": "views-desc",
            "website": "makaaniq"
        };
    Promise.all([this.getPostList(req, configLoan), this.getPostList(req, configTop)]).then((response) => {
        let post = [];
        if (_.isArray(response[0].data) && _.isArray(response[1].data)) {
            post = response[0].data.concat(response[1].data).slice(0, 2);
        }
        callback(null, {
            post,
            makaaniqUrl: blogIqUrl
        });
    }, err => {
        callback(err);
    });
};

module.exports = blogService;
