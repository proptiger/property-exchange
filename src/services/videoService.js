"use strict";

const apiService = require('services/apiService'),
    apiConfig = require('configs/apiConfig');


// let sample = {
//     "statusCode":"2XX",
//     "version":"A",
//     "data": [
//         {
//             "id":222,
//             "objectMediaTypeId":106,
//             "objectMediaType":{
//                 "id":106,
//                 "objectType":{
//                     "id":2,
//                     "type":"property"
//                 },
//                 "mediaType":{
//                     "id":4,
//                     "name":"Video"
//                 },
//                 "objectTypeId":2,
//                 "mediaTypeId":4,
//                 "type":"VideoWalkthrough",
//                 "domainId":2,
//                 "priority":1,
//                 "imageSitemapEnabled":0,
//                 "displayName":"Video Walkthrough",
//                 "mediaDuplicacyRule":{
//                     "id":1,
//                     "duplicacyRule":"NO_DUPLICATES"
//                 }
//             },
//             "objectId":5054948,
//             "priority":1,
//             "sizeInBytes":41906337,
//             "description":"Video Tour",
//             "active":true,
//             "createdAt":1451475623000,
//             "updatedAt":1457246549000,
//             "mediaExtraAttributes":{
//                 "extractPath":"4/2/5054948/106/222/5054948.mp4",
//                 "videoUrlDetails":[{
//                     "resolution":720,
//                     "bitRate":"700",
//                     "url":"http://d1vh6m45iog96e.cloudfront.net/4/2/5054948/106/222/720/700/5054948.mp4"
//                 },
//                 {
//                     "resolution":720,
//                     "bitRate":"1000",
//                     "url":"http://d1vh6m45iog96e.cloudfront.net/4/2/5054948/106/222/720/1000/5054948.mp4"
//                 },
//                 {
//                     "resolution":540,
//                     "bitRate":"500",
//                     "url":"http://d1vh6m45iog96e.cloudfront.net/4/2/5054948/106/222/540/500/5054948.mp4"
//                 },
//                 {
//                     "resolution":540,
//                     "bitRate":"1000",
//                     "url":"http://d1vh6m45iog96e.cloudfront.net/4/2/5054948/106/222/540/1000/5054948.mp4"
//                 },
//                 {
//                     "resolution":360,
//                     "bitRate":"300",
//                     "url":"http://d1vh6m45iog96e.cloudfront.net/4/2/5054948/106/222/360/300/5054948.mp4"
//                 },
//                 {
//                     "resolution":270,
//                     "bitRate":"300",
//                     "url":"http://d1vh6m45iog96e.cloudfront.net/4/2/5054948/106/222/270/300/5054948.mp4"
//                 }
//                 ]},
//                 "imageUrl":"https://im.proptiger.com/18/222/111/899718.jpeg",
//                 "fps":30,
//                 "resolution":"1280×720",
//                 "bitRate":10778,
//                 "duration":31.104,
//                 "imageId":899718,
//                 "image":{
//                     "id":899718,
//                     "imageTypeId":111,
//                     "objectId":222,
//                     "path":"18/222/111/",
//                     "pageUrl":"gallery/222-899718",
//                     "statusId":1,
//                     "createdAt":1452677140000,
//                     "sizeInBytes":98784,
//                     "width":800,
//                     "height":600,
//                     "waterMarkName":"899718.jpeg",
//                     "active":true,
//                     "activeStatus":1,
//                     "seoName":"899718.jpeg",
//                     "absolutePath":"https://im.proptiger.com/18/222/111/899718.jpeg",
//                     "imageType":{"id":111,"objectType":{"id":18,"type":"video"},"mediaType":{"id":1,"name":"Image"},"objectTypeId":18,"mediaTypeId":1,"type":"VideoThumbnail","domainId":2,"priority":0,"imageSitemapEnabled":0,"displayName":"Video Thumbnail","mediaDuplicacyRule":{"id":1,"duplicacyRule":"NO_DUPLICATES"}},"masterImageStatuses":{"id":1,"status":"PROCESSED"}},"statusId":5,"url":"http://d1vh6m45iog96e.cloudfront.net/4/2/5054948/106/222/5054948.mp4","masterVideoStatuses":{"id":5,"status":"Processed"}
//                 }
//     ]
// };

function getVideoForListing(listingId, {req}) {
    let urlConfig = apiConfig.getVideoForListing({listingId});

    // return new Promise((resolve, reject) => {
    //     resolve(parseVideoObject(sample.data[0]));
    // });

    return apiService.get(urlConfig, {req}).then(response => {
        if (response.data && response.data.length) {
            return parseVideoObject(response.data[0]);
        } else {
            return null;
        }
    }, err => {
        throw err;
    });
}

function parseVideoObject(videoObject) {
    // let bitRate = isMobile ? "300" : "700";
    // let resolution = isMobile ? 360 : 720;

    let videoWithSpecifiedBitrate = videoObject.mediaExtraAttributes.videoUrlDetails.sort(
        (a,b) => {
            return b.resolution - a.resolution;
        }
        // v => v.bitRate === bitRate && v.resolution === resolution
    )[0];

    return {
        category: 'Video',
        vsrc: videoWithSpecifiedBitrate.url,
        src: videoObject.imageUrl,
        thumb: videoObject.imageUrl,//`${videoObject.imageUrl}?width=130&height=100`,
        alt: videoObject.description || '',
        title: videoObject.description || '',
        subHtml: videoObject.description || ''
    };
}

module.exports = {
    getVideoForListing
};
