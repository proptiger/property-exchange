'use strict';

const globalConfig = require('configs/globalConfig'),
    apiService = require('services/apiService'),
    logger = require('services/loggerService'),
    utilService = require('services/utilService');

var projectFilters = {
        beds: {
            key: 'bedrooms',
            type: 'equal',
            options: {
                '1': [1],
                '2': [2],
                '3': [3],
                '4': [4],
                '5': [5],
                '6': [6],
                '3plus': [4, 5, 6, 7, 8, 9, 10]
            }
        },
        baths: {
            key: 'bathrooms',
            type: 'equal',
            options: {
                '1': [1],
                '2': [2],
                '3': [3],
                '1plus': [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
                '2plus': [2, 3, 4, 5, 6, 7, 8, 9, 10],
                '3plus': [3, 4, 5, 6, 7, 8, 9, 10]
            }
        },
        propertyType: {
            key: 'unitTypeId',
            type: 'equal',
            options: {
                'apartment': [1],
                'builder-floor': [7],
                'independent-floor': [7],
                //'studio-apartment': [9],
                'independent-house': [19],
                'villa': [2],
                'residential-plot': [3],
                'farmhouse': [8],
                'shop': [5],
                'office': [6],
                //'penthouse': [20]
            }
        },
        projectStatus:{
            key: 'projectStatus',
            type: 'equal',
            options: {
                'launching-soon': ['not launched', 'pre launch'],
                'new-launch': ['launch'],
                'under-construction': ['under construction'],
                'ready-to-move-in': ["ready for possession","occupied","completed"]
            }
        },
        budget: {
            key: 'price',
            type: 'range',
            getRange: function(from, to) {
                return {
                    from: from,
                    to: to
                };
            }
        },
        area: {
            key: 'size',
            type: 'range',
            getRange: function(from, to) {
                return {
                    from: from,
                    to: to
                };
            }
        },
        availability: {
            key: 'possessionDate',
            type: 'range',
            getRange: function(days) {

                if (days === 'immediate') {
                    days = 0;
                } else if (isNaN(days)) {
                    return;
                }

                let returnObj = {
                    from: utilService.dateAhead(null, null, false),
                    to: utilService.dateAhead(null, {
                        days: (days * 1)
                    }, false)
                };

                if (days == '0' || days === 0) {
                    delete returnObj.from;
                }
                return returnObj;
            }
        },

        possession: {
            key: 'possessionDate',
            type: 'range',
            getRange: function(years) {
                return {
                    from: utilService.dateAhead(null, null, false),
                    to: utilService.dateAhead(null, {
                        years: (years * 1)
                    }, false)
                };
            },
            setFilters: function(years, temp) {
                let filterObj, rangeObj = this.getRange(years);

                if (years === 'any') {
                    filterObj = {
                        'type': 'equal',
                        'key': 'projectStatus',
                        'value': 'under contruction'
                    };
                    temp.filters.push(filterObj);
                    return;
                }

                filterObj = {
                    'type': 'range',
                    'key': this.key,
                    'from': rangeObj.from,
                    'to': rangeObj.to
                };
                temp.filters.push(filterObj);
            }
        },
        newOrResale: {
            key: 'isResale',
            type: 'equal',
            options: {
                'new': false,
                'primary': false,
                'resale': true
            }
        },
        locality: {
            key: 'localityId',
            type: 'exact'
        },
        builder: {
            key: 'builderId',
            type: 'exact'
        },
        city: {
            key: 'cityLabel',
            type: 'exact'
        },
        hasTownShip: {
            key: 'hasTownShip',
            type: 'equal',
            options: {
                'true': ['true']
            }
        },
        furnished: {
            key: 'listingFurnished',
            type: 'equal',
            options: {
                'unfurnished': ['Unfurnished'],
                'semifurnished': ['Semi-Furnished'],
                'furnished': ['Furnished']
            }
        },
        reraIdExpired: {
            key: 'reraIdExpired',
            type: 'equal',
            options: {
                'false': false,
                'true': true
            }
        },
        projectUnderInvestigation: {
            key: 'projectUnderInvestigation',
            type: 'equal',
            options: {
                'false': false,
                'true': true
            }
        }
    },
    filters = {
        buyOrRent: {
            key: 'listingCategory',
            type: 'equal',
            options: {
                'buy': ['Primary', 'Resale'],
                'rent': ['Rental'],
                'commercialBuy': ['Primary', 'Resale'],
                'commercialLease': ['Rental']
            }
        },
        beds: {
            key: 'bedrooms',
            type: 'equal',
            options: {
                '1': [1],
                '2': [2],
                '3': [3],
                '4': [4],
                '5': [5],
                '6': [6],
                '3plus': [4, 5, 6, 7, 8, 9, 10]
            }
        },
        baths: {
            key: 'bathrooms',
            type: 'equal',
            options: {
                '1': [1],
                '2': [2],
                '3': [3],
                '1plus': [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
                '2plus': [2, 3, 4, 5, 6, 7, 8, 9, 10],
                '3plus': [3, 4, 5, 6, 7, 8, 9, 10]
            }
        },
        sellerRating: {
            key: 'listingSellerCompanyScore',
            type: 'range',
            getRange: function(filterValue) {
                var filterValueMapping ={
                '1plus': 1,
                '2plus': 2,
                '3plus': 3,
                '4plus': 4,
                'any':0
                }
                return {
                    from: filterValueMapping[filterValue]
                };
            }
        },
        propertyType: {
            key: 'unitTypeId',
            type: 'equal',
            options: {
                'apartment': [1],
                'builder-floor': [7],
                'independent-floor': [7],
                //'studio-apartment': [9],
                'independent-house': [19],
                'villa': [2],
                'residential-plot': [3],
                'farmhouse': [8],
                //'penthouse': [20],
                'shop': [5],
                'office': [6]
            }
        },
        postedBy: {
            key: function(value){
                if(value=='selectAgent')
                    return 'listingSellerMiscellaneousProducts'
                else
                    return 'listingSellerCompanyType';
            },
            type: 'equal',
            options: {
                'builder': ['Builder'],
                'owner': ['Owner'],
                'agent': ['Broker'],
                'selectAgent':['MakaanSelect']
            }
        },
        ownership: {
            key: 'listingOwnershipTypeId',
            type: 'equal',
            options: {
                'freehold': [1],
                'leasehold': [2],
                'power-of-attorney': [3],
                'cooperative-society': [4]
            }
        },
        propertyStatus: {
            key: 'listingConstructionStatusId',
            type: 'equal',
            options: {
                'ready-to-move': [2],
                'under-construction': [1]
            }
        },
        budget: {
            key: 'price',
            type: 'range',
            getRange: function(from, to) {
                return {
                    from: from,
                    to: to
                };
            }
        },
        area: {
            key: 'size',
            type: 'range',
            getRange: function(from, to) {
                return {
                    from: from,
                    to: to
                };
            }
        },
        floor: {
            key: 'floor',
            type: 'range',
            getRange: function(from, to) {
                return {
                    from: from,
                    to: to
                };
            }
        },
        availability: {
            key: 'listingPossessionDate',
            type: 'range',
            getRange: function(days) {

                if (days === 'immediate') {
                    days = 0;
                } else if (isNaN(days)) {
                    return;
                }

                let returnObj = {
                    from: utilService.dateAhead(null, null, false),
                    to: utilService.dateAhead(null, {
                        days: (days * 1)
                    }, false)
                };

                if (days == '0' || days === 0) {
                    delete returnObj.from;
                }
                return returnObj;
            }
        },
        possession: {
            key: 'listingPossessionDate',
            type: 'range',
            getRange: function(years) {
                return {
                    from: utilService.dateAhead(null, null, false),
                    to: utilService.dateAhead(null, {
                        years: (years * 1)
                    }, false)
                };
            },
            setFilters: function(years, temp) {
                let filterObj, rangeObj = this.getRange(years);

                if (years === 'any') {
                    filterObj = {
                        'type': 'equal',
                        'key': 'listingConstructionStatusId',
                        'value': filters.propertyStatus.options['under-construction']
                    };
                    temp.filters.push(filterObj);
                    return;
                }

                filterObj = {
                    'type': 'range',
                    'key': this.key,
                    'from': rangeObj.from,
                    'to': rangeObj.to
                };
                temp.filters.push(filterObj);
            }
        },
        ageOfProperty: {
            key: 'listingMaxConstructionCompletionDate',
            type: 'range',
            getRange: function(years) {
                years = years.split(',');
                let toYear = years[0] ? parseInt(years[0]) : null,
                    fromYear = years[1] ? parseInt(years[1]) : null;

                if (fromYear) {
                    fromYear = utilService.dateAhead(null, {
                        years: (fromYear * -1)
                    }, false);
                }

                if (toYear || toYear === 0) {
                    let diff = (toYear === 0) ? null : { years: (toYear * -1) };
                    toYear = utilService.dateAhead(null, diff, false);
                }

                return {
                    from: fromYear,
                    to: toYear
                };
            },
            setFilters: function(years, temp) {
                let filterObj, timeStamps = this.getRange(years);

                if (years === 'any') {
                    filterObj = {
                        'type': 'equal',
                        'key': 'listingConstructionStatusId',
                        'value': filters.propertyStatus.options['ready-to-move']
                    };
                    temp.filters.push(filterObj);
                    return;
                }

                if (timeStamps.from) {
                    filterObj = {
                        'type': 'range',
                        'key': 'listingMinConstructionCompletionDate',
                        'from': timeStamps.from
                    };
                    temp.filters.push(filterObj);
                }

                if (timeStamps.to) {
                    filterObj = {
                        'type': 'range',
                        'key': 'listingMaxConstructionCompletionDate',
                        'to': timeStamps.to
                    };
                    temp.filters.push(filterObj);
                }
            }
        },
        newOrResale: {
            key: 'listingCategory',
            type: 'equal',
            options: {
                'new': ['Primary'],
                'primary': ['Primary'],
                'resale': ['Resale'],
                'both': ['Primary', 'Resale'],
                'rent':['Rental']
            }
        },
        currentlyLeasedOut: {
            key: 'currentlyLeasedOut',
            type: 'equal',
            options: {
                'yes': true,
                'no': false            
            }
        },
        furnished: {
            key: 'listingFurnished',
            type: 'equal',
            options: {
                'unfurnished': ['Unfurnished'],
                'semifurnished': ['Semi-Furnished'],
                'furnished': ['Furnished']
            }
        },
        amenities: {
            key: 'listingAmenitiesIds',
            type: 'equal',
            options: {
                'swimmingpool': ['swimmingpool'],
                'gym': ['gym'],
                'joggingpark': ['joggingpark'],
                'basketballcourt': ['basketballcourt'],
                'parking': ['parking']
            }
        },
        listingAmenitiesIds:{
            key: 'listingAmenitiesIds',
            type: 'exact'
        },
        listingViewDirectionIds:{
            key: 'listingViewDirectionIds',
            type: 'exact'
        },
        facingId: {
            key: 'facingId',
            type: 'exact'
        },
        rk: {
             key: 'rk',
             type: 'exact'
        },
        latitude: {
            key: 'latitude',
            type: 'range',
            getRange: function(from, to) {
                return {
                    from: from,
                    to: to
                };
            }
        },
        longitude: {
            key: 'longitude',
            type: 'range',
            getRange: function(from, to) {
                return {
                    from: from,
                    to: to
                };
            }
        },
        listingId: {
            key: 'listingId',
            type: 'exact',
            regex: /^\d{6}$/,
            exclusive: true
        },
        notListingId: {
            key: 'listingId',
            type: 'notEqual'
        },
        locality: {
            key: 'localityId',
            type: 'exact'
        },
        builder: {
            key: 'builderId',
            type: 'exact'
        },
        city: {
            key: 'cityLabel',
            type: 'exact'
        },
        paidSellerUserIds: {
            key: 'sellerId',
            type: 'exact'
        },
        ups: {
            key: "listingSellerBenefits",
            type: "notEqual",
            options:{
                'true': ["LocalityExpertDealMaker", "CityExpertDealMaker", "ExpertDealMaker", "DealMaker", "PrepaidSeller"]
            }
        },
        assist: {
            key: 'listingSellerCompanyAssist',
            type: 'equal',
            options: {
                'true': ['true']
            }
        },
        doubleBeds: {
            key: 'listingFurnishingIds',
            type: 'equal',
            options: {
                'true': [2]
            }
        },
        furnishings: {
            key: 'listingFurnishingIds',
            type: 'multipleEqual',
            options: {
                'wardrobe': [1],
                'double-bed': [2],
                'tv': [3],
                'refrigerator': [4],
                'sofa': [5],
                'washing-machine': [6],
                'wifi': [7],
                'microwave': [8],
                'dining-table': [9],
                'gas-connection': [10],
                'ac': [11],
                'bed': [12]
            }
        },
        restrictions: {
            key: 'listingTenantTypeIds',
            type: 'multipleEqual',
            options: {
                'bachelors': [1],
                'family': [2],
                'pets-allowed': [3],
                'non-vegetarian': [4],
                'company-lease': [5]
            }
        },
        listingSellerDisplayAds: {
            key: 'listingSellerDisplayAds',
            type: 'range',
            getRange: function(from, to) {
                return {
                    from: from,
                    to: to
                };
            }
        },
        listingSellerTransactionStatuses: {
            key: 'listingSellerTransactionStatuses',
            type: 'equal',
            options: {
                'paidSellers': ["BUILDER_EXCLUSIVE_CAMPAIGN_SELLER","CAMPAIGN_SELLER"]
            }
        },
        sellerLeadDeficitScore: {
            key: 'sellerLeadDeficitScore',
            type: 'range',
            getRange: function(from) {
                return {
                    from: from
                };
            }
        }
    },
    sorting = {
        queryKey: 'sortBy',
        options: {
            'price': 'price',
            'rating': 'listingSellerCompanyScore',
            'popularity': 'listingPopularityScore',
            'date': 'listingDisplayDate',
            'distance': 'geoDistance',
            'possession': 'possessionDate',
            'score': 'projectLivabilityScore'
        },
        order: {
            'asc': 'ASC',
            'desc': 'DESC'
        }
    },
    projectSorting = {
        queryKey: 'sortBy',
        options: {
            'price': 'minimumFuncPrice',
            'popularity': 'listingPopularityScore',
            'relevance': 'averageFuncListingQualityScore',
            'date' : 'createdDate'
        },
        order: {
            'asc': 'ASC',
            'desc': 'DESC'
        },
        isJsonFacet: ['price','relevance']
    },
    fields = {
        queryKey: 'fields'
    },
    paging = {
        queryKey: 'page'
    };

function _createMultipleValueArray(value, isIntValue){
    let valueArray = [],
        temp;

    value = value ? value.toString().split(',') : [];

    for(let k=0; k<value.length; k++){
        temp = isIntValue ? parseInt(value[k]) : value[k];
        temp && valueArray.push(temp);  //jshint ignore:line
    }
    return (valueArray.length ? valueArray : null);
}

/* to add cityId, localityId based on url */
function _handleUrlDataForSelector(queryParams, filters, urlDetail) {
    let temp;
    if (!urlDetail) {
        return;
    }
    let keyValFilter = {};
    switch (urlDetail.pageType) {
        case 'STATE_URLS':
        case 'STATE_URLS_MAPS':
            keyValFilter.stateId = urlDetail.stateId;
            break;
        case 'CITY_URLS':
        case 'CITY_URLS_MAPS':
        case 'CITY_TAXONOMY_URLS':
        case 'CITY_TAXONOMY_URLS_MAPS':
        case 'CITY_URLS_COMMERCIAL':
            keyValFilter.cityId = queryParams.cityId || urlDetail.cityId;
            break;
        case 'PROJECT_URLS':
        case 'PROJECT_URLS_MAPS':
        case 'PROJECT_URLS_OVERVIEW':
            keyValFilter.cityId = queryParams.cityId || urlDetail.cityId;
            keyValFilter.projectId = urlDetail.projectId;
            keyValFilter.localityId = queryParams.localityId || urlDetail.localityId;
            keyValFilter.suburbId = queryParams.suburbId || urlDetail.suburbId;
            keyValFilter.listingCompanyId = _createMultipleValueArray(queryParams.sellerId, true);
            keyValFilter.sellerId = queryParams.sellerUserId;
            break;
        case 'LOCALITY_SUBURB_TAXONOMY': // to handle fallback for previous support
        case 'LOCALITY_SUBURB_TAXONOMY_MAPS': // to handle fallback for previous support
            keyValFilter.cityId = urlDetail.cityId;
            if (urlDetail.localityId) {
                keyValFilter.localityId = urlDetail.localityId;
            } else {
                keyValFilter.suburbId = urlDetail.suburbId;
            }
            break;

        case 'LOCALITY_URLS':
        case 'LOCALITY_URLS_MAPS':
        case 'LOCALITY_TAXONOMY_URLS':
        case 'LOCALITY_TAXONOMY_URLS_MAPS':
            keyValFilter.cityId = queryParams.cityId || urlDetail.cityId;
            keyValFilter.localityId = queryParams.localityId || urlDetail.localityId;
            break;
        case 'SUBURB_URLS':
        case 'SUBURB_URLS_MAPS':
        case 'SUBURB_TAXONOMY_URLS':
        case 'SUBURB_TAXONOMY_URLS_MAPS':
            keyValFilter.cityId = queryParams.cityId || urlDetail.cityId;
            keyValFilter.suburbId = queryParams.suburbId || urlDetail.suburbId;
            break;
        case 'BUILDER_URLS':
        case 'BUILDER_URLS_MAPS':
        case 'BUILDER_TAXONOMY_URLS':
        case 'BUILDER_TAXONOMY_URLS_MAPS':
            keyValFilter.cityId = queryParams.cityId || urlDetail.cityId;
            keyValFilter.builderId = urlDetail.builderId;
            break;
        case 'NEARBY_LISTING_URLS':
        case 'NEARBY_LISTING_URLS_MAPS':
            keyValFilter.cityId = queryParams.cityId || urlDetail.cityId;
            break;
        case 'COMPANY_URLS':
        case 'COMPANY_URLS_MAPS':
        case 'SELLER_PROPERTY_URLS':
        case 'SELLER_PROPERTY_URLS_MAPS':
        case 'SIMILAR_PROPERTY_URLS':
        case 'SIMILAR_PROPERTY_URLS_MAPS':
        case 'LISTINGS_PROPERTY_URLS':
        case 'LISTINGS_PROPERTY_URLS_MAPS':
        case 'CITY_URLS_OVERVIEW':
        case 'LOCALITY_URLS_OVERVIEW':
            keyValFilter.stateId = queryParams.stateId || urlDetail.stateId;
            keyValFilter.cityId = queryParams.cityId || urlDetail.cityId;
            keyValFilter.listingCompanyId = _createMultipleValueArray(queryParams.companyId || urlDetail.companyId || queryParams.sellerId, true);
            keyValFilter.localityId = queryParams.localityId;
            keyValFilter.suburbId = queryParams.suburbId;
            keyValFilter.builderId = queryParams.builderId;
            keyValFilter.projectId = queryParams.projectId;

            // removing builder id for project page (zero listing page conflict)
            if(keyValFilter.projectId){
                delete keyValFilter.builderId;
            }

            // remove suburbId when localityId present  to avoid includeNearbyResults of suburb
            if(keyValFilter.localityId && keyValFilter.suburbId){
                delete keyValFilter.suburbId;
            }

            if(queryParams.localityOrSuburbId){
                delete keyValFilter.suburbId;
                delete keyValFilter.localityId;
                let localityOrSuburbId = [],
                    localityOrSuburbIdArray = queryParams.localityOrSuburbId.split(',');

                for (let k = 0; k < localityOrSuburbIdArray.length; k++) {
                    localityOrSuburbId.push(parseInt(localityOrSuburbIdArray[k]));
                }
                keyValFilter.localityOrSuburbId = localityOrSuburbId;

            }
            break;
        case 'PROJECT_SERP':
        case 'PROJECT_SERP_DYNAMIC_URLS':
            keyValFilter.builderDbStatus = ["Active", "ActiveInMakaan"];
            keyValFilter.cityId = queryParams.cityId || urlDetail.cityId;
            keyValFilter.localityId = queryParams.localityId || urlDetail.localityId;
            keyValFilter.suburbId = queryParams.suburbId || urlDetail.suburbId;
            keyValFilter.builderId = queryParams.builderId || urlDetail.builderId;
            if (queryParams.localityOrSuburbId) {
                delete keyValFilter.suburbId;
                delete keyValFilter.localityId;
                let localityOrSuburbId = [],
                    localityOrSuburbIdArray = queryParams.localityOrSuburbId.split(',');

                for (let k = 0; k < localityOrSuburbIdArray.length; k++) {
                    localityOrSuburbId.push(parseInt(localityOrSuburbIdArray[k]));
                }
                keyValFilter.localityOrSuburbId = localityOrSuburbId;
            }
            break;
    }
    if (urlDetail.serpType == 'listing') {
        keyValFilter.listingCategory = ["Primary", "Resale"];
        if (urlDetail && urlDetail.listingType && (urlDetail.listingType.toLowerCase() === "rent" || urlDetail.listingType.toLowerCase() === "commerciallease")) {
            keyValFilter.listingCategory = ["Rental"];
        } else if ((queryParams.newOrResale) || (urlDetail.filters && urlDetail.filters.newOrResale)) {
            delete keyValFilter.listingCategory;
        } else if (urlDetail.isSEMLightPage) {
            keyValFilter.listingCategory = ["Primary"];
        }
    }
    if (urlDetail.serpType == 'project') {
      queryParams.sortBy = 'date-desc';
    }

    for (let key in keyValFilter) {
        if (keyValFilter[key]) {
            temp = {
                'type': 'equal',
                'key': key,
                'value': Array.isArray(keyValFilter[key]) ? keyValFilter[key] : _convertToNumber(keyValFilter[key])
            };
            filters.push(temp);
        }
    }
}

function _convertToNumber(value) {
    let numValue = parseFloat(value);
    numValue = isNaN(numValue) ? value : numValue;
    return numValue;
}

function _getSelector(queryParams, filters, sort, urlDetail, itemsPerPage) {
    let temp = queryParams.filters ? queryParams.filters : { filters: [] };
    let taxonomyFilters = urlDetail.filters || {};

    _handleUrlDataForSelector(queryParams, temp.filters, urlDetail);
    for (var filterKey in filters) {
        var vFilter = filters[filterKey],
            filterValue = queryParams[filterKey] || taxonomyFilters[filterKey],
            filterObj;

        if (!filterValue) {
            continue;
        }
        switch (vFilter.type) {

            case 'equal':
                {
                    let sOptions = [],
                        optionArray;

                    if(typeof filterValue == "object"){
                        optionArray = filterValue;
                    } else {
                        optionArray = filterValue.split(",");
                    }

                    for (let i in optionArray) {
                        let option = optionArray[i];
                        if ([undefined,null,""].indexOf(vFilter.options[option])<0) {
                            sOptions = sOptions.concat(vFilter.options[option]);
                        }
                    }

                    if (sOptions.length) {
                        filterObj = {
                            'type': 'equal',
                            'key': typeof(vFilter.key)=='function'?vFilter.key(filterValue):vFilter.key,
                            'value': sOptions
                        };
                        temp.filters.push(filterObj);
                    }

                    break;
                }

            case 'multipleEqual':
                {
                    let sOptions = [],
                        optionArray = filterValue.split(',');

                    for (let i in optionArray) {
                        let option = optionArray[i];
                        if (vFilter.options[option]) {
                            sOptions = sOptions.concat(vFilter.options[option]);
                        }
                    }

                    for (let sOption in sOptions) {
                        filterObj = {
                            'type': 'equal',
                            'key': typeof(vFilter.key)=='function'?vFilter.key(filterValue):vFilter.key,
                            'value': [sOptions[sOption]]
                        };
                        temp.filters.push(filterObj);
                    }

                    break;
                }
            case 'range':
                {
                    var rangeObj;

                    if (['ageOfProperty', 'possession'].indexOf(filterKey) > -1) {
                        vFilter.setFilters(filterValue, temp);
                        break;
                    } else if (filterValue.indexOf(',') > -1) {
                        let rangeVal = filterValue.split(',');
                        if ((vFilter.key == 'latitude' || vFilter.key == 'longitude') && (rangeVal.length !== 2 || queryParams.path)) {
                            break;
                        }
                        rangeObj = vFilter.getRange(rangeVal[0], rangeVal[1]);
                    } else {
                        rangeObj = vFilter.getRange(filterValue);
                    }

                    if (rangeObj) {
                        filterObj = {
                            'type': 'range',
                            'key': typeof(vFilter.key)=='function'?vFilter.key(filterValue):vFilter.key,
                            'from': _convertToNumber(rangeObj.from),
                            'to': _convertToNumber(rangeObj.to)
                        };
                        temp.filters.push(filterObj);
                    }

                    break;
                }
            case 'exact':
                {
                    if (vFilter.regex && !vFilter.regex.test(filterValue)) {
                        break;
                    }
                    if (vFilter.exclusive) {
                        temp.filters.length = 0;
                    }
                    var optionArray = filterValue.split(',');
                    if (optionArray.length) {
                        filterObj = {
                            'type': 'equal',
                            'key': typeof(vFilter.key)=='function'?vFilter.key(filterValue):vFilter.key,
                            'value': optionArray
                        };
                        temp.filters.push(filterObj);
                    }
                    break;
                }
            case 'notEqual':
                {
                        let sOptions = vFilter.options,
                            value = filterValue;
                        if(sOptions){
                            value = sOptions[filterValue];
                        }
                        if(!value){
                            continue;
                        }
                        filterObj = {
                            'type': 'notEqual',
                            'key': typeof(vFilter.key)=='function'?vFilter.key(filterValue):vFilter.key,
                            'value': value
                        };
                        temp.filters.push(filterObj);

                    break;
                }

        }
        if (vFilter.exclusive) {
            break;
        }
    }

    // Sort
    var sortFilter = queryParams[sort.queryKey];
    if (sortFilter) {
        var sortArray = sortFilter.split('-'),
            sortBy = sortArray[0],
            sortOrder = sortArray[1];
        var option = sort.options[sortBy],
            order = sort.order['desc'];
        if (sortOrder && sort.order[sortOrder]) {
            order = sort.order[sortOrder];
        }

        //ignore distance sort if not landmark case
        if ((option === 'geoDistance') && (queryParams.path || (!(urlDetail.placeId || queryParams.placeId)))) {
            option = false;
        }

        //in case of projectSerp use jsonFacet object for sorting
        if (option && sort.isJsonFacet && sort.isJsonFacet.indexOf(sortBy) > -1) {
            temp.jsonFacet = {
                'key': option,
                'order': order
            };
        } else if (option) {
            temp.sort = {
                'key': option,
                'order': order
            };
        }
    }

    if(urlDetail.sortBy && urlDetail.sortBy.field && urlDetail.sortBy.sortOrder){ // taxonomy sort available override other sorting parameters
        temp.sort = {
            'key': urlDetail.sortBy.field,
            'order': urlDetail.sortBy.sortOrder
        };
    }

    // to get only specific fields like images in gallery module but api not supporting
    //fields
    if (queryParams[fields.queryKey]) {

        let requiredFields;
        if (Array.isArray(queryParams[fields.queryKey])) {
            requiredFields = queryParams[fields.queryKey];
        } else {
            requiredFields = queryParams[fields.queryKey].split(',');
        }

        temp.fields = requiredFields;
    }

    // Paging
    var page = queryParams[paging.queryKey],
        rows = itemsPerPage || globalConfig.itemsPerPage;

    if (!page || page < 1) {
        page = 1;
    }
    var start = (page - 1) * rows;

    temp.paging = {
        start: start,
        rows: rows
    };
    if(queryParams.groupByField){
        temp.groupBy = queryParams.groupByField;
    }
    return apiService.createSelector(temp);
}

module.exports.getClientSortParameter = function(key, sortType = 'listing'){
    let sortObj;
    if(!key){
        return;
    }
    if(sortType == 'listing'){
        sortObj = sorting;
    }else if(sortType == 'project'){
        sortObj = projectSorting;
    }else{
        return key;
    }
    let result = '';
    utilService._.forEach(sortObj.options, function(v, k){
        if(v == key){
            result = k;
            return;
        }
    });
    return result;
};

module.exports.getSelectorQuery = function(queryParams, urlDetail, itemsPerPage) {
    let selector = _getSelector(queryParams, filters, sorting, urlDetail, itemsPerPage);
    logger.info("listing selector=" + selector);
    let selectorQueryObj = {
        selector
    };
    // if user has drawn on the maps then ignore nearby url
    if (queryParams.path) {
        selectorQueryObj.gep = queryParams.path;
    } else if (urlDetail.placeId) { // if nearby url then apply google place Id filter to api
        selectorQueryObj.gpid = urlDetail.placeId;
        // to handle draw your region string in resedential-property url
    } else if (queryParams.placeId && urlDetail && urlDetail.pageType && urlDetail.pageType.indexOf('LISTINGS_PROPERTY_URLS') > -1) {
        selectorQueryObj.gpid = queryParams.placeId;
    }
    return selectorQueryObj;
};

module.exports.getProjectSerpSelectorQuery = function(queryParams, urlDetail, itemsPerPage) {
    let selector = _getSelector(queryParams, projectFilters, projectSorting, urlDetail, itemsPerPage);
    logger.info("projectSerp selector=" + selector);
    let selectorQueryObj = {
        selector
    };
    return selectorQueryObj;
};
