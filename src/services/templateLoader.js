'use strict';
const marko = require('marko'),
    path = require('path'),
    debuggerService = require('services/debuggerService'),
    utils = require('services/utilService'),
    seoService      = require('services/seoService'),
    logger = require('services/loggerService'),
    linkTags = require('services/linkTags'),
    globalConfig = require('configs/globalConfig'),
    sharedConfig = require('public/scripts/common/sharedConfig'),
    ampTemplate = require('configs/ampTemplate'),
    _ = utils._;

var appLocals,
 JARVIS_URL,
 SELLER_URL,
 WEBSITE_URL,
 BASE_URL,
 FB,
 GPLUS,
 GMAP_KEY,
 NEW_RELIC_APP_ID,
 GPLUS_REDIRECT_URL,
 FACEBOOK_PIXEL_TRACKING_ID,
 GA_TRACKING_ID,
 ONE_SIGNAL_APP_ID,
 AMPLITUDE_TRACKING_ID,
 EVENT_ROUTER_URL,
 PIXEL_ACCOUNT_ID,
 MIXPANEL_TRACKING_ID,
 DTM_TRACKING_ID,
 GLOBAL_VIRTUAL_NUMBER,
 CDN_URL_CSS,
 downloadAppLink,
 VWO_ID,
 PROPTIGER_URL,
 HOUSING_URL,
 OSM_TILE_URL,
 OSM_ROUTER_URL;


module.exports.setup = function(app) {
    appLocals = app.locals;
    appLocals.environment = app.get('env').trim();
    JARVIS_URL = process.env.JARVIS_URL;
    BASE_URL = process.env.BASE_URL;
    WEBSITE_URL = process.env.WEBSITE_URL;
    CDN_URL_CSS = process.env.CDN_URL_JS; // as being used in Gruntfile also so dont'change
    SELLER_URL = process.env.SELLER_URL;
    PROPTIGER_URL = process.env.PROPTIGER_URL;
    HOUSING_URL = process.env.HOUSING_URL;
    FB = process.env.FB_APP_ID;
    GPLUS = process.env.GAPI_CLIENT_ID;
    GMAP_KEY = process.env.GMAP_KEY;
    GPLUS_REDIRECT_URL = process.env.GAPI_REDIRECT_URL;
    NEW_RELIC_APP_ID = process.env.NEW_RELIC_APP_ID;
    GA_TRACKING_ID = process.env.GA_TRACKING_ID;
    ONE_SIGNAL_APP_ID = process.env.ONE_SIGNAL_APP_ID;
    AMPLITUDE_TRACKING_ID = process.env.AMPLITUDE_TRACKING_ID;
    EVENT_ROUTER_URL = process.env.BASE_URL_EVENTROUTER;
    FACEBOOK_PIXEL_TRACKING_ID = process.env.FACEBOOK_PIXEL_TRACKING_ID;
    PIXEL_ACCOUNT_ID = process.env.PIXEL_ACCOUNT_ID;
    MIXPANEL_TRACKING_ID = process.env.MIXPANEL_TRACKING_ID;
    DTM_TRACKING_ID = process.env.DTM_TRACKING_ID;
    GLOBAL_VIRTUAL_NUMBER = process.env.GLOBAL_VIRTUAL_NUMBER;
    VWO_ID = process.env.VWO_ID;
    OSM_TILE_URL=process.env.OSM_TILE_URL;
    OSM_ROUTER_URL=process.env.OSM_ROUTER_URL;
    downloadAppLink = "/download-mobileapp";
};

function appendSlash(CDN) {
    var NEW_URL = '';
    if (CDN) {
        NEW_URL = CDN[CDN.length - 1] != '/' ? CDN + '/' : CDN;
    } else {
        NEW_URL = '/';
    }
    return NEW_URL;
}

function _setTemplateData(res, req) {
    let layoutData = {},
        seoData = {},
        data = {},
        isMobileRequest = utils.isMobileRequest(res.req);

    for (let key in res.locals) {
        data[key] = res.locals[key];
    }
    for (let key in appLocals) {
        data[key] = appLocals[key];
    }
    layoutData.JARVIS_URL = JARVIS_URL;
    layoutData.BASE_URL = BASE_URL;
    layoutData.GMAP_KEY = GMAP_KEY;
    layoutData.WEBSITE_URL = WEBSITE_URL;
    layoutData.VWO_ID = VWO_ID;
    layoutData.CDN_URL_CSS = appendSlash(CDN_URL_CSS);
    layoutData.downloadAppBuyerLink = downloadAppLink;
    layoutData.downloadAppSellerLink = "/app-for-sell-rent-property-online";
    layoutData.prefixToUrl = utils.prefixToUrl;
    layoutData.OSM_TILE_URL = OSM_TILE_URL;
    layoutData.OSM_ROUTER_URL = OSM_ROUTER_URL;
    /* commenting the download app conditional link for mobile as onelink is not working now -- check globalConfig file */
    // if (isMobileRequest) {
    //     layoutData.downloadAppBuyerLink = globalConfig.downloadAppBuyerLink;
    //     layoutData.downloadAppSellerLink = globalConfig.downloadAppSellerLink;
    // }
    layoutData.pageData = res.req && res.req.pageData || {};
    layoutData.pageData.sellerMicrosite = SELLER_URL;
    layoutData.pageData.trackPageType = sharedConfig.PAGE_TYPE_MAP[layoutData.pageData.pageType];
    layoutData.query = res.req.query;
    layoutData.sellerMicrosite = SELLER_URL;
    layoutData.proptigerUrl = PROPTIGER_URL;
    layoutData.housingUrl = HOUSING_URL;
    layoutData.isMobileRequest = isMobileRequest;
    layoutData.bodyTimeout = 60000;
    layoutData.timeout = 10000;
    layoutData.currentTime = Date.now();
    layoutData.meta = res.req.meta || {};
    layoutData.seoPrevUrl = res.req.seoPrevUrl;
    layoutData.seoNextUrl = res.req.seoNextUrl;
    layoutData.isLocal = (data.environment == 'localhost' || data.environment == 'development');

    if (!layoutData.meta.title) {
        let title = layoutData.pageData && layoutData.pageData.pageType === 'BUYER_DASHBOARD' ? 'my journey' : 'properties';
        title += layoutData.pageData && layoutData.pageData.displayAreaLabel ? ` ${layoutData.pageData.displayAreaLabel}` : '';
        title += ':Makaan.com';

        layoutData.meta.title = title;
    }

    layoutData.social = {
        fb: FB,
        gplus: GPLUS,
        redirectUrl: GPLUS_REDIRECT_URL
    };
    layoutData.GA_TRACKING_ID = GA_TRACKING_ID;
    layoutData.ONE_SIGNAL_APP_ID = ONE_SIGNAL_APP_ID;
    layoutData.AMPLITUDE_TRACKING_ID = AMPLITUDE_TRACKING_ID;
    layoutData.FACEBOOK_PIXEL_TRACKING_ID = FACEBOOK_PIXEL_TRACKING_ID;
    layoutData.IRIS_PROJECT_ID = "makaanbuyer";
    layoutData.EVENT_ROUTER_URL = EVENT_ROUTER_URL;
    layoutData.PIXEL_ACCOUNT_ID = PIXEL_ACCOUNT_ID;
    layoutData.MIXPANEL_TRACKING_ID = MIXPANEL_TRACKING_ID;
    layoutData.DTM_TRACKING_ID = DTM_TRACKING_ID;
    layoutData.GLOBAL_VIRTUAL_NUMBER = GLOBAL_VIRTUAL_NUMBER;

    layoutData.bodyClass = '';
    if (!isMobileRequest && layoutData.pageData && layoutData.pageData.isMap) {
        layoutData.bodyClass = 'dmap-stopscroll';
    }
    //add country code for price icon change on all pages
    layoutData.currencyClass = ' '+ (req.locals && req.locals.numberFormat && req.locals.numberFormat.code || '');
    
    layoutData.NEW_RELIC_APP_ID = NEW_RELIC_APP_ID;
    data.trimString = utils.trimString;
    layoutData.googlePlayShortLink = globalConfig.googlePlayShortLink;
    layoutData.disablePWA = globalConfig.disablePWA; //flag to deactivate service worker (pwa)
    layoutData.disableVWO = globalConfig.disableVWO;

    layoutData.makaanSelectCities = sharedConfig.makaanSelectCities;

    data.isMobileRequest = isMobileRequest;
    data.params = res.req && res.req.params ? res.req.params : {};

    if (!utils.isEmptyObject(data.params) && data.params.postName) {
        res.req["bread-crum"].push([{
            "label": data.params.postName,
            "url": '#'
        }]);
    }

    layoutData.linkTags = linkTags;
    layoutData.numberFormat = req.locals && req.locals.numberFormat || {};
    seoData.breadcrumLinks = res.req["bread-crum"] && seoService.parseBreadcrum(res.req["bread-crum"], layoutData);
    seoData.footer = (res.req.footer && _groupFooters(res.req.footer, res.req)) || {};
    seoData.footerDescription = res.req.footerDescription;
    seoData.headerData = seoService.headerParser(res.req);
    // seoData.qnaData = res.req["qna-data"] && seoService.qnaParser(res.req);
    // seoData.taxonomyLinks = res.req["navigation"] && seoService.parseNavigationLink(res.req["navigation"]);
    data.layoutData = layoutData;
    data.seoData = seoData;

    return data;
}

function _trafficDropHack(req, footer={}){
    var removeSectionsContainingWords = ["amenties","amenities","facing","luxury","1rk","furnished","budget","nearbuy","township","house","upcoming", "under construction"];
    utils._.forEach(footer, (v, k)=>{
        utils._.forEach(removeSectionsContainingWords, (val)=>{
            if(k && k.toString().toLowerCase().indexOf(val) > -1){
                delete footer[k];
            }
        });
    });
    var seoFooter = [], finalFooter = {}, insertFlag = true;
    utils._.assign(finalFooter, footer);

    utils._.forEach(footer, (v, k)=>{
        finalFooter[k] = [];
        utils._.forEach(v, (link)=>{
            insertFlag = true;
            utils._.forEach(removeSectionsContainingWords, (val)=>{
                if(link && link.label && link.label.toLowerCase().indexOf(val)>-1){
                    insertFlag = false;
                    return false;
                }
            });
            insertFlag && finalFooter[k].length < 10 && finalFooter[k].push(link); // jshint ignore:line
        });
        seoFooter.push({[k]:finalFooter[k]});
    });

    //to break sets and push at end if size's > 5
    _.forEach(seoFooter, (entityValue,entityKey)=>{
        _.forEach(entityValue, (v,k)=>{
            if(v.length>5){
                seoFooter.push({
                    [k]: v.splice(5,v.length)
                });
            }
        });
    });

    return seoFooter;
}

function symmetricSeoFooter(footer={},labelMap=[],pageLevel=""){

    let groupMergeByPageLevel = {
        city: ["city-taxonomy","other-footer-urls"],
        locality: ["locality-taxonomy","other-footer-urls"],
        suburb : ["suburb-taxonomy","other-footer-urls"],
        project: ["city-taxonomy","other-footer-urls"],
        property: ["city-taxonomy","other-footer-urls"],
        cityallBuilder: ["city-taxonomy","other-footer-urls"],
        citytopBuilder: ["city-taxonomy","other-footer-urls"],
        cityallBroker : ["city-taxonomy","other-footer-urls"],
        citytopBroker : ["city-taxonomy","other-footer-urls"],
        cityPriceTrend : ["city-taxonomy","other-footer-urls"]
    };
    
    let mergeGroups = groupMergeByPageLevel[pageLevel] || [];
    let mergeGroupsLength = mergeGroups.length;

    let seoFooter = [];
    let taxonomyKeyIndex = [];

    let extraTaxonomyCounter = 0;

    for(let i = 1; i < mergeGroupsLength; i++){
        footer[mergeGroups[0]] = footer[mergeGroups[0]] || [];
        footer[mergeGroups[0]] = footer[mergeGroups[0]].concat(footer[mergeGroups[i]] || []);
        delete footer[mergeGroups[i]];
    }

    _.forEach(footer,(v, k)=>{
        if(v && v.length > 0 ){
            if(k.indexOf("taxonomy") == -1){
                seoFooter.push({
                    [k]:_.cloneDeep(v)
                });
            }else{
                let i = 0;
                while(v.length){
                    seoFooter.push({
                        [labelMap[extraTaxonomyCounter%labelMap.length]] : v.splice(0,5)
                    });
                    taxonomyKeyIndex.push(seoFooter.length-1);
                    extraTaxonomyCounter++;
                }
            }
        }
    });

    //to push untitled footer links at end
    _.forEach(taxonomyKeyIndex, (v, k)=>{
        utils.move(seoFooter, v-k,seoFooter.length-1);
    });

    //to break sets and push at end if size's > 5
    _.forEach(seoFooter, (entityValue,entityKey)=>{
        _.forEach(entityValue, (v,k)=>{
            if(v.length>5){
                seoFooter.push({
                    [k]: v.splice(5,v.length)
                });
            }
        });
    });

    //to push low sized ones at end
    taxonomyKeyIndex.splice(0,taxonomyKeyIndex.length);
    for(var i=0, len=seoFooter.length; i<len; ++i){
        _.forEach(seoFooter[i], (v,k)=>{
            if(v.length<3){
                taxonomyKeyIndex.push(i);
            }
        });
    }
    _.forEach(taxonomyKeyIndex, (v, k)=>{
        utils.move(seoFooter, v-k,seoFooter.length-1);
    });

    return seoFooter;
}

function _groupFooters(footerObj, req) {
    if (!utils.isEmptyObject(footerObj)) {
        let globalFooter = footerObj.global || {},
            globalKeys = Object.keys(globalFooter),
            seoFooter = footerObj.seo || {},
            allFooter_pageLevel=[
            'project',
            'property',
            'state',
            'city',
            'cityallBuilder',
            'citytopBuilder',
            'cityallBroker',
            'citytopBroker',
            'cityPriceTrend',
            'citytopBrokerBuy',
            'citytopBrokerRent',
            'cityallBrokerBuy',
            'cityallBrokerRent',
            'locality',
            'localityallBroker',
            'localitytopBroker',
            'localitytopBrokerBuy',
            'localitytopBrokerRent',
            'localityallBrokerBuy',
            'localityallBrokerRent',
            'localityPriceTrend',
            'localityTaxonomy',
            'suburb',
            'suburbtopBrokerBuy',
            'suburbtopBrokerRent',
            'suburballBrokerBuy',
            'suburballBrokerRent',
            'suburbPriceTrend',
            'suburbTaxonomy'
            ];
        globalFooter['city-overview'] = ((globalFooter['city-overview'] || []).slice(0, 9)).concat((globalFooter['city-overview'] || []).slice(-1)); //first 10 are shown in header
        if(globalFooter["static"]){ // puttting static global links under quick links footer section
            footerObj.quickLinks = globalFooter["static"];
            delete globalFooter["static"];
            globalKeys = Object.keys(globalFooter);
        }
        for (let i = 1, length = globalKeys.length; i < length; i++) { //global footer to have only one object
            let currKey = globalKeys[i];
            seoFooter[currKey] = globalFooter[currKey];
            delete globalFooter[currKey];
        }
        footerObj.global = globalFooter;
        footerObj.taxonomyLabel = utils._.values(footerObj['footer-taxonomy-label']);
        if(allFooter_pageLevel.indexOf(req.urlDetail.pageLevel) > -1){
            let abstractPageLevel = req.urlDetail.pageLevel;
            if(abstractPageLevel.indexOf('city')>-1){
                abstractPageLevel = 'city';
            } else if(abstractPageLevel.indexOf('locality')>-1){
                abstractPageLevel = 'locality';
            } else if(abstractPageLevel.indexOf('suburb')>-1){
                abstractPageLevel = 'suburb';
            } else {
                abstractPageLevel = req.urlDetail.pageLevel;
            }
            footerObj.seo = symmetricSeoFooter(seoFooter,footerObj.taxonomyLabel,abstractPageLevel);
        }
        else
        {
            footerObj.seo = _trafficDropHack(req, seoFooter);
        }
        footerObj['footer-label'] = footerObj['footer-label'] || {};
        footerObj['footer-label'].static = 'Properties in India';
    }

    return footerObj;
}


function _setHeaders(res, contentType) {
    if (!res.headersSent) {
        if (contentType) {
            res.setHeader('content-type', contentType);
        } else {
            res.setHeader('content-type', 'text/html');
        }
        res.setHeader('originalUrl', res.req.originalUrl);
        res.setHeader('skipAjaxyCaching', res.req && res.req.skipAjaxyCaching || false);
        // if(res && res.req && !res.req.isAmp && !res.req.bot) {          
        //     // please do not rename  serverPushPath and serverPush_version as they are keys in gruntfile for revisioning    
        //     var serverPushPath = [],    
        //         serverPushFiles = '',   
        //         serverPushNames = '';   
        //     serverPushPath.push('/scripts/infra.js'); //serverPush_version  
        //     serverPushPath.push('/scripts/dependency/commonBundle.js'); //serverPush_version    
        //     serverPushPath.push('/scripts/app.js'); //serverPush_version    
        //     serverPushPath.forEach(function(spf){   
        //         serverPushFiles += '<'+spf+'> ; as=script ; rel=preload,'   
        //         serverPushNames += spf.split('/').pop()+'_' 
        //     }); 
        //     serverPushFiles = serverPushFiles.slice(0,-1) + ';';    
        //     serverPushNames = serverPushNames.slice(0,-1);  
        //     res.setHeader('link',serverPushFiles);  
        //     res.setHeader('X-serverPush', serverPushNames); 
        //     res.cookie('pushHeader', serverPushNames);  
        // }
        if(res.req){
            res.req.isMarko = true;
        }
        utils.setCacheHeader(res);
    }
    return;
}

function _debugHandler(req, res, options = {}) {
    let data = options.data;
    let template = options.template;
    let finalObj = {
        primaryApis: {},
        asyncApis: {},
        apiTime: 0
    };

    let beforeRenderTime = Date.now();
    finalObj.apiTime = beforeRenderTime - req.apiTime;
    finalObj.primaryApis = utils.deepCopy(req.apiMap, finalObj.primaryApis);
    req.apiMap = {};

    //Execute template.renderOriginal to catch templating errors in debug mode
    template.renderOriginal(data, () => {
        finalObj.templateTime = Date.now() - req.apiTime;
        finalObj.asyncApis = utils.deepCopy(req.apiMap, finalObj.asyncApis);
        res.send(debuggerService.parse(finalObj));
    });
}

module.exports.loadTemplate = function(moduleDir, templatePath = '') {

    let re = new RegExp("^([a-zA-Z0-9-/.]+.marko)");
    let filePath;

    if (re.test(templatePath)) {
        filePath = path.resolve(path.join(moduleDir, templatePath));
    } else {
        filePath = path.resolve(path.join(moduleDir, templatePath, '/view.marko'));
    }

    const template = marko.load(filePath);

    if (!template.renderOriginal) {
        template.renderOriginal = template.render;

        template.renderAsync = function(data, res, contentType, req) {

            data = utils.deepCopy(_setTemplateData(res, req), data);

            if (!req) {
                logger.warn("Pass req object in render async call");
            }
            if ( (req && req.query && req.query.debug) || (res && res.req && res.req.query && res.req.query.debug) ) {
                //Pass template to catch templating errors in debug mode
                return _debugHandler(req, res, { data, template });
            } else {
                _setHeaders(res, contentType);

                //for explicit handling of template exceptions
                try {
                    return template.renderOriginal(data, res);
                } catch (ex) {
                    console.log(ex.stack);
                    return res.status(500).send(ex.message);
                }
            }
        };

        template.renderSync = function(data, res, callback, contentType, req) {

            if (!req) {
                logger.warn("Pass req object in render sync call");
            }
            data = utils.deepCopy(_setTemplateData(res, req), data);
            if ( (req && req.query && req.query.debug) || (res && res.req && res.req.query && res.req.query.debug) ) {
                //Pass template to catch templating errors in debug mode
                return _debugHandler(req, res, { data, template });
            } else {
                _setHeaders(res, contentType);

                //for explicit handling of template exceptions
                try {
                    if (callback && typeof callback === 'function') {
                        return template.renderOriginal(data, (err, output) => {
                            callback(err, output);
                        });
                    } else {
                        return template.renderOriginal(data, res);
                    }
                } catch (ex) {
                    console.log(ex.stack);
                    return res.status(500).send(ex.message);
                }
            }
        };
    }

    return template;
};
