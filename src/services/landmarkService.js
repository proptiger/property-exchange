"use strict";
const utilService = require('services/utilService'),
	apiService = require('services/apiService'),
	apiConfig = require('configs/apiConfig');

var landmarkService = {};

function _parseLandmarkData(response){
	if(response && response.results && response.results.length){
		response.results.forEach((landmarkResponse)=>{
			if(landmarkResponse.buyUrl){
				landmarkResponse.buyUrl = utilService.prefixToUrl(landmarkResponse.buyUrl);
			}
			if(landmarkResponse.rentUrl){
				landmarkResponse.rentUrl = utilService.prefixToUrl(landmarkResponse.rentUrl);
			}
		});
	}
	return response || [];
}

landmarkService.getTopLandmarks = function(config, {req}) {
	let selector, urlConfig,
		countryId = config.countryId || (req && req.locals && req.locals.numberFormat && req.locals.numberFormat.code) || 1,
		selectorObj = {
		    "fields": config.fields || ["name","buyUrl","rentUrl"], 
		    "filters": [
		        {"key": "countryId", "value": countryId},
		        {"type": "wildcard", "key": "buyUrl", "value": "*"},
		        {"key": "landmarkPagesLiveStatus", "value": "Active"}
		    ],
		    "sort": config.sort || [{"key": 'rating',"order": 'DESC'}],
		    "paging": {"start": config.start || 0, "rows": config.rows || 20}
		};

	if(config.filters && Array.isArray(config.filters)){
		selectorObj.filters = selectorObj.filters.concat(config.filters);
	}
	selector = apiService.createSelector(selectorObj);
	urlConfig = apiConfig.allLandmarks(selector);

	return apiService.get(urlConfig,{req}).then((response) => {
	    return _parseLandmarkData(response && response.data);
	}, err => {
	    throw err;
	}).catch(err=>{
		throw err;
	});
};

module.exports = landmarkService;