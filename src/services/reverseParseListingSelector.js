"use strict";

const reverseParseSelectorFunc = require('public/scripts/common/reverseParseListingSelectorFunc');

module.exports.reverseParseSelector = function(selector, options={}) {
    return reverseParseSelectorFunc.mainfunction(selector, options);
};