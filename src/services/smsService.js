"use strict";

const apiService = require('services/apiService'),
    utils = require('services/utilService'),
    apiConfig = require('configs/apiConfig');

var smsService = {};

smsService.sendMessage = function(contactNumber, message, { req }) {
    var phone = contactNumber,
        smsString = message;

    if (phone && smsString) {
        let api = apiConfig.sms({
            contact: phone
        });

        delete req.headers['content-type']; // deleting this for this request to keep content-type default here

        return apiService.post(api, {
            body: smsString,
            req: req
        });
    } else {
        throw utils.getError(400);
    }
};

module.exports = smsService;
