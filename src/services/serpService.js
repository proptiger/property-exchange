"use strict";


  const utils           = require('./utilService'),
        _ = utils._,
        listings        = require('./listingsService'),
        builderService  = require('./builderService'),
        agentService    = require('./agentService'),
        localityService = require('./localityService'),
        projectService  = require('./projectService'),
        apiService      = require('./apiService'),
        globalConfig    = require('configs/globalConfig'),
        BlogService = require('services/blogService'),
        logger = require('services/loggerService'),
        SharedConfig = require('public/scripts/common/sharedConfig');

const   fields = [
            "localityId",
            "displayDate",
            "listing",
            "property",
            "project",
            "builder",
            "displayName",
            "locality",
            "suburb",
            "city",
            "state",
            "currentListingPrice",
            "companySeller",
            "company",
            "user",
            "id",
            "name",
            "label",
            "listingId",
            "propertyId",
            "projectId",
            "propertyTitle",
            "unitTypeId",
            "resaleURL",
            "description",
            "postedDate",
            "verificationDate",
            "size",
            "measure",
            "bedrooms",
            "bathrooms",
            "listingLatitude",
            "listingLongitude",
            "studyRoom",
            "servantRoom",
            "pricePerUnitArea",
            "price",
            "localityAvgPrice",
            "negotiable",
            "rk",
            "buyUrl",
            "rentUrl",
            "overviewUrl",
            "minConstructionCompletionDate",
            "maxConstructionCompletionDate",
            "halls",
            "facingId",
            "noOfOpenSides",
            "bookingAmount",
            "securityDeposit",
            "ownershipTypeId",
            "furnished",
            "constructionStatusId",
            "tenantTypes",
            "bedrooms",
            "balcony",
            "floor",
            "totalFloors",
            "listingCategory",
            "possessionDate",
            "activeStatus",
            "type",
            "logo",
            "profilePictureURL",
            "score",
            "assist",
            "contactNumbers",
            "contactNumber",
            "isOffered",
            "mainImageURL",
            "mainImage",
            "absolutePath",
            "altText",
            "title",
            "imageCount",
            "defaultImageId",
            "updatedAt",
            "qualityScore",
            "projectStatus",
            "throughCampaign",
            "addedByPromoter",
            "listingDebugInfo",
            "videos",
            "imageUrl",
            "rk",
            "penthouse",
            "studio",
            "paidLeadCount",
            "listingSellerTransactionStatuses",
            "listingSellerDisplayAds",
            "listingSellerBenefits",
            "listingSellerPrivileges",
            "listingSellerMiscellaneousProducts",
            "allocation",
            "allocationHistory",
            "masterAllocationStatus",
            "status",
            "sellerCompanyFeedbackCount",
            "companyStateReraRegistrationId",
            "sellerId"
        ];
const experimentCities = SharedConfig.serpExperimentCities;
const contactExperimentCities = SharedConfig.serpContactExperimentCities;
const URL_DETAIL_DEPENDENT_FIELDS = {
    "placeId": "geoDistance"
};

function _getUserLocalition(req) {
    let urlConfig = SharedConfig.clientApis.getCityByLocation();
    if (req.connection && req.connection.remoteAddress) {
        return apiService.get(urlConfig, { req }).then((response) => {
            if(response && response.data && response.data.city){
                return {
                    id:response.data.city.id,
                    name: response.data.city.label
                };
            }
        }).catch(error => {
            logger.error('error in getting User Location in listingsService is: ', error);
            return {}; // if this API fails Serp page should not be blocked from rendering
        });
    } else {
        return new Promise((resolve) => {
            resolve('');
        });
    }
} 

function setPaginationData(req, totalCount, mapView) {

    let paginationData = {},
    listingPerPage = globalConfig.itemsPerPage;
    if (totalCount && totalCount > 0) {
        paginationData.totalPages = Math.ceil(totalCount / listingPerPage);
    } else {
        paginationData.totalPages = 1;
    }
    paginationData.currentUrl = req.url;

    paginationData.filter = true;
    if(!mapView){
        paginationData.filter = false;
    }

    var currentPage = req.query.page;
    if (!currentPage) {
        currentPage = 1;
    }
    paginationData.currentPage = currentPage;
    paginationData.changeQueryParam = utils.changeQueryParam;
    return paginationData;
}

function _parseSnippetData(listingData, params){
    _.forEach(listingData.listings,function(listingT,keyT){
        _.forEach(listingT,function(listing,key){
            listing.estimatedEMI = utils.formatNumber(
                utils.calculateEMI(listing.fullPrice, globalConfig.emi_rate, globalConfig.emi_tenure, globalConfig.downpayment_percent),
                {precision : 2, seperator : params.numberFormat}
            );
        });                     
    });
    return listingData;
}

function _getBlogData(callback, req) {
    let pageData = req.pageData,
        text = "";
    if (pageData && pageData.pageLevel) {
        let level = pageData.pageLevel.toLowerCase();
        if (["city", "citytaxonomy"].indexOf(level) !== -1) {
            text = req.urlDetail.cityName;
        } else if (["locality", "localitytaxonomy"].indexOf(level) !== -1) {
            text = req.urlDetail.localityName;
        } else if (["project", "projecttaxonomy"].indexOf(level) !== -1) {
            text = req.urlDetail.projectName;
        } else if (["broker"].indexOf(level) !== -1) {
            text = req.urlDetail.companyName;
        } else if (["builder"].indexOf(level) !== -1) {
            text = req.urlDetail.builderName;
        } else if (["buildercity"].indexOf(level) !== -1) {
            text = req.urlDetail.cityName;
        } else if (["suburb", "suburbtaxonomy"].indexOf(level) !== -1) {
            text = req.urlDetail.suburbName;
        }
    }

    let blogIqUrl = process.env.BASE_URL_MAKAANIQ || '', 
        configCity = {
            "text": text,
            "count": 5,
            "sort": "views-desc",
            "website": "makaaniq",
            "days": 365
        },
        configTop = {
            "count": 5,
            "days": 50,
            "sort": "views-desc",
            "website": "makaaniq"
        };
    if(req.locals.numberFormat && req.locals.numberFormat.code) {
      configCity.website = configTop.website = "iq" + req.locals.numberFormat.code;
      blogIqUrl = blogIqUrl.replace(/\/iq\//,'/'+req.locals.numberFormat.code+'/iq/');
    }
    Promise.all([BlogService.getPostList(req, configCity), BlogService.getPostList(req, configTop)]).then((response) => {
        let post = [];
        if (_.isArray(response[0].data) && _.isArray(response[1].data)) {
            post = response[0].data.concat(response[1].data).slice(0, 5);
        }
        callback(null, {
            post,
            makaaniqUrl: blogIqUrl
        });
    }, err => {
        callback(err);
    });
}

module.exports.routeHandler = function(req, res, next, mapView = false){

    _routeHandler(req, res, next, mapView);

};

function _checkListingAvailable(data, rental){
    let field, available = false;
    if(rental){
        field = "propertyForRent";
    }else{
        field = "propertyForSale";
    }

    utils._.forEach(data,(v)=>{
        if(v[field]>0) {
            available = true;
            return;
        }
    });
    return available;
}

function _getTrackingFilters(query){
    let trackingObject = {};
    trackingObject.beds = query.beds;
    trackingObject.budget = query.budget && query.budget.replace(/^,|,$/g,'');
    return trackingObject;
}

function _routeHandler(req, res, next, mapView){

    let showBackButtonInFilter = req.showBackButtonInFilter || false;

    req.query.fields = fields.slice(); // to copy fields by value

    let clientXHR  = (req.xhr && req.query.format ==='json') ? true :  false,
        sidebarCountXHR = (clientXHR && req.query.sidebarCount) ? true : false,
        listingData, listingCount, totalCount, seoPrevNext = {}, paginationData,
        indexthreshold,nearbyListings=[], phraseDetectionListing = {},missingQuickFilters=[];

        // MAKAAN-5170 : Change Default Sort order from Relevance to Recency on SERP
        if(req.query && SharedConfig.recencySortedCities.indexOf(req.query.cityId) > -1 || req.urlDetail && SharedConfig.recencySortedCities.indexOf(req.urlDetail.cityId) > -1){
            req.query = req.query || {};
            if(!req.query.sortBy){
                req.query.sortBy = "date-desc"; // listing to be sorted on recency
            }
        }

        for(let key in URL_DETAIL_DEPENDENT_FIELDS) {
            if(req.urlDetail[key]) {
                req.query.fields.push(URL_DETAIL_DEPENDENT_FIELDS[key]);
            }
        }
        req.query.forceFieldFiltering = true;
        let isMobileRequest = utils.isMobileRequest(req);

        let listingAPIPromise = listings.getApiListings(req.query, req.urlDetail, {req});
        let deficitNearbyListingApi = listings.getdeficitNearbyListings(req.query,req.urlDetail,{req,itemsPerPage:10,isMobileRequest});
        let getUserLocalition = _getUserLocalition(req);
        let getPhraseDetectionListing = listings.getPhraseDetectionListing(req.query, req.urlDetail, {req});

    function _getRenderingData(callback, sync, asycSeoPrevNext){

        Promise.all([listingAPIPromise, deficitNearbyListingApi,getUserLocalition, getPhraseDetectionListing]).then((results)=>{

            let selector = results[0].selector;
            let response = results[0].response;
            let nearbyListingResponse = results[1].response;
            let userLocation = results[2];
            phraseDetectionListing = results[3];
            missingQuickFilters = results[0].missingQuickFilters||[];

            totalCount = response && response.data && response.data[0].totalCount || 0;
            paginationData = setPaginationData(req, response.data[0].totalCount, mapView);

            if(sidebarCountXHR){
                return callback(null, {});
            }else if (req.seoAPI && !clientXHR && paginationData.currentPage && paginationData.totalPages && paginationData.currentPage <= paginationData.totalPages) {
               let currentPage = parseInt(paginationData.currentPage),
                    totalPages = parseInt(paginationData.totalPages),
                    currBaseUrl =  req.urlDetail.currBaseUrl;

                if (asycSeoPrevNext) {
                    return callback(null, {
                        seoPrevNext: utils.createSeoPrevNext(currentPage, totalPages, currBaseUrl, seoPrevNext)
                    });
                }
            }

            let conditionalObj = {
                    h1Title: req.urlDetail.h1Title,
                    h2Title: req.urlDetail.h2Title,
                    showNearbyLocalitiesCheckbox: req.showNearbyLocalitiesCheckbox,
                    trackingObject: {},
                    isExperimentCity:experimentCities.indexOf(userLocation && userLocation.id)> -1 || experimentCities.indexOf(req.query && req.query.testCity)> -1,
                    isContactExperimentCity:contactExperimentCities.indexOf(userLocation && userLocation.id)> -1 || contactExperimentCities.indexOf(req.query && req.query.testCity)> -1
                },
                mobileView = utils.isMobileRequest(req) ? true : false;
                mapView    = mapView ? true : false;
            let showTopAgentCard = req.urlDetail.cityId ? true : false;

            conditionalObj.trackingObject = _getTrackingFilters(req.query);
            if(!req.urlDetail.cityId){
                conditionalObj.hidePyrCard = true;
            }

            if(req.data.boundaryEncodedPolygon){
                conditionalObj.boundaryEncodedPolygon = window.escape(req.data.boundaryEncodedPolygon);
            }

            if(req.urlDetail.listingType && (req.urlDetail.listingType.toUpperCase() === 'RENT' || req.urlDetail.listingType.toUpperCase() === 'COMMERCIALLEASE')){
                    conditionalObj.isRental = true;
            } else {
                conditionalObj.isRental = false;
            }

            nearbyListings = listings.parseDeficitNearbyListings(nearbyListingResponse,{mobileView, query: req.query, mapsView: mapView, sellerView: conditionalObj.sellerView, urlDetail: req.urlDetail, locals: req.locals });
            //TODO- verify and replace req.app.locals with req.locals
            listingData = listings.parseListingsApiResponse(response, {mobileView, query: req.query, mapsView: mapView, sellerView: conditionalObj.sellerView, urlDetail: req.urlDetail, locals: req.locals });
            if((req.query && SharedConfig.recencySortedCities.indexOf(req.query.cityId) > -1 )||(req.urlDetail && SharedConfig.recencySortedCities.indexOf(req.urlDetail.cityId) > -1)){
                if(req.query && req.query.sortBy == "date-desc" ){
                    listingData = listings.shuffleListingData(listingData);
                }
            }
            listingData = _parseSnippetData(listingData, req.locals);
            conditionalObj.isZeroListingPage = listingData.isZeroListingPage;

            let pageLevel = req.urlDetail && req.urlDetail.pageLevel;

            /* Have not found any use of similarProvider in serp page (might not be in use currently)*/
            let similarProvider;
            if (!mobileView && pageLevel && pageLevel.toLowerCase() === "city") {
                let category = conditionalObj.isRental ? "rent" : "buy";
                similarProvider = (function() {
                    return localityService.getTopRankedLocalities(req.urlDetail.cityId, category, { filters: selector.filters, paging: { rows: 4 } }, { req }).then((response) => {
                        return {
                            data: response,
                            showSection: _checkListingAvailable(response, conditionalObj.isRental),
                            similar: "topLocality",
                            type: "serp localtiy",
                            schema: "Place"
                        };
                    });
                })();
                listingData.similarTitle = "top localities in " + listingData.cityName;
            } else if (!mobileView && pageLevel && pageLevel.toLowerCase() === "locality") {
                let localityId = req.urlDetail && req.urlDetail.localityId;
                if (localityId && listingData.latitude && listingData.longitude) {
                    similarProvider = (function() {
                        return localityService.getNearbyLocalities(listingData.latitude, listingData.longitude, localityId, [], 4, 15, { req }).then((response) => {
                            return {
                                data: response,
                                showSection: _checkListingAvailable(response, conditionalObj.isRental),
                                similar: "similarLocality",
                                type: "serp locality",
                                schema: "Place"
                            };
                        });
                    })();
                    listingData.similarTitle = "similar localities to " + (req.urlDetail.localityName || listingData.data[0].localityName);
                }
            } else if (!mobileView && pageLevel && pageLevel.toLowerCase() === "project") {
                let projectId = req.urlDetail && req.urlDetail.projectId;
                let builderName = req.urlDetail && req.urlDetail.builderName;
                let listing = listingData.data[0] || {};
                if (projectId) {
                    similarProvider = (function() {
                        return projectService.getSimilarProjectAPIData(projectId, 4, { req }).then((response) => {
                            return {
                                data: response,
                                showSection: _checkListingAvailable(response, conditionalObj.isRental),
                                similar: "similarProject",
                                type: "serp project",
                                schema: "OfferCatalog"
                            };
                        });
                    })();
                    listingData.similarTitle = "similar project ";
                    listingData.similarTitle += listing.projectName ? "to " + builderName + " " + listing.projectName : "";
                }
            } else if (pageLevel && ["builder", "buildercity"].indexOf(pageLevel.toLowerCase()) >= 0) {
                let builderId = req.urlDetail && req.urlDetail.builderId;
                let builderName = req.urlDetail && req.urlDetail.builderName;
                let cityId = req.urlDetail && req.urlDetail.cityId;
                let cityName = req.urlDetail && req.urlDetail.cityName;
                if (builderId) {
                    similarProvider = (function() {
                        return builderService.getSimilarBuilder(builderId, cityId, { count: 4, req, selector: { filters: selector.filters } }, { req }).then((response) => {
                            return {
                                data: response,
                                showSection: _checkListingAvailable(response, conditionalObj.isRental),
                                similar: "",
                                cityUrl: utils.toUrlCase(cityName),
                                schema: "Brand",
                                type: "serp builder",
                            };
                        });
                    })();
                    listingData.similarTitle = "similar builder to " + builderName;
                }
            } 

            indexthreshold = (req.query && req.query.indexthreshold && parseInt(req.query.indexthreshold)) || 0;

            listingCount = listingData.data ? listingData.data.length : 0;
            totalCount = listingData.totalCount;

            // to show seller rating as first listing rating and seller india level rating in case of zero result
            if(listingCount && conditionalObj.sellerData){
                conditionalObj.sellerData.rating = listingData.data[0].postedBy.rating || conditionalObj.sellerData.rating;
            }

            listingData.displayAreaLabel = req.urlDetail.displayAreaLabel || '';

            listingData.isMobile = mobileView;
            listingData.isMaps   = mapView;

            if(mapView && utils.isMasterPlanSupported({name:req.urlDetail.cityName, id:req.urlDetail.cityId})) {
                listingData.showMasterPlan = true;
            }

            listingData.landmarkName = req.urlDetail.placeName;
            if(["NEARBY_LISTING_URLS_MAPS", "NEARBY_URLS_MAPS", "NEARBY_LISTING_TAXONOMY_URLS"].indexOf(req.urlDetail.pageType) != -1  || (req.urlDetail.pageType == 'LISTINGS_PROPERTY_URLS_MAPS' && req.urlDetail.placeId)) {
                listingData.pageType = 'landmark';
                // override latitude & longitude of first listing for maps landing center
                listingData.latitude = req.urlDetail.latitude;
                listingData.longitude = req.urlDetail.longitude;
            }

            //for custom H1/H2 tags in builder and broker pages
            if(req.urlDetail.pageType.indexOf('TAXONOMY') > -1 || req.urlDetail.cityId ) {
              conditionalObj.isTaxonomy = true;
            }
            else {
              conditionalObj.isTaxonomy = false;
            }

            // if need additonal details on serp
            if(req.data.builder && ((['BUILDER_TAXONOMY_URLS','BUILDER_TAXONOMY_URLS_MAPS','BUILDER_URLS','BUILDER_URLS_MAPS'].indexOf(req.urlDetail.pageType) > -1) || (req.query.builderId && ['BUILDER_TAXONOMY_URLS','BUILDER_TAXONOMY_URLS_MAPS','LISTINGS_PROPERTY_URLS','LISTINGS_PROPERTY_URLS_MAPS','BUILDER_URLS','BUILDER_URLS_MAPS'].indexOf(req.query.pageType) > -1))){
                conditionalObj.builderView = true;
                conditionalObj.builderData = builderService.parseBuilderData(req.data.builder, 100);
            }else if((req.data.seller && ((['COMPANY_TAXONOMY_URLS','COMPANY_TAXONOMY_URLS_MAPS','COMPANY_URLS','COMPANY_URLS_MAPS'].indexOf(req.urlDetail.pageType) > -1) || (req.query.companyId && ['COMPANY_TAXONOMY_URLS','COMPANY_TAXONOMY_URLS_MAPS','LISTINGS_PROPERTY_URLS','LISTINGS_PROPERTY_URLS_MAPS','COMPANY_URLS','COMPANY_URLS_MAPS'].indexOf(req.query.pageType) > -1))) || (['SELLER_PROPERTY_URLS', 'SELLER_PROPERTY_URLS_MAPS', ].indexOf(req.query.pageType) > -1)  || (req.query.sellerId && ['SELLER_PROPERTY_URLS','SELLER_PROPERTY_URLS_MAPS','LISTINGS_PROPERTY_URLS','LISTINGS_PROPERTY_URLS_MAPS'].indexOf(req.query.pageType) > -1)) {
                if(req.data.seller){
                    conditionalObj.sellerView = true;
                    conditionalObj.sellerData = agentService.parseCompanyData(req.data.seller, {req});
                    conditionalObj.sellerReviews = req.data.sellerReviews;
                    conditionalObj.feedbackCount = req.data.seller.feedbackCount;

                    showTopAgentCard = false;

                    // Adding additional items in page data required for tracking
                    req.pageData.sellerScore = conditionalObj.sellerData.rating;

                }else if(req.data.builder){
                    conditionalObj.builderView = true;
                    conditionalObj.builderData = builderService.parseBuilderData(req.data.builder, 100);
                    showTopAgentCard = false;
                }
            }

            let privilegeSellerListings = function (args, callback) {
                if (req && req.query && req.query.postedBy && req.query.postedBy.indexOf('owner') !== -1) {
                    listings.getPrivilegeSellerListings({ filters: selector.filters, paging: { rows: 10 }, fields: selector.fields }, { req }).then((response) => {
                    callback(null, response);
                }, error => {
                    callback(error);
                });
                } else {
                    callback(null, {});
                }
            };

            // if need additonal details on snippet
            if(conditionalObj.builderView){
                let builderProjectsSelector = {"fields": ["name","builderId","builderName","minPrice","minResalePrice","maxPrice","maxResalePrice","address"], "filters":[{"key": "builderId","value": [conditionalObj.builderData.id]}] , "sort": {"key":"name","order":"DESC"}};
                if(req.urlDetail.cityId){
                    builderProjectsSelector.filters.push({
                        "key": "cityId",
                        "value": [req.urlDetail.cityId]
                    });
                }
                projectService.getProjectSerpWithFilters({"selector": apiService.createSelector(builderProjectsSelector)}, { req }).then((response)=>{
                    if(response.data && response.data.length){
                        conditionalObj.snippetData = [];
                        response.data.forEach( function(pElem, pIndex) {
                            conditionalObj.snippetData.push(Object.assign({},pElem,{
                                minFormattedPrice: utils.formatNumber(pElem.minResalePrice || pElem.minPrice, {type : 'number', seperator : req.locals.numberFormat}),
                                maxFormattedPrice: utils.formatNumber(pElem.maxResalePrice || pElem.maxPrice, {type : 'number', seperator : req.locals.numberFormat})
                            }))
                        });
                    }
                }).then((data)=>{
                    callback(null, {
                        listingData,
                        privilegeSellerListings,
                        seoPrevNext,
                        listingCount,
                        indexthreshold,
                        conditionalObj,
                        showBackButtonInFilter,
                        paginationData,
                        showTopAgentCard,
                        similarProvider,
                        nearbyListings,
                        phraseDetectionListing,
                        missingQuickFilters
                    });
                }).catch((error)=>{
                    sync ? next(error) : callback(error); //jshint ignore:line
                });
            } else {
                callback(null, {
                    listingData,
                    privilegeSellerListings,
                    seoPrevNext,
                    listingCount,
                    indexthreshold,
                    conditionalObj,
                    showBackButtonInFilter,
                    paginationData,
                    showTopAgentCard,
                    similarProvider,
                    nearbyListings,
                    phraseDetectionListing,
                    missingQuickFilters
                });
            }

        }, (error) => {
            sync ? next(error) : callback(error); //jshint ignore:line
        }).catch((e) => {
            sync ? next(e) : callback(e);  // jshint ignore:line
        });
    }


    let serpMapsPath = '../modules/serp-maps/',
        serpPath      = '../modules/serp/',
        template, templateName, contentType;

    if (clientXHR) {
        templateName    = (mapView || sidebarCountXHR) ? `${serpPath}listingView.marko` : `${serpPath}view.marko`;
        contentType     = 'application/json';
    } else {
        templateName    = mapView ? `${serpMapsPath}view.marko` : `${serpPath}view.marko`;
    }
    template = require('services/templateLoader').loadTemplate(__dirname, templateName);

    if(clientXHR){
        _getRenderingData(function(err, data){
            if(err){
                next(err);
            }else {
                if(!mapView){
                    data = {asyncData:data, asycSeoData: data.seoPrevNext};
                }
                data.schemaMap = globalConfig.schemaMap;
                let conditionalObj = (data.asyncData && data.asyncData.conditionalObj) || data.conditionalObj;
                template.renderSync(data, res, function(error, output){
                    res.send({
                        propcardHtml: output,
                        listingData,
                        totalCount,
                        listingCount,
                        isZeroListingPage: conditionalObj && conditionalObj.isZeroListingPage,
                        nearbyListings,
                        phraseDetectionListing,
                        missingQuickFilters
                    });
                }, contentType, req);
            }
        }, true);
    }else {
        _getRenderingData(function(err, data){
            if(err){
                next(err);
            }else {
                template.renderAsync({
                    asyncData:data,
                    asycSeoData: function(args, callback){
                        _getRenderingData(callback, false, true);
                    },
                    blogData: function(args, callback) {
                        _getBlogData(callback, req);
                    },
                    schemaMap : globalConfig.schemaMap
                }, res, '', req);
            }
        });
    }
}
