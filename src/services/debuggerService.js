/**
 * Name: debuggerService.js
 * Description: Helper file for debugger mode
 * author: [Ramakrishna]
**/

"use strict";
//const	logger = require('services/loggerService');

let debug = {};

// debug.hitAsyncApis = function(data, req){
// 	let arr = [];
// 	for(let key in data){
// 		if(typeof data[key] === 'function'){
// 			arr.push(_executeFunctions(data[key], req));
// 		}
// 	}
// 	return arr;
// }

// function _executeFunctions(fn, req){
// 	return new Promise((resolve, reject) => {
// 		fn({},(err, result)=>{
// 			resolve();
// 		});
// 	});
// }

debug.parse = function(data = {}){
	let status = "OK";
	let newObj = {
		status: status,
		primaryApis: [],
		asyncApis: [],
		totalApiTime: `${data.apiTime} ms`,
		templateRenderTime: `${data.templateTime} ms`
	};
	if(data.error){
		if(data.error.status){
			newObj.status = `Api Error`;
		} else {
			newObj.status = `Node Error: ${data.error.message}`;
			newObj.stack = data.error.stack.split('\n');
		}
	}
	for(let apis in data.primaryApis){
		newObj.primaryApis.push({
			api: apis,
			statusCode: data.primaryApis[apis].statusCode,
			apiTime: data.primaryApis[apis].time
		});
	}
	for(let apis in data.asyncApis){
		if(data.asyncApis[apis].isPrimary){
			newObj.primaryApis.push({
				api: apis,
				statusCode: data.asyncApis[apis].statusCode,
				apiTime: data.asyncApis[apis].time
			});
		}
		else{
			newObj.asyncApis.push({
				api: apis,
				statusCode: data.asyncApis[apis].statusCode,
				apiTime: data.asyncApis[apis].time
			});
		}
	}

	return newObj;
};

module.exports = debug;