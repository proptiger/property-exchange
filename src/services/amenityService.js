"use strict";

var apiService = require('./apiService'),
    apiConfig = require('configs/apiConfig'),
    utilService = require('./utilService'),
    _ = utilService._;

var amenityService = {};

amenityService.getAmenities = function(config, req) {

    let selector = {
        "sort": [{
            field: 'priority',
            sortOrder: 'DESC'
        }],
        "paging": {
            start: config.start,
            rows: config.rows
        }
    };
    if(config.filters){
        selector.filters = config.filters;
    }
    selector = JSON.stringify(selector);
    selector = _.assign({ selector }, req.query);

    var api = apiConfig.amenities({ query: selector, version: 2 });
    return apiService.get(api, { req }).then((response) => {
        return response;
    }, (err) => {
        throw err
    });
}

module.exports = amenityService;
