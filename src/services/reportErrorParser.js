"use strict";

const apiService = require('./apiService'),
    apiConfig = require('configs/apiConfig'),
    _ = require('services/utilService')._;

var reportErrorParser = {};

reportErrorParser.sendMessage = function({
    userId,
    listingId,
    phone,
    errorDetailsIds,
    description
}, {
    req
}) {
        var payload = {
            "user" : {
                "contactNumbers" : phone
            },
            "objectId" : listingId,
            "errorDetails" : errorDetailsIds
        };
        if(userId) {
            var userLogged = { "id" : userId };
            _.merge(payload["user"], userLogged);
        }

        if(description) {
           var otherDescription = { "description" : description };
           _.merge(payload, otherDescription); 
        }

        let api = apiConfig.reportErrorApi();
        return apiService.post(api, {
            body: payload,
            req: req
        });
    
};

module.exports = reportErrorParser ;
