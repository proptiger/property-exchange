"use strict";
var getPropertyType = function(key) {
        let map = {
            1: 'Apartment',
            2: 'Villa',
            3: 'Residential Plot',
            4: 'Commercial',
            5: 'Shop',
            6: 'Office',
            7: 'Independent Floor',
            8: 'Farmhouse',
            9: 'Studio Apartment',
            10: 'Other Residential',
            11: 'Commercial Land',
            12: 'Business Centre',
            13: 'Warehouse',
            14: 'Industrial Setup',
            15: 'Other Commercial',
            16: 'Showroom',
            17: 'Office In ITPark SEZ',
            18: 'Others',
            19: 'Independent House',
            20: 'Penthouse'
        };
        return map[parseInt(key)];
    },
    getPlc = function(key){
        let map = {
            1 : "Open Car Parking",
            2 : "Closed Car Parking",
            3 : "Semi Closed Car Parking",
            4 : "Club House",
            5 : "Internal Development Charges",
            6 : "External Development Charges",
            7 : "IFMS",
            8 : "Lease Rent",
            9  : "Legal Fees",
            10 : "Maintenance Advance",
            11 : "Maintenance Advance Months",
            12 : "Power",
            13 : "Water",
            14 : "Power and Water",
            15 : "Power Backup Charges",
            16 : "Floor Rise",
            17 : "Garden Facing",
            18 : "Road Facing",
            19 : "Golf Facing",
            20 : "Corner Unit",
            21 : "Pool Facing",
            22 : "Internal Court Facing",
            23 : "Club Facing",
            24 : "Lake Facing",
            25 : "Other PLC",
            26 : "Other",
            27 : "Lawn PLC",
            28 : "Terrace Garden PLC",
            29 : "Two side open PLC",
            30 : "Three side open PLC",
            31 : "Car Parking",
            32 : "PLC",
            33 : "Maintenance Deposit"
        };
        return map[parseInt(key)];
    },
    getLanguageText = function(key) {
        let map = {
            'posession': 'Possession Date',
            'area': 'Built Up Area',
            'dateAdded': 'Date Added',
            'parking': 'Car Parking',
            'category': 'New/Resale',
            'floor': 'Floor',
            'facing': 'Facing',
            'age': 'Age of Property',
            'status': 'Status',
            'bath': 'Bathrooms',
            'balcony': 'Balconies',
            'furnishStat': 'Status' ,//'Furnishing',
            'totalFloor': 'Total Floors',
            'bookamt': 'Booking Amount',
            'openSides': 'No. open sides',
            'securityDeposit': 'Security Deposit',
            'availability': 'Availability',
            'tenantPref': 'Preferred Tenants',
            'entryRoadWd': 'Road facing main entry',
            'priceNeg': 'Price Negotiable',
            'overlook': 'Overlooking',
            'addRoom': 'Additional Rooms',
            'ownerType': 'Ownership Type',
            'propertyType':'Type',
            'size':'Area',
            'totalArea': 'Total Area',
            'launchDate':'Launch Date',
            'allowedFloor':'Allowed Floors',
            'noTower':'No of Towers',
            'onlineBooking':'Online Booking',
            'bookingStat': 'Booking Status',
            'complDate':'Completion Date',
            'expComplDate':'Expected Completion Date',
            'preLaunchDate':'pre-Launch Date',
            'expPosDate':'Expected Posession date',
            'township':'Township',
            'totalUnits':'Total Units',
            'totalUnitsSale':'Total Units available for Sale',
            'totalUnitsRent':'Total Units available for Rent',
            'plc': 'plc',
            "carpetArea": 'Carpet area',
            'reraRegistrationNumber': 'RERA ID',
            'noOfSeats':'No of seats',
            'noOfRooms': 'No of rooms',
            'lockInPeriod': 'Locking period',
            'maintenanceCharges': 'Maintenance charges',
            'willingToModifyInteriors': 'Willing to modify',
            'isCornerShop': 'Corner shop',
            'isMainRoadFacing': 'Main road facing',
            'isBoundryWallPresent': 'Boundary wall',
            'pantryType': 'Pantry',
            'currentlyLeasedOut': 'Pre leased property',
            'assureReturnPercentage': 'Assured returns',
        };
        return map[key];
    },
    getProjectStatusKey = function(status) {
        let stat = '';
        status = status.toLowerCase();
        switch (status) {
            case 'launch':
                stat = 'New Launch';
                break;
            case 'on hold':
                stat = 'On Hold';
                break;
            case 'under construction':
                stat = 'Under Construction';
                break;
            case 'not launched':
                stat = 'Launching Soon';
                break;
            case 'pre launch':
                stat = 'Launching Soon';
                break;
            case 'ready for possession':
            case 'occupied':
            case 'completed':
                stat = 'Ready to Move';
                //stat = 'Completed';
                break;
            default:
                stat = 'NA';
        }
        return stat;
    },
    getFacing = function(facing) {
        let map = {
            1: 'East',
            2: 'West',
            3: 'North',
            4: 'South',
            5: 'NorthEast',
            6: 'SouthEast',
            7: 'NorthWest',
            8: 'SouthWest'
        };
        return map[parseInt(facing)];
    },
    getOwnerType = function(ownerTypeId) {
        let map = {
            1: 'Freehold',
            2: 'Leasehol',
            3: 'Power of Attorney',
            4: 'Co-Operative Society'
        };

        return map[parseInt(ownerTypeId)];
    },
    getListingCategory = function(category) {
        let map = {
            'Primary': 'New',
            'Resale': 'Resale',
            'Rental': 'Rental',
            'PrimaryAndResale': 'PrimaryAndResale',
            'PrimaryExpanded': 'PrimaryAndResale',
            'PG': 'PG'
        };
        return map[category];
    },
    getIconMap = function(key) {
        let map = {
            'Gym': 'icon-gym',
            'Sec': 'icon-gym',
            'Pow': 'icon-gym',
            'Spo': 'icon-gym',
            'Chi': 'icon-gym',
            'Clu': 'icon-gym',
            'Cct': 'icon-gym',
            'Swi': 'icon-gym',
            'Gar': 'icon-gym',
            'Lif': 'icon-gym',
            'Uti': 'icon-gym',
            'Jog': 'icon-gym',
            'Bed': 'icon-gym',
            'AC': 'icon-gym',
            'War': 'icon-gym',
            'TV': 'icon-gym',
            'Sof': 'icon-gym',
            'Din': 'icon-gym',
            'Ref': 'icon-gym',
            'Gas': 'icon-gym',
            'Wif': 'icon-gym',
            'Was': 'icon-gym',
        };
        return map[key];
    },
    getAmenityFurnishingValue = function(key) {
        let map = {
            'Bed': 'Beds',
            'AC': 'AC',
            'War': 'Wardrobe',
            'TV': 'TV',
            'Sof': 'Sofa',
            'Din': 'Dining Table',
            'Ref': 'Refrigerator',
            'Gas': 'Gas connection',
            'Was': 'Washing Machine',
            'Wif': 'Wifi',
            'Gym': 'Gymnasium',
            'Sec': '24/7 security',
            'Pow': 'Power Backup',
            'Spo': 'Sports Facility',
            'Chi': 'Children Play Area',
            'Clu': 'Club House',
            'Cct': 'CCTV cameras',
            'Swi': 'Swimming Pool',
            'Gar': 'Garden',
            'Lif': 'Lift',
            'Uti': 'Utility Stores',
            'Jog': 'Jogging Track'
        };
        return map[key];
    },
    getNeighbourhoodTabs = function(){
        let neighbourhoodTabs = [
      //   {
     	// 	"label": 'Master Plan',
     	// 	"targetId": 'masterPlanContent'
     	// }, 
        {
     		"label": 'Nearby',
     		"targetId": 'neighbourhoodContent',
     		"selected": true
     	},
         {
            "label": 'Commute',
            "targetId": 'commuteContent'
        }];
        return neighbourhoodTabs;
    };

module.exports = {
    getPropertyType,
    getLanguageText,
    getProjectStatusKey,
    getFacing,
    getOwnerType,
    getListingCategory,
    getIconMap,
    getAmenityFurnishingValue,
    getNeighbourhoodTabs,
    getPlc
};
