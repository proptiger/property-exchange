"use strict";
const apiService = require('services/apiService'),
    apiConfig = require('configs/apiConfig'),
    logger = require('services/loggerService'),
    propertyService = require('services/propertyService'),
    listingService = require('services/listingsService'),
    maps = require('services/propertyMappingService'),
    imageParser = require('services/imageParser'),
    priorityService = require('services/propertyPriorityService'),
    globalConfig = require('configs/globalConfig'),
    mappingService = require('services/mappingService'),
    utils = require('services/utilService'),
    masterDetailsService = require('services/masterDetailsService'),
    agentService = require('services/agentService'),
    builderService = require('services/builderService'),
    imageService = require('services/imageService'),
    moment = require('moment'),
    config = {
        validActiveStatus: ['active', 'activeinmakaan'],
        dummyStatus: ['dummy'],
        unverifiedStatus: ['Unverified'],
        dominantUnitTypes: ['apartment', 'villa', 'plot'],
        type: {
            'buy': ['primary', 'resale'],
            'rent': ['rental']
        },
        saleCategory: ['Primary', 'Rental'],
        budgetSteps: {
            buy: _getBuySteps(),
            rent: _getRentSteps()
        },
        EACH_BUCKET_PROPERTY_SIZE_VARIATION_FACTOR: 0.05 // 5%
    },
    _ = utils._,
    sponsoredProjects={
        24:[1489205,663923],
        209:[1489205,663923],
        129:[1489205,663923,1736142],
        258:[1489205,663923],
        102:[1736142],
        22:[1736142],
        223:[668128],
        78:[668128],
        453:[668128],
        95:[668128],
        46:[668128],
        287:[668128]
    },

    developerProducts={
        'featuredNationalProjects':{
            'home':'NationalSlot',
        },
        'featuredDeveloper':{
            'home':'FeaturedDevelopers',
        },
        'trendingProjects':{
            'home':'TrendingProjectHomePage',
        },
        'featuredProjects':{
            'home':'FeaturedProjectHomePage',
            'city':'FeaturedProjectCityOverview',
            'locality':'FeaturedProjectLocalityOverview'
        },
        'showcaseProjects':{
           'city':'ShowcaseProjectsCitySerp',
           'locality': 'ShowcaseProjectsSerp',
           'suburb' : 'ShowcaseProjectsSerp',
        },
        'billboardProjects':{
           'city':'BillBoardCitySerp',
           'locality': 'BillBoardSerp',
           'suburb' : 'BillBoardSerp',
        },
        'miniBillboardProjects':{
           'city':'MiniBillBoardCitySerp',
           'locality': 'MiniBillBoardSerp',
           'suburb' : 'MiniBillBoardSerp',
        },
        'suggestedProjects':{
           'city':'SuggestedProjectPropertyPage',
           'locality': 'SuggestedProjectPropertyPage',
           'suburb' : 'SuggestedProjectPropertyPage',
        }
    },
    PROJECT = "PROJECT";

function _getBuySteps() {
    let i = 0,
        steps = [];
    while (i < 1000000000) {
        steps.push(i);
        i += 5000;
    }
    return steps;
}

function _getRentSteps() {
    let i = 0,
        steps = [];
    while (i < 200000000) {
        steps.push(i);
        i += 5000;
    }
    return steps;
}


function getProjectInfo(projectId, options, { req }) {
    logger.info('get project Info');

    options = options || {};

    return apiService.get(apiConfig.project({
        projectId: projectId,
        query: {
            selector: JSON.stringify(options.selector)
        }
    }), { req });
}

function getHomeLoans(loan, callback, { req }) {
    let bankIds = Object.keys(loan);
    imageService.getImageDetails('bank', { bankId: bankIds }, { req }).then(response => {
        let result = [];
        for (let i = 0, length = bankIds.length; i < length; i++) {
            let currKey = bankIds[i],
                currObj = response[currKey];
            if (currObj) {
                result.push({
                    name: loan[currKey].name,
                    image: imageParser.appendImageSize(currObj.logo[0].url, '"width": 120,"height": 90')
                });
            }
        }
        callback(null, result);
    }, error => {
        logger.info('error in the loan image api', error);
        callback(error);
    });
}

function getMainImageObj(absolutePath, images) {
    if(absolutePath && images && images.length) {
        return images.find((image) => {
            return image.absolutePath === absolutePath;
        });
    }
}

function _parseSellerSpecificProjectDetails(apiResponse){
    let projectData = {},response = apiResponse.data || {};
    let jsonFacetsBucket = response.jsonFacetsGroup && response.jsonFacetsGroup.length ? response.jsonFacetsGroup[0].buckets : [],
        facetValues = jsonFacetsBucket && jsonFacetsBucket.length ? jsonFacetsBucket[0].facetValues : [];
    projectData.listingCountBuy = apiResponse.totalCount;
    facetValues && facetValues.forEach(item=>{
        if(item.name === 'totalRooms'){
            item.values && item.values.forEach(bedDetail=>{
                projectData.distinctBedrooms = projectData.distinctBedrooms || [];
                projectData.distinctBedrooms.push(bedDetail.value);
            })
        }
        if(item.name=="unitType"){
            item.values && item.values.forEach(unitTypeDetail=>{
                projectData.Type = projectData.Type ||[];
                projectData.Type.push(unitTypeDetail.value);
            });
        }
        if(item.name=="Size"){
            if(item.aggregates && item.aggregates.minimum){
                projectData.minSize = utils.formatNumber(item.aggregates.minimum, { type : 'number'});
            }
            if(item.aggregates && item.aggregates.maximum){
                projectData.maxSize = utils.formatNumber(item.aggregates.maximum, { type : 'number'});
            }
        }
        if(item.name=="CarpetArea"){
            if(item.aggregates && item.aggregates.minimum){
                projectData.minCarpetArea = utils.formatNumber(item.aggregates.minimum, { type : 'number'});
            }
            if(item.aggregates && item.aggregates.maximum){
                projectData.maxCarpetArea = utils.formatNumber(item.aggregates.maximum, { type : 'number'});
            }
        }
        if(item.name=="Price"){
            if(item.aggregates && item.aggregates.minimum){
                projectData.minPriceNum = item.aggregates.minimum
                projectData.emi = utils.formatNumber(utils.calculateEMI(projectData.minPriceNum, globalConfig.emi_rate, globalConfig.emi_tenure, globalConfig.downpayment_percent), { type : 'number'});
            }
            if(item.aggregates && item.aggregates.maximum){
                projectData.maxPriceNum = item.aggregates.maximum;
            }
        }
       
     });
     projectData.priceRange = {
        minPrice: utils.formatNumber(projectData.minPriceNum, { precision : 2, returnSeperate : true}),
        maxPrice: utils.formatNumber(projectData.maxPriceNum, { precision : 2, returnSeperate : true})
    };
    if(projectData.priceRange.minPrice && projectData.priceRange.maxPrice && projectData.priceRange.minPrice.val && projectData.priceRange.maxPrice.val && projectData.priceRange.minPrice.val== projectData.priceRange.maxPrice.val){
        projectData.priceRange.showSinglePrice = true;
    }
     
    if(projectData.minCarpetArea || projectData.maxCarpetArea){
        projectData.Area =  utils.getRange(projectData.minCarpetArea,projectData.maxCarpetArea,"sq ft");
        projectData.carpetArea = true;
    } else if(projectData.maxSize || projectData.minSize){
        projectData.Area =  utils.getRange(projectData.minSize,projectData.maxSize,"sq ft");
    }
     if (projectData.distinctBedrooms) {
        projectData.distinctBedrooms = projectData.distinctBedrooms.sort(function(a, b) {
            return a > b;
        });
        projectData.distinctBedrooms = projectData.distinctBedrooms.join(',') + ' bhk';
    }
     if(projectData.Type && projectData.Type.length){
        projectData.Type = projectData.Type.join(',');
        if(projectData.Type=="apartment"){
            projectData['Apartment'] = projectData.distinctBedrooms;
        }
    }
    return projectData;
}

function _fetchSellerSpecificProjectDetail(projectId,sellerUserIds,{req}){
    let selectorObj = {
        jsonFacets :{
            groupByField:"projectId",
            fields:["minimumFuncPrice","maximumFuncPrice","minimumFuncSize","maximumFuncSize","totalRooms","unitType", "minimumFuncCarpetArea", "maximumFuncCarpetArea"],
            paging:{rows:0}
        },
        filters: [],
        paging: {"start": 0,"rows":0}
    }, selector,urlConfig;
     selectorObj.filters.push({
        key:"projectId",
        value:projectId
    });
    selectorObj.filters.push({
        key:"sellerId",
        value:sellerUserIds
    });
    selectorObj.filters.push({
        key:"listingCategory",
        value:["Primary","Resale"]
    });
     selector = apiService.createSelector(selectorObj);
    urlConfig = apiConfig.getSellerSpecificProjectDetail({ selector });
     return apiService.get(urlConfig, { req }).then(response => {
        if (response && response.data) {
            return _parseSellerSpecificProjectDetails(response);
        }
    }, err => {
        throw err;
    });
 }
function getSellerSpecificProjectDetail(projectId,sellerUserIds,{req}){
    if(!sellerUserIds || !sellerUserIds.length){
        return new Promise((resolve, reject) => {
            resolve({});
        });        
    }else{
        return _fetchSellerSpecificProjectDetail(projectId,sellerUserIds,{req}).then(function(parsedResponse) {
            return parsedResponse;
        },(err)=> {
            return {};
        });
    }
}

function getProjectDetails(projectId, localityId, { req, isSponsored, selector = {}}) {
    logger.info('get project details');
    let projectDetail = apiService.get(apiConfig.project({
            projectId: projectId,
            query: {
                selector: apiService.createSelector(selector)
            }
        }), {
            req: req,
            isPrimary: true
        }),
        parsedImageObject,
        constructionObj;

    return projectDetail.then((projectResponse) => {
        logger.info('success of project detail api');
        let projectData = _parseProjectData(projectResponse.data, req);
        localityId = localityId || projectData.parsed.locality.id;
        let propertyIds = projectData.parsed.verifyPropertyIds;
        let mainImage = projectResponse && projectResponse.data && 
                        getMainImageObj(projectResponse.data.mainImage.absolutePath, projectResponse.data.images) || projectResponse.data.mainImage,
            defaultImageId = projectResponse && projectResponse.data && projectResponse.data.defaultImageId;
        if (mainImage && defaultImageId) {
            mainImage.id = defaultImageId;
        }
        let imageDetails = imageService.getImageDetails('project', {
            projectId,
            localityId,
            propertyIds
        }, {
            req: req,
            isPrimary: true
        });
        return imageDetails.then((imagesResponse) => {
            logger.info('success of image api');
            parsedImageObject = imageParser.groupProjectImages(imagesResponse, {
                projectId,
                localityId,
                propertyIds,
                mainImage
            }, req);
            constructionObj = parsedImageObject.constructionObj;
            return {
                projectDetail: projectData.parsed,
                raw: projectData.raw,
                galleryContainerData: parsedImageObject.galleryContainerData,
                imageDetails: {
                    data: parsedImageObject.resultArray,
                    group: parsedImageObject.groupObj
                },
                constructionImages: constructionObj && {
                    data: constructionObj.constructionImages,
                    group: constructionObj.constructionGroup,
                    orderedGroup: constructionObj.orderedGroup
                },
                floorPlanImages: parsedImageObject.floorPlanImages
            };
        }, err => {
            throw err;
        });
    }, err => {
        throw err;
    });
}
function getTopProjectsInGA (data, {req}) {
    return apiService.get(apiConfig.topCityProjectsInGA({cityId: data.cityId}), { req }).then((response) => {
        return response.data.map(respData=>{
            return {
                suburb: respData.suburbName,
                topProjects: respData.projectUrls.map(tProjData=>{
                    return {
                        name: tProjData.label,
                        url: utils.prefixToUrl(tProjData.url)
                    }
                })
            }
        });
    }, err => {
        throw err;
    });
}
function getTopProjects(data, {fields, req }) {
    if (!data || !data.cityId) {
        return new Promise(function(resolve, reject) {
            reject("Error in Project service");
        });
    }
    let temp = {},
        defaultFields = [],
        queryFields = [],
        config,
        urlConfig,
        cityId,
        count;
    defaultFields = ["name", "locality", "suburb", "label", "URL", "suburbId", "activeStatus", "projectStatus", "localityId", "projectId", "builder", "displayName"];
    queryFields = defaultFields;
    if (fields && typeof fields === 'object') {
        queryFields = fields;
    }
    config = {
        rows: 1000,
        fields: queryFields,
        groupRows: 9
    };

    cityId = data.cityId || null;
    count = data.count || config.rows;

    temp.filters = [{
        "key": "cityId",
        "value": cityId
    }, {
        "key": "projectDbStatus",
        "value": ["Active", "ActiveInMakaan"]
    }];
    temp.fields = config.fields;
    temp.groupRows = config.groupRows;
    temp.groupBy = {
        field: "suburbId"
    };
    temp.paging = {
        "start": 0,
        "rows": count
    };
    urlConfig = apiConfig.topCityProjects({
        query: {
            selector: apiService.createSelector(temp)
        }
    });
    return apiService.get(urlConfig, { req }).then((response) => {
        return _parseTopProjects(response);
    }, err => {
        throw err;
    });
}

function _parseTopProjects(response) {
    if (!(response && response.data)) {
        return [];
    }
    response = response.data;
    let result = {},
        i, length = response.length,
        temp;
    for (i = 0; i < length; i++) {
        if (response[i].activeStatus && config.validActiveStatus.indexOf((response[i].activeStatus || '').toLowerCase()) === -1) {
            continue;
        }
        let key = response[i].locality.suburbId;
        if (result[key]) {
            temp = result[key];
            result[key] = null;
        } else {
            temp = {};
            temp.suburb = (response[i].locality.suburb.label);
        }
        temp.topProjects = temp.topProjects ? temp.topProjects : [];
        response[i].builder.displayName = response[i].builder.displayName ? response[i].builder.displayName : response[i].builder.name;
        let projects = {
            'name': response[i].name,
            'url': utils.prefixToUrl(response[i].URL),
            'localityId': response[i].localityId,
            'projectId': response[i].projectId,
            'projectStatus': response[i].projectStatus,
            'builder': response[i].builder
        };
        if(projects.url){
            temp.topProjects.push(projects);
        }
        result[key] = temp;
    }
    return result;
}

function _parseLoan(bankArray = []) {
    let result = {};
    for (let bank of bankArray) {
        result[bank.id] = {
            name: bank.name
        };
    }
    return result;
}

function _getRentSalePropertyCount(data = []) {
    let i,
        response;

    response = {
        sale: 0,
        rent: 0,
        avgPricePerUnitArea: 0,
        avgPrice: 0
    };
    for (i = 0; i < data.length; i++) {
        if (config.type.buy.indexOf(data[i].listingCategory.toLowerCase()) !== -1) {
            response.avgPricePerUnitArea += data[i].avgPricePerUnitArea * data[i].count;
            response.sale += data[i].count;
        } else if (config.type.rent.indexOf(data[i].listingCategory.toLowerCase()) !== -1) {
            response.avgPrice += data[i].avgPrice * data[i].count;
            response.rent += data[i].count;
        }
    }
    response.avgPrice = Math.floor(response.rent && response.avgPrice / response.rent) || 0;
    response.avgPricePerUnitArea = Math.floor(response.sale && response.avgPricePerUnitArea / response.sale) || 0;

    return response;
}

function parseSimilarProjects(similarProjectArray, isMobile, params) {

    let similarArray = [];
    let imageSize = 'tile';
    if(isMobile){
        imageSize = 'thumbnail';
    }
    for (let i = 0, length = similarProjectArray.length; i < length; i++) {
        let currentObject = similarProjectArray[i],
            possessionDate = currentObject.possessionDate,
            rawPrice = { "minPrice": currentObject.minResaleOrPrimaryPrice || currentObject.minPrice, "maxPrice": currentObject.maxResaleOrPrimaryPrice || currentObject.maxPrice },
            price = utils.formatNumber(currentObject.minResaleOrPrimaryPrice || currentObject.minPrice, { precision : 2, returnSeperate : true, seperator : params.numberFormat }),
            projectName = currentObject.name,
            absolutePath = imageParser.appendImageSize(currentObject.imageURL, imageSize),
            altText = currentObject.mainImage ? currentObject.mainImage.altText : undefined,
            title = currentObject.mainImage ? currentObject.mainImage.title : undefined,
            locality = currentObject.locality,
            builder = currentObject.builder,
            url = utils.prefixToUrl(currentObject.URL),
            buyUrl = utils.prefixToUrl(currentObject.buyUrl),
            rentUrl = utils.prefixToUrl(currentObject.rentUrl),
            projectId = currentObject.projectId,
            overviewUrl = utils.prefixToUrl(currentObject.overviewUrl),
            cityName,
            localityName,
            medianRent,
            medianPrice,
            propertyForSale,
            propertyForRent,
            builderName,
            avgPriceRisePercentage,
            showBuilder;
        if (locality) {
            localityName = locality.label;
            let suburb = locality.suburb;
            if (suburb) {
                let city = suburb.city;
                if (city) {
                    cityName = city.label;
                }
            }
        }
        if (builder) {
            showBuilder = config.validActiveStatus.indexOf((builder.activeStatus || '').toLowerCase()) > -1 ? true : false;
            builderName = builder.name;
        }

        let countDetails = _getRentSalePropertyCount(currentObject.listingAggregations);
        propertyForRent = countDetails.rent;
        propertyForSale = countDetails.sale;
        medianPrice = utils.formatNumber(countDetails.avgPricePerUnitArea, { precision : 2, seperator : params.numberFormat });
        medianRent = utils.formatNumber(countDetails.avgPrice, { precision : 2, seperator : params.numberFormat });
        avgPriceRisePercentage = currentObject.avgPriceRisePercentage;

        let minPrice = utils.formatNumber(currentObject.minResaleOrPrimaryPrice || currentObject.minPrice, { precision : 2, returnSeperate : true, seperator : params.numberFormat }),
            maxPrice = utils.formatNumber(currentObject.maxResaleOrPrimaryPrice || currentObject.maxPrice, { precision : 2, returnSeperate : true, seperator : params.numberFormat });

        let minPricePerUnitArea = utils.formatNumber(currentObject.minPricePerUnitArea, { precision : 2, returnSeperate : true, seperator : params.numberFormat }),
            maxPricePerUnitArea = utils.formatNumber(currentObject.maxPricePerUnitArea, { precision : 2, returnSeperate : true, seperator : params.numberFormat });

        let priceRange = {
            minPrice,
            maxPrice,
            minPricePerUnitArea,
            maxPricePerUnitArea
        };

        if (minPrice && maxPrice && minPrice.val == maxPrice.val) {
            delete priceRange.maxPrice;
        }
        if (minPricePerUnitArea && maxPricePerUnitArea && minPricePerUnitArea.val == maxPricePerUnitArea.val) {
            delete priceRange.maxPricePerUnitArea;
        }

        similarArray.push({
            buyUrl,
            rentUrl,
            url,
            possessionDate,
            rawPrice,
            price,
            priceRange,
            projectName,
            cityName,
            localityName,
            absolutePath,
            altText,
            projectId,
            builderName,
            showBuilder,
            title,
            medianRent,
            medianPrice,
            propertyForSale,
            propertyForRent,
            overviewUrl,
            name: builderName + ' ' + projectName,
            image: absolutePath,
            avgPriceRisePercentage
        });
    }
    return similarArray;
}

function getSimilarProjectAPIData(projectId, rows = 5, { req,  isMobile }) { //jshint ignore:line
    let selector = apiService.createSelector({
            "fields": ["title", "builder", "displayName", "activeStatus", "projectId", "URL", "buyUrl", "rentUrl", "imageURL", "altText", "mainImage", "minPrice", "maxPrice", "minResaleOrPrimaryPrice", "maxResaleOrPrimaryPrice", "id", "city", "suburb", "label", "name", "type", "user", "contactNumbers", "locality", "contactNumber", "sellerId", "listingCategory", "property", "currentListingPrice", "price", "bedrooms", "bathrooms", "size", "unitTypeId", "project", "projectId", "studyRoom", "servantRoom", "poojaRoom", "companySeller", "company", "companyScore", "listingAggregations","possessionDate"],
            "paging": {
                "start": 0,
                "rows": rows
            }
        }),
        similarProjectUrl = apiConfig.similarProject({
            query: {
                projectId: projectId,
                selector: selector
            }
        });

    return apiService.get(similarProjectUrl, {
        req
    }).then(response => {
        if (response.data) {
            response.data = response.data.filter((obj) => {
                return obj.activeStatus && obj.activeStatus.toLowerCase() === "active";
            });
            return parseSimilarProjects(response.data, isMobile, req.locals);
        }
        return [];
    }, err => {
        throw err;
    });
}

function getSimilarProjects(projectId, callback) {
    return getSimilarProjectAPIData(projectId, 10).then(response => {
        callback(null, response);
    }, err => {
        callback(err);
    });
}

function _getOverviewKeys(details = {}) {
    let resultArray = [];
    // apartmentCase;
    let priorityArray = priorityService.projectPriority();
    let tempObj;
    for (let i = 0, length = priorityArray.length; i < length; i++) {

        if(priorityArray[i] === "launchDate" && details.hideLaunchDate){
            continue;
        }
        if (priorityArray[i] == 'propertyType' && details['propertyType'] == 'Apartment' && details['distinctBedrooms']) {
            tempObj = {
                key: details['propertyType'],
                val: details['distinctBedrooms'] + ' bhk'
            };
        } else {
            tempObj = {
                key: maps.getLanguageText(priorityArray[i]),
                val: details[priorityArray[i]]
            };
            if(tempObj.key === 'Area' && details.hasOwnProperty('carpetArea')){
                tempObj.key = 'Carpet Area';
            }
        }

        if (tempObj.val) {
            resultArray.push(tempObj);
        }
    }
    return resultArray;
}

function getAmenityIds(amenityArray = []) {
    let result = [];
    for (let i = 0, length = amenityArray.length; i < length; i++) {
        let currObj = amenityArray[i],
            amenityMaster = currObj.amenityMaster || {};
        result.push(amenityMaster.amenityId);
    }
    return result;
}

function getAvailability(listingAggregation = []) {
    let result = [];
    for (let i = 0, length = listingAggregation.length; i < length && result.length < 2; i++) {
        let listingCategory = listingAggregation[i].listingCategory;
        if ((listingCategory == 'Primary' || listingCategory == 'Resale') && result.indexOf(listingCategory) == -1) {
            result.push(listingCategory);
        }
    }
    return result.join(', ').replace('Primary', 'New');
}

function _parseProjectData(data = {}, req = {}) {
    let parsedData = {};
    let bedRoomsToDisplay = data.distinctBedroomsFloat.length ? data.distinctBedroomsFloat : data.distinctBedrooms;
    if (bedRoomsToDisplay) {
        bedRoomsToDisplay = bedRoomsToDisplay.sort(function(a, b) {
            return a > b;
        });
    }
    let minSizeToDisplay = data.minCarpetArea ? data.minCarpetArea : data.minSize;
    let maxSizeToDisplay = data.maxCarpetArea ? data.maxCarpetArea : data.maxSize;
    let details = {
        "status": maps.getProjectStatusKey(data.projectStatus),
        "posession": utils.formatDate(data.possessionDate, 'SS YY'),
        "possessionDateFormatted": moment(data.possessionDate).format('DD-MM-YYYY'),
        "propertyType": data.propertyUnitTypes && data.propertyUnitTypes.join(', '),
        "availability": getAvailability(data.listingAggregations),
        "size": utils.getRange(utils.formatNumber(minSizeToDisplay, { type : 'number', seperator : req.locals.numberFormat }), utils.formatNumber(maxSizeToDisplay, { type : 'number', seperator : req.locals.numberFormat }), data.propertySizeMeasure),
        "totalArea": data.sizeInAcres ? `${data.sizeInAcres} acres` : undefined,
        "launchDate": utils.formatDate(data.launchDate, 'SS YY'),
        "hideLaunchDate": data.hideLaunchDate,
        "allowedFloor": undefined, //scrapped
        "noTower": data.numberOfTowers,
        "onlineBooking": undefined, //scrapped
        "bookingStat": undefined, //scrapped
        "complDate": utils.formatDate(data.completionDate, 'SS YY'), //same : completion Date , expected Completion Date, expected Possession Date
        "expComplDate": undefined, //same : completion Date , expected Completion Date, expected Possession Date
        "expPosDate": undefined, //same : completion Date , expected Completion Date, expected Possession Date
        "preLaunchDate": utils.formatDate(data.preLaunchDate, 'SS YY'),
        "township": undefined, //data.hasTownship ? 'present' : 'not present',
        "totalUnits": utils.formatNumber(data.totalUnits, { type : 'number', seperator : req.locals.numberFormat }),
        "totalUnitsSale": undefined, //scrapped
        "totalUnitsRent": undefined, //scrapped
        "distinctBedrooms": bedRoomsToDisplay && bedRoomsToDisplay.join(','),
        "minPrice": data.minResaleOrPrimaryPrice || data.minPrice,
        "maxPrice": data.maxResaleOrPrimaryPrice || data.maxPrice,
        "listingCountBuy": data.listingCountBuy,
        "listingCountRent": data.listingCountRent,
        "reraRegistrationNumber": !data.reraIdExpired && data.reraRegistrationNumber
    };
    
    if(data.minCarpetArea || data.maxCarpetArea){
        details.carpetArea = true;
    }
    
    details.possessionTimestamp = data.possessionDate;
    let wordLimit = utils.isMobileRequest(req) ? globalConfig.wordLimit.mobile : globalConfig.wordLimit.desktop,
        detailsArray = _getOverviewKeys(details),
        strippedDescription = utils.stripHtmlContent(data.description, globalConfig.wordLimit.midLength, true, true),
        fullStrippedDescription = utils.stripHtmlContent(data.description);

    parsedData.keyDetails = detailsArray;
    parsedData.description = {
        fullDescription: data.description,
        smallDescription: strippedDescription.description,
        fullStrippedDescription: fullStrippedDescription.description,
        hideViewMore: strippedDescription.error
    };
    let typeofAmenities = data.isCommercial ? 'Shop' : 'Apartment';
    parsedData.amenities = propertyService.parseAmenities(getAmenityIds(data.projectAmenities), typeofAmenities, 'Primary', data.isCommercial);
    parsedData.amenities.amenityList = parsedData.amenities.amenityList.filter(amenity => amenity.present);
    parsedData.loans = _parseLoan(data.loanProviderBanks);
    parsedData.projectScore = data.livabilityScore > 10 ? 10 : data.livabilityScore;
    if (data.builder) {
        let builder = data.builder;
        let builderBuyUrl = (config.validActiveStatus.indexOf((builder.activeStatus || '').toLowerCase()) > -1) ? utils.prefixToUrl(builder.buyUrl) : undefined;
        let builderWordLimit = globalConfig.wordLimit.longLength; //modifying builder description to show three line(approx 500 letter i.e longlength)
        parsedData.builder = {
            "id": builder.id,
            "name": builder.name,
            "displayName": builder.displayName || '',
            "show": (config.dummyStatus.indexOf((builder.activeStatus || '').toLowerCase()) === -1),
            "description": utils.stripHtmlContent(builder.description, builderWordLimit, true).description,
            "originalDescription": builder.description,
            "logo": imageParser.appendImageSize(builder.imageURL, 'thumbnail'),
            "altText": builder.mainImage && builder.mainImage.altText,
            "url": builderBuyUrl,
            "details": [{
                "key": "Experience",
                "val": builder.establishedDate ? `${utils.timeFromDate(builder.establishedDate)} years` : undefined
            }, {
                "key": "Completion on time",
                "val": builder.percentageCompletionOnTime
            }, {
                "key": "Ongoing Projects",
                "val": builder.projectStatusCount['under construction'] + builder.projectStatusCount['launch'] + builder.projectStatusCount['pre launch'],
                "url": builderBuyUrl ? `${builderBuyUrl}?possession=any` : undefined
            }, {
                "key": "Past Projects",
                "val": builder.projectStatusCount['completed'],
                "url": builderBuyUrl ? `${builderBuyUrl}?ageOfProperty=any` : undefined
            }]
        };
    }

    if (data.locality) {
        let locality = data.locality;
        parsedData.locality = {
            "name": locality.label,
            "description": utils.stripHtmlContent(locality.description, globalConfig.wordLimit.midLength, true).description,
            "url": utils.prefixToUrl(locality.overviewUrl),
            "buyUrl": utils.prefixToUrl(locality.buyUrl),
            "livabilityScore": locality.livabilityScore > 10 ? 10 : locality.livabilityScore,
            "id": locality.localityId,
            "latitude": locality.latitude,
            "longitude": locality.longitude,
            "localityImage": imageParser.appendImageSize(locality.localityHeroshotImageUrl, 'small')
        };
        if (locality.suburb && locality.suburb.city) {
            let city = locality.suburb.city;
            parsedData.city = {
                "name": city.label,
                "id": city.id,
                "url": utils.prefixToUrl(city.overviewUrl),
                "buyUrl": city.buyUrl
            };
        }
    }


    parsedData.projectDetail = {
        title: data.name,
        projectId: data.projectId,
        projectStatus: maps.getProjectStatusKey(data.projectStatus),
        showDisclaimer: _getDisclaimer(data.projectStatus),
        builderName: parsedData.builder ? parsedData.builder.name : undefined,
        builderDisplayName: (parsedData.builder && parsedData.builder.displayName)  ? parsedData.builder.displayName :  '',
        emi: utils.formatNumber(utils.calculateEMI(data.minResaleOrPrimaryPrice, globalConfig.emi_rate, globalConfig.emi_tenure, globalConfig.downpayment_percent), { type : 'number', seperator : req.locals.numberFormat }),
        isNegotiable: true,
        locality: parsedData.locality ? parsedData.locality.name : undefined,
        localityId: parsedData.locality ? parsedData.locality.id : undefined,
        city: parsedData.city ? parsedData.city.name : undefined,
        cityId: parsedData.city ? parsedData.city.id : undefined,
        pricePerSqft: data.minPricePerUnitArea ? utils.formatPriceWithComma(data.minPricePerUnitArea) : undefined,
        keyDetails: detailsArray.splice(0, 3),
        localityName: data.locality && data.locality.label,
        minPriceNum: data.minResaleOrPrimaryPrice || data.minPrice,
        maxPriceNum: data.maxResaleOrPrimaryPrice || data.maxPrice,
        unitTypeId: data.propertyUnitTypes && getMappedUnitTypeIds(data.propertyUnitTypes),
        distinctBedrooms: data.distinctBedrooms && data.distinctBedrooms.join(','),
        isProjectFeaturedBuy: data.isProjectFeaturedBuy,
        isProjectFeaturedRent:  data.isProjectFeaturedRent,
        projectUrl: data.URL
    };

    let minPrice = utils.formatNumber(data.minResaleOrPrimaryPrice || data.minPrice, { precision : 2, returnSeperate : true, seperator : req.locals.numberFormat }),
        maxPrice = utils.formatNumber(data.maxResaleOrPrimaryPrice || data.maxPrice, { precision : 2, returnSeperate : true, seperator : req.locals.numberFormat });

    parsedData.projectDetail.priceRange = {
        minPrice,
        maxPrice
    };

    if(minPrice && maxPrice && minPrice.val && maxPrice.val && minPrice.val== maxPrice.val){
        parsedData.projectDetail.priceRange.showSinglePrice = true;
    }

    if ((data.locality.avgPricePerUnitArea) && (data.avgPricePerUnitArea)) {
        parsedData.projectDetail.trendIncreasing = data.avgPricePerUnitArea > data.locality.avgPricePerUnitArea;
    }

    parsedData.coordinates = {
        lat: data.latitude,
        lng: data.longitude
    };

    parsedData.showMasterPlan = utils.isMasterPlanSupported({
        name: parsedData.city.label,
        id: parsedData.city.id
    });
    parsedData.specifications = propertyService.parseSpecifications(data.resiProjectSpecifications);
    parsedData.verifyPropertyIds = _getVerifiedPropertyIds(data.properties);
    parsedData.config = {
        wordLimit: wordLimit,
        wordLimitMid: globalConfig.wordLimit.midLength
    };

    parsedData.pageGlobal = {
        buyUrl: utils.prefixToUrl(data.buyUrl),
        rentUrl: utils.prefixToUrl(data.rentUrl),
        localityId: data.locality && data.locality.localityId,
        localityName: data.locality && data.locality.label,
        builderName: data.builder && data.builder.name,
        projectName: data.name,
        projectId: data.projectId
    };

    parsedData.titleBar = {
        projectTitle: parsedData.projectDetail.title,
        priceRange: parsedData.projectDetail.priceRange,
        title: data.name,
        projectId: data.projectId,
        projectStatus: maps.getProjectStatusKey(data.projectStatus),
        builderName: parsedData.builder ? parsedData.builder.name : undefined,
        localityId: parsedData.locality ? parsedData.locality.id : undefined,
        cityId: parsedData.city ? parsedData.city.id : undefined,
        localityName: data.locality && data.locality.label,
        minPriceNum: data.minResaleOrPrimaryPrice || data.minPrice,
        maxPriceNum: data.maxResaleOrPrimaryPrice || data.maxPrice,
        unitTypeId: data.propertyUnitTypes && getMappedUnitTypeIds(data.propertyUnitTypes),
        distinctBedrooms: data.distinctBedrooms && data.distinctBedrooms.join(',')
    };
    parsedData.tabs = [
    // {
    //     "label": "Master plan",
    //     "targetId": "masterPlanContent",
    //     "selected": true
    // }, 
    {
        "label": "Nearby",
        "targetId": "neighbourhoodContent"
    }, {
        "label": 'Commute',
        "targetId": 'commuteContent'
    }];
    // if (!parsedData.showMasterPlan) {
    //     parsedData.tabs.splice(0, 1);
    // }
    parsedData.mainImage = [{
        url: data.imageURL || globalConfig.staticProjectImageURL
    }];
    parsedData.properties = data.properties;
    parsedData.avgPricePerUnitArea = data.avgPricePerUnitArea;
    parsedData.offerHeading = data.offerHeading;
    parsedData.offerDesc = data.offerDesc;
    parsedData.salientFeatures = data.salientFeatures;
    parsedData.possession = utils.formatDate(data.possessionDate, 'SS - yy');
    parsedData.size = utils.getRange(utils.formatNumber(data.minSize, { type : 'number', seperator : req.locals.numberFormat }), utils.formatNumber(data.maxSize, { type : 'number', seperator : req.locals.numberFormat }), data.propertySizeMeasure);
    parsedData.unitTypeString = data.unitTypeString;
    parsedData.has3DImages = data.has3DImages;
    parsedData.dominantUnitType = data.dominantUnitType;
    // adding fallback
    if (!data.dominantUnitType || config.dominantUnitTypes.indexOf(data.dominantUnitType.toLowerCase()) === -1) {
        parsedData.dominantUnitType = 'Apartment';
    }
    parsedData.lastUpdatedDate = utils.formatDate(data.lastUpdatedDate, 'dd-mm-YY');
    return {
        parsed: parsedData,
        raw: details
    };
}

function _getVerifiedPropertyIds(properties) {
    //  Use this array to get floor plans for testing [5000010, 5000793, 5003399,5006279,5024655,5002242];
    let verifyPropertyIds = [];
    for (var property in properties) {
        if (properties[property].propertyId && properties[property].optionCategory && properties[property].optionCategory.toLowerCase() === 'actual') {
            verifyPropertyIds.push(properties[property].propertyId);
        }

    }
    return verifyPropertyIds;

}

function _getDisclaimer(projectStatus) {
    return ["New Launch", "Under Construction", "Launching Soon"].indexOf(projectStatus) > -1;
}


function getPaidSellers( _options, { req, isMobile } ){ 

    let topAgentPromise = getTopAgent(_options, { req, isMobile });
    let featuredSellersPromise = agentService.getFeaturedTopAgent(_options.projectId, { req });

   return Promise.all([topAgentPromise, featuredSellersPromise]).then((results)=>{ 

        let topAgents = results[0];
        let featuredSellers = results[1];
        let response = featuredSellers && featuredSellers.length ? utils.shuffleArray(featuredSellers) : (topAgents && topAgents.length ? utils.shuffleArray(topAgents):[]);

        return response;
    }, (err) => {
       throw err;
    });

    
}

function getTopAgent(_options, { req, isMobile },returnPaidUnpaid=false) {
    let options = {},
        contactNumber;

    options.filters = [{
        key: 'projectId',
        value: _options.projectId,
    }];

    if (_options.listingCategory) {
        options.filters.push({
            key: 'listingCategory',
            value: _options.listingCategory.split(','),
        });
    }

    options.paging = {
        start: 0,
        rows: 5
    };

    if (req.cookies && req.cookies.enquiry_info) {
        let __str = req.cookies.enquiry_info.replace(/\\/g, ""),
            cookieObj;
        try {
            cookieObj = JSON.parse(__str);
        } catch (e) {
            cookieObj = {};
        }
        if (cookieObj.phone) {
            contactNumber = cookieObj.phone;
        }
    }

    return agentService.getTopAgents(_options.cityId, {
        selector: options,
        contactNumber,
        giveBoostToExpertDealMakers: _options.giveBoostToExpertDealMakers,
        relaxProjectFilter : _options.relaxProjectFilter
    }, {
        req,
        isMobile
    },returnPaidUnpaid);

}

function _parseTopAgents(response, { isSponsored, builderName }) {
    let topSellers = [];
    //let otherTopSellers = [];
    utils._.forEach(response, (v) => {
        let temp = {
            "companyId": v.id || "",
            "id": v.id || "",
            "companyName": isSponsored ? builderName : v.name,
            "companyPhone": v.contact,
            "companyRating": v.rating,
            "isRelaxed": v.isRelaxed,
            "companyImage": v.image,
            "companyAssist": v.assist,
            "companyUserId": v.userId,
            "companyType": v.type && v.type.toLowerCase() == 'broker' ? 'agent' : v.type,
            "companyMatchingProperties": v.properties,
            "companyAvatar": v.avatar,
            "sellerBhk": v.sellerBhk,
            "sellerType": v.sellerType || '',
            "paidLeadCount": v.paidLeadCount,
            "isFeaturedSeller":v.isFeaturedSeller
        };
        topSellers.push(temp);
    });
    return topSellers;
}


function parseTopAgentForLead(agentPromise, config = {}) {
    let agentDataMap = {};
    return agentPromise.then(response => {
        if (response && response.length) {
            response = _parseTopAgents(response, { isSponsored: config.isSponsored, builderName: config.builderName });
            if(config.virtualNumber){
                let companyUserIds = [];
                response.forEach(item => {
                    companyUserIds.push(item.companyUserId);
                    agentDataMap[item.companyUserId] = item;
                });
                return agentService.getContactNumber(companyUserIds, config.cityId, "Sell", { req: config.req }).then((data) => {
                    data && data.forEach(item => {
                        agentDataMap[item.userId].companyPhone = item.contact || agentDataMap[item.userId].companyPhone;
                    });
                    return Object.values(agentDataMap);
                }, () => {
                    return response;
                });
            } else if (response[0].companyUserId) {
                return agentService.getContactNumber(response[0].companyUserId, config.cityId, "Sell", { req: config.req }).then((data) => {
                    response[0].companyPhone = (data && data.length && data[0].contact) || (response && response.length && response[0].companyPhone);
                    return response;
                }, () => {
                    return response;
                });
            }
            return response;
        }
        // no seller for this project
        throw new Error('No seller available');
    }, () => {
        throw new Error('No seller available');
    });
}

function getMappedUnitTypeIds(propertyTypeArr) {
    let mappedUnitType = [],
        mappedValue,
        propertyType = masterDetailsService.getMasterDetails("unitTypesNameMap");
    _.forEach((propertyTypeArr), (k) => {
        k = k.trim().toLowerCase();
        mappedValue = propertyType[k] && propertyType[k].id;
        mappedUnitType.push(mappedValue);
    });
    return mappedUnitType;
}

function _getMappedUnitType(name) {
    let unitTypes = masterDetailsService.getMasterDetails("unitTypes"),
        mappedValue;
    _.forEach(unitTypes, (k, v) => {
        if (k === name) {
            mappedValue = v;
        }
    });

    return mappedValue;
}

function getSponsoredCompany(projectId, {
    req,
    removeClientIP
}) {
    return apiService.get(apiConfig.getSponsoredCompany({
        projectId
    }), {
        req,
        removeClientIP
    }).then(response => {
        return _parseSponsoredCompany(response.data);
    }, err => {
        throw err;
    });
}

function getSponsoredSeller(sellerId, { req }) {
    let selector = apiService.createFiqlSelector({
            fields: ['user', 'id', 'fullName', 'countryId', 'contactNumbers', 'contactNumber', 'id', 'status', 'companyId', 'userId', 'email', 'company', 'id', 'name', 'type'],
            filters: [{
                type: 'equal',
                key: 'userId',
                value: sellerId
            }, {
                type: 'equal',
                key: 'status',
                value: 'Active',
                filter: 'and'
            }]
        }),
        url = apiConfig.getDetailsBySellerId({
            query: selector
        }, { req }).url;
    return apiService.get(url).then(response => {
        return _parseSponsoredCompany(response.data && response.data[0]);
    }, err => {
        throw err;
    });
}



function _parseSponsoredCompany(data = {}) {
    data = data || {};
    let result = {},
        company = data.company || {},
        user = data.user || {},
        contactNumbers = user.contactNumbers || [];
    result.assist = company.assist;
    result.contact = (contactNumbers[0] || {}).contactNumber;
    result.id = company.id;
    result.name = company.name;
    result.rating = company.score ? company.score / 2 : 0;
    result.ratingClass = utils.getRatingClass(company.score / 2);
    result.image = imageParser.appendImageSize(company.logo || user.profilePictureURL, 'thumbnail');
    result.avatar = utils.getAvatar(company.name);
    result.sellerUrl = '';
    result.userId = user.id;

    return [result];
}

function _getRatingClass(rating) {
    let finalClass = 'r',
        decimalPart = rating - Math.floor(rating);

    if (Math.floor(rating)) {
        finalClass += Math.floor(rating);
    }
    if (decimalPart >= 0.5) {
        finalClass += 'half';
    }
    return finalClass;
}


/*
    Divided various properties into various price buckets ,size and bhk to make different configurations .
    Price ranges are made according to price per sqft .With each price ranges, a configuration is made by grouping
    properties where their size varies by 5% .
    For reference : MO-894
*/
var _bifurcatePropertiesIntoVariousPriceBuckets = function(data, params, {req}) {

    let propertiesArray = data.propertiesList,
        length = propertiesArray && propertiesArray.length,
        priceBuckets = [{}],
        bedrooms = {},
        i,
        j = 0,
        //k = 0,
        unitTypeId,
        configurationsAvailable = params.configurationsAvailable;

    // Make sure that properties are initially sorted with price ,then size , then bhk
    propertiesArray.sort((obj1, obj2) => {
        let priceFirst = obj1.currentListingPrice && obj1.currentListingPrice.price || 0,
            priceSecond = obj2.currentListingPrice && obj2.currentListingPrice.price || 0;
        if (priceFirst === priceSecond) {
            let sizeFirst = (obj1.property && obj1.property.size) || (obj1.property && obj1.property.carpetArea) || 0,
                sizeSecond = (obj2.property && obj2.property.size) || (obj2.property && obj2.property.carpetArea) || 0;
            if (sizeFirst === sizeSecond) {
                let bhkFirst = obj1.property && obj1.property.bedrooms || 0,
                    bhkSecond = obj2.property && obj2.property.bedrooms || 0;
                return bhkFirst - bhkSecond;
            }
            return sizeFirst - sizeSecond;
        }
        return priceFirst - priceSecond;
    });

    for (let unitType of mappingService.getUnitTypes()) {
        if (unitType.displayText == params.unitType) {
            unitTypeId = unitType.id;
        }
    }

    priceBuckets[0].minPrice = {};
    priceBuckets[0].k = 0;
    priceBuckets[0].maxPrice = {};
    priceBuckets[0].minPrice.raw = _minBucketRange(propertiesArray[0].currentListingPrice.price, params.listingCategory);
    priceBuckets[0].minPrice.readable = utils.formatNumber(priceBuckets[0].minPrice.raw,  { returnSeperate : true, convertThousand : true, seperator : req.locals.numberFormat });
    priceBuckets[0].maxPrice.raw = _maxBucketRange(propertiesArray[0].currentListingPrice.price + _adjustmentRatioForBucket(propertiesArray[0].currentListingPrice.price, propertiesArray[0].property.size, params.listingCategory), propertiesArray[0].currentListingPrice.price, params.listingCategory);
    priceBuckets[0].maxPrice.readable = utils.formatNumber(priceBuckets[0].maxPrice.raw,  { returnSeperate : true, convertThousand : true, seperator : req.locals.numberFormat });
    for (i = 0; i < length; i++) {
        priceBuckets[j] = priceBuckets[j] || {};
        priceBuckets[j].propertiesList = priceBuckets[j].propertiesList || [];
        let _properties = propertiesArray[i],
            currentProperty = _properties.property,
            key = currentProperty.bedrooms,
            size = currentProperty.size || currentProperty.carpetArea,
            currentBedroomObject,
            k,
            numberOfConfigurationsInEachBucket = 0,
            forceBreak = false;
        priceBuckets[j].bedrooms = priceBuckets[j].bedrooms || {};
        priceBuckets[j].k = priceBuckets[j].k || 0;
        k = priceBuckets[j].k;
        configurationsAvailable[params.unitType] = configurationsAvailable[params.unitType] || [];
        configurationsAvailable[params.unitType].push(size);
        currentBedroomObject = priceBuckets[j].bedrooms;
        bedrooms[`${key}`] = bedrooms[`${key}`] || { count: 0 };
        for (let bhkConfiguration in currentBedroomObject) {
            for (let configuration in currentBedroomObject[bhkConfiguration]) { //jshint ignore:line
                numberOfConfigurationsInEachBucket++;
            }
        }
        if (numberOfConfigurationsInEachBucket >= 5) { // we break bucket ,if number of configuration exceeds to 5 in that bucket
            forceBreak = true;
        }
        if (!forceBreak && _properties.currentListingPrice.price >= priceBuckets[j].minPrice.raw && _properties.currentListingPrice.price <= priceBuckets[j].maxPrice.raw) {
            priceBuckets[j].propertiesList.push(_properties);

        } else if (_properties.currentListingPrice.price > priceBuckets[j].maxPrice.raw || forceBreak) {
            if (forceBreak) {
                let _newMaxPrice = _findMaxPriceFromPropertyArray(priceBuckets[j].propertiesList);
                priceBuckets[j].maxPrice.raw = _maxBucketRange(_newMaxPrice, _newMaxPrice, params.listingCategory);
                priceBuckets[j].maxPrice.readable = utils.formatNumber(priceBuckets[j].maxPrice.raw, { returnSeperate : true, convertThousand : true, seperator : req.locals.numberFormat });

            }
            j++;
            priceBuckets[j] = priceBuckets[j] || {};
            priceBuckets[j].bedrooms = priceBuckets[j].bedrooms || {};
            currentBedroomObject = priceBuckets[j].bedrooms;
            priceBuckets[j].minPrice = {};
            priceBuckets[j].maxPrice = {};
            priceBuckets[j].propertiesList = priceBuckets[j].propertiesList || [];
            priceBuckets[j].k = 0;
            k = priceBuckets[j].k;
            let _minPrice = _properties.currentListingPrice.price;
            priceBuckets[j].minPrice.raw = _minBucketRange(_minPrice, params.listingCategory);
            priceBuckets[j].minPrice.readable = utils.formatNumber(priceBuckets[j].minPrice.raw, { returnSeperate : true, convertThousand : true, seperator : req.locals.numberFormat });
            priceBuckets[j].propertiesList.push(_properties);
            priceBuckets[j].maxPrice.raw = _maxBucketRange(priceBuckets[j].minPrice.raw + _adjustmentRatioForBucket(_properties.currentListingPrice.price, _properties.property.size, params.listingCategory), priceBuckets[j].minPrice.raw, params.listingCategory);
            priceBuckets[j].maxPrice.readable = utils.formatNumber(priceBuckets[j].maxPrice.raw, { returnSeperate : true, convertThousand : true, seperator : req.locals.numberFormat });
        }
        currentBedroomObject[`${key}`] = currentBedroomObject[`${key}`] || [];
        currentBedroomObject[`${key}`][k] = currentBedroomObject[`${key}`][k] || {};
        currentBedroomObject[`${key}`][k].propertiesList = currentBedroomObject[`${key}`][k].propertiesList || [];
        if (!currentBedroomObject[`${key}`][k].minSize) {
            currentBedroomObject[`${key}`][k].minSize = size;
            currentBedroomObject[`${key}`][k].maxSize = size;
        }
        if (size <= (currentBedroomObject[`${key}`][k].minSize + config.EACH_BUCKET_PROPERTY_SIZE_VARIATION_FACTOR * currentBedroomObject[`${key}`][k].minSize)) {
            currentBedroomObject[`${key}`][k].maxSize = (size > currentBedroomObject[`${key}`][k].maxSize) ? size : currentBedroomObject[`${key}`][k].maxSize;
            currentBedroomObject[`${key}`][k].propertiesList.push(_properties);
            if (size < currentBedroomObject[`${key}`][k].minSize) {
                currentBedroomObject[`${key}`][k].minSize = size;
            }
        } else {
            priceBuckets[j].k++;
            k = priceBuckets[j].k;
            currentBedroomObject[`${key}`][k] = currentBedroomObject[`${key}`][k] || {};
            currentBedroomObject[`${key}`][k].propertiesList = currentBedroomObject[`${key}`][k].propertiesList || [];
            if (!currentBedroomObject[`${key}`][k].minSize) {
                currentBedroomObject[`${key}`][k].minSize = size;
                currentBedroomObject[`${key}`][k].maxSize = size;
            }
            currentBedroomObject[`${key}`][k].maxSize = (size > currentBedroomObject[`${key}`][k].maxSize) ? size : currentBedroomObject[`${key}`][k].maxSize;
            currentBedroomObject[`${key}`][k].propertiesList.push(_properties);
        }
        bedrooms[`${key}`].count++;
        currentBedroomObject[`${key}`][k].balcony = currentBedroomObject[`${key}`][k].balcony || [];
        currentBedroomObject[`${key}`][k].bathrooms = currentBedroomObject[`${key}`][k].bathrooms || [];
        (currentProperty.balcony > 0 && currentBedroomObject[`${key}`][k].balcony.indexOf(currentProperty.balcony) === -1) && currentBedroomObject[`${key}`][k].balcony.push(currentProperty.balcony); //jshint ignore:line
        (currentProperty.bathrooms > 0 && currentBedroomObject[`${key}`][k].bathrooms.indexOf(currentProperty.bathrooms) === -1) && currentBedroomObject[`${key}`][k].bathrooms.push(currentProperty.bathrooms); //jshint ignore:line
    }
    data.priceBuckets = priceBuckets;
    data.bedrooms = bedrooms;

    // Generating SERP link and nunique seller for each bucket .
    for (let _eachPriceBucket of priceBuckets) {
        if (_eachPriceBucket.propertiesList) {
            let _eachBedroomData = _eachPriceBucket.bedrooms,
                bhkKeys;
            if (!_eachBedroomData) {
                continue;
            }
            bhkKeys = Object.keys(_eachBedroomData);
            for (i = 0; i < bhkKeys.length; i++) {
                let _bhkArrayData = _eachBedroomData[bhkKeys[i]],
                    eachbhkBucketKeys = Object.keys(_bhkArrayData),
                    eachbhkBucketKeysLength = eachbhkBucketKeys.length;
                for (let j = 0; j < eachbhkBucketKeysLength; j++) {
                    let _eachbhkBucketData = _bhkArrayData[eachbhkBucketKeys[j]],
                        uniqueSellers;
                    uniqueSellers = _getUniqueSeller(_eachbhkBucketData.propertiesList, params.sellerPropertyCount, params.companyId, req);
                    _eachbhkBucketData.propertiesList = _eachbhkBucketData.propertiesList.filter((elem) => {
                        if (elem) {
                            return elem;
                        }
                    });
                    if (!_eachbhkBucketData.propertiesList.length) {
                        delete _bhkArrayData[eachbhkBucketKeys[j]];
                        continue;
                    }
                    _eachbhkBucketData.uniqueSellers = uniqueSellers;
                    _eachbhkBucketData.bathrooms = _eachbhkBucketData.bathrooms && _eachbhkBucketData.bathrooms.sort() && _eachbhkBucketData.bathrooms.join(",");
                    _eachbhkBucketData.balcony = _eachbhkBucketData.balcony && _eachbhkBucketData.balcony.sort() && _eachbhkBucketData.balcony.join(",");
                    _setMinMaxPriceForBucket(_eachbhkBucketData, params, {req});
                    _eachbhkBucketData.serpFilterLink = utils.generateSerpFilter({
                        beds: bhkKeys[i],
                        minPrice: _eachbhkBucketData.minPrice && _eachbhkBucketData.minPrice.raw,
                        maxPrice: _eachbhkBucketData.maxPrice && _eachbhkBucketData.maxPrice.raw,
                        minSize: _eachbhkBucketData.minSize,
                        maxSize: _eachbhkBucketData.maxSize,
                        propertyType: params.unitType.toLowerCase(),
                        listingType: params.listingCategory.join() == 'Rental' ? 'rent' : 'buy',
                        sellerId: params.companyId,
                        projectId: params.projectId
                    });
                    _eachbhkBucketData.params = {
                        projectId: params.projectId,
                        pricemin: _eachPriceBucket.minPrice && _eachPriceBucket.minPrice.raw,
                        pricemax: _eachPriceBucket.maxPrice && _eachPriceBucket.maxPrice.raw,
                        unitTypeId: unitTypeId,
                        listingCategory: params.listingCategory.join(),
                        bed: bhkKeys[i]
                    };
                    if (!uniqueSellers.propertyCount && params.companyId) {
                        delete _bhkArrayData[eachbhkBucketKeys[j]];
                    }
                }
            }
        }
    }
};

function _findMaxPriceFromPropertyArray(propertiesList) {
    let maxPrice;
    for (let i = 0; i < propertiesList.length; i++) {
        if (!propertiesList[i]) {
            continue;
        }
        let _price = propertiesList[i].currentListingPrice.price;
        if (!_price) {
            continue;
        }
        if (!maxPrice || (_price > maxPrice)) {
            maxPrice = _price;
        }
    }
    return maxPrice;
}


function _setMinMaxPriceForBucket(bucketData, config, {req}) {
    if (!bucketData.propertiesList || !bucketData.propertiesList.length) {
        return;
    }
    let numberFormat = req.locals.numberFormat;
    for (let i = 0; i < bucketData.propertiesList.length; i++) {
        if (!bucketData.propertiesList[i]) {
            continue;
        }
        let _price = bucketData.propertiesList[i].currentListingPrice.price,
            _size = bucketData.propertiesList[i].property.size,
            _pricePerSqft;
        if (!_price) {
            continue;
        }
        if (!bucketData.minPrice) {
            let _obj = {
                readable: utils.formatNumber(_price, { returnSeperate : true, convertThousand : true, seperator : numberFormat }),
                raw: _price
            };
            bucketData.minPrice = _.clone(_obj);
            bucketData.maxPrice = _.clone(_obj);
        } else if (bucketData.maxPrice.raw < _price) {
            bucketData.maxPrice.raw = _price;
            bucketData.maxPrice.readable = utils.formatNumber(_price, { returnSeperate : true, convertThousand : true, seperator : numberFormat });
        } else if (bucketData.minPrice.raw > _price) {
            bucketData.minPrice.raw = _price;
            bucketData.minPrice.readable = utils.formatNumber(_price, { returnSeperate : true, convertThousand : true, seperator : numberFormat });
        }

        _pricePerSqft = (_size && _price / _size) || _price;

        if (!bucketData.minPricePerSqft) {
            let _obj = {
                readable: utils.formatNumber(Math.round(_pricePerSqft), { type : 'number', seperator : numberFormat }),
                raw: _pricePerSqft
            };
            bucketData.minPricePerSqft = _.clone(_obj);
            bucketData.maxPricePerSqft = _.clone(_obj);
        } else if (bucketData.maxPricePerSqft.raw < _pricePerSqft) {
            bucketData.maxPricePerSqft.raw = _pricePerSqft;
            bucketData.maxPricePerSqft.readable = utils.formatNumber(Math.round(_pricePerSqft), { type : 'number', seperator : numberFormat });
        } else if (bucketData.minPricePerSqft.raw > _pricePerSqft) {
            bucketData.minPricePerSqft.raw = _pricePerSqft;
            bucketData.minPricePerSqft.readable = utils.formatNumber(Math.round(_pricePerSqft), { type : 'number', seperator : numberFormat });
        }
    }
    if (bucketData.minPrice && bucketData.minPrice.raw && config.listingCategory.indexOf("Rental") === -1) {
        bucketData.emi = utils.formatNumber(
            utils.calculateEMI(bucketData.minPrice.raw, globalConfig.emi_rate, globalConfig.emi_tenure, globalConfig.downpayment_percent), 
            { type : 'number', seperator : numberFormat }
        );
    }
}


function _minBucketRange(number, listingCategory) {
    let saleType = "buy";
    if (listingCategory.indexOf("Rental") >= 0) {
        saleType = "rent";
    }
    let budgetSteps = config.budgetSteps[saleType];
    for (var i = 0; i < budgetSteps.length; i++) {
        if (budgetSteps[i] < number) {
            continue;
        } else if (number === budgetSteps[i]) {
            return number;
        } else if (budgetSteps[i] > number) {
            return budgetSteps[i - 1] || budgetSteps[i];
        }
    }
}

function _adjustmentRatioForBucket(price, sqft, listingCategory) {
    let pricePerSqft = (price / sqft);
    if (listingCategory.indexOf("Rental") >= 0) {
        if (pricePerSqft < 5) {
            return 5000;
        } else if (pricePerSqft < 20) {
            return 10000;
        } else if (pricePerSqft < 35) {
            return 20000;
        } else if (pricePerSqft < 50) {
            return 30000;
        } else {
            return 50000;
        }
    } else {
        if (pricePerSqft < 3000) {
            return 1500000;
        } else if (pricePerSqft < 5000) {
            return 2500000;
        } else if (pricePerSqft <= 10000) {
            return 3000000;
        } else if (pricePerSqft > 10000) {
            return 5000000;
        }
    }
}


function _maxBucketRange(number, minimumLimit, listingCategory) {
    let saleType = "buy";
    if (listingCategory.indexOf("Rental") >= 0) {
        saleType = "rent";
    }
    let budgetSteps = config.budgetSteps[saleType];
    for (var i = 0; i < budgetSteps.length; i++) {
        if (budgetSteps[i] < number) {
            continue;
        } else if (number === budgetSteps[i]) {
            return number;
        } else if (budgetSteps[i] > number) {
            if ((budgetSteps[i] - number < number - budgetSteps[i - 1]) || (budgetSteps[i - 1] === minimumLimit)) {
                return budgetSteps[i];
            }
            return budgetSteps[i - 1];
        }
    }
}

function isDummy(status, possessionTimestamp) {
    if (status != 'Ready to Move In' || new Date().getTime() < possessionTimestamp) {
        return true;
    }
    return false;
}

function mergeAllProperties(data = {}) {
    let allProperties = [];
    for (var key in data) {
        for (var priceRange in data[key]) {
            allProperties = allProperties.concat(data[key][priceRange]);
        }
    }
    return allProperties;
}

function getMobileConfigurationData(dataArray = [], {req}) {
    let result = [];
    _.forEach(dataArray, (listing) => {
        listing && result.push(listingService.parseListing(listing, req.locals)); //jshint ignore:line
    });
    return result;
}

function makeBhkBuckets(dataArray = []) {
    let bucketInit = {
        listings: [],
        count: 0
    };
    let bhkBucket = [
        _.cloneDeep(bucketInit), //plots
        _.cloneDeep(bucketInit), //1bhk
        _.cloneDeep(bucketInit), //2bhk
        _.cloneDeep(bucketInit), //3bhk
        _.cloneDeep(bucketInit) //3plusbhk
    ];
    dataArray = _.sortBy(dataArray, 'relevanceScore');
    _.forEach(dataArray, listing => {
        if (listing.unitType === "Plot") {
            bhkBucket[0].count++;
            bhkBucket[0].listings.push(listing);
            bhkBucket[0].displayText = 'plots';
            bhkBucket[0].name = 'plots';
            bhkBucket[0].propertyType = 'residential-plot';
        }
        switch (listing.beds) {
            case 1:
                bhkBucket[1].count++;
                bhkBucket[1].listings.push(listing);
                bhkBucket[1].displayText = '1 BHK';
                bhkBucket[1].name = 'oneBHK';
                bhkBucket[1].beds = 1;
                break;
            case 2:
                bhkBucket[2].count++;
                bhkBucket[2].listings.push(listing);
                bhkBucket[2].displayText = '2 BHK';
                bhkBucket[2].name = 'twoBHK';
                bhkBucket[2].beds = 2;
                break;
            case 3:
                bhkBucket[3].count++;
                bhkBucket[3].listings.push(listing);
                bhkBucket[3].displayText = '3 BHK';
                bhkBucket[3].name = 'threeBHK';
                bhkBucket[3].beds = 3;
                break;
            default:
                if (listing.beds > 3) {
                    bhkBucket[4].count++;
                    bhkBucket[4].listings.push(listing);
                    bhkBucket[4].displayText = '3+ BHK';
                    bhkBucket[4].name = 'threeplusBHK';
                    bhkBucket[4].beds = '3plus';
                }
                break;
        }
    });
    _.forEach(bhkBucket, bucket => {
        if (bucket.count > 5) {
            bucket.listings = bucket.listings.slice(0, 4);
            bucket.showMore = true;
        }
    });

    return bhkBucket;
}

function _checkBucketCount(bucketArr) {
    let showBucket = false;
    if (bucketArr) {
        _.forEach(bucketArr, obj => {
            if (obj.count) {
                showBucket = true;
            }
        });
    }
    return showBucket;
}

function _parseConfigurationBucketsMobile(data, params, {req}) {
    let configuration = data.configuration && _mergePrimaryResaleBuckets(data.configuration),
        buy = configuration && configuration.Primary && mergeAllProperties(configuration.Primary),
        rental = configuration && configuration.Rental && mergeAllProperties(configuration.Rental),
        buyConfigurationDataArray = buy && getMobileConfigurationData(buy, {req}),
        rentalConfigurationDataArray = rental && getMobileConfigurationData(rental, {req}),
        buyBucket = buyConfigurationDataArray && makeBhkBuckets(buyConfigurationDataArray),
        rentalBucket = rentalConfigurationDataArray && makeBhkBuckets(rentalConfigurationDataArray),
        showBuyBucket = _checkBucketCount(buyBucket),
        showRentBucket = _checkBucketCount(rentalBucket);
    let result = {
        mobileConfigurationBuckets: {
            buyBucket: buyBucket,
            rentalBucket: rentalBucket
        },
        buy: showBuyBucket,
        rent: showRentBucket
    };
    return result;
}


function _parseConfigurationBuckets(data, params, {req}) {
    let projectId = params.projectId,
        companyId = params.companyId,
        projectProperties = params.projectProperties,
        sellerPropertyCount = data.sellerPropertyCount,
        projectStatus = params.projectStatus,
        possessionTimestamp = params.possessionTimestamp,
        priceRange = {},
        bucketFunnel = {},
        allProperties = [],
        configurationsAvailable = {};
    if (data.configuration) {
        let config = _mergePrimaryResaleBuckets(data.configuration),
            configKeyArray = Object.keys(config),
            allSellerArray = [];
        if (configKeyArray.length) {
            let configKeys = Object.keys(config),
                configKeysLength = configKeys.length;

            for (let i = 0; i < configKeysLength; i++) {
                let _category = configKeys[i],
                    categoryBucket = config[_category],
                    unitTypeArray = Object.keys(categoryBucket);
                if (unitTypeArray.length) {
                    for (let j = 0, typeLength = unitTypeArray.length; j < typeLength; j++) {
                        let rangeBucket = categoryBucket[unitTypeArray[j]],
                            _unitType = unitTypeArray[j],
                            rangeArray = Object.keys(rangeBucket);
                        if (rangeArray.length) {
                            for (let k = 0, rangeLength = rangeArray.length; k < rangeLength; k++) {
                                let currentRangeArray = rangeBucket[rangeArray[k]];
                                allSellerArray = allSellerArray.concat(currentRangeArray);
                                if (currentRangeArray && currentRangeArray.length) {
                                    for (let l = 0; l < currentRangeArray.length; l++) {
                                        if (currentRangeArray[l].property && currentRangeArray[l].property.optionCategory && currentRangeArray[l].property.optionCategory === "Actual") {
                                            allProperties.push(currentRangeArray[l].property.propertyId);
                                        }
                                        bucketFunnel[`${_category}`] = bucketFunnel[`${_category}`] || {};
                                        bucketFunnel[`${_category}`][`${_unitType}`] = bucketFunnel[`${_category}`][`${_unitType}`] || {};
                                        bucketFunnel[`${_category}`][`${_unitType}`].unitTypeId = _getMappedUnitType(_unitType);
                                        bucketFunnel[`${_category}`][`${_unitType}`].propertiesList = bucketFunnel[`${_category}`][`${_unitType}`].propertiesList || [];
                                        bucketFunnel[`${_category}`][`${_unitType}`].propertiesList.push(currentRangeArray[l]);
                                    }
                                }
                            }

                            _bifurcatePropertiesIntoVariousPriceBuckets(bucketFunnel[`${_category}`][`${_unitType}`], {
                                sellerPropertyCount,
                                companyId,
                                unitType: _unitType,
                                projectId,
                                listingCategory: (_category == 'Primary') ? ['Primary', 'Resale'] : ['Rental'],
                                configurationsAvailable
                            }, {req});
                        }
                    }
                }
            }

            allSellerArray.sort((obj1, obj2) => {
                let scoreFirst = obj1.companySeller && obj1.companySeller.company && obj1.companySeller.company.score || 0,
                    scoreSecond = obj2.companySeller && obj2.companySeller.company && obj2.companySeller.company.score || 0;
                return scoreSecond - scoreFirst;
            });
            if (!companyId) {
                allSellerArray = _getUniqueSeller(allSellerArray, sellerPropertyCount, null, req);
                if (allSellerArray && allSellerArray.data) {
                    allSellerArray.data.splice(0, 1);
                }
            }
        }
        _prunePriceBuckets(bucketFunnel);
        if (!companyId) {
            _addDummyConfigurations({ bucketFunnel, projectProperties, projectId, configurationsAvailable, projectStatus, possessionTimestamp });
        }
        data.configuration = bucketFunnel;
        data.configurationList = _parseConfigList(bucketFunnel.Primary, {req});
        return {
            configurationBuckets: data,
            allSellers: allSellerArray && allSellerArray.data,
            priceRange: priceRange,
            allProperties: allProperties
        };
    }

}

function _parseConfigList(projectConfigs, {req}) {
    var configList = [];
    if (!projectConfigs) {
        return configList;
    }
    var mapPriceBuckets = function(priceBuckets) {
        if (!priceBuckets) {
            return configList;
        }
        priceBuckets.forEach(function(item) {
            if (item && item.bedrooms && item.minPrice && item.minPrice.raw !== 'dummy') {
                for (var bedroom in item.bedrooms) {
                    let bedroomObj = item.bedrooms[bedroom][0];
                    if (bedroomObj) {
                        configList.push({
                            "rawPrice": bedroomObj.minPrice.raw,
                            "readablePriceVal": bedroomObj.minPrice.readable.val,
                            "readablePriceUnit": bedroomObj.minPrice.readable.unit,
                            "bedroom": bedroom,
                            "size": bedroomObj.minSize,
                            "unitDisplayName": bedroom + ' bhk, ' + utils.formatNumber(bedroomObj.minSize, { type : 'number', seperator : req.locals.numberFormat }) + ' sq ft'
                        });
                    }
                }
            }
        });
    };
    //TODO: repeat this for all type of configurations
    if (projectConfigs.Apartment) {
        mapPriceBuckets(projectConfigs.Apartment.priceBuckets);
    }
    if (projectConfigs.Plot) {
        mapPriceBuckets(projectConfigs.Plot.priceBuckets);
    }
    if (projectConfigs.Villa) {
        mapPriceBuckets(projectConfigs.Villa.priceBuckets);
    }
    if (projectConfigs.BuilderFloor) {
        mapPriceBuckets(projectConfigs.BuilderFloor.priceBuckets);
    }
    return configList;
}

function _addDummyConfigurations(params) {
    let dummyProperties = [],
        bucketFunnel = params.bucketFunnel,
        projectProperties = params.projectProperties,
        projectId = params.projectId,
        configurationsAvailable = params.configurationsAvailable,
        projectStatus = params.projectStatus,
        possessionTimestamp = params.possessionTimestamp;

    _.forEach(projectProperties, (property) => {
        let __propertyId = property.propertyId;
        let found = false;
        for (let category in bucketFunnel) {
            for (let unitType in bucketFunnel[category]) {
                let _properties = bucketFunnel[category][unitType].propertiesList;
                _properties.forEach(function(p) { //jshint ignore:line
                    if (p.property && p.property.propertyId && p.property.propertyId == __propertyId) {
                        found = true;
                    }
                });
            }
        }
        if (!found) {
            dummyProperties.push(property);
        }
    });
    let dummyConfigurations = {},
        dummyPrice = {
            readable: {
                val: "dummy",
                unit: "dummy"
            },
            raw: "dummy"
        };
    if (dummyProperties.length) {
        dummyProperties.sort((obj1, obj2) => {
            let bhkFirst = obj1.bedrooms || 0,
                bhkSecond = obj2.bedrooms || 0;
            if (bhkFirst === bhkSecond) {
                let sizeFirst = obj1.size || 0,
                    sizeSecond = obj2.size || 0;
                return sizeFirst - sizeSecond;
            }
            return bhkFirst - bhkSecond;
        });
        dummyProperties.forEach((p) => {
            if (p.bedrooms && p.size) {
                let _p = {
                    property: p
                };
                //If we have configuration already available, we don't add it here .
                if (configurationsAvailable[p.unitType] && configurationsAvailable[p.unitType].indexOf(p.size) >= 0) {
                    return;
                }
                dummyConfigurations.Primary = dummyConfigurations.Primary || {};
                dummyConfigurations.Primary[p.unitType] = dummyConfigurations.Primary[p.unitType] || {};
                dummyConfigurations.Primary[p.unitType].unitTypeId = _getMappedUnitType(p.unitType);
                dummyConfigurations.Primary[p.unitType].priceBuckets = dummyConfigurations.Primary[p.unitType].priceBuckets || [{ minPrice: dummyPrice, maxPrice: dummyPrice, propertiesList: [] }];
                dummyConfigurations.Primary[p.unitType].priceBuckets[0].propertiesList.push(_p);
                let _bedroomBucket = dummyConfigurations.Primary[p.unitType].priceBuckets[0].bedrooms || {};
                _bedroomBucket[p.bedrooms] = _bedroomBucket[p.bedrooms] || [{}];
                _bedroomBucket[p.bedrooms][0].minPrice = _bedroomBucket[p.bedrooms][0].maxPrice = dummyPrice;
                _bedroomBucket[p.bedrooms][0].propertiesList = _bedroomBucket[p.bedrooms][0].propertiesList || [];
                if (!_bedroomBucket[p.bedrooms][0].minSize || _bedroomBucket[p.bedrooms][0].minSize > p.size) {
                    _bedroomBucket[p.bedrooms][0].minSize = p.size;
                }
                if (!_bedroomBucket[p.bedrooms][0].maxSize || _bedroomBucket[p.bedrooms][0].maxSize < p.size) {
                    _bedroomBucket[p.bedrooms][0].maxSize = p.size;
                }
                _bedroomBucket[p.bedrooms][0].propertiesList.push(_p);
                _bedroomBucket[p.bedrooms][0].balcony = _bedroomBucket[p.bedrooms][0].balcony || [];
                _bedroomBucket[p.bedrooms][0].bathrooms = _bedroomBucket[p.bedrooms][0].bathrooms || [];
                (p.balcony > 0 && _bedroomBucket[p.bedrooms][0].balcony.indexOf(p.balcony) === -1) && _bedroomBucket[p.bedrooms][0].balcony.push(p.balcony); //jshint ignore:line
                (p.bathrooms > 0 && _bedroomBucket[p.bedrooms][0].bathrooms.indexOf(p.bathrooms) === -1) && _bedroomBucket[p.bedrooms][0].bathrooms.push(p.bathrooms); //jshint ignore:line
                dummyConfigurations.Primary[p.unitType].priceBuckets[0].bedrooms = _bedroomBucket;
                dummyConfigurations.Rental = JSON.parse(JSON.stringify(dummyConfigurations.Primary));
            }
        });

        _.forEach(config.saleCategory, (category) => {
            // MO- 2033 remove dummy listing in case of rent and depend on projectStatus
            if (category === "Rental" && isDummy(projectStatus, possessionTimestamp)) {
                return;
            }
            _.forEach(config.dominantUnitTypes, (unitType) => {
                unitType = _.startCase(_.toLower(unitType));
                if (dummyConfigurations[category] && dummyConfigurations[category][unitType]) {
                    let _eachBedroomData = dummyConfigurations[category][unitType].priceBuckets[0].bedrooms,
                        bhkKeys;
                    bhkKeys = Object.keys(_eachBedroomData);
                    for (let i = 0; i < bhkKeys.length; i++) {
                        let _bhkArrayData = _eachBedroomData[bhkKeys[i]],
                            eachbhkBucketKeys = Object.keys(_bhkArrayData),
                            eachbhkBucketKeysLength = eachbhkBucketKeys.length;
                        for (let j = 0; j < eachbhkBucketKeysLength; j++) {
                            let _eachbhkBucketData = _bhkArrayData[eachbhkBucketKeys[j]];
                            _eachbhkBucketData.propertiesList = _eachbhkBucketData.propertiesList.filter((elem) => {
                                if (elem) {
                                    return elem;
                                }
                            });
                            if (!_eachbhkBucketData.propertiesList.length) {
                                delete _bhkArrayData[eachbhkBucketKeys[j]];
                                continue;
                            }
                            _eachbhkBucketData.bathrooms = _eachbhkBucketData.bathrooms && _eachbhkBucketData.bathrooms.sort() && _eachbhkBucketData.bathrooms.join(",");
                            _eachbhkBucketData.balcony = _eachbhkBucketData.balcony && _eachbhkBucketData.balcony.sort() && _eachbhkBucketData.balcony.join(",");
                            _eachbhkBucketData.serpFilterLink = utils.generateSerpFilter({
                                beds: bhkKeys[i],
                                minSize: _eachbhkBucketData.minSize,
                                maxSize: _eachbhkBucketData.maxSize,
                                propertyType: unitType.toLowerCase(),
                                listingType: category === 'Rental' ? 'rent' : 'buy',
                                projectId: projectId
                            });
                        }
                    }

                    /* Merge configuration with original bucket funnel */
                    if (bucketFunnel[category] && bucketFunnel[category][unitType]) {
                        bucketFunnel[category][unitType].priceBuckets.push(dummyConfigurations[category][unitType].priceBuckets[0]);
                    } else if (bucketFunnel[category] && !bucketFunnel[category][unitType]) {
                        bucketFunnel[category][unitType] = dummyConfigurations[category][unitType];
                    } else if (!bucketFunnel[category]) {
                        bucketFunnel[category] = dummyConfigurations[category];
                    }
                }
            });
        });
    }
}


function _prunePriceBuckets(data) {
    for (let category in data) {
        for (let unitType in data[category]) {
            let _priceBuckets = data[category][unitType].priceBuckets,
                priceBucketKeys = _priceBuckets && Object.keys(_priceBuckets),
                priceBucketKeysLength = priceBucketKeys && priceBucketKeys.length,
                priceBucketKeysDeleted = 0;
            for (let i = 0; i < priceBucketKeysLength; i++) {
                let bedroomsBucket = _priceBuckets[priceBucketKeys[i]].bedrooms,
                    bhkKeys = bedroomsBucket && Object.keys(bedroomsBucket),
                    bhkKeysLength = bhkKeys && bhkKeys.length,
                    bhkKeysDeleted = 0;
                for (let j = 0; j < bhkKeysLength; j++) {
                    let bhkeyArray = bedroomsBucket[bhkKeys[j]].filter((obj) => {
                        if (obj) {
                            return obj;
                        }
                    });
                    if (!bhkeyArray.length) {
                        bhkKeysDeleted++;
                    }
                }
                if (bhkKeysDeleted === bhkKeysLength) {
                    delete _priceBuckets[priceBucketKeys[i]];
                    priceBucketKeysDeleted++;
                }
            }
            if (priceBucketKeysDeleted === priceBucketKeysLength) {
                delete data[category][unitType];
            }
        }
    }
}


function _mergePrimaryResaleBuckets(config) {
    let primary = config.Primary || {},
        resale = config.Resale || {},
        typeArray = _.union(Object.keys(primary), Object.keys(resale)),
        length = typeArray.length,
        result = {},
        customizer = function(objValue, srcValue) {
            if (_.isArray(objValue)) {
                return _.union(objValue, srcValue);
            }
        };
    for (var i = 0; i < length; i++) {
        result[typeArray[i]] = _.mergeWith(primary[typeArray[i]], resale[typeArray[i]], customizer);
    }
    config.Primary = result;
    delete config.Resale;
    return config;
}


function _getUniqueSeller(priceBucket, sellerPropertyCount, companyId, req) {
    let sellerData = [],
        sellerCheck = {},
        facets = [],
        priceRange = {};
    for (let i = 0, length = priceBucket.length; i < length; i++) {
        let currentObject = priceBucket[i],
            currProperty = currentObject.property,
            companySeller = currentObject.companySeller || {},
            sellerId = companySeller && companySeller.company && companySeller.company.id;
        sellerId = companyId ? sellerId == companyId : sellerId;
        if (sellerId) {
            if (!sellerCheck[sellerId]) {
                sellerCheck[sellerId] = true;
                let company = companySeller.company || {},
                    user = companySeller.user || {};
                sellerData.push({
                    name: company.name,
                    rating: company.score ? (company.score / 2) : 0,
                    price: utils.formatNumber(currentObject.currentListingPrice ? currentObject.currentListingPrice.price : '', { precision : 2, seperator : req.locals.numberFormat }),
                    propertyCount: sellerPropertyCount[company.id], //_getPropertyCount(currentObject.sellerId, sellerList.facets.sellerId),
                    sellerURL: '#',
                    phone: (companySeller.user && companySeller.user.contactNumbers && companySeller.user.contactNumbers.length) ? companySeller.user.contactNumbers[0].contactNumber : 'N.A.',
                    id: company.id,
                    ratingClass: _getRatingClass((company.score / 2)),
                    displayImage: imageParser.appendImageSize(company.logo || user.profilePictureURL, 'thumbnail'),
                    avatar: utils.getAvatar(company.name)
                });
            }
            let bed = currProperty.bedrooms;
            if (bed && facets.indexOf(bed) == -1) {
                facets.push(bed);
            }
            if (!priceRange.min || currentObject.currentListingPrice.price < priceRange.min) {
                priceRange.min = currentObject.currentListingPrice.price;
            }
            if (!priceRange.max || currentObject.currentListingPrice.price > priceRange.max) {
                priceRange.max = currentObject.currentListingPrice.price;
            }
        } else {
            delete priceBucket[i];
        }
    }
    return {
        data: sellerData,
        facets: facets.sort(),
        priceRange: priceRange,
        propertyCount: companyId ? priceBucket.filter(function(value) {
            return value !== undefined;
        }).length : priceBucket.length
    };
}

function configurationBuckets(config = {}, { req }) { //jshint ignore:line
    let projectId = config.projectId,
        companyId = config.isSemPage,
        builderName = config.builderName,
        projectName = config.projectName,
        projectProperties = config.projectProperties,
        projectStatus = config.projectStatus,
        possessionTimestamp = config.possessionTimestamp,
        sellerIds = config.sellerUserIds,
        selectorObj = {
            "fields": ["resaleURL", "localityId", "mainImageURL", "projectId", "suburb", "city", "project", "locality", "unitType", "unitTypeId", "contactNumbers", "contactNumber", "bedrooms", "property", "propertyId", "user", "name", "score", "logo", "id", "companySeller", "company", "configuration", "Primary", "Apartment", "price", "currentListingPrice", "size", "bathrooms", "balcony", "optionCategory", "carpetArea", "listingCategory", "mainImage", "altText"]
        };
        if(sellerIds && sellerIds.length){
            selectorObj.filters = [];
            selectorObj.filters.push({
                key: "sellerId",
                value: sellerIds,
            });
            selectorObj.filters.push({
                key:"listingCategory",
                value:["Primary","Resale"]
            });
        }
        let selector = apiService.createSelector(selectorObj)
    return apiService.get(apiConfig.configurationBuckets({
        projectId,
        selector
    }), {
        req
    }).then(response => {
        let configBuckets = _parseConfigurationBuckets(response.data, {
            projectId,
            companyId,
            builderName,
            projectName,
            projectProperties,
            projectStatus,
            possessionTimestamp
        }, {req});

        let mobileConfigBuckets = _parseConfigurationBucketsMobile(response.data, {
            projectId, companyId, builderName, projectName, projectProperties, projectStatus, possessionTimestamp
        }, {req});
        mobileConfigBuckets.configurationBuckets = {};
        mobileConfigBuckets.configurationBuckets.configurationList = configBuckets && configBuckets.configurationBuckets && configBuckets.configurationBuckets.configurationList;
        return mobileConfigBuckets;

        //return configBuckets;
    }, err => {
        throw err;
    });
}

function getFutureSpaceLinks(config = {}, { req }) { //jshint ignore:line
    let propertyIdArray = config.propertyIdArray || [],
        fields = ['id', 'name', 'tableName', 'tableId', 'url', 'config', 'config.content', 'config.content.source'],
        filters = [{
            'filter': 'or',
            'type': 'equal',
            'key': 'tableId',
            'value': config.projectId
        }];
    for (let i = 0, length = propertyIdArray.length; i < length; i++) {
        filters.push({
            'filter': 'or',
            'type': 'equal',
            'key': 'tableId',
            'value': propertyIdArray[i]
        });
    }
    let urlConfig = apiConfig.futureSpaceLinks({ query: apiService.createFiqlSelector({ filters, fields }) });
    return apiService.get(urlConfig, { req }).then(response => {
        return response;
    }, error => {
        throw error;
    });
}

function getProjectSerpWithFilters(config = {}, { req }) { //jshint ignore:line
    let selector = config.selector || JSON.stringify({}),
        urlConfig = apiConfig.multipleProjects({ query: { selector } });
    return apiService.get(urlConfig, { req }).then((response) => {
        return response || {};
    }, err => {
        throw err;
    });
}

function getTopProjectWithFilters(config = {}, { req }) { //jshint ignore:line
    let selectorObj = {},
        //fields = config.fields,
        selector, urlConfig;
    selectorObj.filters = [];
    if (config.cityId) {
        selectorObj.filters.push({
            key: "cityId",
            value: config.cityId,
        });
    }
    if (config.suburbId) {
        selectorObj.filters.push({
            key: "suburbId",
            value: config.suburbId,
        });
    }
    if (config.localityId) {
        selectorObj.filters.push({
            key: "localityId",
            value: config.localityId,
        });
    }

    selectorObj.paging = {
        "start": config.start || 0,
        "rows": config.count
    };
    selectorObj.sort = {
        "key": "projectListingCount",
        "order": "DESC"
    };

    selector = apiService.createSelector(selectorObj);
    urlConfig = apiConfig.multipleProjects({ query: { selector } });
    return apiService.get(urlConfig, { req }).then((response) => {
        return response;
    }, err => {
        throw err;
    });
}

function getTopProjectPriceRange({ cityId, localityId, suburbId }, { req }) {
    return getTopProjectWithFilters({
        cityId,
        localityId,
        suburbId,
        count: 20 //00000000
    }, { req }).then((response) => {
        response = (response && response.data) || [];
        return {
            data: parseSimilarProjects(response, undefined, req.locals),
            totalCount: response.totalCount
        };
    }, (error) => {
        throw error;
    });
}

function parseBedrooms(bedroom, poojaRoom, studyRoom){
    if(studyRoom || poojaRoom) {
        bedroom += .5;
    }
    return bedroom && bedroom > 3 ? '3+' : bedroom;
}

function prepareMobileConfigurationBucket(projectProperties, galleryData = {}, {req}) {
    projectProperties = projectProperties || [];
    galleryData = galleryData.data || [];
    let finalBucket = {},
        currBckt, obj, bedroomCategory;
    for (let ref = 0; ref < projectProperties.length; ref++) {
        if (projectProperties[ref].optionCategory && projectProperties[ref].optionCategory.toLowerCase() === 'actual') {
            obj = projectProperties[ref];
            bedroomCategory = parseBedrooms(obj.bedrooms, obj.poojaRoom, obj.studyRoom);
            finalBucket[`${bedroomCategory}`] = finalBucket[`${bedroomCategory}`] || [];
            currBckt = {};
            currBckt['bhkInfo'] = bedroomCategory + ' BHK';
            currBckt['bedroomCategory'] = bedroomCategory;
            currBckt['bedrooms'] = obj.bedrooms;
            currBckt['bathrooms'] = obj.bathrooms;
            currBckt['balcony'] = obj.balcony || undefined;
            currBckt['size'] = obj.size ? utils.formatNumber(obj.size, { type : 'number', seperator : req.locals.numberFormat }) + ` ${obj.measure || ''}` : '';
            currBckt['unitType'] = obj.unitType;
            currBckt['imageUrl'] = obj.images && obj.images.length ? obj.images[0].absolutePath : '';
            currBckt['imageAlt'] = obj.images && obj.images.length ? obj.images[0].altText : '';
            currBckt['imageId'] = obj.images && obj.images.length ? obj.images[0].id : '';
            currBckt['imageGalleryIndex'] = currBckt.imageId ? _.findIndex(galleryData, { 'id': currBckt.imageId }) : '';
            finalBucket[`${bedroomCategory}`].push(currBckt);
        }
    }
    return {
        'bhkBuckets': finalBucket,
        'bhkBucketKeys': Object.keys(finalBucket) && Object.keys(finalBucket).sort()
    };
}

function getProjectDetailsWithoutImage(projectId, { req }) {
    let projectDetail = apiService.get(apiConfig.project({
        projectId: projectId
    }), {
        req: req,
        isPrimary: true
    });
    return projectDetail.then((response) => {
        response = response && response.data;
        return response;
    }, (err) => {
        throw err;
    });

}

function getProject(config = {}, req) { //jshint ignore:line
    let selectorObj = {
            fields: ["projectStatus", "projectId", "localityId", "cityId", "projectName", "name", "buyUrl", "overviewUrl", "builder", "displayName", "isBuilderListed"],
            filters: [],
            paging: {
                "start": 0,
                "rows": config.projectCount
            }
        },
        selector, urlConfig;

    if (config.cityId) {
        selectorObj.filters.push({
            key: "cityId",
            value: config.cityId,
        });
    }
    if (config.localityId) {
        selectorObj.filters.push({
            key: "localityId",
            value: config.localityId,
        });
    }
    selector = apiService.createSelector(selectorObj);
    urlConfig = apiConfig.getProject({ selector });

    return apiService.get(urlConfig, { req }).then(response => {
        if (response && response.data && response.data.items) {
            return _parseProject(response.data.items);
        }
    }, err => {
        throw err;
    });
}

function _parseProject(projectList = []) {
    let projectClassification = {};
    for (let i = 0, length = projectList.length; i < length; i++) {
        let currProjectStatus = projectList[i].projectStatus.toLowerCase();
        projectList[i].overviewUrl = projectList[i].overviewUrl && utils.prefixToUrl(projectList[i].overviewUrl);
        projectList[i].buyUrl = projectList[i].buyUrl && utils.prefixToUrl(projectList[i].buyUrl);
        projectList[i].rentUrl = projectList[i].rentUrl && utils.prefixToUrl(projectList[i].rentUrl);
        if (!projectClassification[currProjectStatus]) {
            projectClassification[currProjectStatus] = [];
        }
        projectClassification[currProjectStatus].push(projectList[i]);
    }
    return projectClassification;

}

function _parseFeaturedProjects(response,mappingData,appendSellerUserId=false){

    let projects = [],
        data = response.data || [],
        obj, temp, city;

    for(let i=0; i<data.length; i++){
         obj = data[i]; temp = {};
         city = obj.locality && obj.locality.suburb && obj.locality.suburb.city || {};

        temp.projectId = obj.projectId;
        temp.builderName = obj.builder && obj.builder.name;
        temp.builderDisplayName = obj.builder && obj.builder.displayName;
        temp.builderLogo = obj.builder && obj.builder.imageURL;
        temp.projectName = obj.name;
        if(obj.builder && obj.builder.activeStatus.toLowerCase()!='dummy'){
            temp.projectFullName = (temp.builderDisplayName || '') + ' ' + temp.projectName;
        } else {
            temp.projectFullName = temp.projectName;
        }
        temp.localityId = obj.locality && obj.locality.localityId;
        temp.localityName = obj.locality && obj.locality.label;
        temp.cityId = city.id;
        temp.cityName = city.label;
        temp.bedrooms = "";
        temp.propertyTypes = obj.propertyUnitTypes;
        temp.unitTypeIds = [];
        let plotIndex = temp.propertyTypes && temp.propertyTypes.indexOf('Plot');
        if(plotIndex > -1){
            temp.propertyTypes.splice(plotIndex,1);
            temp.propertyTypes.push('Plot');
        }
        for (let unitType of mappingService.getUnitTypes()) {
            if (temp.propertyTypes.indexOf(unitType.displayText) > -1) {
                temp.unitTypeIds.push(unitType.id);
            }
        }
        temp.propertyTypes = temp.propertyTypes && obj.propertyUnitTypes.join(',');
        
        if(obj.distinctBedrooms && obj.distinctBedrooms.length){
            temp.bedrooms = obj.distinctBedrooms.sort((a,b)=>{
                return (a-b);
            });
            let zeroBhkIndex = temp.bedrooms && temp.bedrooms.indexOf(0);
            if(zeroBhkIndex > -1){
                temp.bedrooms.splice(zeroBhkIndex,1);
            }
            temp.bhk = temp.bedrooms;
            temp.bedrooms = temp.bedrooms.length && `${temp.bedrooms.join(',')} BHK`;
        }
        temp.overviewUrl = utils.prefixToUrl(obj.overviewUrl);
        if(appendSellerUserId){
            temp.overviewUrl = `${temp.overviewUrl}?sellerUserIds=${JSON.stringify(mappingData.projectSellerMapping && mappingData.projectSellerMapping[temp.projectId] && mappingData.projectSellerMapping[temp.projectId])}`;
        }
        if (obj.projectSellers && obj.projectSellers.length){
            temp.projectSeller = utils.shuffleArray(obj.projectSellers)[0].sellerId;
        }
        let imageSize = globalConfig.imageSizes.small;
        temp.backgroundImage = obj.imageURL ? `${obj.imageURL}?width=${imageSize.width}&height=${imageSize.height}` : '';
        temp.originalBackgroundImage = obj.imageURL;
        temp.altText = obj.mainImage && obj.mainImage.altText || '' ;
        temp.price = {
            min: utils.formatNumber(obj.minResaleOrPrimaryPrice || obj.minPrice),
            max: utils.formatNumber(obj.maxResaleOrPrimaryPrice || obj.maxPrice)
        };
        temp.minBudget = obj.minResaleOrPrimaryPrice || obj.minPrice;
        temp.maxBudget = obj.maxResaleOrPrimaryPrice || obj.maxPrice;
        projects.push(temp);
    }
    return {
        totalCount: projects.length || 0,
        projects
    };
}
//convert project array into object
function _getProjectObject(data){
    let projects = data && data.projects;
    let projectData = {};
    for(let i=0; i<projects.length; i++){
        projectData[projects[i].projectId] = projects[i];
    }
    return projectData;
}

// putting project information in builder object
function _mapBuilderProjects(builderInformation,builderData,projectData){
     let builderId,builders=[];
     if(projectData && builderData){
         for (builderId of Object.keys(builderInformation.builderProjectMapping)) {
                if(builderData[builderId]){
                    builderData[builderId].projects=[];
                    builderData[builderId].projectTitles=[];
                    builderInformation.builderProjectMapping[builderId].forEach(function(projectId) {
                       builderData[builderId].projects.push(projectData[projectId]);
                       let projectTitleInfo={};
                       projectTitleInfo.projectName = projectData[projectId].projectFullName;
                       projectTitleInfo.projectId = projectData[projectId].projectId;
                       builderData[builderId].projectTitles.push(projectTitleInfo);
                    });
                    builders.push(builderData[builderId]);
                }
        }
    }
    return {
        totalCount: builders.length || 0,
        builders
    }; 
}

function getFeaturedProjects(config = {}, {req}){
    let selectorObj = {
            fields: ['projectId','name','overviewUrl','locality','localityId','suburb','city','id','label','distinctBedrooms','propertyUnitTypes','minResaleOrPrimaryPrice','maxResaleOrPrimaryPrice','minPrice','maxPrice','imageURL','mainImage','altText','builder','displayName','isBuilderListed','activeStatus'],
            filters: [],
            paging: {
                "start": 0,
                "rows": config.projectCount
            }
        }, selector;
    let showSponsoredProjects = false;

    if(config.fetchSponsoredProjects && config.fetchSponsoredProjects.toString()=="true"){
        showSponsoredProjects = true;
        let projectIds = sponsoredProjects[config.cityId] || [];
        if(!projectIds.length){
            return Promise.resolve([]);
        } 
        selectorObj.filters.push({
            key:"projectId",
            value:projectIds,
        });
    }else{
        if (config.cityId) {
            selectorObj.filters.push({
                key: "cityId",
                value: config.cityId,
            });
        }

        if (config.suburbId) {
            selectorObj.filters.push({
                key: "suburbId",
                value: config.suburbId,
            });
        }

        if (config.localityId) {
            selectorObj.filters.push({
                key: "localityId",
                value: config.localityId,
            });
        }
    }

    selector = apiService.createSelector(selectorObj);

    let urlObj =  showSponsoredProjects ? apiConfig.multipleProjects({ query: { selector } }) : apiConfig.featuredProjects({selector}) ;
    
    return apiService.get(urlObj, {req}).then((response)=>{
        return _parseFeaturedProjects(response);
    }, (err)=>{
        throw err;
    });
}
//this function first call the api giving it product type and scope details and get list of projectId
//list of projectIds are passed tp petra project api in selector and the response is passed to parsing project function
function getDeveloperProjects(config = {}, {req} = {}){ 
    return _fetchDeveloperProjectIds(config, {req}).then(function(response) {
        if(response.data && response.data.length) {
            let projectInfo = _extractProjectInfo(response.data);//function call to extract projectIds from the above api response
            return _getProjectsInformation(projectInfo,{req},true);
        } else {
            return {
                totalCount:0,
                projects:[]
            };
        }
    },(err)=> {
        throw err;
    });
}

//function to call jeans get api with productType,saleType,promotedType,scopeType and scopeEntityId is passed as queryParam
function _fetchDeveloperProjectIds(config = {}, {req} = {}){
    let query ={
        promotedType:'PROJECT',
        saleType:'',
        scopes:{},
        product:'',
        status:'Active'
    },source;
    //prefrence for scope
    //Highest to lowest (locality,suburb,city)
    query.saleType = config.saleType.toLowerCase()=="rent" ? 'RENT' : 'BUY';
    if (config.cityId && (!config.suburbId || !config.suburbId.length) && (!config.localityId || !config.localityId.length)){
        query.scopes["CITY"] = config.cityId;
        source = 'city';
    }
    if(config.suburbId && config.suburbId.length){
        query.scopes["SUBURB"] = config.suburbId;
        source = 'suburb';
    }
    if (config.localityId && config.localityId.length){
        query.scopes["LOCALITY"] = config.localityId;
        source = 'locality';
    }

    source = config.pageSource || source;
    query.product = developerProducts[config.productType][source]||'';
    query.scopes = JSON.stringify(query.scopes);
    
    let urlObj = apiConfig.developerCampaigns({ query});

    return apiService.get(urlObj, {req});  
}

//function to make array of projectIds by extracting it from the response of api callP
function _extractProjectInfo(data){
    let projectInfo={
        projectIds:[],
        projectSellerMapping:{}
    };
    data.forEach(function(project) {
        projectInfo.projectIds.push(project.entityId);
        projectInfo.projectSellerMapping[project.entityId]=projectInfo.projectSellerMapping[project.entityId] || [];
        project.productDetails && project.productDetails.forEach(function(product){
            product.scopeDetails && product.scopeDetails.forEach(function(scope){
                scope.userScopeDetails && scope.userScopeDetails.forEach(function(user){
                    projectInfo.projectSellerMapping[project.entityId].push(user.userId);
                });
                projectInfo.projectSellerMapping[project.entityId] = utils.shuffleArray(projectInfo.projectSellerMapping[project.entityId]);
                projectInfo.projectSellerMapping[project.entityId] = projectInfo.projectSellerMapping[project.entityId].length && projectInfo.projectSellerMapping[project.entityId].splice(0,1);
            });
        });
    });
    return projectInfo;
}

// extracting Project Information 
function _getProjectsInformation(data,{req} = {},appendSellerUserId=false){
   let selectorObj = {
        fields: ['projectId','name','overviewUrl','locality','localityId','suburb','city','id','label','distinctBedrooms','propertyUnitTypes','minResaleOrPrimaryPrice','maxResaleOrPrimaryPrice','minPrice','maxPrice','imageURL','mainImage','altText','builder','displayName','isBuilderListed','activeStatus'],
        paging: {rows: data.projectIds && data.projectIds.length,start:0},
        filters: [],
    }, selector;
    selectorObj.filters.push({
        key:"projectId",
        value:data.projectIds||[],
    });
    selector = apiService.createSelector(selectorObj);
    let urlObj = apiConfig.multipleProjects({ query: { selector } }) ; // petra api for fetching multiple project details

    return apiService.get(urlObj, {req}).then((response)=>{
        return _parseFeaturedProjects(response,data,appendSellerUserId); // parse the details of projects returned by petra api
    }, (err)=>{
        throw err;
    }); 
}


//main function for handling main api call for fetching featured developer
function getFeaturedDeveloper(config = {}, {req} = {}){ 
    return _fetchDeveloperBuilderDetails(config, {req}).then(function(response) {
        if(response.data && response.data.length) {
            let builderInformation = _extractBuilderInfo(response.data)||{};//function call to extract projectIds from the above api response  
            let projectDetailPromise = _getProjectsInformation(builderInformation,{req},true);
            let builderDetailPromise = builderService.getBuilderInformation(builderInformation.builderIds,{req},config);

           return Promise.all([projectDetailPromise, builderDetailPromise]).then((results)=>{ 

                let projectData = results[0] && _getProjectObject(results[0]);
                let builderData = results[1];
                let response = _mapBuilderProjects(builderInformation,builderData,projectData)||{};

                return response;
            }, (err) => {
               throw err;
            });

        }else{
            return {
                totalCount:0,
                builders:[]
            };
        }
    },(err)=> {
        throw err;
    });
}

// calling jean API for fetching builder details
function _fetchDeveloperBuilderDetails(config = {}, {req} = {}){
    let query ={
        promotedType:'SELLER',
        saleType:'',
        scopes:{},
        product:''
    },source;
    //prefrence for scope
    //Highest to lowest (locality,suburb,city)
    query.saleType = config.saleType.toLowerCase()=="rent" ? 'RENT' : 'BUY';
    if (config.cityId && (!config.suburbId || !config.suburbId.length) && (!config.localityId || !config.localityId.length)){
        query.scopes["CITY"] =config.cityId;
        source = 'city';
    }
    source = config.pageSource || source;
    query.product = developerProducts[config.productType][source]||'';
    query.scopes = JSON.stringify(query.scopes);
    
    let urlObj = apiConfig.developerCampaigns({ query});

    return apiService.get(urlObj, {req});  
}
//extracting buider Information
function _extractBuilderInfo(data){
    let builderInformation = {builderIds:[],projectIds:[],builderProjectMapping:{},projectSellerMapping:{}};
    let projectId ,spliceLength,i,builderUserId;

    data.forEach(function(builder) {
        //storing builderIds
        builderInformation.builderIds.push(builder.entityId);
        builderInformation.builderProjectMapping[builder.entityId]=builderInformation.builderProjectMapping[builder.entityId] || [];    
        builderUserId = '';
        builder.productDetails && builder.productDetails.forEach(function(builderProduct){
            builderProduct.scopeDetails && builderProduct.scopeDetails.forEach(function(scopeDetail){
                if(scopeDetail.userScopeDetails && scopeDetail.userScopeDetails.length){
                    builderUserId = scopeDetail.userScopeDetails[0].userId;
                }
            });
            builderProduct.productDetailMappings && builderProduct.productDetailMappings.forEach(function(builderPromotedProduct) {
                if(builderPromotedProduct.promotedType && builderPromotedProduct.promotedType.name == PROJECT ){
                    builderInformation.builderProjectMapping[builder.entityId].push(builderPromotedProduct.entityId); // creating builderId projectId mapping  
                }
            });
        });
        builderInformation.builderProjectMapping[builder.entityId] = utils.shuffleArray(builderInformation.builderProjectMapping[builder.entityId]); // shuffling projectId for a builder
        spliceLength = builderInformation.builderProjectMapping[builder.entityId].length < 2 ? builderInformation.builderProjectMapping[builder.entityId].length : 2; // taking at the max 2 project for builder 
        builderInformation.builderProjectMapping[builder.entityId] = builderInformation.builderProjectMapping[builder.entityId].splice(0,spliceLength); 
        
        for(i = 0 ; i < builderInformation.builderProjectMapping[builder.entityId].length; i++){
            projectId = builderInformation.builderProjectMapping[builder.entityId][i] || '';
            builderInformation.projectIds.push(projectId); // pushing projectId in the projectIds array
            builderInformation.projectSellerMapping[projectId] =[];
            builderInformation.projectSellerMapping[projectId].push(builderUserId);
         }
    });
    return builderInformation;
}

module.exports = {
    getProjectDetails,
    getProjectInfo,
    getSimilarProjects,
    getTopProjects,
    getTopProjectsInGA,
    getSimilarProjectAPIData,
    getTopAgent,
    prepareMobileConfigurationBucket,
    configurationBuckets,
    getHomeLoans,
    getSponsoredCompany,
    getFutureSpaceLinks,
    getProjectSerpWithFilters,
    getTopProjectPriceRange,
    getTopProjectWithFilters,
    getProjectDetailsWithoutImage,
    parseSimilarProjects,
    getAvailability,
    getAmenityIds,
    parseTopAgentForLead,
    getMappedUnitTypeIds,
    getSponsoredSeller,
    getProject,
    getFeaturedProjects,
    getDeveloperProjects, // used for returning the developer projects details
    getFeaturedDeveloper,
    getPaidSellers,
    parseProjectData: _parseFeaturedProjects,
    getSellerSpecificProjectDetail
};
