"use strict";

const apiService = require('services/apiService'),
       logger = require('services/loggerService'),
       apiConfig = require('configs/apiConfig');

var trackerCookiesService = {};

trackerCookiesService.setTrackingCookies = function (req, res) {

     logger.info("Calling tracking cookie ...");
     let referrerCookie = req.cookies["REFERER"],
        currentReferrer = req.headers['referer'];

    // set the referer cookie either if it doesn't belong to makaan.com or
     //referrer cookie is not set
    if(currentReferrer && currentReferrer.indexOf("www.makaan.com") < 0) {
         res.cookie("REFERER", currentReferrer, {"path":"/"});
     } else if(!referrerCookie && currentReferrer){
         res.cookie("REFERER", currentReferrer, {"path":"/"});
     }

     let apiUrl = apiConfig.trackerCookies();
        apiUrl.qs = req.query || {};
     return apiService.get(apiUrl,{res, req});

};

module.exports = trackerCookiesService;
