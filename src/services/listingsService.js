'use strict';

const moment = require('moment'),
    apis = require('configs/apiConfig'),
    apiService = require('services/apiService'),
    apiConfig = require('configs/apiConfig'),
    logger = require('services/loggerService'),
    maps = require('services/mappingService'),
    propertyMap = require('services/propertyMappingService'),
    selectorService = require('services/serpSelectorService'),
    utils = require('services/utilService'),
    globalConfig = require('configs/globalConfig'),
    benefitTagConfig = require('configs/benefitTagConfig'),
    imageParser = require('services/imageParser'),
    MasterdetailsService = require('services/masterDetailsService'),
    sharedConfig = require('public/scripts/common/sharedConfig'),
    _ = utils._;

var DUMMY_PROJECT_STATUS = "DUMMY",
    ACTIVE_PROJECT_STATUSES = ["ACTIVE", "ACTIVEINMAKAAN"],
    POSTED_BY_BROKER = "BROKER",
    POSTED_BY_OWNER = "OWNER",
    POSTED_BY_BUILDER = "BUILDER",
    RENTAL_LISTING_CATEGORY = "RENTAL",
    SHOP_PROPERTY_TYPE = "SHOP";

const recencyConfig = [{
    type: "hour",
    typeVal: 4,
    displayText: "just now"
},{
    type: "hour",
    typeVal: 12,
    displayText: "6 Hrs ago"
},{
    type: "hour",
    typeVal: 24,
    displayText: "Today"
},{
    type: "day",
    typeVal: 2,
    displayText: "Yesterday"
},{
    type: "day",
    typeVal: 4,
    displayText: "2 Days ago"
},{
    type: "day",
    typeVal: 7,
    displayText: "This Week"
},{
    type: "day",
    typeVal: 30,
    displayText: "a Week ago"
},{
    type: "day",
    typeVal: 60,
    displayText: "a Month ago"
},{
    type: "day",
    typeVal: 365,
    displayText: "2 Months ago"
}], recencyAllowedCities = [18],
MAX_LOCALITY_CHAR_LENGTH = 22,
SEVENTY_TWO_HRS_MILLISEC = 259200000, // 72*60*60*1000

QUICK_FILTERS = {
    allowedFilters: ["budget",'propertyType','beds','postedBy','area','furnished'],
    priorityMapping: {
        buy:{
            1:'budget',
            2:'propertyType',
            3:'beds',
            4:'postedBy',
            // 5:'area'
        },
        rent:{
            1:'budget',
            2:'propertyType',
            3:'beds',
            4:'postedBy',
            5:'furnished'
        }
    },
    normalFilterMapping: {
        "budget" : "budgetQuickFilter",
        "area": "areaQuickFilter"
    }
};

var listings = {};
var makaanSelectCities = sharedConfig.makaanSelectCities;


let buyKeyPoints = [
                        //{key: 'propertyStatus', label: '', listingCategoryPriority:2},
                        {key: 'propertyAge', label: 'old', listingCategoryPriority:1},
                        {key: 'possession', label: 'Possession by', listingCategoryPriority:2},
                        {key: 'bathrooms', label: 'Bathrooms', listingCategoryPriority:3},
                        {key: 'floor', label: 'floor', listingCategoryPriority:4},
                        {key: 'listingCategory', label: '', listingCategoryPriority:5},
                        {key: 'facing', label: 'facing', listingCategoryPriority:6, filter:"facingId"},
                        {key: 'balcony', label: 'balcony', listingCategoryPriority:7},
                        {key: 'viewDirection', label: 'facing',  listingCategoryPriority:8, filter:"listingViewDirectionIds"},
                        {key: 'taxonomyAmenity', label: '', listingCategoryPriority:9, filter:"listingAmenitiesIds"}
                    ];

let rentKeyPoints = [
                        {key: 'securityDeposit', label: 'deposit'},
                        {key: 'possession', label: 'availability'},
                        {key: 'tenantTypes', label: '' },
                        {key: 'bathrooms', label: 'bathrooms' },
                        {key: 'floor', label: 'floor' },
                        {key: 'balcony', label: 'balcony' },
                        {key: 'facing', label: 'facing', filter:'facingId'},
                        {key: 'taxonomyAmenity', label: '', filter:"listingAmenitiesIds"},
                        {key: 'viewDirection', label: 'facing', filter:"listingViewDirectionIds"}
                    ];

let plotKeyPoints = [
                        {key: 'propertyAge', label: 'old'},
                        {key: 'possession', label: 'Possession by'},
                        {key: 'openSides', label: 'open sides'},
                        {key: 'bookingAmount', label: 'booking amount'},
                        {key: 'facing', label: 'facing' },
                        {key: 'ownershipType', label: 'ownership' },
                        {key: 'taxonomyAmenity', label: '', filter:"listingAmenitiesIds"},
                        {key: 'viewDirection', label: 'facing', filter:"listingViewDirectionIds"}
                    ];

let shopBuyKeyPoints = [
                        {key: 'possession', label: 'Possession by'},
                        {key: 'propertyAge', label: 'old'},
                        {key: 'floor', label: 'floor'},
                        {key: 'facing', label: 'facing' },
                        {key: 'listingCategory', label: ''},
                    ];

let officeBuyKeyPoints = [
                        {key: 'possession', label: 'Possession by'},
                        {key: 'propertyAge', label: 'old'},
                        {key: 'floor', label: 'floor'},
                        {key: 'listingCategory', label: ''},
                    ];

let shopRentKeyPoints = [
                        {key: 'possession', label: 'availability'},
                        {key: 'floor', label: 'floor' },
                        {key: 'facing', label: 'facing' },
                        {key: 'bathrooms', label: 'bathrooms' },
                    ];

let officeRentKeyPoints = [
                        {key: 'possession', label: 'availability'},
                        {key: 'floor', label: 'floor' },
                        {key: 'bathrooms', label: 'bathrooms' },
                    ];                                        

listings.generateDescriptionTitle = function(urlDetail, listing){
    let pageLevel = urlDetail.pageLevel && urlDetail.pageLevel.toLowerCase(),
        pageType = urlDetail.pageType && urlDetail.pageType.toLowerCase(),
        placeName = urlDetail.placeName && urlDetail.placeName.toLowerCase(),
        h3 = urlDetail.h3Title || '',
        descriptionTitle = '',

        bhk = listing.bedrooms ? listing.bhkInfo : '',

        projectName = listing.project.fullName || '',
        cityName = listing.cityName || '',
        localityName = listing.localityName || '',
        builderName = listing.project.builderName || '',
        suburbName = listing.suburbName || '',

        placeTitle = placeName ? `near ${placeName} ${cityName}` : '',
        localityTitle = localityName && cityName ? `in ${localityName} ${cityName}` : '',
        //builderTitle = localityName ? `in ${localityName}` : '',
        projectTitle = projectName && localityName ? `in ${projectName} ${localityName}` : '',
        //cityTitle = cityName && localityName ? `in ${localityName} ${cityName}` : '',
        suburbTitle = localityName && suburbName ? `in ${suburbName} ${localityName}` : '';


    if (pageType && pageType.indexOf('nearby') > -1) {
        descriptionTitle = `${bhk} ${h3} ${placeTitle}`;
        return descriptionTitle;
    }

    switch (pageLevel) {
        case "builder":
        case "buildertaxonomy":
            descriptionTitle = `${bhk} ${h3} ${builderName} ${localityTitle}`;
            break;
        case "buildercity":
            descriptionTitle = `${bhk} ${h3} ${builderName} ${cityName} ${localityTitle}`;
            break;
        case "project":
        case "projecttaxonomy":
            descriptionTitle = `${bhk} ${h3} ${projectTitle}`;
            break;
        case "city":
        case "citytaxonomy":
            descriptionTitle = `${bhk} ${h3} ${localityTitle}`;
            break;
        case "suburb":
        case "suburbtaxonomy":
            descriptionTitle = `${bhk} ${h3} ${suburbTitle}`;
            break;
        default:
            descriptionTitle = `${bhk} ${h3} ${localityTitle}`;
    }
    return descriptionTitle;
};

function _getListingConfigData(listing, index){
    let isPaidSeller = false;
    if(listing.sellerTransactionStatus && (listing.sellerTransactionStatus.isExpertDealMaker || listing.sellerTransactionStatus.isDealMaker)){
        isPaidSeller=true;
    }
    return {
        "selector": listing.selector,
        "sellerType": listing.postedBy.type,
        "builderId": listing.project.builderId,
        "builderName": listing.project.builderName,
        "companyType": listing.postedBy.type,
        "companyId": listing.postedBy.id,
        "companyUserId": listing.postedBy.userId,
        "companyPhone": listing.postedBy.phone,
        "companyImage": listing.postedBy.image,
        "backgroundColor": listing.postedBy.backgroundColor,
        "textColor": listing.postedBy.textColor,
        "nameText":listing.postedBy.nameText,
        "companyName": listing.postedBy.name,
        "companyRating": listing.postedBy.rating,
        "verificationDate": listing.verificationDate,
        "serverTime": listing.currentServerTime,
        "listingUrl": listing.url,
        "imageUrl": listing.mainImageURL,
        "id":listing.listingId,
        "propertyId":  listing.propertyId,
        "projectId": listing.projectId,
        "projectName": listing.projectName,
        "projectStatus": listing.propertyStatus ? listing.propertyStatus : "N/A",
        "mainImageId":listing.defaultImageId ? listing.defaultImageId : "",
        "imageCount": listing.imagesCount,
        "latitude": listing.latitude,
        "longitude": listing.longitude,
        "localityName": listing.localityName,
        "suburbName": listing.suburbName,
        "cityName": listing.cityName,
        "fullName": listing.project.fullName,
        "localityUrl": listing.localityOverviewUrl,
        "rank": index,
        "price": listing.price ? listing.price : "",
        "unitTypeId": listing.propertyTypeId ? listing.propertyTypeId : "",
        "propertyType": listing.propertyType ? listing.propertyType : "",
        "bedrooms": listing.bedrooms ? listing.bedrooms : "",
        "isPlot": listing.isPlot ? true : false,
        "isApartment": listing.isApartment ? true : false,
        "localityId": listing.localityId,
        "suburbId": listing.suburbId,
        "cityId": listing.cityId,
        "listingScore": listing.listingScore,
        "listingCategory": listing.apiListingCategory ? listing.apiListingCategory : "",
        "isCommercial": listing.isCommercialListing ? listing.isCommercialListing : "",
        "size":listing.sizeInfo,
        "isRk":listing.rk ? true : false,
        "isPentHouse":listing.penthouse ? true : false,
        "isStudio":listing.studio ? true : false,
        "defaultImageId": listing.defaultImageId ? listing.defaultImageId : "",
        "sellerCallRatingCount": listing.sellerCallRatingCount || 0,
        "sellerCompanyFeedbackCount": listing.sellerCompanyFeedbackCount || 0,
        "isAccountLocked": listing.isAccountLocked,
        isPaidSeller,
        "sellerTransactionStatus": listing.sellerTransactionStatus,
        "isOriginalSearchResult":listing.isOriginalSearchResult || false,
        "isMakaanSelectSeller":listing.isMakaanSelectSeller || false,
        "mainImageWidth": listing.mainImageWidth,
        "mainImageHeight": listing.mainImageHeight,
        "recency": listing.recency
    };
}

function highlightPhrase(fullDescription, phrases){
    phrases && phrases.forEach(item => {
        fullDescription = fullDescription.replace(item, `<span>${item}</span>`)
    });
    return fullDescription;
}

function _parseIndividualListing(apiListing,extraParams,index){
    extraParams = extraParams || {};

    let listing={},
        numberFormat = extraParams.locals.numberFormat,
        buyCardKeyPoints = [].concat(buyKeyPoints),
        urlDetail = extraParams.urlDetail || {},
        pageType = urlDetail.pageType && urlDetail.pageType.toLowerCase();
    let filters = urlDetail.filters || {};

    let currentListingPrice = apiListing.currentListingPrice,
        mainImage = apiListing.mainImage || {},
        seller = apiListing.companySeller || {},
        sellerCompany = seller.company || {},
        sellerUser = seller.user || {},
        property = apiListing.property,
        project = property.project,
        builder = project.builder,
        locality = project.locality,
        suburb = locality.suburb,
        city = suburb.city;

    listing.negotiable = apiListing.negotiable;
    listing.postedDate = apiListing.postedDate;
    listing.allocation = apiListing.allocation;
    listing.verificationDate = apiListing.verificationDate;
    listing.listingId = apiListing.id;
    listing.propertyId = property.propertyId;
    listing.url = utils.prefixToUrl(apiListing.resaleURL);
    listing.isOriginalSearchResult = extraParams.isOriginalSearchResult;

    listing.description = (apiListing.description)|| '';
    let descText = utils.stripHtmlContent(listing.description, 200, true, false, '...', true);
    listing.description = descText.description || '';
    listing.showReadMore = !descText.error && listing.description.length > 100;
    
    if(apiListing.descriptionPhrases && apiListing.descriptionPhrases.length){
        listing.fullDescription = highlightPhrase(apiListing.description, apiListing.descriptionPhrases);
        listing.descriptionPhrases = apiListing.descriptionPhrases || [];
    }
    
    listing.selector = 'listing_' + listing.listingId;
    listing.propertyType = maps.getPropertyType(property.unitTypeId);
    listing.developedBy = builder.name && ('Developed by ' + builder.name) || '';
    var propertyTypeLowercase = listing.propertyType && listing.propertyType.toLowerCase();
    listing.propertyTypeId = property.unitTypeId;
    listing.isPlot = propertyTypeLowercase === 'residential plot' ? true : false;
    listing.isApartment = propertyTypeLowercase === 'apartment' ? true : false;
    listing.size = property.size;
    listing.measure = property.measure;
    listing.rk = property.rk;
    listing.studio = property.studio;
    listing.penthouse = property.penthouse;
    listing.bhkInfo = property.bedrooms + ((property.rk) ? ' RK' : ' BHK');
    listing.sizeInfo = property.size ? utils.formatNumber(property.size, { type : 'number', seperator : numberFormat }) + ' ' + property.measure : '';
    listing.latitude = apiListing.listingLatitude;
    listing.longitude = apiListing.listingLongitude;
    listing.listingScore = apiListing.qualityScore;
    listing.apiListingCategory = apiListing.listingCategory;
    listing.isCommercialListing = apiListing.isCommercialListing;
    if (extraParams.locals && extraParams.locals.environment == 'beta') {
        listing.listingDebugInfo = apiListing.listingDebugInfo;
    }

    listing.studyRoom = property.studyRoom;
    listing.servantRoom = property.servantRoom;
    listing.pricePerUnitArea = currentListingPrice.pricePerUnitArea;
    listing.readablePricePerUnitArea = utils.formatNumber(listing.pricePerUnitArea, { type : 'number', seperator : numberFormat });
    listing.price = currentListingPrice.price;
    listing.formattedPrice = utils.formatNumber(currentListingPrice.price, { type : 'number', seperator : numberFormat });
    listing.localityAvgPrice = apiListing.localityAvgPrice;
    listing.fullPrice = listing.price;
    listing.priceValue = listing.price ? utils.formatNumber(listing.price, { precision : 2, returnSeperate : true, seperator : numberFormat }) : 'NA';
    listing.relativeVerifiedDate = moment(apiListing.displayDate || apiListing.verificationDate).fromNow();

    listing.localityBuyUrl = utils.prefixToUrl(locality && locality.buyUrl);
    listing.localityRentUrl = utils.prefixToUrl(locality && locality.rentUrl);
    listing.localityOverviewUrl = utils.prefixToUrl(locality && locality.overviewUrl);
    listing.localityId = locality && locality.localityId;
    // commented because we are not handling suburb overview page right now
    //listing.suburbOverviewUrl = utils.prefixToUrl(suburb && suburb.overviewUrl);
    listing.suburbId = suburb && suburb.id;
    listing.suburbBuyUrl = utils.prefixToUrl(suburb && suburb.buyUrl);
    listing.suburbRentUrl = utils.prefixToUrl(suburb && suburb.rentUrl);
    listing.projectOverviewUrl = utils.prefixToUrl(project && project.overviewUrl);
    listing.cityOverviewUrl = utils.prefixToUrl(city && city.overviewUrl);

    listing.throughCampaign = apiListing.throughCampaign;
    listing.addedByPromoter = apiListing.addedByPromoter;
    listing.propertyAge = null;
    let minConstructionCompletionDate = apiListing.minConstructionCompletionDate ? moment(apiListing.minConstructionCompletionDate) : null,
        maxConstructionCompletionDate = apiListing.maxConstructionCompletionDate ? moment(apiListing.maxConstructionCompletionDate) : null;

    if (minConstructionCompletionDate || maxConstructionCompletionDate) {

        minConstructionCompletionDate = maxConstructionCompletionDate; // in case minConstructionCompletionDate is missing

        let totallMonths = moment().diff(minConstructionCompletionDate, 'months');

        let years = Math.floor(totallMonths / 12),
            months = totallMonths % 12;

        listing.propertyAge = years;
        if (months || (years === 0)) { // years == 0 is the case if property is some days old
            listing.propertyAge += ` - ${years+1}`;
        }

        listing.propertyAge += (years === 0 || (years == 1 && !months)) ? ' year' : ' years';
    }

    listing.halls = property.halls;
    listing.facing = apiListing.facingId ? maps.getFacingDirection(apiListing.facingId) : null;
    listing.openSides = apiListing.noOfOpenSides;
    listing.bookingAmount = apiListing.bookingAmount ? utils.formatNumber(apiListing.bookingAmount, {precision : 2, seperator : numberFormat}) : null;
    listing.securityDeposit = apiListing.securityDeposit ? utils.formatNumber(apiListing.securityDeposit, {precision : 2, seperator : numberFormat}) : null;
    listing.ownershipType = apiListing.ownershipTypeId ? maps.getOwnershipType(apiListing.ownershipTypeId) : null;
    listing.furnished = maps.getFurnishingStatus(apiListing.furnished) || apiListing.furnished;
    listing.isReadyToMove = maps.isReadyToMove(apiListing.constructionStatusId);
    listing.propertyStatus = maps.getPropertyStatus(apiListing.constructionStatusId);
    listing.bedrooms = property.bedrooms;
    listing.bathrooms = property.bathrooms || null;
    listing.balcony   = property.balcony || null;
    if([0,'0'].indexOf(apiListing.floor) > -1){
        listing.floor = 'Gr';
    } else {
        listing.floor = typeof apiListing.floor !== 'undefined' ? utils.ordinalSuffix(apiListing.floor || 0) : null;
    }
    listing.totalFloors = apiListing.totalFloors;
    listing.listingCategory = maps.getPropetyLeaseType(apiListing.listingCategory.toLowerCase());
    listing.possessionDateFormatted = apiListing.possessionDate ? moment(apiListing.possessionDate).format('DD-MM-YYYY') : null;
    listing.possession = apiListing.possessionDate ? moment(apiListing.possessionDate).format('MMM YYYY') : null;

    listing.keyPoints = [];

    let keyPointsArray;
    if (listing.listingCategory && listing.listingCategory.toUpperCase() === RENTAL_LISTING_CATEGORY) {
        if(listing.isCommercialListing) {
            if(listing.propertyType.toUpperCase() === SHOP_PROPERTY_TYPE) {
                keyPointsArray = shopRentKeyPoints;
            }
            else {
                keyPointsArray = officeRentKeyPoints;
            }
        }
        else {
            keyPointsArray = rentKeyPoints;
        }
        listing.isRent = true;
    } else {
        if(listing.isCommercialListing) {
            if(listing.propertyType.toUpperCase() === SHOP_PROPERTY_TYPE) {
                keyPointsArray = shopBuyKeyPoints;
            }
            else {
                keyPointsArray = officeBuyKeyPoints;
            }
        }
        else {
            keyPointsArray = listing.isPlot ? plotKeyPoints : buyCardKeyPoints;
        }
    }

    if(apiListing.tenantTypes && apiListing.tenantTypes.length==1){
        if(apiListing.tenantTypes[0].id==2){
            listing.tenantTypes = maps.getTenantTypes(apiListing.tenantTypes[0].id) + " only";
        }
    }

    let count = 0;

    let taxonomyKeyPoints = [];
    let amenitiesMap = MasterdetailsService.getMasterDetails('amenities') || {};
    for(let i=0; i<keyPointsArray.length; i++){

        let tempObj = {
            key: keyPointsArray[i].key,
            value: listing[keyPointsArray[i].key],
            label: keyPointsArray[i].label
        };

        if (!listing.isRent && (tempObj.key === 'propertyAge' && !listing.isReadyToMove)) {
            continue;
        } else if (!listing.isRent && tempObj.key === 'possession' && listing.isReadyToMove) {
            continue;
        }

        if (listing.isRent && tempObj.key === 'propertyAge') {
            continue;
        }else if(listing.isRent && tempObj.key === 'possession' && apiListing.possessionDate && moment(apiListing.possessionDate) <= moment()){
            tempObj.value = 'immediately';
        }

        if((tempObj.value || tempObj.value === 0 || tempObj.key === 'securityDeposit') && count < 3){
            count += 1;
            listing.keyPoints.push(tempObj);
        }

        if(pageType && pageType.indexOf('taxonomy') > -1 && keyPointsArray[i].filter && filters[keyPointsArray[i].filter]){
            let filterValue = filters[keyPointsArray[i].filter];
            if(tempObj.key === 'taxonomyAmenity'){
                let taxonomyAmenityObject = amenitiesMap[filterValue] || {};
                tempObj.value = taxonomyAmenityObject.name;
            }else if(tempObj.key === 'viewDirection'){
                tempObj.value = maps.getDirectionViewType(filterValue);
            }

            tempObj.value && taxonomyKeyPoints.push(tempObj);  //jshint ignore:line
        }
    }
    if (taxonomyKeyPoints.length > 0) {
        listing.keyPoints.splice(0 - taxonomyKeyPoints.length);
        listing.keyPoints = listing.keyPoints.concat(taxonomyKeyPoints);
    }

    if(listing.price && listing.localityAvgPrice){
        listing.priceDiff = listing.price - listing.localityAvgPrice;
    }

    listing.project = {};
    listing.projectId = project.projectId;
    listing.project.actual = false;
    let projectStatus = project.activeStatus ? project.activeStatus.toUpperCase() : null;
    listing.project.projectStatus = projectStatus;
    if (projectStatus != DUMMY_PROJECT_STATUS) {
        listing.project.actual = true;
        listing.project.name = project.name;
        listing.project.fullName = project.name;
        listing.project.overviewUrl = ACTIVE_PROJECT_STATUSES.indexOf(projectStatus) > -1 ? project.overviewUrl : null;
        listing.project.overviewUrl = utils.prefixToUrl(listing.project.overviewUrl);
    }

    let builderStatus = builder && builder.activeStatus ? builder.activeStatus.toUpperCase() : null;
    if (builder && builderStatus && builderStatus != DUMMY_PROJECT_STATUS) {
        if (ACTIVE_PROJECT_STATUSES.indexOf(builderStatus) > -1) {
            listing.project.builderId = builder.id;
            listing.project.builderName = builder.name;
            listing.project.builderDisplayName = builder.displayName ? builder.displayName : '';
            listing.project.builderUrl = utils.prefixToUrl(builder.buyUrl);
            listing.project.fullName = listing.project.builderDisplayName + ' ' + project.name;
        }
    }

    listing.localityName = locality.label;
    listing.suburbName = suburb.label;
    listing.cityName = city.label;
    listing.cityId = city.id;
    listing.projectName = project.name;
    

    /* DEAL_MAKER/Expert DEAL_MAKER logic start*/
    listing.sellerTransactionStatus = {
        isExpertDealMaker: false,
        isDealMaker: false,
        typeLabel: ''
    };
    let pageLevel = (urlDetail.pageLevel && urlDetail.pageLevel.toLowerCase()) || '';

    //Get seller statuses
    let sellerStatuses = maps.getSellerStatuses(apiListing);

    if((pageLevel.indexOf('city') > -1 || pageLevel.indexOf('suburb') > -1 || pageLevel.indexOf('locality') > -1) && 
        sellerStatuses.isCityExpert){
        listing.sellerTransactionStatus.isExpertDealMaker = true;
        listing.sellerTransactionStatus.typeLabel = maps.listingSellerTransactionMapping['CityExpertDealMaker'].label;
        listing.sellerTransactionStatus.type = 'CityExpertDealMaker';
    }else if((pageLevel.indexOf('locality') > -1 || pageLevel.indexOf('suburb') > -1)&& 
        sellerStatuses.isLocalityExpert){
        listing.sellerTransactionStatus.isExpertDealMaker = true;
        listing.sellerTransactionStatus.typeLabel = maps.listingSellerTransactionMapping['LocalityExpertDealMaker'].label;
        listing.sellerTransactionStatus.type = 'LocalityExpertDealMaker';
    }else if(sellerStatuses.isExpertDealMaker){
        listing.sellerTransactionStatus.isExpertDealMaker = true;
        listing.sellerTransactionStatus.typeLabel = maps.listingSellerTransactionMapping['ExpertDealMaker'].label;
        listing.sellerTransactionStatus.type = 'ExpertDealMaker';
    }else if(sellerStatuses.isDealMaker){
        listing.sellerTransactionStatus.isDealMaker = true;
        listing.sellerTransactionStatus.typeLabel = maps.listingSellerTransactionMapping['DealMaker'].label;
        listing.sellerTransactionStatus.type = 'DealMaker';
    }

    listing.isPremiumListing = sellerStatuses.isPremiumProperty;
    listing.showNestawayBenefit = sellerStatuses.showNestawayBenefit;
    listing.isMakaanSelectSeller = sellerStatuses.isMakaanSelectSeller && makaanSelectCities.indexOf(listing.cityId) > -1;
    listing.isAccountLocked = sellerStatuses.isAccountLocked;



    /* DEAL_MAKER/Expert DEAL_MAKER logic end*/

    listing.descriptionTitle = listings.generateDescriptionTitle(urlDetail, listing);

    let postedBy = {};
    postedBy.type = sellerCompany.type ? sellerCompany.type.toUpperCase() : '';
    if ((postedBy.type == POSTED_BY_BROKER || postedBy.type == POSTED_BY_OWNER || postedBy.type == POSTED_BY_BUILDER) && seller) {
        postedBy.name = sellerCompany.name;
        postedBy.paidLeadCount = sellerCompany.paidLeadCount;
        postedBy.id = sellerCompany.id;
        postedBy.image = imageParser.appendImageSize(sellerCompany.logo || sellerUser.profilePictureURL, 'thumbnail');
        //postedBy.rating = Math.round(sellerCompany.score * 10) / (10 * 2); // divided by 2 to show rating out of 5
        postedBy.rating = sellerCompany.score; // divided by 2 to show rating out of 5
        postedBy.ratingBadge = utils.roundNumber(postedBy.rating,1);
        postedBy.ratingBadgeClass = utils.getRatingBadgeClass(postedBy.rating);
        postedBy.assist = sellerCompany.assist;
        let avatarDetails = utils.getAvatar(postedBy.name);
        postedBy.backgroundColor = avatarDetails.backgroundColor;
        postedBy.nameText = avatarDetails.text;
        postedBy.textColor = avatarDetails.textColor;
        postedBy.clickable = true;
        postedBy.userId = sellerUser.id;
        if (sellerUser.contactNumbers && sellerUser.contactNumbers.length > 0) {
            postedBy.phone = sellerUser.contactNumbers[0].contactNumber;
        }
        postedBy.url = utils.prefixToUrl(sellerCompany.companyUrl);
    }

    if ((postedBy.type == POSTED_BY_BUILDER && !listing.project.builderId) || postedBy.type == POSTED_BY_OWNER) {
        postedBy.clickable = false;
    }

    if (postedBy.type === POSTED_BY_BROKER) {
        postedBy.type = 'AGENT';
    }

    listing.postedBy = postedBy;

    listing.sellerCallRatingCount = apiListing.sellerCallRatingCount || 0;
    listing.sellerCompanyFeedbackCount = apiListing.sellerCompanyFeedbackCount || 0;
    if (listing.project.actual) {
        listing.project.overviewUrl = listing.addedByPromoter ? utils.changeQueryParam(listing.project.overviewUrl, globalConfig.semQuery_camelCase, listing.postedBy.id) : listing.project.overviewUrl;
    }

    listing.hasOffer = apiListing.isOffered ? apiListing.isOffered : false;
    listing.currentServerTime = (new Date()).valueOf();

    listing.mainImageURL = imageParser.appendImageSize(mainImage.absolutePath || apiListing.mainImageURL, 'small');
    listing.mainImageAlt = apiListing.propertyTitle || mainImage.altText;
    listing.mainImageTitle = apiListing.propertyTitle || mainImage.title;
    listing.defaultImageId = apiListing.defaultImageId || null;

    listing.imagesCount = apiListing.imageCount || 0;
    listing.imagesCount = listing.mainImageURL ? listing.imagesCount : 0;
    listing.images = [];

    if (apiListing.geoDistance) {
        listing.landmarkDistance = Math.round(apiListing.geoDistance * 10) / 10;
        listing.landmarkDistance = listing.landmarkDistance && listing.landmarkDistance + " km" || (Math.round(apiListing.geoDistance * 10000) / 10) + " m";
    }

    if (apiListing.videos && apiListing.videos.length) {
        listing.videos = apiListing.videos[0] || {};
        listing.mainImageURL = listing.videos.imageUrl;
        listing.mainImageAlt = listing.videos.altText;
        listing.mainImageTitle = listing.videos.title;
        listing.imagesCount++;
        listing.hasVideo = true;
    }

    listing.updatedAt = apiListing.updatedAt;
    listing.displayDate = apiListing.displayDate;
    
    if(apiListing.companyStateReraRegistrationId){
        listing.companyStateReraRegistrationId = apiListing.companyStateReraRegistrationId;
    }

    listing.mainImageWidth = apiListing.mainImageWidth;
    listing.mainImageHeight = apiListing.mainImageHeight;
    if(listing.isRent && recencyAllowedCities.indexOf(listing.cityId) !== -1) {
        listing.recency = utils.getRecency(apiListing.displayDate, recencyConfig);
    }
    if(sharedConfig.recencySortedCities.indexOf(listing.cityId)>-1){
        listing.recencyDisplayDate = utils.formatDate(apiListing.displayDate,"SS dd,YY");
    }
    listing.configData = _getListingConfigData(listing, index);
    let sellerId = listing && listing.postedBy && listing.postedBy.userId;
    listing.sellerBenefitTags = benefitTagConfig.getSellerBenefitTags(sellerId, listing.listingCategory);
    return listing;
}

listings.parseListingsApiResponse = function(res, extraParams) {

    extraParams = extraParams || {};

    if (!res) {
        logger.error("Empty Listing API Response:  " + JSON.stringify(res));
        return null;
    }

    let buyCardKeyPoints = [].concat(buyKeyPoints);
    if (extraParams.query && extraParams.query.newOrResale) {
        buyCardKeyPoints = buyCardKeyPoints.sort(function(a, b) {
            return (a.listingCategoryPriority - b.listingCategoryPriority);
        });
    }

    let lastUpdated = 0;
    //TODO 
    let numberFormat = extraParams.locals.numberFormat;

    let urlDetail = extraParams.urlDetail || {};
    let pageType = urlDetail.pageType && urlDetail.pageType.toLowerCase();

    let listingData = {
        totalCount: res.data[0].totalCount,
        readableTotalCount: utils.formatNumber(res.data[0].totalCount, { type : 'number', seperator : numberFormat }),
        data: [],
        listings: []
    };
    if (res.totalCount == 1) {
        if (res.data[0].isOriginalSearchResult) {
            listingData.totalCount = res.data[0].totalCount;
            listingData.nearbyCount = 0;
        }
        if (!res.data[0].isOriginalSearchResult) {
            listingData.totalCount = 0;
            listingData.nearbyCount = res.data[0].totalCount;
        }
    }
    if (res.totalCount > 1) {
        listingData.totalCount = res.data[0].totalCount;
        listingData.nearbyCount = res.data[1].totalCount;
    }
    if (listingData.totalCount) {
        listingData.isZeroListingPage = false;
    } else {
        listingData.isZeroListingPage = true;
    }
    for (let j in res.data) {

        let items = ((res.data[j] && res.data[j].facetedResponse) ? res.data[j].facetedResponse.items : res.data[j].items) || [];
        // if (!items.length) {
        //     logger.info("No Data found in Listing API. API Response: " + JSON.stringify(res));
        //     return listingData;
        // }

        for (let i in items) {
            let listing = {},
                resData = items[i],
                apiListing = resData.listing;

            if (!apiListing ||
                !apiListing.currentListingPrice ||
                !apiListing.property ||
                !apiListing.property.project ||
                !apiListing.property.project.locality ||
                !apiListing.property.project.locality.suburb ||
                !apiListing.property.project.locality.suburb.city) {

                logger.error("Listing Data missing in Listing API. Listing Details: " + JSON.stringify(apiListing));

                continue;
            }
            let index =  listingData.data.length + 1;
            
            extraParams.isOriginalSearchResult = res && res.data[j].isOriginalSearchResult;

            listing = _parseIndividualListing(apiListing,extraParams,index);
            

            if (listing.updatedAt > lastUpdated) {
                lastUpdated = listing.updatedAt;
            }

            listingData.cityName = listing.cityName;

            // to store first latitude && longitude of the listing
            if ((listing.latitude && listing.longitude) && !(listingData.latitude && listingData.longitude)) {
                listingData.latitude = listing.latitude;
                listingData.longitude = listing.longitude;
            }

            listingData.data.push(listing);
        }
        if (lastUpdated) {
            listingData.lastUpdated = utils.formatDate(lastUpdated, 'dd-mm-YY');
        }

    }

    var originalList = listingData.data.filter(function(item){
        return item.isOriginalSearchResult;
    });
    var similarList = listingData.data.filter(function(item) {
        return !item.isOriginalSearchResult;
    });
    listingData.listings.push(originalList);
    if (originalList.length < 10) {
        listingData.listings.push(similarList);
    }
    listingData.data = originalList;
    return listingData;
};

function _handleIncludeNearbyCase(includeNearbyLocalities, obj) {
    obj.includeNearbyResults = includeNearbyLocalities || false;
    return obj;
}

listings.shuffleListingData = function(listingData){
    let data = listingData.listings && listingData.listings.length ? listingData.listings[0]:[];
    let allListings={shufflingReqd:[],noShufflingReqd:[]},now = new Date().getTime();

    if(data && data.length){
        data.forEach(listing=>{
            if((now - listing.displayDate)<= SEVENTY_TWO_HRS_MILLISEC){
                allListings.shufflingReqd.push(listing);
            }else{
                allListings.noShufflingReqd.push(listing);
            }
        });

        allListings.shufflingReqd = utils.shuffleArray(allListings.shufflingReqd);
        listingData.listings[0] = allListings.shufflingReqd.concat(allListings.noShufflingReqd);
    }
    return listingData;
}

listings.getPrivilegeSellerListings = function(selector={}, {req}){
    if (selector.filters) {
        if (!selector.filters.and) {
            selector.filters.and = [];
        }
        selector.filters.and.push({
            equal: {
                listingSellerPrivileges: ['MakaanPrivilegeSeller']
            }
        });
        selector.groupBy = {"field": "sellerId","max": "listingRelevanceScore","min": null};
        for (let i = 0; i < selector.filters.and.length; i++) {
            if (selector.filters.and[i].equal && selector.filters.and[i].equal.listingSellerCompanyType) {
                selector.filters.and.splice(i, 1);
            }
        }
        selector = JSON.stringify(selector);
    } else {
        selector.filters = [{
            key: 'listingSellerPrivileges',
            value: ['MakaanPrivilegeSeller'],
            type: 'equal'
        },{
            key: 'listingSellerCompanyType',
            value: ['Broker'],
            type: 'equal'
        }];
        selector.groupBy = {
            field: "sellerId",
            max: "listingRelevanceScore",
            min: null
        };
        selector = apiService.createSelector(selector);
    }
    var api = apiConfig.similarLocalityListings({selector});
    return apiService.get(api, { req }).then(function(response) {
        return _parsePrivilegeListings(response, req);
    }, err => {
        throw err;
    });
}

function _getListingTypeTitle(listing) {
    let title = '';
    if (!listing.isPlot) {
        title = listing.bedrooms;
        if (listing.rk){
            title = `${title} RK`;
        } else {
            title = `${title} BHK`;
        }
    }
    if (listing.penthouse){
        title = 'Penthouse';
    } else if (listing.studio) {
        title = 'Studio apartment';
    } else {
        title = `${title} ${maps.getPropertyType(listing.unitTypeId)}`;
    }
    return title;
}

function _parsePrivilegeListings(response, req) {
    let result = {}, items = [];
    if(response && response.totalCount > 0){
        response.data.forEach(function (data) {
            let listing = data.property,
                companySeller = data.companySeller,
                project = listing && listing.project,
                locality = project && project.locality,
                suburb = locality && locality.suburb,
                city = suburb && suburb.city,
                item = {
                    propertyId: listing.propertyId,
                    mainImageURL: data.mainImageURL,
                    price: utils.formatNumber(data.currentListingPrice.price, {
                        returnSeperate: true,
                        seperator: req.locals.numberFormat
                    }),
                    listingTypeTitle: _getListingTypeTitle(listing),
                    size: listing.size,
                    measure: listing.measure,
                    localityId: locality.localityId,
                    locality: (locality.label && locality.label.length) > MAX_LOCALITY_CHAR_LENGTH ? `${locality.label.substring(0, MAX_LOCALITY_CHAR_LENGTH)}...` : locality.label,
                    localityLabel: locality.label,
                    cityId: city.id,
                    cityName: city.label,
                    sellerId: companySeller && companySeller.user && companySeller.user.id,
                    sellerProfile: companySeller && companySeller.user && companySeller.user.profilePictureURL,
                    sellerName: companySeller && companySeller.company && companySeller.company.name,
                    sellerType: companySeller && companySeller.company && companySeller.company.type,
                    resaleURL: data.resaleURL
                };
            items.push(item);
        });
        result = {
            totalCount: response.totalCount,
            items
        };
    }
    return result;
}

function _getAppliedQuicktFilters(filterObj){
    let allowedAppliedFilters = [];
    for (var filterKey in filterObj){
        if(QUICK_FILTERS.allowedFilters.indexOf(filterKey) > -1 && allowedAppliedFilters.indexOf(filterKey)==-1){
            allowedAppliedFilters.push(filterKey);
        }
    }
    return allowedAppliedFilters;
}

function _getMissingQuickFilters(queryParams, urlDetail){
    let listingType = urlDetail && urlDetail.listingType && urlDetail.listingType.toLowerCase();
    let listingTypeQuickFilters = QUICK_FILTERS.priorityMapping[listingType]||{};
    let promoinentAppliedFilters=[],queryParamFilters=[],urlFilters=[],filtersToShow=[];

    queryParamFilters = queryParams ? _getAppliedQuicktFilters(queryParams) : [];
    urlFilters = urlDetail && urlDetail.filters ? _getAppliedQuicktFilters(urlDetail.filters):[];
    promoinentAppliedFilters = queryParamFilters.concat(urlFilters);

    for (var priority in listingTypeQuickFilters){
        if(promoinentAppliedFilters.indexOf(listingTypeQuickFilters[priority])== -1){
            filtersToShow.push(QUICK_FILTERS.normalFilterMapping[listingTypeQuickFilters[priority]] || listingTypeQuickFilters[priority]);
      }
    }

    return filtersToShow;
}

listings.getApiListings = function(queryParams, urlDetail, {req, itemsPerPage}) {
    let QueryObj = selectorService.getSelectorQuery(queryParams, urlDetail, itemsPerPage),
        listingApi, listingApiQs, apiSelector,missingQuickFilters=[];

    missingQuickFilters = _getMissingQuickFilters(queryParams, urlDetail);

    apiSelector = JSON.parse(QueryObj.selector);

    // create gpid and gep as query string obj not to be encoded
    listingApiQs = {
        gpid: QueryObj.gpid,
        gep: QueryObj.gep
    };

    if (!QueryObj.gpid) {
        delete listingApiQs.gpid;
    }

    if (!QueryObj.gep) {
        delete listingApiQs.gep;
    }

    // remove it from QueryObj as send in QS obj
    delete QueryObj.gpid;
    delete QueryObj.gep;

    if(urlDetail.listingType === 'commercialBuy' || urlDetail.listingType === 'commercialLease' || queryParams.listingType ==='commercialBuy' || queryParams.listingType === 'commercialLease') {
        QueryObj.isCommercial = true;
    }

    /*if(urlDetail.sellerId){
        listingApi = apis.listings({
           query: QueryObj
       });
    }else { // if not seller serp call listing with sponsored
        QueryObj = _handleIncludeNearbyCase(includeNearbyLocalities, QueryObj);
        listingApi = apis.listingsWithSponsored({
           query: QueryObj
        });
    }*/

    QueryObj = _handleIncludeNearbyCase(req.includeNearbyLocalities, QueryObj);

    QueryObj.includeSponsoredResults = false; // don't delete this as by default apis has includeSponsoredResults TRUE

    // MAKAAN-3832: Changing the relevance logic to provide leads to paid starved seller 
    if (utils.isMobileRequest(req) || (queryParams.utm_source == 'google' && queryParams.utm_medium == 'cpc')) {
        QueryObj.sortByPaidStarved = true;
    }

    listingApi = apis.listingsWithExpansion({
        query: QueryObj
    });

    switch (urlDetail.pageType) {
        case 'SIMILAR_PROPERTY_URLS':
        case 'SIMILAR_PROPERTY_URLS_MAPS':
            delete QueryObj.includeSponsoredResults;
            delete QueryObj.includeNearbyResults;
            listingApi = apis.serpSimilarListings({
                listingId: urlDetail.listingId,
                query: QueryObj
            });
            break;
        default:
            break;
    }
    
    listingApi.qs = listingApiQs;
    if(queryParams.forceFieldFiltering) {
        listingApi.qs.forceFieldFiltering = true;
    }
    return apiService.get(listingApi, {
        req: req,
        isPrimary: true
    }).then((response) => {
        return {
            selector : apiSelector,
            response,
            missingQuickFilters
        };
    }).catch(error => {
        logger.error('listing api error in listingsService is: ', error);
        throw error;
    });

};

function _filterCampaignListings(response) {
    let listingArray = response.data && response.totalCount >= 1 && response.data[0].isOriginalSearchResult ? response.data[0].facetedResponse.items : [] || [],
        campaignListings = [];
    for (let i = 0, length = listingArray.length; i < length; i++) {
        let currListing = (listingArray[i] || {}).listing || {};
        if (currListing.throughCampaign) {
            campaignListings.push(listingArray[i]);
        }
    }
    if (campaignListings.length) {
        response.data.items = campaignListings;
    }
    var res = {};
    res.data = [];
    var listingData = {};
    listingData.totalCount = campaignListings.length;
    listingData.facetedResponse = {};
    listingData.isOriginalSearchResult = response.data[0].isOriginalSearchResult;
    listingData.facetedResponse.items = campaignListings;
    res.data.push(listingData);
    return res;
}

listings.getSellerListings = function({urlDetail, sellerId}, {req}) {
    let selector = selectorService.getSelectorQuery({sellerUserId:sellerId}, urlDetail),
        emiPrice;
    return apiService.get(apiConfig.listings({
        query: selector
    }), {
        req
    }).then((response) => {
        let parsedListings = listings.parseListingsApiResponse(_filterCampaignListings(response), {
                urlDetail, locals : req.locals
            }).data,
            groupedObj = {},
            priceObj = {},
            sizeObj = {},
            listingCategory = [],
            propertyIdArray = [],
            totalPrice = 0,
            totalArea = 0;

        for (let i = 0, length = parsedListings.length; i < length; i++) {
            let currListing = parsedListings[i],
                bedrooms = currListing.bedrooms,
                size = currListing.size;
            totalPrice += currListing.price;
            totalArea += currListing.size;
            if (priceObj.min > currListing.price || !priceObj.min) {
                priceObj.min = currListing.price;
            }
            if (priceObj.max < currListing.price || !priceObj.max) {
                priceObj.max = currListing.price;
            }
            if (sizeObj.min > currListing.size || !sizeObj.min) {
                sizeObj.min = currListing.size;
            }
            if (sizeObj.max < currListing.size || !sizeObj.max) {
                sizeObj.max = currListing.size;
            }
            if (listingCategory.indexOf(currListing.listingCategory) === -1) {
                listingCategory.push(currListing.listingCategory);
            }
            if (!groupedObj[bedrooms]) {
                groupedObj[bedrooms] = {};
            }
            if (!groupedObj[bedrooms][size]) {
                groupedObj[bedrooms][size] = currListing;
                propertyIdArray.push(currListing.propertyId);
            }
        }
        emiPrice = (priceObj.min) ? utils.calculateEMI(priceObj.min, globalConfig.emi_rate, globalConfig.emi_tenure, globalConfig.downpayment_percent) :  undefined;
        return {groupedObj, priceObj, emiPrice ,sizeObj, listingCategory: listingCategory.join(','), propertyIdArray, pricePerUnitArea: utils.formatNumber(totalPrice/totalArea, { type : 'number', seperator : req.locals.numberFormat})};
    }, (err) => {
        throw err;
    });
};

listings.getListingInPriceRange = function(cityId, min, max, gap, filters, { req }) {
    let selectorObj = {},
        selector;
    selectorObj.filters = [{
        "key": "cityId",
        "value": cityId
    }];
    selectorObj.paging = {
        "rows": 1
    };
    let facetRanges = [{
        "field": "price",
        "start": min,
        "end": max,
        "gap": gap
    }];
    _.forEach(filters, (val, key) => {
        selectorObj.filters.push({
            "key": key,
            "value": val
        });
    });
    facetRanges = JSON.stringify(facetRanges);
    selector = apiService.createSelector(selectorObj);
    let urlConfig = apiConfig.listings({
        version: 1,
        query: {
            selector,
            facetRanges
        }
    });
    return apiService.get(urlConfig, {
        req
    }).then((response) => {
        response = (response && response.data && response.data.facets && response.data.facets.price) || {};
        response = _.map(response, function(value) {
            let key = Object.keys(value)[0];
            return {
                startPoint: key,
                propertyCount: value[key],
                gap: gap
            };
        });
        return response;
    }, (err) => {
        throw err;
    });
};

listings.getListingTitle = function(listingData, price, params) {
    if (!listingData) {
        return "";
    }
    let title = [],
        displayTitle = [];
    if (listingData.bedrooms) {
        displayTitle.push(listingData.bedrooms + "bhk");
        title.push(listingData.bedrooms + "bhk");
    }
    if (listingData.bathrooms) {
        displayTitle.push("+" + listingData.bathrooms + "t");
        title.push(listingData.bedrooms + "bhk");
    }
    if ((listingData.unitTypeId == 3) && (listingData.size)) {
        displayTitle.push(`${listingData.size} ${listingData.measure}`);
    }
    if (listingData.unitTypeId) {
        let unitType = propertyMap.getPropertyType(listingData.unitTypeId);
        displayTitle.push(unitType + ',');
        title.push(unitType + ',');
    }
    if (price) {
        let priceObj = utils.formatNumber(price, {returnSeperate : true, seperator : params.numberFormat}),
            priceStr = priceObj.val + ' <span class="sent-case">' + priceObj.unit + '</span>';
        displayTitle.push(priceStr);
        title.push(priceObj.val + ' ' + priceObj.unit);
    }
    return {
        displayTitle: displayTitle.join(" ").toLowerCase(),
        title: title.join(" ")
    };
};

listings.parseListing = function(listing, params) {
    let property = listing.property,
        project = property && property.project,
        locality = project && project.locality,
        suburb = locality && locality.suburb,
        city = suburb && suburb.city,
        //builder = project && project.builder,
        currentListingPrice = listing.currentListingPrice,
        companySeller = listing.companySeller,
        company = companySeller && companySeller.company,
        user = companySeller && companySeller.user,
        parsedListing = {};

    parsedListing.listingId = listing.id;
    parsedListing.listingUrl = utils.prefixToUrl(listing.resaleURL);
    parsedListing.relevanceScore = listing.relevanceScore;
    parsedListing.mainImage = listing.mainImageURL;
    parsedListing.altText = listing.mainImage && listing.mainImage.altText;
    parsedListing.listingCategory = listing.listingCategory;

    parsedListing.size = property && utils.formatNumber(property.size, {type : 'number', seperator : params.numberFormat});
    parsedListing.sizeUnits = property && property.measure;
    parsedListing.beds = property && property.bedrooms;
    parsedListing.projectId = property && property.projectId;
    parsedListing.unitType = property && property.unitType;
    parsedListing.unitTypeId = property && property.unitTypeId;

    parsedListing.fullPrice = currentListingPrice && utils.formatNumber(currentListingPrice.price, {type : 'number', seperator : params.numberFormat});
    parsedListing.price = currentListingPrice && utils.formatNumber(currentListingPrice.price, {precision : 2, returnSeperate : true, seperator : params.numberFormat });

    parsedListing.projectName = project && project.name;

    parsedListing.cityId = city && city.id;
    parsedListing.localityId = locality && locality.localityId;

    parsedListing.companyId = company && company.id;
    parsedListing.companyName = company && company.name;
    parsedListing.companyImage = imageParser.appendImageSize((company && company.logo) || (user && user.profilePictureURL), 'thumbnail');
    parsedListing.companyPhone = user && user.contactNumbers && (user.contactNumbers.length > 0) && user.contactNumbers[0].contactNumber;
    parsedListing.companyUserId = user && user.id;
    parsedListing.companyRating = company && company.score;
    parsedListing.companyAssist = company && company.assist;
    parsedListing.companyType = company && company.type;
    return parsedListing;
};

listings.getListingsData = function( selectorObj, { req }) {

    let selector = apiService.createSelector(selectorObj);
    let listingApi = apis.listingsData({ selector });

    return apiService.get(listingApi, { req }).then((response) => {
        return response;
    }).catch(error => {
        logger.error('listing api error in listingsService is: ', error);
        throw error;
    });
};

listings.parseDeficitNearbyListings = function (data,extraParams){
    let allListings = data && data.listings ||[],nearbyListingList=[],nearbyListing;
    let nearbyListingsLength =  allListings.length <= 2 ? allListings.length : 2;
    allListings = utils.shuffleArray(allListings);
    allListings = allListings.splice(0,nearbyListingsLength);
    allListings && allListings.forEach(listing => {
        nearbyListing = {};
        let index =  nearbyListingList.length + 1;
        nearbyListing = _parseIndividualListing(listing,extraParams,index);
        nearbyListingList.push(nearbyListing);
    });
    return nearbyListingList;
}

function _addLDFQueryParams(queryParams,isBuy){
    queryParams.listingSellerTransactionStatuses = "paidSellers";
    queryParams.newOrResale = isBuy ? "primary" : "rent";
    queryParams.sellerLeadDeficitScore = "5";
    queryParams.groupByField = {"field":"sellerId","min":null,"max":"listingRelevanceScore"};
    return queryParams;
}
listings.getdeficitNearbyListings = function(queryParams, urlDetail, {req,itemsPerPage,isMobileRequest}) {
    if(isMobileRequest){
        let isBuy = urlDetail.listingType=='buy';
        queryParams = _addLDFQueryParams (queryParams,isBuy);
        let QueryObj = selectorService.getSelectorQuery(queryParams, urlDetail, itemsPerPage),
        deficitNearbyListingApi = apis.getdeficitNearbyListings({
            query: QueryObj
        });
        return apiService.get(deficitNearbyListingApi, {req: req,isPrimary: true}).then((response) => {
            return {
                response : response && response.data||{}
            };
        }).catch(error => {
            logger.error('listing api error in listingsService is: ', error);
            return {}; // if this API fails Serp page should not be blocked from rendering
        });
    }else{
        return new Promise((resolve, reject) => {
            resolve({});
        }); 
    }
}

listings.getPhraseDetectionListing = function(queryParams, urlDetail, {req}) {
    if(urlDetail.listingType !== "commercialBuy" && urlDetail.listingType !== "commercialLease"){
        let QueryObj = selectorService.getSelectorQuery(queryParams, urlDetail, 10),
            listingApi,
            apiSelector,
            listingData = {
                totalCount: 0,
                data: []
            };;

        apiSelector = JSON.parse(QueryObj.selector);

        listingApi = apis.listingPhraseDetection({
            query: QueryObj
        });

        return apiService.get(listingApi, { req }).then(result => {
            if(result && result.totalCount > 0){
                listingData.totalCount = result.totalCount;
                result.data && result.data.forEach(listing => {
                    listingData.data.push(_parseIndividualListing(listing, {locals: req.locals}));
                });
                listingData.schemaMap = globalConfig.schemaMap;
            }
            return listingData;
        }).catch(error => {
            logger.error("Error while getting listing for phrase detection", error);
            throw error;
        });
    }
    else{
        return new Promise((resolve, reject) => {
            resolve({});
        }); 
    }
};

module.exports = listings;
