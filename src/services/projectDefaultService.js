"use strict";

const logger = require('services/loggerService'),
    desktopTemplate = require('./templateLoader').loadTemplate(__dirname, '../modules/project/view.marko'),
    mobileTemplate = require('./templateLoader').loadTemplate(__dirname, '../modules/project/view-mobile.marko'),
    projectService = require('./projectService'),
    imageParser = require('./imageParser'),
    utils = require('./utilService'),
    agentService = require('./agentService'),
    globalConfig = require('configs/globalConfig'),
    // listings = require('./listingsService'),
    BlogService = require('services/blogService');

function _parseAgentData(result) {
    let _agentData = [],
        _otherCompany = [];
    if (result && result[0]) {
        _agentData.push(result[0]);
    }
    for (let i = 1; i < result.length; i++) {
        _otherCompany.push(result[i]);
    }
    if (_otherCompany.length) {
        _agentData.push(JSON.stringify(_otherCompany));
    }
    return _agentData;
}
function _changeProjectDataspecificToSeller(projectData,sellersSpecificProjectData){
    const projectDetailKeys=['priceRange','maxPriceNum','minPriceNum'];
    projectData.size = sellersSpecificProjectData.Area || projectData.size;
    if(projectData.projectDetail){
        projectDetailKeys.forEach(key=>{
            if(projectData.projectDetail[key]){
                projectData.projectDetail[key] = sellersSpecificProjectData[key]|| projectData.projectDetail[key];
            }
        });
    }
    projectData.projectDetail.keyDetails && projectData.projectDetail.keyDetails.forEach(keyInfo=>{
        keyInfo.val = sellersSpecificProjectData[keyInfo.key]|| keyInfo.val;
        if(keyInfo.key === 'Area' && sellersSpecificProjectData.hasOwnProperty('carpetArea')){
            keyInfo.key = 'Carpet Area';
        }
    });
    return projectData;
}


module.exports.routeHandler = function(req, res, next) {
    logger.info('projectDefaultService  called');
    let projectId = req.urlDetail.projectId,
        localityId = req.urlDetail.localityId,
        projectName = req.urlDetail.projectName,
        builderName = req.urlDetail.builderName,
        cityId = req.urlDetail.cityId,
        isMobileRequest = utils.isMobileRequest(req),
        query = req.query || {},
        // sellerId = query.sellerId,
        isSemPage = query[globalConfig.semQuery_smallCase] || query[globalConfig.semQuery_camelCase],
        template = isMobileRequest ? mobileTemplate : desktopTemplate;
    let sellerUserIds = req.query && req.query.sellerUserIds && JSON.parse(req.query.sellerUserIds)||[];

    let projectDetail = projectService.getProjectDetails(projectId, localityId, { req }),
        sellersSpecificProjectDetail = projectService.getSellerSpecificProjectDetail(projectId,sellerUserIds,{req}),
        configurationBuckets,
        topAgent,
        topAgentBucket,
        featuredTopSellers,
        similarProject;

    if (!isSemPage) {
        similarProject = projectService.getSimilarProjectAPIData(projectId, 10, { req });
    } else if (isSemPage) {
        topAgent = agentService.getSellerDetailsByCompanyId(isSemPage, { req });
    }

    function assignGetTopAgentPromise(sellerUserIds=[]){
        if(sellerUserIds.length){
            topAgent = agentService.getMultipleFeaturedSellers(sellerUserIds, { req });
        }else{
            topAgent = projectService.getPaidSellers({
                cityId,
                projectId,
                listingCategory: 'Primary,Resale',
                giveBoostToExpertDealMakers: false,
                relaxProjectFilter:true
            }, { req });
        }
    }

    function getLeadBoxData(pageAllData) {
        var leadBoxData = {};
        leadBoxData.projectTitle = pageAllData.projectDetail.title;
        leadBoxData.localityDetails = pageAllData.projectDetail.locality + ', ' + pageAllData.projectDetail.city;
        leadBoxData.price = '';
        var minPrice = '';
        if (pageAllData.projectDetail.priceRange && pageAllData.projectDetail.priceRange.minPrice && 
            pageAllData.projectDetail.priceRange.minPrice.val) {
            minPrice = pageAllData.projectDetail.priceRange.minPrice.val + ' ' + pageAllData.projectDetail.priceRange.minPrice.unit;
        }
        var maxPrice = '';
        if (pageAllData.projectDetail.priceRange && pageAllData.projectDetail.priceRange.maxPrice && 
            pageAllData.projectDetail.priceRange.maxPrice.val) {
            maxPrice = pageAllData.projectDetail.priceRange.maxPrice.val + ' ' + pageAllData.projectDetail.priceRange.maxPrice.unit;
        }
        if (minPrice && !maxPrice) {
            leadBoxData.price = minPrice;
        }
        if (!minPrice && maxPrice) {
            leadBoxData.price = maxPrice;
        }
        if (minPrice && maxPrice) {
            leadBoxData.price = minPrice + ' - ' + maxPrice;
        }
        leadBoxData.type = pageAllData.unitTypeString;
        leadBoxData.projectId = pageAllData.projectDetail.projectId;
        leadBoxData.builderName = pageAllData.projectDetail.builderName;
        leadBoxData.builderDisplayName = pageAllData.projectDetail.builderDisplayName || '';
        return leadBoxData;
    }

    function getCarouselData(pageAllData) {
        return {
            hasVideo: false,
            isPlot: pageAllData.raw.propertyType && pageAllData.raw.propertyType.toLowerCase() === 'residential plot',
            isApartment: pageAllData.raw.propertyType && pageAllData.raw.propertyType.toLowerCase() === 'apartment',
            listingId: pageAllData.projectDetail.projectId,
            imagesCount: pageAllData.galleryData && pageAllData.galleryData.data && pageAllData.galleryData.data.length,
            mainImageURL: pageAllData.mainImage.src,
            mainImageTitle: pageAllData.mainImage.title,
            mainImageAlt: pageAllData.mainImage.alt,
            mainImageWidth: pageAllData.mainImage.width,
            mainImageHeight: pageAllData.mainImage.height,
            defaultImageId: pageAllData.mainImage.id
        };
    }
    


    Promise.all([projectDetail, sellersSpecificProjectDetail]).then((results)=>{
        var parsedData = results[0];
        var sellersSpecificProjectData = results[1];

        var data = parsedData.projectDetail;
        if(sellerUserIds && sellerUserIds.length >0){
            data = _changeProjectDataspecificToSeller(data,sellersSpecificProjectData);
        }
        data.raw = parsedData.raw;
        data.overview = {size: `${data.projectDetail.pricePerSqft} sqft`}
        data.isSemPage = isSemPage;
        data.projectId = projectId;
        data.isMobileRequest = isMobileRequest;
        var projectStatus = parsedData.raw.status;
        var possessionTimestamp = parsedData.raw.possessionTimestamp;
        data.galleryContainerData = parsedData.galleryContainerData;
        data.isProjectFeatured = (!data.isSemPage) && data.projectDetail && (data.projectDetail.isProjectFeaturedBuy || data.projectDetail.isProjectFeaturedRent);
        req.pageData.isProjectFeatured = data.isProjectFeatured;
        data.sellerUserIds = sellerUserIds;
        if(!data.isSemPage){
            assignGetTopAgentPromise(data.sellerUserIds);
        }
         if (!utils.isEmptyObject(parsedData.imageDetails.data)) {
            data.galleryData = parsedData.imageDetails;
        } else {
            data.galleryData = {
                data: imageParser.parseImages(data.mainImage, isMobileRequest)
            };
        }
        data.mainImage = data.galleryData.data[0];
        data.constructionImages = parsedData.constructionImages;
        if (!utils.isEmptyObject(data.loans)) {
            data.homeLoanDetails = function(args, callback) {
                projectService.getHomeLoans(data.loans, callback, { req });
            };
        }
        data.floorPlanImages = parsedData.floorPlanImages;
        data.chartData = {
            id: 'chart-price-trend',
            category: 'category',
            yAxisLegendText: 'Price / sqft',
            chartType: 'spline',
            extraconfig: {}
        };
        data.homeLoanBlogs = function(args, callback) {
            BlogService.getHomeLoanBlogs(req, callback);
        };

        data.mobileConfigurationBucket = projectService.prepareMobileConfigurationBucket(parsedData.projectDetail.properties, data.galleryData, {req});

        data.configurationBuckets = function(args, callback) {
            let projectProperties = parsedData.projectDetail.properties;
            configurationBuckets = configurationBuckets || projectService.configurationBuckets({ projectId, isSemPage, projectName, builderName, projectProperties, projectStatus, possessionTimestamp, isMobileRequest ,sellerUserIds }, { req });
            configurationBuckets.then(response => {
                callback(null, response);
            }, error => {
                callback(error);
                logger.error("error in listing configuration bucket", error);
            });
        };

        data.agentService = function(args, callback) {
            topAgentBucket = topAgentBucket || projectService.parseTopAgentForLead(topAgent, { builderName, cityId, req });
            Promise.all([topAgentBucket, topAgent]).then((response) => {
                callback(null, {
                    featuredTopSellers,
                    topAgents: _parseAgentData(response[0])
                });
            }).catch(() => {
                callback(null, {
                    topAgents: []
                });
            });
        };

        data.formatPriceWithComma = utils.formatPriceWithComma;

        if (similarProject) {
            data.similarProject = function(args, callback) {
                similarProject.then(response => {
                    callback(null, response);
                }, error => {
                    callback(error);
                });
            };
        } else if (isSemPage) {
            data.semSerpFilterBuy = utils.generateSerpFilter({
                sellerId: isSemPage,
                projectId: projectId,
                listingType: 'buy',
                projectName: projectName,
                builderName: builderName
            });
            data.semSerpFilterRent = utils.generateSerpFilter({
                sellerId: isSemPage,
                projectId: projectId,
                listingType: 'rent',
                projectName: projectName,
                builderName: builderName
            });
            data.pageGlobal.buyUrl = data.pageGlobal.rentUrl = '/listings';
        }
        data.isMobileRequest = isMobileRequest;
        data.leadBoxData = getLeadBoxData(data);
        if(sellerUserIds && sellerUserIds.length){
            data.raw.listingCountBuy = sellersSpecificProjectData.listingCountBuy||0;
            data.raw.listingCountRent = 0;
        }

        data.carouselData = getCarouselData(data);   
        template.renderAsync(data, res, "", req);
    }, err => {
        next(err);
    }).catch(err => {
        next(err);
    });
    
};
