"use strict";
const apiService = require('services/apiService'),
    apiConfig = require('configs/apiConfig'),
    utilService = require('services/utilService'),
    seoService = require('services/seoService'),
    globalConfig = require('configs/globalConfig'),
    mappingService = require('services/mappingService'),
    imageService = require('services/imageService'),
    imageParser = require('services/imageParser'),
    MasterDetailsService = require('services/masterDetailsService'),
    _ = utilService._,
    _getURL = require('public/scripts/common/sharedConfig').getURL,
    PRIMARY = 'Primary',
    RESALE = 'Resale',
    RENT = 'Rental',
    categoryConfig = {
        buyCategory: [PRIMARY, RESALE],
        rentCategory: [RENT],
        unitType: ['Apartment', 'Villa', 'BuilderFloor', 'Plot']
    },
    LABEL_MAP = {
        buy: 'Buy',
        rent: 'Rent',
        Apartment: 'Apartment',
        Villa: 'Villa',
        BuilderFloor: 'Builder Floor',
        Plot: 'Plot',
        '1_bhk':'1 bhk',
        '2_bhk':'2 bhk',
        '3_bhk':'3 bhk',
        '3+_bhk':'3+ bhk',

    };

var config = {
        "bedrooms": { //delete this
            "oneBhk": {
                "display": "1 BHK"
            },
            "twoBhk": {
                "display": "2 BHK"
            },
            "threeBhk": {
                "display": "3 BHK"
            },
            "fourBhk": {
                "display": "3+ BHK"
            }
        },
        "listingCategory": ['Primary', 'Resale', 'Rental'], //delete this (?)
        "listingType": ['buy', 'rent'],
        "unitType": ['Apartment', 'Villa', 'BuilderFloor'],
        "dataPoints": ['count', 'minSize', 'maxSize', 'minPrice', 'maxPrice', 'minPricePerUnitArea', 'maxPricePerUnitArea'], //delete this
        "priceReadableDataPoints": ["minPrice", "maxPrice"], //delete this
        "readableNumberDataPoints": ["minSize", "maxSize", "minPricePerUnitArea", "maxPricePerUnitArea"], //delete this
        "apiKeyMap": { //delete this
            "localityName": "label",
            "cityId": "cityId",
            "localityId": "localityId",
            "latitude": "latitude",
            "realImage": "localityHeroshotImageActualFlag",
            "longitude": "longitude",
            "livabilityScore": "livabilityScore",
            "fullDescription": "description",
            "annualGrowth": "avgPriceRisePercentage",
            "medianRentCity": "averageRentPerMonth",
            "smallDescription": "description",
            "lifestyleDescription": "entityDescriptions",
            "minAffordablePrice": "minAffordablePrice",
            "maxAffordablePrice": "maxAffordablePrice",
            "minLuxuryPrice": "minLuxuryPrice",
            "maxBudgetPrice": "maxBudgetPrice",
            "localityHeroshotImage": "localityHeroshotImageUrl",
            "buyUrl": "buyUrl"
        },
        filterKeyMap: { //delete this
            beds: ["bedrooms"],
            propertyType: ["unitType"],
            listingType: ["listingCategory"]
        },
        labels: { //delete this
            aboveAverage: "Above Average",
            average: "Average"
        },
        getNeighbourhoodTabs() { //delete this
            return [
            // {
            //     label: 'master plan',
            //     targetId: 'masterPlanContent',
            //     selected: true
            // }, 
            {
                label: 'Nearby',
                targetId: 'neighbourhoodContent'
            }, {
                label: 'Commute',
                targetId: 'commuteContent'
            }];
        },
        type: {
            'buy': ['primary', 'resale'],
            'rent': ['rental']
        }
    };

function _getRentSalePropertyCount(data) {
    let i,
        response;

    response = {
        sale: 0,
        rent: 0,
        avgPricePerUnitArea: 0,
        avgPrice: 0
    };
    for (i = 0; i < data.length; i++) {
        if (config.type.buy.indexOf(data[i].listingCategory.toLowerCase()) !== -1) {
            response.avgPricePerUnitArea += data[i].avgPricePerUnitArea * data[i].count;
            response.sale += data[i].count;
        } else if (config.type.rent.indexOf(data[i].listingCategory.toLowerCase()) !== -1) {
            response.avgPrice += data[i].avgPrice * data[i].count;
            response.rent += data[i].count;
        }
    }
    return response;
}

function getNearbyLocalities(lat, lon, localityId, fields = [], count = 3, distance = 10, { req, sort }) {  //jshint ignore:line
   if(!lat || !lon){
        let promise = new Promise(function(resolve,reject){
            let error = new Error('Bad Request');
            error.status = 400;
            reject(error);
        });
       return promise;
    }
    let selectorObj = {},
        selector, urlConfig;
    selectorObj.filters = [{
        key: "geo",
        value: {
            "distance": distance,
            "lat": lat,
            "lon": lon
        },
        type: 'geoDistance'
    }, {
        key: "localityId",
        value: localityId,
        type: 'notEqual'
    }, {
        key: 'localityDbStatus',
        value: ['Active','ActiveInMakaan'],
        type: 'equal'
    }];

    if (_.isEmpty(sort)) {
        selectorObj.sort = [{
            key: 'localityListingCount',
            order: 'DESC'
        }];
    } else {
        selectorObj.sort = sort;
    }

    if (_.isArray(fields) && fields.length > 0) {
        selectorObj.fields = fields;
    }
    selectorObj.paging = {
        "start": 0,
        "rows": count
    };
    selector = apiService.createSelector(selectorObj);
    urlConfig = apiConfig.localities(selector);
    return apiService.get(urlConfig, {req}).then((response) => {
        if (fields === 'all' || (_.isArray(fields) && fields.length > 0)) {
            return response;
        } else {
            return _parseSimilarLocalities(response, req);
        }
    }, err => {
        throw err;
    });
}

function getTopRankedLocalities(cityId,listingCategory="buy",selector={}, {req}){   //jshint ignore:line
    // let fields = ["localityId","label","relevanceScore","avgPriceRisePercentage","cityId"];
    // selector = _.assign(selector, {fields});

    if (_.isArray(selector.filters) && selector.filters.length) {
        if (!selector.filters.and) {
            selector.filters.and = [];
        }
        selector.filters.and.push({
            equal: {
                localityDbStatus: ['Active', 'ActiveInMakaan']
            }
        });
        selector = JSON.stringify(selector);
    } else {
        selector.filters = [{
            key: 'localityDbStatus',
            value: ['Active', 'ActiveInMakaan'],
            type: 'equal'
        }];
        selector = apiService.createSelector(selector);
    }


    let urlConfig = apiConfig.topLocalitiesWithFilters({cityId,listingCategory,selector:selector});

    return apiService.get(urlConfig, {req}).then((response)=>{
        return _parseSimilarLocalities(response, req);
    },(error)=>{
        throw error;
        // return [];
    });
}


function getAllLocalities(config = {}, {req}) {  //jshint ignore:line
    let selectorObj = {},
        fields = config.fields,
        selector, urlConfig;
        selectorObj.filters = [];
    if(config.cityId){
        selectorObj.filters.push({
            key: "cityId",
            value: config.cityId,
        });
    }
    if(config.localityId){
        selectorObj.filters.push({
            key: "localityId",
            value: config.localityId,
        });
    }
    if(config.suburbId){
        selectorObj.filters.push({
            key: "suburbId",
            value: config.suburbId,
        });
    }
    if(config.countryId){
         selectorObj.filters.push({
            key: "countryId",
            value: config.countryId
        });
    }
    selectorObj.filters.push({
        key: "localityDbStatus",
        value: ["Active","ActiveInMakaan"],
    });
    if (_.isArray(fields) && fields.length > 0) {
        selectorObj.fields = fields;
    }
    selectorObj.paging = {
        "start": config.start,
        "rows": config.count
    };
    selectorObj.sort = [{
        key: 'localityListingCount',
        order: 'DESC'
    }];
    selector = apiService.createSelector(selectorObj);
    urlConfig = apiConfig.localities(selector);
    return apiService.get(urlConfig,{req}).then((response) => {
            return response;
    }, err => {
        throw err;
    });
}

function getLocalityOverview(localityId, req, userSelector) {
    let defaultSelector = {
            fields: ["listingAggregations", "listingCategory", "avgPrice", "avgPricePerUnitArea", "count", "localityHeroshotImageUrl", "localityHeroshotImageActualFlag", "bedrooms", "livabilityScore", "suburb", "city", "latitude", "longitude", "minAffordablePrice", "maxAffordablePrice", "minLuxuryPrice", "maxBudgetPrice", "overviewUrl", "buyUrl", "rentUrl", "description", "avgPriceRisePercentage", "projectCount"]
        },
        selector = apiService.createSelector(userSelector || defaultSelector),
        urlConfig = apiConfig.localityDetailsById({
            localityId, selector
        });
    return apiService.get(urlConfig, {req}).then(function(response) {
        return response.data;
    }, err => {
        throw err;
    });
}

function getTopRankedLocalityInCity({cityId, localityId}, {options={}}={}, {req}){
    if (!cityId) {
        return new Promise(function(resolve, reject) {
            reject("Error");
        });
    }

    let temp = {
        paging: {
            start: options.start || 0,
            rows: options.rows || 10
        },
        fields: options.fields || ["localityId", "label", "listingAggregations", "minPricePerUnitArea", "maxPricePerUnitArea", "count", "listingCountBuy", "listingCountRent", "buyUrl", "rentUrl", "overviewUrl", "avgPriceRisePercentage"],
        filters: [{
            "key": 'localityId',
            "value": localityId,
            type: 'notEqual'
        }, {
            key: 'localityDbStatus',
            value: ['Active','ActiveInMakaan'],
            type: 'equal'
        }]
    };

    return apiService.get(apiConfig.topLocalitiesWithFilters({cityId, listingCategory: options.category, selector: apiService.createSelector(temp)})).then(response=>{
        return _parseTopLocality(response.data, {req});
    }, err => {
        throw err;
    });
}

function getLifestyleData (localityId, {req}) {
    if (!localityId) {
        return new Promise(function(resolve, reject) {
            reject("Error: localityId not present");
        });
    }
    let descriptionSelector = {filters:[]};
    descriptionSelector.filters.push({
        'filter': 'and',
        'type': 'equal',
        'key': 'tableId',
        'value': localityId
    }, {
        'filter': 'and',
        'type': 'equal',
        'key': 'entityDescriptionCategories.masterDescriptionCategory.masterDescriptionParentCategories.parentCategory',
        'value': 'LifeAtLocality'
    });
    let descriptionDetails = apiService.get(apiConfig.localityDescription({ query: apiService.createFiqlSelector(descriptionSelector) })),
        localityImages = imageService.getImageDetails('locality', { localityId: localityId }, {req});
    return Promise.all([descriptionDetails, localityImages]).then((response) => {
            return _parseLifestyleData(response, localityId);
        }, err => {
            throw err;
        });
}

function getLocalityDescription (localityId) {
    if (!localityId) {
        return new Promise(function(resolve, reject) {
            reject("Error: localityId not present");
        });
    }
    let selectorObj = {filters:[]};
    selectorObj.filters.push({
        'filter': 'and',
        'type': 'equal',
        'key': 'tableId',
        'value': localityId
    }, {
        'filter': 'and',
        'type': 'equal',
        'key': 'entityDescriptionCategories.masterDescriptionCategory.masterDescriptionParentCategories.parentCategory',
        'value': 'LifeAtLocality'
    });
    let urlConfig = apiConfig.localityDescription({
        query: apiService.createFiqlSelector(selectorObj)
    });
    return apiService.get(urlConfig).then((response) => {
        return response;
    }, err => {
        throw err;
    });
}

function parseLocalityData (data, pageData, {req}) {
    let result = {cityDetails:{}};
    result.latitude = data.latitude;
    result.longitude = data.longitude;
    result.localityHeroshotImage = imageParser.appendImageSize(data.localityHeroshotImageUrl,'large');
    result.localityHeroshotImageActualFlag = data.localityHeroshotImageActualFlag;
    result.livabilityScore = _scoreLogic(data.livabilityScore);
    result.avgPriceRisePercentage = data.avgPriceRisePercentage;
    result.projectCount = data.projectCount;
    //result.overviewDescription = { data.description};
    result.buyUrl = utilService.prefixToUrl(data.buyUrl);
    result.rentUrl = utilService.prefixToUrl(data.rentUrl);
    result.overviewUrl = utilService.prefixToUrl(data.overviewUrl);
    result.taxonomyData = {
        minAffordablePrice: data.minAffordablePrice,
        maxAffordablePrice: data.maxAffordablePrice,
        minLuxuryPrice: data.minLuxuryPrice,
        maxBudgetPrice: data.maxBudgetPrice
    };

    result.listingCountBuy =  utilService.formatNumber(data.listingCountBuy, { type : 'number', seperator : req.locals.numberFormat});
    result.listingCountRent = utilService.formatNumber(data.listingCountRent, { type : 'number', seperator : req.locals.numberFormat});
    result.neighbourhoodSec = {
        tabs: [{
            label: "master plan",
            targetId: "masterPlanContent",
            selected: true
        }, {
            label: "Nearby Localities",
            targetId: "nearbyLocalityMap"
        }, {
            label: "Commute",
            targetId: "commuteContent"
        }, {
            label: "Amenities",
            targetId: "neighbourhoodContent"
        }],
        isMasterPlanSupported: utilService.isMasterPlanSupported({
            id: pageData.cityId
        })
    };
    if(!result.neighbourhoodSec.isMasterPlanSupported){
        result.neighbourhoodSec.tabs.splice(0, 1);
    }
    let strippedDescription = utilService.stripHtmlContent(data.description, globalConfig.wordLimit.longLength, true, true);
    result.overviewDescription = {
        fullDescription: data.description,
        smallDescription: strippedDescription.description,
        hideViewMore: strippedDescription.error,
        textShortLength: globalConfig.wordLimit.longLength
    };
    
    _.assignIn(result, parseLocalityListingAgg(data.listingAggregations, pageData, {req}));
    _.assignIn(result.cityDetails, averagePricesFromAggrigation(data.suburb.city.listingAggregations, {req}));
    return result;
}

function averagePricesFromAggrigation(aggListing=[], {req}) {
    let avgPriceUnitArea_Buy = 0,
        avgPrice_Rent = 0,
        listingCount_Buy = 0,
        listingCount_Rent = 0,
        minPrice_Rent = 0,
        maxPrice_Rent = 0,
        minPriceUnitArea_Buy = 0,
        maxPriceUnitArea_Buy = 0,
        numberFormat = req.locals.numberFormat;
    for (let i = 0, length = aggListing.length; i < length; i++) {
        let currAgg = aggListing[i],
            count = currAgg.count,
            avgPrice = currAgg.avgPrice,
            avgPricePerUnitArea = currAgg.avgPricePerUnitArea,
            minPricePerUnitArea = currAgg.minPricePerUnitArea,
            maxPricePerUnitArea = currAgg.maxPricePerUnitArea,
            minPrice = currAgg.minPrice,
            maxPrice = currAgg.maxPrice;
        if (categoryConfig.buyCategory.indexOf(currAgg.listingCategory) > -1 && count && avgPricePerUnitArea) {
            listingCount_Buy += count;
            avgPriceUnitArea_Buy += avgPricePerUnitArea * count;
            minPriceUnitArea_Buy += minPricePerUnitArea * count;
            maxPriceUnitArea_Buy += maxPricePerUnitArea * count;
        } else if (categoryConfig.rentCategory.indexOf(currAgg.listingCategory) > -1 && count && avgPrice) {
            listingCount_Rent += count;
            avgPrice_Rent += avgPrice * count;
            minPrice_Rent += minPrice * count;
            maxPrice_Rent += maxPrice * count;
        }
    }
    avgPriceUnitArea_Buy = Math.round(avgPriceUnitArea_Buy/listingCount_Buy);
    minPriceUnitArea_Buy = Math.round(minPriceUnitArea_Buy / listingCount_Buy);
    maxPriceUnitArea_Buy = Math.round(maxPriceUnitArea_Buy / listingCount_Buy);
    avgPrice_Rent = Math.round(avgPrice_Rent/listingCount_Rent);
    minPrice_Rent = Math.round(minPrice_Rent/listingCount_Rent);
    maxPrice_Rent = Math.round(maxPrice_Rent/listingCount_Rent);
    return {
        avgPrice_Rent: utilService.formatNumber(avgPrice_Rent, { type : 'number', seperator : numberFormat }),
        maxPrice_Rent: utilService.formatNumber(maxPrice_Rent, { type : 'number', seperator : numberFormat }),
        minPrice_Rent: utilService.formatNumber(minPrice_Rent, { type : 'number', seperator : numberFormat }),
        avgPriceUnitArea_Buy: utilService.formatNumber(avgPriceUnitArea_Buy, { type : 'number', seperator : numberFormat }),
        minPriceUnitArea_Buy: utilService.formatNumber(minPriceUnitArea_Buy, { type : 'number', seperator : numberFormat }),
        maxPriceUnitArea_Buy: utilService.formatNumber(maxPriceUnitArea_Buy, { type : 'number', seperator : numberFormat }),
        listingCount_Buy: utilService.formatNumber(listingCount_Buy, { type : 'number', seperator : numberFormat }),
        listingCount_Rent: utilService.formatNumber(listingCount_Rent, { type : 'number', seperator : numberFormat })
    };
}

function _parseSimilarLocalities(response, req) {
    if (!(response && response.data)) {
        return [];
    }
    response = response.data;
    let result = [],
        i, length = response.length,
        temp,
       // listingAggregations,
        propertiesCount;
    for (i = 0; i < length; i++) {
        temp = {};
       // let avgPricePerUnitArea = 0,
          //avgPrice = 0,
            //listingCount = 0,
            //listingCountRent = 0;
        let listingAggregations = response[i].listingAggregations || {};

        propertiesCount = _getRentSalePropertyCount(listingAggregations);
        temp.medianPrice = propertiesCount.sale && Math.floor(propertiesCount.avgPricePerUnitArea / propertiesCount.sale);
        temp.medianPrice = utilService.formatNumber(temp.medianPrice ? temp.medianPrice : '', { precision : 2, seperator : req.locals.numberFormat });
        temp.medianRent = propertiesCount.rent && Math.floor(propertiesCount.avgPrice / propertiesCount.rent);
        temp.medianRent = utilService.formatNumber(temp.medianRent ? temp.medianRent : '', { precision : 2, seperator : req.locals.numberFormat });
        temp.name = response[i].label;
        temp.localityId = response[i].localityId;
        temp.propertyForSale = propertiesCount.sale;
        temp.propertyForRent = propertiesCount.rent;
        temp.rating = response[i].livabilityScore > 10 ? 10 : response[i].livabilityScore;
        temp.image = imageParser.appendImageSize(response[i].localityHeroshotImageUrl,'tile');
        temp.livabilityScore = response[i].livabilityScore;
        temp.url = utilService.prefixToUrl(response[i].overviewUrl);
        temp.buyUrl = utilService.prefixToUrl(response[i].buyUrl);
        temp.rentUrl = utilService.prefixToUrl(response[i].rentUrl);
        temp.cityName = response[i].suburb && response[i].suburb.city && response[i].suburb.city.label;
        temp.projectCount = response[i].projectCount;

        result.push(temp);
    }
    return result;
}


function parseLifeStyleContent(data,images=[]) {
    var result = [],
        keys = Object.keys(data),
        length = keys.length,
        temp,
        parentCategory;
    for (let i = 0; i < length; i++) {
        let value = data[keys[i]];
        let masterCategory = value.entityDescriptionCategories && value.entityDescriptionCategories.masterDescriptionCategory;
        if (masterCategory) {
            parentCategory = masterCategory.masterDescriptionParentCategories && masterCategory.masterDescriptionParentCategories.parentCategory;
            if (parentCategory.toLowerCase() === "lifestyle") {
                temp = {
                    category: masterCategory.name,
                    description: value.description
                };
                let imageType = masterCategory.name;
                for(let j = 0 ;j < images.length ;j++) {
                    if (images[j].imageType && images[j].imageType.displayName && images[j].imageType.displayName.toLowerCase() === imageType.toLowerCase()) {
                        temp.image = imageParser.appendImageSize(images[j].absolutePath,'medium');
                    }
                }
            }
            if (temp) {
                result.push(temp);
            }
        }
    }
    return result;
}

function _calculateAvg(data, key = 'avgPricePerUnitArea', type = 'buy') {
    let typeArr = config.type[type],
        totalCount = 0,
        weightedAvg = 0,
        sum = 0;
    _.forEach(data, (v) => {
        if (typeArr.indexOf(v.listingCategory.toLowerCase()) !== -1) {
            totalCount += v.count;
            if(v[key]) {
                sum += v.count * v[key];
            }
        }
    });
    weightedAvg = totalCount > 0 ? Math.floor(sum / totalCount) : 0;
    return weightedAvg;
}

function _parseTopLocalities(response, type, req) {
    if (!(response && response.data)) {
        return [];
    }
    response = response.data;
    let result = [],
        i, length = response.length,
        temp;
    for (i = 0; i < length; i++) {
        temp = {};
        temp.localityId = response[i].localityId;
        if (response[i].label) {
            temp.name = response[i].label;
        }
        if (response[i].livabilityScore) {
            temp.livabilityScore = response[i].livabilityScore > 10 ? 10 : response[i].livabilityScore;
        }
        if (response[i].listingAggregations) {
            temp.avgPricePerUnitArea = _calculateAvg(response[i].listingAggregations, 'avgPricePerUnitArea', 'buy');
            temp.avgPricePerUnitArea = utilService.roundNumber(temp.avgPricePerUnitArea, 2);
            temp.avgPricePerUnitArea = utilService.formatNumber(temp.avgPricePerUnitArea, { precision : 2, seperator : req.locals.numberFormat });
        }
        if (response[i].avgPriceRisePercentage) {
            temp.avgPriceRisePercentage = response[i].avgPriceRisePercentage;
        }
        temp.url = utilService.prefixToUrl(response[i].overviewUrl);
        result.push(temp);
    }
    return result;
}

function getTopLocalities(cityId, {options, req}) {
    if (!cityId) {
        return new Promise(function(resolve, reject) {
            reject("Error");
        });
    }
    let temp = {},
        queryFields = [],
        urlConfig,
        count,
        config = {
            rows: 4,
            fields: ["localityId", "label", "overviewUrl", "livabilityScore", "avgPricePerUnitArea", "avgPriceRisePercentage", "listingAggregations"]

        },
        type = (options && options.type) || 'buy';
    queryFields = config.fields;
    count = config.rows;
    if (_.isObject(options)) {
        queryFields = ((_.isArray(options.fields) || options.fields === 'all') && options.fields) || queryFields;
        count = options.count || count;
    }

    temp.filters = [{
        key: "cityId",
        value: cityId
    }, {
        key: 'localityDbStatus',
        value: ['Active','ActiveInMakaan'],
        type: 'equal'
    }];
    if (queryFields !== 'all') {
        temp.fields = queryFields;
    }
    temp.paging = {
        "start": 0,
        "rows": count
    };

    urlConfig = apiConfig.topLocalities(apiService.createSelector(temp));
    return apiService.get(urlConfig, {req}).then((response) => {
        if (queryFields === 'all') {
            return response;
        } else {
            return _parseTopLocalities(response, type, req);
        }
    }, err => {
        throw err;
    });
}

function _parsePopularLocalites(response) {
    if (!(response && response.data)) {
        return [];
    }
    response = response.data;
    let result = {},
        i, length = response.length,
        temp;
    for (i = 0; i < length; i++) {
        let key = response[i].suburbId;
        if (result[key]) {
            temp = result[key];
            result[key] = null;
        } else {
            temp = {};
            temp.suburb = (response[i].suburb.label);
            temp.suburbBuyLink=utilService.prefixToUrl(response[i].suburb.buyUrl);
            temp.suburbRentLink=utilService.prefixToUrl(response[i].suburb.rentUrl);
        }
        temp.topProjects = temp.topProjects ? temp.topProjects : [];
        let projects = {
            'name': response[i].label,
            'localityId': response[i].localityId,
            'url': utilService.prefixToUrl(response[i].buyUrl),
            'rentUrl': utilService.prefixToUrl(response[i].rentUrl),
            'listingCount': response[i].listingCount || response[i].listingCountBuy || response[i].listingCountRent
        };
        temp.topProjects.push(projects);
        result[key] = temp;
    }
    return result;
}

function getPopularLocalities(data, {fields, groupSort, req}) {
    if (!data || !data.cityId) {
        return new Promise(function(resolve, reject) {
            reject(new Error("Error in localityservice"));
        });
    }

    let temp = {},
        defaultFields = [],
        queryFields = [],
        config,
        urlConfig,
        cityId,
        count;
    defaultFields = ["buyUrl", "rentUrl", "overviewUrl", "listingCount", "suburbId", "url", "locality", "suburb", "label"];
    queryFields = defaultFields;
    if (fields && typeof fields === 'object') {
        queryFields = fields;
    }
    config = {
        rows: 1000,
        fields: queryFields,
        groupRows: 9
    };

    cityId = data.cityId || null;
    count = data.count || config.rows;

    temp.fields = config.fields;
    temp.paging = {
        "start": 0,
        "rows": count
    };
    temp.groupBy = {
        field: "suburbId"
    };
    temp.filters = [{
        key: "cityId",
        value: cityId
    }, {
        key: 'localityDbStatus',
        value: ['Active','ActiveInMakaan'],
        type: 'equal'
    }];
    temp.groupRows = config.groupRows;
    temp.groupSort = [{
        field: "localityListingCount",
        sortOrder: "DESC"
    }];
    if(groupSort && typeof groupSort =='object'){
        temp.groupSort = groupSort;
    }
    urlConfig = apiConfig.popularCityLocalities({
        query: {
            selector: apiService.createSelector(temp)
        }
    });
    return apiService.get(urlConfig, {req}).then((response) => {
        return _parsePopularLocalites(response);
    }, err => {
        throw err;
    });
}

function getTrendingSearch(data, {req}) {
    if (!data || !data.localityId) {
        return new Promise(function(resolve, reject) {
            reject(new Error("Error in localityService trending search"));
        });
    }
    let urlConfig,
        rows = data.rows || 3;

    urlConfig = apiConfig.trendingSearchLocality({
        entityId: data.localityId,
        rows: rows,
        category: data.category || "buy"
    });
    return apiService.get(urlConfig, {req}).then((response) => {
        if (response && response.data) {
            var result = [],
                data = response.data,
                length = data.length;
            for (let i = 0; i < length; i++) {
                let temp = {};
                temp.displayText = data[i].displayText;
                temp.redirectUrl = data[i].redirectUrl;
                result.push(temp);
            }
            return result;
        }
        throw new Error("Error in trending search response..");
    }, err => {
        throw err;
    });
}

function _prune(obj){
    _.forEach(obj,(v,k)=>{
        if(!v){
            delete obj[k];
        }
    });
}

function _getSerpRedirectUrl(data, localityData) {
    let filter = {};

    filter.cityId = localityData.cityId;
    filter.cityName = localityData.cityName && utilService.toUrlCase(localityData.cityName);
    filter.localityId = localityData.localityId;
    filter.localityName = localityData.localityName && utilService.toUrlCase(localityData.localityName);
    filter.suburbId = localityData.suburbId;
    filter.suburbName = localityData.suburbName && utilService.toUrlCase(localityData.suburbName);
    filter.listingType = data.category;
    filter.beds = (data.bedrooms == '3+' || data.bedrooms > 3) ? '3plus' : data.bedrooms;
    filter.propertyType = data.unitTypeId && mappingService.getPropertyType(data.unitTypeId, true);
    _prune(filter);

    return Object.keys(filter).length ? _getURL({ url: utilService.listingsPropertyPathName() }, filter).url : null;
}

function _addPropertyToUnit(category, currAgg, result, pageData, bhk_array, req) {
    let unitTypesNameMap = MasterDetailsService.getMasterDetails('unitTypesNameMap');
    let unitType = currAgg.unitType,
        unitTypeLowerCase = unitType && unitType.toLowerCase(),
        unitTypeId = unitTypesNameMap[unitTypeLowerCase] && unitTypesNameMap[unitTypeLowerCase].id,
        bedrooms = currAgg.bedrooms > 3 ? '3+' : currAgg.bedrooms,
        count = currAgg.count,
        numberFormat = req.locals.numberFormat;

        if(!result[category]){
            result[category] = {displayAttrs:{label:LABEL_MAP[category],count:0}};
        }
        if(!result[category][unitType]) {
            result[category][unitType] = {displayAttrs:{label:LABEL_MAP[unitType],count:0, series:[{colorByPoint: true, data:[]}]}};
        }
        if(!result[category][unitType][bedrooms]) {
            result[category][unitType][bedrooms] = {label:LABEL_MAP[`${bedrooms}_bhk`],count:0};
        }
        if(!result[category][bedrooms]) {
            result[category][bedrooms] = {label:LABEL_MAP[`${bedrooms}_bhk`],count:0};
        }
        let categoryObj = result[category],
            unitTypeObj = categoryObj[unitType],
            bedBucket = unitTypeObj[bedrooms],
            globalBedBucket = categoryObj[bedrooms];

        categoryObj.displayAttrs.count +=count;
        unitTypeObj.displayAttrs.count +=count;
        if(!unitTypeObj.displayAttrs.series[0].data.filter((obj)=>{
            if(obj.name == LABEL_MAP[`${bedrooms}_bhk`]) {
                return obj.y += count;
            }
        }).length) {
            unitTypeObj.displayAttrs.series[0].data.push({name:LABEL_MAP[`${bedrooms}_bhk`], y:count, sliced:false});
        }
        unitTypeObj.displayAttrs.minPricePerUnitArea = unitTypeObj.displayAttrs.minPricePerUnitArea < Math.round(currAgg.minPricePerUnitArea) ? unitTypeObj.displayAttrs.minPricePerUnitArea : Math.round(currAgg.minPricePerUnitArea);
        unitTypeObj.displayAttrs.maxPricePerUnitArea = unitTypeObj.displayAttrs.maxPricePerUnitArea > Math.round(currAgg.maxPricePerUnitArea) ? unitTypeObj.displayAttrs.maxPricePerUnitArea : Math.round(currAgg.maxPricePerUnitArea);
        unitTypeObj.displayAttrs.minPriceUnformatted = unitTypeObj.displayAttrs.minPrice = unitTypeObj.displayAttrs.minPrice < Math.round(currAgg.minPrice) ? unitTypeObj.displayAttrs.minPrice : Math.round(currAgg.minPrice);
        unitTypeObj.displayAttrs.maxPriceUnformatted = unitTypeObj.displayAttrs.maxPrice = unitTypeObj.displayAttrs.maxPrice > Math.round(currAgg.maxPrice) ? unitTypeObj.displayAttrs.maxPrice : Math.round(currAgg.maxPrice);
        unitTypeObj.displayAttrs.weigthedAvgPricePerUnitArea = unitTypeObj.displayAttrs.weigthedAvgPricePerUnitArea || 0;
        unitTypeObj.displayAttrs.weigthedAvgPricePerUnitArea += currAgg.maxPricePerUnitArea * count;
        unitTypeObj.displayAttrs.weigthedAvgPrice = unitTypeObj.displayAttrs.weigthedAvgPrice || 0;
        unitTypeObj.displayAttrs.weigthedAvgPrice += currAgg.avgPrice * count;
        unitTypeObj.displayAttrs.unitTypeId = unitTypeId;
        unitTypeObj.displayAttrs.redirectUri = _getSerpRedirectUrl({category, unitType, unitTypeId}, pageData);

        globalBedBucket.count = bedBucket.count += count;
        globalBedBucket.minPrice = bedBucket.minPrice = utilService.formatNumber(currAgg.minPrice, { seperator : numberFormat });
        globalBedBucket.maxPrice = bedBucket.maxPrice = utilService.formatNumber(currAgg.maxPrice, { seperator : numberFormat });
        globalBedBucket.minPriceUnformatted = bedBucket.minPriceUnformatted = currAgg.minPrice;
        globalBedBucket.maxPriceUnformatted = bedBucket.maxPriceUnformatted = currAgg.maxPrice;
        globalBedBucket.weigthedAvgPrice = bedBucket.weigthedAvgPrice = bedBucket.weigthedAvgPrice || 0;
        globalBedBucket.weigthedAvgPrice = bedBucket.weigthedAvgPrice += currAgg.avgPrice * count;
        globalBedBucket.minPricePerUnitArea = bedBucket.minPricePerUnitArea = utilService.formatNumber(Math.round(currAgg.minPricePerUnitArea), { type : 'number', seperator : numberFormat});
        globalBedBucket.maxPricePerUnitArea = bedBucket.maxPricePerUnitArea = utilService.formatNumber(Math.round(currAgg.maxPricePerUnitArea), { type : 'number', seperator : numberFormat});
        bedBucket.redirectUri = _getSerpRedirectUrl({category, bedrooms, unitType, unitTypeId}, pageData);
        globalBedBucket.redirectUri = _getSerpRedirectUrl({category, bedrooms}, pageData);
        if(bedrooms && bhk_array.indexOf(bedrooms) == -1) {
            bhk_array.push(bedrooms);
        }
}

function parseLocalityListingAgg(aggrigationArray = [], pageData, {req}) {  // jshint ignore:line
    let avgPriceUnitArea_Buy = 0,
        avgPrice_Rent = 0,
        avgPrice_Buy = 0,
        listingCount_Buy = 0,
        listingCount_Rent = 0,
        minPrice_Rent = 0,
        maxPrice_Rent = 0,
        minPrice_Buy = 0,
        maxPrice_Buy = 0,
        minPriceUnitArea_Buy = 0,
        maxPriceUnitArea_Buy = 0,
        rent_bhk = [],
        buy_bhk = [],
        dominantUnit = 'buy',
        result = {},
        numberFormat = req.locals.numberFormat;

    for (let i = 0, length = aggrigationArray.length; i < length; i++) {
        let currAgg = aggrigationArray[i],
            count = currAgg.count,
            avgPrice = currAgg.avgPrice,
            avgPricePerUnitArea = currAgg.avgPricePerUnitArea,
            minPricePerUnitArea = currAgg.minPricePerUnitArea,
            maxPricePerUnitArea = currAgg.maxPricePerUnitArea,
            minPrice = currAgg.minPrice,
            maxPrice = currAgg.maxPrice;
        if (categoryConfig.buyCategory.indexOf(currAgg.listingCategory) > -1 && count && avgPricePerUnitArea) {
            listingCount_Buy += count;
            avgPriceUnitArea_Buy += avgPricePerUnitArea * count;
            minPriceUnitArea_Buy += minPricePerUnitArea * count;
            maxPriceUnitArea_Buy += maxPricePerUnitArea * count;
            minPrice_Buy += minPrice * count;
            maxPrice_Buy += maxPrice * count;
            avgPrice_Buy += avgPrice * count;
            _addPropertyToUnit('buy',currAgg, result, pageData, buy_bhk, req);
        } else if (categoryConfig.rentCategory.indexOf(currAgg.listingCategory) > -1 && count && avgPrice) {
            listingCount_Rent += count;
            avgPrice_Rent += avgPrice * count;
            minPrice_Rent += minPrice * count;
            maxPrice_Rent += maxPrice * count;
            _addPropertyToUnit('rent',currAgg, result, pageData, rent_bhk, req);
        }
    }

    avgPriceUnitArea_Buy = Math.round(avgPriceUnitArea_Buy / listingCount_Buy);
    minPriceUnitArea_Buy = Math.round(minPriceUnitArea_Buy / listingCount_Buy);
    maxPriceUnitArea_Buy = Math.round(maxPriceUnitArea_Buy / listingCount_Buy);
    minPrice_Buy = Math.round(minPrice_Buy/listingCount_Buy);
    maxPrice_Buy = Math.round(maxPrice_Buy/listingCount_Buy);
    avgPrice_Buy = Math.round(avgPrice_Buy/listingCount_Buy);


    avgPrice_Rent = Math.round(avgPrice_Rent/listingCount_Rent);
    minPrice_Rent = Math.round(minPrice_Rent/listingCount_Rent);
    maxPrice_Rent = Math.round(maxPrice_Rent/listingCount_Rent);
    rent_bhk.sort((a, b) => { return a.toString() > b.toString(); });
    buy_bhk.sort((a, b) => { return a.toString() > b.toString(); });
    if(!listingCount_Buy) {
        dominantUnit = 'rent';
    }
    return {
        avgPrice_Rent: utilService.formatNumber(avgPrice_Rent, { type : 'number', seperator : numberFormat}),
        avgPrice_Rent_Unformatted: avgPrice_Rent,
        avgPrice_Buy_Unformatted: avgPrice_Buy,
        avgPrice_Buy: utilService.formatNumber(avgPrice_Buy, { type : 'number', seperator : numberFormat}),
        avgPriceUnitArea_Buy: utilService.formatNumber(avgPriceUnitArea_Buy, { type : 'number', seperator : numberFormat}),
        minPriceUnitArea_Buy: utilService.formatNumber(minPriceUnitArea_Buy, { type : 'number', seperator : numberFormat}),
        maxPriceUnitArea_Buy: utilService.formatNumber(maxPriceUnitArea_Buy, { type : 'number', seperator : numberFormat}),
        listingCount_Buy: utilService.formatNumber(listingCount_Buy, { type : 'number', seperator : numberFormat}),
        listingCount_Rent: utilService.formatNumber(listingCount_Rent, { type : 'number', seperator : numberFormat}),
        minPrice_Buy,
        maxPrice_Buy,
        maxPrice_Rent: utilService.formatNumber(maxPrice_Rent, { type : 'number', seperator : numberFormat}),
        minPrice_Rent: utilService.formatNumber(minPrice_Rent, { type : 'number', seperator : numberFormat}),
        parsedAggrigations: result,
        rent_bhk,
        buy_bhk,
        dominantUnit
    };
}

function _scoreLogic(score) {
    return score > 10 ? 10 : score;
}

function _parseTopLocality(topLocalities, {req}){
    let localityIdArray = [];
    for(let i=0, length=topLocalities.length;i<length;i++){
        let currLocality = topLocalities[i];
        _.assignIn(currLocality, averagePricesFromAggrigation(currLocality.listingAggregations, {req}));
        localityIdArray.push(currLocality.localityId);
    }
    return {topLocalities, localityIdArray};
}

function _parseLifestyleData(lifestyleData, localityId) {
    const mainDescriptionCategories = ['Life At','Under The Hood'];
    let description = lifestyleData[0].data,
        image = lifestyleData[1][localityId] || {},
        result = {mainDescription:{},data:[]};
    for(let i=0, length=description.length;i<length;i++){
        let currDesc = description[i].descriptionMap;
        if(mainDescriptionCategories.indexOf(currDesc.Title)>-1){
            result.mainDescription[currDesc.Title] = currDesc;
        } else {
            currDesc.images = image[currDesc.Title];
            if(currDesc.images && currDesc.images.length) {
                for(let i=0,length=currDesc.images.length;i<length;i++){
                    currDesc.images[i].url = imageParser.appendImageSize(currDesc.images[i].url, 'medium');
                }
            }
            currDesc.htmlTitle = currDesc.Title.replace(/ /g,'-');
            result.data.push(currDesc);
        }
    }
    return result;
}

function getAllLocalityPriceRangeParser(data, {req, config={}}){
    let parsedData = [];
    let extraFields = [ "localityId",
                        "label",
                        "overviewUrl",
                        "buyUrl",
                        "rentUrl",
                        "buyTrendUrl",
                        "rentTrendUrl",
                        "projectCount",
                        "avgPriceRisePercentage",
                        "avgPriceRisePercentageApartment",
                        "avgPriceRisePercentagePlot",
                        "avgPriceRisePercentageVilla",
                        "avgPriceRiseMonths",
                        "avgPricePerUnitArea",
                        "avgPricePerUnitArea",
                        "avgPricePerUnitArea",
                        "avgPricePerUnitArea",
                        "avgPricePerUnitAreaApartment",
                        "avgPricePerUnitAreaPlot",
                        "avgPricePerUnitAreaVilla",
                        "priceRiseRank",
                        "priceRiseRankPercentage"
                    ];
    let totalListingCountBuy = 0, totalListingCountRent = 0;
    let trendUrls = config.trendUrls || {};
    _.forEach(data, (v)=>{
        let localityData = {};
        localityData = parseLocalityListingAgg(v.listingAggregations, _.assign({
            localityId: v.localityId,
            cityId: v.cityId,
            suburbId: v.suburbId,
            localityName: v.label
        },config), {req});
        _.forEach(extraFields, (val)=>{
            localityData[val] = v[val];
        });
        localityData['overviewUrl'] = utilService.prefixToUrl(localityData['overviewUrl']);
        localityData['buyTrendUrl'] = utilService.prefixToUrl(trendUrls.buy && trendUrls.buy[v.localityId]);
        localityData['rentTrendUrl'] = utilService.prefixToUrl(trendUrls.rent && trendUrls.rent[v.localityId]);
        localityData['buyUrl'] = utilService.prefixToUrl(localityData['buyUrl']);
        localityData['rentUrl'] = utilService.prefixToUrl(localityData['rentUrl']);
        totalListingCountBuy += v.listingCountBuy || 0;
        totalListingCountRent += v.listingCountRent || 0;
        if(v.suburb){
            localityData.suburb = {
                buyUrl: utilService.prefixToUrl(v.suburb.buyUrl),
                overviewUrl: utilService.prefixToUrl(v.suburb.overviewUrl),
                rentUrl: utilService.prefixToUrl(v.suburb.rentUrl)
            };
            if(v.suburb.city){
                localityData.city = {
                    buyUrl: utilService.prefixToUrl(v.suburb.city.buyUrl),
                    overviewUrl: utilService.prefixToUrl(v.suburb.city.overviewUrl),
                    rentUrl: utilService.prefixToUrl(v.suburb.city.rentUrl)
                };
            }
        }
        parsedData.push(localityData);
    });
    if(config.needTotalCount){
        return {
            parsedData,
            totalListingCountBuy,
            totalListingCountRent
        };
    }
    return parsedData;
    
}

function getAllLocalityPriceRange(config={}, {req}){  //jshint ignore:line
    _prune(config);
    return getAllLocalities({
        cityId: config.cityId,
        localityId: config.localityId,
        suburbId: config.suburbId,
        count: config.rows || 20,//00000000
        start: config.start || 0
    }, {req}).then((response)=>{
        let totalCount = response.totalCount;
        if(config.needRaw){
            return response || {};
        }else{
            response = (response && response.data) || [];
            config.needTotalCount = true;
            let parsedData = getAllLocalityPriceRangeParser(response, {config, req});
            return {
                data: parsedData.parsedData,
                totalListingCountBuy: parsedData.totalListingCountBuy,
                totalListingCountRent: parsedData.totalListingCountRent,
                totalCount
            };
        }
    },(error)=>{
        throw error;
    });
}

function getParsedDataWithPriceTrendUrl (localities = [], config = {}, req) {  //jshint ignore:line
    let localityIds = [];
    let seoUrlParams = [];
        config.needTotalCount = true;
    if (config.templates && !utilService._.isEmpty(config.templates)) {
        utilService._.forEach(localities, (v) => {
            localityIds.push(v.localityId);
        });
        utilService._.forEach(config.templates, (v) => {
            seoUrlParams.push({
                "urlDomain": "locality",
                "domainIds": localityIds,
                "urlCategoryName": v
            });
        });

        return seoService.getEntityUrls(seoUrlParams, req).then((response) => {
            config.trendUrls = {
                buy: response[config.templates.buy],
                rent: response[config.templates.rent]
            };
            let parsedData = getAllLocalityPriceRangeParser(localities, {config, req});
            return {
                data: parsedData.parsedData,
                totalListingCountBuy: parsedData.totalListingCountBuy,
                totalListingCountRent: parsedData.totalListingCountRent,
                totalCount: config.totalCount
            };
        
        },(err)=>{
            throw err;
        });
    }else{
        return getAllLocalityPriceRangeParser(localities, {config, req});
    }
}

function getAllLocalityWithPriceTrendUrl(config = {}, { req }) { //jshint ignore:line
    config.needRaw = true;
    let localityDetailsList = getAllLocalityPriceRange(config, { req });
    config.templates = config.templates || {};
    if (!utilService._.isEmpty(config.templates)) {
        return localityDetailsList.then((localities) => {
            config.totalCount = localities.totalCount;
            localities =  localities.data || [];
            return getParsedDataWithPriceTrendUrl(localities, config, req);
        },(err)=>{
            throw err;
        });
    }else{
        return localityDetailsList;
    }
}

module.exports = {
    parseLocalityData,
    getTrendingSearch,
    getPopularLocalities,
    getTopLocalities,
    parseLifeStyleContent,
    averagePricesFromAggrigation,
    getLocalityDescription,
    getLifestyleData,
    getTopRankedLocalityInCity,
    getLocalityOverview,
    getAllLocalities,
    getNearbyLocalities,
    getTopRankedLocalities,
    getAllLocalityPriceRange,
    getAllLocalityPriceRangeParser,
    parseLocalityListingAgg,
    getAllLocalityWithPriceTrendUrl,
    getParsedDataWithPriceTrendUrl
};
