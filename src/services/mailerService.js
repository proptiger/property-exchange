"use strict";

const apiService = require('services/apiService'),
    utils = require('services/utilService'),
    logger = require('services/loggerService'),
    apiConfig = require('configs/apiConfig');

var mailerService = {};

mailerService.sendMessage = function({
    name,
    city,
    email,
    phone,
    subject,
    message
}, {
    req
}) {
    var payload;
    if (name && email && subject && message && phone && city) {
        payload = {
            "notificationType": "app_download",
            "users": [{
                "email": process.env.CUSTOMER_SERVICE_EMAIL
            }],
            "mediumDetails": [{
                "mediumType": "Email",
                "subject": subject,
                "body": [name, city, email, phone, message].join(" : "),
                "from": "no-reply@proptiger.com",
                "replyTo": email,
                "mailTo": [email]
            }]
        };
    }

    logger.info("payload for mailer service", JSON.stringify(payload));
    if (payload) {
        let api = apiConfig.notification();
        return apiService.post(api, {
            body: payload,
            req: req
        });
    } else {
        throw utils.getError(400);
    }
};

mailerService.partnerWithMakaanEnquiry = function({name, email, phone, message}, {req}){
    let payload = {
        notificationType: "partnering_with_makaan",
        users: [{
            email: process.env.SELLER_SUPPORT_EMAIL
        }],
        payloadMap: {
            name,
            phone,
            message
        },
        mediumDetails: [{
            "mediumType": "Email",
            "from": "no-reply@proptiger.com",
            "replyTo": email,
            "mailTo": [email]
        }]
    };
    let api = apiConfig.notification();
    return apiService.post(api, {
        body: payload,
        req: req
    });
};
mailerService.homeloanAgent = function({name, email, phone,city,branch}, {req}){
    let payload = {
        notificationType:"homeloan_agent_v2",
        users: [{
            email:process.env.HOMELOAN_SUPPORT_EMAIL
        }],
        payloadMap: {
            name,
            phone,
            email,
            city,
            branch
        },
        mediumDetails: [{
            "mediumType": "Email",
            "from": "no-reply@proptiger.com",
            "replyTo":email,
            "mailTo": [email]
        }]
    };
    let api = apiConfig.notification();
    return apiService.post(api, {
        body: payload,
        req: req
    });
};

module.exports = mailerService;
