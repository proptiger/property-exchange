'use strict';

const apiService = require('services/apiService'),
    apiConfig = require('configs/apiConfig'),
    utils = require('services/utilService'),
    imageParser = require('services/imageParser'),
    globalConfig = require('configs/globalConfig');

var builderService = {};

//parse builderData 
builderService.parseBuilderData = function(builderData, wordLimit = 100) {
    builderData = builderData || {};
    let projectStatus = builderData.projectStatusCount || {};

    let builder = {};
    builder.id = builderData.id;
    builder.name = builderData.name || builderData.displayName;
    builder.displayName = builderData.displayName ? builderData.displayName : builderData.name;
    builder.logo = imageParser.appendImageSize(builderData.imageURL, 'thumbnail');
    builder.description = {};
    builder.wordLimit = builderData.isDescriptionCrawlable ? wordLimit : globalConfig.wordLimit.nonCrawlableWordLimit;
    builder.description.fullDescription = builderData.description;
    builder.description.smallDescription = utils.stripHtmlContent(builderData.description, wordLimit, true, true).description;
    builder.url = utils.prefixToUrl(builderData.url);
    builder.age = builderData.establishedDate ? utils.timeFromDate(builderData.establishedDate) + ' years' : undefined;
    builder.yearEstablished = builderData.establishedDate ? parseInt(utils.formatDate(builderData.establishedDate, 'YY')):"";
    builder.timeCompletion = builderData.percentageCompletionOnTime ? builderData.percentageCompletionOnTime + '%' : undefined;
    builder.ongoing = projectStatus['under construction'] + projectStatus['launch'] + projectStatus['pre launch'];
    builder.completed = projectStatus.completed;
    builder.projectsCount = builderData.projectCount;

    return builder;
};

//wrapper for parsing builders information using builderService parser

function _parseFeaturedBuilders(response,config,mappedResponse){
   let wordLimit = config.productType == "featuredNationalProjects" ? 175 : 70;
   let cityId = config.cityId,builderName,cityName = config.cityName;
    let buildersData = {},
        data = response.data || [];

    for(let i=0; i < data.length; i++){
        let id = mappedResponse[data[i].id];
        buildersData[id] = builderService.parseBuilderData(data[i],wordLimit)||{};
        builderName = buildersData[id].name||'';
        if(config.productType == "featuredNationalProjects"){
            buildersData[id].builderPageUrl=`/seller-property?pageType=SELLER_PROPERTY_URLS&sellerId=${data[i].id}&sellerName=${builderName}`;   
        }else{
            buildersData[id].builderPageUrl=`/seller-property?pageType=SELLER_PROPERTY_URLS&cityId=${cityId}&cityName=${cityName}&sellerId=${data[i].id}&sellerName=${builderName}`; 
        }
    }
    return buildersData ; 
}

function mapBuilderIds(response) {
    let result = {},data={};
    if(response && response.data && response.data.length) {
        for(let element of response.data) {
            data[element.companyId] = element.userId;
        }
    }
    result.count = response && response.data && response.data.length || 0;
    result.data = data;
    return result;
}

function _getBuilderIdsBySellerIds(sellerIds, {req} = {}){
    let userId = sellerIds.join(";userId==");
    let urlObj = apiConfig.getBuilderIdBySellerId(userId);
    return apiService.get(urlObj, {req}).then((response)=>{
        return mapBuilderIds(response);
    }, (err)=>{
        throw err;
    });
}

//getting builder Information of list of builders using api 
 builderService.getBuilderInformation = function(sellerIds,{req} = {},config){
    return _getBuilderIdsBySellerIds(sellerIds, {req}).then((mappedResponse)=>{
        if(!mappedResponse.count) {
            return new Promise((resolve) => {
                resolve({});
            });
        }
        let selectorObj = {
            fields: ["id","buyUrl","name","displayName","projectCount","description","imageURL","isDescriptionCrawlable","projectStatusCount","establishedDate","percentageCompletionOnTime"],
            paging: {rows: mappedResponse && mappedResponse.count,start:0},
            filters: [],
        }, selector;
        selectorObj.filters.push({
            key:"id",
            value:Object.keys(mappedResponse.data)||[],
        });
        selector = apiService.createSelector(selectorObj);
        let urlObj = apiConfig.multipleBuilders({ query: { selector } }) ; // petra api for fetching multiple builder details

        return apiService.get(urlObj, {req}).then((response)=>{
            return _parseFeaturedBuilders(response,config,mappedResponse.data); // parse the details of projects returned by petra api
        }, (err)=>{
            throw err;
        }); 
    }, (err)=>{
        throw err;
    });       
}


function _parseBuilderUrlsForGurgaon(response) {
    if (response && response.data) {
        // Adding Central Park temporarily MAKAAN-4044
        let tempBuilder = {"id":106084,"name":"Central Park","displayName":"Central Park","establishedDate":679536000000,"url":"central-park-106084","buyUrl":"central-park-106084","projectStatusCount":{"pre launch":3,"under construction":7,"cancelled":0,"launch":1,"completed":5,"on hold":0,"not launched":1},"projectCount":12,"listingCountBuy":554,"listingCountRent":1090,"images":[{"id":12527003,"altText":"Central Park","title":"Central Park","activeStatus":1,"absolutePath":"https://content.makaan.com/3/106084/305/12527003.jpeg"},{"id":5628284,"altText":"Central Park","title":"Central Park","activeStatus":1,"absolutePath":"https://content.makaan.com/3/106084/305/central-park-5628284.jpeg"}],"mainImage":{"id":0,"altText":"Central Park","title":"Central Park","activeStatus":0,"absolutePath":"https://content.makaan.com/3/106084/305/12527003.jpeg"},"activeStatus":"Active"};
        response.data.splice(2, 0, tempBuilder);
        response.data.splice(response.data.length-1, 1);
    }
    return _parseBuilderUrls(response);
}

function _parseBuilderUrls(response){
    if (!(response && response.data)) {
        return [];
    }
    for(let i = 0; i < response.data.length; i++){
        response.data[i].url = utils.prefixToUrl(response.data[i].url);
    }
    return response;
}

function _parseTopBuildersForGurgaon(response) {
    if (response && response.data) {
        // Adding Central Park temporarily MAKAAN-4044
        let tempBuilder = {"id":106084,"name":"Central Park","displayName":"Central Park","establishedDate":679536000000,"url":"central-park-106084","buyUrl":"central-park-106084","projectStatusCount":{"pre launch":3,"under construction":7,"cancelled":0,"launch":1,"completed":5,"on hold":0,"not launched":1},"projectCount":12,"listingCountBuy":554,"listingCountRent":1090,"images":[{"id":12527003,"altText":"Central Park","title":"Central Park","activeStatus":1,"absolutePath":"https://content.makaan.com/3/106084/305/12527003.jpeg"},{"id":5628284,"altText":"Central Park","title":"Central Park","activeStatus":1,"absolutePath":"https://content.makaan.com/3/106084/305/central-park-5628284.jpeg"}],"mainImage":{"id":0,"altText":"Central Park","title":"Central Park","activeStatus":0,"absolutePath":"https://content.makaan.com/3/106084/305/12527003.jpeg"},"activeStatus":"Active"};
        response.data.splice(2, 0, tempBuilder);
        response.data.splice(response.data.length-1, 1);
    }
    return _parseTopBuilders(response);
}

function _parseTopBuilders(response) {
    if (!(response && response.data)) {
        return [];
    }
    response = response.data;
    let result = [],
        i, length = response.length,
        temp;
    for (i = 0; i < length; i++) {
        let projectStatusCount = response[i].projectStatusCount || {};
        temp = {};
        temp.id = response[i].id;
        temp.name = response[i].name;
        temp.displayName = response[i].displayName ? response[i].displayName : response[i].name;
        temp.experience = response[i].establishedDate ? new Date().getFullYear() - new Date(response[i].establishedDate).getFullYear() : undefined;
        temp.totalProjects = response[i].projectCount;
        temp.ongoingProjects = projectStatusCount['under construction'] + projectStatusCount['launch'] + projectStatusCount['pre launch'];
        temp.propertyForSale = response[i].listingCountBuy;
        temp.propertyForRent = response[i].listingCountRent;
        temp.completed = projectStatusCount['completed'];
        temp.url = utils.prefixToUrl(response[i].url);
        let images = response[i].mainImage || undefined;
        temp.image = (typeof images !== 'undefined') ? response[i].mainImage.absolutePath : undefined;
        temp.image = imageParser.appendImageSize(temp.image, 'tile');
        temp.title = temp.image && response[i].mainImage.title;
        temp.altText = temp.image && response[i].mainImage.altText;

        temp.buyUrl = temp.rentUrl = utils.prefixToUrl(response[i].buyUrl);

        temp.projectStatusCount = response[i].projectStatusCount;

        result.push(temp);
    }
    return result;
}

builderService.getTopBuilders = function(data, { req }) {
    let key, value, temp = {"filters":[]},
        config = {
            rows: 5,
            start: 0,
            fields: ["id", "name", "displayName", "establishedDate", "projectCount", "images", "absolutePath", "url", "activeStatus", "projectStatusCount", "buyUrl", "listingCountBuy", "listingCountRent", "mainImage", "altText", "title"]
        },
        localityId = data.localityId || null,
        cityId = data.cityId || null,
        count = data.count || config.rows,
        start = data.start || config.start;
    key = localityId ? 'localityId' : cityId ? 'cityId' : '';
    value = localityId ? localityId : cityId ? cityId : '';
    if(cityId || localityId){
        temp.filters = [{
            "key": key,
            "value": value
        }, {
            "key": "builderDbStatus",
            "value": ["Active", "ActiveInMakaan"]
        }];
    }
    if(data.countryId) {
        temp.filters.push({
            "key":'countryId',
            "value": data.countryId
        });
    }
    temp.fields = config.fields;
    temp.paging = {
        "start": start,
        "rows": count
    };

    let urlConfig = apiConfig.topBuilder({
        query: {
            selector: apiService.createSelector(temp)
        }
    });
    return apiService.get(urlConfig, { req }).then(function(response) {
        if (data.cityId == 11){ // Adding Central Park temporarily MAKAAN-4044
            if(data.needRaw){
                return _parseBuilderUrlsForGurgaon(response);
            } else {
                return _parseTopBuildersForGurgaon(response);
            }
        } 
        if(data.needRaw){
            return _parseBuilderUrls(response);
        } else {
            return _parseTopBuilders(response);
        }
    }, err => {
        throw err;
    });
};

builderService.getSimilarBuilder = function(builderId, cityId, options = {}, {req}) {  //jshint ignore:line
    let count = options.count || 4,
        selector = {};
    selector.fields = ["id", "name", "displayName", "establishedDate", "projectCount", "images", "absolutePath", "url", "activeStatus", "projectStatusCount", "buyUrl", "listingCountBuy", "listingCountRent", "mainImage", "altText", "title"];
    // selector.filters.and.push({"equal":{"builderDbStatus":["Active","ActiveInMakaan"]}})
    if (cityId) {
        selector.filters = {};
        selector.filters.and = selector.filters.and || [];
        selector.filters.and.push({ "equal": { "cityId": cityId } });
    }

    selector = JSON.stringify(selector);

    let urlConfig = apiConfig.similarBuilder({
        builderId,
        count,
        query: {
            selector: selector
        }
    });
    return apiService.get(urlConfig, {req}).then(response => {
        return _parseTopBuilders(response);
    }, err => {
        throw err;
    });
};

builderService.getAllBuilders = function(config = {}, {req}) {    //jshint ignore:line
    let fields = ["id", "name", "displayName", "establishedDate", "projectCount", "absolutePath", "url", "activeStatus", "projectStatusCount", "buyUrl", "listingCountBuy", "listingCountRent", "mainImage", "altText", "title"];
    let selector = {
            fields: (config.fields && config.fields.concat(fields)) || fields, 
            paging: {
                start: config.start,
                rows: config.rows
            },
            filters: [{
                "key": "builderDbStatus",
                "value": ["Active", "ActiveInMakaan"]
            }]
        },
        urlConfig;
    if (config.cityId) {
        selector.filters.push({
            "key": "cityId",
            "value": config.cityId
        });
    }
    if(config.countryId) {
        selector.filters.push({
            "key":'countryId',
            "value": config.countryId
        });
    }

    urlConfig = apiConfig.allBuilder({
        query: {
            selector: apiService.createSelector(selector)
        }
    });

    return apiService.get(urlConfig, {req}).then(response => {
            return _parseBuilderUrls(response);
    }, err => {
        throw err;
    });
};

builderService.getTopBuildersCards = function(config = {}, {req}) {  //jshint ignore:line
    let selector = [],
        urlConfig = [],
        fields = ["id", "name", "establishedDate", "projectCount", "absolutePath", "url", "activeStatus", "projectStatusCount", "buyUrl", "listingCountBuy", "listingCountRent", "mainImage", "altText", "title"];


    function defaultSelector(config) {
        this.fields = config.fields ? config.fields.concat(fields) : fields;
        this.paging = {
            start: config.start,
            rows: 5
        };
        this.filters = [{
            "key": "builderDbStatus",
            "value": ["Active", "ActiveInMakaan"]
        }];
        if(config.cityId){
            this.filters.push({
                "key": "cityId",
                "value": config.cityId
            });
        }
        if(config.countryId){
            this.filters.push({
                "key": "countryId",
                "value": config.countryId
            });
        }
    }
    var i;
    for ( i = 0; i < 7; i++) {
        selector.push(new defaultSelector(config));  //jshint ignore:line
    }

    //luxuryConfig
    selector[0].filters.push({
            key: 'price',
            type: 'range',
            from: 10000000
        });
        //underConstructionConfig
    selector[1].filters.push({
        "key": "listingConstructionStatusId",
        "value": 1
    });

    //readyToMoveConfig
    selector[2].filters.push({
        "key": "listingConstructionStatusId",
        "value": 2
    });

    //resaleConfig
    selector[3].filters.push({
            key: 'listingCategory',
            value: ['Resale']
        });
        //affordableConfig
    selector[4].filters.push({
            key: 'price',
            type: 'range',
            to: 1000000
        });
        //newProjectConfig
    selector[5].filters.push({
            "key": "listingConstructionStatusId",
            "value": [3, 7]
        });
        //upcomingConfig
    selector[6].filters.push({
        "key": "listingConstructionStatusId",
        "value": [6, 8]
    });

    for (i = 0; i < 7; i++) {
        urlConfig.push(apiService.get(apiConfig.topBuilder({
            query: {
                selector: apiService.createSelector(selector[i])
            }
        }),{req}));
    }

    return Promise.all(urlConfig).then((response) => {
        return (null, { "Luxury": _parseBuilderUrls(response[0]), "Under Construction": _parseBuilderUrls(response[1]), "Ready To Move": _parseBuilderUrls(response[2]), "Resale": _parseBuilderUrls(response[3]), "Affordable": _parseBuilderUrls(response[4]), "New": _parseBuilderUrls(response[5]), "Upcoming": _parseBuilderUrls(response[6]) });
    }, err => {
        throw err;
    });

};

module.exports = builderService;
