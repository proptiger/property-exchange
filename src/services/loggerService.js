"use strict";

const winston = require('winston');

var logger = new(winston.Logger)({
    transports: [
        new(winston.transports.Console)({
            'timestamp': function() {
                let offset = 5.5;   // IST
                return new Date( new Date().getTime() + offset * 3600 * 1000).toUTCString().replace( / GMT$/, "" );
            },
            'level': process.env.LOG_LEVEL || 'debug',
            'colorize': true,
            "formatter": customFormatter
        })
        // new(winston.transports.File)({
        //     filename: 'marketplace.log',
        //     'timestamp': true
        // })
    ]
});

function _getLevel(level){
    let defaultLevel = "info";
    let config = {
        error: {
            start: "\x1b[31m",
            end: "\x1b[0m"
        },
        warn: {
            start: "\x1b[33m",
            end: "\x1b[0m"
        },
        info: {
            start: "\x1b[32m",
            end: "\x1b[0m"  
        }
    };
    return `${config[level] && config[level].start || config[defaultLevel].start}${level}${config[level] && config[level].end || config[defaultLevel].end}`;
}

function customFormatter (options) {
    // Return string will be passed to logger.
    return "\x1b[34mconsolidated-buyer-logs\x1b[0m " + options.timestamp() +' - '+ _getLevel(options.level) +': '+ (undefined !== options.message ? options.message : '') + (options.meta && Object.keys(options.meta).length ? ' '+ _metaFormatter(options.meta) : '' );
}

function _metaFormatter(meta){
    let str = '';
    for(var key in meta){
        str += `${key}=${meta[key]} `;
    }
    return str;
}

module.exports = logger;
