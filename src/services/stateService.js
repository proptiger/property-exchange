"use strict";
const apiService = require('services/apiService'),
    apiConfig = require('configs/apiConfig'),
    utilService = require('services/utilService'),
    logger = require('services/loggerService'),
    _ = utilService._;

var stateService = {};

function parseAllState(data, config = {}) {
    let parsedData = [];
    _.forEach(data, (v) => {
        var stateData = {};
        stateData.stateId = v.stateId;
        stateData.country_id = v.country_id;
        stateData.label = v.label;
        stateData.makaanUrl = utilService.prefixToUrl(v.makaanUrl);
        parsedData.push(stateData);
    });
    return parsedData;
}

stateService.getAllStateList = function(config = {}, { req }) { //jshint ignore:line
    
    let urlConfig = apiConfig.masterStateList();

    return apiService.get(urlConfig, { req }).then(response => {
        if (config.needRaw) {
            return response || {};
        } else {
            let totalCount = response.totalCount;
            response = (response && response.data) || [];
            return {
                data: parseAllState(response, config),
                totalCount
            };
        }
    }, error => {
        throw error;
    });
};

module.exports = stateService;
