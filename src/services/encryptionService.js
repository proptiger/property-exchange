"use strict";

const apiService = require('services/apiService'),
    apiConfig = require('configs/apiConfig');

var encryptionService = {};


encryptionService.getDataViaToken = function(configType,payload,req) {
    let urlConfig = apiConfig[configType]();
    return apiService.post(urlConfig,{body: payload,req}).then(response => {
        return response.data;
    }, err => {
        return ;
    })
};


module.exports = encryptionService;
