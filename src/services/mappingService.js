"use strict";
const utils = require('services/utilService');
const sharedConfig = require('public/scripts/common/sharedConfig');

module.exports.getPropertyStatus = function(key) {
    let map = {
        1: 'Under Construction',
        2: 'Ready to move'
    };
    return map[key];
};

module.exports.isReadyToMove = function(key) {
    if (key == 2) {
        return true;
    }
    return false;
};

module.exports.getFurnishingStatus = function(key) {
    let map = {
        'unfurnished': 'Unfurnished',
        'semiFurnished': 'Semi-furnished',
        'semi-furnished': 'Semi-furnished',
        'fullyFurnished': 'Fully furnished',
        'furnished': 'Fully furnished'
    };
    return map[key];
};

module.exports.getOwnershipType = function(key) {
    let map = {
        'freehold': 'Freehold',
        'leasehold': 'Leasehold',
        'powerOfAttorney': 'Power of attorney',
        'cooperativeSociety': 'Cooperative society'
    }, numberMap = {
        1: 'Freehold',
        2: 'Leasehold',
        3: 'Power of attorney',
        4: 'Cooperative society'
    };

    if(typeof key == 'number'){
        return numberMap[key];
    }
    return map[key];
};

module.exports.getTenantTypes =function(key){
    let map = {
        1: 'Bachelors',
        2: 'Family',
        3: 'Pets allowed',
        4: 'Non-Vegetarian',
        5: 'Company lease'
    };
    return map[key];
};

module.exports.getFacingDirection = function(key) {
    let map = {
        1: 'East',
        2: 'West',
        3: 'North',
        4: 'South',
        5: 'NorthEast',
        6: 'SouthEast',
        7: 'NorthWest',
        8: 'SouthWest'

    };
    return map[key];
};
module.exports.getMakaanSelectCouponStatus = function(key) {
    let map = {
        'GENERATED':1,
        'SHARED_WITH_SELLER':2,
        'SELLER_REDEEMED':3,
        'SELLER_DENIED':4  
    };
    return map;
};
module.exports.getDirectionViewType = function(key) {
    let map = {
        '1': 'Park view',
        '2': 'Road view',
        '3': 'Pool view',
        '4': 'Club view',
        '5': 'Corner',
        '6': 'Other',
        '7': 'Garden view',
        '8': 'Temple view'
    };
    return map[key];
};

module.exports.getPropertyType = utils.getPropertyType;

module.exports.getPropetyLeaseType = function(key) {
     let map = {
      primary : "New",
      resale : "Resale",
      rental : "Rental"
    };
    return map[key];
};

module.exports.getUnitTypes = function(){
    return [{
        id: 1,
        displayText: 'Apartment'
    },{
        id: 2,
        displayText: 'Villa'
    },{
        id: 3,
        displayText: 'Residential plot'
    },{
        id: 5,
        displayText: 'Shop'
    },{
        id: 6,
        displayText: 'Office'
    }];

};

module.exports.dealStatusMapping = {
    "Expert": ['CityExpertDealMaker', 'SuburbExpertDealMaker', 'LocalityExpertDealMaker','ExpertDealMaker'],
    "Normal": ['DealMaker']
};

module.exports.listingSellerTransactionMapping = sharedConfig.listingSellerTransactionMapping;
module.exports.LEAD_PROFILE_QUESTION_TYPE = sharedConfig.LEAD_PROFILE_QUESTION_TYPE;
module.exports.LP_NEXT_BTN_QUESTION_TYPE = sharedConfig.LP_NEXT_BTN_QUESTION_TYPE;


module.exports.enquiryTypeMapping = sharedConfig.enquiryTypeMapping;

module.exports.dealMakersMapping = {
    CITY_EXPERT: "CityExpertDealMaker",
    SUBURB_EXPERT: "SuburbExpertDealMaker",
    LOCALITY_EXPERT: "LocalityExpertDealMaker",
    EXPERT: "ExpertDealMaker",
    DEAL_MAKER: "DealMaker"
};

module.exports.sellerScoreMapping = {
    6: "Property Options",
    9: "Customer's Feedback",
    12: "Deals Closed",
    15: "Response"
}

function getHighestSellerStatus(listingSellerBenefits) {
    let DEAL_MAKER_PRIORITY = ['CityExpertDealMaker','LocalityExpertDealMaker','ExpertDealMaker','DealMaker'];
    if(listingSellerBenefits && listingSellerBenefits.length>0){
        for(let i=0;i<DEAL_MAKER_PRIORITY.length;i++){
            let deal = DEAL_MAKER_PRIORITY[i]; 
            if(listingSellerBenefits.indexOf(deal) > -1 ){
                return sharedConfig.listingSellerTransactionMapping[deal];
            }
        }
    }
    return {};
}

module.exports.getHighestSellerStatus = getHighestSellerStatus;

module.exports.getSellerStatuses = function(listingData) {
    let expertStatus = getHighestSellerStatus(listingData.listingSellerBenefits);

    return {
        expertStatus,
        isMakaanSelectSeller: listingData && listingData.listingSellerMiscellaneousProducts && listingData.listingSellerMiscellaneousProducts.indexOf('MakaanSelect')>=0 ? true: false,
        showNestawayBenefit: listingData && listingData.listingSellerPrivileges && listingData.listingSellerPrivileges.indexOf('MakaanPrivilegeSeller')>=0 ? true : false,
        isPremiumProperty: listingData && listingData.listingSellerBenefits && listingData.listingSellerBenefits.indexOf('ListingPremiumOwner')>=0 ? true : false,
        isCityExpert: listingData && listingData.listingSellerBenefits && listingData.listingSellerBenefits.indexOf('CityExpertDealMaker')>=0 ? true : false,
        isLocalityExpert: listingData && listingData.listingSellerBenefits && (listingData.listingSellerBenefits.indexOf('LocalityExpertDealMaker')>=0 || listingData.listingSellerBenefits.indexOf('SuburbExpertDealMaker')>=0)? true : false,
        isExpertDealMaker: listingData && listingData.listingSellerBenefits &&  listingData.listingSellerBenefits.indexOf('ExpertDealMaker')>=0 ? true : false,
        isDealMaker: listingData && listingData.listingSellerBenefits && listingData.listingSellerBenefits.indexOf('DealMaker')>=0 ? true : false,
        isAccountLocked: (listingData.listingSellerTransactionStatuses && listingData.listingSellerTransactionStatuses.indexOf('ACCOUNT_LOCKED') > -1) ? true : false   
    }
        
}