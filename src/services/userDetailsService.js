"use strict";

const apiService = require('services/apiService'),
    apiConfig = require('configs/apiConfig');

var userDetailsService = {};

userDetailsService.getUserDetails = function(req, res) {
    return apiService.get(apiConfig.userDetails(), {
        req, res
    }).then(response => {
        return userDetailsParser(response);
    });
};

function userDetailsParser(response = {}) {
    let finalResponse = {};
    response = response.data;
    finalResponse.id = response.id;
    finalResponse.email = response.email;
    finalResponse.firstName = response.firstName;
    finalResponse.lastName = response.lastName;
    finalResponse.contactNumber = response.contactNumber;
    finalResponse.profileImageUrl = response.profileImageUrl;
    finalResponse.countryId = response.countryId;
    return response;
}

module.exports = userDetailsService;
