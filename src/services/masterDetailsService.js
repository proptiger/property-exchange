"use strict";
const ApiService = require('services/apiService'),
    ApiConfig = require('configs/apiConfig'),
    utilService = require('./utilService'),
    _ = utilService._,
    logger = require('services/loggerService'),
    datadog = require("datadog"),
    RETRY_MILLISEC = 3000;

var masterData = {
        countriesList: {},
        amenities: {},
        furnishings: {},
        unitTypes: {},
        unitTypesNameMap: {},
        cityList: {},
        allianceCityList: {},
        experimentList: {}
    },
    parser = {
        amenities(data) {
                let result = {};
                _.forEach(data, (v) => {
                    result[v.amenityId] = {
                        name: v.amenityName,
                        abbreviation: v.abbreviation
                    };
                });
                return result;

            },
            furnishings(data) {
                let result = {};
                _.forEach(data, (v) => {
                    result[v.id] = {
                        name: v.name
                    };
                });
                return result;
            },
            unitTypes(data) {
                let result = {};
                _.forEach(data, (v) => {
                    result[v.id] = v.name;
                });
                return result;
            },
            unitTypesNameMap(data) {
                let result = {};
                _.forEach(data, (v) => {
                    let name = v.name && v.name.toLowerCase();
                    result[name] = {
                        label:v.displayName,
                        id: v.id
                    };
                });
                return result;
            },
            cityList(data) {
                return data;
            },
            allianceCityList(data){
                return data;
            },
            countriesList(data) {
                return data;
            },
            experimentList(data) {
                return data.filter(item => {
                    return item && item.name.indexOf("makaan") > -1;
                });
            }
    };

function getMasterDetails(type) {
    if (!type) {
        return {};
    }
    return masterData[type];
}

function _populateMasterData(key, apiUrl) {
    //Passing ignoreReq as it is not a request from the client
    ApiService.get(apiUrl, {ignoreReq: true}).then((response) => {
        response = response && response.data || response;
        if(key == 'unitTypes'){
            masterData[key] = parser[key](response);
            masterData['unitTypesNameMap'] = parser['unitTypesNameMap'](response);
        }else{
            masterData[key] = parser[key](response);
        }
    }).catch((err)=>{
        datadog.tracker.event(`MasterDetailsFetchError`, {name: key, error: err}, {
            alert_type: 'error',
            tags: ['timeout']
        });
        logger.error('Error fetching masterData for '+ key, (err));
        setTimeout(function(){
            logger.info('Retry fetching masterData for '+ key);
            _populateMasterData(key, apiUrl);
        }, RETRY_MILLISEC);
    });
}

function populateAllMasterData() {
    let config = {
        countriesList: ApiConfig.getCountries(),
        amenities: ApiConfig.masterAmenities(),
        furnishings: ApiConfig.masterFurnishings(),
        unitTypes: ApiConfig.masterUnitTypes(),
        cityList: ApiConfig.masterCityList({
            query: {
                selector: JSON.stringify({
                    "fields": ["id", "label", "displayPriority", "displayOrder"],
                    "paging": {
                        "rows": 1000000
                    },
                    "sort": {
                        "field": "displayPriority",
                        "sortOrder": "DESC"
                    }
                })
            }
        }),
        allianceCityList: ApiConfig.getAllianceCities(),
        experimentList: ApiConfig.abExperiments()
    };

    _.forEach(config, (v, k) => {
        _populateMasterData(k, v);
    });
}

module.exports = {
    getMasterDetails,
    populateAllMasterData
};

//masterDetailsService.getMasterDetails(unitTypes(propertyTypes))
