"use strict";

let linkTags = {
    'preconnect': [
        '//content.makaan.com',
        '//www.google-analytics.com',
        process.env.CDN_URL_JS,
        process.env.CDN_URL_CSS
    ],
    'dns-prefetch': [
    ]
};

module.exports = linkTags;
