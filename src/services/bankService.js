"use strict";

const apiService = require('services/apiService'),
    apiConfig = require('configs/apiConfig');

var bankService = {};


bankService.getBankDetails = function(req) {
    let urlConfig = apiConfig.getBankDetails();
    return apiService.get(urlConfig, { req }).then(response => {
        return response.data;
    }, err => {
        //todo: remove mock data
        return require('mock/banks');
        //throw err;
    })
};

bankService.getApprovedBankDetails = function(req, data) {
    let urlConfig = apiConfig.getApprovedBankDetails(data.projectId);
    return apiService.get(urlConfig, { req }).then(response => {
        return response;
    }, err => {
        //todo: remove mock data
        return require('mock/approvedBanks');
        //throw err;
    })
};

bankService.getBankApplicationTracking = function(userId){
    let urlConfig = apiConfig.getBankApplicationTracking(userId);
    return apiService.get(urlConfig).then(response => {
        return response;
    }, err=>{
        //todo: remove mock data
        console.log('Error: getBankApplicationTracking', userId);
        return require('mock/bankApplicationTracking');
        //throw err;
    });
};

module.exports = bankService;
