"use strict";

const   apiService = require('services/apiService'),
        apiConfig = require('configs/apiConfig'),
        utils = require('services/utilService'),
        maps = require('services/mappingService'),
        sellerScoreComponents = [6, 9, 12, 15];


let sellersRatingReviewService = {};

sellersRatingReviewService.getSellersLatestReview = function(sellerCompanyUserIds){
    let api = apiConfig.sellersReviewsMap(sellerCompanyUserIds);
    return apiService.get(api).then((response) => {
        return response;
    }, (err)=>{
        throw err;
    });
}

function parseDataOnCompanyId(data){
    let resultData = {};
    data.forEach(item => {
        if(!resultData[item.entityDataId]){
            resultData[item.entityDataId] = {};
        }
        resultData[item.entityDataId][item.value] = item.extraAttributes.countId;
    });
    return resultData;
}

function _parseReviews (response){
    const MIN_RATING_FOR_DISPLAY = 3,
          RATING_TYPE_QUESTIONS = 2,
          FEEDBACK_TYPE_ID = 50,
          ratingIdToDisplayValueMapping ={
              53:'behavior',
              54:'propertyOptions',
              55:'fieldKnowledge',
              56:'siteVisits',
              57:'negotiationSkills',
              58:'afterServices'
          };
    let rating, i;
    var data = {
        reviewCount: response.totalCount
    };
    if(!response || !Object.keys(response)) {
        return {}
    }
    if(response.data.averageRatings){
        let averageRatings ={};
        for(i=0;i<response.data.averageRatings.length;i++){
            let rating = response.data.averageRatings[i];
            if(rating && ratingIdToDisplayValueMapping[rating.ratingParameterId]){
                averageRatings[ratingIdToDisplayValueMapping[rating.ratingParameterId]] = rating.ratingCategory;
            }
        }
        data.averageRatings = averageRatings;
    }

    data.feedbacks = response.data.feedbackEntityDetailsList.map((feedback)=>{
        let date = (new Date(feedback.createdAt).toDateString()).substr(4),
            buyerName = feedback.username,
            profilePic = feedback.profilePicUrl,
            ratingCategory = feedback.ratingCategory, 
            highlights = [],
            bestRatingHighlight,
            comments,
            city;
        if(profilePic){
            if(profilePic.indexOf('?')>0){
                profilePic+='&';
            } else {
                profilePic+='?';
            }
            profilePic+='width=44&height=44';
        }
        const NEW_LINE_PATTERN =  /\r?\n|\r/g;

        for(var i=0;i<feedback.ratings.length;i++){
            rating = feedback.ratings[i];
            if(rating.ratingParameterId === FEEDBACK_TYPE_ID) {
                comments= rating.answerValue.replace(NEW_LINE_PATTERN,' ');
            }

        }
        if(feedback.cityName){
            // buyerName+=','
            city = feedback.cityName;
        }
        return {
            date,
            buyerName,
            profilePic,
            comments,
            ratingCategory,
            city
        };
    });
    return data;
}

function _parseRatingBreakUp(sellerCompanyId, data){
    data = parseDataOnCompanyId(data);
    let result = [], temp, total = 0;
    for(let companyId in data){
        let companyRatings = {
            "sellerCompanyId": companyId,
            "ratings": {}
        };
        temp = data[companyId];
        ['1','2','3','4','5'].map((val)=>{
            temp[val] = temp[val] || 0;
            total += temp[val];
        });
        total = total || 1;
        ['1','2','3','4','5'].map((val)=>{
            companyRatings.ratings[val] = {
                value: temp[val],
                percentage: (temp[val]/total)*100
            }
        });
        result.push(companyRatings);
    }
    if(result.length){
        return result[0];
    } else {
        return {};
    }
}

sellersRatingReviewService.getSellerRatingsMap = function(listingUserCompanyUserId){
    let ratingSince = utils.dateAhead('', {years: -1}, true);
    let selector = {
        "fields": "countId",
        "filters":[{
            "filter": "and",
            "type": "equal",
            "key": "ratingParameterId",
            "value": 59
        },{
            "value": listingUserCompanyUserId,
            "filter": "and",
            "type": "equal",
            "key": "sellerId"
        },{
            "type": "ge",
            "key": "createdAt",
            "filter": "and",
            "value": ratingSince.toJSON()
        }]
    }
    let api = apiConfig.sellerRatingsMap(apiService.createFiqlSelector(selector));
    return apiService.get(api).then((response) => {
        return _parseRatingBreakUp(listingUserCompanyUserId, response.data||[]);
    }, (err)=>{
        throw err;
    });
}
sellersRatingReviewService.parseSellerScoreDetail = function(response){
    let result = {
        companyUserId: undefined,
        score : []
    };
    if(response && response.breakup && response.breakup.length){
        response.breakup.forEach(item => {
            result.companyUserId = item.sellerId;
            if(sellerScoreComponents.indexOf(item.sellerScoreType.id) > -1){
                result.score.push({
                    id: item.sellerScoreType.id,
                    typeLabel: maps.sellerScoreMapping[item.sellerScoreType.id],
                    value: item.absoluteScore
                });
            }
        });
        result.score.sort((score1, score2) => {
            return score2.value - score1.value;
        });
    }
    return result;
}

sellersRatingReviewService.getSellerReviews = function (companyUserId, {req}){
    return apiService.get(apiConfig.getSellerReviews({ companyUserId, query: {selector:JSON.stringify({})} }),{req}).then((response) => {
        return _parseReviews(response);
    }).catch(err => {
        return {}
    });
}

sellersRatingReviewService.getSellerReviewsByComapnyId = function (companyId, {req}){
    return apiService.get(apiConfig.getCompanyUserIds(companyId, {req})).then((response)=>{
        let comapanyUsers = response.data || [];
        let companyUserId = null;
        for(let companyUser of comapanyUsers) {
            if(companyUser.parentId == 0) {
                companyUserId = companyUser.userId;
            }
        }
        if(companyUserId) {
            return sellersRatingReviewService.getSellerReviews(companyUserId, {req});
        } else {
            return new Promise((resolve) => {
                resolve({});
            });
        }
    }).catch( err => {
        return new Promise((resolve) => {
            resolve({});
        });
    });
}

module.exports = sellersRatingReviewService;
