"use strict";
const ApiService = require('services/apiService'),
    ApiConfig = require('configs/apiConfig'),
    globalConfig = require('configs/globalConfig'),
    UtilService = require('./utilService'),
    _ = UtilService._;

const entityMap = {
        'listing': 17,
        'locality': 4,
        'project': 1,
        'property': 2,
        'video': 18,
        "projectLocality": 4,
        'bank': 5
    },
    imageTypeMap = {
        "project": {
            "clusterPlan": 2,
            "constructionStatus": 3,
            "layoutPlan": 4,
            "main": 6,
            "masterPlan": 7,
            "paymentPlan": 8,
            "amenities": 80,
            "mainOther": 81,
            "3DMasterPlan": 107,
            "galleryImagesBig": 262,
            "galleryImagesSmall": 261
        },
        "property": {
            "floorPlan": 368,
            "3DFloorPlan_PROPTIGER": 89,
            "3DFloorPlan_MAKAAN": 593,
            "VideoWalkthrough": 106
        },
        "locality": {
            "mall": 14,
            "road": 15,
            "school": 16,
            "hospital": 17,
            "other": 18,
            "heroshot": 354,
            'Life At': 581,
            'Under The Hood': 582,
            'The Neighbours': 583,
            'What You Will Find': 584,
            'Living It Up': 585,
            'What You Will Not Find': 586,
            'The Locality Scape': 587,
            'Find Joy In': 588
        },
        "projectLocality": {
            "heroshot": 354
        },
        "listing": {
            "bathroom": 96,
            "balcony": 99,
            "bedroom": 95,
            "dining": 98,
            // "exterior":,
            // "floorPlan":,
            "kitchen": 101,
            // "layoutPlan":,
            "living": 97,
            // "localtionMap":,
            // "masterPlan":
        },
        "video": {
            "VideoThumbnail": 111
        }
    };

const config = {
    project: [],
    listing: [], //["bathroom", "balcony", "bedroom", "dining", "kitchen", "living"],
    serp: [],
    property: ["floorPlan", "3DFloorPlan_PROPTIGER", "3DFloorPlan_MAKAAN", "VideoWalkthrough"],
    locality: ['Life At', 'Under The Hood', 'The Neighbours', 'What You Will Find', 'Living It Up', 'What You Will Not Find', 'The Locality Scape', 'Find Joy In'],
    projectLocality: ["heroshot"]
};

var moveItemToTop = function(data, item, callback){
    let selectedItem;
    _.remove(data, (v)=>{
        if(callback(v, item)){
            selectedItem = v;
            return true;
        }
        return false;
    });
    if(selectedItem){
        data.unshift(selectedItem);
    }
};

var getFiltersArray = function(data) {
    let filterArr = [];
    _.forEach(data, obj => {
        let tempFilter = {
            multiple: true,
            filter: 'or',
            filters: [{
                filter: 'and',
                key: 'imageTypeObj.objectTypeId',
                value: entityMap[obj.entityType]
            }]
        };


        let tempObj;
        if(obj.id && _.isArray(obj.id)){ // case when same entityType has multiple values
            tempObj = {
                multiple: true,
                filter: 'and',
                filters: []
            };
            _.forEach(obj.id, id => {
                tempObj.filters.push({
                    filter: 'or',
                    key: 'objectId',
                    value: id
                });
            });
        }else {
            tempObj = {
                filter: 'and',
                key: 'objectId',
                value: obj.id
            };
        }
        tempFilter.filters.push(tempObj);

        let entityImageTypes = config[obj.entityType];
        if (entityImageTypes && entityImageTypes.length) {
            tempFilter.filters.push({
                multiple: true,
                filter: 'and',
                filters: []
            });
            _.forEach(entityImageTypes, imageType => {
                tempFilter.filters[2].filters.push({
                    filter: 'or',
                    key: 'imageTypeId',//'imageTypeObj.type',
                    value: imageTypeMap[obj.entityType][imageType] //imageType
                });
            });
        }

        tempFilter.filters.push({
            filter: 'and',
            key: 'activeStatus',
            value: 1
        });

        filterArr.push(tempFilter);
    });
    return filterArr;
};

function getImageDetails(page, options = {}, {req, isPrimary}) {  //jshint ignore:line
    switch (page) {
        case 'project':
            if (options.projectId) {
                return _getImages({
                    "project": options.projectId,
                    "projectLocality": options.localityId,
                    "property": options.propertyIds
                }, {
                    "page": page,
                    "req": req,
                    "isPrimary": isPrimary || false
                });
            }
            break;
        case 'listing':
            if (options.listingId) {
                return _getImages({
                    "listing": options.listingId,
                    "project": options.projectId,
                    "property": options.propertyId
                },{
                    "mainImageId": options.mainImageId,
                    "page": page,
                    "req" : req,
                    "isPrimary": isPrimary || false
                });
            }
            break;
        case 'bank':
            if ((_.isArray(options.bankId) && options.bankId.length > 0) || (!_.isArray(options.bankId) && options.bankId)) {
                return _getImages({
                    "bank": options.bankId
                },{req});
            }
            break;
        case 'locality':
            if(options.localityId) {
                return _getImages({
                    "locality": options.localityId
                }, {req});
            }
            break;
    }
    return new Promise(function(resolve, reject) {
        reject(new Error("Provide Correct Parameters"));
    });
}

function generateRequest(options) {
    let requestedEntityType = [];
    _.forEach(options, (v, k) => {
        if(_.isArray(v) && !v.length){
            // skip for blank array
        }else if (v) {
            requestedEntityType.push({
                entityType: k,
                id: v // could be a single value or an array for same entityType
            });
        }
    });
    return requestedEntityType;
}

function _getImages(options, extra = {}) {
    let requestedEntityType = generateRequest(options);
    return _generateUrl(requestedEntityType, extra);
}

function _generateUrl(data, extra = {}) {
    if (!data || !data.length) {
        return new Promise(function(resolve, reject) {
            reject(new Error("Provide Relevant parameters in image service"));
        });
    }

    let filters, selector;

    /* create fiql selector start*/
    filters = getFiltersArray(data);
    selector = ApiService.createFiqlSelector({
        filters
    });
    /* create fiql selector end */

    let urlConfig = ApiConfig.getImage({
        query: {
            filters: selector.filters
        }
    });

    // return urlConfig;
    return ApiService.get(urlConfig, {req: extra.req, isPrimary: extra.isPrimary}).then((response) => {
        response = response && response.data;
        return _parseImages(response, extra);
    }, err => {
        throw err;
    });
}

function _removeSponsoredBuckets(buckets){
    _.forEach(buckets,(v)=>{
        let index = v.buckets.indexOf('sponsoredImage');
        if(index !== -1){
            v.buckets.splice(index,1);
        }
    });
}

function _updateBuckets(config, mainImageBucket){
    let buckets = [];
    if(config.page === 'listing'){
        buckets = _.cloneDeep(globalConfig.listingImageBucketMap);

        if(!mainImageBucket) {return buckets;}

        moveItemToTop(buckets, mainImageBucket, (currentItem, removeItem)=>{
            if(currentItem.buckets.indexOf(removeItem) !== -1){
                moveItemToTop(currentItem.buckets, removeItem, (ci, ri)=>{
                    return ci == ri;
                });
                return true;
            }
            return false;
        });

    }else if(config.page === 'project'){
        buckets = _.cloneDeep(globalConfig.projectImageBucketMap);
        _removeSponsoredBuckets(buckets);
        return buckets;
    }else{
        return buckets;
    }

    return buckets;
}

function _updateImages(images, mainImageDetails){
    //let mainImage;
    if(mainImageDetails.objectId && mainImageDetails.mainImageBucket){
        moveItemToTop(images[mainImageDetails.objectId][mainImageDetails.mainImageBucket], mainImageDetails.mainImageId, (currentItem, removeItem)=>{
            return currentItem.id == removeItem;
        });
    }
}

function _parseImages(response, extra) {
    if (_.isEmpty(response)) {
        return {};
    }
    let result = {
        count:{}
    }, mainImageDetails = {
        mainImageBucket : undefined,
        objectId: undefined,
        mainImageId: extra.mainImageId
    };
    _.forEach(response, image => {
        let objectType = image.imageType && image.imageType.objectType && image.imageType.objectType.type;
        result[image.objectId] = result[image.objectId] || {};
        result[image.objectId][image.imageType.type] = result[image.objectId][image.imageType.type] || [];
        result[image.objectId][image.imageType.type].push({
            category: image.imageType.type,
            url: image.absolutePath,
            title: image.title,
            alt: image.altText,
            createdAt: image.createdAt,
            takenAt: image.takenAt,
            id: image.id,
            jsonDump: image.jsonDump && JSON.parse(image.jsonDump),
            width: image.width,
            height: image.height
        });
        if(extra.mainImageId == image.id){
            mainImageDetails.mainImageBucket = image.imageType.type;
            mainImageDetails.objectId = image.objectId;
        }
        if(objectType){
            result['count'][objectType] = result['count'][objectType] !== undefined ? result['count'][objectType] + 1 : 0;
        }
    });

    result.buckets = _updateBuckets(extra, mainImageDetails.mainImageBucket);
    _updateImages(result, mainImageDetails);
    return result;
}

module.exports = {
    getImageDetails
};
