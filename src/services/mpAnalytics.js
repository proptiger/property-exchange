"use strict";
var mpAnalyticsModel = require('../models/mpAnalyticsModel'),
    logger = require('services/loggerService'),
    apiConfig = require('configs/apiConfig'),
    apiService = require('services/apiService'),
    mongoose = require('mongoose');


function _sendWebhook(filterType,data) {
      let urlConfig = apiConfig.jarvisFilterWebhook({
           type: filterType,
              query : {
                  sourceDomain: 'Makaan'
              }
      });
      let payload = {
          user : JSON.stringify({
               "$distinct_id": data.delivery_id,
               "$properties" : data
          })
      },
      req = {
          headers: {
              'Content-Type': 'application/x-www-form-urlencoded'
          }
      };

      apiService.post(urlConfig,{form:payload,req:req}).then((response) => {
          logger.info(`Sucessfully sent webhook  filter type ${filterType} ` +  JSON.stringify(response));
      }, err => {
          logger.error(`Error while sending webhook filter type ${filterType} ` + err);
      });
}


function _sendContentPyr(data) {
    var filter = {
            event_name: data.event_name,
            page_type: data.extra.page_type,
            visitor_id: data.visitor_id,
            notification_sent: null
    };
    mpAnalyticsModel.findOne(filter).exec().then( (response) => {
        if(response) {
                _sendWebhook(data.event_name,data);
                mpAnalyticsModel.remove(filter,() => {
                    data.notification_sent = true;
                    mpAnalyticsModel.update({visitor_id: data.visitor_id,page_type: data.extra.page_type},data,{upsert: true},() => {
                    });
                });
                 return true;
        }
        return false;
    }).then( (isSent) => {
        if(!isSent) {
            mpAnalyticsModel.findOne({visitor_id: data.visitor_id}).limit(1).sort({$natural:-1}).then ( (response) => {
                if(response && response.page_type && response.page_type !== data.extra.page_type ) {
                    _sendWebhook(data.event_name,data);
                }
            });
        }
    })
    .catch(function(err) {
        logger.error("Error inside promise of _sendContentPyr " + err);
    });
}

function _sendSerpScrollEvent(model) {
    var filtersCriteria = {
        bhk:{
            serp_filter_bhk: null,
            visitor_id: model.visitor_id,
            page_type: model.extra.page_type
        },
        budget: {
            serp_filter_bhk: true,
            serp_filter_budget: null,
            visitor_id: model.visitor_id,
            page_type: model.extra.page_type
        },
        property_type: {
            serp_filter_bhk: true,
            serp_filter_budget: true,
            serp_filter_property_type: null,
            visitor_id: model.visitor_id,
            page_type: model.extra.page_type
        }
    },
    webHookSent = false;
    mpAnalyticsModel.findOne(filtersCriteria.bhk).exec().then( (response) => {
        if(response) {
                _sendWebhook("bhk",model);
                webHookSent = true;
           }
          return webHookSent;
    }).
    then((webHookSent) => {
        if(!webHookSent) {
            mpAnalyticsModel.findOne(filtersCriteria.budget).exec().then((response) =>  {
                if(response) {
                     _sendWebhook("budget",model);
                     webHookSent = true;
                }
                return webHookSent;
            });
        }
    }).
    then((webHookSent) => {
        if(!webHookSent) {
            mpAnalyticsModel.findOne(filtersCriteria.property_type).exec().then((response) =>  {
                if(response) {
                     _sendWebhook("property_type",model);
                     webHookSent = true;
                }
                return webHookSent;
            });
        }
    })
    .catch(function(err) {
        logger.error("Error inside promise of _sendSerpScrollEvent " + err);
    });
}

module.exports.findAndUpdate = function(model,clearMetaData = false) {
    //check if mongoose instance is stil connected
    if (mongoose.connection.readyState === 0 ) {
        return new Promise(function(resolve, reject) {
                reject(new Error("db connection not ready.."));
        });
    }
    if(clearMetaData) {
        mpAnalyticsModel.remove({visitor_id: model.visitor_id,page_type: model.page_type || model.extra.page_type},() =>{
            return mpAnalyticsModel.update({visitor_id: model.visitor_id,page_type:model.page_type || model.extra.page_type},{visitor_id: model.visitor_id,page_type: model.page_type || model.extra.page_type},{upsert: true}).exec();
        });
    }
    return mpAnalyticsModel.update({visitor_id: model.visitor_id,page_type: model.page_type || model.extra.page_type},model,{upsert: true}).exec();
};

module.exports.sendFiltersSuggestion = function(model) {
    switch(model.event_name) {
        case "serp_scroll"    : _sendSerpScrollEvent(model);break;
        case 'enquiry_dropped':
        case 'bj_sitevisit'   :
        case 'bj_register'    :
        case 'bj_book'        :
        case 'child_serp'     :
        case 'bj_posses'      : _sendWebhook(model.event_name,model);break;
        case 'content_pyr'    : _sendContentPyr(model);
    }
};
