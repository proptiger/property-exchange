"use strict";

const apiService = require('services/apiService'),
      apiConfig = require('configs/apiConfig');

var homeloanService = {};

homeloanService.postHomeloanAgentDetails = function(payload, {req}){
    let api = apiConfig.postHomeloanAgentDetails();
    return apiService.post(api, {
        body: payload,
        req: req
    });
};

module.exports = homeloanService;
