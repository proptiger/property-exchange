'use strict';

let apiConfig = require("configs/apiConfig"),
	apiService = require("services/apiService"),
	mappingService = require("services/mappingService"),
	logger = require("services/loggerService"),
	buyerOptInService = require('services/buyerOptInService');

let enquiry = {};

const patterns = {
	NAME: /(name_[0-9]{10})/,
	EMAIL: /(email_[0-9]{10}@email.com)/
};

const allowedFieldsInKira = ["cityId", "minBudget", "maxBudget", "projectId", "bhk", "sendOtp", "listingId", "domainId", "applicationType", "pageName", "pageUrl", "name", "email", "phone", "cityName", "salesType", "countryId", "optIn", "userMedium", "userFrom", "userNetwork", "localityId", "socketId", "moduleId", "userIp"];

function _parseDataForInstantCallback(data, rawData){
	let callingPayload = {
		fromNo: data.phone,
		listingCategory: data.salesType,
		cityId: data.cityId,
		toNo: rawData.companyPhone,
		clientCountryId: rawData.countryId,
		userId: rawData.companyUserId,
		jsonDump: JSON.stringify(rawData, allowedFieldsInKira)
	};
	return callingPayload;
}

function _insertKeyForViewFeedback(obj){
	let api = apiConfig.feedbackToChat(),
        payload = {
            feedbackKey: obj.buyerId + ":" + obj.sellerId ,
            deliveryId: obj.deliveryId
        };
    apiService.post(api, {
        body: payload
    }).then(()=>{
        logger.info('view phone event posted');
    },()=>{
        logger.error('Error: While posting event for view phone');
    });
}

function _postEnquiry(api, data, {req, res, isVerified, cookieData}){
	logger.info("payload--:", JSON.stringify({data, api}));
	return apiService.post(api, {body: data, req, res}).then((response) => {
		buyerOptInService.sendOptInStatus(req.body);
		if(data.enquiryType && data.enquiryType.id == mappingService.enquiryTypeMapping["VIEW_PHONE"] && isVerified){
			let obj = {
				buyerId: response && response.data && response.data.userId,
				sellerId: req.body.companyUserId,
				enquiryType: req.body.enquiryType,
				deliveryId: req.body.deliveryId,
				enquiryId: response && response.data && response.data.id
			};
			_insertKeyForViewFeedback(obj);
		}
		if(cookieData){
			res.cookie("user_enquiry_info", JSON.stringify(cookieData), {
				encode: String
			});
		}
		if(req.isAmp && req.headers["user-agent"].indexOf("OS X")>-1) {
			//Removing Third Party Cookies for Google-Cached Amp Pages
			res.removeHeader('Set-Cookie');
		}
		return response;
	});
}

function _checkDummyData(data, type){
	let exp = patterns[type];
	if(exp && exp.test(data)){
		return "";
	}
	return data;
}

enquiry.postTempEnquiry = function(data, req, res){
	let api = apiConfig.tempEnquiries();
	let cookieData = {
		name: _checkDummyData(data.name, "NAME"),
		email: _checkDummyData(data.email, "EMAIL"),
		phone: data.phone
	};
	return _postEnquiry(api, data, {req, res, cookieData}).then((response) => {
		response.data.isVerificationRequired = true;
		return response;
	});
};

enquiry.postEnquiry = function(data, req, res){
	let api = apiConfig.enquiries();
	if(data.enquiryType.id == mappingService.enquiryTypeMapping["GET_INSTANT_CALLBACK"]){
		api = apiConfig.callNow();
		data = _parseDataForInstantCallback(data, req.body);
	}
	return _postEnquiry(api, data, {req, res, isVerified: true});
};

enquiry.resendAndVerifyOtp = function({putData, enquiryId, otp}, req, res){
	let api = apiConfig.tempEnquiries({enquiryId, 
		query: {
			otp
		}});
	return apiService.put(api, {body: putData, req, res});
};

enquiry.sendOtpOnCall = function(data, req){
	return apiService.get(apiConfig.sendOtpOnCall({
	    userId: data.userId,
	    userNumber: data.phone
	}), {req});
};

enquiry.getLeadDetails = function(options,req){
	let selector = ''
	if(options){
		selector = `fields=${options.fields}&filters=${options.filters}` 
	}
	let url = apiConfig.clientLeadsBySelectors(selector).url;
	return apiService.get(url, {req}).then(function(response) {
        return response;
    }, err => {
        throw err;
    });
}
module.exports = enquiry;