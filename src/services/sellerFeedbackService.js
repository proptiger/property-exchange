"use strict";

//const   GlobalConfig 	= require('configs/globalConfig'),
		const ApiService	 	= require('services/apiService'),
		PropertyService = require('services/propertyService'),
		Logger 			= require('services/loggerService'),
		UtilService		= require('services/utilService'),
		ApiConfig	 	= require('configs/apiConfig'),
	    AWS 			= require('aws-sdk'),
        Consumer 	 	= require('sqs-consumer'),
        smsService      = require('modules/apis/apiParsers/smsService'),
        agentService    = require('services/agentService');

    AWS.config.update({
		region: process.env.AWS_CALL_QUEUE_REGION,
		accessKeyId: process.env.AWS_CALL_QUEUE_ACCESS_KEY,
		secretAccessKey: process.env.AWS_CALL_QUEUE_SECRET_KEY
	});

    //var connection;
    var sellerFeedbackService = {};
    var sqs_consumer = _startListeningSQS();
    var nodeEnvironment = process.env.NODE_ENV || 'development';

    sqs_consumer && sqs_consumer.on('error', function (err) {
        if(!(nodeEnvironment === 'development' || nodeEnvironment === 'localhost')) {
            Logger.error(err.message);
        }
    });   //jshint ignore:line

    function _startListeningSQS(){
    	let app;
    	try{
    		app = Consumer.create({
				queueUrl: process.env.SQS_URL,
  				batchSize: 1,
				handleMessage: _processMessage,
				sqs: new AWS.SQS()
			});

    		app.start();
    	} catch(ex){
    		Logger.info(ex);
    	}
    	return app;
    }

    function _getSocketIdIfMapped({buyerId,sellerId}) {
        if(!buyerId || !sellerId || isNaN(buyerId) || isNaN(sellerId)) {
            return new Promise((resolve, reject) => {
                    let message = "buyerId or sellerId missing";
                    reject(error(message));
            });
        }
        Logger.info("Retreiving any socketId mapped for this buyerId/sellerId combination" , buyerId ,sellerId);
        let api = ApiConfig.feedbackToChat({
            query: {
                feedbackKey: buyerId + ":" + sellerId
            }
        });
        return  ApiService.get(api).then((response) => {
            response = response && response.data;
            if(response && !UtilService.isEmptyObject(response)) {
                let _sockets = Object.keys(response);
                return _sockets[0];
            }
            return;
        },(err)=> {
            Logger.error(err);
        });
    }


    function error(message){
    	message = message || "some error occured";
    	return new Error(message);
    }

    // function getSellerFeedbackSMSTemplate(seller_name,short_url) {
    //     return `Tell us how was your experience with ${seller_name}, it helps us ensure you see the highest rated sellers! To rate, visit: ${short_url}`;
    // }

    // sellerFeedbackService.sendSMSToClient = function(data) {
    //     if (!data || !data.phone || !data.companyName || !data.callId) {
    //         Logger.info("Error while sending feedback message",data);
    //         return new Promise((resolve, reject)=> {
    //             let message = "Error while sending feedback message : payload incorrect";
    //             reject(error(message));
    //         });
    //     }
    //     let phone = data.phone,
    //         pageUrl = data.pageUrl || "",
    //         companyName = data.companyName;
    //     pageUrl = process.env.BASE_URL +  pageUrl;
    //     pageUrl = decodeURIComponent(pageUrl);
    //     pageUrl += ((pageUrl.indexOf("?") === -1 ) ? '?': '&') + UtilService.getFeedbackSMSUrlQueryKey() +  '=' + data.callId;
    //     return ApiService.post(ApiConfig.getShortUrl(), {
    //         body: {longUrl: pageUrl},
    //         baseURL: "https://www.googleapis.com/",
    //         req: {
    //             headers: {
    //                 referer: "crm.proptiger-ws.com"
    //             }
    //         }
    //     }).then((response) => {
    //         if (response.id) {
    //             let smsString = getSellerFeedbackSMSTemplate(companyName, response.id);
    //             smsService.sendSMS(phone, smsString).then(() => {
    //                 Logger.info("Successfully sent feedback message to phone: " + phone);
    //                 return {messageSent: true};
    //             }, () => {
    //                 Logger.info("Error while sending feedback message to phone: " + phone);
    //                 return {messageSent: false};
    //             });
    //         } else {
    //             Logger.info("Error while generating short url for " + JSON.stringify(data));
    //             return {messageSent: false};
    //         }
    //     }, () => {
    //         Logger.info("Error while generating short url for " + JSON.stringify(data));
    //         return {messageSent: false};
    //     });
    // };

    // function _sendFeedbackSMS(data) {
    //     if(!data.sellerId) {
    //          Logger.error("No seller Id passed : Feedbacksms",data);
    //     }
    //     agentService.getSellerDetailsByUserId(data.sellerId, {req: {}}).then((response) => {
    //               if(response && response.company && response.company.name) {
    //                 data.companyName = response.company.name;
    //                 sellerFeedbackService.sendSMSToClient(data);
    //               }
    //         },() => {
    //             Logger.error("Error while fetching seller details : Feedbacksms");
    //     });
    // }

    sellerFeedbackService.getFeedbackEventData = function(data, allDetails = false) {
        let fields = ["id","profilePictureURL", "companySeller", "name", "user", "company", "mainImageURL", "property", "unitType", "bedrooms", "currentListingPrice", "price"];
        let listingId = data.listingId;
        let socketId = data.socketId;
        let sellerId = data.sellerId;
        let callDuration = UtilService.convertTimeToSeconds(data.callDuration) || 0;
        let event = data.event;
        let eventName;

        let postData = {
            sessionId: socketId,
            callId: data.callId,
			cityId: data.cityId,
			callerUserId: data.callerUserId,
            id: data.moduleId,
            phone: data.phone,
            pageUrl: data.pageUrl,
            salesType: data.salesType,
            listingId: listingId,
            projectId: data.projectId,
            companyId: data.companyId
        };

        //temporary code
        postData.environment = nodeEnvironment;
        postData.queueURL = process.env.AWS_CALL_QUEUE_REGION;

        if(data.callStatus) {
            postData.callStatus = data.callStatus;
        }

        if (socketId && event) {
            postData.eventName = event;
            return new Promise((resolve) => {
                resolve(postData);
            });
        } else if (listingId && socketId && callDuration >= 10 && allDetails) {
            return PropertyService.getListingDetail(listingId, {
                req: {},
                fields
            }).then((response) => {
                response = response && response.data;
                if (!response) {
                    throw error("data null in listing");
                }
                postData.eventName = "open_feedback";
                let property = response.property || {};
                postData.listingId = listingId;
                postData.companyId = response.companySeller && response.companySeller.company && response.companySeller.company.id;
                postData.companyUserId = data.sellerId;
                postData.companyImage = response.companySeller && response.companySeller.user && response.companySeller.user.profilePictureURL;
                postData.companyName = response.companySeller && response.companySeller.company && response.companySeller.company.name;
                postData.beds = property.bedrooms;
                postData.image = response.mainImageURL;
                postData.propertyType = property.unitType;
                postData.price = response.currentListingPrice && response.currentListingPrice.price;
                return postData;
            }, () => {
                throw error("Listing API Error");
            });
        } else if(sellerId && data.callId && callDuration >= 10 && allDetails) {
            return agentService.getSellerDetailsByUserId(sellerId, {req: {}}).then((response) => {
                  if(response && response.company && response.company.name) {
                    postData.companyName = response.company.name;
                    postData.companyId = response.company.id;
                    postData.companyUserId = sellerId;
                    return postData;
                  }
            },() => {
                throw error("getSellerDetailsByUserId API Error");
            });
        } else if (!allDetails) {
            let onSuccess = function() {
                if (callDuration && callDuration >= 10) {
                    eventName = "call_feedback";
                } else {
                    eventName = "call_ended";
                }
                postData.eventName = eventName;
                return new Promise((resolve) => {
                    resolve(postData);
                });
            };
            let onError = function() {
                return new Promise((resolve, reject) => {
                    reject(new Error("no socket id found to post feedback/call_ended event"));
                });
            };
            if(socketId) {
                 return onSuccess();
                 
            } else {
                return _getSocketIdIfMapped({
                    sellerId: data.sellerId,
                    buyerId: data.callerUserId
                }).then((socketId) => {
                    if(!socketId) {
                        return onError();
                    }
                    postData.sessionId = socketId;
                    return onSuccess();
                },() => {
                    return onError();
                });
            }
        } else {
            return new Promise((resolve, reject) => {
                let message = "";
                if(!listingId){
                    message = " no listing available in call id: "+data.callId;
                }
                if (!socketId) {
                    message += " no user available in call id: " + data.callId;
                }
                if (!event) {
                    message += " call event not available in call id: " + data.callId;
                }
                if (!callDuration) {
                    message += " call duration is less in call id: " + data.callId;
                }
                reject(error(message));
            });
        }
    };

    function _sendEventToClient(messageDetails, done){
    	let api = ApiConfig.postChatEvent(),
            jsonDump = messageDetails.jsonDump;
        if(!jsonDump && messageDetails.callLog && messageDetails.callLog.communicationLog && messageDetails.callLog.communicationLog.extraData) {
            jsonDump = JSON.parse(messageDetails.callLog.communicationLog.extraData);
        }
    	let processedData = {
    		listingId: 		jsonDump && jsonDump.listingId,
            companyId:      jsonDump && jsonDump.companyId,
            projectId:      jsonDump && jsonDump.projectId,
    		socketId: 		jsonDump && jsonDump.socketId,
            moduleId:       jsonDump && jsonDump.moduleId,
    		sellerId: 		messageDetails.sellerId,
    		callId: 		messageDetails.callLogId,
    		callDuration: 	messageDetails.callDuration,
            phone:          messageDetails.clientPhone || (jsonDump && jsonDump.phone ) ,
            pageUrl:        jsonDump && jsonDump.pageUrl,
            event:          messageDetails.event,
            callerUserId:   messageDetails.callerUserId,
            callStatus:     messageDetails.callLog && messageDetails.callLog.callStatus,
            salesType:      messageDetails.saleType || (jsonDump && jsonDump.salesType)
    	};


    	sellerFeedbackService.getFeedbackEventData(processedData).then((response)=>{
            Logger.info("CALL_EVENT == ",response.eventName);
    		if(response){
    			Logger.info('Sending Event For Call Id '+ messageDetails.callLogId);
	    	 	ApiService.post(api, {
		            body: response
		        }).then(()=>{
                    Logger.info('Call Id '+ messageDetails.callLogId + ' processed');
	    			_done(done,undefined,processedData);
	    		},(err)=>{
	    			Logger.info('Error: While Sending Event: '+err.message);
	    			_done(done,err);
	    		});
    		}
    	},(err)=>{
    		Logger.info('Error: While Getting Listing Data: '+err.message);
    		_done(done,undefined,processedData);
    	});
    }

    function _done(done,err,processedData){
        if(err){
            done(err);
        } else {
            done();
            // if(processedData && !UtilService.isEmptyObject(processedData)) {
            //     /* send SMS Feedback to client when call duration >= 10 */
            //     let callDuration = UtilService.convertTimeToSeconds(processedData.callDuration) || 0;
            //     if (processedData.callId && callDuration && callDuration >= 10 && processedData.sellerId) {
            //         _sendFeedbackSMS(processedData);
            //     }
            // }
        }
    }

    function _processMessage(message, done){
    	try {
	    	let messageDetails = JSON.parse(message.Body);
	    	messageDetails = JSON.parse(messageDetails.Message);
            Logger.info("SQS MESSAGE :---- " +  JSON.stringify(messageDetails));
	    	_sendEventToClient(messageDetails, done);
	    } catch(ex){
	    	done(error("error while parsing message"));
	    }
    }

	sellerFeedbackService.getCallDetails = function(callId){
		let urlConfig = ApiConfig.getCallDetails(callId);
		return ApiService.get(urlConfig,{ignoreReq: true}).then((response)=>{
			response = response && response.data && response.data[0];
			response = response || {};
			let extraData = (response.communicationLog && response.communicationLog.extraData && JSON.parse(response.communicationLog.extraData));
			let details = {
				callId 			: response.id,
				sellerId		: response.communicationLog && response.communicationLog.virtualNumberMapping && response.communicationLog.virtualNumberMapping.userId,
				cityId			: response.communicationLog && response.communicationLog.virtualNumberMapping && response.communicationLog.virtualNumberMapping.cityId,
                callerUserId    : response.communicationLog && response.communicationLog.callerUserId,
				phone			: response.communicationLog && response.communicationLog.callerNumber,
                salesType       : response.communicationLog && response.communicationLog.virtualNumberMapping && response.communicationLog.virtualNumberMapping.listingCategory,
				callStatus		: response.callStatus,
				callDuration	: response.callDuration
			};
			if(extraData){
				details.extraData = extraData;
			}

			return details;
		},(err)=>{
			throw err;
		});
	};


	module.exports =  sellerFeedbackService;
