'use strict';

const apiService = require('services/apiService');

var expressApp;

module.exports.setup = function(app) {
    expressApp = app;
    const MICRO_SERICES = ['madelyne', 'compass', 'pixie', 'petra', 'columbus', 'madrox', 'dawnstar', 'sapphire', 'kira', 'zenithar', 'api', 'cyclops','plutus','themis','jean', 'mystique','flash', 'publish_event'];
    for (let i = 0; i < MICRO_SERICES.length; i++) {
        app.get(`/${MICRO_SERICES[i]}/*`, makeGetToAPI);
        app.post(`/${MICRO_SERICES[i]}/*`, makePostToAPI);
        app.put(`/${MICRO_SERICES[i]}/*`, makePutToAPI);
        app.patch(`/${MICRO_SERICES[i]}/*`, makePatchToAPI);
    }

    // app.get('/dal/*', (req, res) => {
    //     req.baseUrl = process.env.BASE_URL_ALLIANCES;
    //     return makeGetToAPI(req, res);
    // });
    app.get('/xhr/*', (req, res) => {
        req.baseUrl = process.env.SELLER_URL;
        return makeGetToAPI(req, res);
    });
    app.post('/xhr/*', (req, res) => {
        req.baseUrl = process.env.SELLER_URL;
        return makePostToAPI(req, res);
    });
    app.put('/xhr/*', (req, res) => {
        req.baseUrl = process.env.SELLER_URL;
        return makePutToAPI(req, res);
    });
};

/**
 * calls original tracker-cookies.php and sets cookie to the response
 * @param  {Object}   req  [node request object]
 * @param  {Object}   res  [node response object]
 * @param  {Function} next [middleware function to call next route]
 */
function makeGetToAPI(req, res) {
    var url = decodeURIComponent(req.originalUrl);

    var options = {
        url: url,
        req: req,
        res: res
    };

    if (req.baseUrl) {
        options.baseUrl = req.baseUrl;
    }

    return apiService.get(options).then(function(response) {
        res.status(200).send(response);
    }, function(e) {
        res.status(e.status || 500).send(e.body);
    });
}

function makePostToAPI(req, res) {
    return callAPI('post', req, res);
}

function makePutToAPI(req, res) {
    return callAPI('put', req, res);
}

function makePatchToAPI(req, res) {
    return callAPI('patch', req, res);
}

function callAPI(type, req, res) {
    var url = req.originalUrl;
    var options = {
        url: url,
        req: req,
        res: res
    };

    if (req.baseUrl) {
        options.baseUrl = req.baseUrl;
    }

    if (req.headers['content-type']) {
        if (req.headers['content-type'].match('application/x-www-form-urlencoded')) {
            options.form = req.body;
        } else {
            options.body = req.body;
        }
    }

    return apiService[type](options).then(function(response) {
        res.status(200).send(response);
    }, function(e) {
        res.status(e.status || 500).send(e.body);
    });
}
