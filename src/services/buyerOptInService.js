'use strict';
let apiService = require('services/apiService');
let utilService = require('services/utilService');
let _ = utilService._;
let apiConfig = require('configs/apiConfig');

function _getDummyEmail(phone) {
    return `email_${phone}@email.com`;
}


function _getSalesTypeId(type){
	if(type == "buy" || type == "Primary" || type == "Resale"){
		return 1;
	} else {
		return 5;
	}
}

function _getKeyValue(value, obj) {
    if (obj.allowedValues.indexOf(value) > -1) {
        return value;
    } else {
        return obj.defaultValue;
    }
}

function _getDumpData(data){
	let map = [{
		key: "optIn",
		apiKey: "opt-in",
		value: {
			allowedValues: [true, false],
			defaultValue: true
		}
	}];
	return map.map((v) => {
		return {
			name: v.apiKey,
			value: _getKeyValue(data[v.key], v.value)
		};
	});
}

function _getFinalPayload(payload, companyIds) {
    let arr = [];
    for (var key in companyIds) {
        let obj = _.cloneDeep(payload);
        obj.companyId = companyIds[key];
        arr.push(obj);
    }
    return arr;
}

module.exports.sendOptInStatus = function(data){
	let optInApi = apiConfig.buyerOptInStatus();
	let optInPayload = {
		contactNumber: data.phone,
		userEmail: data.email || _getDummyEmail(data.phone),
		cityId: data.cityId,
		salesTypeId: _getSalesTypeId(data.salesType),
		jsonDump: JSON.stringify(_getDumpData(data))
	};
	let finalPayload = _getFinalPayload(optInPayload, data.multipleCompanyIds);
	return apiService.post(optInApi, {
		body: finalPayload,
		ignoreReq: true
	});
};
