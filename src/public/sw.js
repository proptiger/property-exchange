// Chrome's currently missing some useful cache methods,

"use strict";

importScripts('bower_components/sw-toolbox/sw-toolbox.js');
importScripts('sw-offline-google-analytics.js');


var precacheConfigArray = ["/offline-page", "/scripts/app.js","/scripts/infra.js","/styles/css/vendor_mobile.css","/styles/css/offlinepage.css"];

/* versioning of files need to be updated at the time of build */
var PrecacheList = [['/offline-page', 'mknOffline-11'],['/scripts/app.js', 'mknApp-1'],['/scripts/infra.js', 'mknInfra-1'],['/styles/css/vendor_mobile.css','mknVendorCss-1'],['/styles/css/offlinepage.css', 'mknOfflinepageCss-1']];

toolbox.options = {
    debug: true
};

var allCacheNames = [],
    cityNameRegex = '[^/]+',
    localityNameRegex = '[^/]+',
    landmarkNameRegex = '[^/]+',
    builderNameRegex = '([^/]+)',
    someString = '[^/]+',
    hyphen  = "-", slash = "/",
    maps = 'maps';

var toolboxRouteConfigArray = [
    {
        url: new RegExp('/images/(.*).svg'),
        origin: 'CSS_CDN',
        type: 'cacheFirst',
        cache: {
            name : 'mknStaticSvg-2',
            maxAgeSeconds:604800
        }
    },{
        url: new RegExp('/images/(.*)'),
        origin: 'JS_CDN',
        type: 'cacheFirst',
        cache: {
            name : 'mknStaticImg-2',
            maxAgeSeconds:604800
        }
    },{
        url: new RegExp('/usr/img/(.*)'),
        origin: 'JARVIS_URL',
        type: 'cacheFirst',
        cache: {
            name : 'mknJarvisImg-2',
            maxEntries: 10
        }
    },{
        url: new RegExp('(.*).(jpeg|jpg|png)'),
        origin: 'CONTENT_CDN',
        type: 'cacheFirst',
        cache: {
            name : 'mknContentImg-3',
            maxAgeSeconds:604800
        }
    },{
        url: new RegExp('/fonts/(.*)'),
        origin: 'CSS_CDN',
        type: 'cacheFirst',
        cache: {
            name : 'mknFonts-2',
            maxEntries: 10,
            maxAgeSeconds: 604800
        }
    },{
        url: new RegExp('/styles/css/vendor-v2(.*).css'),
        origin: 'CSS_CDN',
        type: 'cacheFirst',
        cache: {
            name : 'mknVendorV2Css-1',
            maxEntries: 1
        }
     },{
        url: new RegExp('/styles/css/home-mobile(.*).css'),
        origin: 'CSS_CDN',
        type: 'cacheFirst',
        cache: {
            name : 'mknHomepageCss-4',
            maxEntries: 1
        }
    },{
        url: new RegExp('/scripts/pageModules/homeController(.*).js'),
        origin: 'JS_CDN',
        type: 'cacheFirst',
        cache: {
            name : 'mknHomepageContrl-2',
            maxEntries: 1
        }
    },{
        url: new RegExp('/styles/css/serp(.*).css'),
        origin: 'CSS_CDN',
        type: 'cacheFirst',
        cache: {
            name : 'mknSerpCss-3',
            maxEntries: 1
        }
    },{
        url: new RegExp('/scripts/pageModules/serpPage(.*).js'),
        origin: 'JS_CDN',
        type: 'cacheFirst',
        cache: {
            name : 'mknSerpCtrl-2',
            maxEntries: 1
        }
    },{
        url: new RegExp('/modules/pyr/scripts/index(.*).js'),
        origin: 'JS_CDN',
        type: 'cacheFirst',
        cache: {
            name : 'mknPyrModule-2',
            maxEntries: 1
        }
    },{
        url: new RegExp('/modules/gallery/scripts/index(.*).js'),
        origin: 'JS_CDN',
        type: 'cacheFirst',
        cache: {
            name : 'mknGalleryModule-2',
            maxEntries: 1
        }
    },{
        url: new RegExp('/modules/newLoginRegister/scripts/index(.*).js'),
        origin: 'JS_CDN',
        type: 'cacheFirst',
        cache: {
            name : 'mknLoginModule-2',
            maxEntries: 1
        }
    },{
        url: new RegExp('/styles/css/property.((.)+).css'),
        origin: 'CSS_CDN',
        type: 'cacheFirst',
        cache: {
            name : 'mknPropCss-2',
            maxEntries: 1
        }
    },{
        url: new RegExp('/scripts/pageModules/propertyPage(.*).js'),
        origin: 'JS_CDN',
        type: 'cacheFirst',
        cache: {
            name : 'mknPropCtrl-2',
            maxEntries: 1
        }
    },{
        url: new RegExp('/styles/css/project.((.)+).css'),
        origin: 'CSS_CDN',
        type: 'cacheFirst',
        cache: {
            name : 'mknProjCss-2',
            maxEntries: 1
        }
    },{
        url: new RegExp('/scripts/pageModules/projectPage(.*).js'),
        origin: 'JS_CDN',
        type: 'cacheFirst',
        cache: {
            name : 'mknProjCtrl-2',
            maxEntries: 1
        }
    },{
        url: new RegExp('/styles/css/propProjCommon.((.)+).css'),
        origin: 'CSS_CDN',
        type: 'cacheFirst',
        cache: {
            name : 'mknPropProjCommonCss-2',
            maxEntries: 1
        }
    },{
        url: new RegExp('/scripts/dependency/projectPropertyCommon(.*).js'),
        origin: 'JS_CDN',
        type: 'cacheFirst',
        cache: {
            name : 'mknProjPropCommonJs-2',
            maxEntries: 1
        }
    },{
        url: new RegExp('/modules/chart/scripts/index(.*).js'),
        origin: 'JS_CDN',
        type: 'cacheFirst',
        cache: {
            name : 'mknChartModule-2',
            maxEntries: 1
        }
    },{
        url: new RegExp('/user/css/app-((.)+).min.css'),
        origin: 'JARVIS_URL',
        type: 'networkFirst',
        cache: {
            name: 'mknChatCss-2',
            maxEntries : 1
        }
    },{
        url: new RegExp('/user/app((.)*).min.js'),
        origin: 'JARVIS_URL',
        type: 'networkFirst',
        cache: {
            name: 'mknChatAppJS-2',
            maxEntries : 1
        }
    },{
        url: new RegExp('/user/mchat-((.)+).min.js'),
        origin: 'JARVIS_URL',
        type: 'networkFirst',
        cache: {
            name: 'mknChatJs-2',
            maxEntries : 1
        }
    },{
        url: new RegExp('/xhr/userService/checkLoggedIn(.*)'),
        type: 'networkOnly'
    },{
        url: new RegExp('/xhr/userService/userLoginStatus(.*)'),
        type: 'networkOnly'
    },{
        url: new RegExp('/petra/app/v1/mylocation(.*)'),
        type: 'cacheFirst',
        cache: {
            name: 'mknUserLocation-3',
            maxAgeSeconds:86400
        }
    },{
        url: new RegExp('/petra/data/v3/entity/locality/top(.*)'),
        type: 'cacheFirst',
        cache: {
            name: 'mknUserTopLocalities-2',
            maxAgeSeconds:604800
        }
    },{
        url: new RegExp('/apis/city/top-seller/(.)+'),
        type: 'cacheFirst',
        cache: {
            name: 'mknSerpTopSellers-2',
            maxEntries: 5
        }
    },{
        url: new RegExp('/petra/data/v1/entity/country(.*)'),
        type: 'cacheFirst',
        cache: {
            name: 'mknCountries-2',
            maxEntries: 2
        }
    },{
        url: new RegExp('/columbus/app/((.)+){2}/typeahead(.*)'),
        type: 'cacheFirst',
        cache: {
            name: 'mknSearchResults-2',
            maxEntries : 10,
            maxAgeSeconds: 86400
        }
    },{
        url: new RegExp('/apis/imageService/listing/(.*)'), // for serp gallery images
        type: 'cacheFirst',
        cache: {
            name: 'mknSerpGalleryImg-1',
            maxAgeSeconds: 86400
        }
    },{
        url: new RegExp('/apis/locality/price-trend/(.*)'), // for project overview
        type: 'cacheFirst',
        cache: {
            name: 'mknLocPrcTrnd-1',
            maxAgeSeconds: 86400
        }
    },{
        url: new RegExp('/apis/project/price-trend/(.*)'), // for project overview
        type: 'cacheFirst',
        cache: {
            name: 'mknProjectPrcTrnd-1',
            maxAgeSeconds: 86400
        }
    },{
        url: new RegExp('/apis/getAmenities?(.*)'), // for project & property overview
        type: 'cacheFirst',
        cache: {
            name: 'mknProjPropNeigh-1',
            maxAgeSeconds: 604800
        }
    },{
        url: new RegExp('/bower_components/(.*)'),
        type: 'networkOnly'
    },{
        url: '/', // homepage
        type: 'fastest',
        fallback: true,
        cache: {
            name: 'makaan-4'
        }
    }, {
        url: new RegExp(maps+slash), // maps serp
        type: 'networkFirst',
        fallback: true,
        cache: {
            name: 'makaanMaps-1',
            maxEntries: 1
        }
    }, {
        url: new RegExp(slash+cityNameRegex+hyphen+'residential-property'+slash), // city serp
        type: 'fastest',
        fallback: true,
        cache: {
            name: 'mknSerpCity-2'
        }
    },{
        url: new RegExp(slash+cityNameRegex+hyphen+'property'+slash), // locality serp
        type: 'fastest',
        fallback: true,
        cache: {
            name: 'mknSerpLoc-2'
        }
    },{
        url: new RegExp(slash+cityNameRegex+slash+'property-for-(buy|rent)-in-(.*)'), // project serp
        type: 'fastest',
        fallback: true,
        cache: {
            name: 'mknSerpProj-3'
        }
    },{
        url: new RegExp(slash+'(buy|rent)'+slash+'nearby'+slash), // nearby serp
        type: 'fastest',
        fallback: true,
        cache: {
            name: 'mknSerpLand-3'
        }
    },{
        url: new RegExp(slash+cityNameRegex+slash+someString+'in'+someString+hyphen+'[0-9]{4,9}'+slash+someString), // property overview
        type: 'fastest',
        fallback: true,
        cache: {
            name: 'mknPropertyOvrvw-2'
        }
    },{
        url: new RegExp(slash+cityNameRegex+slash+someString+'in'+someString+hyphen+'[0-9]{4,9}'), // project overview
        type: 'fastest',
        fallback: true,
        cache: {
            name: 'mknProjectOvrvw-2'
        }
    }
];

var fallbackFunc = function(){
    return caches.match('/offline-page').then(function(response){
        return response;
    });
}

var callbackFunc  = function(routeType){
    return function(request, values, options){
                return toolbox[routeType](request, values, options).then(function(response){
                    if(!response){
                        return fallbackFunc();
                    }
                    return response;
                }, function(){
                    return fallbackFunc();
                });
            };
}

var CDN_MAP = {
   JS_CDN : 'CDN_JS_PATH',
   CSS_CDN : 'CDN_CSS_PATH',
   CONTENT_CDN: 'https://content.makaan(-ws)?.com',
   JARVIS_URL: 'CDN_JARVIS_PATH'
}

for(var i=0; i<toolboxRouteConfigArray.length; i++){

   var routeObj = toolboxRouteConfigArray[i], urlCallback,
       options, origin = CDN_MAP[routeObj.origin] !== 'null' && CDN_MAP[routeObj.origin] || null;
       options = {
           origin : origin,
           successResponses: /^0|([123]\d\d)$/ // to cache only successful responses
       };

   if(routeObj.cache && routeObj.cache.name){
       options.cache = routeObj.cache;
       allCacheNames.push(routeObj.cache.name); // push cacheNames
   }

   var urlCallback = routeObj.fallback ? callbackFunc(routeObj.type) : toolbox[routeObj.type];
   toolbox.router.get(routeObj.url, urlCallback, options);
}

var addDirectoryIndex = function (originalUrl, index) {
    var url = new URL(originalUrl);
    if (url.pathname.slice(-1) === '/') {
        url.pathname += index;
    }
    return url.toString();
};

var getCacheBustedUrl = function (url) {
    var urlWithCacheBusting = new URL(url);
    return urlWithCacheBusting.toString();
};

var stripIgnoredUrlParameters = function (originalUrl, ignoreUrlParametersMatching) {
    var url = new URL(originalUrl);

    url.search = url.search.slice(1) // Exclude initial '?'
      .split('&') // Split into an array of 'key=value' strings
      .map(function(kv) {
        return kv.split('='); // Split each 'key=value' string into a [key, value] array
      })
      .filter(function(kv) {
        return ignoreUrlParametersMatching.every(function(ignoredRegex) {
          return !ignoredRegex.test(kv[0]); // Return true iff the key doesn't match any of the regexes.
        });
      })
      .map(function(kv) {
        return kv.join('='); // Join each [key, value] array into a 'key=value' string
      })
      .join('&'); // Join the array of 'key=value' strings into a string with '&' in between each

    return url.toString();
};

var _generatePrecacheName = function(cacheName, cacheUrl){
    var updatedName = cacheName;
    var temp = cacheUrl.split('.');
    if(temp && temp.length > 2){
        updatedName += temp[temp.length-2];
    }
    return updatedName;
}

var generateCacheMappings = function (precacheConfig, baseUrl) {
    var absoluteUrlToCacheName = {};
    var currentCacheNamesToAbsoluteUrl = {};

    precacheConfig.forEach(function(cacheOption) {
        var cacheName = _generatePrecacheName(cacheOption[1], cacheOption[0]);
        var absoluteUrl = new URL(cacheOption[0], baseUrl).toString();
        allCacheNames.push(cacheName); // push cacheName

        currentCacheNamesToAbsoluteUrl[cacheName] = absoluteUrl;
        absoluteUrlToCacheName[absoluteUrl] = cacheName;
    });

    return {
        absoluteUrlToCacheName: absoluteUrlToCacheName,
        currentCacheNamesToAbsoluteUrl: currentCacheNamesToAbsoluteUrl
    };
};

//var IgnoreUrlParametersMatching = [/^utm_/];
var IgnoreUrlParametersMatching = [];

var mappings = generateCacheMappings(PrecacheList, self.location);
var AbsoluteUrlToCacheName = mappings.absoluteUrlToCacheName;
var cacheNameToAbsoluteUrl = mappings.currentCacheNamesToAbsoluteUrl;


// Here comes the install event!
self.addEventListener('install', function(event) {

    event.waitUntil(
        caches.keys().then(function(cacheNames) {
            return Promise.all(
                Object.keys(cacheNameToAbsoluteUrl).filter(function(cacheName) {
                    return cacheNames.indexOf(cacheName) === -1;
                }).map(function(cacheName) {
                    var urlWithCacheBusting = getCacheBustedUrl(cacheNameToAbsoluteUrl[cacheName]);
                     caches.open(cacheName).then(function(cache){
                        var request = new Request(urlWithCacheBusting, {mode: 'cors'}) ;

                        //we add  {credentials: 'include'} in the following fetch request
                        //var credentials =request.url.indexOf('app-shell')>0?{credentials:'include'}:{};
                        var credentials = {};
                        return fetch(request, credentials).then(function(response){
                            if(response.ok){
                                return cache.put(cacheNameToAbsoluteUrl[cacheName], response);
                            }

                            console.log('Request for %s returned with status %d , so skipping to cache it.',urlWithCacheBusting, response.status);
                            // Get rid of the empty cache if we can't add a successful response to it.
                            return caches['delete'](cacheName);
                        });
                     });
                })
            ).then(function(){
            });

        }).then(function() {
          if (typeof self.skipWaiting === 'function') {
            // Force the SW to transition from installing -> active state
            self.skipWaiting();
          }
        })
    );
});

if(self.clients && (typeof self.clients.claim === 'function')){
    self.addEventListener('activate', function(event){
        event.waitUntil(
            caches.keys().then(function(cacheNames){
                cacheNames.map(function(cacheName){
                    // remove unused caches
                    if((allCacheNames.indexOf(cacheName) === -1) && (cacheName.indexOf('$$$toolbox-cache$$$') === -1)){
                        console.log('deleting out of date case : ',cacheName);
                        return caches['delete'](cacheName);
                    }
                })
            })
        );
        event.waitUntil(self.clients.claim());
    });
}

self.addEventListener('fetch', function(event){
    if(event.request.method === 'GET'){

        var urlWithoutIgnoredParameters = stripIgnoredUrlParameters(event.request.url, IgnoreUrlParametersMatching),
            cacheName = AbsoluteUrlToCacheName[urlWithoutIgnoredParameters];

        if(cacheName){
            event.respondWith(
                // Rely on the fact that each cache we manage should only have one entry, and return that.
                caches.open(cacheName).then(function(cache){
                    return cache.keys().then(function(keys){
                        return cache.match(keys[0]).then(function(response){
                            if(response){
                                return response;
                            }
                            // If for some reason the response was deleted from the cache,
                            // raise and exception and fall back to the fetch() triggered in the catch().
                            throw Error('cache for '+cacheName+' is not available.');
                        });
                    });
                })['catch'](function(){
                    return fetch(event.request);
                })
            );
        }else {
            if(event.request.url.indexOf(self.location.origin) === 0 && event.request.url.indexOf('format=json') == -1){
                event.respondWith(
                    caches.match(event.request).then(function(response){
                        return response || fetch(event.request);
                    })['catch'](function(){
                        return fallbackFunc();
                    })
                );
            }
        }
    }
});
