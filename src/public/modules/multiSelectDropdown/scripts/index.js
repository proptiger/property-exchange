'use strict';
define([
        "doT!modules/multiSelectDropdown/views/multiSelectDropdown",
        "services/filterConfigService",
        "services/urlService",
        "services/utils"
    ],
    function(dropdownTemplate) {
        Box.Application.addModule("multiSelectDropdown", function(context) {

            const FilterConfigService = context.getService('FilterConfigService'),
                //URLService = context.getService('URLService'),
                UtilService = context.getService('Utils');
            const config = context.getGlobal('config');
            const moduleElement = context.getElement();
            const DATA_TYPE = 'type',
                DATA_TARGET = 'target',
                DROPDOWN_WRAPPER = 'multi-select-dropdown',
                DROPDOWN_HEAD = "multi-select-dropdown-head",
                DROPDOWN_OPTION = 'multi-select-dropdown-option',
                DROPDOWN_PLACEHOLDER = 'multi-select-dropdown-placeholder',
                OVERLAY_SEC = 'overlay-sec',
                HIDDEN = 'hidden';
            var pageData = UtilService.getPageData();
            var dropdownElement, dropdownPlaceholder, overlay, moduleConfig, dropdownHead;

            var _addClass = function(reference) {
                if (!reference) {return;}
                $(reference).addClass(config.focusClass);
                $(reference).addClass(config.openClass);
            };

            var _removeClass = function(reference) {
                if (!reference) {return;}
                $(reference).removeClass(config.focusClass);
                $(reference).removeClass(config.openClass);
            };

            var _toggleClass = function(reference) {
                if (!reference) {return;}
                $(reference).toggleClass(config.focusClass);
                $(reference).toggleClass(config.openClass);
            };

            var _getCheckedOptions = function() {
                return $(dropdownElement).find('input:checked');
            };

            return {
                messages: ['userClicked'],
                /**
                 * Initializes the module and caches the module element
                 * @returns {void}
                 */
                init: function() {
                    var templateFromPromise,
                        queryData = Box.DOM.queryData;
                    moduleConfig = context.getConfig() || {};
                    moduleConfig.grouping = (moduleConfig && moduleConfig.grouping) || 1;
                    moduleConfig.suffix = (moduleConfig && moduleConfig.suffix) || '';
                    moduleConfig.prefix = (moduleConfig && moduleConfig.prefix) || ''; //moduleConfig && moduleConfig.grouping;
                    templateFromPromise = (moduleConfig && moduleConfig.templateFromPromise) || false;

                    FilterConfigService.renderFilter(moduleElement, pageData, dropdownTemplate, templateFromPromise).then(function() {
                        dropdownElement = queryData(moduleElement, DATA_TYPE, DROPDOWN_WRAPPER);
                        dropdownPlaceholder = queryData(moduleElement, DATA_TARGET, DROPDOWN_PLACEHOLDER);
                        overlay = queryData(moduleElement, DATA_TYPE, OVERLAY_SEC);

                        var checkedOptions = _getCheckedOptions();
                        context.broadcast('multiSelectDropdownInitiated', {
                            name: $(moduleElement).data('type'),
                            value: checkedOptions,
                            morefilter: $(moduleElement).data('morefilter'),
                            id: moduleElement.id
                        });
                    });
                },

                onclick: function(event, element, elementType) {

                    switch (elementType) {

                        case DROPDOWN_HEAD:
                            dropdownHead = element;
                            _toggleClass(dropdownElement);
                            $(overlay).removeClass(HIDDEN);
                            context.broadcast('multiSelectDropdownOpened',{moduleElement});
                            break;

                        case OVERLAY_SEC:
                            _toggleClass(dropdownElement);
                            $(overlay).addClass(HIDDEN);
                            context.broadcast('multiSelectDropdownClosed',{moduleElement});
                            break;

                        case DROPDOWN_OPTION:
                            _removeClass(dropdownElement);
                            let data = $(element).data() || {};
                            let checkedElements = _getCheckedOptions();
                            let selectedCount = checkedElements.length;
                            _addClass(dropdownElement);

                            let eventData = {};
                            eventData.name = data.name;
                            eventData.morefilter = $(moduleElement).data('morefilter');
                            eventData.id = moduleElement.id;

                            if (selectedCount) {
                                eventData.value = [];
                                for (let i = 0; i < selectedCount; i++) {
                                    eventData.value.push(checkedElements[i].value);
                                }
                                $(dropdownElement).addClass(config.activeClass);
                                let suffix = "";
                                if (selectedCount > moduleConfig.grouping) {
                                    $(checkedElements).each(function(k) {
                                        if (k !== 0 && k < moduleConfig.grouping)
                                        {
                                            suffix += "," + $(this).data('label');
                                        }
                                    });
                                    suffix += " +" + (selectedCount - moduleConfig.grouping).toString();
                                    $(dropdownPlaceholder).html(moduleConfig.prefix + ' ' + $(checkedElements).data('label') + suffix + ' ' + moduleConfig.suffix);
                                } else {
                                    $(checkedElements).each(function(k) {
                                        if (k !== 0)
                                        {
                                            suffix += "," + $(this).data('label');
                                        }
                                    });
                                    $(dropdownPlaceholder).html(moduleConfig.prefix + ' ' + $(checkedElements).data('label') + suffix + ' ' +moduleConfig.suffix);
                                }
                            } else {
                                $(dropdownElement).removeClass(config.activeClass);
                                $(dropdownPlaceholder).html('&nbsp;');
                            }

                            context.broadcast('multiSelectDropdownChanged', eventData);
                            break;
                        default:
                            _removeClass(dropdownElement);
                    }
                },

                onmessage: function(name, data) {
                    switch (name) {
                        case 'userClicked':
                        if (data.element != dropdownHead) {
                            _removeClass(dropdownElement);
                        }
                            break;
                    }
                },

                /**
                 * Destroys the module and clears references
                 * @returns {void}
                 */
                destroy: function() {
                    dropdownElement = null;
                    dropdownPlaceholder = null;
                }
            };
        });
    }
);
