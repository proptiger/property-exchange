"use strict";
define([
    'services/utils',
    'modules/stripContent/scripts/behaviors/tracking'
], function() {
    Box.Application.addModule("stripContent", function(context) {

        const queryData = Box.DOM.queryData,
            _utils = context.getService('Utils'),
            HIDDEN_CLASS = 'hidden',
            SHOW_BUTTON_DATA_TYPE = 'show-btn',
            IS_LESS_FLAG = 'less',
            CONTAINER_FULL_TEXT = 'fullText',
            CONTAINER_SMALL_TEXT = 'smallText';

        var _moduleEl,
            _shortLength,
            _smallTextElement,
            _fullTextElement,
            _moreText,
            _lessText,
            _isHTML,
            offsetHeight,
            _category,
            _sourceModule,

            _changeContent = function(type) {
                switch(type) {
                    case 'small':
                        _smallTextElement.classList.remove('hidden');
                        _fullTextElement.classList.add('hidden');
                        break;
                    case 'full':
                        _smallTextElement.classList.add('hidden');
                        _fullTextElement.classList.remove('hidden');
                        break;
                }
            },

            init = function() {
                _moduleEl = context.getElement();
                _smallTextElement = queryData(_moduleEl, 'type', CONTAINER_SMALL_TEXT);
                _fullTextElement = queryData(_moduleEl, 'type', CONTAINER_FULL_TEXT);

                var _fullText = '',
                    _hideViewMore = false;
                const _config = context.getConfig() || {},
                    _wordwise = true;

                _isHTML = _config.isHTML || false;
                _moreText = _config.moreText || 'show more';
                _lessText = _config.lessText || 'show less';
                _shortLength = _config.shortLength || 80;
                _hideViewMore = _config.hideViewMore || false;
                _category = _config.category;
                _sourceModule = _config.source;

                _fullText = (_isHTML ? _fullTextElement.innerHTML : _fullTextElement.innerText) || '';
                if (_isHTML) {
                    _smallTextElement.innerHTML = _utils.stripHtmlContent(_fullText, _shortLength, false, _wordwise).description;
                } else {
                    _smallTextElement.innerText = _utils.stripContent(_fullText, _shortLength, _wordwise).description;
                }

                if (_fullText.length>_shortLength && !_hideViewMore) {
                    queryData(_moduleEl, 'type', SHOW_BUTTON_DATA_TYPE).classList.remove(HIDDEN_CLASS);
                }

            },

            destroy = function() {
                _moduleEl = null;
                _shortLength = null;
                _smallTextElement = null;
                _fullTextElement = null;
                _isHTML = null;
                offsetHeight = null;
                _category = null;
                _sourceModule = null;
            },

            onclick = function(event, element, elementType) {
                if (elementType === SHOW_BUTTON_DATA_TYPE) {
                    event.preventDefault();
                    let ele = $(element),
                        isLess = ele.data(IS_LESS_FLAG);
                    ele.data(IS_LESS_FLAG, !isLess);
                    if (isLess) {
                        offsetHeight = $('body').scrollTop();
                        _changeContent('full');
                        ele.text(_lessText);
                        setTimeout(()=>{
                            context.broadcast('stripContent_expand', {category: _category, sourceModule: _sourceModule, id: _moduleEl.id});
                        },0)
                    } else {
                        _changeContent('small');
                        ele.text(_moreText);
                        $('body').animate({
                            'scrollTop': offsetHeight
                        }, 'slow');
                        setTimeout(()=>{
                            context.broadcast('stripContent_close', {category: _category, sourceModule: _sourceModule, id: _moduleEl.id});
                        },0)
                    }
                }
            };

        return {
            init,
            destroy,
            onclick,
            behaviors: ['stripContentTracking']
        };
    });
});
