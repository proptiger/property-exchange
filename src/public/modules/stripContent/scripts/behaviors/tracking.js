'use strict';
define([
    'common/trackingConfigService',
    'services/trackingService'
], function(t, trackingService) {
    Box.Application.addBehavior('stripContentTracking', function(context) {
        const CLOSE_MSG = 'stripContent_close',
            EXPAND_MSG = 'stripContent_expand';
        let moduelEl;

        var messages = [CLOSE_MSG, EXPAND_MSG];

        var onmessage = function(name, data) {
            if(data.id!==moduelEl.id){
                return
            }
            let event,
                sourceModule = t[data.sourceModule], 
                category = t[data.category] || t.DESCRIPTION_CATEGORY;

            switch (name) {
                case CLOSE_MSG:
                    event = t.READ_LESS_EVENT;
                    break;
                case EXPAND_MSG:
                    event = t.READ_MORE_EVENT;
                    break;
            }

            let properties = {};
            properties[t.CATEGORY_KEY] = category;
            properties[t.SOURCE_MODULE_KEY] = sourceModule;

            trackingService.trackEvent(event, properties);
        };

        return {
            messages: messages,
            onmessage: onmessage,
            init: function() {
                moduelEl = context.getElement()
            },
            destroy: function() {}
        };
    });
});
