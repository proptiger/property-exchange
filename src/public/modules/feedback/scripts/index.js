"use strict";
define([
    'services/apiService',
    'services/commonService',
    'services/feedbackService',
    'services/defaultService',
    'services/trackingService',
    'common/trackingConfigService',
    'modules/lead/scripts/services/leadService',
    'services/pushNotificationService',
    'services/utils',
    'common/sharedConfig',
    'services/leadPyrService',
    'doT!modules/feedback/views/sellerFeedback-v2/main',
    'doT!modules/feedback/views/sellerFeedback-v2/rating',
    'doT!modules/feedback/views/sellerFeedback-v2/reason',
    'doT!modules/feedback/views/sellerFeedback-v2/similar-listing',
    'doT!modules/feedback/views/sellerFeedback-v2/similar-listing-rows',
    'doT!modules/feedback/views/sellerFeedback-v2/thanks',
    'modules/feedback/scripts/behaviors/feedbackTracking'
], (ApiService,
    CommonService,
    FeedbackService,
    DefaultService,
    trackingService,
    t,
    LeadService,
    pushNotificationService,
    Utils,
    SharedConfig,
    leadPyrService,
    sellerTemplate,
    ratingTemplate,
    reasonTemplate,
    similarListingTemplate,
    similarListingRowsTemplate,
    thanksTemplate) => {
    Box.Application.addModule('feedback', (context) => {
        var messages = ["open_feedback", "close_feedback"],
            behaviors =  ['feedbackTracking'],
            _details = {},
            isProcessing = false,showHomeloan = false,
            moduleEl, masterReasons, moduleConfig;

        const config = {
            selector: {
                "MAIN": {
                    "SELECTED_SELLER": "[data-type=SELLER_LIST] input:checked",
                    "ALL_SELLER": "[data-type=SELLER_LIST] input",
                    "SELLER_LIST": "[data-type=SELLER_LIST] li",
                    "SUBMIT": "[data-type=SELLER_SELECTED_SUBMIT]",
                    "NO": "[data-type=SELLER_SELECTED_NO]"
                },
                "RATING": {
                    "SELECTED_RATING": "[data-type=MAIN_RATING] input:checked",
                    "RATING": "[data-type=MAIN_RATING] input",
                    "PROCEED": "[data-type=RATING_SUBMIT]"
                },
                "REASON": {
                    "SELECTED_REASON": "[data-type=REASON_REASON] input:checked",
                    "REASON_LIST": "[data-type=REASON_REASON] input",
                    "COMMENT_BOX": "[data-otherReasons]",
                    "REASON_PANE": ".reason-pane",
                    "COMMENT": "[data-otherReasons] textarea",
                    "SUBMIT": "[data-type=REASON_SUBMIT]",
                    "REASON_COUNT_HOLDER": "[data-reason-count]",
                    "REASON_COUNT": "[data-reason-count] strong"
                },
                "SIMILAR_LISTING": {
                    "LISTINGS": "[data-similar-listing-container]",
                    "LISTINGS_CHECKBOX": "[data-similar-listing-container] input[type=checkbox]",
                    "ALL_LISTINGS_CHECKBOX": "input[data-select-all]",
                    "LISTINGS_CHECKBOX_CHECKED": "[data-similar-listing-container] input[type=checkbox]:checked",
                    "SELECT_ALL_CONTAINER": "[data-select-all-container]",
                    "ALL_LISTINGS_CHECKBOX_TEXT": "[data-selectunselect-text]"
                },
                "WRAPPER": "[data-feedback-container]",
                "PROCESSING": "[data-processing]",
                "NORMAL": "[data-normal]",
                "BUTTON": "[data-mainbtn]"
            },
            messages: leadPyrService.getValidationStrategyConfig(),
            otherReasonClass: "step-other-reason",
            hideClass: "hide",
            disableClass: "disabled",
            defaultHideTime: 5000,
            ratingDecider : 4,
            otherReasonsId : [53, 64],
            feedbackSeen: "seen"
        };
        let homeloanData={};
        // function _startAllModules() {
        //     CommonService.startAllModules(moduleEl);
        // }

        // function _stopAllModules() {
        //     CommonService.stopAllModules(moduleEl);
        // }

        function _setHomeloanData(data){
            homeloanData={};
            homeloanData.summary_mobile = data.phone;
            homeloanData.summary_email = data.email;
            homeloanData.summary_name = data.name;
            homeloanData.project_cityName = data.cityName;
            homeloanData.project_cityId = data.cityId;
            homeloanData.price_propertyPrice = data.propertyPrice;
        }
        function openHomeloanForm() { 
            context.broadcast('loadModule', {
                name: 'homeloanFlow'
            });
            
            CommonService.bindOnModuleLoad('homeloanFlow', () => {
                context.broadcast('homeloanFlow', {
                    startScreen: "summary",
                    preObject: homeloanData
                });
            });
        }

        function _openPopup(id) {
            context.broadcast('popup:open', {
                id: id,
                options: {
                    config: {
                        modal: true
                    }
                }
            });
        }
        
        function _closePopup(dataset) {
            switch(dataset.modal){
                case "sellerSelection":
                    _track("trackSellerSelectionClose");
                    break;
                case "rating":
                    _track("trackRatingModalClose");
                    break;
                case "reason":
                    (dataset.rating < 4) ? _track("trackNegativeModalClose") : _track("trackPositiveModalClose");
                    break;
            }
            context.broadcast('popup:close');
            checkAndLaunchMProfile();

        }

        function _showError(errorType, field) {
            let message = config.messages[field][errorType]["MESSAGE"];
            $(moduleEl).find('[data-message=' + field + ']').text(message);
            _hideError(field, config.defaultHideTime);
        }

        function _hideError(field, time) {
            setTimeout(() => {
                $(moduleEl).find('[data-message=' + field + ']').html('');
            }, time);
        }

        // function _clearStepsStack() {
        //     _stepsStack = [];
        // }

        // function _updateStepsStack(step) {
        //     if (step) {
        //         if (_stepsStack[_stepsStack.length - 1] !== step) { _stepsStack.push(step); }
        //     } else {
        //         _stepsStack.pop();
        //         step = _stepsStack[_stepsStack.length - 1];
        //     }
        //     return step;
        // }

        function _bindRatingOnChange(step, cb) {
            if(config.selector[step] && config.selector[step]["ALL_SELLER"] && cb) {
                $(moduleEl).find(config.selector[step]["ALL_SELLER"]).on('change', cb);
            }

            if (config.selector[step] && config.selector[step]["RATING"] && cb) {
                $(moduleEl).find(config.selector[step]["RATING"]).on('change', cb);
            }

            if(config.selector[step] && config.selector[step]["REASON_LIST"] && cb){
                $(moduleEl).find(config.selector[step]["REASON_LIST"]).on('change', cb);   
            }
        }

        function _unbindRatingOnChange(step) {
            if (config.selector[step] && config.selector[step]["RATING"]) {
                $(moduleEl).find(config.selector[step]["RATING"]).off();
            }
            if (config.selector[step] && config.selector[step]["ALL_SELLER"]) {
                $(moduleEl).find(config.selector[step]["ALL_SELLER"]).off();
            }
            if (config.selector[step] && config.selector[step]["REASON_LIST"]) {
                $(moduleEl).find(config.selector[step]["REASON_LIST"]).off();
            }
        }

        function _showHideProcessing(hide) {
            let actionButton = $(moduleEl).find(config.selector["NORMAL"]),
                processingButton = $(moduleEl).find(config.selector["PROCESSING"]);
            if (hide) {
                processingButton.addClass(config.hideClass);
                actionButton.removeClass(config.hideClass);
                isProcessing = false;
            } else {
                processingButton.removeClass(config.hideClass);
                actionButton.addClass(config.hideClass);
                isProcessing = true;
            }
        }

        function _gotoStep(step, data) {
            data.step = step;
            _renderView(step, data);
        }

        function _fetchDataFromUI(step) {
            let data = {};
            switch (step) {
                case "MAIN": 
                    data["sellerSelected"] = $(moduleEl).find(config.selector["MAIN"]["SELECTED_SELLER"]).data();
                    break;
                case "RATING":
                    data["rating"] = $(moduleEl).find(config.selector["RATING"]["SELECTED_RATING"]).val();
                    break;
                case "REASON":
                    data["reasons"] = $(moduleEl).find(config.selector["REASON"]["SELECTED_REASON"]).map(function(){return $(this).data();}).get();
                    data["comment"] = $(moduleEl).find(config.selector["REASON"]["COMMENT"]).val();
                    break;
                case "SIMILAR_LISTING":
                    $(moduleEl).find(config.selector[step]["LISTINGS_CHECKBOX_CHECKED"]).each(function(index) {
                        data[index] = JSON.parse(decodeURI($(this).data('collection')));
                    });
                    break;
            }
            return data;
        }

        function _renderView(step, data) {
            let html;
            switch (step) {
                case "MAIN":
                    html = sellerTemplate(data);
                    setTimeout(() => {
                        _bindRatingOnChange(step , ()=> {
                            data.postData = $.extend(data.postData, _fetchDataFromUI(step).sellerSelected); //postData will have sellerSelected object in it.
                            $(moduleEl).find(config.selector["MAIN"]["SELLER_LIST"]).removeClass('active');
                            $(moduleEl).find(config.selector["MAIN"]["SELECTED_SELLER"]).closest('li').addClass('active');
                            // hideShowSubmit(data);
                            getSelectedSellerDetail(data);
                            _track("trackSellerSelectionChosen");
                        });
                    });
                    _track("trackSellerSelectionOpen");
                    break;
                case "RATING":
                    html = ratingTemplate(data); 
                    setTimeout(() => {
                        _bindRatingOnChange(step, () => {
                            data.postData = $.extend(data.postData, _fetchDataFromUI(step)); //postData will have rating variable in it.
                            hideShowSubmit(data);
                            _track("trackRatingChosen");
                        });
                    });
                    _track("trackRatingModalOpen");
                    break;
                case "REASON":
                    data.reasonListName = FeedbackService.getReasonListName(data);
                    html = reasonTemplate(data); 
                    setTimeout(() => {
                        _bindRatingOnChange(step, () => {
                            data.postData = $.extend(data.postData, _fetchDataFromUI(step));
                            showOtherReasonBox(data.postData);
                            if(data.postData.reasons && data.postData.reasons.length){
                                $(moduleEl).find(config.selector["REASON"]["REASON_COUNT"]).html(data.postData.reasons.length);
                                $(moduleEl).find(config.selector["REASON"]["REASON_COUNT_HOLDER"]).removeClass(config.hideClass);
                                data.result.rating < 4 ? _track("trackNegativeReasonChosen") : _track("trackPositiveReasonSelected");
                            } else {
                                $(moduleEl).find(config.selector["REASON"]["REASON_COUNT_HOLDER"]).addClass(config.hideClass);
                            }
                            hideShowSubmit(data);
                        });
                    });
                    data.result.rating < 4 ? _track("trackNegativeModalOpen") : _track("trackPositiveModalOpen");
                    break;
                case "SIMILAR_LISTING": 
                    html = similarListingTemplate();
                    setTimeout(_getSimilarListing);
                    break;
                case "THANKYOU":
                    data.isBuyLead = (_details.postData && _details.postData.salesType == "buy") || showHomeloan;
                    html = thanksTemplate(data); 
                    break;
            }
            $(moduleEl).find(config.selector["WRAPPER"]).html(html);
        }

        function _appendSimilarListing(data){
            let html = similarListingRowsTemplate(data);
            $(moduleEl).find(config.selector["SIMILAR_LISTING"]["LISTINGS"]).html(html);
            $(moduleEl).find(config.selector["BUTTON"]).removeClass(config.disableClass);
        }

        function _getSimilarListing(){
            LeadService.getSimilarListing(_details.result.listingId, _details.result.companyId).then((response) => {
                if (response && response.length) {
                    _appendSimilarListing(response);
                    // _showSelectAll("SIMILAR");
                } else {
                    _gotoStep("THANKYOU",_details);
                }
            }, () => {
                _gotoStep("THANKYOU",_details);
            });
        }

        function _processAndRender(data,buyerId) {
            if(data.throughLeadForm){
                _details.postData = $.extend(true, _details.postData, data);
                _details.result = $.extend(true, _details.result, data);
                _details.throughMProfile = data.throughMProfile;
                 _setHomeloanData(_details.postData);
                getSelectedSellerDetail(_details);
                return;
            }else if(buyerId){
                getHomeloanPersonalDetail(buyerId);
            }
            _details.step = data.step || "MAIN";
            delete(data.step);
            _details.result = data;
            _gotoStep(_details.step, _details);
        }

        function _moveToNextStep(details, currentStep) {
            currentStep = currentStep || details.step;
            _unbindRatingOnChange(currentStep);
            _showHideProcessing(true);
            switch (currentStep) {
                case "MAIN": 
                    _gotoStep("RATING",details);
                    break;
                case "RATING":
                    _gotoStep("REASON", details);
                    break;
                case "REASON":
                    if(details.result.listingId){
                        _gotoStep("SIMILAR_LISTING", details);
                    } else {
                        _gotoStep("THANKYOU",details);
                    }
                    break;
                case "SIMILAR_LISTING":
                    _gotoStep("THANKYOU",details);
                    break;
            }
        }

        function submitRating(data){
            return FeedbackService.submitRating(data).then(response => {
                if(response.data){
                    data.postData = $.extend(true, data.postData, {ratingSubmitId: response.data.id});
                    data.result = $.extend(true, data.result, {rating: data.postData.rating});
                }
                _moveToNextStep(data, "RATING");
            }, () => {
                _showHideProcessing(true);
                _showError("GENERIC", "GLOBAL");
            });
        }

        function submitReasons(data){
            data.postData = $.extend(true, data.postData, _fetchDataFromUI("REASON"));
            return FeedbackService.submitReasons(data).then(() => {
                _moveToNextStep(data, "REASON");
            }, () => {
                _showHideProcessing(true);
                _showError("GENERIC", "GLOBAL");
            });
        }

        function getSelectedSellerDetail(data){
            FeedbackService.getCallFeedbackDetails(data.postData).then(response => {
                 data.result = response;
                homeloanData.project_cityName = response.city;
                homeloanData.project_cityId = response.cityId;
                homeloanData.price_propertyPrice = response.homeloanMinPrice;
                showHomeloan = response.showHomeloan;
                _moveToNextStep(data, "MAIN");
            },() => {
                _showHideProcessing(true);
                _showError("GENERIC", "GLOBAL");
            });
        }

        function _postSimilarListings(details){
            let listingSelected = _fetchDataFromUI("SIMILAR_LISTING");
            $.each(listingSelected, (key, value) => {
                LeadService.updateUserFields(details.postData, value, "SIMILAR");
            });
            LeadService.postSimilarListingEnquiry(listingSelected).then((response) => {
                _moveToNextStep(details, "SIMILAR_LISTING");
            }, () => {
                _showHideProcessing(true);
                _showError("GENERIC", "GLOBAL");
            });
        }

        function updateUserDetails(details){
            if(!details.postData.phone){
                ApiService.get(SharedConfig.apiHandlers.userDetails(details.postData.buyerId).url).then(response => {
                    details.postData.phone = response.contactNumbers && response.contactNumbers[0].contactNumber;
                    _postSimilarListings(details);
                }, () => {
                    _showHideProcessing(true);
                    _showError("GENERIC", "GLOBAL");
                });
            } else {
                _postSimilarListings(details);
            }
        }
        function getHomeloanPersonalDetail(buyerId){
                ApiService.get(SharedConfig.apiHandlers.userDetails(buyerId).url).then(response => {
                    homeloanData.summary_mobile = response.contactNumbers && response.contactNumbers[0].contactNumber;
                    homeloanData.summary_email = response.email;
                    homeloanData.summary_name = response.firstName ? `${response.firstName}`:'';
                }, () => {
                });
        }

        function showOtherReasonBox(data) {
            let reasonElement = $(moduleEl).find(config.selector["REASON"]["COMMENT_BOX"]);
            let showCommentBox = false;
            if(data.rating < config.ratingDecider){
                data.reasons.forEach(reason => {
                    if(reason.label == "Other Reasons" || config.otherReasonsId.indexOf(reason.id) > -1){
                        showCommentBox = true;
                    }
                });
            }
            reasonElement[`${showCommentBox ? "removeClass" : "addClass"}`](config.hideClass);
            let offsetTop = reasonElement.offset().top;
            let height = reasonElement.height();
            $(moduleEl).find(config.selector["REASON"]["REASON_PANE"]).animate({scrollTop: offsetTop-height});
        }

        function selectUnselectAll(step) {
            let selectAllCheckbox = $(moduleEl).find(config.selector[step]["ALL_LISTINGS_CHECKBOX"]),
                selectAllCheckboxText = $(moduleEl).find(config.selector[step]["ALL_LISTINGS_CHECKBOX_TEXT"]),
                selectedListings = $(moduleEl).find(config.selector[step]["LISTINGS_CHECKBOX_CHECKED"]).length,
                allListing = $(moduleEl).find(config.selector[step]["LISTINGS_CHECKBOX"]).length;

            if (selectedListings === allListing) {
                selectAllCheckbox.prop("checked", true);
                selectAllCheckboxText.html("Unselect All");
            } else {
                selectAllCheckbox.prop("checked", false);
                selectAllCheckboxText.html("Select All");
            }
            $(moduleEl).find(config.selector["BUTTON"])[`${selectedListings ? "removeClass" : "addClass" }`](config.disableClass);
        }

        function _submit(step, details) {
            if (isProcessing) {
                return;
            }
            _showHideProcessing();
            switch (step) {
                // case "MAIN":
                //     getSelectedSellerDetail(details);
                //     break;
                case "RATING":
                    submitRating(details);
                    break;
                case "REASON":
                    submitReasons(details);
                    break;
                case "SIMILAR_LISTING":
                    updateUserDetails(details);
                    break;
            }
        }

        function hideShowSubmit(details){
            let currentStep = details.step;
            switch(currentStep){
                // case "MAIN":
                //     if(details.postData && details.postData.companyId){
                //         $(moduleEl).find(config.selector["BUTTON"]).removeClass(config.disableClass);
                //     }
                //     break;
                case "RATING":
                    if(details.postData.rating){
                        $(moduleEl).find(config.selector["BUTTON"]).removeClass(config.disableClass);
                    }
                    break;
                case "REASON":
                    if(details.postData.reasons && details.postData.reasons.length){
                        $(moduleEl).find(config.selector["BUTTON"]).removeClass(config.disableClass);
                    } else {
                        $(moduleEl).find(config.selector["BUTTON"]).addClass(config.disableClass);
                    }
                    break;
                case "SIMILAR_LISTING":
                    break;
            }
        }

        function _track(type){
            context.broadcast(type, {id: moduleEl.id});
        }

        function reset(){
            _details = {};
            isProcessing = false;
        }

        function init() {
            moduleEl = context.getElement();
            moduleConfig = context.getConfig() || {};
            masterReasons = masterReasons || FeedbackService.getFeedbackMasterReasons();
            context.broadcast("moduleLoaded", { name: "feedback", id: moduleEl.id });
        }

        function destroy() {
            _details = [];
        }

        function onmessage(name, data) {
            switch (name) {
                case "open_feedback":
                    let pageType = Utils.getPageData('pageType');
                    if ((SharedConfig.sellerFeedbackRestrictedPage.indexOf(pageType) < 0) && !(CommonService.getOpenedPopupCount()) && (data.forceFeedback || !moduleConfig.nonPopup)) { 
                        if (!data.error) {
                            reset();
                            masterReasons = masterReasons || FeedbackService.getFeedbackMasterReasons();
                            masterReasons.then((response) => {
                                if (response) {
                                    _details.masterReasons = response;
                                    _openPopup('feedback');
                                    if(data && data.buyerIdFromParam){
                                        let buyerId = data.buyerIdFromParam;
                                        data = data.response;
                                        _processAndRender(data,buyerId);
                                    }else{
                                        _processAndRender(data);
                                    }
                                }
                            });
                        }
                    }
                    break;
                case "close_feedback":
                    _closePopup();
                    break;
            }
        }

        function checkAndLaunchMProfile() {
            if(!_details.throughMProfile) {
                context.broadcast("userProfilingPopupOpen", {});
            }
        }

        function onclick(event, element, elementType) {
            let dataset = $(element).data();
            switch (elementType) {
                // case "SELLER_SELECTED_SUBMIT":
                //     _submit("MAIN", _details);
                //     break;
                case "RATING_SUBMIT":
                    _submit("RATING",_details);
                    break;
                case "REASON_SUBMIT":
                    _submit("REASON",_details);
                    break;
                case "DISCARD":
                    FeedbackService.setFeedbackData(config.feedbackSeen, true);
                    _closePopup(dataset);
                    break;
                case "SIMILAR_LISTING_NO":
                    _gotoStep("THANKYOU", _details);
                    break;
                case "SIMILAR_CHECKBOX":
                    selectUnselectAll("SIMILAR_LISTING");
                    break;
                case "SIMILAR_SELECT_ALL":
                    $(moduleEl).find(config.selector["SIMILAR_LISTING"]["LISTINGS_CHECKBOX"]).prop("checked", $(element).prop("checked"));
                    $(moduleEl).find(config.selector["BUTTON"])[`${$(element).prop("checked") ? "removeClass" : "addClass" }`](config.disableClass);
                    $(moduleEl).find(config.selector["SIMILAR_LISTING"]["ALL_LISTINGS_CHECKBOX_TEXT"]).html(`${$(element).prop("checked") ? "Unselect All": "Select All"}`);
                    break;
                case "SIMILAR_LISTING_SUBMIT":
                    _submit("SIMILAR_LISTING", _details);
                    break;
                case "feedback_homeloan_apply":
                    context.broadcast('popup:close',{callback:openHomeloanForm});
                    _track("trackFeedbackContinueHomeloan");
                    break;
                case "CLOSE_FEEDBACK" :
                    context.broadcast('popup:close');
                    _track("trackFeedbackContinueSearch");
                    checkAndLaunchMProfile();
                    break;
            }
        }

        return {
            init,
            messages,
            behaviors,
            onmessage,
            onclick,
            destroy
        };
    });
});
