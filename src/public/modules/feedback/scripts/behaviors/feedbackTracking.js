define([
    'common/trackingConfigService',
    'services/trackingService'
], function(t, trackingService) {
    'use strict';
    Box.Application.addBehavior('feedbackTracking', function(context) {

        var element;
        const messages = [
            "trackRatingModalOpen",
            "trackRatingModalClose",
            "trackRatingChosen",
            "trackNegativeModalOpen",
            "trackNegativeModalClose",
            "trackNegativeReasonChosen",
            "trackPositiveModalOpen",
            "trackPositiveModalClose",
            "trackPositiveReasonSelected",
            "trackSellerSelectionOpen",
            "trackSellerSelectionClose",
            "trackSellerSelectionChosen",
            "trackFeedbackContinueSearch",
            "trackFeedbackContinueHomeloan",

        ];

        var init = function() {
            element = context.getElement();
        };

        var onmessage = function(name, data) {
            if (data.id === element.id) {
                let properties = {};
                properties[t.CATEGORY_KEY] = "Ratings & Reviews";
                let event;
                switch (name) {
                    case "trackRatingModalOpen":
                        event = "Seller Rating Modal Open";
                        break;
                    case "trackRatingModalClose":
                        event = "Seller Rating Modal Close";
                        break;
                    case "trackRatingChosen":
                        event = "Seller Rating Chosen";
                        break;
                    case "trackNegativeModalOpen":
                        event = "Seller Rating Reasons Negative Modal Open";
                        break;
                    case "trackNegativeModalClose":
                        event = "Seller Rating Reasons Negative Modal Close";
                        break;
                    case "trackNegativeReasonChosen":
                        event = "Seller Rating Reasons Negative Reason Chosen";
                        break;
                    case "trackPositiveModalOpen":
                        event = "Seller Rating Reasons Positive Modal Open";
                        break;
                    case "trackPositiveModalClose":
                        event = "Seller Rating Reasons Positive Modal Close";
                        break;
                    case "trackPositiveReasonSelected":
                        event = "Seller Rating Reasons Positive Reason Chosen";
                        break;
                    case "trackSellerSelectionOpen":
                        event = "Seller Selection Modal Open";
                        break;
                    case "trackSellerSelectionClose":
                        event = "Seller Selection Modal Close";
                        break;
                    case "trackSellerSelectionChosen":
                        event = "Seller Selection Chosen";
                        break;
                    case "trackFeedbackContinueSearch" :
                        event = "continue";
                        properties[t.CATEGORY_KEY] = "Lead Form";
                        properties[t.LABEL_KEY] = "homeloanNo"; 
                        break;
                    case "trackFeedbackContinueHomeloan" :
                        event = "continue";
                        properties[t.CATEGORY_KEY] = "Lead Form";
                        properties[t.LABEL_KEY] = "homeloanYes"; 
                        break;

                    default:
                        return;
                }
                trackingService.trackEvent(event, properties);
            }
        };

        return {
            messages,
            onmessage,
            init,
            destroy: function() {}
        };
    });
});