"use strict";
define([
    'doT!modules/loanTracker/views/index',
    'services/bankService',
], function(template) {
    Box.Application.addModule("loanTracker", function(context) {
        const BankService = context.getService('bankService');
        var moduleEl, $moduleEl, moduleId, config, $, templateData;
        var messages=['showhomeloanTracking'];


        // config.latestRequirement = true;
        // config.userId =

        var init = function(){
            moduleEl = context.getElement();
            moduleId = moduleEl.id;
            config = context.getConfig() || {};
            $ = context.getGlobal('jQuery');
            $moduleEl = $(moduleEl);
            context.broadcast('moduleLoaded', {
                'name': 'loanTracker',
                'id': moduleEl.id
            });
            console.log('inloanTracker');
        };

        function activateTracking(userId) {
            let latest = $moduleEl.data('latest');
            BankService.getBankApplicationTracking(userId,
                (success)=>{
                    let totalTracking = success.data.requirements.length;
                    if(latest){
                        templateData = {'requirements': [success.data.requirements[0]] };
                    }else{
                        templateData = success.data;
                    }
                    render(getStatus(templateData), latest, (totalTracking - 1));
                },
                (error)=>{
                    templateData = {};
                    render(templateData);
                }
            );
        }

        function getStatus(templateData){
            let statusValues = {
                'A':['active','','',''],
                'B':['completed','active','',''],
                'C':['completed','completed','active',''],
                'D':['completed','completed','completed','active'],
                'E':['completed','completed','completed','completed']
            };

            templateData.requirements.forEach((requirement)=>{
                requirement.banks.forEach((bank)=>{
                    bank.statusArr = statusValues[bank.status];
                });
            });

            return templateData;
        }
        function render(templateData, latest, totalTracking){
            let html = template({templateData , latest, totalTracking});
            $moduleEl.html(html);
        }
        function onclick(event, clickElement, elementType) {
            switch (elementType){
                case 'viewTracking':
                    context.broadcast('viewTrackingClicked');
                    break;
            }
        }
        function onmessage(name, data){
            switch(name){
                case 'showhomeloanTracking':
                        activateTracking(data.userId);
                    break;
            }
        }
        return {
            init,
            onmessage,
            messages,
            onclick
        };
    });
});
