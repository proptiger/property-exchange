define([
    "doT!modules/pagination/views/index",
    "services/urlService"
    ],
function(template) {
    "use strict";
    Box.Application.addModule("pagination", function(context) {

        var element, urlService, onlyNextPrev, currentPage, totalPages;

        function _handlePageNumberChange(pageNumber){
            if (pageNumber == 1) {
                urlService.removeUrlParam('page');
            } else {
                urlService.changeUrlParam('page', pageNumber);
            }
            context.broadcast('pageChanged');
        }

        return {
            messages: ['resetPagination'],
            init: function() {
                element = context.getElement();
                urlService = Box.Application.getService("URLService");
                currentPage = urlService.getUrlParam('page');
                totalPages = parseInt(context.getConfig('totalPages'));
                onlyNextPrev = context.getConfig('onlyNextPrev');
                if (!currentPage) {
                    currentPage = 1;
                }
                currentPage = parseInt(currentPage);

                var data = {
                    'onlyNextPrev': onlyNextPrev,
                    'totalPages': totalPages,
                    'currentPage' : parseInt(currentPage),
                    'minPages' : 5      // min pages are the minimum number of pages visible on client pagination module
                };
                element.innerHTML = template(data);
            },

            onclick: function(event, element, elementType) {
                switch (elementType) {
                    case 'page-number':
                        var pageNumber = element.getAttribute('data-page');
                        _handlePageNumberChange(pageNumber);
                        break;
                    case 'next-page':
                        if(currentPage !== totalPages){
                            currentPage += 1;
                            _handlePageNumberChange(currentPage);
                        }
                        break;
                    case 'prev-page':
                        if(currentPage && currentPage !== 1){
                            currentPage -= 1;
                            _handlePageNumberChange(currentPage);
                        }
                        break;
                }
            },
            onmessage: function(name) {
                if(name === 'resetPagination') {
                    urlService.removeUrlParam('page');
                }
            }
        };
    });
});
