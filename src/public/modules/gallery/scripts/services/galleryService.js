'use strict';
define([
    'services/utils'
], function(utils) {
    Box.Application.addService('galleryService', function() {
        const MAIN_LEAD_FORM_ID = "main_lead_form";
        /*
            Return the template specific data
        */
        function getViewData(config, data, isMobile) {
            var listingData = data.listingData || {};
            var leadFormData = data.leadFormData || {};
            data = data || {};
            
            var leadBoxData = {};
            var configPageData = config.pageAllData || {};
            if(config.pageType == 'property' || config.pageType == 'project'){
                leadBoxData = configPageData;
                leadBoxData.extraData = config.extraData;
                leadBoxData.extraData.projectId = leadBoxData.projectId;

                leadBoxData.leadConfigData = getLeadFormConfig(leadBoxData.leadFormData, leadBoxData.extraData);
            }else if(config.pageType == 'serp'){
                leadBoxData.projectTitle = listingData.fullName ? listingData.fullName: (listingData.bedrooms ? (listingData.bedrooms +  ' BHK ') : '') + listingData.propertyType;
                leadBoxData.localityDetails = listingData.localityName +', '+listingData.cityName;
                leadBoxData.price = utils.formatNumber(leadFormData.price, { precision : 2, seperator : window.numberFormat });
                leadBoxData.type = (listingData.bedrooms ? (listingData.bedrooms +  ' BHK ') : '') + listingData.propertyType;
                leadBoxData.projectId = leadFormData.projectId;
                leadBoxData.listingId = leadFormData.listingId;
                leadBoxData.builderName = '';
                leadBoxData.propertyType = listingData.propertyType;
                leadBoxData.unitTypeId = listingData.unitTypeId;
                leadBoxData.bedrooms = listingData.bedrooms;
                leadBoxData.localityId = listingData.localityId;
                leadBoxData.listingCategory = listingData.listingCategory;
                leadBoxData.cityId = listingData.cityId;
                leadBoxData.size = listingData.size;
                leadBoxData.isRk = listingData.isRk;
                leadBoxData.isPentHouse = listingData.isPentHouse;
                leadBoxData.isStudio = listingData.isStudio;
                leadBoxData.priceNum = listingData.price;
                leadBoxData.projectStatus = listingData.projectStatus;

                leadBoxData.leadConfigData = getSERPLeadFormConfig(leadBoxData, leadFormData);
            }else if(config.pageType == 'projectSerp') {
                return getProjectSerpViewData.apply(null, arguments);
            }
            if(isMobile && leadBoxData.leadConfigData) {
                leadBoxData.leadConfigData.step ="CALL_NOW";
            }
            if(leadBoxData.leadConfigData){
                leadBoxData.leadConfigData.isPopup = false;
                leadBoxData.leadConfigData.isGallery = true;
            }
            leadBoxData.leadFormData = $.extend({}, leadFormData, leadBoxData.leadFormData);

            return {url:data.url,leadBoxData:leadBoxData ,pageType:config.pageType,isMobile: isMobile};
        }

        function getLeadFormConfig(leadFormData, extraData) {
            let __leadFormElem = $("#" + MAIN_LEAD_FORM_ID),
                rawData;
            if (__leadFormElem.length > 0) {
                rawData = Box.Application.getModuleConfig(__leadFormElem);
            } else if(leadFormData && Object.keys(leadFormData).length && extraData){
                rawData = {
                    "companyId": leadFormData.sellerDetails.sellerId,
                    "companyName": leadFormData.sellerDetails.name,
                    "companyPhone": leadFormData.sellerDetails.phone,
                    "companyRating": leadFormData.sellerDetails.rating,
                    "companyType": leadFormData.sellerDetails.type,
                    "companyUserId": leadFormData.sellerDetails.userId,
                    "companyImage": leadFormData.sellerDetails.image,
                    "companyAssist": leadFormData.sellerDetails.assist,
                    "listingId": leadFormData.listingId,
                    "localityId": extraData.localityId,
                    "projectId": extraData.projectId,
                    "propertyType": [extraData.unitTypeId],
                    "propertyTypeLabel": extraData.propertyType,
                    "similarType": "listing",
                    "isMakaanSelectSeller": leadFormData.isMakaanSelectSeller,
                    "isRental": extraData.listingCategory && extraData.listingCategory === 'Rental' ? true : false                
                };
            }
            return rawData;
        }

        function getSERPLeadFormConfig(leadBoxData, leadFormData) {
            var obj = {
                "companyId": leadFormData.companyId,
                "companyName": leadFormData.companyName,
                "companyPhone": leadFormData.companyPhone,
                "companyImage": leadFormData.companyImage || '',
                "companyRating": leadFormData.companyRating,
                "companyType": leadFormData.companyType,
                "companyUserId": leadFormData.companyUserId,
                "listingId": leadBoxData.listingId,
                "projectId": leadBoxData.projectId,
                "cityId": leadBoxData.cityId,
                "localityId": leadBoxData.localityId,
                "listingCategory": leadBoxData.listingCategory,
                "projectName": leadFormData.projectName,
                "propertyType": leadFormData.propertyTypeLabel,
                "builderId": leadFormData.builderId,
                "bhk": leadBoxData.bedrooms,
                "budget": leadFormData.budget,
                "projectStatus:": leadBoxData.projectStatus,
                "propertyTypeId": leadBoxData.unitTypeId,
                "id": "image-lead-popup",
                "isMakaanSelectSeller": leadFormData.isMakaanSelectSeller||false,
                "source": "gallery pop up"
            }
            try {
                if (leadFormData && leadFormData.otherCompany) {
                    obj.otherCompany = JSON.parse(decodeURI(leadFormData.otherCompany));
                }
            } catch (e) {
                obj.otherCompany = {};
            }
            return obj;
        }

        function getProjectSerpViewData(config, data, isMobile){
            var leadFormData = data.leadFormData || {};
            data = data || {};
            var leadBoxData = {};
            var projectData = data.data || {};
            var project = projectData.project || {};
            var locality = projectData.locality || {};
            var city = projectData.city || {};
            var builder = projectData.builder || {};
            leadBoxData.projectTitle = project.name;
            leadBoxData.localityDetails = locality.name + ', ' + city.name;
            leadBoxData.price = project.price || '';
            leadBoxData.type = project.unitTypes && project.unitTypes.join(', ');
            leadBoxData.projectId = project.id;
            leadBoxData.builderName = builder.name;
            leadBoxData.bedrooms = project.beds;
            leadBoxData.unitTypeId = project.unitTypeId;
            leadBoxData.minPrice = project.minPriceNum;
            leadBoxData.maxPrice = project.maxPriceNum;
            leadBoxData.localityId = locality.id;
            leadBoxData.cityId = city.id;
            leadBoxData.projectStatus = project.status;
            leadBoxData.builderDisplayName = builder.displayName || builder.name;
            return data.leadFormData.then((response) => {
                var leadData = response;
                leadBoxData.leadConfigData = getSERPLeadFormConfig(leadBoxData, leadData);
                if (leadBoxData.leadConfigData) {
                    if (isMobile) {
                        leadBoxData.leadConfigData.step = "CALL_NOW";
                    } else {
                        leadBoxData.leadConfigData.step = "MAIN_TOP_SELLER";
                    }
                }
                if(leadBoxData.leadConfigData){
                    leadBoxData.leadConfigData.isPopup = false;
                    leadBoxData.leadConfigData.isGallery = true;
                }
                leadBoxData.leadFormData = $.extend({}, leadFormData, leadBoxData.leadFormData);
                return {url:data.url,leadBoxData:leadBoxData ,pageType:config.pageType,isMobile: isMobile, leadData: leadData};
            });
            
        }

        return {
            getViewData
        };
    });

    return Box.Application.getService('galleryService');
});
