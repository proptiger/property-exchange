"use strict";
define([
    'jwPlayer',
], function() {
    Box.Application.addService('JWPlayerHelper', function() {
        let isPlayed = false;
        function jwPlayer(options) {
            var playerObject = {};
            playerObject.selector = options.divId;
            playerObject.file = options.file;
            playerObject.image = options.image;
            playerObject.height = options.height;
            playerObject.title = options.title ? options.title : "";
            playerObject.width = options.width ? options.width : '100%';
            playerObject.background = options.background ;
            playerObject.backgroundColor = options.backgroundColor ;
            playerObject.wmode = options.wmode ;
            playerObject.autostart = options.autostart || false;
            playerObject.aspectRatio = options.aspectRatio ? options.aspectRatio : '16:9';
            playerObject.base = "bower_components/jwplayer/";
            doSetup();

            function doSetup() {
                jwplayer(playerObject.selector).setup(playerObject);
                jwplayer(playerObject.selector).onPlay(_onPlay.bind(undefined,playerObject.file));
                jwplayer(playerObject.selector).onPause(_onPause.bind(undefined,playerObject.file));
                jwplayer(playerObject.selector).onSeek(_onSeek.bind(undefined,playerObject.file));
            }
        }

        function _onPlay(file) {
            if(!isPlayed) {
                isPlayed = true;
                Box.Application.broadcast("galleryVideoPlayed",{sourceModule: file});
            }
        }

        function _onPause(file) {
            Box.Application.broadcast("galleryVideoPaused",{sourceModule: file});
            isPlayed = false;
        }

        function _onSeek(file) {
            Box.Application.broadcast("galleryVideoSeeked",{sourceModule: file});
        }

        jwPlayer.initJwPlayer = function(options) {
            isPlayed = false;
            new jwPlayer({
                'divId': options.divId,
                'file': options.fileName,
                'image': options.image,
                'title': options.title,
                'width': options.width,
                'height': options.height,
                'autostart': options.autostart,
                'aspectRatio': options.aspectRatio,
                 background: options.background,
                 backgroundColor: options.backgroundColor,
                 wmode: options.wmode
            });
        };

        return {
            jwPlayer: jwPlayer
        };
    });
});
