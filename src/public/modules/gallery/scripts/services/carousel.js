'use strict';
define([], function() {
    Box.Application.addService('carousel', function() {

        /*
            Function to create the carousel
        */
        function createCarousel(options){
            options = options || {};
            options.delay = options.delay || 500;
            options.slideWidth = options.slideWidth || 100;
            var groupListWidth = 0;
            var groupListOuter = options.itemContainer.parent();
            var groupList = options.itemContainer;
            options.items.each(function(){
                groupListWidth += $(this).outerWidth(true);
            });

            var groupListContainerWidth = groupList.outerWidth() - groupList.width();
            groupListWidth = groupListWidth + groupListContainerWidth;
            var groupListOuterWidth = groupListOuter.outerWidth(true);
            var listOuterWidthDiff = groupListOuterWidth - groupListWidth;
            var widthDiff = groupListOuter.outerWidth(true) - groupListOuter.width();
            if(options.noOuter){
                groupListWidth = groupListWidth + 10;
            }else{
                groupListWidth = groupListWidth + widthDiff + 20;
            }
            options.itemContainer.css({'width': (groupListWidth)+'px'});
            if(options.mobile || listOuterWidthDiff > 0){
                if(options.mobile) {groupListOuter.addClass('mobile');}
                groupListOuter.find('.list-next, .list-prev, .crausal-next, .crausal-prev').addClass('hide');
            }else{
                groupListOuter.removeClass('mobile');
                groupListOuter.find('.list-next, .list-prev, .crausal-next, .crausal-prev').removeClass('hide');
                groupListOuter.find('.list-prev, .crausal-prev').off('click');
                groupListOuter.find('.list-prev, .crausal-prev').on('click', function() {
                groupListWidth = groupList.outerWidth();
                var groupListOuterWidth = groupListOuter.width();
                var maxMargin = groupListOuterWidth - groupListWidth;
                var margin = parseInt(groupList.css('margin-left'), 10);
                if(maxMargin > 0){
                    margin = 0;
                }else{
                    margin += (margin < 0) ? options.slideWidth : 0;
                    margin = (margin > 0) ? 0 :margin;
                }
                groupList.animate({
                    marginLeft: margin+'px'
                }, options.delay);
            });
            groupListOuter.find('.list-next, .crausal-next').off('click');
            groupListOuter.find('.list-next, .crausal-next').on('click', function() {
                groupListWidth = groupList.outerWidth();
                var groupListOuterWidth = groupListOuter.width();
                var maxMargin = groupListOuterWidth - groupListWidth;
                var margin = parseInt(groupList.css('margin-left'), 10);
                if(maxMargin > 0){
                    margin = 0;
                }else{
                    margin -= (groupListWidth - groupListOuterWidth  + margin > 0) ? options.slideWidth : 0;
                    margin = (margin < maxMargin) ? maxMargin : margin;
                }
                groupList.animate({
                    marginLeft: margin+'px'
                }, options.delay);
            });
            }
        }

        return {
            createCarousel: function(options){return createCarousel(options);}
        };
    });
});
