define([
    'common/trackingConfigService',
    'services/trackingService',
    'services/urlService',
    'services/utils'
], function(t, trackingService) {
    'use strict';
    Box.Application.addBehavior('galleryTracking', function(context) {

        var messages = [
            'trackGalleryPrevButtonClick',
            'trackGalleryNextButtonClick',
            'trackGalleryFullScreenClick',
            'trackGalleryCloseButtonClick',
            'trackGalleryBucketClick',
            'galleryVideoPlayed',
            'galleryVideoPaused',
            'galleryVideoSeeked',
            'trackOpenLeadFormGallery',
            'trackGalleryMSKnowMoreClick',
            'trackImageSlideLeadFormOpen',
            'trackGridViewAllImage',
            'trackImageZoomIn',
            'trackImageZoomOut',
            'trackGalleryImageSeenFromThumb'
        ];

        var moduleId = context.getElement().id;

        var onmessage = function(msgName, data) {
            if (data && data.moduleId && data.moduleId != moduleId) {
                return;
            }

            let event = t.GALLERY_INTERACTION_EVENT,
                category = t.GALLERY_CATEGORY,
                label, name,sourceModule,
                sourceModuleConfig = t.LEAD_SOURCE_MODULES,
                properties = {};

            switch (msgName) {
                case 'trackGalleryPrevButtonClick':
                    name = t.PREV_NAME;
                    label = t.LEFT_ARROW;
                    break;
                case 'trackGalleryNextButtonClick':
                    name = t.NEXT_NAME;
                    label = t.RIGHT_ARROW;
                    break;
                case 'trackGalleryFullScreenClick':
                    name = t.ZOOM_NAME;
                    break;
                case 'trackGalleryCloseButtonClick':
                    name = t.CLOSE_NAME;
                    break;
                case 'trackGalleryBucketClick':
                    name = t.CLICK_NAME;
                    label = data.bucketName;
                    if(data.groupList == 'imageThumb') {
                        label = `${label}_image`
                    }
                    break;
                case 'galleryVideoPlayed':
                    name = t.PLAY_LABEL;
                    sourceModule = data.sourceModule;
                    break;
                case 'galleryVideoPaused':
                    name = t.PAUSE_LABEL;
                    sourceModule = data.sourceModule;
                    break;
                case 'galleryVideoSeeked':
                    name = t.SCRUB_LABEL;
                    sourceModule = data.sourceModule;
                    break; 
                case 'trackOpenLeadFormGallery':
                    category = t.LEAD_FORM_CATEGORY;
                    label = data.label;
                    event = t.OPEN_EVENT;
                    if(data.source) {
                        sourceModule = sourceModuleConfig[data.source];
                    }
                    break; 
                 case 'trackGalleryMSKnowMoreClick':
                    event = t.CARD_CLICK;
                    label="Program_Details_Modal";
                    category='Makaan Select- Promo & Voucher Generation- buyers';
                    sourceModule = data.source;       
                    break;
                case 'trackImageSlideLeadFormOpen': 
                    category = t.LEAD_FORM_CATEGORY;
                    event = t.OPEN_EVENT;
                    label = t.GALLERY_POP_UP_LABEL;
                    properties[t.SOURCE_MODULE_KEY] = t.GALLERY_POP_UP_LABEL;
                    break;
                case 'trackGridViewAllImage':
                    label = "See_all_photos";
                    break;
                case 'trackImageZoomIn':
                    label = 'zoom in';
                    break;
                case 'trackImageZoomOut':
                    label = 'zoom out';
                    break;
                case 'trackGalleryImageSeenFromThumb':
                    label = t.THUMBNAIL_LABEL;
                    break;
            }

            properties[t.CATEGORY_KEY] = category;
            properties[t.NAME_KEY] = name;
            properties[t.LABEL_KEY] = label;
            properties[t.ADDITIONAL_CD49] = data.totalImagesCount;
            properties[t.ADDITIONAL_CD53] = data.viewedImageCount;

            if(sourceModule && !(sourceModule.startsWith("http")))
            {
                    properties[t.SOURCE_MODULE_KEY] = sourceModule;
            }

            trackingService.trackEvent(event, properties);
        };

        return {
            messages: messages,
            onmessage: onmessage,
            init: function() {},
            destroy: function() {}
        };
    });
});
