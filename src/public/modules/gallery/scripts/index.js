'use strict';
define([
    "text!modules/gallery/views/index.html",
    "text!modules/gallery/views/moduleTmpl.html",
    'vendor/lightgallery/lightgallery',
    'modules/gallery/scripts/services/jwPlayerHelper',
    'services/utils',
    'services/apiService',
    'modules/gallery/scripts/services/galleryService',
    'services/experimentsService',
    "common/sharedConfig",
    'modules/gallery/scripts/services/carousel',
    'services/commonService',
    'modules/gallery/scripts/behaviors/galleryTracking',
    "css!styles/vendor/lightgallery/lightgallery",
], function (html, moduleTmpl, lightgallery, jwPlayerHelper, utils, apiService, galleryService, experimentsService, sharedConfig) {
    Box.Application.addModule('gallery', function(context) {

        var pageType, pageCategory, trackAction, falseTrigger, VERSION = '';
        const MAIN_LEAD_FORM_ID = "main_lead_form",
        LEAD_FORM_GROUP_TAB_NAME = "Connect Now",
        SEARCH_SIMILAR_GROUP_TAB_NAME = "Search Similar",
        GALLERY_GRID_TIMEOUT = 16,
        OWL_CONFIG = sharedConfig.OWL_CAROUSEL_CONFIG;

        var $,
            config = {},
            moduleEl,
            $moduleEl,
            instance,
            jwPlayerHelper,
            carousel,
            instances = {},
            playerInstances = {"jw": [], "yt": []},
            Application = Box.Application,
            galleryData = {},
            commonService = context.getService('CommonService'),
            dummyGallery,
            imagesSourceUrl,
            leadData={},
            isMobile = utils.isMobileRequest(),
            lastGroupName = LEAD_FORM_GROUP_TAB_NAME,
            currentlyViewImageIndex;
        /*
            Function to call the exposed functionality of jQuery plugin
            First Argument : Name of property/function to be called
            Second Argument : Arguments to pass to called function
        */
        function caller(property, parameters) {
            return (typeof instances[config.id][property] === "function") ? instances[config.id][property](parameters) : false;
        }

        function appendListingData(){
            let size, type, projectTitle;

            if(config.pageType == 'property' || config.pageType == 'project'){
                let configPageData = config.pageAllData || {};
                let extraData = config.extraData || {};
                size = extraData.size || '';
                type = configPageData.type;
                projectTitle = configPageData.projectTitle;
            } else if(config.pageType == 'serp') {
                let listingData = config.listingData;
                size = config.listingData.size || '';
                type = (listingData.bedrooms ? (listingData.bedrooms +  ' BHK ') : '') + listingData.propertyType;
                projectTitle = listingData.fullName ? listingData.fullName : '';
            }
            let imagePropInfoContainer = $moduleEl.find(".image-prop-info");
            imagePropInfoContainer.append(`<div class="prop-detail">${size} ${type} ${projectTitle}</div>`)
            
        }

        function appendLeadFormInGallery() {
            //Append lead form in last gallery slide
            var leadFormElm = $moduleEl.find("#temp-lead-container").clone();
            leadFormElm.find(".gallery_lead_form").attr("data-module", "lead");
            $moduleEl.find(".lg-inner>.lg-item:last").html(leadFormElm.html());
            $moduleEl.find(".lg-inner>.lg-item:last .lg-image").addClass('lead-item');
            //hide the count for  grouptab
            $moduleEl.find(".lg-group-outer-title .lg-group-item:last .lg-group-count").hide();
            $moduleEl.find(".lg-group-outer-image .lg-group-item:last").hide();

            commonService.stopAllModules(moduleEl);
            commonService.startAllModules(moduleEl);
        }

        function handleOuterBtnForLeadSlide(isLeadSlide) {
            let leadFormButton = $moduleEl.find(".js-leadForm"),
            leadFormBtnContainer = leadFormButton.parents(".lead-box"),
            msPromotion = $moduleEl.find(".image_gallery_container > .ms-promotion"),
            listingImageInfoContainer = $moduleEl.find(".image-prop-info"),
            lgOuter = $moduleEl.find(".lg-outer"),
            lgGroutTitle = $moduleEl.find(".lg-group-outer-title"),
            imageThumbNail = $moduleEl.find(".thumb-title"),
            leadFormSibling = $moduleEl.find(".lead-module").siblings(".lg-img-wrap"),
            lgInner = $moduleEl.find(".lg-outer .lg"),
            leadFormContainer = lgOuter.find(".gallery_lead_form");
            if(isLeadSlide && isMobile) {
                leadFormContainer.removeClass("hide");
                leadFormBtnContainer.hide();
                msPromotion.hide();
                lgGroutTitle.hide();
                listingImageInfoContainer.hide();
                imageThumbNail.hide();
                lgOuter.addClass("lg-prevent"); // is used to avoid the execution of events inside "enableSwipe function" in lightgallery which calls event preventDefault.
                if(["v2_mobile", "v3_mobile"].indexOf(VERSION) > -1){
                    lgInner.addClass("white-background");
                }
            } else if(isLeadSlide) {
                leadFormContainer.removeClass("hide");
                leadFormButton.addClass("disabled");
            } else if(isMobile) {
                leadFormContainer.addClass("hide");
                leadFormBtnContainer.show();
                msPromotion.show();
                lgGroutTitle.show();
                listingImageInfoContainer.show();
                imageThumbNail.show();
                lgOuter.removeClass("lg-prevent");
                if(["v2_mobile", "v3_mobile"].indexOf(VERSION) > -1){
                    lgInner.removeClass("white-background");
                }
            } else {
                leadFormButton.removeClass("disabled");
            }
        }

        /*
            Function to create the gallery instance
        */
        function createGallery(el,startIndex) {
            var moduleId = "#"+config.id,
                selectWithin = (config.selectWithin) ? config.selectWithin : moduleId+" .image_gallery_container",
                selector = (config.selector) ? config.selector : ".image_gallery li";
                if(config.mobile == "false"){
                    config.mobile = false;
                }
                if(config.mobile == "true"){
                    config.mobile = true;
                }
            var $lg = el.lightGallery({
                selector: selector,
                //preload: config.preload || 3,
                selectWithin: selectWithin,
                thumbnail: config.thumbnail || false,
                pager: config.pager || false,
                hash: true,
                group: config.group || false,
                groupEl: getGroups(),
                index: (startIndex > 0 && startIndex) || ((config.index > 0) ? config.index : 0),
                closable: config.closable || false,
                autostart: config.autostart && true,
                escKey: config.escKey || config.fullsize || false,
                onBody: config.onBody && true,
                fullsize: config.fullsize || false,
                keyPress: true,
                enableDrag: false,
                galleryData: galleryData,
                mobile: config.mobile,
                version: VERSION || '',
                zoom: ["v2_mobile", "v3_mobile"].indexOf(VERSION) > -1,
                showDescCont: (["v2_mobile", "v3_mobile"].indexOf(VERSION) > -1)
            });

             $lg.on('onAfterOpen.lg', function() {
                if(config.mobile){
                    $lg.find('.lg-group-outer').addClass('mobile');
                    if(VERSION == "v3_mobile"){
                        $lg.find(".lg-group-outer-title").addClass("hide");
                        $(moduleEl).find('.image-prop-info').insertAfter(".lead-box");
                    }
                }
                var item = $lg.find('.title .lg-group-item'),
                    itemContainer = $lg.find('.title .lg-group-list');
                var data = {
                    items : item,
                    itemContainer : itemContainer,
                    delay: 500,
                    slideWidth: 130,
                    mobile: config.mobile,
                    noOuter: true
                };
                carousel.createCarousel(data);
                // itemContainer.css("width", parseInt(itemContainer.css("width")) + 80);
                var item1 = $lg.find('.lg-group-outer-image .lg-group-item'),
                    itemContainer1 = $lg.find('.lg-group-outer-image .lg-group-list');
                var data1 = {
                    items : item1,
                    itemContainer : itemContainer1,
                    delay: 500,
                    slideWidth: 130,
                    mobile: config.mobile,
                    noOuter: true
                };
                carousel.createCarousel(data1);
                commonService.startAllModules(context.getElement());
                appendLeadFormInGallery();
                appendListingData();
                
                if(config.mobile){
                    context.broadcast("destroyGrid", {
                        id: "galleryGrid"
                    });
                }
            });
            $lg.on('onAferAppendSlide.lg', function(event, index,fromPreload) {
                aferAppendSlide(event, index,fromPreload);
            });
            $lg.on('onBeforeSlide.lg', function(event, prevIndex, index) {
                currentlyViewImageIndex = index;
                pauseJWPlayer();
                let galleryData = getData();
                //Lead form slide
                if(galleryData && galleryData.length && index === galleryData.length -1) {
                    handleOuterBtnForLeadSlide(true);
                } else if (galleryData && galleryData.length && prevIndex === galleryData.length - 1){
                    handleOuterBtnForLeadSlide(false);
                }
            });
            $lg.on('onAfterSlide.lg', function(event, prevIndex, index, fromTouch, fromThumb) {
                let galleryData = getData(),
                    imageSrc,
                    viewedImageCount = galleryData.filter((image) => { return image.seen === true; }).length;
                if(galleryData && galleryData[index] && galleryData[index].src && galleryData[index].src !== LEAD_FORM_GROUP_TAB_NAME){
                    imageSrc = galleryData[index].src;
                    galleryData[index].seen = true;
                }
                if(prevIndex !== index && fromThumb){
                    moveGroupTitleCarousel(prevIndex, index);
                }
                if(galleryData && index === galleryData.length -1) {
                    context.broadcast('trackImageSlideLeadFormOpen',{moduleId: moduleEl.id, totalImagesCount: galleryData.length-1, viewedImageCount: viewedImageCount });
                }
                //Lead form slide
                if(galleryData && galleryData.length && index !== galleryData.length -1) {
                    handleOuterBtnForLeadSlide(false);
                }

                if(fromThumb) {
                    //Track image seen from thumbnail view
                    context.broadcast('trackGalleryImageSeenFromThumb',{
                        moduleId: moduleEl.id, 
                        totalImagesCount: galleryData.length-1, 
                        viewedImageCount: viewedImageCount 
                    });
                }
            });
            $lg.on('onBeforeClose.lg', function() {
                beforeDestroy();
            });
            $lg.on('onCloseAfter.lg', function() {
                let moduleElLightGallery = $moduleEl.data('lightGallery');
                if(config.gallery && moduleElLightGallery) {
                    moduleElLightGallery.destroy(true);
                    moduleElLightGallery.modules.hash.destroy();
                }
                $lg.off("onCloseAfter.lg");
                $lg.off("onAfterSlide.lg");
                $lg.off("onBeforeSlide.lg");
                $lg.off("onAferAppendSlide.lg");
                $lg.off("onAfterOpen.lg");
                $lg.off("onAfterOpen.lg");
                $(window).off("keyup.lg");
                cleanUpActivity();
                context.broadcast('gallery_destroyed', {
                    data: $lg.data('lightGallery')
                });
            });
            $lg.on('imageZoomIn', function(){
                context.broadcast("trackImageZoomIn",{moduleId: moduleEl.id})
            });
            $lg.on('imageZoomOut', function(){
                context.broadcast("trackImageZoomOut",{moduleId: moduleEl.id})
            });
        }

        /*
            Function elementInViewport
        */
        function elementInViewport(element) {
            var _getStyle = function(el, property) {
                if ( window.getComputedStyle ) {
                    return document.defaultView.getComputedStyle(el,null)[property];
                }
                if ( el.currentStyle ) {
                    return el.currentStyle[property];
                }
            };
            var _isVisible = function(el, t, r, b, l, w, h) {
                var p = el.parentNode,
                        VISIBLE_PADDING = 2;
                //Either document has been reached, or a parent with fixed position has been reached
                //In both cases the bottom up search need not to be continued and element should be
                //considered visible.
                if ( 9 === p.nodeType ||
                    'fixed' ===_getStyle(p, 'position') ) {
                    return true;
                }

                if (
                    'undefined' === typeof(t) ||
                    'undefined' === typeof(r) ||
                    'undefined' === typeof(b) ||
                    'undefined' === typeof(l) ||
                    'undefined' === typeof(w) ||
                    'undefined' === typeof(h)
                ) {
                    t = el.offsetTop;
                    l = el.offsetLeft;
                    b = t + el.offsetHeight;
                    r = l + el.offsetWidth;
                    w = el.offsetWidth;
                    h = el.offsetHeight;
                }
                if ( p ) {
                    if ( ('hidden' === _getStyle(p, 'overflow') || 'scroll' === _getStyle(p, 'overflow')) ) {
                        if (
                            l + VISIBLE_PADDING > p.offsetWidth + p.scrollLeft ||
                            l + w - VISIBLE_PADDING < p.scrollLeft ||
                            t + VISIBLE_PADDING > p.offsetHeight + p.scrollTop ||
                            t + h - VISIBLE_PADDING < p.scrollTop
                        ) {
                            return false;
                        }
                    }
                    if ( el.offsetParent === p ) {
                        l += p.offsetLeft;
                        t += p.offsetTop;
                    }
                    return _isVisible(p, t, r, b, l, w, h);
                }
                return true;
            };
        return _isVisible(element);
        }


        /*
            Function to move image group name Carousel
        */
        function moveGroupTitleCarousel(prevIndex, index){
            var currentIndex = $moduleEl.find('.lg-item').index($moduleEl.find('.lg-current')),
                groupElement = $moduleEl.find('.title .lg-group-item'),
                groupActiveElement;
            groupElement.each(function(index){
                var dataIndex = groupElement.eq(index).data("index");
                if(currentIndex < dataIndex){
                    groupActiveElement = groupElement.eq(index-1);
                    return false;
                } else if(currentIndex == dataIndex){
                    groupActiveElement = groupElement.eq(index);
                    return false;
                }
            });
            if (groupActiveElement && groupActiveElement.get(0) && config.mobile){
                //In mobile, scroll of thumb images and group title will work
                groupActiveElement.get(0).scrollIntoView({ inline: "center", behavior: "smooth" });
            } else if(groupActiveElement && !elementInViewport(groupActiveElement[0]) ){
                //In desktop, margins will need to be set
                var marginLeft, marginValue;
                marginLeft = parseInt($moduleEl.find('.lg-group-outer-title .lg-group-list').css('margin-left'),10);
                marginValue = ((index - prevIndex) > 0) ? -(groupActiveElement.outerWidth(true)) : groupActiveElement.outerWidth(true);
                marginLeft = (index === 0) ? 0 : marginLeft + marginValue;
                $moduleEl.find('.lg-group-outer-title .lg-group-list').css({
                    'margin-left': marginLeft+'px'
                });
            }
        }

        /*
            Function to handel after slide append event
        */
        function aferAppendSlide(event, index,fromPreload) {
            hasPlayer(event, index,fromPreload);
        }

        /*
            Function to check has jwPlayer in slide
        */
        function hasPlayer(event, index,fromPreload) {
            var galleryItem = $moduleEl.find("li.lslide");
            galleryItem = (galleryItem.length > 0) ? galleryItem : $moduleEl.find(".image_gallery li");
            var data = galleryItem.eq(index).data();
            if (data.vsrc && !fromPreload) {
                var jwPlayerid = 'jwPlayer' + index;
                $(".lg-item").eq(index).find(".lg-video-cont  .lg-video").html("<div id='" + jwPlayerid + "'></div>");
                playVideo(data.thumb, data.vsrc, jwPlayerid);
            }
        }
        /*
            Creates the instance of moudle and initialize same
        */
        function createInstance(startIndex) {
            var moduleId = "#"+config.id,
                instanceSelector = $(moduleId);
            if (instanceSelector.length <= 0) {return;}
            createGallery(instanceSelector,startIndex);
        }

        /*
            Function to get the items group
        */
        function getGroups() {
            let groups = galleryData["group"];
            groups[lastGroupName] = 1;
            return groups;
        }
        /*
            Function to create the jwPlayer instance
            First Argument : image/poster url
            Second Argument : video url
            Third Argument : element id
        */

        function playVideo(imgUrl, videoUrl, elementId, size) {
            var wheight = size ? size.height : '100%';
                elementId = elementId ? elementId : "jwplayer";
            var setupObj = {
                divId: elementId,
                image: imgUrl,
                fileName: videoUrl,
                height: wheight,
                width: '100%',
                autostart: true,
                background: 'transparent',
                backgroundColor: 'transparent',
                wmode: 'transparent'
            };
            if (imgUrl) {
                setupObj.image = imgUrl;
            }
            jwplayer.key = "cAr98kB89aA0ZhpVmJqMLQGxXGRUXKjW3Z9DlufTILI=";
            jwPlayerHelper.jwPlayer.initJwPlayer(setupObj);
            playerInstances.jw.push(elementId);
        }

        /*
            Function to pause all instances of JW Player
        */
        function pauseJWPlayer(){
            var len = playerInstances.jw.length;
            for(var k=0; k<len; k++){
                var playerInstanceId = playerInstances.jw[k];
                // console.log($(".lg-on .lg-outer #"+playerInstanceId).find("#"+playerInstanceId+"_display_button").css("opacity"));
                // ($(".lg-on .lg-outer #"+playerInstanceId).find("#"+playerInstanceId+"_display_button").css("opacity") == 0) ? jwplayer(playerInstanceId).pause() : "";
                jwplayer(playerInstanceId).stop();
            }
        }
        /*
            Function to get service Data
        */
        function getData(){
            var data = galleryData["data"];
            if(data.length <= 0) {return;}
            return data;
        }
        /*
            Function to load dynamic service data
        */
        function loadData(){
            let promise = null;
            if(imagesSourceUrl){
                promise = apiService.get(imagesSourceUrl).then((response)=>{
                    galleryData = config.galleryData = response;
                });
            } else {
                promise = new Promise((resolve) => {
                    galleryData = config.galleryData;//JSON.parse(galleryData);
                    resolve(galleryData);
                });
            }

            return promise.then(function(){
                if(galleryData.data && galleryData.data[galleryData.data.length-1] && 
                    galleryData.data[galleryData.data.length-1].id !== -1){
                    //Pushing element for lead form
                    galleryData.data.push({
                        id: -1,
                        subHtml: "",
                        src: "//:0",
                        thumb: "//:0",
                        alt: LEAD_FORM_GROUP_TAB_NAME,
                        title: LEAD_FORM_GROUP_TAB_NAME
                    });
                }
            });
        }
        /*
            Function to generate gallery template
        */
        function generateTemplate(){
            var serviceData = getData(),
                moduletmpl = doT.template(moduleTmpl),
                dynamicHtml = moduletmpl(serviceData);
            return dynamicHtml;
        }
        /*
            Function to initialize the module instance
        */
        function initializeModule(id, startIndex){
            if(id == config.id){
                loadData().then(() => {
                    var dynamicHtml = generateTemplate();
                    $moduleEl.removeClass("hide");
                    $moduleEl.find(".image_gallery_container").removeClass("hide");
                    $moduleEl.find(".image_gallery").prepend(dynamicHtml);
                    window.setTimeout(function(){
                        createInstance(startIndex);
                    }, 0);
                });
            }
        }

        /*
            Function to destroy module instance
        */
        function destroyModule(id){
            if(config.id === id) {
                (config.gallery) ? $moduleEl.data('lightGallery') && $moduleEl.data('lightGallery').destroy(true) : "";   // jshint ignore:line
                cleanUpActivity();
            }
        }

        /*
            Function to clean up module fragments
        */
        function cleanUpActivity(){
            $moduleEl.find('.image_gallery').empty();
            $moduleEl.find(".lSGroup").remove();
            $moduleEl.find(".image_gallery_container").removeAttr("style");
            $moduleEl.addClass("hide");
        }

        function beforeDestroy() {
            // destroy jwplayer instances if any
            pauseJWPlayer();
            // clear jwplayer instance array
            playerInstances.jw.length = 0;
        }
        function loadImage(startIndex, endIndex){
            $('.lg-group-outer-image .lg-group-list img').each(function(index){
                if(index >= startIndex && index <= endIndex && !$(this).attr('src') ){
                    $(this).attr('src', $(this).attr('data-src'));
                }
            });
        }
        function _openPopup(id) {
            context.broadcast('popup:open', {
                id: id,
                options: {
                    config: {
                        modal: true
                    }
                }
            });
        }
        function _openLeadForm(dataset) {
            let rawData = {
                cityId: dataset.cityid,
                salesType: dataset.listingcategory,
                listingId: dataset.listingid,
                localityId: dataset.localityid,
                projectId: dataset.projectid,
                companyId: dataset.sellerid,
                companyName: dataset.sellername,
                companyPhone: dataset.sellerphone,
                companyRating: dataset.sellerrating,
                companyImage: dataset.sellerimage,
                companyAssist: dataset.sellerassist,
                companyUserId: dataset.selleruserid,
                companyType: dataset.sellertype,
                projectName: dataset.projectname,
                projectOverviewUrl: dataset.projectoverviewurl,
                propertyType: [dataset.propertytypeid],
                builderId: dataset.builderid,
                rank: dataset.rank,
                bhk: dataset.bhk,
                projectStatus: dataset.projectstatus,
                budget: dataset.budget,
                source: dataset.source,
                step: dataset.step,
                id: "lead-popup" ,
                isMakaanSelectSeller:dataset.ismakaanselectseller||false
            };

            context.broadcast('Lead_Open_Step', rawData);
            _openPopup('leadOverviewPopup');
        }
        function loadThumbImage(move){
            var leftMargin = parseInt($moduleEl.find('.lg-group-outer-image .lg-group-list').css('margin-left'), 10);
            var listWidth = $moduleEl.find('.lg-group-outer-image .lg-group-item').outerWidth(true);
            var carausalWidth = window.innerWidth - 2 * $moduleEl.find('.lg-group-outer-image span').outerWidth();
            var thumbsInView = parseInt(carausalWidth / listWidth, 10);
            var firstFrameIndex =  -1*leftMargin/(listWidth);
            var lastFrameIndex  = firstFrameIndex + thumbsInView;
            move == 'prev'? loadImage(firstFrameIndex-2, firstFrameIndex) : loadImage(lastFrameIndex, lastFrameIndex+2);   // jshint ignore:line
        }

        function getVersionedTemplate(viewData) {
            var template = doT.template(html);
            if(viewData) {
                viewData.version = VERSION;
            }
            return template(viewData);
        }

        function getViewData({data}){
            if(config.pageType == 'projectSerp') {
                return galleryService.getViewData(config, data, isMobile)
                .then(function(viewData) {
                    leadData = viewData.leadData;
                    return getVersionedTemplate(viewData);
                });
            }
            var viewData = galleryService.getViewData(config, data, isMobile);
            if(viewData && viewData.leadBoxData && viewData.leadBoxData.leadConfigData) {
                viewData.leadBoxData.leadConfigData.id = "image-lead-popup";    
                viewData.leadBoxData.leadConfigData.source = "gallery pop up";
                viewData.leadBoxData.leadConfigData.moduleId = moduleEl.id;
                lastGroupName = LEAD_FORM_GROUP_TAB_NAME;
            } else {
                lastGroupName = SEARCH_SIMILAR_GROUP_TAB_NAME;
            }
            return getVersionedTemplate(viewData);
        }

        function appendDynamicHtml(dynamicHtml,data){
            $moduleEl.html(dynamicHtml);
            initializeModule(data.id,data.startIndex);
        }

        return {
            /**
             * Initializes the module and caches the module element
             * @returns {void}
             */
            init: function() {
                $ = context.getGlobal('jQuery');
                dummyGallery = $('.dumy-seo-gallery');
                config = context.getConfig();
                var id = config.id;
                imagesSourceUrl = config.imagesSourceUrl;
                jwPlayerHelper = context.getService('JWPlayerHelper');
                carousel = context.getService('carousel');
                if (typeof config.id === "undefined") {return;}
                moduleEl = context.getElement();
                $moduleEl = $(moduleEl);
                context.broadcast('moduleLoaded', {
                    name: 'gallery',
                    id: id
                });
                if(!config.fullsize){
                    initializeModule(id);
                }
                falseTrigger = true;
            },

            /**
             * Destroys the module and clears references
             * @returns {void}
             */
            destroy: function() {
                var configScript = $moduleEl.find('script[type="text/x-config"]');
                $moduleEl.html(configScript);
                config = null;
                moduleEl = null;
                $moduleEl = null;
                $ = null;
                instance = null;
                jwPlayerHelper = null;
                carousel = null;
                instances = null;
                Application = null;
                galleryData=null;
                pageType = null;
                pageCategory = null;
                trackAction = null;
                falseTrigger = null;
            },

            /**
             * Handles the click event
             * @param {Event} event The event object
             * @param {HTMLElement} element The nearest element that contains a data-type attribute
             * @param {string} elementType The data-type attribute of the element
             * @returns {void}
             */
            onclick: function(event, element, elementType) {
                let dataset = $(element).data();
                let label,
                    pageDataModule = utils.getPageData('moduleName'),
                    source = pageDataModule && pageDataModule =="serp" ? 'Gallery_Serp':'Gallery_Property',
                    galleryData = getData();
                switch (elementType) {
                    case 'left-click':
                        context.broadcast('trackGalleryPrevButtonClick',{moduleId: moduleEl.id });
                        break;
                    case 'right-click':
                        context.broadcast('trackGalleryNextButtonClick',{moduleId: moduleEl.id });
                        break;
                    case 'gallery-full-screen':
                        context.broadcast('trackGalleryFullScreenClick',{moduleId: moduleEl.id });
                        break;
                    case 'gallery-close':
                        context.broadcast('trackGalleryCloseButtonClick',{moduleId: moduleEl.id });
                        break;
                    case 'list-element':
                        if(dataset.trackLabel){
                            let viewedImageCount;
                            if(galleryData && typeof galleryData.filter === "function") {
                                viewedImageCount = galleryData.filter((image) => { return image.seen === true; }).length;
                            }
                            context.broadcast('trackGalleryBucketClick', { bucketName: dataset.trackLabel ,moduleId: moduleEl.id,
                                totalImagesCount: galleryData.length-1, viewedImageCount: viewedImageCount, groupList: $(element).find('img').length > 0 ? 'imageThumb' : 'categoryBucket'});
                            if ($moduleEl.find("#owl-demo") && pageDataModule != 'serp') {
                                $('#owl-demo').owlCarousel(OWL_CONFIG);
                            }
                        }
                        
                        break;
                    case 'openLeadForm':
                         label = "gallery";
                         context.broadcast('trackOpenLeadFormGallery', {label,source: dataset.source});
                         _openLeadForm(dataset);
                        break;
                    case "start_module":
                    case "destroy_gallery":
                        Application.broadcast(elementType, {
                            "id": config.id
                        });
                        break;
                    case "remove":
                        caller(elementType);
                        break;
                    case "next-btn":
                        $(element).css('pointer-events', "none");
                        setTimeout(function(){
                            $(element).css('pointer-events', "auto");
                        }, 500);
                        if($(element).parent().hasClass('image-gallery')){
                            loadThumbImage('next');
                        }
                        break;
                    case "back-btn":
                        $(element).css('pointer-events', "none");
                        setTimeout(function(){
                            $(element).css('pointer-events', "auto");
                        }, 500);
                        if($(element).parent().hasClass('image-gallery')){
                            loadThumbImage('prev');
                        }
                        break;
                    case "close":
                        beforeDestroy();
                        $moduleEl.trigger("onCloseAfter.lg");
                        break;
                    case 'social-share':
                        if (config.pageType == 'serp' || config.pageType == 'projectSerp')
                           { context.broadcast('open_share_box', { facebookSend: true, url: dataset.url });}
                        break;
                    case "lead-popup":
                        label = "gallery";
                        context.broadcast('trackOpenLeadFormGallery', {label,source: dataset.source});
                        let rawData = {
                            cityId: leadData.cityId,
                            localityId: leadData.localityId,
                            projectId: leadData.projectId,
                            companyId: leadData.companyId,
                            companyName: leadData.companyName,
                            companyPhone: leadData.companyPhone,
                            companyRating: leadData.companyRating,
                            companyImage: leadData.companyImage,
                            companyAssist: leadData.companyAssist,
                            companyUserId: leadData.companyUserId,
                            companyType: leadData.companyType,
                            projectName: leadData.projectName,
                            builderId: leadData.builderId,
                            rank: leadData.rank,
                            step: dataset.step || leadData.step,
                            id: "lead-popup",
                            otherCompany: JSON.parse(decodeURI(leadData.otherCompany))
                          };
                         context.broadcast('Lead_Open_Step', rawData);
                         _openPopup('leadOverviewPopup');
                         break;
                     case 'galleryMSKnowMore':
                            context.broadcast('trackGalleryMSKnowMoreClick',{source});
                            let popupId = isMobile ? 'msKnowMorePopup':'msKnowMoreDeskPopup';
                            _openPopup(popupId);
                            break;
                    case 'searchSimilar':
                        context.broadcast('searchSimilarClicked',{source});
                        break;
                    case 'openGridView':
                        context.broadcast("trackGridViewAllImage",{
                            moduleId: moduleEl.id
                        });
                        // beforeDestroy();
                        // $moduleEl.trigger("onCloseAfter.lg");
                        setTimeout(function(){
                        context.broadcast("start_gallery", {
                            id: "galleryGrid",
                            "dataSourceUrl": imagesSourceUrl,
                            "listingData": config.listingData,
                            "leadFormData": config.leadFormData,
                            "serp": true,
                            "startIndex": currentlyViewImageIndex
                        })
                        }, GALLERY_GRID_TIMEOUT);
                        break;
                    default:
                        break;
                }
            },

            /*
                List of messages module will listen for
            */
            messages: ["start_gallery", "destroy_gallery", "galleryClientSideLoaded", 'popup:opened',"popup:closed"],

            behaviors: ['galleryTracking'],
            /*
                Handles the messages action
                @param {name}, name of message
                @param {data} , data for message
                returns
            */
            onmessage: function(name, data) {
                pageType = utils.getTrackPageCategory();
                pageCategory = 'Buyer_' + pageType;
                trackAction = 'Click_' + pageType + '_Images';
                switch (name) {
                    case "start_gallery":
                        // caller("start_gallery");
                        
                        if(data.id != moduleEl.id) return;
                        VERSION = data.version;
                        if(data.startIndex >= 0){
                            destroyModule(data.id);
                        }
                        imagesSourceUrl = data && data.dataSourceUrl || imagesSourceUrl;
                        var dynamicHtml;
                        if(config.pageType == 'serp' || config.pageType == 'projectSerp'){
                            config.leadFormData = data.leadFormData; 
                            config.listingData = data.listingData;
                        }

                        if(config.pageType == 'projectSerp'){
                            getViewData({data}).then((dynamicHtml) => {
                                if(dynamicHtml){
                                    appendDynamicHtml(dynamicHtml,data);
                                }
                            });
                            
                        }else{
                            dynamicHtml = getViewData({data});
                            appendDynamicHtml(dynamicHtml,data);
                        }
                        
                        break;
                    case "destroy_gallery":
                        destroyModule(data.id);
                        break;
                    case "galleryClientSideLoaded":
                        dummyGallery.removeClass('topLevel');
                        dummyGallery.css("display", 'none');
                        break;
                    default:
                        break;

                }
            }
        };

    });
});
