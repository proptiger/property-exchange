"use strict";
define([
    'services/commonService',
    'services/leadPyrService',
    'services/utils',
    'common/sharedConfig',
    'modules/lead/scripts/services/leadService',
], (commonService,leadPyrService,utils,sharedConfig) => {
    Box.Application.addModule('sellerSERPProfile', (context) => {
        let messages = [],
            behaviors = [],
            LeadService = context.getService('LeadService');
        let element, moduleId, $, $moduleEl, moduleConfig, sellerStatus='';

        function isMakaanSelect(){
            let makaanSelectCities = sharedConfig.makaanSelectCities; 
            let pageData= utils.getPageData();
            if(moduleConfig.isMakaanSelectSeller && moduleConfig.isMakaanSelectSeller.toString()=="true" && pageData.listingType=="rent" && makaanSelectCities.indexOf(pageData.cityId) > -1){
                return true;
            }
            return false;           
        }

        function openLeadForm() {
            let leadFormData = leadPyrService.getLeadFormRawData();
            leadFormData.source = 'SELLER_MAIN_PROFILE';
            leadFormData.isMakaanSelectSeller = isMakaanSelect();
            LeadService.getCallingNumber(moduleConfig.companyUserId,leadFormData.cityId,leadFormData.salesType).then((result)=>{
                let companyPhone;
                if(result && result.callingNumber){
                    companyPhone = result.callingNumber;
                }
                let rawData = {...leadFormData,...{
                    type: 'openLeadForm',
                    id: "lead-popup",
                    companyId : moduleConfig.companyId,
                    companyName : moduleConfig.companyName,
                    companyRating : moduleConfig.rating,
                    companyPhone : moduleConfig.phone,
                    companyPhone: companyPhone,
                    companyImage : moduleConfig.image,
                    companyType : 'Agent',
                    companyUserId : moduleConfig.companyUserId
                }};
                if(utils.isMobileRequest()){
                  rawData.step="CALL_NOW";  
                }
            context.broadcast('leadform:open', rawData);
            })
            
        }

        function init() {
            //initialization of all local variables
            element = context.getElement();
            moduleId = element.id;
            $ = context.getGlobal('jQuery');
            moduleConfig = context.getConfig();
            $moduleEl = $(element);
            if(moduleConfig.isDealMaker||moduleConfig.isExpertDealMaker){
                sellerStatus = 'paid'
            } 
            let ratingBarsModule = element.querySelector('[data-lazyModule="ratingBars"]');
            if(ratingBarsModule){
                commonService.bindOnModuleLoad('ratingBars', ()=>{
                    context.broadcast('reloadRatingBars', {
                        ratingCount: moduleConfig.ratingCount,
                        listingUserCompanyId: moduleConfig.companyId,
                        listingUserCompanyUserId: moduleConfig.companyUserId,
                        id: ratingBarsModule.id
                    });
                }, [ratingBarsModule.id]);
            }
            let sellerScoreModule = element.querySelector('[data-lazyModule="sellerScore"]');
            if(sellerScoreModule){
                commonService.bindOnModuleLoad('sellerScore', () => {
                    context.broadcast('reloadSellerScore', {
                        companyRating: moduleConfig.rating,
                        listingUserId: moduleConfig.companyUserId,
                        isSellerProfile: true,
                        id: sellerScoreModule.id
                    });
                }, [sellerScoreModule.id]);
            }
            if(!utils.isMobileRequest()){
                $moduleEl.on('mouseenter','[data-type="sellerScoreMainProfile"],[data-type="ratings"],[data-type="reviews"],[data-type="dealsclosed"],[data-type="rera"]',(event)=>{
                    let data =  event.target.dataset;
                    if(data && data.type){
                        trackToolTipHover(data.type);
                    }
                });
            }
        }

        function destroy() {
            //clear all the binding and objects
            if(!utils.isMobileRequest()){
                $moduleEl.off('mouseenter','[data-type="sellerScoreMainProfile"],[data-type="ratings"],[data-type="reviews"],[data-type="dealsclosed"],[data-type="rera"]')
            }
        }

        function onmessage(name, data) { //jshint ignore:line
            // bind custom messages/events
        }

        function onclick(event, element, elementType) {
            // bind custom messages/events
            switch(elementType){
                case 'showFullDesc': 
                    context.broadcast('popup:open', {id: 'brokerDescPopup'});
                break;
                case 'loadReviews':
                    commonService.bindOnModuleLoad('sellerReviews', ()=>{
                        context.broadcast('popup:open',{
                            id: 'sellerReviewsPopup'
                        });
                        context.broadcast('loadSellerReviews:fromSummary',{
                            companyId: moduleConfig.companyId,
                            companyName: moduleConfig.companyName,
                            companyUserId: moduleConfig.companyUserId,
                            companyImage:moduleConfig.image,
                            companyType : 'Agent',
                            isDealMaker: moduleConfig.isDealMaker,
                            isExpertDealMaker:moduleConfig.isExpertDealMaker,
                            source: 'reviewToolTip',
                            otherInfo: {
                                sellerStatus,
                                sellerRating: moduleConfig.rating
                            }
                        });
                    });   
                break;
                case 'lead-form':
                    event.preventDefault();
                    openLeadForm();
                    context.broadcast('trackSellerProfileConnect',{
                        label: sellerStatus=='paid'?'connectDMheader':'connectheader',
                        sellerId: moduleConfig.companyId,
                        sellerStatus,
                        sellerRating: moduleConfig.rating,
                        source:'Seller Main Profile',
                    });
                break;
                case 'sellerScoreMainProfile':
                case 'sellerScoreCountMainProfile':
                        context.broadcast('trackSellerScoreTooltipClicked');
                        break;
            }
        }
        function trackToolTipHover(elementType){
            context.broadcast('trackSellerProfileTooltip',{
                label: elementType,
                sellerId: moduleConfig.companyId,
                sellerStatus,
                sellerRating: moduleConfig.rating,
            })
        }
       
        return {
            init,
            messages,
            behaviors,
            onmessage,
            onclick,
            destroy
        };
    });
});
