
'use strict';
define([
    "services/loggerService",
    "services/commonService",
    "services/filterConfigService",
    "services/localStorageService",
    "services/searchService",
    "services/urlService",
    "services/utils",
    "common/sharedConfig",
    "modules/typeAhead/scripts/index",
    "modules/search/scripts/behaviors/searchTracking"
], function(logger, commonService, filterConfigService, localStorageService, searchService, urlService, utilService,sharedConfig) {
    Box.Application.addModule("search", function(context) {

        const config = context.getGlobal('config');

        const moduleElement = context.getElement(),
            moduleId = moduleElement.id;
        const EXPAND_SEARCH = 'search-expand',
            SEARCH_TYPEAHEAD_NAME = 'searchTypeAhead',
            RENT_PLACEHOLDER = 'pick location or project',
            BUY_PLACEHOLDER  = 'pick location, builder or project',
            ADD_MORE_PLACEHOLDER = 'add more',
            maxTags = 4,
            FILTER_TYPEAHEAD = 'filter-typeahead';

        var typeAheadId,
            searchWrap,
            searchStartFlag = true,
            searchBackSpaceFlag = true,
            searchSuggestionFlag = false,
            localConfig,
            selectedItems = {};

        function _isSearchBoxEmpty(selectedItems, query){
            if (Object.keys(selectedItems).length === 0 && !query) {
                return true;
            }
            return false;
        };

        function emptyTagContainer(){
            $(moduleElement).find('.tag-wrap').html('');
        }

        function returnCityId(){
            if (!selectedItems) { return; }
            let selectedIds = Object.keys(selectedItems);
            return selectedItems[selectedIds[0]] && selectedItems[selectedIds[0]].cityId || '';
        }

        function handleSelectAgentFilter(queryParams={},item={}){
            if(sharedConfig.makaanSelectCities.indexOf(item.cityId)<0){
                let postedBy = queryParams.postedBy && queryParams.postedBy.split(',')||[];
                let selectAgentIndex = postedBy.indexOf('selectAgent');
                if(selectAgentIndex > -1){
                    postedBy.splice(selectAgentIndex,1);
                }
                postedBy = postedBy.toString();
                queryParams.postedBy = postedBy;
            }
            return queryParams;
        }

        function _getSelectedItemRedirectUrl(item){
            let type = item.type, queryParams, redirectUrl = item.redirectUrl;
            if ((type.indexOf('OVERVIEW') == -1) && (type.indexOf('Typeahead-Suggestion') == -1)) {
                queryParams = filterConfigService.getSelectedQueryFilterKeyValObj();
                queryParams = handleSelectAgentFilter(queryParams,item);
                redirectUrl = urlService.changeMultipleUrlParam(queryParams, redirectUrl, true);
            }

            if((type.indexOf('OVERVIEW') == -1)){
                let prefix = utilService.getPageData('isMap') ||  (item.type && item.type.toLowerCase() === 'gp' && !utilService.isMobileRequest()) ? '/maps/': '';
                if(prefix && redirectUrl.indexOf(prefix) < 0) {
                    redirectUrl = redirectUrl[0] == '/' ? redirectUrl.substr(1) : redirectUrl;
                    redirectUrl = `${prefix}${redirectUrl}`;
                }
            }
            return redirectUrl;
        }

        var getSearchUrl = function (eligibleForMultiSelect){
            if (!selectedItems) { return; }
            let selectedIds = Object.keys(selectedItems);
            if (selectedIds.length == 1) {
                searchService.abortAllPendingAPis();
                let newUrl = _getSelectedItemRedirectUrl(selectedItems[selectedIds[0]]);
                selectedIds = [];
                logger.info("Redirecting to " + newUrl);
                if (!eligibleForMultiSelect) {
                    selectedItems = {};
                }
                return newUrl;
            } else if (selectedIds.length > 1) {
                searchService.abortAllPendingAPis();
                let urlParams = {
                    skipPageData: true,
                    overrideParams: searchService.getCityLocalitySuburbOverrideData(selectedItems)
                }
                urlParams.overrideParams['cityId'] = returnCityId();
                return filterConfigService.getUrlWithUpdatedParams(urlParams);
            } else {
                logger.error('selected items : null');
                return '';
            }
        }

        var searchClick = function (eligibleForMultiSelect) {
            urlService.ajaxyUrlChange(getSearchUrl(eligibleForMultiSelect));
        };

        function _updatePlaceHolder(listingType){
            listingType = listingType || utilService.getPageData('listingType');
            var category = listingType ? listingType.toLowerCase() : '';
            if(category == 'rent'){
                $(searchWrap).find('[data-lazyModule="typeAhead"]').find('input').attr('placeholder', RENT_PLACEHOLDER);
            }else if(category == 'buy'){
                $(searchWrap).find('[data-lazyModule="typeAhead"]').find('input').attr('placeholder', BUY_PLACEHOLDER);
            }
        }

        return {
            messages: [
                'pageDataListingTypeChange',
                'triggerSearch',
                'searchResultClicked',
                'searchKeyBackspaced',
                'typeaheadInputFocusin',
                'typeAheadOverlayClicked',
                'searchKeyTyped',
                'searchResultPopulated',
                'searchCleared',
                'tagExpandBtnClicked',
                'searchResultCanceled',
                'addSelectedItemToList',
                'typeAheadModuleInitiated',
                'filterLocationUpdated',
                'resetSelectedList'
            ],
            behaviors: ['searchTracking'],

            /**
             * Initializes the module and caches the module element
             * @returns {void}
             */
            init: function() {
                searchWrap = $(moduleElement).find('.search-wrap');
                if(moduleElement.querySelector('[data-lazyModule="typeAhead"]')){
                    typeAheadId = moduleElement.querySelector('[data-lazyModule="typeAhead"]').id;
                } else {
                    typeAheadId = moduleElement.querySelector('[data-module="typeAhead"]').id;
                }
                
                logger.info('initializing search module');
                localConfig = context.getConfig() || {};
            },

            onmessage: function(name, data) {

                let dataId = data && data.moduleEl && data.moduleEl.id ? data.moduleEl.id : data.id;
                if(name == "typeAheadModuleInitiated" && data && data.config && (data.config.name === SEARCH_TYPEAHEAD_NAME) && dataId === 'homePageSearchBox'){
                    selectedItems = {};
                } else if (name == "filterLocationUpdated" && data.searchModuleId == moduleElement.id) {
                    let cityId = returnCityId() || utilService.getPageData('cityId');
                    let newUrl = getSearchUrl(false);
                    newUrl = newUrl==""?data.tempHref : newUrl;
                    let oldFilterSet = filterConfigService.getUrlFilterKeyValueObj(newUrl);
                    let updatedFilterSet = filterConfigService.getUrlFilterKeyValueObj(data.tempHref);
                    if(cityId){
                        updatedFilterSet = handleSelectAgentFilter(updatedFilterSet,{cityId});
                    }
                    newUrl = urlService.removeMultipleUrlParam(oldFilterSet, newUrl, true);
                    newUrl = urlService.changeMultipleUrlParam(updatedFilterSet, newUrl, true);
                    context.broadcast("newUrlAfterSelection", {
                        newUrl,
                        moduleId: moduleElement.id,
                        data
                    });
                    return;
                } else if (!data || !data.config || data.config.name != SEARCH_TYPEAHEAD_NAME || dataId != typeAheadId) {
                    return;
                }

                var elementData =  data.dataset;

                switch (name) {
                    case 'searchKeyTyped':
                        if(searchStartFlag && data.searchParams && data.searchParams.query && data.searchParams.query.length >= 2) {
                            searchStartFlag = false;
                            searchSuggestionFlag = true;
                            searchBackSpaceFlag = true;
                            context.broadcast('searchModuleKeyTyped', {
                                moduleId: moduleId,
                                query: data.searchParams.query
                            });
                        }
                        break;
                    case 'searchResultPopulated':
                        if(!searchSuggestionFlag) {
                            return;
                        }
                        context.broadcast('searchModuleResultPopulated', {
                            moduleId: moduleId,
                            query: data.searchParams.query,
                            resultCount: data.resultCount,
                            results: data.response
                        });
                        break;
                    case 'searchResultClicked':
                        let allSelection = [];
                        data.tagsArray.forEach(selection => {
                            allSelection.push(`${selection.dataset.entityid}_${selection.dataset.tagtype}`);
                        });
                        context.broadcast('searchModuleResultClicked', {
                            moduleId: moduleId,
                            isClick: data.isClick,
                            isSearchButton: data.isSearchButton,
                            query: data.searchParams.query,
                            resultCount: data.resultCount,
                            index: data.dataset.index,
                            cityId:data.dataset.cityid,
                            localityId: data.dataset.localityid,
                            entityId: data.dataset.entityid,
                            entityType: data.dataset.tagtype,
                            allSelection: allSelection.join(','),
                            totalTagsCount: data.tagsArray.length
                        });
                    case 'addSelectedItemToList':
                        if(Object.keys(selectedItems).length == maxTags){
                            return;
                        }
                        let placeholder = ADD_MORE_PLACEHOLDER;
                        let item = searchService.getResultItemObject(elementData),
                            itemType = item.type.toUpperCase(), eligibleForMultiSelect = true;
                        if ((data.config.multiselect && data.config.multiselect.indexOf(itemType) == -1) || !data.config.multiselect) {
                            //empty if not multiselect category
                            selectedItems = {};
                            eligibleForMultiSelect = false;
                        }
                        selectedItems[item.id] = item;
                        if (typeAheadId == FILTER_TYPEAHEAD && utilService.isMobileRequest()) {
                            context.broadcast('filterTypeAheadUpdated', {selectedItems: selectedItems});
                        }
                        context.broadcast('newCityOrLocalitySearched',{selectedData:item});
                        if (Object.keys(selectedItems).length >= 4 || (typeAheadId == 'headerSearchBox' && utilService.isMobileRequest())) {
                            placeholder = '';
                        }
                        if (data.config.multiselect && data.config.multiselect.indexOf(elementData.tagtype) > -1) {
                            $(data.moduleEl).find('input').attr('placeholder', placeholder);
                        }
                        if (data.triggerSearch) {
                            searchClick(eligibleForMultiSelect);
                            $(moduleElement).removeClass(EXPAND_SEARCH);
                        }
                        break;
                    case 'resetSelectedList':
                        selectedItems = {};
                        if(data.tagsArray && data.tagsArray.length){
                            let placeholder = ADD_MORE_PLACEHOLDER;
                            data.tagsArray.forEach(tag => {
                                let item = searchService.getResultItemObject(tag.dataset),
                                itemType = item.type.toUpperCase();
                                selectedItems[item.id] = item;
                            });
                            if ((data.tagsArray.length == 1 && data.config.multiselect.indexOf(data.tagsArray[0].dataset.tagtype) == -1) || data.tagsArray.length == 4 || (typeAheadId == 'headerSearchBox' && utilService.isMobileRequest()) ){
                                placeholder = ''
                            } 
                            $(data.moduleEl).find('input').attr('placeholder', placeholder);
                        }
                        break;
                    case 'searchCleared':
                        searchStartFlag      = true;
                        searchSuggestionFlag = false;
                        break;
                    case 'searchResultCanceled':
                        delete selectedItems[elementData.tagid];
                        if (typeAheadId == FILTER_TYPEAHEAD && utilService.isMobileRequest()) {
                            context.broadcast('filterTypeAheadUpdated', {selectedItems: selectedItems});
                        }
                        let selectedItemsLength = Object.keys(selectedItems).length;
                        if (_isSearchBoxEmpty(selectedItems, data.searchParams.query)) {
                            _updatePlaceHolder();
                            context.broadcast('typeaheadSetFocus',{id:typeAheadId});
                        } else if (selectedItemsLength < 4) {
                            $(data.moduleEl).find('input').attr('placeholder', ADD_MORE_PLACEHOLDER);
                        }
                        if (!selectedItemsLength) {
                            selectedItems = {};
                            context.broadcast('searchListEmpty');
                        } else if(data.triggerSearch){
                            searchClick(true);
                        }
                        break;
                    case 'searchKeyBackspaced':
                        let tagsArray = data.tagsArray || [];
                        if(!searchBackSpaceFlag) {
                            return;
                        }
                        searchBackSpaceFlag = false;
                        context.broadcast('searchModuleKeyBackspaced', {
                            moduleId: moduleId,
                            query: data.inputElementValue,
                            resultCount: data.resultCount,
                            tagsArray: tagsArray
                        });
                        break;
                    case 'typeaheadInputFocusin':
                        $(moduleElement).addClass(EXPAND_SEARCH);
                        break;
                    case 'typeAheadOverlayClicked':
                        $(moduleElement).removeClass(EXPAND_SEARCH);
                        break;
                    case 'pageDataListingTypeChange':
                        selectedItems = {};
                        if(Object.keys(selectedItems).length < 1){
                            _updatePlaceHolder(data.listingType);
                        }
                        break;
                    case 'tagExpandBtnClicked':
                        $(moduleElement).addClass(EXPAND_SEARCH);
                        break;

                }
            },

            /**
             * Destroys the module and clears references
             * @returns {void}
             */
            destroy: function() {
                selectedItems = {};
            }
        };
    });
});
