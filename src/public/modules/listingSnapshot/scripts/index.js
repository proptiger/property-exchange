"use strict";
define([
    "common/sharedConfig",
    "services/commonService",
    "services/apiService",
    "services/urlService",
    'doT!modules/listingSnapshot/views/template'
], (sharedConfig, commonService, apiService, urlService, template) => {
    Box.Application.addModule('listingSnapshot', (context) => {
        var messages = ['loadListingSnapshots'];
        var element, moduleId, $, $moduleEl, moduleConfig;
        var MODULE_WRAPPER = '.js-listingSnapshot-wrapper';

        function render(data){
            $moduleEl.find(MODULE_WRAPPER).html(template(data));
            commonService.addImageObserver(element);
        }
        function init() {
            //initialization of all local variables
            element = context.getElement();
            moduleId = element.id;
            $ = context.getGlobal('jQuery');
            moduleConfig = context.getConfig() || {};
            $moduleEl = $(element);
            
            commonService.bindOnLoadPromise().then(() => {
                if(moduleConfig.listingData){
                    render(moduleConfig.listingData);
                }
                context.broadcast('moduleLoaded', {
                    'name': 'listingSnapshot',
                    'id': element.id
                });
            });
        }

        function destroy() {
            //clear all the binding and objects
        }

        function onmessage(name, data) {
            // bind custom messages/events
            switch(name){
                case 'loadListingSnapshots':
                    if (!data || data.moduleId!=element.id){
                        return;
                    }
                    let viewData = {
                        listings:[],
                        errorMessage: moduleConfig.errorMessage
                    };
                    let listingDataPending = false, projectDataPending= false;
                    //If listingInfo is available show the content else fetch listing using listingIds
                    if(data.listingInfo ){
                        //lisitngInfo need to have listings array with properties imageUrl, url, title, locality and price (unit and val)
                        viewData.listings = data.listingInfo;
                    }  
                    if(data.listingIds && data.listingIds.length>0) {
                        listingDataPending =  true;
                        apiService.get(sharedConfig.apiHandlers.getListingDetails({listingIds: data.listingIds}).url).then(response => {
                            if(response){
                                data.listingIds.map((listingId)=>{
                                    if(response[listingId] && response[listingId].viewData){
                                        viewData.listings.push(response[listingId].viewData);
                                    }
                                });
                            } 
                            listingDataPending = false;
                        }, ()=>{
                            listingDataPending = false;
                        })
                    } 
                    if(data.projectIds){
                        projectDataPending = true;
                        apiService.get(sharedConfig.apiHandlers.getMultipleProjectDetails({
                            projectIdList: data.projectIds
                        }).url).then(response=>{
                            if(response){
                                data.projectIds.map((projectId)=>{
                                    if(response[projectId]){
                                        viewData.listings.push(response[projectId])
                                    }
                                });
                            } 
                            projectDataPending = false;
                        }, ()=>{
                            projectDataPending = false;
                        })
                    }
                    let listingSnapshotRenderRetry = setInterval(()=>{
                        if(!listingDataPending && !projectDataPending){
                            render(viewData);
                            clearInterval(listingSnapshotRenderRetry);
                        }
                    },500)

                    
                break;
            }
        }

        function onclick(event, element, elementType) {
            // bind custom messages/events
            switch(elementType){
                case 'snapshot-card':
                    event.preventDefault();
                    let dataset = $(element).data();
                    context.broadcast('listingSnapshotClicked',{
                        id: moduleId
                    });
                    urlService.ajaxyUrlChange(dataset.href);
                break;
            }
        }

        return {
            init,
            messages,
            onmessage,
            onclick,
            destroy
        };
    });
});