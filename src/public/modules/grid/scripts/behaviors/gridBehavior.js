define([

], function() {
    "use strict";
    Box.Application.addBehavior('gridBehavior', function(context) {

        var _parent;

        var onmessage = function(name, element) {
            if (name === 'sortClicked') {
                element = $(element);
                $('#' + _parent.id + ' [data-type="sort"]').removeClass('active-up active-down');
                var classAdded = element.data('asc') ? 'active-up' : 'active-down';
                element.addClass(classAdded);
            }
        };

        return {
            messages: ['sortClicked'],
            onmessage: onmessage,
            init: function() {
                _parent = context.getElement();
            },
            destroy: function() {}
        };
    });
});
