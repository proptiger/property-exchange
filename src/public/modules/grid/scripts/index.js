"use strict";
define([
    'services/utils',
    'modules/grid/scripts/services/service',
    'modules/grid/scripts/behaviors/gridBehavior'
], () => {
    Box.Application.addModule('grid', (context) => {
        const SortService = context.getService('sortService'),
            queryData = Box.DOM.queryData,
            query = Box.DOM.query,
            HIDDEN_CLASS = 'hidden',
            CONTACT_SELLER_SELECTOR = 'contactseller-btn',
            CHECKBOX_SELECTOR = '.js-sellerCheck',
            queryAll = Box.DOM.queryAll,
            ALL_CHECKBOX_SELECTOR = '[data-type=allCheck]',
            DISABLED = 'disabled';

        var _currentIndex = 0,
            _grid,
            messages = ["grid_sort", "popup:change"],
            behaviors = ['gridBehavior'],
            config,
            moduleEl,
            configurationText,
            multipleSellerData;


        function init() {
            moduleEl = context.getElement();
            config = context.getConfig();
            _grid = getGridDetails(context);
            configurationText = config.leadConfig;
            if (config.rowsPerLoad < config.totalCount) {
                _showViewMore();
            }
        }

        function getGridDetails(context) {
            let grid = {};
            grid.el = context.getElement();
            grid.loaderEl = queryData(grid.el, 'type', 'loader');
            grid.api = config.api;
            grid.rowsPerLoad = config.rowsPerLoad;
            grid.appendToEl = query(grid.el, "[data-type='datagrid']");
            return grid;
        }

        function _replaceData(appendTo, data) {
            $(appendTo).html(data);
        }

        function _appendData(appendTo, data) {
            $(appendTo).append(data);
        }

        function _toggleLoader(show, loader) {
            if (show) {
                loader.className = loader.className.replace(HIDDEN_CLASS, '');
            } else {
                loader.className += ' ' + HIDDEN_CLASS;
            }
        }

        function _hideViewMore() {
            queryData(_grid.el, 'type', 'moreBtn').className += ' ' + HIDDEN_CLASS;
        }

        function _showViewMore() {
            $(queryData(_grid.el, 'type', 'moreBtn')).removeClass(HIDDEN_CLASS);
        }

        function _allCheckboxChange(currentElement) {
            let checkboxList = queryAll(_grid.el, CHECKBOX_SELECTOR);
            if (currentElement.checked) {
                let checkedLength = checkboxList.length;
                for (let i = 0; i < checkedLength; i++) {
                    checkboxList[i].checked = true;
                }
                _toggleConactSeller(true, checkedLength);
                context.broadcast('trackSelectAllGrid', {id: moduleEl.id, moduleEl});
            } else {
                for (let i = 0; i < checkboxList.length; i++) {
                    checkboxList[i].checked = false;
                }
                _toggleConactSeller(false);
            }
        }

        function _sellerCheckboxChange() {
            let checkedLength = queryAll(_grid.el, CHECKBOX_SELECTOR + ':checked').length,
                totalLength = queryAll(_grid.el, CHECKBOX_SELECTOR).length;
            if (checkedLength) {
                _toggleConactSeller(true, checkedLength);
                if (checkedLength == totalLength) {
                    query(_grid.el, ALL_CHECKBOX_SELECTOR).checked = true;
                } else {
                    query(_grid.el, ALL_CHECKBOX_SELECTOR).checked = false;
                }
            } else {
                _toggleConactSeller(false);
                query(_grid.el, ALL_CHECKBOX_SELECTOR).checked = false;
            }
        }

        function _toggleConactSeller(show, checkedLength = '') {
            let contactButton = queryData(_grid.el, 'type', CONTACT_SELLER_SELECTOR);
            if (show) {
                contactButton.className = contactButton.className.replace(DISABLED, '');
            } else {
                contactButton.className = contactButton.className + ' ' + DISABLED;
            }
            $(contactButton).text(`contact ${checkedLength} ` + (checkedLength > 1 ? `sellers` : `seller`));
        }

        function onmessage(name, data) {
            switch (name) {
                case "grid_sort":
                    if(data.targetId == _grid.el.id){
                        _toggleLoader(false, _grid.loaderEl);
                        if (data.category == 'replace') {
                            _replaceData(_grid.appendToEl, data.response);
                        } else if (data.category == 'append') {
                            _appendData(_grid.appendToEl, data.response);
                        }
                    }
                    break;
                case "popup:change":
                    if (data && data.el && data.el.id == "seller-detail-popup") {
                        _multipleSellerLead();
                    }
            }
        }

        function _multipleSellerLead() {
            if(multipleSellerData){
                context.broadcast('openmultipleSellerLead',multipleSellerData);
            }
            multipleSellerData = null;
        }

        function sortGrid(clickElement, grid) {
            let rowsPerLoad = grid.rowsPerLoad,
                api = grid.api,
                loader = grid.loaderEl,
                sortKey = $(clickElement).data('sortkey'),
                order = !$(clickElement).data('asc');

            _currentIndex = 0;

            $(clickElement).data('asc', order);

            _toggleLoader(true, loader);

            SortService.sort(api, sortKey, order, 'replace', _currentIndex, rowsPerLoad, {}, _grid.el.id);

            context.broadcast('sortClicked', clickElement);
        }

        function viewMore(clickElement, grid) {
            var rowsPerLoad = grid.rowsPerLoad,
                api = grid.api,
                loader = grid.loaderEl,
                element = grid.el;
            _currentIndex += rowsPerLoad;
            _toggleLoader(true, loader);
            SortService.sort(api, 'listingSellerCompanyScore', 'asc', 'append', _currentIndex, rowsPerLoad, config.params, _grid.el.id);
            query(element, ALL_CHECKBOX_SELECTOR).checked = false;
            if (_currentIndex > config.totalCount) {
                _hideViewMore();
            }

        }

        function _open2StepPyr(sellerIdArray){
            multipleSellerData = {
                sellerIdArray,
                configurationText,
                moduleEl
            };
            if(config.openedInPopup){
                context.broadcast('popup:close');
            } else {
                _multipleSellerLead();
            }
        }

        function onclick(event, clickElement, elementType) {
            let sellerIdArray;
            let dataset = $(clickElement).data();
            switch (elementType) {
                case 'sort':
                    sortGrid(clickElement, _grid);
                    break;
                case 'callBtn':
                    sellerIdArray = [{
                        id: dataset.companyId,
                        companyUserId:dataset.companyUserId
                    }];
                    _open2StepPyr(sellerIdArray);
                    break;
                case 'moreBtn':
                    viewMore(clickElement, _grid);
                    context.broadcast('trackViewMoreGrid', {moduleEl, clickElement});
                    break;
                case CONTACT_SELLER_SELECTOR:
                    if(!$(clickElement).hasClass(DISABLED)){
                        let checkedSellers = Box.DOM.queryAll(_grid.el, CHECKBOX_SELECTOR + ':checked'),currentSellerData={},
                            sellerIdArray = [];
                        for (let i = 0, length = checkedSellers.length; i < length; i++) {
                            currentSellerData = $(checkedSellers[i]).data();
                            sellerIdArray.push({
                                id:currentSellerData.sellerid,
                                companyUserId:currentSellerData.companyUserId
                            });
                        }
                        _open2StepPyr(sellerIdArray);
                        break;
                    }
            }
        }

        function onchange(event, element, elementType) {
            if (elementType == 'options') {
                let attribures = query(element, ':selected').data() || {},
                    sortKey = attribures.sortkey,
                    order = attribures.asc;
                SortService.sort(_grid.api, sortKey, order, 'replace');
            } else if (elementType == 'allCheck') {
                _allCheckboxChange( element);
            } else if (elementType == 'sellerCheckbox') {
                _sellerCheckboxChange();
            }
        }

        function destroy() {
            _grid = null;
        }

        return {
            init,
            messages,
            behaviors,
            onmessage,
            onclick,
            destroy,
            onchange
        };
    });
});
