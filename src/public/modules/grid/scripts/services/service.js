'use strict';
define(['services/apiService'], function() {
    Box.Application.addService('sortService', function(application) {
        var apiService = application.getService('ApiService'),
            sort = function(api, key, order, category, baseIndex, rowsToLoad, params, id) {
                var url = [api, '?key=', key, '&order=', order, '&baseIndex=', baseIndex, '&rowsToLoad=', rowsToLoad ].join('')+`&${$.param(params)}`;
                apiService.get(url).then(function(response) {
                    application.broadcast('grid_sort', {
                        response: response,
                        category: category,
                        targetId: id
                    });
                });
            };


        return {
            sort: sort
        };
    });
});
