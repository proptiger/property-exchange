'use strict';
define([
    'common/trackingConfigService',
    'services/trackingService'
], function(t, trackingService) {
    Box.Application.addBehavior('projectTrendTracking', function() {

        var messages = ['projectTrend_select', 'projectTrend_search'];
        var behaviors = ['projectTrendTracking'];

        var onmessage = function(name, data) {
            var category, label, value, sourceModule,
                event = t.PRICE_TREND_CATEGORY,
                properties = {};

            switch (name) {
                case 'projectTrend_search':
                    event = t.SELECT_EVENT;
                    label = data.projectId;
                    value = 1; //hardcoded 1 as only one can be selected at once
                    sourceModule = t.SEARCH_PROJECT_TREND_MODULE;
                    break;
                case 'projectTrend_select':
                    event = t.SELECT_EVENT;
                    label = data.projectId;
                    value = data.totalSelected;
                    if(data.category == 'Similar') {
                        sourceModule = t.SIMILAR_PROJECT_TREND_MODULE;
                    } else if (data.category == 'Shortlisted') {
                        sourceModule = t.SHORTLISTED_PROJECT_TREND_MODULE;
                    } else if (data.category == 'Recently Viewed') {
                        sourceModule = t.RECENTLY_VIEWED_PROJECT_TREND_MODULE;
                    }
                    break;
            }

            properties[t.CATEGORY_KEY] = category;
            properties[t.LABEL_KEY] = label;
            properties[t.VALUE_KEY] = value;
            properties[t.SOURCE_MODULE_KEY] = sourceModule;

            trackingService.trackEvent(event, properties);
        };

        return {
            messages,
            onmessage,
            behaviors,
            init: function() {},
            destroy: function() {}
        };
    });
});