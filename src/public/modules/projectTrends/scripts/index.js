"use strict";
define([
    "doT!modules/projectTrends/views/index",
    "common/sharedConfig",
    'services/apiService',
    'services/loggerService',
    'services/viewStatus',
    'services/shortlist',
    'services/commonService',
    'services/utils',
    'modules/projectTrends/scripts/behaviors/projectTrendTracking'
], function(templateFunction, sharedConfig) {
    Box.Application.addModule("projectTrends", function(context) {
        const messages = ['searchResultClicked', 'favourites-synced'],
            behaviors = ['projectTrendTracking'],
            apiService = context.getService('ApiService'),
            logger = context.getService('Logger'),
            viewStatusService = context.getService('viewStatus'),
            commonService = context.getService('CommonService'),
            //UtilService = context.getService('Utils'),
            shortlistService = context.getService('shortlistService'),
            totalGraphCount = 4,
            queryData = Box.DOM.queryData,
            recentlyViewedDiv = 'recently-viewed',
            shortlistDiv = 'shortlist',
            similarDiv = 'similar',
            apiHandlers = sharedConfig.apiHandlers,
            PROJECT_LIST = 'project-list',
            MODULE_ID = 'trendTypeAheadDiv',
            DURATION = 36,
            ACCORDIAN_LIST_COUNT = 3;

        var graphCount = 2,
            moduleEl,
            projectId,
            maxCountElement,
            noResultElement,
            comparedProjectIds = [],
            trackProjectId;


        // function _parsePriceTrend(response) {
        //     let result = {};
        //     result.series = [];
        //     result.id = 'chart-price-trend';
        //     for (var i = 0; i < response.length; i++) {
        //         for (var key in response[i]) {
        //             if (response[i].hasOwnProperty(key)) {
        //                 let res = response[i][key];
        //                 result.series.push({
        //                     name: key,
        //                     data: _parsePriceData(res)
        //                 });
        //             }
        //         }
        //     }
        //     return result;
        // }

        function _addProjectPriceTrend(response) {
            for (var key in response) {
                if (response.hasOwnProperty(key)) {
                    let res = response[key];
                    return {
                        name: key,
                        data: _parsePriceData(res),
                        id:'chart-price-trend'
                    };
                }
            }
            return {data:[]};
        }

        function _parsePriceData(data) {
            let result = [],
                dateArray = Object.keys(data).sort();
            for(let i=0, length=dateArray.length;i<length;i++){
                let currKey = parseInt(dateArray[i]);
                result.push([currKey, data[currKey]]);
            }
            return result;
        }

        function _getPriceTrend(projectId, prependName) {
            let url = apiHandlers.getProjectPriceTrend({
                projectId, query: {
                    duration: DURATION,
                    prependName: prependName
                }
            }).url;

            apiService.get(url).then((response) => {
                context.broadcast('chartDataAdded', _addProjectPriceTrend(response));
                comparedProjectIds.push(projectId);
                $("[data-projectid='"+projectId+"']").prop('checked', true);
            }, (error) => {
                logger.info(JSON.stringify(error));
                $(noResultElement).removeClass('hidden');
                setTimeout(()=>{
                    $(noResultElement).addClass('hidden');
                },2000);

            });
        }

        function _deletePriceTrend(seriesName) {
            context.broadcast('chartDataDeleted', {
                name: seriesName
            });
        }

        function _shortlistParser(listArray = [], projectId) { // jshint ignore:line
            var result = [];
            for (let i = 0, length = listArray.length; i < length; i++) {
                let currItem = listArray[i],
                    data = currItem.data || {};
                if(projectId!==currItem.projectId){
                    result.push({
                        projectId: currItem.projectId,
                        projectName: data.label,
                        builderName: data.builderName
                    });
                }
            }
            return result.slice(0, ACCORDIAN_LIST_COUNT);
        }

        function _recentViewParser(listArray = []) {
            var result = [];
            for (let i = 0, length = listArray.length; i < length; i++) {
                let currItem = listArray[i],
                    data = currItem.data || {};
                    result.push({
                        projectId: currItem.id,
                        projectName: data.label,
                        builderName: data.builderName
                    });

            }
            return result.slice(0, ACCORDIAN_LIST_COUNT);
        }

        function init() {
            logger.info('projectTrends init');
            moduleEl = context.getElement();
            maxCountElement = queryData(moduleEl, 'type', 'max-count-alert');
            noResultElement = queryData(moduleEl, 'type', 'no-result-alert');

            let config = context.getConfig() || {},
                projectId = parseInt(config.projectId),
                builderName = config.builderName,
                localityId = config.localityId,
                localityLat = config.localityLat,
                localityLng = config.localityLng,
                urlProjectTrend = apiService.get(apiHandlers.getProjectPriceTrend({
                    projectId, query: {
                        duration: DURATION,
                        prependName: `${builderName} `
                    }
                }).url),
                localityTrend = apiService.get(apiHandlers.getLocalityPriceTrend({
                    localityId: localityId,
                    query: {
                        lat: localityLat,
                        lon: localityLng,
                        duration: DURATION
                    }
                }).url),
                similarUrl = apiHandlers.getSimilarProject({
                    projectId: projectId,
                    query: {
                        count: ACCORDIAN_LIST_COUNT
                    }
                }).url;
            trackProjectId = projectId;

            commonService.startAllModules(moduleEl);
            commonService.loadLazyModules(moduleEl);
            comparedProjectIds.push(projectId);

            urlProjectTrend.then(response=>{
                context.broadcast('chartDataAdded', _addProjectPriceTrend(response));
            }, ()=>{
                context.broadcast('chartDataAdded',_addProjectPriceTrend({}));
            });

            localityTrend.then(response=>{
                context.broadcast('chartDataAdded', _addProjectPriceTrend(response));
            }, ()=>{
                context.broadcast('chartDataAdded', _addProjectPriceTrend({}));
            });

            //Similar Projects
            apiService.get(similarUrl).then((response) => {
                queryData(moduleEl, 'type', similarDiv).innerHTML = templateFunction({
                    response,
                    type: "Similar"
                });
            }, (error) => {
                logger.info(JSON.stringify(error));
            });

            //shortlist
            queryData(moduleEl, 'type', shortlistDiv).innerHTML = templateFunction({
                response: _shortlistParser(shortlistService.getAllShortlist('project', ACCORDIAN_LIST_COUNT+1), projectId),
                type: "Shortlisted"
            });

            let recentlyViewed = viewStatusService.recentlyViewed({
                type: 'project',
                numberOfItems: ACCORDIAN_LIST_COUNT + 1
            });
            recentlyViewed.shift();
            //recentlyViewed
            queryData(moduleEl, 'type', recentlyViewedDiv).innerHTML = templateFunction({
                response: _recentViewParser(recentlyViewed), //unshift as the first project recieved in as recently viewed
                type: "Recently Viewed"
            });
        }

        function destroy() {}


        function onclick(event, element, elementType) {
            if (elementType === PROJECT_LIST) {
                let data = $(element).data() || {},
                    prependName = data.buildername + ' ';
                if (element.checked) {
                    //context.broadcast('trackPriceTrends', trackProjectId + '_' + data.trackLabel + '_' + data.projectid);
                    addProject(parseInt(data.projectid), prependName, element);
                    context.broadcast('projectTrend_select', {
                        projectId: parseInt(data.projectid),
                        totalSelected: graphCount - 2, //as initially there are two graphs already present
                        category: data['track-label']
                    });

                } else {
                    removeProject({label:data.label, id:parseInt(data.projectid)});
                }
            }
        }

        function addProject(projectId, prependName, element) {
            if (graphCount <= totalGraphCount) {
                graphCount++;
                _getPriceTrend(projectId, prependName);

            } else {
                if (element) {
                    element.checked = false;
                }
                $(maxCountElement).removeClass('hidden');
                //alert(`count of ${totalGraphCount} reached`);
            }
        }

        function removeProject(projectData) {
            graphCount--;
            _deletePriceTrend(projectData.label);
            $(maxCountElement).addClass('hidden');
            $("[data-projectid='"+projectData.id+"']").prop('checked', false);
            let index = comparedProjectIds.indexOf(projectData.id);
            if(index>-1){
                comparedProjectIds.splice(index,1);
            }
        }


        function onmessage(name, data) {
            switch (name) {
                case 'searchResultClicked':
                    if (data.moduleEl.id == MODULE_ID) {
                        let dataset = data.dataset,
                            searchedProjectId =parseInt(dataset.projectid),
                            prependName = `${dataset.buildername} `;
                        if(comparedProjectIds.indexOf(searchedProjectId)==-1){
                            //context.broadcast('trackPriceTrends', trackProjectId + "_Search_" +searchedProjectId);
                            addProject(searchedProjectId, prependName);
                            context.broadcast('projectTrend_search', {
                                projectId: searchedProjectId
                            });
                        }

                    }
                    break;
                case 'favourites-synced':
                    queryData(moduleEl, 'type', shortlistDiv).innerHTML = templateFunction({
                        response: _shortlistParser(shortlistService.getAllShortlist('project', ACCORDIAN_LIST_COUNT+1), projectId),
                        type: "Shortlisted"
                    });


            }
        }

        return {
            init,
            messages,
            behaviors,
            onmessage,
            onclick,
            destroy
        };
    });
});
