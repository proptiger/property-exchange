define([
    'common/trackingConfigService',
    'services/trackingService',
    'services/urlService',
    'services/filterConfigService'
], function(t, trackingService, urlService, filterConfigService) {
    'use strict';
    Box.Application.addBehavior('errorPageTracking', function(context) {

        var element;
        const messages = [
            'trackErrorPage'
        ];

        var init = function() {
            element = context.getElement();
        };

        var onmessage = function(name, data) {
            let properties = {};
            let category, event, label, nonInteraction;

            let dataset = filterConfigService.getTrackPageFilters();
                
            switch (name) {
                case 'trackErrorPage':
                    category = data.page;
                    event = t.ERROR_EVENT;
                    nonInteraction = 1;
                    break;
            }

            properties[t.CATEGORY_KEY] = category;
            properties[t.LABEL_KEY] = label;
            // properties[t.VALUE_KEY] = value;
            properties[t.NON_INTERACTION_KEY] = nonInteraction;
            properties[t.CITY_ID_KEY] = dataset.cityId;
            properties[t.LOCALITY_ID_KEY] = dataset.localityIds;
            properties[t.SELLER_ID_KEY] = dataset.sellerId;
            properties[t.BUILDER_ID_KEY] = dataset.builderId;
            properties[t.PROJECT_ID_KEY] = dataset.projectId;
            properties[t.LISTING_CATEGORY_KEY] = t.makeValuesUniform(dataset.listingType,t.LISTING_CATEGORY_MAP) ;
            properties[t.BHK_KEY] = t.makeValuesUniform(dataset.beds,t.BHK_MAP) ;
            properties[t.BUDGET_KEY] = dataset.budget;
            properties[t.UNIT_TYPE_KEY] = t.makeValuesUniform(dataset.propertyType,t.UNIT_TYPE_MAP) ;
            properties[t.PROJECT_STATUS_KEY] = t.makeValuesUniform(data.projectStatus,t.PROJECT_STATUS_NAME) ;
            
            trackingService.trackEvent(event, properties);
        };

        return {
            messages,
            onmessage,
            init,
            destroy: function() {}
        };
    });
});
