"use strict";
define([
    'services/utils',
    'services/urlService',
    'modules/error/scripts/behaviors/errorPageTracking'
], function(utils, urlService) {
    Box.Application.addModule("error", function(context) {
        function init(){
            let element = context.getElement(),
                dataset = $(element).data();
            context.broadcast('trackErrorPage', {page: dataset.page});
        }
        function onclick(event, clickElement, elementType) {
            switch (elementType) {
                case 'refresh':
                    window.location.reload();
                    break;
                case 'try-again':
                    urlService.ajaxifySameUrl();
                    break;
            }
        }
        return {
            init,
            onclick,
            behaviors: ['errorPageTracking']
        };
    });
});
