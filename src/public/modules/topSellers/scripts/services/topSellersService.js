"use strict";
define([
    'doT!modules/topSellers/views/index', //include partials
    'doT!modules/topSellers/views/mobileTemplate', //include partials
    'doT!modules/topSellers/views/fallback-sidebar',
    'doT!modules/topSellers/views/project-locality-page', //include partials
    'doT!modules/topSellers/views/home-page',
    'services/utils'
], (desktopTemplate, mobileTemplate, fallbackTemplate, projectLocalityPage, homePage, utils) => {
    const SERVICE_NAME = "topSellersService";

    Box.Application.addService(SERVICE_NAME, () => {

        let isMobileRequest = utils.isMobileRequest();

        let loadedResults = [];

        let _getLabel = function(type, plural, {name, topSellers} = {}){
            let label = {title: "", subTitle: ""};
            let defaultPluralSubtitle = `Reputed sellers in your city who have recently closed deals and are recommended by others`;
            let defaultSingularSubtitle = `Reputed seller in your city who has recently closed deals and is recommended by others`;
            plural = utils.getVernacLanguage() ? '' : plural;
            switch(type){
                case "city":
                    label.title =`City Expert${plural}`;
                    label.subTitle = plural ? defaultPluralSubtitle : defaultSingularSubtitle;
                    break;
                case "locality":
                    label.title = `Locality Expert${plural}`;
                    label.subTitle = plural ? defaultPluralSubtitle.replace("city", "locality") : defaultSingularSubtitle.replace("city", "locality");
                    if(topSellers && topSellers.cityExpertCount){
                        label.title = `City Expert${plural}`;
                    }
                    break;
                case "project":
                    label.title = `Top Deal Maker${plural}`;
                    label.subTitle = `Seller${plural} in ${name} who have recently closed deals`;
                    break;
                case "projectOverview":
                    label.title = `Featured Seller${plural}`;
                    label.subTitle = "";
                    break;
            }
            return label;
        };

        let templateMap = {
            "bigCardTemplate": homePage,
            "rhsDesktopTemplate": desktopTemplate,
            "rhsMobileTemplate": mobileTemplate,
            "overviewTemplate": projectLocalityPage,
            "fallback": fallbackTemplate
        };

        let config = [{
            override: function(moduleConfig){
                return moduleConfig.override;
            },
            getTemplate: function(moduleConfig){
                return templateMap[moduleConfig.template];
            },
            query: function(moduleConfig){
                return moduleConfig.query;
            },
            sellerCount: function(moduleConfig){
                return moduleConfig.query;
            },
            waitForModules: function(moduleConfig){
                return moduleConfig.waitForModules;
            },
            getFallbackTemplate: function(moduleConfig){
                if(moduleConfig.fallback){
                    return templateMap["fallback"];
                } else {
                    return false;
                }
            }
        }, {
            // Home Page, City Overview page, Suburb Overview page
            pageDataKey: "pageType",
            value: ["HOME_PAGE_URLS", "CITY_URLS_OVERVIEW", "SUBURB_URLS_OVERVIEW"],
            template: homePage,
            query: {
                metaInfo: true,
                expertFilter: ["CityExpertDealMaker"],
                category: "All",
                sellerCount: 20
            },
            showDesignation: true,
            labels: function({cityName, topSellerLength}){
                let plural = `${topSellerLength > 1 ? "s" : ""}`;
                let lbls = _getLabel("city", plural);
                return {
                        title: {
                        main: lbls.title,
                        label: `in ${cityName}`
                    },
                    subTitle: lbls.subTitle
                };
            },
            fallbackTemplate: false
        }, {
            // Locality Overview page
            pageDataKey: "pageType",
            value: ["LOCALITY_URLS_OVERVIEW"],
            template: homePage,
            query: {
                metaInfo: true,
                expertFilter: ["LocalityExpertDealMaker"],
                category: "All",
                sellerCount: 20
            },
            showDesignation: true,
            labels: function({cityName, localityName, topSellerLength, topSellers}){
                let plural = `${topSellerLength > 1 ? "s" : ""}`;
                let lbls = _getLabel("locality", plural, {topSellers});
                return {
                        title: {
                        main: lbls.title,
                        label: `in ${localityName}`
                    },
                    subTitle: lbls.subTitle
                };
            },
            fallbackTemplate: false
        }, {
            // Project Overview Page
            pageDataKey: "pageType",
            value: ["PROJECT_URLS_OVERVIEW"],
            template: isMobileRequest ? mobileTemplate : desktopTemplate,
            query: {
                metaInfo: false,
                expertFilter: ["ExpertDealMaker", "DealMaker"],
                hideSpecificExperts: true
            },
            extraClasses: `${mobileTemplate?'cityexpert':''}`,
            labels: function({topSellerLength}){
                let plural = `${topSellerLength > 1 ? "s" : ""}`;
                let lbls = _getLabel("projectOverview", plural);
                return {
                        title: {
                        main: `${isMobileRequest ? lbls.title : ''}`,
                        label: ``
                    },
                    subTitle: lbls.subTitle
                };
            },
            fallbackTemplate: false
        }, {
            // City Serp, Suburb Serp page
            pageDataKey: "pageType",
            value: ["CITY_URLS"],
            template: isMobileRequest ? mobileTemplate : desktopTemplate,
            query: {
                metaInfo: false,
                expertFilter: ["CityExpertDealMaker"],
                sellerCount: 20
            },
            fallbackTemplate: fallbackTemplate,
            labels: function({cityName, topSellerLength}){
                let plural = `${topSellerLength > 1 ? "s" : ""}`;
                let lbls = _getLabel("city", plural);
                return {
                        title: {
                        main: lbls.title,
                        label: `in ${cityName}`
                    },
                    subTitle: lbls.subTitle
                };
            }
        }, {
            // Locality Serp
            pageDataKey: "pageType",
            value: ["LOCALITY_URLS"],
            template: isMobileRequest ? mobileTemplate : desktopTemplate,
            showDesignation: true,
            query: {
                metaInfo: false,
                expertFilter: ["LocalityExpertDealMaker"],
                sellerCount: 20
            },
            waitForModules: true,
            priority: 1,
            fallbackTemplate: fallbackTemplate,
            labels: function({localityName, suburbName, topSellerLength, topSellers}){
                let plural = `${topSellerLength > 1 ? "s" : ""}`;
                let lbls = _getLabel("locality", plural, {topSellers});
                return {
                        title: {
                        main: lbls.title,
                        label: `in ${localityName || suburbName}`
                    },
                    subTitle: lbls.subTitle
                };
            }
        },{
            // suburb Serp
            pageDataKey: "pageType",
            value: ["SUBURB_URLS"],
            template: isMobileRequest ? mobileTemplate : desktopTemplate,
            showDesignation: true,
            query: {
                metaInfo: false,
                expertFilter: ["SuburbExpertDealMaker"],
                sellerCount: 20
            },
            waitForModules: true,
            priority: 1,
            fallbackTemplate: fallbackTemplate,
            labels: function({localityName, suburbName, topSellerLength, topSellers}){
                let plural = `${topSellerLength > 1 ? "s" : ""}`;
                let lbls = _getLabel("locality", plural, {topSellers});
                return {
                        title: {
                        main: lbls.title,
                        label: `in ${localityName || suburbName}`
                    },
                    subTitle: lbls.subTitle
                };
            }
        }, {
            // Project Serp page
            pageDataKey: "pageType",
            value: ["PROJECT_URLS"],
            template: isMobileRequest ? mobileTemplate : desktopTemplate,
            fallbackTemplate: fallbackTemplate,
            query: {
                hideSpecificExperts: true,
                expertFilter: ["ExpertDealMaker", "DealMaker"],
                sellerCount: 12
            },
            labels: function({projectName, topSellerLength}){
                let plural = `${topSellerLength > 1 ? "s" : ""}`;
                let lbls = _getLabel("project", plural, {name: projectName});
                return {
                        title: {
                        main: lbls.title,
                        label: `in ${projectName}`
                    },
                    subTitle: lbls.subTitle
                };
            }
        }, {
            // Normal Serp page
            pageDataKey: "isSerp",
            value: true,
            template: isMobileRequest ? mobileTemplate : desktopTemplate,
            fallbackTemplate: fallbackTemplate,
            priority: 1,
            showDesignation: true,
            waitForModules: function(){
                let pageLevel = utils.getPageData("pageLevel");
                if(["suburb", "suburbTaxonomy", "locality", "localityTaxonomy", "multipleSuburbLocality"].indexOf(pageLevel) > -1){
                    return true;
                } else {
                    return false;
                }
            },
            labels: function({cityName, localityName, suburbName, topSellerLength, topSellers}){
                let plural = `${topSellerLength > 1 ? "s" : ""}`;
                let pageLevel = utils.getPageData("pageLevel");
                if(["suburb", "suburbTaxonomy", "locality", "localityTaxonomy"].indexOf(pageLevel) > -1){
                    let lbls = _getLabel("locality", plural, {topSellers});
                    return {
                            title: {
                            main: lbls.title,
                            label: `in ${localityName || suburbName}`
                        },
                        subTitle: lbls.subTitle
                    };
                } else if(["city", "cityTaxonomy"].indexOf(pageLevel) > -1){
                    let lbls = _getLabel("city", plural);
                    return {
                            title: {
                            main: lbls.title,
                            label: `in ${cityName}`
                        },
                        subTitle: lbls.subTitle
                    };
                } else if(["multipleSuburbLocality".indexOf(pageLevel) > -1]){
                    let lbls = _getLabel("locality", plural, {topSellers});
                    let label = '';
                    label += localityName ? `in ${localityName.split(',').join(', ')}` : '';
                    label += suburbName ? `${!suburbName ? `in`: ', '}${suburbName.split(',').join(', ')}` : '';
                    return {
                        title: {
                            main: lbls.title,
                            label
                        },
                        subTitle: lbls.subTitle
                    }
                }
            },
            query: function(){
                let pageData = utils.getPageData();
                let pageLevel = pageData["pageLevel"];
                let query = {
                    metaInfo: false
                };
                if(["locality", "localityTaxonomy"].indexOf(pageLevel) > -1){
                    query.expertFilter = ["LocalityExpertDealMaker"];
                    query.sellerCount = 20;
                }else if(["suburb", "suburbTaxonomy"].indexOf(pageLevel) > -1){
                    query.expertFilter = ["SuburbExpertDealMaker"];
                    query.sellerCount = 20;
                } else if(["city", "cityTaxonomy"].indexOf(pageLevel) > -1){
                    query.expertFilter = ["CityExpertDealMaker"];
                    query.sellerCount = 20;
                } else if (["multipleSuburbLocality"].indexOf(pageLevel) > -1){
                    if(pageData.suburbId && pageData.localityId){
                        query.expertFilter = ["LocalityExpertDealMaker", "SuburbExpertDealMaker"];
                    } else if(pageData.suburbId){
                        query.expertFilter = ["SuburbExpertDealMaker"];
                    } else {
                        query.expertFilter = ["LocalityExpertDealMaker"];
                    }
                    query.sellerCount = 20;
                }else {
                    query.expertFilter = ["ExpertDealMaker", "DealMaker"];
                    query.hideSpecificExperts = true;
                    query.sellerCount = 12;
                }
                return query;
            }
        }];

        let _prepareConfig = function(config, moduleConfig){
            let obj = $.extend(true, {}, config);
            let functionKeys = [{key: "getTemplate", value: "template"}, {key: "getFallbackTemplate", value: "fallbackTemplate"}, "query", "waitForModules"];
            functionKeys.forEach((v) => {
                let value = typeof v == "string" ? v : v.value;
                let fn = typeof v == "string" ? v : v.key;
                if(typeof obj[fn] == "function"){
                    obj[value] = obj[fn](moduleConfig);
                }
            });
            obj.template = moduleConfig.festiveOffer ? templateMap['overviewTemplate'] : obj.template;

            return obj;
        };

        let getConfig = function(moduleConfig){
            for(let i = 0; i < config.length; i++){
                let tempConfig = config[i];
                let modulePageData = utils.getPageData(tempConfig.pageDataKey);
                if(typeof tempConfig.override == "function" && tempConfig.override(moduleConfig)){
                    return _prepareConfig(tempConfig, moduleConfig);
                } else if(modulePageData === tempConfig.value){
                    return _prepareConfig(tempConfig, moduleConfig);
                } else if(utils.isArray(tempConfig.value) && tempConfig.value.indexOf(modulePageData) > -1){
                    return _prepareConfig(tempConfig, moduleConfig);
                }
            }
        };

        let setLoadedResults = function(id){
            if(loadedResults.indexOf(id) == -1){
                loadedResults.push(id);
            }
        };

        let getLoadedResults = function(){
            return loadedResults;
        };

        let emptyLoadedResults = function(){
            loadedResults = [];
        };
/**
    upper   lower   int
    0       0       0
    0       1       1
    1       0       2
    1       1       3

    For serp pages(locality, suburb), upper part has priority 1 and lower has 0.
    example if topseller in upper are null and in lower are not null, then the condition 1 will excute.
*/
        let getCase = function(resultSet, ownResult){
            let condition = 0;
            resultSet.forEach((v) => {
                v.priority = v.priority || 0;
                let factor = v.topSellers.length ? 1 : 0;
                condition +=  factor * Math.pow(2, v.priority);
            });
            let expertCount = ownResult.expertDealMakerCount || 0;
            if(!ownResult.isSpecificExpert){
                expertCount += (ownResult.localityExpertCount) ? ownResult.localityExpertCount : 0;
                expertCount += (ownResult.cityExpertCount) ? ownResult.cityExpertCount : 0;
            }

            switch(condition){
                case 0:
                    return {};

                case 1:
                    return {
                        subtitles: {
                            expertDealMaker: expertCount > 1 ? "Reputed sellers in your locality who have recently closed deals and recommended by others" : "Reputed seller in your locality who has recently closed deals and recommended by others",
                            dealMaker: ownResult.dealMakerCount > 1 ? "Other highly rated sellers who have recently closed deals {{label}}" : "Other highly rated seller who has recently closed deals {{label}}"
                        },
                        fallback: {
                            hideAppCard: true
                        }
                    };

                case 2:
                    return {
                        subtitles: {
                            experts: ((ownResult.localityExpertCount || ownResult.cityExpertCount) > 1) ? "Reputed sellers in your locality who have recently closed deals and recommended by others" : "Reputed seller in your locality who has recently closed deals and recommended by others"
                        }
                    };

                case 3:
                    return {
                        subtitles: {
                            experts: ((ownResult.localityExpertCount || ownResult.cityExpertCount) > 1) ? "Reputed sellers in your locality who have recently closed deals and recommended by others" : "Reputed seller in your locality who has recently closed deals and recommended by others",
                            expertDealMaker: expertCount > 1 ? "Other highly rated sellers who have recently closed deals {{label}}" : "Other highly rated seller who has recently closed deals {{label}}",
                            dealMaker: ownResult.dealMakerCount > 1 ? "Other highly rated sellers {{label}}" : "Other highly rated sellers {{label}}"
                        }
                    };

            }
        };

        function removeOverlappingSpecificExpert(ownResult, othersResult){
            let ownTopSeller = {};
            let dealMaker = [];
            let expertDealMaker = [];
            ownResult.topSellers.forEach(seller => {
                ownTopSeller[seller.id] = seller;
            });
            othersResult.topSellers.forEach(seller => {
                if(ownTopSeller[seller.id]){
                    if(ownTopSeller[seller.id].isExpertDealMaker){
                        ownResult.expertDealMakerCount-- ;
                    } else if(ownTopSeller[seller.id].isDealMaker){
                        ownResult.dealMakerCount-- ;
                    }
                    delete(ownTopSeller[seller.id]);
                }
            });
            for(let seller in ownTopSeller){
                if(ownTopSeller[seller].isExpertDealMaker){
                    expertDealMaker.push(ownTopSeller[seller]);
                } else if(ownTopSeller[seller].isDealMaker){
                    dealMaker.push(ownTopSeller[seller]);
                };
            }
            ownResult.topSellers = expertDealMaker.concat(dealMaker);
            return ownResult;
        }

		return {
			getConfig,
            setLoadedResults,
            getLoadedResults,
            emptyLoadedResults,
            getCase,
            removeOverlappingSpecificExpert
		};
	});

    return Box.Application.getService(SERVICE_NAME);
});