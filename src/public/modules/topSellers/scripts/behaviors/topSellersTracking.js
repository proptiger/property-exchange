define([
    'common/trackingConfigService',
    'services/trackingService'
], function(t, trackingService) {
    'use strict';
    Box.Application.addBehavior('topSellersTracking', function(context) {

        var element;
        const messages = [
            'trackDealMakerClicked',
            'trackDealMakerViewProfile',
            'trackRatingReviewTooltip'
        ];

        var init = function() {
            element = context.getElement();
        };

        var onmessage = function(name, data) {
            if (data.id === element.id) {
                let properties = {};
                let event,category,label,sellerId, sellerRating, sellerStatus,sectionClicked;
                switch (name) {
                    case 'trackDealMakerClicked':
                        event = t.OPEN_EVENT;
                        category = t["LEAD_FORM_CATEGORY"];
                        if(data.dataset.category == "serp"){
                            label = `connect ${t.SELLER_TYPE_LABEL[data.dataset.sellerType]} RHS`;
                        } else {
                            label = `connect ${t.SELLER_TYPE_LABEL[data.dataset.sellerType]}`;
                        }
                        sectionClicked = 'topSellersClicked';
                        break;
                    case "trackDealMakerViewProfile":
                        event = t.VIEW_PROFILE;
                        category = t.TOPSELLER_CARD_CATEGORY[data.dataset.category];
                        label = `${t.SELLER_TYPE_LABEL[data.dataset.sellerType]} Profile page`;
                        sectionClicked = t.TOPSELLER_CARD_Section[data.dataset.category];
                        break;
                    case 'trackRatingReviewTooltip':
                        category = t.DEAL_REVIEW_CATEGORY;
                        event = t.TOOLTIP_EVENT;
                        label = data.label;
                        sellerId = data.sellerId;
                        sellerStatus = data.sellerStatus=='paid'?t.SELLER_PAID_STATUS.PAID:t.SELLER_PAID_STATUS.NOT_PAID;
                        sellerRating = data.sellerRating;
                        break;
                    default:
                        return;
                }
                
                properties[t.CATEGORY_KEY] = category;
                properties[t.LABEL_KEY] = label;
                properties[t.SECTION_CLICKED] = sectionClicked;
                if(sellerId){
                    properties[t.SELLER_ID_KEY] = sellerId;
                }
                if(sellerRating){
                    properties[t.SELLER_SCORE_KEY] = sellerRating;
                }
                if(sellerStatus){
                    properties[t.SELLER_STATUS_KEY] = sellerStatus;
                }
                trackingService.trackEvent(event, properties);
            }
        };

        return {
            messages,
            onmessage,
            init,
            destroy: function() {}
        };
    });
});