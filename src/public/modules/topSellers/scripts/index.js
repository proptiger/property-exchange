define([
    'modules/topSellers/scripts/services/topSellersService',
    "services/filterConfigService",
    'services/utils',
    'services/defaultService',
    'services/urlService',
    "services/leadPyrService",
    'modules/twosteppyr/scripts/services/twosteppyrService',
    'modules/topSellers/scripts/behaviors/topSellersTracking',
    'behaviors/leadBehaviour'
], function(topSellersService, filterConfigService, utils, defaultService, urlService) {
    'use strict';
    const MODULE_NAME = "topSellers";
    Box.Application.addModule(MODULE_NAME, function(context) {
        var moduleEl,
            $,
            $moduleEl,
            twosteppyrService,
            leadPyrService,
            CommonService,
            sellerList,
            topSellerTemplate,
            topSellerCount,
            userCity = {},
            config,
            topSellerModules,
            configObj,
            tempRenderFn,
            tempResult,
            overrides = {},
            buyResponse = {},
            rentResponse = {},
            trackRatingReview={},
            currentCategory = utils.getPageData('listingType'),
            sellerCardSelector = '[data-type="seller-card"]';

        let isMobileRequest = utils.isMobileRequest();

        const constantConfig = {
            selectors: {
                alignDiv: ".js-top-seller-list",
                sellerCard: ".seller-card"
            },
            class: {
                alignClass: "align-center"
            },
            LOCALITIES_TO_SHOW: 3,
            SELLER_DESIGNATIONS: {
                "designations": ["CityExpertDealMaker", "SuburbExpertDealMaker", "LocalityExpertDealMaker", "ExpertDealMaker", "DealMaker"], // do not change order -> defines priority to show
                display: {
                    "CityExpertDealMaker": {
                        label: "City Expert",
                        class: "expert"
                    },
                    "SuburbExpertDealMaker": {
                        label: "Locality Expert",
                        class: "expert"
                    },
                    "LocalityExpertDealMaker": {
                        label: "Locality Expert",
                        class: "expert"
                    },
                    "ExpertDealMaker": {
                        label: "Expert Deal Maker",
                        class: "expert"
                    },
                    "DealMaker": {
                        label: "Deal Maker",
                        class: "dealmaker"
                    }
                }
            }
        };

        function _setSellerDesignation(seller){
            let designations = constantConfig.SELLER_DESIGNATIONS.designations;
            for(let i = 0; i < designations.length; i++){
                if(seller.sellerStatuses.indexOf(designations[i]) > -1){
                    seller.designation = constantConfig.SELLER_DESIGNATIONS.display[designations[i]];
                    return;
                }
            }
        }
        function _parseSellerRating(rating){
            let ratingInfo={};
            ratingInfo.ratingClass = "not-rated";
            if(rating){
                ratingInfo.ratingClass = utils.getRatingBadgeClass(rating);
                ratingInfo.rating = parseFloat(rating).toFixed(1) >= 0 ? parseFloat(rating).toFixed(1): 0;
            }
            return ratingInfo;
        }

        function _parseTopSellers(response) {
            let title = {},
                subTitle = {},
                labelName = '', pageData = utils.getPageData();

            if (pageData.placeName) {
                labelName = pageData.placeName;
            } else if (pageData.projectName) {
                if (pageData.builderName) {
                    labelName = `${pageData.builderName} ${pageData.projectName || ''}`;
                } else {
                    labelName = pageData.projectName;
                }
            } else if (pageData.localityName || pageData.suburbName) {
                if(pageData.pageLevel.indexOf("multipleSuburbLocality") > -1){
                    labelName += pageData.localityName ? `${pageData.localityName.split(',').join(', ')}` : '';
                    labelName += pageData.suburbName ? `${!labelName ? '': ', '}${pageData.suburbName.split(',').join(', ')}` : '';
                } else {
                    labelName = pageData.localityName || pageData.suburbName;    
                }
            } else if (pageData.cityName) {
                labelName = pageData.cityName;
            } else {
                labelName = userCity.label;
            }
            if(labelName){
                labelName =  pageData.placeName ? `near ${labelName}` : `in ${labelName}`;
            } else{
                labelName = "";
            }

            let pluralDealMaker = response.dealMakerCount > 1;
            let pluralExpertDealMaker = response.expertDealMakerCount > 1;

            if (response.expertDealMakerCount) {
                title.expertDealMakerTitle = pluralExpertDealMaker ? 'Top Expert Deal Makers' : 'Top Expert Deal Maker';
                if(overrides && overrides.subtitles && overrides.subtitles.expertDealMaker){
                    subTitle.expertDealMakerSubTitle = overrides.subtitles.expertDealMaker.replace("{{label}}", labelName);
                } else {
                    if(pluralExpertDealMaker){
                        subTitle.expertDealMakerSubTitle = `Highly rated sellers who have helped buyers find their dream home`;
                    } else {
                        subTitle.expertDealMakerSubTitle = `Highly rated seller who has helped buyers find their dream home`;
                    }
                }
            }
            if (response.dealMakerCount) {
                title.dealMakerTitle = pluralDealMaker ? 'Top Deal Makers' : 'Top Deal Maker';
                if(overrides && overrides.subtitles && overrides.subtitles.dealMaker){
                    subTitle.dealMakerSubTitle = overrides.subtitles.dealMaker.replace("{{label}}", labelName);
                } else {
                    if(pluralDealMaker){
                        subTitle.dealMakerSubTitle = `Highly rated sellers who have helped buyers find their dream home`;
                    } else {
                        subTitle.dealMakerSubTitle = `Highly rated seller who has helped buyers find their dream home`;
                    }
                }
            }

            if(pluralDealMaker){
                title.title = `Deal Makers ${labelName}`;
                subTitle.subTitle = `Other highly rated sellers who have recently closed deals ${labelName}`;
            } else {
                title.title = `Deal Maker ${labelName}`;
                subTitle.subTitle = `Other highly rated seller who has recently closed deals ${labelName}`;
            }

            let labels = {
                defaults: {
                    title: title.title,
                    subTitle: subTitle.subTitle
                }
            };

            let topSellerLength = response.topSellers.length;

            if(typeof configObj.labels == "function"){
                let cityName = pageData.cityName || userCity.label;
                let localityName = pageData.localityName;
                let projectName = `${pageData.builderName} ${pageData.projectName}`;
                let suburbName = pageData.suburbName;
                labels.label = configObj.labels({localityName, cityName, projectName, suburbName, topSellerLength, topSellers: response});
            }


            response.topSellers.forEach(seller => {
                if(seller.localities && seller.localities.length > constantConfig.LOCALITIES_TO_SHOW){
                    seller.moreLocalities = seller.localities.length - constantConfig.LOCALITIES_TO_SHOW;
                    seller.localities = seller.localities.slice(0, constantConfig.LOCALITIES_TO_SHOW);
                }

                if(config && config.festiveOffer){
                    seller.festiveSellerType = seller.type && seller.type.toLowerCase()=="broker"?'AGENT':seller.type;
                }

                if(configObj && configObj.showDesignation || config.festiveOffer){
                    _setSellerDesignation(seller);
                }
                seller.ratingInfo = _parseSellerRating(seller.rating);
            });

            return {
                topSellers: response.topSellers,
                labels,
                title,
                subTitle,
                extraClasses: configObj.extraClasses || '',
                dealMakerCount: response.dealMakerCount,
                expertDealMakerCount: response.expertDealMakerCount,
                totalTopSellerCount: response.topSellers.length,
                localityExpertCount: response.localityExpertCount,
                cityExpertCount: response.cityExpertCount,
                isSpecificExpert: response.isSpecificExpert,
                isBuyAvailable: response.isBuyAvailable,
                isRentAvailable: response.isRentAvailable
            };
        }

        function _attachTopSellersFallbackTemplate() {
            topSellerTemplate = configObj.fallbackTemplate;
            if (utils.getPageData('cityId') && !isMobileRequest) {
                CommonService.bindOnLoadPromise().then(function() {
                    context.broadcast('loadModule', [{
                        name: 'pyr',
                        id: 'serpPyrForm'
                    }]);
                });
            }
        }

        function _parseBuyRentSellers(response){
            let obj = {};
            if(response.buy && response.buy.length){
                buyResponse = response.buy[0];
                buyResponse.isBuyAvailable = true;
                rentResponse.isBuyAvailable = true;
                obj = buyResponse;
                // obj.topSellers = obj.topSellers.concat(obj.topSellers,obj.topSellers, obj.topSellers);
                currentCategory = "buy";
            }
            if(response.rent && response.rent.length){
                rentResponse = response.rent[0];
                rentResponse.isRentAvailable = true;
                buyResponse.isRentAvailable = true;
                rentResponse.isBuyAvailable = buyResponse.isBuyAvailable;
                if(!obj.topSellers){
                    obj = rentResponse;
                    currentCategory = "rent";
                }
            }
            return obj;
        }

        function _populateData(response, data){
            let topSellerListWidth,
                pageData = utils.getPageData(),fallback = false;
            if (response && response.topSellers && response.topSellers.length && !pageData.sellerId && (!pageData.builderId || (pageData.builderId && pageData.pageLevel == 'project'))) {
                //hack to handle cases when API is giving more results than desired.
                if (response.topSellers.length > data.sellerCount) {
                    response.topSellers = response.topSellers.slice(0, data.sellerCount);
                }

                response = _parseTopSellers(response);
                sellerList = response.topSellers;
                context.broadcast('topSellersRendered',{
                    count: response.topSellers.length
                });
                if(config.className){
                    $moduleEl.addClass(config.className);
                }
            } else if(!isMobileRequest){
                response = {
                    hidePyrCard: pageData.cityId && !config.isZeroListingPage ? false : true,
                    hideAppCard: overrides.fallback ? overrides.fallback.hideAppCard : false
                };
                _attachTopSellersFallbackTemplate();
                fallback = true;
            } else {
                return;
            }

            // normalize page level
            let pageLevel;
            if(pageData.pageLevel.indexOf("city") > -1){
                pageLevel = "city";
            } else if(pageData.pageLevel.indexOf("locality") > -1){
                pageLevel = "locality";
            } else if(pageData.pageLevel.indexOf("multipleSuburbLocality") > -1){
                pageLevel = "multipleSuburbLocality";
            }

            let templateData = $.extend({}, response, {
                isMobileRequest: isMobileRequest,
                userCity,
                currentCategory,
                config,
                pageLevel,
                festiveOffer: config.festiveOffer
            });

            if(!topSellerTemplate){
                return;
            }
            var tempHtml = topSellerTemplate(templateData);
            
            if(fallback){
                $moduleEl.find(config.fallbackPlaceHolderSelector).html(tempHtml);
            }else{
                $moduleEl.find(config.templatePlaceHolderSelector).html(tempHtml);
                context.broadcast('topSellersRendered');
            }

            if (templateData && templateData.topSellers) {
                config.hideIfNoDealMaker && $('.js-topDealMaker').removeClass('hide');

                if(config.hideIfNoSeller){
                  $('.js-festiveOffer').removeClass('hide');
                  CommonService.bindOnLoadPromise().then(function() {
                      $('.js-festiveOffer').addClass("onloadimage");
                  });
                }

                topSellerListWidth = $moduleEl.find(constantConfig.selectors.sellerCard).outerWidth(true) * templateData.topSellers.length;
            }

            if (topSellerListWidth && isMobileRequest) {
                $moduleEl.find(constantConfig.selectors.alignDiv).css('width', topSellerListWidth + 'px');

            } else if (templateData.topSellers && templateData.topSellers.length > 3 && ['homePage'].indexOf(config.loadTemplate) !== -1) {
                $(moduleEl).find(constantConfig.selectors.alignDiv).removeClass(constantConfig.class.alignClass);
            }

            CommonService.startAllModules(moduleEl);
        }

        function render(city) {
            var data = {
                sellerType: "dealMakers",
                projectId: utils.getPageData('projectId'),
                giveBoostToExpertDealMakers: true,
                cityId: city && city.id
            };
            configObj.query.sellerCount = configObj.query.sellerCount || topSellerCount;
            data = $.extend(data, configObj.query);

            let dataPromise;
            if(config.topSellersData){
                dataPromise = utils.getEmptyPromise(config.topSellersData);
            }else {
                dataPromise = twosteppyrService.getTopSellers(data);
            }

            dataPromise.then((response) => {
                if(data.category && data.category.toLowerCase() == "all"){
                    response = _parseBuyRentSellers(response);
                } else {
                    response = response[0];
                }
                tempResult = response || {topSellers: []};
                topSellersService.setLoadedResults(moduleEl.id);
                if(topSellerModules.length > 1 && configObj.waitForModules){
                    tempRenderFn = function (updatedResponse) {
                        if(updatedResponse){
                            response = updatedResponse;
                        }
                        _populateData(response, data)
                    }
                    if(topSellersService.getLoadedResults().length == topSellerModules.length){
                        context.broadcast("all-topseller-reponse-recieved");
                    }
                } else {
                    _populateData(response, data);
                }
            }, () => {});
        }

        function openLeadForm(sellerId, step, returnRowData) {
            let leadFormData = leadPyrService.getLeadFormRawData(step);
            let rawData = {...leadFormData,...{
                salesType: currentCategory,
                type: 'openLeadForm',
                id: "lead-popup",
                smartMultiplication: false,
                isAccountLocked: false
            }};
            rawData.cityId = rawData.cityId || userCity.id;
            rawData.cityName = rawData.cityName || userCity.label;
            for (var i = 0; i < sellerList.length; i++) {
                if (sellerList[i].id == sellerId) {
                    let tempSeller = sellerList[i];
                    rawData.companyId = sellerId;
                    rawData.companyName = tempSeller.name;
                    rawData.companyRating = tempSeller.rating;
                    rawData.companyPhone = tempSeller.contact;
                    rawData.companyImage = tempSeller.image;
                    rawData.companyType = 'Agent';
                    rawData.companyUserId = tempSeller.userId;
                    break;
                }
            }
            if(returnRowData){
                return rawData;
            }
            context.broadcast('leadform:open', rawData);
        }

        function _scrollFunction() {
            let scrollTop = $(window).scrollTop();
            let windowHeight = $(window).height();
            let scrollBottom = scrollTop + windowHeight;
            let topSellerCard = $('.js-top-seller-parent .js-to-fix');
            let headerHeight = $('[data-header]').outerHeight(true);
            let topSellerCardHeight = (topSellerCard.outerHeight() + 50);

            let profilePageHeight = $('.js-profile-container').outerHeight(true) || 0;

            $('[data-listing-wrapper]').css({"min-height": topSellerCardHeight+'px'});
            topSellerCardHeight = topSellerCardHeight + profilePageHeight; // to handle builder serp case


            let pageData = utils.getPageData() || {};

            // if(pageData.isSerp && pageData.sellerId && pageData.sellerId.length){
            //     topSellerCard.removeAttr('style');
            //     topSellerCard.addClass('posfix');
            //     topSellerCard.css('top','auto');
            //     return;
            // }

            if (windowHeight > topSellerCardHeight) {
                topSellerCard.removeAttr('style');
                topSellerCard.css('top','');
                topSellerCard.addClass('posfix');
            } else {
                if (scrollBottom > topSellerCardHeight) {
                    topSellerCard.removeAttr('style');
                    topSellerCard.css({ position: 'fixed', bottom: (0) + 'px' });
                } else if (scrollBottom < topSellerCardHeight) {
                    topSellerCard.removeClass('posfix');
                    topSellerCard.removeAttr('style');
                    topSellerCard.css({ top: (headerHeight+profilePageHeight) + 'px' });
                }
            }
        }

        function _getTopSellerTemplate(){
            configObj = topSellersService.getConfig(config);
            return configObj.template;
        }

        function _switchBuyRent(switchTo){
            if(currentCategory == switchTo){
                return;
            }
            if(switchTo == "buy" && buyResponse.isBuyAvailable){
                currentCategory = switchTo;
                _populateData(buyResponse, {sellerCount: topSellerCount});
            } else if(switchTo == "rent" && rentResponse.isRentAvailable){
                currentCategory = switchTo;
                _populateData(rentResponse, {sellerCount: topSellerCount});
            }
        }

        function _showSellerListings(seller) {
            let pageData = utils.getPageData(),
                url, extra = {};

            if (!(pageData.cityId || pageData.builderId || pageData.sellerId) && userCity && userCity.id) {
                extra.cityId = userCity.id;
                extra.cityName = userCity.label;
            }

            url = filterConfigService.getSellerListingUrl(seller, false, extra);
            urlService.ajaxyUrlChange(url);
        }

        function getSellerTransactionStatus(data){
            let status = {};
            status.isDealMaker = data.isDealMaker ? true : false;
            status.isExpertDealMaker = (data.isExpertDealMaker || data.isLocalityExpert || data.isSuburbExpert || data.isCityExpert) ? true : false;
            status.typeLabel = data.expertStatus.label;
            status.type = data.sellerType;
            return status;
        }
        function _loadSellerMiniProfile(data){
            if (!commonService.checkModuleLoaded('sellerMiniProfile')) {
                context.broadcast('loadModule', [{
                    name: 'sellerMiniProfile',
                }]);
            }
            commonService.bindOnModuleLoad('sellerMiniProfile', function() {
                _openSellerMiniProfile(data);
            });
        }
        function _openSellerMiniProfile(data){

            context.broadcast("loadSellerMiniProfile" ,{
                companyName: data.name,
                companyType: 'AGENT',
                companyRating: (data.rating||0),
                ratingCount: data.sellerCallRatingCount || 0,
                rating :(data.rating||0),
                reviewCount: data.sellerFeedbackCount || 0,
                builderId: data.companyId,
                builderName: data.builderName,
                companyId: data.id,
                companyUserId: data.userId,
                leadFormData: openLeadForm(data.id, null, true),
                isPaidSeller: true,
                sellerTransactionStatus: getSellerTransactionStatus(data),
                postedBy: {
                    name: data.companyName,
                    image: data.image,
                    backgroundColor: data.avatarDetails.backgroundColor,
                    nameText: data.avatarDetails.text,
                    textColor: data.avatarDetails.textColor,
                    type: 'AGENT'
                }
            });
        }

        function _bindMouseForRatingReview(){
            if(!isMobileRequest){

                $moduleEl.on('mouseenter', sellerCardSelector, function(){
                    let index = $(this).data('index'),
                        data = (sellerList && sellerList[index]) || {},
                        sellerCardElement = this;
                    if(data.id && data.userId){
                        context.broadcast('showRatingReviewWrapper');
                        context.broadcast('loadRatingReviewWrapper',{
                            element: sellerCardElement,
                            companyName: data.name,
                            companyType: 'AGENT',
                            companyRating: (data.rating||0),
                            ratingCount: data.sellerCallRatingCount || 0,
                            reviewCount: data.sellerFeedbackCount || 0,
                            builderId: data.companyId,
                            builderName: data.builderName,
                            companyId: data.id,
                            companyUserId: data.userId,
                            leadFormData: openLeadForm(data.id, null, true),
                            isPaidSeller: true,
                            sellerTransactionStatus: getSellerTransactionStatus(data),
                            postedBy: {
                                name: data.companyName,
                                image: data.image,
                                backgroundColor: data.avatarDetails.backgroundColor,
                                nameText: data.avatarDetails.text,
                                textColor: data.avatarDetails.textColor,
                                type: 'AGENT'
                            }
                        });
                        if(!trackRatingReview[data.id]){
                            context.broadcast('trackRatingReviewTooltip',{
                                id: moduleEl.id,
                                label: 'RHS',
                                sellerId: data.companyId,
                                sellerStatus: 'paid',
                                sellerRating: data.rating
                            });
                            trackRatingReview[data.id] = true;
                        }
                    }
                });

                $moduleEl.on('mouseleave', sellerCardSelector, function(){
                    context.broadcast('hideRatingReviewWrapper');
                });
            }
        }

        return {
            behaviors: ['topSellersTracking', 'leadBehaviour'],
            init: function() {
                moduleEl = context.getElement();
                $ = context.getGlobal('jQuery');
                $moduleEl = $(moduleEl);
                config = context.getConfig() || {};
                CommonService = context.getService('CommonService');
                twosteppyrService = context.getService('twosteppyrService');
                leadPyrService = context.getService('LeadPyrService');
                topSellerTemplate = _getTopSellerTemplate();
                topSellerModules = CommonService.getModule(MODULE_NAME, document);
                $moduleEl.find(config.templatePlaceHolderSelector).html('');
                $moduleEl.find(config.fallbackPlaceHolderSelector).html('');
                let pageData = utils.getPageData();
                if (config.sellerCount) {
                    topSellerCount = config.sellerCount;
                }
                if (!pageData.cityId && !pageData.builderId && !pageData.sellerId) {
                    //get user city
                    defaultService.getCityByLocation().then((response) => {
                        userCity = response || {};
                        if (userCity) {
                            render(userCity);
                        }
                    });
                } else {
                    render();
                }
                _bindMouseForRatingReview();
                window.addEventListener('scroll', _scrollFunction,{passive:true});
                context.broadcast("moduleLoaded", {
                    name: MODULE_NAME
                });
            },

            onmessage: {
                "update-user-city": function(data) {
                    userCity = data || {};
                },
                "pageDataListingTypeChange": function(data){
                    if(data.id == "homePageSearchBox"){
                        _switchBuyRent(data.listingType);
                    }
                },
                "all-topseller-reponse-recieved": function(){
                    context.broadcast("topseller-results", {
                        id: moduleEl.id,
                        result: tempResult,
                        priority: configObj.priority
                    });
                },
                "topseller-results": function(data){
                    if(moduleEl.id != data.id){
                        let resultSet = [];
                        resultSet.push({
                            priority: configObj.priority,
                            topSellers: tempResult.topSellers
                        });
                        resultSet.push({
                            priority: data.priority,
                            topSellers: data.result.topSellers
                        });
                        overrides = topSellersService.getCase(resultSet, tempResult);
                        if(config.isMultiLocality){
                            let newResponse = topSellersService.removeOverlappingSpecificExpert(tempResult, data.result);
                            tempRenderFn(newResponse);
                        } else {
                            tempRenderFn();    
                        }
                        
                    }
                },
                "serpSearchParamsChanged": function(data){
                    let responseData = currentCategory =='buy' ? buyResponse : rentResponse;
                    config.isZeroListingPage = data.isZeroListingPage;
                    if(!topSellerCount && config.isZeroListingPage)
                        _populateData(responseData, {sellerCount: topSellerCount});
                },
                "rhsDeveloperProductRendered": function(){
                    $moduleEl.find(config.fallbackPlaceHolderSelector).addClass('hide');
                },
                "topSellersRendered": function(){
                    $moduleEl.find(config.fallbackPlaceHolderSelector).addClass('hide');
                }
            },
            onclick: function(event, element, elementType) {

                if(elementType=='seller-rating'){
                    //handling seller rating element issue
                    element = $(element).parents('[data-type="seller-card"]').get(0);
                }


                let dataset = $(element).data() || {};
                switch (elementType) {
                    case "connect-seller":
                        var elementData = $(element).data();
                        if (elementData && elementData.userid) {
                            openLeadForm(elementData.userid, elementData.step);
                        }

                        context.broadcast('trackDealMakerClicked', { id: moduleEl.id, dataset });
                        break;
                    case 'seller-listings':
                    case 'seller-profile':
                        if (dataset.isclickable) {
                            event.preventDefault();
                            if(dataset.showMainProfile){
                                _showSellerListings({
                                    id: dataset.sellerId,
                                    sellerUserId: dataset.sellerUserId,
                                    builderId: dataset.sellerId,
                                    type: dataset.linkType.toUpperCase(),
                                    name: dataset.linkName,
                                    listingType: currentCategory
                                });
                            } else {
                                let index = dataset.index,
                                data = (sellerList && sellerList[index]) || {};
                                _loadSellerMiniProfile(data);
                            }
                            context.broadcast('trackDealMakerViewProfile', { id: moduleEl.id, dataset });
                        }
                        break;
                    case "switch":
                        _switchBuyRent(dataset.category);
                        break;
                    default:
                        return;
                }
            },

            destroy: function() {
                window.removeEventListener('scroll', _scrollFunction,{passive:true});
                $moduleEl.off('mouseenter', sellerCardSelector);
                $moduleEl.off('mouseleave', sellerCardSelector);
                sellerCardSelector = null;
                moduleEl = null;
                $ = null;
                $moduleEl = null;
                sellerList = null;
                topSellerTemplate = null;
                topSellerCount = null;
                config = null;
                topSellerModules = null;
                configObj = null;
                tempRenderFn = null;
                tempResult = null;
                overrides = {};
                buyResponse = {};
                userCity = {};
                rentResponse = {};
                isMobileRequest = null;
                currentCategory = utils.getPageData('listingType');
                topSellersService.emptyLoadedResults();
            }
        };
    });
});