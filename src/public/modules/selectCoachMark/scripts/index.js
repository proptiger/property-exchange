"use strict";
define([
    'bower_components/intro.js/minified/intro.min.js',
],function (introJs) {
    Box.Application.addModule("selectCoachMark", function (context) {
        var messages = ['showSelectCoachMark', 'showBannerCoachMark','hideSelectCoachMark',];
        var onClose = null;
        const SelectCoachMark = '.js-select-coach-mark',
            SelectTooltipHighlight = '.makaan-select-tooltip-highlight',
            CSS_LINK = '/bower_components/intro.js/minified/introjs.min.css'
        ;
        function init() {
            context.broadcast('moduleLoaded', {name: 'selectCoachMark',id: 'selectCoachMark'});
        }
        function onmessage(name,data){
            switch (name) {
                case 'showSelectCoachMark':
                    if(data) {
                        _initCoachMark(data);
                        $(SelectTooltipHighlight).html($(SelectCoachMark).html());
                    }
                    break;
                case 'showBannerCoachMark':
                    if(data) {
                        $("html, body").animate({scrollTop: data.offset}, 2);
                        _initCoachMark(data);
                        onClose = data.onClose;
                    }
                    break;    
                case 'hideSelectCoachMark' :
                    introJs().exit();
                    _enableScroll();
                    if(onClose && typeof onClose == "function") {
                        onClose();
                    }
                    break;
            }
        }
        function _addCoackMarkCss() {
            var head = document.getElementsByTagName('head')[0];
            var link = document.createElement('link');
            link.rel = 'stylesheet';
            link.type = 'text/css';
            link.href = CSS_LINK;
            head.appendChild(link);
        }
        function _enableScroll(){
            $('body').css({'overflow':'auto'});
        }
        function _disableScroll(){
            $('body').css({'overflow':'hidden'});
        }
        function _initCoachMark(data) {
            _addCoackMarkCss();
            var intro = introJs().addStep({
                element: document.querySelectorAll(data.selector)[0],
                intro: data.text,
                position: `${data.position}`,
                scrollTo: "tooltip",
                tooltipClass: `${data.tooltipClass}`,
                highlightClass: `${data.highlightClass}`
            });
            intro.start();
            _disableScroll();
            intro.onexit(function () {
                _enableScroll();
                if (onClose && typeof onClose == "function") {
                    onClose();
                }
            });
        }
        return {
            init,
            messages,
            onmessage
        };
    });
});
