define([
    'common/trackingConfigService',
    'services/trackingService'
], function(t, trackingService) {
    'use strict';
    Box.Application.addBehavior('ratingReviewWrapperTracking', function(context) {

        var element;
        const messages = [
            'trackProfileClick',
            'trackConnectClick'
        ];

        var init = function() {
            element = context.getElement();
        };

        var onmessage = function(name, data) {
            if (data.id === element.id) {
                let properties = {};
                let event,category,label, sellerId, sellerRating, sellerStatus,sectionClicked;
                sellerId = data.sellerId;
                sellerStatus = data.sellerStatus?t.SELLER_PAID_STATUS.PAID:t.SELLER_PAID_STATUS.NOT_PAID;
                sellerRating = data.sellerRating;
                switch (name) {
                    case 'trackProfileClick':
                        category=t.DEAL_REVIEW_CATEGORY;
                        event = t.CLICK_EVENT;
                        label = 'viewprofile';
                        break;
                     case 'trackConnectClick':
                        event = t.OPEN_EVENT;
                        category = t["LEAD_FORM_CATEGORY"];
                        label='dealreview';
                        sectionClicked = 'dealReviewConnectNow';

                        break;
                    default:
                        return;
                }

                properties[t.CATEGORY_KEY] = category;
                if(label){
                    properties[t.LABEL_KEY] = label;
                }
                if(sellerId){
                    properties[t.SELLER_ID_KEY] = sellerId;
                }
                if(sellerRating){
                    properties[t.SELLER_SCORE_KEY] = sellerRating;
                }
                if(sellerStatus){
                    properties[t.SELLER_STATUS_KEY] = sellerStatus;
                }
                properties[t.SECTION_CLICKED] = sectionClicked;
                trackingService.trackEvent(event, properties);
            }
        };

        return {
            messages,
            onmessage,
            init,
            destroy: function() {}
        };
    });
});