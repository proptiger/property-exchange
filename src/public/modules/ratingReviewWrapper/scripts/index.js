
define([
    'doT!modules/ratingReviewWrapper/views/index',
    'services/filterConfigService',
    'services/commonService',
    'services/urlService',
    'services/utils',
    'modules/ratingReviewWrapper/scripts/behaviors/ratingReviewWrapperTracking'
], function(template, filterConfigService, commonService, urlService, utils) {
    "use strict";
    Box.Application.addModule('ratingReviewWrapper', function(context) {

        var moduleElem, $moduleEl, $, config, currentActiveData, isMobileRequest;

        const HIDECLASS = 'hide',
              SELLER_REVIEW_PLACEHOLDER = '.js-sellerReviews',
              SELLER_TYPES = {
                AGENT: 'AGENT',
                BUILDER: 'BUILDER',
                OWNER: 'OWNER'
              },
              TP_ARROW_CONFIG = {
                  SELECTOR_CLASS: '.js-tp-arrow',
                  PIXEL_GAP: 0,
                  CLASSES: {
                      LEFT: 'left',
                      RIGHT: 'right',
                      TOP: 'top',
                      BOTTOM: 'bot'
                  }
              },
              MODULE_SELECTOR_CLASSES = {
                  'RATING': '.js-rating',
                  'REVIEW': '.js-review',
                  'MORE_REVIEWS': '.js-more-review',
                  'REVIEW_CONTAINER': '.js-review-container',
                  'VIEW_PROFILE': '.js-view-profile'
              },
              WRAPPER_PARENT = '.js-rating-review-wrapper';

        let messages = ['loadRatingReviewWrapper','loadSellerReviews:fromSummary','showRatingReviewWrapper','hideRatingReviewWrapper'],
            behaviors = ['ratingReviewWrapperTracking'];

        function displayHandler(show = true){
            if(show){
                $moduleEl.removeClass(HIDECLASS);
            }else {
                $moduleEl.addClass(HIDECLASS);
                let wrapperElement = $(WRAPPER_PARENT);
                if(wrapperElement && wrapperElement.length && !isMobileRequest){
                    $moduleEl.appendTo(wrapperElement);
                }
            }
        }

        function onmessage(name, data) {
            // bind custom messages/events
            switch(name){
                case 'loadRatingReviewWrapper':
                    currentActiveData = data || {};
                    render(data);
                    break;
                case 'showRatingReviewWrapper':
                    displayHandler(true);
                    break;
                case 'hideRatingReviewWrapper':
                case 'loadSellerReviews:fromSummary':
                    if(!isMobileRequest && (!$moduleEl.is(':hover') || name == 'loadSellerReviews:fromSummary')){
                        displayHandler(false);
                    }
                    break;
            }
        }


        let cssObj = {}; 
        function _handleTooltipPlacement(){

            if($moduleEl.hasClass(HIDECLASS) || isMobileRequest){
                return;
            }

            let bounds = currentActiveData && currentActiveData.element && currentActiveData.element.getBoundingClientRect();
            let windowHeight = $(window).height() || (window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight),
                windowWidth = $(window).width() || (window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth),
                tpArrowPos = {
                    className: '',
                    cssObj: {}
                };


            function getLRTB(key){
                /* Beware: Are you double sure you wanna change it ??
                ......................................................
                    Still ???
                ......................................................
                 Good Luck !!
                */
                let obj = {};
                switch (currentActiveData && currentActiveData.position) {
                    case 'top':
                        obj.bottom = '100%';
                        tpArrowPos.className = TP_ARROW_CONFIG.CLASSES.BOTTOM;
                        break;
                    case 'bottom':
                        obj.top = '100%';
                        tpArrowPos.className = TP_ARROW_CONFIG.CLASSES.TOP;
                        break;
                    case 'top_bottom':
                        if((bounds.top-100) <= (windowHeight/2)){
                            obj.top = '100%';
                            tpArrowPos.className = TP_ARROW_CONFIG.CLASSES.TOP;
                        }else {
                            obj.bottom = '100%';
                            tpArrowPos.className = TP_ARROW_CONFIG.CLASSES.BOTTOM;
                        }
                        break;
                    default:
                        if(windowWidth/2>bounds.left){
                            obj.left = '100%';
                            tpArrowPos.className = TP_ARROW_CONFIG.CLASSES.LEFT;
                        }else {
                            obj.right = '100%';
                            tpArrowPos.className = TP_ARROW_CONFIG.CLASSES.RIGHT;
                        }

                        if(windowHeight/2>bounds.bottom){
                            obj.top = '0';
                            tpArrowPos.cssObj = {
                                top: bounds.height/2,
                                bottom: 'auto'
                            };
                        }else {
                            obj.bottom = `calc(100% - ${bounds.height}px)`;
                            tpArrowPos.cssObj = {
                                top: 'auto',
                                bottom: bounds.height/2
                            };
                        }
                        break;
                }
                return key ? (obj[key] || '') : obj;
            }

            cssObj = {};
            if(bounds && bounds.left){
                let temp = getLRTB();
                cssObj = {
                    position: 'absolute',
                    ...temp // jshint ignore:line
                }
            }

            let arrowPlaceholderSelector = $moduleEl.find(TP_ARROW_CONFIG.SELECTOR_CLASS);
            arrowPlaceholderSelector.removeClass(`${TP_ARROW_CONFIG.CLASSES.TOP} ${TP_ARROW_CONFIG.CLASSES.BOTTOM} ${TP_ARROW_CONFIG.CLASSES.LEFT} ${TP_ARROW_CONFIG.CLASSES.RIGHT}`);
            arrowPlaceholderSelector.removeAttr('style');

            if(tpArrowPos.className){
                arrowPlaceholderSelector.addClass(tpArrowPos.className);
                arrowPlaceholderSelector.css(tpArrowPos.cssObj);

            }

            ['left','right','top','bottom'].map((val)=>{
                cssObj[val] = (cssObj[val] || cssObj[val]===0)?cssObj[val]:'';
            });
            $moduleEl.css(cssObj);
        }

        function updateRatingReview(data){
            $moduleEl.find(MODULE_SELECTOR_CLASSES.VIEW_PROFILE).addClass(HIDECLASS);

            let sellerType = (currentActiveData.companyType && currentActiveData.companyType.toUpperCase());
            if((sellerType === SELLER_TYPES.AGENT && currentActiveData.companyId) || (sellerType === SELLER_TYPES.BUILDER && (currentActiveData.builderId||currentActiveData.companyId))){
                $moduleEl.find(MODULE_SELECTOR_CLASSES.VIEW_PROFILE).removeClass(HIDECLASS);
            }
            displayHandler(true);
        }

        function render(data){

            updateRatingReview(data);

            _handleTooltipPlacement();

            $moduleEl.appendTo(currentActiveData.element);

            let  sellerScoreId = moduleElem.querySelector('[data-module="sellerScore"]').id;

            commonService.bindOnModuleLoad('sellerScore', ()=>{
                context.broadcast('reloadSellerScore', {
                    listingUserId: data.companyUserId,
                    companyRating: data.companyRating,
                    id: sellerScoreId
                });
            }, sellerScoreId);
        }

        function trackProfileClick(){
           context.broadcast('trackProfileClick',{
                id: moduleElem.id,
                sellerId: currentActiveData.companyId,
                sellerStatus: currentActiveData.isPaidSeller,
                sellerRating: currentActiveData.rating
            }); 
        }
        function _handleViewProfileClick(){
            //track it before page redirect occurs
            
            let sellerType = (currentActiveData.companyType && currentActiveData.companyType.toUpperCase());
            if(sellerType == SELLER_TYPES.AGENT && currentActiveData.companyId){
                let url = filterConfigService.getSellerListingUrl({
                    type: sellerType,
                    id: currentActiveData.companyId,
                    name: currentActiveData.companyName
                });
                urlService.ajaxyUrlChange(url);
            }else if(sellerType == SELLER_TYPES.BUILDER && currentActiveData.builderId){
                let url = filterConfigService.getSellerListingUrl({
                    type: sellerType,
                    builderId: currentActiveData.builderId,
                    builderName: currentActiveData.builderName
                });
                urlService.ajaxyUrlChange(url);
            }
            
        }

        function _loadSellerMiniProfile(){
            if (!commonService.checkModuleLoaded('sellerMiniProfile')) {
                context.broadcast('loadModule', [{
                    name: 'sellerMiniProfile',
                }]);
            }
            commonService.bindOnModuleLoad('sellerMiniProfile', function() {
                showSellerMiniProfile();
            });
        }

        function showSellerMiniProfile(){
            context.broadcast("loadSellerMiniProfile", {
                companyName: currentActiveData.companyName,
                companyRating: currentActiveData.companyRating,
                companyType: currentActiveData.companyType,
                rating: currentActiveData.rating,
                ratingCount: currentActiveData.ratingCount,
                reviewCount: currentActiveData.reviewCount,
                builderId: currentActiveData.builderId,
                builderName: currentActiveData.builderName,
                companyId: currentActiveData.companyId,
                companyUserId: currentActiveData.companyUserId,
                leadFormData: currentActiveData.leadFormData,
                isPaidSeller: currentActiveData.isPaidSeller,
                sellerTransactionStatus: currentActiveData.sellerTransactionStatus,
                postedBy: currentActiveData.postedBy
            });
        }

        function onclick(event, element, elementType) {
            let closeMiniProfile;
            switch (elementType) {
                case 'view-profile':
                    closeMiniProfile = true;
                    trackProfileClick();
                    if(isMobileRequest){
                        context.broadcast('popup:close', {callback: _loadSellerMiniProfile});
                    } else {
                        _loadSellerMiniProfile();
                    }
                    break;
                case 'connect-seller':
                    if(currentActiveData.leadFormData){
                        context.broadcast('leadform:open', currentActiveData.leadFormData);
                        context.broadcast('trackConnectClick',{
                            id: moduleElem.id,
                            sellerId: currentActiveData.companyId,
                            sellerStatus: currentActiveData.isPaidSeller,
                            sellerRating: currentActiveData.rating
                        });
                        closeMiniProfile = true;

                    }
                    break;
            }

            if(elementType !== 'closePopup'){
                event.stopPropagation();
            }

            if(closeMiniProfile && !isMobileRequest){
                displayHandler(false);
            }
        }

        function _handleMouseEvents(){
            if(!isMobileRequest){
                $moduleEl.on('mouseleave', ()=>{
                    displayHandler(false);
                });
            }
        }

        return {
            init: function() {
                moduleElem = context.getElement();
                $ = context.getGlobal('jQuery');
                $moduleEl = $(moduleElem);
                config = context.getConfig() || {};
                isMobileRequest = utils.isMobileRequest();
                context.broadcast('moduleLoaded', {
                    'name': 'ratingReviewWrapper',
                    'id': moduleElem.id
                });

                let html = template({
                    isSellerSerp: config.isSellerSerp,
                    isBuilderSerp: config.isBuilderSerp,
                    isMobileRequest
                });
                $(moduleElem).addClass(HIDECLASS);
                $(moduleElem).html(html);
                commonService.startAllModules(moduleElem.querySelectorAll('.js-mini-profile'));
                _handleMouseEvents();
            },
            messages,
            onmessage,
            behaviors,
            onclick,
            destroy: function() {
                $ = null;
                moduleElem = null;
                $moduleEl.off('mouseleave');
            }
        };
    });
});
