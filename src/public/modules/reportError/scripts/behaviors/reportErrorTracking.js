define([
    'common/trackingConfigService',
    'services/trackingService',
    'services/utils'
], function(t, trackingService, utils) {
    'use strict';
    Box.Application.addBehavior('reportErrorTracking', function() {

        var messages = [
         'reportErrorDataValidated',
         'please provide the error details',
         'phone number is required',
         'phone number is invalid',
         'feedback already submitted',
         'reportError:close'
         ];

        var  reportchosen = t.REASON_CHOSEN_NAMES,
             step = t.REPORT_ERROR_LABELS ;

        var config = utils.getPageExtraData();
        let scope = {
            category: t.REPORT_ERROR_CATEGORY,
            sourceModule: t.DESCRIPTION_MODULE,
            localityId: window.pageData.localityId,
            cityId: window.pageData.cityId,
            listingId: window.pageData.listingId,
            projectId: window.pageData.projectId,
            builderId: window.pageData.builderId,
            suburbId: window.pageData.suburbId,
            listingCategory: config.listingCategory,
            projectStatus: config.projectStatus,
            budget: config.budget,
            unitType: config.unitType,
            sellerId: config.sellerId,
            bhk: config.beds,
            sellerScore: config.sellerRatiing,
            listingScore: config.listingScore
        };

        function track(eventName, data = {}) {

            let properties = {};
            properties[t.CATEGORY_KEY] = data.category;
            properties[t.SOURCE_MODULE_KEY] = data.sourceModule;
            properties[t.LOCALITY_ID_KEY] = data.localityId;
            properties[t.CITY_ID_KEY] = data.cityId;
            properties[t.LISTING_ID_KEY] = data.listingId;
            properties[t.PROJECT_ID_KEY] = data.projectId;
            properties[t.BUILDER_ID_KEY] = data.builderId;
            properties[t.SUBURB_ID_KEY] = data.suburbId;
            properties[t.LISTING_CATEGORY_KEY] = t.makeValuesUniform(data.listingCategory,t.LISTING_CATEGORY_MAP) ;
            properties[t.PROJECT_STATUS_KEY] = t.makeValuesUniform(data.projectStatus,t.PROJECT_STATUS_NAME) ;
            properties[t.BUDGET_KEY] = data.budget;
            properties[t.UNIT_TYPE_KEY] = t.makeValuesUniform(data.unitType,t.UNIT_TYPE_MAP) ;
            properties[t.SELLER_ID_KEY] = data.sellerId;
            properties[t.NAME_KEY] = data.name;
            properties[t.LABEL_KEY] = data.label;
            properties[t.PHONE_USER_KEY] = data.phone;
            properties[t.BHK_KEY] = t.makeValuesUniform(data.bhk,t.BHK_MAP);
            properties[t.SELLER_SCORE_KEY] = data.sellerScoredata;
            properties[t.LANDMARK_ID_KEY] = data.landmarkId;
            properties[t.LISTING_SCORE_KEY] = data.listingScore;

            trackingService.trackEvent(eventName, properties);

        }

        var onclick = function(event, element, elementType) {
            let eventName,
                data;

            switch (elementType) {

                case "Incorrect_Details":
                case "Property_Sold_Rented_Out":
                case "Report_Spam":
                case "Other":
                case "Incorrect_Contact_Details":
                case "Incorrect_Property_Price":
                case "Incorrect_Property_Location":
                case "Incorrect_Property_Type":
                case "Property_Doesn't_Exist":
                case "Broker_Posted_As_Owner":
                    if($(element).prop("checked")) {
                        eventName = t.REASON_CHOSEN_EVENT;
                        data = {
                            label: step[elementType],
                            name: reportchosen[elementType]
                        };
                    }
                    break;
                default:
                    return;
            }
            if(eventName){
               track(eventName, $.extend(data, scope)); 
            }
            
        };

        var onmessage = function(name, data) {
            let eventName,
                properties;

            switch (name) {
                case "reportErrorDataValidated":
                    eventName = t.SUBMIT_EVENT;
                    properties = {
                        phone: data
                    };
                    break;

                case 'please provide the error details':
                    eventName = t.ERROR_EVENT;
                    properties = {
                        label: 'please provide the error details'
                    };
                    break;

                case 'phone number is required':
                    eventName = t.ERROR_EVENT;
                    properties = {
                        label: 'phone number is required'
                    };
                    break;


                case 'phone number is invalid':
                    eventName = t.ERROR_EVENT;
                    properties = {
                        label: 'phone number is invalid'
                    };
                    break;

                case 'feedback already submitted':
                    eventName = t.ERROR_EVENT;
                    properties = {
                        label : 'feedback already submitted'
                    };
                    break;

                case 'reportError:close':
                    eventName = t.CLOSE_EVENT;
                    properties = {
                        label : "Step"+data 
                    };  
                    break;

                default:
                    return;
            }

            if(eventName){
               track(eventName, $.extend(properties, scope)); 
            }

        };

        return {
            onclick: onclick,
            onmessage: onmessage,
            messages,
            init: function() {},
            destroy: function() {}
        };
    });
});