"use strict";
define([
    "doT!modules/reportError/views/index",
    "services/apiService",
    "common/sharedConfig",
    "services/utils",
    "modules/reportError/scripts/services/reportErrorService",
    "services/commonService",
    "modules/reportError/scripts/behaviors/reportErrorTracking",
    'services/loginService'
], (index, apiService,sharedConfig,Utils,reportService, CommonService) => {
    Box.Application.addModule('reportError', (context) => {


        const mailUrl = sharedConfig.apiHandlers.reportErrorParser().url;
        const loginService = context.getService('LoginService');

        var messages = ["popup:closed","singleSelectDropdownInitiated","singleSelectDropdownChanged","popup:change"],
            moduleEl, $,
            $moduleEl,
            behaviors = ['reportErrorTracking'],
            listingId,
            step1,
            actual_close = true,
            stepNumber=1;

        var mailData={},
            checkBoxData,
            phone_number,
            country,
            what_to_validate_flag,
            invalidFeedback,
            invalidFeedbackCheck1,
            invalidFeedbackCheck2;

        const config = {
            messages: {
                "PHONE": {
                    "INVALID": {
                        "MESSAGE": "phone number is invalid"
                    },
                    "EMPTY": {
                        "MESSAGE": "phone number is required"
                    }
                },
                "REPORT_ERROR": {
                    "EMPTY": {
                        "MESSAGE": "please provide the error details"
                    }
                },
                "GLOBAL": {
                    "SERVER_ERROR": {
                        "MESSAGE": "error occured.try again"
                    },
                    "INVALID_FEEDBACK": {
                        "MESSAGE": "feedback already submitted"
                    }
                }
            },
            selector: {
                "COUNTRY_CODE": "[data-country-code=MAIN_COUNTRY_CODE]",
                "COUNTRY_ID": "input[data-country=MAIN_COUNTRY]"
            },
            defaultHideTime: 5000,
            hideClass: "hidden",
            reportErrorDataObj: {
                primary_reason: [{
                    "value": "Property Sold/Rented Out",
                    "data_type": "Property_Sold_Rented_Out",
                    "dataId": 1
                }, {
                    "value": "Incorrect Details",
                    "data_type": "Incorrect_Details"
                }, {
                    "value": "Report Spam",
                    "data_type": "Report_Spam"
                }, {
                    "value": "Other",
                    "data_type": "Other",
                    "dataId": 8
                }],
                secondary_reason: {
                    incorrect: 
                        [{
                            "value": "Incorrect Contact Details",
                            "data_type": "Incorrect_Contact_Details",
                            "dataId": 2
                        }, {
                            "value": "Incorrect Property Price",
                            "data_type": "Incorrect_Property_Price",
                            "dataId": 3
                        }, {
                            "value": "Incorrect Property Location",
                            "data_type": "Incorrect_Property_Location",
                            "dataId": 4
                        }, {
                            "value": "Incorrect Property Type",
                            "data_type": "Incorrect_Property_Type",
                            "dataId": 5
                        }]
                    ,
                    spam: 
                        [{
                            "value": "Property doesn't exist",
                            "data_type": "Property_Doesn't_Exist",
                            "dataId": 6
                        }, {
                            "value": "Broker posted as owner",
                            "data_type": "Broker_Posted_As_Owner",
                            "dataId": 7
                        }]
                    
                }
            }
        };


        function _setCountry(data) {
            if (!data) {return;}
            if (data.code && data.label) {
                $moduleEl.find(config.selector["COUNTRY_CODE"]).text(data.code);
                $moduleEl.find(config.selector["COUNTRY_ID"]).attr("label", data.label);
            }
        }

        function _showError(errorType, field) {
            let message = config.messages[field][errorType]["MESSAGE"];
            $moduleEl.find('[data-message=' + field + ']').text(message); 
            _hideError(field, config.defaultHideTime);
        }

        function _hideError(field, time) {
            setTimeout(() => {
                $moduleEl.find('[data-message=' + field + ']').html('');
            }, time);
        }


        function _collect_data() {
            listingId = window.pageData.listingId;  
            checkBoxData=$moduleEl.find('input:checked');
            step1 = checkBoxData[0]["value"];
            phone_number = $moduleEl.find('[name="client-phone-num"]').val(); 
            country = $moduleEl.find('[data-country="MAIN_COUNTRY"]').attr("label");    
        }

        var _validate_data =function() {
                switch(what_to_validate_flag) {

                    case "Incorrect Details":
                    case "Report Spam":
                        if(checkBoxData.length===1 ) {
                            _showError('EMPTY', 'REPORT_ERROR');
                            context.broadcast("please provide the error details");
                            return false;
                        }
                    break;

                    case "Other":
                        if (!$.trim($moduleEl.find("#other_textarea").val())) {
                            _showError('EMPTY', 'REPORT_ERROR');
                            context.broadcast("please provide the error details");
                            return false;
                        }
                    break;

                    default :
                        break;
                }    
                if (!phone_number) {
                    _showError('EMPTY', 'PHONE');
                    context.broadcast("phone number is required");
                    return false;
                } else if (phone_number && !Utils.validatePhone(phone_number,country)) {
                    _showError('INVALID', 'PHONE');
                    context.broadcast("phone number is invalid");
                    return false;
                } 
                    return true;
            };

        var  _goback = function() {
                $moduleEl.find('.js-step2').addClass('hidden');   
                $moduleEl.find('.js-error_category').removeClass('hidden'); 
                $moduleEl.find('.js-step2 input:checked').prop("checked",false);
                $moduleEl.find('.js-step2 #other_textarea').val('');
                $moduleEl.find('.js-step2 [name="client-phone-num"]').val('');
                stepNumber=1;
        };

        var  _close = function() {
                $moduleEl.find('.js-step2').addClass('hidden'); 
                $moduleEl.find('.js-step3').addClass('hidden');   
                $moduleEl.find('.js-error_category').removeClass('hidden'); 
                $moduleEl.find('input:checked').prop("checked",false);
                $moduleEl.find('#other_textarea').val('');
                $moduleEl.find('[name="client-phone-num"]').val('');
                actual_close = false;
                stepNumber=1;
        };

         var _gotowelcomenote = function() {
                $moduleEl.find('.js-step2').addClass('hidden'); 
                $moduleEl.find('.js-step3').removeClass('hidden');
                setTimeout(function(){
                    if(actual_close){
                        context.broadcast('popup:close');
                    }
                },5000);         
        };

         var _getvalue = function(userId) {
            var errorDetailsId=[],description;

            for(var i=0;i<checkBoxData.length;i++) {
                if(checkBoxData[i].getAttribute("dataId")){
                    errorDetailsId.push({ "errorTypeId": checkBoxData[i].getAttribute("dataId") });  
                } 
            }

            if ($.trim($moduleEl.find("#other_textarea").val())){ 
                description = $.trim($moduleEl.find("#other_textarea").val());
            }
            mailData["userId"] = userId;
            mailData["phone"] = phone_number;
            mailData["errorDetailsId"] = errorDetailsId;
            mailData["listingId"] = listingId;
            mailData["description"] = description;
        };

        var global = {
            isProcessing: false
        };

        function _showHideProcessing(hide) {
            let actionButton = $moduleEl.find('[data-normal]'),
                processingButton = $moduleEl.find('[data-processing]');
            if (hide) {
                processingButton.addClass(config.hideClass);
                actionButton.removeClass(config.hideClass);
                global.isProcessing = false;
            } else {
                processingButton.removeClass(config.hideClass);
                actionButton.addClass(config.hideClass);
                global.isProcessing = true;
            }
        }

        

        function init() {
            moduleEl = context.getElement();
            $ = context.getGlobal('jQuery');
            $moduleEl = $(moduleEl) ;
            $moduleEl.html(index(config.reportErrorDataObj));
            CommonService.startAllModules(moduleEl);
        }

        function destroy() {
            moduleEl = null;
        }


        function onclick(event, clickElement, elementType) {

            switch(elementType){
            case "Incorrect_Details":      
                $moduleEl.find('.js-error_category').addClass('hidden');
                $moduleEl.find('.js-incorrect_details').removeClass('hidden');
                $moduleEl.find('.js-submit-area').removeClass('hidden');
                $moduleEl.find('.js-backbutton').removeClass('hidden');
                what_to_validate_flag="Incorrect Details";
                stepNumber=2;
                break;

            case "Property_Sold_Rented_Out":      
                $moduleEl.find('.js-error_category').addClass('hidden');
                $moduleEl.find('.js-property_sold_out').removeClass('hidden');
                $moduleEl.find('.js-submit-area').removeClass('hidden');
                $moduleEl.find('.js-backbutton').removeClass('hidden');
                what_to_validate_flag="Property Sold/Rented Out";
                stepNumber=2;
                break;

            case "Report_Spam":   
                $moduleEl.find('.js-error_category').addClass('hidden');
                $moduleEl.find('.js-report_spam').removeClass('hidden');
                $moduleEl.find('.js-submit-area').removeClass('hidden');
                $moduleEl.find('.js-backbutton').removeClass('hidden');
                what_to_validate_flag="Report Spam";
                stepNumber=2;
                break;

            case "Other":      
                $moduleEl.find('.js-error_category').addClass('hidden');
                $moduleEl.find('.js-report_other').removeClass('hidden');
                $moduleEl.find('.js-submit-area').removeClass('hidden');
                $moduleEl.find('.js-backbutton').removeClass('hidden');
                what_to_validate_flag="Other";
                stepNumber=2;
                break;

            case "REPORT_ERROR_BACK":
                _goback();
                break;

            case "REPORT_ERROR_CLOSE":
                context.broadcast('reportError:close',stepNumber);
                context.broadcast('popup:close');
                break;

            case "REPORT_ERROR_SUBMIT":
                if (global.isProcessing) {return;}
                _collect_data();
                actual_close = true;
                invalidFeedbackCheck1 = window.sessionStorage.getItem(listingId);
                invalidFeedbackCheck2 = window.sessionStorage.getItem(step1);
                invalidFeedback = invalidFeedbackCheck1  &&  invalidFeedbackCheck2;
                if (invalidFeedback) {
                    _showError('INVALID_FEEDBACK', 'GLOBAL');
                    context.broadcast("feedback already submitted"); 
                }
                else if(_validate_data()) { 
                context.broadcast("reportErrorDataValidated",phone_number);
                var userId;
                loginService.isUserLoggedIn().then((response) => {
                   userId = response.data.id;
                   _submit(userId);
                }, ()=>{
                   _submit();
                });
                }
                else {
                context.broadcast("reportErrorDataInvalid");    
                }
                break;
            }
        }

        function _submit(userId) {
            _getvalue(userId);

             _showHideProcessing(false);
             reportService.sendMail(mailUrl, mailData)
             .then(() => {
                _showHideProcessing(true);
                _gotowelcomenote();
                window.sessionStorage.setItem(listingId,'listingId');
                window.sessionStorage.setItem(step1,'reportErrorCategoryChosen');
              },() => {
               _showHideProcessing(true); 
               _showError('SERVER_ERROR', 'GLOBAL');
              });
            }

            function onmessage(name,data)  {

            switch(name) {
                case "popup:change":
                case "popup:closed":
                     _close();
                    break;
                case "singleSelectDropdownChanged":
                case "singleSelectDropdownInitiated":
                    let params = data && data.params && JSON.parse(decodeURI(data.params));
                    _setCountry(params);
                    break;
            }

            }



    return {
            messages,
            behaviors,
            init,
            onclick,
            onmessage,
            destroy
        };
    });
});
