"use strict";
define(["common/sharedConfig",
        "services/apiService",
        'services/utils',
        "doT!modules/localityComparison/views/index",
        "doT!modules/localityComparison/views/trend_buy_locality",
        "doT!modules/localityComparison/views/trend_rent_locality",
        "doT!modules/localityComparison/views/trend_buy_project",
        'services/loggerService',
        "modules/localityComparison/scripts/behaviors/tracking"
        ], function (sharedConfig, apiService, Utils, rowTemplate, rowTemplateTrendBuyLocality, rowTemplateTrendRentLocality, rowTemplateTrendBuyProject, logger) {
    Box.Application.addModule('localityComparison', function (context) {
        const apiHandlers = sharedConfig.apiHandlers;
            // TYPEAHEAD_IDS = ['locality-typeahead-buy',
            //                  'locality-typeahead-rent',
            //                  'locality-typeahead-buy-apartment', 
            //                  'locality-typeahead-buy-independent-floor', 
            //                  'locality-typeahead-buy-villa',
            //                  'locality-typeahead-buy-plot'
            //                  ];

        var moduleEl,
            moduleConfig,
            typeaheadId,
            localityIdArray,
            existingLocalityError,
            addLocalityError,
            loader,
            template,
            parser,
            entityDetail,
            unitType;
        var templateConfig = {
            'default': rowTemplate,
            'trend_buy_locality': rowTemplateTrendBuyLocality,
            'trend_buy_project': rowTemplateTrendBuyProject,
            'trend_rent_locality': rowTemplateTrendRentLocality
        };
        var parserConfig = {
            'trend_buy_locality': 'trend',
            'trend_rent_locality': 'trend',
            'trend_buy_project': 'similar'
        };
        var entityConfig = {
            'locality': apiHandlers.getLocalityDetail,
            'project': apiHandlers.getProjectDetail
        };

        function init() {
            moduleEl = context.getElement();
            moduleConfig = context.getConfig();
            localityIdArray = JSON.parse(moduleConfig.localityIdArray || '[]');
            typeaheadId=Box.DOM.queryData(moduleEl,'module','typeAhead').id;
            existingLocalityError = $(Box.DOM.queryData(moduleEl, 'type', 'existing-locality-error'));
            addLocalityError = $(Box.DOM.queryData(moduleEl, 'type', 'add-errror'));
            loader = $(Box.DOM.queryData(moduleEl, 'type', 'loader'));
            template = (moduleConfig.template && templateConfig[moduleConfig.template]) || templateConfig.default;
            parser = (moduleConfig.template && parserConfig[moduleConfig.template]) || '';
            entityDetail = (moduleConfig.entity && entityConfig[moduleConfig.entity]) || entityConfig['locality'];
            unitType = moduleConfig.unitType || '';
        }

        function onclick(event, clickElement, elementType) {
            switch(elementType){
                case 'remove':
                    let id = parseInt(clickElement.dataset.id);
                    Box.DOM.query(moduleEl, `tr[data-id=${id}]`).remove();
                    localityIdArray.splice(localityIdArray.indexOf(id), 1);
                    _broadcastTracking('localityComparison_remove',{id});
                    break;
                case "nav-link":
                    _broadcastTracking('localityComparison_navigate',{localityId:clickElement.dataset.localityId, name:clickElement.dataset.name});
                    context.broadcast('serpLinkClicked',{});
            }
        }

        function onmessage(name, data) {
            switch(name){
                case 'searchResultClicked':
                    if(data.moduleEl.id==typeaheadId) {
                        if(localityIdArray.indexOf(data.dataset.localityId) == -1){
                            let category = data.moduleEl.id.split('-')[2],
                                localityId = data.dataset.localityId,
                                projectId = data.dataset.projectid;
                            _addLocalityToCompare({localityId,category,unitType, projectId});
                        } else {
                            existingLocalityError.removeClass('fadeit');
                            setTimeout(function() {existingLocalityError.addClass('fadeit');}, 2000);
                        }
                    }
                    break;
            }
        }

        function formatPrice(val){
            val = val && Math.floor(val);
            return Utils.formatNumber(val, { type : 'number', seperator : window.numberFormat });
        }

        function _addLocalityToCompare(config) {
            loader.removeClass('fadeit');
            apiService.get(entityDetail({
                localityId:config.localityId,
                projectId: config.projectId,
                query: {
                    parser
                }
            }).url).then((response) => {    
                loader.addClass('fadeit');
                $(moduleEl).find('tbody').prepend(template({response, config, formatPrice}));
                localityIdArray.push(config.localityId || config.projectId);
                _broadcastTracking('localityComparison_added',{localityId:config.localityId});
            }, (error) => {
                loader.addClass('fadeit');
                addLocalityError.removeClass('fadeit');
                setTimeout(function() {addLocalityError.addClass('fadeit');}, 2000);
                logger.info(JSON.stringify(error));
            });
        }

        function _broadcastTracking(event, data) {
            data.moduleEl = moduleEl;
            context.broadcast(event, data);
        }
        return {
            messages: ['searchResultClicked'],
            behaviors: ['localityComparisonTracking'],
            init: init,
            onmessage: onmessage,
            onclick: onclick
        };
    });
});
