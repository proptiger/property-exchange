'use strict';
define([
    'common/trackingConfigService',
    'services/trackingService'
], function(t, trackingService) {
    Box.Application.addBehavior('localityComparisonTracking', function(context) {
        const REMOVE_MSG = 'localityComparison_remove',
            ADD_MSG = 'localityComparison_added',
            NAVIGATE_MSG = "localityComparison_navigate";

        var messages = [REMOVE_MSG, ADD_MSG, NAVIGATE_MSG],
            moduleEl = context.getElement();

        var onmessage = function(eventName, data) {
            if(data.moduleEl.id == moduleEl.id) {
                let event, label, name,
                    sourceModule = t[data.sourceModule], 
                    category = t.COMPARE_CATEGORY;

                switch (eventName) {
                    case REMOVE_MSG:
                        event = t.ADD_REMOVE_EVENT;
                        name = 'remove';
                        label = data.localityId;
                        break;
                    case ADD_MSG:
                        event = t.ADD_REMOVE_EVENT;
                        label = data.localityId;
                        name = 'select';
                        break;
                    case NAVIGATE_MSG:
                        event = t.CLICK_EVENT;
                        label = data.localityId;
                        name = data.name;
                }

                let properties = {};
                properties[t.CATEGORY_KEY] = category;
                properties[t.SOURCE_MODULE_KEY] = sourceModule;
                properties[t.NAME_KEY] = name;
                properties[t.LABEL_KEY] = label;

                trackingService.trackEvent(event, properties);
            }
        };

        return {
            messages: messages,
            onmessage: onmessage,
            init: function() {},
            destroy: function() {}
        };
    });
});
