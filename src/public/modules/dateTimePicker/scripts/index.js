// jshint ignore: start
'use strict';

define(['./dateTimePicker'], function() {
    Box.Application.addModule("dateTimePicker", function(context) {

        var onfocusin = function(event, element) {
            element.parentNode.classList.add('active');
        };

        var onfocusout = function(event, element) {
            if (element && !element.value) {
                element.parentNode.classList.remove('active');
            }
        };

        var init = function() {
            let moduleEl,
                config,
                minDate = new Date(),
                currentMonth = minDate.getMonth(),
                currentDate = minDate.getDate(),
                currentYear = minDate.getFullYear();
            minDate.setDate(minDate.getDate() + 1);
            const PLACEHOLDER = '.js-input-placeholder';
            moduleEl = context.getElement();
            config = context.getConfig() || {};

            // Will be called when page appears
            $(moduleEl).bootstrapMaterialDatePicker({
                weekStart: 0,
                time: false,
                futureDate: config.futureDate || true,
                pastDate: config.pastDate || true,
                currentDate: new Date(currentYear-25, currentMonth, currentDate),
                maxDate: new Date(currentYear, currentMonth, currentDate),
                minDate: config.minDate || minDate
            }).on('change', function(e, selectedDate) {
                // time has been fixed to 11 am of selected date
                selectedDate.set({ 'hour': 11, 'minute': 0, 'second': 0 });
                context.broadcast('dateTimePicker:change', {
                    value: selectedDate.unix() * 1000,
                    el: moduleEl
                });
            });
        };

        return {
            init,
            onfocusin,
            onfocusout
        };
    });
});
