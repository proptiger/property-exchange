define([
    'services/filterConfigService',
    'services/urlService',
    'services/utils',
    'services/commonService',
    'services/viewStatus',
    'services/requirmentNotificationService',
    "common/sharedConfig",
    "services/experimentsService",
    'modules/listing/scripts/behaviors/listingTracking',
    'behaviors/requestPhotosTracking',
], function(filterConfigService, urlService, utils, commonService, viewStatus, RequirmentNotificationService, sharedConfig, experimentsService) {
    'use strict';
    Box.Application.addModule("listing", function(context) {

        var LISTING_CARD = 'listing-card',
            TRANSACTION_PART = '[h-success-rate]';
        var element, moduleId, selector, configData, listingId, propertyId, projectId, mainImageId, verificationDate, rank,
            serverTime, seller = {},
            pageData, $, isMap, pageListingType, isRental, listingUrl, isMobileRequest, transactionElement,
            $sellerListingElement;

        var hoverStartTime, $moduleEl, trackRatingReivew = true;

        var _setViewStatus = function() {
            let status = viewStatus.getStatus({
                time: verificationDate,
                serverTime: serverTime,
                id: listingId,
                type: "listing"
            });
            let domElement = $(element).find('[data-js-seen="' + listingId + '"]');
            if(status === 'contacted'){
                domElement.removeClass('hide');
                domElement.addClass('contacted');
                domElement.html('Already Contacted');
            }else if (status === 'seen') {
                domElement.removeClass('hide');
                domElement.addClass('seen');
                domElement.html('Seen');
            } else if (status === 'new') {
                domElement.removeClass('hide');
            }
        };

        function _getTackingData(dataset= {}) {

            return {
                rank: configData.rank,
                projectId: configData.projectId,
                projectStatus: configData.projectStatus,
                builderId: configData.builderId,
                price: configData.price,
                propertyType: configData.propertyType,
                bedrooms: configData.isPlot ? '0'.toString() : configData.bedrooms,
                companyId: configData.companyId,
                companyScore: configData.companyRating,
                localityId: configData.localityId,
                suburbId: configData.suburbId,
                cityId: configData.cityId,
                listingId: configData.id,
                listingScore: configData.listingScore,
                listingCategory: configData.listingCategory,
                moduleId: moduleId,
                linkType: dataset.linkType,
                linkName: dataset.linkName,
                sellerType: dataset.sellerType,
                isOriginalSearchResult:configData.isOriginalSearchResult,
                recency: configData.recency,
                imageCount: configData.imageCount,
                isSelectListing : configData.isMakaanSelectSeller && configData.listingCategory && configData.listingCategory.toLowerCase()=="rental"
            };
        }

        // function _sendPushNotificationTag(){
        //     let tagData = {
        //         cityId: configData.cityId,
        //         localityId: configData.localityId,
        //         projectId: configData.projectId,
        //         budget: configData.price,
        //         bhk: configData.bedrooms,
        //         sellerId: configData.companyId,
        //         serpType: pageData.trackPageType        //     };
        //     pushNotificationService.sendNotificationTags("VIEWED_PROPERTY", tagData);
        // }

        var scroll = 0;
        var _trackScroll = function () {
            if (scroll) {
                return;
            }
            var minWindow = $(window).scrollTop(),
            maxWindow = minWindow + $(window).height(),
            minElement = $(element).offset().top,
            maxElement = minElement + $(element).height();
            if (maxElement < minWindow || minElement > maxWindow) {
                return;
            }

            context.broadcast('propertyVisible', _getTackingData());
            scroll = 1;
        };

        var _getSerpImagesApiUrl = function() {
            return sharedConfig.apiHandlers.getImages({
                        objectType: "listing",
                        objectId: listingId,
                        query: {
                            'projectId': projectId,
                            "propertyId": propertyId,
                            "mainImageId": mainImageId,
                            "isGallery": true
                        }
                    }).url;
        };

        var _openPropertyInNewTag = function(listingUrl){
            let propertyOverviewWindow = window.open(listingUrl, '_blank'),
                prevPageType = utils.getOmniturePageName();
                propertyOverviewWindow.listingIds = $('[data-module="listingsWrapper"]').data('allListing');
                propertyOverviewWindow.sessionStorage.setItem("prevPageType",JSON.stringify({pageType: prevPageType}));
        };

        var onMouseEnterTransactionPart = function() {
            context.broadcast('trackToolTipShown',{moduleId : moduleId, listingId : propertyId});
        };

        var bindMouseEvents = function(element){
                $(element).on('mouseenter', onMouseEnterTransactionPart);
        };

        function _bindMouseForRatingReview(){
            if(!isMobileRequest && !(pageData.isCommercial)){
                let timeout = null;
                let sellerListingElement = $sellerListingElement.get(0);
                $sellerListingElement.on('mouseenter', ()=>{
                    if(!_isOwnerListing()){
                        _loadMiniProfileModule();
                        context.broadcast('showRatingReviewWrapper');
                        _loadRatingReviewWrapper(sellerListingElement);
                        timeout = setTimeout(function () {
                            _trackRatingReviewTooltip();
                        }, 1000);
                    }
                });

                $sellerListingElement.on('mouseleave', ()=>{
                    clearTimeout(timeout);
                    if(!isMobileRequest && !_isOwnerListing()){
                        context.broadcast('hideRatingReviewWrapper');
                    }
                });
            }
        }


        function _getLeadFromData(dataset = {}) {
           let rawData = $.extend(true,{},configData) || {};
            rawData = $.extend(rawData,dataset);
            if(dataset.step && !isMobileRequest) {
                rawData.step = dataset.step;
            }
            if(isMobileRequest){
                rawData.step ="CALL_NOW";
            }
            if(rawData.propertyType) {
                rawData.propertyTypeLabel = rawData.propertyType;
            }
            if(rawData.unitTypeId) {
                rawData.propertyType = [rawData.unitTypeId];
            }
            if(rawData.price) {
                rawData.budget = rawData.maxBudget = rawData.minBudget = rawData.price;
            }
            rawData.bhk = rawData.bedrooms;
            rawData.listingId = rawData.id;
            rawData.salesType = rawData.listingCategory;
            rawData.id = "lead-popup";
            rawData.moduleId = moduleId;
            rawData.imageCount = configData.imageCount;
            rawData.isCommercial = utils.getPageData('isCommercial');
            return rawData;
        }

        function _openLeadForm(dataset) {
            let rawData = _getLeadFromData(dataset);
            if(window.AB_TEST && window.AB_TEST.newLeadForm){
                context.broadcast('open-new-lead-form', rawData);
            } else {
                context.broadcast('leadform:open', rawData);
            }
        }

        function miniProfileRatingReviewData(){
            let leadFormDataset = $moduleEl.find('[data-type="openLeadForm"]').data() || {};
            return {
                companyName: configData.companyName,
                companyRating: configData.companyRating,
                companyType: configData.sellerType,
                rating: (configData.companyRating||0),
                ratingCount: configData.sellerCallRatingCount,
                reviewCount: configData.sellerCompanyFeedbackCount,
                builderId: configData.builderId,
                builderName: configData.builderName,
                companyId: configData.companyId,
                companyUserId: configData.companyUserId,
                leadFormData: _getLeadFromData(leadFormDataset),
                isPaidSeller: configData.isPaidSeller,
                sellerTransactionStatus: configData.sellerTransactionStatus,
                postedBy: {
                    name: configData.companyName,
                    image: configData.companyImage,
                    backgroundColor: configData.backgroundColor,
                    nameText: configData.nameText,
                    textColor: configData.textColor,
                    type: configData.sellerType
                }
            }
        }
        function _loadRatingReviewWrapper(element){
            let dataObject = miniProfileRatingReviewData();
            let ratingReviewData = $.extend(true, {element, position: 'top_bottom' }, dataObject);
            context.broadcast('loadRatingReviewWrapper',ratingReviewData);
        }
        function _trackRatingReviewTooltip(){
            if (trackRatingReivew) {
                context.broadcast('trackRatingReviewTooltip', {
                    moduleId,
                    label: 'listing',
                    sellerStatus: configData.isPaidSeller,
                    sellerRating: (configData.companyRating || 0)
                });
                trackRatingReivew = false;
            }
        }
        function _loadMiniProfileModule(){
            if (!commonService.checkModuleLoaded('sellerMiniProfile')) {
                context.broadcast('loadModule', [{
                    name: 'sellerMiniProfile',
                }]);
            }
        }
        function _loadSellerMiniProfile(){
            _loadMiniProfileModule();
            commonService.bindOnModuleLoad('sellerMiniProfile', function() {
                context.broadcast("loadSellerMiniProfile", miniProfileRatingReviewData());
            });
        }

        function _isOwnerListing(){
            return (configData && configData.companyType === 'OWNER');
        }

        function openListing(elementClicked, event){
            var stopPropagation = true;
            if(element.classList.contains('js-deficit-listing-scroll')){
                let dataset = $(element).data()||{};
                context.broadcast('trackDeficitListingClicked',{listingId:dataset.listingId,sellerId:dataset.sellerId,moduleId: moduleId,nearbyIndex:dataset.trackScroll});
            }
            if (isMobileRequest && listingUrl) {
                if(experimentsService.hasExperimentVariation("makaan_mob_prop_open", "same_tab")) {
                    //Update current route, for clicked listing as selected listing
                    if(window.history && window.history.replaceState) {
                        let newUrl = urlService.changeUrlParam("selectedListingId", configData.id, null, true); 
                        window.history.replaceState({ url: newUrl }, 'state-' + newUrl, newUrl);    
                    }   
                    urlService.ajaxyUrlChange(listingUrl);
                } else {
                    _openPropertyInNewTag(listingUrl);
                }
                event.preventDefault();
            } else if (listingUrl && !elementClicked.href){
                _openPropertyInNewTag(listingUrl);
                RequirmentNotificationService.setReferrer();
            }else {
                stopPropagation = false;
            }
            return stopPropagation;
        }

        function openGallery(startIndex = 0) {
            let variation = experimentsService.getExperimentVariation('makaan_gallery_revamp'),
                galleryData = {
                    id: "main_gallery_serp"
                },
                moduleName = "gallery", 
                moduleId = "main_gallery_serp";

            if(isMobileRequest){
                switch(variation){
                    case 'gallery_new':
                        galleryData['version'] = "v2_mobile";
                    break;

                    case 'gallery_grid':
                        galleryData['id'] = "galleryGrid";
                        moduleName = moduleId = "galleryGrid";
                    break;
                }
            } 

            if (!commonService.checkModuleLoaded(moduleName)) {
                context.broadcast('loadModule', [{
                    name: moduleName,
                }]);
            }

            commonService.bindOnModuleLoad(moduleName, function() {
                let leadFormData = _getLeadFromData();
                let startIndex = $moduleEl.attr(sharedConfig.DATA_CURRENT_SLIDE_INDEX);
                let data = {
                    "id": galleryData.id,
                    "dataSourceUrl": _getSerpImagesApiUrl(),
                    "listingData":configData,
                    "leadFormData":leadFormData,
                    "url": listingUrl,
                    "serp":true,
                    startIndex
                };
                if(galleryData.version){
                    data['version'] = "v2_mobile";
                }
                context.broadcast('start_gallery', data);
            });
        }

        /** For more than one image, will be handled in carousel module */
        function imageAspectRatioHandling() {
            if(configData.imageCount === 1) {
                let $imgEl = $moduleEl.find("img:first");
                let $boxEl = $moduleEl.find(".img-slide");

                if($imgEl && $boxEl) {
                    $imgEl.addClass(utils.getImageClassForAR(configData.mainImageWidth,
                        configData.mainImageHeight, $boxEl.width(), $boxEl.height()));
                }
            }
        }

        return {
            messages: ['view_status_changed'],
            behaviors: ['listingTracking', 'requestPhotosTracking'],

            /**
             * Initializes the module and caches the module element
             * @returns {void}
             */
            init: function() {
                //let moduleConfig,
                  //  pageExtraData = utils.getPageExtraData();
                element = context.getElement();
                moduleId = element.id;
                configData = context.getConfig() || {};
                $ = context.getGlobal('jQuery');

                $moduleEl = $(element);

                selector = configData.selector;

                listingId = configData.id;
                propertyId = configData.propertyId;
                verificationDate = configData.verificationDate;
                projectId = configData.projectId;
                mainImageId = configData.mainImageId;
                serverTime = configData.serverTime;
                listingUrl = configData.listingUrl;
                rank = configData.rank;
                pageData = utils.getPageData() || {};
                isMap = pageData.isMap;
                pageListingType = pageData.listingType ? pageData.listingType.toLowerCase() : null;
                isRental = pageListingType && pageListingType === 'rent' ? true : false;
                seller.type = configData.companyType ? configData.companyType.toUpperCase() : '';
                seller.id = configData.companyId;
                seller.name = configData.companyName;
                seller.builderId = configData.builderId;
                seller.builderName = configData.builderName;
                isMobileRequest = utils.isMobileRequest();
                transactionElement = $(element).find(TRANSACTION_PART);
                $sellerListingElement = $(element).find('[data-type="seller-listings"]');
                bindMouseEvents(transactionElement);
                _bindMouseForRatingReview();
                context.broadcast('moduleLoaded', {
                    'name': 'listing',
                    'id': element.id
                });
                _setViewStatus();

                _trackScroll();
                
                window.addEventListener('scroll', _trackScroll,{passive:true});
                let configBroadcast = context.broadcast.bind(context, "listing-configData", configData);

                commonService.bindOnModuleLoad("serpPage", configBroadcast);
                commonService.bindOnModuleLoad("shortlist", configBroadcast, [`shortlist-${listingId}`]);
                commonService.bindOnModuleLoad("carousel", configBroadcast, [`carousel-${listingId}`]);

                $moduleEl.addClass("initialized");

                imageAspectRatioHandling();
            },

            onmessage: function(name) {
                switch (name) {
                    case "view_status_changed":
                        _setViewStatus();
                        break;
                    }
            },
            onmouseover: function(event,element,elementType){
                switch(elementType){
                    case 'image-div':
                    case 'back-btn':
                    case 'next-btn':
                        $moduleEl.find('.js-hideOnHover').addClass('hide');
                        $moduleEl.find('.js-showOnHover').removeClass('hide');
                        break;
                }

            },
            onmouseout: function(event,element,elementType){
                switch(elementType){
                    case 'image-div':
                    case 'back-btn':
                    case 'next-btn':
                        $moduleEl.find('.js-hideOnHover').removeClass('hide');
                        $moduleEl.find('.js-showOnHover').addClass('hide');
                        break;
                }
            },
            onmouseenter: function() {
                hoverStartTime = new Date().valueOf();

                if (isMap && isMobileRequest) {
                    return;
                }
                context.broadcast('serpListingMouseentered', {
                    id: listingId,
                    selector: selector
                });
            },

            onmouseleave: function() {
                let trackingData = _getTackingData();
                trackingData.hoverTime = new Date().valueOf() - hoverStartTime;
                context.broadcast('listingHovered', trackingData);

                if (isMap && isMobileRequest) {
                    return;
                }

                context.broadcast('serpListingMouseLeft', {
                    id: listingId,
                    selector: selector
                });
            },

            onclick: function(event, element, elementType) {
                let dataset = $(element).data() || {},
                stopPropagation = true,
                startIndex;

                switch (elementType) {
                    case 'social-share':
                        context.broadcast('open_share_box', {
                            url: dataset.url,
                            page: pageData.trackPageType
                        });
                        break;
                    case LISTING_CARD:
                        context.broadcast('trackFullCardClickSERP', _getTackingData());
                        stopPropagation = openListing(element, event);
                        break;
                    case "listing-link":
                        RequirmentNotificationService.setReferrer();
                        context.broadcast('listingTypeClicked', _getTackingData());
                        if(element.classList.contains('js-deficit-listing')){
                            context.broadcast('trackDeficitListingClicked',{listingId:dataset.listingId,sellerId:dataset.sellerId,moduleId: moduleId,nearbyIndex:dataset.index});
                        }
                        stopPropagation = openListing(element);
                        break;
                    case 'more-img':
                        startIndex = $(event.target).closest('.img-slide').data("index");
                        if(configData.imageCount) {
                            context.broadcast('trackMoreImagesClick', _getTackingData());
                            openGallery(startIndex);
                        }
                        break;
                    case 'image-div':
                        if(isMobileRequest && experimentsService.hasExperimentVariation(sharedConfig.propcard_experiment_key, "true")) {
                            context.broadcast('trackOpenListingThroughImage', _getTackingData());
                            stopPropagation = openListing(element, event);
                        } else if(configData.imageCount){
                            context.broadcast('listingImageClicked', _getTackingData());
                            openGallery(startIndex);
                        }
                        break;
                    case 'dummy-img':
                    case 'openLeadForm':
                        if(element.classList.contains('js-deficit-listing')){
                            context.broadcast('trackDeficitListingConnectClicked',{listingId:dataset.listingId,sellerId:dataset.sellerId,moduleId: moduleId,nearbyIndex:dataset.index});
                        }
                        context.broadcast('trackOpenLeadFormSerp', _getTackingData(dataset));
                        _openLeadForm(dataset);
                        break;
                    case 'openLeadFormViewPhone':
                        context.broadcast('trackOpenViewPhoneListingCard', _getTackingData(dataset));
                        _openLeadForm(dataset);
                        break;
                    case 'commutePoints':
                        context.broadcast('loadModule', {
                            "name": 'commutePoints'
                        });
                        commonService.bindOnModuleLoad('commutePoints', function(){
                            context.broadcast('openCommutePointsPopup', dataset);
                        })
                        break;
                    case 'seller-profile':
                        if(!_isOwnerListing() && dataset.isclickable){
                            event.preventDefault();
                            _loadSellerMiniProfile();
                        } else if(dataset.sellerview || dataset.builderview || pageData.isCommercial) {
                            event.preventDefault();
                        }
                        break;
                    case 'read-more':
                        context.broadcast('listingReadMoreClicked', _getTackingData());
                        stopPropagation = openListing(element, event);
                        break;
                    case 'price-link':
                        context.broadcast('listingPriceClicked', _getTackingData());
                        stopPropagation = openListing(element, event);
                        break;
                    case 'projName':
                        // _sendPushNotificationTag();
                        context.broadcast('trackProjectClickSERP', _getTackingData(dataset));
                        stopPropagation = openListing(element, event);
                        break;
                    case 'localityName':
                        context.broadcast('trackLocalityClickSERP', _getTackingData(dataset));
                        stopPropagation = openListing(element, event);
                        break;
                    case 'seller-listings':
                        if(isMobileRequest && experimentsService.hasExperimentVariation(sharedConfig.propcard_experiment_key, "true")) {
                            context.broadcast('trackOpenListingThroughSeller', _getTackingData());
                            stopPropagation = openListing(element, event);
                            break
                        }
                    case 'seller-rating':
                        dataset = $(element).closest('.js-seller-dtls').data();
                        if(!_isOwnerListing() && isMobileRequest){
                            event.preventDefault();
                            _loadSellerMiniProfile();
                            context.broadcast('trackRatingReviewTooltip',{
                                moduleId,
                                label: 'listing',
                                sellerStatus: configData.isPaidSeller,
                                sellerRating: (configData.companyRating||0)
                            });
                        } else {
                            stopPropagation = openListing(element, event);
                        }
                        break;
                }

                if (stopPropagation) {
                    event.stopPropagation();
                }

            },

            /**
             * Destroys the module and clears references
             * @returns {void}
             */
            destroy: function() {
                window.removeEventListener('scroll', _trackScroll,{passive:true});
                $(transactionElement).off('mouseenter', onMouseEnterTransactionPart);
                $sellerListingElement.off('mouseenter');
                $sellerListingElement.off('mouseleave');
                element = null;
                $sellerListingElement = null;
                transactionElement = null;
            }
        };
    });
});
