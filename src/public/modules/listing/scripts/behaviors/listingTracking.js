define([
    'common/trackingConfigService',
    'services/trackingService',
    'services/urlService',
    'services/utils'
], function(t, trackingService, urlService, utils) {
    'use strict';
    Box.Application.addBehavior('listingTracking', function(context) {

        var messages = [
            'listingImageClicked',
            'listingTypeClicked',
            'listingReadMoreClicked',
            'listingPriceClicked',
            'trackOpenViewPhoneListingCard',
            'trackOpenLeadFormSerp',
            'listingSellerClicked',
            'listingSellerClicked',
            'listingVisible',
            'listingHovered',
            'trackCallNowSubmit',
            'trackProjectClickSERP',
            'trackLocalityClickSERP',
            'trackFullCardClickSERP',
            'trackToolTipShown',
            'trackRatingReviewTooltip',
            'trackMoreImagesClick',
            'trackOpenListingThroughSeller',
            'trackOpenListingThroughImage',
            'trackDeficitListingClicked',
            'trackDeficitListingConnectClicked'
        ];

        var value,
            moduleId = context.getElement().id;

        var onmessage = function(msgName, data={}) {
            if (!data.moduleId || (data.moduleId && data.moduleId != moduleId)) {
                return;
            }

            let category = t.SERP_CARD_CATEGORY,
                event, label, noninteractive,
                leadLabelConfig = t.LEAD_LABELS,
                sellerTypeLabel = t.SELLER_TYPE_LABEL,
                leadSourceModuleConfig = t.LEAD_SOURCE_MODULES,
                sourceModule,sellerRating, sellerStatus,listingId,sellerId,sectionClicked,
                properties = {};


            switch (msgName) {
                case 'trackMoreImagesClick':
                    properties[t.ADDITIONAL_CD57] = "count";
                    event = t.VIEW_DETAILS_EVENT;
                    label = t.GALLERY_LABEL;
                    break;
                case 'listingImageClicked':
                    properties[t.ADDITIONAL_CD57] = "tap";
                    event = t.VIEW_DETAILS_EVENT;
                    label = t.GALLERY_LABEL;
                    break;
                case 'listingTypeClicked':
                    event = t.VIEW_DETAILS_EVENT;
                    label = t.TYPE_LABEL;
                    break;
                case 'listingReadMoreClicked':
                    event = t.VIEW_DETAILS_EVENT;
                    label = t.MORE_LABEL;
                    break;
                case 'listingPriceClicked':
                    event = t.VIEW_DETAILS_EVENT;
                    label = t.PRICE_LABEL;
                    break;
                case 'trackOpenViewPhoneListingCard':
                    category = t.LEAD_FORM_CATEGORY;
                    event = t.OPEN_EVENT;
                    label = leadLabelConfig['ViewPhoneSERP'];
                    sourceModule = leadSourceModuleConfig.SERPCard;
                    sectionClicked = 'listingCardViewPhone';
                    break;
                case 'trackCallNowLeadFormSerp':
                    category = t.LEAD_FORM_CATEGORY;
                    event = t.SUBMIT_EVENT;
                    label = leadLabelConfig['CallNow'];
                    sourceModule = leadSourceModuleConfig.SERPCard;
                    break;

                case 'listingSellerClicked':
                    event = t.VIEW_SELLER_DETAILS_EVENT;
                    break;

                case 'trackOpenLeadFormSerp':
                    category = t.LEAD_FORM_CATEGORY;
                    event = t.OPEN_EVENT;
                    label = data.sellerType ? `connect ${sellerTypeLabel[data.sellerType]} listing` : leadLabelConfig["SingleSeller"];
                    if(utils.isMobileRequest()) {
                        label = data.sellerType ? `connect ${sellerTypeLabel[data.sellerType]} listing` : leadLabelConfig['CallNow'];
                    }
                    sourceModule = leadSourceModuleConfig.SERPCard;
                    sectionClicked = 'listingCardConnectNow';
                    break;

                case 'trackCallNowSubmit':
                    category = t.LEAD_FORM_CATEGORY;
                    event = t.SUBMIT_EVENT;
                    label = leadLabelConfig['CallNow'];
                    sourceModule = leadSourceModuleConfig.SERPCard;
                    break;
                case 'listingVisible':
                    event = t.PROPERTY_VISIBLE_EVENT;
                    noninteractive = 1;
                    break;
                case 'listingHovered':
                    event = t.HOVER_ON_CARD_EVENT;
                    label = data.hoverTime;
                    noninteractive = 1;
                    break;

                case 'trackProjectClickSERP':
                    event = t.VIEW_PROJECT_EVENT;
                    break;

                case 'trackLocalityClickSERP':
                    event = t.VIEW_LOCALITY_EVENT;
                    break;

                case 'trackFullCardClickSERP':
                    event = t.VIEW_DETAILS_EVENT;
                    label = t.FULL_CARD_LABEL;
                    sectionClicked = data.isOriginalSearchResult ? 'serpListingCard' : 'serpSimilarListingCard';
                    break;

                case 'trackToolTipShown':
                    event = t.TRANSACTION_TOOLTIP_DISPLAY;
                    category = t.SERP_VIEW_CATEGORY;
                    break;
                case 'trackRatingReviewTooltip':
                    category = t.DEAL_REVIEW_CATEGORY;
                    event = t.TOOLTIP_EVENT;
                    label = data.label;
                    sellerStatus = data.sellerStatus?t.SELLER_PAID_STATUS.PAID:t.SELLER_PAID_STATUS.NOT_PAID;
                    sellerRating = data.sellerRating;
                    break;
                case 'trackOpenListingThroughSeller':
                    event = t.VIEW_DETAILS_EVENT;
                    label = t.SELLER_LABEL;
                    break;
                case 'trackOpenListingThroughImage':
                    event = t.VIEW_DETAILS_EVENT;
                    label = t.IMAGE_LABEL;
                    break;
                case 'trackDeficitListingClicked':
                    event = t.CARD_CLICK;
                    category = 'Deficit Campaign Sellers';
                    label = 'card_clicked_nearby';
                    listingId = data.listingId;
                    sellerId = data.sellerId;
                    sourceModule = data.nearbyIndex;
                    sectionClicked = `deficit_${data.nearbyIndex}`;
                    break;
                case 'trackDeficitListingConnectClicked':
                    event = 'Open Lead Form';
                    category = 'Deficit Campaign Sellers';
                    label = 'lead_form_nearby';
                    listingId = data.listingId;
                    sellerId = data.sellerId;
                    sourceModule = data.nearbyIndex;
                    sectionClicked = `deficit_${data.nearbyIndex}_ConnectNow`;
                    break;

            }

            properties[t.CATEGORY_KEY] = category;
            properties[t.LABEL_KEY] = label;
            properties[t.NON_INTERACTION_KEY] = noninteractive;
            properties[t.VALUE_KEY] = parseInt(urlService.getUrlParam('page')) || 1;
            properties[t.RANK_KEY] = data.rank;
            properties[t.PROJECT_ID_KEY] = data.projectId ? parseInt(data.projectId) : undefined;
            properties[t.PROJECT_STATUS_KEY] = t.makeValuesUniform(data.projectStatus,t.PROJECT_STATUS_NAME) ; 
            properties[t.BUILDER_ID_KEY] = data.builderId ? parseInt(data.builderId) : undefined;
            properties[t.BUDGET_KEY] = data.price;
            properties[t.UNIT_TYPE_KEY] = t.makeValuesUniform(data.propertyType,t.UNIT_TYPE_MAP) ;
            properties[t.BHK_KEY] = data.bedrooms ? parseInt(data.bedrooms) : undefined;
            properties[t.SELLER_ID_KEY] = data.companyId;
            properties[t.SELLER_SCORE_KEY] = parseFloat(data.companyScore) ? parseFloat(data.companyScore) : undefined;
            properties[t.LOCALITY_ID_KEY] = data.localityId;
            properties[t.SUBURB_ID_KEY] = data.suburbId;
            properties[t.CITY_ID_KEY] = data.cityId;
            properties[t.LISTING_ID_KEY] = data.listingId;
            properties[t.LISTING_SCORE_KEY] = data.listingScore;
            properties[t.SOURCE_MODULE_KEY] = sourceModule;
            properties[t.ADDITIONAL_CD49] = data.imageCount === 0 ? "zero" : data.imageCount;
            properties[t.SELECT_PROPERTY_KEY] = data.isSelectListing ? 'Select Listing':undefined;
            if(sellerRating){
                properties[t.SELLER_SCORE_KEY] = sellerRating;
            }
            if(sellerStatus){
                properties[t.SELLER_STATUS_KEY] = sellerStatus;
            }
            if(listingId){
                properties[t.LISTING_ID_KEY] = listingId;
            }
            if(sellerId){
                properties[t.SELLER_ID_KEY] = sellerId;
            }
            value = utils.getTrackingListingCategory(data.listingCategory);
            properties[t.LISTING_CATEGORY_KEY] = t.makeValuesUniform(value,t.LISTING_CATEGORY_MAP) ;
            properties[t.LINK_TYPE_KEY] = data.linkType && data.linkType.toLowerCase();
            properties[t.LINK_NAME_KEY] = data.linkName && data.linkName.toLowerCase();
            properties[t.SECTION_CLICKED] = sectionClicked;
            if(data.isOriginalSearchResult===false){
                properties[t.SOURCE_MODULE_KEY] = 'Targetted';
            }
            trackingService.trackEvent(event, properties);
        };

        return {
            messages: messages,
            onmessage: onmessage,
            init: function() {},
            destroy: function() {}
        };
    });
});
