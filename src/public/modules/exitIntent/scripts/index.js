"use strict";
define([
    "doT!modules/exitIntent/views/index",
    "doT!modules/exitIntent/views/thanks",
    'services/utils',
    'services/commonService',
    "services/localStorageService",
    "modules/exitIntent/scripts/behaviors/exitIntentTracking"
], (mainTemplate, thanksTemplate, utils, commonService, localStorageService) => {
    Box.Application.addModule("exitIntent", function (context) {
        const config = {
                SELECTOR: {
                    PHONE: {
                        FIELD: '[data-selector=PHONE_NUMBER] input',
                        ERROR: '#js-err-phone'
                    },
                    EXIT_INTENT_POPUP_SEEN: 'EXIT_INTENT_POPUP_SEEN'
                },
                ERROR_MSG: {
                    PHONE: 'Please provide valid phone number'
                },
                EI_REVERSE_PERCENTAGE: .15,
                
            };

        var moduleConfig,
            moduleElement,
            eiScrollMaxLimit,
            eiScrollReverseLimit,
            eiReachMaxLimit = false,
            eiSkip = false,
            eiTimeoutID,
            lastScrollTop = 0;

        var _openPopup = function (id) {
            context.broadcast('popup:open', {
                id: id,
                options: {
                    config: {
                        modal: true
                    }
                }
            });
        }
        var _closePopup = function () {
            context.broadcast('popup:close', {});
        }
        var _startAllModules = function () {
            commonService.startAllModules(moduleElement);
        }
        var _stopAllModules = function () {
            commonService.stopAllModules(moduleElement);
        }
        var _validatePhone = function(phone){
            let flag = true;
            phone = utils.validatePhone(phone, 'india');
            if (!phone) {
                flag = false;
                $(moduleElement).find(config.SELECTOR.PHONE.ERROR).html(config.ERROR_MSG.PHONE);
                setTimeout(function () {
                    $(moduleElement).find(config.SELECTOR.PHONE.ERROR).html("");
                }, 5000);
            }
            return flag;
        }
        function _getScrolledPercent() {
            let scrollTop = $(window).scrollTop(),
                docheight = $(document).height(),
                winheight = $(window).height(),
                trackLength = docheight - winheight,
                scrolled = Math.floor(scrollTop / trackLength * 100);
            return scrolled;
        }
        function _resetTimer() {
            window.clearTimeout(eiTimeoutID);
        }
        function _showExitIntent() {
            eiSkip = true;
            moduleElement.innerHTML = mainTemplate();
            _stopAllModules();
            _startAllModules();
            if (!localStorageService.getItem(config.SELECTOR.EXIT_INTENT_POPUP_SEEN)) {
                _openPopup('exitIntentPopup', true);
                context.broadcast('trackExitIntentSeen', {
                    label: 'Popup Seen'
                });
                localStorageService.setItem(config.SELECTOR.EXIT_INTENT_POPUP_SEEN, new Date().getTime());
            }
        }
        function _scrollFunction(){
            let st = $(window).scrollTop();
            let scrolled = _getScrolledPercent();
            if (st > lastScrollTop) {
                eiScrollMaxLimit = scrolled;
                eiScrollReverseLimit = scrolled - (scrolled * config.EI_REVERSE_PERCENTAGE);
            } else if (scrolled < eiScrollReverseLimit) {
                eiSkip = true;
            } else {
                eiReachMaxLimit = true;
            }
            lastScrollTop = st;
            _resetTimer();
            if (eiReachMaxLimit && scrolled < eiScrollMaxLimit && scrolled > eiScrollReverseLimit) {
                if (!eiSkip) {
                    eiTimeoutID = window.setTimeout(_showExitIntent, 3000);
                }
            }
        }
        return {
            behaviors: ['exitIntentTracking'],
            init: function () {
                window.addEventListener('scroll', _scrollFunction,{passive:true});
                moduleConfig = context.getConfig();
                moduleElement = context.getElement();
                context.broadcast('moduleLoaded', {
                    name: 'exitIntent',
                    id: 'exitIntent'
                });
            },
            onclick: function (event, element, elementType) {
                switch (elementType) {
                    case 'no-thanks':
                    case 'close':
                        _closePopup();
                        context.broadcast('trackExitIntentClosed', {
                            label: 'Popup Closed'
                        });
                        break;
                    case 'submit-number':
                        let phone = $(moduleElement).find(config.SELECTOR.PHONE.FIELD).val().trim();
                        if (_validatePhone(phone)){
                            context.broadcast('trackExitIntentFilled', {
                                label: 'Popup Filled',
                                phone: utils.getExitIntentPhoneEncrypt(phone)
                            });
                            moduleElement.innerHTML = thanksTemplate();
                        }
                        break;
                }
            },
            destroy: function() {
                window.removeEventListener('scroll', _scrollFunction, {passive: true});
                moduleElement = null;
            }
        };
    });
});
