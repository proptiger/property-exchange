'use strict';
define([
    'common/trackingConfigService',
    'services/trackingService'
], function(t, trackingService) {
    Box.Application.addBehavior('exitIntentTracking', function(context) {
        
        var _trackEvent = function(eventName, data) {
            var trackingObj = {};
            trackingObj[t.LABEL_KEY] = data.label;
            trackingObj[t.CATEGORY_KEY] = t.EXIT_INTENT_CATEGORY;
            trackingObj[t.EXIT_INTENT_ENCRYPT_DATA] = data.phone;
            trackingService.trackEvent(eventName, trackingObj);
        }

        return {
            messages: ['trackExitIntentSeen', 'trackExitIntentFilled', 'trackExitIntentClosed'],
            init: function() {},
            onmessage: function(name, data) {
                switch (name) {
                    case 'trackExitIntentSeen':
                        _trackEvent(t.EXIT_INTENT_SEEN, data);
                        break;
                    case 'trackExitIntentFilled':
                        _trackEvent(t.EXIT_INTENT_FILLED, data);
                        break;
                    case 'trackExitIntentClosed':
                        _trackEvent(t.EXIT_INTENT_CLOSED, data);
                        break;
                }
            },
            destroy: function() {}
        };
    });
});
