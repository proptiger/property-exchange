'use strict';
define([
        "doT!modules/rangeInputDropdown/views/rangeInputDropdown",
        "doT!modules/rangeInputDropdown/views/components/dropdownOption",
        'services/urlService',
        "services/filterConfigService",
        "services/utils"
    ],
    function(template, optionTemplate, urlService) {
        Box.Application.addModule("rangeInputDropdown", function(context) {

            const FilterConfigService = context.getService('FilterConfigService'),
                UtilService = context.getService('Utils');
            const config = context.getGlobal('config');
            const moduleElement = context.getElement();
            const DATA_TYPE = 'type',
                DATA_TARGET = 'target',
                RANGE_INPUT_MIN = 'range-input-min',
                RANGE_INPUT_MAX = 'range-input-max',
                RANGE_DROPDOWN_OPTION_MIN = 'range-dropdown-option-min',
                RANGE_DROPDOWN_OPTION_MAX = 'range-dropdown-option-max',
                RANGE_DROPDOWN_OPTIONS_WRAPPER_MIN = 'range-dropdown-options-wrapper-min',
                RANGE_DROPDOWN_OPTIONS_WRAPPER_MAX = 'range-dropdown-options-wrapper-max';

            let UPDATE_MAX_INCREMENT = 0.1, // 10%
                DROPDOWN_OPTIONS_COUNT = 6;

            var dropdownWrapperMin, dropdownWrapperMax,
                dropdownOptionsMin, dropdownOptionsMax,
                inputMin, inputMax,
                minValue, maxValue,
                pageData, moduleConfig,
                moduleElementDataType = $(moduleElement).data('type');


            var _handleBroadcast = function(name){
                var dataValue = minValue + ',' + maxValue;
                if((!(minValue || maxValue)) && maxValue !== ''){
                    dataValue = null;
                }

                context.broadcast('rangeInputDropdownChanged', {
                    name: name,
                    value: dataValue,
                    morefilter: $(moduleElement).data('morefilter'),
                    id: moduleElement.id
                });
            };

            var _checkToUpdateFilter = function(){
                let minRangeValue = $(inputMin).data('value'),
                    maxRangeValue = $(inputMax).data('value'),
                    filterUrlValue = urlService.getUrlParam(moduleElementDataType);

                filterUrlValue = filterUrlValue ? filterUrlValue.split(',') : ['',''];

                if((minRangeValue != filterUrlValue[0]) || (maxRangeValue != filterUrlValue[1])){
                    minValue = minRangeValue;
                    maxValue = maxRangeValue;
                    _handleBroadcast(moduleElementDataType);
                }
            };

            // Handle min filter changed via dropdown and input box
            var _minEventHandler = function(name, value) {
                minValue = value;
                _handleBroadcast(name);
                $(inputMax).focus();
                $(inputMax).trigger('click');
            };

            // Handle max filter changed via dropdown and input box
            var _maxEventHandler = function(name, value) {
                maxValue = value;
                _handleBroadcast(name);
            };

            // Update the max filter options with new values
            var _updateMaxOptions = function(element) {

                minValue = minValue ? parseInt(minValue) : 0;
                if (!minValue || minValue === 0) {
                    return;
                }
                var dataset = $(element).data() || {},
                    html = '', data;

                inputMax = Box.DOM.queryData(moduleElement, DATA_TYPE, RANGE_INPUT_MAX);
                maxValue = $(inputMax).data('value');

                for (var i = 0; i < DROPDOWN_OPTIONS_COUNT; i++) {
                    if(dataset.name === 'floor') {
                        var value = Math.floor(parseInt(minValue) + 4*(i+1));
                    }
                    else {
                        var value = Math.floor(parseInt(minValue) * (1 + (i + 1) * UPDATE_MAX_INCREMENT));
                    }
                    data = {
                        name: dataset.name,
                        value,
                        maxValue,
                    };

                    if (dataset.currency.toString() === 'true') {
                        data.label = '<i class="icon-rupee currency"></i> ' + UtilService.formatNumber(value, { seperator : window.numberFormat });
                    } else {
                        data.label = UtilService.formatNumber(value, { type : 'number', seperator : window.numberFormat });
                    }

                    if (dataset.suffix) {
                        data.label = data.label + ' ' + dataset.suffix;
                    }

                    html += optionTemplate(data);
                }

                data = {
                    name: dataset.name,
                    label: 'Any',
                    value: ''
                };
                html += optionTemplate(data);
                $(dropdownWrapperMax).html(html);

                // Updating reference to max options
                dropdownOptionsMax = Box.DOM.queryAllData(moduleElement, DATA_TYPE, RANGE_DROPDOWN_OPTION_MAX);
            };

            // Handle number shorthand notation in input boxes
            var _replaceValue = function(event) {
                var keyCode = event.keyCode,
                    value = event.target.value.replace(/\,/g, ''),
                    response = {
                        value: value,
                        replaced: true
                    };

                switch (keyCode) {
                    //k or K
                    case 75:
                    case 107:
                        response.value = value.replace(/[k|K]/, '000');
                        break;
                        //l or L
                    case 76:
                    case 108:
                        response.value = value.replace(/[l|L]/, '00000');
                        break;
                        //c or C
                    case 67:
                    case 99:
                        response.value = value.replace(/[c|C]/, '0000000');
                        break;
                    default:
                        response.replaced = false;
                }
                return response;
            };

            // Handle KeyUp event on input boxes
            var _defaultKeyupHandler = function(event) {
                /*if (!event.target.value) {``
                    return false;
                }*/
                if (event.keyCode !== 13) {
                    let response = _replaceValue(event);
                    $(event.target).data('value',response.value);
                    event.target.value = UtilService.formatNumber(response.value, { type : 'number', seperator : window.numberFormat });
                    return response.replaced || false;
                }
                return true;
            };

            function _renderSlider(filterData){
                var templateFromPromise =  (moduleConfig && moduleConfig.templateFromPromise) || false;

                FilterConfigService.renderFilter(moduleElement, filterData, template, templateFromPromise).then(function(){
                    dropdownWrapperMin = Box.DOM.queryData(moduleElement, DATA_TARGET, RANGE_DROPDOWN_OPTIONS_WRAPPER_MIN);
                    dropdownWrapperMax = Box.DOM.queryData(moduleElement, DATA_TARGET, RANGE_DROPDOWN_OPTIONS_WRAPPER_MAX);
                    dropdownOptionsMin = Box.DOM.queryAllData(moduleElement, DATA_TYPE, RANGE_DROPDOWN_OPTION_MIN);
                    dropdownOptionsMax = Box.DOM.queryAllData(moduleElement, DATA_TYPE, RANGE_DROPDOWN_OPTION_MAX);
                    inputMin = Box.DOM.queryData(moduleElement, DATA_TYPE, RANGE_INPUT_MIN);
                    inputMax = Box.DOM.queryData(moduleElement, DATA_TYPE, RANGE_INPUT_MAX);
                    minValue = $(inputMin).data('value');
                    maxValue = $(inputMax).data('value');

                    let eventValue = null;
                    if (minValue || maxValue) {
                        eventValue = minValue + ',' + maxValue;
                    }
                    context.broadcast('rangeInputDropdownInitiated', {
                        name: $(moduleElement).data('type'),
                        value: eventValue,
                        morefilter: $(moduleElement).data('morefilter'),
                        id: moduleElement.id
                    });
                });
            }

            return {
                messages: ['buy-rent-switch-range'],

                /**
                 * Initializes the module and caches the module element
                 * @returns {void}
                 */
                init: function() {

                    moduleConfig = context.getConfig() || {};
                    pageData = UtilService.getPageData();

                    DROPDOWN_OPTIONS_COUNT = moduleConfig.dropdownOptionsCount || DROPDOWN_OPTIONS_COUNT;
                    if(moduleElementDataType === 'budget' && pageData && pageData.listingType === 'buy'){
                        UPDATE_MAX_INCREMENT = 0.2; // 20%
                    }

                    _renderSlider(pageData);
                },

                onmessage: function(name, data){
                    switch(name){
                        case "buy-rent-switch-range":
                            _renderSlider(data);
                            break;
                    }
                },

                onclick: function(event, element, elementType) {

                    let data;
                    switch (elementType) {

                        case RANGE_INPUT_MIN:
                            $(dropdownWrapperMin).addClass(config.activeClass);
                            $(dropdownWrapperMax).removeClass(config.activeClass);
                            break;

                        case RANGE_INPUT_MAX:
                            _updateMaxOptions(element);
                            $(dropdownWrapperMin).removeClass(config.activeClass);
                            $(dropdownWrapperMax).addClass(config.activeClass);
                            break;

                        case RANGE_DROPDOWN_OPTION_MIN:
                            data = $(element).data() || {};
                            // select option
                            $(dropdownOptionsMin).removeClass(config.activeClass);
                            $(element).addClass(config.activeClass);
                            // update input field
                            $(inputMin).val(UtilService.formatNumber(data.value, { type : 'number', seperator : window.numberFormat }));
                            $(inputMin).data('value', data.value);
                            // event Handler
                            _minEventHandler(data.name, data.value);
                            break;

                        case RANGE_DROPDOWN_OPTION_MAX:
                            data = $(element).data() || {};
                            // select option
                            $(dropdownOptionsMax).removeClass(config.activeClass);
                            $(element).addClass(config.activeClass);
                            // update input field
                            $(inputMax).val(UtilService.formatNumber(data.value, { type : 'number', seperator : window.numberFormat }));
                            $(inputMax).data('value', data.value);
                            // event Handler
                            _maxEventHandler(data.name, data.value);
                            break;
                    }
                    event.stopPropagation();
                },

                onkeyup: function(event, element, elementType) {
                    var data = $(event.target).data() || {};
                    switch (elementType) {
                        case RANGE_INPUT_MIN:
                            if (!_defaultKeyupHandler(event)) {
                                return;
                            }

                            if (maxValue && parseInt(data.value) > parseInt(maxValue)) {
                                return;
                            }

                            // unselect option
                            $(dropdownOptionsMin).removeClass(config.activeClass);
                            // event Handler
                            _minEventHandler(data.name, data.value);
                            break;
                        case RANGE_INPUT_MAX:
                            if (!_defaultKeyupHandler(event)) {
                                return;
                            }
                            if (minValue && parseInt(data.value) < parseInt(minValue)) {
                                return;
                            }
                            // unselect option
                            $(dropdownOptionsMax).removeClass(config.activeClass);
                            // event Handler
                            _maxEventHandler(data.name, data.value);
                            break;
                    }
                },

                onmouseleave: function() {
                    _checkToUpdateFilter();
                },

                /**
                 * Destroys the module and clears references
                 * @returns {void}
                 */
                destroy: function() {
                    dropdownWrapperMin = null;
                    dropdownWrapperMax = null;
                    dropdownOptionsMin = null;
                    dropdownOptionsMax = null;
                    inputMin = null;
                    inputMax = null;
                    minValue = null;
                    maxValue = null;
                }
            };
        });
    }
);
