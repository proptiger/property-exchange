"use strict";
define([
    "doT!modules/pushNotification/views/index",
    "services/utils",
    'services/apiService',
    "common/sharedConfig",
    "services/pushNotificationService",
    "services/trackingService",
    "common/trackingConfigService"
], function(subscriptionPanel, utils, ApiService, sharedConfig, pushNotificationService, trackingService, t) {
    Box.Application.addModule("pushNotification", function(context) {

        var element, $moduleEl;
        var messages = ['Lead_User_Created', 'interactiveEventTriggered'];
        var dbRegistrationPending = true;
        const staticConfig = {
            TRIGGER_TIME: 30000,
            INTERACTIVE_TRIGGER_TIME: 10000,
            VISIBILITY_PAGES: [
                "Home",
                "SERP City",
                "SERP Suburb",
                "SERP Project",
                "SERP Builder",
                "SERP Locality",
                "SERP City",
                "SERP Static",
                "SERP Landmark",
                "SERP Agent",
                "SERP General",
                "SERP MAP City",
                "SERP MAP Suburb",
                "SERP MAP Project",
                "SERP MAP Builder",
                "SERP MAP Locality",
                "SERP MAP Static",
                "SERP MAP Landmark",
                "SERP MAP Agent",
                "SERP MAP General"
            ],
            AUTO_REGISTER: false,
            NOTIFY_BUTTON_VISIBLE: false,
            DISCARD_DAYS: 7,
            ONE_SIGNAL_DISCARD_COOKIE: "ONE_SIGNAL_DISCARD"
        };
          const  behaviors = [],
            setIntervalTime = 3000;

        var appId = window.ONE_SIGNAL_APP_ID;
        var OneSignal = window.OneSignal || [];
        var timer;

        if (appId) {
            OneSignal.push(["init", {
                appId: appId,
                autoRegister: staticConfig.AUTO_REGISTER,
                notifyButton: {
                    enable: staticConfig.NOTIFY_BUTTON_VISIBLE /* Set to false to hide */
                }
            }]);
        }

        function init() {
            element = context.getElement();
            $moduleEl = $(element);

            OneSignal.push(function() {
                // If we're on an unsupported browser, do nothing
                if (_checkFunction(OneSignal.isPushNotificationsSupported) && !OneSignal.isPushNotificationsSupported()) {
                    return;
                }
                _checkFunction(OneSignal.getNotificationPermission) && OneSignal.getNotificationPermission().then(function(permission) {
                    if(permission === "denied"){
                        return;
                    }
                    _showSubscriptionPanel(staticConfig.TRIGGER_TIME);
                     //jshint ignore:line
                    _activateEventTagging();
                }); //jshint ignore:line
            });
        }

        function checkDiscardDays(){
            let lastShownTime = utils.getCookie(staticConfig.ONE_SIGNAL_DISCARD_COOKIE);
            let difference = staticConfig.DISCARD_DAYS * 24 * 60 * 60 * 1000;
            if(lastShownTime && ((new Date.getTime()) - parseInt(lastShownTime) < difference)){
                return false;
            }
            return true;
        }

        function _checkFunction(variable) {
            return (typeof variable === "function");
        }

        function _activateEventTagging() {
            _checkFunction(OneSignal.isPushNotificationsEnabled) && OneSignal.isPushNotificationsEnabled(function(isEnabled) {  
                if(isEnabled){
                    pushNotificationService.activateEventTagging();
                    clearInterval(timer);
                }
            }); //jshint ignore:line
        }

        function getPageType(){
            let pageType = utils.getPageData('trackPageType');
            return pageType;
        }

        function _checkSupportedPage() {
            let pageType = getPageType();
            return pageType && (staticConfig.VISIBILITY_PAGES.indexOf(pageType) > -1);
        }

        let isAlreadyShown = false;
        function _showSubscriptionPanel(triggerTimeout) {
            _checkFunction(OneSignal.isPushNotificationsEnabled) && OneSignal.isPushNotificationsEnabled(function(isEnabled) {
                if (!isAlreadyShown && !isEnabled && checkDiscardDays() && _checkSupportedPage()) {
                    setTimeout(function(){
                        if(!isAlreadyShown) {
                            isAlreadyShown = true;
                            let checkHomePage = getPageType() === "Home" ? true : false;
                            $moduleEl.html(subscriptionPanel({
                                isHomePage: checkHomePage
                            }));
                            setTimeout(function(){
                                $moduleEl.find('.push-notification').removeClass('out-of-view');
                                _track(t["VISIBLE_EVENT"]);
                            },500);
                        }
                    }, triggerTimeout); 
                }
            });//jshint ignore:line
        }

        function _hideSubscriptionPanel() {
            //$moduleEl.html("");
            setTimeout(function(){$moduleEl.find('.push-notification').addClass('out-of-view');},500);
        }

        function _track(event, label){
            let category = t["PUSH_NOTIFICATION_CATEGORY"];
            let properties = {
                [t.LABEL_KEY]: label,
                [t.CATEGORY_KEY]: category,
                [t.NON_INTERACTION_KEY]: 1
            };
            trackingService.trackEvent(event, properties);
        }

        function subscribe() {
            OneSignal.push(["registerForPushNotifications"]);
            timer = setInterval(_activateEventTagging, setIntervalTime);
        }

        function onclick(event, clickElement, elementType) {
            switch (elementType) {
                case 'subscribe':
                    subscribe();
                    _hideSubscriptionPanel();
                    _track(t["CLICK_EVENT"], t["YES_LABEL"]);
                    break;

                case 'discard':
                    utils.setcookie(staticConfig.ONE_SIGNAL_DISCARD_COOKIE, (new Date()).getTime());
                    _hideSubscriptionPanel();
                    _track(t["CLICK_EVENT"], t["NO_LABEL"]);
                    break;
            }
        }
        function onmessage(name,data){
            switch(name){
                case 'Lead_User_Created':
                    if(data && data.userId && dbRegistrationPending){
                        pushNotificationService.getUserId().then((notificationUserId)=>{
                            if(!notificationUserId){
                                return;
                            }
                            ApiService.postJSON(sharedConfig.clientApis.pushNotificationUserUpdate().url,{
                                "oneSignalRegId" : notificationUserId,
                                "userId" : data.userId,
                                "platform" : utils.isMobileRequest()?'Mobile':'Desktop',
                                "browser" : utils.getBrowserName()
                            }).then(()=>{
                                dbRegistrationPending = false;
                            })
                            ;
                        }).catch(()=>{
                            //do nothing as user is not registered with push notification
                        });
                    }
                break;
                case 'interactiveEventTriggered': 
                    _showSubscriptionPanel(staticConfig.INTERACTIVE_TRIGGER_TIME);
                break;
            }
        }
        function destroy() {

        }

        return {
            init,
            onclick,
            behaviors,
            destroy,
            messages,
            onmessage
        };
    });
});
