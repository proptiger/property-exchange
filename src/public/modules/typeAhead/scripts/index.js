"use strict";
define([
    "doT!modules/typeAhead/views/index",
    "doT!modules/typeAhead/views/tags",
    'services/loggerService',
    'services/utils',
    "services/commonService",
    'services/trackingService',
    "doT!modules/typeAhead/views/project",
    "doT!modules/typeAhead/views/search",
    "doT!modules/pyr/views/localityTypeAhead",
], function (layout, tagTemplate, logger, utils, commonService) {
    Box.Application.addModule("typeAhead", function(context) {
        const queryData = Box.DOM.queryData,
            INPUT_DATA_TYPE = 'query-text',
            RESULT_DATA_TYPE = 'search-element',
            TAG_DATA_TYPE = 'tags',
            TAG_CANCEL_TYPE = 'tab-cancel',
            TAG_MIN_TYPE = 'tag-minicount',
            OVERLAYSEC_DATA_TYPE = 'overlay-sec',
            HIDDEN = 'hidden',
            INPUT_DISABLE = 'disable',
            messages = ['typeAheadModuleInitiated', 'typeAheadCityChanged', 'typeaheadCategoryChange', 'pageDataListingTypeChange', 'typeaheadSetFocus', 'typeAheadSetSearchText', 'createTag', 'getRecentlySelectedLocation', 'similarLocationClicked', 'disableTypeaheadSearch', 'removeTag', 'recentlyAddedLocation', 'clearTagsArray', 'filterLocationUpdated', 'locationTagsChanged', 'pageLoaded'],
            UP_KEY=38,
            DOWN_KEY=40,
            ENTER_KEY = 13,
            ESC_KEY = 27,
            BACK_KEY = 8,
            defaultPlaceHolder = "pick location, builder or project";

        var pageData = utils.getPageData(), //contains page Data when the module got init(in case of header typeahead it will contain home page data bcz it got init at home page only)
            searchParams = {
                query: undefined, //mandatory field
                typeAheadType: undefined,
                city: undefined,
                locality: undefined,
                rows: 5,
                enhance: 'gp',
                category: utils.getPageData('listingType') || 'buy', //mandatory field
                view: 'buyer' //mandatory field
            },
            fetchTypeaheadSearchResults = 'fetchParsedSearchResults',
            typeAheadServiceName = 'TypeAheadService',
            typeaheadFilePath = 'services/typeAheadService',
            minimumLength = 3,
            tagsArray = [],
            loaderClass = 'typeahead-loader',
            moduleEl,
            tagEl,
            listContainer,
            config,
            typeAheadService,
            fetchSearchResults,
            maxTags = 4,
            multiselect = false,
            isTags = false,
            propogateTags = false,
            firstSelect = false, //flag to check double back to remove tags
            placeholder = "search query",
            inputName = '',
            template = "doT!modules/typeAhead/views/default",
            isExpanded = false,
            resultCount = 0,
            inputElementValue = "";
            

        function _clearResult() {
            listContainer = queryData(moduleEl, 'type', 'search-list');
            while (listContainer.hasChildNodes()) {
                listContainer.removeChild(listContainer.lastChild);
            }
        }

        function _clearInput() {
            let el = queryData(moduleEl, 'type', INPUT_DATA_TYPE);
            if(searchParams){
                searchParams.query = '';
            }
            if(el){
                el.value = '';
            }
        }
        function _fillInput(val) {
            let el = queryData(moduleEl, 'type', INPUT_DATA_TYPE);
            if(el){
                el.value =val;
            }
        }

        function _toggleInput(disable) {
            let el = queryData(moduleEl, 'type', INPUT_DATA_TYPE);
            if (disable) {
                $(el).addClass(INPUT_DISABLE);
            } else {
                $(el).removeClass(INPUT_DISABLE);
            }
        }

        function _clearPreviousResults() {
            let resultList = queryData(moduleEl, 'type', 'search-list');
            if (resultList) {
                resultList.innerHTML = '';
            }
        }

        function _createDefaultParams(defaultParam, config) {
            let searchParam = {},
                keys = Object.keys(defaultParam);
            for (let i = 0, length = keys.length; i < length; i++) {
                let key = keys[i];
                searchParam[key] = config[key] || defaultParam[key];
            }
            return searchParam;
        }

        function _handleLoader(show){
            let loaderElement = moduleEl.querySelector(`.${loaderClass}`);
            if(!loaderElement){
                return;
            }
            if(show){
                loaderElement.classList.remove('playopacity');
            } else{
                loaderElement.classList.add('playopacity');
            }
        }

        function showAllTags(){
            $(moduleEl).find(".tag-wrap .js-suggestion-tag-dummy").addClass('hide');
            $(moduleEl).find(".tag-wrap .js-srched-tag").removeClass('hide');
        }

        function hideTags(tagsArray){
            $(moduleEl).find('.tag-wrap .js-default-suggestion .js-tag').html(tagsArray[0].val);
            $(moduleEl).find(".tag-wrap .js-capsuleCounter").html(`+${tagsArray.length - 1} more`);
            $(moduleEl).find(".tag-wrap .js-suggestion-tag-dummy").removeClass('hide');
            $(moduleEl).find(".tag-wrap .js-srched-tag").addClass('hide');
        }

        function controlTagCounter(tagsArray, expandTags = false){
            if (tagsArray.length > 1 && moduleEl.id != 'filter-typeahead'){
                hideTags(tagsArray);
            } else {
                showAllTags();
            }
        }

        function _createTag(tagsArray, latest = false ,isExpanded = false) {
            let tagList;
            if(latest){
                tagList = tagsArray[tagsArray.length-1];
            } else {
                $(moduleEl).find(".tag-wrap .js-srched-tag").remove();
                tagList = tagsArray;
            }
            let tagHtml = tagTemplate({
                tags: tagList,
                datatype: TAG_CANCEL_TYPE,
                latest
            });
            $(moduleEl).find(".tag-wrap .js-input").before(tagHtml);
            controlTagCounter(tagsArray);
        }

        function _searchResultPopulation() {

            let el = queryData(moduleEl, 'type', INPUT_DATA_TYPE),
            inputValue = el.value;

            _handleLoader(true);
            typeAheadService[fetchSearchResults]({
                searchParams, config, tagsArray: $.extend(true,[],tagsArray)
            }).then((response) => {
                if(typeof response.then === "function") {
                    return;
                }
                if(response && ((response.match && response.match.length === 0) || response.length === 0)){
                    context.broadcast('trackNoMatchingResult', {
                        moduleEl, config, query: searchParams.query
                    });
                } else {
                    resultCount = 0;
                    let keys = Object.keys(response),
                        length = keys.length;
                    for(let i = 0; i < length; i++) {
                        resultCount += response[keys[i]].length;
                    }
                    context.broadcast('searchResultPopulated', {
                        moduleEl, config, searchParams, response, resultCount, tagsArray: $.extend(true,[],tagsArray)
                    });
                    logger.info(response);
                }
                listContainer.innerHTML = template({
                    response: response,
                    inputValue
                });
                _toggleOverlay(true);
                _handleLoader(false);
            }, (error) => {
                logger.info(error);
                _handleLoader(false);
            });
        }

        function _toggleResultDropdown(show) {
            if (show) {
                $(queryData(moduleEl, 'type', 'search-list')).removeClass(HIDDEN);
            } else {
                $(queryData(moduleEl, 'type', 'search-list')).addClass(HIDDEN);
            }
        }

        function _toggleOverlay(removeClass) {
            let overlayDiv = $(queryData(moduleEl, 'type', OVERLAYSEC_DATA_TYPE));
            if (removeClass) {
                overlayDiv.removeClass(HIDDEN);
            } else {
                overlayDiv.addClass(HIDDEN);
            }
        }

        function _searchResultSelection(element, { isClick } = {}, elementData) {

            if(tagsArray.length >= maxTags){
                return ;
            }
            let dataset = $(element).data() || {},
                triggerSearch = false,
                el = queryData(moduleEl, 'type', INPUT_DATA_TYPE),
                inputValue = el.value,
                listingType = utils.getPageData('listingType'),
                tagObj = {
                    tagid: dataset.tagid,
                    val: dataset.val,
                    dataset: dataset
                };
                el.blur();
                if (!multiselect || multiselect.indexOf(dataset.tagtype) == -1) {
                    tagsArray = [];
                    tagsArray.push(tagObj);
                    _createTag(tagsArray, true);
                    _toggleInput(true); //stops user for selecting further if the current selection is not a locality/suburb.
                    if(config.keepSelection){
                        _toggleInput(false);
                    }
                } else {
                    tagsArray.push(tagObj);
                    _createTag(tagsArray, true);
                }

                if (['headerSearchBox'].indexOf(moduleEl.id) > -1) {
                    triggerSearch = true;
                }

                if (tagsArray.length == maxTags) {
                    _toggleInput(true); //stops user for selecting further if reched the highest number
                }

            var resultSelectionTimer = setTimeout(function () {
                _clearPreviousResults();
                 _fillInput(dataset.val);
                if(!config.keepSelection){
                    _clearInput();
                }
                _toggleOverlay(false);
            }, 500);
            context.broadcast('searchResultClicked', {
                dataset, tagsArray: $.extend(true,[],tagsArray), moduleEl, config, element, triggerSearch, isClick, searchParams, resultCount
            });
        }

        function onfocusout(event, element, elementType) {
            if (elementType === INPUT_DATA_TYPE) {
                let query = element.value;
            }
        }

        
        function onkeydown(event, element) {
            inputElementValue = element.value;
        }

        let timer = 0;
        function onkeyup(event, element, elementType) {

            if (elementType === INPUT_DATA_TYPE) {
                // to stop api hit if user is typing continuosly
                clearTimeout (timer);
                timer = setTimeout(_handleKeyup, 200);
            }

            function _handleKeyup(){
                let query = element.value,
                    length = query.length,
                    keycode = event.keyCode;
                searchParams.query = query;
                if (keycode == UP_KEY) {
                    _navigateResults(-1);
                } else if (keycode == DOWN_KEY) {
                    _navigateResults(1);
                } else if (keycode == ENTER_KEY) {
                    _triggerSelection();
                } else if(keycode==ESC_KEY){
                    context.broadcast('typeAheadOverlayClicked', {
                        moduleEl, config, searchParams, tagsArray: $.extend(true,[],tagsArray)
                    });
                    _clearResult();
                    _toggleResultDropdown(false);
                    _toggleOverlay(false);
                }else {
                    if (!inputElementValue.length && keycode == BACK_KEY) {
                        context.broadcast('searchCleared',{
                            moduleEl,config
                        });
                        _clearResult();
                        if(tagsArray.length){
                            let popElement = tagsArray.pop(),
                                $elementTobeDeleted = $(queryData(moduleEl, 'tagid', popElement.tagid)),
                                triggerSearch = false;
                            $elementTobeDeleted.closest('.js-srched-tag').remove();
                            if (['headerSearchBox'].indexOf(moduleEl.id) > -1) {
                                triggerSearch = true;
                            }
                            context.broadcast('searchResultCanceled', {
                                dataset: popElement.dataset || {}, tagsArray: $.extend(true, [], tagsArray), moduleEl, config, element, searchParams, triggerSearch, resultCount
                            })
                        }
                    } else if (inputElementValue.length && keycode == BACK_KEY) {
                        context.broadcast('searchKeyBackspaced', {
                            inputElementValue, moduleEl, config, element, searchParams, resultCount, tagsArray: $.extend(true,[],tagsArray)
                        });
                    }

                    if ((length >= minimumLength) || (!length)) {
                        context.broadcast('searchKeyTyped', {
                            moduleEl, config, searchParams
                        });
                        _searchResultPopulation();
                    }
                }

            }
        }

        function _triggerSelection(data = {}) {
            let currElement = $(queryData(moduleEl, 'type', "search-list")).find("li.hover");
            if (currElement.length > 0 ) {
                _searchResultSelection(currElement[0],{
                    isSearchButton: data.isSearchButton
                });
            }
        }

        function _navigateResults(direction) {
            let resultArray = $(queryData(moduleEl, 'type', "search-list")).find("li"),
                currIndex = resultArray.index($("li.hover")), //-1
                nextIndex;
            if (currIndex == -1) {
                if (direction > 0) {
                    nextIndex = 0;
                } else {
                    nextIndex = direction;
                }
            } else {
                nextIndex = currIndex + direction < resultArray.length ? currIndex + direction : 0;
            }

            if (currIndex > -1) {
                resultArray.eq(currIndex).removeClass('hover');
            }
            resultArray.eq(nextIndex).addClass('hover');
        }

        function updatePlaceHolder(){
            var inputControl = $(moduleEl).find(".js-input[data-type='query-text']");
            inputControl.prop("placeholder",defaultPlaceHolder);
        }

        function updateSelectedLocation(data){
            tagsArray = data.tagsArray;
            _createTag(tagsArray);
            if(tagsArray.length == 0 && moduleEl.id == 'headerSearchBox' && utils.isMobileRequest()){
                _toggleInput(false);
            }
            else if((tagsArray.length == 1 && config.multiselect.indexOf(tagsArray[0].dataset.tagtype) == -1) || tagsArray.length == maxTags || (moduleEl.id == 'headerSearchBox' && utils.isMobileRequest())){
                _toggleInput(true);
            }else {
                _toggleInput(false);
            }
            context.broadcast('resetSelectedList', {
                tagsArray: $.extend(true,[],tagsArray), moduleEl, config
            })
        }

        function onmessage(name, data) {
            let dataId = data && data.moduleEl ? data.moduleEl.id : data && data.id;

            if ((name === 'typeAheadCityChanged') && data) {
                let city = data.city;
                searchParams.city = (city !== 'all') ? city : undefined;
            } else if (name == 'pageDataListingTypeChange' || (name === 'typeAheadModuleInitiated' && dataId === 'homePageSearchBox') || (name == 'clearTagsArray' && dataId == 'mobileFilter')){
                _clearPreviousResults();
                _clearInput();
                _toggleResultDropdown(false);
                _toggleOverlay(false);
                _toggleInput(false);
                tagsArray = []; //reset tags array for new listing type
                _createTag(tagsArray);
            } else if (name == 'createTag' && dataId != moduleEl.id && (moduleEl.id === 'headerSearchBox' || moduleEl.id === "filter-typeahead")){
                tagsArray = data.tagsArray;
                _createTag(tagsArray);
                if(tagsArray.length){
                    let dataset = tagsArray[tagsArray.length - 1].dataset;
                    context.broadcast('addSelectedItemToList', {
                        dataset, tagsArray, moduleEl, config
                    });
                    if ((tagsArray.length == 1 && config.multiselect.indexOf(dataset.tagtype) == -1) || tagsArray.length == maxTags || (moduleEl.id === 'headerSearchBox' && utils.isMobileRequest())){
                        _toggleInput(true);
                    } else {
                        _toggleInput(false);
                    }
                }
            }else if(name === 'typeaheadSetFocus'){
                if(data.id == moduleEl.id){
                    $(moduleEl).find(".js-input[data-type='query-text']").focus();
                }
            }else if(name === 'typeAheadSetSearchText'){
                if(data.id == moduleEl.id){
                    _clearPreviousResults();
                    _fillInput(data.val);
                    _clearInput();
                }
            } else if(name === "getRecentlySelectedLocation"){
                if(tagsArray && tagsArray.length >= 1 && config.multiselect &&  ["headerSearchBox"].indexOf(moduleEl.id) > -1 && utils.getPageData('moduleName') != "home"){
                    if(dataId == 'filter-typeahead' || config.multiselect.indexOf(tagsArray[tagsArray.length - 1].dataset.tagtype) > -1){
                        context.broadcast("recentlyAddedLocation", {
                            tagsArray: $.extend(true,[],tagsArray),
                            locationType: "LOCALITY",
                            moduleId: dataId,
                            typeAheadId: moduleEl.id
                        });
                    }
                }
            } else if(name == 'similarLocationClicked'){
                if(utils.getPageData('moduleName') == "home" && moduleEl.id == "homePageSearchBox"){
                    _searchResultSelection(data.element, {isClick: true}); 
                } else if(utils.getPageData('moduleName') != "home" && moduleEl.id == "headerSearchBox"){
                    _searchResultSelection(data.element, {isClick: true});
                }
            } else if(name == 'disableTypeaheadSearch'){
                if(dataId == moduleEl.id){
                    _toggleInput(true);
                }
            } else if (name == 'removeTag' && dataId != moduleEl.id && moduleEl.id === 'headerSearchBox'){
                tagsArray = data.tagsArray;
                _createTag(tagsArray);
                if(data.dataset){
                    context.broadcast("searchResultCanceled", {
                        dataset: data.dataset, tagsArray:$.extend(true,[],tagsArray), moduleEl, config, searchParams, triggerSearch: false
                    });
                } else {
                    context.broadcast('resetSelectedList', {
                        tagsArray: $.extend(true,[],tagsArray), moduleEl, config
                    })
                }
            }
            else if (name == "filterLocationUpdated" && data.typeaheadModuleId == moduleEl.id) {
                context.broadcast("locationTagsChanged", {
                    moduleEl,
                    tagsArray:$.extend(true,[],tagsArray)
                });
            } else if((name == "locationTagsChanged" && moduleEl.id == "headerSearchBox") || (name === "recentlyAddedLocation" && data.moduleId == moduleEl.id)){
                updateSelectedLocation(data);
            } else if (name == "pageLoaded" && moduleEl.id === 'headerSearchBox' && !utils.getPageData('isSerp') && utils.isMobileRequest()){
                _clearPreviousResults();
                _clearInput();
                _toggleResultDropdown(false);
                _toggleOverlay(false);
                _toggleInput(false);
                tagsArray = []; //reset tags array for new listing type
                _createTag(tagsArray);
                context.broadcast("resetSelectedList", {
                    moduleEl,
                    config
                });
                updatePlaceHolder();
            }
        }
        function initiateSearch(){
            _toggleOverlay(true);
            expandTags();
            _toggleResultDropdown(true);
            let el = queryData(moduleEl, 'type', INPUT_DATA_TYPE),
                query = el.value,
                length = query.length;
            context.broadcast('typeaheadInputFocusin', {
                query , moduleEl, config, tagsArray: $.extend(true, [], tagsArray)
            });
            if (!length) {
                _clearResult();
                _searchResultPopulation();
            }
        }
        function onfocusin(event, element, elementType) {
            if (elementType === INPUT_DATA_TYPE) {
                if (moduleEl.id === 'headerSearchBox' && utils.isMobileRequest()) {
                    context.broadcast('open-filters');
                } else {
                    initiateSearch();
                }
            }
        }

        function expandTags(){
            if(tagsArray.length > 1){
                _toggleOverlay(true);
                context.broadcast("tagExpandBtnClicked", {
                    moduleEl, config
                });
                showAllTags();
            }
        }

        function removeSelectedTag(element, dataset){
            $(element).closest('.js-srched-tag').remove();
            let tagid = dataset.tagid, triggerSearch = false;
            for (let tags = 0; tags < tagsArray.length; tags++) {
                if (tagsArray[tags].tagid == tagid) {
                    tagsArray.splice(tags, 1);
                    break;
                }
            }
            _toggleInput(false);
            if (['headerSearchBox'].indexOf(moduleEl.id) > -1) {
                triggerSearch = true;
            }
            context.broadcast("searchResultCanceled", {
                dataset, tagsArray: $.extend(true, [], tagsArray), moduleEl, config, searchParams, triggerSearch
            });
        }
        
        function onmouseover(event, element, elementType){
            if(elementType==RESULT_DATA_TYPE){
                let currElement =  $(queryData(moduleEl, 'type', "search-list")).find("li.hover");
                currElement.removeClass('hover');
                $(element).addClass('hover');
            }
        }

        function init() {
            logger.info('initialization typeAhead module');
            moduleEl = context.getElement();
            var inputControl = $(moduleEl).find(".js-input[data-type='query-text']");
            inputControl.removeAttr('disabled');
            config = context.getConfig() || {};
            if(config.emptyPlaceHolder){
                inputControl.prop("placeholder","");
            } else {
                inputControl.prop("placeholder", config.placeholder ? config.placeholder : "pick location, builder or project");
            }
            searchParams = _createDefaultParams(searchParams, config);
            minimumLength = config.minLength || minimumLength;
            maxTags = config.maxTags || maxTags;
            isTags = config.isTags || isTags;
            isTags = true;
            tagsArray = [];
            propogateTags = config.propogateTags ? true : false;
            loaderClass = config.loaderElementClass || loaderClass;
            placeholder = config.placeholder || placeholder;
            inputName = config.inputName || '';
            template = config.templatePath || template;
            pageData = utils.getPageData();
            
            /*If some service has to be used as datasource insted of columbus, overrifing default config*/
            if (config.typeaheadResultsDataSource) {
                let typeaheadResultsDataSource = config.typeaheadResultsDataSource;
                typeaheadFilePath = typeaheadResultsDataSource.filePath;
                typeAheadServiceName = typeaheadResultsDataSource.serviceName;
                fetchTypeaheadSearchResults = typeaheadResultsDataSource.functionName;
            }

            require([typeaheadFilePath], function() {
                typeAheadService = context.getService(typeAheadServiceName);
                fetchSearchResults = fetchTypeaheadSearchResults;
            });
            if(config.multiselect){
                multiselect = config.multiselect;
            }

            if (!config.layoutPreloaded) {
                let html = layout({
                    placeholder,
                    inputName,
                    isTags,
                    multiselect: config.multiselect,
                    hideTags: config.hideTypeAheadTags
                });
                let typeAheadPlaceholder = $(moduleEl).find("[data-placeholder]");
                if(typeAheadPlaceholder.length){
                    typeAheadPlaceholder.html(html);
                } else {
                    moduleEl.innerHTML += html;
                }
            }

            require([template], function(data) {
                template = data;
                listContainer.innerHTML = template({initial:true});
            });

            listContainer = queryData(moduleEl, 'type', 'search-list');
            tagEl = queryData(moduleEl, 'type', TAG_DATA_TYPE);

            context.broadcast('typeAheadModuleInitiated', {
                config, moduleEl
            });

            context.broadcast('moduleLoaded', {
                name: "typeAhead",
                id: moduleEl.id
            })
            if (config.preFilledData){
                updateSelectedLocation(config.preFilledData);
            }
        }

        function onclick(event, element, elementType) {
            if (elementType == RESULT_DATA_TYPE) {
                _searchResultSelection(element, {isClick: true});
            } else if (elementType == 'overlay-sec') {
                isExpanded = false;
                context.broadcast('typeAheadOverlayClicked', {
                    moduleEl, config, searchParams, tagsArray:$.extend(true,[],tagsArray)
                });
                _clearPreviousResults();
                _clearInput();
                _toggleResultDropdown(false);
                _toggleOverlay(false);
                controlTagCounter(tagsArray); //update tags view based on tag count
                event.stopPropagation();
            } else if (elementType == TAG_CANCEL_TYPE){
                let dataset = $(element).data() || {};
                removeSelectedTag(element, dataset);
            } else if (elementType == TAG_MIN_TYPE || elementType == TAG_DATA_TYPE){
                if (moduleEl.id === 'headerSearchBox' && utils.getPageData('isSerp') && utils.isMobileRequest()) {
                    context.broadcast('open-filters');
                } else {
                    let el = queryData(moduleEl, 'type', INPUT_DATA_TYPE);
                    if ($(el).hasClass(INPUT_DISABLE)) {
                        if (tagsArray.length == 1 && config.multiselect.indexOf(tagsArray[0].dataset.tagtype) == -1) {
                            let ele = queryData(moduleEl, 'tagid', tagsArray[0].tagid);
                            removeSelectedTag(ele, tagsArray[0]);
                        } else {
                            expandTags();
                        }
                    } else {
                        el.focus();
                    }
                }
            }
        }
        
        function destroy() {
            timer && clearTimeout(timer);  //jshint ignore:line
            typeof resultSelectionTimer !== 'undefined' && clearTimeout(resultSelectionTimer);//jshint ignore:line
            searchParams = fetchTypeaheadSearchResults = typeAheadServiceName = typeaheadFilePath = minimumLength = tagsArray = moduleEl = tagEl = config = typeAheadService = fetchSearchResults = multiselect = isTags = placeholder = template = null;
        }

        return {
            init,
            destroy,
            onclick,
            onkeyup,
            onkeydown,
            messages,
            onmessage,
            onfocusin,
            onfocusout,
            onmouseover
        };
    });
});
