"use strict";
define([
        "doT!modules/buyerDashboard/views/serviceCard",
        "doT!modules/buyerDashboard/views/500",
        "doT!modules/buyerDashboard/views/desktopLayout",
        "doT!modules/buyerDashboard/views/mobileLayout",
        "doT!modules/buyerDashboard/views/savedSearches",
        "doT!modules/buyerDashboard/views/shortlists",
        "doT!modules/buyerDashboard/views/siteVisits",
        "doT!modules/buyerDashboard/views/articles",
        "doT!modules/buyerDashboard/views/desktopMenu",
        "doT!modules/buyerDashboard/views/desktopShortlist",
        "doT!modules/buyerDashboard/views/dektopCallbackCard",
        "doT!modules/buyerDashboard/views/desktopSiteVisit",
        "doT!modules/buyerDashboard/views/desktopArticles",
        "doT!modules/buyerDashboard/views/callBackCards",
        "common/sharedConfig",
        "services/localStorageService",
        "services/urlService",
        "services/apiService",
        "services/shortlist",
        "services/filterConfigService",
        "services/loggerService",
        "services/loginService",
        "services/utils",
        "services/viewStatus",
        "services/commonService",
        "services/allianceIconService",
        "common/trackingConfigService"
    ],

    function(serviceCard, errorPage, desktopLayout, mobileLayout, savedSearches, shortlists, siteVisits, articles, desktopMenu, desktopShortlist, dektopCallbackCard, desktopSiteVisit, desktopArticles, callBackCards, sharedConfig, localStorageService, urlService, apiService, shortlistService, filterConfigService, logger, loginService, utils, viewStatus, commonService, allianceIconService) {

        Box.Application.addModule("buyerDashboard", function(context) {

            const PAGE_CATEGORY = "Buyer_DASHBOARD",
                  MAX_SERVICE_CARDS = 3;

            var element,
                urlParam,
                scope;

            const messages = ['dateTimePicker:change', 'onLoggedIn', 'loggedOut', 'jarvisToBuyerJourney', 'delete-search', 'delete-favourite', 'favourites-synced'],
                mobileQueryTemplateMap = {
                    "dashboard": mobileLayout,
                    "savedsearches": articles,
                    "favourites": callBackCards,
                    "enquired": shortlists,
                    "recentlyviewed": callBackCards,
                    "sitevisits": siteVisits,
                    "homeloan": articles,
                    "unitbook": articles,
                    "possession": articles,
                    "registration": articles
                },
                desktopQueryTemplateMap = {
                    "dashboard": desktopArticles,
                    "savedsearches": desktopArticles,
                    "favourites": dektopCallbackCard,
                    "enquired": desktopShortlist,
                    "recentlyviewed": dektopCallbackCard,
                    "sitevisits": desktopSiteVisit,
                    "homeloan": desktopArticles,
                    "unitbook": desktopArticles,
                    "possession": desktopArticles,
                    "registration": desktopArticles
                };

            var showErrorPage = function() {
                    element.html(errorPage());
                },

                trackDashboardPageView = function(type) {
                    let pageType = type || scope.currentAction;
                    if (pageType === 'Error') {
                        pageType = 'Others';
                    }
                    utils.triggerPageInteractive();
                    if (document.readyState.toLowerCase() === 'complete') {
                        commonService.addImageObserver(context.getElement());
                    }
                },

                sendJarvisEvent = function() {
                    const jarvisKey = "buyerJarvisTime",
                        dayGap = 4,
                        dayMS = 86400000;
                    let jarvisTime = localStorageService.getItem(jarvisKey),
                        currentTime = Date.now(),
                        jarvisStorageData = localStorageService.getItem('jStorage'),
                        deliveryId = jarvisStorageData ? jarvisStorageData.session_id : "",
                        payload = {},
                        event;

                    if (!jarvisTime) {
                        localStorageService.setItem(jarvisKey, currentTime);
                    } 
                    else if (currentTime - jarvisTime > dayGap * dayMS) {
                        localStorageService.setItem(jarvisKey, currentTime);
                        event = scope.completedStage.jarvisEvent;
                        if (event && deliveryId) {
                            setTimeout(() => {
                                payload = {
                                    event_name: event,
                                    delivery_id: deliveryId
                                };
                                window.MpAnalytics.track(payload);
                            }, 5000);
                        }
                    }
                },
                capitalizeFirstLetter = function(string) {
                    return string.charAt(0).toUpperCase() + string.slice(1);
                },
                scrollTop = function() {
                    $('body').scrollTop(0);
                    window.nanobar.go(100);
                    return;
                },
                getArticles = function(type) {
                    var query = {};
                    query['query'] = type;
                    query['articles'] = true;
                    query['alliances'] = scope.alliancesActive;
                    window.nanobar.go(50);
                    return apiService.get(sharedConfig.apiHandlers.buyerDashboard({
                        query
                    }).url); //.then(response=>{
                },
                mobileArticles = function(urlParam, reRenderArea) {
                    scope.currentAction = capitalizeFirstLetter(urlParam);
                    window.nanobar.go(70);
                    getArticles(urlParam).then(response => {
                        window.nanobar.go(90);
                        let serviceCardLength = response.serviceCards && response.serviceCards.length,
                            articlesLength = response.articles && response.articles.length;
                        serviceCardLength = Math.round(articlesLength * 2 / 3) < serviceCardLength ? Math.round(articlesLength * 2 / 3) : serviceCardLength;
                        reRenderArea.html(articles({
                            response: response,
                            serviceCardLength,
                            articlesLength,
                            getServiceIcon: allianceIconService.getServiceIcon,
                            serviceCard
                        }));
                        trackDashboardPageView();
                        scrollTop();
                    }, error => {
                        window.nanobar.go(100);
                        showErrorPage();
                        trackDashboardPageView('Error');
                        logger.error("error in articles api", error);
                    });
                },
                getHtml = function(template, data, partials) {
                    data.alliances = scope.alliancesActive;
                    let templateHtml = template(data);
                    return $(templateHtml).prepend(partials(data))[0];
                },

                showDesktopArticles = function(urlParam, reRenderArea) {
                    scope.currentAction = capitalizeFirstLetter(urlParam);
                    window.nanobar.go(70);
                    getArticles(urlParam).then(response => {
                        window.nanobar.go(90);
                        let serviceCardLength = response.serviceCards && response.serviceCards.length,
                            articlesLength = response.articles && response.articles.length;
                        serviceCardLength = Math.round(articlesLength * 2 / 3) < serviceCardLength ? Math.round(articlesLength * 2 / 3) : serviceCardLength;
                        reRenderArea.html(getHtml(desktopArticles, {
                            response: response,
                            query: urlParam,
                            currentStage: scope.completedStage.currentStage,
                            serviceCardLength,
                            articlesLength,
                            getServiceIcon: allianceIconService.getServiceIcon,
                            serviceCard
                        }, desktopMenu));
                        trackDashboardPageView();
                        scrollTop();
                    }, error => {
                        showErrorPage();
                        window.nanobar.go(100);
                        trackDashboardPageView('Error');
                        logger.error("error in articles api", error);
                    });
                },
                checkShortlistCount = function(response) {
                    return response.favourites || response.enquired || response.recent;
                },
                getFavouriteIds = function(type) {
                    return shortlistService.getAllShortlist(type).map(obj => {
                        return type === 'listing' ? {
                            id: obj.id,
                            time: obj.timestamp
                        } : {
                            id: obj.projectId,
                            time: obj.timestamp
                        };
                    });
                },

                getRecentIds = function(type) {
                    let recent = viewStatus.recentlyViewed({
                        "numberOfItems": 20
                    }).map(obj => {
                        if (obj.type) {
                            return {
                                id: obj.id,
                                type: obj.type,
                                time: obj.time
                            };
                        }
                    });
                    if (type) {
                        recent = recent.filter(result => {
                            return result.type === type;
                        });
                    }
                    return recent;
                },

                getFavouriteCount = function() {
                    return shortlistService.getAllShortlist('listing').length + shortlistService.getAllShortlist('project').length;
                },

                getRecentCount = function() {
                    return getRecentIds().length;
                },
                showFavourites = function(urlParam, reRenderArea) {
                    scope.currentAction = 'Shortlist_' + capitalizeFirstLetter(urlParam);
                    var query = {},
                        finalResponse = {},
                        renderObject = {},
                        recentCount = getRecentCount(),
                        favouriteCount = getFavouriteCount();
                    query['query'] = urlParam;
                    query['alliances'] = scope.alliancesActive;
                    finalResponse.recent = recentCount;
                    finalResponse.favourites = favouriteCount;

                    if (scope.isLoggedIn) {
                        query.leads = true;
                        if (urlParam === 'recentlyviewed') {
                            query.listings = JSON.stringify(getRecentIds('listing'));
                            query.projects = JSON.stringify(getRecentIds('project'));
                        }
                        if (urlParam === 'favourites') {
                            query.listings = JSON.stringify(getFavouriteIds('listing'));
                            query.projects = JSON.stringify(getFavouriteIds('project'));
                        }

                        window.nanobar.go(60);
                        apiService.get(sharedConfig.apiHandlers.buyerDashboard({
                            query
                        }).url).then(response => {
                            $.extend(response, finalResponse);
                            if (!checkShortlistCount(response)) {
                                scope.currentAction = 'Shortlist';
                                if (scope.isMobile) {
                                    mobileArticles(urlParam, reRenderArea);
                                } else {
                                    showDesktopArticles(urlParam, reRenderArea);
                                }
                                return;
                            }
                            window.nanobar.go(90);

                            let serviceCardLength = response.serviceCards && response.serviceCards.length,
                                propertyCardsLength = response.cards && response.cards.length;
                            serviceCardLength = propertyCardsLength < serviceCardLength ? propertyCardsLength : serviceCardLength;
                            serviceCardLength = serviceCardLength < MAX_SERVICE_CARDS ? serviceCardLength : MAX_SERVICE_CARDS;
                            renderObject = {
                                response: response,
                                loggedIn: true,
                                currentStage: scope.completedStage.currentStage,
                                query: urlParam,
                                serviceCard,
                                serviceCardLength,
                                getServiceIcon: allianceIconService.getServiceIcon
                            };
                            reRenderArea.html(scope.isMobile ? mobileQueryTemplateMap[urlParam](renderObject) : getHtml(desktopQueryTemplateMap[urlParam], renderObject, desktopMenu));
                            trackDashboardPageView();
                            scrollTop();
                            commonService.startAllModules(reRenderArea);
                        }, error => {
                            showErrorPage();
                            trackDashboardPageView('Error');
                            logger.error("error in shortlist api call in buyerDashboard while logged in", error);
                            window.nanobar.go(100);
                        });
                    } else {
                        if (!recentCount && !favouriteCount) {
                            scope.currentAction = 'Shortlist';
                            if (scope.isMobile) {
                                mobileArticles(urlParam, reRenderArea);
                            } else {
                                showDesktopArticles(urlParam, reRenderArea);
                            }
                            return;
                        } else {
                            if (urlParam === 'enquired') {
                                renderObject = {
                                    response: finalResponse,
                                    loggedIn: false,
                                    query: urlParam
                                };
                                reRenderArea.html(scope.isMobile ? mobileQueryTemplateMap[urlParam](renderObject) : getHtml(desktopQueryTemplateMap[urlParam], renderObject, desktopMenu));
                                trackDashboardPageView();
                                scrollTop();
                                return;
                            }
                            if (urlParam === 'recentlyviewed') {
                                if (!recentCount) {
                                    renderObject = {
                                        response: finalResponse,
                                        loggedIn: false,
                                        query: urlParam
                                    };
                                    reRenderArea.html(scope.isMobile ? mobileQueryTemplateMap[urlParam](renderObject) : getHtml(desktopQueryTemplateMap[urlParam], renderObject, desktopMenu));
                                    trackDashboardPageView();
                                    scrollTop();
                                    return;
                                }
                                query.listings = JSON.stringify(getRecentIds('listing'));
                                query.projects = JSON.stringify(getRecentIds('project'));
                            }
                            if (urlParam === 'favourites') {
                                if (!favouriteCount) {
                                    renderObject = {
                                        response: finalResponse,
                                        loggedIn: false,
                                        query: urlParam
                                    };
                                    reRenderArea.html(scope.isMobile ? mobileQueryTemplateMap[urlParam](renderObject) : getHtml(desktopQueryTemplateMap[urlParam], renderObject, desktopMenu));
                                    trackDashboardPageView();
                                    scrollTop();
                                    return;
                                }
                                query.listings = JSON.stringify(getFavouriteIds('listing'));
                                query.projects = JSON.stringify(getFavouriteIds('project'));
                            }

                            window.nanobar.go(60);
                            apiService.get(sharedConfig.apiHandlers.buyerDashboard({
                                query
                            }).url).then(response => {
                                window.nanobar.go(90);
                                $.extend(response, finalResponse);

                                let serviceCardLength = response.serviceCards && response.serviceCards.length,
                                    propertyCardsLength = response.cards && response.cards.length;
                                serviceCardLength = propertyCardsLength < serviceCardLength ? propertyCardsLength : serviceCardLength;
                                serviceCardLength = serviceCardLength < MAX_SERVICE_CARDS ? serviceCardLength : MAX_SERVICE_CARDS;

                                renderObject = {
                                    response: response,
                                    loggedIn: false,
                                    query: urlParam,
                                    serviceCard,
                                    getServiceIcon: allianceIconService.getServiceIcon,
                                    serviceCardLength
                                };
                                reRenderArea.html(scope.isMobile ? mobileQueryTemplateMap[urlParam](renderObject) : getHtml(desktopQueryTemplateMap[urlParam], renderObject, desktopMenu));
                                trackDashboardPageView();
                                scrollTop();
                            }, error => {
                                window.nanobar.go(100);
                                showErrorPage();
                                trackDashboardPageView('Error');
                                logger.error("error in shortlist api of buyer dashboard", error);
                            });
                        }
                    }

                },
                getSavedSearches = function() {
                    var query = {};
                    query['query'] = 'savedsearches';
                    query['alliances'] = scope.alliancesActive;
                    window.nanobar.go(70);
                    return apiService.get(sharedConfig.apiHandlers.buyerDashboard({
                        query
                    }).url);
                },
                getSiteVisits = function() {
                    var query = {};
                    query['query'] = 'sitevisits';
                    query['alliances'] = scope.alliancesActive;
                    window.nanobar.go(70);
                    return apiService.get(sharedConfig.apiHandlers.buyerDashboard({
                        query
                    }).url);
                },
                getUserInfo = function() {
                    return scope.userDetails;
                },

                setUserInfo = function(data) {
                    if (!data) {
                        scope.userDetails = undefined;
                        return;
                    }
                    var finalResponse = {};
                    finalResponse.firstName = data.firstName;
                    finalResponse.lastName = data.lastName;
                    finalResponse.profileImage = data.profileImageUrl;
                    finalResponse.contactNumber = data.contactNumber;
                    scope.userDetails = finalResponse;
                },
                
                mobileHandler = function(urlParam) {
                    var reRenderArea = element;
                    scope.currentAction = capitalizeFirstLetter(urlParam);
                    switch (urlParam) {

                        case 'unitbook':
                        case 'homeloan':
                        case 'possession':
                        case 'registration':
                            mobileArticles(urlParam, reRenderArea);
                            break;

                        case 'savedsearches':
                            if (scope.isLoggedIn) {
                                window.nanobar.go(70);
                                getSavedSearches().then(response => {
                                    window.nanobar.go(90);

                                    if (response.cards && !response.cards.length) {
                                        mobileArticles(urlParam, reRenderArea);
                                        return;
                                    }
                                    scope.currentAction = capitalizeFirstLetter(urlParam);
                                    for (let i = 0, len = response.cards.length, cards = response.cards; i < len; i++) {
                                        cards[i].url = filterConfigService.getUrlWithUpdatedParams({
                                            overrideParams: cards[i].urlObject,
                                            skipPageData: true
                                        });
                                    }
                                    let serviceCardLength = response.serviceCards && response.serviceCards.length,
                                        propertyCardsLength = response.cards && response.cards.length;
                                    serviceCardLength = propertyCardsLength < serviceCardLength ? propertyCardsLength : serviceCardLength;
                                    serviceCardLength = serviceCardLength < MAX_SERVICE_CARDS ? serviceCardLength : MAX_SERVICE_CARDS;
                                    reRenderArea.html(savedSearches({
                                        response: response.cards,
                                        localityMap: response.localityIds,
                                        loggedIn: scope.isLoggedIn,
                                        serviceCard,
                                        serviceCardLength,
                                        getServiceIcon: allianceIconService.getServiceIcon,
                                        serviceCardsData: response.serviceCards
                                    }));
                                    trackDashboardPageView();
                                    scrollTop();
                                    return;
                                }, error => {
                                    showErrorPage();
                                    window.nanobar.go(100);
                                    trackDashboardPageView('Error');
                                    logger.error("mobile api get savedsearches call error", error);
                                });
                            } else {
                                mobileArticles(urlParam, reRenderArea);
                            }
                            break;

                        case 'favourites':
                            showFavourites(urlParam, reRenderArea);
                            break;

                        case 'recentlyviewed':
                            showFavourites(urlParam, reRenderArea);
                            break;

                        case 'sitevisits':
                            if (scope.isLoggedIn) {
                                window.nanobar.go(70);
                                getSiteVisits().then(response => {
                                    window.nanobar.go(90);
                                    if (response.cards && !response.cards.length) {
                                        mobileArticles(urlParam, reRenderArea);
                                        return;
                                    }
                                    let serviceCardLength = response.serviceCards && response.serviceCards.length,
                                        propertyCardsLength = response.cards && response.cards.length;
                                    serviceCardLength = propertyCardsLength < serviceCardLength ? propertyCardsLength : serviceCardLength;
                                    serviceCardLength = serviceCardLength < MAX_SERVICE_CARDS ? serviceCardLength : MAX_SERVICE_CARDS;
                                    reRenderArea.html(siteVisits({
                                        response: response.cards,
                                        serviceCard,
                                        serviceCardLength,
                                        getServiceIcon: allianceIconService.getServiceIcon,
                                        serviceCardsData: response.serviceCards
                                    }));
                                    trackDashboardPageView(urlParam);
                                    scrollTop();
                                }, error => {
                                    showErrorPage();
                                    window.nanobar.go(100);
                                    trackDashboardPageView('Error');
                                    logger.error("error in sitevisits api", error);
                                });
                            } else {
                                mobileArticles(urlParam, reRenderArea);
                            }
                            break;

                        default:
                            if (scope.isLoggedIn) {
                                scope.currentAction = 'Mobile Menu';
                                window.nanobar.go(70);
                                apiService.get(sharedConfig.apiHandlers.buyerDashboard().url).then(response => {
                                    window.nanobar.go(90);
                                    response.wishList = getFavouriteCount();
                                    reRenderArea.html(mobileLayout({
                                        loggedIn: true,
                                        response: response,
                                        userInfo: getUserInfo(),
                                        alliances: scope.alliancesActive
                                    }));
                                    trackDashboardPageView();
                                    scrollTop();
                                }, error => {
                                    showErrorPage();
                                    window.nanobar.go(100);
                                    trackDashboardPageView('Error');
                                    logger.error("error in buyerDashboard api call", error);
                                });
                            } else {
                                reRenderArea.html(mobileLayout({
                                    loggedIn: false,
                                    response: {
                                        wishList: getFavouriteCount()
                                    },
                                    alliances: scope.alliancesActive
                                }));
                                trackDashboardPageView();
                                scrollTop();
                            }
                            break;
                    }
                },

                desktopHandler = function(urlParam) {
                    var reRenderArea = element;
                    scope.currentAction = capitalizeFirstLetter(urlParam);
                    switch (urlParam) {

                        case 'unitbook':
                        case 'homeloan':
                        case 'possession':
                        case 'registration':
                            showDesktopArticles(urlParam, reRenderArea);
                            break;

                        case 'savedsearches':
                            if (scope.isLoggedIn) {
                                window.nanobar.go(70);
                                getSavedSearches().then(response => {
                                    window.nanobar.go(90);
                                    if (response.cards && !response.cards.length) {
                                        showDesktopArticles(urlParam, reRenderArea);
                                        return;
                                    }
                                    scope.currentAction = capitalizeFirstLetter(urlParam);
                                    for (let i = 0, len = response.cards.length, cards = response.cards; i < len; i++) {
                                        cards[i].url = filterConfigService.getUrlWithUpdatedParams({
                                            overrideParams: cards[i].urlObject,
                                            skipPageData: true
                                        });
                                    }
                                    let serviceCardLength = response.serviceCards && response.serviceCards.length,
                                        propertyCardsLength = response.cards && response.cards.length;
                                    serviceCardLength = propertyCardsLength < serviceCardLength ? propertyCardsLength : serviceCardLength;
                                    serviceCardLength = serviceCardLength < MAX_SERVICE_CARDS ? serviceCardLength : MAX_SERVICE_CARDS;
                                    reRenderArea.html(getHtml(desktopLayout, {
                                        response: response.cards,
                                        query: urlParam,
                                        localityMap: response.localityIds,
                                        currentStage: scope.completedStage.currentStage,
                                        loggedIn: scope.isLoggedIn,
                                        serviceCard,
                                        serviceCardLength,
                                        getServiceIcon: allianceIconService.getServiceIcon,
                                        serviceCardsData: response.serviceCards
                                    }, desktopMenu));
                                    trackDashboardPageView(urlParam);
                                    scrollTop();
                                    return;
                                }, error => {
                                    showErrorPage();
                                    trackDashboardPageView('Error');
                                    window.nanobar.go(100);
                                    logger.error("mobile api get savedsearches call error", error);
                                });
                            } else {
                                showDesktopArticles(urlParam, reRenderArea);
                            }
                            break;

                        case 'favourites':
                            showFavourites(urlParam, reRenderArea);
                            break;

                        case 'recentlyviewed':
                            showFavourites(urlParam, reRenderArea);
                            break;

                        case 'sitevisits':
                            if (scope.isLoggedIn) {
                                window.nanobar.go(70);
                                getSiteVisits().then(response => {
                                    window.nanobar.go(90);
                                    if (response.cards && !response.cards.length) {
                                        showDesktopArticles(urlParam, reRenderArea);
                                        return;
                                    }
                                    scope.currentAction = capitalizeFirstLetter(urlParam);
                                    let serviceCardLength = response.serviceCards && response.serviceCards.length,
                                        propertyCardsLength = response.cards && response.cards.length;
                                    serviceCardLength = propertyCardsLength < serviceCardLength ? propertyCardsLength : serviceCardLength;
                                    serviceCardLength = serviceCardLength < MAX_SERVICE_CARDS ? serviceCardLength : MAX_SERVICE_CARDS;
                                    reRenderArea.html(getHtml(desktopSiteVisit, {
                                        query: urlParam,
                                        currentStage: scope.completedStage.currentStage,
                                        response: response.cards,
                                        serviceCard,
                                        serviceCardLength,
                                        getServiceIcon: allianceIconService.getServiceIcon,
                                        serviceCardsData: response.serviceCards
                                    }, desktopMenu));
                                    trackDashboardPageView(urlParam);
                                    scrollTop();
                                }, error => {
                                    window.nanobar.go(100);
                                    showErrorPage();
                                    trackDashboardPageView('Error');
                                    logger.error("error in sitevisits api", error);
                                });
                            } else {
                                showDesktopArticles(urlParam, reRenderArea);
                            }
                            break;

                        default:
                            if (scope.isLoggedIn) {
                                window.nanobar.go(40);
                                apiService.get(sharedConfig.apiHandlers.buyerDashboard().url).then(response => {
                                    window.nanobar.go(50);
                                    if (response.currentUrl) {
                                        desktopHandler(response.currentUrl);
                                    } else {
                                        desktopHandler('savedsearches');
                                    }
                                }, error => {
                                    window.nanobar.go(100);
                                    showErrorPage();
                                    trackDashboardPageView('Error');
                                    logger.error("error in buyerDashboard api call", error);
                                });
                            } else {
                                desktopHandler('savedsearches');
                            }
                            break;
                    }

                },
                setHandler = function(urlParam, loggedIn) {
                    if (scope.isMobile) {
                        mobileHandler(urlParam, loggedIn);
                    } else if (scope.isLoggedIn) {
                        var query = {};
                        query['query'] = 'currentstage';
                        apiService.get(sharedConfig.apiHandlers.buyerDashboard({
                            query
                        }).url).then(response => {
                            scope.completedStage = response;
                            sendJarvisEvent();
                            desktopHandler(urlParam ? urlParam : response.currentUrl);
                        }, error => {
                            showErrorPage();
                            trackDashboardPageView('Error');
                            logger.error("error in get current phase api call", error);
                        });
                    } else {
                        scope.completedStage.currentStage = 0;
                        desktopHandler(urlParam, loggedIn);
                    }
                },

                

            init = function(state) {
                    var url = window.location.pathname;
                    element = $(context.getElement());

                    if (!scope || !scope.reloaded) {
                        scope = {
                            isMobile: utils.isMobileRequest(),
                            completedStage: {},
                            error: false,
                            alliancesActive: element.data('alliances')
                        };
                        let config = context.getConfig();
                        if (config) {
                            let loginInfo = config.loginInfo;
                            scope.isLoggedIn = loginInfo && loginInfo.loggedIn;
                            setUserInfo(loginInfo && loginInfo.userInfo);
                        }
                    }

                    if (url.indexOf("/") != -1) {
                        urlParam = url.split('/')[2];
                        urlParam = urlParam === "dashboard" ? "" : urlParam;
                    }

                    if (state === 'loggedIn') {
                        scope.isLoggedIn = true;
                        setHandler(urlParam);
                    } else if (state === 'loggedOut') {
                        scope.isLoggedIn = false;
                        setHandler(urlParam, true);
                    } else {
                        if (scope.isLoggedIn) {
                            scope.isLoggedIn = true;
                            window.nanobar.go(20);
                            setHandler(urlParam);
                        } else {
                            scope.isLoggedIn = false;
                            window.nanobar.go(20);
                            setHandler(urlParam, true);
                        }
                    }

                },

                

                // isShortlist = function(urlParam) {
                //     return urlParam === 'enquired' || urlParam === 'favourites' || urlParam === 'recentlyviewed';
                // },
                _openLeadForm = function(dataset) {
                    let rawData = {
                        cityId: dataset.cityId,
                        salesType: dataset.listingCategory,
                        listingId: dataset.listingId,
                        localityId: dataset.localityId,
                        companyUserId: dataset.userId,
                        projectId: dataset.projectId,
                        companyId: dataset.sellerId,
                        companyName: dataset.sellerName,
                        companyPhone: dataset.sellerContactNumber || dataset.sellerPhone,
                        companyRating: dataset.sellerRating,
                        companyImage: dataset.sellerImage,
                        companyAssist: dataset.sellerAssist,
                        companyType: dataset.sellerType,
                        projectName: dataset.projectName,
                        builderId: dataset.builderId,
                        bhk: dataset.bhk,
                        projectStatus: dataset.projectStatus,
                        minBudget: dataset.budget,
                        maxBudget: dataset.budget,
                        budget: dataset.budget,
                        propertyType: [dataset.unitTypeId],
                        propertyTypeLabel: dataset.unitType,
                        imageUrl: dataset.imageUrl?dataset.imageUrl: "",
                        step: dataset.step,
                        id: "lead-popup"
                    };
                    /* in case of project case, we send price ranges */
                    if(dataset.minPrice && dataset.maxPrice && dataset.minPrice < dataset.maxPrice && !dataset.listingId) {
                        rawData.priceRanges = {};
                        rawData.priceRanges.minPrice = utils.formatNumber(dataset.minPrice, { precision : 2, returnSeperate : true, seperator : window.numberFormat });
                        rawData.priceRanges.maxPrice = utils.formatNumber(dataset.maxPrice, { precision : 2, returnSeperate : true, seperator : window.numberFormat });
                    }
                    context.broadcast('leadform:open', rawData);
                },

                getSellerInfo = function(id, city) {
                    var query = {};
                    query['query'] = 'seller';
                    query[city ? 'projectId' : 'listingId'] = id;
                    if (city) {
                        query['city'] = city;
                    }
                    return apiService.get(sharedConfig.apiHandlers.buyerDashboard({
                        query
                    }).url).then(response => {
                        return response;
                    }, err => {
                        logger.error(err);
                    });
                },
                trackClickLabel = function(dataset) {
                    if (dataset.trackLabel && dataset.trackAction) {
                        utils.trackEvent(dataset.trackAction, PAGE_CATEGORY, dataset.trackLabel);
                        return;
                    }
                    if (dataset.trackLabel && scope && scope.currentAction) {
                        utils.trackEvent(scope.currentAction, PAGE_CATEGORY, dataset.trackLabel);
                        return;
                    }
                },

                deleteSavedSearch = function(searchId) {
                    let query = {
                        query: 'savedsearches',
                        searchId: searchId
                    };
                    return apiService.deleteEntry(sharedConfig.apiHandlers.buyerDashboard({
                        query
                    }).url);
                },
                setSiteVisit = function(data) {
                    var time = data.value,
                        el = data.el.id,
                        messageBox = $("#" + el).closest('.sitevisitcard').find(".error-msg-enquiry"),
                        elData = $(data.el).data() || {},
                        postData = {},
                        leadId = parseInt(elData.leadId);

                    postData.agentId = parseInt(elData.sellerId);
                    postData.eventTypeId = 1;
                    postData.performTime = time;
                    if (elData.trackLabel) {
                        utils.trackEvent('CLICK_' + scope.currentAction, PAGE_CATEGORY, elData.trackLabel + ' Ok');
                    }
                    if (elData.listingId && parseInt(elData.listingId)) {
                        postData.listingDetails = [{
                            listingId: parseInt(elData.listingId)
                        }];
                    }
                    if (elData.projectId && parseInt(elData.projectId)) {
                        postData.projectIds = [parseInt(elData.projectId)];
                    }

                    window.nanobar.go(50);
                    apiService.postJSON(sharedConfig.apiHandlers.buyerDashboard({
                        query: {
                            sitevisit: true,
                            leadId: leadId
                        }
                    }).url, postData).then(() => {
                        messageBox.removeClass('hidden').html('site visit booked successfully');
                        context.broadcast('popup:open', {
                            id: 'messages'
                        });
                        window.nanobar.go(100);
                    }, err => {
                        utils.trackEvent('ERROR_Buyer Journey', 'Error_Buyer', 'error in booking site visit');
                        if (err && err.responseJSON && err.responseJSON.body && err.responseJSON.body.statusCode === "ONLY_ONE_FUTURE_EVENT_ALLOWED") {
                            messageBox.removeClass('hidden').html('site visit already been scheduled');
                            context.broadcast('popup:open', {
                                id: 'messages'
                            });
                        } else {
                            messageBox.removeClass('hidden').html('error in booking site visit.<br>Please try again later');
                            context.broadcast('popup:open', {
                                id: 'messages'
                            });
                        }
                        window.nanobar.go(100);
                        logger.error("error in site visit post api", err);
                    });
                },

                onmessage = function(name, data) {
                    switch (name) {
                        case 'dateTimePicker:change':
                            setSiteVisit(data);
                            break;

                        case 'onLoggedIn':
                            setUserInfo(data.response);
                            scope.isLoggedIn = true;
                            scope.reloaded = true;
                            window.nanobar.go(10);
                            init("loggedIn");
                            break;

                        case 'loggedOut':
                            setUserInfo();
                            scope.reloaded = true;
                            scope.isLoggedIn = false;
                            window.nanobar.go(10);
                            init("loggedOut");
                            break;

                        case 'favourites-synced':
                            scope.reloaded = true;
                            init();
                            break;

                        case 'delete-search':
                            deleteSavedSearch(parseInt(data.searchId)).then(() => {
                                let $countElement = $('#totalCount'),
                                    count = parseInt($countElement.html());
                                if (count === 1) {
                                    scope.reloaded = true;
                                    init();
                                } else {
                                    $('div.propCard[data-search-card="' + data.searchId + '"]').remove();
                                    $countElement.html(count - 1);
                                }
                                context.broadcast('popup:close');
                            }, error => {
                                logger.error("error in delete saved search api call", error);
                            });
                            break;

                        case 'delete-favourite':
                            trackClickLabel(data);
                            let $countElement = $('#favouriteCount'),
                                count = parseInt($countElement.html()),
                                cardType = data.cardType;
                            if (cardType === 'listing' && data.listing) {
                                shortlistService.removeShortlist({
                                    id: data.listing,
                                    type: 'listing'
                                });
                            } else if (cardType === 'project' && data.project) {
                                shortlistService.removeShortlist({
                                    projectId: data.project,
                                    type: 'project'
                                });
                            }
                            if (count === 1) {
                                scope.reloaded = true;
                                init();
                            } else {
                                $('li[data-' + cardType + '="' + data[cardType] + '"]').remove();
                                $countElement.html(count - 1);
                            }
                            context.broadcast('popup:close');
                            break;

                        case 'jarvisToBuyerJourney':
                            if (!data) {
                                break;
                            }
                            switch (data.type) {
                                case 'bj_sitevisit':
                                    context.broadcast('popup:open', {
                                        id: 'addEvent',
                                        steps: '2',
                                        stage: '1'
                                    });
                                    break;

                                case 'bj_book':
                                    context.broadcast('popup:open', {
                                        id: 'addEvent',
                                        steps: '5',
                                        stage: '2'
                                    });
                                    break;

                                case 'bj_posses':
                                    context.broadcast('popup:open', {
                                        id: 'addEvent',
                                        steps: '1',
                                        stage: '3'
                                    });
                                    break;

                                case 'bj_register':
                                    context.broadcast('popup:open', {
                                        id: 'addEvent',
                                        steps: '1',
                                        stage: '4'
                                    });
                                    break;
                            }
                    }
                },

                

                onclick = function(event, _element, elementType) {
                    var dataset;
                    dataset = $(_element).data();
                    switch (elementType) {
                        case 'bookNow':
                            commonService.ajaxify(dataset.link);
                            break;
                        case 'add-event':
                            trackClickLabel(dataset);
                            context.broadcast('popup:open', {
                                id: 'addEvent',
                                steps: '5',
                                stage: scope.completedStage.currentStage,
                                maxStage: 3
                            });
                            break;

                        case 'openLeadForm':
                            // trackClickLabel(dataset);
                            if (parseInt(dataset.sellerId) && parseInt(dataset.userId)) {
                                _openLeadForm(dataset);
                            } else if (parseInt(dataset.projectId)) {
                                getSellerInfo(dataset.projectId, dataset.cityId).then(response => {
                                    _openLeadForm($.extend(dataset, response));
                                }, err => {
                                    logger.error("error in get seller info", err);
                                });
                            }
                            break;

                        case 'login-logout':
                            trackClickLabel(dataset);
                            if (!scope.isLoggedIn) {
                                loginService.openLoginPopup();
                                return;
                            }
                            window.nanobar.go(50);
                            loginService.logoutUser();
                            break;

                        case 'anchor':
                            if (dataset.trackAction) {
                                utils.trackEvent(dataset.trackAction, PAGE_CATEGORY);
                            }
                            var path = _element.pathname;
                            commonService.ajaxify(path);
                            break;

                        case 'enquired':
                            trackClickLabel(dataset);
                            showFavourites('enquired', element);
                            break;

                        case 'favourites':
                            trackClickLabel(dataset);
                            showFavourites('favourites', element);
                            break;

                        case 'recent':
                            trackClickLabel(dataset);
                            showFavourites('recentlyviewed', element);
                            break;

                        case 'delete-search-confirm':
                            utils.trackEvent('CLICK_SavedSearch_Delete', PAGE_CATEGORY, dataset.url);
                            context.broadcast('delete-search', dataset);
                            break;

                        case 'delete-favourite-confirm':
                            context.broadcast('delete-favourite', dataset);
                            break;

                        case 'shortlist-anchor':  /* falls through */
                        case 'request-site-visit': /* falls through */
                        case 'direction':/* falls through */
                        case 'call':
                            trackClickLabel(dataset);
                            break;

                        case 'track-article':
                            if (dataset.trackLabel) {
                                utils.trackEvent('CLICK_' + scope.currentAction + '_Article', PAGE_CATEGORY, dataset.trackLabel);
                            }
                            break;

                        case 'trackSavedSearch':
                            utils.trackEvent('CLICK_Savedsearches_Card', PAGE_CATEGORY);
                            break;
                    }
                },

                destroy = function() {
                    element = undefined;
                    scope = undefined;
                };

            return {
                init,
                onclick,
                messages,
                onmessage,
                destroy
            };
        });
    }
);
