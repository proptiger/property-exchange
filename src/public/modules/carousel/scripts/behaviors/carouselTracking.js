define([
    'common/trackingConfigService',
    'services/trackingService',
    'services/urlService',
    'services/utils'
], function(t, trackingService, urlService, utils) {
    'use strict';
    Box.Application.addBehavior('carouselTracking', function(context) {

        var messages = [
            'trackPrevButtonCarouselClick',
            'trackNextButtonCarouselClick',
            'trackSERPCarouselSwipe'
        ];

        var value,
            moduleId = context.getElement().id;

        var onmessage = function(msgName, data) {
        	if (data.moduleId && data.moduleId != moduleId) {
                return;
            }

            let event = t.GALLERY_INTERACTION_EVENT,
                category = data.category || t.SERP_CARD_CATEGORY,
                name;

            switch (msgName) {
                case 'trackPrevButtonCarouselClick':
                    name = t.PREV_NAME;
                    break;
                case 'trackNextButtonCarouselClick':
                    name = t.NEXT_NAME;
                    break;
                case 'trackSERPCarouselSwipe':
                    name = t.SWIPE_NAME;
                    break;
            }

            let properties = {};
            properties[t.CATEGORY_KEY] = category;
            properties[t.NAME_KEY] = name;
            properties[t.VALUE_KEY] = parseInt(urlService.getUrlParam('page')) || 1;
            properties[t.RANK_KEY] = data.rank;
            properties[t.PROJECT_ID_KEY] = data.projectId ? parseInt(data.projectId) : undefined;
            properties[t.PROJECT_STATUS_KEY] = t.makeValuesUniform(data.projectStatus,t.PROJECT_STATUS_NAME) ;
            properties[t.BUILDER_ID_KEY] = data.builderId ? parseInt(data.builderId) : undefined;
            properties[t.BUDGET_KEY] = data.price;
            properties[t.UNIT_TYPE_KEY] = t.makeValuesUniform(data.propertyType,t.UNIT_TYPE_MAP) ;
            properties[t.BHK_KEY] = data.bedrooms ? parseInt(data.bedrooms) : undefined;
            properties[t.SELLER_ID_KEY] = data.sellerId;
            properties[t.SELLER_SCORE_KEY] = parseFloat(data.sellerScore) ? parseFloat(data.sellerScore) : undefined;
            properties[t.LOCALITY_ID_KEY] = data.localityId;
            properties[t.SUBURB_ID_KEY] = data.suburbId;
            properties[t.CITY_ID_KEY] = data.cityId;
            properties[t.LISTING_ID_KEY] = data.listingId;
            properties[t.LISTING_SCORE_KEY] = data.listingScore;
            properties[t.SELECT_PROPERTY_KEY] = data.isSelectListing ? 'Select Listing':undefined;

            value = utils.getTrackingListingCategory(data.listingCategory);
            properties[t.LISTING_CATEGORY_KEY] = t.makeValuesUniform(value,t.LISTING_CATEGORY_MAP) ;
            
            trackingService.trackEvent(event, properties);
        };

        return {
            messages: messages,
            onmessage: onmessage,
            init: function() {},
            destroy: function() {}
        };
    });
});
