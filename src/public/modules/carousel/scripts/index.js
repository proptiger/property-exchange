//comment entire code
define(["doT!modules/carousel/views/index",
        "common/sharedConfig",
        'services/apiService',
        'services/utils',
        'modules/carousel/scripts/behaviors/carouselTracking'
    ],
    function(html, sharedConfig, _apiService, utilfunc) {
        "use strict";
        const MODULE_NAME = "carousel";
        Box.Application.addModule(MODULE_NAME, function(context) {

            var _data,
                _template,
                _element,
                _imageContainer,
                _imageArrayLength,
                imageCountUpdateSelector,
                _config,
                _currentSlideIndex = 0,
                _imageWidth, //change this according to device
                _imageHeight,
                _constants = {
                    imagesPerSlide: 1,
                    preloadImages: 5, // the number of images to be loaded upfront
                    nextloadImages: 2
                },
                configData, id, moduleId, currentSlideIntervalId, $element;
  
            var changeImageContainerWidth = function() {
                _imageArrayLength = _data.length;
                _imageContainer.style.width = (_imageArrayLength * _imageWidth) + "px";
            };

            var updateCurrentSlideIndexOnDOM = function(currentSlideIndex){
                let $currentListing = $element.closest(sharedConfig.DATA_MODULE_LISTING);
                $currentListing && $currentListing.attr(sharedConfig.DATA_CURRENT_SLIDE_INDEX,currentSlideIndex);
            }

            var handleButtonDisable = function() {
                var backButton = Box.DOM.queryData(_element, 'type', "back-btn");
                var nextButton = Box.DOM.queryData(_element, 'type', "next-btn");
                if(backButton){
                    if (_currentSlideIndex === 0) {
                        backButton.classList.add("disable");
                    } else {
                        backButton.classList.remove("disable");
                    }
                }
                if(nextButton){
                    if (_currentSlideIndex === _imageArrayLength - 1 || (typeof(_config.imageCount) === "number" && _currentSlideIndex === _config.imageCount)) {
                        nextButton.classList.add("disable");
                    } else {
                        nextButton.classList.remove("disable");
                    }
                }
            };
            var setCurrentImage = function(index) {
                _imageContainer.style.transition = "all 0.3s";
                var pixels = -index * _imageWidth + "px";
                _imageContainer.style.left = pixels;
            };
            
            //Function for smooth transition of carousel slider
            var setCurrentImageSmoothly = function setCurrentImageSmoothly(pixelDeviation) {
                if(_imageContainer){
                    _imageContainer.style.transition = "all 0s";
                }
                
                if(_imageContainer && !_imageContainer.style.left){
                    _imageContainer.style.left = "0px";
                }
                var pixels = parseInt(_imageContainer.style.left) + pixelDeviation + "px";
                if(parseInt(pixels) < 0 && parseInt(pixels) > (_imageWidth - parseInt(_imageContainer.style.width))){
                    _imageContainer.style.left = pixels;
                }
            };

            //wait for load event of next image if it is already loaded fire a load event itself
            var setListener = function(index) {
                $($element.find(".img-slide img")[index]).one("load", function() {
                    //$element.removeClass('slide-active');
                    setCurrentImage(index);
                }).each(function() {
                    if (this.complete) { $(this).load(); }
                });
            };

            // gives an array for next images to be loaded
            var nextImages = function() {
                var loadedImageCount = _imageContainer.childElementCount;
                if (_data && _data.length > loadedImageCount) {
                    if (loadedImageCount === _constants.imagesPerSlide) {
                        return _data.slice(loadedImageCount, _constants.imagesPerSlide * _constants.preloadImages);
                    }
                    return _data.slice(loadedImageCount, loadedImageCount + _constants.nextloadImages * _constants.imagesPerSlide);
                }
            };

            //moves the image container to set the positon according to current image


            var apiRequestUrl = function({ objectType, objectId, query } = {}) { //request only images
                return sharedConfig.apiHandlers.getImages({
                    objectType,
                    objectId,
                    query
                });
            };

            var _orderImages = function(images) {
                let imageObjectTypes = [_config.id, _config.projectId, _config.propertyId],
                    result = [];
                images.buckets.forEach((v) => {
                    v.buckets.forEach((val) => {
                        imageObjectTypes.forEach((ids) => {
                            if (images[ids] && images[ids][val]) {
                                result = result.concat(images[ids][val]);
                            }
                        });
                    });
                });
                return result;
            };

            function calculateNextImageAspectRatio(images, _imageWidth, _imageHeight) {
                if(images){
                    images.forEach(function(currentImage){
                        if(currentImage && currentImage.additionalClass === undefined) {
                            currentImage.additionalClass = utilfunc.getImageClassForAR(currentImage.width,
                                currentImage.height, _imageWidth, _imageHeight);
                        }
                    });
                }
            }

            var addImagesToCarousel = function(){
                let images = nextImages();
                calculateNextImageAspectRatio(images, _imageWidth, _imageHeight);
                _imageContainer.insertAdjacentHTML('beforeend', _template({
                        images: images,
                        width: _imageWidth,
                        height: _imageHeight,
                        imageCount: _config.imageCount
                }));
            };

            var showNextImage = function(direction) {            
                if (direction < 0) {
                    if (_currentSlideIndex === 0) {
                        //$element.removeClass("slide-active");
                        return;
                    }
                    _currentSlideIndex = (_currentSlideIndex - _constants.imagesPerSlide);
                } else {
                    if (_currentSlideIndex === _imageArrayLength - 1) {
                        //$element.removeClass("slide-active");
                        return;
                    }
                    addImagesToCarousel();
                    _currentSlideIndex = (_currentSlideIndex + _constants.imagesPerSlide);
                }
                updateCurrentSlideIndexOnDOM(_currentSlideIndex);
                _updateUIImageCount(_currentSlideIndex + 1);
                setCurrentImage(_currentSlideIndex);
                handleButtonDisable();
            };


            //Fetch Images
            var fetchImageDetails = function(){
                if (_imageContainer.childElementCount === _constants.imagesPerSlide) {
                    if (_config.type && _config.id) {
                        let query = { 
                            'projectId': _config.projectId, 
                            "mainImageId": _config.defaultImageId,
                            'isGallery': true
                        };
                        if(_config.propertyId) {
                            query.propertyId = _config.propertyId;
                        }
                        return _apiService.get(apiRequestUrl({ objectType: _config.type, objectId: _config.id, query: query }).url).then(function(response) {
                            if (response) {
                                _data = defineIndex(response.data);
                                changeImageContainerWidth();
                            }
                        });
                    }
                    //if the _data is already present in the _config
                    else if (_config.imagesData) {
                        _data = _config.imagesData;
                        changeImageContainerWidth();
                    }
                }
            };
       
            const defineIndex = (imageList = []) => {
                imageList.forEach((imageObj, index) => {
                    imageObj['index'] = index;
                });
                return imageList;
            };

            //image click handler sends api request on first click and just shift images on remaining clicks
            var changeImage = function(direction) {
                if (_config && typeof(_config.imageCount) !== "number" || _config.imageCount > 1) {
                    renderActiveDotInView(direction);
                    //$element.addClass("slide-active");
                    let fetchImageDetailsPromise = fetchImageDetails();
                    if(fetchImageDetailsPromise && fetchImageDetailsPromise.then && typeof fetchImageDetailsPromise.then == "function"){
                        fetchImageDetailsPromise.then(res => {
                            addImagesToCarousel();
                        });
                    }
                    showNextImage(direction);
                }
            };

            function _updateUIImageCount(_currentSlideIndex) {
                if (_currentSlideIndex < 1 || _currentSlideIndex > _config.imageCount) {
                    return;
                }
                $(imageCountUpdateSelector).html(_currentSlideIndex);
            }

            function renderActiveDotInView(direction) {
                if((_currentSlideIndex === _imageArrayLength-1 && direction > 0) || _currentSlideIndex+direction < -1) {
                    return;
                }
                const transitionTimeout = 350, noWidthClass = "no-width",
                activeClass = "active";

                let dotsElem = _element.querySelector(".dots"),
                allDotElms = $element.find(".dot"), 
                activeIndex = allDotElms.filter("."+activeClass).index(),
                $firstDot = $(allDotElms.not(".no-width")[0]), 
                $lastDot = $(allDotElms.not(".no-width")[allDotElms.length-1]);

                if(activeIndex > -1 && activeIndex+direction !== -1) {
                    $(allDotElms[activeIndex]).removeClass(activeClass);
                    $(allDotElms[activeIndex+direction]).addClass(activeClass);
                    
                    if(direction > 0 && activeIndex > 1 && _currentSlideIndex < _imageArrayLength-3) {
                        //Next case
                        $firstDot.addClass(noWidthClass);
                        //after the transition end
                        setTimeout(function(){
                            //Remove firstchild and append to the end for circling of dots
                            dotsElem.removeChild($firstDot[0]);
                            dotsElem.appendChild($firstDot[0]);
                            $firstDot.removeClass(noWidthClass);
                        }, transitionTimeout);
                        
                    } else if (direction < 0 && activeIndex < 3 && _currentSlideIndex > 2) {
                        $lastDot.addClass(noWidthClass);
                        //Remove last child and prepend to the start
                        dotsElem.removeChild($lastDot[0]);
                        dotsElem.prepend($lastDot[0]);
                        setTimeout(function(){
                            //This is for transition efect
                            $lastDot.removeClass(noWidthClass);
                        }, 0);
                    }
                }
            }

            var startCarouselSlide = function() {
                if (_currentSlideIndex === _imageArrayLength - 1 || (typeof(_config.imageCount) === "number" && _currentSlideIndex === _config.imageCount)) {
                    _currentSlideIndex = 0;
                    updateCurrentSlideIndexOnDOM(_currentSlideIndex);
                    setCurrentImage(0);
                    _updateUIImageCount(_currentSlideIndex + 1);
                    handleButtonDisable();
                } else {
                    changeImage(1);
                }
            };
            var stopCarouselSlide = function() {
                if (currentSlideIntervalId) {
                    clearInterval(currentSlideIntervalId);
                }
            };

            function handleMobileEvents() {
                let startCoordsX, endCoordsX, startCoordsY, endCoordsY, isMoved = false, swipeThreshold = 25;
                $element.on('touchstart', function(e){
                    startCoordsX = e.originalEvent.targetTouches[0].pageX;
                    startCoordsY = e.originalEvent.targetTouches[0].pageY;
                });
                $element.on('touchmove', function(e){
                    var currentCoordsX = e.originalEvent.targetTouches[0].pageX;
                    var currentCoordsY = e.originalEvent.targetTouches[0].pageY;
                    var deviationX = currentCoordsX - endCoordsX;
                    var deviationY = currentCoordsY - endCoordsY;
                    if(Math.abs(startCoordsX - endCoordsX) > swipeThreshold){  
                        isMoved = true;                  
                    }
                    endCoordsX = e.originalEvent.targetTouches[0].pageX;
                    endCoordsY = e.originalEvent.targetTouches[0].pageY;      
                    if((Math.abs(deviationX) / Math.abs(deviationY) > 3)){
                        setCurrentImageSmoothly(deviationX);
                    }
                    if((Math.abs(deviationX) / Math.abs(deviationY) > 0.33)){
                        e.preventDefault();
                    }
                    // Fetch Images from server
                    let fetchImageDetailsPromise = fetchImageDetails();
                    if(fetchImageDetailsPromise && fetchImageDetailsPromise.then && typeof fetchImageDetailsPromise.then == "function"){
                        fetchImageDetailsPromise.then(res => {
                            addImagesToCarousel();
                        });
                    }
                });
                $element.on('touchend', function(){
                    let distance = startCoordsX - endCoordsX;
                    let direction = 0;
                    if(isMoved) {
                        isMoved = false;
                        if(distance < 0 ) {
                            direction = -1;
                        } else if(distance > 0) {
                            direction = 1;
                        }
                        changeImage(direction);
                        if(_imageArrayLength === undefined && direction > 0){
                            setCurrentImage(_currentSlideIndex + direction);
                        }
                        context.broadcast('trackSERPCarouselSwipe', _getTackingData());
                    }
                    else {
                        setCurrentImage(_currentSlideIndex);
                    }
                    startCoordsX = undefined;
                    endCoordsX = undefined;
                });        
            }

            function removeTouchEvents() {
                $element.off('touchstart');
                $element.off('touchmove');
                $element.off('touchend');
            }
         
            //based upon direction move the image container

            var onmessage = function(name, data) {
                switch (name) {
                    case 'triggerListingMouseenter':
                        if (data.id == id && configData.autoScroll) {
                            //change first image quickly and then start slide show with interval
                            setTimeout(startCarouselSlide,1000);
                            currentSlideIntervalId = setInterval(function() { startCarouselSlide(); }, 3000);
                        }
                        break;
                    case 'triggerListingMouseleave':
                        if (data.id == id && configData.autoScroll) {
                            stopCarouselSlide();
                        }
                        break;
                    case "listing-configData":
                        if(data.id != configData.id){
                            return;
                        }
                        let cData = _getConfigData(data);
                        _initConfig(cData);
                        break;
                    case "carousel-configData":
                        if(data.id === configData.id) {
                            let cData = _getConfigData(data);
                            _initConfig(cData);
                        }
                        break;
                }
            };


            var _getTackingData = function() {
                return {
                    rank: configData.rank,
                    projectId: configData.projectId,
                    projectStatus: configData.projectStatus,
                    builderId: configData.builderId,
                    price: configData.price,
                    propertyType: configData.propertyType,
                    bedrooms: configData.bedrooms,
                    sellerId: configData.sellerId,
                    sellerScore: configData.sellerScore,
                    localityId: configData.localityId,
                    suburbId: configData.suburbId,
                    cityId: configData.cityId,
                    listingId: configData.id,
                    listingScore: configData.listingScore,
                    listingCategory: configData.listingCategory,
                    moduleId: moduleId,
                    isSelectListing:configData.isSelectListing,
                    category: configData.gaCategory
                };
            };

            var _getConfigData = function(rawData){
                return {
                    "id":rawData.id,
                    "type":"listing",
                    "projectId": rawData.projectId,
                    "propertyId": rawData.propertyId,
                    "imageCount":rawData.imageCount,
                    "defaultImageId":rawData.defaultImageId,
                    "builderId": rawData.builderId,
                    "sellerId": rawData.companyId,
                    "sellerScore": rawData.companyRating,
                    "projectStatus": rawData.projectStatus,
                    "rank": rawData.rank,
                    "price": rawData.price,
                    "propertyType": rawData.propertyType,
                    "bedrooms": rawData.bedrooms,
                    "localityId": rawData.localityId,
                    "suburbId": rawData.suburbId,
                    "cityId": rawData.cityId,
                    "listingScore": rawData.listingScore,
                    "listingCategory": rawData.listingCategory,
                    "gaCategory": rawData.gaCategory,
                    "mainImageWidth": rawData.mainImageWidth,
                    "mainImageHeight": rawData.mainImageHeight,
                    "autoScroll":false,
                    "isSelectListing":rawData.isMakaanSelectSeller && rawData.listingCategory && rawData.listingCategory.toLowerCase()=="rental"
                };
            };

            var _initConfig = function(data){
                configData = data;
                _config = data;
                id = configData.id;
                if(data.dataAwaited){
                    return;
                }
                $.extend(_constants, _config);
                //_imageWidth = _imageContainer.offsetWidth / _constants.imagesPerSlide;
                _imageHeight = _imageContainer.offsetHeight;
                _imageContainer.childNodes[0].style.width = _imageWidth + "px";
                handleButtonDisable();
                calculateMainImageAspectRatio();
            };

            function calculateMainImageAspectRatio(){
                $element.find("img:first").addClass(
                    utilfunc.getImageClassForAR(configData.mainImageWidth,
                                configData.mainImageHeight, $element.width(), $element.height()));
            }
            function backButtonHandler(){
                context.broadcast('trackPrevButtonCarouselClick', _getTackingData());
                changeImage(-1);
                stopCarouselSlide();
            }
            function nextButtonHandler(){
                console.log('inside nextButton handler');
                context.broadcast('trackNextButtonCarouselClick', _getTackingData());
                changeImage(1);
                stopCarouselSlide();
            }



            return {
                messages: ['triggerListingMouseenter', 'triggerListingMouseleave', 'listing-configData', 'carousel-configData'],
                behaviors: ['carouselTracking'],
                onmessage,
                init: function() {
                    _element = context.getElement();
                    $element = $(_element);
                    moduleId = _element.id;
                    imageCountUpdateSelector = $element.find('[data-target="changeCurrentImageCount"]')[0];
                    _imageContainer = Box.DOM.queryData(_element, 'container', "image-gallery");
                    _imageWidth = _element.clientWidth;
                    _imageContainer.style.width = (2 * _imageWidth) + "px";
                    let data = context.getConfig();
                    _initConfig(data);
                    _template = html;
                    if(data.isMobile) {
                        handleMobileEvents();
                    }
                    context.broadcast("moduleLoaded", {
                        name: MODULE_NAME,
                        id: moduleId
                    });
                },
                
                destroy: function() {
                    _element = null;
                    _config = null;
                    _imageWidth = null;
                    _imageContainer = null;
                    _template = null;
                    stopCarouselSlide();
                    removeTouchEvents();
                },
                onclick: function(event, _element, _elementType) {
                    switch (_elementType) {
                        case 'back-btn':
                            backButtonHandler();
                            break;
                        case 'next-btn':
                            nextButtonHandler();
                            break;
                    }
                    //event.stopPropagation();
                }
            };
        });
    }
);
