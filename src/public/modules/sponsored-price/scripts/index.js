"use strict";
define(["services/utils", "doT!modules/sponsored-price/views/index"], (UtilService, template) => {
    Box.Application.addModule("sponsored-price", (context) => {
        function init() {
            var moduleEl = context.getElement(),
                config = context.getConfig() || {},
                isSemPage = config.isSemPage,
                minPrice, maxPrice,
                emi,
                query = Box.DOM.query;


            if (isSemPage) {
                let sellerPopupBox = $("#all-seller-details");
                minPrice = UtilService.formatNumber(sellerPopupBox.data('price-min'), { precision : 2, returnSeperate : true, seperator : window.numberFormat });
                maxPrice = UtilService.formatNumber(sellerPopupBox.data('price-max'), { precision : 2, returnSeperate : true, seperator : window.numberFormat });
            }

            if (minPrice.unit == maxPrice.unit && minPrice.val == maxPrice.val) {
                maxPrice = undefined;
            }

            let htmlContent = template({
                minPrice,
                maxPrice
            });

            $(Box.DOM.queryData(moduleEl, 'type', 'price-range')).append(htmlContent);
            $(moduleEl).removeClass('hidden');

            let emiElem = query("body","[data-emi-sponsored-wrap]");
            if(emiElem) {
                if(emi) {
                   $(emiElem).find(".emi-price").html(emi);
                }
                $(emiElem).removeClass("hidden");
            }

        }
        return {
            init
        };

    });
});
