/*
Module config data sample
{
    "layoutDesktopTemplate": "doT!modules/filter/views/layout-rent-desktop",
    "layoutMobileTemplate": "doT!modules/filter/views/layout-rent-mobile"
}
*/
'use strict';
define([
    "services/commonService",
    "services/filterConfigService",
    "services/urlService",
    "services/utils",
    "services/searchService",
    "common/sharedConfig",
    "doT!modules/filter/views/layout-desktop",
    "doT!modules/filter/views/layout-desktop-commercial",
    "doT!modules/filter/views/layout-mobile",
    "doT!modules/filter/views/layout-mobile-commercial",
    "doT!modules/filter/views/layout-rent-desktop",
    "doT!modules/filter/views/layout-rent-desktop-commercial",
    "doT!modules/filter/views/layout-rent-mobile",
    "doT!modules/filter/views/layout-rent-mobile-commercial",
    "modules/filter/scripts/behaviors/filterTracking",
    "doT!modules/filter/views/layout-mcards-desktop",
    "doT!modules/filter/views/layout-mini-desktop",
    "doT!modules/filter/views/layout-mini-desktop-commercial",
    "doT!modules/filter/views/layout-mini-mobile",
    "doT!modules/filter/views/layout-mini-mobile-commercial",
], function(commonService, filterConfigService, urlService, utilService, searchService,sharedConfig) {
    Box.Application.addModule("filter", function(context) {

        const moduleElement = context.getElement(),
            $moduleEl = $(moduleElement),
            config = context.getGlobal('config'),
            DATA_TARGET = 'target',
            BUDGET_PLACEHOLDER = 'budget-placeholder',
            BEDROOM_PLACEHOLDER = 'bedroom-placeholder',
            SELLER_RATING_PLACEHOLDER = 'seller-rating-placeholder',
            PROPERTYTYPE_PLACEHOLDER = 'prop-type-placeholder',
            FURNISHTYPE_PLACEHOLDER = 'furnish-type-placeholder',
            AGE_OF_PROPERTY_PLACEHOLDER = 'age-of-property-placeholder',
            LISTEDBY_PLACEHOLDER = 'listed-by-placeholder',
            SORT_PLACEHOLDER = 'sort-placeholder',
            PROJECT_STATUS_PLACEHOLDER = 'status-placeholder',
            AREA_PLACEHOLDER = 'area-placeholder',
            FLOOR_PLACEHOLDER = 'floor-placeholder',
            MORE_FILTER_COUNT_PLACEHOLDER = 'more-filter-count-placeholder',
            PROPERTY_AREA_WRAPPER = 'property-area-dropdown',
            PROPERTY_FLOOR_WRAPPER = 'property-floor-dropdown',
            RESET_PLACEHOLDER = 'reset-filters',
            MORE_FILTER_DIV = 'more-filters-btn-div',
            EXPAND_FILTER_DIV = 'expanded-filters-div',
            INHERIT_SCROLL_CLASS = 'inheritscroll',
            FILTER_CONTAINER_CLASS = 'js-more-filter-dd',
            LOCATION_CONTAINER_ID = 'flocation',
            layoutDesktop = 'doT!modules/filter/views/layout-desktop',
            layoutCommercialDesktop = 'doT!modules/filter/views/layout-desktop-commercial',
            layoutMobile = 'doT!modules/filter/views/layout-mobile',
            layoutMobileCommercial = 'doT!modules/filter/views/layout-mobile-commercial',
            layoutRentDesktop = 'doT!modules/filter/views/layout-rent-desktop',
            layoutCommercialRentDesktop = 'doT!modules/filter/views/layout-rent-desktop-commercial',
            layoutMcardsTemplateName = 'doT!modules/filter/views/layout-mcards-desktop',
            layoutRentMobile = 'doT!modules/filter/views/layout-rent-mobile',
            layoutRentMobileCommercial = 'doT!modules/filter/views/layout-rent-mobile-commercial',
            layoutMiniDesktop = 'doT!modules/filter/views/layout-mini-desktop',
            layoutMiniDesktopCommercial = 'doT!modules/filter/views/layout-mini-desktop-commercial',
            layoutMiniMobile = 'doT!modules/filter/views/layout-mini-mobile',
            layoutMiniMobileCommercial = 'doT!modules/filter/views/layout-mini-mobile-commercial',
            SEARCH_BTN_LOADER = '.js-btnloader',
            SEARCH_COUNT_PROPERTY = '.js-count-properties';
        var pageData = utilService.getPageData();
        var moreFiltersActiveFlag, tempHref; // used to prevent filters to change url when more filters opened
        var applyFilters = {};
        var tempListingType = pageData.listingType;
        var overrideParams = {};
        var budgetPlaceholder, bedroomPlaceholder, sellerRatingPlaceHolder, propertyTypePlaceholder, furnishingPlaceHolder, listedbyPlaceholder, sortPlaceholder, areaPlaceholder, floorPlaceholder, moreFilterCountPlaceholder, resetPlaceholder, filterContainer, projectStatusPlaceholder, ageOfPropertyPlaceHolder;
        var moreFilterMap = {},
            locationData = {},
            moduleConfig,
            listingTypeIds = {
                'BUY_ID': 'buyid',
                'RENT_ID': 'rentid',
                'COMMERCIAL_BUY_ID': 'commercialBuyid',
                'COMMERCIAL_LEASE_ID': 'commercialLeaseid'
            };

        var _toggleClass = function(DROPDOWN_WRAPPER) {
            let reference = Box.DOM.query(moduleElement, "[data-type='"+DROPDOWN_WRAPPER+"']");
            if (!reference) {
                return;
            }
            $(reference).toggleClass(config.activeClass);
            $(reference).toggleClass(config.focusClass);
            $(reference).toggleClass(config.openClass);
        };

        var _removeClass = function(DROPDOWN_WRAPPER){
            let reference = Box.DOM.query(moduleElement, "[data-type='"+DROPDOWN_WRAPPER+"']");
            if (!reference) {
                return;
            }
            $(reference).removeClass(config.activeClass);
            $(reference).removeClass(config.focusClass);
            $(reference).removeClass(config.openClass);
        };
        var _reinitFilters = function() {
            _setResetButtonStatus();
            commonService.stopAllModules(moduleElement);
            commonService.startAllModules(moduleElement);
        };

        var _resetFilters = function(name,resetTempListingType) {
            var filterConfig = filterConfigService.getFilterModuleConfig(),
                newParams = {};
            if (name) {
                if (filterConfig[name] !== null) {
                    newParams[name] = null;
                }
            } else {
                for (let fc in filterConfig) {
                    newParams[fc] = null;
                }
                newParams.page = null;
            }
            moreFiltersActiveFlag = (moduleConfig.mCards || moduleConfig.miniFilter) ? true: false;
            tempHref = undefined;
            applyFilters = {};
            overrideParams = {};
            $(`.${EXPAND_FILTER_DIV}`).addClass('hide');
            $(`.${MORE_FILTER_DIV}`).removeClass('hide');

            urlService.changeMultipleUrlParam(newParams);
            if(resetTempListingType && pageData.listingType){
                tempListingType = pageData.listingType;
            }
            utilService.setPageData("listingType", tempListingType);
            _reinitFilters();
        };


        var _updateBudgetPlaceholder = function(data) {
            if (data.name !== 'budget' || budgetPlaceholder === null) {
                return;
            }

            var str = budgetPlaceholder.getAttribute('data-default');
            if (data.value) {
                var paramArray = data.value.split(',');

                if(paramArray[1] && (paramArray[0] === null || paramArray[0] === '')){
                    paramArray[0] = '0';
                }

                if (paramArray[0] !== null && paramArray[0] !== '') {
                    str = utilService.formatNumber(paramArray[0], { seperator : window.numberFormat });
                    if (paramArray[1] === '' || paramArray[1] === 'undefined') {
                        str += " +";
                    } else {
                        str += ' - ' + utilService.formatNumber(paramArray[1], { seperator : window.numberFormat });
                    }
                }
                str = "<span class='capitalize'>" + str + "</span>";
            }
            $(budgetPlaceholder).html(str);
        };

        var _updateAreaPlaceholder = function(data) {
            if (data.name != 'area' || areaPlaceholder === null) {
                return;
            }
            var str = areaPlaceholder.getAttribute('data-default');
            if (data.value) {
                var paramArray = data.value.split(',');
                if (paramArray[0] !== null && paramArray[0] !== '') {
                    str = utilService.formatNumber(paramArray[0], { type : 'number', seperator : window.numberFormat });
                    if (paramArray[1] === '') {
                        str += " sqft +";
                    } else {
                        str += ' - ' + utilService.formatNumber(paramArray[1], { type : 'number', seperator : window.numberFormat }) + ' sqft';
                    }
                }
            }
            $(areaPlaceholder).html(str);
        };

        var _updateFloorPlaceholder = function(data) {
            if (data.name != 'floor' || floorPlaceholder === null) {
                return;
            }
            var str = floorPlaceholder.getAttribute('data-default');
            if (data.value) {
                var paramArray = data.value.split(',');
                if (paramArray[0] !== null && paramArray[0] !== '') {
                    str = utilService.formatNumber(paramArray[0], { type : 'number', seperator : window.numberFormat });
                    if (paramArray[1] === '') {
                        str += " +";
                    } else {
                        str += ' - ' + utilService.formatNumber(paramArray[1], { type : 'number', seperator : window.numberFormat });
                    }
                }
            }
            $(floorPlaceholder).html(str);
        }

        var _updateBedroomPlaceholder = function(data) {
            if (data.name != 'beds' || bedroomPlaceholder === null) {
                return;
            }
            var paramArray = data.value,
                str = bedroomPlaceholder.getAttribute('data-default');
            if (paramArray && paramArray.length > 0) {
                if(paramArray.length == 4){
                    str = [...paramArray].splice(0,2);
                    str = str.join(',')+ ',2+ bhk';
                } else {
                    str = paramArray.join(", ").replace('plus', '+') + ' bhk';    
                }
            }
            $(bedroomPlaceholder).html(str);
        };

        var _updateSellerRatingPlaceHolder = function(data){
            var str = '';
            if (data.name != 'sellerRating' || bedroomPlaceholder === null) {
                return;
            }
            if(~['any','all'].indexOf((data.value||'').toLowerCase())) {
                str = sellerRatingPlaceHolder.getAttribute('data-default');
            } else {
                str = data.label || data.value || '';
            }
            $(sellerRatingPlaceHolder).html(str);
        };

        var _updatePropertyTypePlaceholder = function(data) {
            if (data.name != 'propertyType' || propertyTypePlaceholder === null) {
                return;
            }
            var paramArray = data.value,
                paramLabel = data.label || paramArray,
                str = propertyTypePlaceholder.getAttribute('data-default');
            if(paramArray && paramArray.length > 0){
                str = paramLabel[0];
                if (paramArray.length>1) {
                    str += ' +' + (paramArray.length-1).toString();
                }
            }
            $(propertyTypePlaceholder).html(str);
        };

        var _updateFurnishingPlaceholder = function(data){
            if (data.name != 'furnished' || furnishingPlaceHolder === null) {
                return;
            }
            var paramArray = data.value,
                paramLabel = data.label || paramArray,
                str = furnishingPlaceHolder.getAttribute('data-default');
            if(paramArray && paramArray.length > 0){
                str = paramLabel[0];
                if (paramArray.length>1) {
                    str += ' +' + (paramArray.length-1).toString();
                }
            }
            $(furnishingPlaceHolder).html(str);
        }

        var _updateAgeOfPropertyPlaceHolder = function(data){
            if (data.name != 'ageOfProperty' || ageOfPropertyPlaceHolder === null) {
                return;
            }
            var paramArray = data.value,
                str = ageOfPropertyPlaceHolder.getAttribute('data-default');
            if(paramArray && paramArray.length > 0){
                str = paramArray[0];
                if (paramArray.length>1) {
                    str += ' +' + (paramArray.length-1).toString();
                }
            }
            $(ageOfPropertyPlaceHolder).html(str);
        }

        var _updatePostedbyPlaceholder = function(data){
            if (data.name != 'postedBy' || listedbyPlaceholder === null) {
                return;
            }
            var paramArray = data.value,
                paramLabel = data.label || paramArray,
                str = listedbyPlaceholder.getAttribute('data-default');
            if(paramArray && paramArray.length > 0){
                str = paramLabel[0];
                if (paramArray.length>1) {
                    str += ' +' + (paramArray.length-1).toString();
                }
            }
            $(listedbyPlaceholder).html(str);
        }

        var _updateSortPlaceholder = function(data) {
            if (data.name !== 'sortBy' || sortPlaceholder === null) {
                return;
            }
            $(sortPlaceholder).html(data.label);
        };

        var _updateMoreFilterCountPlaceholder = function(data) {
            if (!data.morefilter || !data.name || !moreFilterCountPlaceholder) {
                return;
            }

            if ((data.value && data.value.length > 0) || data.value > 0) {
                moreFilterMap[data.name] = true;
            } else {
                delete moreFilterMap[data.name];
            }

            var moreFilterString = moreFilterCountPlaceholder.getAttribute('data-default') || '',
                moreFilterCount = Object.keys(moreFilterMap).length;
            if (moreFilterCount > 0) {
                moreFilterString = "(" + moreFilterCount + ")";
            }

            $(moreFilterCountPlaceholder).html(moreFilterString);
        };

        var _updateProjectStatusPlaceholder = function(data){
            if (data.name != 'projectStatus' || projectStatusPlaceholder === null) {
                return;
            }
            var paramArray = data.value,
                paramLabel = data.label || paramArray,
                str = projectStatusPlaceholder.getAttribute('data-default'),
                length=paramArray && paramArray.length;
            if (length == 1) {
                str = paramLabel[0];
            } else if (length > 1) {
                str = paramLabel[0] + " (+" + (length - 1) + ")";

            }
            $(projectStatusPlaceholder).html(str);
        };

        function _switchTOListMapView(type) {

            let path = window.location.pathname,
                search = window.location.search,
                prefix = '/maps',
                changePath;

            if (pageData.isMap && type == 'list') {
                path = path.replace(prefix, '');
                changePath = true;
            } else if (!pageData.isMap && type == 'map') {
                path = `${prefix}${path}`;
                changePath = true;
            }

            if (changePath) {
                let newUrl = `${path}${search}`;
                urlService.ajaxyUrlChange(newUrl);
            }
            return;
        }

        function _setResetButtonStatus() {
            if(!resetPlaceholder) {
                return;
            }
            let filterConfig = filterConfigService.getSelectedQueryFilterKeyValObj();
            if($.isEmptyObject(filterConfig)) {
                $(resetPlaceholder).addClass('disabled');
            } else {
                $(resetPlaceholder).removeClass('disabled');
            }
        }


        function trackApplyFilters(newHref){
            // for(var key in applyFilters){
            //     applyFilters[key].sourceModuleId = moduleElement.id;
            //     context.broadcast('trackApplyFilters', applyFilters[key]);
            // }
            context.broadcast('trackApplyFilters', {name: "View Properties", sourceModuleId: moduleElement.id, newHref});
        }
         function trackMoreFilters(source){
            context.broadcast('trackMoreFilters', {name: "Filter Open", sourceModuleId: moduleElement.id, source: source});
        }
        var openFilterPopopInMobile = function({force} = {}){
            let fName = "addClass";
            if(!(force || !$('body').hasClass('filters-open'))){
                fName = "removeClass";
            }
            $('body')[fName]('filters-open');
            $(moduleElement).find('#f-container-mob').first()[fName]('posfix');
        };
        var closeFilterPopopInMobile = function(){
            $('body').removeClass('filters-open');
            $(moduleElement).find('#f-container-mob').first().removeClass('posfix');
        };
        function filterPopopInMobile(filterPopopAction){
            if(utilService.isMobileRequest()){
                filterPopopAction.apply({});
            }
        }
        function populateHTML({layoutTemplate, restart, overrideParams}, cb){
            let backButtonEnable = false;
            if(moduleConfig.showBackButtonInFilter && window.history && window.history.length > 2){
                backButtonEnable = true;
            }

            let showFilterClass = moduleConfig.showBackButtonInFilter ? 'hide' : '',
                showBackToDivClass   = backButtonEnable ? '' : 'hide';

            moduleElement.innerHTML = layoutTemplate({
                isMap: pageData.isMap,
                isProjectSerp: !!moduleConfig.needProjectUrl,
                showBackToDivClass,
                showFilterClass,
                backButtonText: backButtonEnable ? moduleConfig.backButtonText : '',
                mCards: moduleConfig.mCards,
                pageData,
                overrideParams,
                filterType:moduleConfig.filterType
            });

            if(typeof cb == "function"){
                cb();
            }

            // Set placeholders
            budgetPlaceholder = Box.DOM.queryData(moduleElement, DATA_TARGET, BUDGET_PLACEHOLDER);
            bedroomPlaceholder = Box.DOM.queryData(moduleElement, DATA_TARGET, BEDROOM_PLACEHOLDER);
            sellerRatingPlaceHolder = Box.DOM.queryData(moduleElement, DATA_TARGET, SELLER_RATING_PLACEHOLDER);
            propertyTypePlaceholder = Box.DOM.queryData(moduleElement, DATA_TARGET, PROPERTYTYPE_PLACEHOLDER);

            furnishingPlaceHolder = Box.DOM.queryData(moduleElement, DATA_TARGET, FURNISHTYPE_PLACEHOLDER);
            ageOfPropertyPlaceHolder = Box.DOM.queryData(moduleElement, DATA_TARGET, AGE_OF_PROPERTY_PLACEHOLDER);
            listedbyPlaceholder = Box.DOM.queryData(moduleElement, DATA_TARGET, LISTEDBY_PLACEHOLDER);
            sortPlaceholder = Box.DOM.queryData(moduleElement, DATA_TARGET, SORT_PLACEHOLDER);
            areaPlaceholder = Box.DOM.queryData(moduleElement, DATA_TARGET, AREA_PLACEHOLDER);
            floorPlaceholder = Box.DOM.queryData(moduleElement, DATA_TARGET, FLOOR_PLACEHOLDER);
            projectStatusPlaceholder = Box.DOM.queryData(moduleElement, DATA_TARGET, PROJECT_STATUS_PLACEHOLDER);
            moreFilterCountPlaceholder = Box.DOM.queryData(moduleElement, DATA_TARGET, MORE_FILTER_COUNT_PLACEHOLDER);
            resetPlaceholder = Box.DOM.query(moduleElement, '[data-type="'+ RESET_PLACEHOLDER + '"]');
            filterContainer = $(Box.DOM.query(moduleElement,`.${FILTER_CONTAINER_CLASS}`));
            if(moduleConfig.isRental){
                $(Box.DOM.query(moduleElement,`#rentid`)).attr('checked','checked');
            }
            if(restart){
                commonService.restartAllModules(moduleElement);
            }
        }

        function loadTemplate({layoutTemplate, restart, overrideParams}, cb){

            if(typeof layoutTemplate == 'function'){
                populateHTML({layoutTemplate, restart, overrideParams}, cb);
            } else {
                require([layoutTemplate], function(layoutTemplate){
                    populateHTML({layoutTemplate, restart, overrideParams}, cb);
                });
            }

        }

        function getTemplate(category){
            let layoutTemplate = layoutDesktop,
                isMobileRequest = utilService.isMobileRequest();
            if (moduleConfig.miniFilter) {
                layoutTemplate = isMobileRequest?layoutMiniMobile:layoutMiniDesktop;
                return layoutTemplate;
            }
            if(category == "buy" && isMobileRequest){
                layoutTemplate = layoutMobile;
            } else if(isMobileRequest){
                if(category == "commercialBuy") {
                    layoutTemplate = layoutMobileCommercial;
                } else if(category == "commercialLease") {
                    layoutTemplate = layoutRentMobileCommercial;
                } else {
                    layoutTemplate = layoutRentMobile;
                }
            } else if(category == "rent"){
                layoutTemplate = layoutRentDesktop;
            }
            return layoutTemplate;
        }
        function showSearchLoader() {
            $moduleEl.find(SEARCH_BTN_LOADER).removeClass('hide');
            $moduleEl.find(SEARCH_COUNT_PROPERTY).addClass('hide');
        }

        function hideSearchLoader() {
            $moduleEl.find(SEARCH_BTN_LOADER).addClass('hide');
            $moduleEl.find(SEARCH_COUNT_PROPERTY).removeClass('hide');
        }

        function _updateSearchCTA({listingCount}) {

            let searchCTA = `View ${listingCount ? listingCount :''} ${!listingCount || listingCount > 1 ? 'Properties' : 'Property'}`;
            $moduleEl.find(SEARCH_COUNT_PROPERTY).html(searchCTA)
        }

        function _getLocationData() {
            let selectedSearchTypeItems = locationData,
                selectedSearchTypeList = Object.keys(selectedSearchTypeItems),
                overrideParams = {};

            if (!selectedSearchTypeList.length) {
                return overrideParams;
            }
            searchService.abortAllPendingAPis();
            overrideParams = searchService.getCityLocalitySuburbOverrideData(selectedSearchTypeItems);
            return overrideParams;
        }


        function _getOverrideParams(){
            let url = tempHref || filterConfigService.getUrlWithUpdatedParams({makeProjectUrl: false}),
            overrideParams = urlService.getAllUrlParam(url);
            if (Object.keys(locationData).length) {
                let locationObj = _getLocationData();
                overrideParams = Object.assign(overrideParams, locationObj);
                overrideParams.localityOrSuburbId = (locationObj.suburbId || locationObj.localityId) ? locationObj.localityOrSuburbId : '';
            }
            overrideParams.cityId = overrideParams.cityId || utilService.getPageData('cityId');
            overrideParams.localityOrSuburbId = overrideParams.localityOrSuburbId || utilService.getPageData('localityId') || utilService.getPageData('suburbId');
            overrideParams.listingType = utilService.getPageData('listingType');
            overrideParams.pageType = utilService.getPageData('isMap') ? 'LISTINGS_PROPERTY_URLS_MAPS' : 'LISTINGS_PROPERTY_URLS';
            overrideParams.postedBy = filterConfigService.returnFilterSelectedValues('postedBy',overrideParams);
            return overrideParams;
        }

        function _updateListingCountInCTA() {
            let urlParams = {
                skipHref: true,
                skipPageData: true,
                overrideParams : _getOverrideParams()
            },
            queryString = filterConfigService.getUrlWithUpdatedParams(urlParams);
            showSearchLoader();
            searchService.getMultipleListingSERP(queryString).then(result => {
                hideSearchLoader();
                if (result) {
                    if (result.isOriginalSearchResult) {
                        _updateSearchCTA({
                            listingCount: result.totalCount
                        })
                    } else {
                        _updateSearchCTA({listingCount: 0});
                    }
                }
            })
        }

        return {
            messages: [
                'singleSelectOptionsInitiated', 'singleSelectOptionsChanged',
                'singleSelectDropdownInitiated', 'singleSelectDropdownChanged',
                'multiSelectOptionsInitiated', 'multiSelectOptionsChanged',
                'multiSelectDropdownInitiated', 'multiSelectDropdownChanged', 'multiSelectDropdownOpened', 'multiSelectDropdownClosed',
                'rangeInputDropdownInitiated', 'rangeInputDropdownChanged',
                'rangeSliderInitiated', 'rangeSliderChanged',
                'searchParamsChanged', 'userClicked', 'showMapView','updateOnlyHeaderBuyRentLabel',
                'open-filters', 'reset-filters', 'newUrlAfterSelection', 'filterTypeAheadUpdated', 'businessTypeChanged'
            ],

            behaviors: ['filterTracking'],

            /**
             * Initializes the module and caches the module element
             * @returns {void}
             */
            init: function() {
                moduleConfig = context.getConfig() || {};

                var layoutDesktopTemplateName = layoutDesktop,
                    layoutMobileTemplateName = layoutMobile,
                    isMobileRequest = utilService.isMobileRequest();
                if (pageData.listingType == 'rent') {
                    layoutDesktopTemplateName = layoutRentDesktop;
                    layoutMobileTemplateName = layoutRentMobile;
                }

                if(pageData.listingType == 'commercialBuy') {
                    layoutDesktopTemplateName = layoutCommercialDesktop;
                    layoutMobileTemplateName = layoutMobileCommercial;
                }

                if(pageData.listingType == 'commercialLease') {
                    layoutDesktopTemplateName = layoutCommercialRentDesktop;
                    layoutMobileTemplateName = layoutRentMobileCommercial;
                }

                if (moduleConfig.layoutDesktopTemplate) {
                    layoutDesktopTemplateName = moduleConfig.layoutDesktopTemplate;
                }

                if (moduleConfig.layoutMobileTemplate) {
                    layoutMobileTemplateName = moduleConfig.layoutMobileTemplate;
                }

                var layoutTemplateName = layoutDesktopTemplateName;
                if (isMobileRequest) {
                    layoutTemplateName = layoutMobileTemplateName;
                }
                if (moduleConfig.mCards) {
                    layoutTemplateName = layoutMcardsTemplateName;
                }
                if (moduleConfig.miniFilter) {
                    layoutTemplateName = isMobileRequest ? (pageData.isCommercial ? layoutMiniMobileCommercial : layoutMiniMobile) : (pageData.isCommercial ? layoutMiniDesktopCommercial : layoutMiniDesktop);
                }

                require([layoutTemplateName], function(layoutTemplate) {
                    loadTemplate({layoutTemplate});
                    context.broadcast('moduleLoaded', {
                        'name': 'filter',
                        'id': moduleElement.id
                    });
                    commonService.startAllModules(moduleElement);
                });

                if(isMobileRequest){
                    $(moduleElement).on('click', '.js-sortby-wrap', function(){
                        let sortByParent = $(moduleElement).find('.js-ddsortby');
                        sortByParent.toggleClass("show-ddsortby");
                        if(sortByParent.hasClass("show-ddsortby")){
                            context.broadcast("trackSortByClick", {name: "Sort Open", sourceModuleId: moduleElement.id});
                        }
                    });
                    $(moduleElement).on('click', '.js-buy-rent-wrap', function(){
                        let buyRentParent = $(moduleElement).find('.js-ddbuyrentwrap');
                        buyRentParent.toggleClass("show-ddsortby");
                    });
                }else{
                    $(moduleElement).on('mouseenter', '.js-sortby-wrap', function(){
                        $(moduleElement).find('.js-ddsortby').addClass('show-ddsortby'); //js-ddsortby // show-ddsortby
                    });
                }


                $(moduleElement).on('mouseleave', '.js-sortby-wrap', function(){
                    $(moduleElement).find('.js-ddsortby').removeClass('show-ddsortby');
                });
                if(moduleConfig.mCards || moduleConfig.miniFilter){
                    moreFiltersActiveFlag = true;
                }


            },

            onclick: function(event, element, elementType) {
                switch (elementType) {
                    case "listing-category-switch":
                        let category = $(element).val();
                        _resetFilters();
                        utilService.setPageData("listingType", category);
                        overrideParams = $.extend(true, {}, {listingType: category});
                        tempHref = filterConfigService.getUrlWithUpdatedParams({makeProjectUrl: moduleConfig.needProjectUrl, overrideParams});
                        let layoutTemplate = getTemplate(category);
                        loadTemplate({layoutTemplate, restart: true, overrideParams: {showExpanded: true}}, () => {
                            _reinitFilters();
                            if(utilService.isMobileRequest()){
                                openFilterPopopInMobile({force: true});
                                _updateListingCountInCTA();
                            }
                        });
                        break;
                    case PROPERTY_AREA_WRAPPER:
                        _toggleClass(PROPERTY_AREA_WRAPPER);
                        // to stop triggering of userClicked event
                        event.stopPropagation();
                        break;
                    case PROPERTY_FLOOR_WRAPPER:
                        _toggleClass(PROPERTY_FLOOR_WRAPPER);
                        event.stopPropagation();
                        break;
                    case 'goto-back-history':
                        window.history.go(-1);
                        break;
                    case 'filters-changed':
                        context.broadcast('filters-changed');
                        break;
                    case 'more-filters':
                        let typeAheadElementId = moduleElement.querySelector('[data-Module="typeAhead"]');
                        if(typeAheadElementId){
                            context.broadcast("getRecentlySelectedLocation", {
                                moduleEl: typeAheadElementId
                            });
                        }
                        _updateListingCountInCTA();
                    case 'cancel-more-filters':
                    case 'apply-filters':
                        var expandDivClass = $(element).data('expanddivclass');
                        var collapseDivClass = $(element).data('collapsedivclass');
                        $('.' + collapseDivClass).addClass('hide');
                        $('.' + expandDivClass).removeClass('hide');

                        if (elementType == 'more-filters') {
                            moreFiltersActiveFlag = true;
                            tempHref = window.location.href;
                            trackMoreFilters(event.customData && event.customData.source);
                        } else if (elementType == 'apply-filters') {
                            let forceUpdate = $(element).data('force-update');
                            trackApplyFilters(tempHref);
                            context.broadcast("filterLocationUpdated", {
                                forceUpdate,
                                tempHref,
                                searchModuleId: moduleElement.querySelector('[data-Module="search"]').id,
                                typeaheadModuleId: moduleElement.querySelector('[data-Module="typeAhead"]').id
                            });
                        } else {
                            moreFiltersActiveFlag = false;
                            tempHref = undefined;
                            applyFilters = {};
                            utilService.setPageData('listingType', tempListingType);
                            let layoutTemplate = getTemplate(tempListingType);
                            loadTemplate({layoutTemplate, restart: true}, () => {
                                _reinitFilters();
                            });
                        }
                        filterPopopInMobile(openFilterPopopInMobile);
                        break;
                    case 'reset-filters':
                        context.broadcast('trackResetFilter', {
                            sourceModuleId: moduleElement.id
                        });
                        filterPopopInMobile(closeFilterPopopInMobile);
                        _resetFilters();
                        break;
                    case 'list-view':
                        if (pageData.isMap) {
                            context.broadcast('trackListViewClick', {
                                sourceModuleId: moduleElement.id
                            });
                        }
                        _switchTOListMapView('list');
                        break;
                    case 'map-view':
                        if (!pageData.isMap) {
                            context.broadcast('trackMapViewClick', {
                                sourceModuleId: moduleElement.id
                            });
                        }
                        _switchTOListMapView('map');
                        break;

                }
            },
            onchange: function(event){
                if(event.target.id === listingTypeIds.BUY_ID ){
                    context.broadcast('buy_rent_switch',{
                        listingType: 'buy'
                    });
                    context.broadcast('trackCategoryOptionClick', {
                        listingType: 'buy'
                    });
                } else if (event.target.id === listingTypeIds.RENT_ID){
                    context.broadcast('buy_rent_switch',{
                        listingType: 'rent'
                    });
                    context.broadcast('trackCategoryOptionClick', {
                        listingType: 'rent'
                    });
                } else if (event.target.id === listingTypeIds.COMMERCIAL_BUY_ID){
                    context.broadcast('buy_rent_switch',{
                        listingType: 'commercialBuy'
                    });
                    context.broadcast('trackCategoryOptionClick', {
                        listingType: 'commercialBuy'
                    });
                } else if (event.target.id === listingTypeIds.COMMERCIAL_LEASE_ID){
                    context.broadcast('buy_rent_switch',{
                        listingType: 'commercialLease'
                    });
                    context.broadcast('trackCategoryOptionClick', {
                        listingType: 'commercialLease'
                    });
                }
            },
            onmessage: function(name, data) {
                if(name === 'businessTypeChanged'){
                    let layoutTemplate = utilService.isMobileRequest() ? (pageData.isCommercial ? layoutMiniMobileCommercial : layoutMiniMobile) : (pageData.isCommercial ? layoutMiniDesktopCommercial : layoutMiniDesktop);
                    loadTemplate({layoutTemplate, restart: true}, () => {
                        _reinitFilters();
                    });
                    return;
                }
                else if(name === 'open-filters'){
                    $(moduleElement).find("[data-type=more-filters]").trigger("click", data);
                    return;
                }
                if(name==='reset-filters'){
                    if(data.trackEvent){
                        context.broadcast('trackResetFilter', {
                            sourceModuleId: moduleElement.id
                        }); 
                    }
                    filterPopopInMobile(closeFilterPopopInMobile);
                    _resetFilters(null,data.resetTempListingType);
                    return;
                }
                if(name==='updateOnlyHeaderBuyRentLabel'){
                    if(data.listingType){
                        $(Box.DOM.query(moduleElement,`#${data.listingType}id`)).attr('checked','checked');
                    }
                    return;
                }
                if(name === 'searchParamsChanged'){
                    // Initialise filters if back button is pressed
                    if (data && data.reinitModules) {
                        _reinitFilters();
                    }
                    return;
                }else if(name === 'userClicked'){
                    _removeClass(PROPERTY_AREA_WRAPPER);
                    _removeClass(PROPERTY_FLOOR_WRAPPER);
                    return;
                }else if (name === 'showMapView') {
                    _switchTOListMapView('map');
                    return;
                } else if(name == 'newUrlAfterSelection'){
                    setTimeout(function(){
                        urlService.ajaxyUrlChange(data.newUrl, data.data && data.data.forceUpdate);
                    });
                    return;
                }
                else if (name=='multiSelectDropdownOpened'){
                    let filterType = $(data.moduleElement).data('type');
                    if (filterType == 'furnishings' || filterType == 'restrictions') {
                        filterContainer.addClass(INHERIT_SCROLL_CLASS);
                    }

                }else if (name=='multiSelectDropdownClosed'){
                    let filterType = $(data.moduleElement).data('type');
                    if (filterType == 'furnishings' || filterType == 'restrictions') {
                        filterContainer.removeClass(INHERIT_SCROLL_CLASS);
                    }
                }

                let senderId = data && data.id ? data.id : '',
                    isChildModule = senderId ? moduleElement.querySelector('#' + senderId) : true;

                // if not broadcast by childeren module or an outside filter then don't listen for filters event
                if(!(isChildModule || data.outsidefilter)){
                    return;
                }

                // Updating placeholders
                if (data) {
                    switch (data.name) {
                        case 'beds':
                            _updateBedroomPlaceholder(data);
                            break;
                        case 'furnished':
                            _updateFurnishingPlaceholder(data);
                            break;
                        case 'postedBy':
                            _updatePostedbyPlaceholder(data);
                            break;
                        case 'propertyType':
                            _updatePropertyTypePlaceholder(data);
                            break;
                        case 'area':
                            _updateAreaPlaceholder(data);
                            break;
                        case 'floor':
                            _updateFloorPlaceholder(data);
                            break;
                        case 'sortBy':
                            _updateSortPlaceholder(data);
                            if($(moduleElement).find('.js-sortby-wrap')){
                                $(moduleElement).find('.js-sortby-wrap').trigger('mouseleave');
                            }
                            break;
                        case 'budget':
                            _updateBudgetPlaceholder(data);
                            break;
                        case 'projectStatus':
                            _updateProjectStatusPlaceholder(data);
                            break;
                        case 'ageOfProperty':
                            _updateAgeOfPropertyPlaceHolder(data);
                            break;
                    }
                    _updateMoreFilterCountPlaceholder(data);
                }


                switch (name) {
                    case 'multiSelectOptionsChanged':
                    case 'multiSelectDropdownChanged':
                    case 'singleSelectOptionsChanged':
                    case 'singleSelectDropdownChanged':
                    case 'rangeInputDropdownChanged':
                    case 'rangeSliderChanged':
                        var value = data.value;
                        if(moduleConfig.plotBhkFilter){
                            if(data.name == "propertyType"){
                                $(moduleElement).find(`[data-name=beds]`).removeAttr("checked");
                            } else if(data.name == "beds"){
                                $(moduleElement).find(`[data-name=propertyType]`).removeAttr("checked");
                            }
                        }
                        if(data.moreFiltersActiveFlag){
                            moreFiltersActiveFlag = data.moreFiltersActiveFlag;
                        }
                        if(moreFiltersActiveFlag){
                            if(data.disableFilter){
                                delete applyFilters[data.disableFilter];
                            }
                            applyFilters[data.name] = data;
                            tempHref = tempHref && (tempHref==window.location.href) ? filterConfigService.getUrlWithUpdatedParams({makeProjectUrl: moduleConfig.needProjectUrl, overrideParams}): tempHref;
                        }else {
                            tempHref = filterConfigService.getUrlWithUpdatedParams({makeProjectUrl: moduleConfig.needProjectUrl});
                        }
                        tempHref = urlService.removeUrlParam('page', tempHref, true);
                        if (!value || value === '') {
                            tempHref = urlService.removeUrlParam(data.name, tempHref, moreFiltersActiveFlag);
                        } else {
                            tempHref = urlService.changeUrlParam(data.name, value.toString(), tempHref, moreFiltersActiveFlag);
                        }
                        if(data.disableFilter){
                            tempHref = urlService.removeUrlParam(data.disableFilter, tempHref, moreFiltersActiveFlag);
                        }
                        data.sourceModuleId = moduleElement.id;
                        context.broadcast('trackFilterChange', data);

                        // update filter on jarvis filter update
                        if(data.outsidefilter && data.name && ["beds","budget","propertyType", "assist"].indexOf(data.name) > -1){
                            let filterElement = moduleElement.querySelector("[data-type='"+data.name+"']").parentElement;
                            commonService.stopAllModules(filterElement);
                            commonService.startAllModules(filterElement);
                        }
                        _updateListingCountInCTA();
                        break;
                    case 'filterTypeAheadUpdated':
                        locationData = data.selectedItems;
                        _updateListingCountInCTA();
                        break;
                }

                _setResetButtonStatus();
            },

            /**
             * Destroys the module and clears references
             * @returns {void}
             */
            destroy: function() {
                $(moduleElement).off();
                moreFiltersActiveFlag = null;
                tempHref = null;
                budgetPlaceholder = null;
                bedroomPlaceholder = null;
                sellerRatingPlaceHolder = null;
                sortPlaceholder = null;
                areaPlaceholder = null;
                floorPlaceholder = null;
                moreFilterMap = {};
            }
        };
    });
});
