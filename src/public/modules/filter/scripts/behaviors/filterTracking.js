define([
    'common/trackingConfigService',
    'services/trackingService',
    'services/urlService',
    'services/filterConfigService'
], function(t, trackingService, urlService, filterConfigService) {
    'use strict';
    Box.Application.addBehavior('filterTracking', function(context) {

        var messages = [
            'trackListViewClick',
            'trackMapViewClick',
            'trackFilterChange',
            'trackResetFilter',
            'trackApplyFilters',
            'trackMoreFilters',
            'trackSortByClick'
        ];

        var moduleEl;

        var onmessage = function(eventName, data) {
            let event = t.NAVIGATION_EVENT,
                category = t.SERP_VIEW_CATEGORY,
                label,
                name,
                filterConfig,
                filterGAKey,
                filterGAValue;

            if(moduleEl.id != data.sourceModuleId){
                return;
            }

            let properties = {};

            switch (eventName) {
                case 'trackListViewClick':
                    label = t.LIST_VIEW_LABEL;
                    break;

                case 'trackMapViewClick':
                    label = t.MAP_VIEW_LABEL;
                    break;

                case 'trackApplyFilters':
                    category = t.FILTER_CATEGORY;
                    event = t.FILTERS_APPLIED_LABEL;
                    name = t.APPLIED_NAME;
                    properties.href = data.newHref;

                    // event = data.name;
                    // filterConfig = filterConfigService.getFilterModuleConfig(data.name);
                    // filterGAKey = filterConfig.gaKey || t.LABEL_KEY;

                    // filterGAValue = data.value;
                    // if (data.value && typeof(data.value) == 'object') {
                    //     filterGAValue = data.value.join();
                    // }
                    // properties[filterGAKey] = filterGAValue;
                    break;
                case 'trackSortByClick':
                    category = t.FILTER_CATEGORY;
                    event = t.SORT_OPEN;
                    label = t.SORT_BY;
                    break;
                case 'trackMoreFilters':
                    category = t.FILTER_CATEGORY;
                    event = t.FILTER_OPEN;
                    name = data.source || t.APPLIED_NAME;
                    break;

                case 'trackFilterChange':
                    category = t.FILTER_CATEGORY;
                    event = data.name;

                    filterConfig = filterConfigService.getFilterModuleConfig(data.name);
                    filterGAKey = filterConfig.gaKey || t.LABEL_KEY;

                    filterGAValue = data.value;
                    if (data.value && typeof(data.value) == 'object') {
                        filterGAValue = data.value.join();
                    }
                    name = t.SELECTED_NAME;
                    if(data.isInline) {
                        name = t.QUICK_FILTER_NAME;
                    } else if(!data.selected){
                        filterGAValue = data.unselectedValue;
                        name = t.UNSELECTED_NAME;
                    }
                    properties[filterGAKey] = t.findMapType(filterGAValue,filterGAKey) || filterGAKey;
                    break;

                case 'trackResetFilter':
                    category = t.FILTER_CATEGORY;
                    event = t.RESET_EVENT;
                    break;
            }

            properties[t.CATEGORY_KEY] = category;
            if (label) {
                properties[t.LABEL_KEY] = label;
            }
            properties[t.VALUE_KEY] = parseInt(urlService.getUrlParam('page')) || 1;
            properties[t.NAME_KEY] = name;

            trackingService.trackEvent(event, properties);
        };

        return {
            messages: messages,
            onmessage: onmessage,
            init: function() {
                moduleEl = context.getElement();
            },
            destroy: function() {}
        };
    });
});
