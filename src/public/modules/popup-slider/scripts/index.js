'use strict';
define([
	"doT!modules/popup-slider/views/partner-with-makaan",
	"doT!modules/popup-slider/views/partner-with-makaan-mobile",
	"services/commonService",
	"modules/popup-slider/scripts/services/popupSliderTrackingService"
	], (partnerWithMakaanTemplate, partnerWithMakaanMobileTemplate, commonService, popupSliderTrackingService) => {
	let MODULE_NAME = "popup-slider";

	Box.Application.addModule(MODULE_NAME, (context) => {
		let moduleEl, moduleConfig;

		let config = {
			groupTemplateMapping: {
				"partner-with-makaan": partnerWithMakaanTemplate,
				"partner-with-makaan-mobile": partnerWithMakaanMobileTemplate
			},
			SELECTORS: {
				TEMPLATE_PLACEHOLDER: "[data-placeholder]"
			}
		};

		function _openPopupSlider(data){
			commonService.stopAllModules(moduleEl);
			popupSliderTrackingService.trackPopupSliderOpened(data);
			let template = config.groupTemplateMapping[data.group];
			data.indexes = `${data.indexes}`.split(",");
			let html = template(data);
			$(moduleEl).find(config.SELECTORS.TEMPLATE_PLACEHOLDER).html(html);
			commonService.startAllModules(moduleEl);
			context.broadcast("popup:open", {
				id: moduleConfig.popupId
			});
		}

		function init(){
			moduleEl = context.getElement();
			moduleConfig = context.getConfig();
			context.broadcast("moduleLoaded", {
				name: MODULE_NAME,
				id: moduleEl.id
			});
		}

		function destroy(){
			moduleEl = moduleConfig = null;
		}

		let onmessage = {
			"open-popup-slider": _openPopupSlider
		};


		return {
			init,
			destroy,
			onmessage
		};
	});
});