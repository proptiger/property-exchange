'use strict';
define([
	"services/trackingService",
	"common/trackingConfigService"
], (trackingService, t) => {

	const SERVICE_NAME = "popupSliderTrackingService";

	Box.Application.addService(SERVICE_NAME, function(){

		const eventNameMap = {
			"partner-with-makaan": {
				event: t.VIEWED_SAMPLE,
				category: t.SELLER_BENEFITS_CATEGORY,
				label: t.BENEFITS_LABELS
			}
		};

		function _getProperties(eventData = {}, data = {}){
			let obj = {
				[t.CATEGORY_KEY]: (typeof eventData.category == "object") ? eventData.category[data.groupType] : eventData.category,
				[t.LABEL_KEY]: (typeof eventData.label == "object") ? eventData.label[data.groupType] : eventData.label
			};
			return obj;
		}

		function trackPopupSliderOpened(data){
			let eventData = eventNameMap[data.group];
			let event = eventData.event;
			let properties = _getProperties(eventData, data);
			trackingService.trackEvent(event, properties);
		}

		return {
			trackPopupSliderOpened
		};
	});

	return Box.Application.getService(SERVICE_NAME);
});