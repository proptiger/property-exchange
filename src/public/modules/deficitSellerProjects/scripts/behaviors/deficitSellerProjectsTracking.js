define([
    'services/trackingService',
    'common/trackingConfigService',
    'services/utils',
    'services/commonService'
], function (trackingService, t, utils, commonService) {
    'use strict';
    const BEHAVIOR_NAME = 'deficitSellerProjectsTracking';
    Box.Application.addBehavior(BEHAVIOR_NAME, (context) => {
        let moduleEl,moduleConfig, $ , sellersTracked = [];
        const SELLER_IN_VIEW_MESSAGE = 'deficitSellersInViewPort';

        //function for building the trackingObj and calling the trackEvent of tracking Service
        let _trackEvent = function (event, label,trackingData={}) {
            var trackingObj = {};
            trackingObj[t.CATEGORY_KEY] = 'Deficit Campaign Sellers';
            trackingObj[t.LABEL_KEY] = label;
            trackingObj[t.SELLER_ID_KEY] = trackingData.sellerUserId||undefined;
            trackingObj[t.PROJECT_ID_KEY] = trackingData.projectId||undefined;
            trackingObj[t.NON_INTERACTION_KEY] = trackingData.nonInteraction;
            trackingObj[t.SECTION_CLICKED] = trackingData.sectionClicked||undefined;
            trackingObj[t.SOURCE_MODULE_KEY] = trackingData.sourceModule || undefined;
            trackingService.trackEvent(event, trackingObj);
        };
        return {
            messages: ['deficitSellerCardClicked', 'deficitConnectClicked', 'serpPageScrolled', 'deficitSellersRendered', SELLER_IN_VIEW_MESSAGE,'topDeficitProjectRendered','topDeficitProjectCardClicked'],
            init: function (){
                moduleEl = context.getElement()||{};
                moduleConfig = context.getConfig();
                $ = context.getGlobal('jQuery');
            },
            destroy: function () {
                let container = moduleEl.querySelector("[deficit-placeholder]");
                let elements = container &&  container.querySelectorAll(".js-deficit-card");
                if(elements && elements.length){
                    commonService.unobserveDomElement(elements);
                }
                container = elements = sellersTracked = null;
                moduleEl = null;
            },
            onmessage: function (msgName, data) {
                let sellerUserId = data && data.dataset && data.dataset.sellerUserId || '';
                if(msgName ==SELLER_IN_VIEW_MESSAGE && moduleConfig.viewType=="projectSellerView"){
                    let dataElements = data.elements || [],target,dataset;
                        dataElements.forEach(element => {
                            target = element.target || element;
                            dataset = $(target).data()||{};
                            if ($(target).hasClass('js-deficit-card') && dataset && sellersTracked.indexOf(dataset.sellerUserId) == -1){
                                sellersTracked.push(dataset.sellerUserId);
                                _trackEvent('Card seen', `card_seen_${dataset.sellerUserId}`);
                            }
                        });
                }else{
                    if(!data || (moduleEl.id != data.moduleId)){
                        return;
                    }
                    switch(msgName) {
                        case 'deficitSellerCardClicked':
                             data.sectionClicked = 'deficitCarouselProjectClick';
                            _trackEvent('Redirect to Project page', `card_clicked_${sellerUserId}`,data);
                            break;
                        case 'deficitConnectClicked':
                            data.sectionClicked = 'deficitCarouselConnectNow';
                            _trackEvent('Open lead form', `lead_form_${sellerUserId}`,data);
                            break;
                        case 'deficitSellersRendered':
                            let container = moduleEl.querySelector("[deficit-placeholder]");
                            let elements = container.querySelectorAll(".js-deficit-card");
                            utils.trackElementInView(elements,".js-deficit-card", '',SELLER_IN_VIEW_MESSAGE);
                            break;
                        case 'topDeficitProjectRendered':
                            data.nonInteraction = 1;
                            _trackEvent('Card seen', 'Top deficit project',data);
                            break;
                        case 'topDeficitProjectCardClicked':
                                if(data.dataset){
                                    data.dataset.sectionClicked = 'topDeficitProject';
                                }
                            _trackEvent('Card Click', 'Top deficit project',data && data.dataset);
                            break;
                    }
                }
            }
        };
    });
});