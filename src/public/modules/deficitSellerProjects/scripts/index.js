/**
 * This is a module used for rendering projects of campaign seller with lead deficiency on serp mobile. It has following:
 * 1. deficitSellerProjects service -- which is used for fetching the deficit sellers along with their projects by doing api call (Randomization is used to pick a seller on random basis if more than 1 seller exists in a project)
 *    and providing the data to module to render in a template.
 * 2. deficitSellerProjectsTracking -- behavior used for tracking various actions or behaviors taken on the cards.
 * 
 * JIRA :  MAKAAN-4279
 */
define([
    'modules/deficitSellerProjects/scripts/services/deficitSellerProjects',
    'modules/deficitSellerProjects/scripts/behaviors/deficitSellerProjectsTracking',
    'doT!modules/deficitSellerProjects/views/index',
    'doT!modules/deficitSellerProjects/views/deficitProject',
    'services/utils',
    'services/commonService',
    'css!styles/css/components/lead-deficit-top-slot'
], function (deficitSellerService, deficitSellerProjectsTracking, deficitSellerProjectTemplate,deficitProjectTemplate, utils, commonService){
    const MODULE_NAME = "deficitSellerProjects";
    Box.Application.addModule(MODULE_NAME, (context) => {
        'use strict';

        let moduleEl, $, $moduleEl, moduleConfig, moduleData = { projectList: [], sellerMap:{} };

        const config = {
            "placeholder":"[deficit-placeholder]"
        };
        const SIMILAR_MODULE_ID = "similarPropDeficit";

        /* function to get the project and seller data to render the template. */
        function getLeadDeficitSellersProject() {
            deficitSellerService.getDeficitSellerProjects().then(response => {
                moduleData.projectList = response.projects;
                moduleData.sellerMap = response.sellerDataMap;
                $moduleEl.find(config.placeholder).html(deficitSellerProjectTemplate(response));
                commonService.startAllModules(moduleEl);
                context.broadcast("deficitSellersRendered", {moduleId: moduleEl.id});
            }, () => {});
        }

        function broadcastProjectRenderedMessage(response){
           if(moduleEl.id == "serpTopSlotDeficit"){
                commonService.bindOnModuleLoad(MODULE_NAME, ()=>{
                    context.broadcast('topDeficitCardRendered', {projectId:response && response.projectId || ''});
                },[SIMILAR_MODULE_ID]);
            } 
        }

         /* function to get the project data to render the template. */
        function getLeadDeficitProject(renderedProjectId) {
            let templateData={};
            deficitSellerService.getLeadDeficitProject(renderedProjectId).then(response => {
                if(!utils.isEmptyObject(response)){
                    templateData.deficitProject = response;
                    templateData.isMobileDevice = utils.isMobileRequest();
                    $moduleEl.find(config.placeholder).html(deficitProjectTemplate(templateData));
                    commonService.startAllModules(moduleEl);
                    context.broadcast("topDeficitProjectRendered", {moduleId: moduleEl.id,projectId:response.projectId,sellerUserId:response.projectSeller,sourceModule:utils.getPageData('moduleName')||''});
                }else{
                    $moduleEl.find(config.placeholder).html('');
                }
                broadcastProjectRenderedMessage(response);
            }, () => {
                    $moduleEl.find(config.placeholder).html('');
                    broadcastProjectRenderedMessage({});
            });
        }

        function _getSellerData({sellerUserId} = {}) {
            return moduleData.sellerMap[sellerUserId];
        }

        function _getProjectData({projectId} = {}) {
            return moduleData.projectList.find(item => {
                return (item.projectId == projectId);
            });
        }
        /* function to prepare config object for lead form on serp page.
            @param {object} dataset : module configration object.
            @param {object} moduleData : module's global object holding the current set of projects and sellers.
            @return {object} : config object for lead form.
        */
        function _getLeadFromData(dataset = {}) {
            let sellerData = _getSellerData(dataset);
            let projectData = _getProjectData(dataset);
            console.log(sellerData, projectData);
            let rawData = $.extend(true, {}, sellerData) || {};
            rawData = $.extend(rawData, dataset);
            delete rawData.sellerUserId;
            if (dataset.step) {
                rawData.step = dataset.step;
            }
            if (projectData.propertyTypes){
                rawData.propertyTypeLabel = projectData.propertyTypes;
            }
            rawData.projectName = projectData.projectName;
            rawData.localityId = projectData.localityId;
            rawData.localityName = projectData.localityName;
            rawData.cityId = projectData.cityId;
            rawData.cityName = projectData.cityName;
            rawData.imageUrl = rawData.companyImage;
            rawData.configurationText = '';
            rawData.priceRanges = {};
            if (projectData.price){
                if(projectData.price.min){
                    rawData.priceRanges.minPrice = {
                        "val": projectData.price.min.split(' ')[0],
                        "unit":projectData.price.min.split(' ')[1]
                    }
                }
                if(projectData.price.max){
                    rawData.priceRanges.maxPrice = {
                        "val": projectData.price.max.split(' ')[0],
                        "unit": projectData.price.max.split(' ')[1]
                    }
                }
            }
            if (projectData.unitTypeIds) {
                rawData.propertyType = projectData.unitTypeIds;
            }
            if (projectData.maxBudget && projectData.minBudget) {
                rawData.maxBudget = projectData.maxBudget;
                rawData.minBudget = projectData.minBudget;
            } else {
                rawData.budget = projectData.maxBudget = projectData.minBudget;
            }
            rawData.bhk = projectData.bhk;
            rawData.salesType = utils.getPageData('listingType');
            rawData.id = "lead-popup";
            rawData.moduleId = moduleEl.id;
            return rawData;
        }

        function _openLeadForm(dataset = {}) {
            let rawData = _getLeadFromData(dataset);
            context.broadcast('leadform:open', rawData);
        }


        return {
            behaviors: ['deficitSellerProjectsTracking'],
            messages:  ['topDeficitCardRendered'],
            init: function () {
                moduleEl = context.getElement();
                $ = context.getGlobal('jQuery');
                $moduleEl = $(moduleEl);
                moduleConfig = context.getConfig();
                if(moduleConfig.viewType=="projectSellerView"){
                    getLeadDeficitSellersProject();
                }else if(moduleEl.id != SIMILAR_MODULE_ID || moduleConfig.zeroListingpage){
                    getLeadDeficitProject();
                }
                context.broadcast('moduleLoaded', {
                    name: MODULE_NAME,
                    id: moduleEl.id
                });
            },
            destroy: function () {
                moduleEl = $ = moduleEl = moduleConfig = moduleData = null;
            },
            onclick: function (event, elementClicked, elementType) {
                let dataset = elementClicked.dataset || {},overviewUrl;
                dataset = Object.assign(dataset,$(elementClicked).closest('.cardholder').data());
                event.stopPropagation();
                switch(elementType){
                    case 'openLeadForm':
                        context.broadcast('deficitConnectClicked', {
                            moduleId: moduleEl.id,
                            dataset
                        });
                        _openLeadForm(dataset);
                        break;
                    case 'list-element':
                        context.broadcast('deficitSellerCardClicked', {
                            moduleId: moduleEl.id,
                            dataset
                        });
                        overviewUrl = $(elementClicked).closest('.cardholder').data('overviewUrl');
                        window.location.href = overviewUrl;
                        break;
                    case 'navigateToProject':
                        dataset.sourceModule = utils.getPageData('moduleName')||'';
                        context.broadcast('topDeficitProjectCardClicked', {
                            moduleId: moduleEl.id,
                            dataset
                        });
                        overviewUrl = dataset.href;
                        if(utils.isMobileRequest()){
                            window.location.href = overviewUrl;
                        }else{
                            window.open(overviewUrl,'_blank');
                        }
                        break;
                }
            },
            onmessage(name, data) {
                switch(name){
                    case 'topDeficitCardRendered':
                        if(moduleEl.id==SIMILAR_MODULE_ID){
                            getLeadDeficitProject(data.projectId);
                        }
                        break;
                }
            }
        }
    });
});