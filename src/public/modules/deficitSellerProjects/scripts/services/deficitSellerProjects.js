'use strict';
define([
    'common/sharedConfig',
    'services/apiService',
    'services/utils',
    'services/filterConfigService',
    'services/leadPyrService'
], function (sharedConfig, apiService, utils, filterConfigService, leadPyrService){
    const SERVICE_NAME = 'deficitSellerProjects';
    Box.Application.addService(SERVICE_NAME, (context) => {

        function getDeficitSellerProjects() {
            let pageData = utils.getPageData() || {} ,propertyTypeIds = [];;
            if(pageData.listingType.toLowerCase()!="rent"){
                let query = {},
                    selectFilterkeyValObj = filterConfigService.getSelectedQueryFilterKeyValObj(),
                    propertyTypeConfig = filterConfigService.getFilterModuleConfig("propertyType").getTemplateData({
                        listingType: "buy"
                    });
                query.localityIds = pageData.localityId || '',
                query.suburbIds = pageData.suburbId ||'';
                
                if (pageData.isSerp && ["suburb", "suburbTaxonomy"].indexOf(pageData.pageLevel) > -1) {
                    delete (query.localityIds);
                } else if (pageData.isSerp && ["locality", "localityTaxonomy"].indexOf(pageData.pageLevel) > -1) {
                    delete (query.suburbIds);
                } else if (pageData.isSerp && ["city"].indexOf(pageData.pageLevel) > -1){
                    delete (query.localityIds);
                    delete (query.suburbIds);
                }

                let _data = {
                    cityId: pageData.cityId,
                };

                if (selectFilterkeyValObj.beds) {
                    _data.bhk = selectFilterkeyValObj.beds.split(",");
                }
                // handling bhk for propertyPage
                if(!selectFilterkeyValObj.beds && pageData.bhk !=undefined){
                    let plusOneBhk = parseInt(pageData.bhk) + 1;
                    _data.bhk = [pageData.bhk.toString(),plusOneBhk.toString()];
                }
                
                if (selectFilterkeyValObj.budget || pageData.budget) {
                    query.budget = (selectFilterkeyValObj.budget && selectFilterkeyValObj.budget.split(","))|| pageData.budget.toString().split(",") ;
                }

                if (["rent", "rental"].indexOf(pageData.listingType) > -1) {
                    propertyTypeConfig = filterConfigService.getFilterModuleConfig("propertyType").getTemplateData({
                        listingType: "rent"
                    });
                }
                if (selectFilterkeyValObj.propertyType) {
                    _data.propertyTypes = selectFilterkeyValObj.propertyType.split(",");
                    propertyTypeIds = [];
                    _data.propertyTypes.forEach(elem => {
                        let _id = leadPyrService.getPropertyIdFromLabel(elem, propertyTypeConfig);
                        if (_id) {
                            propertyTypeIds.push(_id);
                        }
                    });
                    if (propertyTypeIds.length) {
                        query.unitType = propertyTypeIds;
                    }
                }
                // handling property type in case of property Page
                if(!selectFilterkeyValObj.propertyType){
                    _data.propertyTypes = pageData.unitType && pageData.unitType.toString().split(",")||[];

                    if(_data.propertyTypes.indexOf("Apartment")== -1 ){
                        _data.propertyTypes.push('Apartment');
                    }

                    propertyTypeIds = [];
                    _data.propertyTypes.forEach(elem => {
                        let _id = elem && sharedConfig.unitTypeToIdMapping[elem.toLowerCase()];
                        if (_id) {
                            propertyTypeIds.push(_id);
                        }
                    });
                    if (propertyTypeIds.length) {
                        query.unitType = propertyTypeIds;
                    }
                }

                if (_data.bhk) {
                    let index = _data.bhk.indexOf("3plus");
                    let _bhk = $.extend(true, [], _data.bhk);
                    if (index >= 0) {
                        _bhk.splice(index, 1);
                        Array.prototype.push.apply(_bhk, ["4", "5", "6", "7", "8", "9", "10"]);
                    }
                    _bhk = _bhk.filter(n => n);
                    query.bhk = _bhk.join(",");
                }
                
                query.listingCategory = "Primary";
                query.rows = 10;
                query.listingSellerTransactionStatuses = "BUILDER_EXCLUSIVE_CAMPAIGN_SELLER,CAMPAIGN_SELLER";
                query.leadDeficitScore = "5,";
                query.cityId = _data.cityId;
                query.includeNearby = true;
                query.projectDbStatus="Active,ActiveInMakaan";

                return apiService.get(sharedConfig.apiHandlers.getLeadDeficitSellerProjects({
                    query
                }).url).then(response => {
                    return response;
                });
            }else{
                return new Promise((resolve, reject) => {
                    resolve({
                        projects:[],
                        sellerDataMap:{}
                    });
                }); 
            }
        }
        
        function getLeadDeficitProject(excludeProjectId){
            return getDeficitSellerProjects().then(response => {
                let shuffledProjects = utils.shuffleArray(response.projects);
                let deficitProject = {};
                if(shuffledProjects && shuffledProjects[0]){
                    deficitProject = shuffledProjects[0];
                    if(excludeProjectId==deficitProject.projectId){
                        if(shuffledProjects[1]){
                            deficitProject = shuffledProjects[1];
                        }else{
                            return {};
                        }
                    }
                    deficitProject.originalBackgroundImage = deficitProject.originalBackgroundImage ? `${deficitProject.originalBackgroundImage}?width=210&height=210`:'';
                    deficitProject.overviewUrl = `${deficitProject.overviewUrl}?sellerUserIds=${JSON.stringify([deficitProject.projectSeller])}`;
                }
                return deficitProject;
            });
        }
        return {
            getDeficitSellerProjects,
            getLeadDeficitProject
        };
    });
    return Box.Application.getService(SERVICE_NAME);
});