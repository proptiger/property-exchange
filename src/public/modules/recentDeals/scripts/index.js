"use strict";
define([
    'doT!modules/recentDeals/views/template',
    'services/apiService',
    'common/sharedConfig',
    'services/commonService',
], (template,apiService,sharedConfig,CommonService) => {
    Box.Application.addModule('recentDeals', (context) => {
        const SHIFT_CLASS='moveup',
              TOP_CLASS='top',
              FOCUS_CLASS='focus',
              BOTTOM_CLASS='bottom',
              WAITING_CLASS='waiting';

        var messages = [],
            behaviors = [],
            element, moduleId, $, $moduleEl, moduleConfig;

        function render(data){
            $moduleEl.find('.js-deals-scroll').html(template(data));
            CommonService.addImageObserver(element);
        }

        function getData(){
            return apiService.get(sharedConfig.apiHandlers.getRecentDeals(
                {
                    start:0,
                    rows:5
                }).url);
        }
        function showDeals(){
            getData().then((result)=>{
                if(result && result.data){
                    let dealsData = result.data.map((deal,index)=>{
                        let profilePic, buyerName, rating=deal.sellerScore, displayClass, locality;
                        if(deal.sellerLogoUrl){
                            profilePic = deal.sellerLogoUrl + '?width=130&height=100';
                        }
                        //show only valid names (which doesnot have numbers)
                        if(deal.buyer && deal.buyer.match(/^[a-zA-Z ]*$/)){
                            buyerName = deal.buyer;
                        }
                        //convert rating in decimal
                        if(typeof Number.isInteger==='function' && Number.isInteger(rating)){
                            rating = rating.toFixed(1);
                        }
                        if(deal.locality){
                            locality = deal.locality + ', ' + deal.city
                        } else {
                            locality = deal.city
                        }
                        switch(index){
                            case 0:
                                displayClass = SHIFT_CLASS;
                            break;
                            case 1:
                                displayClass = TOP_CLASS;
                            break;
                            case 2:
                                displayClass = FOCUS_CLASS;
                            break;
                            case 3:
                                displayClass = BOTTOM_CLASS;
                            break;
                            case 4:
                                displayClass = WAITING_CLASS;
                            break;
                        }
                        return {
                            profilePic: profilePic ,
                            buyerName: buyerName,
                            sellerName: deal.seller,
                            locality: locality,
                            date:  (new Date(deal.leadPaymentStatus.updatedAt).toDateString()).substr(4),
                            rating: rating,
                            displayClass: displayClass
                        }
                    })
                    if(dealsData && dealsData.length>0){
                        render({deals:dealsData})
                        setInterval(scrollUpList,3000)
                    }
                }
            },(error)=>{

            })
        }
        
        function scrollUpList(){
            let temp = $moduleEl.find('.' + SHIFT_CLASS).removeClass(SHIFT_CLASS);
            $moduleEl.find('.' + TOP_CLASS).removeClass(TOP_CLASS).addClass(SHIFT_CLASS);
            $moduleEl.find('.' + FOCUS_CLASS).removeClass(FOCUS_CLASS).addClass(TOP_CLASS);
            $moduleEl.find('.' + BOTTOM_CLASS).removeClass(BOTTOM_CLASS).addClass(FOCUS_CLASS);
            $moduleEl.find('.' + WAITING_CLASS).first().removeClass(WAITING_CLASS).addClass(BOTTOM_CLASS);
            temp.addClass(WAITING_CLASS);
        }
        function init() {
            //initialization of all local variables
            element = context.getElement();
            moduleId = element.id;
            $ = context.getGlobal('jQuery');
            moduleConfig = context.getConfig();
            $moduleEl = $(element);
            showDeals();
        }

        function destroy() {
            //clear all the binding and objects
        }

        function onmessage(name, data) {
            // bind custom messages/events
        }

        function onclick(event, element, elementType) {
            // bind custom messages/events
        }

        return {
            init,
            messages,
            behaviors,
            onmessage,
            onclick,
            destroy
        };
    });
});