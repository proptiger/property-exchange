"use strict";
define(['doT!modules/multipleDropdownsModule/views/index',
        'modules/multipleDropdownsModule/scripts/services/dropDownService',
        'services/commonService'
       ],
    (template) => {
        Box.Application.addModule("multipleDropdownsModule", function(context) {

            var moduleEl,
                dropdownsIds = [];
            const dropDownService = context.getService('dropDownService'),
                  commonService = context.getService('CommonService');

            var _notififyParentModuleLoaded = function() {
                context.broadcast('moduleLoaded', {
                     name: 'multipleDropdownsModule',
                     id: moduleEl.id
                 });
            };

            var _generateTemplate = function(data,template) {
                let htmlText = "";
                data.forEach(function(obj) {
                    htmlText += template(obj);
                });
                moduleEl.innerHTML = htmlText;
            };
            var init = function() {
                var dropdownsLoaded = 0;
                const dropdownsData = context.getConfig().dropdownTypes,
                      totalDropdown = dropdownsData.length;
                dropdownsIds =  dropdownsData.map(elem => elem.id);

                moduleEl = context.getElement();
                _generateTemplate(dropdownsData, template);
                commonService.startAllModules(moduleEl);

                commonService.bindOnModuleLoad('multipleSelectDropdown', (id) => {
                    dropdownsLoaded = dropdownsIds.indexOf(id) >= 0 ? dropdownsLoaded + 1 : dropdownsLoaded;
                    if(totalDropdown === totalDropdown) {
                        _notififyParentModuleLoaded();
                    }
                });
                commonService.bindOnModuleLoad('singleSelectDropdown', (id) => {
                    dropdownsLoaded = dropdownsIds.indexOf(id) >= 0 ? dropdownsLoaded + 1 : dropdownsLoaded;
                    if(totalDropdown === totalDropdown) {
                        _notififyParentModuleLoaded();
                    }
                });
            };

            var destroy = function() {
                moduleEl = null;
            };

            var onmessage =  function(name, data) {
                switch (name) {
                    case 'multiSelectDropdownChanged':
                    if(data && data.id && dropdownsIds.indexOf(data.id) >= 0 &&  data.name) {
                        if(!data.value) {
                            dropDownService.deleteDropdownsState(data.name);
                        } else {
                            dropDownService.setDropdownState(data.name,data.value);
                        }
                        let dropdownsState = dropDownService.getAllDropdownsState();
                        context.broadcast('dropdownsChanged',dropdownsState);
                    }
                    break;
                    case 'singleSelectDropdownChanged':
                    if(data && data.id && dropdownsIds.indexOf(data.id) >= 0 && data.name) {
                        if(!data.value) {
                            dropDownService.deleteDropdownsState(data.name);
                        } else {
                            dropDownService.setDropdownState(data.name,data.value);
                        }
                        let dropdownsState = dropDownService.getAllDropdownsState();
                        context.broadcast('dropdownsChanged',dropdownsState);
                    }
                    break;
                }
            };

            return {
                messages: ['multiSelectDropdownChanged','singleSelectDropdownChanged'],
                init,
                destroy,
                onmessage
            };
        });
    });
