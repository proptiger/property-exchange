"use strict";
define([
], function() {
    Box.Application.addService("dropDownService", function() {

      var dropdownStates = {};

      var getDropdowState = function(dropdownName){
          return dropdownStates[dropdownName] ;
      };

      var setDropdownState = function(dropdownName,dropdownValue){
            dropdownStates[dropdownName] = dropdownValue;
      };

      var deleteDropdownsState = function(dropdownName) {
          return delete dropdownStates[dropdownName];
      };

      var getAllDropdownsState = function() {
          return dropdownStates;
      };

      return {
           getDropdowState,
           setDropdownState,
           getAllDropdownsState,
           deleteDropdownsState
      };
    });
  });
