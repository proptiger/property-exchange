"use strict";
define([
], function() {
    Box.Application.addService('tabService', function() {


      var activeIndexes = {};

      var getInnerActiveTab = function(parentContainer){
          return activeIndexes[parentContainer] || 0;
      };

      var setInnerActiveTab = function(activeTabIndex,parentContainer){
            activeIndexes[parentContainer] = activeTabIndex;
      };

      return {
           getInnerActiveTab,
           setInnerActiveTab
      };
    });
  });
