/**
	 * Description : Tab Module used across website
	 * ul li must be of following format
	 *   <ul data-module="tabs" >
					 <li data-type="tab" data-target-id="{{target-id-1}}" data-target-tab="#{{target-container}}">
					 </li>
					 <li data-type="tab" data-target-id="{{target-id-2}}" data-target-tab="#{{target-container}}">
					 </li>
			 </ul>
			 <div id="{{target-container}}">
					<div id="{{target-id-1}}" class="js-tabs-pane"  data-parent="#{{target-container}}"></div>
					<div id="{{target-id-2}}" class="js-tabs-pane"  data-parent="#{{target-container}}"></div>
			 </div>
	 *
**/

"use strict";
define(['modules/tabs/scripts/services/service'], () => {
    Box.Application.addModule("tabs", function(context) {

        var moduleEl,
            config,
            $ = context.getGlobal('jQuery'),
            tabService = context.getService('tabService');
        const TARGET_ID_SELECTOR = "data-target-id",
            TARGET_TAB_SELECTOR = "data-target-tab",
            TARGET_INDEX_SELECTOR = "data-index",
            TARGET_DURATION_SELECTOR = "data-duration";

        /*
         * Setting current tab as active
         */
        var _setSelectedTab = function(element, selector) {
            var elems;
            elems = Box.DOM.queryAll(moduleEl, selector);
            if (typeof elems !== 'undefined') {
                for (let i = 0; i < elems.length; i++) {
                    elems[i].classList.remove("selected");
                }
            }
            element.className += " selected";
        };

        /*
         * Setting current tab container as active
         */
        var _showSelectedTabContainer = function(targetId, targetContainer) {
            var elems = $(targetContainer).find('[data-parent="' + targetContainer + '"]');
            if (typeof elems !== 'undefined') {
                for (let i = 0; i < elems.length; i++) {
                    if (elems[i].id === targetId) {
                        elems[i].classList.remove("hidden");
                    } else {
                        if (!elems[i].classList.contains("hidden")) {
                            elems[i].className += " hidden";
                        }
                    }
                }
            }
        };

        var init = function() {
            moduleEl = context.getElement();
            config = context.getConfig();
            context.broadcast('moduleLoaded', {
                name: 'tabs',
                id: moduleEl.id,
                moduleEl: moduleEl
            });
        };

        var destroy = function() {
            moduleEl = null;
            tabService = null;
            config = null;
        };
         var _broadcastTracking = function(data = {}) {
            context.broadcast('tabs_click_track', data);
        };

        function _switchTabElement(element, event) {
        	let selectedIndex, targetId, elem, targetContainer;
        	targetId = element.getAttribute(TARGET_ID_SELECTOR);
        	targetContainer = element.getAttribute(TARGET_TAB_SELECTOR);
        	if (targetId) {
        	    // Notifying tab click event
                let prevElement = $(moduleEl).find('.selected');
        	    context.broadcast('tabClicked', {
        	        element: element,
                    moduleEl: moduleEl,
                    prevElement: prevElement
        	    });
                if(event && event.originalEvent){
                    _broadcastTracking({element, moduleEl});
                }
        	    _setSelectedTab(element, 'li');
        	    if (targetContainer) {
        	        _showSelectedTabContainer(targetId, targetContainer);
        	        if (config && config.shared === false) {
        	            selectedIndex = tabService.getInnerActiveTab(targetId);
        	        } else {
        	            selectedIndex = tabService.getInnerActiveTab(targetContainer.substring(1));
        	        }
        	        elem = Box.DOM.query(targetContainer, "#" + targetId + " li[data-index='" + selectedIndex + "']");
        	        if (typeof elem !== 'undefined' && elem) {
        	            elem.click();
        	        }
        	    }
        	}
        }

       
        var onclick = function(event, element, elementType) {

            if (elementType === "tab") {
                _switchTabElement(element, event);
            } else if (elementType === 'chart' || elementType === 'pie') {
                let meta,
                    targetId = element.getAttribute(TARGET_ID_SELECTOR),
                    selectedIndex = element.getAttribute(TARGET_INDEX_SELECTOR),
                    selectedDuration = element.getAttribute(TARGET_DURATION_SELECTOR),
                    parentContainer = $(element).parents(".js-tabs-data").attr('id'),
                    immediateParentContainer = $(element).parents(".js-tabs-pane").attr('id');
                if (typeof selectedIndex !== 'undefined') {
                    if (config && config.shared === false) {
                        tabService.setInnerActiveTab(selectedIndex, immediateParentContainer);
                    } else {
                        tabService.setInnerActiveTab(selectedIndex, parentContainer);
                    }
                }
                context.broadcast('tabClicked', {
                    element: element
                });
                _broadcastTracking({element, moduleEl});
                _setSelectedTab(element, 'li');
                meta = $.extend(true, {}, config);
                meta.duration = selectedDuration;
                context.broadcast('renderChart', {
                    id: targetId,
                    type: elementType,
                    parent: parentContainer,
                    meta: meta
                });
            }
        };

        var onmessage = function(name, data) {
            switch (name) {
                case 'switchTabs':
                    if (data.moduleIdArray.indexOf(moduleEl.id) > -1) {
                        let targetElement = Box.DOM.query(moduleEl, `[data-category="${data.category}"]`);
                        if (targetElement) {
                            _switchTabElement(targetElement);
                        }
                    }
                    break;
                case 'switchTabByTargetId':
                    let targetElement = Box.DOM.query(moduleEl, `[data-target-id="${data.targetId}"]`);
                    if (targetElement) {
                        _switchTabElement(targetElement);
                    }
                break;
            }
        };


        return {
            init,
            destroy,
            onclick,
            onmessage,
            messages:['switchTabs','switchTabByTargetId']
        };
    });
});
