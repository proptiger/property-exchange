"use strict";
define([
    'services/commonService'
], (CommonService) => {
    Box.Application.addModule('downloadMobileApp', (context) => {
             
        var messages = [],
            moduleEl, $moduleEl, configData;  

        function init() {
            //initialization of all local variables
            moduleEl = context.getElement();
            $ = context.getGlobal('jQuery');
            $moduleEl = $(moduleEl);
            configData = context.getConfig();
        }

        function destroy() {
            //clear all the binding and objects
        }

        function onmessage() {
        }

        function onclick(event, element, elementType) {
            // bind custom messages/events
            let dataset = $(element).data() || {};
            switch (elementType) {
                case 'other-app':
                  if(dataset.url) {
                    CommonService.ajaxify(dataset.url);
                  }
            }
        }

        return {
            init,
            messages,
            onmessage,
            onclick,
            destroy
        };
    });
});
