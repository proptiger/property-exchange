"use strict";
define([
    'doT!modules/socialSharing/views/template',
    'common/sharedConfig',
    'modules/socialSharing/scripts/behaviors/socialSharingBehavior',
    'services/loggerService',
    'services/apiService',
    'modules/socialSharing/scripts/services/socialSharingService',
    'services/utils'
], (template, SharedConfig) => {
    Box.Application.addModule('socialSharing', (context) => {
        //const Logger = context.getService('Logger'),
          const  SocialSharingService = context.getService('SocialSharingService'),
            ApiService = context.getService('ApiService'),
            Utils = context.getService('Utils'),
            config = {
                selector: {
                    "CONTAINER": "[data-social-share-container]",
                    "SOCIAL_CONTENT_CONTAINER": "[data-social-content]",
                    "URLLINK": "[data-type=urlLink]"
                },
                wordcount: 100
            };
        var website_url = window.WEBSITE_URL || "https://www.makaan.com/"
        var messages = ['open_share_box'],
            behaviors = ['socialSharingBehavior'],
            moduleEl, configData, data = {};

        function _openPopup(id) {
            context.broadcast('popup:open', {
                id: id,
                options: {
                    config: {
                        modal: true
                    }
                }
            });
        }

        function init() {
            //initialization of all local variables
            moduleEl = context.getElement();
            configData = context.getConfig();
        }

        function _featureValidate() {
            data.allowCopyToClipboard = document.queryCommandSupported("copy");
        }

        function destroy() {
            //clear all the binding and objects
        }

        function _parseData(pageDetails = {}) {
            data.title = pageDetails.title || $("meta[name='og:title']").attr('content');
            data.image = pageDetails.image || $("meta[name='og:image:secure_url']").attr('content');
            data.description = pageDetails.description || $("meta[name='og:description']").attr('content') || '';
            data.description = data.description.substring(0, config.wordcount) + '...';
            data.location = pageDetails.url || window.location.href;
            data.FBAppId = configData.FBAppId;
        }

        function _clearData() {
            data = {};
            $(moduleEl).find(config.selector["SOCIAL_CONTENT_CONTAINER"]).html('');
        }

        function _makeAbsolute(url) {
            if (url && url.indexOf('http') == -1) {
                url = website_url + url;
            }
            return url;
        }

        function onmessage(name, messageData) {
            switch (name) {
                case "open_share_box":
                    _featureValidate();
                    data.isMobileRequest = Utils.isMobileRequest();
                    _openPopup("socilaSharing");
                    if (!messageData.url) {
                        messageData.url = window.location.pathname;
                    }
                    let pathNameRegex = /(.*?:\/\/)?.*?\//i;
                    ApiService.get(SharedConfig.apiHandlers.seoText({
                        query: {
                            url: (Utils.getVernacLanguage() ? Utils.modVernacUrl(messageData.url) : messageData.url).replace(pathNameRegex,''),
                            fields: 'meta'
                        }
                    }).url).then((response) => {
                        response = response && response.meta;
                        if(!response) return;
                        _parseData({
                            title: response["og:title"],
                            description: response["og:description"],
                            image: response["og:image:secure_url"] || '/new-logo.png',
                            url: messageData.url.replace(pathNameRegex,'')
                        });
                        data.location = _makeAbsolute(data.location);
                        $(moduleEl).find(config.selector["CONTAINER"]).html(template(data));
                        context.broadcast('socialSharingClicked', { id: moduleEl.id, data: data });
                    }, () => {
                        data.location = _makeAbsolute(data.location);
                        $(moduleEl).find(config.selector["CONTAINER"]).html(template(data));
                        context.broadcast('socialSharingClicked', { id: moduleEl.id, data: data });
                    });
                    $.extend(data, messageData);
                    break;
            }
            // bind custom messages/events
        }

        function onclick(event, element, elementType) {
            // bind custom messages/events
            switch (elementType) {
                case "Facebook-Share":
                case "Facebook-Feed":
                case "Facebook-Send":
                case "Twitter":
                case "Linkedin":
                case "Gplus":
                case "Email":
                case "Gmail":
                case "Whatsapp":
                    SocialSharingService.shareOn(elementType, data);
                    context.broadcast('socialSharedClicked', { id: moduleEl.id, type: elementType, data: data });
                    break;
                case "copyUrl":
                    let copyTargetArea = $(moduleEl).find(config.selector["URLLINK"]);
                    if (Utils.copyToClipboard(copyTargetArea)) {
                        $(element).text("copied");
                        $(element).addClass('btnv2-p');
                    } else {
                        $(element).text("copy url");
                    }
                    break;
                case "closePopup":
                    _clearData();
                    break;
            }
        }

        return {
            init,
            messages,
            behaviors,
            onmessage,
            onclick,
            destroy
        };
    });
});
