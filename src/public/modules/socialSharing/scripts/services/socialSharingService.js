'use strict';
define(['services/loggerService'], () => {
    Box.Application.addService('SocialSharingService', () => {
        //const Logger = application.getService('Logger');

        function shareOn(shareon, data) {
            var url = data.location || window.location.href;
            var title = data.title || document.title;
            switch (shareon) {
                case 'Facebook-Share':
                    window.open("https://www.facebook.com/dialog/share?app_id=" + data.FBAppId + "&display=popup&href=" + encodeURIComponent(url));
                    break;
                case 'Facebook-Feed':
                    window.open("https://www.facebook.com/dialog/feed?app_id=" + data.FBAppId + "&display=popup&caption=" + encodeURIComponent(title) + "&link=" + encodeURIComponent(url));
                    break;
                case 'Facebook-Send':
                    window.open("http://www.facebook.com/dialog/send?app_id=" + data.FBAppId + "&link=" + encodeURIComponent(url) + "&redirect_uri=https://www.makaan.com/");
                    break;
                case 'Gplus':
                    window.open("https://plus.google.com/share?url=" + url + " ?utm_source=googleplus&utm_medium=social&utm_campaign=sharing", "sharer?", "toolbar=0,status=0,width=574,height=436");
                    break;
                case 'Twitter':
                    window.open("https://www.twitter.com/home?status= " + encodeURIComponent(url + "?utm_source=twitter&utm_medium=social&utm_campaign=sharing") + "&t=" + encodeURIComponent(title), "sharer?", "toolbar=0,status=0,width=574,height=260");
                    break;
                case 'Linkedin':
                    window.open("http://www.linkedin.com/shareArticle?mini=true&url=" + encodeURIComponent(url + "?utm_source=linkedin&utm_medium=social&utm_campaign=sharing") + "&t=" + encodeURIComponent(title), "sharer?", "toolbar=0,status=0,width=750,height=436");
                    break;
                case 'Whatsapp':
                    window.open("whatsapp://send?text=" + encodeURIComponent(title) + " - " + encodeURIComponent(url + "?utm_source=whatsapp&utm_medium=social&utm_campaign=sharing"));
                    break;
                case 'Gmail':
                    window.open("https://mail.google.com/mail/u/0/?view=cm&fs=1&body=" + encodeURIComponent(data.description + "\nRead more..  " + url + "?utm_source=email&utm_medium=social&utm_campaign=sharing") + "&su=" + encodeURIComponent(title) + "&tf=1");
                    break;
                case 'Email':
                    window.open("mailto:?Subject=" + encodeURIComponent(title) + "&Body=" + encodeURIComponent(data.description + "\nRead more..  " + url + "?utm_source=email&utm_medium=social&utm_campaign=sharing"), "sharer?");
                    break;
            }
        }

        return {
            shareOn
        };
    });
});
