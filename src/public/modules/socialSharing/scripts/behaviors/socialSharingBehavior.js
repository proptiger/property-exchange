define([
    'common/trackingConfigService',
    'services/trackingService'
], function(t, trackingService) {
    'use strict';
    Box.Application.addBehavior('socialSharingBehavior', function(context) {

        var messages = ['socialSharedClicked', 'socialSharingClicked'],element;

        var onmessage = function(name, data = {}) {
            if (data.id !== element.id) {
                return;
            }
            let event, category, label, properties = {};
            switch (name) {
                case 'socialSharingClicked':
                    // label = data.type;
                    event = t.SHARED;
                    category = t.SHARE;
                    break;
                case 'socialSharedClicked':
                    label = data.type;
                    event = t.CLICK_EVENT;
                    category = t.SHARE;
                    break;
            }
            properties[t.CATEGORY_KEY] = category;
            properties[t.LABEL_KEY] = label;
            if(label) {
              label = label.indexOf("-") >=0 ? label.split("-")[0] : label;
              properties[t.SOCIAL_INTERACTION_KEY] = label;  
            }
            properties[t.SHARE_ITEM_NAME_KEY] = data.data.title;
            if (data.data) {
                data = data.data;
                properties[t.PAGE_KEY] = data.page;
                properties[t.URL_KEY] = data.url || window.location.href;
                properties[t.LISTING_CATEGORY_KEY] = t.makeValuesUniform(data.LISTING_CATEGORY_KEY,t.LISTING_CATEGORY_MAP) ;
                properties[t.PROJECT_STATUS_KEY] = t.makeValuesUniform(data.PROJECT_STATUS_KEY,t.PROJECT_STATUS_NAME) ;
                properties[t.BUDGET_KEY] = data.BUDGET_KEY;
                properties[t.UNIT_TYPE_KEY] = t.makeValuesUniform(data.UNIT_TYPE_KEY,t.UNIT_TYPE_MAP) ;
                properties[t.BHK_KEY] = t.makeValuesUniform(data.BHK_KEY,t.BHK_MAP) ;
                properties[t.SELLER_ID_KEY] = data.SELLER_ID_KEY;
                properties[t.EMAIL_KEY] = data.EMAIL_KEY;
                properties[t.PHONE_USER_KEY] = data.PHONE_USER_KEY;
                properties[t.VISITOR_CITY_KEY] = data.VISITOR_CITY_KEY;
                properties[t.LOCALITY_ID_KEY] = data.LOCALITY_ID_KEY;
                properties[t.CITY_ID_KEY] = data.CITY_ID_KEY;
                properties[t.LISTING_ID_KEY] = data.LISTING_ID_KEY;
                properties[t.PROJECT_ID_KEY] = data.PROJECT_ID_KEY;
                properties[t.BUILDER_ID_KEY] = data.BUILDER_ID_KEY;
                properties[t.LANDMARK_ID_KEY] = data.LANDMARK_ID_KEY;
                properties[t.SELLER_SCORE_KEY] = data.SELLER_SCORE_KEY;
                properties[t.LISTING_SCORE_KEY] = data.LISTING_SCORE_KEY;
                properties[t.SUBURB_ID_KEY] = data.SUBURB_ID_KEY;
                properties[t.ALLIANCE_ID_KEY] = data.ALLIANCE_ID_KEY;
                properties[t.NON_INTERACTION_KEY] = data.NON_INTERACTION_KEY;
            }

            if (event) {
                trackingService.trackEvent(event, properties);
            }
        };

        return {
            messages: messages,
            onmessage: onmessage,
            init: function() {
                element = context.getElement();
            },
            destroy: function() {}
        };
    });
});
