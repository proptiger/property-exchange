"use strict";
define([
    'common/sharedConfig',
    'services/utils',
    'services/apiService'
], function(sharedConfig, utils, apiService) {
    Box.Application.addService('sellerReviewsService', function() {

        let cachedData = {},
            companyInfo ={};
        const $ = Box.Application.getGlobal('jQuery');
        let getReviews = function(companyUserId, companyName){
            if(!companyUserId){
                return utils.getEmptyPromise();
            }
            companyInfo = {
                companyUserId, companyName
            }
            const cData = cachedData[companyUserId]
            if(cData){
                return utils.getEmptyPromise(Object.assign({},cData));
            }

            return apiService.get(sharedConfig.clientApis.getSellerFeedback(companyUserId).url).then((reviews)=>{
                reviews['companyName'] = companyName;
                cachedData[companyUserId] = Object.assign({},reviews);
                return reviews;
            });
        };
        let getLastSavedReview = function(companyUserId,companyName){
            return getReviews(companyUserId||companyInfo.companyUserId,companyName||companyInfo.companyName);
        }
        return {
           getReviews,
           getLastSavedReview
        };
    });
});
