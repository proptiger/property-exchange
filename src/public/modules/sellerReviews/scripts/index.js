"use strict";
define([
    'doT!modules/sellerReviews/views/template',
    'services/commonService',
    'services/utils',
    'modules/sellerReviews/scripts/services/sellerReviewsService',
    'modules/sellerReviews/scripts/behaviors/sellerReviewsTracking'
], (template,  commonService, utils) => {
    Box.Application.addModule('sellerReviews', (context) => {
        var messages = ['loadSellerReviews','popup:closed','loadSellerReviews:fromSummary','loadDetailedReviews'],
            behaviors = ['sellerReviewsTracking'],
            sellerReviewsService;
        var element, moduleId, $, $moduleEl, moduleConfig, reviewContainer;

        function render(reviews,appendHtml=true,extraData={}){
            let data = reviews, showDetailedReviews = !appendHtml ? true : moduleConfig.showDetailedReviews;
            if(data.reviewCount===0){
                return;
            }
            data.sellerName = reviews.companyName;
            data.avatar = utils.getAvatar(reviews.companyName);
            data.showDetailedReviews = showDetailedReviews;
            data.showHeadingWrap = !appendHtml ? true : moduleConfig.showHeadingWrap;
            data.source = !appendHtml ? '' : (moduleConfig.source||'');
            data.hideSellerHeader = !appendHtml ? true : false;
            data.showPrevious = !appendHtml ? true : false;
            data.allReviewsCount = data.feedbacks.length;
            data.isDealMaker = extraData.isDealMaker && extraData.isDealMaker.toLowerCase()=="true";
            data.isExpertDealMaker = extraData.isExpertDealMaker && extraData.isExpertDealMaker.toLowerCase()=="true";
            data.companyImage = extraData.companyImage;
            data.companyType = extraData.companyType;

            if(!showDetailedReviews){
                data.feedbacks = data.feedbacks.slice(0,moduleConfig.maxReviews||1);
                data.feedbacks = data.feedbacks.map((feedback)=>{
                    const COMMENT_MAX_LENGTH = 80;
                    let comments = feedback.comments, isTruncatedText=false;
                    if(feedback.comments.length>COMMENT_MAX_LENGTH){
                        comments = feedback.comments.substr(0,COMMENT_MAX_LENGTH);
                        isTruncatedText=true;
                    }
                    return Object.assign({},feedback, {
                        comments,
                        isTruncatedText,
                        highlights: [feedback.bestRatingHighlight]
                    });
                });
                data.autoplay = data.feedbacks.length>1 && !utils.isMobileRequest();
                $moduleEl.find(reviewContainer);
            } else {
                $moduleEl.find('.js-loader').addClass('hide');
            }
            let reviewHtml = template(data);
            if(!appendHtml)
                return reviewHtml;
            $moduleEl.find(reviewContainer).html(reviewHtml);
            commonService.startAllModules(element);

        }
        function ratingValueMap(value){
            if(value>4){
                return 'Great';
            }
            if(value>3){
                return 'Good';
            }
            if(value>2){
                return 'Satisfactory';
            }
            if(value>1){
                return 'Poor';
            }
        }

        function init() {
            //initialization of all local variables
            element = context.getElement();
            moduleId = element.id;
            $ = context.getGlobal('jQuery');
            moduleConfig = context.getConfig();
            $moduleEl = $(element);
            sellerReviewsService = context.getService('sellerReviewsService');
            context.broadcast('moduleLoaded', {
                name: 'sellerReviews',
                id: element.id
            });
            reviewContainer = moduleConfig.placeHolder ? moduleConfig.placeHolder : ".js-sellerReviews";
        }

        function destroy() {
            //clear all the binding and objects
        }

        function onmessage(name, data) {
            // bind custom messages/events
            switch(name){
                case 'loadSellerReviews':
                    if(!data.companyUserId || moduleConfig.showDetailedReviews || (data.id!=moduleId)){
                        return;
                    }
                    sellerReviewsService.getReviews(data.companyUserId, data.companyName).then((reviews)=>{
                        render(reviews,true,data);
                    });
                break;
                case 'loadSellerReviews:fromSummary':
                    if(data.id===moduleId){
                        return;
                    }
                    sellerReviewsService.getLastSavedReview(data.companyUserId, data.companyName).then((reviews)=>{
                        render(reviews,true,data);
                    });
                    let trackingData = Object.assign({},{
                        id: moduleId,
                        label: data.source||moduleConfig.source,
                        sellerId: data.companyUserId,
                    },data.otherInfo)
                    context.broadcast('trackFeedbackSeeAll',trackingData);
                break;
                case 'popup:closed':
                    $moduleEl.find('.js-loader').removeClass('hide');
                break;
                case 'loadDetailedReviews':
                    if(data.id!=moduleId){
                        return;
                    }
                    let sellerData = data.sellerData;
                    let detailedReviews ='';
                    sellerReviewsService.getReviews(sellerData.companyUserId, sellerData.companyName).then((reviews)=>{
                        detailedReviews = render(reviews,false,{});
                        context.broadcast('detailedReviewsLoaded',{detailedReviews, id: data.id});
                    });
                break;
            }
        }

        function onclick(event, element, elementType) {
            // bind custom messages/events
            switch(elementType){
                case 'more':
                    if(moduleConfig.showDetailedReviews){
                        return;
                    }
                    context.broadcast('popup:open',{
                        id: 'sellerReviewsPopup'
                    });
                    context.broadcast('loadSellerReviews:fromSummary',{
                        id : moduleId,
                        isDealMaker : moduleConfig.isDealMaker,
                        isExpertDealMaker : moduleConfig.isExpertDealMaker,
                        companyId: moduleConfig.companyId,
                        companyUserId: moduleConfig.companyUserId,
                        companyName: moduleConfig.companyName,
                        companyImage : moduleConfig.image,
                        companyType : moduleConfig.companyType,
                        source: moduleConfig.source,
                        showMoreRequest: true
                    });
                break;
            }
        }

        return {
            init,
            messages,
            behaviors,
            onmessage,
            onclick,
            destroy
        };
    });
});
