define([
    'common/trackingConfigService',
    'services/trackingService'
], function(t, trackingService) {
    'use strict';
    Box.Application.addBehavior('sellerReviewsTracking', function(context) {

        var element;
        const messages = [
            'trackFeedbackSeeAll'
        ];

        var init = function() {
            element = context.getElement();
        };

        var onmessage = function(name, data) {
            if (data.id === element.id) {
                let properties = {};
                let event,category,sourcemodule, label,sellerId, sellerRating, sellerStatus;
                sellerId = data.sellerId;
                sellerStatus = data.sellerStatus?t.SELLER_PAID_STATUS.PAID:t.SELLER_PAID_STATUS.NOT_PAID;
                sellerRating = data.sellerRating;
                category='reviews'
                switch (name) {
                    case 'trackFeedbackSeeAll':
                        event = t.CLICK_EVENT;
                        label = data.label;
                        break;
                    default:
                        return;
                }
                properties[t.CATEGORY_KEY] = category;
                if(label){
                    properties[t.LABEL_KEY] = label;
                }
                if(sellerId){
                    properties[t.SELLER_ID_KEY] = sellerId;
                }
                if(sellerRating){
                    properties[t.SELLER_SCORE_KEY] = sellerRating;
                }
                if(sellerStatus){
                    properties[t.SELLER_STATUS_KEY] = sellerStatus;
                }
                trackingService.trackEvent(event, properties);
            }
        };

        return {
            messages,
            onmessage,
            init,
            destroy: function() {}
        };
    });
});