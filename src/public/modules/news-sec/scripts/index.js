"use strict";

define([""
], function() {
    const HREF_ATTR = 'data-href',
        NEWS_DATA_TYPE = 'news-item';
    Box.Application.addModule('news-sec', function(context) {
        return {
            init: function() {
                $ = context.getGlobal('jQuery');
                moduleEl = context.getElement();
                $moduleEl = $(moduleEl);
            },
            destroy: function() {
                $ = null;
                moduleEl = null;
                $moduleEl = null;
            },
            onclick: function(event, element, elementType) {
                context.broadcast('elementClicked', {
                    element: element,
                    elementType: elementType
                });

                var hrefLoc = element.getAttribute(HREF_ATTR);
                if(elementType === NEWS_DATA_TYPE && hrefLoc) {
                    event.preventDefault();
                    window.location.href = hrefLoc;
                }
            }
        };

    });
});
