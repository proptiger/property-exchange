/**
 * @fileoverview Module for the wrapper around listing cards
 * @author [Aditya Jha]
 */

/*global Box*/

define([
    'services/commonService',
    'services/utils',
    'services/urlService',
    'services/filterConfigService',
    'common/sharedConfig',
    'services/experimentsService',
    'modules/pagination/scripts/index' , // to always load client side pagination module
    'modules/listingsWrapper/scripts/behaviors/listingsWrapperTracking'
], function(commonService, utils, urlService, filterConfigService, sharedConfig, experimentsService) {
    "use strict";
    Box.Application.addModule('listingsWrapper', function(context) {

        var listingsWrapper, listingCount = 0,
            INCLUDE_NEARBY_LOCALITIES = 'inlcude-nearby-localities',
            CALL_NOW_BUTTON = '[data-call-now]',
            INCLUDE_NEARBY_LOCALITIES_COOKIE = sharedConfig.includeNearbyLocalitiesCookieName,
            mapsAlvSidebarId = 'mapsAlvSidebar',
            serpListingWrapperClass = 'search-result-wrap',
            isZeroListingPage,
            PAGINATIONHOLDER = '[data-pagination-holder]',
            CARDHOLDER = '[data-cardholder]';

        var propertyVisibleData = {},pvTimer;

        const PROPERTY_VISIBLE_BATCH_REQUEST_TIME = 3000; //3 sec

        var _mergePropertyVisibleData = function(d) {
            // Note that this function assume that upstream data ready for merging contains only values of type string ,boolean or integer .
            let keys =  Object.keys(d),
                length = keys.length;
            for( let i = 0; i < length; i++) {
                let _key = keys[i];
                //Handling for setting zero for 0 image count
                if(_key === 'imageCount') {
                    d[_key] = d[_key] === 0 ? "zero" : d[_key];
                }
                if(_key ==="isSelectListing"){
                    d[_key] = d[_key] ? "Select Listing":"false";
                }
                if(propertyVisibleData.hasOwnProperty(_key) && propertyVisibleData[_key]) {
                    propertyVisibleData[_key] = (propertyVisibleData[_key] || "").toString() + "," + (d[_key]);
                } else {
                     propertyVisibleData[_key] = d[_key];
                }
            }
        };

        var _startPropertyVisibleTimer = function() {
            pvTimer = setInterval(() => {
                if(utils.isEmptyObject(propertyVisibleData)) {
                    return;
                }
                context.broadcast("trackPropertyVisible",propertyVisibleData);
                propertyVisibleData = {};
            },PROPERTY_VISIBLE_BATCH_REQUEST_TIME);
        };


        var _updateListings = function(listingsHtml,infinte) {
            var $listingsWrapper = $(listingsWrapper);
            if(infinte) {
                $listingsWrapper.find(PAGINATIONHOLDER).html($(listingsHtml).find(PAGINATIONHOLDER).html());
                $listingsWrapper.find(CARDHOLDER + ':last').after($(listingsHtml).find(CARDHOLDER));
                context.broadcast('moreListingsLoaded');
            } else {
                $listingsWrapper.html(listingsHtml);
                context.broadcast('listingsUpdated');
            }
            var nonInitListings = listingsWrapper.querySelectorAll("[data-module=listing]:not(.initialized)")
            nonInitListings.forEach(listing => {
                commonService.startAllModules(listing.parentNode, true);
            });
            if(!listingCount && utils.getPageData('isMap') && utils.isMobileRequest()){
                $(listingsWrapper).parent().addClass('moveup');
            }else {
                $(listingsWrapper).parent().removeClass('moveup');
            }
            utils.triggerPageInteractive();
            commonService.loadLazyModules(listingsWrapper);
        };

        var _updateMapListingWidth = function(count){
            if(utils.getPageData('isMap') && utils.isMobileRequest()){
                listingCount = count || listingCount;
                let listingsWrapperWidth = (280 * listingCount) + 120 ; // 280 is the width of each card on serp mobile map
                listingsWrapperWidth = isZeroListingPage ? listingsWrapperWidth + 300 : listingsWrapperWidth;
                let width = listingsWrapperWidth ? `${listingsWrapperWidth}px` : `100%`;
                $(listingsWrapper).find(`.${serpListingWrapperClass}`).css('width',width);
            }
        };

        var trackEmptyListingWrapper = function(){
            context.broadcast('trackZeroResultsFound');
        };

        function _getAllListing(){
            let listingIds = $(listingsWrapper).find('[data-module="listing"]').map(function(){
                return $(this).data("listing-id");
            });
            $(listingsWrapper).data('allListing', listingIds);
        }

        function _abTestParser(){
            if(window.AB_TEST && window.AB_TEST.callNowFree){
                $(listingsWrapper).find(CALL_NOW_BUTTON).html("Call Now Free");
            }
        }

        function _scrollToSelectedListing(){
            let urlData = urlService.getAllUrlParam();
            if (urlData.selectedListingId) {
                var selectedListing = document.querySelector(`div[data-listing-id="${urlData.selectedListingId}"]`);
                if(selectedListing){
                    selectedListing.scrollIntoView({behavior: "instant"});
                }
            }
        }

        return {
            messages: ['updateListings', 'mapClicked', 'markerClicked','propertyVisible'],
            behaviors: ['listingsWrapperTracking'],
            init: function() {
                if(utils.isMobileRequest()) {
                    experimentsService.switchElements(sharedConfig.propcard_experiment_key);
                }
                //let pageExtraData = utils.getPageExtraData();

                let moduleConfig = context.getConfig() || {},
                    totalCount = moduleConfig.totalCount;

                listingsWrapper = context.getElement();
                listingCount = moduleConfig.listingCount;
                isZeroListingPage = moduleConfig.isZeroListingPage;

                if(moduleConfig.hasOwnProperty('totalCount')){
                    context.broadcast('trackSerpListingTotalCount', { totalCount });
                }

                if(isZeroListingPage){
                    trackEmptyListingWrapper();
                }
                _startPropertyVisibleTimer();

                utils.deleteCookie('makaanUserPerPageCount'); // delete cookie as not useful anymore

                _updateMapListingWidth(listingCount);

                _getAllListing();

                _abTestParser();

                _scrollToSelectedListing();

            },
            destroy: function() {
                listingsWrapper = null;
                $(window).off('scroll');
                clearInterval(pvTimer);
            },
            onmessage: function(name, data) {
                if(name === 'updateListings') {
                    listingCount = data.listingCount;
                    if(data.isZeroListingPage){
                        trackEmptyListingWrapper();
                    }
                    _updateListings(data.propcardHtml,data.infinite);
                    _updateMapListingWidth(data.listingCount);
                    if(data && typeof data.callback === 'function'){
                        data.callback();
                    }
                    _getAllListing();
                    context.broadcast('trackSerpListingTotalCount', { totalCount: data.totalCount });
                    _abTestParser(); // used for ab testing
                }else if(name === 'markerClicked'){
                    $(`#${mapsAlvSidebarId}`).addClass('moveup');
                }else if(name === 'mapClicked'){
                    $(`#${mapsAlvSidebarId}`).removeClass('moveup');
                } else if (name === "propertyVisible") {
                    _mergePropertyVisibleData(data);
                }
            },
            onclick:function(event,element,elementType){
                let dataset=$(element).data();
                switch(elementType){
                    case INCLUDE_NEARBY_LOCALITIES:
                        let isChecked = $(element).find('input').is(':checked'),
                            v;

                        context.broadcast('trackNearbyLocalitiesClicked', { isChecked });

                        if(isChecked){
                            v = 1;
                            utils.setcookie(INCLUDE_NEARBY_LOCALITIES_COOKIE, true);
                        }else {
                            v = 0;
                            utils.setcookie(INCLUDE_NEARBY_LOCALITIES_COOKIE, false);
                        }
                        urlService.changeUrlParam('v',v); // adding this parameter to get updated url for current url as we don't hit same url in ajaxify
                        break;
                    case "knowMoreAbout":
                        if(dataset.trackType){
                            context.broadcast('trackKnowMoreAboutSerp', $.extend({trackType: dataset.trackType,linkName: dataset.linkName, linkType: dataset.linkType}, filterConfigService.getTrackPageFilters()));
                        }
                        break;
                    case "vernacSwitch":
                        context.broadcast('trackListingVernacSwitch');
                        if(dataset.url){
                            window.location.href = dataset.url;
                        }
                        break;
                }

            }
        };
    });
});
