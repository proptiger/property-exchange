define([
    'common/trackingConfigService',
    'services/trackingService',
    'services/urlService',
    'services/utils'
], function(t, trackingService, urlService, utils) {
    'use strict';
    Box.Application.addBehavior('listingsWrapperTracking', function() {

        var messages = [
            'trackNearbyLocalitiesClicked',
            'trackKnowMoreAboutSerp',
            'trackZeroResultsFound',
            'trackPropertyVisible',
            'moreListingsLoaded',
            'trackSerpListingTotalCount'
        ];

        var onmessage = function(msgName, data) {

            let category, event, label, projectStatus, budget, bhk, nonInteraction,linkName,linkType,value;
            data = data || {};

            switch (msgName) {
                case 'trackNearbyLocalitiesClicked':
                    category = t.NEARBY_SERP_CATEGORY;
                    event = t.NEARBY_SUGGESTIONS_EVENT;
                    label = t.UNSELECT_LABEL;
                    if (data.isChecked) {
                        label = t.SELECT_LABEL;
                    }
                    break;

                case 'trackKnowMoreAboutSerp':
                    category = t.HEADLINE_CATEGORY;
                    event = t.HEADLINE_SERP_EVENTS[data.trackType];
                    bhk = data.beds;
                    budget = data.budget;
                    projectStatus = data.projectStatus;
                    linkName = data.linkName;
                    linkType = data.linkType;
                    break;

                case 'trackZeroResultsFound':
                    category = t.ZERO_RESULTS_CATEGORY;
                    event = t.ERROR_EVENT;
                    nonInteraction = 1;
                    break;
                case 'trackPropertyVisible':
                    event = t.PROPERTY_VISIBLE_EVENT;
                    bhk = data.bedrooms;
                    nonInteraction = 1;
                    category = t.SERP_CARD_CATEGORY;
                    budget = data.budget || data.price;
                    break;

                case 'moreListingsLoaded':
                    category = t.NAVIGATION_EVENT;
                    event = t.NEXT_PAGE_EVENT;
                    label = t.AUTO_LOAD_LABEL;
                    nonInteraction = 1;
                    break;
                case 'trackSerpListingTotalCount':
                    category = t.LISTING_COUNT_CATEGORY;
                    event = t.LISTING_COUNT_EVENT;
                    label = data.totalCount;
                    nonInteraction = 1;
                    break;
                case 'trackListingVernacSwitch':
                    category = t.VERNAC_CATEGORY;
                    event = t.VERNAC_SWITCH;
                    label = data.label || "";
                    break;
            }

            let properties = {};
            properties[t.CATEGORY_KEY] = category;
            properties[t.LABEL_KEY] = label;
            properties[t.NON_INTERACTION_KEY] = nonInteraction;
            properties[t.VALUE_KEY] = parseInt(urlService.getUrlParam('page')) || 1;
            properties[t.RANK_KEY] = data.rank;
            properties[t.PROJECT_ID_KEY] = data.projectId  ? ((data.projectId.toString().split(",").length > 1 ) ? data.projectId : parseInt(data.projectId)) : undefined;
            properties[t.PROJECT_STATUS_KEY] = t.makeValuesUniform(data.projectStatus,t.PROJECT_STATUS_NAME) ;
            properties[t.BUILDER_ID_KEY] = data.builderId ? ((data.builderId.toString().split(",").length > 1 ) ? data.builderId : parseInt(data.builderId)): undefined;
            properties[t.BUDGET_KEY] = budget;
            properties[t.UNIT_TYPE_KEY] = t.makeValuesUniform(data.propertyType,t.UNIT_TYPE_MAP) ;
            properties[t.BHK_KEY] = bhk ? ((bhk.toString().split(",").length > 1 ) ? bhk : parseInt(bhk))  : undefined;
            properties[t.SELLER_ID_KEY] = data.sellerId || data.companyId;
            properties[t.SELLER_SCORE_KEY] = (data.sellerScore) ? ((data.sellerScore.toString().split(",").length > 1 ) ? data.sellerScore : parseFloat(data.sellerScore)) : undefined;
            properties[t.LOCALITY_ID_KEY] = data.localityId;
            properties[t.SUBURB_ID_KEY] = data.suburbId;
            properties[t.CITY_ID_KEY] = data.cityId;
            properties[t.LISTING_ID_KEY] = data.listingId;
            properties[t.LINK_NAME_KEY] = linkName && linkName.toLowerCase();
            properties[t.LINK_TYPE_KEY] = linkType && linkType.toLowerCase();
            properties[t.LISTING_SCORE_KEY] = data.listingScore;
            properties[t.SOURCE_MODULE_KEY] = 'Targetted';
            properties[t.RECENCY_KEY] = data.recency;
            properties[t.ADDITIONAL_CD49] = data.imageCount;
            value = utils.getTrackingListingCategory(data.listingCategory);
            properties[t.LISTING_CATEGORY_KEY] = t.makeValuesUniform(value,t.LISTING_CATEGORY_MAP) ;
            properties[t.SELECT_PROPERTY_KEY] = data.isSelectListing;
            
            trackingService.trackEvent(event, properties);
        };

        return {
            messages: messages,
            onmessage: onmessage,
            init: function() {},
            destroy: function() {}
        };
    });
});
