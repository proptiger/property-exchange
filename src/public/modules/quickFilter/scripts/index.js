'use strict';
define([
'doT!modules/quickFilter/views/index',
'services/filterConfigService',
'services/utils',
'services/urlService',
'services/experimentsService',
'services/commonService',
'common/trackingConfigService',
'services/trackingService',
'services/intersectionObserverService',
'modules/filter/scripts/behaviors/filterTracking',
'css!styles/css/quick-filter'
], function(template, filterConfigService, utils, urlService, experimentsService, commonService, t, trackingService, intersectionObserverService) {
	Box.Application.addModule('quickFilter', function(context) {
	 	var moduleEl,$moduleEl,config;
	 	const EXPERIMENT_NAME="makaan_quick_filters",
	 	ENABLED_VARIATION="enabled",
	 	QUERY_STRING_PARAM="inlineFilters",
	 	FILTER_IN_VIEW_MESSAGE="quick_filter_in_view";
	 	function init(){
            moduleEl = context.getElement();
	 		if(experimentsService.hasExperimentVariation(EXPERIMENT_NAME, ENABLED_VARIATION) || 
	 			urlService.getUrlParam(QUERY_STRING_PARAM) === "true"){
		 		config = context.getConfig() || {};
	            $moduleEl = $(moduleEl);
	            render();
	        } else {
	        	moduleEl.remove();
	        	commonService.stopModule(moduleEl);
	        }
	 	}

	 	function render(){
	 		filterConfigService.renderFilter(moduleEl, utils.getPageData(), template);

	 		if(moduleEl.offsetHeight > 110) {
	 			//Increase the width to accomodate only two rows
	 			$moduleEl.find(".container").addClass("xl-width");
	 		} else {
	 			//normal width to accomodate only two rows
	 			$moduleEl.find(".container").addClass("n-width");
	 		}

	 		if(intersectionObserverService.isObserverSupported) {
	 			//If intersection observer supported, card will be seen on init
	 			trackFilterSeen();	
	 		} else {
				utils.trackElementInView([moduleEl], `#${moduleEl.id}`, '', FILTER_IN_VIEW_MESSAGE);
	 		}
	 	}

	 	function trackFilterSeen() {
	 		let fiterName = moduleEl.dataset.type && moduleEl.dataset.type.replace("QuickFilter", ""),
	 		properties = {};

	 		properties[t.CATEGORY_KEY] = t.FILTER_CATEGORY;
	 		properties[t.NAME_KEY] = t.QUICK_FILTER_NAME;
	 		properties[t.LABEL_KEY] = fiterName;

	 		trackingService.trackEvent(t.CARD_SEEN, properties);
	 	}


	 	function destroy(){
	 		moduleEl = null;
	 		config = null;
	 	}

	 	function onclick(event, element, elementType){
	 		switch(elementType) {
	 			case "filter-link":
	 				let fiterName = element.dataset.name;
	 				let filterValue = element.dataset.value;
	 				context.broadcast('trackFilterChange', {
	 					sourceModuleId: moduleEl.id,
	 					name: fiterName,
	 					value: filterValue,
	 					isInline: true
	 				});
	 				let newUrl = urlService.changeUrlParam(fiterName, filterValue.toString(), null, true);
	 				urlService.ajaxyUrlChange(newUrl, false, false, true);
	 				break;
	 			case "open-filters":
	 				context.broadcast('open-filters', {
	 					source: "inline"
	 				});
	 				break;
	 		}
	 	}

	 	function onmessage(name, data) {
	 		if(name === FILTER_IN_VIEW_MESSAGE) {
	 			let elements = data.elements || [];
	 			elements.forEach(element => {
	 				let target = element.target || element;

	 				if(target === moduleEl) {
	 					trackFilterSeen();
	 				}
	 			});
	 		}
	 	}

	 	return {
	 		init,
	 		destroy,
	 		onclick,
	 		messages: [FILTER_IN_VIEW_MESSAGE],
	 		onmessage,
	 		behaviors: ['filterTracking']
	 	};
	});
});