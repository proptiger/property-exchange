define([
    'services/filterConfigService',
    'services/urlService',
    'services/utils',
    'services/viewStatus',
    'services/apiService',
    "common/sharedConfig",
    'services/commonService',
    'modules/project-card/scripts/behaviors/projectTracking'
], function(filterConfigService, urlService, utils, viewStatus, ApiService, sharedConfig, CommonService) {
    'use strict';
    Box.Application.addModule("project-card", function(context) {

        var element, moduleId, configData, projectId, propertyIds, mainImageId, rank,
            pageData, isMobileRequest, cityId, isFeatured;

        var _setViewStatus = function() {
            let status = viewStatus.getStatus({
                id: projectId,
                type: "project"
            });
            let domElement = $(element).find('[data-js-seen="' + projectId + '"]');
            if (status === 'contacted') {
                domElement.removeClass('hide');
                domElement.addClass('contacted');
                domElement.html('Already Contacted');
            } else if (status === 'seen') {
                domElement.removeClass('hide');
                domElement.addClass('seen');
                domElement.html('Seen');
            } else if (isFeatured=='true') {
                domElement.removeClass('hide');
                domElement.addClass('featured');
                domElement.html('featured');
            } else if (status === 'new') {
                domElement.removeClass('hide');
            }
        };

        function _openPopup({ id }) {
            context.broadcast('popup:open', {
                id: id,
                options: {
                    config: {
                        modal: true
                    }
                }
            });
        }

        function _getCityTopSellersApi() {
            return ApiService.get(sharedConfig.apiHandlers.getCityTopSellers({
                cityId,
                query: {
                    parser: "lead",
                    projectId: projectId
                }
            }).url).then((response) => {
                return response;
            }, () => {});
        }

        function _getLeadFormData(dataset) {
            return {
                cityId: dataset.cityid,
                localityId: dataset.localityid,
                projectId: dataset.projectid,
                companyId: dataset.companyId,
                companyName: dataset.companyName,
                companyPhone: dataset.companyPhone,
                companyRating: dataset.companyRating,
                companyImage: dataset.companyImage,
                companyAssist: dataset.companyAssist,
                companyUserId: dataset.companyUserId,
                companyType: dataset.companyType,
                projectName: dataset.projectname,
                builderId: dataset.builderid,
                imageUrl: dataset.projectimage,
                rank: dataset.rank,
                step: dataset.step,
                id: "lead-popup",
                moduleId: moduleId,
                otherCompany: dataset.otherCompany
            };
        }

        function _openLeadForm(dataset) {
            let rawData = _getLeadFormData(dataset);
            context.broadcast("leadform:open", rawData);
        }


        var _getSerpImagesApiUrl = function() {
            return sharedConfig.apiHandlers.getImages({
                objectType: "project",
                objectId: projectId,
                query: {
                    'projectId': projectId,
                    "propertyId": propertyIds,
                    // "mainImageId": mainImageId,
                    "isGallery": true
                }
            }).url;
        };

        function _getTackingData() {
            return {
                rank: configData.rank,
                projectId: configData.projectId,
                projectStatus: configData.projectStatus,
                builderId: configData.builderId,
                price: configData.price,
                propertyType: configData.propertyType,
                bedrooms: configData.bedrooms,
                sellerId: configData.sellerId,
                sellerScore: configData.sellerScore,
                localityId: configData.localityId,
                suburbId: configData.suburbId,
                cityId: configData.cityId,
                moduleId: moduleId
            };
        }

        var scroll = 0;
        var _trackScroll = function() {
            if (scroll) {
                return;
            }
            var minWindow = $(window).scrollTop(),
                maxWindow = minWindow + $(window).height(),
                minElement = $(element).offset().top,
                maxElement = minElement + $(element).height();
            if (maxElement < minWindow || minElement > maxWindow) {
                return;
            }
            context.broadcast('trackProjectVisible', _getTackingData());
            scroll = 1;
        };

        return {
            messages: ['view_status_changed'],
            behaviors: ['projectTracking'],

            /**
             * Initializes the module and caches the module element
             * @returns {void}
             */
            init: function() {
                element = context.getElement();
                moduleId = element.id;
                configData = context.getConfig() || {};
                projectId = configData.projectId;
                cityId = configData.cityId;
                propertyIds = configData.propertyIds;
                mainImageId = configData.mainImageId;
                rank = configData.rank;
                isFeatured = configData.isFeatured;
                pageData = utils.getPageData() || {};
                isMobileRequest = utils.isMobileRequest();

                context.broadcast('moduleLoaded', {
                    'name': 'project-card',
                    'id': element.id
                });
                _setViewStatus();

                _trackScroll();
                window.addEventListener('scroll', _trackScroll,{passive:true});
            },

            onmessage: function(name) {
                switch (name) {
                    case "view_status_changed":
                        _setViewStatus();
                        break;
                }
            },

            onclick: function(event, element, elementType) {
                let dataset = $(element).data();
                let url;
                switch (elementType) {
                    case 'social-share':
                        context.broadcast('open_share_box', {
                            url: dataset.url
                        });
                        break;
                    case 'openLeadForm':
                        _getCityTopSellersApi().then((response) => {
                            dataset = $.extend(dataset, response[0]);
                            response.shift();
                            if (response.length) {
                                response = response.splice(0, 4);
                            }
                            dataset.otherCompany = response;
                            context.broadcast('trackOpenLeadFormProjectSerp', _getTackingData());
                            _openLeadForm(dataset);
                        }, () => {
                            _openLeadForm(dataset);
                        });
                        break;

                    case "score-ring-popup":
                        let popupid = dataset.openid;
                        _openPopup({
                            id: popupid
                        });
                        break;

                    case 'image-div':
                        if (configData.imageCount) {
                            context.broadcast('trackCardImageClicked', _getTackingData());

                            let sellerPromise = _getCityTopSellersApi().then((response) => {
                                dataset = $.extend(dataset, response[0]);
                                response.shift();
                                if (response.length) {
                                    response = response.splice(0, 4);
                                }
                                dataset.otherCompany = encodeURI(JSON.stringify(response));
                                let leadFormData = _getLeadFormData(dataset);
                                leadFormData.price = dataset.data.project && dataset.data.project.price;
                                return leadFormData;
                            });
                            context.broadcast('start_gallery', {
                                "id": 'main_gallery_serp',
                                "dataSourceUrl": _getSerpImagesApiUrl(),
                                "url": dataset.data.project.overviewUrl,
                                "leadFormData": sellerPromise,
                                "data": dataset.data
                            });
                        }
                        break;
                    case 'project-card':
                        context.broadcast('trackFullCardClickProjectSERP', _getTackingData());
                        url = dataset.url;
                        url && window.open(url); //jshint ignore:line
                        break;
                    case 'navigate-project':
                        context.broadcast('trackProjectClickProjectSERP', _getTackingData());
                        event.stopPropagation();
                        break;
                    case 'navigate-locality':
                        context.broadcast('trackLocalityClickProjectSERP', _getTackingData());
                        url = dataset.url;
                        url && CommonService.ajaxify(url); //jshint ignore:line
                        event.stopPropagation();
                        break;

                    default:
                }
            },

            /**
             * Destroys the module and clears references
             * @returns {void}
             */
            destroy: function() {
                element = null;
                window.removeEventListener('scroll', _trackScroll,{passive:true});
            }
        };
    });
});
