define([
    'common/trackingConfigService',
    'services/trackingService',
    'services/urlService'
], function(t, trackingService, urlService) {
    'use strict';
    Box.Application.addBehavior('projectTracking', function(context) {

        var messages = [
            'trackProjectVisible',
            'trackOpenLeadFormProjectSerp',
            'trackCardImageClicked',
            'trackFullCardClickProjectSERP',
            'trackLocalityClickProjectSERP',
            'trackProjectClickProjectSERP',
            'trackCallNowSubmit',
            'trackProjectClickSERP',
            'trackLocalityClickSERP'
        ];

        var moduleId = context.getElement().id;

        var onmessage = function(msgName, data) {
            if (!data.moduleId || (data.moduleId && data.moduleId != moduleId)) {
                return;
            }

            let category = t.SERP_CARD_CATEGORY,
                event, label,noninteractive,
                leadLabelConfig = t.LEAD_LABELS,
                //leadSourceModuleConfig = t.LEAD_SOURCE_MODULES,
                sourceModule;

            switch (msgName) {

                case 'trackOpenLeadFormProjectSerp':
                    category = t.LEAD_FORM_CATEGORY;
                    event = t.OPEN_EVENT;
                    label = leadLabelConfig.ProjectSerp;
                    break;
                case 'trackProjectVisible':
                    event = t.PROPERTY_VISIBLE_EVENT;
                    noninteractive = 1;
                    break;
                case 'trackFullCardClickProjectSERP':
                    event = t.VIEW_DETAILS_EVENT;
                    label = t.FULL_CARD_LABEL;
                    break;
                case 'trackProjectClickProjectSERP':
                    event = t.VIEW_DETAILS_EVENT;
                    label = t.PROJECT_DETAILS_LABEL;
                    break;
                case 'trackLocalityClickProjectSERP':
                    event = t.VIEW_DETAILS_EVENT;
                    label = t.LOCALITY_DETAILS_LABEL;
                    break;
                case 'trackCardImageClicked':
                    event = t.VIEW_DETAILS_EVENT;
                    label = t.GALLERY_LABEL;
                    break;
                case 'trackCallNowSubmit':
                    category = t.LEAD_FORM_CATEGORY;
                    event = t.SUBMIT_EVENT;
                    label = leadLabelConfig['CallNow'];
                    break;
                case 'trackProjectClickSERP':
                    event = t.VIEW_PROJECT_EVENT;
                    break;

                case 'trackLocalityClickSERP':
                    event = t.VIEW_LOCALITY_EVENT;
                    break;

            }

            let properties = {};
            properties[t.CATEGORY_KEY] = category;
            properties[t.LABEL_KEY] = label;
            properties[t.NON_INTERACTION_KEY] = noninteractive;
            properties[t.VALUE_KEY] = parseInt(urlService.getUrlParam('page')) || 1;
            properties[t.RANK_KEY] = data.rank;
            properties[t.PROJECT_ID_KEY] = data.projectId ? parseInt(data.projectId) : undefined;
            properties[t.PROJECT_STATUS_KEY] = t.makeValuesUniform(data.projectStatus,t.PROJECT_STATUS_NAME) ;
            properties[t.BUILDER_ID_KEY] = data.builderId ? parseInt(data.builderId) : undefined;
            properties[t.BUDGET_KEY] = data.price;
            properties[t.UNIT_TYPE_KEY] = t.makeValuesUniform(data.propertyType,t.UNIT_TYPE_MAP) ;
            properties[t.BHK_KEY] = data.bedrooms ? parseInt(data.bedrooms) : undefined;
            properties[t.SELLER_ID_KEY] = data.sellerId;
            properties[t.SELLER_SCORE_KEY] = parseFloat(data.sellerScore) ? parseFloat(data.sellerScore) : undefined;
            properties[t.LOCALITY_ID_KEY] = data.localityId;
            properties[t.SUBURB_ID_KEY] = data.suburbId;
            properties[t.CITY_ID_KEY] = data.cityId;
            properties[t.SOURCE_MODULE_KEY] = sourceModule;

            trackingService.trackEvent(event, properties);
        };

        return {
            messages: messages,
            onmessage: onmessage,
            init: function() {},
            destroy: function() {}
        };
    });
});
