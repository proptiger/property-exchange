define([
    'common/trackingConfigService',
    'services/trackingService',
    'services/urlService',
    'services/utils'
], function(t, trackingService){
    "use strict";
    const BEHAVIOR_NAME = "galleryGridTracking";

    Box.Application.addBehavior(BEHAVIOR_NAME, function(context){
        let moduleEl, $, $moduleEl, config;

        return {
            messages:[
                "trackImageThumbnailClick",
                "trackThumbnailSeen",
                "trackOpenLeadFormGalleryGrid"
            ],
            init(){
                moduleEl = context.getElement();
                $ = context.getGlobal('jQuery');
                $moduleEl = $(moduleEl);
                config = context.getConfig();
            },
            onmessage(eventName, data){
                if (data && data.moduleId && data.moduleId != moduleEl.id) {
                    return;
                }
                let event = t.GALLERY_INTERACTION_EVENT,
                    category = t.GALLERY_CATEGORY,
                    label, name, sourceModule,
                    sourceModuleConfig = t.LEAD_SOURCE_MODULES,
                    properties = {};

                switch(eventName){
                    case "trackImageThumbnailClick":
                        label = `${data.categoryName}_Thumbnail`;
                        break;

                    case "trackThumbnailSeen":
                        event = t.GALLERY_OPEN_EVENT;
                        label = 'Thumbnail_Seen';
                        break;
                    case "trackOpenLeadFormGalleryGrid":
                        category = t.LEAD_FORM_CATEGORY;
                        label = 'gallery';
                        event = t.OPEN_EVENT;
                        if(data.source) {
                            sourceModule = `${sourceModuleConfig[data.source]}_thumbnail`;
                        }
                        break;
                }
                properties[t.CATEGORY_KEY] = category;
                properties[t.LABEL_KEY] = label;
                properties[t.ADDITIONAL_CD49] = data.totalImagesCount;
                properties[t.ADDITIONAL_CD53] = data.viewedImageCount;

                if(sourceModule) {
                    properties[t.SOURCE_MODULE_KEY] = sourceModule;
                }

                trackingService.trackEvent(event, properties);
            },
            destroy(){
                moduleEl = $ = $moduleEl = config = null;
            }
        }
    })
})