'use strict';

define([
    "text!modules/galleryGrid/views/index.html",
    "text!modules/galleryGrid/views/imageThumb.html",
    "text!modules/galleryGrid/views/imageList.html",
    'services/utils',
    'services/apiService',
    'services/commonService',
    'modules/gallery/scripts/services/galleryService',
    'css!styles/css/components/galleryGrid',
    'modules/galleryGrid/scripts/behaviors/galleryGridTracking',
], function (gridHtml, imageThumbTmpt, imageList, utils, apiService, commonService, galleryService){
        Box.Application.addModule('galleryGrid', function(context){
            const MAIN_LEAD_FORM_ID = "main_lead_form",
                moduleConfig = {
                    OUTER_LIST_CONTAINER: ".outer-ul",
                    OUTER_LIST: ".outer-li",
                    INNER_LIST_CONTAINER: ".inner-ul",
                    INNER_LIST: ".inner-li",
                    DATA_INNER_INDEX: "data-inner-index"
                },
                GALLERY_GRID_TIMEOUT = 16,
                minimumImageInCategoryCount = 6;

            let moduleEl,
                $moduleEl,
                $,
                config,
                imagesSourceUrl,
                isMobile = utils.isMobileRequest(),
                galleryData = {},
                parsedGalleryData = {},
                moduleData,
                previousHash,
                hashName = "grid",
                isOpen = false;

            function _startGallery(moduleName, moduleId){
                commonService.bindOnLoadPromise().then(() => {
                    context.broadcast('loadModule', {
                        "name": moduleName,
                        "loadId" : moduleId
                    });
                });
            }

            function handleURLHash(open = true, callbackAfterHashChange = null) {
                if(open){
                    previousHash = window.location.hash.substr(1);
                    var hashJoin = previousHash ? '&' : '';
                    var hashFinal = previousHash + hashJoin + hashName;
                    isOpen = true;
                    window.location.hash = hashFinal;
                } else {
                    isOpen = false;
                    if(window.location.hash.indexOf('grid') > -1){
                        window.history.back();
                    }
                    if (typeof callbackAfterHashChange === "function"){
                        setTimeout(callbackAfterHashChange, GALLERY_GRID_TIMEOUT);
                    }
                    
                }
                
            }

            function hideOnBrowserBack() {
                if(window.location.hash.indexOf("grid") > -1){
                } else if(isOpen){
                    isOpen = false;
                    destroyGallery();
                }
            }

            function onModuleLoad() {
                $(window).on("hashchange", hideOnBrowserBack)
            }

            function renderGallery(data = {}){
                imagesSourceUrl = data && data.dataSourceUrl || imagesSourceUrl;
                if(config.pageType == 'serp' || config.pageType == 'projectSerp'){
                    config.listingData = data.listingData;
                    config.leadFormData = data.leadFormData;
                    config.listingUrl = data.listingUrl;
                }
                $('html').addClass('grid-fullsize-on');
                $moduleEl.removeClass("hide");
                handleURLHash();
                if(config.pageType == 'projectSerp') {
                    getViewData({data}).then((dynamicHtml) => {
                        appendDynamicHtml(dynamicHtml, data);
                    });
                } else {
                    appendDynamicHtml(getViewData({data}), data);
                }
            }

            function appendDynamicHtml(dynamicHtml, data) {
                $moduleEl.append(dynamicHtml);
                initializeModule(data.id, data.startIndex); // startIndex is group start index(not image start index)
                if(isMobile){
                    context.broadcast("destroy_gallery", {
                        id: config.pageType == 'serp' ? "main_gallery_serp" : "main_gallery_desktop"
                    });
                }
            }

            function getImageCategory(startIndex){
                let imageCategory = 0, imageCategoryArray, imageCategoryName;
                for (let item in parsedGalleryData){
                    imageCategory += parsedGalleryData[item].length;
                    if(imageCategory >= startIndex){
                        imageCategoryArray = parsedGalleryData[item];
                        imageCategoryName = item;
                        break;
                    }
                }
                let startImageCategoryIndex = imageCategoryArray.findIndex(item => {
                    return item.index == startIndex;
                });
                return {
                    startImageCategoryIndex,
                    imageCategoryName   
                };
            }

            function renderStartImage(startImageCategoryIndex, imageCategoryName) {
                if (startImageCategoryIndex >= minimumImageInCategoryCount){
                    $moduleEl.find(`[data-category="${imageCategoryName}"]`).click();
                }
            }

            function initializeModule(moduleId, startIndex = 0){
                if(moduleId == config.id){
                    loadData().then(() => {
                        let imageThumbs = generateTemplate();
                        let { startImageCategoryIndex, imageCategoryName } = getImageCategory(startIndex);
                        let $galleryContainer = $moduleEl.find(".gallery-image-container");
                        $galleryContainer.html(imageThumbs);
                        renderStartImage(startImageCategoryIndex, imageCategoryName);
                        commonService.addImageObserver($galleryContainer);
                        let imageToFocus = $moduleEl.find(moduleConfig.INNER_LIST+"[data-index="+startIndex+"]");
                        $moduleEl.find(moduleConfig.INNER_LIST).removeClass("active");
                        if (imageToFocus && imageToFocus.length > 0){
                            imageToFocus.addClass("active");
                            let innerIndex = imageToFocus.attr(moduleConfig.DATA_INNER_INDEX);
                            innerIndex = innerIndex && parseInt(innerIndex, 10);
                            if(innerIndex && innerIndex >= minimumImageInCategoryCount) {
                                imageToFocus.get(0).scrollIntoView({
                                    block: "center"
                                });
                            } else {
                                imageToFocus.parents(moduleConfig.OUTER_LIST)[0].scrollIntoView();
                            }
                        }
                        context.broadcast("trackThumbnailSeen", {
                            moduleId: moduleEl.id
                        });
                    });
                }
            }

            function parseGalleryData(galleryData){
                let newModel = {},
                    groupIndex = 0,
                    groupIndexArray = [];

                galleryData && $.each(galleryData.group, (key, value) => {
                    groupIndex += parseInt(value, 10);
                    groupIndexArray.push({groupIndex: groupIndex, title:key, count: value});
                });
                groupIndexArray.forEach((item, index) => {
                    newModel[item.title] = [];
                    let startIndex;
                    if(index == 0){
                        startIndex = 0;
                    } else {
                        startIndex = groupIndexArray[index-1].groupIndex;
                    }
                    for(let i = startIndex; i < item.groupIndex; i++){
                        galleryData.data[i].index = i;
                        galleryData.data[i].count = item.count;
                        newModel[item.title].push(galleryData.data[i]);
                    }
                })
                return newModel;
            }

            function generateTemplate(){
                parsedGalleryData = parseGalleryData(galleryData);
                console.log("newModel", parsedGalleryData);
                let thumbTemplate = doT.template(imageThumbTmpt);
                let imageThumbs = thumbTemplate({ parsedGalleryData, minimumImageInCategoryCount});
                return imageThumbs;
            }

            /*
            Function to load dynamic service data
            */
            function loadData(){
                let promise = null;
                if(imagesSourceUrl){
                    promise = apiService.get(imagesSourceUrl).then((response)=>{
                        galleryData = config.galleryData = response;
                    });
                } else {
                    promise = new Promise((resolve) => {
                        galleryData = config.galleryData;//JSON.parse(galleryData);
                        resolve(galleryData);
                    });
                }

                return promise;
            }

            function getViewData({data}){
                var gridTemplate = doT.template(gridHtml);
                if(config.pageType == 'projectSerp') {
                    return galleryService.getViewData(config, data, isMobile)
                    .then(function(viewData) {
                        return gridTemplate(viewData);
                    });
                }
                var viewData = galleryService.getViewData(config, data, isMobile);
                if(viewData && viewData.leadBoxData && viewData.leadBoxData.leadConfigData) {
                    viewData.leadBoxData.leadConfigData.moduleId = moduleEl.id;
                }
                return gridTemplate(viewData);
            }

            function startOriginalGallery(imageIndex){
                let moduleId, galleryData;
                if(config.pageType == 'serp'){
                    moduleId = "main_gallery_serp";
                    galleryData = {
                        id: moduleId,
                        startIndex: imageIndex,
                        dataSourceUrl: imagesSourceUrl,
                        listingData: config.listingData,
                        leadFormData: config.leadFormData,
                        "url": config.listingUrl,
                        "serp":true,
                        version: 'v3_mobile'
                    }

                } else {
                    moduleId = "main_gallery_desktop";
                    galleryData = {
                        startIndex: imageIndex,
                        version: 'v3_mobile',
                        id: moduleId
                    };
                }

                if (!commonService.checkModuleLoaded('gallery', moduleId)) {
                    _startGallery('gallery', moduleId);
                }

                commonService.bindOnModuleLoad('gallery', function() {
                    context.broadcast('start_gallery',galleryData);
                }, [moduleId]);
            }

            function _openPopup(id) {
                context.broadcast('popup:open', {
                    id: id,
                    options: {
                        config: {
                            modal: true
                        }
                    }
                });
            }

            function _openLeadForm(dataset) {
                let rawData = {
                    cityId: dataset.cityid,
                    salesType: dataset.listingcategory,
                    listingId: dataset.listingid,
                    localityId: dataset.localityid,
                    projectId: dataset.projectid,
                    companyId: dataset.sellerid,
                    companyName: dataset.sellername,
                    companyPhone: dataset.sellerphone,
                    companyRating: dataset.sellerrating,
                    companyImage: dataset.sellerimage,
                    companyAssist: dataset.sellerassist,
                    companyUserId: dataset.selleruserid,
                    companyType: dataset.sellertype,
                    projectName: dataset.projectname,
                    projectOverviewUrl: dataset.projectoverviewurl,
                    propertyType: [dataset.propertytypeid],
                    builderId: dataset.builderid,
                    rank: dataset.rank,
                    bhk: dataset.bhk,
                    projectStatus: dataset.projectstatus,
                    budget: dataset.budget,
                    source: dataset.source,
                    step: dataset.step,
                    id: "lead-popup" ,
                    isMakaanSelectSeller:dataset.ismakaanselectseller||false
                };

                context.broadcast('Lead_Open_Step', rawData);
                _openPopup('leadOverviewPopup');
            }

            function destroyGallery(){
                $moduleEl.html('');
                $moduleEl.addClass("hide");
                $('html').removeClass('grid-fullsize-on');
            }

            return {
                behaviors: ["galleryGridTracking"],
                /**
                * Initializes the module and caches the module element
                * @returns {void}
                */
                init : function(){
                    $ = context.getGlobal('jQuery');
                    moduleEl = context.getElement();
                    $moduleEl = $(moduleEl);
                    config = context.getConfig();
                    imagesSourceUrl = config.imagesSourceUrl;
                    commonService = context.getService('CommonService');
                    context.broadcast('moduleLoaded', {
                        name: 'galleryGrid',
                        id: config.id || moduleEl.id
                    });
                    onModuleLoad();
                },
                /**
                * Destroys the module and clears references
                * @returns {void}
                */
                destroy: function() {
                    var configScript = $moduleEl.find('script[type="text/x-config"]');
                    $moduleEl.html(configScript);
                    $ = config = moduleEl = $moduleEl = galleryData = null;
                    $(window).off("hashchange")
                },
                onclick: function(event, element, elementType) {
                    let dataset = $(element).data();
                    switch(elementType){
                        case "close":
                            handleURLHash(false, destroyGallery);
                            break;
                        case "list-item":
                            let data = $(element).data();
                            context.broadcast("trackImageThumbnailClick", {
                                moduleId: moduleEl.id,
                                categoryName: data.title
                            });
                            handleURLHash(false, startOriginalGallery.bind(undefined, data.index));
                            break;
                        case 'openLeadForm':
                            _openLeadForm(dataset);
                            context.broadcast("trackOpenLeadFormGalleryGrid", {moduleId: moduleEl.id, source: dataset.source});
                            break;
                        case 'see-all':
                            let state = dataset.open;
                            $(element).data("open", !state);
                            let $imageContainer = $(element).closest(moduleConfig.OUTER_LIST).find(moduleConfig.INNER_LIST_CONTAINER);
                            let $imageThumbs = $imageContainer.find(moduleConfig.INNER_LIST);
                            if (!state){
                                //add image if not already added
                                if ($imageThumbs.length <= minimumImageInCategoryCount){
                                    let imageTmplt = doT.template(imageList);
                                    $imageContainer.append(imageTmplt({
                                        imageData: parsedGalleryData,
                                        category: dataset.category,
                                        startIndex: minimumImageInCategoryCount
                                    }));
                                    commonService.addImageObserver($imageContainer);
                                }
                                $(element).html("Hide All");
                                $imageThumbs.removeClass('hide');
                            } else {
                                // hide images
                                $(element).html("Show All");
                                const extraImages = $imageThumbs.slice(minimumImageInCategoryCount);
                                extraImages.each(item => $(extraImages[item]).addClass('hide'));
                            }
                            break;
                    }
                },
                messages: ["start_gallery", "destroyGrid"],
                onmessage: function(name, data){
                    switch(name){
                        case "start_gallery":
                            if(data.id !=moduleEl.id){
                                return;
                            }
                            renderGallery(data);
                            break;
                        case "destroyGrid":
                            if(data.id == moduleEl.id){
                                destroyGallery();
                            }
                            break;
                    }
                }
            }
        })
})