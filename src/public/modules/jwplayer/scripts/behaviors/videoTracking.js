define([
    'common/trackingConfigService',
    'services/trackingService'
], function(t, trackingService) {
    'use strict';
    Box.Application.addBehavior('videoTracking', function() {

        var messages = ['trackVideoComplete', 'trackVideoPlay', 'trackVideoPause'],
            category = t.VIDEO_CATEGORY,
            event, label, sourceName;

        var onmessage = function(name, data) {

            switch (name) {
                case 'trackVideoComplete':
                    event = t.CLICK_EVENT;
                    label = t.WATCH_FULL_LABEL;
                    sourceName = data.name;
                    break;

                case 'trackVideoPlay':
                    event = t.CLICK_EVENT;
                    label = t.PLAY_LABEL;
                    sourceName = data.name;
                    break;

                case 'trackVideoPause':
                    event = t.CLICK_EVENT;
                    label = t.PAUSE_LABEL;
                    sourceName = data.name;
                    break;
            }

            let properties = {};
            properties[t.CATEGORY_KEY] = category;
            properties[t.LABEL_KEY] = label;
            properties[t.NAME_KEY] = sourceName;

            if (event) {

                trackingService.trackEvent(event, properties);
            }
        };

        return {
            messages: messages,
            onmessage: onmessage,
            init: function() {},
            destroy: function() {}
        };
    });
});
