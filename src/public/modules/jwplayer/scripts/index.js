'use strict';
define([
    'services/utils',
    'jwPlayer',
    'modules/jwplayer/scripts/behaviors/videoTracking'
], function() {
    Box.Application.addModule('jwplayer', function(context) {
        var _moduleEl,
            dataset,
            _defaultConfig = {
                "width": "100%",
                "autostart": true,
                "aspectratio": "4:3",
                "base": "bower_components/jwplayer/"
            },
            _playerInstance,
            _playerId,
            falseTrigger = true,
            setFalseTrigger = function() {
                falseTrigger = true;
            },
            trackComplete = function() {
                context.broadcast('trackVideoComplete', { name: dataset.name });
            },
            trackPlay = function() {
                // let pageType = utils.getTrackPageCategory();
                if (falseTrigger) {
                    falseTrigger = false;
                    return;
                }
                context.broadcast('trackVideoPlay', { name: dataset.name });
                // utils.trackEvent(pageType + '_video_watch', 'Buyer_' + pageType, 'play');
            },
            trackPause = function() {
                // let pageType = utils.getTrackPageCategory();
                context.broadcast('trackVideoPause', { name: dataset.name });
                // utils.trackEvent(pageType + '_video_watch', 'Buyer_' + pageType, 'pause');
            },
            init = function() {
                _moduleEl = context.getElement();
                dataset = $(_moduleEl).data();
                $.extend(true, _defaultConfig, context.getConfig().playerConfig);
                let posterImage = Box.DOM.query(_moduleEl, '[data-poster]');
                if (posterImage) {
                    _defaultConfig.image = posterImage.getAttribute('src');
                }
                jwplayer.key = "cAr98kB89aA0ZhpVmJqMLQGxXGRUXKjW3Z9DlufTILI=";
                _playerId = context.getConfig().id;
                _playerInstance = jwplayer(_playerId);
                _playerInstance.setup(_defaultConfig);
                jwplayer(_playerId).onPlay(trackPlay);
                jwplayer(_playerId).onPause(trackPause);
                jwplayer(_playerId).onComplete(trackComplete);
                jwplayer(_playerId).onIdle(setFalseTrigger);
                if (_defaultConfig.openFullScreen) {
                    jwplayer(_playerId).onDisplayClick(function() { jwplayer(_playerId).setFullscreen(true); });
                }
            },
            
            
            destroy = function() {
                _moduleEl = null;
            },
            messages = ['pausePlayer'],
            onmessage = function(name) {
                if (name == 'pausePlayer') {
                    jwplayer(_playerId).pause(true);
                }
            },
            behaviors = ['videoTracking'];
        return {
            init,
            destroy,
            messages,
            onmessage,
            behaviors
        };
    });
});
