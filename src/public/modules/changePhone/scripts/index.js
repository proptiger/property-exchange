"use strict";
define(["text!modules/changePhone/views/mobile-input.html",
	"text!modules/changePhone/views/otp-form.html",
	"services/apiService",
	"services/loginService",
	"services/utils",
	"modules/newLoginRegister/scripts/registerBehavior"],function(mobileInputTemplate, otpFormTemplate){
	Box.Application.addModule('changePhone',function(context){
		var config, moduleEl, $, $moduleEl, $contactNumber, $errorMsg, OTPCode = '', configScript,
		hideOtpOnCall = false;
		var commonService, apiService,loginService;
		// var trackingService = Box.Application.getService('gaTracking');
		var utils = Box.Application.getService('Utils');
		// var controlsService = context.getService('controlsService');
		var trackingData = {
			mobileScreen: {
				action:'Enter_Mobile',
				category:'Login_SignUp_Seller',
				label:'Enter_Mobile'
			},
			otpScreen: {
				action:'Enter_OTP',
				category:'Login_SignUp_Seller',
				label:'Enter_OTP'
			},
			phonenumber:{
				action:'Warning_Registered_Mobile',
				category:'Seller_registration_error',
				label:'mobile number already registered'
			},
			resendOTP:{
				action:'Resend_OTP_Seller',
				category:'Login_SignUp_Seller',
				label: 'Resend_OTP'
			},
			mobileNumberError:{
				action:'Mobile_Incorrect',
				category:'Seller_registration_error',
				label: 'please provide valide mobile number'
			},
			failureOTP:{
				action:'SellerOTP_Incorrect',
				category:'Seller_registration_error',
				label: 'wrong_OTP'
			},
			submitOTP:{
				action:'Submit_OTP_Seller',
				category:'Login_SignUp_Seller',
				label: 'Submit_OTP'
			}
		};

		function gaTrack(data){
			utils.trackEvent(data.action, loginService.getTrackingCategory(data.category), data.label);
		}

		function bindControlEvents(){
            if($moduleEl){
                $moduleEl.find('input').on('focus', function(){
                    $(this).closest('.login-input-box').addClass('active');
                });
                $moduleEl.find('input').on('blur', function(){
                    if(!$(this).val()){
                        $(this).closest('.login-input-box').removeClass('active');
                    }
                });
            }
        }
        function unBindBlurEvents(){
        	if($moduleEl){
        		$moduleEl.find('input').off('focus');
            	$moduleEl.find('input').off('blur');
        	}
        }
        var showLoader = function(){
            $(".main-loading.box-loading").removeClass('hide');
        };
        var hideLoader = function(){
            $(".main-loading.box-loading").addClass('hide');
        };
        var changeInputType = function(){
			if(utils.isMobileRequest()){
				$moduleEl.find('input.js-number').attr('type', 'number');
			}
        };
		function updateUser(){
			showLoader();
			var postData = {};
			var phonenumber = loginService.getUserProp('phonenumber');
			var phoneNumberObj = {
				"contactNumber": phonenumber,
				"priority": 1,
				"sendOTP": true
			};
			loginService.setUserProp('contactNumber',phonenumber);
			//todo: remove this after confirmation from midl
			// postData = {
			// 	email:loginService.getUserProp('email')
			// };
			postData.contactNumbers = [];
			postData.contactNumbers.push(phoneNumberObj);
			apiService.postJSON(config.updateUserURL, postData, null, true).then(_sendOtpSuccessHandler, _sendOtpErrorHandler);
		}
		
		function setupViewTransitions(template){
			$moduleEl.empty();
			var data = {
				name: loginService.getUserProp('name'),
				email: loginService.getUserProp('email'),
				phone: loginService.getUserProp('phonenumber'),
				profileImg: loginService.getUserProp('profileImg'),
				backButton: config.backButton
			};
			if(data.profileImg && data.profileImg.length >0){
				data['jsdummyimg'] = 'hide';
				data['jsprofileimg'] = 'show';
			} else {
				data.profileImg = "";
				data['jsdummyimg'] = 'show';
				data['jsprofileimg'] = 'hide';
			}
			var htmlTemplate = doT.template(template);
			$moduleEl.append(htmlTemplate(data));
			changeInputType();
    		setTimeout(function(){$moduleEl.find('input:visible').eq(0).focus();},100);
			if(config.viewTransition){
				$moduleEl.addClass('login-content-box');
				$moduleEl.addClass('p-center');
			}
		}
		
		function validateOTP(OPTCode){
			var id = $moduleEl.attr('id');
			showLoader();
			loginService.validateOTP(OPTCode).then(function(data){
				hideLoader();
				OTPCode = '';
				var resp = data;
				if(resp.data){
					gaTrack(trackingData.submitOTP);
					// var phoneNumber = controlsService.get('phoneNumber');
					var phoneNumber =  loginService.getUserProp('phonenumber');
					if(phoneNumber) {
						$('#phn-no').html(phoneNumber);
					}
					$moduleEl.find('.otp .icon-check').addClass('green');
	                $moduleEl.find('.otp-success').toggleClass('hide');
	                $errorMsg.addClass('hide');
	                if(id === 'changePhone'){
	                	Box.Application.broadcast("popup:close", {});
	                }else{
	                	Box.Application.broadcast('change_phone_otp_verified',{
	                		id: $moduleEl.attr('id')
	                	});
	                }
				}else{
					gaTrack(trackingData.failureOTP);
					$errorMsg.removeClass('hide');
				}
				
            }, function(){
            	hideLoader();
				gaTrack(trackingData.failureOTP);
                $errorMsg.removeClass('hide');
            });
			//todo: show success message and remove popup if loaded from profile pages
		}
		function resendOTP(){
			updateUser();
		}
		function resendOtpOnCall() {
			showLoader();
			hideOtpOnCall = true;
			var userId = loginService.getUserProp('id');
			var phoneNumber = loginService.getUserProp('phonenumber');
			var postData = {
				userId: userId,
				phoneNumber: phoneNumber
			};
			apiService.postJSON(config.sendOtpOnCallURL, postData, null, true).then(_sendOtpSuccessHandler, _sendOtpErrorHandler);
		}
		function _sendOtpSuccessHandler(response){
			hideLoader();
			var result = response;
			if(result.data){
            	$moduleEl.removeClass('p-center').addClass('p-left');
				setupViewTransitions(otpFormTemplate);
				if (hideOtpOnCall) {
					$moduleEl.find('.call-otp-view').addClass('hide');
				}
				gaTrack(trackingData.otpScreen);
			} else if(result.error){
				$errorMsg.removeClass('hide').text(result.error.msg);
			} else {
				$errorMsg.removeClass('hide').text("Some technical error occured, please try again later");
			}				
        }
        function _sendOtpErrorHandler(error){
        	hideLoader();
        	trackingData.phonenumber.label = error.responseJSON.error.msg;
        	gaTrack(trackingData.phonenumber);	
            $errorMsg.removeClass('hide').text(error.responseJSON.error.msg);
        }
		
		return {
					behaviors: ['registerBehavior'],
                    init: function() {
                        config = context.getConfig();
		                moduleEl = context.getElement();
						$ = context.getGlobal('jQuery');
						commonService = context.getService('CommonService');
						apiService = context.getService('ApiService');
						loginService = context.getService('LoginService');
                        $moduleEl = $(moduleEl);
                        configScript = $moduleEl.find('script[type="text/x-config"]');
						setupViewTransitions(mobileInputTemplate);
						$contactNumber = $moduleEl.find('#contact-number');
						if(!config.updateUserURL){
							config.updateUserURL = '/xhr/userService/updateUser';
						}
						if(!config.sendOtpOnCallURL){
							config.sendOtpOnCallURL = '/xhr/userService/sendOtpOnCall';
						}
						bindControlEvents();
						gaTrack(trackingData.mobileScreen);
						context.broadcast('moduleLoaded', {
			            	name: 'changePhone',
			            	'options': {
		                    	'callback': function(){
		                    		setTimeout(function(){$moduleEl.find('input').eq(0).focus();},100);
		                    	}
		                    }
		            	});
		        	},
		        	destroy: function(){
	                    $moduleEl.html(configScript);
	                    configScript = null;
						config = null;
						OTPCode = null;
			            moduleEl = null;
						$ = null;
						$moduleEl = null;
						loginService=null;
						unBindBlurEvents();
		        	},
					onclick: function(event, element, elementType) {
				        switch (elementType) {
				            case 'send-otp':
				            	$errorMsg = $moduleEl.find('.success-error-msg');
				            	$errorMsg.addClass('hide');
				            	var phonenumber = $.trim($contactNumber.val()),
				            		phonenumberlength = phonenumber.toString().length;
				            	// controlsService.set('phoneNumber', phonenumber);
								if($contactNumber.val() && phonenumberlength === 10){
									loginService.setUserProp('phonenumber',$contactNumber.val());
									updateUser();
								}else{
									gaTrack(trackingData.mobileNumberError);
									$contactNumber.closest('.login-input-box').addClass('error');
									$contactNumber.on('blur', function(){
										if($(this).val()){
											$(this).closest('.login-input-box').removeClass('error');
										}else{
											$(this).closest('.login-input-box').addClass('error');
										}
									});
								}
				                break;
							case 'validate-otp':
								$errorMsg = $moduleEl.find('.success-error-msg');
								$errorMsg.addClass('hide');
								var OTPCodeLength = OTPCode.toString().length;
								if(OTPCodeLength === 4){
									validateOTP(OTPCode);
								}else{
									gaTrack(trackingData.failureOTP);
									$errorMsg.removeClass('hide');
								}
								break;
							case 'resend-otp':
								gaTrack(trackingData.resendOTP);
								resendOTP();
								break;
							case 'resend-otp-on-call':
								gaTrack(trackingData.resendOTP);
								resendOtpOnCall();
								break;
							case 'backOTP':
								setupViewTransitions(mobileInputTemplate);
								$moduleEl.find('input').eq(0).closest('.login-input-box').addClass('active');
								$contactNumber = $moduleEl.find('#contact-number');
								break;
							case 'change-phone-back':
								Box.Application.broadcast('unloadChangePhone',{
									'id': 'newLoginRegister'
								});
								break;
						}
					},
					onkeyup: function(event, element, elementType){
						switch (elementType){
							case 'validateOTP':
								var key = event.keyCode || event.which,
									allInputs = $moduleEl.find('.otp-code input'),
									currentInput = $(event.target),
									currentIndex = allInputs.index(currentInput);
								if(key == 37){
									var nextIndex = (currentIndex > 0) ? currentIndex-1 : 0;
									allInputs.eq(nextIndex).focus();
								}else if(key == 39){
									allInputs.eq(currentIndex+1).focus();
								}
								else if(currentInput.val() !=='' && key !== 8 && key !== 46){
									allInputs.eq(currentIndex+1).focus();
									OTPCode = $moduleEl.find('.otp-code input').map(function() {
									    return this.value;
									}).get().join('');
									if(OTPCode.toString().length === 4){
										$moduleEl.find('[data-type="validate-otp"]').click();
									}
								}
								
						}
					},
					onkeydown: function(event, element, elementType){
						switch (elementType){
							case 'number':
							case 'validateOTP':
								var key = event.keyCode || event.which;
			                    if (((key >= 48 && key <= 57) || (key >= 96 && key <= 105) || key == 8 || key == 9 || key == 46 || key == 37 || key == 39) && (event.shiftKey === false || (event.shiftKey === true && key == 9))){
			                    	var maxlength = (elementType === 'number') ? 10 : 1;
									var value = $(element).val(),
										valueLength = value.toString().length;
									if(valueLength >= maxlength && key !== 8 && key!== 46 && key !== 37 && key!== 39){
										event.preventDefault();
									}
									if(elementType === 'validateOTP'){
										var allInputs = $moduleEl.find('.otp-code input'),
											currentInput = $(event.target),
											currentIndex = allInputs.index(currentInput);
										if((key === 46 || key === 8) && currentInput.val() ===''){
											var nextIndex = (currentIndex > 0) ? currentIndex-1 : 0;
											allInputs.eq(nextIndex).focus();
										}
									}
			                    }else{
			                    	 event.preventDefault();
			                    }
								break;
							default:
								break;
						}
					}
                };
	});
});
