'use strict';
define([
    "services/loggerService",
    "services/commonService",
    "services/filterConfigService",
    "services/localStorageService",
    "services/searchService",
    "services/urlService",
    "services/utils",
    'services/defaultService',
    "services/typeAheadService",
    "modules/typeAhead/scripts/index",
    "modules/searchFilter/scripts/behaviors/searchTracking"
], function(logger, commonService, filterConfigService, localStorageService, searchService, urlService, utilService,defaultService) {
    Box.Application.addModule("searchFilter", function(context) {

        const config = context.getGlobal('config');

        const moduleEl = context.getElement(),
            moduleId = moduleEl.id,
            $moduleEl = $(moduleEl),
            typeAheadService = context.getService('TypeAheadService');
        const SEARCH_BUTTON = 'search-button',
            ADD_MORE_PLACEHOLDER = 'add more',
            SEARCH_TYPEAHEAD_NAME = 'searchTypeAhead',
            RENT_PLACEHOLDER = 'pick location or project',
            BUY_PLACEHOLDER  = 'pick location, builder or project',
            COMMERCIAL_BUY_PLACEHOLDER = 'pick location, builder or project',
            COMMERCIAL_LEASE_PLACEHOLDER = 'pick location or project',
            AGENT_PLACEHOLDER = 'search by location, project or landmark';
        const filterModuleId  = 'home-filters';

        var searchStartFlag = true,
            searchBackSpaceFlag = true,
            searchSuggestionFlag = false,
            localConfig,
            searchWrap,
            typeAheadId;
        let wrapperFunc,
            SEARCH_STATUS_ENUM = {
                new: 10,
                started:20,
                selected:30,
                done:40
            },
            SEARCH_TYPE_ENUM = {
                BUY:1,
                RENT:2,
                COMMERCIAL_BUY:3,
                COMMERCIAL_LEASE:4,
                AGENT:5,
            },
            enumSaleTypeMapping = {
                1: "buy",
                2: "rent",
                3: "commercialBuy",
                4: "commercialLease"
            },
            selectedItems = {
                1: {},
                2: {},
                3: {}
            },
            tagsData = [];

        let wrapperUtilFuncs = function() {

            let searchType = SEARCH_TYPE_ENUM.BUY,
                redirectUrlQueryParams = '',
                redirectUrl;
            function hideNewSearchBtn(){
                $moduleEl.closest(".home-search-container").find('.js-NewSearchFilters').addClass('hide');
            }
            function showNewSearchBtn(){
                $moduleEl.closest(".home-search-container").find('.js-NewSearchFilters').removeClass('hide');
            }
            function updatePlaceholder(){
                let placeHolder = '';
                if (searchType == SEARCH_TYPE_ENUM.BUY){
                    placeHolder = BUY_PLACEHOLDER;
                } else if (searchType == SEARCH_TYPE_ENUM.RENT){
                    placeHolder = RENT_PLACEHOLDER;
                } else if (searchType == SEARCH_TYPE_ENUM.COMMERCIAL_BUY){
                    placeHolder = COMMERCIAL_BUY_PLACEHOLDER
                } else if (searchType == SEARCH_TYPE_ENUM.COMMERCIAL_LEASE){
                    placeHolder = COMMERCIAL_LEASE_PLACEHOLDER
                } else {
                    placeHolder = AGENT_PLACEHOLDER;
                }
                $moduleEl.find('.js-input').attr('placeholder', placeHolder);
            }
            function showSearchLoader(){
                $moduleEl.find('.js-btnloader').removeClass('hide');
                $moduleEl.find('.js-count-properties').addClass('hide');
            }
            function hideSearchLoader(){
                $moduleEl.find('.js-btnloader').addClass('hide');
                $moduleEl.find('.js-count-properties').removeClass('hide');
            }

            function resetSearchText(){
                tagsData = [];
                selectedItems[searchType] = {};
                context.broadcast("removeTag", {
                    tagsArray:[],moduleEl: {id: typeAheadId}
                })
            }

            function showError(txt){
                $moduleEl.find('.js-search-error').html(txt);
                setTimeout(()=>{
                    $moduleEl.find('.js-search-error').html('')
                },2000);
            }

            function resetFilters(trackEvent){
                context.broadcast('reset-filters',{
                    resetTempListingType: true,
                    trackEvent
                });
                redirectUrlQueryParams = '';
            }

            function updateSearchCTA({listingCount,continueSearch}={}){/*Original Search Result doesnot include near by localities*/
                let searchCTA = continueSearch?'Continue with last search':'Search';
                switch(searchType){
                    case SEARCH_TYPE_ENUM.BUY:
                    case SEARCH_TYPE_ENUM.RENT:
                    case SEARCH_TYPE_ENUM.COMMERCIAL_BUY:
                    case SEARCH_TYPE_ENUM.COMMERCIAL_LEASE:
                        if(listingCount>=1){
                            searchCTA = `View ${listingCount} `;
                            searchCTA+=listingCount==1?'Property':'Properties';
                        }
                    break;
                    case SEARCH_TYPE_ENUM.AGENT:
                        searchCTA = 'Search Top Agents'
                    break;
                }
                $moduleEl.find('.js-count-properties').html(searchCTA)
            }

            function showFilters(){
                $moduleEl.find('.js-withFilters').removeClass('hide');
                context.broadcast('open-filters');
                //on mobile there are 2 search buttons, need to play with hide/show accordingly
                if(utilService.isMobileRequest()){
                    $moduleEl.find('.js-searchbtn').removeClass('hide');
                }
            }

            function hideFilters(trackEvent){
                $moduleEl.find('.js-withFilters').addClass('hide');
                //on mobile there are 2 search buttons, need to play with hide/show accordingly
                if(utilService.isMobileRequest()){
                    $moduleEl.find('.js-searchbtn').addClass('hide');
                }
                resetFilters(trackEvent);
                updateSearchCTA();
            }

            function resetSearch(trackEvent){
                let placeholder = '';
                tagsData = [];
                updatePlaceholder();
                hideFilters(trackEvent);
                hideNewSearchBtn();
                resetSearchText();
                redirectUrl=null;
                context.broadcast("typeAheadModuleInitiated", {
                    id: typeAheadId
                })
            }

            function redirect(url){
                let queryString = redirectUrlQueryParams.indexOf('?') > -1 ? redirectUrlQueryParams.split('?')[1] : '';
                if(Object.keys(selectedItems[searchType]).length == 1){
                    url += queryString ? `?${queryString}` : '';
                } else {
                    url += queryString ? `&${queryString}` : '';
                }
                urlService.ajaxyUrlChange(url);
            }

            function searchClick(){
                let selectedSearchTypeItems = selectedItems[searchType];
                let selectedSearchTypeList = Object.keys(selectedSearchTypeItems);
                if(!selectedSearchTypeList.length){
                    return;
                }
                if(selectedSearchTypeList.length == 1){
                    let elementSelected = selectedSearchTypeItems[selectedSearchTypeList[0]];
                    searchService.abortAllPendingAPis();
                    if (searchType == SEARCH_TYPE_ENUM.AGENT && ['city', 'location'].indexOf(elementSelected.showType) > -1){
                        returnTopAgentsURL(elementSelected.type, elementSelected.entityId).then(url => {
                            redirectUrl = url;
                        })
                    } else {
                        redirectUrl = _getSelectedItemRedirectUrl(elementSelected);
                    }
                } else if(selectedSearchTypeList.length > 1){
                    searchService.abortAllPendingAPis();
                    let urlParams = {
                        skipPageData: true,
                        overrideParams: searchService.getCityLocalitySuburbOverrideData(selectedSearchTypeItems)
                    }
                    redirectUrl = filterConfigService.getUrlWithUpdatedParams(urlParams);
                }
            }

            function updateListingCountInCTA(){
                let selectedSearchTypeItems = selectedItems[searchType];
                let selectedSearchTypeList = Object.keys(selectedSearchTypeItems);
                if(selectedSearchTypeList.length == 1){
                    let url = redirectUrl;
                    let params = redirectUrlQueryParams.indexOf('?') > -1 ? redirectUrlQueryParams.split('?')[1] : '';
                    let onlyListingCount=true;
                    showSearchLoader(); //getMultipleListingSERP
                    searchService.getListingSERP(url,params.replace(/&/g,'@@@@'),onlyListingCount,searchType)
                        .then(result => {
                            hideSearchLoader();
                            if(result){
                                if(result.isOriginalSearchResult){
                                    updateSearchCTA({listingCount:result.totalCount})
                                } else {
                                    updateSearchCTA()
                                }
                            }
                        },
                        error=>{
                            hideSearchLoader();
                            console.log(error)
                        });
                } else if(selectedSearchTypeList.length > 1){
                    let queryString = redirectUrl.split('?')[1];
                    let params = redirectUrlQueryParams.indexOf('?') > -1 ? redirectUrlQueryParams.split('?')[1] : '';
                    queryString += `&${params}`;
                    showSearchLoader();
                    searchService.getMultipleListingSERP(queryString).then(result => {
                        hideSearchLoader();
                        if(result){
                            if(result.isOriginalSearchResult){
                                    updateSearchCTA({listingCount:result.totalCount})
                                } else {
                                    updateSearchCTA()
                                }
                        }
                    })
                }
            }

            function updateCurrentSearchContext(displayText, redirectUrl){
                currentSearchContext[searchType] = {
                    displayText,
                    redirectUrl
                }
            }

            function searchStateChanged(newStatus,data){
                switch(newStatus){
                    case SEARCH_STATUS_ENUM.new:
                        resetSearch(data&&data.trackEvent);
                        resetFilters();
                    break;
                    case SEARCH_STATUS_ENUM.started:
                        updatePlaceholder();
                    break;
                    case SEARCH_STATUS_ENUM.selected:
                        updateSearchCTA();
                        let placeholder = ADD_MORE_PLACEHOLDER;
                        let eligibleForMultiSelect = true;
                        let listingTypeEnum = wrapperFunc.getCurrentSaleType() || SEARCH_TYPE_ENUM.BUY;
                        let tagsObj = {
                            tagid: data.dataset.tagid,
                            val: data.dataset.val,
                            dataset: data.dataset
                        };
                        tagsData.push(tagsObj);
                        context.broadcast("createTag", {
                            tagsArray:tagsData, moduleEl: {id: typeAheadId}
                        });
                        if(searchType == SEARCH_TYPE_ENUM.AGENT){
                            context.broadcast("disableTypeaheadSearch", {
                                id: typeAheadId
                            });
                        }
                        var elementData = data.dataset;
                        let item = searchService.getResultItemObject(elementData) || {};
                        let itemType = item.type.toUpperCase();
                        if ((data.config.multiselect && data.config.multiselect.indexOf(itemType) == -1) || !data.config.multiselect || searchType == SEARCH_TYPE_ENUM.AGENT) {
                            //empty if not multiselect category
                            eligibleForMultiSelect = false;
                        };
                        
                        localStorageService.setRecentSearches(item, enumSaleTypeMapping[listingTypeEnum]);

                        selectedItems[listingTypeEnum][item.id] = item;

                        if (Object.keys(selectedItems[listingTypeEnum]).length >= 4) {
                            placeholder = '';
                            context.broadcast("hideSimilarLocalities",{});
                        }
                        if (data.config.multiselect && data.config.multiselect.indexOf(elementData.tagtype) > -1 && searchType != SEARCH_TYPE_ENUM.AGENT && Object.keys(selectedItems[listingTypeEnum]).length < 4) {
                            $(data.moduleEl).find('input').attr('placeholder', placeholder);
                            context.broadcast("recentlyAddedLocation", {
                                locationType: "LOCALITY",
                                tagsArray: tagsData,
                                typeAheadId
                            }); 
                        }
                        searchClick();
                        if(decideOnfilters(item, eligibleForMultiSelect)){
                            if(item.showType != 'landmark'){
                                if (!commonService.checkModuleLoaded('filter',filterModuleId)) {
                                    context.broadcast('loadModule', [{
                                        name: 'filter',
                                        loadId:filterModuleId
                                    }]);
                                }
                                showFilters();
                                updateListingCountInCTA();
                            }
                        }
                        showNewSearchBtn();

                    break;
                    case SEARCH_STATUS_ENUM.done:
                        let allSelection = [];
                        tagsData.forEach(selection => {
                            allSelection.push(`${selection.dataset.entityid}_${selection.dataset.tagtype}`);
                        });
                        context.broadcast('trackSearchBtnClicked', {
                            queryUrl: redirectUrl,
                            allSelection: allSelection,
                            totalTagsCount: tagsData.length,
                            isSearchButton: true,
                        });
                        if(!Object.keys(selectedItems[searchType]).length){
                            showError("Please select a locality/project/builder to search");
                            return;
                        }
                        redirect(redirectUrl);
                    break;
                }
            }

            return {
                searchStateChanged,
                setRedirectUrlQueryParams: function(url){
                    redirectUrlQueryParams = url;
                    if(searchType!==SEARCH_TYPE_ENUM.AGENT){
                        updateListingCountInCTA();
                    }
                },
                getRedirectUrlQueryParams: function(){
                    return redirectUrlQueryParams;
                },
                setSearchType: function(type){
                    switch(type){
                        case 'rent':
                            searchType = SEARCH_TYPE_ENUM.RENT;
                        break;
                        case 'buy':
                            searchType = SEARCH_TYPE_ENUM.BUY;
                        break;
                        case 'agent':
                            searchType = SEARCH_TYPE_ENUM.AGENT;
                        break;
                        case 'commercialBuy':
                            searchType = SEARCH_TYPE_ENUM.COMMERCIAL_BUY;
                        break;
                        case 'commercialLease':
                            searchType = SEARCH_TYPE_ENUM.COMMERCIAL_LEASE;
                        break;
                    }
                    resetSearch();
                    updateSearchCTA();
                },
                getCurrentSaleType: function(){
                    return searchType;
                },
                searchClick,
                updateListingCountInCTA
            }
        }

        function _getSelectedItemRedirectUrl(item){
            let type = item.type, queryParams, redirectUrl = item.redirectUrl;
            if ((type.indexOf('OVERVIEW') == -1) && (type.indexOf('Typeahead-Suggestion') == -1)) {
                queryParams = filterConfigService.getSelectedQueryFilterKeyValObj();
                redirectUrl = urlService.changeMultipleUrlParam(queryParams, redirectUrl, true);
            }

            if((type.indexOf('OVERVIEW') == -1)){
                let prefix = utilService.getPageData('isMap') ||  (item.type && item.type.toLowerCase() === 'gp' && !utilService.isMobileRequest()) ? '/maps/': '';
                if(prefix && redirectUrl.indexOf(prefix) < 0) {
                    redirectUrl = redirectUrl[0] == '/' ? redirectUrl.substr(1) : redirectUrl;
                    redirectUrl = `${prefix}${redirectUrl}`;
                }
            }
            return redirectUrl;
        }


        var decideOnfilters = function (selectedItem) {
            let redirectUrl,
                showFilters = true;
                searchService.abortAllPendingAPis();
                let type=selectedItem.type,
                    queryParams={},
                    agentSearchEnabled = utilService.getPageData('agentSearchEnabled'),
                    searchText = selectedItem.displayText;
                if ((type.indexOf('OVERVIEW') >=0) || (type.indexOf('Typeahead-Suggestion') >=0)) {
                    showFilters= false;
                } else if(agentSearchEnabled =="Yes" && selectedItem.showType!='builder'){
                    //Check for Agent Search, ignore for builders
                    showFilters= false;
                } 
            return showFilters;
        };

        var _isSearchBoxEmpty = function(selectedItem, query){
            if(!Object.keys(selectedItem).length && !query){
                return true;
            }
            return false;
        }

        var returnTopAgentsURL = function(type,id){
            return searchService.getTopAgentsURL(type,id).then(
                (result)=>{
                    return (result.data['MAKAAN_'+type+'_TOP_BROKERS-'+id]);
                },()=>{})
        }
        var startSearchFilter = function(){
            wrapperFunc.searchStateChanged(SEARCH_STATUS_ENUM.new);    
        }

        return {
            messages: [
            'pageDataListingTypeChange',
            'searchResultClicked',
            'searchKeyBackspaced',
            'searchKeyTyped',
            'searchResultPopulated',
            'searchCleared',
            'multiSelectOptionsChanged',
            'multiSelectDropdownChanged',
            'singleSelectOptionsChanged',
            'singleSelectDropdownChanged',
            'rangeInputDropdownChanged',
            'rangeSliderChanged',
            'listingTypeChange',
            'searchResultCanceled',
            'reset-filters-clicked'
            ],
            behaviors: ['searchFilterTracking'],

            /**
             * Initializes the module and caches the module element
             * @returns {void}
             */
            init: function() {
                localConfig = context.getConfig() || {};
                searchWrap = $(moduleEl).find('.search-wrap');
                typeAheadId = moduleEl.querySelector('[data-module="typeAhead"]').id;
                wrapperFunc = new wrapperUtilFuncs();
                startSearchFilter();
                context.broadcast('moduleLoaded', {
                    name: 'searchFilter',
                    id: moduleEl.id
                });
                context.broadcast('loadModule', {
                    name: 'showSimilarLocalities'
                });
            },

            onclick: function(event,element, elementType) {
                let trackEvent=true;
                switch (elementType) {
                    case SEARCH_BUTTON:
                        wrapperFunc.searchStateChanged(SEARCH_STATUS_ENUM.done);
                        break;
                    case 'reset-filters':
                        wrapperFunc.searchStateChanged(SEARCH_STATUS_ENUM.new,{
                            trackEvent: true
                        });
                        break;
                }
            },

            onmessage: function(name, data) {
                let tempHref;
                switch (name) {
                    case 'searchKeyTyped':
                        if(searchStartFlag && data.searchParams && data.searchParams.query && data.searchParams.query.length >= 2) {
                            searchStartFlag = false;
                            searchSuggestionFlag = true;
                            searchBackSpaceFlag = true;
                            context.broadcast('searchModuleKeyTyped', {
                                moduleId: moduleId,
                                query: data.searchParams.query
                            });
                        }
                        break;
                    case 'searchResultPopulated':
                        if(!searchSuggestionFlag) {
                            return;
                        }
                        context.broadcast('searchModuleResultPopulated', {
                            moduleId: moduleId,
                            query: data.searchParams.query,
                            resultCount: data.resultCount,
                            results: data.response
                        });
                        break;
                    case 'searchResultClicked':
                        if (!data || !data.config || data.config.name != SEARCH_TYPEAHEAD_NAME || data.moduleEl.id != typeAheadId){
                            return;
                        }
                        let allSelection = [];
                        data.tagsArray.forEach(selection => {
                            allSelection.push(`${selection.dataset.entityid}_${selection.dataset.tagtype}`);
                        });
                        context.broadcast('searchModuleResultClicked', {
                            moduleId: moduleId,
                            isClick: data.isClick,
                            isSearchButton: false,
                            query: data.searchParams ? data.searchParams.query:'',
                            index: data.dataset.index,
                            cityId:data.dataset.cityid,
                            localityId: data.dataset.localityid,
                            entityId: data.dataset.entityid,
                            entityType: data.dataset.tagtype,
                            allSelection: allSelection.join(','),
                            totalTagsCount: data.tagsArray.length
                        });

                        wrapperFunc.searchStateChanged(SEARCH_STATUS_ENUM.selected, data);
                        

                        break;
                    case 'multiSelectOptionsChanged':
                    case 'multiSelectDropdownChanged':
                    case 'singleSelectOptionsChanged':
                    case 'singleSelectDropdownChanged':
                    case 'rangeInputDropdownChanged':
                    case 'rangeSliderChanged':
                        if(data.id==='cityList'){ //todo: this is a hack and need to be fixed
                            return
                        }
                        var value = data.value;
                        tempHref = wrapperFunc.getRedirectUrlQueryParams();
                        if (!value || value === '') {
                            tempHref = urlService.removeUrlParam(data.name, tempHref, true);
                        } else {
                            tempHref = urlService.changeUrlParam(data.name, value.toString(), tempHref, true);
                        }
                        wrapperFunc.setRedirectUrlQueryParams(tempHref);
                        break;
                    case 'searchCleared':
                        searchStartFlag      = true;
                        searchSuggestionFlag = false;
                        break;
                    case 'searchKeyBackspaced':
                        if(!searchBackSpaceFlag) {
                            return;
                        }
                        searchBackSpaceFlag = false;
                        context.broadcast('searchModuleKeyBackspaced', {
                            moduleId: moduleId,
                            query: data.inputElementValue,
                            resultCount: data.resultCount
                        });
                        break;
                    case 'pageDataListingTypeChange':
                    case 'listingTypeChange':
                        tempHref = wrapperFunc.getRedirectUrlQueryParams();
                        tempHref = urlService.changeUrlParam('listingType', data.listingType, tempHref, true);
                        wrapperFunc.setRedirectUrlQueryParams(tempHref);

                        wrapperFunc.setSearchType(data.listingType);
                        break;
                    case 'searchResultCanceled':
                        let dataId = data && data.moduleEl && data.moduleEl.id ? data.moduleEl.id : data.id;
                        if (!data || !data.config || data.config.name != SEARCH_TYPEAHEAD_NAME || dataId != typeAheadId) {
                            return;
                        }
                        let elementData = data.dataset;
                        let tagid = elementData.tagid;
                        for (let tags = 0; tags < tagsData.length; tags++) {
                            if (tagsData[tags].tagid == tagid) {
                                tagsData.splice(tags, 1);
                                break;
                            }
                        }
                        let currentListingType = wrapperFunc.getCurrentSaleType();
                        delete selectedItems[currentListingType][elementData.tagid];
                        wrapperFunc.searchClick();

                        context.broadcast("removeTag", {
                            tagsArray:data.tagsArray, dataset: elementData ,moduleEl: {id: typeAheadId}
                        })
                        let selectedItemsLength = Object.keys(selectedItems[currentListingType]).length;
                        if (_isSearchBoxEmpty(selectedItems[currentListingType], data.searchParams.query)) {
                            wrapperFunc.searchStateChanged(SEARCH_STATUS_ENUM.new, {});
                            context.broadcast('typeaheadSetFocus',{id:typeAheadId});
                        } else if (selectedItemsLength < 4) {
                            $(data.moduleEl).find('input').attr('placeholder', ADD_MORE_PLACEHOLDER);
                            wrapperFunc.updateListingCountInCTA();
                            context.broadcast("recentlyAddedLocation", {
                                locationType: "LOCALITY",
                                tagsArray: data.tagsArray,
                                typeAheadId
                            });
                        }
                        if (!selectedItemsLength) {
                            selectedItems[currentListingType] = {};
                        } 
                        break;
                    case 'reset-filters-clicked':
                        wrapperFunc.searchStateChanged(SEARCH_STATUS_ENUM.new,{
                            trackEvent: true
                        });
                        break;
                }
            },

            /**
             * Destroys the module and clears references
             * @returns {void}
             */
            destroy: function() {
            }
        };
    });
});
