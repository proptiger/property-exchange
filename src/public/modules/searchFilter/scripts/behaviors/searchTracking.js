define([
    'common/trackingConfigService',
    'services/trackingService',
    'services/utils'
], function(t, trackingService,utilService) {
    'use strict';
    Box.Application.addBehavior('searchFilterTracking', function(context) {

        var messages = [
            'searchModuleKeyTyped',
            'searchModuleKeyBackspaced',
            'searchModuleResultClicked',
            'searchModuleResultPopulated',
            'trackNoMatchingResult',
            'trackSearchBtnClicked',
            'trackAddMoreVisible'
        ];

        var moduleId = context.getElement().id;

        const LOCALITY_ID = 'LOCALITY',
            SUBURB_ID = 'SUBURB',
            CITY_ID = 'CITY';

        var _getResultsCSV = function(results) {
            let csv = [];
            if (results) {
                results.forEach(function(result) {
                    let values = result.values;
                    if (values) {
                        values.forEach(function(value) {
                            csv.push((value.entityId || value.googlePlaceId) + '_' + value.type);
                        });
                    }
                });
            }
            return csv.join();
        };

        var onmessage = function(msgName, data) {
            if (data.moduleId && data.moduleId != moduleId) {
                return;
            }

            let category = t.TYPEAHEAD_CATEGORY,
                label, event, value, name, rank, sourceModule, nonInteraction,
                localityId,
                suburbId,
                entityType,
                cityId,
                nullSearch,
                tagsCount,
                isAgentSearch = 1,
                url;

            if(utilService.getPageData('agentSearchEnabled')=="Yes"){
                isAgentSearch = 2;
            }

            switch (msgName) {
                case 'searchModuleKeyTyped':
                    event = t.TYPED_EVENT;
                    label = data.query;
                    break;
                case 'searchModuleResultClicked':
                    event = t.SUGGESTION_CHOSEN_EVENT;
                    label = data.query;
                    name = data.allSelection;
                    rank = data.index;
                    cityId = data.cityId;
                    localityId = data.localityId;
                    tagsCount = data.totalTagsCount;
                    sourceModule = t.ENTER_MODULE;
                    if (data.isClick) {
                        sourceModule = t.CLICK_MODULE;
                    } else if (data.isSearchButton) {
                        sourceModule = t.SEARCH_BUTTON_MODULE;
                    }
                    entityType = data.entityType;
                    if(entityType === LOCALITY_ID){
                        localityId = data.entityId;
                    } else if(entityType === SUBURB_ID){
                        suburbId = data.entityId;
                    } else if(entityType === CITY_ID){
                        cityId = data.entityId;
                    }
                    break;
                case 'searchModuleResultPopulated':
                    event = t.SUGGESTIONS_RECEIVED_EVENT;
                    label = data.query;
                    value = data.resultCount;
                    name = _getResultsCSV(data.results);
                    break;
                case 'searchModuleKeyBackspaced':
                    event = t.BACKSPACE_USED_EVENT;
                    value = data.resultCount;
                    if (data.query) {
                        label = data.query;
                    }
                    break;
                case 'trackNoMatchingResult':
                    if(!(data.query && data.query.length >= 4)){ // track no result only when atleast 4 characters are typed
                        return;
                    }
                    event = t.ERROR_EVENT;
                    label = data.query;
                    category = t.NO_MATCHING_RESULTS_CATEGORY;
                    nullSearch = data.query;
                    nonInteraction = 1;
                    break;
                case 'trackSearchBtnClicked':
                    event = t.SUGGESTION_CHOSEN_EVENT;
                    label = data.queryUrl;
                    url = data.queryUrl;
                    name = data.allSelection;
                    tagsCount = data.totalTagsCount;
                    break;
                case 'trackAddMoreVisible':
                    category = 'Multi Search';
                    event = t.VISIBLE_EVENT;
                    break;

            }

            let properties = {};
            properties[t.CATEGORY_KEY] = category;
            properties[t.LABEL_KEY] = label;
            properties[t.URL_KEY] = url;
            properties[t.VALUE_KEY] = value;
            properties[t.NAME_KEY] = name;
            properties[t.RANK_KEY] = rank;
            properties[t.SOURCE_MODULE_KEY] = sourceModule;
            properties[t.NON_INTERACTION_KEY] = nonInteraction;
            properties[t.CITY_ID_KEY] = cityId;
            properties[t.EMAIL_KEY] = tagsCount; // to track tags count for experiment
            if(nullSearch) {
                properties[t.NULL_SEARCH_KEY] = nullSearch;
            }

            properties[t.LOCALITY_ID_KEY] = localityId;
            properties[t.SUBURB_ID_KEY] = suburbId;

            trackingService.trackEvent(event, properties);
        };

        return {
            messages: messages,
            onmessage: onmessage,
            init: function() {},
            destroy: function() {}
        };
    });
});
