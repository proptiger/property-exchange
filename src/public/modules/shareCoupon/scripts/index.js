/**
 * This is a client side Module used for handling user interaction in the popup for coupon sharing.
 * It handles the click button on each and every popup.
 * JIRA : MAKAAN-4194.
 */
'use strict';
define([
        'modules/shareCoupon/scripts/services/couponService',
        'modules/shareCoupon/scripts/behaviors/shareCouponTracking',
        'services/commonService',
        'services/leadPyrService',
        'modules/lead/scripts/services/leadService',
        'services/urlService',
        'services/apiService',
        'common/sharedConfig',
        'services/utils',
        'services/filterConfigService',
        'services/defaultService',
        'services/localStorageService',
        'doT!modules/shareCoupon/views/shareContact',
        'doT!modules/shareCoupon/views/otp',
        'doT!modules/shareCoupon/views/changeContact',
        'doT!modules/shareCoupon/views/couponAlreadyShared',
        'doT!modules/shareCoupon/views/tooEarly',
        'doT!modules/shareCoupon/views/homeFinalized',
        'doT!modules/shareCoupon/views/homeNotFinalized',
        'doT!modules/shareCoupon/views/selectSellers',
        'doT!modules/shareCoupon/views/nonSelectSellers',
        'doT!modules/shareCoupon/views/noSelectCoupon',
        'doT!modules/shareCoupon/views/contactSupport',
        'doT!modules/shareCoupon/views/shareAddress',
        'doT!modules/shareCoupon/views/success',
    ], function(
        couponService,
        shareCouponTracking,
        CommonService, 
        LeadPyrService, 
        leadService, 
        urlService, 
        apiService, 
        sharedConfig, 
        UtilService,
        filterConfigService,
        defaultService,
        LocalStorageService,
        shareContactHtml, 
        otpHtml,
        changeContactHtml,
        couponAlreadySharedHtml,
        tooEarlyHtml,
        homeFinalizedHtml,
        homeNotFinalizedHtml,
        selectSellersHtml,
        nonSelectSellersHtml,
        noSelectCouponHtml,
        contactSupportHtml,
        shareAddressHtml, 
        successHtml){
     Box.Application.addModule("shareCoupon", (context => {
        let moduleEl, $, $moduleEl, moduleConfig, _details = {screenData:{},response:{},postData:{},step:'',isMobileRequest:false};
        let global ={isProcessing:false,popupOpened:false};
        const config = {
                "selector":{
                    "TEMPLATE_CONTAINER": "[data-shareCoupon-container]",
                    "PHONE": "[data-type=PHONE_FIELD] input",
                    "COUNTRY_ID": "input[data-country=COUNTRY_ID_FIELD]",
                    "OTP": "[data-type=OTP_FIELD] input",
                    "OTP_RESEND": "[data-type=OTP_RESEND]",
                    "THANKYOU_MESSAGE": ".js-thankyou",
                    "PROCESSING": "[data-processing]",
                    "NORMAL": "[data-normal]"
                },
                "ERROR":{
                    "PHONE":'.js-mobile-errorMsg',
                    "OTP":'.js-otp-errorMsg',
                    "SELECT_SELLER":'.js-selectSeller-errorMsg',
                    "NON_SELECT_SELLER":'.js-nonSelectSeller-errorMsg',
                    "ADDRESS":'.js-address-errorMsg',
                    "PHONE_PROCEED":'.js-mobileProceed-errorMsg',
                    "HOME_FINALIZED":'.js-homeFinalized-errorMsg'
                },
                "ERROR_MSG":{
                    "PHONE":'Please Enter Valid Number',
                    "OTP":'Please Enter Valid OTP',
                    "SELECT_SELLER":'Please Select a seller',
                    "NON_SELECT_SELLER":'Please Select a seller',
                    "ADDRESS":'Please select any locality/society',
                    "API_ERROR":"some error occured"
                }
            },
            CLASSES = {
                HIDE : "hide"
            };
        const confirmRedemptionUrlSms = "https://seller.makaan.com/select-program";
        const COUPON_COOKIE_NAME = "SELECT_COUPON_GENERATED";

        function _startAllModules() {
            CommonService.startAllModules(moduleEl);
        }

        function _stopAllModules() {
            CommonService.stopAllModules(moduleEl);
        }

        function clearMessage(tmpElement) {
             setTimeout(() => {
                 tmpElement.text('');
             }, 4000);
         }

        function setError(selector, message) {
             var tmpElement = $moduleEl.find(selector);
             tmpElement.text(message);
             clearMessage(tmpElement);
        }
        function typeAheadView(element, view) {
             if (view) {
                 element.closest('.typeAhead-hl').addClass('active');
             } else {
                 element.removeClass('active');
             }
         }

        // used for displaying or hiding processing button 
        function _showProcessing(show = true) {
            let actionButton = $(moduleEl).find(config.selector["NORMAL"]),
                processingButton = $(moduleEl).find(config.selector["PROCESSING"]);
            if (!show) {
                processingButton.addClass(CLASSES.HIDE);
                actionButton.removeClass(CLASSES.HIDE);
                global.isProcessing = false;
            } else {
                processingButton.removeClass(CLASSES.HIDE);
                actionButton.addClass(CLASSES.HIDE);
                global.isProcessing = true;
            }
        }
        // used for getting redirection url
        function getRedirectionUrl(data){
            let urlparams = {};
            urlparams.cityId = 21;
            urlparams.cityName = 'Pune';
            urlparams.listingType = 'rent';
            urlparams.postedBy = 'selectAgent';
            urlparams.sellerId = '';
            urlparams.builderId = '';
            urlparams.projectId = '';
            let url =  filterConfigService.getUrlWithUpdatedParams({overrideParams: urlparams});
            window.location.href = url;
            _showProcessing(false);
        }

        // hiding or displaying resend button on otp popup
        function _showResendButton(show = true) {
            if (show) {
                $(moduleEl).find(config.selector["OTP_RESEND"]).removeClass(CLASSES.HIDE);
            } else {
                $(moduleEl).find(config.selector["OTP_RESEND"]).addClass(CLASSES.HIDE);
            }
        }
        // showing loader in popup
        function _showPopupLoader(show = false){
            if(show){
                $(moduleEl).find('.js-main-loading').removeClass('hide');    
            } else {
                $(moduleEl).find('.js-main-loading').addClass('hide');
            }
        }
        // validating mobile
        function validateMobile (value) {
            let country= _details.screenData.countryName || 'india';
            return value && UtilService.validatePhone(value,country) ? true :false;
        }

        //function to render module Template, stop and start template modules.
        function _appendHtmlToView(html, callback) {
            _stopAllModules();
            $(moduleEl).find(config.selector["TEMPLATE_CONTAINER"]).html(html);
            _startAllModules();
            if (callback && typeof callback === "function") {
                callback();
            }
        }
        //opening popup 
        function _openPopup(id) {
            context.broadcast('HideProcessing');
            context.broadcast('popup:open', {
                id: "shareCouponPopup",
                options: {
                    config: {
                        modal: true
                    }
                }
            });
        }

        function resetVariables(){
            typeAheadView($moduleEl.find('.typeAhead-hl'), false);
            global.popupOpened = false;
            _details = {screenData:{},response:{},postData:{},step:''}
        }
        //closing popup
        function _closePopup(cb) {
            context.broadcast('popup:close', {_details, callback: cb});
            resetVariables();
        }
        //adding active class on the input box
        function _addClassActive(inputBoxId,selector) {
            CommonService.bindOnModuleLoad("InputBox", (id) => {
                if (id === inputBoxId) {
                    $(moduleEl).find(selector).parent().addClass('active');
                }
            });
        }
        function trackShareCouponEvent(label,source){
            context.broadcast('trackShareCouponEvent',{id:moduleEl.id,value:_details.screenData.userId,label,source});
        }

        // function to decide which template to render based on current step.
        function renderView(details, step){
            let html, cb;
            step = step || details.step;
            context.broadcast('trackCouponStepOpen',{ id:moduleEl.id, label:step, value:details.screenData.userId});
            _showProcessing(false);
            switch(step){
                case "SHARE_CONTACT":
                    html = shareContactHtml(details);
                    cb = _addClassActive.bind(undefined,'share-coupon-phone' ,".js-mobile");
                    break;
                case "OTP":
                    html = otpHtml(details);
                    break;
                case 'COUPON_ALREADY_SHARED':
                    html = couponAlreadySharedHtml(_details);
                    break;
                case'CONFIRM_HOME':
                    html = homeFinalizedHtml({});
                    break;
                case'HOME_NOT_FOUND':
                    html = homeNotFinalizedHtml({});
                    break;
                case'SELECT_SELLER':
                    html = selectSellersHtml(_details);
                    break;
                case 'NON_SELECT_SELLER':
                    html  = nonSelectSellersHtml(_details);
                    break;
                case 'SELLER_NOT_SELECT_SELLER':
                    html = contactSupportHtml(_details);
                    break;
                case 'NO_SELECT_COUPON':
                    html  = noSelectCouponHtml(_details);
                    break;
                case'SHARE_ADDRESS':
                    html = shareAddressHtml({});
                    break;
                case'SHOW_SUCCESS':
                    html = successHtml(details);
                    break;
                case 'CHANGE_CONTACT':
                    html = changeContactHtml(details);
                    break;
                case 'TOO_EARLY_FOR_REDEMPTION':
                    html = tooEarlyHtml({});
                    break;
            }
            _appendHtmlToView(html,cb);
            if(!global.popupOpened){
                global.popupOpened = true;
                _openPopup();
            } 
        }


        function saveSellerDetails(couponData){
            if(couponData.underProcess && couponData.underProcess.length){
                let underProcessCoupon = couponData.underProcess[0];
                _details.screenData.sellerId = underProcessCoupon.redeemerUserId;
                _details.screenData.sellerContact = underProcessCoupon.redeemerDetails.contact;
                _details.screenData.sellerEmail = underProcessCoupon.redeemerDetails.sellerEmail;
                _details.screenData.sellerName = underProcessCoupon.redeemerDetails.sellerName;
                _details.screenData.couponCode = underProcessCoupon.code;
            }
        }
        function couponCheckPopupHandler(response){
            let couponsData = response; 
            if(couponsData){
                if(couponsData.active && couponsData.active.length){
                    _details.activeCoupon = couponsData.active[0];
                    _details.screenData.couponCode = _details.activeCoupon.code;
                    _details.step = UtilService.isStorageExceedsTime(_details.activeCoupon,sharedConfig.couponShareEligibleTime) ? 'CONFIRM_HOME' : 'TOO_EARLY_FOR_REDEMPTION';
                }else if(couponsData.underProcess && couponsData.underProcess.length){
                    _details.underProcessCoupon = couponsData.underProcess[0];
                    _details.step = 'COUPON_ALREADY_SHARED';
                }else{
                    _details.step = 'NO_SELECT_COUPON';
                }
            }else{
                _details.step = 'NO_SELECT_COUPON';
            }
            renderView(_details);
        }

        function couponCheckBroadcastHandler(response){
            let couponsData = response;
            if(couponsData){
                if(couponsData.active && couponsData.active.length){
                    let activeCoupon = couponsData.active[0];
                    handleCouponGenerated(true,activeCoupon.displayCode,false);
                }else if(couponsData.underProcess && couponsData.underProcess.length){
                    let underProcessCoupon = couponsData.underProcess[0];
                    handleCouponGenerated(true,underProcessCoupon.code,true);
                }else{
                    handleNoCouponGenerated(true);
                }
            }else{
                handleNoCouponGenerated(true);
            }
        }
         // checking Coupon Status 
        function checkCouponStatus(broadcast){
            let userId = _details.screenData.userId,
                expiryDate = new Date().getTime();
            couponService.checkCouponStatus({userId,expiryDate}).then(response => {
                saveSellerDetails(response);
                if(broadcast){
                    couponCheckBroadcastHandler(response);
                }else{
                    couponCheckPopupHandler(response);
                }
            }, (err) => {
                if(broadcast){
                    handleNoCouponGenerated(true);
                }else{
                    _showProcessing(false);
                    context.broadcast('HideProcessing',{msg:config.ERROR_MSG['API_ERROR']});
                }
            });
        }
        // sending Otp to the user
        function sendOtp(showResend = true){      
            _showResendButton(showResend);
            let userId = _details.screenData && _details.screenData.userId || '',
                userNumber = _details.screenData.phone;
            couponService.sendOtp({userId,userNumber}).then(response => {
                _showResendButton();
                _details.step = 'OTP';
                renderView(_details,'OTP');
            }, (err) => {
                _showProcessing(false);
                _showResendButton();
                setError(config.ERROR['PHONE_PROCEED'],config.ERROR_MSG['API_ERROR']);
            });
        }
        // getting userId by phone
        function getUserByPhone(){
            let userNumber = _details.screenData.phone;
             couponService.getUserByPhone(userNumber).then(response => {
                if(response.id){
                    _details.screenData.userId = response.id;
                    sendOtp();
                }else{
                    _details.step = "CHANGE_CONTACT";
                    renderView(_details,'CHANGE_CONTACT');
                }
            }, (err) => {
                _details.step = "CHANGE_CONTACT";
                renderView(_details,'CHANGE_CONTACT')
            });
        }

        function handleCouponGenerated(broadcast,couponDisplayCode,underProcess){
            if(broadcast){
                context.broadcast('hideLoader',{
                    couponGenerated : true,
                    couponDisplayCode,
                    underProcess
                });
            }
        }


        function handleNoCouponGenerated(broadcast){
            if(broadcast){
                context.broadcast('hideLoader',{
                    couponGenerated : false,
                });
            }else{
                _details.step = 'SHARE_CONTACT';
                renderView(_details,'SHARE_CONTACT');
            }
        }

        //function to get user details.
        function verifyUser(broadcast = false){
            let urlData = urlService.getAllUrlParam();
            let couponCookieData = LocalStorageService.getItem(COUPON_COOKIE_NAME)||{};
            let userId = urlData.uid || couponCookieData.generatorUserId;
            let phone = urlData.phone || couponCookieData.userContact;
            if(userId && phone){
                couponService.getUserData({userId,phone}).then(response => {
                    if(response.isVerified){
                        _details.screenData.userId = userId ;
                        _details.screenData.buyerName = response.buyerName;
                        _details.screenData.phone = phone;
                        checkCouponStatus(broadcast);
                    }else{
                        handleNoCouponGenerated(broadcast);
                    }
                }, (err) => {
                    handleNoCouponGenerated(broadcast);
                });
            }else if(userId){
                couponService.getUserData({userId,phone:''}).then(response => {
                        _details.screenData.userId = userId ;
                        _details.screenData.buyerName = response.buyerName;
                        _details.screenData.phone = response.buyerContact;
                        checkCouponStatus(broadcast);
                }, (err) => {
                    handleNoCouponGenerated(broadcast);
                });
            }else{
               handleNoCouponGenerated(broadcast);
            }
        }
        
        //function to verify the otp entered by the user.
        function verifyOtp(formData, details) {
            let userId = _details.screenData.userId,
                otp = formData.otp;
             couponService.verifyOtp(userId,formData).then(response => {
                    checkCouponStatus();
            }, (err) => {
                _showProcessing(false);
                setError(config.ERROR['OTP'],config.ERROR_MSG['OTP']);
            });
        }

        // function called at final stage for sharing Coupon
        function _shareCoupon(){
            let buyerUserId = _details.screenData && _details.screenData.userId || '';
            _details.postData.couponId =  _details.activeCoupon.couponId;
            _details.postData.redeemerUserId = _details.screenData.sellerId;
            _details.postData.propertyAddress = _details.screenData.propertyAddress;
            couponService.shareCoupon(buyerUserId,_details.postData).then(response =>{
                _details.step = 'SHOW_SUCCESS';
                renderView(_details,'SHOW_SUCCESS');
            },(err)=>{
                _showProcessing(false);
                setError(config.ERROR['ADDRESS'],config.ERROR_MSG['API_ERROR'])
            });
        }
        //fetching sellers for which buyer has dropped the lead
        function fetchBuyerSellers(){
            let buyerUserId = _details.screenData.userId;
            couponService.fetchBuyerSellers(buyerUserId,5).then(response=>{
                _details.selectSellers = response.selectSellers;
                _details.nonSelectSellers = response.nonSelectSellers;
                if(_details.nonSelectSellers && _details.nonSelectSellers.length){
                    _details.showSellerNotFound = true;
                }
                _details.step = "SELECT_SELLER";
                _showPopupLoader(false);
                renderView(_details,"SELECT_SELLER");
            },(err) =>{
                setError(config.ERROR['HOME_FINALIZED'],config.ERROR_MSG['API_ERROR']);
                _showPopupLoader(false);
                _showProcessing(false);
            });
        }

        // function to get screen data and making appropriate api calls based on the current screen.
        function _submitScreenData(step,source=''){
            let data={};
            _showProcessing();
            context.broadcast('trackCouponProceed',{id:moduleEl.id,label:_details.step ,source, value:_details.screenData.userId});
            switch(step){
                case 'SHARE_CONTACT':
                    _details.screenData.phone = $(moduleEl).find(config.selector['PHONE']).val().trim();
                    _details.screenData.otpContact = _details.screenData.phone && _details.screenData.phone.substr(_details.screenData.phone.length - 4)
                    if(validateMobile(_details.screenData.phone)){
                        getUserByPhone();
                    }else{
                        _showProcessing(false);
                        setError(config.ERROR['PHONE'],config.ERROR_MSG['PHONE'])
                    }
                    break;
                case "OTP":
                    data["otp"] = $(moduleEl).find(config.selector['OTP']).val().trim();
                    if(!data.otp){
                        _showProcessing(false);
                        setError(config.ERROR['OTP'],config.ERROR_MSG['OTP']);
                    }else{
                        verifyOtp(data, _details);
                    }
                    break;
                case "CONFIRM_HOME":
                    if(_details.screenData.homeFinalized && _details.screenData.homeFinalized=="no"){
                        _details.step="HOME_NOT_FOUND";
                        renderView(_details,"HOME_NOT_FOUND");
                    }else{
                        fetchBuyerSellers();
                    }
                    
                    break;
                case 'SHARE_ADDRESS':
                     if(!_details.screenData.propertyAddress){
                        _showProcessing(false);
                        setError(config.ERROR['ADDRESS'],config.ERROR_MSG['ADDRESS']);
                     }else{
                        _shareCoupon();
                    }
                    break;
                case 'SELECT_SELLER':
                    if(_details.screenData.sellerId){
                        couponService.saveBuyerSellers(_details.screenData.userId,_details.screenData.sellerId);
                        _details.step="SHARE_ADDRESS";
                        renderView(_details,"SHARE_ADDRESS");
                    }else{
                         _showProcessing(false);
                        setError(config.ERROR['SELECT_SELLER'],config.ERROR_MSG['SELECT_SELLER']);
                    }
                    break;
                case 'NON_SELECT_SELLER':
                    if(_details.screenData.nonSelectSellerId){
                        couponService.saveBuyerSellers(_details.screenData.userId,_details.screenData.nonSelectSellerId);
                        _details.step="SELLER_NOT_SELECT_SELLER";
                        renderView(_details,"SELLER_NOT_SELECT_SELLER");
                    }else{
                         _showProcessing(false);
                        setError(config.ERROR['NON_SELECT_SELLER'],config.ERROR_MSG['NON_SELECT_SELLER']);
                    }
                    break;
                case 'CHANGE_CONTACT':
                    _details.step = 'SHARE_CONTACT';
                    renderView(_details,'SHARE_CONTACT');
                    break;  
                case 'NO_SELECT_COUPON':
                    _details.step = 'SHARE_CONTACT';
                    renderView(_details,'SHARE_CONTACT');
                    break;                   
            }
        }

        function buildCouponShareDescription(){
            let description = ` Hi ${_details.screenData.sellerName},\n ${_details.screenData.buyerName||'Tenant'} with mobile number ${_details.screenData.phone} has requested you to give discount of Rs 2000 from your brokerage. \n Confirm on this link ${confirmRedemptionUrlSms} to add Rs 2000 in your Makaan Select Wallet.\n \n Unlocked voucher code - ${_details.screenData.couponCode} \n\n Makaan.com`;
            return description;
        }

        function shareCouponWithSeller(medium,source){
            let trackLabel =`Share ${medium}`;
            trackShareCouponEvent(trackLabel,source);
            let sharingDescription = buildCouponShareDescription(),
                sharingEmailTitle = "Makaan Select - Deal Confirmation Request";
            switch (medium){
                case 'Gmail':
                    window.open("https://mail.google.com/mail/u/0/?view=cm&fs=1&body=" + encodeURIComponent(sharingDescription) + "&su=" + encodeURIComponent(sharingEmailTitle)+"&to="+_details.screenData.sellerEmail + "&tf=1");
                    break;
                case 'Sms':
                    if(_details.screenData.isIphone){
                        window.open(`sms:${_details.screenData.sellerContact}&body=${encodeURIComponent(sharingDescription)}`);
                    }else{
                        window.open(`sms:${_details.screenData.sellerContact}?body=${encodeURIComponent(sharingDescription)}`);
                    }
                    break;
                case 'Whatsapp':
                    window.open("whatsapp://send?text=" + encodeURIComponent(sharingDescription));
                    break;

            }
        }

        return {
            init: function(){
                moduleEl = context.getElement();
                $ = context.getGlobal('jQuery');
                $moduleEl = $(moduleEl);
                moduleConfig = context.getConfig()||{};
                _details.isMobileRequest = UtilService.isMobileRequest() ? true : false;
                context.broadcast('moduleLoaded', {
                    name: 'shareCoupon'
                });
                _details.screenData.isIphone = UtilService.isIphone(); 
            }, 
            destroy: function(){
                moduleEl = $ = $moduleEl = moduleConfig = null;
            },
            behaviors: ['shareCouponTracking'],
            messages: ['HomeFinalized','singleSelectOptionsInitiated','singleSelectOptionsChanged','singleSelectDropdownInitiated','singleSelectDropdownChanged','typeAheadOverlayClicked','searchResultClicked','popup:closed','loadCouponRedemptionView','sharingIconClicked'],
            onmessage: function(name, data){
                switch(name){
                    case 'loadCouponRedemptionView':
                            verifyUser(true);
                            break;
                    case 'HomeFinalized':
                            verifyUser();
                            break;
                    case 'singleSelectOptionsInitiated':
                    case 'singleSelectOptionsChanged':
                        switch(data.name){
                            case 'select_seller':
                                _details.screenData.sellerId = data.value;
                                break;
                        }
                        break;
                    case 'singleSelectDropdownInitiated':
                    case 'singleSelectDropdownChanged':
                        switch(data.name){
                            case 'countries':
                                let params = data && data.params && JSON.parse(decodeURI(data.params));
                                 _details.screenData.countryName = params.label.trim();
                                 $moduleEl.find('#COUNTRY_CODE').text(params.code);
                                 _details.screenData.countryCode = params.code;
                                 _details.screenData.countryId = data.value;
                                 break;
                        }
                        break;
                    case 'typeAheadOverlayClicked':
                    case 'searchResultClicked':
                         typeAheadView($moduleEl.find('.typeAhead-hl'), false);
                        switch(data.config && data.config.inputName){
                            case 'user_city_name':
                                 _details.screenData.propertyAddress = data && data.dataset ? data.dataset.val : '';
                                 break;
                        }
                        $moduleEl.find('.typeAhead-hl').removeClass('active');
                         break;
                    case 'popup:closed':
                         if(data && data.moduleEl && data.popupId=="shareCouponPopup")
                            context.broadcast('trackCouponStepClose',{id:moduleEl.id, label:_details.step, value:_details.screenData.userId});
                            resetVariables();
                         break;
                    case 'sharingIconClicked':
                            shareCouponWithSeller(data.medium,'Modal');
                            break;
                }
            },
            onclick: function(event, element, elementType){

                if (global.isProcessing) {
                  return;
                }

                switch(elementType){
                    //When user submits the contact no.
                    case 'SHARE_CONTACT_SUBMIT':
                        _submitScreenData("SHARE_CONTACT");
                        break;
                    // when user want to receive the OTP on call.
                    case 'OTP_ONCALL':
                        trackShareCouponEvent('OTP_ONCALL');
                        LeadPyrService.sendOTPOnCall({
                            userId: _details.screenData.userId,
                            userNumber: _details.screenData.phone,
                            element: $(element)
                        });
                        break;
                    // when user want to receive the OTP again.
                    case 'OTP_RESEND':
                        trackShareCouponEvent('OTP_RESEND');
                        sendOtp(false);
                        break;
                    // when user submits the OTP.
                    case 'OTP_SUBMIT':
                        _submitScreenData("OTP");
                        break;
                    //when user submits the address
                    case 'SHARE_ADDRESS_SUBMIT':
                        _submitScreenData("SHARE_ADDRESS");
                        break;
                    //when user submits after choosing Select Seller
                    case 'SELECT_SELLER_SUBMIT':
                        _submitScreenData('SELECT_SELLER');
                        break;
                    //when user submits after choosing non Select Seller
                    case 'NON_SELECT_SELLER_SUBMIT':
                       _submitScreenData('NON_SELECT_SELLER');
                        break;
                    // when user does not find any seller (not in select or non select seller)
                    case 'selectNoSeller':
                            _details.screenData.sellerId = '';
                            trackShareCouponEvent('SELECT_SELLER_NO_SELECT');
                            if(_details.nonSelectSellers && _details.nonSelectSellers.length){
                                _details.step = "NON_SELECT_SELLER";
                                renderView(_details,"NON_SELECT_SELLER");
                            }else{
                                _details.step = "SELLER_NOT_SELECT_SELLER";
                                renderView(_details,"SELLER_NOT_SELECT_SELLER");
                            }
                            break;
                    // if user Clicks on No in home finalized 
                    case 'homeNo':
                     _details.screenData.homeFinalized = 'no';
                     _submitScreenData("CONFIRM_HOME",'NO'); 
                        break;
                    // if user Clicks on Yes in home finalized 
                    case 'homeYes':
                     _details.screenData.homeFinalized = 'yes';
                     _showPopupLoader(true);
                     _submitScreenData("CONFIRM_HOME",'YES'); 
                        break;
                    // used for handling closing popup
                    case 'closeCouponPopUp':
                          context.broadcast('trackCouponOkay',{id:moduleEl.id,label:_details.step,value:_details.screenData.userId});
                           _closePopup();
                        break;
                    // used for handling submit for changing contact
                    case 'CHANGE_CONTACT_SUBMIT':
                        _details.screenData.phone = '';
                        _submitScreenData('CHANGE_CONTACT');
                        break;
                    case 'NO_SELECT_COUPON_SUBMIT':
                        _details.screenData.phone = '';
                        _submitScreenData('NO_SELECT_COUPON');
                        break;
                    case 'HomeNotFinalizedBack':
                        trackShareCouponEvent(`${_details.step}_BACK`);
                        _details.screenData.homeFinalized = '';
                        _details.step = "CONFIRM_HOME";
                        renderView(_details,"CONFIRM_HOME");
                        break;
                    case 'nonSelectSellerBack':
                        trackShareCouponEvent(`${_details.step}_BACK`);
                        _details.screenData.sellerId = undefined;
                        _details.step = "SELECT_SELLER";
                        renderView(_details,"SELECT_SELLER");
                        break;
                    case 'contactSupportBack':
                        trackShareCouponEvent(`${_details.step}_BACK`);
                        _details.screenData.nonSelectSellerId = undefined;
                        _details.step = "NON_SELECT_SELLER";
                        renderView(_details,"NON_SELECT_SELLER");
                        break;
                    case 'shareAddressBack':
                        trackShareCouponEvent(`${_details.step}_BACK`);
                        _details.screenData.sellerId = undefined;
                        _details.screenData.propertyAddress = '';
                        _details.step = "SELECT_SELLER";
                        renderView(_details,"SELECT_SELLER");
                        break;
                    case 'search-rental':
                        trackShareCouponEvent('Search Property','Modal');
                        _showProcessing(true);
                        getRedirectionUrl({listingCategory:'rent'});
                        break;
                    case 'redirect-makaan':
                        trackShareCouponEvent(`${_details.step}_OKAY`);
                        let url = '/';
                        window.location.href = url;
                        break;
                    case 'share-coupon-gmail':
                        shareCouponWithSeller('Gmail','Popup');
                        break;
                    case 'share-coupon-whatsapp':
                        shareCouponWithSeller('Whatsapp','Popup');
                        break;
                    case 'share-coupon-sms':
                        shareCouponWithSeller('Sms','Popup');
                        break;
                }
            },
            onchange: function(event, element, elementType){
                let data;
                switch(elementType){
                    case 'choose-selectSeller':
                         data = element.dataset;
                        _details.screenData.sellerId = data.sellerId;
                        _details.screenData.sellerName = data.sellerName;
                        _details.screenData.sellerContact = data.sellerContact;
                        _details.screenData.sellerEmail = data.sellerEmail;
                        context.broadcast('trackSellerChosen',{id:moduleEl.id,label:'Select Seller',source:data.sellerId||'',value:_details.screenData.userId});
                        break;
                    case 'choose-nonSelectSeller':
                         data = element.dataset;
                        _details.screenData.nonSelectSellerId = data.sellerId;
                        context.broadcast('trackSellerChosen',{id:moduleEl.id,label:'Non Select Seller',source:data.sellerId||'',value:_details.screenData.userId});
                        break;
                }
            },
            onfocusin: function(event, element, elementType) {
                 switch (elementType) {
                     case 'query-text':
                         if ($moduleEl.find(element).closest('.typeAhead-hl.active')) {
                            //typeAheadView($moduleEl.find(element).closest('.typeAhead-hl'), true);
                             if (element.name === 'user_city_name') {
                                 context.broadcast('typeAheadCityChanged', {
                                     city: ''
                                 });
                             }
                         }
                         break;
                 }
            }
        }
    }));
});