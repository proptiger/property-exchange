define([
    'common/trackingConfigService',
    'services/trackingService'
], function(t, trackingService) {
    'use strict';
    Box.Application.addBehavior('shareCouponTracking', function(context) {

        var element;
        const messages = [
            'trackCouponStepOpen',
            'trackCouponStepClose',
            'trackCouponProceed',
            'trackCouponOkay',
            'trackShareCouponEvent',
            'trackSellerChosen'
        ];

        var init = function() {
            element = context.getElement();
        };

        var onmessage = function(name, data) {
            if (data.id === element.id) {
                let properties = {};
                let event,category = t.SELECT_VOUCHER_REDEMPTION_CATEGORY,
                    label = data.label,
                    value=data.value||'',
                    sourcemodule = data.source || '';
                switch (name) {
                    case 'trackCouponStepOpen':
                        event = 'Modal Open';
                        break;
                    case 'trackCouponStepClose':
                        event = 'Modal Closed';
                        break;
                    case 'trackCouponProceed':
                        event = t.CLICK_EVENT;
                        label = `${label}_PROCEED`;
                        break;
                    case 'trackCouponOkay':
                        event = t.CLICK_EVENT;
                        label = `${label}_OKAY`;
                        value=data.value;
                        break;
                    case 'trackShareCouponEvent':
                        event = t.CLICK_EVENT;
                        break;
                    case 'trackSellerChosen':
                        event = 'Seller Chosen';
                        break;
                    default:
                        return;
                }
                
                properties[t.CATEGORY_KEY] = category;
                properties[t.SOURCE_MODULE_KEY] = sourcemodule;
                properties[t.LABEL_KEY] = label;
                properties[t.USER_ID_KEY] = value;

                trackingService.trackEvent(event, properties);
            }
        };

        return {
            messages,
            onmessage,
            init,
            destroy: function() {}
        };
    });
});