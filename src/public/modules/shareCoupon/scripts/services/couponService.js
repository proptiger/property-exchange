/**
 * This is a client side Module service used for handling all the api call.
 * JIRA : MAKAAN-4194.
 */
'use strict';
define(['common/sharedConfig','services/apiService','services/localStorageService'], function(sharedConfig,apiService,LocalStorageService){
    const SERVICE_NAME = "couponService";
    Box.Application.addService(SERVICE_NAME, (context) => {

        const COUPON_COOKIE_NAME = "SELECT_COUPON_GENERATED";

        function _checkPhoneVerified({phone, response}){

            let isVerified = false,filteredContacts,buyerContact='';

            if(phone){
                filteredContacts = response.contactNumbers.filter(contact => {
                    return contact.contactNumber == phone;
                });
                isVerified = filteredContacts.length ? filteredContacts[0].isVerified : false;
            }else{
                filteredContacts = response.contactNumbers.filter(contact => {
                    return contact.priority == 1;
                });
            }

            buyerContact = filteredContacts.length ? filteredContacts[0].contactNumber:'';

            return {isVerified,buyerContact};
        }

        function getUserData({userId,phone}){
            return apiService.get(sharedConfig.apiHandlers.userDetails(userId).url).then(response => {
                let contactDetails = _checkPhoneVerified({phone, response});
                contactDetails.buyerName  = response && response.fullName && response.fullName!= "user" ? response.fullName : "Tenant" ;
                return contactDetails;
            }, (err) => {
                return err;
            }); 
        }

        function sendOtp({userId,userNumber}){
            return apiService.get(sharedConfig.apiHandlers.authenticateUser({userId,userNumber}).url);
        }
        function checkCouponStatus({userId,expiryDate}){
           return apiService.get(sharedConfig.apiHandlers.couponDetails({userId,expiryDate}).url);
        }
        function getUserByPhone(userNumber){
            return apiService.get(sharedConfig.apiHandlers.userDetailsByContactNumber(userNumber).url);
        }
        function verifyOtp(userId,formData){
            let payload = JSON.stringify(formData);
            return apiService.postJSON(sharedConfig.apiHandlers.authenticateUser({userId}).url,payload);
        }
        function generateCoupon(postData){
            let payload = JSON.stringify(postData);
            return apiService.postJSON(sharedConfig.apiHandlers.couponDetails({userId:'',expiryDate:''}).url,payload);
        }
        function setCouponGeneratedCookie(couponData){
            LocalStorageService.setItem(COUPON_COOKIE_NAME, couponData);
        }
        function shareCoupon(userId,postData){
            let payload = JSON.stringify(postData);
            return apiService.postJSON(sharedConfig.apiHandlers.couponDetails({userId}).url,payload); 
        }
        function fetchBuyerSellers(buyerUserId,saleType){
            return apiService.get(sharedConfig.apiHandlers.buyerSellers({buyerUserId,saleType,sellerUserId:''}).url);
        }
        function saveBuyerSellers(buyerUserId,sellerUserId){
            return apiService.post(sharedConfig.apiHandlers.buyerSellers({buyerUserId,saleType:'',sellerUserId}).url);
        }
        return {
            getUserData,
            sendOtp,
            checkCouponStatus,
            getUserByPhone,
            verifyOtp,
            generateCoupon,
            setCouponGeneratedCookie,
            shareCoupon,
            fetchBuyerSellers,
            saveBuyerSellers
        }
    });
    return Box.Application.getService(SERVICE_NAME);
});