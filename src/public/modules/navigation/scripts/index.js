"use strict";
define([
    'services/utils',
    'doT!modules/navigation/views/index',
    'doT!modules/navigation/views/localityOverview'
    ], (utils, defaultTemplate, localityTemplate) => {
    Box.Application.addModule('navigation', (context) => {
        const DATA_PLACEHOLDER = 'placeholder',
            NAVIGATION_CONTAINER = 'navigation-container',
            NAVIGATION_AREA = 'navigation-area',
            $ = Box.Application.getGlobal('jQuery'),
            WINDOW = $(window),
            FIXED_CLASS = 'posfixed',
            HIDE = 'hidden',
            SELECTED_CLASS = 'active',
            THRESHOLD = 2;

        let template, _navigation, _navigationArea, _extraOffset, _headerOffset, _max_width_id, _tabs, messages = ['showNavigationTab'], currentSection;
            //isHeaderFixed = true;

        function init() {
            _tabs =context.getConfig();
            const TABS = _tabs;
            _max_width_id = TABS.data[0].id;
            template = TABS.templatePath === 'locality' ? localityTemplate : defaultTemplate;
            _generateTemplate(TABS, template);
            _extraOffset = TABS.offset;
            _headerOffset = (!TABS.nonStickyHeader && $('[data-header]').outerHeight()) || 0;
            _navigation = _getNavigationDetails(_extraOffset, _headerOffset, TABS.excludeNavigation);
            _navigationArea = TABS.scrollSpy ? Box.DOM.query(context.getElement(), "[data-placeholder="+NAVIGATION_AREA+"]") : window;
            _bindOnScroll(_navigation, TABS, _extraOffset, _headerOffset);
            _initialSelection(_navigation);
            if (TABS.fixedWidth) {
                _setWidth(_navigation.el, _max_width_id);
            }
        }

        function destroy() {
            _unbindOnScroll();
        }

        function onclick(event, element, elementType) {
        let $el = $(element),
            destinationSelector = '#' + $el.data('goto');
            switch (elementType) {
                case 'navigate':
                    if (!$el.hasClass(SELECTED_CLASS)) {
                        let offset = _calculateOffset(element, _navigation, _extraOffset, _headerOffset);
                        utils.gotoEl(destinationSelector, offset.threshold, offset.offset);
                        context.broadcast('track_navigation', {element});
                    }
                    break;
                case 'scrollSpy':
                    if (_navigationArea && !$el.hasClass(SELECTED_CLASS)) {
                        utils.scrollSpy(_navigationArea, destinationSelector);
                        utils.trackEvent('click','Filter',$(element).data('track-label'));
                    }
                    break;
            }
        }

        function onmessage(name, data) {
            switch (name) {
                case "showNavigationTab":
                    _showNavigationTab.call(undefined, data.id, _navigation);
            }
        }

        function _initialSelection(navigation) {
            $(navigation.el).find('[data-type=navigate]:not(".' + HIDE + '")').eq(0).addClass(SELECTED_CLASS);
        }

        function _showNavigationTab(id, navigation) {
            return $(Box.DOM.query(navigation.el, "[data-goto=" + id + "]")).removeClass(HIDE);
        }

        function _generateTemplate(data, template) {
            updateTabsStatus(data);
            let element = context.getElement(),
                placeholder = Box.DOM.queryData(element, DATA_PLACEHOLDER, 'navigation');
            placeholder.innerHTML = template(data);
        }

        function updateTabsStatus(data) {
            let length = data.data.length;
            data.scrollType = data.scrollSpy ? 'scrollSpy' : 'navigate';
            for (let i = 0; i < length; i++) {
                if ($("#" + data.data[i].id).length > 0) {
                    data.data[i].hide = false;
                }
            }
        }

        function _getNavigationDetails(extraOffset, headerOffset, excludeNavigation) {
            let navigation = {};
            navigation.el = $(context.getElement()).closest('[data-type="navigation"]');
            navigation.navigationOffset = navigation.el.offset();
            navigation.navigationFixedThreshold = navigation.navigationOffset.top - (extraOffset + headerOffset);
            navigation.navigationHeight = navigation.el.outerHeight();
            if(excludeNavigation) {
                navigation.navigationOffset.top = 0;
                navigation.navigationHeight = 0;
            }
            return navigation;
        }

        function _setWidth(element, max_width_id) {
            let width = $("#" + max_width_id).outerWidth();
            element.css("width", width + 'px');
        }

        function _bindOnScroll() {
            $(_navigationArea).on('scroll', _scrollHandler);
        }

        function _unbindOnScroll() {
            $(_navigationArea).off('scroll', _scrollHandler);
        }

        function _scrollHandler() {
            if(_tabs.scrollSpy){
                _checkInView(_navigation, _tabs, _extraOffset, _headerOffset);
            } else {
                _fixNavigation(_navigation, _tabs, _extraOffset, _headerOffset);
            }
        }

        function _fixNavigation(navigation, tabs, extraOffset,headerOffset) {
            let $navigationEl=navigation.el.closest('[data-type=' + NAVIGATION_CONTAINER + ']');
            if (navigation.navigationFixedThreshold <= WINDOW.scrollTop()) {
                $navigationEl.addClass(FIXED_CLASS);
                context.broadcast('navigation-header-fixed-add');

                if($('.header').hasClass('hide')){
                    $navigationEl.removeClass('withheader');
                } else {
                    $navigationEl.addClass('withheader');
                }

            } else {
                $navigationEl.removeClass(FIXED_CLASS);
                context.broadcast('navigation-header-fixed-remove');
            }
            _checkInView(navigation, tabs, extraOffset, headerOffset);
        }

        function _checkInView(navigation, tabs, extraOffset, headerOffset) {
            let data = tabs.data,
                length = data.length,
                //element = null,
                boundaries = {},
                scrollTop = WINDOW.scrollTop(),
                insideBoundaries = false,
                firstNavigationBoundary ;

            for (let i = 0; i < length; i++) {
                boundaries = _generateBoundaries(data[i].id, navigation, extraOffset, headerOffset);
                firstNavigationBoundary = !firstNavigationBoundary && !$.isEmptyObject(boundaries) ? $.extend(true, firstNavigationBoundary, boundaries) : firstNavigationBoundary;
                if (scrollTop >= boundaries.lower && boundaries.upper > scrollTop) {
                    if(!currentSection || currentSection != data[i]){
                        navigation.el.find('.' + SELECTED_CLASS).removeClass(SELECTED_CLASS);
                        navigation.el.find('[data-goto="' + data[i].id + '"]').addClass(SELECTED_CLASS);
                        currentSection = data[i];
                        context.broadcast("scrollSectionChanged", {element: currentSection});
                    }
                    insideBoundaries = true;
                }
            }

            if (!insideBoundaries) {
                if (firstNavigationBoundary && scrollTop < firstNavigationBoundary.lower) {
                    navigation.el.find('.' + SELECTED_CLASS).removeClass(SELECTED_CLASS);
                    navigation.el.find('[data-type=navigate]:not(".' + HIDE + '")').eq(0).addClass(SELECTED_CLASS);
                }else{
                    navigation.el.find('.' + SELECTED_CLASS).removeClass(SELECTED_CLASS);
                }

            }
        }

        function _generateBoundaries(id, navigation, extraOffset, headerOffset) {
            var element = $('#' + id),
                boundaries = {},
                margin = parseInt(element.css('margin-bottom')),
                offset = element.offset();
            if (offset) {
                boundaries.lower = offset.top - (navigation.navigationHeight + THRESHOLD + extraOffset + margin + headerOffset);
                boundaries.upper = offset.top + element.outerHeight() - (navigation.navigationHeight + THRESHOLD + extraOffset + headerOffset);
            }
            return boundaries;
        }

        function _calculateOffset(element, navigation, extraOffset, headerOffset) {
            let result = {},
                gotoElement = $('#' + $(element).data('goto')),
                margin = parseInt(gotoElement.css('margin-bottom')),
                gotoOffset = gotoElement.offset().top;
            if (navigation.el.closest('[data-type=' + NAVIGATION_CONTAINER + ']').css("position").toLowerCase() !== 'fixed') {
                result = {
                    "threshold": navigation.navigationOffset.top + headerOffset,
                    "offset": navigation.navigationHeight * 2 + extraOffset + margin + headerOffset
                };
                if (navigation.navigationFixedThreshold > gotoOffset - result.offset) {
                    result.offset = gotoOffset - (navigation.navigationFixedThreshold);
                }
            } else {
                result = {
                    "threshold": navigation.navigationOffset.top - headerOffset,
                    "offset": navigation.navigationHeight + extraOffset + margin + headerOffset
                };
            }
            return result;
        }

        return {
            init,
            onclick,
            destroy,
            messages,
            onmessage
        };
    });
});
