'use strict';
define([
    'services/loggerService',
    'services/utils',
    'common/sharedConfig',
    'services/localStorageService',
    'services/filterConfigService',
    'services/urlService',
    'services/defaultService',
    'services/leadPyrService',
    'services/apiService'
], (Logger, Utils, SharedConfig, LocalStorageService, filterConfigService, UrlService, DefaultService) => {
    const SERVICE_NAME = 'LeadService';
    Box.Application.addService(SERVICE_NAME, (application) => {
        const ApiService = application && application.getService('ApiService'),
            leadPyrService = application && application.getService('LeadPyrService') || {};

        const defaultConfig = {
            required: {
                "companyId": null,
                "companyName": null,
                "companyPhone": null,
                "companyUserId": null
            },

            optional: {
                "name": null,
                "email": null,
                "phone": null,
                "salesType": null,
                "countryId": null,
                "cityId": null,
                "cityName": null,
                "minBudget": null,
                "maxBudget": null,
                "localityId": null,
                "projectId": null,
                "propertyType": null,
                "bhk": null,
                "enquiryType": null,
                "companyRating": null,
                "companyImage": null,
                "companyAssist": null,
                "companyType": null,
                "sendOtp": true,
                "projectName": null,
                "propertyTypeLabel": null,
                "companyMatchingProperties": null,
                "otherCompany": [],
                "listingImage": null,
                "imageUrl": null,
                "priceRanges": null,
                "listingId": null,
                "configurationText": null,
                "masterReasons": null,
                "optIn": null,
                "optInCheckedStatus": null,
                "mainHeading": null,
                "hideSellersImages": null,
                "sellerType": null,
                "paidLeadCount": null,
                "isAccountLocked": null,
                "isCommercial": null,
            },
            extra: {
                "others": {
                    "userAgent": window.navigator.userAgent
                }
            },
            defaultValue: {
                "domainId": 1,
                "enquiryType": {
                    "id": 2
                },
                "applicationType": Utils.isMobileRequest() ? 'Mobile Site' : 'Desktop Site',
                "pageName": Utils.getPageData('pageType') || '',
                "pageUrl": window.location.pathname,
                "optInCheckedStatus": true
            },
            displayFields: [
                "companyName",
                "companyRating",
                "companyPhone",
                "companyImage",
                "companyAssist",
                "companyUserId",
                "companyType",
                "otherCompany",
                "projectName",
                "companyMatchingProperties",
                "propertyTypeLabel",
                "imageUrl",
                "priceRanges",
                "configurationText",
                "masterReasons",
                "optInCheckedStatus",
                "mainHeading",
                "hideSellersImages",
                "sellerType",
                "paidLeadCount",
                "isAccountLocked"
            ],
            allowedMultipleValue: [
                'companyId',
                'localityId',
                'propertyType'
            ],
            manualFields: ['name', 'email', 'phone', 'jsonDump', 'multipleCompanyIds', 'cityName', 'cityId', 'salesType', 'companyType', "pageUrl", "pageName", "maxBudget", "countryId", "optInCheckedStatus", "optIn", "showOptInBox", "isVerified"],
            trackFields: {
                "builderId": null,
                "rank": null,
                "budget": null,
                "bhk": null,
                "projectStatus": null,
                "source": null,
                "isMakaanSelectSeller":false
            }
        };
        const config = {
            defautStep: "MAIN",
            steps: ["MAIN", "OTP", "SIMILAR", "THANKS", "VIEW_PHONE", "VIEW_PHONE_MORPHED", "MAIN_TOP_SELLER", "CALL_STATUS_SCREEN", "SIMILAR_SELLERS", "CALL_NOW",'USER_DETAILS','SHOW_SELECT_COUPON'],
            cookies: {
                userMedium: "USER_MEDIUM",
                userFrom: "USER_FROM",
                userNetwork: "USER_NETWORK",
                userIp: "USER_IP"
            },
            apiKeyMap: {
                companyId: "multipleCompanyIds",
                localityId: "localityIds",
                propertyType: "propertyTypes",
                others: "jsonDump"
            },
            modulesParameters: {
                "isPopup": null,
                "similarType": "LISTING",
                "skipSimilar": false,
                "id": null,
                "phoneElemSelector": null,
                "targetId": undefined,
                "nameElemSelector": null,
                "emailElemSelector": null
            },
            enquiryTypesMapping: {
                PYR: 1,
                GET_CALLBACK: 2,
                CALL_NOW: 3,
                GET_INSTANT_CALLBACK: 4,
                CONTACT_SELLER: 5,
                VIEW_PHONE: 7,
                AUTO_PYR: 9,
                SIMILAR_SELLERS: 8,
                SHORTLIST_PYR: 14
            },
            indiaCountryCode: 1
        };
        const stepsConfig = {
            MAIN: {
                required: defaultConfig.required, //$.extend(true,{"listingId": null}, defaultConfig.required),
                optional: defaultConfig.optional,
                userFields: ['phone', 'countryId', "optIn"],
                defaultValue: defaultConfig.defaultValue,
                extra: defaultConfig.extra,
                displayFields: defaultConfig.displayFields,
                trackFields: defaultConfig.trackFields,
                allowedMultipleValue: defaultConfig.allowedMultipleValue,
                manualFields: defaultConfig.manualFields,
                responseKeyMap: {
                    "id": "tempEnquiryId",
                    "userId": "userId",
                    "reqIsVerified": "isVerified",
                    "callId": "callId"
                }
            },
            MAIN_TOP_SELLER: {
                required: defaultConfig.required,
                optional: defaultConfig.optional,
                userFields: ['phone', 'countryId', 'optIn'],
                defaultValue: defaultConfig.defaultValue,
                extra: defaultConfig.extra,
                displayFields: defaultConfig.displayFields,
                trackFields: defaultConfig.trackFields,
                allowedMultipleValue: defaultConfig.allowedMultipleValue,
                manualFields: defaultConfig.manualFields,
                responseKeyMap: {
                    "id": "tempEnquiryId",
                    "userId": "userId",
                    "reqIsVerified": "isVerified",
                    "callId": "callId"
                }
            },
            OTP: {
                required: $.extend(true, { "enquiryId": null }, defaultConfig.required),
                optional: defaultConfig.optional,
                userFields: ['otp','name', 'email'],
                defaultValue: defaultConfig.defaultValue,
                extra: defaultConfig.extra,
                displayFields: defaultConfig.displayFields,
                trackFields: defaultConfig.trackFields,
                allowedMultipleValue: defaultConfig.allowedMultipleValue,
                manualFields: defaultConfig.manualFields,
                responseKeyMap: {
                    "otpValidationSuccess": "isVerified",
                    "userId": "userId",
                    "callId": "callId"
                }
            },
            USER_DETAILS: {
                required: defaultConfig.required,
                optional: defaultConfig.optional,
                userFields: ['name', 'email'],
                defaultValue: defaultConfig.defaultValue,
                extra: defaultConfig.extra,
                displayFields: defaultConfig.displayFields,
                trackFields: defaultConfig.trackFields,
                allowedMultipleValue: defaultConfig.allowedMultipleValue,
                manualFields: defaultConfig.manualFields,
                responseKeyMap: {}
            },
            SHOW_SELECT_COUPON: {
                required: defaultConfig.required,
                optional: defaultConfig.optional,
                userFields: [],
                defaultValue: defaultConfig.defaultValue,
                extra: defaultConfig.extra,
                displayFields: defaultConfig.displayFields,
                trackFields: defaultConfig.trackFields,
                allowedMultipleValue: defaultConfig.allowedMultipleValue,
                manualFields: defaultConfig.manualFields,
                responseKeyMap: {
                   "id":"couponId",
                   "code":"couponCode",
                   "amount":"couponAmount",
                   "generatorUserId":"buyerUserId",
                   "expiryDate":"couponExpiryDate",
                   "isNew":"isNew",
                   "notificationSent":'notificationSent',
                   "displayCode":"displayCode",
                   "displayContact":"displayContact"
                }
            },
            SIMILAR: {
                required: {
                    "listingId": null,
                    "companyId": null,
                    "sellerId": null
                },
                optional: $.extend(true, {
                    callingNumber: null,
                    companyName: null,
                    companyPhone: null
                }, defaultConfig.optional),
                userFields: ['phone', 'countryId'],
                defaultValue: $.extend(defaultConfig.defaultValue, {
                    sendOtp: false
                }),
                trackFields: defaultConfig.trackFields,
                extra: defaultConfig.extra,
                displayFields: defaultConfig.displayFields,
                allowedMultipleValue: defaultConfig.allowedMultipleValue,
                manualFields: defaultConfig.manualFields, //['name','email','phone','cityName','cityId'],
                responseKeyMap: {}
            },
            SIMILAR_SELLERS: {
                required: {
                    "listingId": null,
                    "companyId": null
                },
                optional: $.extend(true, {
                    callingNumber: null,
                    companyName: null,
                    companyPhone: null
                }, defaultConfig.optional),
                userFields: ['phone', 'countryId'],
                defaultValue: $.extend(defaultConfig.defaultValue, {
                    sendOtp: false
                }),
                trackFields: defaultConfig.trackFields,
                extra: defaultConfig.extra,
                displayFields: defaultConfig.displayFields,
                allowedMultipleValue: defaultConfig.allowedMultipleValue,
                manualFields: defaultConfig.manualFields, //['name','email','phone','cityName','cityId'],
                responseKeyMap: {}
            },
            THANKS: {
                required: {},
                optional: {
                    "name": null
                },
                userFields: [],
                defaultValue: {},
                extra: {},
                displayFields: defaultConfig.displayFields,
                trackFields: defaultConfig.trackFields,
                allowedMultipleValue: [],
                manualFields: [],
                responseKeyMap: {}
            },
            VIEW_PHONE: {
                required: {
                    "companyName": null,
                    "companyPhone": null
                },
                optional: {
                    "companyType": null
                },
                userFields: ['phone', 'countryId'],
                defaultValue: {},
                trackFields: defaultConfig.trackFields,
                extra: {},
                displayFields: defaultConfig.displayFields,
                allowedMultipleValue: [],
                manualFields: [],
                responseKeyMap: {}
            },
            VIEW_PHONE_MORPHED: {
                required: $.extend(true, { "listingId": null }, defaultConfig.required),
                optional: defaultConfig.optional,
                userFields: ['phone', 'countryId', 'optIn'],
                defaultValue: defaultConfig.defaultValue,
                trackFields: defaultConfig.trackFields,
                extra: defaultConfig.extra,
                displayFields: defaultConfig.displayFields,
                allowedMultipleValue: defaultConfig.allowedMultipleValue,
                manualFields: defaultConfig.manualFields,
                responseKeyMap: {
                    "id": "tempEnquiryId",
                    "userId": "userId",
                    "reqIsVerified": "isVerified",
                    "callId": "callId"
                }
            },
            CALL_NOW: {
                required: $.extend(true, { "listingId": null }, defaultConfig.required),
                optional: defaultConfig.optional,
                userFields: ['phone', 'countryId', "name", "email", "optIn"],
                defaultValue: defaultConfig.defaultValue,
                trackFields: defaultConfig.trackFields,
                extra: defaultConfig.extra,
                displayFields: defaultConfig.displayFields,
                allowedMultipleValue: defaultConfig.allowedMultipleValue,
                manualFields: defaultConfig.manualFields,
                responseKeyMap: {
                    "id": "tempEnquiryId",
                    "userId": "userId",
                    "reqIsVerified": "isVerified"
                }
            },
            CALL_STATUS_SCREEN: {
                required: $.extend(true, { "listingId": null }, defaultConfig.required),
                optional: defaultConfig.optional,
                userFields: ['phone', 'countryId'],
                defaultValue: defaultConfig.defaultValue,
                extra: defaultConfig.extra,
                displayFields: defaultConfig.displayFields,
                allowedMultipleValue: defaultConfig.allowedMultipleValue,
                manualFields: defaultConfig.manualFields,
                responseKeyMap: {
                    "callId": "callId",
                    "userId": "userId"
                }
            },
        };
        var _prune = leadPyrService.prune;
        var _getOtherCompanyIds = leadPyrService.getOtherCompanyIds;
        var _getUserDataFromCookies = leadPyrService.getUserDataFromCookies;
        var _updateStepData = leadPyrService.updateStepData;
        var _updateDummyFields = leadPyrService.updateDummyFields;
        let virtualNumbers = {};
        function _updateOtherParameters(config, processedData = {}, moduleData = {}) {
            processedData['others'] = config.extra.others || {};
            $.each(moduleData, function(key, val) {
                if (!config.required[key] && !config.optional) {
                    processedData['others'][key] = val;
                }
            });
        }

        function _updateApiKeys(data, map) {
            for (let i in map) {
                if (data[i]) {
                    let key = map[i];
                    if ($.isPlainObject(key)) {
                        if (key.type === 'array' && key.typeName) {
                            data[key.typeName] = data[key.typeName] || [];
                            data[key.typeName].push(data[i]);
                        } else if (key.type === 'object' && key.typeName) {
                            data[key.typeName] = data[key.typeName] || {};
                            data[key.typeName][key.name] = data[i];
                        } else {
                            data[key.name] = data[i];
                        }
                    } else {
                        data[key] = data[i];
                    }
                    delete data[i];
                }
            }
        }

        function getPushNotificationData(data) {
            let postData = data.postData,
                displayData = data.displayData;
            if (postData && displayData) {
                let callId = data.response[data.initialStep].callId || data.response[data.step].callId || data.response[data.prevStep].callId,
                    sellerId = postData.multipleCompanyIds && postData.multipleCompanyIds.join(),
                    localityId = postData.localityIds,
                    cityId = postData.cityId,
                    bhk = postData.bhk,
                    budget = displayData.maxBudget,
                    reason = postData.reason;
                return { callId, sellerId, localityId, cityId, bhk, budget, reason };
            }
        }

        function _updateManualFields(data, fields) {
            let cookiesData = _getUserDataFromCookies();
            let sessionData = LocalStorageService.getSessionItem('leadDrip') || {};
            let processedData = data.processedData;
            let displayData = data.displayData;
            let rawData = data.rawData;
            $.each(fields, (key, val) => {
                if (val == 'jsonDump') {
                    processedData[val] = JSON.stringify(processedData[val]);
                } else if (val == 'name') {
                    processedData[val] = processedData[val] || cookiesData[val];
                } else if (val == 'email') {
                    processedData[val] = processedData[val] || cookiesData[val];
                } else if (val == 'phone') {
                    processedData[val] = processedData[val] || cookiesData[val];
                    processedData[val] = processedData[val] ? processedData[val].toString().replace(/\s/g, '') : processedData[val];
                } else if (val == 'countryId') {
                    processedData[val] = processedData[val] || cookiesData[val] || 1;
                } else if (val == 'cityName') {
                    processedData[val] = processedData[val] || Utils.getPageData('cityName');
                } else if (val == 'cityId') {
                    processedData[val] = processedData[val] || Utils.getPageData('cityId');
                } else if (val == 'optInCheckedStatus') {
                    displayData[val] = ("smartMultiplication" in rawData) ? rawData.smartMultiplication : leadPyrService.getOptInCheckedStatus(cookiesData);
                } else if (val == 'optIn') {
                    processedData[val] = ("smartMultiplication" in rawData) ? rawData.smartMultiplication : (processedData[val] || leadPyrService.getOptInCheckedStatus(cookiesData));
                } else if (val == 'multipleCompanyIds') {
                    if (!$.isArray(processedData[val])) {
                        processedData[val] = [processedData[val]];
                    }
                    processedData[val] = Utils.unique(processedData[val].concat(_getOtherCompanyIds(displayData)));
                } else if (val == 'salesType') {
                    processedData[val] = (processedData[val] && processedData[val].toLowerCase()) || Utils.getPageData('listingType');
                    processedData[val] = processedData[val] && processedData[val].toLowerCase();
                    if (['rent', 'rental'].indexOf(processedData[val]) !== -1) {
                        processedData[val] = 'rent';
                    } else {
                        processedData[val] = 'buy';
                    }
                } else if (val == 'companyType') {
                    displayData[val] = displayData[val] && displayData[val].toLowerCase() == 'broker' ? 'agent' : displayData[val];
                } else if (val == 'maxBudget') {
                    displayData.price = (processedData[val] && Utils.formatNumber(processedData[val], { precision : 2, returnSeperate : true, seperator : window.numberFormat })) || {};
                } else if (val == 'pageUrl') {
                    processedData[val] = window.location.pathname + encodeURIComponent(window.location.search);
                } else if (val == 'pageName') {
                    processedData[val] = Utils.getPageData('pageType');
                } else if (val == 'showOptInBox') {
                    displayData[val] = ("smartMultiplication" in rawData) ? rawData.smartMultiplication : true;
                }
                else if(val == 'isVerified') {
                    if(typeof sessionData[val] === 'boolean'){
                        processedData["phone"] = sessionData["phone"];
                    }
                    processedData[val] = sessionData[val];
                }
                data[val] = (typeof data[val] == "string" && data[val].trim()) || data[val];
            });
            return {
                processedData,
                rawData,
                displayData
            };
        }

        function updateUserFields(formData, data, step) {
            let fields = stepsConfig[step].userFields;
            if (!fields) {
                return;
            }
            $.each(fields, (key, val) => {
                data[val] = typeof(formData[val]) !== "undefined" ? formData[val] : data[val];
            });
        }

        function _processOtherCompany(stepConfig, displayData) {
            if (displayData.otherCompany && displayData.otherCompany.length > 0) {
                let temp = {};
                displayData.otherCompany = displayData.otherCompany.map((company) => {
                    temp = {};
                    temp = $.extend(true, temp, _updateStepData(false, stepConfig.required, company));
                    temp = $.extend(true, temp, _updateStepData(false, stepConfig.optional, company));
                    temp.companyAvatar = _getAvatar(company.companyName);
                    _prune(temp);
                    return temp;
                });
            }
        }

        function _getAvatar(companyName) {
            return (companyName && Utils.getAvatar(companyName)) || {};
        }

        function _hideNumber(phone) {
            if (!phone) {
                return "";
            }
            phone = phone.toString();
            let starString = "";
            for (let j = 0; j < phone.length - 4; j++) {
                starString += "*";
            }
            return phone.substr(0, 2) + starString + phone.substr(-2);
        }

        function _updateDisplayData(stepConfig, processedData) {
            let displayData = _prune(processedData, stepConfig.displayFields);

            displayData.callingNumber = displayData.companyPhone;
            displayData.cryptedPhoneNumber = _hideNumber(displayData.companyPhone);
            displayData.companyAvatar = _getAvatar(displayData.companyName);

            _processOtherCompany(stepConfig, displayData);
            _prune(displayData);
            return displayData;
        }

        function _getDefaultResponseForProcessedData() {
            let response = {};
            $.each(config.steps, (key, val) => {
                response[val] = {};
            });
            return response;
        }

        function processStepData(rawData, step) {
            return leadPyrService.processStepData({
                rawData,
                step,
                updateManualFields: _updateManualFields,
                updateDisplayData: _updateDisplayData,
                updateApiKeys: _updateApiKeys,
                stepsConfig,
                config,
                getDefaultResponseForProcessedData: _getDefaultResponseForProcessedData,
                updateOtherParameters: _updateOtherParameters
            });
        }

        function _getUTMParameters() {
            let utm = {};
            utm.userMedium = leadPyrService.stripQuotes(Utils.getCookie(config.cookies.userMedium));
            utm.userFrom = leadPyrService.stripQuotes(Utils.getCookie(config.cookies.userFrom));
            utm.userNetwork = leadPyrService.stripQuotes(Utils.getCookie(config.cookies.userNetwork));
            return utm;
        }

        function _processCallInitData(moduleData) {
            let modulePayload = moduleData.postData,
                moduleDisplayData = moduleData.displayData,
                socket = LocalStorageService.getItem('jStorage') || {},
                jsonData = {};

            jsonData = $.extend(true, jsonData, modulePayload, _getUTMParameters());
            if(jsonData.localityIds){
                jsonData.localityId = $.isArray(jsonData.localityIds) ? jsonData.localityIds[0] : jsonData.localityIds;
            }
            jsonData.tempEnquiryId = moduleData.response[moduleData.initialStep].tempEnquiryId;
            jsonData.socketId = socket.session_id;
            jsonData.moduleId = moduleData.moduleParameters.id;
            jsonData.userIp = leadPyrService.stripQuotes(Utils.getCookie(config.cookies.userIp));
            if (!$.isArray(jsonData.multipleCompanyIds)) {
                jsonData.multipleCompanyIds = [jsonData.multipleCompanyIds];
            }
            jsonData.companyId = jsonData.multipleCompanyIds[0];
            _prune(jsonData, ['jsonDump', 'enquiryType', 'multipleCompanyIds', 'propertyTypes', 'localityIds','bhk']);

            _updateDummyFields(jsonData, ['email', 'name']);

            let callPayLoad = {
                "fromNo": modulePayload.phone,
                "listingCategory": modulePayload.salesType,
                "cityId": modulePayload.cityId,
                "userId": moduleDisplayData.companyUserId,
                "clientCountryId": modulePayload.countryId,
                "toNo": moduleDisplayData.companyPhone,
                "jsonDump": JSON.stringify(jsonData)
            };

            return callPayLoad;
        }

        function connectBuyerSeller(data) {
            let getInstallCallApi = SharedConfig.apiHandlers.getInstantCall().url,
                failedEnquiryApi = SharedConfig.apiHandlers.failedEnquiryPush().url,
                enquiryApi = SharedConfig.apiHandlers.postEnquiry().url;


            let payload;
            let callerPayload = _processCallInitData(data);
            let extraPayload = {
                data: data.postData
            };
            payload = JSON.stringify({ callerPayload, extraPayload });
            return ApiService.postJSON(getInstallCallApi, payload).then(function(response) {
                let responseData = {};
                response = (response && response.data) || {};
                responseData.callId = response.id;
                responseData.userId = response.communicationLog && response.communicationLog.callerUserId;
                responseData.callingNumber = response.virtualNumberMapping && response.virtualNumberMapping.virtualNumber && response.virtualNumberMapping.virtualNumber.number;
                return responseData;
            }, function(error) {
                ApiService.postJSON(failedEnquiryApi, JSON.stringify({
                    payload: data.postData,
                    api: enquiryApi
                }));
                return error;
            });
        }

        function processDataAccordingToLead(response) {
            $.each(response, (k, v) => {
                v.step = "similar";
                v.processed = encodeURI(JSON.stringify(processStepData(v).postData));
            });
            return response;
        }

        function getSimilarListing(listingId, companyId, count=5, isCommercial = false, distinctSeller = false) {
            let similarListingUrl = SharedConfig.apiHandlers.getSimilarListing({
                listingId,
                companyId,
                isCommercial,
                query: {count, distinctSeller}
            }).url;
            return ApiService.get(similarListingUrl).then(function(response) {
                if (!$.isArray(response)) {
                    return [];
                }
                return processDataAccordingToLead(response);
            }, function(error) {
                throw error;
            });
        }

        function _updateMultipleSellerIds(data) {
            data.multipleSellerIds = [data.sellerId];
            delete data.sellerId;
        }

        function postSimilarListingEnquiry(data = {}, enquiryTypeId = 0) {
            let postEnquiry = SharedConfig.apiHandlers.postEnquiry().url;
            let apis = [];
            $.each(data, (k, v) => {
                _updateDummyFields(v, ['email', 'name']);
                _updateEnquiryType("SIMILAR", { postData: v }, enquiryTypeId);
                _updateMultipleSellerIds(v);
                apis.push(ApiService.postJSON(postEnquiry, v));
            });
            return Promise.all(apis).then((response) => {
                return response;
            }, (error) => {
                throw error;
            });
        }

        function isIndian(data) {
            return leadPyrService.isIndian(data.postData.countryId);
        }

        function updateOTPFlag(data) {
            let isIndianFlag = isIndian(data);
            return leadPyrService.checkOTPVerified(data.postData.phone, data.postData.isVerified).then((response) => {
                data.postData.sendOtp = isIndianFlag && !response.isVerified;
                data.isLoggedIn = response.isLoggedIn;
                data.buyerUserId = response.buyerUserId;
            }, (response) => {
                data.postData.sendOtp = isIndianFlag && !response.isVerified;
                data.isLoggedIn = response.isLoggedIn;
                data.buyerUserId = response.buyerUserId;
            });
        }

        function _getDeliveryId() {
            let socket = LocalStorageService.getItem('jStorage') || {};
            return socket.session_id;
        }

        function _postLead(api, data) {
            let postData = {};
            data.postData = data.postData || {};
            data.displayData = data.displayData || {};
            postData = $.extend(true, postData, data.postData);
            /* extra attributes appended for view form feedback case */
            postData.deliveryId = _getDeliveryId();
            if (data.displayData.companyUserId) {
                postData.companyUserId = data.displayData.companyUserId;
            }
            postData.multipleSellerIds = leadPyrService.setMultipleSellerIDs(data);
            _prune(postData, ["otp"]);
            return leadPyrService.postLead(api, postData);
        }

        function _updateEnquiryType(step, data, enquiryTypeId) {
            data.postData.enquiryType = data.postData.enquiryType || {};
            if (enquiryTypeId) {
                data.postData.enquiryType.id = enquiryTypeId;
                return;
            }
            enquiryTypeId = data.postData.enquiryType.id;
            if (!enquiryTypeId) {
                enquiryTypeId = config.enquiryTypesMapping.GET_CALLBACK;
            }
            switch (step) {
                case "VIEW_PHONE_MORPHED":
                    enquiryTypeId = config.enquiryTypesMapping.VIEW_PHONE;
                    break;
                case "MAIN":
                    enquiryTypeId = config.enquiryTypesMapping.GET_INSTANT_CALLBACK;
                    break;
                case "MAIN_TOP_SELLER":
                    if (data.displayData.configurationText && data.displayData.configurationText.type === 'favourite') {
                        enquiryTypeId = config.enquiryTypesMapping.SHORTLIST_PYR;
                    } else if (data.displayData.configurationText) {
                        enquiryTypeId = config.enquiryTypesMapping.PYR;
                    } else {
                        enquiryTypeId = config.enquiryTypesMapping.GET_INSTANT_CALLBACK;
                    }
                    break;
                case "CALL_NOW":
                    enquiryTypeId = config.enquiryTypesMapping.CALL_NOW;
                    break;
                case "SIMILAR":
                case "SIMILAR_SELLERS":
                    enquiryTypeId = config.enquiryTypesMapping.SIMILAR_SELLERS;
                    break;
            }
            data.postData.enquiryType.id = enquiryTypeId;
        }

        function postEnquiry(data, step, enquiryTypeId) {
            let postEnquiry = SharedConfig.apiHandlers.postEnquiry().url;
            step = step || data.step;
            _updateEnquiryType(step, data, enquiryTypeId);
            return _postLead(postEnquiry, data);
        }
        function postUserDetails(data) {
            let api = SharedConfig.apiHandlers.userDetails(data.id).url;
            return leadPyrService.postUserDetails(api, data);
        }

        function postTemporaryLead(data, enquiryTypeId) {
            let postEnquiry = SharedConfig.apiHandlers.postTempEnquiry().url;
            _updateEnquiryType(data.step, data, enquiryTypeId);
            return _postLead(postEnquiry, data);
        }
        function verifyTempEnquiry({tempEnquiryId}) {
            let postEnquiry = SharedConfig.apiHandlers.verifyTempEnquiry().url;
            return leadPyrService.verifyTempEnquiry(postEnquiry, {tempEnquiryId});
        }

        function verifyOTP(data) {
            let postEnquiry = SharedConfig.apiHandlers.postTempEnquiry().url,
                userEnquiryId = data.response[data.initialStep].tempEnquiryId || data.response[data.step].tempEnquiryId || data.response[data.prevStep].tempEnquiryId,
                userId = data.response[data.initialStep].userId || data.response[data.step].userId || data.response[data.prevStep].userId;
            let putData = {
                userEnquiryId: userEnquiryId,
                otp: data.postData.otp,
                phone: data.postData.phone,
                email: data.postData.email,
                name: data.postData.name,
                userId: userId
            };
            return leadPyrService.verifyOTP(postEnquiry, putData);
        }

        function getSellerData(sellerUserId) {
            let url = SharedConfig.apiHandlers.getRawSeller({ sellerUserId }).url;
            return ApiService.get(url).then((response) => {
                return response;
            });
        }

        function updateResponse(step, data, response) {
            $.each(stepsConfig[step]["responseKeyMap"], (apiKey, codeKey) => {
                data["response"][step][codeKey] = response[apiKey] !== undefined && response[apiKey] !== null ? response[apiKey] : data["response"][step][codeKey];
            });
        }

        function getSimilarProjectTopSeller() {
            return new Promise((resolve, reject) => {
                reject();
            });
        }

        function getCallingNumber(userId, cityId, listingType) {
            listingType = listingType == "rent" ? "rent" : "sell";
            let key = userId+'_'+cityId+'_'+listingType;
            if(virtualNumbers[key]){
                return Utils.getEmptyPromise(virtualNumbers[key]);
            }
            let url = SharedConfig.apiHandlers.getVirtualNumber({ userId, cityId, listingType, location: window.location.href }).url;
            return ApiService.get(url).then((response) => {
                response.callingNumber = response.callingNumber || window.GLOBAL_VIRTUAL_NUMBER;
                virtualNumbers[key] = response;
                return response;
            });
        }

        function _getRatingPayload(data) {
            //let socket = LocalStorageService.getItem('jStorage') || {};
            let callId = data.response[data.initialStep].callId || data.response[data.step].callId || data.response[data.prevStep].callId;
            let payload = {
                rating: data.postData.rating,
                callId,
                socketId: _getDeliveryId(),
                moduleId: data.moduleParameters.id,
                sellerId: data.displayData.companyUserId
            };
            if (data.postData.reason) {
                payload.reason = data.postData.reason;
            }
            return payload;
        }

        function submitRating(data) {
            let url = SharedConfig.apiHandlers.callRating().url;
            let payload = _getRatingPayload(data);
            return ApiService.postJSON(url, payload);
        }

        function getModuleParameters(rawData) {
            return leadPyrService.getModuleParameters(rawData, config);
        }

        function parseUrlQueryParams(){
            let queryParams = UrlService.getAllUrlParam();
            let encUserId = queryParams[Utils.getLeadDripUserIdKey()];
            let leadDripParam = {};
            if(encUserId){
                encUserId = decodeURIComponent(encUserId);
                let promise = DefaultService.getDecryptedTokenValue(encUserId);
                promise.then(response => {
                    if(response && response.userId){
                        ApiService.get(SharedConfig.apiHandlers.userDetails(response.userId).url).then(result => {
                            if(result && result.contactNumbers && result.contactNumbers.length){
                                let contactNumbers = result.contactNumbers.filter(item => item.isVerified);
                                if(contactNumbers.length){
                                    leadDripParam =  {phone:contactNumbers[0].contactNumber, userId: response.userId, isVerified: true};
                                } else {
                                    leadDripParam =  {phone:result.contactNumbers[0].contactNumber, userId: response.userId, isVerified: false};
                                }
                            }
                            LocalStorageService.setSessionItem('leadDrip', leadDripParam);
                        });
                    }
                });
            }
        }

        let updateOptInStatus = leadPyrService.updateOptInStatus;

        return {
            processStepData,
            updateUserFields,
            connectBuyerSeller,
            getSimilarListing,
            postSimilarListingEnquiry,
            postTemporaryLead,
            updateResponse,
            verifyOTP,
            updateOTPFlag,
            getSimilarProjectTopSeller,
            postEnquiry,
            postUserDetails,
            isIndian,
            getCallingNumber,
            submitRating,
            getSellerData,
            getPushNotificationData,
            processDataAccordingToLead,
            getModuleParameters,
            updateOptInStatus,
            parseUrlQueryParams,
            verifyTempEnquiry
        };
    });
    return Box.Application.getService(SERVICE_NAME);
});
