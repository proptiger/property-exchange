'use strict';
define(['services/loggerService',
    'services/utils',
    'common/sharedConfig',
    'services/apiService',
    'common/trackingConfigService',
], (Logger, Utils, SharedConfig, ApiService, t) => {
    Box.Application.addService('TrackLeadEvents', (application) => {
        let config = {},
            moduleId;

        function setTrackingConfig() {
            let pageType = Utils.getTrackPageCategory();
            config = {
                PAGE_TYPE: pageType,
                PAGE_CATEGORY: 'Buyer_' + pageType,
                LEAD_SUBMIT: 'lead_submit',
                LEAD_STORED: 'lead_stored',
                LEAD_CLOSE: 'CLOSE_lead',
                LEAD_BACK: 'lead_back',
                LEAD_DISMISS: 'lead_dismiss',
                LEAD: 'lead'
            };
        }

        function _getSimilarListingsLabel(data) {
            let allListingId = [],
                allListingIdLabels = [],
                label, ids;
            $.each(data, (k, v) => {
                let rank;
                try {
                    rank = parseInt(k) + 1;
                } catch (ex) {
                    rank = k + 1;
                }
                if (v && v.listingId) {
                    allListingId.push(v.listingId);
                    allListingIdLabels.push(v.listingId + '_' + rank);
                }
            });
            ids = allListingId.join(",");
            label = "similar_" + allListingIdLabels.join(",") + "_" + Object.keys(data).length;

            return {
                label, ids
            };
        }

        function trackLeadModuleEvent(data, type, eventdata, id) {
            moduleId = id;
            let action;
            switch (type) {
                case "MAIN_SUBMIT_CLICKED":
                case "MAIN_TOP_SELLER_SUBMIT_CLICKED":
                    action = config["LEAD_SUBMIT"] + '_connect_now';
                    application.broadcast('trackConnectNowLead', _getTrackData(data, eventdata));
                    break;
                case "CALL_NOW_SUBMIT_CLICKED":
                    application.broadcast('trackCallNowLead', _getTrackData(data, eventdata));
                    break;
                case "VIEW_PHONE_MORPHED_SUBMIT_CLICKED":
                     application.broadcast('viewPhoneSubmitLead', _getTrackData(data, eventdata));
                     break;
                case "VIEW_PHONE_MORPHED_SEEN":
                     application.broadcast('trackViewPhoneSeenMorphedLead', _getTrackData(data, eventdata));
                     break;
                case "MAIN_CONNECT_NOW_SUCCESS":
                    action = config["LEAD_STORED"] + '_connect_now';
                    break;
                case "MAIN_VIEW_PHONE":
                    action = config["LEAD_SUBMIT"] + '_view_number';
                    application.broadcast('trackViewPhoneLead', _getTrackData(data, eventdata));
                    break;
                case "SIMILAR_LEAD_SUBMIT_SUCCESS":
                    action = config["LEAD_STORED"] + '_similar';
                    let labelData = _getSimilarListingsLabel(data);
                    break;
                case "SIMILAR_SELLERS_LEAD_SUBMIT_CLICKED":
                    let _eventData = _getTrackData(data, eventdata);
                    _eventData.label = "similarSeller";
                    _eventData.sectionClicked = "similarSellersYes";
                    application.broadcast('trackSimilarSubmitLead',_eventData);
                    break;
                case "SIMILAR_LEAD_SUBMIT_CLICKED":
                    _eventData = _getTrackData(data, eventdata);
                    _eventData.label = "similar";
                    _eventData.sectionClicked = "similarPropertiesShare";
                    application.broadcast('trackSimilarSubmitLead',_eventData);
                    break;
                case "OTP_VISIBLE":
                    application.broadcast("trackOtpViewedLead", _getTrackData(data, eventdata));
                    break;
                case 'OTP_CORRECT':
                    application.broadcast("trackOtpCorrectLead", _getTrackData(data, eventdata));
                    break;
                case 'OTP_INCORRECT':
                    application.broadcast("trackOtpIncorrectLead", _getTrackData(data, eventdata));
                    break;
                case 'OTP_RESEND':
                    application.broadcast("trackOtpResendLead", _getTrackData(data, eventdata));
                    break;
                case 'SIMILAR_SELECT_ALL':
                    application.broadcast('trackSelectAllSimilarLead', _getTrackData(data, eventdata));
                    break;
                case "SIMILAR_SKIP_CLICKED":
                    break;
                case "VIEW_PHONE_GOTIT_CLICKED":
                    application.broadcast('trackDismissLead', _getTrackData(data, eventdata));
                    break;
                case "VIEW_PHONE_JOURNEY_CLICKED":
                    application.broadcast('trackClaimNowLead', _getTrackData(data, eventdata));
                    break;
                case "THANKS_GOTIT_CLICKED":
                    action = config["LEAD_DISMISS"];
                    application.broadcast('trackDismissLead', _getTrackData(data, eventdata));
                    break;
                case "MOBILE_CALL_NOW":
                    action = config["LEAD_SUBMIT"] + '_call_now';
                    application.broadcast('trackCallNowLead', _getTrackData(data, eventdata));
                    break;
                case "CLOSE_LEAD":
                    action = config["LEAD_CLOSE"];
                    application.broadcast('trackCloseLead', _getTrackData(data, eventdata));
                    break;
                case "CLOSE_PROP_LEAD":
                    application.broadcast('trackPropCloseLead', _getTrackData(data, eventdata));
                case "BACK":
                    action = config["LEAD_BACK"];
                    application.broadcast('trackBackLead', _getTrackData(data, eventdata));
                    break;
                case 'LEAD_FORM_ERROR':
                    application.broadcast('trackLeadFormErrors', $.extend(_getTrackData(data), eventdata));
                    break;
                case 'FEEDBACK_OPEN':
                    application.broadcast('trackFeedbackShown',$.extend(_getTrackData(data), eventdata));
                    break;
                case 'FEEDBACK_RATING_STORED':
                    application.broadcast('trackFeedbackRating',$.extend(_getTrackData(data), eventdata));
                    break;
                case 'Homeloan_APPLY_NOW':
                    application.broadcast('trackLeadContinueHomeloan',$.extend(_getTrackData(data), eventdata));
                    break;
                case 'LEAD_CONTINUE_SEARCH':
                    application.broadcast('trackLeadContinueSearch',$.extend(_getTrackData(data), eventdata));
                    break;
                case 'USER_DETAILS_SKIP_CLICKED':
                    application.broadcast('trackUserDetailSkipped',$.extend(_getTrackData(data), eventdata));
                    break;
                case 'USER_DETAILS_SUBMIT_CLICKED':
                    application.broadcast('trackUserDetailSubmitted',$.extend(_getTrackData(data), eventdata));
                    break;
                case 'CLOSE_USER_DETAILS':
                    application.broadcast('trackUserDetailClose',$.extend(_getTrackData(data), eventdata));
                    break;
                case 'SimilarListings_CardSeen':
                case 'SimilarSellers_CardSeen':
                case 'NameEmail_CardSeen':
                case 'CallingSCREEN_Seen':
                    application.broadcast('trackCardSeen',$.extend(_getTrackData(data), eventdata));
                    break;
                case 'Lead_API_SUCCESS':
                    application.broadcast('trackLeadApiSuccess',$.extend(_getTrackData(data), eventdata));
                    break;
                case 'Lead_API_FAILURE':
                    application.broadcast('trackLeadApiFailure',$.extend(_getTrackData(data), eventdata));
                    break;
            }
        }

        function mapFromArray(object, property, join) {
        	let response = "";
        	for(let i in object){
        		let element = object[i];
        		if(response){
        			response += ',';
        		}
        		if($.isArray(element[property]) && join){
        			response += element[property].join();
        			continue;
        		}
        		response += element[property];
        	}
            return response;
        }

        function _getTrackData(allData = {}, eventData = {}) {
            let value,
                response = {},
                trackData = allData.trackData,
                postData = allData.postData,
                displayData = allData.displayData,
                sourceModuleConfig = t.LEAD_SOURCE_MODULES,
                stepsConfig = t.LEAD_STEPS_MAP;

            if(stepsConfig[allData.step]) {
                response[t.LABEL_KEY] = t.LEAD_LABELS["SingleSeller"] + '_' + stepsConfig[allData.step];
            }
            if(allData.hasTenantPreferences !== undefined) {
                response[t.HAS_TENANT_PREFERENCES] = allData.hasTenantPreferences ? "YES" : "NO";
            }

            response[t.EMAIL_KEY] = postData && postData.email;
            response[t.PHONE_USER_KEY] = postData && postData.phone;
            response[t.NAME_KEY] = postData && postData.name;

            response[t.BUYER_OPT_IN_KEY] = postData && postData.optIn ? t.BUYER_OPT_IN_TRUE : t.BUYER_OPT_IN_FALSE;

            response[t.RANK_KEY] = trackData && trackData.rank;
            response[t.SOURCE_MODULE_KEY] = trackData && trackData.source && (sourceModuleConfig[trackData.source] || trackData.source);
            response.isSelectListing = trackData && trackData.isMakaanSelectSeller && postData.salesType=="rent";

            response[t.CITY_ID_KEY] = postData && postData.cityId;
            response[t.LISTING_CATEGORY_KEY] = postData && t.makeValuesUniform(postData.salesType,t.LISTING_CATEGORY_MAP) ;

            if ($.isEmptyObject(eventData)) {
                response[t.LOCALITY_ID_KEY] = postData && $.isArray(postData.localityIds) && postData.localityIds.join();
                response[t.PROJECT_ID_KEY] = postData && postData.projectId;
                response[t.LISTING_ID_KEY] = postData && postData.listingId;
                response[t.UNIT_TYPE_KEY] = displayData && (displayData.propertyTypeLabel) && t.makeValuesUniform(displayData.propertyTypeLabel,t.UNIT_TYPE_MAP);

                response[t.SELLER_ID_KEY] = postData && $.isArray(postData.multipleCompanyIds) && postData.multipleCompanyIds.join();

                response[t.BUILDER_ID_KEY] = trackData && trackData.builderId;
                response[t.BUDGET_KEY] = trackData && trackData.budget;
                response[t.BHK_KEY] = trackData && trackData.bhk && t.makeValuesUniform(trackData.bhk.toString(),t.BHK_MAP) ;
                response[t.PROJECT_STATUS_KEY] = trackData && t.makeValuesUniform(trackData.projectStatus,t.PROJECT_STATUS_NAME) ;
                response[t.SELLER_NAME_KEY] = displayData && displayData.companyName;
            } else {
                response[t.LOCALITY_ID_KEY] = mapFromArray(eventData, 'localityIds', true);
                response[t.PROJECT_ID_KEY] = mapFromArray(eventData, 'projectId');
                response[t.LISTING_ID_KEY] = mapFromArray(eventData, 'listingId');

                value = mapFromArray(eventData, 'propertyTypes', true);
                response[t.UNIT_TYPE_KEY] =  t.makeValuesUniform(value,t.UNIT_TYPE_MAP);
                response[t.SELLER_ID_KEY] = mapFromArray(eventData, 'multipleCompanyIds', true);
            }
            response.id = moduleId;
            response[t.ADDITIONAL_CD49] = allData.imageCount;

            return response;
        }

        return {
            trackLeadModuleEvent,
            setTrackingConfig
        };
    });
});
