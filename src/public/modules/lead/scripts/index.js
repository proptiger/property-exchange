"use strict";
define([
    'doT!modules/lead/views/main',
    'doT!modules/lead/views/otp',
    'doT!modules/lead/views/similar',
    'text!modules/lead/views/thanks.html',
    'text!modules/lead/views/claimCashback.html',
    'doT!modules/lead/views/viewPhone',
    'doT!modules/lead/views/viewPhoneMorphed',
    'doT!modules/lead/views/similar-listing-element',
    'doT!modules/lead/views/similar-sellers-element',
    'doT!modules/lead/views/mainTopSeller',
    'doT!modules/lead/views/callStatusScreens',
    'doT!modules/lead/views/similarSellers',
    'doT!modules/lead/views/name-email',
    'doT!modules/lead/views/user-profiling',
    'doT!modules/lead/views/showCoupon',
    'common/sharedConfig',
    'services/pushNotificationService',
    "services/urlService",
    'services/filterConfigService',
    "modules/deficitSellerProjects/scripts/services/deficitSellerProjects",
    'services/feedbackService',
    "services/localStorageService",
    "services/leadProfilingService",
    'services/loggerService',
    'modules/lead/scripts/services/leadService',
    'modules/shareCoupon/scripts/services/couponService',
    'modules/lead/scripts/services/trackLeadEvents',
    'services/utils',
    'services/apiService',
    'services/commonService',
    'services/leadPyrService',
    'services/loginService',
    'modules/lead/scripts/behaviors/newLeadFormTracking',
    'services/loginService',
    'services/defaultService',
    'services/feedbackService',
    'behaviors/leadBehaviour',
    "css!styles/css/leadEnquiry"
], (mainStepTemplate,
    otpStepTemplate,
    similarStepTemplate,
    thanksStepHTML,
    claimCashbackHTML,
    viewPhoneStepTemplate,
    viewPhoneMorphedStepTemplate,
    similarListingsTemplate,
    similarSellersTemplate,
    mainTopSellerStepTemplate,
    callStatusScreensTemplate,
    similarSellersStepTemplate,
    userDetailsTemplate,
    userProfilingTemplate,
    showCouponTemplate,
    SharedConfig,
    pushNotificationService,
    urlService,
    filterConfigService,
    deficitSellerService,
    feedbackService,
    localStorageService,
    leadProfilingService) => {
    Box.Application.addModule('lead', (context) => {
        //const Logger = context.getService('Logger');
        const LeadService = context.getService('LeadService');
        const couponService = context.getService('couponService');
        const Utils = context.getService('Utils');
        const CommonService = context.getService('CommonService');
        const TrackLeadEvents = context.getService('TrackLeadEvents');
        // const ApiService = context.getService('ApiService');
        const LeadPyrService = context.getService('LeadPyrService');
        const loginService = context.getService('LoginService');
        const defaultService = context.getService('DefaultService');
        const feedbackService = context.getService('FeedbackService');
        const M_PROFILE_CLOSE_STEP = ["USER_PROFILING", "USER_PROFILING_MOBILE"];
        const M_PROFILE_SKIP_STEP = ["OTP_ONCALL", "OTP_RESEND", "VIEW_PHONE_MORPHED", "MAIN", "OTP", "CONTINUE_SEARCH", "THANKS","CALL_NOW"];
        const PETRA = 'PETRA',SAPPHIRE = 'SAPPHIRE',KIRA = 'KIRA';
        //const Application = Box.Application;
        //const query = Box.DOM.query;
        const config = {
            selector: {
                "MAIN": {
                    "PHONE": "[data-type=PHONE_FIELD] input",
                    "COUNTRY_CODE": "[data-country-code=COUNTRY_CODE_FIELD]",
                    "COUNTRY_ID": "input[data-country=COUNTRY_ID_FIELD]",
                    "OPT_IN": "input[data-type=BUYER_OPT_IN]:checked",
                },
                "CALL_NOW": {
                    "PHONE": "[data-type=PHONE_FIELD] input",
                    "COUNTRY_CODE": "[data-country-code=COUNTRY_CODE_FIELD]",
                    "COUNTRY_ID": "input[data-country=COUNTRY_ID_FIELD]",
                    "OPT_IN": "input[data-type=BUYER_OPT_IN]:checked"
                },
                "MAIN_TOP_SELLER": {
                    "PHONE": "[data-type=PHONE_FIELD] input",
                    "COUNTRY_CODE": "[data-country-code=COUNTRY_CODE_FIELD]",
                    "COUNTRY_ID": "input[data-country=COUNTRY_ID_FIELD]",
                    "OPT_IN": "input[data-type=BUYER_OPT_IN]:checked"
                },
                "OTP": {
                    "OTP": "[data-type=OTP_FIELD] input",
                    "EMAIL": "[data-type=EMAIL_FIELD] input",
                    "NAME": "[data-type=NAME_FIELD] input",
                    "RESEND": "[data-type=OTP_RESEND]"
                },
                "USER_DETAILS": {    
                    "EMAIL": "[data-type=EMAIL_FIELD] input",
                    "NAME": "[data-type=NAME_FIELD] input",
                },
                "CALL_STATUS_SCREEN": {
                    "SELECTED_RATING": "[data-type=REASON_RATING] input:checked",
                    "RATING": "[data-type=REASON_RATING] input",
                    "FEEDBACK_RATING_CONTAINER": "[call-feedback-rating]",
                    "FEEDBACK_RATING_REASON_CONTAINER": "[call-feedback-reason]",
                    "SUBMIT_RATING_REASON": "[data-type=REASON_SUBMIT]",
                    "SELECTED_REASON": "input[data-type=REASON_REASON]:checked",
                    "FEEDBACK_BACK": "[data-type=FEEDBACK_BACK]"
                },
                "SIMILAR": {
                    "LISTINGS": "[data-similar-container]",
                    "LISTINGS_CHECKBOX": "[data-similar-container] input[type=checkbox]",
                    "ALL_LISTINGS_CHECKBOX": "input[data-select-all]",
                    "LISTINGS_CHECKBOX_CHECKED": "[data-similar-container] input[type=checkbox]:checked",
                    "SELECT_ALL_CONTAINER": "[data-select-all-container]",
                    "CALL_STATUS_HEADER": "[js-call-status-header]"
                },
                "SIMILAR_SELLERS": {
                    "LISTINGS": "[data-similar-container]",
                    "LISTINGS_CHECKBOX": "[data-similar-container] input[type=checkbox]",
                    "ALL_LISTINGS_CHECKBOX": "input[data-select-all]",
                    "LISTINGS_CHECKBOX_CHECKED": "[data-similar-container] input[type=checkbox]:checked",
                    "SELECT_ALL_CONTAINER": "[data-select-all-container]",
                    "CALL_STATUS_HEADER": "[js-call-status-header]"
                },
                "VIEW_PHONE": {
                    "JOURNEY": "[data-journey-url]",
                },
                "VIEW_PHONE_MORPHED": {
                    "PHONE": "[data-type=PHONE_FIELD] input",
                    "COUNTRY_CODE": "[data-country-code=COUNTRY_CODE_FIELD]",
                    "COUNTRY_ID": "input[data-country=COUNTRY_ID_FIELD]",
                    "OPT_IN": "input[data-type=BUYER_OPT_IN]:checked"
                },
                "THANKS": {
                    "JOURNEY": "[data-journey-url]"
                },
                "TOP_SELLER": "[data-type=MAIN_TOP_SELLER_BOX]",
                "WRAPPER": "[data-lead-container]",
                "PROCESSING": "[data-processing]",
                "NORMAL": "[data-normal]",
                LEAD_NO_SHADOW: '.js-lead-shadow',
                M_PROFILE_IN_CARD: '.js-m-profile-card',
                M_PROFILE_POPUP: '#mProfilePopup',
                GALLERY_LEAD_FORM: '.lg-img-wrap'
            },
            similarScreenDataFunction: {
                "LISTING": _getSimilarListing,
                "PROJECT": _getSimilarProject,
                "SIMILAR_SELLERS": _getSimilarSellers
            },
            resendTryTime: 5000,
            otpValidationTime: 30 * 60 * 1000,
            hideClass: "hide",
            activeClass: "active"
        };
        const experimentCity=SharedConfig.serpExperimentCities;
        const contactExperimentCities =  SharedConfig.serpContactExperimentCities;
        const HOTJAR_LEAD_CLOSE_TRIGGER = "lead_closed_trigger";
        var global = {
            isProcessing: false,
            isWaitingForFeedbackEvent: false,
            isMakaanSelectSeller:false,
            isExperimentCity:false,
            isContactExperimentCity:false,
            homeloanLqLeadPosted:false
        };
        let feedbackData = null;

        var messages = [
                "Lead_Open_Step",
                "singleSelectDropdownChanged",
                "popup:change",
                "singleSelectDropdownInitiated",
                "lead_no_similar",
                "Disconnect",
                "Dial",
                "Hangup",
                "call_ended",
                "call_feedback",
                "userLoggedIn",
                "userLoggedOut",
                "NewCall",
                "navigateNextStep",
                "leadFormError",
                "postLead",
                "userProfilingCompletedViaPopup",
                "userProfilingPopupOpenViaMobile",
                "userProfilingPopupClosed"
            ],
            behaviors = ["newLeadFormTracking", "leadBehaviour"],
            moduleEl, _details, _stepsStack = [],
            moduleParameters,
            _callStepStack = [],
            homeloanData,
            configData,
            isCommercial;

        const SKIP_INIT_TRACKING_SOURCES = ["gallery pop up"],PROP_EXP_CLASS='_vwo_propLF_exp';

        function _setHomeloanData(data){
            homeloanData={};
            homeloanData.summary_mobile = data.postData.phone;
            homeloanData.summary_email = data.postData.email;
            homeloanData.summary_name = data.postData.name;
            homeloanData.summary_countryId = data.postData.countryId;
            homeloanData.project_cityName = data.postData.cityName;
            homeloanData.project_cityId = data.postData.cityId;
            homeloanData.price_propertyPrice = data.postData.minBudget;
        }

         function openHomeloanForm() { 
            context.broadcast('loadModule', {
                name: 'homeloanFlow',
            });
            
            CommonService.bindOnModuleLoad('homeloanFlow', () => {
                context.broadcast('homeloanFlow', {
                    startScreen: "summary",
                    preObject: homeloanData,
                });
            });
        }
        function _trackStepSeen(currentStep,data){
            let appendString = data.stepsSkipped.indexOf('MAIN') > -1 ?"_ConnectNow":(data.stepsSkipped.indexOf('VIEW_PHONE_MORPHED') > -1 ?"_ViewPhone":'');
            if(!appendString)
                appendString =data.prevStep =="MAIN" ? "_ConnectNow" : (data.prevStep=="VIEW_PHONE_MORPHED"?"_ViewPhone":''); 

            switch(currentStep){
                case "SIMILAR":
                        _trackLead("SimilarListings_CardSeen",{label:`SimilarListings${appendString}`});
                        break;
                case "SIMILAR_SELLERS":
                         _trackLead("SimilarSellers_CardSeen",{label:`SimilarSellers${appendString}`});
                        break;
                case "USER_DETAILS":
                         _trackLead("NameEmail_CardSeen",{label:`NameEmail${appendString}`});
                        break;
                case "CALL_STATUS_SCREEN":
                         _trackLead("CallingSCREEN_Seen",{label:'Calling_Screen'});
                        break;
            }
        }

        function _trackLeadApiSuccess(api,response={}){
            switch(api){
                case SAPPHIRE:
                        _trackLead("Lead_API_SUCCESS",{uniqueId: `${SAPPHIRE}_${response.id}`});
                        break;
                case KIRA:
                        _trackLead('Lead_API_SUCCESS',{uniqueId:`${KIRA}_${response.callId}`});
                        break;
                case PETRA:
                        response.enquiryIds && response.enquiryIds.forEach(function(enquiryId){
                            _trackLead('Lead_API_SUCCESS',{uniqueId:`${PETRA}_${enquiryId}`});
                        });
                        break;
            }
        }
        function _trackLeadApiFailure(api){
            _trackLead("Lead_API_FAILURE",{uniqueId: api});
        }
        function trackSelectCouponSeen(data){
            if(data.isNew){
                context.broadcast('selectCouponSeen',{id:moduleEl.id,source:data.id,label:'New_Voucher_Generated'});
            }else{
                context.broadcast('selectCouponSeen',{id:moduleEl.id,source:data.id,label:'Old_Voucher_Generated'});
            }
        }

        function getDisplayContact(phone){
            phone = phone.toString();
            let displayContact = `+91 ${Utils.getMaskedPhoneNumber(phone)}`; 
            return displayContact;
        }

        function handleShowCouponStep(){
            if(_details && _details.response && !Utils.isEmptyObject(_details.response.SHOW_SELECT_COUPON)){
                _renderView("SHOW_SELECT_COUPON", _details);
            }else{
                let couponPostData = _getCouponPostData(_details);
                couponService.generateCoupon(couponPostData).then(response=>{
                    _details.postData.couponId = response && response.id||'';
                    addCouponViewDetails(response);
                    _renderView("SHOW_SELECT_COUPON", _details);
                },(err) => {
                     _showHideProcessing();
                    _connectAfterCouponScreen(_details);
                });
            }
        }
        function _gotoStep(step, data, isReverseTraversing = false) {
            if (step == 'USER_PROFILING_MOBILE'){
                _renderView(step, data);
            } else {
                 _updateStepsStack(step);
                _trackStepSeen(step,data);
                if(step!="USER_DETAILS" && step!="SHOW_SELECT_COUPON" ){
                    data.prevStep = (!isReverseTraversing) ? (data.step || null) : data.prevStep; 
                }
                //!isReverseTraversing && (data.prevStep = data.step || null);   // jshint ignore:line
                data.step = step;
                data.stepsStack = _stepsStack;
                data.stackLength = _stepsStack.length;
                data.isConnectNow = window.numberFormat && window.numberFormat.isConnectNow;
                data.showSelectBanner = global.isMakaanSelectSeller && data.postData.salesType=="rent" || false;
                data.selectCouponPrice = SharedConfig.selectCouponPrice;
                data.showHomeLoanInterest = _details.postData.salesType == "buy" && LeadPyrService.showHomeLoanCheckBox();
                data.isMobile = Utils.isMobileRequest();
                if(step=="SHOW_SELECT_COUPON"){
                    handleShowCouponStep();
                }else{
                    _renderView(step, data);
                }
            }
        }

        function _focusInput(selector, type) {
            if (!(moduleParameters && moduleParameters.isPopup)) {
                return;
            }
            $(moduleEl).find(`[name=${selector}]`).focus();
            if (type === "module") {
                CommonService.bindOnModuleLoad("InputBox", (id) => {
                    if (id === selector) {
                        $(moduleEl).find(`[name=${selector}]`).focus();
                    }
                });
            }
        }

        function _fetchDataFromUI(step) {
            let data = {};
            switch (step) {
                case "MAIN":
                case "MAIN_TOP_SELLER":
                case "VIEW_PHONE_MORPHED":
                case "CALL_NOW":
                    data["PHONE".toCamelCase()] = $(moduleEl).find(config.selector[step]['PHONE']).val().trim();
                    data["COUNTRY_ID".toCamelCase()] = $(moduleEl).find(config.selector[step]['COUNTRY_ID']).val().trim();
                    data["OPT_IN".toCamelCase()] = $(moduleEl).find(config.selector[step]['OPT_IN']).val() ? true : false;
                    break;
                case "OTP":
                    data["OTP".toCamelCase()] = $(moduleEl).find(config.selector[step]['OTP']).val().trim();
                    break;
                case "USER_DETAILS":  
                    data["NAME".toCamelCase()] = $(moduleEl).find(config.selector[step]['NAME']).val().trim();
                    data["EMAIL".toCamelCase()] = $(moduleEl).find(config.selector[step]['EMAIL']).val().trim();
                    break;

                case "SIMILAR_SELLERS":
                case "SIMILAR":
                    $(moduleEl).find(config.selector[step]["LISTINGS_CHECKBOX_CHECKED"]).each(function(index) {
                        data[index] = JSON.parse(decodeURI($(this).data('collection')));
                    });
                    break;
            }
            return data;
        }

        function _startAllModules() {
            CommonService.startAllModules(moduleEl);
        }

        function _stopAllModules() {
            CommonService.stopAllModules(moduleEl);
        }

        function _appendSimilarListing(data) {
            let similarListingsHTML = similarListingsTemplate(data);
            $(moduleEl).find(config.selector["SIMILAR"]["LISTINGS"]).html(similarListingsHTML);
        }

        function _appendSimilarSellers(data) {
            let similarListingsHTML = similarSellersTemplate(data);
            $(moduleEl).find(config.selector["SIMILAR_SELLERS"]["LISTINGS"]).html(similarListingsHTML);
            _startAllModules();
        }

        function _appenClaimCashback() {
            if (_details.step != "USER_PROFILING" && _details.postData && _details.postData.salesType != "buy" && !(isCommercial)) {
                _moveToUserProfilingStep(_details);
            } else {
                _details.isBuyLead = _details.postData.salesType == "buy" ? true : false;
                let claimCashback = doT.template(claimCashbackHTML)(_details);
                $(moduleEl).find(config.selector["SIMILAR_SELLERS"]["LISTINGS"]).html(claimCashback);
                let _callHeaderElem = $(moduleEl).find(config.selector["SIMILAR_SELLERS"]["CALL_STATUS_HEADER"]);
                if (_callHeaderElem && _callHeaderElem.length) {
                    _callHeaderElem.addClass(config.hideClass);
                    if (moduleParameters.isPopup) {
                        $(moduleEl).find(config.selector["SIMILAR_SELLERS"]["LISTINGS"] + " .second-pane").addClass("similar-container-center");
                    }
                }
            }
        }

        function _showSelectAll(step) {
            $(moduleEl).find(config.selector[step]["SELECT_ALL_CONTAINER"]).removeClass(config.hideClass);
        }


        function _getSimilarListing() {
            if (!_details.postData.listingId) {
                _appenClaimCashback();
                return;
            }
            let count = 5;
            LeadService.getSimilarListing(_details.postData.listingId, _details.postData.multipleCompanyIds[0], count, _details.postData.isCommercial).then((response) => {
                if (response && response.length) {
                    _appendSimilarListing(response);
                    _showSelectAll("SIMILAR");
                } else {
                    _appenClaimCashback();
                }
            }, _appenClaimCashback);
        }

        function _getSimilarSellers() {
            if (!_details.postData.listingId) {
                _appenClaimCashback();
                return;
            }
            let count = 5;
            LeadService.getSimilarListing(_details.postData.listingId, _details.postData.multipleCompanyIds[0], count, _details.postData.isCommercial,true).then((response) => {
                if (response && response.length) {
                    _appendSimilarSellers(response);
                    _showSelectAll("SIMILAR_SELLERS");
                } else {
                    _appenClaimCashback();
                }
            }, _appenClaimCashback);
        }

        function _getSimilarProject() {
            if (!_details.postData.listingId) {
                _appenClaimCashback();
                return;
            }
            LeadService.getSimilarProjectTopSeller(_details.postData.listingId).then((response) => {
                if (response && response.length) {
                    _appendSimilarListing(response);
                    _showSelectAll();
                } else {
                    _appenClaimCashback();
                }
            }, _appenClaimCashback);
        }

        function _appendHtmlToView(html, callback) {
            _stopAllModules();
            $(moduleEl).find(config.selector["WRAPPER"]).html(html);
            _startAllModules();
            if (callback && typeof callback === "function") {
                callback();
            }
        }


        function _renderView(step, data={}) {
            let html, 
                func = emptyFunction;
                if(step !== "USER_PROFILING" && step !== "USER_PROFILING_MOBILE" && step!=="SIMILAR"){
                    global.isWaitingForFeedbackEvent = false;
                }
                data.showSelectBenefits = global.isMakaanSelectSeller && data.postData && data.postData.salesType=="rent";
                data.vwoExpClass = Utils.getPageData('pageLevel')=="property" ? PROP_EXP_CLASS : '';
            switch (step) {
                case "MAIN":
                    html = mainStepTemplate(data);
                    func = _focusInput.bind(undefined, _details.moduleParameters.phoneElemSelector, "module");
                    break;
                case "MAIN_TOP_SELLER":
                    html = mainTopSellerStepTemplate(data);
                    func = _focusInput.bind(undefined, _details.moduleParameters.phoneElemSelector, "module");
                    setTimeout(() => {
                        context.broadcast('popup:main_top_seller', {});
                    });
                    break;
                case "OTP":
                    html = otpStepTemplate(data);
                    func = _focusInput.bind(undefined, "otp_lead");
                    _trackLead("OTP_VISIBLE");
                    break;
                case "USER_DETAILS":  
                    html = userDetailsTemplate(data);
                    break;
                case "SHOW_SELECT_COUPON": 
                    let htmlData = data.response && data.response.SHOW_SELECT_COUPON || {};
                    htmlData.vwoExpClass = data.vwoExpClass;
                    html = showCouponTemplate( htmlData);
                    break;
                case "SIMILAR":
                    html = similarStepTemplate(data);
                    let dataFunc = config.similarScreenDataFunction[moduleParameters.similarType.toUpperCase()];
                    setTimeout(dataFunc);
                    break;
                case "SIMILAR_SELLERS":
                    html = similarSellersStepTemplate(data);
                    dataFunc = config.similarScreenDataFunction["SIMILAR_SELLERS"];
                    setTimeout(dataFunc);
                    break;
                case "THANKS":
                    let htmlFunction = doT.template(thanksStepHTML, undefined, { claimCashback: claimCashbackHTML });
                    data.isBuyLead = data.postData.salesType=="buy" ? true :false;
                    _setHomeloanData(data);
                    html = htmlFunction(data);
                    break;
                case "VIEW_PHONE":
                    html = viewPhoneStepTemplate(data);
                    break;
                case "VIEW_PHONE_MORPHED":
                    _trackLead("VIEW_PHONE_MORPHED_SEEN");
                    html = viewPhoneMorphedStepTemplate(data);
                    func = _focusInput.bind(undefined, _details.moduleParameters.phoneElemSelector, "module");
                    break;
                case "CALL_NOW":
                    if(!data || !data.trackData || SKIP_INIT_TRACKING_SOURCES.indexOf(data.trackData.source) === -1 || data.stepsStack.length !== 1){
                        _trackLead("VIEW_PHONE_MORPHED_SEEN");
                    }
                    html = viewPhoneMorphedStepTemplate(data);
                    func = _focusInput.bind(undefined, _details.moduleParameters.phoneElemSelector, "module");
                    break;
                case "CALL_STATUS_SCREEN":
                    global.isWaitingForFeedbackEvent = true;
                    html = callStatusScreensTemplate(data);
                    break;
                case "USER_PROFILING":
                    html = userProfilingTemplate(data);
                    break;
                case "USER_PROFILING_MOBILE":
                    html = userProfilingTemplate(data);
                    _openPopup('leadOverviewPopup');
                    break;    
            }
            _appendHtmlToView(html, func);
        }

        function _clearStepsStack() {
            _stepsStack = [];
        }

        function _clearCallStepStack() {
            _callStepStack = [];
        }

        function _updateStepsStack(step) {
            if (step) {
                if (_stepsStack[_stepsStack.length - 1] !== step) { _stepsStack.push(step); }
            } else {
                _stepsStack.pop();
                step = _stepsStack[_stepsStack.length - 1];
            }
            return step;
        }

        function _updateCallStepStack(step) {
            if (step) {
                if (_callStepStack[_callStepStack.length - 1] !== step) { _callStepStack.push(step); }
            }
        }

        function _closePopup(cb) {
            context.broadcast('popup:close', {_details, callback: cb,isLeadPopup:true});
            _clearStepsStack();
            _clearCallStepStack();
            _details = undefined;
            context.broadcast('leadPopupClosed',{id:moduleEl.id});
        }

        function _setEnquiryCookie(data) {
            LeadPyrService.setEnquiryCookie(data);
        }

        function _trackLead(type, data) {
            _details.hasTenantPreferences = global.hasTenantPreferences;
            _details.imageCount = global.imageCount === undefined ? "na" : (global.imageCount === 0 ? "zero" : global.imageCount);
            TrackLeadEvents.trackLeadModuleEvent(_details, type, data, moduleEl.id);
        }

        function _updateResponseInData(step, details, data) {
            LeadService.updateResponse(step, details, data);
            //update global details too
            _details = $.extend(true, _details, details);
        }

        function _updateContactNumbers(details, setExpiry = false) {
            let obj = {
                "contactNumber": details.postData.phone,
                "isVerified": details.response[details.step].isVerified
            };
            LeadPyrService.updateContactNumbers(obj, setExpiry);
        }

        function _trackDeficitSellerProject(event,deficitProject){
            let deficitTrackingData = {
                id:moduleEl.id,
                projectId:deficitProject.projectId,
                sellerId:deficitProject.projectSeller
            };
            context.broadcast(`trackLeadFormDeficitProject_${event}`,deficitTrackingData);
        }

        function _moveToUserProfilingStep(details) {
            if (!_isMProfileOpenInCard()) {
                if (!configData.isPopup){
                    if(configData.isGallery){
                        $(config.selector.GALLERY_LEAD_FORM).addClass('lp-col-no-shadow');
                    } else {
                        $(config.selector.LEAD_NO_SHADOW).addClass('lp-col-no-shadow');
                    }
                    details.isLeadCard = true;
                }
                details.isMobile = Utils.isMobileRequest();
                checkUserProfileAndShowDeflicit(details);
            } else {
                checkDeficitAndShowThanks(details);
            }
        }

        function _mustShowMProfile(details) {
            let mProfileCardSeen = localStorageService.getItem(SharedConfig.MPROFILE_CARD_SEEN),
                mProfileAnswered = localStorageService.getItem(SharedConfig.MPROFILE_ANSWERED);
            return details && details.postData && details.postData.salesType != "rent" && mProfileCardSeen && !mProfileAnswered;
        }

        function _decideThanksOrUserProfiling(details) {
            if (_mustShowMProfile(details)) {
                _showHideProcessing();
                deficitSellerService.getLeadDeficitProject().then(response => {
                    if(response){
                        details.deficitProject = response;
                        _details.deficitProject = response;
                        _trackDeficitSellerProject('seen',response);
                    }
                    _showHideProcessing(true);
                    _gotoStep("THANKS", details);
                }, function() {
                    _showHideProcessing(true);
                   _moveToUserProfilingStep(details);
                });
            }else{
               _moveToUserProfilingStep(details);
            }
        }

        function checkUserProfileAndShowDeflicit(details) {
            let questionObj = {
                saleType: Utils.capitalize(Utils.getPageData('listingType')),
                entityType: 'user',
                entityId: localStorageService.getItem('mkn_lead_userid'),
                questionOrder: 'next'
            };
            _showHideProcessing();
            leadProfilingService.getLeadProfileQuestion(questionObj).then(function (response) {
                let isComplete = 'No';
                if (response.profileDetail.profilingQuestionnaireId != null) {
                    _showHideProcessing(true);
                    _gotoStep("USER_PROFILING", details);
                } else {
                    isComplete = 'Yes'
                    _showHideProcessing(true);
                    _handleNoUserProfiling(details);
                }
                context.broadcast("trackMProfileCompleteStatus", {isComplete});
            }, function (err) {
                context.broadcast("mProfileApiFailure", {});
                _showHideProcessing(true);
                _handleNoUserProfiling(details);
            })
        }

        function _handleNoUserProfiling(details){
            if(feedbackData !== null) {
                //Feedback popup was stalled due to similar sellers 
                launchFeedbackPopup(_details, feedbackData, true);
                feedbackData = null;
            }else{
                checkDeficitAndShowThanks(details);
            }
        }

        function checkDeficitAndShowThanks(details){
            if(details && details.postData && details.postData.salesType!="rent"){
                _showHideProcessing();
                deficitSellerService.getLeadDeficitProject().then(response => {
                    if(response){
                        details.deficitProject = response;
                        _details.deficitProject = response;
                        _trackDeficitSellerProject('seen',response);
                    }
                    _showHideProcessing(true);
                    _gotoStep("THANKS", details);
                }, function() {
                    _showHideProcessing(true);
                   _gotoStep("THANKS", details);
                });
            }else{
                _gotoStep("THANKS", details);
            }
        }
        function _isMProfileOpenInCard() {
            return $(config.selector.M_PROFILE_IN_CARD)[0];
        }

        function _getCouponPostData(details){
            let data = {
                cityId :details.postData.cityId,
                entityId : _details.enuiryId ? _details.enuiryId : 0,
                entityTypeId : 1,//denoting coupon is generated for rent
                generatorUserId : details.buyerUserId
            };
            return data;
        }
        function _postLeadDropProcess(details){

            if(isCommercial) {
                _gotoStep("THANKS", details);
            }else if (details.displayData.configurationText) {
                _decideThanksOrUserProfiling(details);
            } else if (["VIEW_PHONE_MORPHED", "CALL_NOW"].indexOf(details.prevStep) > -1 || _details.stepsSkipped.indexOf("VIEW_PHONE_MORPHED") > -1 || _details.stepsSkipped.indexOf("CALL_NOW") > -1) {
                _updateSellerDataAndGotoStep("SIMILAR", details);
            } else {
                details.displayData.callStep = "NewCall";
                _updateCallStepStack("NewCall");
                _gotoStep("CALL_STATUS_SCREEN", details);
            }
        }
        function _connectAfterCouponScreen(details){
            if(details.displayData.configurationText || (details.prevStep && ["VIEW_PHONE_MORPHED", "CALL_NOW"].indexOf(details.prevStep) > -1 || _details.stepsSkipped.indexOf("VIEW_PHONE_MORPHED") > -1 || _details.stepsSkipped.indexOf("CALL_NOW") > -1)) {
                _moveToNextStep(details);
            }else {
                _details.tempEnquiryId && _verifyTempEnquiry();
                _connectSellerBuyer(details);
            }
            _showHideProcessing(true);

        }
        function _verifyTempEnquiry(){
            let tempEnquiryId = _details.tempEnquiryId;
            LeadService.verifyTempEnquiry({tempEnquiryId});
        }
        function _moveToNextStep(details, submitButton) {
            let currentStep = submitButton ? submitButton : details.step;
            switch (currentStep) {
                case "MAIN_VIEW_CONNECT":
                    _gotoStep("MAIN", details);
                    break;
                case "MAIN":
                case "MAIN_TOP_SELLER":
                    if (!details.postData.sendOtp) {
                        if(global.isMakaanSelectSeller && details.postData.salesType=="rent"){
                            _gotoStep("SHOW_SELECT_COUPON", details);
                        }else{

                            if(isCommercial) {
                                _gotoStep("THANKS", details);
                            }
                            else if (details.displayData.configurationText) {
                                _decideThanksOrUserProfiling(details);
                            } else if (!LeadService.isIndian(details)) {
                                (moduleParameters.skipSimilar) ? _decideThanksOrUserProfiling(details) : _gotoStep("SIMILAR", details); // jshint ignore:line
                            } else {
                                details.displayData.callStep = "NewCall";
                                _updateCallStepStack("NewCall");
                                _gotoStep("CALL_STATUS_SCREEN", details);
                            }
                        }
                    } else {
                        _gotoStep("OTP", details);
                    }
                    break;
                case "OTP":
                     _gotoStep("USER_DETAILS",details);
                    break;
                case "USER_DETAILS":
                    if(global.isMakaanSelectSeller && _details.postData.salesType=="rent"){
                        _gotoStep("SHOW_SELECT_COUPON",details);
                    }else{
                        _postLeadDropProcess(details);
                    }
                    break;
                case "SHOW_SELECT_COUPON":  
                    _postLeadDropProcess(details);
                    break;
                case "SIMILAR":
                case "SIMILAR_SELLERS":
                    if(isCommercial) {
                        _gotoStep("THANKS", details);
                    }
                    else {
                         _decideThanksOrUserProfiling(details);
                    }
                    break;
                case "MAIN_VIEW_PHONE":
                    let prevStep = _details.step;
                    if(_details.postData && $(moduleEl).find(config.selector.MAIN.PHONE)){
                        _details.postData.phone = $(moduleEl).find(config.selector.MAIN.PHONE).val()||'';
                    }
                    _stepInterceptor(_details, submitButton).always((data) => {
                        (prevStep === data.step) ? _gotoStep("VIEW_PHONE_MORPHED", details): _gotoStep(data.step, details); // jshint ignore:line
                    });
                    context.broadcast("leadContactNumberEntered", {});
                    break;
                case "VIEW_PHONE_MORPHED":
                case "CALL_NOW":
                    details.postData.sendOtp ? _gotoStep("OTP", details) :(global.isMakaanSelectSeller && details.postData.salesType=="rent" ?  _gotoStep("SHOW_SELECT_COUPON", details) :_updateSellerDataAndGotoStep("SIMILAR", details)); // jshint ignore:line
                    if (!_mustShowMProfile(details)) {
                        context.broadcast("leadContactNumberEntered", {});
                    }
                    break;
                case "THANKS":
                    break;
                case "BACK":
                    let newStep = _updateStepsStack();
                    if (newStep === 'OTP' && (details.response.OTP.isVerified || details.response.VIEW_PHONE_MORPHED.isVerified)) {
                        newStep = _updateStepsStack();
                    }
                    if (!details.step) {
                        _closePopup();
                    } else {
                        _gotoStep(newStep, details, true);
                    }
                    break;
                case "SIMILAR_SKIP":
                    if(isCommercial) {
                        _gotoStep("THANKS", details);
                    } else {
                        _decideThanksOrUserProfiling(details);
                    }
                    break;
                case "CALL_STATUS_SCREEN":
                    _gotoStep("SIMILAR_SELLERS", _details);
                    break;

            }
        }

        function _updateSellerDataAndGotoStep(step, details) {
            LeadService.getSellerData(details.displayData.companyUserId).then(function(response) {
                global.isProcessing = false;
                details.displayData.allowCallInNight = response.allowCallInNight;
                details.displayData.sellerTime = Utils.getSellerCallingTime();
                _gotoStep(step, details);
            }, function() {
                global.isProcessing = false;
                _gotoStep(step, details);
            });
        }

        function _postTempEnquiry(formData, details, cb) {
            let step = details.step;
            LeadService.postTemporaryLead(details).then((response) => {
                _trackLeadApiSuccess(SAPPHIRE,response);
                response.reqIsVerified = !details.postData.sendOtp;
                _updateResponseInData(step, details, response);
                _updateContactNumbers(details);
                _details.tempEnquiryId = response.id;
                if (typeof cb === 'function') {
                    cb();
                }
            }, () => {
                _trackLeadApiFailure(SAPPHIRE);
                context.broadcast("SHOW_ERROR", { errorType: "GENERIC", field: "GLOBAL" });
            }).always(() => {
                _showHideProcessing(true);
            });
        }

        function _updateNumberExpiryTime(details) {
            _updateContactNumbers(details, true);
        }

        function _updateCallingNumber(details, callingNumber) {
            if (callingNumber) {
                details.displayData.callingNumber = callingNumber;
            }
        }

        function _setAlreadyContacted(details) {
            LeadPyrService.setAlreadyContact(details.postData.listingId, details.postData.phone);
            _changeViewStatus();
        }

        function _postEnqiryForOtherCompany(details) {
            if (details.postData.multipleCompanyIds.length > 1) {
                let tempDetails = $.extend(true, {}, details);
                tempDetails.postData.multipleCompanyIds.shift();
                LeadService.postEnquiry(tempDetails).then((response) => {
                    _trackLeadApiSuccess(PETRA,response);
                }, () => {
                    _trackLeadApiFailure(PETRA);
                });
            }
        }

        function emptyFunction() {}

        function _connectSellerBuyer(details, moveForward = true) {
            let {isAccountLocked} = details.displayData;
            if(isAccountLocked){
                details.displayData.lockedSellerText = 'We have informed the seller of your requirements and they will contact you shortly.';
                _showHideProcessing(true);
                return _connectFailedOrSkipped(details, "SIMILAR");
            }else {
                delete details.displayData.lockedSellerText;
            }
            postHomeLoanLqLead();
            _postEnqiryForOtherCompany(details);
            LeadService.getSellerData(details.displayData.companyUserId).then(function(response) {
                details.displayData.sellerAvailable = response.sellerAvailable;
                details.displayData.sellerTime = Utils.getSellerCallingTime();
                if (response.sellerAvailable) {
                    _initBuyerSellerCall(details, moveForward);
                } else {
                    details.displayData.callStep = "SellerUnavailable";
                    _updateCallStepStack("SellerUnavailable");
                    _gotoStep("CALL_STATUS_SCREEN", details);
                    let _detailCopyObj = $.extend(true, {}, details);
                    _detailCopyObj.postData.multipleCompanyIds.splice(1);
                    details.postData.phone && _postEnquiry(_detailCopyObj); // jshint ignore:line

                }
            }, function() {
                _initBuyerSellerCall(details, moveForward);
            });
        }
        function addCouponViewDetails(data){
            if(data){
            data.expiryDate = Utils.formatDate(data.expiryDate, 'MM dd,YY');
            data.displayCode = data.displayCode || Utils.getMaskedCode(data.code,4);
            data.userContact = _details.postData.phone;
            data.displayContact = _details.postData.phone && getDisplayContact(_details.postData.phone)||'';
            _updateResponseInData("SHOW_SELECT_COUPON",_details,data);
            trackSelectCouponSeen(data);
            couponService.setCouponGeneratedCookie(data);
            }
        }

        function _postEnquiry(details, step,moveToNextStep=false) {
            step = step || details.step;
            return LeadService.postEnquiry(details, step).then((response) => {
                _trackLeadApiSuccess(PETRA,response);
                context.broadcast('enquiry_submit', details);
                _details.enuiryId = response && response.enquiryIds && response.enquiryIds.length ? response.enquiryIds[0] : '';
                _updateResponseInData(step, details, response);
                addCouponViewDetails(response.couponDetails);
                if(moveToNextStep){
                    _moveToNextStep(details);
                }
            }, () => {
                _trackLeadApiFailure(PETRA);
            });
        }
        function postHomeLoanLqLead(){
            if(!global.homeloanLqLeadPosted && _details.isHomeLoanChecked){
                _setHomeloanData(_details);
                LeadPyrService.postHomeLoanLqLead(homeloanData).then((response)=>{
                    _trackLeadApiSuccess(PETRA,response);
                    global.homeloanLqLeadPosted = true;
                    LeadPyrService.setHomeLoanLQDropTime();
                }, () => {
                    _trackLeadApiFailure(PETRA);
                });
            }
        }
        function _postUserDetails(details) {
            let data = {
                "id":details.response.OTP.userId,
            };
            if(details.postData.email){
                data.emails = [{"email":details.postData.email,"priority":1}];
            }
            if(details.postData.name){
                data.fullName = details.postData.name;
            }
            return LeadService.postUserDetails(data).then((response) => {
                context.broadcast('user_details_updated', response);
            });
        }

        function _connectFailedOrSkipped(details, step){
            details.displayData.callStep = "Disconnect";
            _updateCallStepStack("Disconnect");
            _gotoStep(step, details);
            _postEnquiry(details, step);
        }

        function _initBuyerSellerCall(details, moveForward) {
            let step = details.step == "USER_DETAILS" || details.step == "SHOW_SELECT_COUPON" ? 'OTP': details.step;
            LeadService.connectBuyerSeller(details).then((response) => {
                _trackLeadApiSuccess(KIRA,response);
                _trackLead("MAIN_CONNECT_NOW_SUCCESS");
                _updateResponseInData(step, details, response);
                moveForward && _moveToNextStep(details); // jshint ignore:line
                _updateCallingNumber(details, response.callingNumber);
                _setAlreadyContacted(details);
                !details.isLoggedIn && _updateNumberExpiryTime(details); // jshint ignore:line
            }, () => {
                _trackLeadApiFailure(KIRA);
                _connectFailedOrSkipped(details, "SIMILAR_SELLERS");
            }).always(() => {
                _showHideProcessing(true);
            });
        }

        function _sendPushNotification(type, details) {
            let tagData = {};
            if (details) {
                tagData = LeadService.getPushNotificationData(details);
            }
            pushNotificationService.sendNotificationTags(type, tagData);
        }
        function _updateUserDetailsAndConnect(formData, details){
            if(details.postData.email || details.postData.name){
                _postUserDetails(details);
            }
            if(global.isMakaanSelectSeller && _details.postData.salesType=="rent"){    
                _moveToNextStep(details);
            }else{
                if(details.displayData.configurationText || (details.prevStep && ["VIEW_PHONE_MORPHED", "CALL_NOW"].indexOf(details.prevStep) > -1)) {
                    _moveToNextStep(details);
                }else {
                    _connectSellerBuyer(details);
                }
            }
            _showHideProcessing(true);

        }

        function _verifyAndConnect(formData, details) {
            let step = details.step;

            LeadService.verifyOTP(details).then((otp_response) => {
                _setEnquiryCookie(formData);
                _updateResponseInData(step, details, otp_response);
                _updateContactNumbers(details);
                _details.buyerUserId = _details.response.OTP.userId;

                if (details.response[step].isVerified) {
                    _trackLead("OTP_CORRECT");
                    _sendPushNotification("OTP_VERIFIED");
                    !details.isLoggedIn && _updateNumberExpiryTime(details); // jshint ignore:line
                    postHomeLoanLqLead();
                    if (details.displayData.configurationText) {
                        details.postData.sendOtp = false;
                        details.postData.phone && _postEnquiry(_details, "MAIN_TOP_SELLER"); // jshint ignore:line
                        _moveToNextStep(details);
                    } else if (details.prevStep && ["VIEW_PHONE_MORPHED", "CALL_NOW"].indexOf(details.prevStep) > -1) {
                        details.postData.sendOtp = false;
                        details.postData.phone && _postEnquiry(_details, details.prevStep,true); // jshint ignore:line
                        _trackLead(details.prevStep + "_SUBMIT_CLICKED");
                    } else{
                        let callback = _moveToNextStep.bind(undefined,details); 
                        if (global.isMakaanSelectSeller && details.postData.salesType=="rent") {
                            details.postData.sendOtp = false;
                            _postTempEnquiry({}, details, callback);
                        }else{
                            callback();
                        }
                    }
                } else {
                    _trackLead("OTP_INCORRECT");
                    _showHideProcessing(true);
                    context.broadcast("SHOW_ERROR", { moduleEl, errorType: "INVALID", field: "OTP" });
                }

            }, () => {
                context.broadcast("SHOW_ERROR", { moduleEl, errorType: "GENERIC", field: "GLOBAL" });
            }).always(() => {
                _showHideProcessing(true);
            });
                    

        }

        function _postSimilarEnquiry(formData, details) {
            let step = details.step;
            LeadService.postSimilarListingEnquiry(formData).then((response) => {
                response && response.forEach(function(responseObject) {
                  _trackLeadApiSuccess(PETRA,responseObject);
                });
                _trackLead("SIMILAR_LEAD_SUBMIT_SUCCESS", formData);
                _updateResponseInData(step, details, response);
                _showHideProcessing(true);
                _moveToNextStep(details);
            }, () => {
                _trackLeadApiFailure(PETRA);
                _showHideProcessing(true);
                context.broadcast("SHOW_ERROR", { moduleEl, errorType: "GENERIC", field: "GLOBAL" });
            });
        }

        function _showHideProcessing(hide) {
            let actionButton = $(moduleEl).find(config.selector["NORMAL"]),
                processingButton = $(moduleEl).find(config.selector["PROCESSING"]);
            if (hide) {
                processingButton.addClass(config.hideClass);
                actionButton.removeClass(config.hideClass);
                global.isProcessing = false;
            } else {
                processingButton.removeClass(config.hideClass);
                actionButton.addClass(config.hideClass);
                global.isProcessing = true;
            }
        }

        function _changeViewStatus() {
            context.broadcast('view_status_changed');
        }


        function _submit(step, details,skip=false) {
            if (global.isProcessing) {
                return;
            }
            let formData = _fetchDataFromUI(step);
            switch (step) {
                case "MAIN":
                case "MAIN_TOP_SELLER":
                case "VIEW_PHONE_MORPHED":
                case "CALL_NOW":
                    _showHideProcessing();
                    LeadService.updateUserFields(formData, details.postData, step);
                    LeadService.updateOTPFlag(details).always(() => {
                        _setEnquiryCookie(formData);
                        _trackLead(step + "_SUBMIT_CLICKED");
                        _sendPushNotification("LEAD_SUBMIT");
                        LeadPyrService.setAlreadyContact(details.postData.listingId);
                        _changeViewStatus();
                        let callback = (function(details) {
                            _moveToNextStep(details);
                            if (global.isMakaanSelectSeller && details.postData.salesType=="rent") { 
                                _showHideProcessing(true);
                            }else if (!details.displayData.configurationText && !(details.postData.sendOtp) && LeadService.isIndian(details) && ["VIEW_PHONE_MORPHED", "CALL_NOW"].indexOf(details.prevStep)== -1) { // jshint ignore:line
                                _connectSellerBuyer(details, false);
                            } else if (!details.postData.sendOtp) {
                                details.postData.phone && _postEnquiry(details, step); // jshint ignore:line
                            }
                        }).bind(undefined, details);
                        if (details.postData.sendOtp || (global.isMakaanSelectSeller && details.postData.salesType=="rent")) {
                            _postTempEnquiry(formData, details, callback);
                        } else {
                            callback();
                        }
                    });
                    break;
                case "OTP":
                    _showHideProcessing();
                    LeadService.updateUserFields(formData, details.postData, step);
                    _verifyAndConnect(formData, details);
                    break;
                case "USER_DETAILS":   
                    let trackType = skip ? 'USER_DETAILS_SKIP_CLICKED':'USER_DETAILS_SUBMIT_CLICKED';
                    _trackLead(trackType);   
                    _showHideProcessing();
                    LeadService.updateUserFields(formData, details.postData, step);
                    _updateUserDetailsAndConnect(formData, details);
                    break;
                case "SHOW_SELECT_COUPON":
                    _showHideProcessing();
                    _connectAfterCouponScreen(details);
                    break;
                case "SIMILAR":
                case "SIMILAR_SELLERS":
                    _showHideProcessing();
                    $.each(formData, (k, v) => {
                        LeadService.updateUserFields(details.postData, v, step);
                    });
                    _trackLead(step + "_LEAD_SUBMIT_CLICKED", formData);
                    _postSimilarEnquiry(formData, details);
                    break;
            }
        }

        function _handleSelectAll(step) {
            let selectAllCheckbox = $(moduleEl).find(config.selector[step]["ALL_LISTINGS_CHECKBOX"]),
                selectedListing = $(moduleEl).find(config.selector[step]["LISTINGS_CHECKBOX_CHECKED"]).length,
                allListing = $(moduleEl).find(config.selector[step]["LISTINGS_CHECKBOX"]).length;

            if (selectedListing === allListing) {
                selectAllCheckbox.prop("checked", true);
            } else {
                selectAllCheckbox.prop("checked", false);
            }
        }

        function _setInitialStep(details) {
            details.initialStep = details.step;
        }

        function _otherCompanyStringToArray(data) {
            if (data.otherCompany && !$.isArray(data.otherCompany)) {
                try {
                    data.otherCompany = JSON.parse(data.otherCompany);
                } catch (ex) {
                    data.otherCompany = [];
                }
            }
        }

        function _setModuleParametersInData(details) {
            details.moduleParameters = details.moduleParameters || {};
            if (moduleParameters) {
                $.extend(true, details.moduleParameters, moduleParameters);
            }
        }

        function _submitLeadExternally(data = {}) {
            _details = LeadService.processStepData(data);
            _postEnquiry(_details);
        }

        function handleStepSkipping(skipStep, moveTo, submitButton) {
            switch (skipStep) {
                case "VIEW_PHONE_MORPHED":
                    if (submitButton) {
                        let formData = _fetchDataFromUI("MAIN");
                        LeadService.updateUserFields(formData, _details.postData, skipStep);
                        _setEnquiryCookie(formData);
                    }
                    _trackLead(skipStep + "_SUBMIT_CLICKED");
                    _details.step = moveTo;
                    _details.stepsSkipped.push(skipStep);
                    if(_details.postData.phone){
                        postHomeLoanLqLead();
                        return _postEnquiry(_details, skipStep); // jshint ignore:line
                    }else{
                        return Promise.resolve({});
                    }
                    break;
            }
            _details.step = moveTo;
            _details.stepsSkipped.push(skipStep);
        }

        function _stepInterceptor(rawData, submitButton) {
            _details = (!_details || !_details.displayData) && rawData && LeadService.processStepData(rawData) || _details;
            let step = submitButton ? submitButton : _details.step,
                defer;
            switch (step) {
                case "VIEW_PHONE_MORPHED":
                case "MAIN_VIEW_PHONE":
                    defer = $.Deferred();
                    LeadService.getCallingNumber(_details.displayData.companyUserId, _details.postData.cityId, _details.postData.salesType).then(() => {
                        LeadService.updateOTPFlag(_details).always(() => {
                            let moveTo = global.isMakaanSelectSeller && _details.postData.salesType=="rent" ? "SHOW_SELECT_COUPON": "SIMILAR";
                            if(!_details.postData.sendOtp){
                                handleStepSkipping("VIEW_PHONE_MORPHED", moveTo, submitButton).always(() => {
                                    defer.resolve(_details);
                                });
                            }else{
                                defer.resolve(_details);
                            }
                        });
                    }, () => {
                        defer.resolve(_details);
                    });
                    return defer.promise();
                case "MAIN":
                    defer = $.Deferred();
                    LeadService.getCallingNumber(_details.displayData.companyUserId, _details.postData.cityId, _details.postData.salesType).then((response) => {
                        response.callingNumber && _updateCallingNumber(_details, response.callingNumber); // jshint ignore:line
                        defer.resolve(_details);
                    }, () => {
                        defer.resolve(_details);
                    });
                    return defer.promise();
                default:
                    return Utils.getEmptyPromise(_details);
            }
        }

        function _preProcessRawData(rawData) {
            _otherCompanyStringToArray(rawData);
        }
        
        function _setPropDetailsText(budget){
            budget = budget || Utils.getPageData('budget') ||'';
            let urlData = urlService.getAllUrlParam();
            global.isExperimentCity = global.isExperimentCity || experimentCity.indexOf(urlData.testCity)> -1;
            global.isContactExperimentCity = global.isContactExperimentCity || contactExperimentCities.indexOf(urlData.testCity)> -1;
            let displayData = _details.displayData||{},
                postData = _details.postData||{},
                propDetailsText = '';

            if(postData.bhk || displayData.propertyTypeLabel){
                propDetailsText = `${postData.bhk ? postData.bhk+'BHK' : ''} ${displayData.propertyTypeLabel||''} | `;
            }
            if(displayData.projectName || postData.cityName){
                propDetailsText = `${propDetailsText}${displayData.projectName && displayData.projectName.toLowerCase()!="project" ? displayData.projectName + ',':''}${postData.cityName||''} `;
            }

            _details.displayData.showPrice = displayData.price;

            if(!(displayData.price && displayData.price.val)){
                _details.displayData.showPrice = Utils.formatNumber(budget, { precision : 2, returnSeperate : true});
            }
            _details.displayData.propDetailsText = propDetailsText;
            _details.displayData.isExperimentCity = global.isExperimentCity;
            _details.displayData.isContactExperimentCity = global.isContactExperimentCity;
        }

        function _processAndRender() {
            _setModuleParametersInData(_details);
            _gotoStep(_details.step, _details);
            _setInitialStep(_details);
            global.isProcessing = false;
        }

        function _showResendButton(show = true) {
            if (!show) {
                $(moduleEl).find(config.selector["OTP"]["RESEND"]).addClass(config.hideClass);
            } else {
                $(moduleEl).find(config.selector["OTP"]["RESEND"]).removeClass(config.hideClass);
            }
        }

        function _resendOTP(details) {
            delete details.postData.otp;
            _showResendButton(false);
            LeadService.verifyOTP(details).always(() => {
                _showResendButton();
            }).then(() => {
                context.broadcast("SHOW_SUCCESS", { moduleEl, successType: "SUCCESS_OTP", field: "GLOBAL" });
            }, () => {
                context.broadcast("SHOW_ERROR", { moduleEl, errorType: "GENERIC", field: "GLOBAL" });
            });
            $(moduleEl).find('[name=otp_lead]').val('');
            _focusInput("otp_lead");
            window.setTimeout(_showResendButton, config.resendTryTime);
        }

        function _trigger(eventName, data = {}) {
            context.broadcast(eventName, data);
        }

        function _fireModuleLoaded() {
            _trigger("moduleLoaded", { name: "lead", id: moduleEl.id });
        }
        
        function init() {
            //initialization of all local variables
            configData = context.getConfig();
            global.isMakaanSelectSeller = configData.isMakaanSelectSeller && configData.isMakaanSelectSeller.toString() == "true" || false; 
            global.hasTenantPreferences = configData.hasTenantPreferences || false;
            let isPopup = configData.isPopup;
            moduleEl = context.getElement();
            isCommercial = Utils.getPageData('isCommercial');
            TrackLeadEvents.setTrackingConfig();
            LeadService.parseUrlQueryParams();
            moduleParameters = LeadService.getModuleParameters(configData);
            configData.isPopup = isPopup;
            moduleParameters.isPopup = isPopup;
            if (!moduleParameters.isPopup) {
                _preProcessRawData(configData);
                _stepInterceptor(configData).always(() => {
                    defaultService.getCityByLocation().always((response) => {
                        let userCity = response || {};
                        if ((userCity && userCity.id)){
                            global.isExperimentCity = experimentCity.indexOf(userCity.id) > -1;
                            global.isContactExperimentCity = contactExperimentCities.indexOf(userCity.id) > -1;
                        }
                        _setPropDetailsText(configData.budget||'');
                        _processAndRender();
                        _fireModuleLoaded();
                    });
                });
            }else{
                _fireModuleLoaded();
            }

            loginService.isUserLoggedIn().then((response) => {
                _setUserProfileImage(response.profileImageUrl);
            }, () => {});

        }
        function launchFeedbackPopup(details, data, throughMProfile){
            if(_ismProfilePopupOpen() && throughMProfile !== true) {
                feedbackData = data;
                return;
            }
            let obj = {};
            if(details){
                obj.buyerId = details.response[details.initialStep].userId || details.response[details.step].userId || details.response[details.prevStep].userId;
                obj.companyId = details.postData.multipleCompanyIds[0];
                obj.listingId = details.postData.listingId;
                obj.phone = details.postData.phone;
                obj.email = details.postData.email;
                obj.name = details.postData.name;
                obj.cityName = details.postData.cityName;
                obj.cityId = details.postData.cityId;
                obj.propertyPrice = details.postData.minBudget;
                obj.salesType = details.postData.salesType;
                obj.sellerId = details.displayData.companyUserId;
            } else {
                obj.buyerId = data.callerUserId;
                obj.companyId = data.companyId;
                obj.listingId = data.listingId;
                obj.phone = data.phone;
                obj.sellerId = data.companyUserId;
            }
            obj.throughLeadForm = true;
            obj.throughMProfile = throughMProfile === true;
            feedbackService.getRating(obj.buyerId, obj.sellerId).then(response => {
                if(!response.length){
                    if(_isLeadPopupOpen()){
                        return _closePopup(function(){context.broadcast('open_feedback', obj);});
                    }else{
                        return context.broadcast('open_feedback', obj);
                    }
                } else {
                    _handleClose();
                }
            }, () => {
                _handleClose();
            });
        }

        function destroy() {
            //clear all the binding and objects
            _details = _stepsStack = [];
        }
        function trackSelectListingClicked(saleType,listingId){
            if(global.isMakaanSelectSeller && saleType && saleType.toLowerCase()=="rental"){
                context.broadcast('selectListingClicked',{id:moduleEl.id,source:listingId});
            }
        }

        function getCallId(details, step) {
            return details && details.response && details.response[step] && details.response[step].callId;
        }

        function onmessage(name, data) {
            // bind custom messages/events
            let callId;
            switch (name) {
                case "Lead_Open_Step":
                    if (data.id === moduleEl.id) {
                        global.isMakaanSelectSeller = data.isMakaanSelectSeller && data.isMakaanSelectSeller.toString() =="true" || false;
                        global.hasTenantPreferences = data.hasTenantPreferences || false;
                        global.imageCount = data.imageCount;
                        trackSelectListingClicked(data.salesType,data.listingId); 
                        _preProcessRawData(data);
                        _stepInterceptor(data).always(() => {
                            defaultService.getCityByLocation().always((response) => {
                                let userCity = response || {};
                                if ((userCity && userCity.id)){
                                    global.isExperimentCity = experimentCity.indexOf(userCity.id) > -1;
                                    global.isContactExperimentCity = contactExperimentCities.indexOf(userCity.id) > -1;
                                }
                                _setPropDetailsText(data.budget||'');
                                _processAndRender();
                            });
                        });
                        _sendPushNotification("LEAD_OPEN");
                    }
                    break;
                case "postLead":
                    if (data.id === moduleEl.id) {
                        _submitLeadExternally(data);
                    }
                    break;
                case "lead_no_similar":
                    if (data.id == moduleEl.id) {
                        if (_details) {
                            _details.noSimilar = true;
                        }
                    }
                    break;
                case "NewCall":
                    _sendPushNotification(name, _details);
                    break;
                case "Hangup":
                case "Disconnect":
                    callId = _details && (getCallId(_details, _details.initialStep) || 
                                getCallId(_details, _details.step) || 
                                getCallId(_details, _details.prevStep)||getCallId(_details,"OTP"));
                    if (data.id === moduleEl.id && data.callId === callId) {
                        _updateCallStepStack(name + ":" + data.callStatus);
                        let nextCallStep = _getCallStep(name, data.callStatus);
                        _details.displayData.callStep = nextCallStep;
                        if (nextCallStep === "Dial" && _details.step !== "USER_PROFILING" && _details.step !=="SIMILAR"){
                            _renderView("CALL_STATUS_SCREEN", _details);
                        } else {
                            _dismissStep();
                        }
                        _sendPushNotification(name, _details);
                    }
                    break;
                case "Dial":
                    if (!global.isWaitingForFeedbackEvent) {
                        return;
                    }
                    callId = _details && (getCallId(_details, _details.initialStep) || 
                                getCallId(_details, _details.step) || 
                                getCallId(_details, _details.prevStep)||getCallId(_details,"OTP"));
                    if (data.id === moduleEl.id && data.callId === callId) {
                        _updateCallStepStack(name + ":" + data.callStatus);
                        let nextCallStep = _getCallStep(name, data.callStatus);
                        _details.displayData.callStep = nextCallStep;
                        if(_details.step !== "USER_PROFILING" && _details.step !=="SIMILAR") {
                            _renderView("CALL_STATUS_SCREEN", _details);
                        }
                        _sendPushNotification(name, _details);
                    }
                    break;
                case "call_feedback":
                    if (!global.isWaitingForFeedbackEvent) {
                        return;
                    }
                    if (data.id === moduleEl.id && data.callId === callId) {
                        _updateCallStepStack(name + ":" + data.callStatus);
                        _sendPushNotification(name, _details);
                    }
                    feedbackData  = data;
                    _dismissStep();        
                    break;
                case "call_ended":
                    callId = _details &&(getCallId(_details, _details.initialStep) || 
                                getCallId(_details, _details.step) || 
                                getCallId(_details, _details.prevStep)||getCallId(_details,"OTP"));
                    if (data.id === moduleEl.id && data.callId === callId) {
                        if (_callStepStack.length === 1 || !_callStepStack.length) {
                            _updateCallStepStack("Hangup");
                            _details.displayData.callStep = "Hangup";
                           _dismissStep();
                        } else if (_callStepStack.indexOf("Dial:answered") >= 0) {
                            _gotoStep("SIMILAR", _details);
                        }
                    }
                    break;
                case "userLoggedIn":
                    _setUserProfileImage(data.profileImageUrl);
                    break;
                case "userLoggedOut":
                    _unsetUserProfileImage();
                    break;

                case "navigateNextStep":
                    if (data && data.id === moduleEl.id && data.step) {
                        _submit(data.step, _details);
                    }
                    break;
                case "leadFormError":
                    _trackLead('LEAD_FORM_ERROR', data);
                    break;
                case "userProfilingCompletedViaPopup":
                case "userProfilingPopupClosed":
                    if(feedbackData !== null) {
                        //Feedback popup was stalled due to mProfiling or SIMILAR SELLERS
                        launchFeedbackPopup(_details, feedbackData, true);
                        feedbackData = null;
                    }
                case "userProfilingCompletedViaPopup":
                    if (_details){
                        _removeUserProfieFromCard();
                        checkDeficitAndShowThanks(_details);
                    } else {
                        _closePopup();
                    }
                    break;
                case "userProfilingPopupOpenViaMobile":
                    if(moduleParameters.isPopup){
                        if (typeof _details === 'undefined') {
                            _details = {};
                        }
                        if (data && data.isPopupRequest) {
                            _details.isPopup = true;
                            _details.isSticky = false;
                        } else {
                            _details.isSticky = true;
                        }
                        _details.step = 'USER_PROFILING_MOBILE';
                        _gotoStep("USER_PROFILING_MOBILE", _details);
                    }
                    break;
            }
        }

        function _removeUserProfieFromCard() {
            $(config.selector.LEAD_NO_SHADOW).removeClass('lp-col-no-shadow');
            $(config.selector.GALLERY_LEAD_FORM).removeClass('lp-col-no-shadow');
        }

        function _setUserProfileImage(imageUrl) {
            if (imageUrl) {
                moduleParameters.profileImageUrl = imageUrl;
            }
        }

        function _unsetUserProfileImage() {
            delete moduleParameters.profileImageUrl;
        }

        function _getCallStep(callEvent, callStatus) {
            let newCallStep = callEvent;
            if (callEvent === "Hangup" || callEvent === "Disconnect") {
                if ((_callStepStack.indexOf("Dial:answered") >= 0) || callStatus === "answered") {
                    newCallStep = "Dial";
                } else if ((_callStepStack.indexOf("Dial:unanswered") >= 0)) {
                    newCallStep = "Disconnect";
                }
            } else if (callEvent === 'Dial' && callStatus === 'unanswered') {
                newCallStep = "Disconnect";
            }
            return newCallStep;
        }

        function onclick(event, element, elementType) {
            let url,
            pageDataModule = Utils.getPageData('moduleName'),popupId,
            source = pageDataModule && pageDataModule =="serp" ? 'LeadFormSerp':'LeadFormProperty';
            switch (elementType) {
                case "MAIN_VIEW_CONNECT":
                case "VIEW_PHONE_MORPHED_CONNECT":
                    _moveToNextStep(_details, "MAIN_VIEW_CONNECT");
                    break;
                case "MAIN_VIEW_PHONE":
                    _moveToNextStep(_details, "MAIN_VIEW_PHONE");
                    break;
                case "OTP_ONCALL":
                    LeadPyrService.sendOTPOnCall({
                        userId: _details.response[_details.prevStep].userId,
                        userNumber: _details.postData.phone,
                        element: $(element)
                    });
                    break;
                case "OTP_RESEND":
                    _resendOTP(_details);
                    _trackLead("OTP_RESEND");
                    break;
                case "SIMILAR_SKIP":
                    _moveToNextStep(_details, "SIMILAR_SKIP");
                    _trackLead("SIMILAR_SKIP_CLICKED");
                    break;
                case "SIMILAR_SELECT_ALL":
                    if ($(element).is(":checked")) {
                        _trackLead("SIMILAR_SELECT_ALL");
                    }
                    $(moduleEl).find(config.selector["SIMILAR_SELLERS"]["LISTINGS_CHECKBOX"]).prop("checked", $(element).prop("checked"));
                    break;
                case "SIMILAR_CHECKBOX":
                    _handleSelectAll("SIMILAR");
                    break;
                case "SIMILAR_SELLERS_CHECKBOX":
                    _handleSelectAll("SIMILAR_SELLERS");
                    break;
                case "THANKS_GOTIT":
                case "SIMILAR_GOTIT":
                case "SIMILAR_SELLERS_GOTIT":
                    _trackLead("THANKS_GOTIT_CLICKED");
                    _trigger("Lead_Flow_Completed", { id: moduleEl.id });
                    _closePopup();
                    break;
                case "THANKS_JOURNEY":
                case "SIMILAR_SELLERS_JOURNEY":
                case "SIMILAR_JOURNEY":
                    _trackLead("THANKS_JOURNEY_CLICKED");
                    url = $(moduleEl).find(config.selector["THANKS"]["JOURNEY"]).data('journey-url');
                    _closePopup();
                    setTimeout(() => {
                        CommonService.ajaxify(url);
                    });
                    break;
                case "VIEW_PHONE_SUMIT":
                    _trackLead("THANKS_GOTIT_CLICKED");
                    _trigger("Lead_Flow_Completed", { id: moduleEl.id });
                    _closePopup();
                    break;
                case "VIEW_PHONE_JOURNEY":
                    _trackLead("VIEW_PHONE_JOURNEY_CLICKED");
                    url = $(moduleEl).find(config.selector["VIEW_PHONE"]["JOURNEY"]).data('journey-url');
                    _closePopup();
                    setTimeout(() => {
                        CommonService.ajaxify(url);
                    });
                    break;
                case "BACK":
                    _trackLead("BACK");
                    _moveToNextStep(_details, "BACK");
                    _showHideProcessing(true);
                    context.broadcast('otpBackButtonClicked');
                    break;
                case "CONTINUE_SEARCH":
                    _trackLead("LEAD_CONTINUE_SEARCH");
                case "CLOSE_LEAD":
                    if(element.classList.contains(PROP_EXP_CLASS)){
                        _trackLead('CLOSE_PROP_LEAD');
                    }
                    _handleClose("CLOSE_LEAD");
                    break;
                case "CALL_STATUS_RETRY":
                    _clearCallStepStack();
                    _details.displayData.callStep = "NewCall";
                    _updateCallStepStack("NewCall");
                    _gotoStep("CALL_STATUS_SCREEN", _details);
                    _connectSellerBuyer(_details, false);
                    break;
                case "DISMISS_LEAD":
                    _dismissStep();
                    break;
                case "BUYER_OPT_IN":
                    let checkedStatus = $(element).prop("checked");
                    _details.displayData.optInCheckedStatus = checkedStatus;
                    LeadService.updateOptInStatus(checkedStatus);
                    break;
                case "HOMELOAN_OPT_IN":
                    let isHomeLoanChecked = $(element).prop("checked");
                    _details.isHomeLoanChecked = isHomeLoanChecked;
                    break;
                case "skipUserDetails":  
                     _submit("USER_DETAILS", _details,true);
                     break;
                case "CLOSE_USER_DETAILS":  
                    _handleClose("CLOSE_USER_DETAILS");
                    break;
                case "homeloan_apply":
                    _handleClose("Homeloan_APPLY_NOW", openHomeloanForm);
                    if(moduleParameters && moduleParameters.isPopup === false)
                        openHomeloanForm();
                    break;
                case "showCoupon_proceed":
                    context.broadcast('selectCouponClicked',{id:moduleEl.id});
                    _submit("SHOW_SELECT_COUPON", _details);
                    break;
                case 'mainMSKnowMore':
                    context.broadcast('trackSelectKnowMore',{id:moduleEl.id,source});
                    popupId = Utils.isMobileRequest() ? 'msKnowMorePopup' : 'msKnowMoreDeskPopup';
                    _openPopup(popupId);
                    break;
                case 'viewPhomeMSKnowMore':
                    context.broadcast('trackSelectKnowMore',{id:moduleEl.id,source});
                    popupId = Utils.isMobileRequest() ? 'msKnowMorePopup' : 'msKnowMoreDeskPopup';
                    _openPopup(popupId);
                    break;
                case 'searchSelectProperties':
                    let urlparams = {postedBy:'selectAgent'};
                    let url =  filterConfigService.getUrlWithUpdatedParams({overrideParams: urlparams});
                    window.location.href = url;
                    break;
                case 'tipSimilarMSKnowMore':
                    context.broadcast('trackSelectKnowMore',{id:moduleEl.id,source:`${source}_Similar`});
                    popupId = Utils.isMobileRequest() ? 'msKnowMorePopup' : 'msKnowMoreDeskPopup';
                    _openPopup(popupId);
                    break;
                case 'tipCallingMSKnowMore':
                    context.broadcast('trackSelectKnowMore',{id:moduleEl.id,source:`${source}_Calling`});
                    popupId = Utils.isMobileRequest() ? 'msKnowMorePopup' : 'msKnowMoreDeskPopup';
                    _openPopup(popupId);
                    break;
                case 'preLeadSelect_proceed':
                    context.broadcast('trackPreLeadSelect',{id:moduleEl.id,source});
                    _processAndRender();
                    break;
                case 'deficitProjectConnect':
                    let dataset = element.dataset || {};
                    _trackDeficitSellerProject('click',dataset);
                    window.location.href = dataset.href;
                    break;
            }
        }

        function _openPopup(id) {
            context.broadcast('popup:open', {
                id: id,
                options: {
                    config: {
                        modal: true
                    }
                }
            });
        }

        function _openMprofile(step){
            if (M_PROFILE_CLOSE_STEP.indexOf(step) > -1) {
                context.broadcast('userProfilingPopupClosed');
            } else if (M_PROFILE_SKIP_STEP.indexOf(step) == -1 && !_isMProfileOpenInCard()) {
                context.broadcast("userProfilingPopupOpen", {});
                return true;
            }
        }

        function _dismissStep() {
            if(_details && _details.step=="CALL_STATUS_SCREEN"){
                 _gotoStep("SIMILAR", _details);
            }
        }

        function _handleClose(trackEvent, popupCloseCb) {
            let step = _details.step;
            _appendHtmlToView('',null);
            popupCloseCb = popupCloseCb || function(){
                if(!_openMprofile(step)) {
                    //mProfile not opened, load feedback popup
                    feedbackService.getFeedbackStatus();
                }
            }
            _trackLead(trackEvent);
            _clearStepsStack();
            _closePopup(popupCloseCb);
            Utils.executeHotjarTrigger(HOTJAR_LEAD_CLOSE_TRIGGER);
        }

        function onmouseover(event, element, elementType) {
            switch (elementType) {
                case "MAIN_TOP_SELLER_BOX":
                    $(moduleEl).find(config.selector["TOP_SELLER"]).removeClass(config.activeClass);
                    $(element).addClass(config.activeClass);
                    break;
            }
        }
        function onkeyup(event, element, elementType) {
            if(event.keyCode == 37 || event.keyCode == 39){ //37:left key arrow , 39:right key arrow
                event.stopPropagation();                    // stopping event from being propogated to light gallery
            }
        }

        function _ismProfilePopupOpen(){
            return $(config.selector.M_PROFILE_POPUP).hasClass('show-popup') ||
            (_details && _details.step === "USER_PROFILING");
        }
        function _isLeadPopupOpen(){
            return  $(`#${moduleEl.id}`) && $(`#${moduleEl.id}`).closest('[data-role="popup"]').hasClass('show-popup');
        }

        return {
            init,
            messages,
            behaviors,
            onmessage,
            onclick,
            onmouseover,
            destroy,
            onkeyup
        };
    });
});
