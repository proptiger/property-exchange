define([
    'common/trackingConfigService',
    'services/trackingService',
    'services/urlService',
    'services/utils',
], function(t, trackingService, urlService,utils) {
    'use strict';
    Box.Application.addBehavior('newLeadFormTracking', function(context) {

        var element;
        const messages = [
            'trackViewPhoneLead',
            'trackBackLead',
            'trackCloseLead',
            'trackDismissLead',
            'trackSelectAllSimilarLead',
            'trackConnectNowLead',
            'trackOtpViewedLead',
            'trackOtpCorrectLead',
            'trackOtpIncorrectLead',
            'trackOtpResendLead',
            'trackCallNowLead',
            'trackSimilarSubmitLead',
            'trackLeadFormErrors',
            'trackViewPhoneSeenMorphedLead',
            'trackClaimNowLead',
            'viewPhoneSubmitLead',
            'trackFeedbackRating',
            'trackFeedbackShown',
            'trackLeadContinueHomeloan',
            'trackLeadContinueSearch',
            'trackUserDetailSkipped',
            'trackUserDetailSubmitted',
            'trackUserDetailClose',
            'trackCardSeen',
            'trackSelectKnowMore',
            'selectListingClicked',
            'selectCouponSeen',
            'selectCouponClicked',
            'trackPreLeadSelect',
            'trackLeadFormDeficitProject_seen',
            'trackLeadFormDeficitProject_click',
            'trackLeadApiSuccess',
            'trackLeadApiFailure',
            'trackPropCloseLead'
        ];

        function _fireVWOGoal(){
            if(document.cookie.indexOf('_vis_opt_exp_4_goal_1') > -1 && window.pageData && window.pageData.isSerp){
                window._vis_opt_queue = window._vis_opt_queue || [];
                window._vis_opt_queue.push(function() {_vis_opt_goal_conversion(200);});
            }
        }
        function _fireTrovitTrackingEvents(){
            if(ta && typeof ta === 'function'){
                ta('init', 'in', 1, '08da1cf9c6fd35673244a4f70cb27eed');
                ta('send', 'lead');
            }
        }

        var init = function() {
            element = context.getElement();
        };

        var onmessage = function(name, data) {
            if(data.id !== element.id){
                return;
            }
            let properties = {};
            let category = t.LEAD_FORM_CATEGORY,
                labelConfig = t.LEAD_LABELS,
                //sourceModuleConfig = t.LEAD_SOURCE_MODULES,
                valueConfig = t.LEAD_VALUES,
                value = urlService.getUrlParam('page') || 1,
                event, label, nonInteraction,projectId,sellerId,uniqueId,sectionClicked;
            let pageData = utils.getPageData();
            let sourceModule = data && data['Source Module'] || '';

            switch (name) {
                case 'trackViewPhoneLead':
                    event = t.SUBMIT_EVENT;
                    label = labelConfig["ViewPhone"];
                    _fireTrovitTrackingEvents();
                    break;
                case 'trackViewPhoneSeenMorphedLead':
                    event = t.VIEW_PHONE_MORPHED_SEEN_EVENT;
                    break;    
                case 'viewPhoneSubmitLead':
                    event = t.SUBMIT_EVENT;
                    label = labelConfig["ViewPhone"];
                    _fireVWOGoal();
                    _fireTrovitTrackingEvents();
                    break;
                case 'trackBackLead':
                    event = t.NAVIGATION_EVENT;
                    value = valueConfig.Back;
                    break;

                case 'trackCloseLead':
                    event = t.NAVIGATION_EVENT;
                    value = valueConfig.Close;
                    break;
                case 'trackPropCloseLead':
                    event = t.NAVIGATION_EVENT;
                    value = valueConfig.Close;
                    sourceModule = "Prop Exp";
                    break;
                case 'trackDismissLead':
                    event = t.NAVIGATION_EVENT;
                    value = valueConfig.Dismiss;
                    break;

                case 'trackSelectAllSimilarLead':
                    event = t.SELECT_ALL_SELLERS_EVENT;
                    break;

                case 'trackOtpViewedLead':
                    event = t.OTP_EVENT;
                    label = labelConfig.Visible;
                    break;

                case 'trackOtpCorrectLead':
                    event = t.OTP_EVENT;
                    label = labelConfig.Correct;
                    break;

                case 'trackOtpIncorrectLead':
                    event = t.OTP_EVENT;
                    label = labelConfig.Incorrect;
                    break;

                case 'trackOtpResendLead':
                    event = t.OTP_EVENT;
                    label = labelConfig.Resend;
                    break;

                case 'trackConnectNowLead':
                    event = t.SUBMIT_EVENT;
                    label = labelConfig.ConnectNow;
                    _fireVWOGoal();
                    _fireTrovitTrackingEvents();
                    break;

                case 'trackCallNowLead':
                    event = t.SUBMIT_EVENT;
                    label = labelConfig.CallNow;
                    _fireVWOGoal();
                    _fireTrovitTrackingEvents();
                    break;

                case 'trackSimilarSubmitLead':
                    event = t.SUBMIT_EVENT;
                    label = data.label;
                    sectionClicked = data.sectionClicked;
                    _fireTrovitTrackingEvents();
                    break;
                case 'trackLeadFormErrors':
                    event = t.ERROR_EVENT;
                    category = t.LEAD_FORM_ERROR_CATEGORY;
                    label = data.label;
                    nonInteraction = 0;
                    break;

                case 'trackClaimNowLead':
                    event = t.CLAIM_NOW_EVENT;
                    break;

                case 'trackFeedbackRating':
                    event = t.FEEDBACK_RATING_STORED;
                    category = t.FEEDBACK_CATEGORY;
                    label = data.callId;
                    value = data.rating;
                    break;
                case 'trackFeedbackShown':
                    event = t.FEEDBACK_OPEN;
                    category = t.FEEDBACK_CATEGORY;
                    label = data.callId;
                    break;
                case 'trackLeadContinueHomeloan' :
                    event = 'continue';
                    label = "homeloanYes" ;
                    break;
                case 'trackLeadContinueSearch' :
                    event = 'continue';
                    label = "homeloanNo" ;
                    break;
                case 'trackUserDetailSkipped':
                    event =t.CLICK_EVENT;
                    sourceModule = 'skip';
                    break;
                case 'trackUserDetailSubmitted':
                    event =t.CLICK_EVENT;
                    sourceModule ='nameAndEmail';
                    break;
                case'trackUserDetailClose':
                    event =t.CLICK_EVENT;
                    sourceModule = 'close';
                    break;
                case 'trackCardSeen':
                    event = t.CARD_SEEN;
                    label = data.label;
                    break;
                case 'trackSelectKnowMore':
                    event = t.CARD_CLICK;
                    label="Program_Details_Modal";
                    category=t.SELECT_VOUCHER_GENERATION_CATEGORY;
                    sourceModule = data.source;
                    break;
                case 'selectListingClicked':
                    event = "Listing Clicked";
                    label="Seller_Promo_Serp";
                    category=t.SELECT_VOUCHER_GENERATION_CATEGORY;
                    sourceModule = data.source;
                    break;
                case 'selectCouponSeen':
                    event = "Modal Seen";
                    label=data.label;
                    category=t.SELECT_VOUCHER_GENERATION_CATEGORY;
                    sourceModule = data.source;
                    break;
                 case 'selectCouponClicked':
                    event = "Modal Proceed";
                    label="Voucher_Generated";
                    category=t.SELECT_VOUCHER_GENERATION_CATEGORY;
                    sourceModule = data.source;
                    break;
                case 'trackPreLeadSelect':
                    event = "Modal Proceed";
                    label="Pre_Lead_Select";
                    category='Makaan Select- Promo & Voucher Generation- buyers';
                    sourceModule = data.source;
                    break;
                case 'trackLeadFormDeficitProject_seen':
                    event = t.CARD_SEEN;
                    category = 'Deficit Campaign Sellers';
                    label = 'card_seen_deficit_leadForm';
                    projectId = data.projectId;
                    sellerId = data.sellerId;
                    break;
                case 'trackLeadFormDeficitProject_click':
                    event = t.CARD_CLICK;
                    category = 'Deficit Campaign Sellers';
                    label = 'card_clicked_deficit_leadForm';
                    projectId = data.projectId;
                    sellerId = data.sellerId;
                    sectionClicked = 'leadFormDeficitProject';
                    break;
                case 'trackLeadApiSuccess':
                    event = t.LEAD_API_SUCCESS_EVENT;
                    label = 'Lead Form';
                    uniqueId = data && data.uniqueId;
                    break;
                case 'trackLeadApiFailure':
                    event = t.LEAD_API_FAILURE_EVENT;
                    label = 'Lead Form';
                    uniqueId = data && data.uniqueId;
                    break;
            }
            properties = $.extend(true, properties, data);

            properties[t.SOURCE_MODULE_KEY] = sourceModule;

            properties[t.CATEGORY_KEY] = category;
            if(label){
                properties[t.LABEL_KEY] = label;
            }
            if(pageData && pageData.isProjectFeatured){
                // overriding label with prefix in case of featured proejct overview
                properties[t.LABEL_KEY] = 'Featured_'+ properties[t.LABEL_KEY];
            }
            if(sellerId){
                properties[t.SELLER_ID_KEY] = sellerId;
            }
            if(projectId){
                properties[t.PROJECT_ID_KEY] = projectId;
            }
            properties[t.VALUE_KEY] = value;
            properties[t.NON_INTERACTION_KEY] = nonInteraction;
            properties[t.SELECT_PROPERTY_KEY] = data.isSelectListing ? 'Select Listing':undefined;
            properties[t.UNIQUE_ENQUIRY_ID_KEY] = uniqueId;
            properties[t.SECTION_CLICKED] = sectionClicked;

            trackingService.trackEvent(event, properties);
        };

        return {
            messages,
            onmessage,
            init,
            destroy: function() {}
        };
    });
});
