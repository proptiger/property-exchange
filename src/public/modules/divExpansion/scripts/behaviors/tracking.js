'use strict';
define([
    'common/trackingConfigService',
    'services/trackingService'
], function(t, trackingService) {
    Box.Application.addBehavior('divExpansionTracking', function(context) {
        const CLOSE_MSG = 'divExpansion_close',
            EXPAND_MSG = 'divExpansion_expand';

        var messages = [CLOSE_MSG, EXPAND_MSG],
            moduleEl,
            onmessage = function(name, data) {
            if(data.moduleEl.id == moduleEl.id) {
                let event, sourceModule, category = t.DESCRIPTION_CATEGORY;

                switch (name) {
                    case CLOSE_MSG:
                        event = t.READ_LESS_EVENT;
                        break;
                    case EXPAND_MSG:
                        event = t.READ_MORE_EVENT;
                        break;
                }
                sourceModule = t[data.sourceModule];

                let properties = {};
                properties[t.CATEGORY_KEY] = category;
                properties[t.SOURCE_MODULE_KEY] = sourceModule;

                trackingService.trackEvent(event, properties);
            }
        };

        return {
            messages: messages,
            onmessage: onmessage,
            init: function() {
                moduleEl = context.getElement();
            },
            destroy: function() {}
        };
    });
});
