"use strict";
define([
    'services/loggerService',
    'services/utils',
    'modules/divExpansion/scripts/behaviors/tracking'
], function() {
    Box.Application.addModule("divExpansion", function(context) {
        const queryData = Box.DOM.queryData,
            logger = context.getService('Logger'),
            LIST_DATATYPE = 'list';

        var moduleEl,
            config,
            offsetHeight,
            _category;

        function init() {
            logger.info('initialization divExpansion module');
            moduleEl = context.getElement();
            config = context.getConfig() || {};
            _category = config.category;
            context.broadcast('moduleLoaded',{
                name: 'divExpansion',
                id: moduleEl.id
            });
        }

        function onclick(event, element, elementType) {
            if (elementType == 'toggleLink') {
                event.preventDefault();
                let parentElement = queryData(moduleEl, 'type', LIST_DATATYPE);
                if($(element).hasClass('active')){
                    context.broadcast('divExpansion_close', {sourceModule: _category, moduleEl});
                    $(element).removeClass('active');
                    $(parentElement).removeClass('active');
                    $('body').animate({
                        'scrollTop': offsetHeight || $(parentElement).offset().top
                    }, 300);
                } else {
                    context.broadcast('divExpansion_expand', {sourceModule: _category, moduleEl});
                    offsetHeight = $('body').scrollTop();
                    $(parentElement).addClass('active');
                    $(element).addClass('active');
                }
                // if(dataset.trackLabel){
                //     utils.trackEvent(trackAction, pageCategory, dataset.trackLabel + (trackFlag ? '_less' : '_more'));
                // }
                event.stopPropagation();

            }
        }

        function destroy() {}

        return {
            init,
            destroy,
            onclick,
            behaviors: ['divExpansionTracking']
        };
    });
});
