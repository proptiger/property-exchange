"use strict";
define([
    'doT!modules/semLead/views/template',
    'doT!modules/semLead/views/thankYou',
    'modules/semLead/scripts/behaviors/semLeadBehavior',
    'services/commonService',
    'services/utils',
    'services/apiService',
    'common/sharedConfig',
    'services/defaultService',
    'services/filterConfigService'
], (template, thankYouTemplate, semLeadBehavior, CommonService, Utils, ApiService, SharedConfig, defaultService, filterConfigService) => {
    Box.Application.addModule('semLead', (context) => {
        const config = {
            selector: {
                "MAIN": {
                    "PHONE": "[data-type=MAIN_PHONE] input",
                    "COUNTRY_CODE": "[data-country-code=MAIN_COUNTRY_CODE]",
                    "COUNTRY_ID": "input[data-country=MAIN_COUNTRY]",
                    "EMAIL": "[data-type=EMAIL] input",
                },
                "MAIN_TOP_SELLER": {
                    "PHONE": "[data-type=MAIN_TOP_SELLER_PHONE] input",
                    "COUNTRY_CODE": "[data-country-code=MAIN_TOP_SELLER_COUNTRY_CODE]",
                    "COUNTRY_ID": "input[data-country=MAIN_TOP_SELLER_COUNTRY]"
                },
                "THANKS": {
                    "JOURNEY": "[data-journey-url]"
                },
                "WRAPPER": "[data-lead-container]",
                "PROCESSING": "[data-processing]",
                "NORMAL": "[data-normal]",
                "DROPDOWN": "[data-countryPopulate]",
                "LEADMODULE": "[data-module=semLead]"
            },
            messages: {
                "EMAIL": {
                    "INVALID": {
                        "MESSAGE": "email is invalid"
                    },
                    "EMPTY": {
                        "MESSAGE": "email is required"
                    }
                },
                "PHONE": {
                    "INVALID": {
                        "MESSAGE": "phone number is invalid"
                    },
                    "EMPTY": {
                        "MESSAGE": "phone number is required"
                    }
                },
                "NAME": {
                    "INVALID": {
                        "MESSAGE": "name is invalid"
                    }
                }
            },
            LEAD_COOKIE: "enquiry_info",
            HIDE_GALLERY_WRAP: ".hide-gallery",
            HIDE_VIEW_IMAGE_WRAP: ".hide-me-button",
            defaultHideTime: 5000,
            globalCallTimeout: 15000,
            hideClass: "hide",
            activeClass: "active"
        };
        var messages = ['singleSelectDropdownChanged', 'singleSelectDropdownInitiated', 'popup:open', 'trackSemProjectPhoneClick', 'trackSemProjectLeadFormOpen', 'trackSemLeadFormSubmit'],
            behaviors = ['semLeadBehavior'],
            moduleEl, configData = {
                hasSubmitterLead: false,
                isNewSession: true
            },
            parseData = {};

        function getLeadCookie(configData) {
            let cookieData = Utils.getCookie(config.LEAD_COOKIE);
            try {
                cookieData = JSON.parse(JSON.parse(cookieData));
                if (cookieData) {
                    configData.email = cookieData.email;
                    configData.phone = cookieData.phone;
                    configData.hasSubmitterLead = true;
                }
            } catch (e) {}
            return configData;
        }

        function _startAllModules(moduleEl) {
            CommonService.startAllModules(moduleEl);
        }

        function init() {
            //initialization of all local variables
            configData = context.getConfig();
            moduleEl = context.getElement();
            configData = getLeadCookie(configData);
            $(moduleEl).find("[data-lead-container]").html(template(configData));
            _startAllModules(moduleEl);
            configData.isNewSession = true;
            if (configData.hasSubmitterLead) {
                _showGallery();
            }
        }

        function destroy() {
            //clear all the binding and objects
        }

        function onmessage(name, data) {
            // bind custom messages/events
            switch (name) {
                case "singleSelectDropdownChanged":
                case "singleSelectDropdownInitiated":
                    let params = data && data.params && JSON.parse(decodeURI(data.params));
                    _setCountry(params);
                    let dropDowntoChange = $(configData.otherModuleId).find(config.selector["DROPDOWN"]);
                    context.broadcast('changeDropdownValue', { id: dropDowntoChange.attr('id'), element: dropDowntoChange.find('[data-value=' + params.value + ']') });
                    break;
                case "popup:open":
                    configData.source = data.source;
                    break;
                case "trackSemProjectPhoneClick":
                    if (configData.isPopup) {
                        _parseData();
                        configData.source = data.source;
                        context.broadcast('trackSemProjectPhoneLeadData', { data: configData, id: moduleEl.id });
                    }
                    break;
                case "trackSemProjectLeadFormOpen":
                    if (configData.isPopup) {
                        _parseData();
                        context.broadcast('trackSemProjectLeadFormData', { data: configData, id: moduleEl.id, sourceModule: data.sourceModule });
                    }
                    break;
                case "trackSemLeadFormSubmit":
                    configData.hasSubmitterLead = true;
                    configData.isNewSession = false;
                    break;

            }
        }

        function onclick(event, element, elementType) {
            // bind custom messages/events
            switch (elementType) {
                case "CALL_ME_NOW":
                    let phoneCheck, mobileCheck;
                    phoneCheck = _validate("PHONE", "MAIN");
                    mobileCheck = _validate("EMAIL", "MAIN");
                    phoneCheck && mobileCheck && _submit(); //jshint ignore:line
                    break;
                case "closePopup":
                    _parseData();
                    context.broadcast('trackSemLeadFormClose', { data: configData, id: moduleEl.id });
                    break;
                case "EXPLORE_OTHER":
                    context.broadcast('trackSemLeadFormExplore', { data: configData, id: moduleEl.id });
                    context.broadcast('popup:close');
                    break;
            }
        }

        function onfocusout(event, element) {
            let $el = $(element),
                //value = $el.val(),
                field = $el.parents('[data-type]').data('type');
            switch (field) {
                case "MAIN_PHONE":
                    _validate("PHONE", "MAIN");
                    break;
                case "EMAIL":
                    _validate("EMAIL", "MAIN");
                    break;
            }
        }

        function _submit() {
            let postEnquiry = SharedConfig.apiHandlers.postEnquiry().url; //'/apis/enquiries';
            _parseData();
            return ApiService.postJSON(postEnquiry, parseData).then((response) => {
                $(config.selector["LEADMODULE"]).find('.leadform-wrap').html(thankYouTemplate({
                    sellerUrl: _getSellerUrl()
                }));
                if (window.pixel && typeof(window.pixel.parse) === 'function') {
                    window.pixel.parse("ajax_call");
                }
                context.broadcast('trackSemLeadFormSubmit', { data: configData, id: moduleEl.id });
                _showGallery();
            }, (reject) => {
                configData.errorMessage = reject.statusText;
                context.broadcast('trackSemLeadFormError', { data: configData, id: moduleEl.id });

            });
        }

        function _getSellerUrl() {
            return filterConfigService.getSellerListingUrl({
                id: configData.companyId,
                name: configData.companyName,
                type: 'AGENT'
            }, false, {
                listingType: 'buy'
            });
        }

        function _parseData() {
            parseData.countryId = $(moduleEl).find(config.selector["MAIN"]["COUNTRY_ID"]).attr("value");
            parseData.email = $(moduleEl).find(config.selector["MAIN"]["EMAIL"]).val();
            parseData.phone = $(moduleEl).find(config.selector["MAIN"]["PHONE"]).val();
            parseData.projectId = configData.projectId;
            parseData.domainId = configData.domainId;
            parseData.cityId = configData.cityId;
            parseData.cityName = configData.cityName;
            parseData.companyId = configData.companyId;
            parseData.projectName = configData.projectName;
            parseData.multipleCompanyIds = [configData.companyId];
            parseData.salesType = configData.salesType;
            parseData.pageName = Utils.getPageData('pageType') || '';
            parseData.applicationType = Utils.isMobileRequest() ? 'Mobile Site' : 'Desktop Site';
            parseData.pageUrl = window.location.pathname;
            parseData.jsonDump = window.navigator.userAgent;
            parseData.enquiryType = {
                id: 2 // GET_CALLBACK
            };
            _setCity();
            $.extend(configData, parseData);

        }

        function _showGallery() {
            $(config.HIDE_GALLERY_WRAP).css('display', 'none');
            $(config.HIDE_VIEW_IMAGE_WRAP).remove();
        }

        function _setCity() {
            defaultService.getCities().then(function(response) {
                configData.city = response;
            }, function() {});
        }

        function _setCountry(data) {
            if (!data) {return;}
            if (data.code && data.value) {
                $(moduleEl).find(config.selector["MAIN"]["COUNTRY_CODE"]).text(data.code);
                $(moduleEl).find(config.selector["MAIN"]["COUNTRY_ID"]).val(data.value).data("label", data.label);
            }
        }

        function _validate(type, step) {
            let errorFlag = false,
                el = $(moduleEl).find(config.selector[step][type]),
                value = el.val();

            switch (type) {
                case "PHONE":
                    let country = $(moduleEl).find(config.selector[step]["COUNTRY_ID"]).data("label");
                    if (!value) {
                        _showError('EMPTY', 'PHONE');
                        errorFlag = true;
                    } else if (value && !Utils.validatePhone(value, country)) {
                        _showError('INVALID', 'PHONE');
                        errorFlag = true;
                    } else {
                        _hideError("PHONE");
                    }
                    break;
                case "EMAIL":
                    if (!value) {
                        _showError('EMPTY', 'EMAIL');
                        errorFlag = true;
                    } else if (value && !Utils.isEmail(value, "")) {
                        _showError('INVALID', 'EMAIL');
                        errorFlag = true;
                    } else {
                        _hideError("EMAIL");
                    }
                    break;
            }
            return !errorFlag;
        }

        function _showError(errorType, field) {
            let message = config.messages[field][errorType]["MESSAGE"];
            $(moduleEl).find('[data-message=' + field + ']').text(message);
            configData.errorMessage = message;
            context.broadcast('trackSemLeadFormError', { data: configData, id: moduleEl.id });

            _hideError(field, config.defaultHideTime);
        }

        function _hideError(field, time) {
            setTimeout(() => {
                $(moduleEl).find('[data-message=' + field + ']').html('');
            }, time);
        }

        return {
            init,
            messages,
            behaviors,
            onmessage,
            onclick,
            onfocusout,
            destroy
        };
    });
});
