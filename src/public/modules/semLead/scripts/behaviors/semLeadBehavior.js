define([
    'common/trackingConfigService',
    'services/trackingService'
], function(t, trackingService) {
    'use strict';
    Box.Application.addBehavior('semLeadBehavior', function(context) {

        var messages = ['trackSemLeadFormSubmit', 'trackSemLeadFormError', 'trackSemLeadFormClose', 'trackSemProjectPhoneLeadData', 'trackSemLeadFormExplore', 'trackSemProjectLeadFormData'],
            element,sourceModule;
         const PROPTIGER_COMPANY_ID = "3508859";

       var  onmessage = function(name, data) {
            if (data.id !== element.id) {
                return;
            }
            let event, category, label, properties = {};
            switch (name) {
                case 'trackSemProjectPhoneLeadData':
                case 'trackSemLeadFormSubmit':
                    category = t.LEAD_FORM_CATEGORY;
                    event = t.SUBMIT_EVENT;
                    label = t.SEM_PAGES;
                    break;
                case 'trackSemLeadFormError':
                    category = t.LEAD_FORM_CATEGORY;
                    event = t.ERROR_EVENT;
                    label = data.data && data.data.errorMessage;
                    break;
                case 'trackSemLeadFormClose':
                    category = t.LEAD_FORM_CATEGORY;
                    event = t.NAVIGATION_EVENT;
                    label = t.SEM_PAGES;
                    properties[t.VALUE_KEY] = t.CLOSE_NAME;
                    break;
                case 'trackSemLeadFormExplore':
                    category = t.SEM_PAGES;
                    event = t.CLICK_EVENT;
                    label = t.EXPLORE_MORE_LABEL;
                    data.data = false;
                    break;
                case 'trackSemProjectLeadFormData':
                    category = t.LEAD_FORM_CATEGORY;
                    event = (data.data && data.data.hasSubmitterLead && !data.data.isNewSession) ? t.SUBMIT_EVENT : t.OPEN_EVENT;
                    label = t.SEM_PAGES;
                    data.data = false;
                    sourceModule =  data.sourceModule;
                    break;
            }

            properties[t.CATEGORY_KEY] = category;
            properties[t.LABEL_KEY] = label;
            properties[t.SOURCE_MODULE_KEY] = sourceModule;
            if (data.data) {
                properties[t.NAME_KEY] = data.data.source;
                properties[t.URL_KEY] = data.data.pageUrl;
                properties[t.PROJECT_STATUS_KEY] = t.makeValuesUniform(data.data.projectStatus,t.PROJECT_STATUS_NAME) ;
                properties[t.UNIT_TYPE_KEY] = t.makeValuesUniform(data.data.unitType,t.UNIT_TYPE_MAP) ;
                properties[t.SELLER_ID_KEY] = PROPTIGER_COMPANY_ID;
                properties[t.LISTING_CATEGORY_KEY] = t.LISTING_CATEGORY_MAP["PRIMARY"];
                properties[t.VISITOR_CITY_KEY] = data.data.city;
                properties[t.EMAIL_KEY] = data.data.email;
                properties[t.PAGE_KEY] = t.PAGE_TYPE_MAP.SEM_PROJECT_URLS;
                properties[t.PHONE_USER_KEY] = data.data.phone;
                properties[t.LOCALITY_ID_KEY] = data.data.localityId;
                properties[t.CITY_ID_KEY] = data.data.cityId;
                properties[t.PROJECT_ID_KEY] = data.data.projectId;
                properties[t.BUILDER_ID_KEY] = data.data.builderId;
            }
            if (event) {
                trackingService.trackEvent(event, properties);
            }
        };

        return {
            messages: messages,
            onmessage: onmessage,
            init: function() {
                element = context.getElement();
            },
            destroy: function() {}
        };
    });
});
