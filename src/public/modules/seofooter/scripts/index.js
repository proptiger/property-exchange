"use strict";
define([
    'services/commonService'
], (commonService) => {
    Box.Application.addModule('seofooter', (context) => {
        let moduleEl;

        function init() {
            //initialization of all local variables
            moduleEl = context.getElement();
            commonService.startAllModules(moduleEl);
        }

        function destroy() {
            //clear all the binding and objects
        }

        function onclick(event, element, elementType) {
            // bind custom messages/events
            switch (elementType) {
                case 'seo-footer-btn':
                    _toggleFooter(moduleEl);
                    break;
            }
        }

        return {
            init,
            onclick,
            destroy
        };
    });

    function _toggleFooter(moduleEl) {
        $(moduleEl).toggleClass('collapseit');
    }
});
