/**
 * Name: shortlist/index.js
 * Description: handle the shortlist of listings/projects
 *
 * @author: [Harvinder Singh]
 * Date: Oct 27, 2015
 **/

 define([
    'common/sharedConfig',
    'services/shortlist',
    'services/commonService',
    'services/apiService',
    'services/localStorageService',
    'services/utils',
    'services/sessionService',
    'modules/shortlist/scripts/behaviors/shortlistTracking'
    ],
    function(sharedConfig, shortlistService, commonService, apiService, localStorageService, utils, sessionService) {
        'use strict';
        const MODULE_NAME = "shortlist";
        Box.Application.addModule(MODULE_NAME, function(context) {

            var element, shortlistItem, moduleConfig, moduleId,
                //pageData = utils.getPageData(),
                //LISTING_TYPE = 'listing',
                PROJECT_TYPE = 'project',
                ACTIVE_CLASS = 'active',
                FAVOURITE = 'favourite',
                TEMP_FAVOURITE_SESSION_ID = 'tf',
                FALSE_FLAG = 'false';



            function shortlistClickHandler(trackCheck) { // toggle class
                var isShortlisted;
                if (!element.classList.toggle('active')) {
                    shortlistService.removeShortlist(shortlistItem);
                    isShortlisted = false;
                } else {
                    shortlistService.addShortlist(shortlistItem);
                    context.broadcast('shortlistItemAdded');
                    isShortlisted = true;
                }
                trackCheck && context.broadcast('listingShortlistStatusUpdated', {
                    selector: shortlistItem.selector,
                    isShortlisted: isShortlisted,
                    rank: moduleConfig.rank,
                    source: moduleConfig.source,
                    projectId: moduleConfig.projectId,
                    projectStatus: moduleConfig.projectStatus,
                    builderId: moduleConfig.builderId,
                    price: moduleConfig.price,
                    propertyType: moduleConfig.propertyType,
                    bedrooms: moduleConfig.bedrooms,
                    sellerId: moduleConfig.sellerId,
                    sellerScore: moduleConfig.sellerScore,
                    localityId: moduleConfig.localityId,
                    suburbId: moduleConfig.suburbId,
                    cityId: moduleConfig.cityId,
                    listingId: moduleConfig.listingId,
                    listingScore: moduleConfig.listingScore,
                    listingCategory: moduleConfig.listingCategory,
                    isSelectListing:moduleConfig.isSelectListing,
                    moduleId: moduleId
                });   //jshint ignore:line
            }

            function _getQueryObject() {
                let tempQuery = {},
                    //budget,
                    //minBudget,
                    //maxBudget,
                    tempVariable,
                    tempModuleConfig;

                    tempQuery['projectId'] = moduleConfig.projectId;

                    tempModuleConfig = moduleConfig.data || moduleConfig;

                    tempVariable = _getParsedBudget(tempModuleConfig);
                    tempQuery['budget'] = [tempVariable.minBudget,tempVariable.maxBudget].join();

                    if(tempModuleConfig.unitTypeId) {
                        tempQuery['unitType'] = tempModuleConfig.unitTypeId;
                    }

                    if(tempModuleConfig.listingCategory) {
                        tempQuery['listingCategory'] = tempModuleConfig.listingCategory;
                    }

                    _getParsedBhk(tempModuleConfig,tempQuery);

                    return tempQuery;
            }

            function shortListLeadHandler() {
                let sessionId = sessionService.getSessionId() && sessionService.getSessionId().c;
                if(localStorageService.getItem(FAVOURITE) == FALSE_FLAG || localStorageService.getItem(TEMP_FAVOURITE_SESSION_ID) === sessionId || !$(element).hasClass(ACTIVE_CLASS)) {
                    return;
                }

                let query,
                    url,
                    rawData,
                    cityId;

                cityId =  moduleConfig.cityId || moduleConfig.data.cityId;
                query = _getQueryObject();
                url = sharedConfig.apiHandlers.getCityTopSellers({cityId,query}).url;
                commonService.bindOnLoadPromise().then(() => {
                      return apiService.get(url).then(function(response) {
                              if (response && response.length) {
                                rawData = _prepareShortlistLeadData(response,moduleConfig);
                                rawData.id = 'lead-popup';
                                context.broadcast('Lead_Open_Step',rawData);
                                _openPopup('leadOverviewPopup',true);
                              }
                      });
                });
            }

            function _openPopup(id, closeOnEsc) {
                context.broadcast('popup:open', {
                    id: id,
                    options: {
                        config: {
                            modal: closeOnEsc || false
                        }
                    }
                });
            }

            function _prepareShortlistLeadData(sellerResponse) {
                let companyIds = [],
                    rawData = {},
                    //propertyType = [],
                    //bhk = [],
                    //localityId = [],
                    tempVariable,
                    tempText,
                    tempModuleConfig;

                tempModuleConfig = moduleConfig.data || moduleConfig;

                if(moduleConfig.type === PROJECT_TYPE) {
                    tempVariable = tempModuleConfig.builderName;
                    tempVariable = tempVariable+' '+tempModuleConfig.label;
                } else {
                    tempVariable = tempModuleConfig.propertyType;
                    if(tempModuleConfig.isStudio){
                        tempVariable = 'studio apartment';
                    } else if(tempModuleConfig.isPentHouse){
                        tempVariable = 'pentHouse';
                    }
                    if(parseInt(tempModuleConfig.bedrooms)) {
                        if(tempModuleConfig.isRk) {
                            tempVariable = 'RK '+tempVariable;
                        } else {
                            tempVariable = 'BHK '+tempVariable;
                        }
                        tempVariable = tempModuleConfig.bedrooms+' '+tempVariable;
                    }
                    tempVariable = tempVariable+' - '+tempModuleConfig.size;
                }
                    tempText = tempVariable+' added to your shortlist';

                for (let key in sellerResponse) {
                    companyIds.push({ companyId: sellerResponse[key].id,companyUserId:sellerResponse[key].userId});
                }
                companyIds.shift();
                if(companyIds.length) {
                    companyIds = companyIds.splice(0,4);
                }

                rawData.targetId = moduleId;
                rawData.companyId = sellerResponse[0].id;
                rawData.companyName = sellerResponse[0].name;
                rawData.companyPhone = sellerResponse[0].contact||'';
                rawData.companyUserId = sellerResponse[0].userId;
                rawData.configurationText =  {
                    type: FAVOURITE,
                    text: tempText,
                    heading: "Update your requirements , and connect with top sellers",
                    footer: "save"
                };
                rawData.source = "shortlist";
                rawData.otherCompany = companyIds;
                rawData.step = "MAIN_TOP_SELLER";
                rawData.isCommercial = utils.getPageData('isCommercial');

                rawData.salesType = 'buy';
                tempVariable = tempModuleConfig.listingCategory;
                if(tempVariable && tempVariable.toLowerCase().trim() === 'rental') {
                    rawData.salesType = 'rent';
                }

                if(tempModuleConfig.unitTypeId) {
                   rawData.propertyType = tempModuleConfig.unitTypeId.toString().split(',');
                }

                tempVariable = _getParsedBudget(tempModuleConfig);
                rawData.minBudget = tempVariable.minBudget;
                rawData.maxBudget = tempVariable.maxBudget;
                rawData.budget = [rawData.minBudget,rawData.maxBudget].join();
                _getParsedBhk(tempModuleConfig,rawData);

                if(tempModuleConfig.localityId) {
                    rawData.localityId = tempModuleConfig.localityId.toString().split();
                }

                rawData.projectStatus = tempModuleConfig.projectStatus;
                return rawData;
            }

            function _getParsedBudget(tempModuleConfig) {
               let tempVariable,
                   tempBudget = {};
               tempVariable = parseInt(tempModuleConfig.minPrice || tempModuleConfig.price || 0);
               tempBudget.minBudget  = (tempVariable - Math.ceil(tempVariable*0.15)).toString();
               tempVariable = parseInt(tempModuleConfig.maxPrice || tempModuleConfig.price || 50000000);
               tempBudget.maxBudget  = (tempVariable + Math.ceil(tempVariable*0.15)).toString();
               return tempBudget;
            }

            function _getParsedBhk(tempModuleConfig,reqObject) {
                let tempVariable;
                if(tempModuleConfig.bedrooms) {
                    reqObject.bhk = tempModuleConfig.bedrooms.toString().split(',');
                    tempVariable = parseInt(reqObject.bhk[0]);
                    (tempVariable - 1) > 0 && reqObject.bhk.unshift((tempVariable-1).toString()); //jshint ignore:line
                    tempVariable = parseInt(reqObject.bhk[reqObject.bhk.length-1]);
                    reqObject.bhk.push((tempVariable+1).toString());
                }
            }

            var init = function() { //based on data attribute check whether the given listing is shortlisted by user
                element = context.getElement();
                moduleConfig = context.getConfig();
                moduleId = element.id;
                _checkShortlist(moduleConfig);
                context.broadcast("moduleLoaded", {name: MODULE_NAME, id: element.id});
            };

            function _checkShortlist(moduleConfig){
                if(moduleConfig.dataAwaited){
                    return true;
                }

                shortlistItem = {
                    type: moduleConfig.type,
                    id: moduleConfig.listingId,
                    selector: moduleConfig.selector,
                    data: moduleConfig.data,
                    projectId: moduleConfig.projectId,
                    rank: moduleConfig.rank
                };
                if(shortlistService.checkShortlist(shortlistItem)) {
                    element.classList.add('active');
                }
            }

            function _setModuleConfig(allData){
                return {
                    "type":"listing",
                    "source": "serp",
                    "selector": allData.selector,
                    "listingId":allData.id,
                    "projectId":allData.projectId,
                    "rank": allData.rank,
                    "builderId": allData.builderId,
                    "price": allData.price,
                    "unitTypeId": allData.unitTypeId,
                    "propertyType": allData.propertyType,
                    "bedrooms": allData.bedrooms,
                    "sellerId": allData.companyId,
                    "sellerScore": allData.companyRating,
                    "localityId": allData.localityId,
                    "suburbId": allData.suburbId,
                    "cityId": allData.cityId,
                    "listingScore": allData.listingScore,
                    "listingCategory": allData.listingCategory,
                    "size":allData.size,
                    "isRk":allData.isRk,
                    "isPentHouse":allData.isPentHouse,
                    "isStudio":allData.isStudio,
                    "projectStatus": allData.projectStatus,
                    "isSelectListing":allData.isMakaanSelectSeller && allData.listingCategory && allData.listingCategory.toLowerCase()=="rental"
                };
            }

            return {
                messages: ['shortlistIconClicked', 'favourites-synced', 'enquiry_submit', 'popup:close', 'listing-configData'],
                behaviors: ['shortlistTracking'],

                init: init,

                destroy: function() {},

                onmessage: function(name, data){
                    if(name === 'shortlistIconClicked'){
                        if(data.moduleId == element.id) {
                            return;
                        }
                        if(data.moduleId != element.id && data.shortlistItem && data.shortlistItem.type == shortlistItem.type) {
                            if((data.shortlistItem.id && data.shortlistItem.id == shortlistItem.id) || (data.shortlistItem.type === 'project' && data.shortlistItem.projectId == shortlistItem.projectId)){
                                shortlistClickHandler();
                                return;
                            }
                        }
                    } else if(name === 'favourites-synced'){
                        $(element).removeClass('active');
                        init();
                    } else if(name === 'enquiry_submit'){
                        if(data && data.displayData && data.displayData.configurationText && data.displayData.configurationText.type === FAVOURITE  &&  data.moduleParameters && data.moduleParameters.targetId === moduleId) {
                            localStorageService.setItem(FAVOURITE,FALSE_FLAG);
                        }
                    } else if(name === 'popup:close') {
                        if(data && data.displayData && data.displayData.configurationText && data.displayData.configurationText.type === FAVOURITE  &&  data.moduleParameters && data.moduleParameters.targetId === moduleId) {
                            let sessionId = sessionService.getSessionId() && sessionService.getSessionId().c;
                                localStorageService.setItem(TEMP_FAVOURITE_SESSION_ID,sessionId);
                        }
                    } else if(name == "listing-configData" && data.id == moduleConfig.listingId){
                        moduleConfig = _setModuleConfig(data);
                        _checkShortlist(moduleConfig);
                    }
                },

                onclick: function(event, element, elementType) {
                    switch (elementType) {
                        case 'fav-btn': //toggle between shortlisting and removing the shortlist
                            context.broadcast('shortlistIconClicked', {
                                shortlistItem: shortlistItem,
                                moduleId: element.id
                            });
                            shortlistClickHandler(true);
                            event.stopPropagation();
                            event.stopImmediatePropagation();
                            shortListLeadHandler();
                            break;
                    }
                }
            };
        });
    }
);
