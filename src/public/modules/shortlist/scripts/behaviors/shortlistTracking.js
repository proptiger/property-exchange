define([
    'common/trackingConfigService',
    'services/trackingService',
    'services/urlService',
    'services/utils'
], function(t, trackingService, urlService, utils) {
    'use strict';
    Box.Application.addBehavior('shortlistTracking', function(context) {

        var messages = [
            'listingShortlistStatusUpdated'
        ];

        var value, moduleId, config;


        var onmessage = function(msgName, data={}) {
            if (data.moduleId && data.moduleId != moduleId) {
                return;
            }

            let event = t.SHORTLIST_EVENT,
                label = t.UNSHORTLIST_LABEL,
                category = t.SERP_CARD_CATEGORY;
            switch (msgName) {
                case 'listingShortlistStatusUpdated':
                    if (data.isShortlisted) {
                        label = t.SHORTLIST_LABEL;
                    }
                    if (data.source == 'serpProject') {
                        category = t.SERP_CARD_CATEGORY; 
                    } else if (data.source == 'headline') {
                        category = t.HEADLINE_CATEGORY;
                    }

                    break;
            }

            let properties = {};
            properties[t.CATEGORY_KEY] = category;
            properties[t.LABEL_KEY] = label;
            properties[t.VALUE_KEY] = parseInt(urlService.getUrlParam('page')) || 1;
            properties[t.RANK_KEY] = data.rank;
            properties[t.PROJECT_ID_KEY] = data.projectId ? parseInt(data.projectId) : undefined;
            if(data.projectStatus && config.inactiveProjectStatuses.indexOf(data.projectStatus.toLowerCase()) === -1) {
                properties[t.PROJECT_STATUS_KEY] = t.makeValuesUniform(data.projectStatus,t.PROJECT_STATUS_NAME) ;
            }
            properties[t.BUILDER_ID_KEY] = data.builderId ? parseInt(data.builderId) : undefined;
            properties[t.BUDGET_KEY] = data.price;
            properties[t.UNIT_TYPE_KEY] = t.makeValuesUniform(data.propertyType,t.UNIT_TYPE_MAP) ;
            properties[t.BHK_KEY] = data.bedrooms ? parseInt(data.bedrooms) : undefined;
            properties[t.SELLER_ID_KEY] = data.sellerId;
            properties[t.SELLER_SCORE_KEY] = parseFloat(data.sellerScore) ? parseFloat(data.sellerScore) : undefined;
            properties[t.LOCALITY_ID_KEY] = data.localityId;
            properties[t.SUBURB_ID_KEY] = data.suburbId;
            properties[t.CITY_ID_KEY] = data.cityId;
            properties[t.LISTING_ID_KEY] = data.listingId;
            properties[t.LISTING_SCORE_KEY] = data.listingScore;
            value = utils.getTrackingListingCategory(data.listingCategory);
            properties[t.LISTING_CATEGORY_KEY] = t.makeValuesUniform(value,t.LISTING_CATEGORY_MAP) ;
            properties[t.SELECT_PROPERTY_KEY] = data.isSelectListing ? 'Select Listing':undefined;
            if(!data.isOriginalSearchResult){
                properties[t.SOURCE_MODULE_KEY] = 'Targetted';
            }
            trackingService.trackEvent(event, properties);
        };

        return {
            messages: messages,
            onmessage: onmessage,
            init: function() {
                moduleId = context.getElement().id;
                config = context.getGlobal("config") || {};
            },
            destroy: function() {}
        };
    });
});
