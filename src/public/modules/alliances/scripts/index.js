"use strict";
define([
        "doT!modules/alliances/views/errorPopup",
        "doT!modules/alliances/views/couponScreen",
        "doT!modules/alliances/views/500",
        "services/urlService",
        "services/defaultService",
        "services/loggerService",
        "services/localStorageService",
        "services/apiService",
        "common/sharedConfig",
        "services/utils",
        "services/commonService",
        'modules/alliances/scripts/behaviors/allianceTracking'
    ],
    function(errorPopup, couponScreen, errorScreen, urlService, defaultService, logger, localStorageService, apiService, sharedConfig, utils, commonService) {
        Box.Application.addModule("alliances", function(context) {

            var element,$moduleEl,
                scope = {};

            //const CITY_ID = "cityId",
            const CATEGORY_ID = "categoryId",
                SERVICE_ID = "serviceId",
                //HIDE_CLASS = "hidden",
                CITY_DROPDOWN = 'alliance-cities',
                CITY_COOKIE = 'alliancesCity',
                URL_CITY_PARAM = 'allianceCities',
                LEADFORM_FLOW = 'form',
                //LEAD_SUMMARY = 'summary',
                //LEAD_OTP = 'otp',
                //DEFAULT_COUNTRY = 'INDIA',
                //DISABLE_CLASS = 'js-disable',
                //ACTIVE_CLASS = 'active',
                MOBILE_SCROLL_OFFSET = 70,
                DESKTOP_SCROLL_OFFSET = 100,
                SCROLL_ANIMATION_DURATION = 1000,
                SCROLL_QUERY = 'category';

            const errorMap = {
                name: {
                    empty: {
                        message: 'name is required'
                    },
                    invalid: {
                        message: 'name is invalid'
                    }
                },
                phone: {
                    empty: {
                        message: 'phone number is required'
                    },
                    invalid: {
                        message: 'phone number is invalid'
                    }
                },
                email: {
                    empty: {
                        message: 'email is required'
                    },
                    invalid: {
                        message: 'email is invalid'
                    }
                },
                otp: {
                    invalid: {
                        message: 'otp is invalid'
                    },
                    fail: {
                        message: 'service is temporarily not available'
                    }
                },
                city: {
                    empty: {
                        message: 'please select the city'
                    }
                }
            };

            const LOCAL_STORAGE_KEY = "MAKAAN_ALLIANCES";
            const SERVICE_PAGE = 'Providers',
                CATEGORY_PAGE = 'Services',
                COUPON_PAGE = 'Offer',
                ALLIANCE_HOME = 'Alliance Home';

            const messages = ['singleSelectDropdownChanged'];

            function clearMessage(tmpElement) {
                setTimeout(() => {
                    tmpElement.text('');
                }, 2000);
            }

            function setError(selector, message) {
                var tmpElement = $moduleEl.find(selector);
                tmpElement.text(message);
                clearMessage(tmpElement);
            }

            var scrollToId = function(id) {
                    let scrollOffset = scope.mobile ? MOBILE_SCROLL_OFFSET : DESKTOP_SCROLL_OFFSET;
                    commonService.bindOnLoad(() => {
                        if ($('#' + id).length) {
                            $('html, body').animate({ scrollTop: $("#" + id).offset().top - scrollOffset }, SCROLL_ANIMATION_DURATION);
                        }
                    });
                },
                getHashQuery = function(query) {
                    if (!query) {
                        return;
                    }
                    let hashValue = window.location && window.location.hash;
                    let regex = new RegExp("[\\#&]" + query + "=([^&]*)"),
                        results = regex.exec(hashValue);
                    return $.isArray(results) && results.length >= 1 ? results[1] : '';
                },

                getLocalUserInfo = function() {
                    let userInfo = localStorageService.getItem(LOCAL_STORAGE_KEY);
                    if (!userInfo) {
                        userInfo = {
                            alliances: []
                        };
                    }
                    return userInfo;
                },

                renderPage = function() {
                    //always check scope.render
                    //let query = urlService.getAllUrlParam(),
                    let cityId = scope.cityId;

                    if (!cityId) {
                        defaultService.getAllianceCities().then(response => {
                            $(element).append(errorPopup({
                                city: true,
                                cities: response || []
                            }));
                            $('body').css('overflow', 'hidden');
                        }, () => {
                            $(element).html(errorScreen());
                        });
                    }
                },

                init = function() {
                    element = context.getElement();
                    $moduleEl = $(element);
                    commonService.stopAllModules(element);
                    commonService.startAllModules(element);

                    $('body').css('overflow', '');

                    let dataset = $(element).data();
                    let urlParams = urlService.getAllUrlParam();
                    scope.userId = dataset.userId || urlParams.userId;
                    scope.firstName = dataset.userFirstName || urlParams.firstName;
                    scope.emailId = dataset.userEmailId || urlParams.emailId;
                    scope.leadId = urlParams.leadId;
                    scope.listingCategory = urlParams.listingCategory;
                    scope.serviceId = urlParams.serviceId;
                    scope.page = scope.serviceId ? SERVICE_PAGE : CATEGORY_PAGE;
                    scope.render = dataset.render;
                    scope.cityId = parseInt(urlParams[URL_CITY_PARAM]) || parseInt(dataset.cityId);
                    scope.userInfo = getLocalUserInfo();

                    if (scope.cityId) {
                        utils.setcookie(CITY_COOKIE, scope.cityId);
                    }
                    scope.mobile = utils.isMobileRequest();

                    let afterHashUrl = getHashQuery(SCROLL_QUERY);
                    if (afterHashUrl) {
                        scrollToId(afterHashUrl);
                    }

                    renderPage();
                },



                ajaxifyUrlQuery = function(key, value, ajaxify = true) {
                    let newHref = urlService.changeUrlParam(key, value, window.location.pathname + window.location.search, true);
                    newHref += window.location.hash;
                    if (ajaxify) {
                        commonService.ajaxify(newHref);
                    } else {
                        window.location = newHref;
                    }
                },

                getTrackData = function() {
                    let trackData = {};
                    trackData.cityId = scope.cityId;
                    trackData.emailId = scope.emailId;
                    trackData.page = scope.page;
                    trackData.allianceId = scope.serviceId;
                    return trackData;
                },

                onmessage = function(name, data) {
                    switch (name) {
                        case 'singleSelectDropdownChanged':
                            if (data.id === CITY_DROPDOWN) {
                                // commonService.ajaxify(window.location.pathname + '?' + URL_CITY_PARAM + '=' + data.value);
                                let trackData = getTrackData();
                                trackData.label = data.value;
                                context.broadcast('trackCityDropdownChanged', trackData);
                                ajaxifyUrlQuery(URL_CITY_PARAM, data.value);
                            }
                            break;
                    }
                },

                bindFormEvents = function() {
                    if (element) {
                        $(element).find('input').on('focus', function() {
                            $(this).closest('.js-input-box').addClass('active');
                        });
                        $(element).find('input').on('blur', function() {
                            if (!$(this).val()) {
                                $(this).closest('.js-input-box').removeClass('active');
                            }
                        });
                    }
                },

                unbindFormEvents = function() {
                    if (element) {
                        $(element).find('input').off('focus');
                        $(element).find('input').off('blur');
                    }
                    $('body').css('overflow', '');
                },

                showError = function(data, timeout) {
                    let parent = $(data.element).closest('.input-box');
                    parent.find('.error-msg').remove();
                    let error = $('<div class="error-msg"></div>');
                    error.text(data.errorMessage);
                    error.css({
                        'font-size': '12px'
                    });
                    parent.append(error);
                    error.fadeOut(timeout || 7000);
                },

                selectCityCheck = function() {
                    if (!scope.cityId) {
                        let errorData = {
                            element: $('.js-input-placeholder')[0],
                            errorMessage: errorMap.city.empty.message
                        };
                        showError(errorData, 2000);
                        return false;
                    }
                    return true;
                },

                onclick = function(event, clickElement, elementType) {
                    let dataset = $(clickElement).data(),
                        postObject = {},
                        query = {},
                        trackData = getTrackData();
                    switch (elementType) {
                        case 'anchor':
                            query = dataset.query && dataset.query.split("=");
                            if (!selectCityCheck()) {
                                return;
                            }
                            if (query.length === 2) {
                                let queryName = query[0],
                                    queryValue = query[1];
                                if (queryName === SERVICE_ID) {
                                    trackData.allianceId = dataset.categoryId;
                                    trackData.name = dataset.name;
                                    context.broadcast('trackViewOffer', trackData);
                                }
                                ajaxifyUrlQuery(queryName, queryValue);
                            }
                            break;

                        case 'select-city':
                            let selectedCityId = $('.js-city-selected:checked').data('cityId');
                            if (selectedCityId) {
                                trackData.cityId = selectedCityId;
                                context.broadcast('trackCitySelectPopupAlliances', trackData);
                                ajaxifyUrlQuery(URL_CITY_PARAM, selectedCityId, false);
                            } else {  
                                setError('.js-select-city-error','please select a city first')
                            }
                            break;

                        case 'back-anchor':
                            let urlParam = dataset.urlParam;
                            if (urlParam) {
                                let newHref = urlService.removeUrlParam(urlParam, window.location.pathname + window.location.search, true);
                                trackData.allianceId = dataset.categoryId;
                                trackData.name = dataset.serviceName;
                                context.broadcast('trackBackButton', trackData);
                                if ([CATEGORY_ID, SERVICE_ID, LEADFORM_FLOW].indexOf(urlParam) > -1) {
                                    commonService.ajaxify(newHref);
                                }
                            }
                            break;

                        case 'show-coupon':
                            if (!selectCityCheck()) {
                                return;
                            }
                            scope.dealId = dataset.couponId;
                            postObject = {};
                            postObject.userId = scope.userId;
                            postObject.userOfferServiceRequests = [];
                            postObject.userOfferServiceRequests[0] = {
                                coupon: {
                                    id: dataset.couponId
                                },
                                cityId: scope.cityId
                            };

                            trackData.name = dataset.serviceName + ',' + dataset.providerName;
                            trackData.label = dataset.trackLabel;
                            scope.categoryId = trackData.allianceId = dataset.categoryId;
                            context.broadcast("trackShowCouponClickAlliances", trackData);


                            apiService.postJSON(sharedConfig.apiHandlers.alliances({
                                query: {
                                    type: 'userOffer'
                                }
                            }).url, postObject).then(response => {
                                let finalResponse = scope.showCouponResponse = response;
                                scope.page = COUPON_PAGE;
                                finalResponse.emailId = scope.emailId = response.emailId;
                                finalResponse.firstName = scope.firstName;
                                finalResponse.serviceId = scope.serviceId;
                                finalResponse.categoryId = scope.categoryId;
                                finalResponse.serviceName = dataset.serviceName;
                                scope.currentCoupon = response.couponCode;
                                urlService.changeUrlParam(LEADFORM_FLOW, true);
                                $(element).html(couponScreen(finalResponse));
                                unbindFormEvents();
                                bindFormEvents();
                            }, error => {
                                $(element).html(errorScreen());
                                logger.error('error in post call for alliances coupons', error);
                            });
                            break;

                        case 'remove-more-count':
                            trackData.allianceId = dataset.categoryId;
                            trackData.name = dataset.name;
                            context.broadcast('trackRemoveMoreCount', trackData);
                            $(clickElement).addClass('hidden');
                            $(clickElement).closest('.js-expand-service').css('max-height', Math.ceil(dataset.count / 3) * 334 + 'px');
                            $(clickElement).siblings('.js-remove-blur').removeClass('blurit');
                            break;

                        case 'copy-coupon':
                            let copyTargetArea = $('.js-copy-code')[0];
                            if (utils.copyToClipboard(copyTargetArea)) {
                                $(clickElement).text("copied");
                            } else {
                                $(clickElement).text("copy");
                            }
                            trackData.allianceId = dataset.categoryId;
                            trackData.label = scope.currentCoupon;
                            trackData.name = dataset.serviceName + ',' + dataset.providerName;
                            context.broadcast('trackCopyCoupon', trackData);
                            break;

                        case 'show-terms-conditions':
                            $('.js-terms-conditions-popup').text($(clickElement).find('.js-terms-conditions-text').text());
                            context.broadcast('popup:open', {
                                id: 'termsPopup'
                            });
                            break;

                        case 'subscribe':
                            let email = $('.js-email').val();
                            trackData.allianceId = dataset.categoryId;
                            trackData.name = dataset.serviceName + ',' + dataset.providerName;
                            if (!email) {
                                let errorData = {
                                    element: element.querySelector('[name="client-email"]'),
                                    errorMessage: errorMap.email.empty.message
                                };
                                showError(errorData);
                                return;
                            } else if (!utils.isEmail(email)) {
                                let errorData = {
                                    element: element.querySelector('[name="client-email"]'),
                                    errorMessage: errorMap.email.invalid.message
                                };
                                showError(errorData);
                                context.broadcast('trackIncorrectEmailAlliances', trackData);
                                return;
                            }
                            query.userEmail = scope.emailId = email;
                            query.userOfferLeadId = scope.showCouponResponse && scope.showCouponResponse.leadId;
                            query.type = 'subscribe';

                            // trackData.allianceId = scope.dealId;
                            context.broadcast('trackSubscribeSubmit', trackData);

                            apiService.patch(sharedConfig.apiHandlers.alliances({
                                query
                            }).url).then(() => {
                                $(element).find('.js-submitted').addClass('hidden');
                                $('.js-user-email').text(scope.emailId);
                                $('.js-patch-success').removeClass('hidden');
                            }, error => {
                                $(element).html(errorScreen());
                                logger.error('error in post call for alliances coupons', error);
                            });
                            break;

                        case 'bread-services':
                            trackData.name = ALLIANCE_HOME;
                            trackData.allianceId = dataset.categoryId;
                            context.broadcast('trackBreadcrumServices', trackData);
                            break;

                        case 'bread-providers':
                            trackData.name = dataset.serviceName;
                            trackData.allianceId = dataset.categoryId;
                            context.broadcast('trackBreadcrumProviders', trackData);
                            break;

                        case 'bread-service-provider':
                            trackData.name = dataset.serviceName;
                            trackData.allianceId = dataset.categoryId;
                            context.broadcast('trackBreadcrumServiceProviders', trackData);
                            break;

                        case 'explore-more':
                            trackData.allianceId = dataset.categoryId;
                            trackData.name = dataset.serviceName;
                            context.broadcast('trackExploreMore', trackData);
                            break;

                        case 'provider-site':
                            trackData.allianceId = dataset.categoryId;
                            trackData.name = dataset.serviceName;
                            context.broadcast('trackProviderSiteLinkClick', trackData);
                            break;
                    }
                },
                destroy = function() {
                    unbindFormEvents();
                    element = null;
                    scope = null;
                };

            return {
                init,
                onclick,
                messages,
                behaviors: ['allianceTracking'],
                onmessage,
                destroy
            };
        });
    }
);
