define([
    'common/trackingConfigService',
    'services/urlService',
    'services/trackingService'
    ], function(t, urlService, trackingService) {
    'use strict';
    Box.Application.addBehavior('allianceTracking', function(context) {

        var element;
        const messages = [
            'trackCityDropdownChanged',
            'trackRemoveMoreCount',
            'trackViewOffer',
            'trackCopyCoupon',
            'trackSubscribeSubmit',
            'trackBreadcrumServices',
            'trackBreadcrumProviders',
            'trackBreadcrumServiceProviders',
            'trackBackButton',
            'trackExploreMore',
            'trackCitySelectPopupAlliances',
            'trackShowCouponClickAlliances',
            'trackIncorrectEmailAlliances',
            'trackProviderSiteLinkClick'
        ];

        var init = function() {
            element = context.getElement();
        };

        var onmessage = function(name, data) {
            let properties = {};
            let category = t.ALLIANCES_CATEGORY,
                event,
                label;

            switch (name) {
                case 'trackCityDropdownChanged':
                    event = t.SELECT_CITY;
                    label = data.label;
                    break;

                case 'trackRemoveMoreCount':
                    event = t.VIEW_MORE;
                    break;

                case 'trackViewOffer':
                    event = t.VIEW_OFFER;
                    break;

                case 'trackCopyCoupon':
                    event = t.COPY_COUPON;
                    label = data.label;
                    break;

                case 'trackSubscribeSubmit':
                    event = t.SUBMIT_EVENT;
                    break;

                case 'trackBreadcrumServices':
                    event = t.NAVIGATION_EVENT;
                    label = t.SERVICES_LABEL;
                    break;

                case 'trackBreadcrumProviders':
                    event = t.NAVIGATION_EVENT;
                    label = t.PROVIDERS_LABEL;
                    break;

                case 'trackBreadcrumServiceProviders':
                    event = t.NAVIGATION_EVENT;
                    label = t.SERVICE_PROVIDER_LABEL;
                    break;

                case 'trackBackButton':
                    event = t.NAVIGATION_EVENT;
                    label = t.BACK_LABEL;
                    break;

                case 'trackExploreMore':
                    event = t.NAVIGATION_EVENT;
                    label = t.EXPLORE_MORE_LABEL;
                    break;

                case 'trackCitySelectPopupAlliances':
                    event = t.SELECT_CITY;
                    label = data.label;
                    break;

                case 'trackShowCouponClickAlliances':
                    event = t.SHOW_COUPON_EVENT;
                    label = data.label;
                    break;

                case 'trackIncorrectEmailAlliances':
                    event = t.ERROR_EVENT;
                    label = t.INCORRECT_EMAIL_LABEL;
                    break;

                case 'trackProviderSiteLinkClick':
                    event = t.NAVIGATION_EVENT;
                    label = t.VISIT_PROVIDER_SITE_LABEL;
                    break;
            }

            properties[t.CATEGORY_KEY] = category;
            if (label) {
                properties[t.LABEL_KEY] = label;
            }
            properties[t.CITY_ID_KEY] = data.cityId;
            properties[t.EMAIL_KEY] = data.emailId;
            properties[t.PAGE_KEY] = data.page;
            properties[t.ALLIANCE_ID_KEY] = data.allianceId;
            properties[t.NAME_KEY] = data.name;

            trackingService.trackEvent(event, properties);
        };

        return {
            messages,
            onmessage,
            init,
            destroy: function() {}
        };
    });
});
