'use strict';

define([
	'services/commonService',
	'services/utils',
	'modules/newLeadForm/scripts/services/newLeadService',
	"common/sharedConfig",
	'services/leadPyrService',
	'doT!modules/newLeadForm/views/index',
	'modules/newLeadForm/scripts/services/newLeadTrackingService',
	'services/feedbackService',
	'modules/lead/scripts/behaviors/newLeadFormTracking',
	"css!styles/css/newLeadForm"
], (commonService, utilService, newLeadService, sharedConfig, leadPyrService, baseTemplate, newLeadTrackingService, feedbackService) => {

	let MODULE_NAME = "newLeadForm";
	Box.Application.addModule(MODULE_NAME, (context) => {

		let moduleEl, moduleConfig, currentStep, rawData, details = {}, global = {};
		
		const config = {
			SELECTORS: {
				header: "[data-header-box]",
				detail: "[data-detail-box]",
				content: "[data-content-box]",
				footer: "[data-footer-box]",
				info: "[data-info-line-box]",
				full: "[data-full-box]",
				autoFocusSelector: "[data-auto-focus] input",
				callFeedback: "[data-call-feedback]",
				formElementModule: "[data-module=form-elements]",
				feedbackReason: "[data-feedback-reasons]",
				feedbackRating: "[data-feedback] li",
				back: "[data-back-button]",
				error: "[data-error-box]",
				"CALL_STATUS_SELECTOR": {
					"CALL_CONNECTED": "[data-call-connected]",
					"CALL_DISCONNECTED": "[data-call-disconnected]"
				},
				"CONNECT_NOW": {
					"NAME": '[data-name] [data-value-selector]',
					"EMAIL": '[data-email] [data-value-selector]',
					"PHONE": '[data-phone] [data-value-selector]'
				},
				"VIEW_PHONE_MORPHED": {
					"NAME": '[data-name] [data-value-selector]',
					"EMAIL": '[data-email] [data-value-selector]',
					"PHONE": '[data-phone] [data-value-selector]'
				},
				"CALL_NOW": {
					"NAME": '[data-name] [data-value-selector]',
					"EMAIL": '[data-email] [data-value-selector]',
					"PHONE": '[data-phone] [data-value-selector]'
				},
				"VERIFY_OTP": {
					"OTP": '[data-otp] [data-value-selector] input'
				},
				"SIMILAR_PROPERTIES": {
					custom: true,
					"CHECKED_PROPERTIES": "[data-similar-container] input:checked"
				},
				"SIMILAR_SELLERS": {
					custom: true,
					"CHECKED_PROPERTIES": "[data-similar-container] input:checked"
				},
				"FEEDBACK": {
					custom: true,
					"RATING": "[data-feedback] li:not(.disable)",
					"REASON": "[data-feedback-reasons] input:checked"
				}
			},
			VALIDATION: {
				"CONNECT_NOW": {
					"NAME": utilService.isName,
					"EMAIL": utilService.isEmail,
					"PHONE": function(phone){
						return utilService.validatePhone(phone, details.countryLabel);
					}
				},
				"VIEW_PHONE_MORPHED": {
					"NAME": utilService.isName,
					"EMAIL": utilService.isEmail,
					"PHONE": function(phone){
						return utilService.validatePhone(phone, details.countryLabel);
					}
				},
				"CALL_NOW": {
					"NAME": utilService.isName,
					"EMAIL": utilService.isEmail,
					"PHONE": function(phone){
						return utilService.validatePhone(phone, details.countryLabel);
					}
				},
				"VERIFY_OTP": {
					"OTP": utilService.validateOtp
				},
				"FEEDBACK": {
					custom: true,
					fn: function(data){
						if(data.callRating){
							if(data.callRating > 3  || (data.callRating <= 3 && data.ratingReason)){
								return false;
							}
							return "Please provide rating and/or reason";
						}
						return "Please provide rating and/or reason";
					}
				},
				ERROR_MESSAGES: {
					GENERIC: "Some error occurred"
				}
			},
			CLASS: {
				hide: "hide",
				DISABLE: "disable"
			},
			ERROR_TIMEOUT: 5000
		};

		const behaviors = ["newLeadFormTracking"];

		function _getDataFromUI(stepName){
			let obj = {};
			let stepConfig = config["SELECTORS"][stepName] || {};
			if(stepConfig.custom){
				switch(stepName){
					case "SIMILAR_PROPERTIES":
					case "SIMILAR_SELLERS":
						obj.similarProperties = $(moduleEl).find(stepConfig.CHECKED_PROPERTIES).map((id, elem) => {return decodeURI($(elem).data('collection'));}).get();
						break;
					case "FEEDBACK":
						obj.callRating = $(moduleEl).find(stepConfig.RATING).data("rating");
						obj.ratingReason = $(moduleEl).find(stepConfig.REASON).val();
						break;
				}
			} else {
				for(let key in config["SELECTORS"][stepName]){
					let selector = config["SELECTORS"][stepName][key];
					obj[key.toLowerCase()] = $(moduleEl).find(selector).map((id, elem) => {
						return $(elem).val();
					}).get().join("");
				}
			}
			return obj;
		}

		function _validate(stepName, data){
			let validationStrategies = config["VALIDATION"][stepName];
			let obj = {
				passed: true,
				validations: {}
			};
			if(validationStrategies && validationStrategies.custom && typeof validationStrategies.fn == "function"){
				obj.message = validationStrategies.fn(data);
				if(obj.message){
					obj.passed = false;
					obj.global = true;
				}
			} else {
				for(let key in validationStrategies){
					let lcKey = key.toLowerCase();
					if(["name", "email"].indexOf(lcKey) !== -1 && data[lcKey] === ""){
						continue;
					}
					let result = validationStrategies[key](data[lcKey]);
					obj.validations[lcKey] = validationStrategies[key](data[lcKey]);
					if(!result){
						obj.passed = false;
					}
				}
			}
			return obj;
		}

		function _showError(step, validationObj){
			let trackData = $.extend(true, {}, rawData, details);
			newLeadTrackingService.trackLeadModuleEvent(trackData, "LEAD_FORM_ERROR");
			for(let key in validationObj.validations){
				let t = validationObj.validations[key];
				if(!t){
					let selector = config.SELECTORS[step][key.toUpperCase()];
					let id = $(moduleEl).find(selector).closest(config.SELECTORS.formElementModule).attr("id");
					context.broadcast("form-validation-error", {
						id
					});
				}
			}
			if(validationObj.global){
        		$(moduleEl).find(config.SELECTORS.error).removeClass(config.CLASS.hide);
        		let msg = validationObj.message || config.VALIDATION.ERROR_MESSAGES.GENERIC;
        		$(moduleEl).find(config.SELECTORS.error).html(msg);
	        	setTimeout(function(){
	        		$(moduleEl).find(config.SELECTORS.error).addClass(config.CLASS.hide);
	        		$(moduleEl).find(config.SELECTORS.error).html("");
	        	}, config.ERROR_TIMEOUT);
			}
			global.isProcessing = false;
		}

		function _toggleFeedbackReasons(show){
			if(show){
				$(moduleEl).find(config.SELECTORS.feedbackReason).removeClass(config.CLASS.hide);
			} else {
				$(moduleEl).find(config.SELECTORS.feedbackReason).addClass(config.CLASS.hide);
			}
		}

		function _parseTemplateData(data){
			data.price = utilService.formatNumber(data.budget, { precision : 2, returnSeperate : true, seperator : window.numberFormat });
			data.companyAvatar = utilService.getAvatar(data.companyName);
			let contactedData = utilService.getCookie("user_enquiry_info");
			try {
				contactedData = JSON.parse(contactedData) || {};
				if(typeof contactedData != "object"){ // backward compatibilty
					contactedData = JSON.parse(contactedData) || {};
				}
			} catch(e){
				contactedData = {};
			}
			data.sellerTypeLabel = sharedConfig.listingSellerTransactionMapping[data.sellerType] && sharedConfig.listingSellerTransactionMapping[data.sellerType].label;
			data.default = contactedData;
			data.optInStatus = details.optIn;
			data.maskedPhone = utilService.getMaskedPhoneNumber(data.companyPhone);
			return data;
		}

		function _insertPartials(step, data, apiData){
			let templateData = $.extend(true, {stepName: step.stepName, stepData: apiData}, data, step.viewData);
			templateData = _parseTemplateData(templateData);
			for(let key in step.templates){
				let tpl = step.templates[key];
				let html = "";
				if(tpl){
					html = tpl(templateData);
					$(moduleEl).find(config.SELECTORS[key]).removeClass(config.CLASS.hide);
				} else {
					$(moduleEl).find(config.SELECTORS[key]).addClass(config.CLASS.hide);
				}
				$(moduleEl).find(config.SELECTORS[key]).html(html);
			}
			let infoLine = step.viewData && step.viewData.infoLine || "";
			if(typeof infoLine == "function"){
				infoLine = infoLine(details, templateData);
			}
			$(moduleEl).find(config.SELECTORS.info).html(infoLine);
		}

		function _updateDetails(response, details, currentStep){
			switch(currentStep.stepName){
				case "CONNECT_NOW":
				case "VERIFY_OTP":
				case "VIEW_PHONE_MORPHED":
				case "CALL_STATUS":
				case "CALL_NOW":
					if(response && response.data){
						context.broadcast('Lead_User_Created', {userId: response.data.userId});
						details.userId = response.data.userId;
						details.userEnquiryId = response.data.id;
					}
					if(response && response.data && response.data.communicationLog){
						let buyerId = response.data.communicationLog && response.data.communicationLog.callerUserId;
						let sellerId = rawData.companyUserId || (rawData.multipleCompanyIds && rawData.multipleCompanyIds[0]);
						feedbackService.setEligibleCallFeedbackData({buyerId, sellerId});
						details.callInfo = {
							callId: response.data.id,
							callingNumber: response.data.virtualNumberMapping && response.data.virtualNumberMapping.virtualNumber && response.data.virtualNumberMapping.virtualNumber.number
						};
						details.userId = buyerId;
					}
					break;
			}
		}

		function _renderStep(currentStep, data, apiData){
			rawData.stepName = currentStep.stepName;
			if(currentStep.back){
				$(moduleEl).find(config.SELECTORS.back).removeClass(config.CLASS.hide);
			} else {
				$(moduleEl).find(config.SELECTORS.back).addClass(config.CLASS.hide);
			}
			_insertPartials(currentStep, data, apiData);
			commonService.startAllModules(moduleEl);
			setTimeout(function (){
				$(moduleEl).find(config["SELECTORS"].autoFocusSelector).focus();
			}, 1000);
			let trackData = $.extend(true, {}, rawData, details);
			switch(currentStep.stepName){
				case "VERIFY_OTP":
					newLeadTrackingService.trackLeadModuleEvent(trackData, "OTP_VISIBLE");
					break;
				case "SIMILAR_PROPERTIES":
					newLeadTrackingService.trackLeadModuleEvent(trackData, "VIEW_PHONE_MORPHED_SEEN");
					break;
				case "FEEDBACK":
					newLeadTrackingService.trackLeadModuleEvent(trackData, "FEEDBACK_OPEN");
					break;
			}
			global.isProcessing = false;
		}

		function _renderLeadForm(data=rawData, apiData={}){
			let html = baseTemplate();
			details = {
				optIn: leadPyrService.getOptInCheckedStatus()
			};
			global = {};
			$(moduleEl).find(moduleConfig.placeholder).html(html);
			_renderStep(currentStep, data, apiData);
		}

		function _renderNextStep(currentStep, rawData){
			if(currentStep.stepData && typeof currentStep.stepData == "function"){
				return currentStep.stepData(rawData).then((response) => {
					if(response.skipStep){
						_submit(currentStep, {skipStepPromise: true});
					} else {
						_renderStep(currentStep, rawData, response);
					}
				}, () => {
					_showError(currentStep.stepName, {global: true});
				});
			} else {
				return _renderStep(currentStep, rawData, currentStep.stepData);
			}
		}

		function _submit(step, elementData={}){
			global.isProcessing = true;
			let uiData = _getDataFromUI(step.stepName);
			let validation = _validate(step.stepName, uiData);
			if(validation && !validation.passed){
				_showError(step.stepName, validation);
				return;
			}
			details = $.extend(true, {}, details, uiData );
			if(elementData.skipStepPromise){
				currentStep = newLeadService.getNextStep(step, {rawData, details});
				return _renderNextStep(currentStep, rawData);
			}
			if(typeof step.stepPromise == "function"){
				return step.stepPromise(rawData, details).then((response) => {
					if(response.error){
						response.global = true;
						return _showError(step.stepName, response);
					}
					_updateDetails(response, details, currentStep);
					currentStep = newLeadService.getNextStep(step, {response, rawData, details});
					return _renderNextStep(currentStep, rawData);
				}, () => {
					_showError(step.stepName, {global: true});
				});
			} else {
				currentStep = newLeadService.getNextStep(step, {rawData, details});
				return _renderNextStep(currentStep, rawData);
			}
		}

		function _storeCountryCode(data){
			details.countryId = data.value;
			details.countryLabel = data.label.toLowerCase();
		}

		function _toggleRetryCall(step){
			$(moduleEl).find(`[data-calling][data-step]`).addClass(config.CLASS.hide).siblings(`[data-step=${step}]`).removeClass(config.CLASS.hide);
		}

		function _toggleCallScreen(showConnected = true){
			if(showConnected){
				$(moduleEl).find(config.SELECTORS.CALL_STATUS_SELECTOR.CALL_CONNECTED).removeClass(config.CLASS.hide);
				$(moduleEl).find(config.SELECTORS.CALL_STATUS_SELECTOR.CALL_DISCONNECTED).addClass(config.CLASS.hide);
				$(moduleEl).find(config.SELECTORS.info).html(currentStep.viewData.infoLine);
			} else {
				$(moduleEl).find(config.SELECTORS.CALL_STATUS_SELECTOR.CALL_CONNECTED).addClass(config.CLASS.hide);
                $(moduleEl).find(config.SELECTORS.CALL_STATUS_SELECTOR.CALL_DISCONNECTED).removeClass(config.CLASS.hide);
				$(moduleEl).find(config.SELECTORS.info).html(currentStep.viewData.infoLineDisconnected);
			}
		}

		function _callEvents(data){
			let callId = details.callInfo && details.callInfo.callId;
			let callStep = 1;
			if(data.callId != callId){
				return;
			}
			switch(data.eventName){
				case "NewCall":
					// send push notification
					_toggleRetryCall(callStep);
					_toggleCallScreen(true);
					break;
				case "Dial":
					_toggleRetryCall(callStep);
					_toggleCallScreen(true);
					break;
                case "Hangup":
                	// call cut by buyer
                	if(data.callStatus == "unanswered"){
                		callStep = 2;
                	}
                	_toggleRetryCall(callStep);
                	_toggleCallScreen(false);
                	break;
                case "Disconnect":
                	// call cut by seller
                	if(data.callStatus == "unanswered"){
                		// _toggleCallScreen(false);
                		details.feedback = false;
                		_submit(currentStep);
                	}
                	break;
                case "call_feedback":
                	// openFeedback
                	// _toggleCallScreen(false);
            		details.feedback = true;
            		_submit(currentStep);
                	break;
                case "call_ended":
                	// call_ended
                	_toggleCallScreen(false);
                	details.feedback = false;
                	_submit(currentStep);
                	break;
			}
		}

		function _back(step){
			let backStep = step.back;
			if(typeof backStep != "object"){
				backStep = newLeadService.getFirstStep(rawData);
			}
			currentStep = backStep;
			_renderNextStep(currentStep, rawData);
		}

		function init(){
			moduleEl = context.getElement();
			moduleConfig = context.getConfig();
			context.broadcast("moduleLoaded", {
				name: MODULE_NAME
			});
		}

		let onmessage = {
			"open-new-lead-form": function(data){
				utilService.openPopup(moduleConfig.popupId, true);
				rawData = data;
				rawData.moduleId = moduleEl.id;
				currentStep = newLeadService.getFirstStep(data);
				rawData.step = currentStep.stepName;
				_renderLeadForm(data);
			},
			"country-code-changed": _storeCountryCode,
			"NewCall": _callEvents,
			"Hangup": _callEvents,
			"Disconnect": _callEvents,
			"Dial": _callEvents,
			"call_feedback": _callEvents,
			"call_ended": _callEvents
		};

		function onclick(event, element, elementType){
			let elementData = $(element).data();
			let trackData = $.extend(true, {}, details, rawData);
			switch(elementType){
				case "submit":
					if(!global.isProcessing){
						_submit(currentStep, elementData);
					}
					break;
				case "switch-step":
					rawData.stepName = elementData.step;
					rawData.step = elementData.step;
					currentStep = newLeadService.getFirstStep(elementData);
					_renderLeadForm();
					break;
				case "optIn":
					let status = $(element).prop("checked");
					leadPyrService.updateOptInStatus(status);
					details.optIn = status;
					break;
				case "feedback-rating":
					let rating = elementData.rating;
					$(moduleEl).find(config.SELECTORS.feedbackRating).addClass(config.CLASS.DISABLE);
					$(element).removeClass(config.CLASS.DISABLE);
					let show = false;
					if(rating <= 3){
						show = true;
					}
					_toggleFeedbackReasons(show);
					break;
				case "otp-on-call":
					$(moduleEl).find(config.SELECTORS.VERIFY_OTP.OTP).val("");
					let userId = details.userId;
					let userNumber = details.phone;
					element = $(element);
					leadPyrService.sendOTPOnCall({userId, userNumber, element}).then(() => {
					}, () => {
						_showError(currentStep, {global: true});
					});
					break;
				case "resend-otp":
					$(moduleEl).find(config.SELECTORS.VERIFY_OTP.OTP).val("");
					newLeadTrackingService.trackLeadModuleEvent(trackData, "OTP_RESEND");
					$(element).html("Sending...");
					newLeadService.resendOtp(details).then(() => {
						$(element).html("Resend");
					}, () => {
						$(element).html("Resend");
					});
					break;
				case "retry-call":
					if(!global.isProcessing){
						global.isProcessing = true;
						newLeadService.retryCall({details, rawData}).then((response) => {
							global.isProcessing = false;
							_updateDetails(response, details, currentStep);
						}, () => {
							_showError(currentStep, {global: true});
						});
					}
					break;
				case "closePopup":
					newLeadTrackingService.trackLeadModuleEvent(trackData, "CLOSE_LEAD");
					break;
				case "back":
					if(currentStep.back){
						_back(currentStep);
					}
			}
		}

		return {
			init,
			onmessage,
			onclick,
			behaviors
		};
	});
});
