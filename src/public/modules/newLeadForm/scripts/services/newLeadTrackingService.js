'use strict';
define(['services/loggerService',
    'services/utils',
    'common/sharedConfig',
    'services/apiService',
    'common/trackingConfigService'
], (Logger, Utils, SharedConfig, ApiService, t) => {
    const SERVICE_NAME = "newLeadTrackingService";
    Box.Application.addService(SERVICE_NAME, (application) => {
        let config = {},
            moduleId;

        let pageType = Utils.getTrackPageCategory();
        config = {
            PAGE_TYPE: pageType,
            PAGE_CATEGORY: 'Buyer_' + pageType,
            LEAD_SUBMIT: 'lead_submit',
            LEAD_STORED: 'lead_stored',
            LEAD_CLOSE: 'CLOSE_lead',
            LEAD_BACK: 'lead_back',
            LEAD_DISMISS: 'lead_dismiss',
            LEAD: 'lead'
        };

        function _getSimilarListingsLabel(data) {
            let allListingId = [],
                allListingIdLabels = [],
                label, ids;
            $.each(data, (k, v) => {
                let rank;
                try {
                    rank = parseInt(k) + 1;
                } catch (ex) {
                    rank = k + 1;
                }
                if (v.listingId) {
                    allListingId.push(v.listingId);
                    allListingIdLabels.push(v.listingId + '_' + rank);
                }
            });
            ids = allListingId.join(",");
            label = "similar_" + allListingIdLabels.join(",") + "_" + Object.keys(data).length;

            return {
                label, ids
            };
        }

        function trackLeadModuleEvent(data, type) {
            moduleId = data.moduleId;
            let _eventData;
            let action;
            switch (type) {
                case "MAIN_SUBMIT_CLICKED":
                case "MAIN_TOP_SELLER_SUBMIT_CLICKED":
                    action = config["LEAD_SUBMIT"] + '_connect_now';
                    application.broadcast('trackConnectNowLead', _getTrackData(data));
                    break;
                case "CALL_NOW_SUBMIT_CLICKED":
                    application.broadcast('trackCallNowLead', _getTrackData(data));
                    break;
                case "VIEW_PHONE_MORPHED_SUBMIT_CLICKED":
                     application.broadcast('viewPhoneSubmitLead', _getTrackData(data));
                     break;
                case "VIEW_PHONE_MORPHED_SEEN":
                     application.broadcast('trackViewPhoneSeenMorphedLead', _getTrackData(data));
                     break;
                case "MAIN_CONNECT_NOW_SUCCESS":
                    action = config["LEAD_STORED"] + '_connect_now';
                    break;
                case "MAIN_VIEW_PHONE":
                    action = config["LEAD_SUBMIT"] + '_view_number';
                    application.broadcast('trackViewPhoneLead', _getTrackData(data));
                    break;
                case "SIMILAR_LEAD_SUBMIT_SUCCESS":
                    action = config["LEAD_STORED"] + '_similar';
                    break;
                case "SIMILAR_SELLERS_LEAD_SUBMIT_CLICKED":
                    _eventData = _getTrackData(data);
                    _eventData.label = "similarSeller";
                    application.broadcast('trackSimilarSubmitLead',_eventData);
                    break;
                case "SIMILAR_LEAD_SUBMIT_CLICKED":
                    _eventData = _getTrackData(data);
                    _eventData.label = "similar";
                    application.broadcast('trackSimilarSubmitLead',_eventData);
                    break;
                case "OTP_VISIBLE":
                    application.broadcast("trackOtpViewedLead", _getTrackData(data));
                    break;
                case 'OTP_CORRECT':
                    application.broadcast("trackOtpCorrectLead", _getTrackData(data));
                    break;
                case 'OTP_INCORRECT':
                    application.broadcast("trackOtpIncorrectLead", _getTrackData(data));
                    break;
                case 'OTP_RESEND':
                    application.broadcast("trackOtpResendLead", _getTrackData(data));
                    break;
                case 'SIMILAR_SELECT_ALL':
                    application.broadcast('trackSelectAllSimilarLead', _getTrackData(data));
                    break;
                case "SIMILAR_SKIP_CLICKED":
                    break;
                case "VIEW_PHONE_GOTIT_CLICKED":
                    application.broadcast('trackDismissLead', _getTrackData(data));
                    break;
                case "VIEW_PHONE_JOURNEY_CLICKED":
                    application.broadcast('trackClaimNowLead', _getTrackData(data));
                    break;
                case "THANKS_GOTIT_CLICKED":
                    action = config["LEAD_DISMISS"];
                    application.broadcast('trackDismissLead', _getTrackData(data));
                    break;
                case "MOBILE_CALL_NOW":
                    action = config["LEAD_SUBMIT"] + '_call_now';
                    application.broadcast('trackCallNowLead', _getTrackData(data));
                    break;
                case "CLOSE_LEAD":
                    action = config["LEAD_CLOSE"];
                    application.broadcast('trackCloseLead', _getTrackData(data));
                    break;
                case "BACK":
                    action = config["LEAD_BACK"];
                    application.broadcast('trackBackLead', _getTrackData(data));
                    break;
                case 'LEAD_FORM_ERROR':
                    application.broadcast('trackLeadFormErrors', _getTrackData(data));
                    break;
                case 'FEEDBACK_OPEN':
                    application.broadcast('trackFeedbackShown',_getTrackData(data));
                    break;
                case 'FEEDBACK_RATING_STORED':
                    application.broadcast('trackFeedbackRating',_getTrackData(data));
                    break;
            }
        }

        function _getTrackData(eventData = {}) {
            let response = {},
                sourceModuleConfig = t.LEAD_SOURCE_MODULES,
                stepsConfig = t.LEAD_STEPS_MAP;

            if(stepsConfig[eventData.stepName]) {
                response[t.LABEL_KEY] = t.LEAD_LABELS["SingleSeller"] + '_' + stepsConfig[eventData.stepName];
            }

            response[t.EMAIL_KEY] = eventData && eventData.email;
            response[t.PHONE_USER_KEY] = eventData && eventData.phone;
            response[t.NAME_KEY] = eventData && eventData.name;

            response[t.BUYER_OPT_IN_KEY] = eventData && eventData.optIn ? t.BUYER_OPT_IN_TRUE : t.BUYER_OPT_IN_FALSE;

            response[t.RANK_KEY] = eventData && eventData.rank;
            response[t.SOURCE_MODULE_KEY] = eventData && sourceModuleConfig[eventData.source];

            response[t.CITY_ID_KEY] = eventData && eventData.cityId;
            response[t.LISTING_CATEGORY_KEY] = eventData && t.makeValuesUniform(eventData.salesType,t.LISTING_CATEGORY_MAP) ;

            response[t.LOCALITY_ID_KEY] = eventData && $.isArray(eventData.localityIds) && eventData.localityIds.join();
            if(!response[t.LOCALITY_ID_KEY]){
                response[t.SELLER_ID_KEY] = eventData && eventData.localityId;
            }

            response[t.PROJECT_ID_KEY] = eventData && eventData.projectId;
            response[t.LISTING_ID_KEY] = eventData && eventData.listingId;
            response[t.UNIT_TYPE_KEY] = eventData && (eventData.propertyTypeLabel) && t.makeValuesUniform(eventData.propertyTypeLabel,t.UNIT_TYPE_MAP);

            response[t.SELLER_ID_KEY] = eventData && $.isArray(eventData.multipleCompanyIds) && eventData.multipleCompanyIds.join();
            if(!response[t.SELLER_ID_KEY]){
                response[t.SELLER_ID_KEY] = eventData && eventData.companyId;
            }

            response[t.BUILDER_ID_KEY] = eventData && eventData.builderId;
            response[t.BUDGET_KEY] = eventData && eventData.budget;
            response[t.BHK_KEY] = eventData && eventData.bhk && t.makeValuesUniform(eventData.bhk.toString(),t.BHK_MAP) ;
            response[t.PROJECT_STATUS_KEY] = eventData && t.makeValuesUniform(eventData.projectStatus,t.PROJECT_STATUS_NAME) ;
            response[t.SELLER_NAME_KEY] = eventData && eventData.companyName;
            response.id = moduleId;

            return response;
        }

        return {
            trackLeadModuleEvent
        };
    });
    return Box.Application.getService(SERVICE_NAME);
});
