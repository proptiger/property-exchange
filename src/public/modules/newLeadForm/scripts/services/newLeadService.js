'use strict';
define([
	'doT!modules/newLeadForm/views/header',
	'doT!modules/newLeadForm/views/detail',
	'doT!modules/newLeadForm/views/content',
	'doT!modules/newLeadForm/views/footer',
	'doT!modules/newLeadForm/views/full',
	'common/sharedConfig',
	'services/apiService',
	'modules/lead/scripts/services/leadService',
	'services/leadPyrService',
	'services/defaultService',
	'services/localStorageService',
	'modules/newLeadForm/scripts/services/newLeadTrackingService'
], (headerTemplate, detailTemplate, contentTemplate, footerTemplate, fullTemplate, sharedConfig, apiService, leadService, leadPyrService, defaultService, localStorageService, newLeadTrackingService) => {
	const SERVICE_NAME = "newLeadService";

	Box.Application.addService(SERVICE_NAME, (application) => {
		const DEFAULT_STEP = "CONNECT_NOW";

		const LABELS = {
			SITE_VISIT: "site_visit",
			CALL_FEEDBACK: "quality"
		};

		function _getSimilarData(data){
			let count = 2;
			return leadService.getSimilarListing(data.listingId, data.companyId, count);
		}

		function _postEnquiry(rawData, details){
			let socketId = (localStorageService.getItem('jStorage') || {}).session_id;
			let apiData = {
				enquiryType: {},
				socketId
			};
			let type;
			leadPyrService.setAlreadyContact(rawData.listingId);
			application.broadcast("view_status_changed");
			let step = rawData.stepName;
			if(step == "VERIFY_OTP"){
				step = rawData.step;
			}
			switch(step){
				case "VIEW_PHONE_MORPHED":
					apiData.enquiryType.id = sharedConfig.enquiryTypeMapping.VIEW_PHONE;
					type = "VIEW_PHONE_MORPHED_SUBMIT_CLICKED";
					break;
				case "CALL_NOW":
					apiData.enquiryType.id = sharedConfig.enquiryTypeMapping.CALL_NOW;
					type = "MOBILE_CALL_NOW";
					break;
				case "SIMILAR_SELLERS":
					type = "SIMILAR_SELLERS_LEAD_SUBMIT_CLICKED";
					apiData.enquiryType.id = sharedConfig.enquiryTypeMapping.SIMILAR_SELLERS;
					break;
				case "SIMILAR_PROPERTIES":
					apiData.enquiryType.id = sharedConfig.enquiryTypeMapping.SIMILAR_SELLERS;
					type = "SIMILAR_LEAD_SUBMIT_CLICKED";
					break;
				case "CONNECT_NOW":
					apiData.enquiryType.id = sharedConfig.enquiryTypeMapping.GET_INSTANT_CALLBACK;
					type = "MAIN_SUBMIT_CLICKED";
					break;
				default:
					apiData.enquiryType.id = sharedConfig.enquiryTypeMapping.GET_INSTANT_CALLBACK;
					type = "MAIN_SUBMIT_CLICKED";
			}
			apiData = $.extend(true, apiData, details, rawData);
			if(type){
				newLeadTrackingService.trackLeadModuleEvent(apiData, type);
			}
			delete apiData.similarProperties;
			let api = sharedConfig.apiHandlers.leadForm({action: "submit"}).url;
			return apiService.postJSON(api, apiData);
		}

		const STEP_CONFIG = {
			CONNECT_NOW : {
				stepName: "CONNECT_NOW",
				nextStep: function(apiData = {}){
					if(apiData.data && apiData.data.isVerificationRequired){
						return STEP_CONFIG.VERIFY_OTP;
					}
					return STEP_CONFIG.CALL_STATUS;
				},
				stepPromise: _postEnquiry,
				templates: {
					header: headerTemplate,
					detail: detailTemplate,
					content: contentTemplate,
					footer: footerTemplate
				},
				viewData: {
					obfuscatePhone: true,
					infoLine: "Share your details"
				}
			},
			VIEW_PHONE_MORPHED: {
				stepName: "VIEW_PHONE_MORPHED",
				nextStep: function(apiData = {}){
					if(apiData.data && apiData.data.isVerificationRequired){
						return STEP_CONFIG.VERIFY_OTP;
					}
					return STEP_CONFIG.SIMILAR_PROPERTIES;
				},
				stepPromise: _postEnquiry,
				templates: {
					header: headerTemplate,
					detail: detailTemplate,
					content: contentTemplate,
					footer: footerTemplate
				},
				viewData: {
					obfuscatePhone: true,
					infoLine: "Share your details"
				}
			},
			CALL_NOW: {
				stepName: "CALL_NOW",
				nextStep: function(apiData = {}){
					if(apiData.data && apiData.data.isVerificationRequired){
						return STEP_CONFIG.VERIFY_OTP;
					}
					return STEP_CONFIG.SIMILAR_PROPERTIES;
				},
				stepPromise: _postEnquiry,
				templates: {
					header: headerTemplate,
					detail: detailTemplate,
					content: contentTemplate,
					footer: footerTemplate
				},
				viewData: {
					obfuscatePhone: true,
					infoLine: "Share your details"
				}
			},
			VERIFY_OTP: {
				stepName: "VERIFY_OTP",
				nextStep: function(data, rawData){
					if(rawData.step == "VIEW_PHONE_MORPHED"){
						return STEP_CONFIG.SIMILAR_PROPERTIES;
					} else if(rawData.step == "CALL_NOW"){
						return STEP_CONFIG.SIMILAR_PROPERTIES;
					}
					return STEP_CONFIG.CALL_STATUS;
				},
				back: true,
				stepPromise: function(rawData, details){
					let api = sharedConfig.apiHandlers.leadForm({action: "verify"}).url;
					let trackData = $.extend(true, {}, rawData, details);
					return apiService.postJSON(api, details).then((response) => {
						if(response && response.data && response.data.otpValidationSuccess){
							newLeadTrackingService.trackLeadModuleEvent(trackData, "OTP_CORRECT");
							return _postEnquiry(rawData, details);
						} else {
							newLeadTrackingService.trackLeadModuleEvent(trackData, "OTP_INCORRECT");
							return {
								error: true,
								message: "Wrong OTP"
							};
						}
					});
				},
				templates: {
					content: contentTemplate,
					footer: footerTemplate,
					detail: detailTemplate
				},
				viewData: {
					obfuscatePhone: true,
					infoLine: function(details){
						return `Verify your number ${details.phone}<span class="edit" data-type="back">edit</span>`;
					}
				}
			},
			SIMILAR_PROPERTIES: {
				stepName: "SIMILAR_PROPERTIES",
				back: true,
				stepData: _getSimilarData,
				nextStep: function(){
					return STEP_CONFIG.THANK_YOU;
				},
				stepPromise: function(rawData, details){
					let similarProperties = details.similarProperties;
					let promises = [];
					for(let key in similarProperties){
						let prop = similarProperties[key];
						let dummyRawData;
						try{
							dummyRawData = JSON.parse(prop);
							dummyRawData.stepName = "SIMILAR_PROPERTIES";
							dummyRawData.moduleId = rawData.moduleId;
						} catch(e){
							dummyRawData = false;
						}
						if(!dummyRawData){
							continue;
						}
						promises.push(_postEnquiry(dummyRawData, details));
					}
					return Promise.all(promises);
				},
				templates: {
					detail: detailTemplate,
					content: contentTemplate,
					footer: footerTemplate
				},
				viewData: {
					infoLine: function(details, templateData){
						if(templateData && templateData.stepData && templateData.stepData.length === 0){
							if(["CALL_NOW"].indexOf(templateData.step) > -1){
								return `Click on ‘Call’ to connect with ${templateData.companyName} for Free!`;
							} else {
								return `You can connect with ${templateData.companyName} on the above number`;
							}
						}
						return "You may also like";
					}
				}
			},
			CALL_STATUS: {
				stepName: "CALL_STATUS",
				nextStep: function(response, rawData, details){
					if(details.feedback){
						return STEP_CONFIG.FEEDBACK;
					} else {
						return STEP_CONFIG.SIMILAR_SELLERS;
					}
				},
				templates: {
					detail: detailTemplate,
					content: contentTemplate,
					footer: footerTemplate
				},
				viewData: {
					infoLine: "Connecting you with seller...",
					infoLineDisconnected: "Could not connect with seller"
				}
			},
			SIMILAR_SELLERS: {
				stepName: "SIMILAR_SELLERS",
				stepData: function(data){
					return _getSimilarData(data).then((response) => {
						let tempMap = {};
						if(response && response.length){
							return response.filter((v) => {
								let val = !(tempMap[v.companyId]);
								tempMap[v.companyId] = 1;
								return val;
							});
						} else {
							return {
								skipStep: true
							};
						}
					});
				},
				nextStep: function(){
					return STEP_CONFIG.THANK_YOU;
				},
				stepPromise: function(rawData, details){
					let similarProperties = details.similarProperties;
					let promises = [];
					for(let key in similarProperties){
						let prop = similarProperties[key];
						let dummyRawData;
						try{
							dummyRawData = JSON.parse(prop);
							dummyRawData.stepName = "SIMILAR_SELLERS";
							dummyRawData.moduleId = rawData.moduleId;
						} catch(e){
							dummyRawData = false;
						}
						if(!dummyRawData){
							continue;
						}
						promises.push(_postEnquiry(dummyRawData, details));
					}
					return Promise.all(promises);
				},
				templates: {
					detail: false,
					content: contentTemplate,
					footer: footerTemplate
				},
				viewData: {
					infoLine: "Based on your requirements, we found these other sellers for you"
				}
			},
			FEEDBACK: {
				stepName: "FEEDBACK",
				stepData: function(){
					return defaultService.getRiviewOptions().then((response) => {
						return response[LABELS.CALL_FEEDBACK];
					});
				},
				stepPromise: function(rawData, details){
					let trackData = $.extend(true, {}, rawData, details);
					let api = sharedConfig.apiHandlers.callRating().url;
					let apiData = {
						callId: details.callInfo.callId,
						rating: details.callRating,
						reason: details.ratingReason,
						sellerId: rawData.companyUserId
					};
					if(apiData.rating <= 3){
						apiData.reason = details.ratingReason;
					}
					return apiService.postJSON(api, apiData).then((response) => {
						newLeadTrackingService.trackLeadModuleEvent(trackData, "FEEDBACK_RATING_STORED");
						return response;
					});
				},
				nextStep: function(){
					return STEP_CONFIG.THANK_YOU;
				},
				templates: {
					header: false,
					detail: detailTemplate,
					content: contentTemplate,
					footer: footerTemplate
				},
				viewData: {
					infoLine: "How would you rate your experience?"
				}
			},
			THANK_YOU: {
				stepName: "THANK_YOU",
				templates: {
					header: false,
					detail: false,
					content: false,
					footer: footerTemplate,
					full: fullTemplate
				}
			}
		};

		function getNextStep(currentStep, {rawData, response, details}){
			let nextStep = currentStep.nextStep;
			if (typeof nextStep == "function"){
				return nextStep(response, rawData, details);
			} else {
				return nextStep;
			}
		}

		function getFirstStep(data){
			let step = data.step || DEFAULT_STEP;
			return STEP_CONFIG[step];
		}

		function resendOtp(details){
			let api = sharedConfig.apiHandlers.leadForm({action: "resend-otp"}).url;
			let apiData = {
				userEnquiryId: details.userEnquiryId,
				phone: details.phone,
				userId: details.userId
			};

			return apiService.postJSON(api, apiData);
		}

		function retryCall({details, rawData}){
			return _postEnquiry(rawData, details);
		}

		return {
			getFirstStep,
			getNextStep,
			resendOtp,
			retryCall
		};

	});

	return Box.Application.getService(SERVICE_NAME);

});
