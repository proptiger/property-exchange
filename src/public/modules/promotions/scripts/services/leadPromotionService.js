'use strict';
define([], function(){
    const SERVICE_NAME = "leadPromotionService";
    Box.Application.addService(SERVICE_NAME, (context) => {

        function _getPriorityEmail(emails){
            emails.sort(function(email1,email2){
                return email1.priority - email2.priority;
            });
            return emails[0].email
        }
        
        function checkPhoneVerified({userId, email, phone, response}){
            let verificationDetail = {isVerified: false, phone: null},
                verifiedContacts;

            
            if(email && response.email == email){
                verifiedContacts = response.contactNumbers.filter(contactNumber => {
                    return contactNumber.isVerified;
                });
                verificationDetail =  verifiedContacts.length ? {isVerified: true, phone: verifiedContacts[0].contactNumber} : {isVerified: false, phone: response.contactNumbers[0].contactNumber};
                verificationDetail['email'] = email;
            } else if(phone){
                verifiedContacts = response.contactNumbers.filter(contact => {
                    return contact.contactNumber == phone;
                });
                verificationDetail = verifiedContacts.length ? {isVerified: verifiedContacts[0].isVerified, phone} : {isVerified: false, phone};
                verificationDetail['email'] = response.emails.length ? _getPriorityEmail(response.emails) : (verifiedContacts.length ? response.email : '');
            }else{
                verificationDetail['email'] = response.emails.length ? _getPriorityEmail(response.emails) : (verifiedContacts.length ? response.email : '');
            }
            verificationDetail['name'] = response.fullName||'';
            return verificationDetail;
        }

        function preProcessData(details, moduleConfig){
            details.postData.sendOtp =  details.postData.isVerified ? false : true;
            delete(details.postData.isVerified);
            details.step = details.postData.sendOtp ? "MAIN" : ( moduleConfig.showbutton ? "SHOW_INTERESTED" : "SHOW_SUCCESS" );
            details.postData = $.extend(true, details.postData, {enquiryType: {id : 17}, domainId: 1, applicationType: 'Desktop Site', salesType: "buy", countryId: 1});
            details.displayData = {companyUserId : moduleConfig.companyUserId};
            details.postData.multipleCompanyIds = [moduleConfig.companyId];
            details.postData.cityId = moduleConfig.cityId;
            details.postData.cityName = moduleConfig.cityName;
            if(moduleConfig.projectId) {
                details.postData.projectId = moduleConfig.projectId;
            }
            if(moduleConfig.listingId){
                let listingData = {};
                listingData.bhk = moduleConfig.bhk;
                listingData.listingId = moduleConfig.listingId;
                listingData.localityIds = moduleConfig.localityId;
                listingData.propertyTypes = [moduleConfig.propertyTypeId];
                listingData.salesType = moduleConfig.listingCategory == 'Primary' ? 'buy': 'rent';
                details.postData = $.extend(true, details.postData, listingData);
            }
        }

        return {
            checkPhoneVerified,
            processRawData: preProcessData
        }
    });
    return Box.Application.getService(SERVICE_NAME);
});