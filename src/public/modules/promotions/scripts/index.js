/**
JIRA- MAKAAN-3365
    (Marketing compaign) Handle the functionlity of automatic lead creation for Project and Listings based on URL parameters(projectId/listingId ,userId and phone/email).
    Its Completely independant module(currently on a newly added static page 'promotions').
    It is associated to page autoLeadGeneration page.
*/

'use strict';
define([
        'modules/promotions/scripts/services/leadPromotionService',
        'modules/promotions/scripts/behaviors/promotionsTracking',
        'services/commonService',
        'services/leadPyrService',
        'modules/lead/scripts/services/leadService',
        'services/urlService',
        'services/apiService',
        'services/filterConfigService',
        'common/sharedConfig',
        'doT!modules/promotions/views/main',
        'doT!modules/promotions/views/otp',
        'doT!modules/promotions/views/thankyou',
        'behaviors/leadBehaviour'
    ], function(leadPromotionService,promotionsTracking, CommonService, LeadPyrService, leadService, urlService, apiService, filterConfigService, sharedConfig, mainHtml, otpHtml, thankyouHtml){
    Box.Application.addModule("promotions", (context => {
        let moduleEl, $, $moduleEl, moduleConfig, isMobileRequest, _details = {};

        const selector = {
                "TEMPLATE_CONTAINER": "[data-container]",
                "PHONE": "[data-type=PHONE_FIELD] input",
                "COUNTRY_ID": "input[data-country=COUNTRY_ID_FIELD]",
                "OTP": "[data-type=OTP_FIELD] input",
                "OTP_RESEND": "[data-type=OTP_RESEND]",
                "THANKYOU_MESSAGE": ".js-thankyou",
                "INTERESED_BUTTON": ".interestedButton"
            },
            CLASSES = {
                HIDE : "hide"
            },
            resendTryTime = 5000;

        // To hide show page loader.
        function fullPageLoader(show = false){
            if(show){
                $('.js-main-loading').removeClass(CLASSES.HIDE);    
            } else {
                $('.js-main-loading').addClass(CLASSES.HIDE);
            }
        }

        function _startAllModules() {
            CommonService.startAllModules(moduleEl);
        }

        function _stopAllModules() {
            CommonService.stopAllModules(moduleEl);
        }

        //function to render module Template, stop and start template modules.
        function _appendHtmlToView(html, callback) {
            _stopAllModules();
            $(moduleEl).find(selector["TEMPLATE_CONTAINER"]).html(html);
            _startAllModules();
            fullPageLoader(false);
            if (callback && typeof callback === "function") {
                callback();
            }
        }

        function _focusInput(selector, type) {
            $(moduleEl).find(`[name=${selector}]`).focus();
            if (type === "module") {
                CommonService.bindOnModuleLoad("InputBox", (id) => {
                    if (id === selector) {
                        $(moduleEl).find(`[name=${selector}]`).focus();
                    }
                });
            }
        }

        // function to decide which template to render based on current step.
        function renderView(details, step){
            let html, cb;
            step = step || details.step;
            switch(step){
                case "MAIN":
                    html = mainHtml(details);
                    cb = _focusInput.bind(undefined, "promotional-lead-phone", 'module');
                    break;
                case "OTP":
                    html = otpHtml(details);
                    cb = _focusInput.bind(undefined, "otp_lead");
                    break;
                case "SHOW_SUCCESS":
                    html = thankyouHtml({});
                    $(selector.THANKYOU_MESSAGE).removeClass(CLASSES.HIDE);
                    setTimeout(getRedirectionUrl, 5000);
                    break;
            }
            _appendHtmlToView(html, cb); 
        }

        
        //function to get user details inordern to generate lead for a user.
        function getUserData(){
            let urlData = urlService.getAllUrlParam();
            let userId = urlData.uid;
            let email = urlData.email;
            let phone = urlData.phone;
            let projectId = urlData.projectId;
            let listingId = urlData.listingId;
            apiService.get(sharedConfig.apiHandlers.userDetails(userId).url).then(response => {
                _details.postData = $.extend(true, _details.postData, leadPromotionService.checkPhoneVerified({userId, email, phone, response}));
                _details.response = response;
                leadPromotionService.processRawData(_details, moduleConfig);
                if(_details.step == "SHOW_SUCCESS") {
                    $(selector.THANKYOU_MESSAGE).removeClass(CLASSES.HIDE);
                    context.broadcast('trackAutoLeadSubmit'); 
                    _submitScreenData(_details);
                } else if(_details.step == "SHOW_INTERESTED") {
                    let dataObj = {
                        clientId: userId,
                        domainId: sharedConfig.PROMOTION_PAGE_IDS.MAKKAN_DOMAIN_ID,
                        caseTypeId: sharedConfig.PROMOTION_PAGE_IDS.RIPPLE_CASE_ID,
                        caseSubTypeId: sharedConfig.PROMOTION_PAGE_IDS.RIPPLE_SALE_TYPE_ID,
                        countryIds: [sharedConfig.PROMOTION_PAGE_IDS.INDIA_COUNTRY_ID],
                        productType: sharedConfig.PROMOTION_PAGE_IDS.RIPPLE_PRODUCT_TYPE,
                        cityIds: [moduleConfig.cityId]
                    };
                    let tempKey = projectId ? 'projectIds' : 'listingIds';
                    dataObj[tempKey] = projectId ? [projectId] : [listingId];
                    _details.rippleCaseId = '';
                    apiService.postJSON(sharedConfig.apiHandlers.tempRippleCase().url, dataObj).then((response) => {
                        let ripplecase = (response && response.length) ? response[0] : null;
                        _details.rippleCaseId = ripplecase ? ripplecase.id : '';
                    });
                    $(selector.INTERESED_BUTTON).removeClass(CLASSES.HIDE);
                    fullPageLoader(false);
                } else {
                    $(selector.THANKYOU_MESSAGE).removeClass(CLASSES.HIDE);
                    renderView(_details);
                }
            }, (err) => {
                fullPageLoader(false);
            });
        }

        //function to update details object to based on the data in response of an api call.
        function _updateResponseInData(step, details, data) {
            details["response"][step] = details["response"][step] || {};
            leadService.updateResponse(step, details, data);
            _details = $.extend(true, _details, details);
        }

        //function to post temporaty enquiry and this also generates otp based on {sendOtp flag} in details object.
        function _postTempEnquiry(details){
            let step = details.step;
            leadService.postTemporaryLead(details, details.postData.enquiryType.id).then((response) => {
                response.reqIsVerified = !details.postData.sendOtp;
                _updateResponseInData(step, details, response);
                details.step = "OTP";
                details.prevStep = details.initialStep = "MAIN";
                renderView(details, "OTP");
            }, () => {
                context.broadcast("SHOW_ERROR", { errorType: "GENERIC", field: "GLOBAL" });
            }).always(() => {
                fullPageLoader(false);
            });
        }

        //functiom to post enquiry.
        function _postEnquiry(details){
            leadService.postEnquiry(details, undefined, details.postData.enquiryType.id).then(response => {
                context.broadcast('enquiry_submit',response);
            }).always(() => {
                fullPageLoader(false);
            });
        }

        //function to verify the otp entered by the user.
        function _verifyAndConnect(formData, details) {
            let step = details.step;
            leadService.verifyOTP(details).then((otp_response) => {
                _updateResponseInData(step, details, otp_response);
                details.postData.sendOtp = false;
                if (details.response[step].isVerified) { //is true if the otp entered is correct.
                    context.broadcast('trackOTPLeadSubmit');
                    _postEnquiry(details);
                    renderView(details,"SHOW_SUCCESS");
                } else {
                    fullPageLoader(false);
                    context.broadcast("SHOW_ERROR", { moduleEl, errorType: "INVALID", field: "OTP" });
                }

            }, () => {
                context.broadcast("SHOW_ERROR", { moduleEl, errorType: "GENERIC", field: "GLOBAL" });
            }).always(() => {
                fullPageLoader(false);
            });
                    

        }

        // function to get screen data and making appropriate api calls based on the current screen.
        function _submitScreenData(details, step){
            step = step || details.step;
            fullPageLoader(true);
            switch(step){
                case 'MAIN':
                    details.postData.phone = $(moduleEl).find(selector['PHONE']).val().trim();
                    details.postData.countryId = $(moduleEl).find(selector['COUNTRY_ID']).val().trim();
                    context.broadcast('trackTempLeadSubmit');
                    _postTempEnquiry(details);
                    break;
                case "OTP":
                    let data = {};
                    data["otp"] = $(moduleEl).find(selector['OTP']).val().trim();
                    leadService.updateUserFields(data, details.postData, step);
                    _verifyAndConnect(data, details);
                    break;
                case "SHOW_SUCCESS":
                    _postEnquiry(details);
                    renderView(details, "SHOW_SUCCESS");
                    break;
            }
        }

        // function to redirect to listing page with various filters depending on project and listing detail after successful submission of lead.
        function getRedirectionUrl(){
            let urlparams = {};
            urlparams.cityId = moduleConfig.cityId;
            urlparams.cityName = moduleConfig.cityName;
            urlparams.listingType = moduleConfig.listingCategory ? (moduleConfig.listingCategory == "Primary" ? 'buy' : 'rent') : 'buy';
            urlparams.beds = moduleConfig.bhk;
            urlparams.localityId = moduleConfig.localityId;
            urlparams.budget = moduleConfig.budget;
            urlparams.localityName = moduleConfig.localityName || '';
            let url =  filterConfigService.getUrlWithUpdatedParams({skipFilterData: true, overrideParams: urlparams});
            window.location.href = url;
        }

        function _showResendButton(show = true) {
            if (!show) {
                $(moduleEl).find(selector["OTP_RESEND"]).addClass(CLASSES.HIDE);
            } else {
                $(moduleEl).find(selector["OTP_RESEND"]).removeClass(CLASSES.HIDE);
            }
        }

        function _resendOTP(details) {
            delete details.postData.otp;
            _showResendButton(false);
            leadService.verifyOTP(details).always(() => {
                _showResendButton();
            }).then(() => {
                context.broadcast("SHOW_SUCCESS", { moduleEl, successType: "SUCCESS_OTP", field: "GLOBAL" });
            }, () => {
                context.broadcast("SHOW_ERROR", { moduleEl, errorType: "GENERIC", field: "GLOBAL" });
            });
            $(moduleEl).find('[name=otp_lead]').val('');
            _focusInput("otp_lead");
            window.setTimeout(_showResendButton, resendTryTime);
        }

        return {
            init: function(){
                moduleEl = context.getElement();
                $ = context.getGlobal('jQuery');
                $moduleEl = $(moduleEl);
                moduleConfig = context.getConfig();
                isMobileRequest = moduleConfig.isMobileRequest ? true : false;
                !moduleConfig.noSeller ? getUserData() : fullPageLoader(false);
            }, 
            destroy: function(){
                moduleEl = $ = $moduleEl = moduleConfig = null;
            },
            behaviors: ['leadBehaviour','promotionsTracking'],
            messages: ['navigateNextStep', 'userInterested'],
            onmessage: function(name, data){
                switch(name){
                    // gets the message from lead behavior when user presses submit button on the screen.
                    case "navigateNextStep":
                        if(moduleEl.id != data.id) return;
                        _submitScreenData(_details, data.step);
                        break;
                    case "userInterested":
                        if(_details.rippleCaseId) {
                            let dataObj = {
                                "rippleActionCreationDto":{
                                    "reasonId": sharedConfig.PROMOTION_PAGE_IDS.RIPPLE_CASE_REASON_ID,
                                    "notes":"case is verified"
                                }
                            };
                            apiService.putJSON(sharedConfig.apiHandlers.tempRippleCase(_details.rippleCaseId).url, JSON.stringify(dataObj)).then(() => {});
                        }
                        context.broadcast('trackUserInterestedLeadSubmit');
                        _submitScreenData(_details, "SHOW_SUCCESS");
                        break;
                }
            },
            onclick: function(event, element, elementType){
                switch(elementType){
                    //When user asks for OTP on call.
                    case 'OTP_ONCALL':
                        LeadPyrService.sendOTPOnCall({
                            userId: _details.response[_details.prevStep].userId,
                            userNumber: _details.postData.phone,
                            element: $(element)
                        });
                        break;
                    // when user want to receive the OTP again.
                    case 'OTP_RESEND':
                        _resendOTP(_details);
                        break;
                }
            }
        }
    }));
});