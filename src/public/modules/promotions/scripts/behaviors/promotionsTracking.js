define([
    'common/trackingConfigService',
    'services/trackingService'
], function(t, trackingService) {
    'use strict';
    Box.Application.addBehavior('promotionsTracking', function(context) {

        var element;
        const messages = [
            'trackAutoLeadSubmit',
            'trackUserInterestedLeadSubmit',
            'trackOTPLeadSubmit',
            'trackTempLeadSubmit'
        ];

        var init = function() {
            element = context.getElement();
        };

        var onmessage = function(name, data) {
                let properties = {};
                let category =t.LEAD_FORM_CATEGORY,
                    event = t.SUBMIT_EVENT,
                    label ="Lead_Gen",
                    sourceModule;
                switch (name) {
                    case 'trackAutoLeadSubmit':
                        sourceModule = 'Automatically';
                        break;
                    case 'trackUserInterestedLeadSubmit':
                        sourceModule = "I'm Interested";
                        break;
                    case 'trackOTPLeadSubmit':
                        event = "OTP";
                        break;
                }
                properties[t.CATEGORY_KEY] = category;
                properties[t.LABEL_KEY] = label;
                properties[t.SOURCE_MODULE_KEY] = sourceModule;

                trackingService.trackEvent(event, properties);
        };
        return {
            messages,
            onmessage,
            init,
            destroy: function() {}
        };
    });
});