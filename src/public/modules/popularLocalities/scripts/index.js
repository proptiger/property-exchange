"use strict";
define([
    'doT!modules/popularLocalities/views/template',
    'services/apiService',
    'common/sharedConfig',
    "services/urlService",
], (template,apiService,sharedConfig,urlService) => {
    Box.Application.addModule('popularLocalities', (context) => {
        var messages = ['loadPopularLocalities'],
            behaviors = [],
            element, moduleId, $, $moduleEl, moduleConfig;
        function render(data={}){
            $moduleEl.find('.locality-list-wrap').html(template(data))
        }
        function getData(cityId){
            return apiService.get(sharedConfig.apiHandlers.getPopularLocalities({cityId}).url)
            
        }
        function init() {
            //initialization of all local variables
            element = context.getElement();
            moduleId = element.id;
            $ = context.getGlobal('jQuery');
            moduleConfig = context.getConfig();
            $moduleEl = $(element);
            context.broadcast('moduleLoaded', {
                'name': 'popularLocalities',
                'id': element.id
            });
        }

        function destroy() {
            //clear all the binding and objects
        }

        function onmessage(name, data) {
            // bind custom messages/events
            switch(name){
                case 'loadPopularLocalities':
                    let cityId = moduleConfig.defaultCityId;
                    if(data && data.cityId){
                        cityId = data.cityId;
                    }
                    getData(cityId).then((result)=>{
                        render(result)
                    },(error)=>{
                        render();
                        //as this is an internal api so if need to be tested on local, uncomment below code and try
                        // render({popularLocalities:[{
                        //     listingCount:128,
                        //     localityName:"Dadar East",
                        //     localitySERPUrl:"listings?listingType=buy&pageType=LOCALITY_URLS&localityName=Dadar East&localityId=50044&templateId=MAKAAN_LOCALITY_LISTING_BUY",
                        //     uniqueSellerCount:40

                        // },{
                        //     listingCount:128,
                        //     localityName:"Dadar East",
                        //     localitySERPUrl:"listings?listingType=buy&pageType=LOCALITY_URLS&localityName=Dadar East&localityId=50044&templateId=MAKAAN_LOCALITY_LISTING_BUY",
                        //     uniqueSellerCount:40

                        // },{
                        //     listingCount:128,
                        //     localityName:"Dadar East",
                        //     localitySERPUrl:"listings?listingType=buy&pageType=LOCALITY_URLS&localityName=Dadar East&localityId=50044&templateId=MAKAAN_LOCALITY_LISTING_BUY",
                        //     uniqueSellerCount:40
                        // }]})
                    });
                break;
            }
        }

        function onclick(event, element, elementType) {
            // bind custom messages/events
            switch(elementType){
                case 'locality-link':
                    event.preventDefault();
                    let dataset = $(element).data();

                    context.broadcast('popularLocalityClicked',{
                        id: moduleId,
                        index: dataset.index+1
                    });
                    urlService.ajaxyUrlChange(dataset.href);
                break;
            }
        }

        return {
            init,
            messages,
            behaviors,
            onmessage,
            onclick,
            destroy
        };
    });
});