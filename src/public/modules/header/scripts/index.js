"use strict";
define([
    'services/utils',
    "services/localStorageService",
    'services/commonService',
    'services/loginService',
    'services/filterConfigService',
    'services/urlService',
    'common/sharedConfig',
    'modules/header/scripts/behaviors/headerTracking',
    'behaviors/headerLoginBehaviour',
    'doT!modules/menuDrawer_SEO/views/menuDrawerSeller'
], (utils, localStorageService, commonService, loginService, filterConfigService, urlService, sharedConfig) => {
    Box.Application.addModule('header', (context) => {
        const config = context.getGlobal('config'),
            windowObj = context.getGlobal('window'),
            UserNameSelector = "[data-user-name]",
            LoginElement = "[data-login-element]",
            UserImageSelector = "[data-user-image]",
            LoginSelector = "[data-type='login']",
            AvatarDataType = "[data-link-type='avatar']",
            UserNameDataType = "[data-link-type='user-name']",
            NameInitial = "[data-initial]",
            SellerSiteLink = "[data-type='track-sell-your-property']",
            BuyRentHeader = "[data-type=categoryDropdown]",
            //DummyImage = "/images/avtaar.png",
            queryData = Box.DOM.queryData,
            query = Box.DOM.query,
            HIDE = 'hide',
            DATA_TARGET = 'target',
            DATA_TYPE = 'type',
            CATEGORY_PLACEHOLDER = 'categoryPlaceholder',
            CATEGORY_DROPDOWN_OPTION = 'categoryDropdownOption',
            HIDDEN = 'hidden',
            HIDE_ON_SEARCH = 'js-hideonsearch',
            OVERLAY = 'overlay-sec',
            MENUDRAWER = 'hamburger',
            MENUDRAWER_CLOSE = 'menudrawer-close',
            MENU_DRAWER_OPTION = 'menu_drawer_option',
            MENUDRAWER_SELECT_OPTION = 'menu-option-select',
            FAV_INDICATOR = '[data-fav-indicator]',
            FAV_INDICATOR_CLASS = 'fadein',
            FAV_INDICATOR_TIMEOUT = 3000,
            NON_ACTIVE_EVENT_INTERVAL = 30 * 1000,
            isMobile = utils.isMobileRequest(),
            menuDrawerTemplateSeller = 'doT!modules/menuDrawer_SEO/views/menuDrawerSeller',
            SelectCoachMark = '.js-select-coach-mark',
            SelectToolTip = '.makaan-select',
            SelectToolTipContent = '.makaan-select-tooltip-content .tooltiptext',
            LOCAL_STORAGE_SELECT_KEY = "SELECT_COUPON_GENERATED",
            MAKAAN_SELECT_URL = '/makaan-select',
            COACH_KNOW_MORE_TEXT = `<span onclick="Box.Application.broadcast('coachMarkKnowMore')">Save &#x20B9;2000 on brokerage with <label>Select</label> Agents<a href="javascript:void(0)" class="link">Know More</a></span>`,
            COACH_UNLOCK_TEXT= `<span onclick="Box.Application.broadcast('coachMarkUnlockVoucher')">Save &#x20B9;2000 on brokerage on finalizing your home<a href="javascript:void(0)" class="link">Unlock Select Voucher</a></span>`
            ;
            
        var messages = ['typeAheadOverlayClicked', 'login', 'logout', 'loggedOut', 'onLoggedIn', 'hideBuyRentHeader', 'showBuyRentHeader', 'pageDataListingTypeChange', 'updateOnlyHeaderBuyRentLabel', 'notLoggedIn', 'shortlistItemAdded', 'buy_rent_switch', 'pageLoaded', 'coachMarkKnowMore', 'coachMarkUnlockVoucher'],
            behaviors = ['headerTracking', 'headerLoginBehaviour'],
            moduleEl,
            categoryPlaceholder,
            categoryDropdown,
            categoryOptions,
            overlaySec,
            nonActiveEventTimer,
            onlineStatus = navigator.onLine || true,
            onlineStatusTimer,
            isLoggedIn,
            onSellerJourneyPage,
            userImageLink,
            userNameLink,
            isSellerLoggedIn,
            toolTipTimeOut,
            nonActiveTimerCount = 1;

        $(SelectToolTip).hover(function () {
            if (!toolTipTimeOut) {
                toolTipTimeOut = window.setTimeout(function () {
                    toolTipTimeOut = null;
                    context.broadcast('trackSelectCoachMarkHover', _getSelectTrackData());
                }, 2000);
            }
        },
        function () {
            if (toolTipTimeOut) {
                window.clearTimeout(toolTipTimeOut);
                toolTipTimeOut = null;
            }
        });

        function _setUserName(status, data){
            context.broadcast('setUserName', {
                moduleId: moduleEl.id,
                status,
                data
            });
        }
        function hideFullPageLoader(){
            $('.js-main-loading').addClass('hide');
        }
        function checkNonActiveEvent(){
            let cnt = nonActiveTimerCount;
            if (((cnt < 2) || (cnt <= 10 && cnt % 2 == 0) || (cnt > 10 && cnt <= 40 && cnt % 10 == 0) || (cnt == 58)) && cnt <= 58) {
                return true
            } else {
                return false;
            }
        }
        function init() {
            moduleEl = context.getElement();
            if (utils.getPageData('pageType') && utils.getPageData('pageType').indexOf("PROJECT_SERP") != -1) {
                context.broadcast('hideBuyRentHeader');
            }
            //initialization of all local variables
            onSellerJourneyPage = windowObj.location.pathname.includes(config.sellerJourneyUrl);
            commonService.startAllModules(moduleEl);
            userImageLink = $(UserImageSelector).find(AvatarDataType);
            userNameLink = $(UserNameSelector).find(UserNameDataType);
            categoryPlaceholder = Box.DOM.queryData(moduleEl, DATA_TARGET, CATEGORY_PLACEHOLDER);
            categoryOptions = Box.DOM.queryAllData(moduleEl, DATA_TYPE, CATEGORY_DROPDOWN_OPTION);

            if (onlineStatusTimer) {
                clearInterval(onlineStatusTimer);
            }

            if(!nonActiveEventTimer){
                nonActiveEventTimer = window.setInterval(function(){
                    if(checkNonActiveEvent()){
                        context.broadcast("trackNonActiveEvent", {counter: nonActiveTimerCount});
                    }
                    nonActiveTimerCount++;
                }, NON_ACTIVE_EVENT_INTERVAL);
            }

            if (isMobile) {
                onlineStatusTimer = setInterval(function() {
                    if (navigator && ('onLine' in navigator)) {
                        if (!navigator.onLine && onlineStatus) {
                            onlineStatus = false;
                            $('.offline-message-js').fadeIn(1000, function() {
                                $('.offline-message-js').delay(2000).fadeOut(3000);
                            });
                        } else if (navigator.onLine) {
                            onlineStatus = true;
                        }
                    }
                }, 1000);
            }
            let qs = utils.getAllQueryStringParam();
            let hashArray = (windowObj.location.hash && windowObj.location.hash.split('&')) || [];
            if(!onSellerJourneyPage || !(qs.loginPopup || hashArray.indexOf('loginPopup') > -1 || hashArray.indexOf('#loginPopup') > -1)){
                hideFullPageLoader()
            }
            
            loginService.isUserLoggedIn().then((response) => {
                hideFullPageLoader();
                response = (response && response.data) || {};
                _setUserName('login', response);
                toggleLogoutMenuDrawer(true);
            }, () => {
                hideFullPageLoader();
                // Open signup form if user is not logged in and signup=true in query params
                if (onSellerJourneyPage && !isLoggedIn && (qs.loginPopup || hashArray.indexOf('loginPopup') > -1 || hashArray.indexOf('#loginPopup') > -1)) {
                    loginService.openLoginPopup();
                }
                if (!isMobile) {
                    context.broadcast('notLoggedIn');
                }
            }).always(()=>{
                hideFullPageLoader();
            });

            if (!isLoggedIn && onSellerJourneyPage) {
                userImageLink.removeAttr('href');
            }
        }

        function _initSelectToolTip() {
            if ($(SelectCoachMark).length) {
                let text = COACH_UNLOCK_TEXT,
                    selector = SelectCoachMark,
                    couponData = _getCouponData(),
                    coachMarkData = utils.selectCoachMarkVisibility(couponData,sharedConfig.couponShareEligibleTime);

                if (coachMarkData && coachMarkData.coachMarkKey == sharedConfig.coachMarkSelector.knowMoreKey) {
                    text = COACH_KNOW_MORE_TEXT;
                }
                $(SelectToolTipContent).html(text);
                if (coachMarkData && coachMarkData.coachMarkFlag) {
                    _showSelectCoachMark({
                        text,
                        isMobile,
                        selector,
                        tooltipClass: 'makaan-select-tooltip',
                        highlightClass: 'makaan-select-tooltip-highlight',
                        position: 'bottom-middle-aligned'
                    });
                }
            }
        }
        function _getCouponData() {
            return localStorageService.getItem(LOCAL_STORAGE_SELECT_KEY);
        }
        function _getSelectTrackData() {
            let couponData = _getCouponData();
            let couponCode = (couponData && couponData.code) || '';
            let userId = (couponData && couponData.generatorUserId) || '';
            return {
                isKnowMore: couponCode ? false : true,
                couponCode: couponCode,
                userId: userId
            }
        }
        function _showSelectCoachMark(coachMarkData) {
            context.broadcast('loadModule', {
                name: 'selectCoachMark',
                loadId: 'selectCoachMark'
            });
            commonService.bindOnModuleLoad('selectCoachMark', () => {
                context.broadcast('showSelectCoachMark', coachMarkData);
                context.broadcast('trackSelectCoachMarkSeen', _getSelectTrackData());
            });
        }
        function destroy() {
            //clear all the binding and objects
            if (onlineStatusTimer) {
                clearInterval(onlineStatusTimer);
            }

            if(nonActiveEventTimer) {
                window.clearInterval(nonActiveEventTimer);
            }
        }

        function onmessage(name, data) {
            // bind custom messages/events
            switch (name) {
                case 'hideBuyRentHeader':
                    $(moduleEl).find(BuyRentHeader).addClass('hide');
                    break;
                case 'showBuyRentHeader':
                    $(moduleEl).find(BuyRentHeader).removeClass('hide');
                    break;
                case 'onLoggedIn':
                case 'login':
                    _setUserName('login', data.response, data.roles);
                    toggleLogoutMenuDrawer(true);
                    break;
                case 'logout':
                case 'loggedOut':
                    _setUserName('logout');
                    toggleLogoutMenuDrawer(false);
                    break;
                case 'typeAheadOverlayClicked':
                    $(query(moduleEl, `.${HIDE_ON_SEARCH}`)).removeClass(HIDDEN);
                    break;
                case 'pageDataListingTypeChange':
                case 'updateOnlyHeaderBuyRentLabel':
                    if (data.listingType && data.listingType.length && !isMobile) {
                        $(categoryOptions).removeClass(config.activeClass);
                        $(categoryOptions).filter(`.${data.listingType}`).addClass(config.activeClass);
                        categoryPlaceholder.innerHTML = (data.listingLabel || data.listingType).split(/(?=[A-Z])/).join(' ');
                    }
                    break;
                case 'notLoggedIn':
                    $(LoginSelector).removeClass(HIDE);
                    _initSelectToolTip();
                    break;
                case 'shortlistItemAdded':
                    $(moduleEl).find(FAV_INDICATOR).addClass(FAV_INDICATOR_CLASS);
                    setTimeout(function() {
                        $(moduleEl).find(FAV_INDICATOR).removeClass(FAV_INDICATOR_CLASS);
                    }, FAV_INDICATOR_TIMEOUT);
                    break;
                case 'buy_rent_switch':
                    if(data.listingType){
                        buyRentFilter(data.listingType)    
                    }
                    break;
                case 'pageLoaded':
                    setMakaanSelectLogoVisibility();
                    break;
                case 'coachMarkKnowMore':
                    context.broadcast('hideSelectCoachMark',{});
                    let popupid = utils.isMobileRequest() ? 'msKnowMorePopup' : 'msKnowMoreDeskPopup';
                    context.broadcast('trackSelectCoachMarkClick', _getSelectTrackData());
                    _openPopup(popupid, true);
                    break;
                case 'coachMarkUnlockVoucher':
                    let pageTab = utils.isMobileRequest() ? '_self' : '_blank';
                    context.broadcast('hideSelectCoachMark', {});
                    context.broadcast('trackSelectCoachMarkClick', _getSelectTrackData());
                    window.open(MAKAAN_SELECT_URL, pageTab);
                    break;
            }   
        }

        function _openPopup(id, closeOnEsc) {
            context.broadcast('popup:open', {
                id: id,
                options: {
                    config: {
                        modal: closeOnEsc || false
                    }
                }
            });
        }

        function onclick(event, element, elementType) {
            //let pageType = utils.getTrackPageCategory(),
                //pageCategory = 'Buyer_' + pageType,
              let  clickElementData = $(element).data() || {};
            // bind custom messages/events
            switch (elementType) {
                case 'download-app':
                    context.broadcast('trackDownloadClick',clickElementData);
                    if(clickElementData.url) {
                      commonService.ajaxify(clickElementData.url);
                    }
                    break;
                case 'logout':
                    logout(moduleEl);
                    break;
                case 'categoryDropdown':
                    categoryDropdown = element;
                    overlaySec = queryData(moduleEl, 'type', OVERLAY);
                    $(categoryDropdown).toggleClass(config.openClass);
                    $(overlaySec).toggleClass(HIDDEN);
                    break;
                case 'categoryDropdownOption':
                    let listingType = clickElementData.listingType;
                    let listingLabel = clickElementData.label;
                    context.broadcast('trackCategoryOptionClick', { listingType });

                    if (!$(element).hasClass(config.activeClass)) {
                        utils.trackIdentity(undefined, {
                            buy_rent_switch: true
                        });
                        $(categoryOptions).removeClass(config.activeClass);
                        $(element).addClass(config.activeClass);
                        categoryPlaceholder.innerHTML = listingLabel.split(/(?=[A-Z])/).join(' ');
                        buyRentFilter(listingType, listingLabel)
                    }
                    $(categoryDropdown).removeClass(config.openClass);
                    $(overlaySec).toggleClass(HIDDEN);
                    break;
                case OVERLAY:
                    //$('body').removeClass('openmenu');
                    $(overlaySec).toggleClass(HIDDEN);
                    $(categoryDropdown).removeClass(config.openClass);
                    $(queryData(moduleEl, 'type', MENUDRAWER)).removeClass('open');
                    break;
                case MENUDRAWER:
                    context.broadcast('trackMenuDrawerClickHeader');
                    var bodyHeight = $(document).height();
                    $('body').addClass('openmenu');
                    $('.md-overlay').height(bodyHeight);
                    //$('.mdbox').height(bodyHeight);
                    overlaySec = queryData(moduleEl, 'type', OVERLAY);
                    $(queryData(moduleEl, 'type', MENUDRAWER)).toggleClass('open');
                    //$(overlaySec).toggleClass(HIDDEN);
                    $(moduleEl).find('[data-type="js-accordian-sec"].active').removeClass('active');
                    break;
                case MENUDRAWER_CLOSE:
                    $('body').removeClass('openmenu');
                    context.broadcast('hideMenuDrawerNextLevel');
                    break;
                case MENUDRAWER_SELECT_OPTION:
                    $('body').removeClass('openmenu');
                    break;
                case 'logoutmenudrawer':
                    loginService.logoutUser();
                    break;
                case MENU_DRAWER_OPTION:
                    overlaySec = queryData(moduleEl, 'type', OVERLAY);
                    $(queryData(moduleEl, 'type', MENUDRAWER)).toggleClass('open');
                    $(overlaySec).toggleClass(HIDDEN);
                    $(moduleEl).find('[data-type="js-accordian-sec"].active').removeClass('active');
                    break;
                case 'track-logo':
                    context.broadcast('trackLogoClick',clickElementData);
                    break;
                case 'track-sell-your-property':
                    context.broadcast('trackSellPropertyClick',clickElementData);
                    event.preventDefault();
                    let dataset = $(element).data();
                    loginService.setUserProp('sellerMicrositeUrl', dataset.href);
                    loginService.openLoginPopup('checkRegistered');
                    break;
                case 'makaan-select-logo':
                    context.broadcast('trackMakaanSelectLogoClick',clickElementData);
                    window.location.href = clickElementData.href;
                    break;
                case 'page-back-btn':
                    context.broadcast('trackPageBackBtnClick');
                    window.history.back();
                    break;

            }
        }

        function buyRentFilter(listingType, listingLabel){
            if (utils.getPageData('isSerp')) {
                context.broadcast('pageDataListingTypeChange', {
                    listingType,
                    listingLabel,
                    id: 'headerSearchBox',
                    'config': {
                        'name': 'searchTypeAhead'
                    }
                });
                urlService.ajaxyUrlChange(filterConfigService.getUrlWithUpdatedParams({
                    skipFilterData: true,
                    overrideParams: {
                        listingType: listingType
                    }
                }), true);
            } else {
                utils.setPageData('listingType', listingType);
                context.broadcast('pageDataListingTypeChange', {
                    listingType,
                    listingLabel,
                    id: 'headerSearchBox',
                    'config': {
                        'name': 'searchTypeAhead'
                    }
                });
            }
        }

        function setMakaanSelectLogoVisibility() {
            let pageListingType = utils.getPageData('listingType');
            let pageCityId = utils.getPageData('cityId');
            if(pageListingType=='rent' && sharedConfig.makaanSelectCities.indexOf(pageCityId) > -1 ) {
                $('.js-makaanSelectLogo').parent('li').removeClass('hide');
                $('.js-makaanSelectLogo').addClass('js-select-coach-mark');
                $('.js-homeloanLogo').parent('li').addClass('hide');
                if (!isMobile){
                    _initSelectToolTip();
                }
            }
            else {
                $('.js-makaanSelectLogo').parent('li').addClass('hide');
                $('.js-homeloanLogo').parent('li').removeClass('hide');
            }
        }

        function toggleLogoutMenuDrawer(login) {
            let el = $('#loginchecker');
            if (login) {
                el.text('logout').removeClass('hidden');
            } else {
                el.text('').addClass('hidden');
            }
        }

        function toggleListPropertyHeader(loginSeller) {
            let el = $('.sellersitelink');
            if (loginSeller) {
                el.addClass('hidden');
            } else {
                el.removeClass('hidden');
            }
        }

        function logout() {
            loginService.logoutUser().then(() => {
                _setUserName('logout');
            }, () => {
                //notification service
            });
        }

        return {
            init,
            messages,
            behaviors,
            onmessage,
            onclick,
            destroy
        };
    });
});
