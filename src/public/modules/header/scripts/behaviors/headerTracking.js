define([
    'common/trackingConfigService',
    'services/trackingService'
], function(t, trackingService) {
    'use strict';
    Box.Application.addBehavior('headerTracking', function() {

        var messages = [
            'trackLogoClick',
            'trackSellPropertyClick',
            'trackMyJourneyClick',
            'trackLoginClick',
            'trackCategoryOptionClick',
            'trackMenuDrawerClickHeader',
            'trackMyDashboardClick',
            'trackDownloadClick',
            'trackNonActiveEvent',
            'trackMakaanSelectLogoClick',
            "interactive-event",
            "trackSelectCoachMarkSeen",
            "trackSelectCoachMarkHover",
            "trackSelectCoachMarkClick",
            "trackPageBackBtnClick"
        ];

        let nonActive = 1;

        var _getSelectCoachMarkCategory = function (isKnowMore) {
            if (isKnowMore) {
                return t.SELCT_COACH_MARK_KNOW_MORE;
            } else {
                return t.SELCT_COACH_MARK_UNLOCK;
            }
        }

        var onmessage = function(name, data = {}) {
            let event = t.NAVIGATION_EVENT,
                category = t.HEADER_CATEGORY,
                properties = {},
                label,action;

            switch (name) {
                case 'trackLogoClick':
                    label = t.HOME_LABEL;
                    break;
                case 'trackSellPropertyClick':
                    label = t.LIST_PROPERTY_LABEL;
                    break;
                case 'trackMyJourneyClick':
                    label = t.MY_ACCOUNT_LABEL;
                    break;
                case 'trackMyDashboardClick':
                    event = t.MY_DASHBOARD_EVENT;
                    category = t.MY_DASHBOARD_CATEGORY;
                    break;
                case 'trackLoginClick':
                    label = t.LOGIN_LABEL;
                    break;
                case 'trackCategoryOptionClick':
                    label = data.listingType;
                    break;
                case 'trackMenuDrawerClickHeader':
                    category = t.MENU_DRAWER_CATEGORY;
                    event = t.CLICK_EVENT;
                    label = t.OPEN_LABEL;
                    break;
                case 'trackNonActiveEvent':
                    event = t.NON_ACTIVE;
                    label = data.counter && data.counter * 30;
                    nonActive++;
                    properties[t.NON_INTERACTION_KEY] = 1;
                    break;
                case 'trackMakaanSelectLogoClick':
                    label = 'Select_Header_Navigation';
                    event = t.CLICK_EVENT;
                    category = t.SELECT_VOUCHER_REDEMPTION_CATEGORY;
                    break;
                case "interactive-event":
                    nonActive = 1;
                    return;
                case 'trackSelectCoachMarkSeen':
                    event = t.SELECT_COACH_MARK_VISIBLE;
                    label = t.SELECT_COACH_MARK_LABEL;
                    category = _getSelectCoachMarkCategory(data.isKnowMore);
                    properties[t.COUPON_CODE_KEY] = data.couponCode;
                    properties[t.USER_ID_KEY] = data.userId;
                    break;
                case 'trackSelectCoachMarkHover':
                    event = t.SELECT_COACH_MARK_HOVER;
                    label = t.SELECT_COACH_MARK_LABEL;
                    category = _getSelectCoachMarkCategory(data.isKnowMore);
                    properties[t.COUPON_CODE_KEY] = data.couponCode;
                    properties[t.USER_ID_KEY] = data.userId;
                    break;
                case 'trackSelectCoachMarkClick':
                    event = t.SELECT_COACH_MARK_CLICK;
                    label = t.SELECT_COACH_MARK_LABEL;
                    category = _getSelectCoachMarkCategory(data.isKnowMore);
                    properties[t.COUPON_CODE_KEY] = data.couponCode;
                    properties[t.USER_ID_KEY] = data.userId;
                    break;
                case 'trackPageBackBtnClick':
                    event = t.CLICK_EVENT;
                    label = t.BACK_BUTTON_LABEL;
                    category = t.NAVIGATION_CATEGORY;
                    break;
            }

            properties[t.CATEGORY_KEY] = category;
            properties[t.LABEL_KEY] = label;
            properties[t.LINK_TYPE_KEY] = data.linkType;
            properties[t.LINK_NAME_KEY] = data.linkName;
            trackingService.trackEvent(event, properties);
        };

        return {
            messages: messages,
            onmessage: onmessage,
            init: function() {},
            destroy: function() {}
        };
    });
});
