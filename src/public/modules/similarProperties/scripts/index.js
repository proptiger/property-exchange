
define([
    'doT!modules/similarProperties/views/index',
    'services/apiService',
    'common/sharedConfig',
    'services/commonService',
], function(template, apiService, sharedConfig,commonService) {
    "use strict";
    Box.Application.addModule('similarProperties', function(context) {

        var moduleElem, $, config, similarPropertiesCache = {},
            apiPromises = {},
            HIDECLASS = 'hide';

        let messages = ['reloadSimilarProperties'];

        function onmessage(name, data){
            if(data.id != moduleElem.id){
                return;
            }
            switch (name) {
                case 'reloadSimilarProperties':
                    render(data);
                    break;
                default:
            }
        }

        function setCacheData(url, res){
            let properties = res || {};
            similarPropertiesCache[url] = properties;
        }

        function render(data){
            if(data && data.queryParams){
                let html;
                $(moduleElem).html('');
                let url = sharedConfig.apiHandlers.getSellerMatchingProperties().url + `?${data.queryParams}`;
                if(similarPropertiesCache[`${url}`]){
                    html = template({
                        properties: similarPropertiesCache[`${url}`].data || [],
                        propertyCount: similarPropertiesCache[`${url}`].totalCount
                    });
                    $(moduleElem).html(html);
                    commonService.startAllModules(moduleElem);
                    if(similarPropertiesCache[`${url}`].totalCount){
                        $(moduleElem).removeClass(HIDECLASS);
                    }
                }else {
                    if(!apiPromises[url]){
                        apiPromises[url] = apiService.get(url);
                    }
                    apiPromises[url].then((res)=>{
                        setCacheData(url, res);
                        html = template({
                            properties: similarPropertiesCache[`${url}`].data || [],
                            propertyCount: similarPropertiesCache[`${url}`].totalCount
                        });
                        $(moduleElem).html(html);
                        commonService.startAllModules(moduleElem);
                        if(similarPropertiesCache[`${url}`].totalCount){
                            $(moduleElem).removeClass(HIDECLASS);
                        }
                    }, ()=>{
                        $(moduleElem).addClass(HIDECLASS);
                    }).always(()=>{
                        apiPromises[url] = null;
                    });
                }
            }
        }

        return {
            init: function() {
                moduleElem = context.getElement();
                $ = context.getGlobal('jQuery');
                config = context.getConfig() || {};
                context.broadcast('moduleLoaded', {
                    'name': 'similarProperties',
                    'id': moduleElem.id
                });
            },
            messages,
            onmessage,
            destroy: function() {
                $ = null;
                moduleElem = null;
            }
        };
    });
});
