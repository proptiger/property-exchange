"use strict";
define(["text!modules/resetPassword/views/index.html",
    'services/loggerService',
    'services/apiService'
],function(html){
    /**
    * module for forgot password
    **/
	Box.Application.addModule('resetPassword',function(context){
		var config, moduleEl, $, $moduleEl, defaultConfig = {
            resetPasswordURL: '/xhr/userService/resetPassword',
            newPasswordSelector : '#newpassword',
            confirmPasswordSelector : '#confirmpassword'
        };
        var commonService;
        var logger = Box.Application.getService('Logger');
        var apiService = Box.Application.getService('ApiService');
        var showError = function(target,message){
            var targetContainer = $(moduleEl).find(target).closest('.login-input-box');
            targetContainer.addClass('error');
            targetContainer.find('.js-errorMessage').html(message);
        };
        var isValidData = function(){
            var isValid=true;
            var newPwd = $moduleEl.find(config.newPasswordSelector).val();
            var confirmNew = $moduleEl.find(config.confirmPasswordSelector).val();
            if(!newPwd){
                showError(config.newPasswordSelector,"Please enter new password");
                isValid=false;
            }
            else if(newPwd.length < 6){
                showError(config.newPasswordSelector,"Password must be atleast 6 characters long");
                isValid=false;   
            }
            if(!confirmNew){
                showError(config.confirmPasswordSelector,"Please enter confirm password");
                isValid=false;
            }else if(newPwd!==confirmNew){
                 showError(config.confirmPasswordSelector,"New password and confirm password do not match");
                 isValid=false;
            }
            
            var postData = {
                newPassword: newPwd,
                confirmNewPassword: confirmNew,
                token: unescape(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + window.escape('token').replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"))
            };
            return isValid? postData:false ;
        };
        var submitForReset = function(postData){
            apiService.postJSON(config.resetPasswordURL, postData, null, true).then(_successHandler, _errorHandler);
            function _successHandler(response){                 // jshint ignore:line
                logger.log('password reset successfully');
                $moduleEl.find('.js-succsess-error-msg').html("Password changed, please login with new password.");
                setTimeout(function(){
                    context.broadcast('PasswordResetSuccess');
                    window.location.href="/";
                },2000);
            }
            function _errorHandler(error){
                logger.log('error occurred');
                logger.log(error);
                $moduleEl.find('.js-succsess-error-msg').html("Oops! error occurred. please try again in sometime.");
            }
        };
        
         function bindControlEvents(){
            if($moduleEl){
                $moduleEl.find('input').on('focus', function(){
                    $(this).closest('.login-input-box').addClass('active');
                });
                $moduleEl.find('input').on('blur', function(){
                    if(!$(this).val()){
                        $(this).closest('.login-input-box').removeClass('active');
                    }
                    else{
                         $(this).closest('.login-input-box').removeClass('error');
                    }
                });
            }
        }
        function unBindBlurEvents(){
            if($moduleEl){
                $moduleEl.find('input').off('focus');
                $moduleEl.find('input').off('blur');
            }

        }
		return {
	        init: function() {
                config = context.getConfig();
                moduleEl = context.getElement();
				$ = context.getGlobal('jQuery');
                $moduleEl = $(moduleEl);
                $moduleEl.append(html);
                config = $.extend(defaultConfig, config);
                commonService = Box.Application.getService('CommonService');
				context.broadcast('moduleLoaded', {
	            	name: 'resetPassword'
            	});
                context.broadcast('popup:open', {
                    'id': moduleEl.id
                });
                bindControlEvents();
        	},
        	destroy: function(){
	            moduleEl = null;
				$ = null;
				$moduleEl = null;
                unBindBlurEvents();
        	},
            onclick: function(event, element, elementType) {
	        	switch (elementType) {
	        		case 'reset-submit':
                        var postData = isValidData();
	        			if(postData) {
                            submitForReset(postData);
	        			}
	        			break;
                }
            }
        };
	});
});
