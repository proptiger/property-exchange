"use strict";
define(['services/utils',"services/commonService"], (utils, CommonService) => {
    Box.Application.addModule("slider", (context) => {
        const QUERY = Box.DOM.query,
            QUERYALL = Box.DOM.queryAll,
            TRIGGER = Box.DOM.trigger,
            ELEMENT_SELECTOR = '[data-type="list-element"]',
            PARENT_SELECTOR = '[data-type="list-container"]',
            PREVIOUS_CLASS = '.js-prev',
            NEXT_CLASS = '.js-next',
            NEXT_BUTTON_SELECTOR = '[data-type="next-btn"]';

        var _xtransform = 0,
            _slider = {},
            _state = {
                previousButtonDisabled: true,
                nextButtonDisabled: false,
                currentIndex: 0,
                previousIndex: -1,
                horizontalScroll: 0
            },
            _autoplayInterval,
            ARROW_CLASS,
            LAZYLOAD_ELEMENT,
            PRELOAD_COUNT,
            //trackFlag,
            messages = ['slideTo','stopAutoSlider','startAutoSlider'],
            imageTags,moduleEl,lazyModules,config;

        function _swipe(width, slider) {
            if(_slider.lazyloadImages){
                let element = imageTags.eq(_state.currentIndex)[0],
                    source = element.getAttribute('data-slidersrc'),
                    src = element.getAttribute('src');
                if (source && !src) {
                    element.setAttribute('src', source);
                }
            }
            if(_slider.loadLazyModules){
                let currModuleElement = lazyModules.eq(_state.currentIndex)[0];
                for(let i=0, length = lazyModules.length; i<length; i++){
                    CommonService.stopModule(lazyModules[i]);
                }
                context.broadcast('loadModule', {
                    "name": _slider.lazyLoadModule,
                    "loadId": currModuleElement.id
                });
            }
            var availableWidth = (Math.abs(width) + slider.snapWidth) - (slider.elementWidth * slider.totalSlides);
            if (availableWidth > 0) {
                width = -((Math.abs(width) - availableWidth)); // in case some transformation has already been done
            }

            if(_state.horizontalScroll) {
                $(slider.el).scrollLeft(-_state.horizontalScroll);
            }

            $(slider.parent).css({
                transform: 'translateX(' + width + 'px)'
            });
            if (slider.transitionDuration) {
                slider.parent.style.transitionDuration = slider.transitionDuration;
            }
        }

        function _disableArrowButtons(slider, state) {
            let nextButton = $(slider.nextButton),
                prevButton = $(slider.previousButton),
                visibleElementsCount = slider.visibleElementsCount,
                totalSlides = slider.totalSlides;

            nextButton.removeClass(ARROW_CLASS);
            prevButton.removeClass(ARROW_CLASS);
            if (totalSlides === 1) {
                prevButton.addClass(ARROW_CLASS);
                nextButton.addClass(ARROW_CLASS);
                state.nextButtonDisabled = true;
                state.previousButtonDisabled = true;
            } else if (totalSlides <= visibleElementsCount) {
                prevButton.addClass(ARROW_CLASS);
                nextButton.addClass(ARROW_CLASS);
                state.nextButtonDisabled = true;
                state.previousButtonDisabled = true;
            } else if (state.currentIndex === totalSlides - visibleElementsCount) {
                nextButton.addClass(ARROW_CLASS);
                state.nextButtonDisabled = true;
                state.previousButtonDisabled = false;
            } else if (state.currentIndex === 0) {
                prevButton.addClass(ARROW_CLASS);
                state.previousButtonDisabled = true;
                state.nextButtonDisabled = false;
            } else {
                nextButton.removeClass(ARROW_CLASS);
                prevButton.removeClass(ARROW_CLASS);
                state.nextButtonDisabled = false;
                state.previousButtonDisabled = false;
            }
        }

        function _setWidth(slider) {
            let width = slider.elementWidth * slider.totalSlides;
            if (slider.calcInPercentage) {
                $(slider.parent).find(ELEMENT_SELECTOR).each(function() {
                    let percentVal = (100 / slider.totalSlides);
                    width = `calc(${percentVal}% - ${slider.elementMarginInPercentage}px)` ;
                    $(this).css("width", width);
                });
                $(slider.parent).css("width", (slider.totalSlides * 100) + "%");
            } else {
                $(slider.parent).css("width", width + 'px');
            }
            _slider.elementWidth = $(slider.parent).find(ELEMENT_SELECTOR).eq(0).outerWidth(true);
        }

        function _reset(direction, slider, state) {
            if (direction == 'next') {
                state.currentIndex = 0;
                _swipe(0, slider);
            } else if (direction == 'prev') {
                state.currentIndex = slider.totalSlides - slider.visibleElementsCount;
                _swipe(-(state.currentIndex * slider.elementWidth),slider);
            }
            _disableArrowButtons(slider, state);
        }

        function _startPlaying(moduleEl) {
            _autoplayInterval = setInterval(function() {
                TRIGGER(QUERY(moduleEl, NEXT_BUTTON_SELECTOR), 'click');
            }, 5000);
        }

        function initializeSliderDetails(moduleEl) {
            let element = QUERY(moduleEl, ELEMENT_SELECTOR);
            _slider.el = moduleEl;
            _slider.calcInPercentage = config.calcInPercentage || false;
            _slider.widthDivisionFactor = config.widthDivisionFactor || 1;
            _slider.snapWidth = ($('#' + moduleEl.getAttribute('data-window')).outerWidth());
            _slider.elementWidth = $(element).outerWidth(true);
            _slider.parent = QUERY(moduleEl, PARENT_SELECTOR);
            _slider.totalSlides = ($(_slider.parent).find(ELEMENT_SELECTOR).length)/_slider.widthDivisionFactor;
            _slider.previousButton = QUERY(moduleEl, PREVIOUS_CLASS);
            _slider.nextButton = QUERY(moduleEl, NEXT_CLASS);
            _slider.visibleElementsCount =Math.floor(_slider.snapWidth / _slider.elementWidth);
            _slider.lazyloadImages = config.lazyloadImages;
            _slider.loadLazyModules = config.loadLazyModules;
            _slider.lazyLoadModule = config.lazyLoadModule;
            if ((_slider.elementWidth - $(element).outerWidth()) >= _slider.elementWidth - (_slider.snapWidth % _slider.elementWidth)) {
                _slider.visibleElementsCount++;
            }
            if (config && Object.keys(config).length > 0) {
                _slider.autoplay = config.autoplay || false;
                _slider.transitionDuration = config._transitionDuration || false;
                _slider.elementMarginInPercentage = _slider.visibleElementsCount == 1 && config.elementMarginInPercentage ? parseInt(config.elementMarginInPercentage) : 0;
            }
            // if(config.tracking){
            //     trackFlag = true;
            // }
            ARROW_CLASS = config.hide?'hidden':'disable';
            LAZYLOAD_ELEMENT = config.lazyLoadElement || 'img';
            PRELOAD_COUNT = typeof config.preloadCount != 'undefined' ? config.preloadCount : 1;
            if(config.lazyloadImages){
                imageTags = $(moduleEl).find(`${ELEMENT_SELECTOR} ${LAZYLOAD_ELEMENT}`);
                for (let i = 0; i < _slider.visibleElementsCount + PRELOAD_COUNT; i++) {
                    let currImage = imageTags[i],
                        source = currImage.getAttribute('data-slidersrc'),
                        src = currImage.getAttribute('src');
                    if(source && !src){
                        currImage.setAttribute('src', source);
                    }
                }
            }
            if(config.loadLazyModules){
                lazyModules = $(moduleEl).find(`${ELEMENT_SELECTOR} [data-lazyModule="${_slider.lazyLoadModule}"]`);
                for (let i = 0; i < _slider.visibleElementsCount + PRELOAD_COUNT; i++) {
                    let currModuleElement = lazyModules[i];
                    context.broadcast('loadModule', {
                        "name": _slider.lazyLoadModule,
                        "loadId": currModuleElement.id
                    });
                }
            }
        }

        function _postInit() {
            moduleEl = context.getElement();
            initializeSliderDetails(moduleEl);
            _setWidth(_slider);
            _disableArrowButtons(_slider, _state);
            utils.isMobileRequest() && _trackMobileViewEvent(); //jshint ignore:line
            if (_slider.autoplay) {
                _startPlaying(_slider.el);
            }
        }

        function init() {
            moduleEl = context.getElement();
            config   = context.getConfig() || {};
            _postInit();
        }

        function destroy() {
            _slider = null;
            _state = null;
            if (_autoplayInterval) {
                clearInterval(_autoplayInterval);
            }
        }

        function onclick(event, element, elementType) {
            if (elementType === "next-btn" && !_state.nextButtonDisabled) {
                _state.previousIndex = _state.currentIndex;
                _state.currentIndex++;
                _swipe((_xtransform - _slider.elementWidth * _state.currentIndex), _slider);
                if(event.originalEvent) { //event should not be broadcasted in case of trigger
                    context.broadcast(_slider.el.id + "_slider_next_clicked", {
                        "index": _state.currentIndex + 1
                    });
                    _broadcastTracking('Right');
                }
                _highlightBullets(_slider, _state.currentIndex);
            } else if (elementType === "next-btn" && _slider.visibleElementsCount == 1) {
                _reset('next', _slider, _state);
                _highlightBullets(_slider, _state.currentIndex);
            } else if (elementType === "back-btn" && !_state.previousButtonDisabled) {
                if(event.originalEvent) {
                    context.broadcast(_slider.el.id + "_slider_prev_clicked", {
                        "index": _state.currentIndex + 1
                    });
                    _broadcastTracking('Left');
                }
                _state.currentIndex--;
                _swipe(-(_xtransform + _slider.elementWidth * (_state.currentIndex)), _slider);
                _highlightBullets(_slider, _state.currentIndex);
            } else if (elementType === "back-btn" && _slider.visibleElementsCount == 1) {
                _reset('prev', _slider, _state);
                _highlightBullets(_slider, _state.currentIndex);
            } else if (elementType === "list-element") {
                context.broadcast(_slider.el.id + "_slider_element_clicked",{
                    data: $(element).data("element-data"),
                    index : _state.currentIndex,
                    eventSource : event
                });
                _broadcastTracking('Click', element.dataset);
            } else if (elementType === "skip-slide") {
                _slideTo(element.dataset.index, undefined, _slider, _state);
                _broadcastTracking('bulletClick', element.dataset);
            } else if (elementType === "seeAll-btn") {
                CommonService.ajaxify(element.dataset.url);
            }
            _disableArrowButtons(_slider, _state);
        }

        function onmessage(name, data) {
            // bind custom messages/events
            switch (name) {
                case "slideTo":
                    if (data && data.index && data.id === _slider.el.id) {
                        _slideTo(data.index - 1, data.id, _slider, _state);
                    }
                    break;
                case "stopAutoSlider":
                    if (_autoplayInterval && moduleEl.id == data.id) {
                        clearInterval(_autoplayInterval);
                    }
                    break;
                case "startAutoSlider":
                    if (_slider.autoplay) {
                        _startPlaying(_slider.el);
                    }
                    break;
            }
        }


        function _trackMobileViewEvent() {
            $(_slider.el).on("scroll", function (e) {
                let horizontal = e.currentTarget.scrollLeft;
                let index = Math.ceil((horizontal/_slider.elementWidth)+0.5)-1;
                _state.horizontalScroll = horizontal;
                if(index !== _state.previousIndex){
                    context.broadcast(_slider.el.id + "_slider_element_viewed",{
                        data: $(_slider.el).find("li").not("li li").eq(index + _state.currentIndex).data("element-data"),
                        index : index+1
                    });
                    _state.previousIndex = index;
                }
            });
        }

        function _broadcastTracking(event, dataset={}) {
            context.broadcast('slider_clicked', {event, dataset, element:_slider.el});
        }

        function _slideTo(index, id, slider, state) {
            if(index<0) {return;}
            if (state.currentIndex > index) {
                state.previousIndex = state.currentIndex;
                state.currentIndex = index;
                _swipe(-(_xtransform + _slider.elementWidth * (_state.currentIndex)), _slider);
            } else {
                if (index <= slider.totalSlides-1) {
                    state.previousIndex = state.currentIndex;
                    state.currentIndex = index;
                    _swipe((_xtransform - _slider.elementWidth * _state.currentIndex), _slider);
                }
                // else {
                //     state.currentIndex = slider.totalSlides - 1
                // }
                // _swipe((_xtransform - _slider.elementWidth * _state.currentIndex), _slider);
            }
            _disableArrowButtons(slider, state);
            _highlightBullets(slider, index);
        }

        function _highlightBullets(slider, index) {
            let highlightBullets = $(QUERYALL(slider.el, '[data-type="skip-slide"]'));
            if(highlightBullets.length){
                highlightBullets.removeClass('selected');
                highlightBullets.eq(index).addClass('selected');
            }
        }

        return {
            init: init,
            destroy: destroy,
            onclick: onclick,
            messages,
            onmessage
        };
    });
});
