"use strict";
define(['services/loginService',
    'services/defaultService',
    'common/trackingConfigService',
    'services/leadPyrService',
    'services/pushNotificationService',
    'services/commonService',
    'modules/newLoginRegister/scripts/socialBehavior',
    'services/filterConfigService',
    'services/utils',
    'services/localStorageService',
    'modules/jarvisWrapper/scripts/behaviors/jarvisTracking'
], function(loginService, defaultService, t, leadPyrService, pushNotificationService) {
    Box.Application.addModule('jarvisWrapper', function(context) {

        let moduleEl,
            application = Box.Application,
            timer,globalWindow = context.getGlobal('window'),scriptLoaded = false;
        const filterModuleMap = {
                bhk: "multiSelectOptions",
                budget: "rangeSlider",
                property_type: "multiSelectOptions",
                enquiry_dropped: "slider"
            },
            filterNameMap = {
                beds: "bhk",
                property_type: "property_type"
            },
            filterConfigService = context.getService('FilterConfigService'),
            utilService = context.getService('Utils'),
            localStorageService = context.getService('localStorageService'),
            JARVIS_FILTER_TIMEOUT = 10000;

        var _triggerJarvisMixpanelEvent = function(trackingPayload) {
            let jarvisStorageData = localStorageService.getItem('jStorage'),
                deliveryId = jarvisStorageData.session_id,
                filterPayload = {};
            trackingPayload.delivery_id = deliveryId;
            switch (trackingPayload.event_name) {
                case 'serp_scroll':
                    let selectFilterkeyValObj = filterConfigService.getSelectedQueryFilterKeyValObj();
                    filterPayload = {
                        serp_filter_bhk: selectFilterkeyValObj.beds ? true : null,
                        serp_filter_budget: selectFilterkeyValObj.budget ? true : null,
                        serp_filter_property_type: selectFilterkeyValObj.propertyType ? true : null,
                    };
                    break;
            }
            $.extend(trackingPayload, filterPayload);
            globalWindow.MpAnalytics.track(trackingPayload);
        };

        var _clearAnyMixpanelEventContent = function(element, clearOnlyHtml = true) {
            let jarvisFilterWrapper = moduleEl.querySelector(".mixpanel-dialog");
            if (element) {
                application.stop(element);
            } else {
                application.stopAll(jarvisFilterWrapper);
            }
            if (jarvisFilterWrapper) {
                jarvisFilterWrapper.innerHTML = "";
                jarvisFilterWrapper.parentNode.classList.add("inactive");
                if (clearOnlyHtml) {
                    context.broadcast("jarvisEvents:pop");
                }
            }
        };

        var _startMixpanelEventDialogFadeoutTimer = function(element = null) {
            let jarvisFilterWrapper = moduleEl.querySelector(".mixpanel-dialog");
            if (jarvisFilterWrapper) {
                jarvisFilterWrapper.parentNode.classList.remove("inactive");
            }
            clearTimeout(timer);
            timer = setTimeout(function() {
                _clearAnyMixpanelEventContent(element);
                let sourceModule = $('.proptiger-chat-wrapper .mixpanel-events-cross').attr("data-card-parent"),
                    name = $('.proptiger-chat-wrapper .mixpanel-events-cross').attr("data-card-type"),
                    eventData = {};
                if(sourceModule)  {
                    eventData.sourceModule = sourceModule;
                } 
                if(name) {
                    eventData.name = name;
                }
                eventData.interactive = 1;
                eventData.id = moduleEl.id;
                context.broadcast('trackChatCardAutoFade', eventData);
            }, JARVIS_FILTER_TIMEOUT);
        };

        var _emitToJarvisSocket = function(eventName, data) {
            data = data || {};
            let jarvisStorageData = localStorageService.getItem('jStorage') || {},
                deliveryId = jarvisStorageData.session_id;
            data.deliveryId = deliveryId;
            $(document).trigger("socketEmit", [eventName, data]);
        };

        var _verifyContact = function(data = {}){
            let phone = data.userPhone || "";
            leadPyrService.checkOTPVerified(phone).then((response) => {
                //user logged in
                data.numberVerifcation = response;
                $(document).trigger("numberStatus", data);
            }, (response) => {
                //user not logged in
                data.numberVerifcation = response;
                $(document).trigger("numberStatus", data);
            });
        };

        var _savePhone = function(data = {}){
            leadPyrService.updateContactNumbers({
                contactNumber: data.phone,
                isVerified: true
            });
        };

        var onmessage = function(name, data) {
            let eventData,
                pattern;
            switch (name) {
                case 'interactiveEventTriggered':
                        if(!scriptLoaded){
                            $.getScript(window.JARVIS_URL + 'user/app.min.js');
                            scriptLoaded = true;
                        }
                        break;
                case 'jarvisFilterAdded':
                    let pageType = utilService.getPageData("pageType");
                    if (data && data.type && (utilService.getPageData("isSerp") === true) && ["SIMILAR_PROPERTY_URLS", "SIMILAR_PROPERTY_URLS_MAPS"].indexOf(pageType) === -1) {
                        let moduleName = filterModuleMap[data.type],
                            moduleId = "jarvis-" + data.type + "-filter",
                            moduleData = {
                                name: moduleName,
                                loadId: moduleId
                            },
                            element = document.getElementById(moduleId);
                        _startMixpanelEventDialogFadeoutTimer();
                        application.stop(element);
                        context.broadcast('loadModule', moduleData);
                        return;
                    }
                    _clearAnyMixpanelEventContent();
                    break;
                case 'triggerJarvisMixpanelEvent':
                    _triggerJarvisMixpanelEvent(data);
                    break;
                case 'multiSelectOptionsSelected':
                    pattern = new RegExp("jarvis-[a-zA-z_]*-filter");
                    if(pattern.test(data.id)) {
                        let name = filterNameMap[data.type];
                        context.broadcast('trackChatMultiSelectFilterInteraction', {
                            id: moduleEl.id,
                            label: data.value,
                            name,
                            'sourceModule': 'Filter'
                        });
                    }
                    break;  
                case 'rangeSliderSelected':
                    pattern = new RegExp("jarvis-[a-zA-z_]*-filter");
                        if(pattern.test(data.id)) {
                            context.broadcast('trackChatRangeFilterInteraction', {
                                id: moduleEl.id,
                                label: data.value,
                                name: data.type,
                                'sourceModule': 'Filter'
                            });
                    }
                    break;    
                case 'slider_clicked':
                    pattern = new RegExp("jarvis-[a-zA-z_]*-filter");
                    let eventName,
                        label;
                    if(pattern.test(data.element.id)) {
                        switch(data.event) {
                            case 'Click':
                               eventName = "trackChatGalleryClick";
                               let elem = $(data.element).find("li[data-index=" + data.dataset.index + "] a");
                               if(elem) {
                                 label = $(elem).attr("href");
                               }
                               break;
                            case 'Left':
                            case 'Right':
                               eventName = "trackChatGalleryInteraction";
                               label = data.event;
                               break; 
                        }
                        context.broadcast(eventName, {
                            label,
                            id: moduleEl.id,
                            'sourceModule': 'Similar Property'
                        });
                    }
                    break;      
                case 'jarvisFilterApplied':
                    let element = document.getElementById(data.id);
                    context.broadcast("trackChatFilterApplied", {
                        label: data.value,
                        id: moduleEl.id,
                        name: data.type,
                        'sourceModule': 'Filter'
                    });
                    _clearAnyMixpanelEventContent(element);
                    break;
                case 'jarvisPopupAdded':
                    _startMixpanelEventDialogFadeoutTimer();
                    break;
                case 'pageLoaded':
                    _clearAnyMixpanelEventContent();
                    break;
                case 'userLoggedIn':
                    data.name = data.firstName;
                    _emitToJarvisSocket('chat-user-login', data);
                    break;
                case 'jarvisEvents:push':
                    context.broadcast("mpAnalytics:push", data);
                    break;
                case 'jarvisEvents:pop':
                    break;
                case "jarvis:verifyContact":
                    _verifyContact(data);
                    // pushNotificationService.sendNotificationTags("CHAT_LEAD_FILLED");
                    break;
                case "jarvis:phoneVerified":
                    _savePhone(data);
                    // pushNotificationService.sendNotificationTags("CHAT_OTP_VERIFIED");
                    break;
                case "jarvis:userSendsMessage":
                    // pushNotificationService.sendNotificationTags("USER_MESSAGE_SENT");
                    break;
                case "jarvis:userReceivesMessage":
                    // pushNotificationService.sendNotificationTags("AGENT_MESSAGE_SENT");
                    break;
                case "open-m-chat":
                    let displayStatus = $(moduleEl).find("#inner-wrapper").css("display");
                    if(displayStatus == "none"){
                        $(moduleEl).find("#makaan-man").click();
                    } else {
                        //chat already opened
                    }
                    break;
            }
        };
        var onclick = function(event, element, elementType) {
            switch (elementType) {
                case 'open-basic-login':
                    loginService.openLoginPopup("basic");
                    break;
            }
        };
        var init = function() {
            moduleEl = context.getElement();
            $(moduleEl).hide();
            $(moduleEl).on('mouseenter mouseleave', '.mixpanel-dialog', function(event) {
                if (event.type === 'mouseenter') {
                    clearTimeout(timer);
                } else {
                    _startMixpanelEventDialogFadeoutTimer();
                }
            });
        };
        return {
            messages: ['interactiveEventTriggered','open-m-chat', 'jarvis:userReceivesMessage', 'jarvis:userSendsMessage', 'jarvis:phoneVerified', 'jarvis:verifyContact', 'jarvisFilterAdded', 'triggerJarvisMixpanelEvent', 'jarvisFilterApplied', 'jarvisPopupAdded', 'pageLoaded', 'userLoggedIn', "jarvisEvents:push", "jarvisEvents:pop","multiSelectOptionsSelected","slider_clicked","rangeSliderSelected"],
            behaviors: ['socialBehavior', 'jarvisTracking'],
            init,
            onclick,
            onmessage
        };

    });
});
