define([
    'common/trackingConfigService',
    'services/trackingService'
], function(t, trackingService) {
    'use strict';
    Box.Application.addBehavior('jarvisTracking', function(context) {

        var messages = [
            'trackChatRangeFilterInteraction',
            'trackChatGalleryClick',
            'trackChatGalleryInteraction',
            'trackChatFilterApplied',
            'trackChatCardAutoFade',
            'trackChatMultiSelectFilterInteraction'
        ];

        var element;

        var onmessage = function(eventName, data) {
            if(element.id !== data.id){
                return;
            }
            let category = t.JARVIS_CATEGORY,
                event,
                label,
                sourceModule,
                name,
                nonInteractive;

            switch (eventName) {
                case 'trackChatRangeFilterInteraction':
                    event = t.INTERACTION_EVENT;
                    label = data.label;
                    nonInteractive = 0;
                    sourceModule = data.sourceModule;
                    name = data.name;
                    break;

                case 'trackChatGalleryClick':
                    event = t.CLICK_EVENT;
                    label = data.label;
                    nonInteractive = 0;
                    sourceModule = data.sourceModule;
                    break;

                case 'trackChatGalleryInteraction':
                    event = t.INTERACTION_EVENT;
                    label = data.label;
                    nonInteractive = 0;
                    sourceModule = data.sourceModule;
                    break;

                case 'trackChatFilterApplied':
                    event = t.CLICK_EVENT;
                    label = data.label; 
                    nonInteractive = 0;
                    sourceModule = data.sourceModule;
                    name = data.name;
                    break;

                case 'trackChatCardAutoFade':
                    event = t.DISMISSED_EVENT;
                    label = t.JARVIS_AUTO_FADE_EVENT;
                    nonInteractive = 1;
                    sourceModule = data.sourceModule;
                    name = data.name;
                    break;

                case 'trackChatMultiSelectFilterInteraction':
                    event = t.INTERACTION_EVENT;
                    label = data.label;
                    sourceModule = data.sourceModule;
                    name = data.name;
                    nonInteractive = 0;
                    break;

            }

            let properties = {};
            properties[t.CATEGORY_KEY] = category;
            properties[t.LABEL_KEY] = label;
            properties[t.SOURCE_MODULE_KEY] = sourceModule;
            properties[t.NAME_KEY] = name;
            properties[t.NON_INTERACTION_KEY] = nonInteractive;

            trackingService.trackEvent(event, properties);
        };

        var init = function(){
            element = context.getElement();
        };

        return {
            messages: messages,
            onmessage: onmessage,
            init,
            destroy: function() {}
        };
    });
});
