'use strict';

define([
        "doT!modules/eligibilityCalculator/views/index",
        "common/utilFunctions",
        "services/commonService",
        "services/homeloanService",
    ],
    function(template, utilFunctions, commonService, HLS) {
        Box.Application.addModule("eligibilityCalculator", function(context) {
            var moduleEl, $moduleEl, defaultConfig, formData = {};
            const ERROR_MSG_CLEAR_TIME_DELAY = 4000;

            function _updateTargetText(name, value) {
                formData.existingEmi = value;
                value = utilFunctions.toCurrencyFormat(value);
                $moduleEl.find('.js-' + name + 'Text').val(value);
                _calculatedEligibilityBroadcast();
            }



            function _validRange(target, newVal) {
                switch (target) {
                    case 'emiSlider':
                        if (!_isValidRange(defaultConfig.rangeConfig.range, newVal)) {
                            _setTextError('emi');
                            return false;
                        } else {
                            _removeTextError('emi');
                            return true;
                        }
                        break;
                }
            }

            function _isValidRange(range, newVal) {
                var maxAllowed = range.max.value;
                var minAllowed = range.min.value;
                if (minAllowed <= newVal && newVal <= maxAllowed) {
                    return true;
                } else {
                    return false;
                }
            }

            function _setTextError(target) {
                $moduleEl.find('.js-' + target + 'Text').addClass('input-range-error');
            }

            function _removeTextError(target) {
                $moduleEl.find('.js-' + target + 'Text').removeClass('input-range-error');
            }

            function _calculatedEligibilityBroadcast() {
                formData.loanAmount = utilFunctions.calculateLoanEligibility(formData.grossEarning || 0, formData.existingEmi || 0);
                context.broadcast('eligibilityCalculated', {
                    name: 'eligibilityCalculated',
                    data: formData
                });
            }

            function clearMessage(tmpElement) {
                setTimeout(() => {
                    tmpElement.text('');
                }, ERROR_MSG_CLEAR_TIME_DELAY);
            }

            // Events
            var init = () => {
                defaultConfig = {
                    "rangeConfig": {
                        "id": "emiSlider",
                        "start": [0],
                        "doubleHandle": false,
                        "step": 10,
                        "tooltips": false,
                        "label": true,
                        "type": "emi",
                        "formatter": "price",
                        "range": {
                            "min": {
                                "value": 0
                            },
                            "max": {
                                "value": 99999999
                            }
                        }
                    },
                    "profession": {
                        "templateFromPromise": true,
                        "defaultValue": "Salaried"
                    },
                    "grossEarning": {
                        "label": "Gross Earnings (annual)",
                        "inputName": "emiel_gross_earning",
                        "value": "",
                        "type": "number",
                        "maxlength": 50
                    }
                };
                moduleEl = context.getElement();
                $moduleEl = $(moduleEl);
                moduleEl.innerHTML = template(defaultConfig);
                commonService.startAllModules(moduleEl);
                formData.profession = defaultConfig.profession.defaultValue;
                context.broadcast('moduleLoaded', {
                    name: 'eligibilityCalculatorLoaded'
                });
            };
            var messages = ['rangeSliderChanged', 'singleSelectDropdownChanged', 'InputBox:changed', 'showIncomeError'];
            var onmessage = (name, data) => {
                switch (name) {
                    case 'rangeSliderChanged':
                        if (data.id == 'emiSlider') {
                            let value = parseInt(data.value);
                            _updateTargetText('emi', value);
                            _removeTextError(data.name);
                        }
                        break;
                    case 'InputBox:changed':
                        switch (data.element.name) {
                            case 'emiel_gross_earning':
                                formData.grossEarning = +data.value;
                                _calculatedEligibilityBroadcast();
                                break;
                        }
                        break;
                    case 'singleSelectDropdownChanged':
                        if (data.name === "income") {
                            formData.profession = data.value;
                            _calculatedEligibilityBroadcast();
                        }
                        break;
                    case 'showIncomeError':
                        var tmpElement = $moduleEl.find("input[name='emiel_gross_earning']").closest('.js-input-placeholder').parent().find('.error-msg');
                        tmpElement.text('Valid gross-earnings is required');
                        clearMessage(tmpElement);
                        break;
                }
            };
            var onchange = (event, element, elementType) => {
                switch (elementType) {
                    case 'emiInput':
                        $(element).blur();
                        var dataAttr = $(element).data();
                        var newVal = parseInt(element.value.replace(/,/g, ''));
                        if (!_validRange(dataAttr.target, newVal)) {
                            return;
                        }
                        _updateTargetText('emi', newVal);
                        defaultConfig.rangeConfig.value = parseInt(newVal);
                        defaultConfig.rangeConfig.start = [parseInt(newVal)];
                        defaultConfig.rangeConfig = $.extend(true, {}, defaultConfig.rangeConfig);
                        defaultConfig.rangeConfig.sliderOption = $.extend(true, {}, defaultConfig.rangeConfig);
                        context.broadcast('reloadSlider', defaultConfig.rangeConfig);
                        break;
                }
            };
            var onkeyup = function(event, element, elementType) {
                switch (elementType) {
                    case 'input-box':
                        switch (element.name) {
                            case 'emiel_existing_emi':
                                var newVal = $moduleEl.find('.js-emi').val();
                                _updateTargetText('emi', newVal);
                                defaultConfig.rangeConfig.value = parseInt(newVal);
                                defaultConfig.rangeConfig.start = [parseInt(newVal)];
                                defaultConfig.rangeConfig = $.extend(true, {}, defaultConfig.rangeConfig);
                                defaultConfig.rangeConfig.sliderOption = $.extend(true, {}, defaultConfig.rangeConfig);
                                context.broadcast('reloadSlider', defaultConfig.rangeConfig);
                                break;
                            case 'emiel_gross_earning':
                                var newVal = $moduleEl.find("input[name='emiel_gross_earning']").val();
                                formData.grossEarning = newVal;
                                _calculatedEligibilityBroadcast();
                                break;
                        }
                };
            }
            var destroy = function() {};
            return {
                messages,
                init,
                onmessage,
                onchange,
                onkeyup,
                destroy
            };
        });
    });
