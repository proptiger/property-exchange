define([
    'services/trackingService',
    'common/trackingConfigService',
    'services/utils'
], function(trackingService, t, utils) {
    'use strict';
    Box.Application.addBehavior('sponsoredProjectAdTracking', function(context) {

        var moduleEl, moduleConfig;
        const messages = ['serpPageScrolled', 'propertyPageScrolled', 'rhs_sponsored_ad_clicked', 'sponsored_ad_clicked'];

        var init = function() {
            moduleEl = context.getElement();
            moduleConfig = context.getConfig();
        };
        const adsTracked = {
            property:{
                inline:[],
            },
            serp:{
                inline:[],
                rhs:[]
            }
        };

        var _trackEvent = function(event, {label, sourceModule} = {}) {
            var trackingObj = {};
            trackingObj[t.CATEGORY_KEY] = 'Sponsored Non-Seller';
            trackingObj[t.LABEL_KEY] = label;
            trackingObj[t.SOURCE_MODULE_KEY] = sourceModule;
            trackingService.trackEvent(event, trackingObj);
        }

        var onmessage = function(name, data) {
            let label,
                sourceModule;
            switch(name){
                case "serpPageScrolled":
                    if(moduleConfig.isRHS){ //case where behavior is associated to the RHS module of sponsored ads.
                        let viewPortElement = utils.attributeListInViewPort("#"+moduleConfig.moduleId+ " > .js-sponsored-ads-rhs", 'project');
                        viewPortElement.forEach(el => {
                            sourceModule = 'rhs';
                            label = el;
                            if(adsTracked['serp'][sourceModule].indexOf(label)==-1){
                                adsTracked['serp'][sourceModule].push(label);
                                _trackEvent("Card Seen", {sourceModule, label});
                            }
                        });
                    } else { // case where behavior is associated to the inline module of sponsored ads.
                        let viewPortElement = utils.attributeListInViewPort("#"+moduleConfig.moduleId+ " > .js-sponsored-ads", 'project');
                        viewPortElement.forEach(el => {
                            sourceModule = 'inline';
                            label = el;
                            if(adsTracked['serp'][sourceModule].indexOf(label)==-1){
                                adsTracked['serp'][sourceModule].push(label);
                                _trackEvent("Card Seen", {sourceModule, label});
                            }
                        });
                    }
                    break;
                case "propertyPageScrolled":
                    let viewPortElement = utils.attributeListInViewPort(".js-sponsored-ads", 'project');
                    viewPortElement.forEach(el => {
                        sourceModule = 'inline';
                        label = el;
                        if(adsTracked['property'][sourceModule].indexOf(label)==-1){
                                adsTracked['property'][sourceModule].push(label);
                                _trackEvent("Card Seen", {sourceModule, label});
                            }
                    });
                    break;
                // listens to the message getting broadcasted by its associated module.
                case "rhs_sponsored_ad_clicked":
                    if(data.moduleId != moduleEl.id) return;

                    label = data && data.adId;
                    sourceModule = 'rhs';
                    _trackEvent('Card Clicked', {label, sourceModule});
                    break;
                // listens to the message getting broadcasted by its associated module.
                case "sponsored_ad_clicked":
                    if(data.moduleId != moduleEl.id) return;

                    label = data && data.adId;
                    sourceModule = 'inline';
                    _trackEvent('Card Clicked', {label, sourceModule});
                    break;
            }
        };
        
        return {
            init,
            onclick,
            onmessage,
            messages,
            destroy: function() {}
        };
    });
});