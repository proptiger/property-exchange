/**
 * This modules is being used to show sponsored project Ads on experimental basis.
 * Currently this module is running on serp and property page.
 * In future it will get backend driven.
 *
 * JIRA: MAKAAN-3679
 *
 * Module config
 *      isMobile: <boolean> : true if the user is accessing the serp and property page on mobile.
 *      isRHS: <boolean> : true when the module gets initiated on the RHS of serp page.
 *      adsContainer: <string>: specifies the Data-attribute in which the sponsored projects ads template will get into.
 */

define([
    "doT!modules/sponsoredProjectAds/views/index",
    "doT!modules/sponsoredProjectAds/views/bannerOnly",
    "services/utils",
    'common/sharedConfig',
    'services/apiService',
    'services/promotedNonSellerService',
    'modules/sponsoredProjectAds/scripts/behaviors/sponsoredProjectAdTracking'
    ], function(template,bannerTemplate, utils, sharedConfig, apiService, promotedNonSellerService){
        "use strict";
    const MODULE_NAME = "sponsoredProjectAds";
    Box.Application.addModule(MODULE_NAME, (context) => {
        let moduleEl, $, $moduleEl, moduleConfig, pageData, data={};
        const MAXIMUM_PROJECTS_INLINE = 2;
        /* function to get the local data info from pageData and moduleConfig. */
        function getPageData(){
            pageData = utils.getPageData();
            data.id = moduleConfig.moduleId;
            data.isSerp = pageData.isSerp ? true : false;
            data.isRHS = moduleConfig.isRHS ? true : false;
            data.isNonSeller = moduleConfig.isNonSeller ? true : false;
            data.page = pageData.moduleName;
            data.listingType = pageData.listingType;
            data.cityId = pageData.cityId;
            data.localityOrSuburbIds = pageData.localityId && pageData.localityId.toString().split(",");
            data.title = data.isNonSeller ? 'Featured Ads' : 'Sponsored';
        }

        /* This handles the randomization logic on projects for mobile and serp (inline projects). */
        function handleRandomization(response){
            if(moduleConfig.isMobile && response.length){
                return [response[Math.floor(Math.random()*response.length)]];
            } else if(!data.isRHS && data.isSerp){
                response = utils.shuffleArray(response);
                return response.length >= MAXIMUM_PROJECTS_INLINE ? [response[0], response[1]] : response;
            } else {
                return response;
            }
            
        }

        function render(ads) {
            data.sponsoredAds = ads;
            if(data.sponsoredAds.length){
                if(data.sponsoredAds[0].bannerOnly){
                    $moduleEl.find(moduleConfig.adsContainer).html(bannerTemplate(data));
                }else{
                    $moduleEl.find(moduleConfig.adsContainer).html(template(data));
                }
            }
        }
        /* function to get list of sponsored projects which are filtered on the basis of page, saleType and city at Node. */
        function fetchSponsoredProjects(){
            return apiService.get(sharedConfig.apiHandlers.getSponsoredProjectAds(data).url).then(response => {
                render(handleRandomization(response));
            });
        }

        function init(){
            moduleEl = context.getElement();
            $ = context.getGlobal('jQuery');
            $moduleEl = $(moduleEl);
            moduleConfig = context.getConfig();
            getPageData();
            if(data.isNonSeller && data.isSerp) {
                render(promotedNonSellerService.getPromotedNonSellers(data));
            } else if(!data.isNonSeller){
                fetchSponsoredProjects();   
            }
        }

        function destroy(){
            moduleEl = $ = $moduleEl = moduleConfig = null;
        }

        function onclick(event, element, elementType){
            let id = ($(element).data()).id;
                
            switch(elementType){
                // this is the case when a user click on the RHS module of sponsored ads.
                case 'rhs_sponsored_ad_clicked':
                    context.broadcast("rhs_sponsored_ad_clicked", {isSerp: data.isSerp, adId: id, moduleId : moduleEl.id});
                    break;
                // this is the case when a user click on the inline module of sponsored ads.
                case 'sponsored_ad_clicked':
                    context.broadcast("sponsored_ad_clicked", {isSerp: data.isSerp, adId: id, moduleId : moduleEl.id});
                    break;
            }
        }

        var behaviors = ['sponsoredProjectAdTracking'];
        return {
            init,
            destroy,
            behaviors,
            onclick
        }
    });
});