define([
    "doT!modules/viewOnMap/views/index",
    'services/commonService',
    'services/experimentsService',
    'services/utils',
    "modules/viewOnMap/scripts/behaviors/viewOnMapTracking"
], function(template, commonService, experimentsService,UtilService) {
    "use strict";
    Box.Application.addModule("viewOnMap", function(context) {

        const ZOOM_LEVEL = 13;
        var moduleEl, leadData;
        function _openLeadForm() {
            return _closePopup(function(){context.broadcast('leadform:open', leadData);});
        }
        function _closePopup(cb) {
            context.broadcast('popup:close', {callback: cb});
        }
       
        // data should contain latitude, longitude, pagetype, title 
        var onmessage = function(name, data = {}) {
            if (name === 'viewOnMap') {
                var mapData = {
                    latitude: data.latitude,
                    longitude: data.longitude,
                    title: data.title,
                    pageType: data.pagetype,
                    zoom: ZOOM_LEVEL,
                    size:data.size,
                    price:data.price,
                    unit: data.priceUnit,
                    pricePerUnit:data.priceperunit,
                    sellerName: data.companyName,
                    sellerPic: data.companyImage,
                    sellertype: data.companyType,
                    avatarText: data.avatarText,
                    avatarTextColor: data.avatarTextColor,
                    avatarBGColor: data.avatarBGColor
                };
                leadData = data||{};
                if(UtilService.isMobileRequest()){
                  leadData.step="CALL_NOW";  
                }
                let htmlContent = template({
                    mapData: mapData
                });
                $(moduleEl).html(htmlContent);
                commonService.startAllModules(moduleEl);
                context.broadcast('popup:open', {
                    id: 'viewOnMapPopup'
                });
                experimentsService.experimentElementsClass(document.getElementsByClassName('vom-popup'), 'makaan_pdp_map', 'new', 'new-map-view');
            }
        };
        var onclick = function(event, element, elementType) {
            let clickElementData = $(element).data() || {};
            switch(elementType){
                case 'closePopup':
                    context.broadcast('trackCloseViewOnMap', {moduleId: moduleEl.id});
                    break;
                case 'map-connect-now':
                    context.broadcast("trackLeadOpen", clickElementData);
                    _openLeadForm();    
            }        
        };

        return {
            messages: ['viewOnMap'],
            init: function() {
                moduleEl = context.getElement();
                config = context.getConfig() || {};
            },
            destroy: function() {},
            onmessage,
            behaviors: ['viewOnMapTracking'],
            onclick
        };
    });
});
