define([
    'common/trackingConfigService',
    'services/trackingService',
    'services/utils'
], function(t, trackingService, utils) {
    'use strict';
    Box.Application.addBehavior('viewOnMapTracking', function(context) {

        var messages = [
            'trackCloseViewOnMap'
        ];

        var value,
            moduleId = context.getElement().id;

        var onmessage = function(msgName, data) {
            if (data.moduleId && data.moduleId != moduleId) {
                return;
            }

            let event = t.VIEW_ON_MAP,
                category = t.HEADLINE_CATEGORY,
                label,
                pageData = utils.getPageData() || {};

            switch (msgName) {
                case 'trackCloseViewOnMap':
                    label = t.CLOSE_LABEL;
                    break;
                default:
                    return;
            }

            let properties = {};
            properties[t.CATEGORY_KEY] = category;
            properties[t.LABEL_KEY] = label;
            properties[t.PROJECT_ID_KEY] = pageData.projectId;
            properties[t.PROJECT_STATUS_KEY] = t.makeValuesUniform(pageData.projectStatus,t.PROJECT_STATUS_NAME) ;
            properties[t.BUILDER_ID_KEY] = pageData.builderId;
            properties[t.BUDGET_KEY] = pageData.budget;
            properties[t.UNIT_TYPE_KEY] = t.makeValuesUniform(pageData.unitType,t.UNIT_TYPE_MAP) ;
            properties[t.BHK_KEY] = t.makeValuesUniform(pageData.bhk,t.BHK_MAP) ;
            properties[t.SELLER_ID_KEY] = pageData.sellerId;
            properties[t.SELLER_SCORE_KEY] = pageData.sellerScore;
            properties[t.LOCALITY_ID_KEY] = pageData.localityId;
            properties[t.SUBURB_ID_KEY] = pageData.suburbId;
            properties[t.CITY_ID_KEY] = pageData.cityId;
            properties[t.LISTING_ID_KEY] = pageData.listingId;
            properties[t.LISTING_SCORE_KEY] = pageData.listingScore;

            value = utils.getTrackingListingCategory(pageData.listingCategory);
            properties[t.LISTING_CATEGORY_KEY] = t.makeValuesUniform(value,t.LISTING_CATEGORY_MAP) ;

            trackingService.trackEvent(event, properties);
        };

        return {
            messages: messages,
            onmessage: onmessage,
            init: function() {},
            destroy: function() {}
        };
    });
});