"use strict";
define([
    'services/commonService',
    'services/utils'
], (commonService, utils) => {
    Box.Application.addModule('commutePoints', (context) => {
        var messages = ['openCommutePointsPopup', 'tabClicked'];
        var element, moduleId, $, $moduleEl, moduleConfig;

        function _loadSubmodules() {
            context.broadcast('loadModule', [{
                parent: 'mapsModule',
                name: 'customDirectionModule'
            }, {
                parent: 'mapsModule',
                name: 'mapSvgLegendsModule'
            }, {
                parent: 'mapsModule',
                name: 'communicatorModule'
            }]);
        }

        function render(data){
            context.broadcast('loadModule', {
                "name": 'mapsModule'
            });
            commonService.bindOnModuleLoad('mapsModule', function(){
                _loadSubmodules();
                context.broadcast('mapSetCenterAndZoom', { latitude: data.latitude, longitude: data.longitude, title: data.title });
                context.broadcast("popup:open",{id:"commutePointsPopup"});
                commonService.bindOnModuleLoad('customDirectionModule', function(){
                    commonService.bindOnModuleLoad('communicatorModule', function(){
                        context.broadcast('updateMarkerPosition', data);
                        context.broadcast('loadCustomDirectionModule');
                    });
                });
            });
        }

        function init() {
            //initialization of all local variables
            element = context.getElement();
            moduleId = element.id;
            $ = context.getGlobal('jQuery');
            moduleConfig = context.getConfig();
            $moduleEl = $(element);
            context.broadcast('moduleLoaded', {
                name: 'commutePoints',
                id: element.id
            });
        }

        function destroy() {
            //clear all the binding and objects
        }

        function onmessage(name, data) {
            // bind custom messages/events
            switch(name){
                case 'openCommutePointsPopup':
                    if(data.id!=moduleId){
                        return;
                    }
                    render(data);
                    break;
            }
        }

        function onclick(event, element, elementType) {
            switch(elementType){
                case 'closePopup':
                    let ele = document.getElementById('commute-map-section');
                    commonService.stopAllModules(ele);
                    commonService.stopModule(ele);
            }
        }

        return {
            init,
            messages,
            onmessage,
            onclick,
            destroy
        };
    });
});
