"use strict";
define([
    'services/utils',
    'modules/breadcrumb/scripts/behaviors/tracking'
], function() {
    Box.Application.addModule("breadcrumb", function(context) {
        const NAVIGATE_LINK = 'navigate-link',
            BREADCRUM = '.breadcrumb-seo',
            WRAP = 'down-wrap',
            SNIPPET = 'featured-snippet';

        function onclick(event, element, elementType) {
            switch (elementType) {
                case NAVIGATE_LINK:
                    context.broadcast('breadcrumb_navigation_click', element.dataset);
                    /* falls through */
                case WRAP:
                    $(BREADCRUM).addClass('bc-mobile');
                    break;
                case SNIPPET:
                    context.broadcast('popup:open', {id: 'featuredSnippet'});
                    console.warn("WOW");
                    break;
            }
        }
        return {
            init: function() {},
            destroy: function() {},
            onclick,
            behaviors: ['breadcrumbTracking'],
            messages: []
        };
    });
});
