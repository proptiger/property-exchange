'use strict';
define([
    'common/trackingConfigService',
    'services/trackingService'
], (t, trackingService) => {
    Box.Application.addBehavior("breadcrumbTracking", () => {

        function onmessage(name, data) {
            let event, category, label, linkName, linkType;

            switch (name) {
                case 'breadcrumb_navigation_click':
                    label = data.trackLabel;
                    category = t.BREADCRUMB_CATEGORY;
                    event = t.CLICK_EVENT;
                    linkName = data.linkName;
                    linkType = data.linkType;
            }

            let properties = {};
            properties[t.CATEGORY_KEY] = category;
            properties[t.LABEL_KEY] = label;
            properties[t.LINK_NAME_KEY] = linkName;
            properties[t.LINK_TYPE_KEY] = linkType;

            trackingService.trackEvent(event, properties);
        }
        return {
            init: function() {},
            onclick: function() {},
            destroy: function() {},
            messages: ['breadcrumb_navigation_click'],
            onmessage
        };
    });
});
