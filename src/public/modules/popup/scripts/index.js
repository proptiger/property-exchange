/**
 * @name: popup.js
 * @description: Library to open/close popup/modal
 * @author: Vishal Kumar
 *
 * A t3 module to open/close popups
 *
 * To open a popup, you have to have an element with attribute data-role="popup"
 * inside <layout-put into="popups"></layout-put> in your marko file
 *
 * e.g. a popup with html <div data-role="popup" id="abc">...</div>
 * To open this popup, trigger a message
 * context.broadcast('popup:open', {id: 'abc'})
 *
 * To Close currently opened popup,
 * context.broadcast('popup:close')
 *
 * Messages:
 * popup:change: triggers whenever a popup is opened/closed with data as popup DOM element
 */

"use strict";

define(["modules/popup/scripts/di/css.event",
    "services/utils",
    "services/commonService"
], function(CssEvent, Utils, CommonService) {
    Box.Application.addModule('popup', function(context) {
        var config, moduleEl, $, $moduleEl, $popupContainer, $commonPopupContainer, $pagePopupContainer;
        const POPUP_CHANGE = 'popup:change';

        /**
         * flag to check if the hash state is changed by it's own
         * (It can also be changed by browser back button action)
         * @type {Boolean}
         */
        var selfStateChange = false;

        /**
         * stores callback for current popup hide event
         * @type {Function}
         */
        var callbackAfterHashChange;

        /**
         * Mantains a stack of currently opened popup selectors
         * @type {Array}
         */
        var popupStack = [];

        var pageSelector = '#content';
        var footerSelector = '#seofooter-wrap';
        var popupCommonSelector = '[data-role="popup"]';
        var pageHideClassName = "hidden-xs";
        var inputIsFocused = false;

        /**
         * returns the top element in the stack
         * @return {String} top popup selector
         */
        popupStack.top = function() {
            if (this.length) {
                return this[this.length - 1];
            }
        };

        /**
         * Clear the stack
         */
        popupStack.empty = function() {
            this.length = 0;
        };

        /**
         * returns the camel case string
         * @return {String} in camelCase format
         */
        function camelCase(str) {
            return str
                .replace(/\s(.)/g, function($1) {
                    return $1.toUpperCase();
                })
                .replace(/\s/g, '')
                .replace(/^(.)/, function($1) {
                    return $1.toLowerCase();
                });
        }
        /**
         * Opens popup above the page/popup
         * @param  {String}   popupSelector Class or ID of popup selector
         * @param  {Function} callback      Function to be called after popup opens
         */
        function openPopup(popupSelector, options) {
            var hashFinal;
            var previousHash = window.location.hash.substr(1);
            var hashJoin = previousHash ? '&' : '';

            options = options || {};

            var config = $.extend({
                modal: false,
                escKey: true
            }, options.config);

            var $popup = $(popupSelector + popupCommonSelector);
            if (!$popup.length || popupStack.top() === popupSelector) {
                return;
            }

            hashFinal = previousHash + hashJoin + camelCase(popupSelector.replace(/#|\.|\s/g, ''));
            selfStateChange = true;

            window.location.hash = hashFinal;

            $popup.removeClass('hide');
            _nextAnimationFrame(function() {
                $popup.addClass('show-popup');
                $popup.scrollTop(0);
            }, 0); // move popup to top

            if (!popupStack.top()) {
                $popupContainer.removeClass("hide");
            } else {
                // if (!ignoreHideTopPopup) {
                $(popupStack.top().popupSelector + popupCommonSelector).addClass('hide');
                // }
            }

            showPopupContainer(popupSelector);

            // dispatch popupopen event
            //context.broadcast(POPUP_CHANGE, {el: $(popupSelector)[0]});
            context.broadcast(POPUP_CHANGE, {el: document.getElementById(popupSelector)});

            popupStack.push({
                popupSelector,
                config
            });

            if (typeof options.callback === "function") {
                options.callback();
            }

            CommonService.popupOpened();
        }

        /**
         * Hides the currently opened popup and shows the last popup opened or
         * the page
         * @return {Boolean} returns true if there were any popup to hide otherwise false
         */
        function hidePopupAndBack(callback) {
            callbackAfterHashChange = callback;

            if (popupStack.top()) {
                hidePopup();
                selfStateChange = true;

                // remove hash from url (simply go back in window.History)
                window.history.back();

                return true;
            }

            return false;
        }

        function onModuleLoad() {
            $(window)
                .on('hashchange', hideOnBrowserBack)
                .on('pronto.cssLoaded', closeAllPopups);

            $(window).resize(function(){ // On resize
                if (!inputIsFocused) {
                    setPopupContainerHeight();
                }
            });

            initPopup();
        }

        function setPopupContainerHeight() {
            $('#popup-container').height($(window).height());
        }

        function hideOnBrowserBack() {
            // ignore if changed by this popup library
            if (selfStateChange) {
                selfStateChange = false;

                return;
            }

            hidePopup();
        }

        /**
         * Hides currently opened popup if any
         */
        function hidePopup() {
            if (!popupStack.top()) {
                return;
            }
            var currentPopupSelector = popupStack.pop().popupSelector;
            var $el = $(currentPopupSelector + popupCommonSelector);

            $el.removeClass('show-popup');

            CssEvent.onTransitionEnd($el[0], () => {
                //Make sure the menu is open before setting the overflow.
                //This is to accout for fast clicks
                $el.addClass('hide');
                if (popupStack.top()) {
                    var newPopupSelector = popupStack.top().popupSelector + popupCommonSelector;
                    $(newPopupSelector).removeClass('hide');
                    showPopupContainer(newPopupSelector);

                    // dispatch event
                    context.broadcast(POPUP_CHANGE, {el: $el[0]});
                } else {
                    $(pageSelector).eq(0).removeClass("hide").removeClass("hidden-xs");

                    $(footerSelector).eq(0).removeClass("hide").removeClass("hidden-xs");
                    $popupContainer.addClass("hide");

                    $('body').removeClass('popup-open');

                    // dispatch event
                    context.broadcast(POPUP_CHANGE, {el: $el[0]});
                }

                if (typeof callbackAfterHashChange === "function") {
                    _nextAnimationFrame(callbackAfterHashChange);
                    callbackAfterHashChange = null;
                }

                CommonService.popupClosed();
            });
            // _nextAnimationFrame(function() {
            //     // $(currentPopupSelector + popupCommonSelector).addClass('hide');
            // });
        }

        function closeAllPopups() {
            var popupDetails;
            $popupContainer.addClass("hide");
            $('body').removeClass('popup-open');
            CommonService.resetPopupCount();
            if(!popupStack || !popupStack.length) {
                return;
            }
            while (popupDetails = popupStack.pop()) { // jshint ignore:line
                $(popupDetails.popupSelector + popupCommonSelector).addClass('hide');
            }
        }

        function showPopupContainer(popupSelector) {
            // hide page-popup container if popup belongs to common-popup container
            if ($commonPopupContainer.find(popupSelector).length) {
                $pagePopupContainer.addClass('hide');
                $commonPopupContainer.removeClass('hide');
            } else {
                $pagePopupContainer.removeClass('hide');
                $commonPopupContainer.addClass('hide');
            }

            $('body').addClass('popup-open');
        }

        /**
         * initialize listening common events for popups
         */
        function initPopup() {
            $popupContainer.on("click", ".popup-overlay", function() {
                hidePopupIfNotModal();
                return false;
            })
            .on('focus', 'input', function() {
                inputIsFocused = true;
            })
            .on('blur', 'input', function() {
                inputIsFocused = false;
            });

            // close popup on pressing esc key
            $(document).on('keydown', _hidePopupOnESCKey);
        }

        function _hidePopupOnESCKey(e) {
            let popupDetails = popupStack.top();

            if (popupDetails && e.which == 27) {
                hidePopupIfNotModal();
            }
        }

        function _nextAnimationFrame(func) {
            if (window.requestAnimationFrame) {
                return window.requestAnimationFrame(func);
            }
            return setTimeout(func, 16);
        }

        function hidePopupIfNotModal() {
            var popupDetails = popupStack.top();

            if (popupDetails) {
                var config = popupDetails.config;

                if (config.modal !== true) {
                    hidePopupAndBack();
                }
            }

        }

        /*
            Function to create selectors HTMLs
        */
        // function getSelectorMap(selectorsArray){
        //     var selectors = [];
        //     $.each(selectorsArray, function(index){
        //         selectors.push({"id": selectorsArray[index].slice(1), "template": $(selectorsArray[index]).html()});
        //     });
        //     return selectors;
        // }

        return {
            init: function() {
                moduleEl = context.getElement();
                $ = context.getGlobal('jQuery');
                $moduleEl = $(moduleEl);
                $popupContainer = $moduleEl;
                $commonPopupContainer = $moduleEl.find("#common-popup-container");
                $pagePopupContainer = $moduleEl.find('#page-popup-container');

                setPopupContainerHeight();

                onModuleLoad();
                context.broadcast('moduleLoaded', {
                    name: "popup",
                    id: moduleEl.id
                });
            },
            destroy: function() {
                // unbind events on document
                $(document).off('keydown', _hidePopupOnESCKey);

                config = null;
                moduleEl = null;
                $ = null;
                $moduleEl = null;
                $popupContainer = null;
                $commonPopupContainer = null;
                $pagePopupContainer = null;
                selfStateChange = null;
                callbackAfterHashChange = null;
                popupStack = null;
                pageSelector = null;
                footerSelector = null;
                popupCommonSelector = null;
                pageHideClassName = null;
            },
            /**
             * Handles the click event
             * @param {Event} event The event object
             * @param {HTMLElement} element The nearest element that contains a data-type attribute
             * @param {string} elementType The data-type attribute of the element
             * @returns {void}
             */
            onclick: function(event, element, elementType) {
                context.broadcast('elementClicked', {
                    element: element,
                    elementType: elementType
                });
                switch (elementType) {
                    case 'closePopup':
                        let popupId = $(event.target).closest('[data-role="popup"]').attr('id');
                        hidePopupAndBack();
                        context.broadcast('trackClosePopup', {moduleEl: element});
                        context.broadcast("popup:closed",{moduleEl: element,popupId});     //line included for get required functionality in report error link on property page

                        break;
                    case 'popupOverlay':
                        hidePopupIfNotModal();
                        // context.broadcast('trackClosePopup', {moduleEl: element});
                        break;
                    default:
                        break;
                }
            },
            /*
                List of messages module will listen for
            */
            messages: ["popup:open", "popup:close", "popup:destroy"],
            /*
                Handles the messages action
                @param {name}, name of message
                @param {data} , data for message
                return
            */
            onmessage: function(name, data) {

                switch (name) {
                    case 'popup:open':
                        let popupSelector = '#' + data.id;
                        openPopup(popupSelector, data.options);
                        context.broadcast("popup:opened",{id:data.id});
                        break;
                    case 'popup:close':
                        hidePopupAndBack(data && data.callback);
                        context.broadcast("popup:closed",{isLeadPopup: data && data.isLeadPopup || false});     //line included for get required functionality in report error link on property page
                        break;
                    case "onHidePopup":
                        context.broadcast("un_load_module", {
                            id: data.moduleId
                        });

                }
            }

        };

    });
});
