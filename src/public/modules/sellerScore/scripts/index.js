
define([
    'doT!modules/sellerScore/views/index',
    'doT!modules/sellerScore/views/scoreBreakup',
    'services/apiService',
    'common/sharedConfig',
    'services/utils'
], function(template, breakupTemplate, apiService, sharedConfig, utils) {
    "use strict";
    Box.Application.addModule('sellerScore', function(context) {

        var moduleElem, $, config, instance, userScoreData = {},
            apiPromises = {};

        const HIDECLASS = 'hide',
              SCORE_TOOLTIP = '.js-score-tooltip',
              SCORE_BREAKUP_CONTAINER = "[data-breakup-container]";

        let messages = ['reloadSellerScore'];

        function onmessage(name, data){
            if(data.id != moduleElem.id){
                return;
            }
            switch (name) {
                case 'reloadSellerScore':
                    render(data);
                    break;
                default:
            }
        }

        function setCacheData(listingUserId, res){
            let sellerScore = res.data && res.data.score || [];
            userScoreData[`${listingUserId}`] = sellerScore
        }

        function loaderHandler(show = true){
            let loaderElem = config.loaderSelector && $(moduleElem).find(config.loaderSelector);
            if(!loaderElem){
                return;
            }
            if(show){
                loaderElem.removeClass(HIDECLASS);
            }else{
                loaderElem.addClass(HIDECLASS);
            }
        }

        function renderScoreComponent(data){
            let breakupContainer = instance.find(SCORE_BREAKUP_CONTAINER);
            let html = breakupTemplate({
                scoreDetails: userScoreData[`${data.listingUserId}`],
                companyRating: data.ratingCount,
                isSellerProfile: data.isSellerProfile ? true : false
            });
            loaderHandler(false);
            breakupContainer.html(html);
        }

        function render(data){
            if(data && data.listingUserId){
                instance = $(moduleElem);

                let html, scoreDetails,
                    placeHolderSelector = config.placeHolderSelector && instance.find(config.placeHolderSelector);

                if(placeHolderSelector){
                    instance = placeHolderSelector;
                }

                instance.html('');

                data.companyRating = utils.getReadablePriceInWord(data.companyRating, 1, false, true);
                data.companyRating = data.companyRating && parseFloat(data.companyRating).toFixed(1);
                let scoreClass = utils.getRatingBadgeClass(data.companyRating);

                instance.html(template({
                    companyRating: data.companyRating > 0 ? data.companyRating : 0,
                    isSellerProfile: data.isSellerProfile ? true : false,
                    scoreClass
                }));

                loaderHandler(true);

                if(!data.companyRating || data.companyRating < 0){
                    loaderHandler(false);
                    return;
                }

                if(userScoreData[`${data.listingUserId}`]){
                    renderScoreComponent(data);
                }else {
                    let url = sharedConfig.apiHandlers.sellerScoreBreakup(data.listingUserId).url;
                    if(!apiPromises[url]){
                        apiPromises[url] = apiService.get(url);
                    }

                    apiPromises[url].then((res)=>{
                        setCacheData(data.listingUserId, res);
                        renderScoreComponent(data);
                    }, ()=>{
                        loaderHandler(false);
                    }).always(()=>{
                        apiPromises[url] = null;
                    });
                }
            }
        }

        return {
            init: function() {
                moduleElem = context.getElement();
                $ = context.getGlobal('jQuery');
                config = context.getConfig() || {};
                context.broadcast('moduleLoaded', {
                    'name': 'sellerScore',
                    'id': moduleElem.id
                });
            },
            onclick: function(event, element, elementType){
                switch(elementType){
                    case "show-score-tip":
                        $(moduleElem).find(SCORE_TOOLTIP).toggleClass(HIDECLASS);
                        break;
                }
            },
            messages,
            onmessage,
            destroy: function() {
                $ = null;
                moduleElem = null;
            }
        };
    });
});
