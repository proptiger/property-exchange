"use strict";
define([
    'doT!modules/priceTrends/views/topLocality',
    'common/sharedConfig',
    'services/loggerService',
    'services/commonService',
    'services/apiService',
    'services/utils',
    'services/urlService',
    'services/defaultService'
], (topLocalityTemplate,sharedConfig) => {
    Box.Application.addModule('priceTrends', (context) => {
        //const Logger = context.getService('Logger'),
           const  CommonService = context.getService('CommonService'),
            apiService = context.getService('ApiService'),
            //utils = context.getService('Utils'),
            //urlService = context.getService('URLService'),
            defaultService= context.getService('DefaultService');
            ///query = Box.DOM.query,
            //queryAll = Box.DOM.queryAll;
        var moduleEl, $moduleEl,
            application = Box.Application,
            messages = ['renderChart','singleSelectDropdownChanged'],
            behaviors = [],
            chartsRendered={},
            config,
            activeChartData,
            currentCityId;

        function _resetCharts() {
            chartsRendered = {};
            _initChart(activeChartData);
        }
        function _resetAll(cityId,cityName){
            _resetCity(cityId,cityName);
            _resetCharts();
            _renderTopLocalities(cityId,cityName);
        }
        function _resetCity(cityId,cityName){
            if(cityId){
                currentCityId = cityId;    
                $moduleEl.find('.js-titleCity').html(' in ' + cityName);
            }
            $moduleEl.find('.popular-cities [data-type=city]').removeClass('selected');
            $moduleEl.find('.popular-cities [data-type=city][data-cityid='+cityId+']').addClass('selected');
        }
        var _renderChart = function(data) {
            var id = data.id;
            let duration = data.meta.duration,
                xPlotBand,
                __seriesData,
                dataPointMaxIndex = 0,
                series = [];
            var url = sharedConfig.apiHandlers.getCityPriceTrend({
                cityId: currentCityId,
                query: {
                    duration: duration,
                    compare: 'topLocality'
                }
            }).url;
            apiService.get(url).then(function(response) {
                if (response) {
                    var __city, i, j, result = [],
                        key, value,
                        __month, sortedMonth, dataPointMaxCount = 0,
                        currentCount;
                    for (i = 0; i < Object.keys(response).length; i++) {
                        __city = Object.keys(response)[i];
                        var __subObj = response[__city];
                        sortedMonth = Object.keys(__subObj).sort();
                        result = [];
                        currentCount = 0;
                        for (j = 0; j < sortedMonth.length; j++) {
                            key = sortedMonth[j];
                            __month = parseInt(key, 10);
                            value = Math.round(__subObj[key]);
                            if (value) {
                                result.push([__month, value]);
                            }
                            currentCount++;
                            if (currentCount >= dataPointMaxCount) {
                                dataPointMaxCount = currentCount;
                                dataPointMaxIndex = i;
                            }
                        }
                        if (result.length) {
                            series.push({
                                name: __city,
                                data: result
                            });
                        }
                    }
                }
                __seriesData = series[dataPointMaxIndex];
                if (__seriesData && __seriesData.data && Object.keys(__seriesData.data).length >= 2) {

                    let xAxisNegativeThreshold = 0;
                    if (duration == 12) {
                        xAxisNegativeThreshold = 24 * 3600 * 1000 * 2 * 50;
                    } else if (duration == 36) {
                        xAxisNegativeThreshold = 24 * 3600 * 1000 * 3 * 50;
                    } else if (duration == 60) {
                        xAxisNegativeThreshold = 24 * 3600 * 1000 * 6 * 50;
                    }
                    let xAxisLength = Object.keys(__seriesData.data).length;
                    xPlotBand = {
                        color: '#D4E8F6',
                        from: __seriesData.data[(xAxisLength - 2)][0] - xAxisNegativeThreshold,
                        to: __seriesData.data[(xAxisLength - 1)][0] + 24 * 3600 * 1000 * 0.5 * 50
                    };
                }
                context.broadcast('chartDataLoaded', {
                    series: series,
                    id: id,
                    xPlotBand: xPlotBand
                });
                chartsRendered[id]=true;
            }, () => {
                context.broadcast('chartDataLoaded', {
                    series: [],
                    id: id
                });
            });
        };

        function _initChart(data) {
            let moduleId = data.id,
                chartElem = document.getElementById(moduleId);
            if (!application.isStarted(chartElem)) {
                context.broadcast('loadModule', {
                    name: 'chart',
                    loadId: moduleId
                });
            }
            if(!chartsRendered[data.id]){
                _renderChart(data);
            }
        }

        function _showChart(data) {
            $moduleEl.find('.js-priceTrend-chart').addClass('hide');
            $moduleEl.find('#' + data.id ).removeClass('hide');
            activeChartData = data;
        }

        
        function _renderTopLocalities(cityId,cityName){
            apiService.get(sharedConfig.apiHandlers.getCityTopLocalities({cityId:cityId}).url).then(response => {
                $moduleEl.find('.js-topLocalities').html(topLocalityTemplate({
                    topLocalities:response,
                    cityName: cityName
                }));
            },() => {
                $moduleEl.find('.js-error').removeClass('hide');
            });
        }
        function render(resp){
            if(resp.id){
                currentCityId = resp.id;
                _resetCity(resp.id, resp.userCity);
                _renderTopLocalities(resp.id, resp.userCity);
                // As soon as module gets loaded , trigger default action
                CommonService.bindOnModuleLoad('chart', function (id) {
                    if (id === "price-trend-chart-tweleve-months") {
                        var data,meta = {};
                        meta.duration = 12;
                        data = {
                            id: "price-trend-chart-tweleve-months",
                            meta: meta
                        };
                        _initChart(data);
                        activeChartData = data;
                    }
                }, ["price-trend-chart-tweleve-months"]);
            }
        }
        function init() {
            //initialization of all local variables
            moduleEl = context.getElement();
            $moduleEl = $(moduleEl);
            config =context.getConfig();
            defaultService.getCityByLocation().then(function(resp) {
                render(resp);
            }, function(err) {
                console.log(err);
                //Hard city Delhi for price trend if error to read user city
                render({
                    id:6,
                    userCity: 'Delhi',
                    label:'Delhi'
                });
            });
            CommonService.bindOnLoadPromise().then(()=>{
                context.broadcast('loadModule', {
                    name: 'chart',
                    loadId: 'price-trend-chart-tweleve-months'
                });
            });
        }

        function destroy() {
            //clear all the binding and objects
        }

         function onmessage(name, data) {
            switch (name) {
                case 'renderChart':
                    _showChart(data);
                    _initChart(data);
                    break;
                case 'singleSelectDropdownChanged':
                    if(data && data.id=='cityList_priceTrend' && data.value){
                        _resetAll(data.value,data.label);
                    }
                break;
            }
        }

        function onclick(event, element, elementType) {
            let dataset = $(element).data();
            // bind custom messages/events
            switch (elementType) {
                case 'city':
                    _resetAll(dataset.cityid,dataset.cityname);
                break;
            }
        }

        return {
            init,
            messages,
            behaviors,
            onmessage,
            onclick,
            destroy
        };
    });
});
