'use strict';
define([
        "doT!modules/singleSelectDropdown/views/singleSelectDropdown",
        "services/filterConfigService",
        "services/urlService",
        "services/utils"
    ],
    function(dropdownTemplate) {
        Box.Application.addModule("singleSelectDropdown", function(context) {

            const FilterConfigService = context.getService('FilterConfigService'),
                //URLService = context.getService('URLService'),
                UtilService = context.getService('Utils');
            const config = context.getGlobal('config');
            const moduleElement = context.getElement();
            const DROPDOWN_WRAPPER = 'single-select-dropdown',
                DROPDOWN_HEAD = 'single-select-dropdown-head',
                DROPDOWN_OPTION = 'single-select-dropdown-option',
                DROPDOWN_PLACEHOLDER = 'single-select-dropdown-placeholder',
                OVERLAY_SEC = 'overlay-sec';
            const HIDE = 'hidden';
            var pageData = UtilService.getPageData();
            var dropdownElement, menuOptions, placeholder, overlay, dropdownHead;

            var _addClass = function(reference) {
                if (!reference) {
                    return;
                }
                $(reference).addClass(config.focusClass);
                $(reference).addClass(config.openClass);
            };
            var _removeClass = function(reference) {
                if (!reference) {
                    return;
                }
                $(reference).removeClass(config.focusClass);
                $(reference).removeClass(config.openClass);

                if ($(reference).hasClass(config.activeClass)) {
                    $(reference).addClass(config.focusClass);
                }
            };
            var _toggleClass = function() {
                let reference = Box.DOM.query(moduleElement, "[data-type='" + DROPDOWN_WRAPPER + "']");
                if (!reference) {
                    return;
                }

                if (!$(reference).hasClass(config.openClass)) {
                    _addClass(reference);
                } else {
                    _removeClass(reference);
                }

            };
            var _getSelectedElement = function() {
                return $(moduleElement).find("li.active")[0];
            };

            var _updateDropDownValue = function(element) {
                if ($(element).hasClass(config.activeClass)) {
                    return;
                }
                $(overlay).addClass(HIDE);
                let data = $(element).data() || {};
                if (!dropdownElement || !menuOptions || !placeholder) {
                    return;
                }
                $(menuOptions).removeClass(config.activeClass);
                $(element).addClass(config.activeClass);
                $(dropdownElement).addClass(config.activeClass);
                _removeClass(dropdownElement);
                $(placeholder).html($(element).html());
                $(placeholder).attr("title", $(element).html());

                let eventData = UtilService.deepCopy(data);
                eventData.morefilter = $(moduleElement).data('morefilter');
                eventData.id = moduleElement.id;
                eventData.label = $(element).html();
                context.broadcast('singleSelectDropdownChanged', eventData);
            };
            var resetValue = function(){
                $(moduleElement).find('.js-selectboxWrapper').removeClass('active focus');
            };

            return {
                messages: ['userClicked', 'changeDropdownValue','updateSingleDropdownByValue','SetDropdownList'],

                /**
                 * Initializes the module and caches the module element
                 * @returns {void}
                 */
                init: function() {
                    const moduleConfig = context.getConfig() || {};
                    let templateFromPromise = (moduleConfig && moduleConfig.templateFromPromise) || false;
                    let defaultValue = (moduleConfig && moduleConfig.defaultValue && moduleConfig.defaultValue.toString()) || false;
                    FilterConfigService.renderFilter(moduleElement, pageData, dropdownTemplate, templateFromPromise, defaultValue, moduleConfig).then(function() {
                        dropdownElement = Box.DOM.query(moduleElement, "[data-type='" + DROPDOWN_WRAPPER + "']");
                        menuOptions = Box.DOM.queryAll(dropdownElement, "[data-type='" + DROPDOWN_OPTION + "']");
                        placeholder = Box.DOM.query(dropdownElement, "[data-target='" + DROPDOWN_PLACEHOLDER + "']");
                        overlay = Box.DOM.queryData(moduleElement, 'type', OVERLAY_SEC);
                        var selectedElement = _getSelectedElement();
                        if (selectedElement) {
                            let eventData = UtilService.deepCopy($(selectedElement).data());
                            eventData.morefilter = $(moduleElement).data('morefilter');
                            eventData.id = moduleElement.id;
                            context.broadcast('singleSelectDropdownInitiated', eventData);
                        } else {
                            context.broadcast('singleSelectDropdownInitiated', {
                                id: moduleElement.id,
                                name:moduleConfig.name||''
                            });
                        }
                        context.broadcast('moduleLoaded', {
                            name: 'singleSelectDropdown',
                            id: moduleElement.id
                        });

                    });
                },

                onclick: function(event, element, elementType) {

                    switch (elementType) {

                        case DROPDOWN_HEAD:
                            dropdownHead = element;
                            _toggleClass();
                            $(overlay).removeClass(HIDE);
                            context.broadcast('singleSelectDropdownHeadClicked',{
                                id: moduleElement.id
                            });
                            break;

                        case OVERLAY_SEC:
                            _toggleClass();
                            $(overlay).addClass(HIDE);
                            break;

                        case DROPDOWN_OPTION:
                            _updateDropDownValue(element);
                            break;

                        default:
                            _removeClass(dropdownElement);
                    }
                },

                onmessage: function(name, data) {
                    switch (name) {
                        case 'userClicked':
                            if (data.element != dropdownHead) {
                                _removeClass(dropdownElement);
                            }
                            break;
                        case 'changeDropdownValue':
                            if (data.id && data.element && data.id === moduleElement.id) {
                                _updateDropDownValue(data.element);
                            }

                            break;
                        case 'updateSingleDropdownByValue':
                            if (data.id === moduleElement.id && data.value) {
                                _updateDropDownValue($(moduleElement).find('[data-value=' + data.value + ']'));
                            }                        
                        break;
                        case 'SetDropdownList':
                            if (data.id === moduleElement.id) {
                                 if (!data.list) {
                                    return;
                                }
                                 var liList = "";
                                 var list=data.list;
                                 for (var i = 0; i < list.length; i++){
                                     liList += '<li class="js-selected" data-type="single-select-dropdown-option" data-value="'+list[i].value+'" data-name="'+data.liName + '" data-params= " '+ encodeURI(JSON.stringify(list[i]))+ '">'+list[i].label+'</li>'
                                 }
                                 if (liList.length > 0) {
                                     $(moduleElement).find(".js-ulList").html(liList);
                                     menuOptions = Box.DOM.queryAll(dropdownElement, "[data-type='" + DROPDOWN_OPTION + "']");
                                 }
                                 resetValue();
                            }
                         break;
                    }
                },

                /**
                 * Destroys the module and clears references
                 * @returns {void}
                 */
                destroy: function() {
                    dropdownElement = null;
                    menuOptions = null;
                    placeholder = null;
                }
            };
        });
    }
);
