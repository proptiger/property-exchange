/**
 * @fileoverview Module for the neighborhood in mobile view
 * @author [Aditya Jha]
 */

/*global Box*/
define([
    'doT!modules/mobileNeighborhood/views/index',
    'services/nearByAmenityService'
], (template) => {
    "use strict";
    Box.Application.addModule('mobileNeighborhood', (context) => {

        var doT, $, mobileNeighborhood, nearByAmenityService, position,
            amenities, amenitiesData, selected;

        var _getSelectedData = function(data) {
            let listData = [];
            data.sort(function(a, b) {
                if (a.geoDistance <= b.geoDistance) {
                    return -1;
                } else {
                    return 1;
                }
            });
            for (let i = 0; i < data.length && i < 5; i++) {
                let temp = {
                    name: data[i].name,
                    distance: data[i].geoDistance
                };
                temp.distance = parseFloat(temp.distance).toFixed(2);
                temp.distance = temp.distance.toString() + ' km';
                listData.push(temp);
            }
            return listData;
        };

        var _addModuleContainer = function() {
            var htmlContent = template({
                selected: amenities[selected],
                selectedIndex : selected,
                amenities: amenities,
                selectedData: _getSelectedData(amenitiesData[amenities[selected].name])
            });

            $(mobileNeighborhood).html(htmlContent);
        };

        var onmessage = function(name, data) {
            if (name === 'nearByAmenityDataRecieved') {
                amenitiesData = data.data;
                for (let i = 0; i < amenities.length; i++) {
                    if (amenitiesData.hasOwnProperty(amenities[i].name)) {
                        amenities[i].count = amenitiesData[amenities[i].name].length;
                        if (!selected && amenities[i].count) {
                            selected = i;
                        }
                    }
                }
                _addModuleContainer();
            }
        };

        var onclick = function(event, element, elementType) {
            switch (elementType) {
                case 'amenity-type':
                    let name = $(element).data('name');
                    for (let i = 0; i < amenities.length; i++) {
                        if (name === amenities[i].name) {
                            selected = i;
                            _addModuleContainer();
                        }
                    }
                    context.broadcast('trackMobileNeighbourhoodAmenity', $(element).data());
                break;
            }
        };

        return {
            messages: ['nearByAmenityDataRecieved'],
            onmessage,
            onclick,
            init() {
                doT = context.getGlobal('doT');
                $ = context.getGlobal('jQuery');
                mobileNeighborhood = context.getElement();
                position = {
                    lat: $(mobileNeighborhood).data('lat'),
                    lng: $(mobileNeighborhood).data('lng')
                };
                nearByAmenityService = context.getService('nearByAmenityService');
                amenities = nearByAmenityService.amenity;
                nearByAmenityService.fetchData(position.lat, position.lng, nearByAmenityService.distance, 0, 999);
            },
            destroy() {
                doT = null;
                $ = null;
                mobileNeighborhood = null;
                nearByAmenityService = null;
            }
        };
    });
});
