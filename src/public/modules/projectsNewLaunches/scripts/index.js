"use strict";
define([
    'doT!modules/projectsNewLaunches/views/template',
    'services/loggerService',
    "common/sharedConfig",
    "services/apiService",
    "services/commonService",
    "common/utilFunctions",
    'services/utils',
    'services/defaultService'
], (template,logger,sharedConfig,apiService,commonService,utilFunctions) => {
    Box.Application.addModule('projectsNewLaunches', (context) => {
        const Logger = context.getService('Logger'), 
            utilService = context.getService('Utils'),
            defaultService = context.getService('DefaultService');
        var moduleEl, $moduleEl, configData, $;
        var messages = ['singleSelectDropdownChanged'],
            behaviors = [];
        function resetCity(cityId,cityName){
            $moduleEl.find('.js-titleCity').html(' in ' + cityName);
            $moduleEl.find('.popular-cities [data-type=city]').removeClass('selected');
            $moduleEl.find('.popular-cities [data-type=city][data-cityid='+cityId+']').addClass('selected');
        }
        function render(cityId,cityName){
            resetCity(cityId,cityName);
            apiService.get(sharedConfig.apiHandlers.getNewProjects({cityId:cityId}).url).then(response => {
                var data = response.data;
                if(data && data.items && data.items.length>0){
                    var projectDetails = {data:[]};
                    $.each(data.items,function(index,item){
                        projectDetails.data.push({
                            id:item.projectId,
                            name: item.builder.name +" " + item.name,
                            address: item.completeProjectAddress,
                            startingPrice: utilFunctions.formatNumber(item.minPrice, { seperator : window.numberFormat }),
                            imgUrl:((item.mainImage&&item.mainImage.absolutePath)?item.mainImage.absolutePath + '?width=150&height=200':''),
                            imgTitle:((item.mainImage&&item.mainImage.title)?item.mainImage.title:''),
                            overviewUrl: item.overviewUrl

                        });
                    });
                    ;
                    $moduleEl.find('.js-projectList').html(template(projectDetails));
                }
            }, error => {
                $moduleEl.find('.js-projectList').html('No new projects launched');
            });
        }
        function init() {
            //initialization of all local variables
            moduleEl = context.getElement();
            configData = context.getConfig() || {};
            $ = context.getGlobal('jQuery');
            $moduleEl = $(moduleEl);
            
            commonService.bindOnLoadPromise().then(()=>{
                defaultService.getCityByLocation().then(function(city) {
                    if (city && city.id) {
                        render(city.id, city.label);
                    } else {
                        render(6, 'New Delhi');
                    }
                }, function(error) {
                   //for error show projects from new-delhi
                   render(6,'New Delhi');
                });
                context.broadcast('moduleLoaded', {
                    'name': 'projectsNewLaunches',
                    'id': moduleEl.id
                });
            });
        }

        function destroy() {
            //clear all the binding and objects
        }

        function onmessage(name, data) {
            // bind custom messages/events
            switch(name){
                case 'singleSelectDropdownChanged':
                    if(data && data.id=='cityList_projectsNewLaunches' && data.value){
                        render(data.value,data.label);
                    }
                break;
            }
        }

        function onclick(event, element, elementType) {
            // bind custom messages/events
            switch (elementType) {
                    case 'city':
                        var dataset = element.dataset;
                        if(dataset.cityid){
                            render(dataset.cityid,dataset.cityname)   
                        }
                        break;
                    default:
                }
        }

        return {
            init,
            messages,
            behaviors,
            onmessage,
            onclick,
            destroy
        };
    });
});
