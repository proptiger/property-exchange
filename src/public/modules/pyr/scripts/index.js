'use strict';
define([
    'doT!modules/pyr/views/pyr-step1',
    'doT!modules/pyr/views/pyr-step2',
    'doT!modules/pyr/views/pyr-step3',
    'doT!modules/pyr/views/pyr-step4',
    'services/utils',
    'common/trackingConfigService',
    'behaviors/leadTracking',
    'modules/pyr/scripts/services/pyrService',
    'services/commonService',
    'services/filterConfigService',
    'services/urlService',
    'services/leadPyrService',
    'services/defaultService',
    'css!styles/css/pyr'
], function(pyrStep1Template, pyrStep2Template, pyrStep3Template, pyrStep4Template, utilService, t) {
    Box.Application.addModule('pyr', function(context) {

        var moduleEl,
            scope,
            doT,
            $ = context.getGlobal('jQuery'),homeloanData;

        const query = Box.DOM.query,
            queryAll = Box.DOM.queryAll,
            pyrService = context.getService('PyrService'),
            commonService = context.getService('CommonService'),
            urlService = context.getService("URLService"),
            filterConfigService = context.getService('FilterConfigService'),
            defaultService = context.getService('DefaultService'),
            leadPyrService = context.getService('LeadPyrService'),
            formErrorMap = leadPyrService.getValidationStrategyConfig();


        const config = {
            selectors: {
                FLOATING_COUNT_SELECTOR: ".floating-count",
                LOCALITY_SUGGESTION_SELECTOR: "search-list",
                SELECTED_LOCALITIES_LIST: ".js-localities-selected-list",
                SELECTED_LOCALITIES_TEXT: ".js-localities-selected-text",
                LOCALITY_NO_SELECT: ".js-locality-no-select",
                SUBMIT_SELECTOR: ".js-submit",
                SUBMITTING_SELECTOR: ".js-submitting",
                TYPEAHEAD_SUGGESTION_BOX: ".js-suggestion-box",
                TICK_SELECTOR: ".js-tick",
                LOCALITIES_PLACEHOLDER: "location-placeholder",
                RIGHT_STEP_CONTAINER: ".js-right-step1-container",
                MAX_ALLOWED_LOCALITIES: 6,
                PYR_PLACEHOLDER: ".js-pyr-placeholder",
                OVERLAY_SEC: "pyr-overlay",
                PARENT_MODULE_ID: "requirements",
            },
            enquiryKeys: ["name", "phone", "email", "countryId", "minBudget", "maxBudget", "salesType", "bhk", "localityIds", "cityId", "cityName", "localityId", "listingId", "projectId", "multipleCompanyIds", "multipleSellerIds", "tempEnquiryId", "optIn"],
            pageSpecificDataKeys: ['projectId', 'propertyId', 'cityId', 'listingId'],
            filterBudgetModuleConfig: filterConfigService.getFilterModuleConfig("budget"),
            bedsType: filterConfigService.getFilterModuleConfig("beds").templateData.options,
            stepTypeMapping: {
                step1: "FORM",
                step2: "SELLER",
                step3: "OTP",
                step4: "THANKYOU"
            },
        };


        var _beforeInitialize = function() {
            scope = {};
            scope.pyrData = {};
            scope.pyrData.trackData = {};
            scope.pyrData.beds = config.bedsType;
        };

        var _startAllModulesInContext = function() {
            commonService.startAllModules(moduleEl);
        };

        var _stopAllModulesInContext = function() {
            commonService.stopAllModules(moduleEl);
        };

        function _setHomeloanData(){
            homeloanData={};
            homeloanData.summary_mobile = scope.pyrData.phone;
            homeloanData.summary_email = scope.pyrData.email;
            homeloanData.summary_name = scope.pyrData.name;
            homeloanData.project_cityName = scope.pyrData.cityName;
            homeloanData.project_cityId = scope.pyrData.cityId;
            homeloanData.price_propertyPrice = scope.pyrData.minBudget;
        }
        function openHomeloanForm() { 
            context.broadcast('loadModule', {
                name: 'homeloanFlow',
            });
            
            commonService.bindOnModuleLoad('homeloanFlow', () => {
                context.broadcast('homeloanFlow', {
                    startScreen: "summary",
                    preObject: homeloanData,
                });
            });
        }

        function _appendHtmlToView(html, callback) {
            _stopAllModulesInContext();
            $(moduleEl).find(config.selectors.PYR_PLACEHOLDER).html(html);
            _startAllModulesInContext();
            if (callback && typeof callback === "function") {
                callback();
            }
        }

        function emptyFunction() {}

        function _focusInput(selector) {
            $(moduleEl).find(`[name=${selector}]`).focus();
        }

        var _renderView = function(data) {
            let html,
                func = emptyFunction;
            switch (scope.pyrData.stage) {
                case "step1":
                    html = pyrStep1Template(data);
                    break;
                case "step2":
                    html = pyrStep2Template(data);
                    break;
                case "step3":
                    html = pyrStep3Template(data);
                    func = (function() {
                        context.broadcast('trackShowOtpPyr', getTrackData());
                        _focusInput("otp");
                    }).bind(undefined);
                    break;
                case "step4":
                    data.isBuyLead = data.salesType &&  data.salesType =="buy";
                    html = pyrStep4Template(data);
                    break;
            }
            _appendHtmlToView(html, func);
        };

        var _addRealTimeInputValidations = function() {
            $(moduleEl).on('blur', 'input[name="client-email"]', function() {
                var email = moduleEl.querySelector('input[name="client-email"]');
                if (email.value && !utilService.isEmail(email.value)) {
                    let errorData = {
                        element: moduleEl.querySelector('[name="client-email"]'),
                        errorMessage: formErrorMap.EMAIL.INVALID.MESSAGE
                    };
                    _showErrors(errorData);
                }
            });
            $(moduleEl).on('blur', 'input[name="client-phone-num"]', function() {
                var phone = moduleEl.querySelector('[name="client-phone-num"]'),
                    phoneVal = phone.value;
                if (phoneVal && !utilService.validatePhone(phoneVal, "")) {
                    let errorData = {
                        element: moduleEl.querySelector('[name="client-phone-num"]'),
                        errorMessage: formErrorMap.PHONE.INVALID.MESSAGE
                    };
                    _showErrors(errorData);
                } else {
                    let countryEl = moduleEl.querySelector('.js-countries ul li.active'),
                        countryId = (countryEl) ? countryEl.getAttribute('data-value') : 1;
                    scope.pyrData.phone = phoneVal;
                    scope.pyrData.countryId = countryId;
                }
            });
            $(moduleEl).on('blur', 'input[name="client-name"]', function() {
                var name = moduleEl.querySelector('[name="client-name"]');
                if (name.value && !utilService.isName(name.value, "")) {
                    let errorData = {
                        element: moduleEl.querySelector('[name="client-name"]'),
                        errorMessage: formErrorMap.NAME.INVALID.MESSAGE
                    };
                    _showErrors(errorData);
                }
            });
        };

        var _getDefaultOptions = function(_options) {
            _options = _options || {};
            let options = {
                pageName: utilService.getPageData('pageType') || '',
                pageUrl: window.location.pathname,
                sendOtp: false,
                applicationType: utilService.isMobileRequest() ? 'Mobile Site' : 'Desktop Site',
                jsonDump: window.navigator.userAgent,
                domainId: 1,
                enquiryType: {
                    id: 1
                }
            };
            $.extend(options, _options);
            return options;
        };

        var _renderDefaultCountry = function(data) {
            var element = query(moduleEl, '[data-module="singleSelectDropdown"]');
            if (element && data && data.id == element.id) {
                if (scope.pyrData.countryId) {
                    _setCountryCode(data);
                }
            }
        };

        var _getDefaultEnquiryData = function(enquiryData) {
            var _enquiryData = $.extend(true, {}, enquiryData),
                cookieData = pyrService.getEnquiryCookie(),
                loggedInUserCookieData = pyrService.getLoginCookie();

            _enquiryData.stage = _enquiryData.stage || "step1";
            _enquiryData.name = enquiryData.name || (loggedInUserCookieData && loggedInUserCookieData.name) || (cookieData && cookieData.name);
            _enquiryData.email = enquiryData.email || (loggedInUserCookieData && loggedInUserCookieData.email) || (cookieData && cookieData.email);
            _enquiryData.phone = enquiryData.phone || (loggedInUserCookieData && loggedInUserCookieData.phone) || (cookieData && cookieData.phone);
            _enquiryData.countryId = enquiryData.countryId || ((cookieData && cookieData.countryId) ? cookieData.countryId : 1);
            _enquiryData.cityId = enquiryData.cityId || utilService.getPageData('cityId') || (cookieData && cookieData.cityId) || enquiryData.userCityId;
            _enquiryData.cityName = enquiryData.cityName || utilService.getPageData('cityName') || (cookieData && cookieData.cityName) || enquiryData.userCityName;
            _enquiryData.bhk = enquiryData.bhk || ((cookieData && cookieData.bhk) ? cookieData.bhk : "");
            if(["string","number"].indexOf(typeof _enquiryData.bhk) > -1 && _enquiryData.bhk) {
                _enquiryData.bhk = _enquiryData.bhk.toString().split(",");
            }
            if (enquiryData.localityIds) {
                _enquiryData.localityIds = enquiryData.localityIds;
            } else if (cookieData && cookieData.localityIds && cookieData.cityId == utilService.getPageData('cityId')) {
                _enquiryData.localityIds = cookieData.localityIds;
            }
            _enquiryData.salesType = enquiryData.salesType || utilService.getPageData('listingType') || ((cookieData && cookieData.salesType) ? cookieData.salesType : "buy");
            _enquiryData.salesType = ["rental", "rent"].indexOf(_enquiryData.salesType.toLowerCase()) >= 0 ? "rent" : "buy";
            if (enquiryData.multipleCompanyIds) {
                _enquiryData.multipleCompanyIds = enquiryData.multipleCompanyIds;
            }
            _enquiryData.isPopup = enquiryData.isPopup || false;
            return _enquiryData;
        };

        var _renderPyrForm = function(moduleEl, options) {
            let _pyrData,
                configBudget;
            _pyrData = $.extend(true, options, scope.pyrData);
            if (options.trackData) {
                scope.trackData = $.extend(true, scope.trackData, options.trackData);
            }
            let currentFilterObject = filterConfigService.getSelectedQueryFilterKeyValObj();

            scope.pyrData = _getDefaultEnquiryData(_pyrData);
            scope.pyrData.budgetFilterOptions = config.filterBudgetModuleConfig.getModuleConfig("budget", {
                listingType: scope.pyrData.salesType || "buy"
            });
            configBudget = scope.pyrData.budget && scope.pyrData.budget.start;
            if (configBudget) {
                scope.pyrData.budgetFilterOptions.start[0] = configBudget[0] || scope.pyrData.budgetFilterOptions.start[0];
                scope.pyrData.budgetFilterOptions.start[1] = configBudget[1] || scope.pyrData.budgetFilterOptions.start[1];
            } else if (currentFilterObject.budget) {
                let budget = currentFilterObject.budget.split(",");
                scope.pyrData.budgetFilterOptions.start[0] = budget[0];
                scope.pyrData.budgetFilterOptions.start[1] = budget[1] || (scope.pyrData.budgetFilterOptions.range && scope.pyrData.budgetFilterOptions.range.max && scope.pyrData.budgetFilterOptions.range.max.value) || '';
            }
            scope.pyrData.budgetFilterOptions.connect = true;
            if (currentFilterObject.beds) {
                let bhk = currentFilterObject.beds.split(",");
                scope.pyrData.bhk = bhk;
            }
            scope.pyrData.optInCheckedStatus = leadPyrService.getOptInCheckedStatus();
            let templateData = $.extend(true, {}, scope.pyrData);
            leadPyrService.pruneDummyNameAndEmailFields(templateData);
            _renderView(templateData);
        };

        var getTrackData = function() {
            let trackData = scope.trackData || {},
                pyrData = scope.pyrData,
                filtersUsed = [];
            trackData.currentStep = config.stepTypeMapping[scope.pyrData.stage];
            if (scope.pyrData.phone) {
                trackData[t.PHONE_USER_KEY] = scope.pyrData.phone;
            }
            if (!trackData[t.BUDGET_KEY] && pyrData.minBudget && pyrData.maxBudget) {
                trackData[t.BUDGET_KEY] = pyrData.minBudget + ',' + pyrData.maxBudget;
                filtersUsed.push(trackData[t.BUDGET_KEY]);
            }
            if (!trackData[t.LOCALITY_ID_KEY] && pyrData.localityIds && pyrData.localityIds.map) {
                trackData[t.LOCALITY_ID_KEY] = pyrData.localityIds.map(listing => {
                    return listing.id;
                }).join();
                filtersUsed.push(trackData[t.LOCALITY_ID_KEY]);
            }
            if (!trackData[t.BHK_KEY]) {
                trackData[t.BHK_KEY] = pyrData.bhk && utilService.isArray(pyrData.bhk) && pyrData.bhk.join() && t.makeValuesUniform(pyrData.bhk.join(),t.BHK_MAP);
                filtersUsed.push(trackData[t.BHK_KEY]);
            }
            if (pyrData.enquiryType) {
                trackData.enquiryType = pyrData.enquiryType;
            }
            trackData[t.NAME_KEY] = pyrData.name;
            if (!trackData[t.LISTING_CATEGORY_KEY]) {
                trackData[t.LISTING_CATEGORY_KEY] = t.makeValuesUniform(pyrData.salesType,t.LISTING_CATEGORY_MAP) ;
            }

            if(!trackData[t.BUYER_OPT_IN_KEY]){
                trackData[t.BUYER_OPT_IN_KEY] = pyrData.optIn ? t.BUYER_OPT_IN_TRUE : t.BUYER_OPT_IN_FALSE;
            }

            if (!trackData[t.SELLER_ID_KEY]) {
                trackData[t.SELLER_ID_KEY] = pyrData.multipleCompanyIds && pyrData.multipleCompanyIds.join();
            }

            if (filtersUsed.length) {
                trackData[t.SHARE_YOUR_REQUIREMENTS] = filtersUsed.join("|");
            }

            return {
                id: moduleEl.id,
                trackData
            };
        };


        var _verifyPhone = function() {
            var phone = moduleEl.querySelector('[name="client-phone-num"]').value,
                countryEl = moduleEl.querySelector('.js-countries ul li.active'),
                optIn = moduleEl.querySelector('input[data-type=BUYER_OPT_IN]:checked'),
                country = (countryEl) ? countryEl.innerHTML : undefined,
                formError = false,
                countryId = (countryEl) ? countryEl.getAttribute('data-value') : undefined;
            if (!phone) {
                formError = true;
                let errorData = {
                    element: moduleEl.querySelector('[name="client-phone-num"]'),
                    errorMessage: formErrorMap.PHONE.EMPTY.MESSAGE
                };
                _showErrors(errorData);
            } else if (country && !utilService.validatePhone(phone, country)) {
                formError = true;
                let errorData = {
                    element: moduleEl.querySelector('[name="client-phone-num"]'),
                    errorMessage: formErrorMap.PHONE.INVALID.MESSAGE
                };
                _showErrors(errorData);
            }

            if (!formError) {
                scope.pyrData.phone = phone;
                scope.pyrData.countryId = countryId;
                scope.pyrData.optIn = optIn ? true : false;
                leadPyrService.updateDummyFields(scope.pyrData, ['email', "name"]);
                _shareWithSellers();
            }
        };

        var _shareWithSellers = function() {
            pyrService.getPropertyAdvisors(scope.pyrData, 5).then((response) => {
                if (response) {
                    if(response.length==0){
                        $(moduleEl).find('.js-noSellers').removeClass('hide');
                        $(moduleEl).find('.js-goBack').removeClass('hide');
                        $(moduleEl).find('.js-submit').addClass('hide');
                        return;
                    }
                    let sellerIds = [],
                        userIds = [],
                        sellerName;
                    $.each(response, (k, v) => {
                        v.id && sellerIds.push(v.id); //jshint ignore:line
                        v.userId && userIds.push(v.userId); //jshint ignore:line
                        sellerName = v.name || '';
                    });
                    scope.pyrData.multipleCompanyIds = sellerIds;
                    scope.pyrData.multipleSellerIds = userIds;
                    scope.pyrData.sellerName = sellerName;
                    _updateOTPFlag().always(() => {
                        if(scope.pyrData.sendOtp) {
                             _postTempEnquiry();
                        } else {
                            _postEnquiry();
                        }
                    });
                } else {
                    scope.pyrData.stage = "step4";
                    _reRenderForm(true);
                }
            },() => {
                let errorData = {
                    element: moduleEl.querySelector(config.selectors.SUBMIT_SELECTOR),
                    errorMessage: formErrorMap.GLOBAL.GENERIC.MESSAGE
                };
                _showErrors(errorData);
            });
        };

        var _verifyName = function() {
            var name = moduleEl.querySelector('[name="client-name"]').value,
                formError = false;
            if (name && !utilService.isName(name)) {
                formError = true;
                let errorData = {
                    element: moduleEl.querySelector('[name="client-name"]'),
                    errorMessage: formErrorMap.NAME.INVALID.MESSAGE
                };
                _showErrors(errorData);
            }
            if (!formError) {
                scope.pyrData.name = name;
            }
            return formError;
        };

        var _verifyEmail = function() {
            var email = moduleEl.querySelector('[name="client-email"]').value,
                formError = false;
            if (email && !utilService.isEmail(email)) {
                formError = true;
                let errorData = {
                    element: moduleEl.querySelector('[name="client-email"]'),
                    errorMessage: formErrorMap.EMAIL.INVALID.MESSAGE
                };
                _showErrors(errorData);
            }
            if (!formError) {
                scope.pyrData.email = email;
            }
            return formError;
        };

        var _getRequirements = function() {
            var phone = scope.pyrData.phone,
                countryId = scope.pyrData.countryId,
                minBudgetElem = query(moduleEl, 'input[name="js-range-value-1"]'),
                maxBudgetElem = query(moduleEl, 'input[name="js-range-value-2"]'),
                minBudget,
                maxBudget,
                pageSpecificDataKeys = config.pageSpecificDataKeys;

            if (minBudgetElem) {
                minBudget = minBudgetElem.value;
            }
            if (maxBudgetElem) {
                maxBudget = maxBudgetElem.value;
            }

            var enquiryInfo = {
                phone: phone,
                countryId: countryId
            };
            if (scope.pyrData.salesType) {
                enquiryInfo.salesType = scope.pyrData.salesType;
            }
            if (minBudget) {
                enquiryInfo.minBudget = minBudget;
            }
            if (maxBudget) {
                enquiryInfo.maxBudget = maxBudget;
            }
            if (scope.pyrData.localityIds) {
                enquiryInfo.localityIds = scope.pyrData.localityIds;
            } else if (utilService.getPageData('localityId')) {
                enquiryInfo.localityId = utilService.getPageData('localityId');
            }

            for (let i = 0; i < pageSpecificDataKeys.length; i++) {
                let key = pageSpecificDataKeys[i];
                if (scope.pyrData[key]) {
                    enquiryInfo[key] = scope.pyrData[key];
                } else if (utilService.getPageData(key)) {
                    enquiryInfo[key] = utilService.getPageData(key);
                }
            }

            if (scope.pyrData.bhk) {
                enquiryInfo.bhk = scope.pyrData.bhk;
            }
            if (scope.pyrData.seller) {
                enquiryInfo.seller = scope.pyrData.seller;
            }
            let _enquiryCookie = $.extend(true, {}, enquiryInfo);
            leadPyrService.prune(_enquiryCookie, ["name", "email"]);
            pyrService.setEnquiryCookie(_enquiryCookie);
            $.extend(scope.pyrData, enquiryInfo);
            scope.pyrData.stage = "step2";
            _reRenderForm(true);
        };

        var postTempLead = function(enquiryInfo) {
            enquiryInfo.sendOtp = true;
            pyrService.postTempLead(enquiryInfo).then((response) => {
                let trackData = getTrackData();
                trackData.uniqueIds = [`SAPPHIRE_${response.id}`];
                context.broadcast("Lead_PYR_API_SUCCESS",trackData);
                var tempEnquiryId = response && response.id;
                scope.pyrData.tempEnquiryId = tempEnquiryId;
                scope.pyrData.userId = response.userId;
                scope.pyrData.stage = "step3";
                _reRenderForm(true);
            }, () => {
                let trackData = getTrackData();
                trackData.uniqueIds = [SAPPHIRE];
                context.broadcast("Lead_PYR_API_FAILURE",trackData);
            });
        };

        function _updateOTPFlag() {
            let isIndianFlag = leadPyrService.isIndian(scope.pyrData.countryId);
            return leadPyrService.checkOTPVerified(scope.pyrData.phone).then((response)=>{
                scope.pyrData.sendOtp = isIndianFlag && !response.isVerified;
                scope.pyrData.isLoggedIn = response.isLoggedIn;
                if (urlService.getUrlParam("utm_otp") === "false") {
                    scope.pyrData.sendOtp = false;
                }
            },(response)=>{
                scope.pyrData.sendOtp = isIndianFlag && !response.isVerified;
                scope.pyrData.isLoggedIn = response.isLoggedIn;
                if (urlService.getUrlParam("utm_otp") === "false") {
                    scope.pyrData.sendOtp = false;
                }
            });
        }

        var _getSanitizedEnquiryFields = function() {
            var enquiryInfo = {},
                options = {},
                enquiryKeys = config.enquiryKeys;
            for (let i = 0; i < enquiryKeys.length; i++) {
                if (scope.pyrData.hasOwnProperty(enquiryKeys[i])) {
                    if (scope.pyrData[enquiryKeys[i]] || typeof scope.pyrData[enquiryKeys[i]] == "boolean") {
                        enquiryInfo[enquiryKeys[i]] = scope.pyrData[enquiryKeys[i]];
                        if (typeof enquiryInfo[enquiryKeys[i]] === 'string') {
                            enquiryInfo[enquiryKeys[i]] = enquiryInfo[enquiryKeys[i]].trim();
                        }
                    }
                }
            }
            $.extend(enquiryInfo, _getDefaultOptions(options));
            return enquiryInfo;
        };

        var submitEnquiry = function(enquiryInfo) {
            var errorData = {};
            context.broadcast('trackSubmitPyr', getTrackData());
            pyrService.submitEnquiry(enquiryInfo).then(function(data) {
                    _setLoaderState(false);
                    if (data) {
                        let response,
                            elementId = moduleEl.id,
                            trackingAction,trackData = getTrackData();
                        response = data;
                        scope.pyrData.isOtpVerified = data.isOtpVerified;
                        scope.pyrData.enquiryId = data.enquiryIds[0];
                        scope.pyrData.userId = data.userId;
                        scope.id = elementId;
                        scope.pyrData.leadSubmitted = true;
                        trackingAction = '_PYR_Select';
                        scope.pyrData.stage = "step4";
                        trackData.uniqueIds = [];
                        data.enquiryIds && data.enquiryIds.forEach(function(enquiryId){
                            trackData.uniqueIds.push(`PETRA_${enquiryId}`);
                        });
                        context.broadcast("Lead_PYR_API_SUCCESS",trackData);
                        _reRenderForm(true);
                    } else {
                        _showErrors(errorData);
                    }
                },
                function() {
                    let trackData = getTrackData();
                    trackData.uniqueIds = ['PETRA'];
                    context.broadcast("Lead_PYR_API_FAILURE",trackData);
                    _setLoaderState(false);
                    _showErrors(errorData);
                });
        };

        var _postEnquiry = function() {
            _setLoaderState(true);
            submitEnquiry(_getSanitizedEnquiryFields());
        };
        var _postTempEnquiry = function() {
            _setLoaderState(true);
             postTempLead(_getSanitizedEnquiryFields());
        };

        var _reRenderForm = function(selfInvoked, data = {}) {
            let config = context.getConfig() || {};
            if (data.pyrData) {
                scope.pyrData = $.extend(true, scope.pyrData, data.pyrData);
            }
            if (config.isPopup) {
                if (!selfInvoked) {
                    data.trackData = data.trackData || {};
                    scope.trackData = $.extend(true, scope.trackData, data.trackData);
                    context.broadcast('trackOpenPyr', data);
                }
            } else {
                scope.trackData = $.extend(true, scope.trackData, data.trackData);
                scope.trackData.id = moduleEl.id;
            }
            _renderPyrForm(moduleEl, config);
        };

        var _setLoaderState = function(showLoader) {
            let loader,
                submitButton;
            loader = moduleEl.querySelector(config.selectors.SUBMITTING_SELECTOR);
            submitButton = moduleEl.querySelector(config.selectors.SUBMIT_SELECTOR);
            if (!loader || !submitButton) {
                return;
            }
            if (showLoader) {
                loader.classList.remove("hide");
                submitButton.classList.add("hide");
            } else {
                submitButton.classList.remove("hide");
                loader.classList.add("hide");
            }
        };

        var _resendOtp = function() {
            var resendElem = moduleEl.querySelector("[data-type='OTP_RESEND']"),
                _data = {
                    userId: scope.pyrData.userId,
                    phone: scope.pyrData.phone,
                    userEnquiryId: scope.pyrData.tempEnquiryId,
                };
            context.broadcast('trackResendOtpPyr', getTrackData());
            pyrService.verifyOTP(_data).then(function(response) {
                if (response) {
                    resendElem.innerHTML = formErrorMap.GLOBAL.SUCCESS_OTP.MESSAGE;
                    $(resendElem).addClass("not-clickable");
                    leadPyrService.focusInput(query(moduleEl, "[name='otp']"));
                } else {
                    resendElem.innerHTML = formErrorMap.GLOBAL.GENERIC.MESSAGE;
                    $(resendElem).addClass("not-clickable");
                    setTimeout(() => {
                        resendElem.innerHTML = "resend?";
                    }, 2000);
                }
            }, () => {
                resendElem.innerHTML = formErrorMap.GLOBAL.GENERIC.MESSAGE;
                setTimeout(() => {
                    resendElem.innerHTML = "resend?";
                }, 2000);
            });
        };
        var _updateContactNumbers = function(setExpiry = false) {
            let obj = {
                "contactNumber": scope.pyrData.phone,
                "isVerified": scope.pyrData.isVerified
            };
            leadPyrService.updateContactNumbers(obj, setExpiry);
        };
        var _verifyOtp = function() {
            var otpElem = moduleEl.querySelector('[name="otp"]'),
                otpEntered = otpElem.value.trim();
            if (otpEntered) {
                var _data = {
                    userId: scope.pyrData.userId,
                    phone: scope.pyrData.phone,
                    name: scope.pyrData.name,
                    email: scope.pyrData.email,
                    userEnquiryId: scope.pyrData.tempEnquiryId,
                    otp: otpEntered
                };
                pyrService.verifyOTP(_data).then(function(response) {
                    _updateContactNumbers();
                    if (response && response.isOtpVerified) {
                        context.broadcast('trackOtpCorrectPyr', getTrackData());
                        scope.pyrData.isVerified = true;
                        !scope.pyrData.isLoggedIn && _updateContactNumbers(true); //jshint ignore:line
                        scope.pyrData.isOtpVerified = true;
                        _postEnquiry();
                    } else {
                        context.broadcast('trackOtpIncorrectPyr', getTrackData());
                        let errorData = {
                            element: otpElem,
                            errorMessage: formErrorMap.OTP.INVALID.MESSAGE,
                            errorClass: "otp-error"
                        };
                        _showErrors(errorData);
                    }
                });

            } else {
                let errorData = {
                    element: otpElem,
                    errorMessage: formErrorMap.OTP.EMPTY.MESSAGE
                };
                _showErrors(errorData);
            }
        };

        var _showErrors = function(data) {
            context.broadcast('trackPyrError', $.extend(getTrackData(), {
                label: data.errorMessage
            }));
            let element = data.element,
                message = data.errorMessage;
            if (element && message) {
                var $li = $(element).parent('div');
                $li.find('.error-msg').remove();
                var error = $('<div class="error-msg"></div>');
                error.text(message);
                if (data.errorClass) {
                    error.addClass(data.errorClass);
                }
                error.appendTo($li);
                $(error).fadeOut(7000);
            }
        };

        var _setBhkData = function(element) {
            let bhkTpes = [],
                index = -1;
            if (scope.pyrData.bhk) {
                bhkTpes = scope.pyrData.bhk;
                index = bhkTpes.indexOf(element.value);
            }
            if (element.checked) {
                if (index === -1) {
                    bhkTpes.push(element.value);
                }
            } else {
                if (index !== -1) {
                    bhkTpes.splice(index, 1);
                }
            }
            scope.pyrData.bhk = bhkTpes;
        };

        var _setLocalityData = function(data) {
            if (data.moduleEl.id === "pyrTypeAhead") {
                var entityId,
                    localitySelectedElem = data.element,
                    enitityName,
                    localityText = "",
                    localityList = "",
                    entitytype,
                    index = -1,
                    localityData,
                    localityPlaceholder,
                    localityCountIcon,
                    tick;

                entityId = localitySelectedElem.getAttribute("data-entityid");
                enitityName = localitySelectedElem.getAttribute("data-val");
                entitytype = localitySelectedElem.getAttribute("data-entitytype");
                localityPlaceholder = moduleEl.querySelector('[data-type="' + config.selectors.LOCALITIES_PLACEHOLDER + '"]');
                tick = query(localityPlaceholder, config.selectors.TICK_SELECTOR);

                if (scope.pyrData.localityIds) {
                    for (let i = 0; i < scope.pyrData.localityIds.length; i++) {
                        if (parseInt(scope.pyrData.localityIds[i].id, 10) === parseInt(entityId, 10)) {
                            index = i;
                        }
                    }
                } else {
                    scope.pyrData.localityIds = [];
                }
                if (index === -1) {

                    if (scope.pyrData.localityIds && scope.pyrData.localityIds.length >= config.selectors.MAX_ALLOWED_LOCALITIES) {
                        let errorMsg = "Can only select maximum of " + config.selectors.MAX_ALLOWED_LOCALITIES + " localties",
                            errorElem = query(moduleEl, config.selectors.SELECTED_LOCALITIES_LIST + "-error");
                        $(errorElem).html(errorMsg);
                        setTimeout(function() {
                            $(errorElem).html("");
                        }, 15000);
                        _clearLocalitySuggestion();
                        return;
                    }
                    localityData = {
                        id: entityId,
                        name: enitityName,
                        type: entitytype
                    };
                    scope.pyrData.localityIds.push(localityData);
                }

                localityCountIcon = localityPlaceholder.querySelector(config.selectors.FLOATING_COUNT_SELECTOR);

                if (scope.pyrData.localityIds.length) {
                    for (let i = 0; i < scope.pyrData.localityIds.length; i++) {
                        localityList += '<li><span class="selected-location-value">' + scope.pyrData.localityIds[i].name + '</span> <span class="close" data-type="remove-locality" data-index="' + scope.pyrData.localityIds[i].id + '"><i class="icon-close"></i></span></li>';
                    }
                    localityText += '<li>' + scope.pyrData.localityIds[0].name + '</li>';
                    if (scope.pyrData.localityIds.length >= 2) {
                        let countIcon = (scope.pyrData.localityIds.length > 2) ? ("+<span>" + (scope.pyrData.localityIds.length - 2)) + "</span>" : "";
                        localityText += "<li>" + scope.pyrData.localityIds[1].name + " " + countIcon + "</li>";
                    }
                    localityCountIcon.innerHTML = scope.pyrData.localityIds.length;
                    localityCountIcon.classList.remove("hide");
                    tick.classList.remove("inactive");
                    query(moduleEl, config.selectors.LOCALITY_NO_SELECT).classList.add("hide");
                    query(moduleEl, config.selectors.SELECTED_LOCALITIES_TEXT).innerHTML = localityText;
                    query(moduleEl, config.selectors.SELECTED_LOCALITIES_LIST + " .head-label").classList.remove("hide");
                    query(moduleEl, config.selectors.SELECTED_LOCALITIES_LIST + " ul").innerHTML = localityList;
                } else if (!scope.pyrData.localityIds || scope.pyrData.localityIds.length === 0) {
                    localityCountIcon.innerHTML = "";
                    localityCountIcon.classList.add("hide");
                    query(moduleEl, config.selectors.SELECTED_LOCALITIES_LIST + " .head-label").classList.add("hide");
                    tick.classList.add("inactive");
                    query(moduleEl, config.selectors.LOCALITY_NO_SELECT).classList.remove("hide");
                }
                _clearLocalitySuggestion();
            }
        };

        var _clearLocalitySuggestion = function() {
            var locationSuggestionElem,
                localtionSearchElem = query(moduleEl, '[data-type="query-text"]');
            locationSuggestionElem = query(moduleEl, '[data-type="' + config.selectors.LOCALITY_SUGGESTION_SELECTOR + '"]');
            while (locationSuggestionElem.hasChildNodes()) {
                locationSuggestionElem.removeChild(locationSuggestionElem.lastChild);
            }
            localtionSearchElem.value = "";
        };

        var _removeLocalitySuggestion = function(localityElem) {
            var localityText = "",
                localityId,
                localityPlaceholder,
                tick,
                localityCountIcon;
                localityId = localityElem.getAttribute("data-index");
                localityElem.parentNode.remove();
                localityPlaceholder = moduleEl.querySelector('[data-type="' + config.selectors.LOCALITIES_PLACEHOLDER + '"]');
                localityCountIcon = localityPlaceholder.querySelector(config.selectors.FLOATING_COUNT_SELECTOR);
                tick = query(localityPlaceholder, config.selectors.TICK_SELECTOR);
                for (let i = 0; i < scope.pyrData.localityIds.length; i++) {
                    if (parseInt(scope.pyrData.localityIds[i].id, 10) === parseInt(localityId, 10)) {
                        scope.pyrData.localityIds.splice(i, 1);
                    }
                }
                if (scope.pyrData.localityIds.length) {
                    localityText += '<li>' + scope.pyrData.localityIds[0].name + '</li>';
                    if (scope.pyrData.localityIds.length >= 2) {
                        let countIcon = (scope.pyrData.localityIds.length > 2) ? ("+<span>" + (scope.pyrData.localityIds.length - 2)) + "</span>" : "";
                        localityText += "<li>" + scope.pyrData.localityIds[1].name + " " + countIcon + "</li>";
                    }
                }

                query(moduleEl, config.selectors.SELECTED_LOCALITIES_TEXT).innerHTML = localityText;

                if (scope.pyrData.localityIds && scope.pyrData.localityIds.length) {
                    query(moduleEl, config.selectors.LOCALITY_NO_SELECT).classList.add("hide");
                    tick.classList.remove("inactive");
                    localityCountIcon.innerHTML = scope.pyrData.localityIds.length;
                    localityCountIcon.classList.remove("hide");
                } else {
                    query(moduleEl, config.selectors.LOCALITY_NO_SELECT).classList.remove("hide");
                    query(moduleEl, config.selectors.SELECTED_LOCALITIES_LIST + " .head-label").classList.add("hide");
                    tick.classList.add("inactive");
                    localityCountIcon.innerHTML = "";
                    localityCountIcon.classList.add("hide");
                }
        };

        var _focus = function(element, focusFlag) {
            var boxes,
                i;
            boxes = queryAll(moduleEl, '.requirement-box');
            for (i = 0; i < boxes.length; i++) {
                if (boxes[i] === element) {
                    $(boxes[i]).toggleClass('moveup');
                    if (focusFlag) {
                        boxes[i].classList.add("js-focus");
                    } else {
                        boxes[i].classList.remove("js-focus");
                    }
                    continue;
                } else {
                    $(boxes[i]).toggleClass('removeup');
                }
            }
        };

        var _closeAllOpenCards = function() {
            let elem = query(moduleEl, '.requirement-box.js-focus');
            _startActionItems(elem);
        };

        var _addOverlay = function(add = false) {
            let overlay = query(moduleEl, "[data-type='" + config.selectors.OVERLAY_SEC + "']"),
                rightContainer = query(moduleEl, config.selectors.RIGHT_STEP_CONTAINER),
                elem = query(moduleEl, '.requirement-box.js-focus');
            if (!overlay) {
                return;
            }
            if (add) {
                $(overlay).addClass("hidden");
                $(rightContainer).css({
                    'z-index': ''
                });
                if (elem) {
                    $(elem).css({
                        'z-index': ''
                    });
                }
            } else {
                $(overlay).removeClass("hidden");
                $(rightContainer).css({
                    'z-index': '2'
                });
                if (elem) {
                    $(elem).css({
                        'z-index': '2'
                    });
                }
            }
        };

        var _startActionItems = function(element) {
            let tick = query(element, config.selectors.TICK_SELECTOR),
                focusFlag,
                dataType = element.getAttribute("data-type");
            switch (dataType) {
                case config.selectors.LOCALITIES_PLACEHOLDER:
                    let suggestionBox = query(moduleEl, config.selectors.TYPEAHEAD_SUGGESTION_BOX),
                        locationSelectedElem = query(moduleEl, config.selectors.SELECTED_LOCALITIES_LIST),
                        locationSuggestionElem = query(moduleEl, '[data-type="' + config.selectors.LOCALITY_SUGGESTION_SELECTOR + '"]'),
                        localtionSearchElem = query(moduleEl, '[data-type="query-text"]'),
                        localityNoSelectElem = query(moduleEl, config.selectors.LOCALITY_NO_SELECT);

                    focusFlag = (suggestionBox.className.indexOf('hide') !== -1) ? true : false;
                    tick.classList.toggle("hide");
                    localityNoSelectElem.classList.remove("hide");
                    if (scope.pyrData.localityIds && scope.pyrData.localityIds.length) {
                        tick.classList.remove("inactive");
                        localityNoSelectElem.classList.add("hide");
                    }
                    suggestionBox.classList.toggle("hide");
                    locationSelectedElem.classList.toggle("hide");
                    locationSuggestionElem.classList.toggle("open");
                    _clearLocalitySuggestion();
                    _focus(element, focusFlag);
                    if (focusFlag) {
                        if (!utilService.isMobileRequest()) {
                            localtionSearchElem.focus();
                        }
                        localityNoSelectElem.classList.add("hide");
                    }
                    break;
            }
        };

        var init = function() {
            let config;
            config = context.getConfig() || {};
            moduleEl = context.getElement();
            _beforeInitialize(config);
            doT = context.getGlobal('doT');
            $.extend(scope.pyrData, config.pyrData || {});
            _addRealTimeInputValidations();
            defaultService.getCityByLocation().then(function(city) {
                config.userCityId = city.id;
                config.userCityName = city.label;
                _renderPyrForm(moduleEl, config);
                context.broadcast('moduleLoaded', { name: 'pyr',id: moduleEl.id });
                }, function() {
                   _renderPyrForm(moduleEl, config);
                   context.broadcast('moduleLoaded', { name: 'pyr',id: moduleEl.id });
            });
        };


        /*
          broadcast listener for module /
        */
        var onmessage = function(name, data) {
            switch (name) {
                case 'openPyr':
                    _reRenderForm(false, data);
                    break;
                case 'searchResultClicked':
                    _setLocalityData(data);
                    break;
                case 'singleSelectDropdownChanged':
                    _setCountryCode(data);
                    break;
                case 'singleSelectDropdownInitiated':
                    _renderDefaultCountry(data);
                    break;
                case 'cardOut':
                    if (data.id == config.selectors.PARENT_MODULE_ID && scope.pyrData.stage == "step4") {
                        scope.pyrData.stage = "step1";
                        _reRenderForm(true);
                    }
            }
        };

        var _setCountryCode = function(data) {
            if (!data.value) {
                return;
            }
            var countryId = data.value,
                found = false,
                el = query(moduleEl, '.countryCode');
            defaultService.getCountries().then(function(countries) {
                $.each(countries, function(i, item) {
                    if (parseInt(item.value, 10) === parseInt(countryId, 10)) {
                        found = true;
                        $(el).html(item.code);
                    }
                });
                if (!found) {
                    $(el).html('--');
                }
            });
        };

        var onchange = function(event, element, elementType) {
            if (elementType === "bhk-type") {
                _setBhkData(element);
            }
        };

        var oninput = function(event, element, elementType) {
            if (elementType === "otp") {
                if (element.value.length === 4) {
                    let formError;
                    formError = _verifyName() || _verifyEmail();
                    if (!formError) {
                        _verifyOtp();
                    }
                }
            }
        };

        var onclick = function(event, element, elementType) {

            switch (elementType) {
                case "CONTINUE_NEXT":
                    _verifyPhone();
                    break;

                case "SUBMIT":
                    _getRequirements();
                    break;

                case "OTP_ONCALL":
                    leadPyrService.sendOTPOnCall({
                        userId: scope.pyrData.userId,
                        userNumber: scope.pyrData.phone,
                        element: $(element)
                    });
                    break;

                case "OTP_RESEND":
                    _resendOtp();
                    break;

                case config.selectors.LOCALITIES_PLACEHOLDER:
                    _startActionItems(element);
                    _addOverlay(false);
                    break;

                case "remove-locality":
                    _removeLocalitySuggestion(element);
                    break;

                case "BACK":
                    let currentStep = parseInt(scope.pyrData.stage.slice(-1)),
                        nextStep = "step" + (currentStep - 1);
                    scope.pyrData.stage = nextStep;
                    _reRenderForm(true);
                    break;

                case config.selectors.OVERLAY_SEC:
                    _addOverlay(true);
                    _closeAllOpenCards();
                    break;

                case "bhk-type":
                    $(moduleEl).find(`[data-type=property-type]`).removeAttr("checked");
                    break;

                case "property-type":
                    $(moduleEl).find(`[data-type=bhk-type]`).removeAttr("checked");
                    break;

                case "BUYER_OPT_IN":
                    let checkedStatus = $(element).prop("checked");
                    scope.pyrData.optInCheckedStatus = checkedStatus;
                    leadPyrService.updateOptInStatus(checkedStatus);
                    break;
                case "GO_BACK_REFINE":
                    $(moduleEl).find('.js-step2Back').click();
                    break;
                case "pyr_homeloan_apply":
                    context.broadcast('trackLeadContinueSearch', getTrackData());
                    _setHomeloanData();
                    context.broadcast('popup:close', {callback:openHomeloanForm});
                    break;
                case "pyr_close_lead":
                    context.broadcast('trackLeadContinueHomeloan', getTrackData());
                    context.broadcast('popup:close');
                    break;
            }
        };

        var destroy = function() {
            $(moduleEl).off();
        };

        return {
            init,
            messages: ['cardOut',
                'openPyr',
                "searchResultClicked",
                "singleSelectDropdownChanged",
                "singleSelectDropdownInitiated",
                "popup:change"
            ],

            behaviors: ['leadTracking'],
            onmessage,
            onchange,
            onclick,
            destroy,
            oninput
        };
    });
});
