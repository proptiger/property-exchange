'use strict';
define(['common/sharedConfig',
    'services/utils',
    'services/leadPyrService'
], function(sharedConfig) {
    const SERVICE_NAME = 'PyrService';
    Box.Application.addService(SERVICE_NAME, function(application) {

        const utils = application.getService('Utils'),
            leadPyrService = application.getService('LeadPyrService'),
            //COOKIE_NAME = "enquiry_info",
            LOGIN_COOKIE = "user_info",
            enquiryApi = sharedConfig.apiHandlers.postEnquiry().url,
            tempEnquiryApi = sharedConfig.apiHandlers.postTempEnquiry().url;

        var getEnquiryCookie = leadPyrService.getEnquiryCookie;
        var getPropertyAdvisors =  leadPyrService.getTopSellers;
        var stripQuotes = leadPyrService.stripQuotes;


        var setEnquiryCookie = function(enquiryInfo) {
            leadPyrService.setEnquiryCookie(enquiryInfo);
        };

        var getLoginCookie = function() {
            let cookieData = utils.getCookie(LOGIN_COOKIE);
            if(cookieData && cookieData.length){
                try{
                     cookieData =  JSON.parse(cookieData);
                } catch(e) {
                }
            }
            return cookieData;
        };

        var submitEnquiry = function(enquiryInfo, temp=false){
            var apiUrl = temp ? tempEnquiryApi : enquiryApi;
            var _enquiryInfo =  $.extend(true,{},enquiryInfo);
             if(_enquiryInfo.localityIds && _enquiryInfo.localityIds.length) {
                  _enquiryInfo.localityIds = enquiryInfo.localityIds.filter(localityIds => localityIds.type.toLowerCase() === 'locality').map(localityIds => localityIds.id);
                  _enquiryInfo.suburbIds   = enquiryInfo.localityIds.filter(localityIds => localityIds.type.toLowerCase() === 'suburb').map(localityIds => localityIds.id);
             }

             if(enquiryInfo.bhk && enquiryInfo.bhk.length) {
                 let index = enquiryInfo.bhk.indexOf("3plus");
                 if(index >= 0 ) {
                     _enquiryInfo.bhk.splice(index,1);
                     Array.prototype.push.apply(_enquiryInfo.bhk, ["4","5","6","7","8","9","10"]);
                 }
             }

             ["localityIds","suburbIds","propertyTypes"].forEach(function(key){
                 if(_enquiryInfo[key] && !_enquiryInfo[key].length) {
                     delete _enquiryInfo[key];
                 }
             });
             return leadPyrService.postLead(apiUrl,_enquiryInfo);
         };

        var verifyOTP = function(data) {
            return leadPyrService.verifyOTP(tempEnquiryApi, data);
        };

        var postTempLead = function(enquiryInfo){
            return submitEnquiry(enquiryInfo, true);
        };

        return {
            setEnquiryCookie,
            getEnquiryCookie,
            submitEnquiry,
            verifyOTP,
            getPropertyAdvisors,
            getLoginCookie,
            postTempLead,
            stripQuotes
        };

    });
    return Box.Application.getService(SERVICE_NAME);
});
