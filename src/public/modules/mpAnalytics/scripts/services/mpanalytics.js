define([
       'common/sharedConfig',
       'services/utils',
       'services/apiService'
   ], function(sharedConfig,utils,apiService) {
    'use strict';

    const SERVICE_NAME = 'MP-Analytics',
          mpAnalyticsApi = sharedConfig.apiHandlers.mpAnalytics().url;

    Box.Application.addService(SERVICE_NAME, function() {

        var track = function(trackingPayload) {
            // trackingPayload = trackingPayload || {};
            // trackingPayload.type = "track";
            // var apiData = JSON.stringify(trackingPayload);

            // return apiService.postJSON(mpAnalyticsApi,apiData).then(function(response) {
            //       return response;
            //     },function(error){
            //         return error;
            // });
        };

        var identify = function(payload) {
            // payload = payload || {};
            // payload.type = "identify";
            // var apiData = JSON.stringify(payload);

            // return apiService.postJSON(mpAnalyticsApi,apiData).then(function(response) {
            //       return response;
            //     },function(error){
            //         return error;
            // });
        };


        return {
            track,
            identify
        };
    });

    return Box.Application.getService(SERVICE_NAME);
});
