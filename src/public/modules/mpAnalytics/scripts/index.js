'use strict';
define(['modules/mpAnalytics/scripts/services/mpanalytics',
        'services/utils',
    'services/localStorageService'],
    function(mpAnalyticsService,utils) {
        Box.Application.addModule("mpAnalytics", function(context) {
            var MpAnalytics,
                globalWindow = context.getGlobal('window'),
                timeoutIdleId;
            const mpAnalyticsConfig = {
                SCREEN_INACTIVITY_TIMEOUT_TIME: 20000,
                SCREEN_INACTIVITY_PAGE_TYPE: ['CITY_URLS','PROJECT_URLS','NEARBY_LISTING_URLS', 'NEARBY_URLS', 'NEARBY_LISTING_TAXONOMY_URLS','CITY_URLS_OVERVIEW','LOCALITY_URLS_OVERVIEW','SUBURB_URLS_OVERVIEW','SELLER_PROPERTY_URLS'],
                IDLE_EVENT_TYPES: ["keypress","click"]
            },
            localStorageService = context.getService('localStorageService');
            var _initEventQueue = function() {
                globalWindow.MpAnalyticsData = {};
                globalWindow.MpAnalyticsData = {
                        pageTimestamp: new Date().getTime(),
                        eventsQueue: []
                };
                globalWindow.MpAnalyticsData.eventsQueue.top = function() {
                    if (this.length) {
                        return this[this.length - 1];
                    }
                };
            };
            var _setupClientIdleEventListner = function () {
                mpAnalyticsConfig.IDLE_EVENT_TYPES.forEach( (eventName) => {
                     globalWindow.addEventListener(eventName, _resetIdleTimer, false);
                });
               _startIdleTimer();
            };
            var _startIdleTimer = function() {
               timeoutIdleId = globalWindow.setTimeout(_goInactive, mpAnalyticsConfig.SCREEN_INACTIVITY_TIMEOUT_TIME);
            };

            var _resetIdleTimer = function() {
               globalWindow.clearTimeout(timeoutIdleId);
               _goActive();
            };


            var init = function() {
                let config = context.getConfig(),
                    options = {};
                if(config) {
                    options.debug = config.debug || true;
                }
                _initEventQueue();
                globalWindow.MpAnalytics = new MpAnalytics(options);
                globalWindow.MpAnalytics.identify();
                _setupClientIdleEventListner();
            };

            
            
            var _goInactive = function() {
                let jarvisStorageData = localStorageService.getItem('jStorage'),
                    deliveryId = jarvisStorageData.session_id,
                    pageData = utils.getPageData(),
                    currentPageTags = {};
                if(pageData.localityName) {
                    currentPageTags.locality = [pageData.localityName];
                }
                if(pageData.cityName) {
                    currentPageTags.city = [pageData.cityName];
                }
                if(pageData.projectName) {
                    currentPageTags.project = [pageData.projectName];
                }
                if(pageData.suburbName) {
                    currentPageTags.suburb = [pageData.suburbName];
                }
                let trackingPayload = {
                    event_name: "content_pyr",
                    current_page_tags: currentPageTags,
                    delivery_id: deliveryId
                };
                if(mpAnalyticsConfig.SCREEN_INACTIVITY_PAGE_TYPE.indexOf(utils.getPageData("pageType")) >=0 ) {
                    globalWindow.MpAnalytics.track(trackingPayload);
                }
            };

            var _goActive = function() {
               _startIdleTimer();
            };
            
            MpAnalytics = function (visitorId, options) {
                this.options = options || {};
            };

            MpAnalytics.prototype = {
                identify: function(visitor_id) {
                    visitor_id = visitor_id || utils.getCookie("_mpa");
                    let data = {};
                    if(visitor_id) {
                        data.visitor_id = visitor_id;
                    }
                    data.traits = {
                        page_type: utils.getPageData("pageType") || "home"
                    };
                    _initEventQueue();
                    //mpAnalyticsService.identify(data);
                    this.log("identified");
                },
                track: function(trackingPayload) {
                    let visitor_id = utils.getCookie("_mpa");
                    trackingPayload = trackingPayload || {};
                    trackingPayload.extra = trackingPayload.extra || {};
                    trackingPayload.extra.page_type = utils.getPageData("pageType") || "home";
                    trackingPayload.extra.pageTimestamp = globalWindow.MpAnalyticsData.pageTimestamp;
                    if(visitor_id) {
                        // let data = {
                        //     visitor_id: visitor_id,
                        //     traits: trackingPayload
                        // };
                        //mpAnalyticsService.track(data);
                        this.log("tracked");
                    }
                },
                log: function (message) {
            		if(this.options.debug) {
                        console.log("[mp-analytics] " + message);
                    }
            	},
            };


            var destroy = function() {
                mpAnalyticsConfig.IDLE_EVENT_TYPES.forEach( (eventName) => {
                     globalWindow.removeEventListener(eventName, _resetIdleTimer, false);
                });
            };

            var onmessage = function(name, data) {
                switch (name) {
                    case 'pageLoaded': globalWindow.MpAnalytics.identify();break;
                    case 'mpAnalytics:pop':globalWindow.MpAnalyticsData.eventsQueue.shift();break;
                    case 'mpAnalytics:push': globalWindow.MpAnalyticsData.eventsQueue.push({[`${data.key}`]: data.data});break;
                }
            };
            return {
                messages: ["pageLoaded","mpAnalytics:pop","mpAnalytics:push"],
                init,
                destroy,
                onmessage
            };
        });
    }
);
