
define([
    'doT!modules/ratingBars/views/index',
    'services/apiService',
    'common/sharedConfig'
], function(template, apiService, sharedConfig) {
    "use strict";
    Box.Application.addModule('ratingBars', function(context) {

        var moduleElem, $, config, userCompanyIdData = {},
            apiPromises = {},
            HIDECLASS = 'hide';

        let messages = ['reloadRatingBars'];

        function onmessage(name, data){
            if(data.id != moduleElem.id){
                return;
            }
            switch (name) {
                case 'reloadRatingBars':
                    render(data);
                    break;
                default:
            }
        }

        function setCacheData(listingUserCompanyId, res){
            let ratings = res.data && res.data.ratings || {},
                defaultRating = {value: 0, percentage: 0};
            userCompanyIdData[`${listingUserCompanyId}`] = {
                rating1: ratings['1'] || defaultRating,
                rating2: ratings['2'] || defaultRating,
                rating3: ratings['3'] || defaultRating,
                rating4: ratings['4'] || defaultRating,
                rating5: ratings['5'] || defaultRating
            };
        }

        function loaderHandler(show = true){
            let loaderElem = config.loaderSelector && $(moduleElem).find(config.loaderSelector);
            if(!loaderElem){
                return;
            }

            if(show){
                loaderElem.removeClass(HIDECLASS);
            }else{
                loaderElem.addClass(HIDECLASS);
            }
        }

        function render(data){
            if(data && data.listingUserCompanyId){
                let html, ratings,
                    instance = $(moduleElem),
                    placeHolderSelector = config.placeHolderSelector && instance.find(config.placeHolderSelector);

                if(placeHolderSelector){
                    instance = placeHolderSelector;
                }
                instance.html('');
                loaderHandler(true);

                if(data.ratingCount === 0 && !userCompanyIdData[`${data.listingUserCompanyId}`]){
                    setCacheData(data.listingUserCompanyId, {});
                }

                if(userCompanyIdData[`${data.listingUserCompanyId}`]){
                    ratings = userCompanyIdData[`${data.listingUserCompanyId}`];
                    html = template({
                        ratings,
                        ratingCount: data.ratingCount,
                    });
                    loaderHandler(false);
                    instance.html(html);
                }else {
                    let url = sharedConfig.apiHandlers.sellerRatingMap(data.listingUserCompanyUserId).url;

                    if(!apiPromises[url]){
                        apiPromises[url] = apiService.get(url);
                    }

                    apiPromises[url].then((res)=>{
                        setCacheData(data.listingUserCompanyId, res);
                        html = template({
                            ratings: userCompanyIdData[`${data.listingUserCompanyId}`],
                            ratingCount: data.ratingCount,
                        });
                        loaderHandler(false);
                        instance.html(html);
                    }, ()=>{
                        loaderHandler(false);
                    }).always(()=>{
                        apiPromises[url] = null;
                    });
                }
            }
        }

        return {
            init: function() {
                moduleElem = context.getElement();
                $ = context.getGlobal('jQuery');
                config = context.getConfig() || {};
                context.broadcast('moduleLoaded', {
                    'name': 'ratingBars',
                    'id': moduleElem.id
                });
            },
            messages,
            onmessage,
            destroy: function() {
                $ = null;
                moduleElem = null;
            }
        };
    });
});
