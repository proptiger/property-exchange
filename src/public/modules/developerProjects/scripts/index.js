/**
 * This is a module used for rendering developer ads across all pages.It has following:
 * 1. developerProjects -- service used for fetching the developer projects by doing api call
 *    and then decide the template and template data depending on productType and gives the html to the module
 * 2. developerProjectsTracking -- behavior used for tracking project seen and project card clicked.
 * 
 * JIRA :  MAKAAN-3620
 */
define([
    'services/commonService',
    'modules/developerProjects/scripts/services/developerProjects',
    'modules/developerProjects/scripts/behaviors/developerProjectsTracking'
], function(commonService,developerProjectsService,developerProjectsTracking) {

    'use strict';
    Box.Application.addModule("developerProjects", function(context) {
        
        //product type contains the type of productType decided by the service based on config.
        let $, moduleEl,moduleId, $moduleEl, config,productType;
        const rhsDeveloperProduct=['billboardProjects','miniBillboardProjects'];
        
        //it paste the html returned by the service into placeholder given in config
        function _renderProjects(data) {
            let featuredEntityHtml = data.featuredEntityHtml||'';
            $moduleEl.find(config.developerProjectsPlaceHolder).html(featuredEntityHtml);
            commonService.startAllModules(moduleEl);
            if(config.className && featuredEntityHtml){
                $moduleEl.addClass(config.className)
            }
            if(rhsDeveloperProduct.indexOf(productType) > -1 && featuredEntityHtml ){
                context.broadcast('rhsDeveloperProductRendered');
            }
            context.broadcast('checkDeveloperAdVisibility',{id:moduleId});
        } 

        return {
            behaviors: ['developerProjectsTracking'],
            messages:['developerProjectsFetched'],
            init() {
                moduleEl = context.getElement();
                moduleId = moduleEl.id;
                $ = context.getGlobal('jQuery');
                $moduleEl = $(moduleEl);
                config = context.getConfig() || {};
                productType = developerProjectsService.getProductType(config);
                
                //call to the service function for building the project HTML
                //INPUT : productType, config and moduleId used for broadcasting message back to the dedicated module
                developerProjectsService.getProjectHtml(productType,config,moduleId);

                context.broadcast('moduleLoaded', {
                    'name': 'developerProjects'
                });
            },

            onclick(event, clickElement, elementType) {
                let dataset = $(clickElement).data();
                let projectId,builderId;
                //listens the click event on any project card 
                //and broadcast the message to behavior for tracking
                switch (elementType) {
                    case 'list-element':
                        projectId = dataset.id ? dataset.id : 'explore';
                        context.broadcast('developer_project_clicked',{ projectId,id:moduleEl.id});
                        break;
                    case "builder-project": // when any builder project is clicked
                        projectId = dataset.id ? dataset.id : 'explore';
                        context.broadcast('builder_project_clicked',{ projectId,id:moduleEl.id});
                        break;
                    case "tab": // when tab on project card is clicked
                        projectId = dataset.id ? dataset.id : 'explore';
                        context.broadcast('builder_project_tab_clicked',{ projectId,id:moduleEl.id});
                }
            },

            onmessage(name,data){
                switch (name){
                    //message broadcasted by the service when projectHTML is ready
                    //calls the _renderProjects function with projectHTML returned by the service as an input.
                    case 'developerProjectsFetched':
                        if(data.id == moduleId){           
                            _renderProjects(data);  

                        }
                        break;
                }
            },
            destroy() {
                moduleEl = null;
                $moduleEl = null;
            }
        };
    });

});
