/**
 * This is a service used for rendering developer ads across all pages.It has following:
 *    used for fetching the developer projects when the function is called by the module
      by doing api call with cityId,localityId,suburbId fetched from pageData as an input
 *    and then decide the template and template data depending on productType and gives the html back to the module
 * 
 * JIRA :  MAKAAN-3620
 */


'use strict';

define(['doT!modules/developerProjects/views/index',
        'doT!modules/developerProjects/views/featuredProjects',
        'doT!modules/developerProjects/views/nationalAdProjects',
        'doT!modules/developerProjects/views/featuredDevelopers',
        'services/utils',
        'services/defaultService',
        'services/apiService',
        'common/sharedConfig',
        ], (developerAdsTemplate,featuredProjectTemplate,nationalAdProjectsTemplate,featuredDeveloperTemplate,utils,defaultService,apiService,sharedConfig) => {

    const SERVICE_NAME = "developerProjects";

    Box.Application.addService(SERVICE_NAME, function(application){

        const templateName ={
            featured : '',
            developerAds:'developerAdsTemplate'
        };
        //mapping of productType with the template
        const productHtmlMapping ={
            featuredProjects:featuredProjectTemplate,
            suggestedProjects:featuredProjectTemplate,
            showcaseProjects:developerAdsTemplate,
            trendingProjects:developerAdsTemplate,
            billboardProjects:developerAdsTemplate,
            miniBillboardProjects:developerAdsTemplate,
            featuredDeveloper:featuredDeveloperTemplate,
            featuredNationalProjects:nationalAdProjectsTemplate
        },
        ProjectFetchedMessage = 'developerProjectsFetched';
        // array of productType that can contain slider
        const sliderProducts = ['trendingProjects','showcaseProjects','featuredDeveloper'];
        const builderProducts =['featuredDeveloper','featuredNationalProjects'];

        //function exposed by the service and used by module and its behavior:
        //it accepts moduleConfig as an input and give productType as an output.
        function getProductType(moduleConfig){
            if(moduleConfig.isFeatured){
                return 'featuredProjects';
            }else if(moduleConfig.isTrending){
                return 'trendingProjects';
            }else if(moduleConfig.isBillboard){
                return 'billboardProjects';
            }else if(moduleConfig.isMiniBillboard){
                return 'miniBillboardProjects';
            }else if(moduleConfig.isShowcase){
                return 'showcaseProjects';
            }else if(moduleConfig.isSuggested){
                return 'suggestedProjects';
            }else if(moduleConfig.isFeaturedDeveloper){
                return 'featuredDeveloper';
            }else if(moduleConfig.isFeaturedNational){
                return 'featuredNationalProjects';
            }
        }
        
        //function exposed by the service and used by module 
        //it accepts moduleConfig,productType and moduleId as an input 
        //it calls _buildProjectHtml with url and label as function parameters
        function getProjectHtml(productType,config,id)
        {
            let pageData = utils.getPageData(), projectHtml = '';
            let url,query = {},label='';
            query.saleType = pageData.listingType;
            query.pageSource = config.pageSource;
            if (config.pageSource == 'home') {
                    defaultService.getCityByLocation().then((response) => {
                    let userCity = response || {};
                    
                    if ((userCity && userCity.id)) {
                        query.cityId = JSON.stringify([userCity.id]);
                        label = userCity.label;
                        query.label = label;
                        url = sharedConfig.apiHandlers.getDeveloperProjects({
                            productType,
                            query
                        }).url;
                        _buildProjectHtml(url, label,productType,id);
                    }
                });
            } else if (pageData.cityId) {
                query.cityId = JSON.stringify([pageData.cityId]);
                let pageLevel = (pageData.pageLevel && pageData.pageLevel.toLowerCase()) || '';
                label = pageData.cityName;
                let labelSource = 'city';
                if (pageLevel.indexOf('suburb') > -1 || (pageLevel.indexOf('property') > -1)){
                    label = pageData.suburbName;
                    labelSource = 'suburb';
                    query.suburbId = JSON.stringify(pageData.suburbId && pageData.suburbId.toString().split(',')||[]);
                }
                if (pageLevel.indexOf('locality') > -1 || (pageLevel.indexOf('property') > -1 )){
                    label = label && labelSource == 'suburb' && pageLevel=="multiplesuburblocality" ? `${label}, ${pageData.localityName}`: pageData.localityName ;
                    query.localityId = JSON.stringify(pageData.localityId && pageData.localityId.toString().split(',')||[]);
                }
                query.label = label
                url = sharedConfig.apiHandlers.getDeveloperProjects({
                    productType,
                    query
                }).url;
                _buildProjectHtml(url, label,productType,id);
            }
        }


        //retrn count of project/builders required in different products
        function _getEntityCount(productType){  
            switch(productType){
                case 'billboardProjects':
                    return 1;
                case 'miniBillboardProjects':
                    return 2;
                case 'featuredProjects':
                    return 5;
                case 'suggestedProjects':
                    return 3;
                case 'featuredNationalProjects':
                    return 1;
            }

        }
        //return specified no of projeects/builders
        function _splitEntity(entityList,productType){
            let entities =[],
                maxEntityCount =_getEntityCount(productType),
                loopingLength = maxEntityCount && maxEntityCount < entityList.length ? maxEntityCount : entityList.length;
            for(var i = 0; i<loopingLength; i++){
                entities.push(entityList[i]);
            }
            return entities; 
        }

        // function used for building the project html and passing it 
        // to the dedicated module by broadcasting a message with moduleId and projectHTML as data.

        function _buildProjectHtml(url, label,productType,id) {
            let isMobile = utils.isMobileRequest(),featuredEntityHtml ='';
            if(url){
                apiService.get(url).then((response) => {
                    let target = builderProducts.indexOf(productType) > -1 ? 'builders':'projects'; // builder in case of national ad slot and featured developers
                    let fetchedEntity = response && response[target] || [],
                        template = productHtmlMapping[productType]|| null,
                        exploreMoreUrl = response.projectsUrl,
                        title = _getTitle(productType)||'',
                        className = _getClassName(productType)||{},
                        hasSlider = sliderProducts.indexOf(productType) > -1,
                        lengthForFallBack = _getFallbackLength(productType);

                    let shuffledEntity = utils.shuffleArray(fetchedEntity);//randomizing project details.
                    let featuredEntity = _splitEntity(shuffledEntity,productType);


                    featuredEntityHtml = template({
                        label,
                        featuredEntity,
                        title,
                        className,
                        hasSlider,
                        exploreMoreUrl,
                        lengthForSlider: isMobile ? 1 :(productType=='trendingProjects' ? 5 : 3),
                        widthDivisionFactor : productType=='trendingProjects' ? 2 :'',
                        lengthForFallBack,
                        isMobile,
                        applyFeaturedTag:productType == 'billboardProjects',
                        productType
                    });
                    application.broadcast(ProjectFetchedMessage,{id , featuredEntityHtml}); 
                },() => {
                    application.broadcast(ProjectFetchedMessage,{id , featuredEntityHtml}); 
                });
            }else{
                    application.broadcast(ProjectFetchedMessage,{id , featuredEntityHtml:''});    
            }
        }

        // return the length for showing fallback
        function _getFallbackLength(productType){   
            switch(productType){
                case 'billboardProjects':
                    return 0;
                case 'showcaseProjects':
                    return 2;
                case 'featuredDeveloper':
                    return 3;
                default :
                    return '';
            }
        }
        //return the title depending on productTYpe
        function _getTitle(productType){   
            switch(productType){
                case 'trendingProjects':
                    return 'Trending';
                case 'showcaseProjects':
                    return'Showcase';
                case 'suggestedProjects':
                    return 'Suggested';
                default :
                    return'Featured';
            }
        }
        // return the class for html
        // js- classes are used for tracking purposes and other for css 
        function _getClassName(productType){
            let className = {
                parentClass:'',
                cardClass:''
            };
            switch(productType){
                case 'trendingProjects':
                    className.cardClass ='h-card js-trendingProject';
                    break;
                case 'billboardProjects':
                    className.parentClass = 'sponsored-project';
                    className.cardClass = 'js-billboardProject'
                    break;
                case 'miniBillboardProjects':
                    className.parentClass = 'mini-bill-board';
                    className.cardClass = 'small-card js-miniBillboardProject';
                    break;
                case 'featuredProjects':
                    className.cardClass = 'js-featuredProject';
                    break;
                case 'showcaseProjects':
                    className.cardClass = 'js-showcaseProject';
                    break;
                case 'suggestedProjects':
                    className.cardClass = 'js-suggestedProject';
                    break;
                case 'featuredDeveloper':
                    className.cardClass = 'js-featuredDeveloper';
                    break;
                case 'featuredNationalProjects':
                    className.cardClass = 'js-featuredNationalProject';
                    break;

            }
            return className;
        }
       
        return {
            getProjectHtml,
            getProductType
        };
    });

    return Box.Application.getService(SERVICE_NAME);
});