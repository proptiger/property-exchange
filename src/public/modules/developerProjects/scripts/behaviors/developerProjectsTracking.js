// behavior used for tracking Developer ads
//JIRA -- MAKAAN-3620
define([
    'services/trackingService',
    'common/trackingConfigService',
    'services/utils',
    'modules/developerProjects/scripts/services/developerProjects',
], function(trackingService, t, utils,developerProjectsService) {
    'use strict';
    Box.Application.addBehavior('developerProjectsTracking', function(context) {

        var moduleEl, moduleConfig,productData;
        const messages = ['localityOverviewPageScrolled','cityOverviewPageScrolled','serpPageScrolled', 'propertyPageScrolled','homePageScrolled' ,'developer_project_clicked','builder_project_clicked','builder_project_tab_clicked','checkDeveloperAdVisibility'],
              FEATURED_PROJECTS = 'featuredProjects';
        var trackedProjectIds ={
            featuredProjects:{
                home:[],
                city:[],
                locality:[]
            },
            trendingProjects:[],
            showcaseProjects:[],
            billboardProjects:[],
            miniBillboardProjects:[],
            suggestedProjects:[],
            featuredDeveloper:[],
            featuredNationalProjects:[]
        };

        var init = function() {
            moduleEl = context.getElement();
            moduleConfig = context.getConfig();
            let productType = developerProjectsService.getProductType(moduleConfig); // calling service function for productType
            productData = _getProductData(productType);
        };

        //function for building the trackingObj and calling the trackEvent of tracking Service
        var _trackEvent = function(event, {label, sourceModule, sectionClicked} = {}) {
            var trackingObj = {};
            trackingObj[t.CATEGORY_KEY] = 'Broker Monetization';
            trackingObj[t.LABEL_KEY] = label;
            trackingObj[t.SOURCE_MODULE_KEY] = sourceModule;
            trackingObj[t.SECTION_CLICKED] = sectionClicked;
            trackingService.trackEvent(event, trackingObj);
        };
        //it gives the className to be tracked and source module to be passes to ga based on productType
        function _getProductData(productType){
            let data={
                className :'',
                sourceModule:''
            };
            data.sourceModule = productType;
            switch(productType){
                case 'trendingProjects':
                    data.className = '.js-trendingProject';
                    break;
                case 'billboardProjects':
                    data.className = '.js-billboardProject';
                    break;
                case 'miniBillboardProjects':
                    data.className = '.js-miniBillboardProject';
                    break;
                case 'featuredProjects':
                    data.className = '.js-featuredProject';
                    break;
                case 'showcaseProjects':
                    data.className = '.js-showcaseProject';
                    break;
                case 'suggestedProjects':
                    data.className = '.js-suggestedProject';
                    break;
                case 'featuredDeveloper':
                    data.className = '.js-featuredDeveloper';
                    break;
                case 'featuredNationalProjects':
                    data.className = '.js-featuredNationalProject';
                    break;
            }
            return data;
        }
        function _getPageSource(message){
            switch (message){
                case 'localityOverviewPageScrolled':
                    return 'locality';
                case 'cityOverviewPageScrolled':
                    return 'city';
                case 'homePageScrolled':
                    return 'home';
                default :
                    return '';
            }
        }

        var onmessage = function(name, data) {
            let label,
                sourceModule = productData.sourceModule || '',
                className = productData.className,sectionClicked = sourceModule;

            switch(name){
                case 'localityOverviewPageScrolled'://message broadcasted by localityOverview page when it is scrolled.
                case 'cityOverviewPageScrolled'://message broadcasted by cityOverview page when it is scrolled.
                case "serpPageScrolled":     // message broadcasted by serpPage when it is scrolled
                case "propertyPageScrolled":  // message broadcasted by propertyPage when it is scrolled
                case "homePageScrolled": // message broadcasted by homeController when homePage is scrolled
                case 'checkDeveloperAdVisibility':
                    //checking which projects are visible in viewPort
                    let viewPortElement = utils.attributeListInViewPort(className, 'id');
                        viewPortElement.forEach(el => {
                            label = el||'explore';
                            if( sourceModule!=FEATURED_PROJECTS && trackedProjectIds[sourceModule].indexOf(label)==-1){
                                _trackEvent("Card Seen", {sourceModule, label});//tracking visible projects with project id as label
                                trackedProjectIds[sourceModule].push(label);//pushing projectId in the tracked ProjectIDS array
                            }else if(sourceModule==FEATURED_PROJECTS){
                                let pageSource = _getPageSource(name);
                                if(trackedProjectIds[sourceModule][pageSource].indexOf(label)==-1){
                                    _trackEvent("Card Seen", {sourceModule, label});//tracking visible projects with project id as label
                                    trackedProjectIds[sourceModule][pageSource].push(label);//pushing projectId in the tracked ProjectIDS array
                                }

                            }
                        });
                    break;
                case "developer_project_clicked": // message broadcasted by the module when any of the developerProject is clicked
                    if(data.id != moduleEl.id) return;
                    label = data && data.projectId;
                    _trackEvent('Card Clicked', {label, sourceModule,sectionClicked});
                    break;
                case "builder_project_clicked": // message broadcasted by the module when any of the developerProject is clicked
                    if(data.id != moduleEl.id) return;
                    label = data && data.projectId;
                    _trackEvent('Developer Project Card Clicked', {label, sourceModule,sectionClicked});
                    break;
                case 'builder_project_tab_clicked':
                    if(data.id != moduleEl.id) return;
                    label = data && data.projectId;
                    _trackEvent('Developer Project Tab Clicked', {label, sourceModule});
                    break;
            }
        };

        return {
            init,
            onmessage,
            messages,
            destroy: function() {}
        };
    });
});
