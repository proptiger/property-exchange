'use strict';
define([
	'doT!modules/dealMaker-notification/templates/index',
	'services/trackingService',
	'common/trackingConfigService',
	'services/commonService'
	], (template, trackingService, t, commonService) => {
	const MODULE_NAME="dealMaker-notification";
	Box.Application.addModule(MODULE_NAME, (context) => {
		let moduleEl, moduleConfig, $;
		const config = {
				SELECTORS: {
					CONTENT: "[data-content-placeholder]",
					NOTIFICATION: "[data-notification]"
				},
				CLASS: {
					showClass: "show",
					animationClass: "time-out"
				},
				TIMEOUT: 8000
			};
		let timer;

		function _closeDealMakerNotification(){
			$(moduleEl).find(config["SELECTORS"]["NOTIFICATION"]).removeClass(config["CLASS"]["showClass"]);
		}

		function _openDealMakerNotification(){
			if(timer){
				clearTimeout(timer);
			}
			$(moduleEl).find(config["SELECTORS"]["NOTIFICATION"]).addClass(config["CLASS"]["showClass"]);
		}

		function _trackNotificationVisible(){
			let properties = {};
			properties[t.CATEGORY_KEY] = t.NOTIFICATION_CATEGORY;
			trackingService.trackEvent(t.VISIBLE_EVENT, properties);
		}

		function _startAnimation(){
			_openDealMakerNotification();
			_trackNotificationVisible();
			setTimeout(() => {
				$(moduleEl).find(config["SELECTORS"]["NOTIFICATION"]).addClass(config["CLASS"]["animationClass"]);
			})
			timer = setTimeout(function() {
				_closeDealMakerNotification();
				$(moduleEl).find(config["SELECTORS"]["NOTIFICATION"]).removeClass(config["CLASS"]["animationClass"]);
			}, config["TIMEOUT"]);
		}

		function _getTemplateData(data){
			let obj = {
				sellerImage: data.sellerImage,
				sellerName: data.sellerName
			}
			if(data.buyers == 1){
				obj.singleBuyer = true;
			} else {
				obj.buyers = data.buyers;
				obj.singleBuyer = false;
			}
			if(!data.dummyProject && data.project && data.builder){
				obj.place = `${data.builder} ${data.project}`;
			} else {
				obj.place = `${data.locality}`;
			}
			return obj;
		}

		function init(){
			moduleEl = context.getElement();
			moduleConfig = context.getConfig();
			$ = context.getGlobal('jQuery');
			let templateData = _getTemplateData(moduleConfig);
			let html = template(templateData);
			$(moduleEl).find(config["SELECTORS"]["CONTENT"]).html(html);
			commonService.bindOnLoadPromise().then(() => {
				setTimeout(() => {
					_startAnimation();
				}, 3000)
			})
		}

		function onclick(event, element, elementType){
			switch(elementType){
				case 'dm-notification-connect-now':
                    context.broadcast('openLeadForm', {
                    	source: "deal_maker_notification"
                    });
                    break;
                case 'dm-notification-close':
                    _closeDealMakerNotification();
                    break;
                default:
			}
		}

		function destroy(){
			moduleEl = moduleConfig = null;
		}

		return {
			init,
			onclick,
			destroy
		}
	})
})