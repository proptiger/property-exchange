define([
    'common/trackingConfigService',
    'services/trackingService',
    'services/urlService'
], function(t, trackingService, urlService) {
    'use strict';
    Box.Application.addBehavior('saveSearchTracking', function() {

        var messages = [
            'trackOpenSaveSearch',
            'trackCloseSetAlert',
            'trackFormFilledSetAlert',
            'trackSubmitSetAlert',
            'trackErrorSetAlert'
        ];

        //var moduleId = context.getElement().id;

        var onmessage = function(msgName, data) {
            let properties = {};
            let category = t.LEAD_FORM_CATEGORY,
                labelConfig = t.LEAD_LABELS,
                sourceModuleConfig = t.LEAD_SOURCE_MODULES,
                valueConfig = t.LEAD_VALUES,
                value = urlService.getUrlParam('page') || 1,
                event,
                label,
                sourceModule,
                name,
                email,
                listingCategory;

            switch (msgName) {
                case 'trackOpenSaveSearch':
                    event = t.OPEN_EVENT;
                    label = labelConfig[data.trackId];
                    listingCategory = data.listingCategory;
                    sourceModule = sourceModuleConfig[data.trackId];
                    break;

                case 'trackCloseSetAlert':
                    event = t.NAVIGATION_EVENT;
                    value = valueConfig[t.Close];
                    label = labelConfig[data.trackId];
                    sourceModule = sourceModuleConfig[data.trackId];
                    break;

                case 'trackFormFilledSetAlert':
                    event = t.FORM_FILLED_EVENT;
                    label = labelConfig[data.trackId];
                    name = data.name;
                    email = data.email;
                    sourceModule = sourceModuleConfig[data.trackId];
                    break;

                case 'trackSubmitSetAlert':
                    event = t.SUBMIT_EVENT;
                    sourceModule = sourceModuleConfig[data.trackId];
                    name = data.name;
                    email = data.email;
                    label = labelConfig[data.trackId];
                    break;

                case 'trackErrorSetAlert':
                    event = t.ERROR_EVENT;
                    sourceModule = sourceModuleConfig[data.trackId];
                    name = data.name;
                    email = data.email;
                    label = data.error;
                    break;
            }

            properties[t.CATEGORY_KEY] = category;
            properties[t.LABEL_KEY] = label;
            properties[t.VALUE_KEY] = value;
            properties[t.SOURCE_MODULE_KEY] = sourceModule;
            properties[t.NAME_KEY] = name;
            properties[t.EMAIL_KEY] = email;
            properties[t.LISTING_CATEGORY_KEY] = t.makeValuesUniform(listingCategory,t.LISTING_CATEGORY_MAP) ;

            trackingService.trackEvent(event, properties);
        };

        return {
            messages: messages,
            onmessage: onmessage,
            init: function() {},
            destroy: function() {}
        };
    });
});
