/*
 * Description: module for the save search || set alert functionality
 * @author: [Aditya Jha]
 * Date: Dec 10, 2015
 */

define([
    'services/urlService',
    "doT!modules/saveSearch/views/directSetAlert",
    "doT!modules/saveSearch/views/index",
    'services/loginService',
    'services/filterConfigService',
    'services/utils',
    'services/apiService',
    'services/commonService',
    'services/leadPyrService',
    'common/sharedConfig',
    'modules/saveSearch/scripts/behaviors/saveSearchTracking'
], function(urlService, directSetAlertTemplate, template, loginService, filterConfigService, utils, apiService, commonService, leadPyrService,SharedConfig) {
    "use strict";
    Box.Application.addModule("saveSearch", function(context) {

        const POPUP_SET_ALERT_BUTTON = "saveAlertButton";

        var saveSearchElement, $, userDetail = {}, popupOpen, postApiCall, isLoggedIn, trackData = {}, directSetAlertFlag = false;

        function _getName(data) {
            var name = [];
            if(data) {
                if(data.bhk) {
                    name.push(data.bhk+"bhk");
                }
                if(data.budget) {
                    name.push(data.budget);
                }
                if(data.locality) {
                    name.push(data.locality);
                }
                name = name.join('-').replace(/ /g, '');
            } else {
                name = "new-properties";
            }
            return name;
        }

        function _setEnquiryCookie(obj){
            leadPyrService.setEnquiryCookie(obj);
        }
        var _getFilterInfo = function() {
            let filterConfig = filterConfigService.getSelectedQueryFilterKeyValObj();
            let budget = filterConfig.budget;
            if (budget) {
                budget = budget.split(',');
                budget[0] = utils.formatNumber(budget[0], { precision : 2, seperator : window.numberFormat });
                if(budget.length > 1 && budget[1]) {
                    budget[1] = utils.formatNumber(budget[1], { precision : 2, seperator : window.numberFormat });
                    budget = budget[0] + ' - ' + budget[1];
                } else {
                    budget = budget[0];
                }
            }
            let pageData = utils.getPageData() || {};
            let fitlerInfo = {
                bhk: filterConfig.beds,
                budget: budget,
                locality: pageData.localityName,
                property: filterConfig.propertyType,
            };
            return fitlerInfo;
        };


        var _addModuleContainer = function(element) {
            let $childElement = $(element).children('.mod-content');
            let filterInfo = _getFilterInfo();
            let htmlContent = template({
                filterInfo: filterInfo,
                name: _getName(filterInfo),
                userEmail: userDetail.email || '',
                setAlertButton: POPUP_SET_ALERT_BUTTON
            });
            if ($childElement.length) {
                $childElement.replaceWith(htmlContent);
            } else {
                $(element).append(htmlContent);
            }
            commonService.startAllModules(element);
        };


        // var _openLoginPopup = function() {
        //     loginService.openLoginPopup();
        // };
        var _trimUrlQuery = function() {
            if(urlService.getAllUrlParam("setAlert")) {
                let newHref = urlService.removeUrlParam("setAlert", undefined, true);
                newHref = urlService.removeUrlParam("email", newHref, true);
                urlService.ajaxyUrlChange(newHref, true);
            }
        };

         var _directSetAlertClose = function() {
            _trimUrlQuery();
            directSetAlertFlag = false;
            setTimeout(function() {
                context.broadcast('popup:close');
                popupOpen = undefined;
            }, 3000);
        };

        var _send = function(name,email,messageElement) {
            if(postApiCall) {
                postApiCall.abort();
            }
            let apiUrl = window.location.pathname;
            if(window.location.search) {
                apiUrl += window.location.search + '&name=' + encodeURI(name) + '&savedSearches=true';
            } else {
                apiUrl += '?name=' + encodeURI(name) + '&savedSearches=true';
                apiUrl += `&cityId=${window.pageData.cityId||''}&projectId=${window.pageData.projectId||''}`;
            }

            let saveSearchUrl = SharedConfig.apiHandlers.saveSearch().url;

            if(directSetAlertFlag && !isLoggedIn) {
                apiUrl += '&nonLoggedIn=true';
            }

            else if(!isLoggedIn){
                apiUrl += '&nonLoggedIn=true&email='+ encodeURI(email);
            }

            _setEnquiryCookie({email});
            context.broadcast('trackSubmitSetAlert', trackData);
            postApiCall = apiService.postJSON(saveSearchUrl,{
                query: apiUrl,
                pageData: utils.getPageData() || {}
            });
            postApiCall.then((response) => {
                if(response && response.data) {
                    //let setAlertId = response.data.id || '';
                    //trackSetAlert();
                    messageElement.innerHTML = "thank you! we will keep you notified of new properties matching your requirements";
                    if(directSetAlertFlag) {
                        _trimUrlQuery();
                        directSetAlertFlag = false;
                    }
                    setTimeout(function() {
                        context.broadcast('popup:close');
                        popupOpen = undefined;
                    }, 3000);
                }
                postApiCall = null;
            }, (error) => {
                postApiCall = null;
                let responseJSON = error && error.responseJSON ? error.responseJSON : null;
                if(responseJSON && responseJSON.body && responseJSON.body.error) {
                    if(responseJSON.body.statusCode == 498) {
                        error = "you've already set an alert for this requirement";

                    } else if(responseJSON.body.statusCode == 499) {
                        error = 'A search is already saved with this name, please change the name';

                    } else{
                        error = 'Oops! something went wrong';

                    }
                    messageElement.innerHTML =  error;
                    if(directSetAlertFlag) {
                        _directSetAlertClose();
                    }
                    context.broadcast('trackErrorSetAlert', $.extend(trackData,{error}));
                }
            });
        };




        var onmessage = function(name, data) {
            if (name === 'openSaveSearch') {
                // check if user is loggedIn to get his email
                trackData.trackId = data.trackId;
                trackData.listingCategory = data.listingCategory;
                context.broadcast('trackOpenSaveSearch', trackData);
                // utils.trackEvent('set_alert_open', 'Buyer_' + utils.getTrackPageCategory(), filterConfigService.trackPageFilters());
                userDetail = {};
                loginService.isUserLoggedIn().then((response) => {
                    userDetail = response.data;
                    _addModuleContainer(saveSearchElement);
                    popupOpen = true;
                    isLoggedIn = true;
                }, () => {
                    userDetail = leadPyrService.getEnquiryCookie() || {};
                    _addModuleContainer(saveSearchElement);
                    popupOpen = true;
                    isLoggedIn = false;
                });
            } else if (name === "popup:change") {
                var currentModuleId = saveSearchElement.id,
                    popupElem = data;
                if(popupElem && popupElem.el &&  popupElem.el.querySelector("[data-module='saveSearch']") && popupElem.el.querySelector("[data-module='saveSearch']").id === currentModuleId) {
                    commonService.stopAllModules(saveSearchElement);
                    popupOpen = undefined;
                }
            } else if (name === 'onLoggedIn') {
                if(data && data.response) {
                    userDetail = data.response;
                }
                if(popupOpen) {
                    _addModuleContainer(saveSearchElement);
                }
            } else if (name === 'directSetAlertWelcome') {
                let userEmail = urlService.getUrlParam("email"),
                    filterInfo = _getFilterInfo(),
                    userName = _getName(filterInfo);
                    directSetAlertFlag = true;
                    trackData.trackId = data.trackId;
                    context.broadcast('trackOpenSaveSearch', trackData);
                    trackData.name = userName;
                    trackData.email = userEmail;
                    $(saveSearchElement).append(directSetAlertTemplate);
                let messageElement = saveSearchElement.querySelector("[data-message='message']");
                    loginService.isUserLoggedIn().then(() => {
                        popupOpen = true;
                        isLoggedIn = true;
                        _send(userName,userEmail,messageElement);
                    }, () => {
                        popupOpen = true;
                        isLoggedIn = false;
                        _send(userName,userEmail,messageElement);
                    });


            }
        };

        // var trackSetAlert = function(){
        //     // utils.trackEvent('set_alert_submit', 'Buyer_' + utils.getTrackPageCategory(), filterConfigService.trackPageFilters());
        // };

        // var _setSyncSaveSearch = function(id, email){
        //     window.syncSearch = window.syncSearch || [];
        //     window.syncSearch.push({id,email});
        // };

        return {
            messages: ['directSetAlertWelcome','openSaveSearch', 'onLoggedIn',"popup:change"],
            behaviors: ['saveSearchTracking'],
            init: function() {
                saveSearchElement = context.getElement();
                $ = context.getGlobal('jQuery');
                context.broadcast('moduleLoaded', {
                    'name': 'saveSearch',
                    'id': saveSearchElement.id
                });
                userDetail = {};
            },
            destroy: function() {

            },
            onmessage,
            onclick: function(event, element, elementType) {
                if (elementType === POPUP_SET_ALERT_BUTTON) {
                        // make post request to set data
                        //let selector = $(saveSearchElement);
                        let nameSelector = $(saveSearchElement).find("#savesearch_name [data-type='input-box']");
                        let emailSelector = $(saveSearchElement).find("#savesearch_email [data-type='input-box']");

                        let messageElement = saveSearchElement.querySelector("[data-message='message']");
                        let nameMessageElement = saveSearchElement.querySelector("[data-message-name='message']");
                        let emailMessageElement = saveSearchElement.querySelector("[data-message-email='message']");

                        let error;
                        let name = trackData.name = nameSelector.val().trim();
                        let email = trackData.email = emailSelector.val().trim();
                        context.broadcast('trackFormFilledSetAlert', trackData);

                        if(!name) {
                            error = "please enter a name";
                            nameMessageElement.innerHTML = error;
                        } else {
                            nameMessageElement.innerHTML = '';
                        }

                        if(!email) {
                            error = "please enter an email";
                            emailMessageElement.innerHTML = error;
                        } else if(!utils.isEmail(email)) {
                            error = 'please enter a valid email';
                            emailMessageElement.innerHTML = error;
                        } else{
                            emailMessageElement.innerHTML = '';
                        }

                        if (error){
                            context.broadcast('trackErrorSetAlert', $.extend(trackData,{error}));
                            return;
                        }
                        _send(name,email,messageElement);
                } else if (elementType === 'closePopup'){
                    context.broadcast('trackCloseSetAlert', trackData);
                }
            }
        };
    });
});
