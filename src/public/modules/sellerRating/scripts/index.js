/**
 * @fileoverview Module for the seller rating stars
 * @author [Aditya Jha]
 */

/*global Box*/

define([
    'doT!modules/sellerRating/views/index',
    'services/utils'
], function(template,utils) {
    "use strict";
    Box.Application.addModule('sellerRating', function(context) {

        var sellerRatingElement, $, rating, schema;

        var _addModuleContainer = function(sellerRatingElement, rating) {
            var ratingClass = "not-rated";
            if(rating){
                ratingClass = utils.getRatingBadgeClass(rating);
                rating = parseFloat(rating).toFixed(1);
                rating = rating >= 0 ? rating : 0;
            }
            let htmlContent = template({
                rating: rating,
                schema: schema,
                ratingClass: ratingClass
            });
            $(sellerRatingElement).html(htmlContent);
        };


        return {
            init: function() {
                sellerRatingElement = context.getElement();
                $ = context.getGlobal('jQuery');
                rating = $(sellerRatingElement).data('sellerrating');
                schema = $(sellerRatingElement).data('schema');
                if(rating !== undefined){
                    _addModuleContainer(sellerRatingElement, rating);
                }
                context.broadcast('moduleLoaded', {
                    'name': 'sellerRating',
                    'id': sellerRatingElement.id
                });
            },
            messages: ['reloadSellerRating'],
            onmessage(messages, data){
                if(data.id !== sellerRatingElement.id){
                    return;
                }
                switch (messages) {
                    case 'reloadSellerRating':
                        _addModuleContainer(sellerRatingElement, data.rating);
                        break;
                }
            },
            destroy: function() {
                $ = null;
            }
        };
    });
});
