'use strict';

define([
	"services/trackingService",
	"common/trackingConfigService"
], (trackingService, t) => {

	const SERVICE_NAME = "getInTouchTrackingService";

	Box.Application.addService(SERVICE_NAME, function(){

		function _getProperties(data = {}){
			let obj = {
				[t.CATEGORY_KEY]: t.SELLER_BENEFITS_CATEGORY,
				[t.LABEL_KEY]: t.BENEFITS_LABELS[data.currentSection] || t.BENEFITS_LABELS["default"]
			};
			return obj;
		}

		function trackSellerLeadSubmit(data){
			let event = t.OPEN_EVENT;
			let properties = _getProperties(data);
			trackingService.trackEvent(event, properties);
		}

		function trackSellerLeadFormOpen(data){
			let event = t.SUBMIT_EVENT;
			let properties = _getProperties(data);
			trackingService.trackEvent(event, properties);
		}

		return {
			trackSellerLeadSubmit,
			trackSellerLeadFormOpen
		};
	});

	return Box.Application.getService(SERVICE_NAME);
});