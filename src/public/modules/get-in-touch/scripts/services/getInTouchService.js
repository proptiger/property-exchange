'use strict';

define([], () => {

	const SERVICE_NAME = "getInTouchService";

	Box.Application.addService(SERVICE_NAME, function(){

		const pnbBranchConfig={
            ahmedabad:[{
                label:'Ahmedabad',
                value:'Ahmedabad'
            },{
                label:'Odhav',
                value:'Odhav'
            }],
            bangalore:[{
                label:'ITPL',
                value:'ITPL'
            },{
                label:'Manyta Tech Park',
                value:'Manyta Tech Park'
            },{
                label:'BTM',
                value:'BTM'
            },{
                label:'Indra Nagar',
                value:'Indra Nagar'
            },{
                label:'Malleshwaram',
                value:'Malleshwaram'
            },{
                label:'Marathalli',
                value:'Marathalli'
            },{
                label:'Vijayanagar',
                value:'Vijayanagar'
            },{
                label:'Kasturi Nagar',
                value:'Kasturi Nagar'
            }],
            chennai:[{
                label:'Chennai',
                value:'Chennai'
            },{
                label:'Ambattur',
                value:'Ambattur'
            },{
                label:'OMR',
                value:'OMR'
            }],
            delhi:[{
                label:'Barakhamba Road',
                value:'Barakhamba Road'
            },{
                label:'Green Park',
                value:'Green Park'
            },{
                label:'Janakpuri',
                value:'Janakpuri'
            },{
                label:'Pitampura',
                value:'Pitampura'
            }],
            hyderabad:[{
                label:'Banjara Hills',
                value:'Banjara Hills'
            },{
                label:'Lakdi Ka Pool',
                value:'Lakdi Ka Pool'
            }],
            kolkata:[{
                label:'Kolkata',
                value:'Kolkata'
            },{
                label:'Purana Dass Road',
                value:'Purana Dass Road'
            }],
            mumbai:[{
                label:'Panvel',
                value:'Panvel'
            },{
                label:'Prabhadevi',
                value:'Prabhadevi'
            },{
                label:'Vile Parle',
                value:'Vile Parle'
            }],
            noida:[{
                label:'Noida',
                value:'Noida',
            },{
                label:'Noida Sector 63',
                value:'Noida Sector 63'
            }],
            pune:[{
                label:'JM Road',
                value:'JM Road'
            },{
                label:'Hinjewadi',
                value:'Hinjewadi'
            },{
                label:'Kharadi',
                value:'Kharadi'
            },{
                label:'Kondhwa',
                value:'Kondhwa'
            },{
                label:'Pimpri',
                value:'Pimpri'
            },{
                label:'Warje',
                value:'Warje'
            }],
            junagadh:[{
                label:'Gujrat',
                value:'Gujrat'
            }]
        };

        const config = {
			SELECTORS: {
				TEMPLATE_PLACEHOLDER: "[data-placeholder]",
				PHONE: "[data-type=PHONE_FIELD] input",
                COUNTRY_ID: "input[data-country=COUNTRY_ID_FIELD]",
                COUNTRY_CODE: "[data-country-code=COUNTRY_CODE_FIELD]",
                EMAIL: "[data-type=EMAIL_FIELD] input",
                NAME: "[data-type=NAME_FIELD] input",
                COMMENTS: "[data-type=COMMENTS]",
                CITY_ID: "input[data-city=CITY_ID_FIELD]",
                BRANCH_ID: "input[data-branch=BRANCH_ID_FIELD]"
			},
			ERRORS: {
				PHONE: {
					FIELD: "[data-message=PHONE]",
					MESSAGE: "Please enter valid phone number"
				},
				EMAIL: {
					FIELD: "[data-message=EMAIL]",
					MESSAGE: "Please enter valid email"
				},
				NAME: {
					FIELD: "[data-message=NAME]",
					MESSAGE: "Please enter valid name"
				},
				SUCCESS: {
					FIELD: "[data-message=SUCCESS]",
					MESSAGE: "Thank you for submitting your query"
				},
				GENERIC: {
					FIELD: "[data-message=GLOBAL]",
					MESSAGE: "Some error occurred"	
				},
				CITY: {
					FIELD: "[data-message=CITY]",
					MESSAGE: "Please select city"
				},
				BRANCH: {
					FIELD: "[data-message=BRANCH]",
					MESSAGE: "Please select branch"
				}
			},
			placeholderMap: {
				dealMaker: `I am interested in partnering with Makaan-'Deal Maker'`,
				expertDealMaker: `I am interested in partnering with Makaan-'Expert Deal Maker'`,
				addsBenefits: `I am interested in partnering with Makaan-'City/Locality Expert'`,
				cityExpert: `I am interested in partnering with Makaan-'City Expert'`,
				localityExpert: `I am interested in partnering with Makaan-'Locality Expert'`,
				projectPromotion: `I am interested in partnering with Makaan-'Focused Projects'`,
				default: `I am interested in partnering with Makaan`
			},
			hideTime: 5000
		};


        function getBranchConfig (argument) {
        	return pnbBranchConfig;
        }
        function getConfig (argument) {
        	return config;
        }

		return {
			getBranchConfig,
			getConfig
		};
	});

	return Box.Application.getService(SERVICE_NAME);
});