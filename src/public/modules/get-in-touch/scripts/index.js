'use strict';
define([
	"doT!modules/get-in-touch/views/index",
	"services/commonService",
	"services/utils",
	"services/apiService",
	"common/sharedConfig",
	"modules/get-in-touch/scripts/services/getInTouchTrackingService",
	"modules/get-in-touch/scripts/services/getInTouchService"
	], (template, commonService, utils, apiService, sharedConfig, getInTouchTrackingService,getInTouchService) => {
	let MODULE_NAME = "get-in-touch";

	Box.Application.addModule(MODULE_NAME, (context) => {
		let moduleEl, moduleConfig, sectionData,isHomeloanAgent,branchModuleId,pnbBranchConfig={},config={};

		function _submitSuccess(){
			if(isHomeloanAgent){
				$(moduleEl).find('.js-homeloanHeading').addClass('hide');
				$(moduleEl).find('.js-homeloanFields').addClass('hide');
				$(moduleEl).find('.js-homeloanThankyou').removeClass('hide');
			}
			else{
			_showMessage("SUCCESS", true);
			}
			getInTouchTrackingService.trackSellerLeadSubmit(sectionData);
		}

		function _submitFail(){
			_showMessage("GENERIC");
		}

		function _showMessage(type, close=false){
			$(moduleEl).find(config.ERRORS[type].FIELD).html(config.ERRORS[type].MESSAGE);
			setTimeout(function(){
				$(moduleEl).find(config.ERRORS[type].FIELD).html("");
				if(close){
					context.broadcast("popup:close");
				}
			}, config.hideTime);
		}

		function _submitForm(data){
			let query={};
			if(isHomeloanAgent){
				query.homeloanAgent = true;
			}else{
				query.partnerWithMakaan=true;
			}
			let api = sharedConfig.apiHandlers.mailerService({query}).url;
			apiService.postJSON(api, data).then(_submitSuccess,_submitFail);
		}

		function _validate(){
			let phone = $(moduleEl).find(config.SELECTORS["PHONE"]).val();
			let countryId = $(moduleEl).find(config.SELECTORS["COUNTRY_ID"]).data("label");
			let email = $(moduleEl).find(config.SELECTORS["EMAIL"]).val();
			let name = $(moduleEl).find(config.SELECTORS["NAME"]).val();
			let message = $(moduleEl).find(config.SELECTORS["COMMENTS"]).val();
			let city=isHomeloanAgent ? $(moduleEl).find(config.SELECTORS["CITY_ID"]).data("label"):'';
			let branch=isHomeloanAgent ? $(moduleEl).find(config.SELECTORS["BRANCH_ID"]).data("label"):'';
			let countryCode=$(moduleEl).find(config.SELECTORS["COUNTRY_ID"]).val();
			let flag = true;
			phone = utils.validatePhone(phone, countryId);
			if(!phone){
				flag = false;
				_showMessage("PHONE");
			}
			if(email && !utils.isEmail(email)){
				flag = false;
				_showMessage("EMAIL");
			}
			if(name && !utils.isName(name)){
				flag = false;
				_showMessage("NAME");
			}
			if(isHomeloanAgent && !city){
				flag = false;
				_showMessage("CITY");
			}
			if(isHomeloanAgent && !branch){
				flag = false;
				_showMessage("BRANCH");
			}
			return flag ? {name, email, phone, message,city,countryCode,branch} : flag;
		}

		function _validateAndSubmit(){
			let validation = _validate();
			if(validation){
				_submitForm(validation);
			}
		}

		function _getSectionPlaceholder(data={}){
			return config.placeholderMap[data && data.currentSection] || config.placeholderMap["default"];
		}

		function _setValue(data) {
			branchModuleId = data && data.name==='branch' ? data.id : branchModuleId;
			let params = data && data.params && JSON.parse(decodeURI(data.params));
            if (!params) {
                return;
            }
            if (params.value) {
            	if(data.name=="countries" && params.code){
                	$(moduleEl).find(config.SELECTORS["COUNTRY_CODE"]).text(params.code);
                	$(moduleEl).find(config.SELECTORS["COUNTRY_ID"]).val(params.value).data("label", params.label);
            	}
            	else if(data.name=='cities'){
                	$(moduleEl).find(config.SELECTORS["CITY_ID"]).val(params.value).data("label", params.label);
                	$(moduleEl).find(config.SELECTORS["BRANCH_ID"]).val('').data("label","");
                	var list = pnbBranchConfig[params.label.toLowerCase()];
                	if(list){
	                    context.broadcast('SetDropdownList',{list,id:branchModuleId,liName:'branch'});
	                    $(moduleEl).find('.js-branchList').removeClass('hide');
                    }
                    else{
                    	$(moduleEl).find(config.SELECTORS["BRANCH_ID"]).val(params.label).data("label", params.label);
                    	$(moduleEl).find('.js-branchList').addClass('hide');
                    }	
            	}
            	else if(data.name=='branch'){
                	$(moduleEl).find(config.SELECTORS["BRANCH_ID"]).val(params.value).data("label", params.label);	
            	}
            }
        }

		function _populateData(data){
			sectionData = data;
			let obj={};
			obj.homeloan=moduleConfig.homeloan;
			obj.hideComment=moduleConfig.hideComment;
			obj.placeholder = _getSectionPlaceholder(data);
			let html = template(obj);
			$(moduleEl).find(config.SELECTORS.TEMPLATE_PLACEHOLDER).html(html);
			commonService.startAllModules(moduleEl);
			getInTouchTrackingService.trackSellerLeadFormOpen(sectionData);
		}

		function init(){
			moduleEl = context.getElement();
			moduleConfig = context.getConfig();
			pnbBranchConfig = getInTouchService.getBranchConfig();
			config = getInTouchService.getConfig();
			context.broadcast("moduleLoaded", {
				name: MODULE_NAME,
				id: moduleEl.id
			});
		}

		function destroy(){
			moduleEl = moduleConfig = sectionData = null;
		}

		let onmessage = {
			"open-get-in-touch": function(data){
				isHomeloanAgent=data.homeloanAgent;
				if(!data.outsidePopup){
					context.broadcast("popup:open", {
						id: moduleConfig.popupId
					});
				}
				_populateData(data);
			},
			"singleSelectDropdownChanged": _setValue,
			"singleSelectDropdownInitiated": _setValue
		};

		function onclick(event, element, elementType){
			switch(elementType){
				case "VALIDATE_AND_SUBMIT":
					_validateAndSubmit();
					break;
				case "HOMELOAN_OKAY":
				context.broadcast("popup:close");
				break;

			}
		}

		return {
			init,
			destroy,
			onmessage,
			onclick
		};
	});
});