define([
    'doT!modules/banks/views/index',
    "common/sharedConfig",
    "services/apiService",
    "services/commonService",
    "common/utilFunctions"
], function(bankDetailsTemplate, sharedConfig, apiService, commonService, utilFunctions) {
    'use strict';
    Box.Application.addModule("banks", function(context) {

        var element, moduleId, $, $moduleEl, moduleConfig;

        function render() {
            apiService.get(sharedConfig.apiHandlers.getBankDetails().url).then(response => {
                var data = response;
                if (data && data.results && data.results.length > 0) {
                    var bankDetails = { data: [] };
                    $.each(data.results, function(index, item) {
                        let rateOfInterest = item.maxInterestRate + " %";
                        if (item.maxInterestRate !== item.minInterestRate) {
                            rateOfInterest = item.minInterestRate + ' - ' + rateOfInterest;
                        }
                        bankDetails.data.push({
                            id: item.id,
                            name: item.name.replace(' ', '_'),
                            showName: item.name.indexOf('(') > 0 ? item.name.substr(0, item.name.indexOf('(')) : item.name,
                            bankLogo: item.bankLogo + '?width=70&height=40',
                            rateOfInterest: rateOfInterest,
                            emiPerLac: item.minInterestRate ? utilFunctions.getEmi(100000, item.minInterestRate, 20) : ''
                        });
                    });
                    element.innerHTML = bankDetailsTemplate(bankDetails);
                    if (moduleConfig && moduleConfig.enableSelectAllOption) {
                        $moduleEl.find('.bank-list-wrap').removeClass('hide');
                        $moduleEl.find("input[type='checkbox'][data-type=selectAllBanks]").prop('checked', true);
                        toggleAllBankOptions(true);
                    }
                    context.broadcast('bankRendered');
                }
            }, () => {
                $moduleEl.find('.js-error').removeClass('hide');
            });
        }

        function broadcastInterestedBankList() {
            var selected = getSelectedbanks();
            context.broadcast('interestedBankList', {
                selected: selected,
                name: 'selectedBankList'
            });
        }

        function toggleAllBankOptions(value) {
            $moduleEl.find('[data-type=bank]').each(function() {
                $moduleEl.find('[data-type=bank][data-name="' + $(this).data('name') + '"]').prop('checked', value);
            });
            broadcastInterestedBankList();
        }

        function getSelectedbanks() {
            var selected = [];
            $moduleEl.find('input:checked').each(function() {
                $(this).attr('data-id') && selected.push($(this).attr('data-id'));
            });
            $moduleEl.find('.js-selectedBankCount').text(selected.length);
            return selected;
        }

        function checkedApprovedBankFromList(data) {
            apiService.get(sharedConfig.apiHandlers.getApprovedBankDetails({ projectId: data.projectId }).url).then(response => {
                var data = response.data;
                if (data && data.results && data.results.length > 0) {
                    $moduleEl.find("input[type='checkbox'][data-type=selectAllBanks]").prop('checked', false);
                    toggleAllBankOptions(false);
                    context.broadcast('setApprovedBankCount', {
                        count: data.results.length
                    })
                    data.results.forEach((bank) => {
                        $moduleEl.find("input[type='checkbox'][data-id='" + bank.id + "']").prop('checked', true);
                    });
                }
                broadcastInterestedBankList();
            }, error => {});
        }

        return {
            init: function() {
                element = context.getElement();
                moduleId = element.id;
                $ = context.getGlobal('jQuery');
                moduleConfig = context.getConfig();
                $moduleEl = $(element);
                commonService.bindOnLoadPromise().then(() => {
                    render();
                    context.broadcast('moduleLoaded', {
                        'name': 'banks',
                        'id': element.id
                    });
                });

            },
            onclick: function(event, element, elementType) {
                switch (elementType) {
                    case 'bank':
                        $moduleEl.find('[data-type=selectAllBanks]').prop('checked', false);
                        var dataset = element.dataset;
                        if (dataset.id) {
                            context.broadcast('bankOptionChanged', {
                                id: dataset.id,
                                selected: element.checked,
                                name: dataset.name
                            });
                            getSelectedbanks();
                            broadcastInterestedBankList();
                        }
                        break;
                    case 'getCall':
                        context.broadcast('homeLoanGetCall');
                        break;
                    case 'selectAllBanks':
                        toggleAllBankOptions($(element).prop('checked'));
                        break;
                    default:
                }
            },
            messages: ['banksForLoanChanged', 'getApprovedBankListChecked', 'selectBanks'],
            onmessage: function(name, data) {
                switch (name) {
                    case 'banksForLoanChanged':
                        if (data.count || data.count === 0) {
                            $moduleEl.find('.js-selectedBankCount').text(data.count);
                        }
                        break;
                    case 'getApprovedBankListChecked':
                        checkedApprovedBankFromList(data);
                        break;
                    case 'selectBanks':
                        $moduleEl.find("input[type='checkbox'][data-type=selectAllBanks]").prop('checked', false);
                        toggleAllBankOptions(false);
                        for (var i = 0; i < data.selectedBanks.length; i++) {
                            $moduleEl.find("input[type='checkbox'][data-id='" + data.selectedBanks[i] + "']").prop('checked', true);
                        }
                        broadcastInterestedBankList();
                }
            },
            /**
             * Destroys the module and clears references
             * @returns {void}
             */
            destroy: function() {
                element = null;
            }
        };
    });
});
