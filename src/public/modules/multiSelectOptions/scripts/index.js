'use strict';
define([
        "doT!modules/multiSelectOptions/views/multiSelectOptions",
        "doT!modules/multiSelectOptions/views/multiSelectCheckbox",
        "common/sharedConfig",
        "services/filterConfigService",
        "services/utils"
    ],
    function(optionsTemplate, checkboxTemplate,sharedConfig) {
        Box.Application.addModule("multiSelectOptions", function(context) {

            const FilterConfigService = context.getService('FilterConfigService'),
                UtilService = context.getService('Utils');
            const moduleConfig = context.getConfig();
            const moduleElement = context.getElement();
            const DATA_TYPE = 'type',
                CHECKBOX_VIEW = 'checkbox',
                OPTIONS_WRAPPER = 'multiple-select-option',
                APPLY_FILTER    = 'apply-filters';
            var pageData = UtilService.getPageData();
            var selectOptions, applyrequired,moduleEl,$;

            var _getCheckedOptions = function() {
                var values = [];
                var labels = [];
                for (let i = 0; i < selectOptions.length; i++) {
                    if (selectOptions[i].checked) {
                        let optData = $(selectOptions[i]).data()
                        values.push(optData.value || selectOptions[i].value);
                        labels.push(optData.label || selectOptions[i].label);
                    }
                }
                return {values, labels};
            };

            function render(cityId){
                var template = optionsTemplate,
                    data = $(moduleElement).data() || {},
                    defaultValue = (data.defaultvalue && data.defaultvalue.toString()) || undefined, 
                    templateFromPromise =  (moduleConfig && moduleConfig.templateFromPromise) || false,
                    newPageData = Object.assign({}, pageData, cityId?{cityId}:{});
                if (data.view == CHECKBOX_VIEW) {
                    template = checkboxTemplate;
                }
                applyrequired = data.applyrequired;
                FilterConfigService.renderFilter(moduleElement, newPageData, template, templateFromPromise,defaultValue).then(function(){
                    selectOptions = Box.DOM.queryAllData(moduleElement, DATA_TYPE, OPTIONS_WRAPPER);
                    var checkedOptions = _getCheckedOptions();
                    // if(data.type=="postedBy"){
                    //     handleSelectFilter(newPageData.cityId);
                    // }
                    context.broadcast('multiSelectOptionsInitiated', {
                        name: data.type,
                        value: checkedOptions.values,
                        label: checkedOptions.labels,
                        morefilter: data.morefilter,
                        id: moduleElement.id
                    });
               });
            }

            return {
                messages:['newCityOrLocalitySearched','searchListEmpty'],
                /**
                 * Initializes the module and caches the module element
                 * @returns {void}
                 */
                init: function() {
                    moduleEl = context.getElement();
                    $ = context.getGlobal('jQuery');
                    render();
                },

                onclick: function(event, element, elementType) {
                    var data = $(element).data() || {};

                    if(event.currentTarget.id !== moduleElement.id){
                        return;
                    }
                    let eventOptions = _getCheckedOptions();
                    let eventData = {
                        name: data.name,
                        morefilter: $(moduleElement).data('morefilter'),
                        value: eventOptions.values,
                        label: eventOptions.labels,
                        id: moduleElement.id,
                        selected: element.checked,
                        unselectedValue: data.value,
                        mCards: data.mcards,
                        moreFiltersActiveFlag : data.mcards ? true : false
                    };

                    switch (elementType) {
                        case OPTIONS_WRAPPER:
                        //TODO handle mcards case in a better way
                            if(data.mcards){
                                if(data.name == 'beds'){
                                    eventData.disableFilter = 'propertyType';
                                }
                                if(data.name == 'propertyType'){
                                    eventData.disableFilter = 'beds';
                                }
                                let eDataOpt = _getCheckedOptions();
                                eventData.value = eDataOpt.values;
                                eventData.label = eDataOpt.labels;
                            }
                            if(!applyrequired){
                                context.broadcast('multiSelectOptionsChanged', eventData);
                            }
                            let moduleData = $(moduleElement).data() || {};
                            context.broadcast('multiSelectOptionsSelected', {id: moduleEl.id,type: moduleData.type ,value: eventData.value.join(",")});
                            break;
                        case APPLY_FILTER:
                            if(applyrequired && eventData.value.length) {
                                eventData.outsidefilter = true;
                                let moduleData = $(moduleElement).data() || {};
                                context.broadcast('multiSelectOptionsChanged', eventData);
                                context.broadcast("jarvisFilterApplied",{id: moduleEl.id,type: moduleData.type ,value: eventData.value.join(",")});
                            }
                            break;
                    }
                },
                onmessage: function(name, data) {
                    switch(name){
                        case 'newCityOrLocalitySearched':
                        case 'searchListEmpty':
                            if($(moduleElement).data('iscitydependent') && data.selectedData && data.selectedData.cityId){
                                render(data.selectedData.cityId);
                            }
                            break;
                    }
                },

                /**
                 * Destroys the module and clears references
                 * @returns {void}
                 */
                destroy: function() {
                    selectOptions = null;
                }
            };
        });
    }
);
