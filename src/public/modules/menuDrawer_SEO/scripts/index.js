"use strict";
define([
    'services/utils',
    'services/loginService',
    'services/urlService',
    'services/commonService',
    'services/trackingService',
    'common/trackingConfigService',
    'common/sharedConfig',
    'behaviors/headerLoginBehaviour',
], function(utils, LoginService, urlService, commonService, trackingService, t, sharedConfig) { //jshint ignore:line
    Box.Application.addModule("menuDrawer_SEO", function(context) {
        
        var $, moduleEl, $moduleEl, simpleMenuDrawer,
            messages = ['hideMenuDrawerNextLevel','changeMenuDrawerTemplate', 'onLoggedIn', 'login', 'logout', 'loggedOut', 'notLoggedIn'],
            config = {}; //jshint ignore:line

        const LOGOUT_MENU_DRAWER = 'logout-menu',
            MENU_DRAWER_OPTION = 'md-opt',
            MENU_DRAWER_PREVIOUS = 'md-prv',
            MENU_DRAWER_NEXT = 'md-nxt';

        function init() {
            $ = context.getGlobal('jQuery');
            moduleEl = context.getElement();
            config = context.getConfig();
            // IIFE to prevent variable pollution
            (function() {
                if(utils.isMobileRequest()) return;
                var firstMenu = moduleEl.querySelector('ul#md-0');
                var vernacURI = utils.getVernacLanguage();
                if(firstMenu){
                    for(var i in sharedConfig.vernacTrans) {
                        if(vernacURI!==i) {
                            _appendMenuItem(sharedConfig.vernacTrans[i], {
                                data:{
                                    "noajax": true,
                                    "type": MENU_DRAWER_OPTION,
                                    "category": t.VERNAC_CATEGORY,
                                    "track-event": t.VERNAC_SWITCH,
                                    "url": utils.modVernacUrl(window.location.pathname, {
                                        langCode: i, 
                                        appendAfter: ''
                                    })
                                },
                                parent: firstMenu
                            });
                        }
                    }
                    if(vernacURI) {
                        _appendMenuItem('Read in English', {
                            data:{
                                "noajax": true,
                                "type": MENU_DRAWER_OPTION,
                                "category": t.VERNAC_CATEGORY,
                                "track-event": t.VERNAC_SWITCH,
                                "url": utils.modVernacUrl(window.location.pathname)
                            },
                            parent: firstMenu
                        });
                    }
                }
            })();
            context.broadcast('moduleLoaded', {
                name: 'menuDrawer_SEO'
            });
        }

        function _appendMenuItem(text, extraParams) {
            var extraParams = extraParams || {},
                dataAttr = extraParams.data || {},
                parent = extraParams.parent || moduleEl.querySelector('ul#md-0'),
                menuListElem = document.createElement('li'),
                menuSpanElem = document.createElement('span');
            menuSpanElem.appendChild(document.createTextNode(text));
            Object.entries(dataAttr).forEach(({0:key, 1: value})=>{
                menuListElem.setAttribute('data-'+key, value);
            });
            menuListElem.appendChild(menuSpanElem);
            parent.insertBefore(menuListElem, parent.firstChild);
        }

        function _setUserName(status, data){
            context.broadcast('setUserName', {
                moduleId: moduleEl.id,
                status,
                data
            });
        }

        function onclick(event, clickElement, elementType) {
            let dataset = $(clickElement).data();
            // Tracking Event for MenuDrawer Elements
            if(~elementType.search(/^md-/)) {
                let tEvent = dataset.trackEvent || t.CLICK_EVENT,
                    tLabel = dataset.trackLabel || (elementType==='md-p'?'BACK':clickElement.innerText),
                    properties = {};
                properties[t.LABEL_KEY] = tLabel;
                properties[t.CATEGORY_KEY] = dataset.category || t.MENU_DRAWER_CATEGORY;
                properties[t.SOURCE_MODULE_KEY] = dataset.source || t.MENU_DRAWER_MODULE;
                properties[t.LINK_NAME_KEY] = dataset.linkName;
                properties[t.LINK_TYPE_KEY] = dataset.linkType;

                trackingService.trackEvent(tEvent, properties);
            }
            switch (elementType) {
                // newMenuDrawer - Logic
                case MENU_DRAWER_OPTION:
                    if(dataset.url){
                        if(dataset.noajax) {
                            window.location.href = utils.prefixToUrl(dataset.url);
                        } else {
                            urlService.ajaxyUrlChange(dataset.url);
                        }
                    }
                    break;
                case MENU_DRAWER_NEXT: 
                    if(dataset.md) {
                        $("#menuDrawer_SEO #md-"+dataset.md).addClass('active');
                    }
                    break;
                case MENU_DRAWER_PREVIOUS: 
                    if(dataset.md) {
                        $("#menuDrawer_SEO #md-"+dataset.md).removeClass('active');
                    }
                    break;
                case LOGOUT_MENU_DRAWER:
                    LoginService.logoutUser();
                    $('body').removeClass('openmenu');
                    break;
            }
        }

        function onmessage(name, data) { //jshint ignore:line
            let $moduleEl = $(moduleEl);
            switch (name) {
                case 'hideMenuDrawerNextLevel':
                    // newMenuDrawer - Clear
                    $("#menuDrawer_SEO").children().removeClass('active');
                    $("#menuDrawer_SEO").children().first().addClass('active');
                    break;
                case 'changeMenuDrawerTemplate':
                    if (data.template) {
                        if (!simpleMenuDrawer) {
                            simpleMenuDrawer = $moduleEl.html();
                        }
                        require([data.template], function(layoutTemplate) {
                            $moduleEl.html(layoutTemplate(config));
                        });
                    } else {
                        if (simpleMenuDrawer) {
                            $moduleEl.html(simpleMenuDrawer);
                        }
                    }
                    commonService.stopAllModules(moduleEl);
                    commonService.startAllModules(moduleEl,[],null,null,null,true);
                    break;
                case 'onLoggedIn':
                case 'login':
                    _setUserName('login', data.response, data.roles);
                    $('body').removeClass('openmenu');
                    break;
                case 'logout':
                case 'loggedOut':
                    _setUserName('logout');
                    break;
            }
        }

        function destroy() {}

        return {
            behaviors : ['headerLoginBehaviour'],
            init,
            messages,
            destroy,
            onclick,
            onmessage
        };
    });
});
