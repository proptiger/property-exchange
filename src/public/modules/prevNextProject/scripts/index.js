define([
    'doT!modules/prevNextProject/views/index',
    'services/commonService'
], function(prevNextTemplate, commonService) {
    'use strict';
    Box.Application.addModule("prevNextProject", function(context) {
        var element, $, configData;

        function getTopAgents() {
            var top = configData.topAgents[1] && JSON.parse(configData.topAgents[1]);
            var topAgents = [];
            topAgents.push(configData.topAgents[0]);
            for (var i in top) {
                topAgents.push(top[i]);
            }
            return topAgents;
        }

        function setPrevProjects(prevProjects) {
            sessionStorage.setItem('prevProjects', JSON.stringify(prevProjects));
        }

        function getPrevProjects() {
            var prevProjects = sessionStorage.getItem('prevProjects') && JSON.parse(sessionStorage.getItem('prevProjects')) || [];
            return prevProjects;
        }

        function getPrevNextProject() {
        	var prevProjects = getPrevProjects();
            var similarProjects = configData.data;
            var topAgents = getTopAgents();
            var prevNextData = {
                nextProject: similarProjects[0],
                topAgents: topAgents,
                projectName: configData.fullName
            };
            if (prevProjects.length>0 && similarProjects) {
                var j = 0;
                for (var i = 0; i < prevProjects.length; i++) {
                    if (j<similarProjects.length && prevProjects[i].projectId == similarProjects[j].projectId) {
                        j++;
                        i = -1;
                    }
                }
                prevNextData.nextProject = similarProjects[j];
                prevNextData.prevProject = prevProjects[prevProjects.length - 1]
            }
            return prevNextData;
        }

        function render() {
            var prevNextData = getPrevNextProject();
            element.innerHTML = prevNextTemplate(prevNextData);
        }

        return {
            init: function() {
                element = context.getElement();
                $ = context.getGlobal('jQuery');
                configData = context.getConfig();
                render();
                context.broadcast('moduleLoaded', {
                    'name': 'prevNextProject',
                    'id': element.id
                });

                commonService.bindOnLoadPromise().then(()=>{
                    commonService.addImageObserver(element);
                });
            },

            onclick: function(event, element, elementType) {
                switch (elementType) {
                    case 'next-project':
                        var prevProjects = getPrevProjects();
                        var project = {
                            projectId: configData.projectId,
                            projectName: configData.fullName,
                            url: document.location.pathname,
                            image: configData.projectImage
                        };
                        prevProjects.push(project);
                        setPrevProjects(prevProjects);
                        context.broadcast('trackNextProjectBtnClick', {label: 'Footer', sourceModule:'Next'});
                        break;
                    case 'prev-project':
                        var prevProjects = getPrevProjects();
                        prevProjects.splice(-1, 1);
                        setPrevProjects(prevProjects);
                        context.broadcast('trackPrevProjectBtnClick', {label: 'Footer', sourceModule:'Previous'});
                        break;
                    case 'connect-now':
                        context.broadcast('callConnect', { type: 'connect-now' });
                        break;
                }
            },

            destroy: function() {
                element = null;
            }
        }
    })
})
