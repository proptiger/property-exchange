'use strict';
define([
	'services/commonService',
	'doT!modules/form-elements/views/textField',
	'doT!modules/form-elements/views/phoneField',
	'doT!modules/form-elements/views/otpField'
], (commonService, textFieldTemplate, phoneFieldTemplate, otpFieldTemplate) => {
	const MODULE_NAME="form-elements";

	Box.Application.addModule(MODULE_NAME, (context) => {

		let moduleConfig, moduleEl, $;

		const config = {
			ACTIVE_CLASS: "active",
			ERROR_CLASS: "error",
			ERROR_TIMEOUT: 5000,
			SELECTORS: {
				PARENT: "[data-parent]",
				COUNTRY_CODE: "[data-country-code]",
                FOCUS_SELECTOR: ".js-input"
			},
			VALIDATION_STRATEGIES: {
                "numbers-only": numberValidation
			},
            KEY_CODE_MAP: {
                PERIOD: 46,
                BACKSPACE: 8,
                HORIZONTAL_TAB: 9,
                ESCAPE: 27,
                CARRIAGE_RETURN: 13,
                DATA_LINK_ESCAPE: 16,
                SPECIAL_CHARACTER: 190,
                LOWERCASE_N: 110,
                CAPITAL_A: 65,
                CAPITAL_C: 67,
                CAPITAL_X: 88,
                SPECIAL_CHARACTERS: [35, 36, 37, 38, 39]
            }
		};

		function numberValidation(event){
			let charCode = (event.which) ? event.which : event.keyCode;
            if ([config.KEY_CODE_MAP.PERIOD, config.KEY_CODE_MAP.BACKSPACE, config.KEY_CODE_MAP.HORIZONTAL_TAB, config.KEY_CODE_MAP.ESCAPE, config.KEY_CODE_MAP.CARRIAGE_RETURN, config.KEY_CODE_MAP.SPECIAL_CHARACTER, config.KEY_CODE_MAP.LOWERCASE_N].indexOf(charCode) !== -1) {
                return true;
            } else if (charCode === config.KEY_CODE_MAP.CAPITAL_A && event.ctrlKey === true) {
                return true;
            } else if (charCode === config.KEY_CODE_MAP.CAPITAL_C && event.ctrlKey === true) {
                return true;
            } else if (charCode === config.KEY_CODE_MAP.CAPITAL_X && event.ctrlKey === true) {
                return true;
            } else if (config.KEY_CODE_MAP.SPECIAL_CHARACTERS.indexOf(charCode) !== -1) {
                return true;
            }

            //numbers and alphabets
            if ((event.shiftKey || (charCode < 48 || charCode > 57)) && (charCode < 96 || charCode > 105)) {
                event.preventDefault();
            }
		}

		function onfocusin() {
            $(moduleEl).find(config.SELECTORS.PARENT).addClass(config.ACTIVE_CLASS);
        }

        function onfocusout() {
        	let value = $(moduleEl).find(moduleConfig.valueSelector).val();
            if (!value && !moduleConfig.isAlwaysActive) {
                $(moduleEl).find(config.SELECTORS.PARENT).removeClass(config.ACTIVE_CLASS);
            }
        }

        function onkeydown(event){
        	if(moduleConfig.validationStrategy && config.VALIDATION_STRATEGIES[moduleConfig.validationStrategy]){
        		config.VALIDATION_STRATEGIES[moduleConfig.validationStrategy](event);
        	}
        }

        function _updateCountryCode(data){
        	if(data.name == "countries"){
        		let params = data.params;
        		try{
        			params = window.decodeURI(params);
        			params = JSON.parse(params);
        		} catch(e){
        			// make india as default if something goes wrong
        			params = {
        				code: "+91",
        				value: 1,
        				label: "India"
        			};
        		}
        		$(moduleEl).find(config.SELECTORS.COUNTRY_CODE).html(params.code);
        		context.broadcast("country-code-changed", params);
        	}
        }

        function _showError(data){
        	if(data.id == moduleEl.id) {
        		$(moduleEl).find(config.SELECTORS.PARENT).addClass(config.ERROR_CLASS);
	        	setTimeout(function(){
	        		$(moduleEl).find(config.SELECTORS.PARENT).removeClass(config.ERROR_CLASS);
	        	}, config.ERROR_TIMEOUT);
    	    }
        }

		function _renderTemplate(moduleConfig){
			let template;
			switch(moduleConfig.type){
				case "text":
				case "email":
					template = textFieldTemplate;
					break;
				case "phone":
					template = phoneFieldTemplate;
					break;
				case "otp":
					template = otpFieldTemplate;
			}
			let html = template(moduleConfig);
			$(moduleEl).find(moduleConfig.placeholder).html(html);
			commonService.startAllModules(moduleEl);
			let value = $(moduleEl).find(moduleConfig.valueSelector).val();
            if (value) {
                $(moduleEl).find(config.SELECTORS.PARENT).addClass(config.ACTIVE_CLASS);
            }
		}

		function init(){
			moduleEl = context.getElement();
            $ = context.getGlobal("jQuery");
			moduleConfig = context.getConfig();
			_renderTemplate(moduleConfig);
			context.broadcast("moduleLoaded", {
				name: MODULE_NAME
			});
		}

		let onmessage = {
			"singleSelectDropdownInitiated": _updateCountryCode,
			"singleSelectDropdownChanged": _updateCountryCode,
			"form-validation-error": _showError
		};

		function destroy(){
			moduleEl = moduleConfig = null;
		}

        function onclick(event, element, elementType){
            let elementData = $(element).data();
            switch(elementType){
                case "input-field":
                    if(elementData.wrapper){
                        $(element).find(config["SELECTORS"]["FOCUS_SELECTOR"]).focus();
                    }
                    break;
            }
        }

		return {
			init,
			destroy,
			onfocusin,
			onfocusout,
			onkeydown,
			onmessage,
            onclick
		};

	});
});