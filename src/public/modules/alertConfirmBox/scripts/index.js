"use strict";
define([
        "doT!modules/alertConfirmBox/views/index"
    ],
    function(template) {
        Box.Application.addModule("alertConfirmBox", function(context) {
            const OPEN_ALERTCONFIRM_BOX = 'open_alertconfirm_box',
                CANCEL = 'closePopup',
                SUBMIT = 'submit',
                messages = [OPEN_ALERTCONFIRM_BOX];

            var moduleEl,
                jsonDump = {},
                successCallback;

            function init() {
                moduleEl = context.getElement();
            }

            function onmessage(name, data) {
                switch (name) {
                    case OPEN_ALERTCONFIRM_BOX:
                        if (data.moduleId == moduleEl.id) {
                            moduleEl.innerHTML = template(data.displayData);
                            jsonDump = data.jsonDump;
                            if (typeof data.successCallback === "function") {
                                successCallback = data.successCallback;
                            }
                        }
                        break;
                }
            }

            function onclick(event, element, elementType) {
                switch (elementType) {
                    case CANCEL:
                        context.broadcast('alertConfirm_Cancel', {
                            moduleEl: moduleEl.id,
                            jsonDump: jsonDump
                        });
                        context.broadcast('popup:close');
                        break;
                    case SUBMIT:
                        context.broadcast('alertConfirm_Submit', {
                            moduleId: moduleEl.id,
                            jsonDump: jsonDump
                        });
                        context.broadcast('popup:close', {
                            callback: successCallback
                        });
                        break;
                }
            }

            return {
                init,
                onclick,
                messages,
                onmessage,
            };
        });
    }
);
