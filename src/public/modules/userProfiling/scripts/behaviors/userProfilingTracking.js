'use strict';
define([
    'common/trackingConfigService',
    'services/trackingService'
], function(t, trackingService) {
    Box.Application.addBehavior('userProfilingTracking', function(context) {
        
        var element = context.getElement();

        var _getLabel = function(isSticky) {
            if (isSticky) {
                return '_sticky';
            } else {
                return '_popup';
            }
        }

        var _trackEvent = function(eventName, data) {
            var trackingObj = {};
            if ([t.M_PROFILE_COMPLETE, t.M_PROFILE_API_FAILURE].indexOf(eventName) > -1) {
                trackingObj[t.LABEL_KEY] = data.label;
            } else {
                trackingObj[t.LABEL_KEY] = _getLabel(data.isSticky);
                trackingObj[t.QUESTION] = data.question;
                trackingObj[t.ANSWER] = data.answer;
                trackingObj[t.ADDITIONAL_CD53] = data.source;
            }
            trackingObj[t.CATEGORY_KEY] = t.PROFILING_INFO;
            trackingService.trackEvent(eventName, trackingObj);
        }

        return {
            messages: ['trackUserProfilingCardSeen', 'trackUserProfilingCardAnswered', 'trackUserProfilingCardClose', 'trackUserProfilingClickPrevious', 'trackUserProfilingClickSkip', 'trackUserProfilingCompleteness', 'trackUserProfilingApiFailure'],

            init: function() {},

            onmessage: function(name, data) {
                switch (name) {
                    case 'trackUserProfilingCardSeen':
                        _trackEvent(t.CARD_SEEN, data);
                        break;
                    case 'trackUserProfilingCardAnswered':
                        _trackEvent(t.CARD_ANSWERED, data);
                        break;
                    case 'trackUserProfilingCardClose':
                        _trackEvent(t.CLOSE_EVENT, data);
                        break;
                    case 'trackUserProfilingClickPrevious':
                        _trackEvent(t.CLICK_PREVIOUS, data);
                        break;
                    case 'trackUserProfilingClickSkip':
                        _trackEvent(t.CLICK_SKIP, data);
                        break;
                    case 'trackUserProfilingCompleteness':
                        _trackEvent(t.M_PROFILE_COMPLETE, data);
                        break;
                    case 'trackUserProfilingApiFailure':
                        _trackEvent(t.M_PROFILE_API_FAILURE, data);
                        break;
                }
            },

            destroy: function() {}
        };
    });
});
