/**
 * This modules is being used to ask some questions to users when he submits a lead.
 * This modules is currently inititiated twice on SERP page, one in lead form popup and other above the listings as a sticky.
 * If a uses skips the questions on lead from, we show the questions on serp above lead form.
 *
 * JIRA: MAKAAN - 3975
 *
 * Module config
 *      isPopup: <boolean> : true when this module is initiated on serp page and false otherwise
 */
'use strict';
define([
        "doT!modules/userProfiling/views/index",
        "doT!modules/userProfiling/views/dateRange",
        "doT!modules/userProfiling/views/radio",
        "doT!modules/userProfiling/views/checkbox",
        "doT!modules/userProfiling/views/multiple",
        "doT!modules/userProfiling/views/textBox",
        "doT!modules/userProfiling/views/textArea",
        "doT!modules/userProfiling/views/action",
        "doT!modules/userProfiling/views/typeahead",
        "doT!modules/userProfiling/views/multipleTypeahead",
        "services/localStorageService",
        "services/leadProfilingService",
        'services/utils',
        'services/experimentsService',
        'services/feedbackService',
        'common/sharedConfig',
        "modules/userProfiling/scripts/behaviors/userProfilingTracking",
        'services/commonService'
    ],
    function (mainTemplate, dateRangeTemplate, radioTemplate, checkboxTemplate, multipleTemplate, textBoxTemplate, textAreaTemplate, actionTemplate, typeaheadTemplate, multipleTypeaheadTemplate, localStorageService, leadProfilingService, UtilService, experimentsService, feedbackService,sharedConfig) {
        Box.Application.addModule("userProfiling", function (context) {
            const 
                DATA_TARGET = 'target',
                QUESTION_PLACEHOLDER = 'question-placeholder',
                ACTION_PLACEHOLDER = 'action-placeholder',
                HEADING_PLACEHOLDER = 'heading-placeholder',
                SUB_HEADING_PLACEHOLDER = 'subheading-placeholder',
                PERCENTAGE_PLACEHOLDER = 'percentage-placeholder',
                PROGRESS_PLACEHOLDER = 'progress-placeholder',
                NEXT_QUESTION = 'next-ques',
                PREVIOS_QUESTION = 'prev-ques',
                SKIP_QUESTION = 'skip-ques',
                CLOSE = 'close',
                WRAPPER = 'wrapper',
                CALENDER_MONTH = 'calendar-month',
                SELECTOR = {
                    STICKY: '.js-sticky-lead-profiling',
                    STICKY_RESET: '.js-sticky-reset',
                    STICKY_BOTTOM: 'sticky-lead-profiling-btm',
                    PROFILE_DATA: '#js-profile-data',
                    QUESTION_DATA: '#js-ques-data',
                    PARENT_QUESTION: '#js-parent-question',
                    TEXT_BOX: '.js-input-textbox',
                    TEXT_AREA: '.js-input-textarea',
                    ERROR_MSG: '.error-msg',
                    DATE_RANGE: '.date-range',
                    LOADER: '.js-main-loading',
                    ERROR: '#errorOcurred',
                    DISABLE_SELECT: 'disable-select',
                    LEAD_POPUP: '#leadOverviewPopup',
                    CLOSE_LEAD_POPUP: '.js-close-lead-popup',
                    CLOSE_M_PROFILE_POPUP: '.js-close-mprofile-popup',
                    TYPEAHEAD_ID: "mProfileTypeahead",
                    TYPEAHEAD_MODULE_ID: "mprofile-filter-typeahead",
                    TYPEAHEAD_LOCATION_TYPE: "LOCALITY",
                    MULTIPLE_TYPEAHEAD: "js-multiple-typeahead"
                },
                CommonService = context.getService('CommonService'),
                ERROR_MSG_CLEAR_TIME_DELAY = 4000,
                ERROR_MSG= 'Some error occured... please try again',
                FINAL_QUESTION_ID = 16,
                HEADING_CONFIG = {
                    headingTxt :'Complete your mProfile',
                    subheadingTxt : 'To get personalised search experience',
                    headingTxtStickyBottom: 'Let us help you in finding your perfect home!',
                    subheadingTxtStickyBottom: 'Complete your mProfile to kick start',
                    headingFinalTxt: 'Thank You'
                },
                MONTH_NAMES = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
                MONTHS_TO_SHOW = 3,
                WEEK_DAYS = 7,
                MAX_WEEK = 4,
                MKN_LEAD_USER_ID = 'mkn_lead_userid',
                STICKY_SESSION_KEY = 'm_profile_session',
                STICKY_HR_LIMIT_MILLISEC = 172800000, // 48 Hours
                API_MIDORA = 'MIDORA'; 
            var pageData = UtilService.getPageData(),
                entityType = 'user',
                saleType = UtilService.capitalize(pageData.listingType),
                answeredMap = {},
                questionCnt = 0,
                QUESTION_TYPE ={},
                isMobileRequest = UtilService.isMobileRequest(),
                headingElement, subheadingElement, questionElement, percentageElement, progressElement, wrapperElement, actionElement, moduleConfig, moduleElement, entityId, typeAheadInputAtrr = [], typeAheadValues = {}, typeAheadSelectedValues = [];
            
            // Updates the percentage element in html
            var _updatePercentage = function (percentage) {
                percentage = Math.round(percentage) + "%";
                $(progressElement).css({
                    width: percentage
                });
                percentageElement.innerHTML = percentage;
            };

            var _showError = function (message, tmpElement) {
                if (!tmpElement) {
                    tmpElement = $(moduleElement).find(SELECTOR.ERROR);
                }
                tmpElement.text(message);
                _clearMessage(tmpElement);
            }
            
            var _clearMessage = function(tmpElement) {
                setTimeout(() => {
                    tmpElement.text('');
                }, ERROR_MSG_CLEAR_TIME_DELAY);
            }

            var _showQuestionError = function(questionId) {
                var elem = $(moduleElement).find(`#error-${questionId}`);
                _showError(elem.data('message'), elem);
            }

            var _startAllModules = function() {
                CommonService.startAllModules(moduleElement);
            }

            var _stopAllModules = function () {
                CommonService.stopAllModules(moduleElement);
            }
            var _getDateRangeList = function() {
                let currentDate = new Date(),
                    currentMonth = currentDate.getMonth(),
                    currentYear = currentDate.getFullYear(),
                    year, dateData, monthIndex, value, options = [], currentMonthIndex;
                    currentDate.setDate('01');
                for (var i = 0; i < MONTHS_TO_SHOW; i++) {
                    currentMonthIndex = i + currentMonth;
                    year = currentYear;
                    if (currentMonthIndex > 11){
                        currentMonthIndex = currentMonthIndex - 12;
                        year += 1;
                    }
                    dateData = new Date(currentDate.setMonth(currentMonthIndex))
                    monthIndex = dateData.getMonth();
                    value = `${MONTH_NAMES[monthIndex]}, ${year}`;
                    options.push({ key: (monthIndex+1), val: value});
               }
               return options;
            }

            var _getCurrentWeek = function () {
                let currentDate = new Date(),
                    curDate = currentDate.getDate(),
                    week = Math.ceil(curDate/WEEK_DAYS);
                if (week > MAX_WEEK) {
                    week = MAX_WEEK;
                }
                return week;
            }

            var generateChildQuestionHtml = function(response){
                let childQuestionHtml  = '',
                    questionDetail = response.questionDetail;

                questionDetail.childQuestions && questionDetail.childQuestions.forEach(function(childQuestion){
                    switch (childQuestion.questionTypeId){
                        case QUESTION_TYPE.TYPEAHEAD:
                            typeAheadInputAtrr.push(childQuestion.attributeQuestion);
                            childQuestionHtml = childQuestionHtml + typeaheadTemplate(childQuestion);
                            break;
                        case QUESTION_TYPE.TEXT:
                            childQuestionHtml = childQuestionHtml + textBoxTemplate(childQuestion);
                            break;
                    }
                });

                return  childQuestionHtml;
            }

            var _getDateRangeTemplate = function (response) {
                let result = response.questionDetail;
                let quesId = result.questionId;
                let selectedMonth = (response.responseDetail && response.responseDetail[quesId] && response.responseDetail[quesId].value) || '';
                let selectedAnswer = (response.responseDetail && response.responseDetail[quesId] && response.responseDetail[quesId].answerOptionId) || '';
                let dateOptions = _getDateRangeList(selectedMonth);
                let currentWeek = _getCurrentWeek();

                let disableFlag = true;
                if (selectedMonth && selectedMonth != dateOptions[0].val){
                    disableFlag = false;
                }
                if (result) {
                    result.dateOptions = dateOptions;
                    result.dateOptionSelected = selectedMonth || dateOptions[0].val;
                    result.ansOptionSelected = selectedAnswer || '';
                    result.currentWeek = currentWeek;
                    result.currentWeekIndex = (currentWeek - 1);
                    result.disableFlag = disableFlag;
                }
                return dateRangeTemplate(result);
            }

            // var showHideCloseBtn = function(showCloseBtn=true){
            //     if (!moduleConfig.isPopup || isMobileRequest) {
            //         return;
            //     }
            //     if (showCloseBtn) {
            //         $(SELECTOR.CLOSE_LEAD_POPUP).removeClass('hide');
            //         $(moduleElement).find(SELECTOR.CLOSE_M_PROFILE_POPUP).removeClass('hide');
            //     } else {
            //         $(SELECTOR.CLOSE_LEAD_POPUP).addClass('hide');
            //         $(moduleElement).find(SELECTOR.CLOSE_M_PROFILE_POPUP).addClass('hide');
            //     }
            // }

            var renderNextQuestionHtml  = function (response){
                if (response && response.questionDetail) {
                    actionElement.innerHTML = actionTemplate(response);
                    let quesId = response.questionDetail.questionId;
                    let stickyBottom = isMobileRequest && experimentsService.hasExperimentVariation("makaan_mpsticky_pos", "bottom");
                    if (quesId == FINAL_QUESTION_ID) {
                        headingElement.innerHTML = HEADING_CONFIG.headingFinalTxt;
                        subheadingElement.innerHTML = `Your profile is now ${response.profileDetail.completeness}% complete`;
                        //showHideCloseBtn(true);
                    } else {
                        headingElement.innerHTML = stickyBottom ? HEADING_CONFIG.headingTxtStickyBottom : HEADING_CONFIG.headingTxt;
                        subheadingElement.innerHTML = stickyBottom ? HEADING_CONFIG.subheadingTxtStickyBottom : HEADING_CONFIG.subheadingTxt;
                        //showHideCloseBtn(false);
                    }
                    answeredMap[quesId] = false;
                    switch (response.questionDetail.questionTypeId) {
                        case QUESTION_TYPE.DATE_RANGE:
                            questionElement.innerHTML = _getDateRangeTemplate(response);
                            break;
                        case QUESTION_TYPE.RADIO:
                            questionElement.innerHTML = radioTemplate(response.questionDetail);
                            break;
                        case QUESTION_TYPE.CHECKBOX:
                            questionElement.innerHTML = checkboxTemplate(response.questionDetail);
                            break;
                        case QUESTION_TYPE.MULTIPLE:
                            let childQuestionHtml = generateChildQuestionHtml(response);
                            response.questionDetail.childQuestionHtml = childQuestionHtml;
                            questionElement.innerHTML = multipleTemplate(response.questionDetail);
                            break;
                        case QUESTION_TYPE.TEXT:
                            response.questionDetail.isParentQuestion = true; 
                            questionElement.innerHTML = textBoxTemplate(response.questionDetail);
                            break;
                        case QUESTION_TYPE.TEXT_AREA:
                            questionElement.innerHTML = textAreaTemplate(response.questionDetail);
                            break;
                        case QUESTION_TYPE.MULTIPLE_TYPEAHEAD:
                            typeAheadInputAtrr.push(response.questionDetail.attributeQuestion);
                            if (response.questionDetail.respValue){
                                response.questionDetail.preFilledData = {
                                    tagsArray: response.questionDetail.respValue,
                                    locationType: SELECTOR.TYPEAHEAD_LOCATION_TYPE,
                                    moduleId: SELECTOR.TYPEAHEAD_MODULE_ID,
                                    typeAheadId: SELECTOR.TYPEAHEAD_ID
                                }
                            } else {
                                response.questionDetail.preFilledData = '';
                            }
                            questionElement.innerHTML = multipleTypeaheadTemplate(response.questionDetail);
                            break;
                        default:
                            break;
                    }
                }
                _stopAllModules();
                _startAllModules();
                if (response.profileDetail && response.profileDetail.completeness) {
                    _updatePercentage(response.profileDetail.completeness);
                }
                $(moduleElement).find(SELECTOR.LOADER).addClass('hide');
                localStorageService.setItem(sharedConfig.MPROFILE_CARD_SEEN,true);
                localStorageService.setItem(sharedConfig.MPROFILE_ANSWERED,false);
                context.broadcast("trackUserProfilingCardSeen", _getTrackingData(response.questionDetail.questionId, ''));
            }

            var renderNextQuestion = function (profilingQuestionId, answerOptionId, questionOrder = 'next') {
                let questionObj = {saleType, entityType, entityId, questionOrder};
                if (profilingQuestionId){
                    questionObj.profilingQuestionId = profilingQuestionId;
                }
                if (answerOptionId) {
                    questionObj.answerOptionId = answerOptionId;
                }
                questionElement.innerHTML = '';
                $(moduleElement).find(SELECTOR.LOADER).removeClass('hide');
                leadProfilingService.getLeadProfileQuestion(questionObj).then(function (response) {
                    if (response.profileDetail.profilingQuestionnaireId == null) {
                        _profileComplete();
                    } else {
                        renderNextQuestionHtml(response);     
                    }
                },function (err) {
                    _trackUserProfilingApiFailure(API_MIDORA);
                    $(moduleElement).find(SELECTOR.LOADER).addClass('hide');
                    _showError(ERROR_MSG);
                    if (!$(moduleElement).find(SELECTOR.ERROR).length){
                        context.broadcast("userProfilingCompletedViaPopup");
                    }
                });
            }

            var _profileComplete = function(){
                _trackCompletenessEvent('Yes');
                $(moduleElement).find(SELECTOR.LOADER).addClass('hide');
                _hideSticky();
                if (moduleConfig.isPopup) {
                    context.broadcast("userProfilingCompletedViaPopup");
                }
            }

            var _getRequestData = function(dataArr) {
                let requestData = {}, profilingResponses = [], obj = {},profileData = $(SELECTOR.PROFILE_DATA).data();

                for (let i = 0; i < dataArr.length; i++) {
                    obj = {
                        "createdBy": entityId,
                        "entityId": entityId,
                        "profilingQuestionId": dataArr[i].quesId
                    }
                    if (dataArr[i].ansId) {
                        obj.answerOptionId = dataArr[i].ansId;
                    }  
                    if (dataArr[i].ansVal) {
                        obj.value = dataArr[i].ansVal;
                    }
                    if (dataArr[i].skipQuestion){
                        obj.skipQuestion = true;
                    }
                    if (dataArr[i].id){
                        obj.id = dataArr[i].id;
                    }
                    profilingResponses.push(obj);
                }
                requestData =  {
                    "entityId": entityId,
                    "profilingQuestionnaireId": profileData.questionnaireid,
                    "profilingResponses": profilingResponses
                };
                if (profileData.profileid) {
                    requestData.id = profileData.profileid;
                }
                return requestData;
            }

            // Return the tracking data for a particular question
            var _getTrackingData = function (trackQuesLabel='', trackAns='') {
                return {
                    isSticky: moduleConfig.isSticky || !moduleConfig.isPopup,
                    question: trackQuesLabel,
                    answer: trackAns,
                    source: moduleConfig.source || ''
                };
            };

            var _trackQuestionAnswered = function (questionCount, questionId,trackAns) {
                let trackQuesLabel = `${questionCount}_${questionId}`;
                context.broadcast("trackUserProfilingCardAnswered", _getTrackingData(trackQuesLabel, trackAns));
                localStorageService.setItem(sharedConfig.MPROFILE_ANSWERED,true);
            }

            var _saveQuestion = function(requestData){
                $(moduleElement).find(SELECTOR.LOADER).removeClass('hide');
                leadProfilingService.saveLeadProfileQuestion(requestData).then((response) => {
                    $(moduleElement).find(SELECTOR.LOADER).addClass('hide');
                    if (response && response.profileDetail && response.profileDetail.profilingQuestionnaireId == null) {
                        _profileComplete();
                    } else {
                        renderNextQuestion(response.profilingResponses[0].profilingQuestionId, response.profilingResponses[0].answerOptionId, 'next');
                    }
                },
                function (err) {
                    $(moduleElement).find(SELECTOR.LOADER).addClass('hide');
                    answeredMap[requestData.profilingResponses[0].profilingQuestionId] = false;
                    _showError(ERROR_MSG);
                    _trackUserProfilingApiFailure(API_MIDORA);
                });
            }

            // Toggle the display of sticky as per status
            var _toggleStickyDisplay = function(status) {
                _collapseStickyUI(true);
                if (status) {
                    $(wrapperElement).addClass('show-lp-wrap');
                } else if (!moduleConfig.isPopup) {
                    $(wrapperElement).removeClass('show-lp-wrap');
                }  
            };

            // Collapse the sticky module as per status
            var _collapseStickyUI = function(status) {
                if (status && !moduleConfig.isPopup) {
                    $(wrapperElement).addClass('on-scroll');
                } else {
                    $(wrapperElement).removeClass('on-scroll');
                }
            };

            var _hideSticky = function(){
                $(SELECTOR.STICKY).addClass('hide');
                $(SELECTOR.STICKY).find(SELECTOR.STICKY_RESET).html('');
            }

            var _showSticky = function () {
                $(SELECTOR.STICKY).removeClass('hide');
                if (isMobileRequest && UtilService.getPageData('moduleName') == "serp") {
                    experimentsService.experimentElementClass(document.querySelector(SELECTOR.STICKY), "makaan_mpsticky_pos", "bottom", SELECTOR.STICKY_BOTTOM);
                }
                _toggleStickyDisplay(true);
            }

            var _updateStickySession = function(){
                entityId = entityId || localStorageService.getItem(MKN_LEAD_USER_ID);
                let sessionObj = localStorageService.getItem(STICKY_SESSION_KEY) || {};
                sessionObj[entityId] = new Date().getTime();
                localStorage.setItem(STICKY_SESSION_KEY, JSON.stringify(sessionObj));
            }

            var _checkStickySession = function() {
                let sessionObj = localStorageService.getItem(STICKY_SESSION_KEY);
                let check = true;
                if (sessionObj && sessionObj[entityId]) {
                    let nowMilliSeconds = new Date().getTime(),
                        timeDiff = nowMilliSeconds - sessionObj[entityId];
                    if (timeDiff < STICKY_HR_LIMIT_MILLISEC){
                        check = false;
                    }
                }
                return check;
            }

            var _renderSticky = function () {
                renderNextQuestion();
                _collapseStickyUI(false);
            }

            var _renderPopup = function (){
                questionElement.innerHTML = '';
                renderNextQuestion();
                _hideSticky();
                _toggleStickyDisplay(false);
                _openPopup('mProfilePopup', true);
            }

            var _mProfileOpen = function(drip=false) {
                entityId = _getEntityId();
                if (entityId) {
                    let questionObj = {saleType, entityType, entityId, questionOrder:'next'},
                        isComplete = 'No', mProfileRendered = false;
                    questionElement.innerHTML = '';
                    leadProfilingService.getLeadProfileQuestion(questionObj).then(function (response) {
                        if (response && response.profileDetail && response.profileDetail.profilingQuestionnaireId == null) {
                            _hideSticky();
                            isComplete = 'Yes';
                        } else {
                            if (isMobileRequest) {
                                CommonService.bindOnModuleLoad('lead', function (id) {
                                    let data = {};
                                    if (drip) {
                                        data.isPopupRequest = true;
                                    }
                                    context.broadcast("userProfilingPopupOpenViaMobile", data);
                                }, ['lead-popup']);
                            } else {
                                CommonService.bindOnModuleLoad('popup', function () {
                                    _renderPopup();
                                });
                                if (drip) {
                                    setTimeout(function () {
                                        _hideSticky();
                                    }, 200);
                                }
                            }
                            mProfileRendered = true;
                        }
                        if (drip) {
                            _trackCompletenessEvent(isComplete);
                        }
                    },                    
                    function (err) {
                        _trackUserProfilingApiFailure(API_MIDORA);
                        _hideSticky();
                    }).always(function(){
                        if(!mProfileRendered) {
                            feedbackService.getFeedbackStatus();
                        }
                    });
                }
            }

            var _resetQuestionWrap = function () {
                $(SELECTOR.STICKY_RESET).html('');
            }

            var _trackCompletenessEvent = function(isComplete = 'No'){
                context.broadcast("trackUserProfilingCompleteness", {
                    label: isComplete
                });
            }

            var _trackUserProfilingApiFailure = function (api) {
                context.broadcast("trackUserProfilingApiFailure", {
                    label: api
                });
            }
            
            var _trackCompletenessStatus = function(){
                let entityId = _getEntityId(),
                isComplete = 'No',
                questionObj = {saleType, entityType, entityId, questionOrder:'next'};
                if (entityId){
                    leadProfilingService.getLeadProfileQuestion(questionObj).then(function (response) {
                        if (response.profileDetail.profilingQuestionnaireId == null) {
                            isComplete = 'Yes';
                        }
                        _trackCompletenessEvent(isComplete);
                    },function (err) {
                        _trackUserProfilingApiFailure(API_MIDORA);
                    });
                }
            }

            var _openPopup = function (id) {
                context.broadcast('popup:open', {
                    id: id,
                    options: {
                        config: {
                            modal: true
                        }
                    }
                });
            }

            var _closePopup = function(){
                context.broadcast('popup:close', {});
            }

            // Checks if sticky module is collapsed or not
            var _isStickyUICollapsed = function() {
                return $(wrapperElement).hasClass('on-scroll');
            };

            var _isLeadPopupOpen = function(){
                return $(SELECTOR.LEAD_POPUP).hasClass('show-popup');
            }

            var _checkQuestion = function(){
                entityId = entityId || _getEntityId()
                if (!_checkStickySession() && isMobileRequest) {
                    return;
                }
                let questionObj = { saleType, entityType, entityId, questionOrder:'next'};
                questionElement.innerHTML = '';
                leadProfilingService.getLeadProfileQuestion(questionObj).then(function (response) {
                    if (response && response.profileDetail && response.profileDetail.profilingQuestionnaireId == null) {
                        _hideSticky();
                    } else {
                        leadProfilingService.checkUserPhoneVerified({userId:entityId}).then(function (response) {
                            if (response.isVerified) {
                                _showSticky();
                            } else {
                                _hideSticky();
                            }
                        }, function () {
                            _hideSticky();
                        });
                    }
                },
                function (err) {
                    _trackUserProfilingApiFailure(API_MIDORA);
                    _hideSticky();
                });
            }

            var _validateTextBox = function (offLocation) {
                if (offLocation) {
                    $(moduleElement).find('.office_error').html('');
                } else {
                    let msg = '',
                        txtData = $(moduleElement).find(SELECTOR.TEXT_BOX).data(),
                        quesId = txtData.quesid,
                        answerVal = $(moduleElement).find('[data-type=' + txtData.type + '] input ').val().trim();
                    if (answerVal == '') {
                        msg = $(moduleElement).find(`#error-${quesId}`).data('message');
                    }
                    $(moduleElement).find(`#error-${quesId}`).html(msg);
                }
            }
            var setHtmlElements  = function(){
                questionElement = Box.DOM.queryData(moduleElement, DATA_TARGET, QUESTION_PLACEHOLDER);
                actionElement = Box.DOM.queryData(moduleElement, DATA_TARGET, ACTION_PLACEHOLDER);
                headingElement = Box.DOM.queryData(moduleElement, DATA_TARGET, HEADING_PLACEHOLDER);
                subheadingElement = Box.DOM.queryData(moduleElement, DATA_TARGET, SUB_HEADING_PLACEHOLDER);
                percentageElement = Box.DOM.queryData(moduleElement, DATA_TARGET, PERCENTAGE_PLACEHOLDER);
                progressElement = Box.DOM.queryData(moduleElement, DATA_TARGET, PROGRESS_PLACEHOLDER);
                wrapperElement = Box.DOM.queryData(moduleElement, DATA_TARGET, WRAPPER);
            }

            var handleNextQuestionClick = function (element){
                let requestData, questionData, quesId, answer, mergeAns, trackAns, error = false, ansArr = [],
                    questionType = $(moduleElement).find(SELECTOR.QUESTION_DATA).data('type'),
                    currentQuestionId = $(moduleElement).find(SELECTOR.QUESTION_DATA).data('questionid');
                    $(moduleElement).find(SELECTOR.ERROR_MSG).html('');
                switch (questionType) {
                    case QUESTION_TYPE.RADIO:
                        questionData = $(element).data();
                        answer = questionData.ansid;
                        ansArr.push({
                            id: questionData.respid,
                            quesId: questionData.quesid,
                            ansId: answer
                        });
                        trackAns = questionData.answer;
                        break;
                    case QUESTION_TYPE.CHECKBOX:
                        questionData = $(moduleElement).find(SELECTOR.QUESTION_DATA).data();
                        quesId = questionData.questionid;
                        var checkedElm = $(moduleElement).find('input[name="checkbox"]:checked');
                        if (checkedElm.length) {
                            checkedElm.each(function (e, v) {
                                answer = v.id;
                                mergeAns = mergeAns ? `${mergeAns} - ${answer}` : answer;
                                ansArr.push({
                                    quesId: quesId,
                                    quesType: questionData.type,
                                    ansId: answer
                                });
                            });
                        }
                        else {
                            _showQuestionError(quesId);
                            error = true;
                        }
                        trackAns = mergeAns;
                        break;
                    case QUESTION_TYPE.DATE_RANGE:
                        questionData = $(element).data();
                        answer = $(moduleElement).find(`input:radio${SELECTOR.DATE_RANGE}:checked`).val();
                        ansArr.push({
                            id: questionData.respid,
                            quesId: questionData.quesid,
                            ansId: questionData.ansid,
                            ansVal: answer
                        });
                        trackAns = `${answer} - ${questionData.answer}`;
                        break;
                    case QUESTION_TYPE.TEXT:
                        questionData = $(moduleElement).find(SELECTOR.TEXT_BOX).data();
                        answer = $(moduleElement).find('[data-type=' + questionData.type + '] input').val().trim();
                        if (!answer) {
                            $(moduleElement).find(SELECTOR.ERROR_MSG).html($(moduleElement).find(SELECTOR.ERROR_MSG).data('message'));
                            error = true;
                        }
                        ansArr.push({
                            id: questionData.respid,
                            quesId: questionData.quesid,
                            ansVal: answer
                        });
                        trackAns = answer;
                        break;
                    case QUESTION_TYPE.TEXT_AREA:
                        questionData = $(moduleElement).find(SELECTOR.TEXT_AREA).data();
                        answer = $(moduleElement).find(SELECTOR.TEXT_AREA).val().trim();
                        ansArr.push({
                            id: questionData.respid,
                            quesId: questionData.quesid,
                            ansVal: answer
                        });
                        trackAns = answer;
                        break;
                    case QUESTION_TYPE.MULTIPLE:
                        let childElementData;
                        questionData = $(SELECTOR.PARENT_QUESTION).data();
                        ansArr.push({
                            id: questionData.respid,
                            quesId: questionData.quesid,
                            ansVal: ''
                        });
                        $(moduleElement).find(SELECTOR.TEXT_BOX).each(function () {
                            childElementData = $(this).data();
                            if (childElementData && childElementData['questionType'] == 'typeAhead') {
                                answer = typeAheadValues[childElementData['inputName']] || '';
                            } else {
                                answer = $(moduleElement).find('[data-type=' + childElementData['type'] + '] input').val().trim();
                            }
                            mergeAns = mergeAns ? `${mergeAns} - ${answer}` : answer;
                            quesId = childElementData['quesid'];
                            let id = childElementData['respid'];
                            if (answer != '') {
                                ansArr.push({
                                    id,
                                    quesId,
                                    ansVal: answer
                                });
                            } else {
                                _showQuestionError(quesId);
                                error = true;
                            }
                        });
                        trackAns = mergeAns;
                        break;
                    case QUESTION_TYPE.MULTIPLE_TYPEAHEAD:
                        questionData = $(moduleElement).find(SELECTOR.QUESTION_DATA).data();
                        quesId = questionData.questionid;
                        answer = typeAheadValues[questionData.attributeQuestion] || '';
                        if (answer.length) {
                            ansArr.push({
                                id: questionData.respid,
                                quesId: quesId,
                                quesType: questionData.type,
                                ansVal: JSON.stringify(answer)
                            });
                        } else {
                            _showQuestionError(quesId);
                            error = true;
                        }
                        trackAns = questionData.attributeQuestion;
                        break;
                }
                if (ansArr.length && !error) {
                    requestData = _getRequestData(ansArr);
                    if (!answeredMap[currentQuestionId]) {
                        answeredMap[currentQuestionId] = true;
                        _saveQuestion(requestData);
                    }
                    questionCnt++;
                    _trackQuestionAnswered(questionCnt, currentQuestionId, trackAns);
                    event.stopPropagation();
                }
            }

            function _getEntityId(){
                let userId = localStorageService.getItem(MKN_LEAD_USER_ID) || '';
                userId = typeof(userId) =="object" ? '' : userId;
                return userId;
            }

            function _disableWeek(weekIndex, currentMonth){
                $(moduleElement).find('.multiselectlbl').each(function(index,obj){
                    let labelData = $(obj).data();
                    let ansId = `#ans_${labelData.ansid}`;
                    if (labelData.weekindex < weekIndex && currentMonth) {
                        $(obj).addClass(SELECTOR.DISABLE_SELECT);
                        $(moduleElement).find(ansId).prop("disabled", true);
                        $(moduleElement).find(ansId).prop("checked", false);
                    } else {
                        $(obj).removeClass(SELECTOR.DISABLE_SELECT);
                        $(moduleElement).find(ansId).prop("disabled", false);
                    }
                });
            }

            return {
                messages: ['userProfilingUpdateAnswerCount', 'userProfilingPopupClosed', 'userProfilingCompletedViaPopup', 'serpPageScrolled', 'typeAheadOverlayClicked', 'searchResultClicked', 'searchResultCanceled', 'resetSelectedList', 'propertyPageScrolled', 'userProfilingPopupOpen', 'mProfilePopupOpen', 'leadContactNumberEntered', 'trackMProfileCompleteStatus', 'mProfileApiFailure'],
                behaviors: ['userProfilingTracking'],
                /**
                 * Initializes the module and caches the module element
                 * @returns {void}
                 */
                init: function() {
                    moduleConfig = context.getConfig();
                    moduleElement = context.getElement();
                    entityId = _getEntityId();
                    moduleElement.innerHTML = mainTemplate();
                    QUESTION_TYPE = leadProfilingService.getQuestionType();
                    setHtmlElements();
                    if (moduleConfig.isLeadPopup) {
                        renderNextQuestion();
                        _hideSticky();
                        _toggleStickyDisplay(false);
                    } else {
                        entityId = _getEntityId();
                        if (entityId) {
                            _checkQuestion();
                        } else {
                            _hideSticky();
                        }
                    }
                    context.broadcast('moduleLoaded', {
                        'name': 'userProfiling',
                        'id': moduleElement.id
                    });
                },
                onkeyup: function (event, element, elementType) {
                    if (!$(element).hasClass(SELECTOR.MULTIPLE_TYPEAHEAD)){
                        _validateTextBox();
                    }
                },
                onclick: function(event, element, elementType) {
                    let currentQuestionId = $(moduleElement).find(SELECTOR.QUESTION_DATA).data('questionid');
                    switch (elementType) {
                        case NEXT_QUESTION:
                            if ($(moduleElement).find(element).hasClass(SELECTOR.DISABLE_SELECT))
                                return false;
                            if (!answeredMap[currentQuestionId]){
                                handleNextQuestionClick(element);
                                answeredMap[currentQuestionId] = true;
                            }
                            break;
                        case PREVIOS_QUESTION:
                            answeredMap[currentQuestionId] = false;
                            renderNextQuestion(currentQuestionId, '', 'previous');
                            context.broadcast("trackUserProfilingClickPrevious", _getTrackingData(currentQuestionId, ''));
                            break;    
                        case SKIP_QUESTION:
                            answeredMap[currentQuestionId] = true;
                            _saveQuestion(_getRequestData([{
                                quesId: currentQuestionId,
                                skipQuestion: true
                            }]));
                            context.broadcast("trackUserProfilingClickSkip", _getTrackingData(currentQuestionId, ''));
                            event.stopPropagation();
                            break;
                        case CLOSE:
                            if (isMobileRequest) {
                                _updateStickySession();
                                _hideSticky();
                            } else {
                                if (moduleConfig.isPopup){
                                    context.broadcast('userProfilingPopupClosed');
                                } else {
                                    if ($(wrapperElement).hasClass('on-scroll')) {
                                        _renderSticky();
                                    } else {
                                        _toggleStickyDisplay(false);
                                    }
                                }
                            }
                            context.broadcast("trackUserProfilingCardClose",_getTrackingData(currentQuestionId,''));
                            break;
                        case WRAPPER:
                            if (moduleConfig.isLeadCard || moduleConfig.isPopup) {
                                return;
                            }
                            if (isMobileRequest) {
                                context.broadcast("userProfilingPopupOpenViaMobile");
                                return;
                            }
                            if (!_isStickyUICollapsed()) {
                                return;
                            }
                            if (!entityId) {
                                entityId = _getEntityId();
                            }
                            _renderSticky();
                            break;
                        case CALENDER_MONTH:
                            let weekIndex = _getCurrentWeek()-1;
                            let currentDate = new Date();
                            let currentMonth = currentDate.getMonth() + 1;
                            let dateData = $(element).data();
                            if (dateData.value == currentMonth){
                                _disableWeek(weekIndex, true);
                            } else {
                                _disableWeek(weekIndex, false);
                            }
                            break;
                    }
                },

                onmessage: function (name, data) {
                    switch (name) {
                        case 'mProfilePopupOpen':
                            if (moduleConfig.askSecond) {
                                if (_isLeadPopupOpen()){
                                    return;
                                }
                                moduleConfig.source = data.source || '';
                                localStorageService.setItem(MKN_LEAD_USER_ID, data.userId);
                                _mProfileOpen(true);
                            } else {
                                _resetQuestionWrap();
                            }
                            break;
                        case 'userProfilingPopupOpen':
                            if (moduleConfig.askSecond && _getEntityId()) {
                                if (_isLeadPopupOpen()) {
                                    return;
                                }
                                _mProfileOpen();
                            }
                            break;
                        // incase the serp or property page is scrolled; triggered by serp or property page module
                        case 'propertyPageScrolled':
                        case 'serpPageScrolled':
                            _collapseStickyUI(true); 
                            break;
                        // trigged by lead module when popup is closed when user tries to close the popup while the question is open
                        // or if he has answered the questions
                        case 'userProfilingPopupClosed':  
                            if (!moduleConfig.isPopup) {
                                _toggleStickyDisplay(true);
                            } else {
                                let questionId = $('#js-ques-data').data('questionid')||'';
                                context.broadcast("trackUserProfilingCardClose", _getTrackingData(questionId));
                                _closePopup();
                            }
                            _checkQuestion();
                            // break not added intentionally 
                        // triggered when a user has answered all the questions that were asked in one stretch 
                        case 'userProfilingCompletedViaPopup':
                            if (!moduleConfig.isPopup) {
                                _toggleStickyDisplay(true);
                            } else {
                                _toggleStickyDisplay(false);
                            }
                            break;
                        case 'typeAheadOverlayClicked':
                        case 'searchResultClicked':
                            if(data.config && data.config.inputName && typeAheadInputAtrr.indexOf(data.config.inputName) > -1){
                                if (data.config.questionType == QUESTION_TYPE.MULTIPLE_TYPEAHEAD){
                                    typeAheadValues[data.config.inputName] = typeAheadValues[data.config.inputName]||[];
                                    if (data.dataset && typeAheadSelectedValues.indexOf(data.dataset.tagid) == -1) {
                                        typeAheadSelectedValues.push(data.dataset.tagid);
                                        typeAheadValues[data.config.inputName].push(data.dataset);
                                    }
                                } else {
                                    typeAheadValues[data.config.inputName] = data && data.dataset ? data.dataset.val : '';
                                    _validateTextBox(typeAheadValues[data.config.inputName]);
                                }
                            }
                            break;
                        case 'searchResultCanceled':
                            if (data.config && data.config.inputName && typeAheadInputAtrr.indexOf(data.config.inputName) > -1) {
                                if (data.config && data.config.questionType && data.config.questionType == QUESTION_TYPE.MULTIPLE_TYPEAHEAD) {
                                    let tagid = data.dataset.tagid,
                                        index = typeAheadSelectedValues.indexOf(tagid),
                                        tempArr = typeAheadValues[data.config.inputName];
                                    if (index > -1){
                                        typeAheadSelectedValues.splice(index, 1);
                                        for (let i = 0; i < tempArr.length; i++){
                                            if (tempArr[i].tagid == tagid) {
                                                typeAheadValues[data.config.inputName].splice(i, 1);
                                            }
                                        }
                                    }
                                }
                            }
                            break;
                        case 'resetSelectedList':
                            if (data.config && data.config.inputName && typeAheadInputAtrr.indexOf(data.config.inputName) > -1) {
                                if (data.config.questionType == QUESTION_TYPE.MULTIPLE_TYPEAHEAD) {
                                    typeAheadValues[data.config.inputName] = typeAheadValues[data.config.inputName]||[];
                                    if (data.tagsArray && data.tagsArray.length) {
                                        data.tagsArray.forEach(tag => {
                                            if (typeAheadSelectedValues.indexOf(tag.tagid) == -1){
                                                typeAheadSelectedValues.push(tag.tagid);
                                                typeAheadValues[data.config.inputName].push(tag.dataset);
                                            }
                                        });
                                    }
                                }
                            }
                            break;
                        case 'leadContactNumberEntered':
                            if (moduleConfig.isPopup) {
                                _trackCompletenessStatus();
                            }
                            break;
                        case 'trackMProfileCompleteStatus':
                            if(moduleConfig.isPopup){
                                _trackCompletenessEvent(data.isComplete);
                            }
                            break;
                        case 'mProfileApiFailure':
                            let api = data.api || API_MIDORA;
                            _trackUserProfilingApiFailure(api);
                            break;
                    }
                },
                /**
                 * Destroys the module and clears references
                 * @returns {void}
                 */
                destroy: function() {}
            };
        });
    }
);