'use strict';

define([
	'doT!modules/prompt/views/serp-filter-prompt',
	'services/trackingService',
	'common/trackingConfigService',
	'services/utils',
	'services/filterConfigService',
	'services/localStorageService',
	'services/sessionService'
], (serpFilterPromptTemplate, trackingService, t, utils, filterConfigService, localStorageService, sessionService) => {
	const MODULE_NAME = "prompt";
	Box.Application.addModule(MODULE_NAME, (context) => {

		let moduleEl, moduleConfig, $, config, pageData = utils.getPageData();

		const MAP = {
			storageKey: 'serp-prompt',
			PROMPT_CONTAINER: '[data-prompt]',
			OUT_OF_VIEW_CLASS: "out-of-view"
		};

		const configMap = {
			"serp-filter-prompt": {
				template: function(){
					let templateData = {
						listingType: pageData.listingType
					};
					if(pageData.projectId){
						templateData.project = `${pageData.builderName} ${pageData.projectName}`;
					}
					if(pageData.localityId){
						templateData.locality = pageData.localityName;
					}
					if(pageData.cityId){
						templateData.city = pageData.cityName;
					}
					let appliedFilters = filterConfigService.getSelectedQueryFilterKeyValObj();
					if(appliedFilters.propertyType){
						templateData.filters = {
							propertyType: appliedFilters.propertyType.split(',').map((v) => {
								return utils.ucword(v.replace("-", " "));
							}).join(", ")
						};
					}
					if(appliedFilters.budget){
						templateData.filters = templateData.filters || {};
						appliedFilters.budget = appliedFilters.budget.split(",").map((v) => {
							return utils.formatNumber(v, { seperator : window.numberFormat }).trim();
						}).filter((v) => {
							return v;
						});
						if(appliedFilters.budget.length == 1){
							templateData.filters.budget = `above ${appliedFilters.budget[0]}`;
						} else if(appliedFilters.budget[0]){
							templateData.filters.budget = `between ${appliedFilters.budget[0]} and ${appliedFilters.budget[1]}`;
						} else {
							templateData.filters.budget = `below ${appliedFilters.budget[1]}`;
						}
					}
					return serpFilterPromptTemplate(templateData);
				},
				showPrompt: function(){
					let sessionId = sessionService.getSessionId().c;
					let promptStatus = localStorageService.getItem(MAP["storageKey"]) || {};
					let flag = window && window.AB_TEST && window.AB_TEST.serpPrompt;
					if(!promptStatus[sessionId] && utils.isMobileRequest() && flag){
						$(moduleEl).find(MAP["PROMPT_CONTAINER"]).removeClass('hide');
						$(moduleEl).find(MAP["PROMPT_CONTAINER"]).removeClass(MAP["OUT_OF_VIEW_CLASS"]);
						promptStatus = {
							[sessionId]: true
						};
						localStorageService.setItem(MAP["storageKey"], promptStatus);
					}
				},
				closePrompt: function(){
					$(moduleEl).find(MAP["PROMPT_CONTAINER"]).addClass('hide');
					$(moduleEl).find(MAP["PROMPT_CONTAINER"]).addClass(MAP["OUT_OF_VIEW_CLASS"]);
				},
				submit: function(){
					context.broadcast("open-filters");
					this.closePrompt();
				},
				track: function(element){
					let dataset = $(element).data();
					let eventName = t['CLICK_EVENT'];
					let properties = {};
					properties[t['CATEGORY_KEY']] = t['SERP_NOTIFICATION'];
					properties[t['LABEL_KEY']] = dataset.label;
					trackingService.trackEvent(eventName, properties);
				}
			}
		};

		function init(){
			moduleEl = context.getElement();
			moduleConfig = context.getConfig();
			$ = context.getGlobal("jQuery");
			context.broadcast("moduleLoaded", {
				name: MODULE_NAME
			});
			config = configMap[moduleConfig.type];
			let html = config.template();

			$(moduleEl).find(moduleConfig.placeholder).html(html);
			config.showPrompt();
		}

		function onclick(event, element, elementType){
			switch(elementType){
				case "close":
					config.closePrompt(element);
					config.track(element);
					break;
				case "submit":
					config.submit(element);
					config.track(element);
			}
		}

		return {
			init,
			onclick
		};
	});
});
