'use strict';
define([
        "doT!modules/emiCalculator/views/index",
        "common/utilFunctions"
    ],
    function(template,utilFunctions) {
        Box.Application.addModule("emiCalculator", function(context) {
            var config;
            var EMI_INPUT_SELECTOR = '.js-incomeText, .js-tenureText, .js-interestText';
            var emiCalculatorConfig = {
                    interestRateConfig: {
                        start: [8.5],
                        'range': {
                            'min': {
                                value: 1
                            },
                            'max': {
                                value: 20
                            }
                        }
                    },
                    tenureConfig: {
                        start: [20],
                        'range': {
                            'min': {
                                value: 1
                            },
                            'max': {
                                value: 30
                            }
                        }
                    },
                    incomeConfig: {
                        start: [100000],
                        'range': {
                            'min': {
                                value: 100000
                            },
                            'max': {
                                value: 999999999
                            }
                        },
                        min_income:100000
                    }
                },
            commonService = context.getService('CommonService');

            var moduleEl, $moduleEl,
                emiData = {};
            
            var _utilityKeys = function(event){
                var charCode = (event.which) ? event.which : event.keyCode;
                if(event.target.id==="emi_interest" && (charCode===110 || charCode===190) ){ //exception of decimal for interest field
                    return true;
                }
                if ([8,9,27,13].indexOf(charCode) !== -1) {
                    return true;
                } else if(charCode === 65 && event.ctrlKey === true) {
                    return true;
                } else if(charCode === 67 && event.ctrlKey === true) {
                    return true;
                }else if(charCode === 88 && event.ctrlKey === true){
                    return true;
                }else if(charCode >= 35 && charCode <= 39) {
                     return true;
                }
                return false;
            };
            var _isNumberKey = function(event) {
                if(_utilityKeys(event)){
                    return true;
                }
                var charCode = (event.which) ? event.which : event.keyCode;
                if ((event.shiftKey || (charCode < 48 || charCode > 57)) && (charCode < 96 || charCode > 105)) {
                    event.preventDefault();
                }
            };

            var _toCurrencyFormat = function(num){
                if(!num){
                    return 0;
                }
                if(typeof num === 'number'){
                    num = num.toString();
                }
                num = num.replace(/,/g,'');
                return num.replace(/(\d)(?=(\d\d)+\d$)/g, "$1,");
            };
            
            var _validRange = function(target,newVal){
                switch(target){
                    case 'emi_income':
                        if(!isValidRange(emiCalculatorConfig.incomeConfig.range,newVal,emiData.emi_income)){
                            setTextError('income');
                            return false;
                        } else {
                            removeTextError('income');
                        }
                    break;
                    case 'emi_tenure':
                        if(!isValidRange(emiCalculatorConfig.tenureConfig.range,newVal,emiData.emi_tenure)){
                            setTextError('tenure');
                            return false;
                        } else {
                            removeTextError('tenure');
                        }
                    break;
                    case 'emi_interest':
                        if(!isValidRange(emiCalculatorConfig.interestRateConfig.range,newVal,emiData.emi_interest)){
                            setTextError('interest');
                            return false;
                        } else {
                            removeTextError('interest');
                        }
                    break;
                }
                return true;
                function isValidRange(range,newVal){
                    var maxAllowed = range.max.value;
                    var minAllowed = range.min.value;
                    if(minAllowed<=newVal && newVal<=maxAllowed){
                        return true;    
                    } else {
                        return false;    
                    }
                }
               
            };
            function setTextError(target){
                $moduleEl.find('.js-'+ target + 'Text').addClass('input-range-error');
            }
            function removeTextError(target){
                $moduleEl.find('.js-'+ target + 'Text').removeClass('input-range-error');
            }
           
            function init() {
                moduleEl = context.getElement();
                $moduleEl = $(moduleEl);
                config = context.getConfig() || {};
                render(config.price);
                context.broadcast('emiCalculatorLoaded'); 
            }
            function render(price){
                if(!price){
                    price=1000000 //default value to get emi calculator render
                }
                emiCalculatorConfig.incomeConfig.start[0] = price;                
                moduleEl.innerHTML = template(emiCalculatorConfig);
                commonService.startAllModules(moduleEl);
                emiData = {
                    'emi_interest': emiCalculatorConfig.interestRateConfig.start[0],
                    'emi_tenure': emiCalculatorConfig.tenureConfig.start[0],
                    'emi_income': emiCalculatorConfig.incomeConfig.start[0]
                };
                _calculateEMI(emiData.emi_income);
                _updateAllTargetText();
                $moduleEl.find(EMI_INPUT_SELECTOR).on("keydown",_isNumberKey);
                let incomeInput = $moduleEl.find('.js-incomeText');
                incomeInput.on('keyup',()=>{
                    incomeInput.val(_toCurrencyFormat(incomeInput.val()));
                });
            }

            function _calculateEMI(loanAmount) {
                var emiPerMonth = utilFunctions.getEmi(loanAmount , emiData.emi_interest, emiData.emi_tenure, emiData.emi_income);
                context.broadcast('emiDataChanged', {
                    emiPerMonth: emiPerMonth, 
                    totalPayement: emiPerMonth * emiData.emi_tenure * 12, 
                    loan:loanAmount
                });
            }
           
            function _updateAllTargetText(){
                for(var item in emiData){
                    _updateTargetText(item,emiData[item]);
                }
            }
            function _updateTargetText(id,value){
                if(id=='emi_income'){
                    value = _toCurrencyFormat(value);
                }
                $moduleEl.find('#'+id).val(value);
            }
            var onmessage = function(name, data) {
            };
          
            var onchange = function(event, element, elementType) {
                switch (elementType) {
                    case 'emiInputs':
                        $(element).blur();
                        var dataAttr = $(element).data();  
                        var newVal;
                        if(event.target.id==='emi_rate'){
                            newVal = parseFloat(element.value);
                        } else {
                            newVal = parseInt(element.value.replace(/,/g,''));
                        }
                        
                        if(!_validRange(event.target.id,newVal)){
                            return;
                        }
                        emiData[event.target.id] = newVal;
                        _calculateEMI(emiData['emi_income']);
                        break;
                    }
            };
            return {
                messages: [],
                init,
                onmessage,
                onchange
            };
        });
    }
);