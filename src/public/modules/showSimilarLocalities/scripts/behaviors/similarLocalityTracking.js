define([
    'common/trackingConfigService',
    'services/trackingService',
    'services/utils'
], function(t, trackingService,utilService) {
    'use strict';
    Box.Application.addBehavior('similarLocalityTracking', function(context) {

        var messages = [
            "similarLocationClicked"
        ];

        var moduleId = context.getElement().id;

        const LOCALITY_ID = 'LOCALITY',
            SUBURB_ID = 'SUBURB',
            CITY_ID = 'CITY';

        var onmessage = function(msgName, data) {
            if (data.moduleEl && data.moduleEl.id != moduleId) {
                return;
            }

            let category = t.TYPEAHEAD_CATEGORY,
                label, event, name, sourceModule, nonInteraction,
                localityId,
                suburbId,
                entityType,
                cityId,
                tagsCount;

            switch (msgName) {
                case 'similarLocationClicked':
                    let dataset = data.element.dataset;
                    event = t.SUGGESTED_LOCALITY_EVENT;
                    name = dataset.entityid + '_' + dataset.entityname;
                    localityId = dataset.localityId;
                    tagsCount = 1;
                    sourceModule = t.CLICK_MODULE;
                    entityType = dataset.tagtype;
                    if(entityType === LOCALITY_ID){
                        localityId = dataset.entityid;
                    } else if(entityType === SUBURB_ID){
                        suburbId = dataset.entityid;
                    } else if(entityType === CITY_ID){
                        cityId = dataset.entityid;
                    }
                    break;
            }

            let properties = {};
            properties[t.CATEGORY_KEY] = category;
            properties[t.NAME_KEY] = name;
            properties[t.SOURCE_MODULE_KEY] = sourceModule;
            properties[t.NON_INTERACTION_KEY] = nonInteraction;
            properties[t.CITY_ID_KEY] = cityId;
            properties[t.EMAIL_KEY] = tagsCount;
            properties[t.LOCALITY_ID_KEY] = localityId;
            properties[t.SUBURB_ID_KEY] = suburbId;

            trackingService.trackEvent(event, properties);
        };

        return {
            messages: messages,
            onmessage: onmessage,
            init: function() {},
            destroy: function() {}
        };
    });
});
