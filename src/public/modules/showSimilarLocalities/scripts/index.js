"use strict";
define([
    'doT!modules/showSimilarLocalities/views/index',
    'common/sharedConfig',
    'services/apiService',
    'services/utils',
    'modules/showSimilarLocalities/scripts/behaviors/similarLocalityTracking',
    ], function(tagsHtml, sharedConfig, apiService, utilService){
        const MODULE_NAME = 'showSimilarLocalities';
        Box.Application.addModule(MODULE_NAME, function(context){
        let moduleEl, $moduleEl, moduleConfig, $locationTagContainer = "", $tagsContainer,tempPromises = [],
        config = {
            SELECTOR: {
                TAG_CONTAINER: "[data-tag-container]",
                TAG_PARENT: ".js-tag-parent"
            }
        };
        var _parseTopLocalities = function(localities, alreadySelected) {

            let category = utilService.getPageData('listingType'),
                urlKey = category && category.toLowerCase() == 'rent' ? 'rentUrl' : 'buyUrl',
                currentCity = alreadySelected && alreadySelected.length ? alreadySelected[0].dataset.city.toLowerCase() : '';

            // Add top localities
            let topLocalities = [];
            for (let i = 0; i < localities.length; i++) {
                let locality = localities[i],
                    cityName = locality.suburb && locality.suburb.city ? locality.suburb.city.label : '';

                if(currentCity && cityName.toLowerCase() != currentCity){
                    continue;
                }

                let obj = {
                    id: "TYPEAHEAD-LOCALITY-" + locality.localityId,
                    type: "LOCALITY",
                    showType: 'location',
                    displayText: `${locality.label} - ${cityName}`,
                    redirectUrl: locality[urlKey],
                    latitude: locality.latitude,
                    longitude: locality.longitude,
                    city: cityName,
                    entityId: locality.localityId,
                    entityName: locality.label
                };
                topLocalities.push(obj);
            }
            return topLocalities;
        };

        var _filterResults = function(results, alreadySelected) {
            var newResults = [], selectedIds = [];
            selectedIds = alreadySelected.map(item => item.tagid);
            newResults = results.filter(item => {
                return item.id && selectedIds.indexOf(item.id) == -1;
            });
            return newResults;
        };

        function getLocalitiesSuburbs(selectedValues){
            let refLocation = selectedValues[selectedValues.length - 1],
                lat = refLocation.dataset.latitude,
                lon = refLocation.dataset.longitude;
            
            abortAllPendingAPis(); // abort all pending apis for search
            if (lat && lon) {
                tempPromises.push(apiService.get(sharedConfig.clientApis.nearbyLocalities({lat,lon}).url));
            }
            return tempPromises[0].then(function (response) {
                // Add nearby localities
                let nearbyLocalities = _parseTopLocalities(response.data, selectedValues);
                        // filter duplicate results
                    nearbyLocalities = _filterResults(nearbyLocalities, selectedValues);
                    return nearbyLocalities;
                });
        }

        function abortAllPendingAPis() {
            for (let i = 0; i < tempPromises.length; i++) {
                try {
                    tempPromises[i].abort();
                } catch (e) {
                    console.log('in catch inside searchService function abortAllPendingAPis of abort', e);
                }
            }
            tempPromises = [];
        }

        function renderLocalities(selectedValues){
            getLocalitiesSuburbs(selectedValues).then(localityList => {
                if(!localityList.length){
                    $locationTagContainer.addClass('hide');
                } else {
                    $tagsContainer.find(config.SELECTOR.TAG_PARENT).html(tagsHtml({localityList}));
                    $locationTagContainer.removeClass("hide");
                }
            });
        }

        return {
            behaviors: ['similarLocalityTracking'],
            init(){
                moduleEl = context.getElement();
                $moduleEl = $(moduleEl);
                moduleConfig = context.getConfig();
                $locationTagContainer = $moduleEl.find(moduleConfig.placeHolder);
                $tagsContainer = $moduleEl.find(config.SELECTOR.TAG_CONTAINER);
                context.broadcast("moduleLoaded", {
                    name: MODULE_NAME
                });
                context.broadcast("getRecentlySelectedLocation", {
                    moduleEl
                });
            }, 
            destroy(){
                moduleEl = $moduleEl = moduleConfig = null;
            },

            messages: ["recentlyAddedLocation", "hideSimilarLocalities"],

            onmessage(name, data){
                switch(name){
                    case "recentlyAddedLocation":
                        if(['homePageSearchBox', 'headerSearchBox'].indexOf(data.typeAheadId) > -1  && data.tagsArray && data.tagsArray.length > 0 && data.tagsArray.length < 4 && (data.moduleId == moduleEl.id || utilService.getPageData('moduleName') == "home")){
                            if(data.locationType == "LOCALITY"){
                                renderLocalities(data.tagsArray);
                            }
                        }
                        break;
                    case "hideSimilarLocalities":
                        $tagsContainer.find(config.SELECTOR.TAG_PARENT).html('');
                        $locationTagContainer.addClass("hide");
                        break;
                }
            },
            onclick: function(event, element, elementType){
                switch(elementType){
                    case 'nearby':
                        context.broadcast("similarLocationClicked", {
                            moduleEl,
                            element
                        });
                        break;
                }
            }
        }
    })
})