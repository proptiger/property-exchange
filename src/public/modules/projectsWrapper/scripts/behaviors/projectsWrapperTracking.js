define([
    'common/trackingConfigService',
    'services/trackingService',
    'services/urlService'
], function(t, trackingService) {
    'use strict';
    Box.Application.addBehavior('projectsWrapperTracking', function() {

        var messages = [
            'trackNearbyLocalitiesClicked',
            'trackKnowMoreAboutSerp',
            'trackZeroResultsFound'
        ];

        var onmessage = function(msgName, data) {

            let category, event, label, projectStatus, budget, bhk, nonInteraction;

            switch (msgName) {
                case 'trackNearbyLocalitiesClicked':
                    category = t.NEARBY_SERP_CATEGORY;
                    event = t.NEARBY_SUGGESTIONS_EVENT;
                    label = t.UNSELECT_LABEL;
                    if (data.isChecked) {
                        label = t.SELECT_LABEL;
                    }
                    break;

                case 'trackKnowMoreAboutSerp':
                    category = t.HEADLINE_CATEGORY;
                    event = t.HEADLINE_SERP_EVENTS[data.trackType];
                    bhk = data.beds;
                    budget = data.budget;
                    projectStatus = data.projectStatus;
                    break;

                case 'trackZeroResultsFound':
                    category = t.ZERO_RESULTS_CATEGORY;
                    event = t.ERROR_EVENT;
                    nonInteraction = 1;
                    break;
            }

            let properties = {};
            properties[t.CATEGORY_KEY] = category;
            properties[t.LABEL_KEY] = label;
            properties[t.BHK_KEY] = t.makeValuesUniform(bhk,t.BHK_MAP) ;
            properties[t.BUDGET_KEY] = budget;
            properties[t.PROJECT_STATUS_KEY] = t.makeValuesUniform(projectStatus,t.PROJECT_STATUS_NAME) ;
            properties[t.NON_INTERACTION_KEY] = nonInteraction;

            trackingService.trackEvent(event, properties);
        };

        return {
            messages: messages,
            onmessage: onmessage,
            init: function() {},
            destroy: function() {}
        };
    });
});