/**
 * @fileoverview Module for the wrapper around Project cards
 * @author [Ashish Yadav]
 */

/*global Box*/

define([
    'services/commonService',
    'services/utils',
    'services/urlService',
    'services/filterConfigService',
    'common/sharedConfig',
    'modules/pagination/scripts/index' , // to always load client side pagination module
    'modules/projectsWrapper/scripts/behaviors/projectsWrapperTracking'

], function(commonService, utils, urlService, filterConfigService) {
    "use strict";
    Box.Application.addModule('projectsWrapper', function(context) {

        var projectsWrapper, projectCount = 0,
            isZeroPage;

       // var page, index, wH, offsetArray, hT;

        var _updateProjects = function(html) {
            $(projectsWrapper).html(html);
            var modulesInWrapper = Box.DOM.queryAll(projectsWrapper, '[data-module]');
            for(var i=0;i<modulesInWrapper.length;i++) {
                Box.Application.start(modulesInWrapper[i]);
            }

            utils.triggerPageInteractive();
            commonService.loadLazyModules(projectsWrapper);
        };

        var trackEmptyProjectWrapper = function(){
            context.broadcast('trackZeroResultsFound');
        };

        return {
            messages: ['updateProjects'],
            behaviors: ['projectsWrapperTracking'],
            init: function() {
                projectsWrapper = context.getElement();
                projectCount = context.getConfig('projectCount');
                isZeroPage = context.getConfig('isZeroPage');
                if(!projectCount){
                    trackEmptyProjectWrapper();
                }
            },
            destroy: function() {
                projectsWrapper = null;
            },
            onmessage: function(name, data) {
                if(name === 'updateProjects') {
                    projectCount = data.projectCount;
                    if(!projectCount){
                        trackEmptyProjectWrapper();
                    }
                    _updateProjects(data.propcardHtml);
                    if(data && typeof data.callback === 'function'){
                        data.callback();
                    }
                }
            },
            onclick:function(event,element,elementType){
                let dataset=$(element).data();
                switch(elementType){
                    case "knowMoreAbout":
                        if(dataset.trackType){
                            context.broadcast('trackKnowMoreAboutSerp', $.extend({trackType: dataset.trackType}, filterConfigService.getTrackPageFilters()));
                        }
                        break;
                }

            }
        };
    });
});
