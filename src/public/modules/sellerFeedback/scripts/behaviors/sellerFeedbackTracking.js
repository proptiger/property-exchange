define([
    'common/trackingConfigService',
    'services/trackingService'
], function(t, trackingService) {
    'use strict';
    Box.Application.addBehavior('sellerFeedbackTracking', function(context) {

        var element;
        const messages = [
            'trackFeedbackSubmit',
            'trackFeedbackError',
            'trackRecommend',
            'trackFeedbackUpdate',
            'trackFeedback_DealClose',
            'trackMissingSellerInfo'
        ];

        var init = function() {
            element = context.getElement();
        };

        var onmessage = function(name, data) {
            if (data.id === element.id) {
                let properties = {};
                let event,category,sourcemodule, label;
                category='buyer_feedback'
                switch (name) {
                    case 'trackFeedbackSubmit':
                        event = t.SUBMIT_EVENT;
                        sourcemodule=data.leadId;
                        break;
                    case 'trackFeedbackError':
                        event = t.ERROR_EVENT;
                        sourcemodule=data.leadId;
                        break;
                    case 'trackRecommend':
                        event= t.CLICK_EVENT;
                        sourcemodule= data.recommend;
                        break;
                    case 'trackFeedbackUpdate':
                        event = t.SUBMIT_EVENT;
                        sourcemodule=data.leadId;
                        label = data.label;
                        break;
                    case 'trackFeedback_DealClose':
                        event = t.CLICK_EVENT;
                        sourcemodule=data.buyerId;
                        label = data.label;
                        break;
                    case 'trackMissingSellerInfo':
                        event = t.SUBMIT_EVENT;
                        sourcemodule=data.buyerId;
                        label = data.sellerName + '-' + data.sellerNumber;
                        break;
                    default:
                        return;
                }
                properties[t.CATEGORY_KEY] = category;
                properties[t.SOURCE_MODULE_KEY] = sourcemodule;
                if(label){
                    properties[t.LABEL_KEY] = label;
                }
                trackingService.trackEvent(event, properties);
            }
        };

        return {
            messages,
            onmessage,
            init,
            destroy: function() {}
        };
    });
});