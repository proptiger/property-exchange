define(['common/sharedConfig','services/apiService'], function(sharedConfig) {
    'use strict';
    Box.Application.addBehavior('imageUploadBehavior', function(context) {

        var moduleEl,$, $moduleEl,  apiService, moduleConfig;
        const messages = [];
        const MAX_IMAGE_SIZE = 6 * 1024 * 1024 // 6MB
        
        function scrollToImage(){
            let buyerImageWrapTop = ($moduleEl.find("#buyerImageWrap").offset().top);
            $("html, body").animate({ scrollTop: buyerImageWrapTop },'slow');
        }
        function showImageLoader(){
            $moduleEl.find('.js-loader').removeClass('hide');
        }
        function hideImageLoader(){
            $moduleEl.find('.js-loader').addClass('hide');
        }
        function showImageError(errorMsg){
            $moduleEl.find('.js-imageError').removeClass('hide').html(errorMsg);
        }
        function hideImageError(){
            $moduleEl.find('.js-imageError').addClass('hide');   
        }
        function showImagePreview(imgPath){
            $moduleEl.find('.js-buyerImage').attr({'src':imgPath})
            $moduleEl.find('.js-buyerImageWrap').removeClass('hide')
            hideImageLoader()
            hideImageError()
        }
        function upload(dataurl){
            const data = new FormData();
            data.append('image', dataurl,'buyerImage');
            apiService.post(sharedConfig.apiHandlers.uploadUserReviewImage(moduleConfig.buyerId).url,
                data,
                null,
                null,
                {
                    processData:false,
                    contentType:false,
                }
            ).then((result)=>{
                if(result && result.data && result.data.absolutePath){
                    showImagePreview(result.data.absolutePath + '?width=120&height=120')
                    context.broadcast('imageUploadSuccess',{
                        imagePath:result.data.absolutePath
                    })
            } else {
                hideImageLoader()
                showImageError('Error uploading image, please try again')
            }
            },(error)=>{
                showImageError('Error uploading image, please try again')
            })
        }
        function uploadImage(){
            let file = document.querySelector('input[type=file]#imageUpload').files[0];
            if(file.size>MAX_IMAGE_SIZE){
                showImageError('Please upload image of less than 6MB');
                return;
            }
            
            scrollToImage();
            showImageLoader();
            var reader = new FileReader();
            reader.onload = function(e) {
                var img = new Image();
                img.onload=function(){
                    var canvas = document.createElement('canvas');
                    var ctx = canvas.getContext("2d");
                    const MAX_WIDTH = 800,
                        MAX_HEIGHT = 800,
                        COMPRESSION=0.9;
                    var width = img.width;
                    var height = img.height;
                    if (width > height) {
                      if (width > MAX_WIDTH) {
                        height *= MAX_WIDTH / width;
                        width = MAX_WIDTH;
                      }
                    } else {
                      if (height > MAX_HEIGHT) {
                        width *= MAX_HEIGHT / height;
                        height = MAX_HEIGHT;
                      }
                    }
                    canvas.width = Math.ceil(width);
                    canvas.height = Math.ceil(height);
                    ctx.drawImage(img, 0, 0, width, height);
                    canvas.toBlob((blob)=>{
                        upload(blob)
                    },'image/jpeg', COMPRESSION)
                }
                img.src=e.target.result; 
            };
            reader.readAsDataURL(file);
        }

        var init = function() {
            moduleEl = context.getElement();
            moduleConfig = context.getConfig();
            $ = context.getGlobal('jQuery');
            apiService = context.getService('ApiService');
            $moduleEl = $(moduleEl);
            $moduleEl.find('input[type=file]#imageUpload').on('change', uploadImage);
        };

        var onmessage = function(name, data) {
            
        };

        return {
            init,
            destroy: function() {}
        };
    });
});