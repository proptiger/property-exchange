"use strict";
define(['common/sharedConfig',
    'services/urlService',
    'services/apiService',
    'modules/sellerFeedback/scripts/behaviors/sellerFeedbackTracking',
    'modules/sellerFeedback/scripts/behaviors/imageUploadBehavior',
    'modules/newLoginRegister/scripts/socialBehavior'], (sharedConfig, urlService) => {
    Box.Application.addModule('sellerFeedback', (context) => {
        var moduleEl,$, $moduleEl,  apiService, moduleConfig;
        const Animate_Speed=500;
        const FEEDBACK_MIN_LENGTH = 100;
        let SCREEN_WIDTH;
        let SCREENS_WITH_SELLER_CONTEXT= {
                IMAGE_UPLOAD: 1,
                FEEDBACK: 2
            },
            SCREENS_WITHOUT_SELLER_CONTEXT= {
                CONFIRM_DEAL_CLOSE: 1,
                COMEBACK_LATER: 2,
                CHOOSE_SELLER: 3,
                ENTER_SELLER_INFO: 4,
                IMAGE_UPLOAD: 5,
                FEEDBACK: 6
            };
        let FLOW_CONTEXT;
        let feedbackId;
        let userinfo, postDataContext;
        let HELP_SELECTORS = {
            TOP: {help: '.js-help', scroll: '#helpfulbox'},
            HOW: {help: '.js-show_how_help', scroll: '#helpfullinkbox'}
        }
        //moveNext and movePrev are simple function to move screens to and fro
        function moveNext(step){
            if(!SCREEN_WIDTH){
                SCREEN_WIDTH = $moduleEl.find('.multi-screen-slide-wrapper').width();
            }
            let width = -(SCREEN_WIDTH*step)
            $("html, body").animate({ scrollTop: 0 },'fast',()=>{
                $moduleEl.find('.screen-list').css({
                    transform: "translateX(" + width+"px)"
                })
            });
        }
        function movePrev(step){
            if(!SCREEN_WIDTH){
                SCREEN_WIDTH = $moduleEl.find('.multi-screen-slide-wrapper').width();
            }
            let width = -SCREEN_WIDTH*(step-1)
            $moduleEl.find('.screen-list').css({
                transform: "translateX(" + width+"px)"
            })
        }
        function moveScreens(step,backward){
            if(!SCREEN_WIDTH){
                SCREEN_WIDTH = $moduleEl.find('.multi-screen-slide-wrapper').width();
            }
            let nextStep = -step;
            if(backward){
                nextStep += 1
            }
            let width = SCREEN_WIDTH*nextStep
            $("html, body").animate({ scrollTop: 0 },'fast',()=>{
                $moduleEl.find('.screen-list').css({
                    transform: "translateX(" + width+"px)"
                })
            });
        }
        //moveNextStep and movePrevStep functions checkes current status 
        //of the flow and then decide next step
        function moveNextStep(currentStep, userInput){
            let nextStepFrom = currentStep;
            if(!moduleConfig.sellerContextFlow && 
                currentStep<FLOW_CONTEXT.IMAGE_UPLOAD){
                switch(currentStep){
                    case FLOW_CONTEXT.CONFIRM_DEAL_CLOSE:
                        if(userInput.dealClosed){
                            nextStepFrom = FLOW_CONTEXT.COMEBACK_LATER;
                        } else {
                            nextStepFrom = FLOW_CONTEXT.CONFIRM_DEAL_CLOSE;
                        }
                    break;
                    case FLOW_CONTEXT.CHOOSE_SELLER:
                        if(userInput.sellerFound){
                            nextStepFrom = FLOW_CONTEXT.ENTER_SELLER_INFO;
                        } else {
                            nextStepFrom = FLOW_CONTEXT.CHOOSE_SELLER;
                        }
                    break;

                }    
            }  
            return moveScreens(nextStepFrom);          
        }
        function movePrevStep(currentStep){
            let prevStepFrom = currentStep;
            if(!moduleConfig.sellerContextFlow){
                switch(currentStep){
                    case FLOW_CONTEXT.COMEBACK_LATER:
                        prevStepFrom = FLOW_CONTEXT.CONFIRM_DEAL_CLOSE;
                    break;
                    case FLOW_CONTEXT.ENTER_SELLER_INFO:
                        prevStepFrom = FLOW_CONTEXT.CHOOSE_SELLER;
                    break;
                    case FLOW_CONTEXT.IMAGE_UPLOAD:
                        prevStepFrom = FLOW_CONTEXT.CHOOSE_SELLER;
                    break;
                }    
            }  
            return moveScreens(prevStepFrom,true);          
        }
        // function skipScreen(screenNumber){
        //     $moduleEl.find(`.js-screen${screenNumber}`).addClass('hide');
        // }
        function addEvents(){
            $moduleEl.find('.js-feedbackText').on('keyup',()=>{
                let feedback = $moduleEl.find('.js-feedbackText').val().trim();
                if(feedback.length>FEEDBACK_MIN_LENGTH){
                    $moduleEl.find('.btnsubmit').removeClass('disabled'); 
                } else {
                    $moduleEl.find('.btnsubmit').addClass('disabled');
                }
            });
            addTypeAheadBehaviour();
        }
        function addTypeAheadBehaviour(){
            //make typeahead like feature for filtering sellers
            //without showing seller phone number
            let $sellerFilter = $moduleEl.find('.js-seller-filter')
            let companyDataMap = {}
            $moduleEl.find('.js-seller-item').each((i,item)=>{
                let data = item.dataset;
                companyDataMap[data.companyid] = {
                    ph: data.ph || '',
                    name: data.companyname.toLowerCase() || ''
                }
            });

            $sellerFilter.on('keyup',()=>{
                let typeData = $sellerFilter.val();
                if(typeData && typeData.length>0){
                    typeData = typeData.toLowerCase();
                    for(let company in companyDataMap){
                        let $sellerItem = $moduleEl.find('li[data-companyid='+company+']');
                        let companyInfo = companyDataMap[company];
                        if(companyInfo.ph.indexOf(typeData)>-1 || companyInfo.name.indexOf(typeData)>-1){
                            $sellerItem.removeClass('hide')
                        } else {
                            $sellerItem.addClass('hide')
                        }
                    }    
                } else {
                    $moduleEl.find('.js-seller-item').removeClass('hide');
                }
            })
        }
        function showError(errorMsg,target='.js-error'){
            $moduleEl.find(target).removeClass('hide').html(errorMsg);
        }
        function hideError(target='.js-error'){
            $moduleEl.find(target).addClass('hide');   

        }
        function submitSellerDetails(){
            let sellerName = $moduleEl.find('.js-sellerName').val().trim();
            let sellerNumber = $moduleEl.find('.js-sellerNumber').val().trim();
            let ERROR_TARGET = '.js-error-seller-';
            if(!sellerName || sellerName.length<3){
                return showError('Please provide valid name',ERROR_TARGET + 'name');
            }
            hideError(ERROR_TARGET + 'name')
            if(!sellerNumber || sellerNumber.length<10){
                return showError('Please provide valid mobile number',ERROR_TARGET+'number');
            }
            hideError(ERROR_TARGET + 'number')
            context.broadcast('trackMissingSellerInfo',{
                id:moduleEl.id,
                buyerId: postDataContext.buyerId,
                sellerName,
                sellerNumber
            });
            $moduleEl.find('.js-enter-seller-info').addClass('hide');
            $moduleEl.find('.js-thanks-seller-info').removeClass('hide');
            setTimeout(()=>{
                window.redirect('/');
            },2000)
        }
        function submitFeedback(){
            let errorMsg = "";
            let buyerName = $moduleEl.find('.js-buyerName').val().trim();
            //validations
            if(buyerName.length < 4){
                errorMsg = "Please provide your name(min 4 chars)";
                showError(errorMsg);
                context.broadcast('trackFeedbackError',{
                        id:moduleEl.id,
                        leadId: postDataContext.leadId,
                        error: errorMsg
                    });

                return;
            }
            //end validations
            let feedback = $moduleEl.find('.js-feedbackText').val().trim();
            let noRecommend=$moduleEl.find('#no-recommend-id').is(':checked');
            let jsonDump = {
                name: buyerName,
                city: postDataContext.city
            }
            if(userinfo && userinfo.picture){
                jsonDump['picture'] = userinfo.picture
            }
            let data={
                "createdBy": postDataContext.buyerId,          //  buyer id
                "jsonDump": JSON.stringify(jsonDump),
                "ratings": [
                    {
                        "ratingParameterId": 50,     // must be 50
                        "value":3,                   // can be used to send rating. For now we can send some fixed number
                        "answerValue": feedback
                    },  
                    {
                        "ratingParameterId": 51,
                        "value": noRecommend?2:1, //2 is no: 1 is yes
                        "answerValue": noRecommend?"no":"yes"
                    }
                ]
            };
            $moduleEl.find('.btnsubmit').addClass('disabled');
            var url = sharedConfig.clientApis.sellerFeedback(
                postDataContext.sellerId,
                buyerName,
                postDataContext.leadId).url;
            apiService.postJSON(url, data).then((response)=>{
                    feedbackId = response.data.id;
                    context.broadcast('trackFeedbackSubmit',{
                        id:moduleEl.id,
                        leadId: postDataContext.leadId
                    });
                    $("html, body").animate({ scrollTop: 0 },'fast',()=>{
                        moveNextStep(FLOW_CONTEXT.FEEDBACK)
                    });
                },(err)=>{
                    showError("Looks like feedback already submitted for this request or Some error in our servers. Please try after some time.");
                    context.broadcast('trackFeedbackError',{
                        id:moduleEl.id,
                        leadId: postDataContext.leadId,
                        error: err.statusText
                    });
                    $moduleEl.find('.btnsubmit').removeClass('disabled');
                }
            );
        }   
        function updateFeedback(index,id,categoryId,rating, questionTxt){
            var url = sharedConfig.clientApis.sellerFeedbackUpdate(feedbackId).url;
            let data = {
                "answerOptionId": id,
                "ratingParameterId": categoryId,
                "value": rating,
                "answerValue": questionTxt
            }
            apiService.putJSON(url, JSON.stringify({
                "ratings": [data]
            })).then(()=>{},(err)=>{});
            context.broadcast('trackFeedbackUpdate',{
                id:moduleEl.id,
                leadId: postDataContext.leadId,
                label: parseInt(index,10) + FLOW_CONTEXT.FEEDBACK + 1
            });
        }
        
        
        function updateMCQ(dataset, newRating){
            if(dataset && dataset.index){
                let rating = newRating?newRating:dataset.rating;
                let questionTxt = $moduleEl.find('.js-OptionIndex-'+dataset.id).text()
                updateFeedback(dataset.index,dataset.id,dataset.categoryid, rating, questionTxt);
                moveNextStep(parseInt(dataset.index,10) + FLOW_CONTEXT.FEEDBACK + 1);    
            }
        }
        function recommendSelected(){
            let noRecommend=$moduleEl.find('#no-recommend-id').is(':checked');
            let yesRecommend=$moduleEl.find('#yes-recommend-id').is(':checked');
            let feedbackTxt = $moduleEl.find('.js-feedbacklbl');
            let feedbackInput = $moduleEl.find('.js-feedbackText');
            if(noRecommend){
                feedbackTxt.text(`What went wrong? (min ${FEEDBACK_MIN_LENGTH} chars)`);    
                feedbackInput.attr('placeholder', 'Talk about what went wrong. You can write about poor behavior, knowledge, property options, site visits or responsiveness');
                context.broadcast('trackRecommend',{
                    id:moduleEl.id,
                    recommend:  'No'
                });
            } else if (yesRecommend){
                $moduleEl.find('.js-feedbacklbl').text(`A few words would be helpful! (min ${FEEDBACK_MIN_LENGTH} chars)`);    
                feedbackInput.attr('placeholder', 'You can mention about the experience of your home hunting with us. How was the seller behavior? Was the seller responsive? How did the site visit go? etc.');
                context.broadcast('trackRecommend',{
                    id:moduleEl.id,
                    recommend:  'Yes'
                });
            }
            feedbackInput.focus();
            $moduleEl.find('.btnsubmit').show(Animate_Speed);
            $moduleEl.find('.js-feedbackWrap').show(Animate_Speed);
        }
        function showHelp(showContext, hideContext){
            $moduleEl.find(showContext.help).removeClass('hide');
            $moduleEl.find(hideContext.help).addClass('hide');
            let scrollTop = ($moduleEl.find(showContext.scroll).offset().top);
            $("html, body").animate({ scrollTop: scrollTop },'slow');
        }
        function hideHelp(context){
            $moduleEl.find(context.help).addClass('hide');
            $("html, body").animate({ scrollTop: 0 },'slow');
        }
        function trackDealClose(label){
            context.broadcast('trackFeedback_DealClose',{
                id:moduleEl.id,
                buyerId: postDataContext.buyerId,
                label: label 
            });
        }
        function onclick(event, element, elementType) {
            // bind custom messages/events
            switch(elementType){
                case 'showCommentScreen':
                    $("html, body").animate({ scrollTop: 0 },'fast',()=>{
                        moveNextStep(FLOW_CONTEXT.IMAGE_UPLOAD);
                    });
                break;
                case 'questions':
                    updateMCQ(event.target.dataset)
                    break;
                // case 'nextMCQ':
                //     updateMCQ(event.target.dataset)
                // break;
                // case 'previous':
                //     let dataset_prev = event.target.dataset;
                //     var startIndex = moduleConfig.questions_startIndex;
                //     if(dataset_prev && dataset_prev.index){
                //         movePrev(parseInt(dataset_prev.index,10)+startIndex);    
                //     }
                //     break;
                case 'submit':
                    submitFeedback();
                break;
                case 'show_top_help':
                    showHelp(HELP_SELECTORS.TOP,HELP_SELECTORS.HOW);
                break;
                case 'hide_top_help':
                   hideHelp(HELP_SELECTORS.TOP);
                break;
                case 'show_how_help':
                    showHelp(HELP_SELECTORS.HOW,HELP_SELECTORS.TOP);
                break;
                case 'hide_how_help':
                    hideHelp(HELP_SELECTORS.HOW);
                break;
                case 'remcommend':
                    recommendSelected();
                break;
                case 'finalized-yes':
                    moveNextStep(FLOW_CONTEXT.CONFIRM_DEAL_CLOSE,{
                        dealClosed: true
                    })
                    trackDealClose(elementType)
                break;
                case 'finalized-no':
                    moveNextStep(FLOW_CONTEXT.CONFIRM_DEAL_CLOSE,{
                        dealClosed: false
                    });
                    trackDealClose(elementType)
                break;
                case 'seller-not-found':
                    moveNextStep(FLOW_CONTEXT.CHOOSE_SELLER,{
                        sellerFound: false
                    });
                    trackDealClose(elementType)
                break;
                case 'submit-seller-details':
                    submitSellerDetails();
                break;
                case 'skip-login':
                    moveNextStep(FLOW_CONTEXT.IMAGE_UPLOAD);
                break;
                case 'back-finalized-no':
                    movePrevStep(FLOW_CONTEXT.COMEBACK_LATER);
                break;
                case 'back-enter-sellerInfo':
                    movePrevStep(FLOW_CONTEXT.ENTER_SELLER_INFO);
                break;
                case 'back-image-upload':
                    movePrevStep(FLOW_CONTEXT.IMAGE_UPLOAD);
                break;
            }
        }
        function onchange(event, element, elementType){
            switch(elementType){
                case 'select-seller':
                    let data = element.dataset;
                    $moduleEl.find('.js-seller-name').text(data.companyname);
                    postDataContext = Object.assign(postDataContext,{
                        sellerId: data.companyid,
                        leadId: data.leadid,
                        city: data.city
                    })
                    moveNextStep(FLOW_CONTEXT.CHOOSE_SELLER,{
                        sellerFound: true
                    });
                    trackDealClose(elementType+'-'+data.companyid)
                break;
            }
        }
        function onmessage(name, data) {
           switch (name) {
                case 'ExternalLoggedIn':
                    let token = data.data.token
                    const url = `https://graph.facebook.com/v2.10/me?access_token=${token}&fields=id%2Cname%2Cpicture&format=json`
                    $.ajax({
                        url: url,
                        method: 'get'
                    }).then((response)=>{
                        userinfo = response
                        if(userinfo && userinfo.name){
                            $moduleEl.find('.js-buyerName').val(userinfo.name)
                        }
                    }).always((error)=>{
                        moveNextStep(FLOW_CONTEXT.IMAGE_UPLOAD)
                    })
                    break;
                case 'imageUploadSuccess':
                    userinfo = {
                        'picture': {
                            data: {
                                url: data.imagePath
                            }
                        }
                    }
                    break;
                }
        }
        function init() {
            //initialization of all local variables
            moduleEl = context.getElement();
            moduleConfig = context.getConfig();
            $ = context.getGlobal('jQuery');
            apiService = context.getService('ApiService');
            $moduleEl = $(moduleEl);
            $moduleEl.find('.loader-wrap').addClass('hide');
            //Decide which flow user is in i.e. Deal reported by Seller or not
            //With deal reported flow, seller context would be available else ask user to choose seller context
            if(moduleConfig.sellerContextFlow){
                FLOW_CONTEXT = SCREENS_WITH_SELLER_CONTEXT;
                
            } else {
                FLOW_CONTEXT = SCREENS_WITHOUT_SELLER_CONTEXT;
            }
            postDataContext = {
                sellerId: moduleConfig.sellerId,
                leadId: moduleConfig.leadId,
                buyerId: moduleConfig.buyerId,
                city: moduleConfig.city
            }
            addEvents()
        }
        function destroy(){
            $moduleEl.find('.js-feedbackText').off('keyup');
            $moduleEl.find('.js-seller-filter').off('keyup')
        }
        return {
            init,
            onclick,
            behaviors: ['sellerFeedbackTracking','socialBehavior','imageUploadBehavior'],
            messages: ['ExternalLoggedIn','imageUploadSuccess'],
            onmessage,
            onchange,
            destroy
        };
    });
});