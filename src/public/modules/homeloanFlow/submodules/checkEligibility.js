/**
 * This is a submodule of homeloanFlow being used for checking whether user is eligible for instant homeloan by indiaBulls or not
 * JIRA: MAKAAN-3255
 */

 "use strict";
 define([
     'doT!modules/homeloanFlow/views/checkEligibility',
     'services/homeloanService',
 ], (templateD,homeloanService) => {
     Box.Application.addModule('checkEligibility', (context) => {

         const SCREEN_NAME = 'checkEligibility';
         var element, $moduleEl, $,moduleId;
         let renderData = {},allScreenData={};

         //save current screen Data to be passed to the parent Module
         function saveScreenData(data) {
            var obj = {
                checkEligibilty_isEligible : data.isEligible,
                checkEligibilty_loanAmount : data.loanAmount,
                checkEligibilty_tenure : data.tenure,
                checkEligibilty_emi : data.emi,
                checkEligibilty_interestRate : data.interestRate
            }
            return obj;
        }
        // makaan wrapper api is called to check instant loan eligibility with indiabulls
        function checkInstantLoanEligibilty(){
            let inputObj ={}
            let  payload = _generatePayload();
            homeloanService.checkIndiaBullsEligibilty(payload).then((response) => {
                if (response.data) {
                    inputObj = saveScreenData(response.data);
                }
                if(!response.data || !inputObj.checkEligibilty_isEligible){
                    context.broadcast('skipNext', { self: SCREEN_NAME, inputObject: inputObj }); 
                }else{
                    context.broadcast('nextClicked', { self: SCREEN_NAME, inputObject: inputObj }); 
                }
            }, (error) => {
                context.broadcast('skipNext', { self: SCREEN_NAME, inputObject: inputObj }); 
            });
        }

        //generate payload for post call in checkInstantLoanEligibilty function
         var _generatePayload = () => {
            let parsedPayload = {};
            parsedPayload.fullName = allScreenData.summary_name || '';
            parsedPayload.emailId = allScreenData.summary_email || '';
            parsedPayload.mobileNumber = allScreenData.summary_mobile || '';
            parsedPayload.dateOfBirth = allScreenData.summary_dob || '';
            parsedPayload.tenure = 240;
            parsedPayload.employmentType = allScreenData.income_profession || '';
            parsedPayload.loanAmount = allScreenData.loan_loanAmount;
            parsedPayload.grossSalary = allScreenData.income_grossEarning || '';
            parsedPayload.gender = allScreenData.summary_gender || '';
            parsedPayload.existingObligation = 0;
            return parsedPayload;
        };
         
        // function to render the scren content
         function render() {
             var useTemplate =templateD;
             $moduleEl.html(useTemplate(renderData));
             checkInstantLoanEligibilty();
         }

         return {
             messages: ['homeloan:checkEligibility'],
             init: function() {

                 element = context.getElement();
                 moduleId = element.id;
                 $ = context.getGlobal('jQuery');
                 $moduleEl = $(element);
                 context.broadcast('moduleLoaded', {
                     name: 'checkEligibility'
                 });
             },
             destroy: function() {
                 /*moduleEl = null;
                 $ = null;*/
             },
             onmessage: function(name, data) {
                 switch (name) {
                    //message broadcasted by parent module to render this screen
                     case 'homeloan:checkEligibility': 
                         allScreenData = data.allScreenData;
                         render();
                         break;
                 }
             },
         };
     });
 });
