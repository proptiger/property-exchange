"use strict";
define([
    'doT!modules/homeloanFlow/views/otp',
    'services/commonService',
    'services/homeloanService',
    'services/utils',
    'services/filterConfigService',
    'behaviors/homeloanFlowTracking'
], (templateD,commonService, homeloanService, UtilService, FilterConfigService) => {

    Box.Application.addModule('otp', (context) => {

        const SCREEN_NAME = 'otp',
            otpSelector = '#otp_number';
        const ERROR_MSG_CLEAR_TIME_DELAY = 4000;
        var isMobileDevice;
        let tmpFields = {},
            allScreenData = {},
            inputObj = {};
        var element, $moduleEl, $, moduleConfig, moduleId, commonService,homeloanService;

        function getScreenFields() {
            tmpFields.otpNumber = $moduleEl.find(otpSelector).val();
        }
        var _generatePayload = () => {
            let parsedPayload = {
                email: '',
                phone: '',
                countryId: '',
                name: '',
                salesType: 'homeloan',
                cityId: '',
                cityName: '',
                projectId: '',
                // listingId: -1,
                // localityIds: [-1],
                applicationType: isMobileDevice ? 'Mobile Site' : 'Desktop Site',
                jsonDump: window.navigator.userAgent,
                pageName: UtilService.getPageData('pageType'),
                pageUrl: window.location.href.split('#')[0],
                domainId: 1,
                sendOtp: true,
                enquiryType: { id: 11 },
                multipleCompanyIds: [499], // Proptiger
                interestedBanks: [],
                loanAmount: ''
            };
            // Parse object
            parsedPayload.sendEnquiryDropNotification = false;
            parsedPayload.firstName = allScreenData.summary_name || '';
            parsedPayload.lastName = allScreenData.summary_name || '';
            parsedPayload.countryId = allScreenData.summary_countryId || '';
            parsedPayload.phone = allScreenData.summary_mobile || '';
            parsedPayload.cityId = allScreenData.project_cityId || '';
            parsedPayload.cityName = allScreenData.project_cityName || '';
            parsedPayload.projectId = allScreenData.project_projectId || '';
            parsedPayload.loanAmount = allScreenData.loan_loanAmount || '';
            parsedPayload.interestedBanks = allScreenData.bank_selectedBanks || [];
            parsedPayload.propertyPrice = allScreenData.price_propertyPrice || '';
            parsedPayload.constructionPrice = allScreenData.price_constructionPrice || '';
            parsedPayload.employmentType = allScreenData.income_profession || '';
            parsedPayload.annualIncome = allScreenData.income_grossEarning || '';
            parsedPayload.currentEmi = allScreenData.income_existingEmi || '';
            parsedPayload.dateOfBirth = allScreenData.summary_dob || '';
            parsedPayload.userResidenceCityId = allScreenData.summary_cityId || '';
            parsedPayload.tempEnquiryId = allScreenData.summary_enquiryId || '';
            parsedPayload.userId = allScreenData.summary_userId || '';
            parsedPayload.email = allScreenData.summary_email || '';
            parsedPayload.name = allScreenData.summary_name || '';
            parsedPayload.gender = allScreenData.summary_gender || '';
            parsedPayload.countryId = allScreenData.summary_countryId || '';
            parsedPayload.phone = allScreenData.summary_mobile || '';
            parsedPayload.propertyIdentified= allScreenData.property_propertyIdentifies;
            return parsedPayload;
        };

        function render(data) {
            $moduleEl.html(templateD(data));
            $moduleEl.find('#otp_mobileNumber').text(allScreenData.summary_mobile);
            commonService.startAllModules(element);
        }

        function clearMessage(tmpElement) {
            setTimeout(() => {
                tmpElement.text('');
            }, ERROR_MSG_CLEAR_TIME_DELAY);
        }

        function setError(message) {
            var tmpElement = $moduleEl.find('#opt-error-msg');
            tmpElement.text(message);
            clearMessage(tmpElement);
        }

        function saveScreenData() {
            var obj = {
                otp_otpNumber: tmpFields.otpNumber,

            }
            return obj;
        }

        function validate() {
            var otpNumber = tmpFields.otpNumber;
            var flag = true;
            if (!(otpNumber && otpNumber.trim().length === 4)) {
                setError('Valid input is required');
                flag = false;
            }
            return flag;
        }
        var validateSubmitOtp = () => {
            var otpPayload = {
                email: allScreenData.summary_email || '',
                name: allScreenData.summary_name || '',
                phone: allScreenData.summary_mobile || '',
                sendOtp: false,
                userId: allScreenData.summary_userId || ''
            };
            $moduleEl.find('.js-otpSubmit').addClass('disabled');
            homeloanService.submitOtp(otpPayload, allScreenData.summary_enquiryId, tmpFields.otpNumber).then((response) => {
                $moduleEl.find('.js-otpSubmit').removeClass('disabled');
                if (response.data.otpValidationSuccess) {
                    context.broadcast('trackOtpVerified', { id: moduleId, label: 'verified' });
                    inputObj = saveScreenData();
                    context.broadcast('nextClicked', { self: SCREEN_NAME, inputObject: inputObj });
                    console.log("OTP submission successful");
                } else {
                    setError("Otp is invalid.");
                }
            }, (error) => {
                $moduleEl.find('.js-otpSubmit').removeClass('disabled');
                setError("Some Error Occured");
            });
        }


        return {
            messages: ['homeloan:otp', 'updateFields'],
            behaviors: ['homeloanFlowTracking'],
            init: function() {

                element = context.getElement();
                moduleId = element.id;
                $ = context.getGlobal('jQuery');
                moduleConfig = context.getConfig();
                $moduleEl = $(element);
                commonService = context.getService('CommonService');
                homeloanService = context.getService('homeloanService');
                context.broadcast('moduleLoaded', {
                    name: 'otp'
                });
            },
            destroy: function() {
                /*moduleEl = null;
                $ = null;*/
            },
            onmessage: function(name, data) {
                switch (name) {
                    case 'homeloan:otp':
                        allScreenData = data.allScreenData;
                        isMobileDevice = data.isMobileDevice;
                        render(data.renderData);
                        break;
                    case 'updateFields':
                          allScreenData=data.allScreenData;
                          $moduleEl.find('#otp_mobileNumber').text(allScreenData.summary_mobile);
                          break;
                }
            },
            onclick: function(event, element, elementType) {
                var screenName = $(element).data('screen');
                switch (elementType) {
                    case "submitOtp":
                        switch (screenName) {
                            default: context.broadcast('trackSubmitOtp', { id: moduleId, label: 'application_success' });
                            getScreenFields();
                            var flag = validate();
                            if (flag) {
                                validateSubmitOtp();
                            }
                            break;
                        }
                        break;
                    case 'otpOnCall':
                        homeloanService.sendOtpOnCall(allScreenData.summary_userId, allScreenData.summary_mobile).then((response) => {
                            setError("You will receive call in few seconds.");
                        }, (error) => {
                            setError("Some Error Occured");
                        });
                        break;
                    case 'resendOtp':
                        var payload = _generatePayload();
                        $moduleEl.find('.js-resendOtp').addClass('hide');
                        homeloanService.postEnquiry(payload).then((response) => {
                            $moduleEl.find('.js-resendOtp').removeClass('hide');
                            tmpFields.summary_enquiryId = response.enquiryIds[0] || '';
                            allScreenData.summary_enquiryId = response.enquiryIds[0] || '';
                            tmpFields.summary_userId = response.userId || '';
                            allScreenData.summary_userId = response.userId || '';
                        }, (error) => {
                            $moduleEl.find('.js-resendOtp').removeClass('hide');
                            tmpFields.summary_enquiryId = '';
                            tmpFields.summary_userId = '';
                            allScreenData.summary_enquiryId = '';
                            allScreenData.summary_userId = '';
                        });
                        break;
                    case 'previous':
                        switch (screenName) {
                            default:
                                context.broadcast('previousClicked', { self: SCREEN_NAME ,previous:screenName});
                            break;
                        }
                        break;
                }
            },
            onfocusout: function(event, element, elementType) {
                switch (elementType) {
                    case 'otp':
                        getScreenFields();
                        break;
                }
            }
        };
    });
});
