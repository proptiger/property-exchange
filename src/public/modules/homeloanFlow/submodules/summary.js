 "use strict";
 define([
     'doT!modules/homeloanFlow/views/summary',
     'services/commonService',
     'services/homeloanService',
     'services/utils',
     'common/utilFunctions',
     'services/filterConfigService',
     'behaviors/homeloanFlowTracking',
     'services/pyrService'
 ], (templateD,commonService,homeloanService, UtilService, utilFunctions, FilterConfigService, pyrService) => {
     Box.Application.addModule('summary', (context) => {

         const SCREEN_NAME = 'summary',minimumValue = 500000, ERROR_MSG_CLEAR_TIME_DELAY = 6000;
         var isMobileDevice, cityData = {},projectPageData={},passData={templateData:{}};
         var element, $moduleEl, $, moduleConfig, moduleId, commonService, pyrService,homeloanService,allBanksId=[],page='',source='';
         let renderData = {},scrollTopPixels={},scroll=true,count=0,typeaheadCount=0,previousSelectedBanks=[];
         let LoginService = context.getService('LoginService');
         let inputBoxName=['summary_name','summary_email','summary_mobile','income_gross_earning','income_existing_emi','price_loan_amount']

         var enteredData={
            summary_nri:false,
            income_profession:'Salaried',
            property_propertyIdentifies:false,
            bank_selectedBanks:[]
         };
         
         var  fieldValidation = {     
             required: (value)=> { 
             return value ? true : false;      
             },
             requiredResidenceCity: (value)=> {
                if(enteredData.summary_countryId==1 && value || enteredData.summary_countryId!=1 ){
                    return true;
                }
                return false;      
             },                
             validateEmail: (value)=> { 
                return value && UtilService.isEmail(value) ? true :false;    
             },
             validateMobile:(value)=> {
                let country= enteredData.summary_country || 'india';
                return value && UtilService.validatePhone(value,country) ? true :false;
             },
             isNumber:(value)=> {
                return value && !isNaN(value) ? true :false;
             } ,
             moreThanMinimum:(value)=> {
                return value && value >= minimumValue ? true :false;
             },
             lessThanMax:(value)=> {
                return value && value.toString().length <= 9 ? true :false;
             }       
        }; 

        function validateLoanEligibility(){
            let loanAmount=0;
            if(enteredData.income_grossEarning && enteredData.income_existingEmi){
                loanAmount = utilFunctions.calculateLoanEligibility(enteredData.income_grossEarning, enteredData.income_existingEmi, homeloanService.returnHomeloanConfig());
                enteredData.income_loanAmount = loanAmount;
                if(loanAmount > 0 ){
                    return true;
                }
            }
            setError(".js-salary","Please enter valid income");
            setError(".js-emi","Please enter valid emi");
            return false;
        }

        function validateTypeaheadFields(){
            var flag = true;
            homeloanService.returnTypeaheadFieldConfig().forEach((field)=>{
                if (field.validation) {     
                   field.validation.forEach((validationMethod)=> {     
                         if(!fieldValidation[validationMethod](enteredData[field.key])){
                            flag=false;
                            setErrorTypeahead(field.selector,field.errorMsg,scrollTopPixels[field.key]);
                            if(scroll){
                                scrollToElement(scrollTopPixels[field.key]-10);
                                scroll = false;
                            }
                         }         
                     });        
                 }
            });
            return flag;
        }
        function calculateTextScrollPixels(){
            homeloanService.returnTextFieldConfig().forEach((field)=>{
                if($moduleEl.find(field.selector).offset() && $moduleEl.find('.scroll-content'))
                scrollTopPixels[field.key]=$moduleEl.find(field.selector).offset().top-$moduleEl.find('.scroll-content').offset().top
            });
        }
        function calculateTypeaheadScrollPixels(){
            homeloanService.returnTypeaheadFieldConfig().forEach((field)=>{
                if($moduleEl.find(field.selector).offset() && $moduleEl.find('.scroll-content'))
                scrollTopPixels[field.key]=$moduleEl.find(field.selector).offset().top-$moduleEl.find('.scroll-content').offset().top
            });
        }
        function scrollToElement(scrollValue){
          $moduleEl.find('.scroll-content').animate({scrollTop:scrollValue}, 2000);  
        }

        function storeAndValidateTextFields(){
            var flag = true;
            homeloanService.returnTextFieldConfig().forEach((field)=>{
                enteredData[field.key]=$moduleEl.find(field.selector).val();
                if (field.validation) {     
                   field.validation.forEach((validationMethod)=> {     
                         if(!fieldValidation[validationMethod](enteredData[field.key])){
                            flag=false;
                            setError(field.selector,field.errorMsg);
                            if(scroll){
                                scrollToElement(scrollTopPixels[field.key]-10);
                                scroll =false;
                            }
                         }         
                     });        
                 }
            });
            return flag;
        }

        function validate(){
            var flag1 = storeAndValidateTextFields()
            var flag2 =validateTypeaheadFields() 
            var flag3 = validateLoanEligibility();
            return flag1 && flag2 && flag3;
         }
         function typeAheadView(element, view) {
             if (view) {
                 element.closest('.typeAhead-hl').addClass('active');
             } else {
                 element.removeClass('active');
             }
         }

 
        var _generatePayload = () => {
             let parsedPayload = {
                 email: '',
                 phone: '',
                 countryId: '',
                 name: '',
                 salesType: 'homeloan',
                 cityId: '',
                 cityName: '',
                 projectId: '',
                 applicationType: isMobileDevice ? 'Mobile Site' : 'Desktop Site',
                 jsonDump: window.navigator.userAgent,
                 pageName: 'homeloan',
                 pageUrl: window.location.href.split('#')[0],
                 domainId: 1,
                 sendOtp: true,
                 enquiryType: { id: 11 },
                 multipleCompanyIds: [499], // Proptiger
                 interestedBanks: [],
                 loanAmount: ''
             };
             parsedPayload.sendEnquiryDropNotification = false;
             parsedPayload.firstName = enteredData.summary_name || '';
             parsedPayload.lastName = enteredData.summary_name || '';
             parsedPayload.cityId = enteredData.project_cityId || '';
             parsedPayload.cityName = enteredData.project_cityName || '';
             parsedPayload.projectId = enteredData.project_projectId || '';
             parsedPayload.loanAmount = enteredData.loan_loanAmount || '';
             parsedPayload.interestedBanks = enteredData.bank_selectedBanks || [];
             parsedPayload.propertyPrice = enteredData.price_propertyPrice || '';
             parsedPayload.constructionPrice = enteredData.price_constructionPrice || '';
             parsedPayload.employmentType = enteredData.income_profession || '';
             parsedPayload.annualIncome = enteredData.income_grossEarning || '';
             parsedPayload.currentEmi = enteredData.income_existingEmi || '';
             parsedPayload.dateOfBirth = enteredData.summary_dob || '';
             parsedPayload.userResidenceCityId = enteredData.summary_cityId || '';
             parsedPayload.sendOtp = enteredData.summary_countryId!=1 ? false : true;
             parsedPayload.email = enteredData.summary_email || '';
             parsedPayload.name = enteredData.summary_name || '';
             parsedPayload.gender = enteredData.summary_gender || '';
             parsedPayload.countryId = enteredData.summary_countryId || '';
             parsedPayload.phone = enteredData.summary_mobile || '';
             parsedPayload.propertyIdentified= enteredData.property_propertyIdentifies;
             return parsedPayload;
         };

         function updateFields() {
             let cookieData = pyrService.getEnquiryCookie(),
                 summary_name = (cookieData && cookieData.name) || '',
                 summary_mobile = (cookieData && cookieData.phone) || '',
                 summary_email = (cookieData && cookieData.email) || '';
             if (summary_name) {
                 $moduleEl.find('.js-name').val(summary_name);
                 $moduleEl.find('.js-name').closest('.input-box.fc-wrap').addClass('active');
             }
             if (summary_email) {
                 $moduleEl.find('.js-email').val(summary_email);
                 $moduleEl.find('.js-email').closest('.input-box.fc-wrap').addClass('active');

             }
             if (summary_mobile) {
                 $moduleEl.find('.js-mobile').val(summary_mobile);
                 $moduleEl.find('.js-mobile').closest('.input-box.fc-wrap').addClass('active');

             }
             
         }
         function autoFillProjectPageData(){
            if (projectPageData.project_cityName && projectPageData.project_cityId) {
                $moduleEl.find("input[name='project_city_name']").val(projectPageData.project_cityName);
                enteredData.project_cityName = projectPageData.project_cityName;
                $moduleEl.find("input[name='project_city_id']").val(projectPageData.project_cityId);
                enteredData.project_cityId = projectPageData.project_cityId;
            }
            if(projectPageData.price_propertyPrice){
                $moduleEl.find('.js-loanAmount').val(projectPageData.price_propertyPrice);
                $moduleEl.find('.js-loanAmount').closest('.input-box.fc-wrap').addClass('active');
            }
         }

         function updatePrefillData(){
            autoFillProjectPageData();
            $moduleEl.find('.js-name').val(projectPageData.summary_name);
            $moduleEl.find('.js-email').val(projectPageData.summary_email);
            $moduleEl.find('.js-mobile').val(projectPageData.summary_mobile);
         }

         function render() {
             var useTemplate =templateD;
             $moduleEl.html(useTemplate(renderData));
             commonService.startAllModules(element);
         }

         function modifyRenderData() {
             let cookieData = pyrService.getEnquiryCookie();
             renderData.name = (cookieData && cookieData.name) || projectPageData.summary_name ||'';
             renderData.mobile = (cookieData && cookieData.phone) || projectPageData.summary_mobile ||'';
             renderData.email = (cookieData && cookieData.email) || projectPageData.summary_email ||'';
           
             LoginService.isUserLoggedIn().then((response) => {
                 var newData = (response && response.data) || {};
                 renderData.name = renderData.name || newData.firstName || '';
                 renderData.email = renderData.email || newData.email || '';
                 renderData.mobile = renderData.mobile || newData.contactNumber || '';
             }, () => {
             });

            renderData.preFillData = projectPageData;
         }

         function clearMessage(tmpElement) {
             setTimeout(() => {
                 tmpElement.text('');
             }, ERROR_MSG_CLEAR_TIME_DELAY);
         }

         function setError(selector, message) {
             var tmpElement = $moduleEl.find(selector).closest('.js-input-placeholder').parent().find('.error-msg');
             tmpElement.text(message);
             clearMessage(tmpElement);
         }
         function setErrorTypeahead(selector, message) {
             var tmpElement = $moduleEl.find(selector).closest('.typeAhead-hl.fc-wrap').find('.error-msg');
             tmpElement.text(message);
             clearMessage(tmpElement);
         }

         return {
             messages: ['homeloan:summary', 'moduleLoaded', 'singleSelectOptionsInitiated', 'singleSelectOptionsChanged', 'singleSelectDropdownInitiated', 'singleSelectDropdownChanged', 'typeAheadOverlayClicked', 'searchResultClicked', 'dateTimePicker:change', 'showError','multiSelectOptionsChanged','saveAllBanksId','typeAheadModuleInitiated','updatePrefillData'],
             behaviors: ['homeloanFlowTracking'],
             init: function() {

                 element = context.getElement();
                 moduleId = element.id;
                 $ = context.getGlobal('jQuery');
                 moduleConfig = context.getConfig();
                 $moduleEl = $(element);
                 commonService = context.getService('CommonService');
                 pyrService = context.getService('pyrService');
                 homeloanService = context.getService('homeloanService');
                 context.broadcast('moduleLoaded', {
                     name: 'summary'
                 });
             },
             destroy: function() {
                 /*moduleEl = null;
                 $ = null;*/
             },
             onmessage: function(name, data) {
                 switch (name) {
                     case 'homeloan:summary':
                         projectPageData = data.allScreenData;
                         isMobileDevice = data.isMobileDevice;
                         renderData = data.renderData;
                         page = data.page;
                         source = data.source;
                         modifyRenderData();
                         render();
                         break;
                     case 'moduleLoaded':
                         if (data.name === 'InputBox') {
                             if(inputBoxName.indexOf(data.id)>=0){
                                updateFields();
                                count++;
                                if(count===6)
                                    calculateTextScrollPixels();
                             }
                         }
                         break;
                     case 'singleSelectOptionsInitiated':
                     case 'singleSelectOptionsChanged':
                            switch(data.name){
                                case 'gender': 
                                    enteredData.summary_gender = data.value; 
                                break;
                                case 'existingEmi':
                                    if(data.value==='yes'){
                                        $moduleEl.find('.js-emiContainer').removeClass('hide');
                                        let prefillEmi =projectPageData && projectPageData.income_existingEmi || 0;
                                        $moduleEl.find('.js-emi').val(prefillEmi);  
                                    }
                                    else
                                    {
                                      $moduleEl.find('.js-emiContainer').addClass('hide');
                                      $moduleEl.find('.js-emi').val('0');  
                                    }
                                break;
                                case 'bankPrefrence':
                                    if(data.value==='yes'){
                                        $moduleEl.find('.js-bankContainer').removeClass('hide');
                                        enteredData.bank_selectedBanks=previousSelectedBanks;
                                    }
                                    else{
                                        $moduleEl.find('.js-bankContainer').addClass('hide');
                                        enteredData.bank_selectedBanks=allBanksId;
                                    }
                                break;  
                                case 'coApplicant': 
                                    enteredData.bank_coApplicant = data.value; 
                                break;
                                case 'propertyIdentifies': 
                                    enteredData.property_propertyIdentifies = data.value==='yes'; 
                                break; 
                            }
                         break;
                     case 'singleSelectDropdownInitiated':
                     case 'singleSelectDropdownChanged':
                        switch(data.name){
                            case 'countries':
                                let params = data && data.params && JSON.parse(decodeURI(data.params));
                                 enteredData.summary_country = params.label.trim();
                                 $moduleEl.find('#COUNTRY_CODE').text(params.code);
                                 enteredData.summary_countryCode = params.code;
                                 enteredData.summary_countryId = data.value;
                                 if(data.value!=1){
                                    $moduleEl.find('#summary_city').hide('fast');
                                 }else{
                                    $moduleEl.find('#summary_city').show('fast');
                                 }
                                 break;
                            case 'income':
                                enteredData.income_profession = data.value;
                                break;
                        }            
                         break;
                    case 'multiSelectOptionsChanged':
                            switch(data.name){
                                case'banks':
                                enteredData.bank_selectedBanks=data.value;
                                previousSelectedBanks=data.value
                                break;
                            }
                        break
                     case 'typeAheadOverlayClicked':
                     case 'searchResultClicked':
                         typeAheadView($moduleEl.find('.typeAhead-hl'), false);
                         switch (data.config && data.config.typeAheadType) {
                             case 'city':
                                switch(data.config && data.config.inputName){
                                    case 'project_city_name':
                                        if ($moduleEl.find('#project_property_type .txt').text().trim() != 'Residential Home') {
                                            $moduleEl.find('#project_project').hide('fast');
                                        }
                                        enteredData.project_cityName = data && data.dataset ? data.dataset.val : '';
                                        enteredData.project_cityId = data && data.dataset ? data.dataset.entityid : '';
                                        $moduleEl.find("input[name='project_city_id']").val(enteredData.project_cityId);
                                        break;
                                    case 'summary_city_name':
                                         enteredData.summary_cityName = data && data.dataset ? data.dataset.val : '';
                                         enteredData.summary_cityId= data && data.dataset ? data.dataset.entityid : '';
                                         break;
                                }
                                 break;
                         }
                         $moduleEl.find('.typeAhead-hl').removeClass('active');
                         break;
                     case 'dateTimePicker:change':
                         let dateString = utilFunctions.formatDate(data.value, 'dd MM YY');
                         $moduleEl.find('.js-dob').val(dateString);
                         $moduleEl.find('.js-dobtimeStamp').val(data.value);
                         break;
                     case 'showError':
                         var tmpElement = $moduleEl.find("#errorOcurred");
                         tmpElement.text('Some Error Occured');
                         clearMessage(tmpElement);
                         break;
                    case 'saveAllBanksId':
                            allBanksId=data.idList;
                            enteredData.bank_selectedBanks=allBanksId;
                            break;
                    case  'typeAheadModuleInitiated':
                            autoFillProjectPageData();
                            if(data.moduleEl && ['summary_city','project_city'].indexOf(data.moduleEl.id)>-1){
                                typeaheadCount++
                                if(typeaheadCount===2){
                                    calculateTypeaheadScrollPixels();
                                }
                            }
                            break;
                    case 'updatePrefillData':
                            projectPageData = data.allScreenData;
                            updatePrefillData();
                            break;
                 }
             },
             onclick: function(event, element, elementType) {
                 var screenName = $(element).data('screen');
                 switch (elementType) {
                     case 'next':
                        scroll =true;
                        var flag = validate();
                        if (flag) {
                            context.broadcast('trackHomeLoanSubmit', { id: moduleId, label: 'Home Loan',page,source });
                            passData.self=SCREEN_NAME,
                            passData.templateData.name=enteredData.summary_name;
                            // passData.templateData.coApplicant=enteredData.bank_coApplicant==="yes";
                            passData.templateData.coApplicant=false;
                            if(!enteredData.bank_selectedBanks.length){
                                enteredData.bank_selectedBanks=allBanksId;
                            }
                            var payload = _generatePayload();
                            $moduleEl.find('.js-summarySubmit').addClass('disabled');
                            homeloanService.postEnquiry(payload).then((response) => {
                                $moduleEl.find('.js-summarySubmit').removeClass('disabled');
                                 passData.templateData.applicationId=response.enquiryIds[0] || '';
                                 enteredData.summary_enquiryId = response.enquiryIds[0] || '';
                                 enteredData.summary_userId = response.userId || '';
                                 passData.inputObject=enteredData;
                                 if (enteredData.summary_countryId!=1 || response.isOtpVerified) {
                                     context.broadcast('skipNext', passData);
                                 } else {
                                     context.broadcast('nextClicked',passData);
                                 }
                            }, (error) => {
                                $moduleEl.find('.js-summarySubmit').removeClass('disabled');
                                 var tmpElement = $moduleEl.find("#errorOcurred");
                                 tmpElement.text('Some Error Occured');
                                 clearMessage(tmpElement);
                                 enteredData.summary_enquiryId = '';
                                 enteredData.summary_userId = '';
                             });
                         }
                         break;
                     case 'previous':
                         switch (screenName) {
                             default: context.broadcast('previousClicked', { self: SCREEN_NAME ,previous:screenName });
                             break;
                         }
                         break;
                     case 'nriClicked':
                            enteredData.summary_nri = $moduleEl.find('.js-nri')[0].checked ? true : false ;
                         break;
                    case 'closePopup':
                         typeAheadView($moduleEl.find('.typeAhead-hl'), false);
                         break;
                 }
             },
             onfocusin: function(event, element, elementType) {
                 switch (elementType) {
                     case 'query-text':
                         if ($moduleEl.find(element).closest('.typeAhead-hl.active')) {
                            //typeAheadView($moduleEl.find(element).closest('.typeAhead-hl'), true);
                             if (element.name === 'summary_city_name'|| element.name === 'project_city_name') {
                                 context.broadcast('typeAheadCityChanged', {
                                     city: ''
                                 });
                             }
                         }
                         break;
                 }
             }
         };
     });
 });
