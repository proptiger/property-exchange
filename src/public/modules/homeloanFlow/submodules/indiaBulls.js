 /**
 * This is a submodule of homeloanFlow being used for submitting the lead for instant homeloan to indiaBulls
 * JIRA: MAKAAN-3255
 */
 "use strict";
 define([
     'doT!modules/homeloanFlow/views/indiaBulls',
     'services/homeloanService',
     'services/commonService',
 ], (templateD,homeloanService,commonService) => {
     Box.Application.addModule('indiaBulls', (context) => {

         const SCREEN_NAME = 'indiaBulls',ERROR_MSG_CLEAR_TIME_DELAY = 4000;
         var element, $moduleEl, $,moduleId;
         let renderData = {},allScreenData={};

         // a function to set error message passed as a parameter when api fails.
         function setError(message) {
            var tmpElement = $moduleEl.find('#errorOcurred');
            tmpElement.text(message);
            clearMessage(tmpElement);
        }
        //clear the error message by setting the element of error element to null.
        function clearMessage(tmpElement) {
            setTimeout(() => {
                tmpElement.text('');
            }, ERROR_MSG_CLEAR_TIME_DELAY);
        }

        //save current screen Data to be passed to the parent Module
         function saveScreenData(data) {
            var obj = {
                indiaBulls_postEnquiry : true,
                indiaBulls_isDuplicate : data.isDuplicate,
                indiaBulls_applicationId : data.generatedLeadId,
            }
            return obj;
        }

        // post indiabulls enquiry when user clicked apply now 
        function postIndiaBullsEnquiry(){
            let inputObj ={}
            let  payload = _generatePayload(); 
            $moduleEl.find('.js-applyNow').addClass('disabled');
            homeloanService.postIndiaBullsEnquiry(payload).then((response) => {
                if (response.data) {
                    inputObj = saveScreenData(response.data);
                    context.broadcast('nextClicked', { self: SCREEN_NAME, inputObject: inputObj });
                }
            }, (error) => {
                $moduleEl.find('.js-applyNow').removeClass('disabled');
                setError("Some Error Occured");
            });
        }

        //generate payload for post call in postIndiaBullsEnquiry function
         var _generatePayload = () => {
            let parsedPayload = {};
            parsedPayload.name = allScreenData.summary_name || '';
            parsedPayload.email = allScreenData.summary_email || '';
            parsedPayload.contactNumber = allScreenData.summary_mobile || '';
            parsedPayload.countryId = allScreenData.summary_countryId || '';
            return parsedPayload;
        };

        // a function to add details like name,loanAmount,tenure,emi,roi in template data used for rendering data on screen
        function modifyRenderData(){
                renderData.name = allScreenData.summary_name||'', 
                renderData.loanAmount = allScreenData.checkEligibilty_loanAmount,
                renderData.tenure = allScreenData.checkEligibilty_tenure,
                renderData.emi = allScreenData.checkEligibilty_emi,
                renderData.interestRate = allScreenData.checkEligibilty_interestRate
        }
        // function to render the scren content
         function render() {
             var useTemplate =templateD;
             modifyRenderData();
             $moduleEl.html(useTemplate(renderData));
             commonService.startAllModules(element);
         }

         return {
             messages: ['homeloan:indiaBulls'],
             init: function() {

                 element = context.getElement();
                 moduleId = element.id;
                 $ = context.getGlobal('jQuery');
                 $moduleEl = $(element);
                 context.broadcast('moduleLoaded', {
                     name: 'indiaBulls'
                 });
             },
             destroy: function() {
                 /*moduleEl = null;
                 $ = null;*/
             },
             onclick: function(event, element, elementType) {
                var screenName = $(element).data('screen');
                switch (elementType) {
                    // apply Now Click Handler
                    case "applyNow":
                         postIndiaBullsEnquiry();
                        break;
                    // may be later click handler
                    case 'later':
                        let inputObj = {indiaBulls_postEnquiry : false};
                        context.broadcast('nextClicked', { self: SCREEN_NAME, inputObject: inputObj });
                        break;
                }
            },
             onmessage: function(name, data) {
                 switch (name) {
                    //message broadcasted by parent module to render this screen
                     case 'homeloan:indiaBulls':
                         allScreenData = data.allScreenData;
                         renderData = data.renderData;
                         render();
                         break;
                 }
             },
         };
     });
 });
