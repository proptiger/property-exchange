 "use strict";
 define([
     'doT!modules/homeloanFlow/views/optional',
     'services/commonService',
     'services/homeloanService',
     'services/utils',
     'common/utilFunctions',
     'services/filterConfigService',
     'behaviors/homeloanFlowTracking',
 ], (templateD,commonService, HLS, UtilService, utilFunctions, FilterConfigService, pyrService) => {
     Box.Application.addModule('optional', (context) => {

         const SCREEN_NAME = 'optional',ERROR_MSG_CLEAR_TIME_DELAY = 4000;
         var isMobileDevice;
         var element, $moduleEl, $, moduleConfig, moduleId, commonService,HLS;
         let renderData = {},allScreenData={};
         let LoginService = context.getService('LoginService');
         var optionalData={
            co_propertyType:'Residential Home',
            co_relation:'wife'
         };
         
         var  fieldValidation = {     
             required: (value)=> { 
             return value ? true : false;      
             },                
             isNumber:(value)=> {
                return value && !isNaN(value) ? true :false;
             }     
        }; 

        function storeAndValidateTextFields(){
            var flag = true;
            HLS.returnOptionalTextFieldConfig().forEach((field)=>{
                optionalData[field.key]=$moduleEl.find(field.selector).val();
                if (field.validation) {     
                   field.validation.forEach((validationMethod)=> {     
                         if(!fieldValidation[validationMethod](optionalData[field.key])){
                            flag=false;
                            setError(field.selector,field.errorMsg);
                         }         
                     });        
                 }
            });
            return flag;
        }

        
         var _generatePayload = () => {
            let parsedPayload = {
                email: '',
                phone: '',
                countryId: '',
                name: '',
                salesType: 'homeloan',
                cityId: '',
                cityName: '',
                projectId: '',
                // listingId: -1,
                // localityIds: [-1],
                applicationType: isMobileDevice ? 'Mobile Site' : 'Desktop Site',
                jsonDump: window.navigator.userAgent,
                pageName: UtilService.getPageData('pageType'),
                pageUrl: window.location.href.split('#')[0],
                domainId: 1,
                sendOtp: true,
                enquiryType: { id: 11 },
                multipleCompanyIds: [499], // Proptiger
                interestedBanks: [],
                loanAmount: ''
            };
            // Parse object
            let homeLoanPropertyTypes = FilterConfigService.getFilterModuleConfig('homeLoanPropertyType');
            parsedPayload.sendEnquiryDropNotification = false;
            parsedPayload.firstName = allScreenData.summary_name || '';
            parsedPayload.lastName = allScreenData.summary_name || '';
            parsedPayload.countryId = allScreenData.summary_countryId || '';
            parsedPayload.phone = allScreenData.summary_mobile || '';
            parsedPayload.cityId = allScreenData.project_cityId || '';
            parsedPayload.cityName = allScreenData.project_cityName || '';
            parsedPayload.projectId = allScreenData.project_projectId || '';
            parsedPayload.loanAmount = allScreenData.loan_loanAmount || '';
            parsedPayload.interestedBanks = allScreenData.bank_selectedBanks || [];
            parsedPayload.propertyPrice = allScreenData.price_propertyPrice || '';
            parsedPayload.constructionPrice = allScreenData.price_constructionPrice || '';
            parsedPayload.employmentType = allScreenData.income_profession || '';
            parsedPayload.annualIncome = allScreenData.income_grossEarning || '';
            parsedPayload.currentEmi = allScreenData.income_existingEmi || '';
            parsedPayload.dateOfBirth = allScreenData.summary_dob || '';
            parsedPayload.userResidenceCityId = allScreenData.summary_cityId || '';
            parsedPayload.tempEnquiryId = allScreenData.summary_enquiryId || '';
            parsedPayload.userId = allScreenData.summary_userId || '';
            parsedPayload.email = allScreenData.summary_email || '';
            parsedPayload.name = allScreenData.summary_name || '';
            parsedPayload.gender = allScreenData.summary_gender || '';
            parsedPayload.countryId = allScreenData.summary_countryId || '';
            parsedPayload.phone = allScreenData.summary_mobile || '';
            parsedPayload.propertyIdentified= allScreenData.property_propertyIdentifies;

            if (allScreenData.project_propertyType) {
                homeLoanPropertyTypes.templateData.options.forEach((propertyType) => {
                    if (propertyType.value == optionalData.co_propertyType) {
                        parsedPayload.homeLoanTypeId = propertyType.id;
                    }
                });
            }
            return parsedPayload;
        };
         function modifyTemplateData(){
            renderData.templateData.isDuplicate = allScreenData.indiaBulls_isDuplicate;
            renderData.templateData.indiaBullsEnquiry = allScreenData.indiaBulls_postEnquiry || false;
            renderData.templateData.indiaBullsApllicationId = allScreenData.indiaBulls_applicationId||''; 
         }

         function render() {
             var useTemplate =templateD;
             modifyTemplateData();
             $moduleEl.html(useTemplate(renderData));
             commonService.startAllModules(element);
         }

         function clearMessage(tmpElement) {
             setTimeout(() => {
                 tmpElement.text('');
             }, ERROR_MSG_CLEAR_TIME_DELAY);
         }

         function setError(selector, message) {
             var tmpElement = $moduleEl.find(selector).closest('.js-input-placeholder').parent().find('.error-msg');
             tmpElement.text(message);
             clearMessage(tmpElement);
         }

         return {
             messages: ['homeloan:optional', 'moduleLoaded', 'singleSelectOptionsInitiated', 'singleSelectOptionsChanged', 'singleSelectDropdownInitiated', 'singleSelectDropdownChanged','showError'],
             behaviors: ['homeloanFlowTracking'],
             init: function() {

                 element = context.getElement();
                 moduleId = element.id;
                 $ = context.getGlobal('jQuery');
                 moduleConfig = context.getConfig();
                 $moduleEl = $(element);
                 commonService = context.getService('CommonService');
                 pyrService = context.getService('pyrService');
                 HLS = context.getService('homeloanService');
                 context.broadcast('moduleLoaded', {
                     name: 'optional'
                 });
             },
             destroy: function() {
                 /*moduleEl = null;
                 $ = null;*/
             },
             onmessage: function(name, data) {
                 switch (name) {
                     case 'homeloan:optional':
                         allScreenData = data.allScreenData;
                         isMobileDevice = data.isMobileDevice;
                         renderData = data.renderData;
                         render();
                         break;
                     case 'singleSelectOptionsInitiated':
                     case 'singleSelectOptionsChanged':
                            switch(data.name){
                                case 'loanDuration': 
                                optionalData.co_loanDuration = data.value; 
                                break;   
                            }
                         break;
                     case 'singleSelectDropdownInitiated':
                     case 'singleSelectDropdownChanged':
                        switch(data.name){
                            case 'propertyType':
                                context.broadcast('trackPropertyType', { id: moduleId, label: 'propertyDetail', value: data.value })
                                optionalData.co_propertyType = data.value;
                                break;
                            case 'coRelation':
                            optionalData.co_relation = data.value;
                            break;
                        }            
                         break;
                     case 'showError':
                         var tmpElement = $moduleEl.find("#errorOcurred");
                         tmpElement.text('Some Error Occured');
                         clearMessage(tmpElement);
                         break;
                 }
             },
             onclick: function(event, element, elementType) {
                 var screenName = $(element).data('screen');
                 switch (elementType) {
                     case 'next':
                     context.broadcast('trackHomeloanClick', { id: moduleId, label: 'screen2' });
                     context.broadcast('nextClicked', { self: SCREEN_NAME, inputObject:{} });
                         break;
                     case 'previous':
                         switch (screenName) {
                             default: context.broadcast('previousClicked', { self: SCREEN_NAME,previous:screenName });
                             break;
                         }
                         break;
                 }
             },
             onfocusin: function(event, element, elementType) {
                 switch (elementType) {
                     case 'query-text':
                         if ($moduleEl.find(element).closest('.typeAhead-hl.active')) {
                             if (element.name == 'summary_city_name') {
                                 context.broadcast('typeAheadCityChanged', {
                                     city: ''
                                 });
                             }
                         }
                         break;
                 }
             }
         };
     });
 });
