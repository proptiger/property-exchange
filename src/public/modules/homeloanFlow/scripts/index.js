/**
 * This is a module used for applying homeloan.It has following subModules:
 * 1. summary.js -- used for taking input and handling error on the main homeloan screen.
 * 2. otp.js -- used for taking otp as input and submitting it.
 * 3. checkEligibility.js -- used for checkingEligibility for applying instant homeloan for indiabulls .
 * 4. indiaBulls.js -- used for posting India Bulls homeloan Enquiry.
 * 5. optional.js -- used for taking optional field as input and displaying thank you.
 */
"use strict";
define([
    'doT!modules/homeloanFlow/views/index',
    'services/commonService',
    'services/homeloanService',
    'services/utils',
    'services/filterConfigService',
    'modules/homeloanFlow/submodules/otp',
    'modules/homeloanFlow/submodules/summary',
    'modules/homeloanFlow/submodules/optional',
    'modules/homeloanFlow/submodules/checkEligibility',
    'modules/homeloanFlow/submodules/indiaBulls'
], (indexTempalte, commonService, HLS, UtilService, FilterConfigService) => {

    Box.Application.addModule('homeloanFlow', (context) => {
        var element, $moduleEl, $ = context.getGlobal('jQuery'),
            moduleConfig, moduleId, pScreen;
        var screenData = {},
            visitedScreen = {},
            cityData = {},
            templateData={},
            flow,page='',source='';
        let LoginService = context.getService('LoginService');
        var isMobileDevice = false;
        const SCREEN_NAME = {
            SUMMARY: 'summary',
            OTP: 'otp',
            OPTIONAL:'optional',
            NONE: '',
            CHECK_ELIGIBILTY:'checkEligibility',
            INDIA_BULLS:'indiaBulls'
        };
        //scren flow for applying homeloan
        var homeLoanFlow = {
            applyNowFlow: {
                [SCREEN_NAME.SUMMARY]:{
                    previous: SCREEN_NAME.NONE,
                    next:SCREEN_NAME.OTP,
                },
                [SCREEN_NAME.OTP]:{
                    next: SCREEN_NAME.CHECK_ELIGIBILTY,
                    previous:SCREEN_NAME.SUMMARY
                },
                [SCREEN_NAME.CHECK_ELIGIBILTY]:{
                    previous: SCREEN_NAME.OTP,
                    next:SCREEN_NAME.INDIA_BULLS,
                },
                [SCREEN_NAME.INDIA_BULLS]:{
                    previous: SCREEN_NAME.INDIA_BULLS,
                    next:SCREEN_NAME.OPTIONAL,
                },
                [SCREEN_NAME.OPTIONAL]:{
                    previous: SCREEN_NAME.INDIA_BULLS,
                    next:SCREEN_NAME.NONE,
                }
            }
        };
        const startFreshScreenArray = [SCREEN_NAME.CHECK_ELIGIBILTY,SCREEN_NAME.INDIA_BULLS,SCREEN_NAME.OPTIONAL];
        // function to generate render data like next and previous screen of current screen
        function getRenderData(screenName) {
            var obj = {
                screenData: {
                    previous: homeLoanFlow[flow][screenName].previous || '',
                    next: homeLoanFlow[flow][screenName].next
                },
                templateData:templateData
            }
            return obj;
        }
        // broadcast to open popup
        function openHomeloanPopup(){
            context.broadcast('popup:open', {
                id: 'applyHomeLoanProject',
                options: {
                    config: {
                        id: 'homeLoanPopup',
                        modal: true,
                        escKey: true
                    }
                }
            });
        }

        function processData(startScreen, preObject, moduleName) {
            isMobileDevice = UtilService.isMobileRequest() ? true : false;
            var rdata = getRenderData(startScreen);
            screenData = $.extend(screenData, preObject);
            for (var screen in visitedScreen) {
                $moduleEl.find('#' + screen).addClass('hide');
            }
            if (visitedScreen[startScreen]) {
                openHomeloanPopup();
                $moduleEl.find(`#${startScreen}`).removeClass('hide');
                $moduleEl.find(`#${startScreen}` + `-previous`).addClass('hide');
                context.broadcast('updatePrefillData',{allScreenData:screenData});

            } else {
                visitedScreen[startScreen] = true;
                $moduleEl.find(`#${startScreen}`).removeClass('hide');
                commonService.bindOnModuleLoad(moduleName, () => {
                    openHomeloanPopup();
                    context.broadcast(`homeloan:${startScreen}`, { renderData: rdata, allScreenData: screenData, isMobileDevice,page,source});
                });
                $moduleEl.find(`#${startScreen}` + `-previous`).addClass('hide');
            }
        }

        // function to hide the current screen and make next screen visible and make its entry in visited screen

        function shownext(selfScreen, nextScreen, skipFlag) {
            var rdata = getRenderData(nextScreen);
            var startFreshScreen = startFreshScreenArray.indexOf(nextScreen) > -1;
            if (skipFlag) {
                rdata.screenData.previous = selfScreen;
            }
            if (visitedScreen[nextScreen] && !startFreshScreen) {
                $moduleEl.find(`#${selfScreen}`).addClass('hide');
                $moduleEl.find(`#${nextScreen}`).removeClass('hide');
                context.broadcast('updateFields',{allScreenData: screenData});
            } else {
                visitedScreen[nextScreen] = true;
                $moduleEl.find(`#${selfScreen}`).addClass('hide');
                $moduleEl.find(`#${nextScreen}`).removeClass('hide');
                commonService.bindOnModuleLoad(nextScreen, () => {
                    context.broadcast(`homeloan:${nextScreen}`, { renderData: rdata, allScreenData: screenData, isMobileDevice,page,source});
                });
            }

        }
        // set the projectData to null so that it can be updated for every new project
        function updateScreenData(data={}){
            screenData.project_cityName = data.cityName || '';
            screenData.project_cityId = data.cityId || '';
            screenData.price_propertyPrice = data.minPrice || '';
        }

        return {
            messages: ['homeloanFlow', 'nextClicked', 'skipNext', 'previousClicked', 'popup:closed'],
            behaviors: ['homeloanFlowTracking'],
            init: function() {
                element = context.getElement();
                moduleId = element.id;
                $ = context.getGlobal('jQuery');
                moduleConfig = context.getConfig() || {};
                $moduleEl = $(element);
                $moduleEl.find('#homeloanFlowDiv').html(indexTempalte());
                visitedScreen = {};
                commonService = context.getService('CommonService')
                commonService.startAllModules(element, null, null, null, 'homeloanFlow');
                updateScreenData();
                context.broadcast('moduleLoaded', {
                    name: 'homeloanFlow'
                });
            },
            destroy: function() {
                /*moduleEl = null;
                $ = null;*/
            },
            onmessage: function(name, data) {
                switch (name) {
                    // this message is broadcasted whenever apply now for homeloan is clicked from modules like
                    //1.Project Page 2. Property Page 3. Homeloan Page 4. lead Module 5. two-step-pyr module 5. pyr-module
                    case 'homeloanFlow':
                        flow = 'applyNowFlow';
                        page = data.page;
                        source = data.source;
                        processData(data.startScreen, data.preObject, data.startScreen);
                        break;
                    //these messages are broadcasted by its subModule only when it has to switch to next screen(nextClicked) or to the screen next to next (skipNext)
                    case 'nextClicked':
                    case 'skipNext':
                        var skipScreen = homeLoanFlow[flow][data.self].next
                        var nextScreen = name=='skipNext'? homeLoanFlow[flow][skipScreen].next : skipScreen;
                        screenData = $.extend(screenData, data.inputObject);
                        screenData.previous= name=='skipNext'? data.self : screenData.previous
                        templateData=$.extend(templateData,data.templateData);
                        if (!nextScreen) {
                            context.broadcast('popup:close');
                        } else {
                            shownext(data.self, nextScreen, true);
                        }
                        break;
                    // this message is also broadcasted by its submodules only whenever back icon is pressed
                    case 'previousClicked':
                        var previousScreen = data.previous;
                        $moduleEl.find(`#${data.self}`).addClass('hide');
                        $moduleEl.find(`#${previousScreen}`).removeClass('hide');
                        break;
                }
            }
        };
    });
});
