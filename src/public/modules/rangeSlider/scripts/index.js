/*
 Module config data sample
     start: [<minRange>,<maxRange>]
      step: <step>,
      doubleHandle: <true/false>,
     'range': {
         'min': {value: <value>,label:<lable> },
         '20%': {value: <value>,label:<lable> },
         'max': {value: <value>,label:<lable> }
     }
     'formatterType': <price/rates>
}
*/
'use strict';
define([
        "doT!modules/rangeSlider/views/index",
        "slider",
        "services/utils"
    ],
    function(template, noUiSlider) {
        Box.Application.addModule("rangeSlider", function(context) {
            var moduleEl,
                applyrequired,
                scope = {};
            const   RANGE_SLIDER    = '.js-range-slider',
                    RANGE_VALUE_1   = 'js-range-value-1',
                    RANGE_VALUE_2   = 'js-range-value-2',
                    APPLY_FILTER    = 'apply-filters',
                    UtilService = context.getService('Utils'),
                    filterConfigService = context.getService('FilterConfigService');
            var pageData = UtilService.getPageData();
             var _getMargin = function(range) {
                var ordered = [],
                    index,
                    margin = 0;

                for (index in range) {
                    if (range.hasOwnProperty(index)) {
                        ordered.push([range[index], index]);
                    }
                }
                if (ordered.length && typeof ordered[0][0] === "object") {
                    ordered.sort(function(a, b) {
                        return a[0][0] - b[0][0];
                    });
                } else {
                    ordered.sort(function(a, b) {
                        return a[0] - b[0];
                    });
                }
                if (ordered.length >= 2) {
                    margin = ((parseFloat(ordered[1][0]) - parseFloat(ordered[0][0])) * parseFloat(ordered[1][1])) / 100;
                }
                return margin;
            };

            var _prepareSliderConfig = function (config) {
                var start = config.start,
                    snap  = config.snap || false,
                    range =  config.range,
                    step  = config.step,
                    tooltips = config.tooltips,
                    margin,
                    keys,
                    length,
                    sliderConfiguration;
                scope.formatter = config.formatter;
                scope.sliderRange = $.extend(true,{},config.range);
                keys = Object.keys(scope.sliderRange);
                length =  keys.length;
                for(let i = 0; i < length ; i++) {
                    range[keys[i]] = scope.sliderRange[keys[i]].value;
                }

                sliderConfiguration = {
                    snap: snap,
                    start: start,
                    range: range,
                    connect : config.connect,
                    behaviour: "tap",
                    tooltips: tooltips || false,
                    format: {
                        to: function(value) {
                            return value;
                        },
                        from: function(value) {
                            return value;
                        }
                    }
                };

                if(scope.formatter) {
                    delete sliderConfiguration.format;
                }

                if(snap) {
                    margin = _getMargin(range);
                    sliderConfiguration.margin = margin;
                }
                if(step) {
                    sliderConfiguration.step = step;
                    sliderConfiguration.range = {
                        "min": range['min'],
                        "max": range['max']
                    };
                }
                return sliderConfiguration;
            };

            var _encodedValue = function(needle,haystack){
                var keys,
                    length;
                keys = Object.keys(haystack);
                length =  keys.length;
               for(let i= 0; i < length ; i++) {
                   if(haystack[keys[i]].value == needle) {
                      return haystack[keys[i]].label ? haystack[keys[i]].label : haystack[keys[i]].value;
                   }
               }
               return null;
            };

            var _formatValues = function(value,formatterType) {
                if(formatterType === "price") {
                    return UtilService.formatNumber(value, { seperator : window.numberFormat });
                } else if (formatterType === "rates") {
                    return parseFloat(value).toFixed(1) + " %";
                } else if (formatterType === "year") {
                    value = parseInt(value);
                    return (value > 1) ? (value + " yrs") : (value + " yr");
                }
                return value;
            };
            var _getSelectedRangeValues = function(data){

                let eventValue = [],
                    sliderData = data || scope.skipSlider.noUiSlider.get();

                eventValue = sliderData;

                if (eventValue[0] === scope.minVal && eventValue[1] === scope.maxVal) {
                    eventValue = [];
                }else if(eventValue[0] === scope.minVal){
                    eventValue[0] = '';
                }else if(eventValue[1] === scope.maxVal){
                    eventValue[1] = '';
                }
                if(!UtilService.isArray(eventValue)){
                    return eventValue;
                }else{
                    return eventValue.join(',');
                }
            };
            var _attachSliderEvents = function(data){
                var element = moduleEl;
                if(data && data.id == element.id) {
                    scope.skipSlider.noUiSlider.on('update', function(values, handle) {
                        let formattedValue ;
                        if(scope.formatter) {
                            formattedValue = _formatValues(values[handle],scope.formatter);
                        } else {
                            formattedValue = _encodedValue(values[handle],scope.sliderRange);
                        }

                        scope.actualValues[handle].value = values[handle];
                        
                        scope.skipValues[handle].innerHTML = formattedValue;
                        var labelString = scope.sliderName ? (scope.sliderName + '<br>' + formattedValue) : formattedValue;
                        $(scope.skipSlider).find('.noUi-tooltip').html(labelString);
                        
                        context.broadcast('rangeSliderSelected', {
                                name: scope.sliderType,
                                value: values.join(","),
                                morefilter: $(moduleEl).data('morefilter'),
                                id: moduleEl.id
                        });
                    });

                    if(!applyrequired){
                        scope.skipSlider.noUiSlider.on('slide', function() {

                            let eventValue = _getSelectedRangeValues();

                            context.broadcast('rangeSliderChanged', {
                                name: scope.sliderType,
                                value: eventValue,
                                morefilter: $(moduleEl).data('morefilter'),
                                id: moduleEl.id
                            });
                        });
                    }
                }
            };

           

            var init = function() {
                let config,
                    range = {},
                    minRange,
                    maxRange,
                    sliderData,
                    filterModuleConfig,
                    data,
                    start,
                    sliderConfiguration,
                    label,
                    sliderName,
                    doubleHandle;

                moduleEl = context.getElement();
                let moduleConfig = context.getConfig();
                applyrequired = $(moduleEl).data('applyrequired');
                scope.sliderType = $(moduleEl).data('type');
                if (scope.sliderType) {
                    filterModuleConfig = filterConfigService.getFilterModuleConfig(scope.sliderType);
                    config = filterModuleConfig.getModuleConfig(scope.sliderType, pageData);
                } else {
                    config = context.getConfig() || {};
                    scope.sliderType = config.type;
                }
                if(!config){
                    return;
                }
                $.extend(config,moduleConfig);

                start = config.start;
                doubleHandle = config.doubleHandle ;
                minRange = start[0] || 0;
                maxRange = start[1] || undefined;
                label = config.label;
                sliderName = config.sliderName;
                sliderConfiguration = _prepareSliderConfig(config);
                range = config.range;
                var labelValue = {};
                if(label){
                    labelValue.leftLabel = _formatValues(range.min, scope.formatter);
                    labelValue.rightLabel = _formatValues(range.max, scope.formatter);
                }
                scope.sliderName = sliderName;
                moduleEl.innerHTML = template({range:range,doubleHandle: doubleHandle, applyrequired: applyrequired, label:label, labelValue:labelValue, sliderName:sliderName});
                scope.skipSlider = Box.DOM.query(moduleEl, RANGE_SLIDER);
                noUiSlider.create(scope.skipSlider, sliderConfiguration);

                scope.skipValues = [
                    Box.DOM.query(moduleEl, "." + RANGE_VALUE_1),
                    Box.DOM.query(moduleEl, "." + RANGE_VALUE_2)
                ];
                scope.actualValues = [
                    Box.DOM.query(moduleEl, 'input[name="' + RANGE_VALUE_1 + '"]'),
                    Box.DOM.query(moduleEl, 'input[name="' + RANGE_VALUE_2 + '"]')
                ];

                data = {};
                data.id =  moduleEl.id;
                _attachSliderEvents(data);

                // broadcast about its existence
                context.broadcast('moduleLoaded', {
                    name: 'rangeSlider',
                    id: moduleEl.id
                });

                sliderData = scope.skipSlider.noUiSlider.get();

                scope.minVal = range.min;
                scope.maxVal = range.max;

                let eventValue = _getSelectedRangeValues([minRange, maxRange]);

                // broadcast as soon as slider gets loaded
                context.broadcast('rangeSliderInitiated', {
                    name: scope.sliderType,
                    value: eventValue,
                    morefilter: $(moduleEl).data('morefilter'),
                    id: moduleEl.id
                });
                if(label){
                    $(moduleEl).find('.' + RANGE_VALUE_1).addClass('hide');
                    $(moduleEl).find('.'+RANGE_VALUE_2).addClass('hide');
                }else{
                    $(moduleEl).find('.left-label').addClass('hide');
                    $(moduleEl).find('.right-label').addClass('hide');
                }
            };

            

            

            var _reloadSlider = function(data) {
                var element,
                    sliderConfiguration;
                element = moduleEl;
                sliderConfiguration = _prepareSliderConfig(data.sliderOption);
                scope.skipSlider.noUiSlider.destroy();
                noUiSlider.create(scope.skipSlider, sliderConfiguration);
                _attachSliderEvents(data);
            };

            var onmessage = function(name, data) {
                switch(name){
                    case 'reloadSlider' : 
                    if(data.id === moduleEl.id){
                        if(data.sliderOption && data.sliderOption.label ){
                            var leftLabel = _formatValues(data.sliderOption.range.min.value, data.sliderOption.formatter); 
                            var rightLabel = _formatValues(data.sliderOption.range.max.value, data.sliderOption.formatter);
                            $(moduleEl).find('.left-label').text(leftLabel);
                            $(moduleEl).find('.right-label').text(rightLabel);
                        }
                        _reloadSlider(data);
                        context.broadcast('rangeSliderChanged', data);
                    }
                    break;
                }
            };

            return {
                init,
                messages: ['reloadSlider'],
                onmessage,
                onclick: function(event, element, elementType){
                    if(elementType === APPLY_FILTER && applyrequired){

                        let eventValue = _getSelectedRangeValues();

                        context.broadcast("jarvisFilterApplied",{id: moduleEl.id,type: scope.sliderType,value: eventValue});
                        context.broadcast('rangeSliderChanged', {
                            name: scope.sliderType,
                            outsidefilter: true,
                            value: eventValue,
                            morefilter: $(moduleEl).data('morefilter'),
                            id: moduleEl.id
                        });
                    }
                }
            };
        });
    }
);
