define(['services/commonService', 'modules/homeloan/scripts/behaviors/homeloanTracking', 'services/utils'], function(commonService) {
    'use strict';
    Box.Application.addModule("homeloan", function(context) {

        var element,
            moduleId,
            configData,
            $,
            $moduleEl,
            //initialData,
            chartData,
            selectedBanks = [],
            interestedBanks = [],
            preObj = {},
            loanAmount,
            UtilService = context.getService('Utils'),
            configurationWrap = '.js-configuration-wrap';

        function _updateBasePrice(price, unit) {
            $moduleEl.find('.js-base-price').text(price);
            $moduleEl.find('.js-base-price-unit').text(unit);
        }

        function _updateUnitInfo(unitInfo) {
            $moduleEl.find(configurationWrap + ' .js-config-label').text(unitInfo);
        }

        function _openPopup(config) {
            context.broadcast('popup:open', {
                id: config.id,
                options: {
                    config: {
                        modal: config.modal
                    }
                }
            });
        }

        function loadChart(chartData) {
            if (!chartData) {
                return;
            }
            var series = [{
                name: 'amount',
                colorByPoint: true,
                data: [{
                    name: 'Interest',
                    y: chartData.interest,
                    amount: chartData.readableInterest.val + ' ' + chartData.readableInterest.unit
                }, {
                    name: 'Principal',
                    y: chartData.principal,
                    amount: chartData.readablePrincipal.val + ' ' + chartData.readablePrincipal.unit

                }]
            }];
            context.broadcast('loadModule', {
                name: 'chart'
            });
            commonService.bindOnModuleLoad('chart', () => {
                context.broadcast('chartDataLoaded', { id: configData.emiChartId, series: series });
            });
        }

        function trackBankLead(event) {
            let pageSource = configData.source;
            context.broadcast(event, {
                id: moduleId,
                label: selectedBanks.join(','),
                value: selectedBanks.length,
                page: pageSource
            });
        }

        function openHomeloanForm(source) {
            let pageSource = configData.source;
            source = pageSource =="HOME LOAN" ? 'Apply Now 3' : source;
            context.broadcast('trackGetHomeLoan', {
                id: moduleId,
                page: pageSource,
                source
            });
            context.broadcast('loadModule', {
                name: 'homeloanFlow'
            });
            commonService.bindOnModuleLoad('homeloanFlow', () => {
                context.broadcast('homeloanFlow', {
                        startScreen: "summary",
                        preObject: preObj,
                        page:pageSource,
                        source
                    });
            });
        }
        return {
            messages: ['emiDataChanged', 'emiCalculatorLoaded', 'moduleLoaded', 'twosteppyrPopupClosed', 'bankOptionChanged', 'emiSliderChanged', 'homeLoanGetCall','TWO_STEP_PYR_LEAD_SUBMIT_SUCCESS', 'openHomeloanPopup', 'popup:closed', 'interestedBankList'],
            behaviors: ['homeloanTracking'],

            /**
             * Initializes the module and caches the module element
             * @returns {void}
             */

            init: function() {
                element = context.getElement();
                moduleId = element.id;
                configData = context.getConfig() || {};
                preObj.project_cityName = configData.cityName;
                preObj.project_cityId = configData.cityId;
                preObj.price_propertyPrice = configData.minPrice;
                $ = context.getGlobal('jQuery');
                $moduleEl = $(element);
                context.broadcast('moduleLoaded', {
                    'name': 'homeloan',
                    'id': element.id
                });
                context.broadcast('loadModule', {
                    name: 'emiCalculator',
                    loadId: 'emiCalculator'
                });
            },

            onmessage: function(name, data) {

                let pageSource = configData.source;
                switch (name) {
                    case "emiDataChanged":
                        if (data.emiPerMonth)
                            var emi = UtilService.formatNumber(data.emiPerMonth, { precision : 2, returnSeperate : true, convertThousand : true, seperator : window.numberFormat });
                        var loan = UtilService.formatNumber(data.loan, { precision : 2, returnSeperate : true, convertThousand : true, seperator : window.numberFormat });
                        var interest = UtilService.formatNumber(data.totalPayement - data.loan, { precision : 2, returnSeperate : true, convertThousand : true, seperator : window.numberFormat });
                        $moduleEl.find('.js-loan').text(loan.val);
                        $moduleEl.find('.js-loan-unit').text(loan.unit);
                        if (emi.unit == 'K') {
                            $moduleEl.find('.js-emi').text(Math.round(emi.val * 1000));
                        } else {
                            $moduleEl.find('.js-emi').text(emi.val);
                            $moduleEl.find('.js-emi-unit').text(emi.unit);
                        }
                        $moduleEl.find('.js-interest').text(interest.val);
                        $moduleEl.find('.js-interest-unit').text(interest.unit);
                        preObj.income_loan = loan.val;
                        preObj.income_loanUnit = loan.unit;
                        preObj.price_propertyPrice = data.loan;
                        loanAmount = data.loan;
                        chartData = {};
                        chartData.principal = data.loan;
                        chartData.interest = data.totalPayement - chartData.principal;
                        chartData.readablePrincipal = UtilService.formatNumber(chartData.principal, { precision : 2, returnSeperate : true, convertThousand : true, seperator : window.numberFormat });
                        chartData.readableInterest = UtilService.formatNumber(chartData.interest, { precision : 2, returnSeperate : true, convertThousand : true, seperator : window.numberFormat });
                        loadChart(chartData);
                        break;
                    case 'moduleLoaded':
                        if (data.name === 'chart' && data.id === configData.emiChartId) {
                            if (chartData) {
                                loadChart(chartData);
                            }
                        }
                        break;
                    case 'twosteppyrPopupClosed':
                        if (data.isHomeloanLead) {
                            $moduleEl.find('.step1').addClass('hide');
                            $moduleEl.find('.step2').removeClass('hide');
                            $("[data-goto=homeloancard]").removeClass("active").trigger("click");
                        }
                        break;
                    case 'homeLoanGetCall':
                        openHomeloanForm("Get Rates");
                        break;
                    case 'TWO_STEP_PYR_LEAD_SUBMIT_SUCCESS':
                        if (data.isHomeloanLead && data.userInfo && data.userInfo.name) {
                            $moduleEl.find('.js-loan-user').text('Hey ' + data.userInfo.name);
                            trackBankLead('trackHomeLoanLeadAfterOTP');
                        }
                        break;
                    case 'bankOptionChanged':
                        if (data.selected) {
                            selectedBanks.push(data.name);
                            interestedBanks.push(data.id);
                            context.broadcast('trackSelectBankChanged', {
                                id: element.id,
                                label: data.name,
                                page: pageSource
                            });
                        } else {
                            selectedBanks.splice(selectedBanks.indexOf(data.name), 1);
                            interestedBanks.splice(interestedBanks.indexOf(data.id), 1);
                        }
                        context.broadcast('banksForLoanChanged', {
                            count: selectedBanks.length
                        });
                        break;
                    case 'openHomeloanPopup':
                        context.broadcast('popup:open', {
                            id: 'applyHomeLoanProject',
                            options: {
                                config: {
                                    id: 'homeLoanPopup',
                                    modal: true,
                                    escKey: true
                                }
                            }
                        });
                        break;
                    case "popup:closed":
                        let ele = document.getElementById('homeloanFlow');
                        commonService.stopAllModules(ele);
                        commonService.stopModule(ele);
                        break;
                    case 'interestedBankList':
                        console.log(data.selected);
                        preObj.bank_selectedBanks = data.selected;
                        break;
                }
            },

            onclick: function(event, element, elementType) {
                let pageSource = configData.source;
                switch (elementType) {
                    case 'home-loan':
                        openHomeloanForm("Apply Now");
                        break;
                    case 'showHideBankDetails':
                        if ($moduleEl.find('.js-showHideBankDetails').hasClass("show")) {
                            $("[data-goto=homeloancard]").removeClass("active").trigger("click");
                        }
                        $moduleEl.find('.js-showHideBankDetails').toggleClass('show');
                        context.broadcast('trackCompareBanks', {
                            id: moduleId,
                            page: pageSource
                        });
                        break;
                    case 'makaanIQArticle':
                        var dataset = element.dataset;
                        context.broadcast('trackMakaanIQArticle', {
                            id: moduleId,
                            page: pageSource,
                            label: dataset.name
                        });
                        break;
                    case 'recalculate':
                        $moduleEl.find('.step2').addClass('hide');
                        $moduleEl.find('.step1').removeClass('hide');
                        context.broadcast('trackHomeloanRecalculate', {
                            id: moduleId,
                            page: pageSource
                        });
                        break;
                    default:
                }
            },

            /**
             * Destroys the module and clears references
             * @returns {void}
             */
            destroy: function() {
                element = null;
            }
        };
    });
});
