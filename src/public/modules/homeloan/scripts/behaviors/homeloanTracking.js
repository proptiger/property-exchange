define([
    'common/trackingConfigService',
    'services/trackingService'
], function(t, trackingService) {
    'use strict';
    Box.Application.addBehavior('homeloanTracking', function(context) {

        var element;
        const messages = [
            // 'trackLoanConfiguration',
            'trackLoanOptionsChanged',
            'trackCompareBanks',
            'trackSelectBankChanged',
            'trackLoanGetCall',
            'trackGetHomeLoan',
            'trackHomeLoanLeadAfterOTP',
            'trackHomeLoanLeadBeforeOTP',
            'trackMakaanIQArticle',
            'trackHomeloanRecalculate',
            'trackHomeLoanError'
        ];

        var init = function() {
            element = context.getElement();
        };

        var onmessage = function(name, data) {
            if (data.id === element.id) {
                let properties = {};
                let event,category,label,value,page,sourcemodule;
                category = 'Lead Form';
                switch (name) {
                    // case 'trackLoanConfiguration':
                    //     sourcemodule = 'Configuration';
                    //     event = t.CLICK_EVENT;
                    //     page='Project';
                    //     break;
                    case 'trackLoanOptionsChanged':
                        sourcemodule = data.sourcemodule;
                        event = t.CLICK_EVENT;
                        value = data.value;
                        page = data.page;
                        break;
                     case 'trackCompareBanks':
                        sourcemodule = 'Compare Banks';
                        event = t.CLICK_EVENT;
                        page=data.page;
                        break;
                    case 'trackLoanGetCall':
                        sourcemodule = 'Get Call';
                        event = t.CLICK_EVENT;
                        label = data.label;
                        value=data.value;
                        page = data.page;
                        break;
                    case 'trackGetHomeLoan':
                        sourcemodule = data.source;
                        event = t.OPEN_LABEL;
                        page = data.page;
                        label="Home Loan"
                        break;
                    case 'trackHomeLoanLeadAfterOTP':
                        event = 'OTP';
                        label = data.label;
                        value=data.value;
                        page = data.page;
                        break;
                    case 'trackHomeLoanLeadBeforeOTP':
                        event = 'Submit';
                        label = data.label;
                        value=data.value;
                        page = data.page;
                        break;
                    case 'trackMakaanIQArticle':
                        sourcemodule = 'MakaanIQ';
                        event = t.CLICK_EVENT;
                        page = data.page;
                        label = data.label;
                        break;
                    case 'trackHomeloanRecalculate':
                        sourcemodule = 'Calculate EMI';
                        event = t.CLICK_EVENT;
                        page = data.page;
                        break;
                    case 'trackHomeLoanError':
                        sourcemodule = 'Error';
                        event = t.CLICK_EVENT;
                        page = data.page;
                        break;
                    default:
                        return;
                }
                
                properties[t.CATEGORY_KEY] = category;
                properties[t.SOURCE_MODULE_KEY] = sourcemodule;
                properties[t.LABEL_KEY] = label;
                properties[t.VALUE_KEY] = value;
                properties[t.PAGE_KEY] = page;               

                trackingService.trackEvent(event, properties);
            }
        };

        return {
            messages,
            onmessage,
            init,
            destroy: function() {}
        };
    });
});