'use strict';
define([
        "doT!modules/InputBox/views/index"
    ],
    function(template) {
        Box.Application.addModule("InputBox", function(context) {
            var moduleEl,
                isFocussed = false;
            var onfocusin = function(event, element) {
                element.parentNode.classList.add('active');
            };

            var onfocusout = function(event, element) {
                if (!element.value) {
                    element.parentNode.classList.remove('active');
                }
                context.broadcast('InputBox:changed', {value:element.value, element:element});
            };
            var oninput = function(event, element) {
                if (!isFocussed && !element.parentNode.classList.contains('active')) {
                    isFocussed = true;
                    element.parentNode.classList.add('active');
                }
            };

            var _isNumberKey = function(event) {
                var charCode = (event.which) ? event.which : event.keyCode;
                if ([46, 8, 9, 27, 13, 110, 190].indexOf(charCode) !== -1) {
                    return true;
                } else if (charCode === 65 && event.ctrlKey === true) {
                    return true;
                } else if (charCode === 67 && event.ctrlKey === true) {
                    return true;
                } else if (charCode === 88 && event.ctrlKey === true) {
                    return true;
                } else if (charCode >= 35 && charCode <= 39) {
                    return true;
                }
                if ((event.shiftKey || (charCode < 48 || charCode > 57)) && (charCode < 96 || charCode > 105)) {
                    event.preventDefault();
                }


            };


            var _checkForFocus = function(moduleEl) {
                let inputElem = moduleEl.querySelector('[data-type="input-box"]');
                if (inputElem && inputElem.value) {
                    inputElem.parentNode.classList.add('active');
                }
            };

            var init = function() {
                let config;
                const PLACEHOLDER = '.js-input-placeholder';
                moduleEl = context.getElement();
                config = context.getConfig() || {};
                if (!config.inputName) {
                    return;
                }
                config.editable = config.editable || true;
                config.textIndent = config.textIndent || "0px";
                config.className = config.className || "";
                config.required = config.required || false;
                let html = template(config);
                $(moduleEl).find(PLACEHOLDER).html(html);
                if (config.type && config.type === "number") {
                    let inputElem = moduleEl.querySelector('[data-type="input-box"]');
                    if (inputElem) {
                        $(inputElem).bind("keydown", _isNumberKey);
                    }
                }
                _checkForFocus(moduleEl);
                context.broadcast('moduleLoaded', {
                    name: 'InputBox',
                    id: moduleEl.id
                });
            };
            var destroy = function() {
                $(moduleEl).off();
            };

            return {
                init,
                onfocusin,
                onfocusout,
                destroy,
                oninput
            };
        });
    }
);
