define([
    'common/trackingConfigService',
    'services/trackingService',
    'services/urlService'
], function(t, trackingService, urlService) {
    'use strict';
    Box.Application.addBehavior('twosteppyrTracking', function(context) {

        var element;
        const messages = [
            'OTP_RESEND',
            'OTP_VISIBLE',
            'SUBMIT_SUCCESS',
            'FORM_ERROR',
            'FORM_FILL',
            'FORM_SUBMIT',
            'FORM_OPEN',
            'Lead_TWO_PYR_API_SUCCESS',
            'Lead_TWO_PYR_API_FAILURE'
        ],
        categoryMapping = {
            HOMELOAN: t.LOAN_CATEGORY,
            TOPSELLER_PYR: t.LEAD_FORM_CATEGORY,
            GET_CALLBACK: t.LEAD_FORM_CATEGORY
        },
        sourceModuleMapping = {
            TOPSELLER_PYR: t.LEAD_SOURCE_MODULES.TopSellers
        };

        var init = function() {
            element = context.getElement();
        };

        var onmessage = function(name, data) {
            if(element.id !== data.id){
                return;
            }
            let properties = {};
            let category = data.category,
                labelConfig = t.LEAD_LABELS,
                event,
                value,
                label,
                page,
                sellerIds,
                sourceModule,
                phone,
                nonInteraction, 
                listingCategory,
                uniqueIds,
                sectionClicked,
                valuesMapping = {
                    TOPSELLER_PYR: urlService.getUrlParam('page') || 1,
                    HOMELOAN: data.postData && data.postData.interestedBanks && data.postData.interestedBanks.length
                },
                labelMapping = {
                    HOMELOAN: data.postData && data.postData.interestedBanks && data.postData.interestedBanks.join(","),
                    TOPSELLER_PYR: {
                        LISTING: labelConfig.TopSellers,
                        NO_LISTING: labelConfig.TopSellerNoListing
                    }
                };

                sourceModule = sourceModuleMapping[data.moduleConfig.type] || t.LEAD_SOURCE_MODULES[data.trackData.sourceSection];
                phone        = data.postData.phone;
                sellerIds    = data.sellerIds;
                value        = valuesMapping[data.moduleConfig.type];
                listingCategory = data.postData.salesType;

                if (data.moduleConfig.type === "HOMELOAN") {
                    label = labelMapping.HOMELOAN;
                } else if(data.moduleConfig.type === "TOPSELLER_PYR") {
                    if(data.moduleConfig.listingPresent) {
                        label = labelMapping.TOPSELLER_PYR.LISTING;
                    } else {
                        label = labelMapping.TOPSELLER_PYR.NO_LISTING;
                    }
                } else if (data.moduleConfig.type === "GET_CALLBACK") {
                    label = labelConfig[data.trackData.sourceSection];
                }

            switch (name) {
                case "FORM_OPEN":
                    event = t.OPEN_EVENT;
                    sectionClicked = data.trackData && data.trackData.sectionClicked;
                    break;

                case "FORM_FILL":
                    event = t.FORM_FILLED_EVENT;
                    break;

                case "FORM_SUBMIT":
                    properties[t.BUYER_OPT_IN_KEY] = data.postData.optIn ? t.BUYER_OPT_IN_TRUE : t.BUYER_OPT_IN_FALSE;
                    event = t.SUBMIT_EVENT;
                    break;    

                case "OTP_RESEND":
                    event = t.OTP_EVENT;
                    label = t.RESEND_LABEL;
                    break;

                case "OTP_VISIBLE":
                    event = t.OTP_EVENT;
                    label = t.VISIBLE_LABEL;
                    page  = data.trackData && data.trackData.pageSource;
                    break;

                case "FORM_ERROR":
                    event = t.ERROR_EVENT;
                    label = data.label;
                    nonInteraction = 0;
                    page = data.trackData && data.trackData.pageSource;
                    break;
                case "Lead_TWO_PYR_API_SUCCESS":
                    event = t.LEAD_API_SUCCESS_EVENT;
                    label = 'Two Step PYR';
                    uniqueIds = data && data.uniqueIds;
                    break;
                case "Lead_TWO_PYR_API_FAILURE":
                    event = t.LEAD_API_FAILUIRE_EVENT;
                    label = 'Two Step PYR';
                    uniqueIds = data && data.uniqueIds;
                    break;
            }

            properties[t.CATEGORY_KEY] = category;
            properties[t.LABEL_KEY] = data.label;
            properties[t.VALUE_KEY] = value;
            properties[t.SELLER_ID_KEY] = sellerIds;
            properties[t.SOURCE_MODULE_KEY] = sourceModule;
            properties[t.PHONE_USER_KEY] = phone;
            properties[t.NON_INTERACTION_KEY] = nonInteraction;
            properties[t.LISTING_CATEGORY_KEY] = t.makeValuesUniform(listingCategory,t.LISTING_CATEGORY_MAP) ;
            properties[t.SECTION_CLICKED] = sectionClicked;
            if(uniqueIds && uniqueIds.length){
                uniqueIds.forEach(function(uniqueId){
                    properties[t.UNIQUE_ENQUIRY_ID_KEY] = uniqueId;
                    trackingService.trackEvent(event, properties);
                });
            }else{
                trackingService.trackEvent(event, properties);
            }
        };

        return {
            messages,
            onmessage,
            init,
            destroy: function() {}
        };
    });
});
