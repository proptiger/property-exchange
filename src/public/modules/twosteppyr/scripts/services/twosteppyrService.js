'use strict';
define([
    'services/utils',
    'common/sharedConfig',
    "services/leadPyrService",
    "services/filterConfigService"
], (utils, SharedConfig, leadPyrService, filterConfigService) => {
    const SERVICE_NAME = 'twosteppyrService';
    Box.Application.addService(SERVICE_NAME, () => {
        const postTempEnquiryUrl = SharedConfig.apiHandlers.postTempEnquiry().url,
            postEnquiryUrl = SharedConfig.apiHandlers.postEnquiry().url;
        var pageData = utils.getPageData();
        let config = {
            defaultValue: {
                "cityId": pageData.cityId,
                "projectId": pageData.projectId,
                // "listingId": window.pageData.listingId ? window.pageData.listingId : undefined,
                "localityId": pageData.localityId ? pageData.localityId : undefined,
                "cityName": pageData.cityName,
                "applicationType": utils.isMobileRequest() ? "Mobile Site" : "Desktop Site",
                "jsonDump": window.navigator.userAgent,
                "pageName": pageData.pageType || '',
                "domainId": 1,
                "sendOtp": true,
                "optInCheckedStatus": true,
                "bhk": pageData.bhk ? pageData.bhk : undefined,
                "maxBudget": pageData.budget ? pageData.budget : undefined,
                "minBudget": pageData.budget ? pageData.budget : undefined
            },
            steps: ["MAIN", "OTP", "THANKS"],
            defautStep: "MAIN",
            required: {},
            optional: {
                "name": null,
                "email": null,
                "phone": null,
                "salesType": null,
                "countryId": null,
                "cityId": null,
                "cityName": null,
                "localityId": null,
                "projectId": null,
                "enquiryType": null,
                "sendOtp": true,
                "otherCompany": [],
                displayAreaLabel: null,
                interestedBanks: null,
                loanAmount: null,
                formTitle: null,
                listingPresent: null,
                optIn: null,
                "isCommercial": null
            },
            displayFields: {
                "displayAreaLabel": null,
                "formTitle": null,
                "listingPresent": null,
                "otherCompany": null,
                "optInCheckedStatus": null
            },
            trackFields: {
                "pageSource": null,
                "sourceSection": null,
                "sectionClicked":null
            },
            modulesParameters: {
                "isPopup": null
            },
            configurabales: {
                "HOMELOAN": {
                    enquiryType: {
                        id: 11
                    },
                    salesType: "homeloan",
                    multipleCompanyIds: 499
                },
                "TOPSELLER_PYR": {
                    enquiryType: {
                        id: 1
                    }
                },
                "GET_CALLBACK": {
                    enquiryType: {
                        id: 2
                    },
                    salesType: "buy"
                },
                "TOP_DEAL_MAKERS": {
                    enquiryType: {
                        id: 1
                    }
                }
            },
            manualFields: ['name', 'email', 'phone', 'jsonDump', 'multipleCompanyIds', 'cityName', 'cityId', 'salesType', 'companyType', "pageUrl", "pageName", "maxBudget", "countryId", "interestedBanks", "loanAmount", "optInCheckedStatus"]
        };

        const stepsConfig = {
            MAIN: {
                required: config.required,
                optional: config.optional,
                defaultValue: config.defaultValue,
                displayFields: config.displayFields,
                trackFields: config.trackFields,
                manualFields: config.manualFields
            },
            OTP: {
                required: config.required,
                optional: config.optional,
                defaultValue: config.defaultValue,
                displayFields: config.displayFields,
                trackFields: config.trackFields,
                manualFields: config.manualFields
            },
            THANKS: {
                required: config.required,
                optional: config.optional,
                defaultValue: config.defaultValue,
                displayFields: config.displayFields,
                trackFields: config.trackFields,
                manualFields: config.manualFields
            }
        };

        function postTempLead(data = {}) {
            return leadPyrService.postLead(postTempEnquiryUrl, data);
        }

        var verifyOTP = function(data) {
            return leadPyrService.verifyOTP(postTempEnquiryUrl, data);
        };

        function postLead(data = {}) {
            return leadPyrService.postLead(postEnquiryUrl, data);
        }

        var getUserDataFromCookies = leadPyrService.getUserDataFromCookies;
        var _getOtherCompanyIds = leadPyrService.getOtherCompanyIds;

        function _updateManualFields(data, fields) {
            let cookiesData = getUserDataFromCookies();
            let displayData = data.displayData;
            let rawData = data.rawData;
            let _processedData;
            if (rawData.type && config.configurabales[rawData.type]) {
                _processedData = $.extend(true, {}, config.configurabales[rawData.type], data.processedData);
            }

            let processedData = _processedData || data.processedData;
            $.each(fields, (key, val) => {
                if (val == 'jsonDump') {
                    processedData[val] = JSON.stringify(processedData[val]);
                } else if (val == 'name') {
                    processedData[val] = processedData[val] || cookiesData[val];
                } else if (val == 'email') {
                    processedData[val] = processedData[val] || cookiesData[val];
                } else if (val == 'phone') {
                    processedData[val] = processedData[val] || cookiesData[val];
                } else if (val == 'countryId') {
                    processedData[val] = processedData[val] || cookiesData[val] || 1;
                } else if (val == 'cityName') {
                    processedData[val] = processedData[val] || utils.getPageData('cityName');
                } else if (val == 'cityId') {
                    processedData[val] = processedData[val] || utils.getPageData('cityId');
                } else if (val == 'multipleCompanyIds') {
                    if (processedData[val] && !$.isArray(processedData[val])) {
                        processedData[val] = [processedData[val]];
                    }
                    processedData[val] = processedData[val] ? utils.unique(processedData[val].concat(_getOtherCompanyIds(displayData))) : utils.unique(_getOtherCompanyIds(displayData));
                } else if (val == 'salesType') {
                    processedData[val] = (processedData[val] && processedData[val].toLowerCase()) || utils.getPageData('listingType');
                    processedData[val] = processedData[val] && processedData[val].toLowerCase();
                    if (['rent', 'rental'].indexOf(processedData[val]) !== -1) {
                        processedData[val] = 'rent';
                    } else if (processedData[val] === "homeloan") {
                        processedData[val] = "homeloan";
                    } else {
                        processedData[val] = 'buy';
                    }
                } else if (val == 'companyType') {
                    displayData[val] = displayData[val] && displayData[val].toLowerCase() == 'broker' ? 'agent' : displayData[val];
                } else if (val == 'optInCheckedStatus') {
                    displayData[val] = typeof cookiesData[val] == "boolean" ? cookiesData[val] : config.defaultValue[val];
                } else if (val == 'maxBudget') {
                    displayData.price = (displayData[val] && utils.formatNumber(displayData[val], { precision : 2, returnSeperate : true, seperator : window.numberFormat })) || {};
                } else if (val == 'pageUrl') {
                    processedData[val] = window.location.pathname + encodeURIComponent(window.location.search);
                } else if (val == 'pageName') {
                    processedData[val] = utils.getPageData('pageType');
                }
                data[val] = (typeof data[val] == "string" && data[val].trim()) || data[val];
            });
            return {
                processedData,
                rawData,
                displayData
            };
        }

        function updateConfig() {
            var obj = {};
            pageData = utils.getPageData();
            obj["cityId"] = pageData.cityId;
            obj["projectId"] = pageData.projectId;
            obj["localityId"] = pageData.localityId ? pageData.localityId : undefined;
            obj["cityName"] = pageData.cityName;
            obj["pageName"] = pageData.pageType || '';
            obj["bhk"] = pageData.bhk ? pageData.bhk : undefined;
            obj["maxBudget"] = pageData.budget ? pageData.budget : undefined;
            obj["minBudget"] = pageData.budget ? pageData.budget : undefined;
            return obj;
        }

        function processStepData(rawData) {
            let step=rawData.step || "MAIN";
            $.extend(stepsConfig[step].defaultValue, updateConfig());
            return leadPyrService.processStepData({
                rawData,
                updateManualFields: _updateManualFields,
                stepsConfig,
                config
            });
        }
        function setMultipleSellerIDs(data) {
            return leadPyrService.setMultipleSellerIDs(data);
        }

        function getTopSellers(data) {
            let cookieData = leadPyrService.getUserDataFromCookies(),
                selectFilterkeyValObj = filterConfigService.getSelectedQueryFilterKeyValObj(),
                propertyTypeConfig = filterConfigService.getFilterModuleConfig("propertyType").getTemplateData({
                    listingType: "buy"
                });

            if (["rent", "rental"].indexOf(data.category) > -1 || ["rent", "rental"].indexOf(utils.getPageData('listingType')) > -1) {
                propertyTypeConfig = filterConfigService.getFilterModuleConfig("propertyType").getTemplateData({
                    listingType: "rent"
                });
            }
            let localityIds = utils.getPageData('localityId');
            let suburbIds = utils.getPageData('suburbId');
            let _data = {
                cityId: utils.getPageData('cityId') || data.cityId,
                localityIds: (localityIds && typeof localityIds == 'string' ? localityIds.split(',') : [localityIds]) || undefined,
                localityOrSuburbIds: utils.getPageData('localityOrSuburbId')? utils.getPageData('localityOrSuburbId').split(",") : undefined,
                projectId: data.projectId &&  `${data.projectId || ''}`.split(',') || undefined, // use case for featured projects mainly
                suburbIds: (suburbIds && typeof suburbIds == 'string' ? suburbIds.split(',') : [suburbIds]) || undefined,
                salesType: data.category || utils.getPageData('listingType'),
                phone: cookieData.phone,
                expertFilter: utils.isArray(data.expertFilter) ? data.expertFilter.join(",") : "",
                metaInfo: data.metaInfo,
                hideSpecificExperts: data.hideSpecificExperts,
                ignoreExpert: utils.isArray(data.ignoreExpert) ? data.ignoreExpert.join(",") : ""
            };

            if(utils.getPageData('isSerp') && ["suburb", "suburbTaxonomy"].indexOf(utils.getPageData('pageLevel')) > -1){
               delete (_data.localityIds); 
            } else if(utils.getPageData('isSerp') && ["locality", "localityTaxonomy"].indexOf(utils.getPageData('pageLevel')) > -1){
                delete (_data.suburbIds);
            } else if(utils.getPageData('isSerp') && ['multipleSuburbLocality'].indexOf(utils.getPageData('pageLevel')) > -1){
                delete (_data.localityIds);
                delete (_data.suburbIds);
            }
            if (selectFilterkeyValObj.beds) {
                _data.bhk = selectFilterkeyValObj.beds.split(",");
            }
            if (selectFilterkeyValObj.rk) {
                _data.rk = selectFilterkeyValObj.rk;
            }
            if (selectFilterkeyValObj.budget) {
                let _budget = selectFilterkeyValObj.budget.split(",");
                _data.minBudget = _budget[0];
                _data.maxBudget = _budget[1];
            }
            if (selectFilterkeyValObj.propertyType) {
                _data.propertyTypes = selectFilterkeyValObj.propertyType.split(",");
                let propertyTypeIds = [];
                _data.propertyTypes.forEach((elem) => {
                    let _id = leadPyrService.getPropertyIdFromLabel(elem, propertyTypeConfig);
                    if (_id) {
                        propertyTypeIds.push(_id);
                    }
                });
                if (propertyTypeIds.length) {
                    _data.propertyTypes = propertyTypeIds;
                }
            }
            if (!data.sellerCount) {
                data.sellerCount = 4;
            }
            if (data.sellerType) {
                _data.sellerType = data.sellerType;
            }
            if (data.giveBoostToExpertDealMakers) {
                _data.giveBoostToExpertDealMakers = data.giveBoostToExpertDealMakers;
            }
            return leadPyrService.getTopSellers(_data, data.sellerCount);
        }

        return {
            postTempLead,
            processStepData,
            setMultipleSellerIDs,
            postLead,
            verifyOTP,
            getTopSellers
        };
    });
    return Box.Application.getService(SERVICE_NAME);
});
