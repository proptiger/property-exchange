define([
    'doT!modules/twosteppyr/views/homeloan-main',
    'doT!modules/twosteppyr/views/homeloan-otp',
    'doT!modules/twosteppyr/views/topSellerPyr-main',
    'doT!modules/twosteppyr/views/topSellerPyr-otp',
    'doT!modules/twosteppyr/views/topSellerPyr-thanks',
    'text!modules/lead/views/thanks.html',
    'text!modules/lead/views/claimCashback.html',
    'doT!modules/twosteppyr/views/topSellerPyrSellersList',
    'services/commonService',
    'services/leadPyrService',
    'services/urlService',
    'services/utils',
    'modules/twosteppyr/scripts/services/twosteppyrService',
    'behaviors/leadBehaviour',
    'modules/twosteppyr/scripts/behaviors/twosteppyrTracking'
], function(
    homeLoanMainTemplate,
    homeLoanOtpTemplate,
    topSellerPyrMainTemplate,
    topSellerPyrOtpTemplate,
    topSellerPyrThanksTemplate,
    getCallbackThanksTemplate,
    getCallbackClaimCashbackTemplate,
    topSellerPyrSellersListTemplate,
    commonService,
    leadPyrService,
    urlService,
    utils,
    twosteppyrService) {
    'use strict';
    Box.Application.addModule("twosteppyr", function(context) {

        var moduleEl,
            $,
            userData = {},
            scope = {},
            //selectedBanks = [],
            moduleConfig,
            mainTemplate,
            otpTemplate,
            thanksTemplate,
            homeloanData;

        const config = {
            selector: {
                "MAIN": {
                    "PHONE": "[data-type=PHONE_FIELD] input",
                    "COUNTRY_CODE": "[data-country-code=COUNTRY_CODE_FIELD]",
                    "COUNTRY_ID": "input[data-country=COUNTRY_ID_FIELD]",
                    "OPT_IN": "input[data-type=BUYER_OPT_IN]:checked",
                    "NAME": "[data-type=NAME_FIELD] input",
                    "VALID_EMAIL": "[data-type=VALID_EMAIL] input",
                },
                "OTP": {
                    "OTP": "[data-type=OTP_FIELD] input",
                    "RESEND": "[data-type=OTP_RESEND]",
                    "NAME": "[data-type=NAME_FIELD] input",
                    "VALID_EMAIL": "[data-type=VALID_EMAIL] input",
                },
                "WRAPPER": "[twosteppyr-placeholder]",
                "SELLER_LIST": "[data-type='seller-list']",
                "PROCESSING": "[data-processing]",
                "NORMAL": "[data-normal]"
            },
            hideClass: "hide",
            activeClass: "active",
            formTitleMap: {
                HOMELOAN: "connect for best home loans",
                GET_CALLBACK: "share my information"
            }
        };

        var global = {
            isProcessing: false
        };

        function _initTemplates(type) {
            switch (type) {
                case "HOMELOAN":
                    mainTemplate = homeLoanMainTemplate;
                    otpTemplate = homeLoanOtpTemplate;
                    thanksTemplate = topSellerPyrThanksTemplate;
                    break;
                case "GET_CALLBACK":
                case "TOP_DEAL_MAKERS":
                    mainTemplate = homeLoanMainTemplate;
                    otpTemplate = homeLoanOtpTemplate;
                    thanksTemplate = doT.template(getCallbackThanksTemplate, undefined, { claimCashback: getCallbackClaimCashbackTemplate });
                    break;
                case "TOPSELLER_PYR":
                    mainTemplate = topSellerPyrMainTemplate;
                    otpTemplate = topSellerPyrOtpTemplate;
                    thanksTemplate = topSellerPyrThanksTemplate;
                    break;
            }
        }

        function _updateOTPFlag() {
            let isIndianFlag = leadPyrService.isIndian(userData.postData.countryId);
            return leadPyrService.checkOTPVerified(userData.postData.phone).then((response) => {
                userData.postData.sendOtp = isIndianFlag && !response.isVerified;
                userData.isLoggedIn = response.isLoggedIn;
                if (urlService.getUrlParam("utm_otp") === "false") {
                    userData.postData.sendOtp = false;
                }
            }, (response) => {
                userData.postData.sendOtp = isIndianFlag && !response.isVerified;
                userData.isLoggedIn = response.isLoggedIn;
                if (urlService.getUrlParam("utm_otp") === "false") {
                    userData.postData.sendOtp = false;
                }
            });
        }

        function _postTempLead() {
            if (!userData.postData.multipleCompanyIds || !userData.postData.multipleCompanyIds.length) {
                _gotoStep("THANKS", userData);
            } else {
                twosteppyrService.postTempLead(userData.postData).then((response) => {
                    if (response) {
                        _track("Lead_TWO_PYR_API_SUCCESS",{uniqueIds: [`SAPPHIRE_${response.id}`]});
                        scope.postEnquiryResponse = response;
                        userData.postData.sendOtp = true;
                        _gotoStep("OTP", userData);
                    }
                }, () => {
                    _track("Lead_TWO_PYR_API_FAILURE",{uniqueIds: ['SAPPHIRE']});
                    context.broadcast("SHOW_ERROR", {
                        errorType: "GENERIC",
                        field: "GLOBAL"
                    });
                });
            }
        }

        var _verifyOTP = function(userData, scope) {
            var _data = {
                userId: scope.postEnquiryResponse.userId,
                phone: userData.postData.phone,
                name: userData.postData.name,
                email: userData.postData.email,
                userEnquiryId: scope.postEnquiryResponse.id,
                otp: userData.postData.otp
            };
            return twosteppyrService.verifyOTP(_data);
        };

        var _updateContactNumbers = function(setExpiry = false) {
            let obj = {
                "contactNumber": userData.postData.phone,
                "isVerified": scope.isVerified
            };
            leadPyrService.updateContactNumbers(obj, setExpiry);
        };

        function _showResendButton(show = true) {
            if (!show) {
                $(moduleEl).find(config.selector.OTP.RESEND).addClass("invisible");
                $(moduleEl).find(config.selector.OTP.RESEND).prop("pointer-events", "none");
            } else {
                $(moduleEl).find(config.selector.OTP.RESEND).removeClass("invisible");
                $(moduleEl).find(config.selector.OTP.RESEND).removeProp("pointer-events");
            }
        }

        function _showHideProcessing(hide) {
            let actionButton = $(moduleEl).find(config.selector["NORMAL"]),
                processingButton = $(moduleEl).find(config.selector["PROCESSING"]);
            if (hide) {
                processingButton.addClass(config.hideClass);
                actionButton.removeClass(config.hideClass);
                global.isProcessing = false;
            } else {
                processingButton.removeClass(config.hideClass);
                actionButton.addClass(config.hideClass);
                global.isProcessing = true;
            }
        }

        var _resendOtp = function() {
            delete userData.postData.otp;
            _verifyOTP(userData, scope).then(function(response) {
                if (response) {
                    context.broadcast("SHOW_SUCCESS", {
                        moduleEl,
                        successType: "SUCCESS_OTP",
                        field: "GLOBAL"
                    });
                    _focusInput("OTP", true);
                } else {
                    context.broadcast("SHOW_ERROR", {
                        moduleEl,
                        errorType: "GENERIC",
                        field: "GLOBAL"
                    });
                }
                _showResendButton(false);
                setTimeout(() => {
                    _showResendButton(true);
                }, 2000);
            }, () => {
                context.broadcast("SHOW_ERROR", {
                    moduleEl,
                    errorType: "GENERIC",
                    field: "GLOBAL"
                });
                _showResendButton(false);
                setTimeout(() => {
                    _showResendButton(true);
                }, 2000);
            });

        };

        function emptyFunction() {}

        function _startAllModules() {
            commonService.startAllModules(moduleEl);
        }

        function _stopAllModules() {
            commonService.stopAllModules(moduleEl);
        }

        function _appendHtmlToView(html, callback) {
            _stopAllModules();
            $(moduleEl).find(config.selector["WRAPPER"]).html(html);
            _startAllModules();
            if (callback && typeof callback === "function") {
                callback();
            }
        }

        function _focusInput(selector, clear = false) {
            let _elem = $(moduleEl).find(`[name=${selector}]`);
            if (_elem) {
                _elem.focus();
                if (clear) {
                    _elem.val("");
                }
            }
        }

        function _appendSellerHTML() {
            let html;
            if (userData.displayData && userData.displayData.otherCompany && userData.displayData.otherCompany.length) {
                let templateData = $.extend(true, [], userData.displayData.otherCompany);
                templateData[Math.ceil(templateData.length / 2) - 1].active = true;
                html = topSellerPyrSellersListTemplate(templateData);
            } else {
                html = '<div class="img-wrap ta-c"><img alt="" src="/images/pyr-house.png"></div>';
            }
            $(moduleEl).find(config.selector["SELLER_LIST"]).html(html);
        }

        function _gotoStep(step, data = {}) {
            let html,
                func = emptyFunction,
                templateData = $.extend(true, {}, data);

            leadPyrService.pruneDummyNameAndEmailFields(templateData.postData);
            switch (step) {
                case "MAIN":
                    html = mainTemplate(templateData);
                    if (moduleConfig.type === "TOPSELLER_PYR") {
                        func = (function() {
                            _appendSellerHTML();
                        }).bind(undefined);
                    }
                    break;
                case "OTP":
                    html = otpTemplate(templateData);
                    func = _focusInput.bind(undefined, "OTP");
                    _track("OTP_VISIBLE");
                    break;
                case "THANKS":
                    templateData.step = "THANKS";
                    templateData.isBuyLead = userData && userData.postData.salesType=="buy";
                    html = thanksTemplate(templateData);
                    let isHomeloanLead = (userData.postData.enquiryType.id === 11);
                    if (isHomeloanLead) {
                        context.broadcast('popup:close');
                        context.broadcast('twosteppyrPopupClosed', {
                            id: moduleEl.id,
                            isHomeloanLead
                        });
                    }
                    break;
            }
            _appendHtmlToView(html, func);
        }

        function _submitLead() {
            delete userData.postData.otp;
            if (!userData.postData.multipleCompanyIds || !userData.postData.multipleCompanyIds.length) {
                _gotoStep("THANKS", userData);
            } else {
                twosteppyrService.postLead(userData.postData).then((response) => {
                    if (response) {
                        let uniqueIds=[];
                        response.enquiryIds && response.enquiryIds.forEach(function(enquiryId){
                           uniqueIds.push(`PETRA_${enquiryId}`);
                        });
                         _track('Lead_TWO_PYR_API_SUCCESS',{uniqueIds});
                        _gotoStep("THANKS", userData);
                        context.broadcast("TWO_STEP_PYR_LEAD_SUBMIT_SUCCESS", {
                            id: moduleEl.id,
                            isHomeloanLead: userData.postData.enquiryType.id === 11,
                            userInfo: userData.postData
                        });
                    }
                }, () => {
                    _track("Lead_TWO_PYR_API_FAILURE",{uniqueIds: ['PETRA']});
                    context.broadcast("SHOW_ERROR", {
                        errorType: "GENERIC",
                        field: "GLOBAL"
                    });
                });
            }
        }

        function _fetchDataFromUI(step) {
            let data = {},
                phoneElem = $(moduleEl).find(config.selector[step]['PHONE']),
                nameElem = $(moduleEl).find(config.selector[step]['NAME']),
                emailElem = $(moduleEl).find(config.selector[step]['VALID_EMAIL']),
                countryIdElem = $(moduleEl).find(config.selector[step]['COUNTRY_ID']),
                otpElem = $(moduleEl).find(config.selector[step]['OTP']),
                optInElem = $(moduleEl).find(config.selector[step]['OPT_IN']);

            if (phoneElem && phoneElem.val()) {
                data["PHONE".toCamelCase()] = phoneElem.val().trim();
            }

            if (nameElem && nameElem.val()) {
                data["NAME".toCamelCase()] = nameElem.val().trim();
            }

            if (emailElem && emailElem.val()) {
                data["EMAIL".toCamelCase()] = emailElem.val().trim();
            }

            if (countryIdElem && countryIdElem.val()) {
                data["COUNTRY_ID".toCamelCase()] = countryIdElem.val().trim();
            }

            if (otpElem && otpElem.val()) {
                data["OTP".toCamelCase()] = otpElem.val().trim();
            }

            if (optInElem) {
                data["OPT_IN".toCamelCase()] = optInElem.val() ? true : false;
            }

            return data;
        }
        function _setHomeloanData(data){
            homeloanData={};
            homeloanData.summary_mobile = data.postData.phone;
            homeloanData.summary_email = data.postData.email;
            homeloanData.summary_name = data.postData.name;
            homeloanData.project_cityName = data.postData.cityName;
            homeloanData.project_cityId = data.postData.cityId;
            homeloanData.price_propertyPrice = data.postData.minBudget;
        }
        function openHomeloanForm() { 
            context.broadcast('loadModule', {
                name: 'homeloanFlow',
            });
            
            commonService.bindOnModuleLoad('homeloanFlow', () => {
                context.broadcast('homeloanFlow', {
                    startScreen: "summary",
                    preObject: homeloanData,
                });
            });
        }
        

        function _submit(step) {
            if (global.isProcessing) {
                return;
            }
            let formData = _fetchDataFromUI(step);
            switch (step) {
                case "MAIN":
                    
                    _track("FORM_FILL");
                    _track("FORM_SUBMIT");
                    _showHideProcessing(false);
                    $.extend(userData.postData, formData);
                    _updateOTPFlag().always(() => {
                        if (!userData.postData.sendOtp || ["GET_CALLBACK"].indexOf(moduleConfig.type) > -1) {
                            _submitLead();
                        } else {
                            _postTempLead();
                        }
                    });
                    _setHomeloanData(userData);
                    _showHideProcessing(true);
                    break;
                case "OTP":
                    _showHideProcessing(false);
                    $.extend(userData.postData, formData);
                    _verifyOTP(userData, scope).then((response) => {
                        if (response) {
                            _updateContactNumbers();
                            if (response.isOtpVerified) {
                                scope.isVerified = true;
                                !scope.isLoggedIn && _updateContactNumbers(true); //jshint ignore:line
                                _submitLead();
                            } else {
                                context.broadcast("SHOW_ERROR", {
                                    errorType: "INVALID",
                                    field: "OTP"
                                });
                            }
                        }
                    }, () => {
                        context.broadcast("SHOW_ERROR", {
                            errorType: "GENERIC",
                            field: "GLOBAL"
                        });
                    });
                    _showHideProcessing(true);
                    break;
            }
        }

        function _track(broadcastMessage, data) {
            let sellerListTrackingText = _getTrackingLabel(),
                trackData = $.extend(true, userData, { sellerIds: sellerListTrackingText, moduleConfig, id: moduleEl.id });
            context.broadcast(broadcastMessage, $.extend(true, trackData, data));
        }

        function _processAndRender(moduleConfig = {}) {
            moduleConfig.formTitle = config.formTitleMap[moduleConfig.type];
            moduleConfig.isCommercial = utils.getPageData('isCommercial');
            userData = twosteppyrService.processStepData(moduleConfig);
            if(userData && userData.postData){
                userData.postData.multipleSellerIds = twosteppyrService.setMultipleSellerIDs(userData);
            }
            _initTemplates(moduleConfig.type);
            _gotoStep("MAIN", userData);
        }

        function _postInitActions(moduleConfig = {}) {
            _processAndRender(moduleConfig);
            context.broadcast('moduleLoaded', {
                'name': 'twosteppyr',
                'id': moduleEl.id
            });
        }

        function _getTrackingLabel() {
            let label = "",
                sellerList = userData.displayData && userData.displayData.otherCompany;
            if (sellerList && sellerList.length) {
                let ids = [];
                sellerList.forEach((v) => {
                    ids.push(v.id);
                });
                return ids.join(",") + '_' + ids.length;
            }
            return label;
        }


        return {
            messages: [
                'singleSelectDropdownChanged',
                'singleSelectDropdownInitiated',
                'navigateNextStep',
                "openGetCallBackForm",
                "openTwoStepPyr"
            ],
            behaviors: ['leadBehaviour', 'twosteppyrTracking'],

            init: function() {
                moduleEl = context.getElement();
                $ = context.getGlobal('jQuery');
                moduleConfig = context.getConfig() || {};
                moduleConfig.category = moduleConfig.salesType;
                if (moduleConfig.type === "TOPSELLER_PYR") {
                    twosteppyrService.getTopSellers(moduleConfig).then((response) => {
                        if (response.length) {
                            $.extend(moduleConfig, { otherCompany: response });
                            _postInitActions(moduleConfig);
                        } else {
                            _postInitActions(moduleConfig);
                        }
                    }, () => {
                        _postInitActions(moduleConfig);
                    });
                } else {
                    _postInitActions(moduleConfig);
                }
            },

            onmessage: function(name, data) {
                switch (name) {
                    case "navigateNextStep":
                        if (data && data.id === moduleEl.id && data.step) {
                            _submit(data.step);
                        }
                        break;
                    case "leadFormError":
                        _track("FORM_ERROR", data);
                        break;
                    case "openTwoStepPyr":
                        if (data.id === moduleEl.id) {
                            let toTrackopen = data.trackOpen || false;
                            _processAndRender(data);
                            (toTrackopen) ? _track("FORM_OPEN", data): ""; //jshint ignore:line
                        }
                        break;
                }
            },
            onclick: function(event, element, elementType) {
                switch (elementType) {
                    case "OTP_ONCALL":
                        leadPyrService.sendOTPOnCall({
                            userId: scope.postEnquiryResponse.userId,
                            userNumber: scope.postEnquiryResponse.phone,
                            element: $(element)
                        });
                        break;
                    case "OTP_RESEND":
                        _track("OTP_RESEND");
                        _resendOtp();
                        break;
                    case "BACK":
                        _gotoStep("MAIN", userData);
                        break;
                    case "THANKS_JOURNEY":
                        let url = $(element).data('journey-url');
                        context.broadcast('popup:close');
                        setTimeout(() => {
                            commonService.ajaxify(url);
                        });
                        break;
                    case 'CONTINUE_SEARCH':
                          _track('trackLeadContinueSearch');
                    case "THANKS_GOTIT":
                    case "CLOSE_LEAD":
                        context.broadcast("popup:close");
                        break;
                    case "BUYER_OPT_IN":
                        let checkedStatus = $(element).prop("checked");
                        userData.displayData.optInCheckedStatus = checkedStatus;
                        leadPyrService.updateOptInStatus(checkedStatus);
                        break;
                    case "homeloan_apply":
                        _track('trackLeadContinueHomeloan');
                        if(moduleConfig.isOpen){
                            openHomeloanForm();
                        }else{
                            context.broadcast("popup:close",{callback:openHomeloanForm});
                        }
                        break;
                }
            },
            onmouseover: function(event, element, elementType) {
                switch (elementType) {
                    case "seller-card":
                        $(moduleEl).find("[data-type=seller-card]").removeClass(config.activeClass);
                        $(element).addClass(config.activeClass);
                }
            },
            destroy: function() {
                moduleEl = null;
                userData = null;
                $ = null;
                scope = null;
            }
        };
    });
});
