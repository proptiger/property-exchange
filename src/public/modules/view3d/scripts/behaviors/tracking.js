'use strict';
define([
    'common/trackingConfigService',
    'services/trackingService'
], function(t, trackingService) {
    Box.Application.addBehavior('view3dTracking', function(context) {
        const moduleEl = context.getElement();

        var messages = ['trackWalkthroughLoaded_'+moduleEl.id, 'trackConfigurationLoaded_'+moduleEl.id, 'trackInteractionIframe_'+moduleEl.id];

        var onmessage = function(name, data) {
            let event,
                category,
                budget,
                bhk,
                listingId,
                unitType;
            let properties = {},
                nonInteraction;

            switch (name) {
                case 'trackWalkthroughLoaded_'+moduleEl.id:
                    category = t.THREE_D_PHY;
                    event = t.THREE_D_PHY_EVENTS.WALKTHROUGH_LOADED_EVENT;
                    nonInteraction = 1;
                    break;
                case 'trackConfigurationLoaded_'+moduleEl.id:
                    category = t.THREE_D_PHY;
                    budget = data.budget;
                    bhk = data.bhk;
                    unitType = data.unittype;
                    listingId = data.listingid;
                    event = t.THREE_D_PHY_EVENTS.CONFIGURATION_LOADED_EVENT;
                    nonInteraction = 1;
                    break;
                case 'trackInteractionIframe_'+moduleEl.id:
                    category = t.THREE_D_PHY;
                    event = t.THREE_D_PHY_EVENTS.INTERACTION_EVENT;
                    break;
            }

            properties[t.CATEGORY_KEY] = category;
            properties[t.BUDGET_KEY] = budget;
            properties[t.BHK_KEY] = t.makeValuesUniform(bhk,t.BHK_MAP);
            properties[t.UNIT_TYPE_KEY]  = t.makeValuesUniform(unitType,t.UNIT_TYPE_MAP) ;
            properties[t.LISTING_ID_KEY]  = listingId;
            properties[t.NON_INTERACTION_KEY] = nonInteraction;

            trackingService.trackEvent(event, properties);
        };


        return {
            messages: messages,
            onmessage: onmessage,
            init: function() {},
            destroy: function() {}
        };
    });
});
