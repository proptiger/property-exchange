"use strict";
define([
    "doT!modules/view3d/views/index",
    'services/utils',
    'services/commonService',
    'modules/view3d/scripts/behaviors/tracking'
], function(template) {
    Box.Application.addModule("view3d", function(context) {
        var isFrameLoaded = false,
            isClickTriggered = false,
            config;

    //    const queryData = Box.DOM.queryData,
      //      _utils = context.getService('Utils'),
        const FULL_SIZE_CLASS = "lg-fullsize-on",
            ACTIVE_CLASS = "active",
            LOADING_STATE_CLASS = "loading-state",
            READY_STATE_CLASS = "ready-state",

            moduleEl = context.getElement(),
            HTML_CONTAINER = $('html'),

            VIEWER_IFRAME_DATA_TYPE = Box.DOM.queryData(moduleEl, 'type', 'viewer-iframe'), //$('#viewer-iframe'),
            MAIN_IFRAME_DATA_TYPE = Box.DOM.queryData(moduleEl, 'type', 'main-iframe'), //$('#viewer-iframe'),
            VIEWER_OVERLAY_DATA_TYPE = Box.DOM.queryData(moduleEl, 'type', 'viewer-overlay'), //$('#viewer-overlay'),
            VIEWER_IFRAME_CLOSE_DATA_TYPE = Box.DOM.queryData(moduleEl, 'type', 'viewer-iframe-close'), //$('#viewer-iframe-close'),
            EXPLORE_3D_VIEW_DATA_TYPE = Box.DOM.queryData(moduleEl, 'type', 'explore-3d-view'), //$('#viewer-iframe-close'),
            iframeData = $(MAIN_IFRAME_DATA_TYPE).data(),

            messages = [
                "ready_iframe_" + iframeData.icid
            ],
            behaviors = [
                'view3dTracking'
            ];


        function _loadAfterClick() {
            if(!isFrameLoaded) {
                if (config['isMobileRequest']) {
                    // Hide header
                    HTML_CONTAINER.addClass(FULL_SIZE_CLASS);

                    // Expand Viewer
                    $(VIEWER_IFRAME_DATA_TYPE).addClass(ACTIVE_CLASS);
                }

                isFrameLoaded = true;

                $(EXPLORE_3D_VIEW_DATA_TYPE).html(template({
                    text: config['exploreText'],
                    isConfiguration: config['isConfiguration'],
                    className: "ready"
                }));
                // Display "Hide Viewer close" button
                $(VIEWER_IFRAME_CLOSE_DATA_TYPE).show();

                // Hide 3dviewer overlay div (including "explore 3d" & "open desktop gallery" buttons)
                $(VIEWER_OVERLAY_DATA_TYPE).hide();

                if (config['isConfiguration']) {
                    // Broadcast event to parent module
                    context.broadcast('explore-3d-view-clicked');
                } else {
                    context.broadcast('viewer-global-overlay-hide');
                }
                MAIN_IFRAME_DATA_TYPE.contentWindow.postMessage({
                    "type": 'showUI',
                    "data": {}
                }, '*');
            }
        }

        function _trackEvent() {
            let dataset = $(MAIN_IFRAME_DATA_TYPE).data() || {};
            if (config['isConfiguration']) {
                context.broadcast('trackConfigurationLoaded_' + moduleEl.id, dataset);
            } else {
                context.broadcast('trackWalkthroughLoaded_' + moduleEl.id, dataset);
            }
        }

        function init() {
            isFrameLoaded = false;
            config = context.getConfig() || {};

            MAIN_IFRAME_DATA_TYPE.setAttribute('src', MAIN_IFRAME_DATA_TYPE.dataset.futurespaceSrc);
            $(VIEWER_IFRAME_CLOSE_DATA_TYPE).hide();
            $(EXPLORE_3D_VIEW_DATA_TYPE).html(template({
                text: config['exploreText'],
                isConfiguration: config['isConfiguration'],
                className: "ready"
            }));

        }

        function onclick(event, element, elementType) {
            switch (elementType) {
                    case "close-3d-view":

                    isClickTriggered = false;

                    context.broadcast('trackInteractionIframe_' + moduleEl.id);

                    MAIN_IFRAME_DATA_TYPE.contentWindow.postMessage({
                        "type": 'hideUI',
                        "data": {}
                    }, '*');

                    if (config['isMobileRequest']) {
                        // Show header
                        HTML_CONTAINER.removeClass(FULL_SIZE_CLASS);

                        // Collapse Viewer
                        $(VIEWER_IFRAME_DATA_TYPE).removeClass(ACTIVE_CLASS);
                    }

                    $(EXPLORE_3D_VIEW_DATA_TYPE).html(template({
                        text: config['exploreText'],
                        isConfiguration: config['isConfiguration'],
                        className: "ready"
                    }));

                    // Hide "Hide Viewer close" button
                    $(VIEWER_IFRAME_CLOSE_DATA_TYPE).hide();

                    // Show 3dviewer overlay div (including "explore 3d" & "open desktop gallery" buttons)
                    $(VIEWER_OVERLAY_DATA_TYPE).show();

                    if (config['isConfiguration']) {
                        // Broadcast event to parent module
                        context.broadcast('close-3d-view-clicked');
                    } else {
                        context.broadcast('viewer-global-overlay-show');
                    }
                    break;
                case "explore-3d-view":
                    isClickTriggered = true;
                    MAIN_IFRAME_DATA_TYPE.contentWindow.postMessage({
                        "type": 'showUI',
                        "data": {}
                    }, '*');

                    context.broadcast('trackInteractionIframe_' + moduleEl.id);

                    if (isFrameLoaded) {
                        // this is responsible for opening in full screen
                        if (config['isMobileRequest']) {
                            // Hide header
                            HTML_CONTAINER.addClass(FULL_SIZE_CLASS);

                            // Expand Viewer
                            $(VIEWER_IFRAME_DATA_TYPE).addClass(ACTIVE_CLASS);
                        }

                        // Display "Hide Viewer close" button
                        $(VIEWER_IFRAME_CLOSE_DATA_TYPE).show();

                        // Hide 3dviewer overlay div (including "explore 3d" & "open desktop gallery" buttons)
                        $(VIEWER_OVERLAY_DATA_TYPE).hide();

                        if (config['isConfiguration']) {
                            // Broadcast event to parent module
                            context.broadcast('explore-3d-view-clicked');
                            context.broadcast('futureSpaceConfigClicked');
                        } else {
                            context.broadcast('viewer-global-overlay-hide');

                        }
                    } else {
                        $(EXPLORE_3D_VIEW_DATA_TYPE).html(template({
                            text: config['loadingText'],
                            isConfiguration: config['isConfiguration'],
                            className: "loading"
                        }));

                        $(MAIN_IFRAME_DATA_TYPE).load(_loadAfterClick);
                    }
                    break;
            }
        }

        function onmessage(name) {
            switch (name) {
                case 'ready_iframe_' + iframeData.icid:
                    if (!isFrameLoaded) {
                        isFrameLoaded = true;
                        _trackEvent();
                        $(VIEWER_OVERLAY_DATA_TYPE).removeClass(LOADING_STATE_CLASS).addClass(READY_STATE_CLASS);
                        if(isClickTriggered) {
                            // Display "Hide Viewer close" button
                            $(VIEWER_IFRAME_CLOSE_DATA_TYPE).show();

                            // Hide 3dviewer overlay div (including "explore 3d" & "open desktop gallery" buttons)
                            $(VIEWER_OVERLAY_DATA_TYPE).hide();

                            if (config['isConfiguration']) {
                                // Broadcast event to parent module
                                context.broadcast('explore-3d-view-clicked');
                                context.broadcast('futureSpaceConfigClicked');
                            } else {
                                context.broadcast('viewer-global-overlay-hide');
                            }
                        }
                    }
                    break;
            }
        }

        function destroy() {
            _resetModule();
        }

        function _resetModule() {
            MAIN_IFRAME_DATA_TYPE.setAttribute('src', '');
            $(VIEWER_IFRAME_CLOSE_DATA_TYPE).hide();
            $(EXPLORE_3D_VIEW_DATA_TYPE).html(template({
                text: config['exploreText'],
                isConfiguration: config['isConfiguration'],
                className: "ready"
            }));
            
            isFrameLoaded = true;
            isClickTriggered = false;
            if (config['isConfiguration']) {
                // Broadcast event to parent module
                context.broadcast('close-3d-view-clicked');
            } else {
                context.broadcast('viewer-global-overlay-show');
            }

            $(VIEWER_OVERLAY_DATA_TYPE).show().addClass(LOADING_STATE_CLASS).removeClass(READY_STATE_CLASS);
            $(MAIN_IFRAME_DATA_TYPE).unload();

        }

        return {
            init,
            onclick,
            behaviors,
            onmessage,
            destroy,
            messages: messages
        };
    });
});
