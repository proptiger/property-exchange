'use strict';
define([
        "doT!modules/singleSelectOptions/views/singleSelectOptions",
        "doT!modules/singleSelectOptions/views/singleSelectRadioButtons",
        "doT!modules/singleSelectOptions/views/singleSelectList",
        "common/sharedConfig",
        "services/filterConfigService",
        "services/utils"
    ],
    function(optionsTemplate, radioTemplate, listTemplate,sharedConfig) {
        Box.Application.addModule("singleSelectOptions", function(context) {

            const FilterConfigService = context.getService('FilterConfigService'),
                UtilService = context.getService('Utils');
            const moduleConfig = context.getConfig();
            const moduleElement = context.getElement();
            const DATA_TYPE = 'type',
                OPTIONS_WRAPPER = 'single-select-option',
                RADIO_VIEW = 'radio',
                LIST_VIEW = 'list';

            var pageData = UtilService.getPageData();
            var selectOptions, selectedElement, IS_CHECKBOX;

            var _getSelectedElement = function() {
                return $(moduleElement).find("input:checked")[0];
            };

            return {
                /**
                 * Initializes the module and caches the module element
                 * @returns {void}
                 */
                init: function() {
                    var template = optionsTemplate,
                        templateFromPromise =  (moduleConfig && moduleConfig.templateFromPromise) || false,
                        dataset = $(moduleElement).data() || {};
                    if (dataset.view == RADIO_VIEW) {
                        template = radioTemplate;
                    } else if (dataset.view == LIST_VIEW) {
                        template = listTemplate;
                    }

                    // MAKAAN-5170 : Change Default Sort order from Relevance to Recency on SERP
                    if(dataset.type=="sortBy"){
                        if(pageData && sharedConfig.recencySortedCities.indexOf(pageData.cityId)> -1 && !pageData.sortBy){
                            pageData.sortBy = "date-desc";
                        }
                    }

                    FilterConfigService.renderFilter(moduleElement, pageData, template,templateFromPromise).then(function(){
                        IS_CHECKBOX = moduleElement.querySelectorAll('input[type="checkbox"]');
                        IS_CHECKBOX = IS_CHECKBOX && IS_CHECKBOX.length ? true : false;
                        selectOptions = Box.DOM.queryAllData(moduleElement, DATA_TYPE, OPTIONS_WRAPPER);
                        selectedElement = _getSelectedElement();
                        var eventData = {};
                        if (selectedElement) {
                            eventData = UtilService.deepCopy($(selectedElement).data());
                        } else {
                            eventData.name = dataset.type;
                        }
                        eventData.morefilter = dataset.morefilter;
                        eventData.id = moduleElement.id;
                        context.broadcast('singleSelectOptionsInitiated', eventData);
                    });
                },

                onclick: function(event, element, elementType) {
                    switch (elementType) {
                        case OPTIONS_WRAPPER:
                            if (!IS_CHECKBOX && selectedElement == element) {
                                $('.js-sortby-wrap').trigger('mouseleave');
                                break;
                            }

                            let eventData = UtilService.deepCopy($(element).data());

                            if(IS_CHECKBOX && !element.checked){
                                eventData.unselectedValue = eventData.value;
                                selectedElement = null;
                                eventData.value = null;
                                $(selectOptions).removeAttr('checked');
                            }else {
                                selectedElement = element;
                                $(selectOptions).removeAttr('checked');
                                element.checked = true;
                                eventData.selected = element.checked;
                            }

                            eventData.morefilter = $(moduleElement).data('morefilter');
                            eventData.id = moduleElement.id;
                            context.broadcast('singleSelectOptionsChanged', eventData);
                            break;
                    }
                    event.stopPropagation();
                },

                /**
                 * Destroys the module and clears references
                 * @returns {void}
                 */
                destroy: function() {
                    selectOptions = null;
                }
            };
        });
    }
);
