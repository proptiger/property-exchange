/**
 * @fileoverview Type-ahead for location search using google apis and default amenity to be shown on commute
 * @author [Aditya Jha]
 */

/*global Box*/
'use strict';
define([
    'doT!modules/mapsModule/views/customDirection',
    'doT!modules/mapsModule/views/customDirectionSuggestions',
    'modules/mapsModule/scripts/services/mapsConfig',
    'modules/mapsModule/scripts/services/mapDirection',
    'modules/mapsModule/scripts/services/placeService',
    'common/sharedConfig',
    'services/apiService',
    'services/commonService',
    'services/localStorageService'
], (template, suggestionsTemplete, mapsConfig, mapDirection, placeService, sharedConfig, apiService, commonService, localStorageService) => {
    Box.Application.addModule('customDirectionModule', (context) => {

        var doT, $, customDirectionModule, map,
            suggestionsInfo, position, cityId,
            selectedResults, citySearchStorageKey, suggestionResult;

        const SUGGESTIONS_DIV_ID = 'customDirectionModule_suggestions',
              SEARCH_TEXT_ID = 'customDirectionModule_searchText';
        var rendererOptions = {
            hideRouteList: true,
            suppressMarkers: true,
            preserveViewport: false,
            suppressInfoWindows: false
        };

        var _attachEvents = function() {

            $('#'+SEARCH_TEXT_ID).focus(function() {
                $('#'+SUGGESTIONS_DIV_ID).show();
            });

            $('#'+SEARCH_TEXT_ID).blur(function() {
                $('#'+SEARCH_TEXT_ID).val('');
                let suggestionsDivSelector = $('#'+SUGGESTIONS_DIV_ID);
                suggestionsDivSelector.empty();
                suggestionsDivSelector.hide();
            });
        };

        var _renderTemplate = function(template, content) {
            var htmlContent = template(content);
            return htmlContent;
        };

        var _clearTemplate = function(customDirectionModule) {
            if($(customDirectionModule).children('.mod-content')){
                $(customDirectionModule).children('.mod-content').remove();
            }
        };

        var _addModuleContainer = function(customDirectionModule, show) {
            _clearTemplate(customDirectionModule);
            $(customDirectionModule).append(_renderTemplate(template, {show:show}));
        };

        var _displaySuggestions = function(results) {
            let ref = document.getElementById(SUGGESTIONS_DIV_ID);
            if(!ref) {
                console.log("no such div exists");
                return;
            }
            if(results) {
                suggestionsInfo = results;
                $(ref).html(_renderTemplate(suggestionsTemplete, {suggestions:suggestionsInfo}));
            } else {
                suggestionsInfo = ['No Results Found'];
                $(ref).html('<div class="notxt">No Results Found</div>');
            }
        };

        var _checkIfAlreadyExists = function(result) {
            for(var i=0;i<selectedResults.length;i++) {
                if(selectedResults[i].placeId == result.placeId) {
                    return true;
                }
            }
            return false;
        };

        var _removeFirstResult = function() {
            if(selectedResults.length >= 5) {
                let removeCount = selectedResults.length - 4; // 4 because we want to remove 1 element
                selectedResults.splice(0,removeCount);
            }
        };

        var _updateSelectedResults = function(result) {
            if(_checkIfAlreadyExists(result)) {
                return;
            }
            _removeFirstResult();
            selectedResults.push(result);
        };

        var _updateCommuteMarkerInfo = function(status, details) {

            if(status !== 'OK'){
                return;
            }

            let distanceElement = $('#commuteLandmark').find('[data-selector="commuteDistance"]');
            $(distanceElement).html('<i class="icon-jogging-track"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span></i> ' + details.distance);
            let durationElement = $('#commuteLandmark').find('[data-selector="commuteDuration"]');
            $(durationElement).html('<i class="icon-bus"></i> ' + details.duration);
        };

        var _displaySelectedResult = function(data) {
            let index = selectedResults.length - 1 - parseInt(data.index);
            if(index < 0) {
                return;
            }
            context.broadcast('drawMarkers', {
                markers:[{
                    latitude: selectedResults[index].latitude,
                    longitude: selectedResults[index].longitude,
                    name: selectedResults[index].description,
                }],
                markerType: 'commuteLandmark',
                markersFitFlag:true
            });
            if(!data.source) {
                placeService.getPlaceDetail(selectedResults[index].placeId, selectedResults[index].placeType, _placeDetailFromId);
            } else {
                rendererOptions.suppressMarkers = false;
                let destination = {lat: selectedResults[index].latitude, lng: selectedResults[index].longitude};
                mapDirection.getDirection(position, destination, _updateCommuteMarkerInfo, rendererOptions);
                context.broadcast('drawMarkers', {
                    markers:[{
                        latitude: destination.lat,
                        longitude: destination.lng,
                        name: result.description
                    }],
                    markerType: 'commuteLandmark',
                    markersFitFlag:true
                });
            }

        };

        var _parseApiResponse = function(response) {
            let amenities = [];
            for(var i=0;i<response.data.amenities.length;i++) {
                if(response.data.amenities[i].localityAmenityTypes.name === 'airport') {
                    amenities.push({
                        description:response.data.amenities[i].name,
                        distance:parseFloat(mapsConfig.distanceBetweenPoints(position, {lat:response.data.amenities[i].latitude, lng:response.data.amenities[i].longitude})).toFixed(2).toString() + ' km',
                        source:"proptiger",
                        latitude:response.data.amenities[i].latitude,
                        longitude:response.data.amenities[i].longitude,
                        placeId:response.data.amenities[i].id
                    });
                }
            }
            return amenities;
        };

        var _populateSelectedResults = function(cityId) {
            selectedResults = null;
            selectedResults = localStorageService.getItem(citySearchStorageKey);
            if(selectedResults) {
                selectedResults = selectedResults.results; // care for project, locality and listing has to be taken, this is flawed
            } else if(cityId) {
                let apiUrl = sharedConfig.apiHandlers.getAmenities({cityId}).url;
                console.log('GetAmenities----------customDirectionModule');
                apiService.get(apiUrl, false).then((response) => {
                    selectedResults = _parseApiResponse(response);
                    localStorageService.setItem(citySearchStorageKey, {results:selectedResults});
                }, () => {
                    selectedResults = [];
                    console.log("could not log api data");
                });
            }
        };

        var _updateLatestResult = function(status, details) {
            if(status !== 'OK'){
                return;
            }

            var last = selectedResults.length - 1;
            if(last < 0) {
                return;
            }
            selectedResults[last].distance = details.distance;
            selectedResults[last].duration = details.duration;
            _updateCommuteMarkerInfo(status, details);
            localStorageService.setItem(citySearchStorageKey, {results:selectedResults});
            context.broadcast('showUpdatedResults', {data:selectedResults.slice().reverse(), type:'customDirection'});
        };

        var _placeDetailFromId = function(place) {
            let result = {
                destination: place,
                placeId: place.osm_id,
                placeType: place.osm_type,
                description: place.display_name,
                latitude: place.lat,
                longitude: place.lon,
                distance: null,
                duration: null
            };
            _updateSelectedResults(result);
            let destination = {lat: result.destination.lat, lng: result.destination.lon};
            mapDirection.getDirection(position, destination, _updateLatestResult, rendererOptions);
            context.broadcast('drawMarkers', {
                markers:[{
                    latitude: destination.lat,
                    longitude: destination.lng,
                    name: result.description
                }],
                markerType: 'commuteLandmark',
                markersFitFlag:true
            });
        };

        var onkeyup = function(event, element, elementType) {
            if(elementType === 'searchText') {
                let searchText = element.value;
                if(searchText.length === 0) {
                    return;
                }
                placeService.getSuggestions(position, 2, searchText, _displaySuggestions);
            }
        };

        var onclick = function(event, element, elementType) {
            context.broadcast('customDirectionModuleClicked');
            if(!element) {
                return;
            }
            let $element = $(element);
            if(elementType === 'commute') {
                _addModuleContainer(customDirectionModule, true);
                _attachEvents();
                context.broadcast('showUpdatedResults', {data:selectedResults.slice().reverse(), type:'customDirection'});
            } else if($element.data('target') === 'suggestion') {
                suggestionResult = true;
                placeService.getPlaceDetail(elementType, $element.data('osm-type'), _placeDetailFromId);
                $('#'+SEARCH_TEXT_ID).trigger('blur');
            }
        };

        var onmousedown = function(event, element, elementType) {
            if(element && $(element).data('target')) {
                let dataset = $(element).data();
                if(dataset.trackLabel){
                    context.broadcast('trackCommuteSuggestion', dataset.trackLabel);
                }
                event.preventDefault();
                suggestionResult = true;
                placeService.getPlaceDetail(elementType, $(element).data('osm-type'), _placeDetailFromId);
                $('#'+SEARCH_TEXT_ID).trigger('blur');
            }
        };

        var onmessage = function(name, data) {
            if(name === 'initMapReferenceIncustomDirectionModule') {
                map = data;
                mapDirection.init();
                placeService.init(map);
                mapDirection.setMapRef(map);
            } else if(name === 'removeCustomDirectionModule') {
                mapDirection.removeDirection();
                context.broadcast('landmark');
                _addModuleContainer(customDirectionModule, false);
                context.broadcast('removeCommunicatorModule');
            } else if(name === 'loadCustomDirectionModule') {    
                // context.broadcast('clearMarkerOverlay');
                _addModuleContainer(customDirectionModule, true);
                context.broadcast('showUpdatedResults', {data:selectedResults.slice().reverse(), type:'customDirection'});
                context.broadcast('mapSetCenterAndZoom', {latitude: position.lat,longitude: position.lng});
                _attachEvents();
            } else if(name === 'displayDirection') {
                context.broadcast('commuteLandmark');
                if(data.type === 'customDirection') {
                    if(data.show) {
                        _displaySelectedResult(data);
                    } else {
                        mapDirection.removeDirection();
                    }

                }
            } else if(name === 'updateMarkerPosition') {
                position = {lat: parseFloat(data.latitude), lng: parseFloat(data.longitude)};
            }
        };

        return {
            behaviors: [],
            messages: ['initMapReferenceIncustomDirectionModule', 'removeCustomDirectionModule', 'loadCustomDirectionModule', 'displayDirection', 'updateMarkerPosition'],
            onmessage,
            onkeyup,
            onclick,
            onmousedown,
            init() {
                $ = context.getGlobal('jQuery');
                doT = context.getGlobal('doT');
                customDirectionModule = context.getElement();
                let data = $(customDirectionModule).data() || {};
                position = {lat: parseFloat(data.lat), lng: parseFloat(data.lng)};
                cityId = parseInt(data.cityid);
                citySearchStorageKey = "MAKAAN_CITY_"+cityId.toString();
                suggestionsInfo = [];

                commonService.bindOnLoadPromise().then(()=>{
                    _populateSelectedResults(cityId);
                    context.broadcast('moduleLoaded', {'name':'customDirectionModule', 'id':customDirectionModule.id});
                    context.broadcast('mapSubmoduleLoaded', 'customDirectionModule');
                });

            },
            destroy() {
                suggestionResult = null;
            }
        };
    });
});
