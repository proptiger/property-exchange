/**
 * @fileoverview Module for the near by Amenity
 * @author [Aditya Jha]
 */

/*global Box*/
"use strict";
define([
    'doT!modules/mapsModule/views/nearByAmenityModule',
    'services/nearByAmenityService',
    'modules/mapsModule/scripts/services/markerFactory',
    'modules/mapsModule/scripts/services/mapFactory',
    'modules/mapsModule/scripts/services/mapOverlay',
    'modules/mapsModule/scripts/services/mapDirection',
    'modules/mapsModule/submodules/mapMarkersEvents'
], (template, nearByAmenityService, markerFactory, mapFactory, mapOverlay, mapDirection) => {

    Box.Application.addModule('nearByAmenityModule', (context) => {
        var map, nearByAmenityModule, $, doT, amenity,
        nearByAmenityData = [], nearByAmenityDataMarkers = {}, position,title,
        templateData = [], listData, direction,
        selectedElement, apiDataFetched, removeAmenityModule;
        var olay = {
            type:null,
            overlay:null
        };
        var rendererOptions = {
            hideRouteList: true,
            suppressMarkers: true,
            preserveViewport: true,
            suppressInfoWindows: true
        };

        function _initTemplateData() {
            for(var i=0;i<amenity.length;i++) {
                templateData.push({
                    name:amenity[i].name,
                    count:0,
                    displayName:amenity[i].displayName
                });
            }
        }

        function _updateTemplateData(data) {
            for(var i=0;i<templateData.length;i++) {
                if(data.hasOwnProperty(templateData[i].name)) {
                    templateData[i].count = data[templateData[i].name].length;
                }
            }
        }

        function _removeMarker(){
            if (map){
                mapFactory.action.removeMarkerPane(map);
            }
        }

        function _removeOverlay() {
            // olay.overlay.remove();
            _removeMarker();
            olay.overlay.destroy();
            olay.type = null;
            olay.overlay = null;
        }

        function _drawNeighbourhoodMarkers(type, markers) {
            if(olay.type && olay.type === type) { // An overlay of same type already exists
                return;
            } else if(olay.type && olay.type !== type) { // An overlay of different type exists
                _removeOverlay();
            }

            if(!markers || !markers.length) {
                return;
            }
            mapFactory.action.markers.add(map, markers);
            olay.type = type;
            // olay.overlay = new window.Overlay(markers, map);
            // olay.overlay.setMap(map);
            
            var markersOverlay = mapOverlay.getOverlayClass();
            olay.markers = new markersOverlay(markers, map);
            
            olay.overlay = mapFactory.action.getOverlay(map, "markerPane");
            mapFactory.action.bounds(map, markers, false, {latitude:position.lat,longitude:position.lng,title});
        }

        function _drawDirectionCallback(status, details){
            let selectionId = details.id;//String(details.lat).replace('.', '') + '-' + String(details.lng).replace('.', '');
            let $selection = $('#'+selectionId).find('.waiting');
            if(status === 'OK' && !$selection.hasClass('completed')) {
                $selection.addClass('completed');
                $selection.html('<span class="dstnc_wrap"><i class="icon-jogging-track"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span></i> ' + details.distance + '</span><span class="time_car_wrap"><i class="icon-bus"></i> ' + details.duration + '</span>');
            }
        }

        var _drawDirection = function(marker) {
            $('.amenity_marker').removeClass('st-h');
            $('#'+marker.id).addClass('st-h');
            let destination = {lat: marker.latitude, lng: marker.longitude, id: marker.id};
            mapDirection.getDirection(position, destination, _drawDirectionCallback, rendererOptions);
            direction = true;
        };

        var _removeDirection = function() {
            if(direction) {
                mapDirection.removeDirection();
                direction = false;
            }
        };

        var _addNeighbourhoodMarkers = function(type, position, results) {
            let markerList = [], mouseEnterSetTimeoutEvent;
            for(let i=0; i<results.length; i++) {
                let m = new markerFactory.marker(type, results[i]);
                (function(m){
                    m.mouseEnter(function(){
                        //$(m.div).find('.on-hov').css('display','block');
                        if(mouseEnterSetTimeoutEvent){
                            clearTimeout(mouseEnterSetTimeoutEvent);
                            mouseEnterSetTimeoutEvent = null;
                        }
                        mouseEnterSetTimeoutEvent = setTimeout(function(){
                            mouseEnterSetTimeoutEvent = null;
                            $(m.div).find('#'+m.data.id).addClass('st-h');
                            _drawDirection(m.data);
                        },1000);

                    });
                    m.mouseOut(function() {
                        //$(m.div).find('.on-hov').css('display','none');
                        //_removeDirection();
                        if(mouseEnterSetTimeoutEvent){
                            clearTimeout(mouseEnterSetTimeoutEvent);
                            mouseEnterSetTimeoutEvent = null;
                        }
                    });
                })(m);
                markerList.push(m);
            }

            nearByAmenityDataMarkers[type] = markerList;
            _drawNeighbourhoodMarkers(type,nearByAmenityDataMarkers[type]);
        };

        var _addModuleContainer = function(nearByAmenityModule) {
            if(removeAmenityModule) {
                return;
            }
            if($(nearByAmenityModule).children('.mod-content')){
                $(nearByAmenityModule).children('.mod-content').remove();
            }

            let htmlContent =  template({
                amenities:templateData
            });

            $(nearByAmenityModule).append(htmlContent);
        };

        var _getResults = function(data) {
            listData = [];
            for(let i=0;i<data.length && i<5;i++) {
                let datacopy = $.extend(true, {}, data[i]);
                listData.push(datacopy);
                listData[i].geoDistance = parseFloat(listData[i].geoDistance).toFixed(2);
                listData[i].geoDistance = listData[i].geoDistance.toString() + ' km';
            }
            return listData;
        };

        var _removeHoverCard = function(){
            listData = listData || [];
            for(var i=0; i<listData.length; i++){
                (function(i){
                    let id = listData[i].id;
                    $('#'+id).removeClass('st-h');
                })(i);
            }
        }


        var _activateDefaultAmenity = function() {
            let toClick = $(nearByAmenityModule).find('[data-type]');
            if(toClick.length > 0) {
                for(let i=0; i<toClick.length; i++) {
                    if(!toClick[i].getAttribute('disabled')) {
                        $(toClick[i]).trigger('click');
                        break;
                    }
                }
            }
        };

        var _displaySelectedResult = function(data) {
            $('.amenity_marker').removeClass('st-h');
            let index = parseInt(data.index),
            selector = $('#'+listData[index].id);
            selector.addClass('st-h');

            let destination = {lat: listData[index].latitude, lng: listData[index].longitude, id: listData[index].id};
            mapDirection.getDirection(position, destination, _drawDirectionCallback, rendererOptions);
        };

        var _removeSelectedResult = function(data) {
            let index = parseInt(data.index),
            selector = $('#'+listData[index].id);
            selector.removeClass('st-h');
        };

        var _removeSelectedClass = function(nearByAmenityModule) {
            if(selectedElement) {
                $(nearByAmenityModule).find('[data-type="'+ selectedElement +'"]').removeClass('selected');
                selectedElement = undefined;
            }
        };

        var onclick = function (event, element, elementType) {
            context.broadcast('nearByAmenityModuleClicked');
            if(selectedElement && elementType){
                context.broadcast('trackNearbyAmenities', {elementType});
            }
            for(var i=0;i<amenity.length;i++) {
                if(elementType === amenity[i].name && templateData[i].count && elementType !== selectedElement) {
                    _removeDirection();
                    // draw markers
                    _removeSelectedClass(nearByAmenityModule);
                    $(element).addClass('selected');
                    selectedElement = elementType;
                    _addNeighbourhoodMarkers(elementType, position, nearByAmenityData[elementType]);
                    context.broadcast('showUpdatedResults', {name:amenity[i].displayName, type:"nearByAmenity", data:_getResults(nearByAmenityData[elementType])});
                    break;
                }
            }
        };

        var onmessage = function(name, data) {
            if(name === 'initMapReferenceInnearByAmenityModule') {
                map = data;
                context.broadcast('nearByAmenityModuleInitiated');
                mapFactory.action.center(map, position);
                mapDirection.init();
                mapDirection.setMapRef(map);
            } else if(name === 'nearByAmenityDataRecieved') {
                apiDataFetched = true;
                nearByAmenityData = data.data;
                _updateTemplateData(nearByAmenityData);
                _addModuleContainer(nearByAmenityModule);
                _activateDefaultAmenity();
            } else if(name === 'removeAmenityModule') {
                removeAmenityModule = true;
                if(olay.type) {
                    _removeOverlay();
                }
                _removeDirection();
                _removeSelectedClass(nearByAmenityModule);
                $(nearByAmenityModule).hide();
            } else if(name === 'loadNearByAmenityModule') {
                removeAmenityModule = false;
                _addModuleContainer(nearByAmenityModule);
                if(!apiDataFetched) {
                    nearByAmenityService.fetchData(position.lat, position.lng, nearByAmenityService.distance, 0, 999);
                } else {
                    _activateDefaultAmenity();
                }
                $(nearByAmenityModule).show();
            } else if(name === 'displayDirection') {
                if(data.type === 'nearByAmenity') {
                    if(data.show) {
                        _displaySelectedResult(data);
                        direction = true;
                    } else {
                        _removeSelectedResult(data);
                        _removeDirection();
                    }
                }
            }
        };

        return {
            messages: ['nearByAmenityDataRecieved', 'initMapReferenceInnearByAmenityModule', 'removeAmenityModule', 'loadNearByAmenityModule', 'displayDirection'],
            behaviors: [],
            onmessage,
            onclick,
            init() {
                nearByAmenityModule = context.getElement();
                nearByAmenityData = null;
                $ = context.getGlobal('jQuery');
                doT = context.getGlobal('doT');
                amenity = nearByAmenityService.amenity;
                _initTemplateData();
                apiDataFetched = undefined;
                position = {lat: parseFloat($(nearByAmenityModule).data('lat')), lng: parseFloat($(nearByAmenityModule).data('lng'))};
                title = $(nearByAmenityModule).data('title'),
                context.broadcast('moduleLoaded', {'name':'nearByAmenityModule', 'id':nearByAmenityModule.id});
	            context.broadcast('mapSubmoduleLoaded', 'nearByAmenityModule');
                direction = false;
            },
            destroy() {
            }
        };
    });
});
