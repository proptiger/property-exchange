/**
 * @fileoverview Module for the
 * @author [Aditya Jha]
 */

"use strict";

/*global Box*/
define([
    'doT!modules/mapsModule/views/communicatorModule'
], (template) => {
    Box.Application.addModule('communicatorModule', (context) => {

        var doT, $, communicatorModule, selected;

        function _initSelected() {
            selected = {
                element: null,
                type: null,
                index: -1
            };
        }
        var _addModuleContainer = function(communicatorModule, content) {
            if($(communicatorModule).children('.mod-content')){
                $(communicatorModule).children('.mod-content').remove();
            }
            content.selectedIndex = (selected.index > -1 && selected.type == content.type) ? selected.index : null;
            var htmlContent =  template(content);
            $(communicatorModule).append(htmlContent);

            context.broadcast('communicatorModuleRendered', content);
        };

        var onmessage = function(name, data) {
            if(name === 'loadCommunicatorModule') {
                _initSelected();
                _addModuleContainer(communicatorModule, {results:null,type:null});
                $(communicatorModule).show();
            } else if(name === 'showUpdatedResults') {
                if(selected.type !== data.type) {
                    _initSelected();
                    selected.type = data.type;
                }
                _addModuleContainer(communicatorModule, {results:data.data,type:data.type,name:data.name});
                $(communicatorModule).show();

            } else if(name === 'removeCommunicatorModule') {
                context.broadcast('clearMarkerOverlay', 'commuteLandmark');
                _addModuleContainer(communicatorModule, {results:null,type:null});
                $(communicatorModule).hide();
                _initSelected();
            }
        };


        var activeDirectionMap = {
            'customDirection': {

            },
            'nearByAmenity':{

            }
        };

        function mouseenter(element){

            let data = $(element).data() || {};
            let index = parseInt(data.index);

            if(!(data && data.target) || typeof index != 'number') {
                return;
            }

            if(activeDirectionMap[data.target] && activeDirectionMap[data.target][`${index}`]){
                return;
            }

            if(index >= 0) {
                if(data){
                    context.broadcast('trackCommuteSearch', data);
                }
                if(data.target === 'customDirection') {
                    activeDirectionMap[data.target][`${index}`] = true;
                    context.broadcast('displayDirection', {
                        type:'customDirection',
                        index,
                        source:data.source,
                        show: true
                    });
                } else if(data.target === 'nearByAmenity') {
                    activeDirectionMap[data.target][`${index}`] = true;
                    context.broadcast('displayDirection', {
                        type:'nearByAmenity',
                        index:index,
                        show: true
                    });
                }
            }
         }

         function mouseleave(element){

             let data = $(element).data() || {};
             let index = parseInt(data.index);

             if(!(data && data.target) || typeof index != 'number') {
                 return;
             }

             if(!(activeDirectionMap[data.target] && activeDirectionMap[data.target][`${index}`])){
                 return;
             }

             if(data.target === 'customDirection') {
                 activeDirectionMap[data.target] = {};
                 activeDirectionMap[data.target][`${index}`] = false;
                 activeDirectionMap['nearByAmenity'] = {};
             } else if(data.target === 'nearByAmenity') {
                 activeDirectionMap[data.target] = {};
                 activeDirectionMap[data.target][`${index}`] = false;
                 activeDirectionMap['customDirection'] = {};
             }
         }

        /*function mouseleave(){

            let data = $(this).data() || {};
            let index = parseInt(data.index);

            if(!(data && data.target) || typeof index != 'number') {
                return;
            }

            if(!(activeDirectionMap[data.target] && activeDirectionMap[data.target][`${index}`])){
                return;
            }

            if(data.target === 'customDirection') {
                activeDirectionMap[data.target] = {};
                activeDirectionMap[data.target][`${index}`] = false;
                activeDirectionMap['nearByAmenity'] = {};
                context.broadcast('displayDirection', {
                    type:'customDirection',
                    index,
                    source:data.source,
                    show: false
                });
            } else if(data.target === 'nearByAmenity') {
                activeDirectionMap[data.target] = {};
                activeDirectionMap[data.target][`${index}`] = false;
                activeDirectionMap['customDirection'] = {};
                context.broadcast('displayDirection', {
                    type:'nearByAmenity',
                    index:index,
                    show: false
                });
            }
        }*/


        return {
            messages: ['loadCommunicatorModule', 'showUpdatedResults', 'removeCommunicatorModule'],
            onmessage,
            init() {
                doT = context.getGlobal('doT');
                $ = context.getGlobal('jQuery');
                communicatorModule = context.getElement();
                _initSelected();
                context.broadcast('moduleLoaded', {'name':'communicatorModule', 'id':communicatorModule.id});
                context.broadcast('mapSubmoduleLoaded', 'communicatorModule');

                let mouseEnterSetTimeoutEvent;
                $(communicatorModule).off('mouseenter').on('mouseenter', 'ul>li',
                    function(){
                        var _this = this,
                            id = $(_this).data('value');

                        $('#'+id).addClass('st-h');
                        if(mouseEnterSetTimeoutEvent){
                            clearTimeout(mouseEnterSetTimeoutEvent);
                            mouseEnterSetTimeoutEvent = null;
                        }
                        mouseEnterSetTimeoutEvent = setTimeout(function(){
                            mouseenter(_this);
                            mouseEnterSetTimeoutEvent = null;
                        },1000);
                    }
                );

                $(communicatorModule).off('mouseleave').on('mouseleave', 'ul>li', function(){
                        var _this = this,
                            id = $(_this).data('value');
                        if(mouseEnterSetTimeoutEvent){
                            $('#'+id).removeClass('st-h');
                            clearTimeout(mouseEnterSetTimeoutEvent);
                            mouseEnterSetTimeoutEvent = null;
                        }
                        mouseleave(_this);
                });


            },
            destroy() {
                $(communicatorModule).off('mouseenter');
                $(communicatorModule).off('mouseleave');
                _initSelected();
            }
        };
    });
});
