/**
 * Name: mapModule
 * Description: Maps Module Logic
 * @author: [Shakib Mohd]
 * Date: Oct 05, 2015
 **/
  "use strict";

/** globals: [google, window, document] */
define([
    'doT!modules/mapsModule/views/index',
    'modules/mapsModule/scripts/services/mapsConfig',
    'modules/mapsModule/scripts/services/mapFactory',
    'modules/mapsModule/scripts/services/markerFactory',
    'services/shortlist',
    'modules/mapsModule/scripts/services/mapOverlay',
    'modules/mapsModule/submodules/mapMarkersEvents',
    'modules/mapsModule/submodules/zoomMapModule',
    'modules/mapsModule/scripts/behaviors/mapsBehavior',
    'services/utils',
    'modules/mapsModule/scripts/behaviors/mapTracking'
], (template, mapsConfig, mapFactory, markerFactory, shortlistService,mapOverlay) => {
    Box.Application.addModule('mapsModule', (context) => {

        var mapsModule, map, _config,
            boundaryPolygons, doT, mapsModuleRef, landmark, utils, infinite;

        var behaviors = [
            'mapsBehavior',
            'mapTracking'
        ];

        var messages = [
            'clearMarkerOverlay',
            'mapZoomin',
            'mapZoomout',
            'mapSetCenter',
            'drawBoundaryPolygons',
            'destroyBoundaryPolygons',
            'drawMarkers',
            'mapSubmoduleLoaded',
            'updateMarkerShortlistStatus',
            'mapFitBounded',
            'setMapToViewPort',
            'mapSetCenterAndZoom'
        ];

        // waitForMap
        var _waitForMap = function(func) {
            return function waitingForMap() {
                if(!map) {
                    $('.body_map').bind('map-loaded', () => {
                       func.apply(this, arguments);
                    });
                } else {
                    func.apply(this, arguments);
                }
            };
        };

        var _addModuleContainer = function(mapsModule) {
            if ($(mapsModule).children('.mod-content')) {
                $(mapsModule).children('.mod-content').remove();
            }
            var htmlContent = template({
                mapModuleId: mapsConfig.mapModuleId
            });
            $(mapsModule).append(htmlContent);
            Box.Application.startAll($('#' + mapsConfig.mapModuleId + '-mandatory-submodules'));
        };


        // Zoom Level
        var _setZoomLevel = function(config) {
            if (!map) {
                return;
            }
            mapFactory.action.zoomLevel(map, config.minZoom, config.maxZoom);
        };
        _setZoomLevel = _waitForMap(_setZoomLevel);

        var _setMapCener = function(data) {
            if (!map) {
                return;
            }
            mapFactory.action.center(map, {
                lat: data.latitude,
                lng: data.longitude
            });
        };
        _setMapCener = _waitForMap(_setMapCener);

        var _mapSetCenterAndZoom = function(data) {
            if (!map) {
                return;
            }
            mapFactory.action.setCenterAndZoom(map, {
                lat: data.latitude,
                lng: data.longitude
            },data.zoom||mapsConfig.state.zoom);
            let marker = {latitude: data.latitude,longitude: data.longitude,title:data.title||_config.title};
             mapFactory.action.addMarker(map, marker,'',true);
        };
        _mapSetCenterAndZoom = _waitForMap(_mapSetCenterAndZoom);

        var _mapSubmoduleLoaded = function(data) {
            if (!map) {
                return;
            }
            context.broadcast('initMapReferenceIn' + data, map); // broadcast for map's submodules

            switch (data) {
                case 'mapSvgLegendsModule':
                    let pageData = utils.getPageData() || {};
                    if(!(pageData && pageData.cityName)) {
                        if(pageData.cityId) {
                            let city = utils.isMasterPlanSupported({id:pageData.cityId});
                            if(city) {
                                pageData.cityName = city.name;
                            } else {
                                break;
                            }
                        } else {
                            break;
                        }
                    }
                    context.broadcast('showSvgLegends', true);
                    context.broadcast('addSvgOverlay', {
                        'svgName': 'landusage',
                        'city': pageData.cityName
                    });
                    context.broadcast('addSvgOverlay', {
                        'svgName': 'roads',
                        'city': pageData.cityName
                    });
                    context.broadcast('addSvgOverlay', {
                        'svgName': 'train',
                        'city': pageData.cityName
                    });
                    if(pageData.cityName.toLowerCase() !== 'bangalore') {
                        context.broadcast('addSvgOverlay', {
                            'svgName': 'drains_electric',
                            'city': pageData.cityName
                        });
                    }
                    break;
            }
            return;
        };
        _mapSubmoduleLoaded = _waitForMap(_mapSubmoduleLoaded);

        var _updateAllMarkerShortlistStatus = function(){
                var listingMarkers = mapsModule.querySelectorAll('.listing_marker'),
                    listingMarkersLength = listingMarkers.length;

                for(let i=0; i<listingMarkersLength; i++){
                    let marker = listingMarkers[i],
                        listingId = marker && marker.id ? parseInt(marker.id.replace('listing_','')) :  0,
                        isShortlisted = shortlistService.checkShortlist({
                            type:'listing',
                            id: listingId,
                        });

                        _updateMarkerShortlistStatus({
                            selector: marker.id,
                            isShortlisted
                        });
                }
        };
        _updateAllMarkerShortlistStatus = _waitForMap(_updateAllMarkerShortlistStatus);

        var _updateMarkerShortlistStatus = function(data) {
            let selector = $('#' + data.selector);
            if (!selector) { return; }

            if (data.isShortlisted) {
                selector.addClass('st-f');
            } else {
                selector.removeClass('st-f');
            }

        };
        _updateMarkerShortlistStatus = _waitForMap(_updateMarkerShortlistStatus);

        var _setMapToViewPort = function(viewport){
            viewport = viewport ? viewport.split(',') : null;
            if(viewport && viewport.length == 3){
                mapFactory.action.setCenterAndZoom(map, {
                    lat: viewport[0],
                    lng: viewport[1]
                }, viewport[2]);
            }
        };
        _setMapToViewPort = _waitForMap(_setMapToViewPort);

        var _destroyBoundaryPolygons = function() {
            if (!map) {
                return;
            }
            for (var i = 0; boundaryPolygons && (i < boundaryPolygons.length); i++) {
                boundaryPolygons[i].destroy();
            }
            boundaryPolygons = [];
        };
        _destroyBoundaryPolygons = _waitForMap(_destroyBoundaryPolygons);

        var _drawBoundaryPolygons = function(polygonsList) {
            if (!map) {
                return;
            }
            _destroyBoundaryPolygons();
            boundaryPolygons = polygonsList;
            mapFactory.action.polygons.add(map, boundaryPolygons);
        };
        _drawBoundaryPolygons = _waitForMap(_drawBoundaryPolygons);



        /**
         * This function listens to messages from other modules and takes action accordingly.
         * @param {message} name,data The name of the custom event and additional data, if any.
         * @returns {void}
         */
        var onmessage = function(name, data) {
            let pageType = utils.getTrackPageCategory();
            let pageCategory = 'Buyer_' + pageType;
            if (name === 'mapZoomin') {
                let currZoom = map.getZoom();
                if (currZoom < mapsConfig.state.maxZoom) {
                    mapFactory.action.zoom(map, currZoom + 1);
                    if ((currZoom + 1) === mapsConfig.state.maxZoom) {
                        // disable the zoom in functionality
                        $('.map-zoom-control [data-type="zoomin"]').addClass('disable');
                    }
                    // enable the zoom in functionality
                    $('.map-zoom-control [data-type="zoomout"]').removeClass('disable');
                } else {
                    // ideally the zoom in button should be disabled
                    $('.map-zoom-control [data-type="zoomin"]').addClass('disable');
                }
            } else if (name === 'mapZoomout') {
                let currZoom = map.getZoom();
                if (currZoom > mapsConfig.state.minZoom) {
                    mapFactory.action.zoom(map, currZoom - 1);
                    if ((currZoom - 1) === mapsConfig.state.minZoom) {
                        // disable the zoom out functionality
                        $('.map-zoom-control [data-type="zoomout"]').addClass('disable');
                    }
                    //enable the zoom in functionality
                    $('.map-zoom-control [data-type="zoomin"]').removeClass('disable');
                } else {
                    // ideally zoomout button should be disabled
                    $('.map-zoom-control [data-type="zoomout"]').addClass('disable');
                }
            } else if (name === 'mapSetCenter') {
                _setMapCener(data);
            } else if (name === 'mapSetCenterAndZoom') {
                clearMarkerOverlay(markerOverlay.type );
                _mapSetCenterAndZoom(data);
            } else if (name === 'mapSubmoduleLoaded') {
                _mapSubmoduleLoaded(data);
            } else if (name === 'setZoomLevels') {
                _setZoomLevel(data);
            } else if (name === 'drawBoundaryPolygons') {
                _drawBoundaryPolygons(data); // data referes to polygonsList
            } else if (name === 'destroyBoundaryPolygons') {
                _destroyBoundaryPolygons();
            } else if (name === 'drawMarkers') {
                infinite = data.infinite;
                if (data.markers) {
                    _drawMarkers(data.markers, data.markerType, data.markersFitFlag);
                }
            } else if(name === 'setMapToViewPort'){
                _setMapToViewPort(data);
            } else if (name === 'updateMarkerShortlistStatus') {
                if(data && data.all && data.markerType === 'listing'){
                    _updateAllMarkerShortlistStatus();
                }else{
                    _updateMarkerShortlistStatus(data);
                }
            } else if(name == 'mapFitBounded') {
                let zoomToSet = Math.min(mapsConfig.state.maxFitboundZoom, map.getZoom());
                if(zoomToSet < map.getZoom()) {
                    mapFactory.action.zoom(map, zoomToSet);
                }
            } else if(name === 'clearMarkerOverlay') {
                clearMarkerOverlay(data);
            }
        };

        var beforeInitialize = function() {
            //reset factory flags for drawing mode on loading of this directive
            mapsConfig.state.polygonFilter.polygonExist = false;
            mapsConfig.state.polygonFilter.visible = false;
            mapsConfig.state.polygonFilter.currentDrawingStatus = false;
        };

        var markers, curr, markerOverlay = {
            overlay:null,
            type:null
        };

        function clearMarkerOverlay(markerType) {
            if (markerOverlay.overlay && markerOverlay.type == markerType) {
                if(!infinite) {
                    //markerOverlay.overlay.remove();
                    mapFactory.action.removeMarkerPane(map);
                }
                markerOverlay = {
                    overlay:null,
                    type:null
                };
            }
        }

        var _addMarkersToMap = function(currMarkers, markersFitFlag, markerType) {
            if (!map) {
                return;
            }
            markers = currMarkers;
            curr = currMarkers;

            // clearMarkerOverlay(markerType);

            // when there is no marker to draw
            if (!(curr && curr.length)) {
                return;
            }

            mapFactory.action.markers.add(map, curr);
            var Overlay = mapOverlay.getOverlayClass();
            markerOverlay.type = markerType;
            markerOverlay.markers = new Overlay(markers, map);
            markerOverlay.overlay = mapFactory.action.getOverlay(map, "markerPane");

            if(markerType !== 'landmark' && !$.isEmptyObject(_config) && _config.pageType == 'landmark') {
                mapFactory.action.bounds(map, curr, false, {
                    latitude: parseFloat(_config.latitude),
                    longitude: parseFloat(_config.longitude)
                });
            } else if (markersFitFlag) {
                mapFactory.action.bounds(map, curr, false, {latitude:_config.latitude,longitude:_config.longitude,title:_config.title||''});
            }
        };
        _addMarkersToMap = _waitForMap(_addMarkersToMap);

        var _drawMarkers = function(data, markerType, markersFitFlag) {
            let markers = [],
                marker;
            for (let i = 0; i < data.length; i++) {
                marker = new markerFactory.marker(markerType, data[i]);
                markers.push(marker);
            }

            _addMarkersToMap(markers, markersFitFlag, markerType);
        };
        _drawMarkers = _waitForMap(_drawMarkers);

        var _attachMouseWheelEvents = function() {
            mapsModuleRef.attr("tabindex", "0");
            mapsModuleRef.off('focus').on('focus', function() {
                mapFactory.action.setOptions(map, {
                    scrollwheel: true
                });
            });
            mapsModuleRef.off('blur').on('blur', function() {
                mapFactory.action.setOptions(map, {
                    scrollwheel: false
                });
            });
        };

        // function _getPolygonList(polygons) {
        //     let polygonList = [];
        //     polygonList.push(new mapPolygonService.Polygon({
        //         encodedPolygon: polygons[0]
        //     }, {
        //         fillColor: '#2691ec',
        //         strokeColor: '#e81c28',
        //         strokeWeight: 2,
        //         fillOpacity: 0
        //     }));

        //     return polygonList;
        // }

        var proceed = function() {
            context.broadcast('moduleLoaded', {
                'name': 'mapsModule',
                'id': mapsModule.id
            });
            let elementId = mapsConfig.mapModuleId;
            map = mapFactory.initialize(mapsConfig, elementId);

            _attachMouseWheelEvents(); // attach event to solve scrolling zoom in/out issue

            mapFactory.bind.zoom(map, function(zoom) {
                mapsConfig.state.zoom = zoom;
                context.broadcast('zoomUpdated', zoom);
            });

            mapFactory.bind.click(map, function(){
                context.broadcast('mapClicked');
            });

            setTimeout(function() {
                // Trigger MAP LOAD*/
                $('.body_map').trigger('map-loaded');
            }, 0);

            // if(_config.boundaryEncodedPolygon){
            //     let PolygonList = _getPolygonList([unescape(_config.boundaryEncodedPolygon)]);
            //     _drawBoundaryPolygons(PolygonList);
            // }
        };

        var _handleLandmarkCase = function(_config) {
            landmark.name = _config.name;
            landmark.latitude = parseFloat(_config.latitude);
            landmark.longitude = parseFloat(_config.longitude);

            context.broadcast('drawMarkers', {
                markers: [{
                    latitude: landmark.latitude,
                    longitude: landmark.longitude,
                    name: landmark.name
                }],
                markerType: 'landmark'
            });
        };

        return {
            behaviors,
            messages,
            onmessage,
            init() {
                // capture the reference when the module is started
                mapsModule = context.getElement();
                mapsModuleRef = $('#' + mapsModule.id);
                _config = context.getConfig() || {};
                doT = context.getGlobal('doT');
                utils = context.getService('Utils');
                beforeInitialize();
                landmark = {};
                _addModuleContainer(mapsModule);

                mapFactory.includeJS(proceed);

                mapsModuleRef.find('.map-loading-div').addClass('hide'); // to hide loading div

                if(_config.latitude && _config.longitude && _config.zoom){
                    setTimeout(function(){
                        context.broadcast('mapSetCenterAndZoom', {
                            latitude: _config.latitude,
                            longitude: _config.longitude,
                            title:_config.title||'',
                            zoom :  _config.zoom
                        });
                    }, 500);
                } else if(_config.latitude && _config.longitude){
                    context.broadcast('mapSetCenter', {
                        latitude: _config.latitude,
                        longitude: _config.longitude
                    });
                }

                if(_config.pageType &&  _config.pageType == 'landmark') {
                    _handleLandmarkCase(_config);
                } else if (_config.latitude && _config.longitude) {
                    if(_config.pageType) {
                        switch(_config.pageType) {
                            case 'property':
                            case 'locality':
                            case 'project':
                            case 'SERP':
                                context.broadcast('drawMarkers', {
                                    markers: [{
                                        latitude: _config.latitude,
                                        longitude: _config.longitude,
                                        title: _config.title
                                    }],
                                    markerType: _config.pageType
                                });
                                break;
                        }
                    }
                }
            },
            destroy() {
                if (window.google) {
                    if (map) {
                        map.off();
                        map.remove();
                    }
                }

                let boundaryPolygonsLength = boundaryPolygons && boundaryPolygons.length;
                if(boundaryPolygonsLength && map){
                    for(let i = 0; i<boundaryPolygonsLength; i++){
                        boundaryPolygons[i].destroy();
                    }
                }

                map = null;
                mapsModule = null;
                mapsModuleRef.off('focus');
                mapsModuleRef.off('blur');
                $('.body_map').unbind('map-loaded');
                boundaryPolygons = [];
                utils = null;
                markerOverlay = {
                    overlay: null,
                    type: null
                };
            }
        };

    }); // end of Module
}); //end of define
