/**
   * Name: maps Behaviour
   * Description: Maps Module Behavior
   * @author: [Shakib Mohd]
   * Date: Oct 05, 2015
**/

"use strict";

define([
	'modules/mapsModule/scripts/services/mapsConfig',
	'services/urlService',
], (mapsConfig, urlService) => {
	Box.Application.addBehavior('mapsBehavior', (context) => {

		const DRAW_SHAPE_URL_KEY = 'path';

		var onmessage = function(name, data) {
			if(name === 'setUrlPolygonPath') {
				let encodedPath = data;
				if(encodedPath){
	                encodedPath = window.escape(encodedPath);
	            }
	            let polygonExistFlag = encodedPath ? false : true,
				value = urlService.getUrlParam(DRAW_SHAPE_URL_KEY);
				if(!polygonExistFlag) {
					if(!value || (value && window.escape(value) != encodedPath)) {
						urlService.changeUrlParam(DRAW_SHAPE_URL_KEY, encodedPath);
						context.broadcast('resetPagination');
					}
				} else if(value) {
					urlService.removeUrlParam(DRAW_SHAPE_URL_KEY);
					context.broadcast('resetPagination');
					context.broadcast('hideMarkers');
				}
				context.broadcast('showMapFilter', polygonExistFlag);
	            mapsConfig.state.polygonFilter.polygonExist = polygonExistFlag;
				context.broadcast('polygonExistStatusChange', !polygonExistFlag);
			} else if(name == 'drawingModeSwitched') {
				if(data) {
					context.broadcast('hideHeatMapModule');
					context.broadcast('hideSearchInViewPortModule');
					context.broadcast('hideMasterPlan');
					context.broadcast('hideMarkers');
					$('.bot-control').hide();
				} else {
					context.broadcast('showHeatMapModule');
	                context.broadcast('showSearchInViewPortModule');
	                context.broadcast('showMasterPlan');
	                context.broadcast('showMarkers');
					$('.bot-control').show();
				}
			}else if(name == 'zoomUpdated'){
				context.broadcast('updateSvgStroke', data);
			}else if(name == 'listingShortlistStatusUpdated'){
				context.broadcast('updateMarkerShortlistStatus', {
					selector : data.selector,
					isShortlisted: data.isShortlisted
				});
			}
		};

    	return {
			messages: ['setUrlPolygonPath', 'drawingModeSwitched', 'zoomUpdated', 'listingShortlistStatusUpdated'],
			onmessage,
			init() {
			},
			destroy(){
			}
    	};
	});
});
