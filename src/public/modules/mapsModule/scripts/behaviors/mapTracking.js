define([
    'common/trackingConfigService',
    'services/trackingService'
], function(t, trackingService) {
    'use strict';
    Box.Application.addBehavior('mapTracking', function(context) {

        var messages = [
            'mapZoomin',
            'mapZoomout',
            'trackToggleHeatMap',
            'trackHeatMapAmenities',
            'trackDrawRegionToggle',
            'trackSvgOption',
            'trackSvgToggle',
            'trackMapsSearchHere',
            'trackMapMarkerHover',
            'trackMapMarkerClick',
            'trackNearbyAmenities'
        ];

        var onmessage = function(eventName, data) {
            let category = t.MAPS_CATEGORY,
                amenityLabels = t.MAP_LABELS,
                sourceModule = t.MAP_SOURCE_MODULE,
                event,
                label,
                name,
                projectStatus,
                budget,
                bhk,
                listingId,
                listingCategory,
                localityId,
                cityId,
                projectId,
                builderId,
                unitType,
                sellerId,
                nonInteraction,
                temp;

            switch (eventName) {
                case 'mapZoomin':
                    event = t.INTERACTION_EVENT;
                    label = t.ZOOM_IN_LABEL;
                    break;

                case 'mapZoomout':
                    event = t.INTERACTION_EVENT;
                    label = t.ZOOM_OUT_LABEL;
                    break;

                case 'trackToggleHeatMap':
                    event = t.INFO_TOGGLE;
                    label = data && (data.open ? t.EXPLORE_LOCALITY_OPEN_LABEL : t.EXPLORE_LOCALITY_CLOSE_LABEL); 
                     break;
                case 'trackHeatMapAmenities':
                    event = t.INFO_TOGGLE;
                    label = data && amenityLabels[data];
                    break;

                case 'trackDrawRegionToggle':
                    event = t.INFO_TOGGLE;
                    label = data && (data.open ? t.DRAW_REGION_OPEN_LABEL : t.DRAW_REGION_CLOSE_LABEL); 
                    break;

                case 'trackSvgOption':
                    event = t.INFO_TOGGLE;
                    label = data && amenityLabels[data];
                    break;  

                case 'trackSvgToggle':
                    event = t.INFO_TOGGLE;
                    label = data && (data.open ? t.MASTERPLAN_OPEN_LABEL : t.MASTERPLAN_CLOSE_LABEL); 
                    break;  

                case 'trackMapsSearchHere':
                    event = t.INFO_TOGGLE;
                    label = t.SEARCH_AREA_LABEL;
                    break; 

                case 'trackMapMarkerHover':
                    event = t.INTERACTION_EVENT;
                    label = t.HOVER_LABEL;
                    temp = data && data.data;
                    name = data && data.type;
                    if(name === 'listing' && temp){
                        listingId = temp.listingId;
                        projectId = temp.projectId;
                        listingCategory = temp.apiListingCategory;
                        projectStatus = temp.propertyStatus;
                        budget = temp.price;
                        unitType = temp.propertyType;
                        bhk = temp.bedrooms;
                        sellerId = temp.postedBy && temp.postedBy.id;
                        localityId = temp.localityId;
                        cityId = temp.cityId;
                    } else {
                        return;
                    }
                    break;

                case 'trackMapMarkerClick':
                    event = t.INTERACTION_EVENT;
                    label = t.CLICK_LABEL;
                    temp = data && data.data;
                    name = data && data.type;
                    if(name === 'listing' && temp){
                        listingId = temp.listingId;
                        projectId = temp.projectId;
                        listingCategory = temp.apiListingCategory;
                        projectStatus = temp.propertyStatus;
                        budget = temp.price;
                        unitType = temp.propertyType;
                        bhk = temp.bedrooms;
                        sellerId = temp.postedBy && temp.postedBy.id;
                        localityId = temp.localityId;
                        cityId = temp.cityId;
                    } else {
                        return;
                    }
                    break;

                case 'trackNearbyAmenities':
                    event = t.INFO_TOGGLE;
                    label = data && amenityLabels[data.elementType];
                    break;
            }

            let properties = {};
            properties[t.CATEGORY_KEY] = category;
            properties[t.LABEL_KEY] = label;
            properties[t.SOURCE_MODULE_KEY] = sourceModule;
            properties[t.NAME_KEY] = name;
            properties[t.PROJECT_STATUS_KEY] = t.makeValuesUniform(projectStatus,t.PROJECT_STATUS_NAME) ;
            properties[t.BUDGET_KEY] = budget;
            properties[t.BHK_KEY] = t.makeValuesUniform(bhk,t.BHK_MAP) ;
            properties[t.LOCALITY_ID_KEY] = localityId;
            properties[t.CITY_ID_KEY] = cityId;
            properties[t.PROJECT_ID_KEY] = projectId;
            properties[t.BUILDER_ID_KEY] = builderId;
            properties[t.UNIT_TYPE_KEY] = t.makeValuesUniform(unitType,t.UNIT_TYPE_MAP) ;
            properties[t.LISTING_CATEGORY_KEY] = t.makeValuesUniform(listingCategory,t.LISTING_CATEGORY_MAP) ;
            properties[t.SELLER_ID_KEY] = sellerId;

            trackingService.trackEvent(event, properties);
        };

        return {
            messages: messages,
            onmessage: onmessage,
            init: function() {},
            destroy: function() {}
        };
    });
});