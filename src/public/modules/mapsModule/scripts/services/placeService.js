/**
 * @fileoverview
 * @author [Aditya Jha]
 */

/*global Box*/

'use strict';
define(['services/apiService',"common/sharedConfig"], (apiService,sharedConfig) => {

    let SERVICE_NAME = 'placeService';
    Box.Application.addService(SERVICE_NAME, () => {

        var factory = {};
        var placeService, autocompleteService;

        factory.init = function(map) {
            // placeService = new google.maps.places.PlacesService(map);
            // autocompleteService = new google.maps.places.AutocompleteService();
            
        };

        factory.getSuggestions = function(location, radius, searchText, callback) {
            // let circle = new google.maps.Circle({
            //     center: location,
            //     radius: radius
            // });
            // autocompleteService.getPlacePredictions({
            //     input: searchText,
            //     componentRestrictions: {country:'in'},
            //     bounds: circle.getBounds()
            // }, (results, status) => {
            //     if(status == 'OK') {
            //         callback(results);
            //     } else {
            //         callback();
            //     }
            // });
            apiService.get(sharedConfig.apiHandlers.getPlaceSuggestions(searchText).url).then(results => {
                callback(results);    
            }, () => {
                callback();
            });
        };

        factory.getPlaceDetail = function(placeId, placeType, callback) {
            // placeService.getDetails({
            //     placeId:placeId
            // }, (place, status) => {
            //     if(status === google.maps.places.PlacesServiceStatus.OK) {
            //         if(callback) {
            //             callback(place);
            //         }
            //     } else {
            //         console.log("invalid placeid");
            //     }
            // });
            apiService.get(sharedConfig.apiHandlers.getPlaceDetails({placeId, placeType}).url).then(place => {
                if(callback) {
                    callback(place);    
                }
            }, () => {
                console.log("invalid placeid");
            });
            
        };

        factory.destroy = function() {
            placeService = null;
            autocompleteService = null;
        };

        return factory;
    });
    return Box.Application.getService(SERVICE_NAME);
});
