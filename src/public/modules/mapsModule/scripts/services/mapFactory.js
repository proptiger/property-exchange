/**
   * Name: mapFactory
   * Description: Maps API
   * @author: [Shakib Mohd]
   * Date: Oct 05, 2015
**/

/** globals: google */
"use strict";

define([
	'modules/mapsModule/scripts/services/mapFilter',
	'modules/mapsModule/scripts/services/mapOverlay'
], (mapFilter, mapOverlay) => {

	let SERVICE_NAME = 'mapFactory';

	Box.Application.addService(SERVICE_NAME, (application) => {

		var existingPanes = ["mapPane", "tilePane", "overlayPane", "shadowPane", "markerPane", "tooltipPane", "popupPane"];
		var factory = {};
		var markerReferenceList = [];

		function _addCss(cssLink) {
            var head = document.getElementsByTagName('head')[0];
            var link = document.createElement('link');
            link.rel = 'stylesheet';
            link.type = 'text/css';
            link.href = cssLink;
            head.appendChild(link);
		}
		
		function _addMarker(map, data, htmlDom){
			var markerReference = '';
			if (htmlDom && htmlDom.html) {
				var myIcon = L.divIcon({
				    className: htmlDom.className,
				    html: htmlDom.html
				});
				markerReference = L.marker([data.latitude, data.longitude], {
				    icon: myIcon
				}).addTo(map);
				_addEvents(markerReference);
			} else {
					var html = `<div class="landmark_marker"><i class="icon-mapmarker"></i>`
					html = data.title ? `${html} <div class="on-hov"><span class="name">${data.title}</span> </div></div>` : `${html}</div>`;
				var myIcon = L.divIcon({
					className: "",
					html
				})
				markerReference = L.marker([data.latitude, data.longitude], {
				    icon: myIcon
				}).addTo(map);
			}
			markerReferenceList.push(markerReference);
		}

		function _removeMarkerReference(map){
			if (markerReferenceList.length){
				for (let i = 0; i < markerReferenceList.length; i++){
					map && map.removeLayer(markerReferenceList[i]);
				}
			}
			markerReferenceList = [];
		}

		function _setBoundZoom(map){
			if (markerReferenceList.length) {
				var group = new L.featureGroup(markerReferenceList);
				map.fitBounds(group.getBounds(), {padding: [50, 50]});
			}
		}

		function _addEvents(markerReference) {
			markerReference._icon.addEventListener('mouseover', _addDirection);
			markerReference._icon.addEventListener('click', _addDirection);
		}

		function _addDirection() {
			let dataSet = $(this).children(0).data();
			if (dataSet && dataSet.target) {
				application.broadcast('displayDirection', {
				    type: dataSet.target,
				    index: dataSet.index || 0,
				    source: dataSet.source || '',
				    show: true
				});
			}
		}

	    // ===========================================
	    // Initialize the map and return a reference
	    // ===========================================
		factory.initialize = function(mapsConfig, elementId){
	        let state = mapsConfig.state, map;
	        map = L.map(elementId, {
	        	zoomControl: state.zoomControl || false,
	        	minZoom: state.minZoom,
	        	maxZoom: state.maxZoom,
	        	scrollWheelZoom: false
			}).setView([state.center.lat, state.center.lng], state.zoom);
			L.tileLayer(window.OSM_TILE_URL, {
				maxZoom: state.maxZoom,
	        	attribution: '&copy; <a target="_blank" href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
			}).addTo(map);
			map.attributionControl.setPosition("bottomleft");
			return map;
		};

	     // =========================================
	    // Include Leaflet
	    // =========================================
	    factory.includeJS = function(callback) {
	        callback = typeof callback == 'function' ? callback : function(){};
	        if(!window.L) {
	            require(["https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.4.0/leaflet.js"], callback);
	            _addCss("https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.4.0/leaflet.css");
	            _addCss("https://cdnjs.cloudflare.com/ajax/libs/leaflet-routing-machine/3.2.12/leaflet-routing-machine.css");
	        } else {
	       		callback();
	        }
	    };

	    factory.action = {
	    	zoom(map, zoom) { 
	            zoom = parseInt(zoom) ? parseInt(zoom) : 15;
	            map.setZoom(zoom);
	        },
	        setOptions(map, options) {
	        	if(options){
	        		map.setOptions(options);
	        	}
	        },
			setCenterAndZoom(map, center, zoom) {
				var lat, lng, c;
	            if(center.lat && center.lng && zoom){
		            lat = parseFloat(center.lat)? parseFloat(center.lat):0;
		            lng = parseFloat(center.lng)? parseFloat(center.lng):0;
					zoom = parseInt(zoom) ? parseInt(zoom) : 15;
					map.setView([lat, lng], zoom); 
	            }
			},
	        zoomLevel(map, zoomMin, zoomMax) {
	            zoomMin = parseInt(zoomMin)? parseInt(zoomMin):10;
	            zoomMax = parseInt(zoomMax)? parseInt(zoomMax):22;
				map.setMinZoom(zoomMin);
				map.setMaxZoom(zoomMax);
	        },
	        center(map, center) {
	            var lat, lng, c;
	            if(center.lat && center.lng){
		            lat = parseFloat(center.lat)? parseFloat(center.lat):0;
					lng = parseFloat(center.lng)? parseFloat(center.lng):0;
					map.setView([lat, lng]);
	            }
	        },
			getBounds(map) {
				return map.getBounds();
			},
        	markers: {
	            add(map, markers) {
	                if(!markers || !markers.length) {
	                    return;
	                }

	                for (var i=0; i<markers.length; i++) {
	                    markers[i].init(map);
	                }
	            }
	        },
			polygons: {
	            add(map, polygons) {
	                if(!polygons || !polygons.length) {
	                    return;
	                }
	                for(var i = 0; i < polygons.length; i++) {
	                    polygons[i].init(map);
	                }
	            }
	        },
	        bounds(map, markerList, otherBounds, projectLatLng) {
	            var pos, currentMarker,
	                bounds = map.getBounds();
	            if(otherBounds) {
	                bounds.extend(otherBounds.getCenter());
	                bounds.extend(otherBounds.getNorthEast());
	                bounds.extend(otherBounds.getSouthWest());
				}
				_removeMarkerReference(map);
	            for (var i = 0; i < markerList.length; i++) {
					currentMarker = markerList[i];
					if (currentMarker.data.latitude && currentMarker.data.longitude) {
						var html = '',
							data = {
							    latitude: currentMarker.data.latitude,
							    longitude: currentMarker.data.longitude
							};
						if (currentMarker && currentMarker.div && currentMarker.div.innerHTML){
							html = currentMarker.div.innerHTML;
						}
						_addMarker(map, data, {className: 'map-fit-width', html});
	                }
	            }

	            if(projectLatLng && projectLatLng.latitude && projectLatLng.longitude){
					_addMarker(map, projectLatLng, {});
				}
				_setBoundZoom(map);
				bounds = map.getBounds();
	            if(pos || otherBounds){
	                map.fitBounds(bounds);
					application.broadcast('mapFitBounded');
	            }
			},
			addMarker(map, data, htmlDom,stopSetBounds){
				_addMarker(map, data, htmlDom);
				if(!stopSetBounds){
					_setBoundZoom(map);
				}
			},
			removeMarkerPane(map) {
				_removeMarkerReference(map);
			},
			divBounds(map) {
				if(!map) {
					console.log("map undefined");
					return;
				}
				return map.getBounds();
			},
			fitBounds(map, latLngs) {
				map.fitBounds(latLngs);
				application.broadcast('mapFitBounded');
 			},
 			getOverlay(map, overlayType) {
 				if(existingPanes.indexOf(overlayType) > -1) {
 					return map.getPane(overlayType);
 				}
 				existingPanes.push(overlayType);
 				return map.createPane(overlayType);
 			}
	    };

	    // =========================================
	    // Map Bindings
	    // =========================================
	    factory.bind = {
	        zoom(map, callback) {
	            // google.maps.event.addListener(map, 'zoom_changed', () => {
	            //     callback(map.getZoom());
	            // });
				map.on("zoomlevelschange", function(event) {
					callback(map.getZoom());
				})
	        },
			click(map, callback){
				// google.maps.event.addListener(map, 'click', function(event){ //event
				// 	/*console.log('latitude is: '+event.latLng.lat());
				// 	console.log('longitude is: '+event.latLng.lng());*/
				// 	callback(event);
				// });
				map.on("click", function(event) {
					callback();
				})
			},
	        center(map, callback) {
	            // google.maps.event.addListener(map, 'center_changed', () => {
	            //     let c = map.getCenter(),
	            //         center = {
	            //             lat: c.lat(),
	            //             lng: c.lng()
	            //         };
	            //     callback(center);
	            // });
	        },
	        filter(filter, callback) {
	            filter.addListener('state_changed', () => {
	                let position = filter.get('position'),
	                    distance = filter.get('distance');
	                callback(position.lat(), position.lng(), distance);
	            });
	        },
			pan(map) {
				// google.maps.event.addListener(map, 'dragend', () => {
				// 	$('.map-btn-wrapper.top-control').css('display','table');
				// 	$('#svgLegends > .svg-legend').addClass('svg-legend-moved');
	            // });
			}
	    };

	    return factory;
	});
	return Box.Application.getService(SERVICE_NAME);
});
