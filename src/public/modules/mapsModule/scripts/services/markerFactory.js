/**
   * Name: markerFactory
   * Description: handles bindings of markers
   * @author: [Shakib Mohd]
   * Date: Oct 12, 2015
**/

/** globals: [] */
'use strict';
define([
	'modules/mapsModule/scripts/services/markerBuilder'
], (markerBuilder) => {

	let SERVICE_NAME = 'markerFactory';
	Box.Application.addService(SERVICE_NAME, (application) => {

		var factory = {};


		function _isEmpty(arg){
			if(arg && arg.length){
				return false;
			}
			return true;
		}


	    // =========================================
	    // Marker Class
	    // =========================================
	    factory.marker = function(type, data) {
	        this.type = type;
	        this.data = data;
	        // Callbacks
	        this._callbacks = {
	            mouseEnter: [],
	            mouseOut: [],
	            click: [],
	            resume: []
	        };
	    };

	    // =============== Initiate marker ============== //
	    factory.marker.prototype.init = function() {
	        let self = this, id = self.type+'Id',
	            selector = '#'+self.type+'_'+self.data[id];

	        self.elem = null;
	        self.addMarkerBuilder();

	        // Marker Events
	        self.mouseEnter(() => {
	            if(!self.elem) { self.elem = $(selector); }
	            /**start: remove all opened tooltip **/
	            $('.listing_marker').parent().css('z-index',0);
	            $('.listing_marker').removeClass('st-h');
	            /**end:  remove all opened tooltip **/
	            self.elem.parent().css('z-index',1);
	            self.elem.addClass('st-h');
	            application.broadcast('trackMapMarkerHover', self);
	        });

	        self.mouseOut(() => {
	        	self.elem.parent().css('z-index',0);
	            self.elem.removeClass('st-h');
	        });

	        self.click(() => {
	        	if(self.type === 'nearbyLocality'){
	        		$('.nearloc_marker_js').removeClass('st-h');
		        	$(self.div).find('.nearloc_marker_js').addClass('st-h');
	        	}
			    application.broadcast('markerClicked', self.data);
			    application.broadcast('trackMapMarkerClick', self);
			});


	    };
	    // ============================================== //
	    factory.marker.prototype.destroy = function(){
	        for(var cb in this._callbacks) {
	            this._callbacks[cb] = [];
	        }
	        this._callbacks = {};
	        this.template = undefined;
	        // this.scope.$destroy();
	        this.scope = undefined;
	        this.data = undefined;
	        this.builder = undefined;
	        this.elem = undefined;
	        this.type = undefined;
	    };

	    // =============== Events Handler =============== //
	    // Add event callback
	    factory.marker.prototype.addCallback = function(name, callback) {
	        if(!(this._callbacks && this._callbacks.hasOwnProperty(name))) {
	            this._callbacks[name] = [];
	        }
	        this._callbacks[name].push(callback);
	        return this;
	    };

	    // Trigger event callback
	    factory.marker.prototype.triggerEvent = function(name, page) {
	        $.each(this._callbacks[name], (idx, func) => {
	            setTimeout(() => {
	                func(this.data, page);
	            }, 0);
	        });
	        return this;
	    };
	    // ============================================== //

	    factory.marker.prototype.addMarkerBuilder = function(){
	        this.builder = markerBuilder[this.type];
	    };

	    // =============== Predefined Events =============== //
	    // mouseEnter
	    // mouseEnter() => To trigger mouseEnter
	    // mouseEnter(callback) => To add callback
	    factory.marker.prototype.mouseEnter = function(callback) {
	        if(_isEmpty(arguments)) {
	            this.triggerEvent('mouseEnter', 'MAP');
	        } else {
	            this.addCallback('mouseEnter', callback);
	        }
	        return this;
	    };

	    // mouseOut
	    // mouseOut() => To trigger mouseOut
	    // mouseOut(callback) => To add callback
	    factory.marker.prototype.mouseOut = function(callback) {
	        if(_isEmpty(arguments)) {
	            this.triggerEvent('mouseOut', 'MAP');
	        } else {
	            this.addCallback('mouseOut', callback);
	        }
	        return this;
	    };

	    // click
	    // click() => To trigger click
	    // click(callback) => To add callback
	    factory.marker.prototype.click = function(callback) {
	        if(_isEmpty(arguments)) {
	            this.triggerEvent('click', 'MAP');
	        } else {
	            this.addCallback('click', callback);
	        }
	        return this;
	    };

	    // resume
	    // resume() => To trigger resume
	    // resume(callback) => To add callback
	    factory.marker.prototype.resume = function(callback) {
	        if(_isEmpty(arguments)) {
	            this.triggerEvent('resume');
	        } else {
	            this.addCallback('resume', callback);
	        }
	        return this;
	    };

	    return factory;
	});
	return Box.Application.getService(SERVICE_NAME);
});
