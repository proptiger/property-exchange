/**
   * Name: markerBuilder
   * Description: builds marker html based on marker type
   * @author: [Shakib Mohd]
   * Date: Oct 12, 2015
**/

/** globals: [] */
'use strict';
define([
    'doT!modules/mapsModule/views/markers/listingMarker',
    'doT!modules/mapsModule/views/markers/propertyMarker',
    'doT!modules/mapsModule/views/markers/amenityMarker',
    'doT!modules/mapsModule/views/markers/landmarkMarker',
    'doT!modules/mapsModule/views/markers/commuteLandmarkmarker',
    'doT!modules/mapsModule/views/markers/nearbyLocalityMarker',
    'services/shortlist',
    'services/viewStatus'
], (listingMarkerHtml, propertyMarkerHtml, amentiyMarkerHtml, landmarkMarkerHtml, commuteLandmarkHtml, nearbyLocalityHtml, shortlistService, viewStatus) => {

    let SERVICE_NAME = 'markerBuilder';
	Box.Application.addService(SERVICE_NAME, (application) => {

        var htmlContent, templateData,
		$ = application.getGlobal('jQuery');

        var property = {
            build(data){
                let classes = [], tempClass;
                this.type = 'property';

                classes.push(this.type);
                tempClass = data && data.addDefaultClass || '';
                classes.push(tempClass);
                classes = classes.join(' ');

                templateData = {
                    classes: classes,
                    name: data.title
                };

                htmlContent =  propertyMarkerHtml(templateData);
                return $(htmlContent);
            }
        };

		var listing = {
            build(data){
                let classes = [], tempClass;
                this.type = 'listing';

                classes.push(this.type);
                tempClass = data.addDefaultClass || '';
                classes.push(tempClass);
                tempClass = shortlistService.checkShortlist({
                    type:'listing',
                    id: data.listingId,
                }) ? 'st-f' : '';
                classes.push(tempClass);
                tempClass = viewStatus.getStatus({
                    time:data.verificationDate,
                    serverTime:data.currentServerTime,
                    id:data.listingId,
                    type:'listing',
                });
                if(tempClass == 'seen') {
                    classes.push('st-c');
                }
                classes = classes.join(' ');

                templateData = {
                    id: data.selector,
                    classes: classes,
                    priceValue: typeof data.priceValue == "object" ?  data.priceValue.val : 'N/A',
                    priceValueUnit: typeof data.priceValue == "object" ? data.priceValue.unit : '',
                    sizeInfo: data.sizeInfo || '',
                    bhkInfo: data.isPlot ? '' : (data.bhkInfo || ''),
                    propertyType: data.propertyType || '',
                    imageUrl: data.mainImageURL ? data.mainImageURL : '',
                    shortlisted: shortlistService.checkShortlist({
                        type:'listing',
                        id: data.listingId,
                    }) ? 'st-f' : ''
                };

                htmlContent =  listingMarkerHtml(templateData);
                return $(htmlContent);
            }
		};

        var nearbyLocality = {
            build(data){
                return $(nearbyLocalityHtml(data))
            }
        }

		var amenity = {
            stringReplace(orig_str, pattern, replaceWith) {
                return String(orig_str).replace(pattern, replaceWith);
            },
            build(data, type, iconName) {
                this.type = type;
                templateData = {
                    //id: this.stringReplace(data.latitude, '.', '') + '-'+ this.stringReplace(data.longitude, '.', ''),
                    id: data.id,
                    amenityType: type,
                    iconName: iconName || '',
                    name: data.name,
                    index: data.index || ''
                };

                htmlContent =  amentiyMarkerHtml(templateData);
                return $(htmlContent);
            }
        };


        var school = {
            build(data) {
                return amenity.build(data, 'school', 'school');
            }
        };

        var restaurant = {
            build(data) {
                return amenity.build(data, 'restaurant', 'restaurant');
            }
        };

        var hospital = {
            build(data) {
                return amenity.build(data, 'hospital', 'hospital');
            }
        };

        var shopping_mall = {
            build(data) {
                return amenity.build(data, 'shopping_mall', 'shopping_mall');
            }
        };

        var atm = {
            build(data) {
                return amenity.build(data, 'atm', 'atm');
            }
        };

		var cinema = {
            build(data) {
                return amenity.build(data, 'cinema', 'cinema');
            }
        };

        var night_life = {
            build(data) {
                return amenity.build(data, 'night_life', 'night_life');
            }
        };

        var landmark = {
            build(data) {
                templateData = {
                    id: "landmark",
                    name: data.name
                };

                htmlContent =  landmarkMarkerHtml(templateData);
                return $(htmlContent);
            }
        };

        var commuteLandmark = {
            build(data) {
                templateData = {
                    id: 'commuteLandmark',
                    name: data.name,
                };

                htmlContent =  commuteLandmarkHtml(templateData);
                return $(htmlContent);
            }
        };

		return {
			listing,
			project:property,
            property:property,
            locality:property,
			school,
            restaurant,
            hospital,
            shopping_mall,
            atm,
    		night_life,
			cinema,
            landmark,
            commuteLandmark,
            nearbyLocality
		};
	});
    return Box.Application.getService(SERVICE_NAME);

});
