/**
 * @fileoverview
 * @author [Aditya Jha]
 */

/*global Box*/

"use strict";
define(['modules/mapsModule/scripts/services/mapFactory'], (mapFactory) => {
    let SERVICE_NAME = 'mapDirection';
    Box.Application.addService(SERVICE_NAME, () => {

        var factory = {};
        var map, directionService, directionDisplay;

        factory.init = function() {
            //directionService = new google.maps.DirectionsService();
        };

        factory.getMapRef = function() {
            return map;
        };

        factory.setMapRef = function(mapRef) {
            map = mapRef;
        };

        factory.removeDirection = function() {
            if(directionDisplay) {
                map.removeControl(directionDisplay);
                directionDisplay.remove();
            }
        };

        function getDirection(origin, destination, callback, rendererOptions) {
            let waypoints = [
                L.latLng(origin.lat, origin.lng),
                L.latLng(destination.lat, destination.lng)
            ];
            L.Routing.control({
                waypoints: waypoints,
                addWaypoints: false,
                router: new L.Routing.OSRMv1({
                    serviceUrl: window.OSM_ROUTER_URL
                }),
                lineOptions: {
                    styles: [{color: '#7cbaff', opacity: 1, weight: 5}]
                },
                createMarker: function() { 
                    return null;
                 },
                show: !rendererOptions.hideRouteList,
                fitSelectedRoutes: !rendererOptions.preserveViewport
            })
            .on("routesfound", function(routeEvent){
                let route = routeEvent.routes[0];

                callback('OK', {
                    mode: 'DRIVING',
                    distance: getDistanceText(route.summary.totalDistance),
                    duration: getTimeText(route.summary.totalTime),
                    lat: destination.lat,
                    lng: destination.lng,
                    id: destination.id
                });
                factory.removeDirection();
                directionDisplay = this;
            })
            .on("routingerror", function(error){
                console.log('Drawdirection Error with status: ', error.status);
                callback('ERROR', {msg: 'Error calculating direction'});
                factory.removeDirection();
            })
            .addTo(map);
        }
        function getDistanceText(distance){
            return distance < 100 ? `${parseInt(distance)} mtrs` : `${(distance/1000).toFixed(1)} km`
        }
        function getTimeText(time){
            const timeInSeconds = 60 * 60;
            let timeText;
            if(time > timeInSeconds){
                var timeInHrs = time/timeInSeconds;
                var remainingSeconds = time - timeInHrs;
                timeText = `${parseInt(timeInHrs)}hr ${Math.ceil(remainingSeconds*60)}`
            } else if(time < 60){
                timeText = `${Math.floor(time)} sec`
            } else{
                timeText = `${Math.floor(time/60)} mins`
            }
            return timeText;
        }
        factory.getDirection = function(origin, destination, callback, rendererOptions) {
            if(!L.Routing) {
                require(["https://cdnjs.cloudflare.com/ajax/libs/leaflet-routing-machine/3.2.12/leaflet-routing-machine.min.js"], getDirection.bind(factory, origin, destination, callback, rendererOptions));
            } else {
                getDirection.apply(factory, arguments);
            }
        };

        factory.destroy = function() {
            map = null;
            factory.removeDirection();
        };

        return factory;
    });
    return Box.Application.getService(SERVICE_NAME);
});
