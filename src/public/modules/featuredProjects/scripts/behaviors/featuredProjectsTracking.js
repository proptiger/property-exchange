define([
    'common/trackingConfigService',
    'services/trackingService'
], function(t, trackingService) {
    'use strict';
    Box.Application.addBehavior('featuredProjectsTracking', function(context) {
        var element;
        const messages = ['featuredProjectTracking','sponsoredProjectSeenTracking','sponsoredProjectCloseTracking','sponsoredProjectClickedTracking'];
        var init = function() {
            element = context.getElement();
        };
        var onmessage = function(name, data) {
            if(data.id!= element.id){
                return;
            }
            let properties = {};
            let label = data.projectId,category,event,sectionClicked;
            switch (name) {
                case 'featuredProjectTracking':
                    category = 'featured';
                    event = t.CLICK_EVENT;
                    break;
                case 'sponsoredProjectSeenTracking':
                    category = 'Omaxe project promotion';
                    event = t.VISIBLE_EVENT;
                    break;
                case 'sponsoredProjectClickedTracking':
                    category = 'Omaxe project promotion';
                    event = t.CLICK_EVENT;
                    sectionClicked = 'omaxeProject';
                    break;
                case 'sponsoredProjectCloseTracking':
                    category = 'Omaxe project promotion';
                    event = t.CLOSE_EVENT;
                    break;
            }
            properties[t.CATEGORY_KEY] = category;
            properties[t.LABEL_KEY] = label;
            properties[t.SECTION_CLICKED] = sectionClicked;
            trackingService.trackEvent(event, properties);
        };
        return {
            messages,
            onmessage,
            init,
            destroy: function() {}
        };
    });
});
