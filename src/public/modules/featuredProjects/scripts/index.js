/**
    This module is used for showing two main things "Featured Projects" and "sponsored Projects (as of now only of Omaxe)".
    Featured Projects are always inline,  as of now exists on "Home page" and "SERP page".
    Sponsored Projects can be inline, as of now on "Property Page" and "SERP RHS".

    module config parameters

    pageSource {{string}} : Exists for "Home page" and empty for all Others.
    featuredProjectsPlaceHolder {{string}} : specifies the container Element.
    className {{string}} : Exists for "Home page" and empty for all Others.
    isSponsored {{boolean}} : true if rendering sponsored projects.
    sponsoredProjectTitle {{string}} : specifies sponsored project title depending on placement.
    isRHS {{boolean}} : true for SERP RHS.
*/

define([
    'doT!modules/featuredProjects/views/index',
    'doT!modules/featuredProjects/views/mobileSponsored',
    'services/utils',
    'services/apiService',
    'services/commonService',
    'services/defaultService',
    'common/sharedConfig',
    'modules/featuredProjects/scripts/behaviors/featuredProjectsTracking'
], function(featuredProjectTemplate,sponsoredProjectTemplate, utils, apiService, commonService, defaultService, sharedConfig) {

    'use strict';
    Box.Application.addModule("featuredProjects", function(context) {

        let $, moduleEl, $moduleEl, config, pageData,sponsoredProjectTracked=[];

        function _renderProjects(url, label) {
            let isMobile = utils.isMobileRequest();
            apiService.get(url).then((response) => {
                let projects = response && response.projects || [],
                    exploreMoreUrl = response.projectsUrl,
                    template = config.isSponsored && isMobile && pageData.isSerp ? sponsoredProjectTemplate : featuredProjectTemplate;

                if(config.isSponsored){
                    projects = utils.shuffleArray(projects);
                    if(!projects.length && !isMobile)
                        $('.js-google-ad-serp').removeClass('hide');
                }
                $moduleEl.find(config.featuredProjectsPlaceHolder).html(template({
                    label,
                    projects,
                    exploreMoreUrl,
                    lengthForSlider: isMobile ? 1 : 2,
                    isMobile,
                    isSponsored:config.isSponsored,
                    title: config.sponsoredProjectTitle,
                    isRHS: config.isRHS
                }));
                commonService.startAllModules(moduleEl);
                context.broadcast('featuredProjectsRendered',{
                    count: projects.length
                });
                if(config.className && projects.length){
                    $moduleEl.addClass(config.className)
                }
                if(config.isSponsored && projects.length){
                    if(pageData.isSerp && isMobile){
                        setTimeout(function(){
                            $moduleEl.find('.js-spcard').addClass('show-sp-wrap');
                            trackSponsoredProject(projects[0].projectId);
                        }, 1000);
                    } else {
                        let sponsoredProjectsInViewPort = utils.attributeListInViewPort('.js-sponsoredProject','id')||[];
                        processProjectList(sponsoredProjectsInViewPort);
                    }
                }
            });
        }
        function processProjectList(projectsList){
            if(projectsList.length){
                projectsList.forEach(function(projectId) {
                    if(config.isSponsored && sponsoredProjectTracked.indexOf(projectId)==-1){
                        sponsoredProjectTracked.push(projectId);
                        trackSponsoredProject(projectId);
                    }
                });
            }
        }

        function fetchFeaturedProjects(fetchSponsoredProjects){
            let url,query = {fetchSponsoredProjects};
            if (config.pageSource == 'home') {
                    defaultService.getCityByLocation().then((response) => {
                    let userCity = response || {};
                    if ((userCity && userCity.id)) {
                        url = sharedConfig.apiHandlers.getFeaturedProjects({
                            cityId: userCity.id,
                            query
                        }).url;
                        _renderProjects(url, userCity.label);
                    }
                });
                return;
            } else if (pageData.cityId) {
                let pageLevel = (pageData.pageLevel && pageData.pageLevel.toLowerCase()) || '',
                    label = pageData.cityName;
                if (pageLevel.indexOf('suburb') > -1) {
                    label = pageData.suburbName;
                    query.suburbId = pageData.suburbId;
                } else if (pageLevel.indexOf('locality') > -1) {
                    label = pageData.localityName;
                    query.localityId = pageData.localityId;
                }

                url = sharedConfig.apiHandlers.getFeaturedProjects({
                    cityId: pageData.cityId,
                    query
                }).url;
                _renderProjects(url, label);
            }else{
                $('.js-google-ad-serp').removeClass('hide');
            }
        }
        function trackSponsoredProject(id){
            context.broadcast('sponsoredProjectSeenTracking',{ projectId : id,id:moduleEl.id});
        }


        return {
            behaviors: ['featuredProjectsTracking'],
            messages:['serpPageScrolled', 'propertyPageScrolled'],
            init() {
                moduleEl = context.getElement();
                $ = context.getGlobal('jQuery');
                $moduleEl = $(moduleEl);
                config = context.getConfig() || {};
                pageData = utils.getPageData();

                if(config.isSponsored){
                    fetchFeaturedProjects(true);
                }else{
                    fetchFeaturedProjects(false);
                }

                context.broadcast('moduleLoaded', {
                    'name': 'featuredProjects'
                });
            },
            onclick(event, clickElement, elementType) {
                let dataset = $(clickElement).data();
                let projectId;
                switch (elementType) {
                    case 'list-element':
                        projectId = dataset.id ? dataset.id : 'explore';
                        if(config.isSponsored){
                            context.broadcast('sponsoredProjectClickedTracking',{ projectId,id:moduleEl.id});
                        }else{
                            context.broadcast('featuredProjectTracking',{ projectId,id:moduleEl.id});
                        }
                        break;
                    case 'close-spcard':
                        $moduleEl.find('.js-spcard').addClass('hide');
                        projectId = dataset.id ? dataset.id : '';
                        context.broadcast('sponsoredProjectCloseTracking',{ projectId,id:moduleEl.id});
                        break;
                }
            },
            onmessage(name,data){
                switch (name){
                    case 'serpPageScrolled':
                    case 'propertyPageScrolled':
                        if(config.isSponsored){
                            var sponsoredProjectsInViewPort = utils.attributeListInViewPort('.js-sponsoredProject','id') || [];
                            processProjectList(sponsoredProjectsInViewPort);
                        }
                }
            },
            destroy() {
                moduleEl = null;
                $moduleEl = null;
            }
        };
    });

});
