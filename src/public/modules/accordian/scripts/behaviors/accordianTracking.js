define([
    'common/trackingConfigService',
    'services/trackingService'
], function(t, trackingService) {
    'use strict';
    Box.Application.addBehavior('accordianTracking', function() {

        var messages = [
            'trackClickAccodianHead'
        ];

        var onmessage = function(name, data) {
            let event = t.CLICK_EVENT,
                accordianLabels = t.ACCORDIAN_LABELS,
                label,
                linkName,
                linkType;

            switch (name) {
                case 'trackClickAccodianHead':
                    label = accordianLabels[data.id];
                    break;
            }

            let properties = {};
            properties[t.CATEGORY_KEY] = data.category;
            properties[t.LABEL_KEY] = label;
            properties[t.SOURCE_MODULE_KEY] = data.source;
            properties[t.SELLER_ID_KEY] = data.agentId;
            properties[t.BUILDER_ID_KEY] = data.builderId;
            properties[t.CITY_ID_KEY] = data.cityId;
            properties[t.LINK_NAME_KEY] = linkName;
            properties[t.LINK_TYPE_KEY] = linkType;

            trackingService.trackEvent(event, properties);
        };

        return {
            messages: messages,
            onmessage: onmessage,
            init: function() {},
            destroy: function() {}
        };
    });
});
