"use strict";
define([
    'modules/accordian/scripts/behaviors/accordianTracking'
], function() {
    Box.Application.addModule("accordian", function(context) {
        //const queryData = Box.DOM.queryData,
        var moduleEl,config = {},messages = [''];

        const ACCORDIAN_SEC = "js-accordian-sec",
            ACCORDIAN_HEAD = "js-accordian-head",
            ACTIVE = 'active';

        function init() {
            moduleEl = context.getElement();
            config = context.getConfig();
        }

        function onclick(event, clickElement, elementType) {
            let dataset = $(clickElement).data();
            switch (elementType) {
                case ACCORDIAN_HEAD:
                    if (dataset.trackId) {
                        context.broadcast('trackClickAccodianHead', { id: dataset.trackId });
                    }
                    let currentSec = $(clickElement).closest(`[data-type=${ACCORDIAN_SEC}]`),
                        removeClass = currentSec.hasClass(ACTIVE),
                        accordianSec;
                    $(moduleEl).find(`[data-type=${ACCORDIAN_SEC}]`).removeClass('active');
                    accordianSec = $(clickElement).closest(`[data-type=${ACCORDIAN_SEC}]`);
                    accordianSec.toggleClass(ACTIVE);
                    if (removeClass) {
                        currentSec.removeClass(ACTIVE);
                    } else {
                        currentSec.addClass(ACTIVE);
                    }
                    context.broadcast('accordianClicked', {
                        element: accordianSec,
                        moduleEl: moduleEl
                    });
                    break;
            }
        }

        function onmessage(name, data) {}

        function destroy() {}

        return {
            init,
            messages,
            behaviors: ['accordianTracking'],
            destroy,
            onclick,
            onmessage
        };
    });
});
