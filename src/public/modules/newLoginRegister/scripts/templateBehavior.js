"use strict";
define([
    'services/loginService',
    'services/defaultService',
    'services/utils'
], function() {
    Box.Application.addBehavior('templateBehavior', function(context) {

        //var apiService, 
        var config;
        var loginService = Box.Application.getService('LoginService');
        var commonService = Box.Application.getService('CommonService');
        var defaultService = Box.Application.getService('DefaultService');
        var utils = Box.Application.getService('Utils');
        var moduleEl, $, $moduleEl, pageType;

        var TEMPLATES = {
            SOCIAL_LOGIN:  '#sociable_connect',
            MAIN_LOGIN: '#main_login',
            FORGOT_PASSWORD: '#forgot_password',
            OTP_VERIFY: '#otp-verify',
            CHANGE_PASSWORD: '#change-password',            
            CREATE_SELLER: '#create_seller',
            CITY_SELLER_TYPE :   '#city_seller_type',
            MOBILE_INPUT:   '#change_phone',
            TERMS_CONDITION: '#terms_condition',
            THANKS:         '#thanks'
        };
        var TEMPLATES_MAP = {
            sociable_connect: {
                next: '#sociable_connect',
                prev: null
            },
            main_login: {
                next: '#main_login',
                prev: '#sociable_connect'
            },
            forgot_password: {
                next: '#forgot_password',
                prev: '#main_login'
            },
            create_seller: {
                next: '#create_seller',
                prev: '#main_login'
            },
            city_seller_type: {
                next: '#city_seller_type',
                prev: '#create_seller'
            },
            city_seller_type_init: {
                next: '#city_seller_type',
                prev: null
            },
            terms_condition: {
                next: '#terms_condition',
                prev: null
            },
            thanks: {
                next: '#thanks',
                prev: '#terms_condition'
            }
        };
        var TEMPLATES_WITH_NO_BACK_BUTTON = ['#sociable_connect', '#create_seller', '#city_seller_type', '#change_phone', '#terms_condition', '#thanks'];
        var active_template = '';
    

        var changeTemplate = function(next,prev,back){
            if(back){
                if(next){
                    $moduleEl.find(next).removeClass('p-left').addClass('p-center');    
                }
                if(prev){
                    $moduleEl.find(prev).removeClass('p-center').addClass('p-right');    
                }
                
            }
            else{
                if(prev){
                    $moduleEl.find(prev).removeClass('p-center').addClass('p-left');    
                }
                if(next){
                    $moduleEl.find(next).removeClass('p-right').addClass('p-center');    
                }
            }
            //Work around to hide close button on T&C page.
            //Bad way of doing things.
            if(next===TEMPLATES.TERMS_CONDITION){
                $moduleEl.find('.close-login').addClass('hide');
            }
            setTimeout(function(){$moduleEl.find(next + ' input:visible').eq(0).focus();},500);
            active_template = (next) ? next : active_template;
            if(TEMPLATES_WITH_NO_BACK_BUTTON.indexOf(next) === -1 && next){
                $moduleEl.find('.common-back-btn').removeClass('invisible');
            }else{
                $moduleEl.find('.common-back-btn').addClass('invisible');
            }
        };

        function setGeoCity(resp){
            if(resp && resp.id){
                loginService.setUserProp('cityId', resp.id);
                $moduleEl.find('.city-name').text(resp.label);
                $moduleEl.find('#city_seller_type .mian-field-box').removeClass('invisible');
            }
        }

        function setRegister(){
            $moduleEl.find('.js-username label').text('email');
            $moduleEl.find('.js-username .error').text('Please provide a valid email (3-50 chars)');
            $moduleEl.find('.js-remember-links').addClass('hide');
            $moduleEl.find('.js-new-register').addClass('hide');
            $moduleEl.find('.js-login-btn').text('sign up');
            $moduleEl.find('.js-name').removeClass('hide');
            loginService.setUserProp('NewRegistration',true);
        }
        function resetRegister(){
            loginService.setUserProp('NewRegistration',false);
            $moduleEl.find('.js-username label').text('email/username');
            $moduleEl.find('.js-username .error').text('Please provide a valid username/email (3-50 chars)');
            $moduleEl.find('#main-login .js-succsess-error-msg').text('');
            $moduleEl.find('.js-remember-links').removeClass('hide');
            $moduleEl.find('.js-new-register').removeClass('hide');
            $moduleEl.find('.js-login-btn').text('Login');
            $moduleEl.find('.js-name').addClass('hide');
        }

        function loadChangePhoneModule(){
            var data = {
                messageData:[{
                    name:'changePhone',
                    loadId:'change_phone'
                }],
                'message':'loadModule'
            };
            commonService.initializeCallbacks({data:data});
        }

        function clearTemplate(){
            $moduleEl.find('.p-center').removeClass('p-center');
        }

        function loadTemplate(type){
            let prevTemplate = (type) ? null : TEMPLATES.CREATE_SELLER;
            $moduleEl.find('.js-main-box').removeClass('hide');
            //Use case when seller is either migrated or created in MarketForce
            var pendingRegistration = loginService.getUserProp('pendingRegistration');
            var userRole = loginService.getUserProp('roles');
            if(userRole && pendingRegistration!=='PHONE_NOT_VERIFIED'){
                if(userRole.length >0 && userRole.indexOf("Seller")>-1){
                    //track t&C loaded
                    changeTemplate(TEMPLATES.TERMS_CONDITION, prevTemplate);
                    return;
                }
            }
            defaultService.getCityByLocation().then(function(resp){
                setGeoCity(resp);
                changeTemplate(TEMPLATES.CITY_SELLER_TYPE, prevTemplate);
            },function(){
                changeTemplate(TEMPLATES.CITY_SELLER_TYPE, prevTemplate);
            });  
        }

        function completeSellerRegistration(){
            var pendingRegistration = loginService.getUserProp('pendingRegistration');
            if(pendingRegistration){
                switch(pendingRegistration){
                    case 'PHONE_NOT_VERIFIED':
                    case 'INCORRECT_ROLE':
                        changeTemplate(null,TEMPLATES.CITY_SELLER_TYPE);
                        loadChangePhoneModule();   
                        break;
                    case 'TERMS_NOT_ACCEPTED':
                        changeTemplate(TEMPLATES.TERMS_CONDITION,TEMPLATES.CITY_SELLER_TYPE); 
                        break;
                }
                return;
            }
            changeTemplate(null,TEMPLATES.CITY_SELLER_TYPE);
            loadChangePhoneModule();
        }

        function handleBackAction(){
            var register = loginService.getUserProp('NewRegistration');
            if(register){
                resetRegister();
            }
            changeTemplate(TEMPLATES_MAP[active_template.replace('#','')].prev, active_template, true);
        }

        function renderTemplate(templateName){
            var template = TEMPLATES_MAP[templateName];
            if(template){
                changeTemplate(template.next, template.prev);
            }
        }

        function setCompanyDetail(){
            let $element = $moduleEl.find('#companyname'),
                companyName = $.trim($element.val());
            if(companyName === '' || utils.isValidCompanyName(companyName)){
                loginService.setUserProp('companyName', companyName);
                $element.closest('.login-input-box').removeClass('error');
                return true;
            }else{
                utils.trackEvent('EmailSignup_AgencyNameNotFilled', 'Seller_registration_error', 'please provide valid agency name');
                $element.closest('.login-input-box').addClass('error');
                return false;
            }
            $element = $moduleEl.find('#companyrera');
            let companyRera = $.trim($element.val());
            if(companyRera === ''){
                loginService.setUserProp('companyRera', companyRera);
                $element.closest('.login-input-box').removeClass('error');
                return true;
            }else{
                utils.trackEvent('EmailSignup_AgencyReraNotFilled', 'Seller_registration_error', 'please provide valid agency rera');
                $element.closest('.login-input-box').addClass('error');
                return false;
            }
 
        }

        return {
            messages: ['template-render', 'create-seller', 'openLoginType', 'set-geo-city', 'openOTPVerifyForm'],
            init: function() {
                moduleEl = context.getElement();
                config = context.getConfig();
                $ = context.getGlobal('jQuery');
                config = $.extend({}, config);
                $moduleEl = $(moduleEl);
                context.broadcast('templateBehaviorLoaded');
            },
            destroy: function() {
                /*moduleEl = null;
                $ = null;*/
            },
            onmessage: function(name, data) {
                switch (name) {
                    case 'openLoginType':
                        //let loginType = data.type;
                        pageType = utils.getTrackPageCategory();
                        utils.trackEvent('Form_open', loginService.getTrackingCategory('Login'), 'Buyer_' + pageType);
                        break;
                    case 'template-render':
                        if(data.name){
                            if (data.name === 'openOTPVerifyForm') {
                                setTimeout(changeTemplate(TEMPLATES.OTP_VERIFY, TEMPLATES.FORGOT_PASSWORD),2000);
                            }
                            else if(data.name === 'loadPreviousTemplate'){
                                clearTemplate();
                                changeTemplate(active_template, null);
                            }else{
                                renderTemplate(data.name);
                            }
                            if(data.name === 'city_seller_type_init' || data.name === 'city_seller_type'){
                                utils.trackEvent('Open_seller_registration', loginService.getTrackingCategory('Login_SignUp_Seller'), 'Open_seller_registration');
                            }
                        }
                        break;
                    case 'create-seller':
                        clearTemplate();
                        loadTemplate('direct');
                        break;
                    case 'set-geo-city':
                        setGeoCity(data.resp);
                        break;
                    default:
                        break;
                }
            },
            onclick: function(event, element, elementType) {
                pageType = utils.getTrackPageCategory();
                let pageCategory = 'Buyer_' + pageType;
                switch (elementType) {
                    case 'basic-login':
                        loginService.setUserProp('NewRegistration',false);
                        changeTemplate(TEMPLATES.MAIN_LOGIN,TEMPLATES.SOCIAL_LOGIN);
                        break;
                    case 'forgot-password':
                        changeTemplate(TEMPLATES.FORGOT_PASSWORD,TEMPLATES.MAIN_LOGIN);
                        break;
                    case 'forgot-submit':
                        var email = $('.js-email').val();
                        if (commonService.isEmail(email)) {
                            $('.js-email-error').removeClass('active');
                            // setTimeout(changeTemplate(TEMPLATES.OTP_VERIFY, TEMPLATES.FORGOT_PASSWORD),2000);
                        }
                        else {
                            $('.js-email-error').addClass('active');
                        }
                        break;
                    case 'otp-verify':
                        //  verify the entered OTP
                        //var otp = $moduleEl.find(TEMPLATES.OTP_VERIFY + ' .js-otp').val();
                        //var userEmail = $moduleEl.find(TEMPLATES.OTP_VERIFY + ' .js-otp-user-email').val();
                        break;
                    case 'backVerifyOtp':
                        changeTemplate(TEMPLATES.FORGOT_PASSWORD, TEMPLATES.OTP_VERIFY, true);
                        break;
                    case 'change-password':
                        setTimeout(changeTemplate(TEMPLATES.CHANGE_PASSWORD, TEMPLATES.OTP_VERIFY), 1000);
                        break;
                    case 'seller-type-next':
                        if(setCompanyDetail()){
                            completeSellerRegistration();
                        }
                        break;
                    case 'NewRegistration':
                        changeTemplate(TEMPLATES.MAIN_LOGIN,TEMPLATES.SOCIAL_LOGIN);
                        setRegister();
                        break;
                    case 'back-action':
                        handleBackAction();
                        break;
                    case 'create-seller':
                        clearTemplate();
                        utils.trackEvent('ContinueAs_Seller', loginService.getTrackingCategory('Login_SignUp'), pageCategory);
                        loadTemplate();
                        break;
                }
            }
        };
    });
});
