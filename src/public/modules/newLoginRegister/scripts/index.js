"use strict";
define([
    'text!modules/newLoginRegister/views/index.html',
    'doT!modules/newLoginRegister/views/tandc',
    'services/shortlist',
    'services/searchService',
    'services/apiService',
    'services/loginService',
    'services/trackingService',
    'modules/newLoginRegister/scripts/socialBehavior',
    'modules/newLoginRegister/scripts/registerBehavior',
    'modules/newLoginRegister/scripts/templateBehavior',
    'modules/newLoginRegister/scripts/otpBehavior',
    'services/utils',
    'services/localStorageService',
    'services/defaultService',
    'services/urlService',
    'modules/changePhone/scripts/index',
    "css!styles/css/newLoginRegister"
], function(html, tandchtml, shortlist) {
    /**
     * module for internal login
     * Events are
     * login__userLoggedIn (broadcasted when user is logged in)
     * moduleLoaded (broadcasted when module is loaded)
     * script__load__success__fb (internal usage)
     * script__load__failure__fb (internal usage)
     * login_module_template_show (internal usage)
     * login_module_template_hide (internal usage)
     */
    Box.Application.addModule('newLoginRegister', function(context) {
        const ERROR_CATEGORY = 'Seller_registration_error';
        const LOGIN_QUERY = '?login=true';
        const HASH_QUERY = window.location.hash;
        var apiService, loginService, commonService, utils, localStorage, defaultService, urlService,trackingService;
        var $, $moduleEl;
        var loginError = 'ERROR_Login';
        //var SUCCESS_ERROR_SELECTOR = ' .js-succsess-error-msg';
        var BUTTON_STROKE_CLASS = 'btn-with-strok';
        var moduleConfig = null, // to store module config
            moduleEl, // to store module DOM element
            newUser = true,
            checkingForUser = false,
            //viewStack = [],
            //currView,
            pageType, // for tracking the page
            //forgotPasswordLoaded = false,
            userData = {};
        // var TEMPLATES = {
        //     OTP_VERIFY: '#otp-verify',
        //     FORGOT_PASSWORD: '#forgot_password'
        // };
        let $domElement = {};
        const LOGIN_COOKIE = "user_info";
        const COMMON_SELLER_STORE = "dup_user";
        const IS_INTERNAL = "in_ip";
        const INTERNAL_IP_LIST = ['203.122.20.10'];
        /*
            function to validate the input values as per passed parameters
        */
        function validInput(text, minLength, maxLength) {
            if (!text) {
                return false;
            }
            if (minLength && text.length < minLength) {
                return false;
            }
            if (maxLength && text.length > maxLength) {
                return false;
            }
            return true;
        }
        /*
            function to initialize the common DOM elements
        */
        function initDomElement($moduleEl) {
            $domElement.inputs = $moduleEl.find('input');
            $domElement.username = $moduleEl.find('#username');
            $domElement.password = $moduleEl.find('#password');
            $domElement.name = $moduleEl.find('#name');
            // $domElement.agentrera = $moduleEl.find('#agentrera');
            $domElement.rememberme = $moduleEl.find('[name="rememberme"]');
            $domElement.successErrorMsg = $moduleEl.find('#main_login .js-succsess-error-msg');
            $domElement.emailError = $moduleEl.find('.js-email-error');
            $domElement.forgotEmail = $moduleEl.find('.js-email');
        }
        /*
            function to validate the inputs
        */
        function validateInputs() {
            var validInputs = true;
            var username = $domElement.username.val();
            var password = $domElement.password.val();
            var fullName = $domElement.name.val();
            // var agentRERA = $domElement.agentrera.val();
            var newRegister = loginService.getUserProp('NewRegistration');
            if (newRegister) {
                userData.fullName = $domElement.name.val();
                if (!utils.isValidCompanyName(fullName)) {
                    utils.trackEvent('EmailSignup_NameNotFilled', ERROR_CATEGORY, 'please provide your name (3-100 chars)');
                    $domElement.name.closest('.login-input-box').addClass('error');
                    return false;
                } else {
                    $domElement.name.closest('.login-input-box').removeClass('error');
                }
            }
            // if(!validInput(agentRERA, 1, 50)) {
            //     if (newRegister) {
            //         utils.trackEvent('EmailSignup_AgentRERAFailed', ERROR_CATEGORY, 'please provide a valid RERA no');
            //     } else {
            //         utils.trackEvent(loginError, ERROR_CATEGORY, 'please provide a valid RERA no');
            //     }
            //     $domElement.password.closest('.login-input-box').addClass('error');
            //     return false;
            // } else {
            //     $domElement.password.closest('.login-input-box').removeClass('error');
            // }
            var validationFunction = (newRegister) ? 'isEmail' : 'isValidValue';
            if (!validInput(username, 3, 50) || !commonService[validationFunction](username)) {
                if (newRegister) {
                    utils.trackEvent('EmailSignup_IncorrectEmail', ERROR_CATEGORY, 'please provide a valid email (3-50 chars)');
                } else {
                    utils.trackEvent(loginError, ERROR_CATEGORY, 'please provide a valid email or username (3-50 chars)');
                }
                $domElement.username.closest('.login-input-box').addClass('error');
                return false;
            } else {
                $domElement.username.closest('.login-input-box').removeClass('error');
            }
            if (!validInput(password, 6, 32)) {
                if (newRegister) {
                    utils.trackEvent('EmailSignup_PasswordFailed', ERROR_CATEGORY, 'please provide a valid password (6-32 chars)');
                } else {
                    utils.trackEvent(loginError, ERROR_CATEGORY, 'please provide a valid password (6-32 chars)');
                }
                $domElement.password.closest('.login-input-box').addClass('error');
                return false;
            } else {
                $domElement.password.closest('.login-input-box').removeClass('error');
            }
            return validInputs;
        }

        var showLoader = function() {
            $(".main-loading.box-loading").removeClass('hide');
        };
        var hideLoader = function() {
            $(".main-loading.box-loading").addClass('hide');
        };

        var internalLogin = function(data) {
            var postData = {};
            var fromEvent = false;
            if (!data || !data.username || !data.password) {
                userData.username = $domElement.username.val();
                userData.password = $domElement.password.val();
                postData = {
                    "username": userData.username,
                    "password": userData.password
                };
                fromEvent = true;
            } else {
                postData.username = data.username;
                postData.password = data.password;
            }
            if ($domElement.rememberme.prop('checked') || (data && data.rememberme)) {
                postData.rememberme = true;
                userData.rememberme = true;
            }
            if (fromEvent || (!fromEvent && !checkingForUser && !newUser)) {
                if (postData && postData.username && postData.password) {
                    $domElement.successErrorMsg.text("");

                    var newRegister = loginService.getUserProp('NewRegistration');
                    if (newRegister && commonService.isEmail(postData.username)) {
                        userData.fullName = $domElement.name.val();
                        if (!userData.fullName) {
                            $domElement.name.closest('.login-input-box').addClass('error');
                        } else {
                            utils.trackEvent('submit_signup', loginService.getTrackingCategory('Login_SignUp'), 'user_created');
                            createUser();
                        }
                    } else {
                        showLoader();
                        loginService.doLogin(postData).then(loginSuccess, _errorHandler);
                    }

                }
            }
            if (!commonService.isValidValue(postData.username)) {
                $domElement.username.closest('.login-input-box').addClass('error');
            }

            function _errorHandler(error) {
                loginFailed(error);
            }
        };
        /*
            function to login if values are valid
        */
        function checkAndLogin() {
            //var signupError = 'ERROR_Sign Up',

            var username = $domElement.username.val();
            var password = $domElement.password.val();
            var fullName = $domElement.name.val();
            loginService.setUserProp('fullName', fullName);
            loginService.setUserProp('name', fullName);
            loginService.setUserProp('username', username);
            loginService.setUserProp('password', password);
            loginService.setUserProp('email', username);
            if (validateInputs()) {
                internalLogin();
                return;
            }
        }



        function loginFailed(error) {
            var errorMsg = (error.status === 497) ? "wrong credentials/password" : 'some error occurred at server , please try after some time';
            utils.trackEvent('Email', loginService.getTrackingCategory('Login_Error'), errorMsg);
            showLoginError(errorMsg);
        }

        function showLoginError(message) {
            hideLoader();
            $domElement.successErrorMsg.text(message);
        }

        function removeLoginError() {
            $domElement.successErrorMsg.text('');
            $moduleEl.find('.login-input-box').removeClass('error');
        }

        function _setLoginCookie(data) {
            var userCookie = {
                name: data.firstName,
                email: data.email,
                phone: data.contactNumber
            };
            utils.setcookie(LOGIN_COOKIE, JSON.stringify(userCookie));
        }

        function _isInternalIP(ipAddr) {
            if (INTERNAL_IP_LIST.indexOf(ipAddr) === -1) {
                return false;
            }
            return true;
        }

        function _trackDuplicateSeller(data) {
            var isInternal = utils.getCookie(IS_INTERNAL);
            if (!isInternal) {
                loginService.getClientIp().then((ipAddr) => {
                    if (!_isInternalIP(ipAddr)) {
                        _trackUserInLocalStorage(data);
                    }
                    else {
                        utils.setcookie(IS_INTERNAL, Math.random());
                    }
                });
            }
        }

        function _trackUserInLocalStorage(data) {
            //  do the following only for sellers
            if (data.roles && data.roles.indexOf('Seller') !== -1) {
                var duplicateSeller = localStorage.getItem(COMMON_SELLER_STORE);
                if (duplicateSeller === null) {
                    duplicateSeller = [data.id];
                }
                else if (duplicateSeller.indexOf(data.id) === -1) {
                    duplicateSeller.push(data.id);
                    //  make async API call here
                    loginService.duplicateSeller(duplicateSeller[0], duplicateSeller.slice(1));
                }
                localStorage.setItem(COMMON_SELLER_STORE, duplicateSeller);
            }
        }

        function loadCityTemplate() {
            $moduleEl.find('.p-center').removeClass('p-center');
            context.broadcast('template-render', {
                name: 'city_seller_type_init'
            });
        }
        var closeLogin = function(callback) {
            loginService.closeLoginPopup(callback);
        };


        function checkRegistrationComplete(provider, pageType) {
            loginService.registrationComplete().then(function(response) {
                if (response.register.pending) {
                    context.broadcast('create-seller', {});
                } else if (response.data.roles) {
                    utils.trackEvent('Login_' + provider + '_success_Seller', loginService.getTrackingCategory('Login'), 'Seller_' + pageType);
                    if (window.history.length > 2) {
                        closeLogin();
                    }
                    // TODO if required have to add landing page for each kind of user
                    let redirectTo = urlService.getUrlParam('redirectTo');
                    redirectTo = redirectTo ? urlService.changeUrlParam("login", "true", redirectTo, true) : LOGIN_QUERY;
                    redirectTo = (redirectTo) ? redirectTo + HASH_QUERY : HASH_QUERY;
                    window.setTimeout(function() {
                        window.location.href = ((moduleConfig.sellerMicrositeUrl + redirectTo)||'').replace(/([^:]\/)\/+/gi,'$1');
                    }, 0);

                }
            });
        }
        //This is used by social login as well.
        function loginSuccess(response) {
            var data = response;
            let provider = loginService.getUserProp('loginProvider') || 'email';
            pageType = utils.getTrackPageCategory();
            hideLoader();
            context.broadcast("onLoggedIn", {
                response: data.data
            });
            _setLoginCookie(data.data);
            _trackDuplicateSeller(data.data);
            shortlist.syncShortlists();
            $moduleEl.find('#user-name').text(data.data.firstName);
            // searchService.syncSavedSearches();
            let roles = data.data.roles;
            if (loginService.getUserProp('getRegistered')) {
                trackingService.trackEvent('Login_' + provider + '_success_Buyer', {
                    "Login Type": provider,
                    category: 'ListProperty_Login',
                    label: 'Buyer_' + pageType
                });
                checkRegistrationComplete(provider, pageType);
            } else if (roles && roles.length > 0) {
                checkRegistrationComplete(provider, pageType);
            } else {
                trackingService.trackEvent('Login_' + provider + '_success_Buyer', {
                    "Login Type": provider,
                    category: 'Login',
                    label: 'Buyer_' + pageType
                });
                loginService.cleanUserDetails();
                closeLogin();
            }
        }

        function createUser() {
            showLoader();
            userData.email = userData.username;
            if (!userData.fullName) {
                userData.fullName = userData.username.split('@')[0];
            }
            loginService.setUserProp('fullName', userData.fullName);
            loginService.createUser(userData).then(_successHandler, _errorHandler);

            function _successHandler() {
                loginUser();
            }

            function _errorHandler() {
                hideLoader();
                var email = $domElement.username.val();
                loginService.checkUserExists(email).then(function(response) {
                    if (response.userExists) {
                        showLoginError("User already registered, please try login");
                    } else {
                        showLoginError("Some error occured, please try later!");
                    }
                }, function() {
                    showLoginError("Some error occured, please try later!");
                });
            }
        }

        function loginUser() {
            var postData = {
                "username": userData.email,
                "password": userData.password
            };
            loginService.doLogin(postData).then(_successHandler, _errorHandler);

            function _successHandler(data) {
                hideLoader();
                context.broadcast("onLoggedIn", {
                    response: data.data
                });
                shortlist.syncShortlists();
                // searchService.syncSavedSearches();
                // loginService.cleanUserDetails();
                // closeLogin();
                loginService.setUserProp('id', data.data.id);
                $moduleEl.find('#user-name').text(data.data.firstName);

                if (loginService.getUserProp('getRegistered')) {
                    defaultService.getCityByLocation().then(function(resp) {
                        context.broadcast('set-geo-city', {
                            resp: resp
                        });
                        loadCityTemplate();
                    }, function() {
                        loadCityTemplate();
                    });
                } else {
                    context.broadcast('template-render', {
                        name: 'create_seller'
                    });
                }
                if (data.data) {
                    _setLoginCookie(data.data);
                }
            }

            function _errorHandler() {
                showLoginError("Please try again later!!");
            }
        }

        function bindControlEvents($moduleEl) {
            if ($moduleEl) {
                $domElement.inputs.on('focus', function() {
                    $(this).closest('.login-input-box').addClass('active');
                }).on('blur', function() {
                    let $this = $(this),
                        id = $this.attr('id'),
                        value = $this.val(),
                        $loginInputBox = $this.closest('.login-input-box');
                    if (!value) {
                        $loginInputBox.removeClass('active').addClass('error');
                    } else {
                        switch (id) {
                            case 'username':
                                let newRegister = loginService.getUserProp('NewRegistration'),
                                    validationFunction = (newRegister) ? 'isEmail' : 'isValidValue';
                                if (value && commonService[validationFunction](value)) {
                                    $loginInputBox.removeClass('error');
                                } else {
                                    $loginInputBox.addClass('error');
                                }
                                break;
                            case 'password':
                                if (validInput(value, 6, 32)) {
                                    $loginInputBox.removeClass('error');
                                } else {
                                    $loginInputBox.addClass('error');
                                }
                                break;
                            case 'name':
                                if (utils.isValidCompanyName(value)) {
                                    $loginInputBox.removeClass('error');
                                } else {
                                    $loginInputBox.addClass('error');
                                }
                                break;
                            default:
                                break;
                        }
                    }
                });
            }
        }

        function unBindBlurEvents() {
            $domElement.inputs.off('focus');
            $domElement.inputs.off('blur');
        }


        var unloadChangePhone = function() {
            let changePhone = document.getElementById('change_phone');
            if (Box.Application.isStarted(changePhone)) {
                commonService.stopModule(changePhone);
            }
        };

        var templateBehaviorLoaded = function(config) {
            html = doT.template(html)({
                "sellerMicrositeUrl": moduleConfig.sellerMicrositeUrl,
                "cities": config
            });
            loginService.setUserProp('sellerMicrositeUrl', moduleConfig.sellerMicrositeUrl);
            $moduleEl.append(html);

            // Highlight signup button if signup=true in query params
            if (urlService.getUrlParam('signup') === 'true') {
                var $loginButton = $(moduleEl).find('[data-type="basic-login"]');
                var $registerButton = $(moduleEl).find('[data-type="NewRegistration"]');
                $loginButton.insertAfter($registerButton);
                $loginButton.addClass(BUTTON_STROKE_CLASS);
                $registerButton.removeClass(BUTTON_STROKE_CLASS);
            }

            let pending = loginService.getUserProp('pending');
            if (!pending) {
                let type = loginService.getUserProp('type');
                let templateName = 'sociable_connect';
                switch (type) {
                    case 'basic':
                        templateName = 'main_login';
                        break;
                    default:
                        break;
                }
                context.broadcast('template-render', {
                    name: templateName
                });
            } else {
                context.broadcast('create-seller', {});
            }
            initDomElement($moduleEl);
            bindControlEvents($moduleEl);
            context.broadcast('moduleLoaded', {
                name: 'newLoginRegister'
            });
            context.broadcast('popup:open', {
                'id': moduleEl.id
            });
            $moduleEl.find('.js-terms-text').append(tandchtml({}));
        };

        return {
            behaviors: ['socialBehavior', 'registerBehavior', 'templateBehavior', 'otpBehavior'],
            init: function() {
                moduleEl = context.getElement();
                moduleConfig = context.getConfig() || {};
                $ = context.getGlobal('jQuery');
                commonService = context.getService('CommonService');
                loginService = context.getService('LoginService');
                apiService = context.getService('ApiService');
                urlService = context.getService('URLService');
                trackingService = context.getService('TrackingService');
                utils = context.getService('Utils');
                localStorage = context.getService('localStorageService');
                defaultService = context.getService('DefaultService');
                $moduleEl = $(moduleEl);
                var config = context.getConfig();
                context.broadcast("moduleLoaded", { name: "newLoginRegister", id: moduleEl.id });
                config.titleCity = 'Select City';
                defaultService.getCities().then(function(response) {
                    config.cities = response;
                    templateBehaviorLoaded(config);
                }, function() {
                    templateBehaviorLoaded(config);
                });
            },

            destroy: function() {
                moduleEl = null;
                var configScript = $moduleEl.find('script[type="text/x-config"]')[0];
                $moduleEl.empty();
                $moduleEl.html(configScript);
                commonService = null;
                loginService = null;
                apiService = null;
                userData = {};
                unBindBlurEvents();
            },
            onkeydown: function(event, element, elementType) {
                switch (elementType) {
                    case "inputBox":
                        var key = event.keyCode || event.which;
                        if (key === 13) {
                            checkAndLogin();
                            break;
                        }
                        break;
                }
            },
            onclick: function(event, element, elementType) {
                pageType = utils.getTrackPageCategory();
                let pageCategory = 'Buyer_' + pageType;
                switch (elementType) {
                    case 'basic-login':
                        utils.trackEvent('Login_email', loginService.getTrackingCategory('Login'), 'registration_type');
                        break;
                    case 'login-btn':
                        checkAndLogin();
                        break;
                    case 'toggle-password':
                        $(element).closest('.login-input-box').find('input#password').
                        attr('type', function(index, attr) {
                            return attr == 'password' ? 'text' : 'password';
                        });
                        break;
                    case 'NewRegistration':
                        utils.trackEvent('Login_create_account', loginService.getTrackingCategory('Login'), pageCategory);
                        removeLoginError();
                        break;
                    case 'close-login':
                        utils.trackEvent('Login_closed', loginService.getTrackingCategory('Login'), pageCategory);
                        utils.trackEvent('Click_Cross', 'Seller_registration_Cross', loginService.getRegistrationScreenLabel());
                        closeLogin();
                        break;
                    case 'fb-login':
                        utils.trackEvent('Login_facebook', loginService.getTrackingCategory('Login'), 'registration_type');
                        break;
                    case 'gplus-login':
                        utils.trackEvent('Login_google', loginService.getTrackingCategory('Login'), 'registration_type');
                        break;
                    case 'remember-me':
                        utils.trackEvent('Remember_Me', loginService.getTrackingCategory('Login'), 'Remember_Me');
                        break;
                    case 'seller-login':
                        utils.trackEvent('Login_Seller', loginService.getTrackingCategory('Login'), pageCategory);
                        break;
                    case 'skip-seller':
                        utils.trackEvent('Click_ContinueAs_Buyer', loginService.getTrackingCategory('Login_SignUp'), pageCategory);
                        loginService.cleanUserDetails();
                        closeLogin();
                        break;
                }
            },
            messages: ['SocialLoggedIn', 'homeLoginUnload', 'unloadChangePhone'],
            onmessage: function(name, data) {
                pageType = utils.getTrackPageCategory();
                switch (name) {
                    case 'SocialLoggedIn':
                        loginSuccess(data.data);
                        break;
                    case 'homeLoginUnload':
                        unloadChangePhone();
                        commonService.stopModule(moduleEl);
                        break;
                    case 'unloadChangePhone':
                        if (data.id === 'newLoginRegister') {
                            unloadChangePhone();
                            Box.Application.broadcast('template-render', {
                                name: 'loadPreviousTemplate'
                            });
                        }
                        break;
                }
            }
        };
    });
});
