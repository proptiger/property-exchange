"use strict";
define([
  "services/apiService",
  "services/utils"
], function() {
  Box.Application.addBehavior('otpBehavior', function(context) {
    var moduleEl, $, $moduleEl, $errorMsg, OTPCode, utils;
    var loginService = Box.Application.getService('LoginService');
    var commonService = Box.Application.getService('CommonService');
    var apiService = Box.Application.getService('ApiService');
    //const LOGIN_COOKIE = "user_info";

    function showLoader(){
      $moduleEl.find(".main-loading.box-loading").removeClass('hide');
    }

    function hideLoader(){
      $moduleEl.find(".main-loading.box-loading").addClass('hide');
    }

    function loginSuccess(response){
      var data = response;
      //let provider = loginService.getUserProp('loginProvider') || 'email';
      hideLoader();
      context.broadcast("onLoggedIn",{
          response: data
      });
      $moduleEl.find('#update-password-name').text(data.fullName);
      $moduleEl.find('[data-type="change-password"]').click();
    }

    function updatePassword(newPass) {
      if (!newPass) {return;}
      loginService.changePasswordLoggedIn(newPass).then(function() {
        $moduleEl.find('#change-password success-error-msg').removeClass('hide');
        setTimeout(window.location.reload(), 2000);
      }, function() {
        $moduleEl.find('#change-password success-error-msg').text('Password update failed');
        $moduleEl.find('#change-password success-error-msg').removeClass('hide');
        setTimeout(loginService.closeLoginPopup(), 2000);
      });
    }

    function validateOTP(OPTCode, userId){
      showLoader();
      loginService.validateOTPAndLogin(OPTCode, userId).then(function(data){
        hideLoader();
        var resp = data;
        if(resp.data){
          $errorMsg.addClass('hide');
          loginSuccess(resp.data);
        }else{
          $errorMsg.removeClass('hide');
        }
      }, function(){
        hideLoader();
        $errorMsg.removeClass('hide');
      });
    }

    function submitForReset(email, sendOtpOnCall){
      if(commonService.isEmail(email)){
        // $(moduleEl).find('#forgot-password .js-email-error').removeClass('active');
        showLoader();
        var postData = {
          "email" : email,
          "source": window.location.hostname,
          "sendOtpOnCall": sendOtpOnCall || false
        };

        loginService.forgotPassword(postData).then(_successHandler, _errorHandler);
        function _successHandler(response){                 // jshint ignore:line
            hideLoader();
            Box.Application.broadcast('template-render',{
                name: 'openOTPVerifyForm'
            });
            utils.trackEvent('Email_Submit', loginService.getTrackingCategory('Login_Forgot_Password'), 'Success');
            // console.log(response);
            $(moduleEl).find('#otp-verify .js-otp-user-id').attr('value', response.userId);
            $(moduleEl).find('#otp-verify .js-otp-user-email').attr('value', response.email);
            $moduleEl.find('#update-password-email').text(response.email);
            if (response.phone) {
              $moduleEl.find('#update-password-phone').text(response.phone);
            }
            // $(moduleEl).find('#forgot_password .js-succsess-error-msg').html("OTP to reset password has been sent to your registerd mobile number");
        }
        function _errorHandler(){ // jshint ignore:line
            hideLoader();
            utils.trackEvent('Email_Submit', loginService.getTrackingCategory('Login_Forgot_Password'), 'Failure_sorry! you are not a registered user.');
            $(moduleEl).find('#forgot_password .js-succsess-error-msg').html("Sorry! you are not a registered user.");
        }
      }
      else{
        utils.trackEvent('Reset_password', 'User_email_error', 'please provide a valid email');
      }
    }

    // clear existing entered OTP (if exists)
    function clearOTP() {
      $moduleEl.find('.otp-code input').map(function() {
        this.value = '';
      });
    }

    function getEmailId() {
      return $moduleEl.find('#otp-verify .js-otp-user-email').attr('value');
    }


    return {
      message: [],
      init: function() {
        moduleEl = context.getElement();
        //config = context.getConfig();
        $ = context.getGlobal('jQuery');
        $moduleEl = $(moduleEl);
        commonService = context.getService('CommonService');
        apiService = context.getService('ApiService');
        loginService = context.getService('LoginService');
        utils = context.getService('Utils');
        // configScript = $moduleEl.find('script[type="text/x-config"]');
        
        // bindControlEvents();
        // gaTrack(trackingData.mobileScreen);
        context.broadcast('moduleLoaded', {
          name: 'changePhone',
          'options': {
            callback: function(){
              setTimeout(function(){$moduleEl.find('input').eq(0).focus();},100);
            }
          }
        });
      },
      destroy: function(){
        // $moduleEl.html(configScript);
        // configScript = null;
        // config = null;
        // OTPCode = null;
        // moduleEl = null;
        // $ = null;
        // $moduleEl = null;
        // loginService=null;
        // unBindBlurEvents();
      },
      onclick: function(event, element, elementType) {
        var email;
        switch (elementType) {
          case 'change-password':
            var newPassword = $('#change-password .js-password').val();
            updatePassword(newPassword);
            break;
          case 'forgot-submit':
            utils.trackEvent('Email_Submit', loginService.getTrackingCategory('Login_Forgot_Password'), 'attempt');
            email = $('.js-email').val();
            submitForReset(email);
            break;
          case 'resend-otp':
            clearOTP();
            email = getEmailId();
            submitForReset(email);
            break;
          case 'resend-otp-on-call':
            $moduleEl.find('.call-otp-view').addClass('hide');
            clearOTP();
            email = getEmailId();
            submitForReset(email, true);
            break;
        }
      },
      onkeyup: function(event, element, elementType){
        switch (elementType){
          case 'validateOTP':
            var key = event.keyCode || event.which,
              allInputs = $moduleEl.find('.otp-code input'),
              currentInput = $(event.target),
              currentIndex = allInputs.index(currentInput);
            if(key == 37){
              var nextIndex = (currentIndex > 0) ? currentIndex-1 : 0;
              allInputs.eq(nextIndex).focus();
            }else if(key == 39){
              allInputs.eq(currentIndex+1).focus();
            }
            else if(currentInput.val() !=='' && key !== 8 && key !== 46){
              allInputs.eq(currentIndex+1).focus();
              OTPCode = $moduleEl.find('.otp-code input').map(function() {
                  return this.value;
              }).get().join('');
              if(OTPCode.toString().length === 4){
                $errorMsg = $moduleEl.find('.success-error-msg');
                $errorMsg.addClass('hide');
                //var OTPCodeLength = OTPCode.toString().length;
                var userId = $moduleEl.find('#otp-verify .js-otp-user-id').attr('value');
                validateOTP(OTPCode, userId);
              }
            }
            break;
        }
      },
      onkeydown: function(event, element, elementType){
        switch (elementType){
          case 'number':
          case 'validateOTP':
            var key = event.keyCode || event.which;
            if (((key >= 48 && key <= 57) || (key >= 96 && key <= 105) || key == 8 || key == 9 || key == 46 || key == 37 || key == 39) && (event.shiftKey === false || (event.shiftKey === true && key == 9))){
              var maxlength = (elementType === 'number') ? 10 : 1;
              var value = $(element).val(),
                valueLength = value.toString().length;
              if(valueLength >= maxlength && key !== 8 && key!== 46 && key !== 37 && key!== 39){
                event.preventDefault();
              }
              if(elementType === 'validateOTP'){
                var allInputs = $moduleEl.find('.otp-code input'),
                  currentInput = $(event.target),
                  currentIndex = allInputs.index(currentInput);
                if((key === 46 || key === 8) && currentInput.val() ===''){
                  var nextIndex = (currentIndex > 0) ? currentIndex-1 : 0;
                  allInputs.eq(nextIndex).focus();
                }
              }
            }else{
               event.preventDefault();
            }
            break;
          default:
            break;
        }
      }
    };
  });
});