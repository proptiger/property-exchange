"use strict";
define([
    'services/leadPyrService',
    'services/urlService',
    "services/apiService",
    'services/utils',
    "services/loginService"], function(leadPyrService, urlService) {
	Box.Application.addBehavior('registerBehavior',function(context) {
        var TEMPLATES = {
            CITY_SELLER_TYPE :   '#city_seller_type',
            SOCIAL_LOGIN:   '#sociable_connect',
            MOBILE_INPUT:   '#change_phone',
            TERMS_CONDITION: '#terms_condition',
            THANKS:         '#thanks',
            CREATE_SELLER: '#create_seller'
        };
        // var trackingService = Box.Application.getService('gaTracking');
        var SUCCESS_ERROR_SELECTOR = ' .js-succsess-error-msg';
        var SELLER_ERROR_SELECTOR = ' .js-succsess-error-msg-seller';
        var CITY_DROPDOWN_SELECTOR = '.js-location-list';
        //var SELLER_BROKER_TYPE = ' .js-company';
        var commonService, apiService, config;// activeInputIndex=0;
        var loginService = Box.Application.getService('LoginService');
        var utils = context.getService('Utils');
		var moduleEl,$,$moduleEl;
        var trackingData = {
            Owner:{
                action:'SELECT_SignUp_Who are you',
                category:'Login_SignUp_Seller',
                label: 'Owner'
            },
            Broker:{
              action:'SELECT_SignUp_Who are you',
                category:'Login_SignUp_Seller',
                label: 'Agent'  
            },
            Builder:{
              action:'SELECT_SignUp_Who are you',
                category:'Login_SignUp_Seller',
                label: 'Builder'  
            },
            fbLogin:{
                action:'CLICK_SignUp_Social',
                category:'Seller_SignUp',
                label: 'Facebook'
            },
            gplusLogin:{
                action:'CLICK_SignUp_Social',
                category:'Seller_SignUp',
                label: 'Google'
            },
            citySelected:{
                action:'Select_City',
                category:'Login_SignUp_Seller',
                label: 'city'
            },
            thanks:{
                action:'Registration_Success',
                category:'Login_SignUp_Seller',
                label: 'Add_Property_screen'  
            },
            addProperty: {
                action:'Click_AddProperty',
                category:'Login_SignUp_Seller',
                label: 'Add_Property'
            },
            tAndCLoaded:{
                action:'T&C_Seller',
                category:'Seller_SignUp',
                label: 'T&C_Seller'  
            },
            showMobileScreen:{
                action:'Enter_Mobile',
                category:'Seller_SignUp',
                label: 'Enter_Mobile'  
            },
            emailError:{
                action:'EmailSignup_IncorrectEmail',
                category:'Seller_registration_error',
                label: 'plaese Provide valid email'  
            },
            disagree:{
                action:'T&C_Seller_Disagree',
                category:'Login_SignUp_Seller',
                label: 'T&C_Seller_Disagree'  
            },
            back:{
                action:'Click_back',
                category:'Seller_registration_Back',
                label: "back"  
            },
            close:{
                action:'Click_Cross',
                category:'Seller_registration_Cross',
                label: "Close"  
            }     
        };

        function setUtmParams(utmSource, utmMedium, utmCampaign, data = {}) {
            data.utmSource = leadPyrService.stripQuotes(urlService.getUrlParam('utm_source')) || utmSource;
            data.utmMedium = leadPyrService.stripQuotes(urlService.getUrlParam('utm_medium')) || utmMedium;
            data.utmCampaign = leadPyrService.stripQuotes(urlService.getUrlParam('utm_campaign')) || utmCampaign;
            return data;
        }

        function gaTrack(data){
            utils.trackEvent(data.action, loginService.getTrackingCategory(data.category), data.label);
        }
        function createSeller(){
            showLoader();
            var data = loginService.getUser();
            data = setUtmParams('standard registration', 'web', 'unknown_campaign', data);
            apiService.postJSON(config.createSellerURL, data).then(_successHandler, _errorHandler);
            function _successHandler(){
                hideLoader();
                loginService.setUserProp('pendingRegistration',false);
                $moduleEl.find( TEMPLATES.TERMS_CONDITION+ SELLER_ERROR_SELECTOR).addClass('hide');
                context.broadcast('template-render',{
                    name: 'thanks'
                });
                gaTrack(trackingData.thanks);
            }
            function _errorHandler(){
                hideLoader();
                // TODO Error handling
                $moduleEl.find( TEMPLATES.TERMS_CONDITION+ SELLER_ERROR_SELECTOR).removeClass('hide');
                showHideElement($moduleEl.find('[data-type="TermsAgreed"]'), 'removeClass');
            }
        }
        
        function updateSeller() {
            showLoader();
            var data = {
                firstTimeUser: true
            };
            data = setUtmParams('mf registration', 'web', 'unknown_campaign', data);
            data = JSON.stringify(data);
            apiService.putJSON(config.updateSellerURL, data).then(_successHandler, _errorHandler);
            function _successHandler() {
                hideLoader();
                $moduleEl.find(TEMPLATES.TERMS_CONDITION + SELLER_ERROR_SELECTOR).addClass('hide');
                gaTrack(trackingData.thanks);
                context.broadcast('template-render',{
                    name: 'thanks'
                });
            }
            function _errorHandler() {
                hideLoader();
                // TODO Error handling
                $moduleEl.find(TEMPLATES.TERMS_CONDITION + SELLER_ERROR_SELECTOR).removeClass('hide');
                showHideElement($moduleEl.find('[data-type="TermsAgreed"]'), 'removeClass');
            }
        }

        function bindControlEvents(){
            if($moduleEl){
                $moduleEl.find('input').on('focus', function(){
                    $(this).closest('.login-input-box').addClass('active');
                });
                $moduleEl.find('input').on('blur', function(){
                    if(!$(this).val()){
                        $(this).closest('.login-input-box').removeClass('active');
                    }
                });
            }
        }

        // function bindBasicEvents(){
        //     $moduleEl.find('#email').on('blur', function(){
        //         var email = $(this).val();
        //         if(email && commonService.isEmail(email)){
        //             $(this).closest('.login-input-box').removeClass('error');
        //         }else{
        //             gaTrack(trackingData.emailError);
        //             showEmailError('Please provide a valid email (3-50 chars)');
        //         }
               
        //     });
        // }
        
        function unBindBlurEvents(){
            $moduleEl.find('#email').off('blur');
            $moduleEl.find('#fullname').off('blur');
            $moduleEl.find('#password').off('blur');
            $moduleEl.find('input').off('focus');
            $moduleEl.find('input').off('blur');
        }
        
        // function bindBlurEvents(selector){
        //     $moduleEl.find(selector).on('blur', function(){
        //         if($(this).val()){
        //             $(this).closest('.login-input-box').removeClass('error');
        //         }else{
        //             $(this).closest('.login-input-box').addClass('error');
        //         }
        //     });
        // }
       
        function showErrMsg(mainSelector,show){
            console.log('erro.....', mainSelector);
            var errorEl = $moduleEl.find( mainSelector+ SUCCESS_ERROR_SELECTOR);
            show?errorEl.removeClass('hide'):errorEl.addClass('hide'); //jshint ignore:line
        }
        function citySelected(element, city, id){
            var citySelected;
            var cityId;
            if(element){
                citySelected = $(element).text();
                cityId = $.trim($(element).data("city-id"));
            }else{
                citySelected = city;
                cityId =  id;
            }
            trackingData.citySelected.label = citySelected;
            gaTrack(trackingData.citySelected);
            $moduleEl.find('.city-name').text(citySelected);
            $moduleEl.find(CITY_DROPDOWN_SELECTOR).removeClass("open");
            $moduleEl.find('.location-list>ul>li').removeClass("active");
            $(element).addClass('active');
            showErrMsg(TEMPLATES.CITY_SELLER_TYPE,false);
            loginService.setUserProp('cityId', cityId);
            $moduleEl.find('#city_seller_type .mian-field-box').removeClass('invisible');
        }
        function sellerSelected(element){
            $moduleEl.find('.seller-type-list>li').removeClass('active');
            $(element).addClass('active');
            var sellerType = $.trim($(element).data('seller-type'));
            loginService.setUserProp('sellerType', sellerType);
            gaTrack(trackingData[sellerType]);
            if(sellerType === 'Owner'){
                $moduleEl.find('.seller-type-input-box').addClass('invisible');
                $moduleEl.find('.js-seller-type-next').click();
                return;      
            }
            var comapnyNameLabel = "agency name";
            if(sellerType === 'Builder'){
                comapnyNameLabel = "builder name";
            }
            $moduleEl.find('.seller-type-input-box label').html(comapnyNameLabel);
            $moduleEl.find('.seller-type-input-box').removeClass('invisible');
        }
        function setListCity(city){
            var cityDiv = $moduleEl.find("[data-type='changecity']")[0];
            var textToChange = cityDiv.childNodes[0];
            textToChange.nodeValue = city;
            var cityList = $moduleEl.find(".drop-down-list");
            cityList.find("li").removeClass("active");
            cityList.find("[data-value='"+city+"']").addClass("active");
            $moduleEl.find('#search-bar').val("");
        }
        /*
            function to show hide terms agreed button
        */
        function showHideElement(element, action){
            element[action]('invisible');
        }

        function termsAgreed(){
            showHideElement($moduleEl.find('[data-type="TermsAgreed"]'), 'addClass');
            loginService.termsAccepted().then(function(){
                showErrMsg(TEMPLATES.TERMS_CONDITION, false);
                var userRole = loginService.getUserProp('roles');
                //Use case when seller is either migrated or created in MarketForce
                if(userRole){
                    if(userRole.length >0 && userRole.indexOf("Seller")>-1){
                        if(loginService.getUserProp('acceptedTermAndConditions') > 0){
                            context.broadcast('template-render',{
                                name: 'thanks'
                            });
                            gaTrack(trackingData.thanks);
                        }else{
                            updateSeller();
                        }
                        return;
                    }
                }
                createSeller();
            }, function(){
                showErrMsg(TEMPLATES.TERMS_CONDITION, true);
                showHideElement($moduleEl.find('[data-type="TermsAgreed"]'), 'removeClass');
                //todo: show erorr message
            });
        }
        function closeRegister(){
            loginService.closeLoginPopup();
        }
        function toggleCityList(target){
                $moduleEl.find(".js-drop-down-list").toggleClass("active");
                var city = target.getAttribute('data-value');
                if(city){
                    setListCity(city);
                    window.localStorage.setItem('localityCity', target.getAttribute('data-value'));
                    window.localStorage.setItem('localityCityId', target.getAttribute('data-city-id'));
                    window.localStorage.setItem('localityMap', '');
                }
        }
        function resetCityDropDown(){
                $moduleEl.find('.drop-down-list input').val('');
                $moduleEl.find('.js-addLocality .drop-down-list ul li').removeClass('hide');
        }
        function showLoader(){
            $(".main-loading.box-loading").removeClass('hide');
        }
        function hideLoader(){
            $(".main-loading.box-loading").addClass('hide');
        }
        // function showEmailError(message){
        //     var loginBox = $moduleEl.find('#email').closest('.login-input-box');
        //     loginBox.addClass('error');
        //     loginBox.find('.error').html(message);
        // }

        

		return {
					messages: ['change_phone_otp_verified','citySelected'],
					init:function(){
						moduleEl = context.getElement();
                        config = context.getConfig();
						$ = context.getGlobal('jQuery');
                        config = $.extend({}, config);
						$moduleEl = $(moduleEl);
						commonService = context.getService('CommonService');
						apiService = context.getService('ApiService');
                        if(!config){
                            config = {};
                        }

                        if(!config.createUserURL){
                            config.createUserURL = '/xhr/userService/createUser';
                        }
                        if(!config.createSellerURL){
                            config.createSellerURL = '/xhr/userService/createSeller';
                        }
                        if (!config.updateSellerURL) {
                            config.updateSellerURL = '/xhr/userService/updateSeller';
                        }
                        if(!config.loginURL){
                            config.loginURL = '/xhr/userService/doLogin';
                        }
   
                        bindControlEvents();
                       
					},
					destroy:function(){
						/*moduleEl=null;
						$=null;*/
                        var configScript = $moduleEl.find('script[type="text/x-config"]')[0];
                        $moduleEl.empty();
                        $moduleEl.html(configScript);
						/*commonService = null;
						apiService = null;*/
                        unBindBlurEvents();
					},
					onmessage: function(name,data){
						switch(name){
						   case 'change_phone_otp_verified':
                                Box.Application.stop(document.getElementById(data.id));
                                loginService.setUserProp('phoneNotVerified',false);
                                gaTrack(trackingData.tAndCLoaded);
                                context.broadcast('template-render',{
                                    name: 'terms_condition'
                                });
                                break;
                            case 'citySelected':
                                citySelected(data.element, data.city, data.id);
                                break;
						}
					},
                    oninput: function(event, element, elementType) {
                        switch (elementType) {
                            case "searchcity":
                                $moduleEl.find('.location-list-box .location-list ul li').addClass('hide').filter(function() { return $(this).text().trim().toLowerCase().indexOf(event.target.value.trim().toLowerCase()) === 0; }).removeClass('hide');
                                break;
                            default:
                                break;
                        }
                    },
					onclick: function(event, element, elementType) {
                        if(elementType !== 'city-dropdown' && elementType !== 'searchcity'){
                            $moduleEl.find(CITY_DROPDOWN_SELECTOR).removeClass('open');
                        }
				        switch (elementType) {
				            case 'city-dropdown':
                                //$moduleEl.find(CITY_DROPDOWN_SELECTOR).addClass("open");
                                $moduleEl.find(CITY_DROPDOWN_SELECTOR).toggleClass('open');
                                break;
                            case 'city-list-item':
                                citySelected(element);
                                break;
                            case 'seller-type':
                                sellerSelected(element);
                                break;
                            case 'backSocial':
                                trackingData.back.label = 'connectWith';
                                gaTrack(trackingData.back);
                                break;
                            case 'backBasicRegister':
                                trackingData.back.label = 'email signUp';
                                gaTrack(trackingData.back);
                                break;
                            case 'backPassword':
                                trackingData.back.label = 'enter password';
                                break;
                            case 'TermsAgreed':
                                loginService.setUserProp('termsNotVerified',false);
                                termsAgreed();
                                break;
                            case 'upload-listing':
                                gaTrack(trackingData.addProperty);
                                closeRegister();
                                break;
                            case 'close':
                                gaTrack(trackingData.disagree);
                                closeRegister();
                                break;
                            case 'back-action':
                                trackingData.back.label = loginService.getRegistrationScreenLabel();
                                gaTrack(trackingData.back);
                                break;
                            case 'backOTP':
                                trackingData.back.label = 'backOTP';
                                gaTrack(trackingData.back); 
                                break;
                            case "changecity":
                                if(event.target.nodeName.toLowerCase() == 'li'){
                                    $moduleEl.find(".js-drop-down-list").addClass('hide');
                                    toggleCityList(event.target);
                                    resetCityDropDown();
                                    $moduleEl.find(".js-drop-down-list").removeClass('hide');
                                }else{
                                    toggleCityList(event.target);
                                    resetCityDropDown();
                                    setTimeout(function(){
                                        $moduleEl.find('[data-type="searchcity"]')[0].focus();    
                                    },1000);
                                }
                            
                                break;
                        }
				    }
				};
	});
});