
define([
    'services/trackingService',
    "services/apiService",
    "common/sharedConfig",
    "services/commonService",
    "services/utils",
    "common/trackingConfigService",
    "doT!modules/askYourQuestion/views/index",
    "doT!modules/askYourQuestion/views/thankyou",
], function (trackingService, apiService, sharedConfig, commonService, utilService, t, moduleTemplate, thankyouTemplate) {
    "use strict";
    const MODULE_NAME = "askYourQuestion";
    Box.Application.addModule(MODULE_NAME, (context) => {
        let moduleEl, $, $moduleEl, moduleConfig, underProcessing = false, errorFlag = false;
        const config = {
            defaultHideTime: 5000,
            SELECTOR: {
                "PHONE": "[data-type=PHONE_FIELD] input",
                "COUNTRY_CODE": "[data-country-code=COUNTRY_CODE_FIELD]",
                "COUNTRY_ID": "input[data-country=COUNTRY_ID_FIELD]",
                "QUESTION": "[data-comment] textarea",
                "BUTTON_NORMAL": "[data-normal]",
                "BUTTON_PROCESSING": "[data-processing]",
                "ERROR_TEXT": ".show-error",
                "CONTENT_BOX": ".content-box",
                "FORM_CONTAINER": "[data-form-container]"
            },
            messages: {
                "PHONE": "Please enter a valid phone number",
                "QUESTION": "Please enter the question before submit",
                "API_FAILURE_DEFAULT": "Some error occurred.. try again"
            },
            classes: {
                "SKELETON_CLASS": "form-row-skeleton"
            }
        }

        function _trackEvent (event, label) {
            var trackingObj = {};
            trackingObj[t.CATEGORY_KEY] = 'Ask a Question';
            trackingObj[t.LABEL_KEY] = label;
            trackingService.trackEvent(event, trackingObj);
        }

        function showProcessing() {
            underProcessing = true;
            $moduleEl.find(config.SELECTOR["BUTTON_NORMAL"]).addClass('hide');
            $moduleEl.find(config.SELECTOR["BUTTON_PROCESSING"]).removeClass('hide');
        }

        function hideProcessing() {
            underProcessing = false;
            $moduleEl.find(config.SELECTOR["BUTTON_NORMAL"]).removeClass('hide');
            $moduleEl.find(config.SELECTOR["BUTTON_PROCESSING"]).addClass('hide');
        }

        function _showError({ field }) {
            $moduleEl.find(`[data-message="${field}"]`).html(config.messages[field]);
            _hideError({field, timeOut: config.defaultHideTime})
        }

        function _hideError({ field, timeOut }) {
            if (timeOut){
                setTimeout(() => {
                    $moduleEl.find(`[data-message="${field}"]`).html('');
                }, timeOut);
            } else {
                $moduleEl.find(`[data-message="${field}"]`).html('');
            }
        }

        function postData(){
            let phoneNumber = $moduleEl.find(config.SELECTOR.PHONE).val().trim();
            let question = $moduleEl.find(config.SELECTOR.QUESTION).val().trim();
            let listingId = moduleConfig.listingId;
            let url = sharedConfig.apiHandlers.postAskYourQuestion().url;
            let currentDate = new Date();
            currentDate = `${utilService.formatDate(currentDate.getTime(), "dd-mm-YY")} ${currentDate.toLocaleTimeString()}`;
            showProcessing();
            $moduleEl.find(config.SELECTOR["ERROR_TEXT"]).html('');
            apiService.postJSON(url, { phoneNumber, question, listingId, currentDate }).then(response => {
                hideProcessing();
                $moduleEl.find(config.SELECTOR["CONTENT_BOX"]).html(thankyouTemplate({}));
                _trackEvent("Submit", phoneNumber);
            }, err => {
                let msg = err && err.body && err.body.error && err.body.error.msg || config.messages["API_FAILURE_DEFAULT"];
                $moduleEl.find(config.SELECTOR["ERROR_TEXT"]).html(msg);
                hideProcessing();
            });
        }

        function ValidateFields(field) {
            let value, country;
            switch(field){
                case "PHONE":
                    country = $(moduleEl).find(config.SELECTOR["COUNTRY_ID"]).data("label");
                    value = $moduleEl.find(config.SELECTOR.PHONE).val();
                    if (!value || (value && !utilService.validatePhone(value, country))) {
                        _showError({ field: 'PHONE' });
                        errorFlag = true;
                    } else {
                        _hideError({ field: "PHONE" });
                    }
                    break;
                case "QUESTION":
                    value = $moduleEl.find(config.SELECTOR.QUESTION).val().trim();
                    if (!value) {
                        _showError({ field: 'QUESTION' });
                        errorFlag = true;
                    } else {
                        _hideError({ field: "QUESTION" });
                    }
                    break;
            }
        }

        function submit() {
            errorFlag = false;
            let fieldsToValidate = ["PHONE", "QUESTION"];
            fieldsToValidate.forEach(field => { ValidateFields(field)})
            if (!errorFlag && !underProcessing) {
                postData();
            }
        }

        function _setCountry(data) {
            if (data && data.code && data.value) {
                $(moduleEl).find(config.SELECTOR["COUNTRY_CODE"]).text(data.code);
                $(moduleEl).find(config.SELECTOR["COUNTRY_ID"]).val(data.value).data("label", data.label);
            }
        }

        function render() {
            $moduleEl.find(config.SELECTOR["FORM_CONTAINER"]).html(moduleTemplate({}));
            commonService.startAllModules(moduleEl);
        }

        return {
            init(){
                moduleEl = context.getElement();
                $ = context.getGlobal('jQuery');
                $moduleEl = $(moduleEl);
                moduleConfig = context.getConfig();
                render();
            },
            destroy(){
                moduleEl, $, $moduleEl, moduleConfig = null;
            },
            onclick(event, element, elementType){
                switch (elementType) {
                    case "submit_question":
                        submit();
                        break;
                }
            },
            messages: ["singleSelectDropdownChanged", "singleSelectDropdownInitiated"],
            onmessage(name, data = {}){
                if (data.id != "ask-question-country"){
                    return;
                }
                switch (name) {
                    case "singleSelectDropdownChanged":
                    case "singleSelectDropdownInitiated":
                        let params = data && data.params && JSON.parse(decodeURI(data.params));
                        _setCountry(params);
                        break;
                }
            }
        }
    })
})