"use strict";
define([
    "text!modules/login/views/index.html",
    'services/shortlist',
    'services/searchService',
    'services/apiService',
    'services/loginService',
    'modules/login/scripts/socialBehavior',
    'services/utils'
], function(html, shortlist){
    /**
     * module for internal login
     * Events are
     * login__userLoggedIn (broadcasted when user is logged in)
     * moduleLoaded (broadcasted when module is loaded)
     * script__load__success__fb (internal usage)
     * script__load__failure__fb (internal usage)
     * login_module_template_show (internal usage)
     * login_module_template_hide (internal usage)
     */
    Box.Application.addModule('login', function(context) {
        const ERROR_CATEGORY = 'Login_Error';
        var Application;
        var apiService, loginService, commonService,utils;
        var $, $moduleEl;
        //var MAIN_BOX_SELECTOR = '.js-main-box';
        var SUCCESS_ERROR_SELECTOR = ' .js-succsess-error-msg';
        var moduleConfig = null, // to store module config
            moduleEl,   // to store module DOM element
            newUser = true,
            checkingForUser = false,
            origUName = '',
            //viewStack = [],
            //currView,
            pageType, // for tracking the page
            //forgotPasswordLoaded = false,
            userData = {};
        var TEMPLATES = {
            SOCIAL_LOGIN:  '#sociable-connect',
            MAIN_LOGIN: '#main-login',
            FORGOT_PASSWORD: '#forgot-password',
        };
        var defaultConfig = {
            baseURL : '/',
            internalLoginUrl : 'xhr/userService/doLogin',            // to store URL for posting login credentials to
            redirectUrl: '/listings',
            forgotPasswordURL: '/xhr/userService/forgotPassword',
            createUserURL: '/xhr/userService/createUser',
            loginURL: '/xhr/userService/doLogin'
        };
        const loginTypeStepMaping = {
           basic: {
               next: TEMPLATES.MAIN_LOGIN,
               prev:TEMPLATES.SOCIAL_LOGIN,
               back: false
           },
           social: {
               next: TEMPLATES.SOCIAL_LOGIN,
               prev: TEMPLATES.MAIN_LOGIN,
               back: true
           }
        },
        LOGIN_COOKIE = "user_info";
        function validInput(text,minLength,maxLength){
            if(!text)
            {
                return false;
            }
            if(minLength && text.length < minLength)
            {
                return false;
            }
            if(maxLength && text.length > maxLength)
            {
                return false;
            }
            return true;
        }
        var showLoader = function(){
            $(".main-loading.box-loading").removeClass('hide');
        };
        var hideLoader = function(){
            $(".main-loading.box-loading").addClass('hide');
        };

        var internalLogin = function(data){
            var postData = {};
            var fromEvent = false;
            if(!data || !data.username || !data.password){
                userData.username = $moduleEl.find('#username').val();
                userData.password = $moduleEl.find('#password').val();
                postData = {
                    "username" : userData.username,
                    "password" : userData.password
                };
                fromEvent = true;
            }else{
                postData.username = data.username;
                postData.password = data.password;
            }
            if($(moduleEl).find('[name="rememberme"]').prop('checked') || (data && data.rememberme)){
              postData.rememberme = true;
              userData.rememberme = true;
            }
            if(fromEvent || (!fromEvent && !checkingForUser && !newUser)){
                if(postData && postData.username && postData.password){
                    $moduleEl.find('#main-login .js-succsess-error-msg').text("");
                    
                    var newRegister = loginService.getUserProp('NewRegistration');
                    if(newRegister && commonService.isEmail(postData.username)){
                        userData.fullName = $moduleEl.find('#name').val();
                        if(!userData.fullName){
                            $moduleEl.find('#name').closest('.login-input-box').addClass('error');    
                        } else{
                            createUser();    
                        }   
                    } else{
                        showLoader();
                        apiService.post(moduleConfig.baseURL + moduleConfig.internalLoginUrl,
                                        postData,
                                        {
                                            contentType: 'application/x-www-form-urlencoded'
                                        },
                                        true).then(loginSuccess, _errorHandler);
                    }

                }
            }
            if(!commonService.isValidValue(postData.username)){
                $moduleEl.find('#username').closest('.login-input-box').addClass('error');
            }
            function _errorHandler(error){
                loginFailed(error);
                console.log('error occurred on login : '+error);
            }
        };
        function checkAndLogin(){
            var signupError = 'ERROR_Sign Up',
                loginError = 'ERROR_Login';
            var username = $moduleEl.find('#username').val();
            var password = $moduleEl.find('#password').val();
            var fullName = $moduleEl.find('#name').val();
            var validInputs = true;
            loginService.setUserProp('fullName',fullName);
            loginService.setUserProp('username', username);
            loginService.setUserProp('password', password);
            loginService.setUserProp('email', username);
            var newRegister = loginService.getUserProp('NewRegistration');
            if(newRegister){
                userData.fullName = $moduleEl.find('#name').val();
                if(!validInput(fullName,3,100)){
                    utils.trackEvent(signupError, ERROR_CATEGORY, 'please provide your name (2-100 chars)');
                    $moduleEl.find('#name').closest('.login-input-box').addClass('error');
                    validInputs = false;    
                } else {
                    $moduleEl.find('#name').closest('.login-input-box').removeClass('error');
                } 
            }
            var validationFunction = (newRegister) ? 'isEmail' : 'isValidValue';
            if(!validInput(username,3,50) || !commonService[validationFunction](username)){
                utils.trackEvent(newRegister ? signupError : loginError, ERROR_CATEGORY, 'please provide a valid email (3-50 chars)');
                $moduleEl.find('#username').closest('.login-input-box').addClass('error');
                validInputs = false;  
            } else {
                $moduleEl.find('#username').closest('.login-input-box').removeClass('error');
            }
            if(!validInput(password,6,32)){
                utils.trackEvent(newRegister ? signupError : loginError, ERROR_CATEGORY, 'please provide a valid password (6-32 chars)');
                $moduleEl.find('#password').closest('.login-input-box').addClass('error');
                validInputs = false; 
            } else {
                 $moduleEl.find('#password').closest('.login-input-box').removeClass('error');
            }
            if(validInputs){
                internalLogin();
                return;
            }
        }
        function bindBlurBasicLoginScreen(){
            $moduleEl.find('#username').on('blur', function(){
                var newRegister = loginService.getUserProp('NewRegistration'),
                    validationFunction = (newRegister) ? 'isEmail' : 'isValidValue',
                    $this = $(this),
                    value = $this.val();
                if(value && commonService[validationFunction](value)){
                    $this.closest('.login-input-box').removeClass('error');
                }else{
                    $this.closest('.login-input-box').addClass('error');
                }
            });
            $moduleEl.find('#password').on('blur', function(){
                if(validInput($(this).val(),6,32)){
                    $(this).closest('.login-input-box').removeClass('error');
                }else{
                    $(this).closest('.login-input-box').addClass('error');
                }
            });
            $moduleEl.find('#name').on('blur', function(){
                if(validInput($(this).val(),3,100)){
                    $(this).closest('.login-input-box').removeClass('error');
                }else{
                    $(this).closest('.login-input-box').addClass('error');
                }
            });
        }

        
        function loginFailed(error){
        var errorMsg = (error.status === 497) ? "wrong credentials/password" : 'some error occurred at server , please try after some time';
            utils.trackEvent('Email', 'Login_Error', errorMsg);
            showLoginError(errorMsg);
        }
        function showLoginError(message){
            hideLoader();
            $moduleEl.find('#main-login .js-succsess-error-msg').text(message);
        }
        function removeLoginError(){
         $moduleEl.find('#main-login .js-succsess-error-msg').text('');
         $moduleEl.find('#username').closest('.login-input-box').removeClass('error');
         $moduleEl.find('#password').closest('.login-input-box').removeClass('error');
         $moduleEl.find('#name').closest('.login-input-box').removeClass('error');
        }
        function _setLoginCookie(data) {
            let contactNumbers = [];
            $.each(data.contactNumbers,(key,val)=>{
                let temp = {};
                temp.contactNumber = val.contactNumber;
                temp.isVerified = val.isVerified;
                // temp.priority = data.priority;
                contactNumbers.push(temp);
            });
            var userCookie = {
                name: data.firstName,
                email: data.email,
                phone: data.contactNumber,
                contactNumbers:contactNumbers
            };
            utils.setcookie(LOGIN_COOKIE,JSON.stringify(userCookie));
        }
        var closeLogin = function(callback){
            var data={};
            if(callback) {
                data.options = {
                    callback: callback
                };
            }
            Box.Application.broadcast('homeLoginUnload');
            context.broadcast("popup:close", data);
        };
        //This is used by social login as well.
        function loginSuccess(response){
            var data = response;
            if(!moduleConfig.isBuyer){
                apiService.get('/xhr/userService/registrationComplete').
                then(function(response){
                    if(response.register.pending){
                        loginService.setUserProp('id',response.data.id);
                        loginService.setUserProp('name',response.data.firstName);
                        loginService.setUserProp('fullName',response.data.firstName);
                        loginService.setUserProp('email',response.data.email);
                        loginService.setUserProp('contactNumber',response.data.contactNumber);
                        loginService.setUserProp('pendingRegistration',response.register.pendingRegistration);
                        loginService.setUserProp('roles',response.data.roles);
                        loginService.setUserProp('acceptedTermAndConditions',response.data.acceptedTermAndConditions.length);
                        hideLoader();
                        closeLogin(startSellerRegistration);
                    }
                    else{
                        window.location = moduleConfig.redirectUrl;
                    }
                }, function(error){
                    console.log('Error : '+ error);
                    hideLoader();
                });
            }
            else if(moduleConfig.isBuyer){
                let provider = loginService.getUserProp('loginProvider') || 'email';
                pageType = utils.getTrackPageCategory();
                utils.trackEvent('Login_' + provider + '_success', 'Login', 'Buyer_' + pageType);
                hideLoader();
                context.broadcast("onLoggedIn",{
                    response: data.data,
                    roles: data.roles
                });
                shortlist.syncShortlists();
                // searchService.syncSavedSearches();
                loginService.cleanUserDetails();
                closeLogin();
            }
            _setLoginCookie(data.data);
        }
        function startSellerRegistration(){
            Application.broadcast('InitiateRegistration');
        }

        function createUser(){
            showLoader();
            userData.email = userData.username;
            if(!userData.fullName){
                userData.fullName = userData.username.split('@')[0];    
            }
            loginService.setUserProp('fullName', userData.fullName);
            apiService.postJSON(moduleConfig.createUserURL, userData, null, true).then(_successHandler, _errorHandler);
            function _successHandler(){
                loginUser();
            }
            function _errorHandler(error){
                hideLoader();
                var email=$moduleEl.find('#username').val();
                loginService.checkUserExists(email).then(function(response){
                    if(response.userExists){
                       showLoginError("User already registered");
                    } else {
                       showLoginError("Some error occured, please try later!");
                    }
                }, function(){
                    showLoginError("Some error occured, please try later!");
                });
                console.log('error occurred in creating user :'+ error);
            }
        }
        function loginUser() {
            var postData = {
                "username" : userData.email,
                "password" : userData.password
            };
            var headers = {
                contentType: 'application/x-www-form-urlencoded'
            };
            apiService.post(moduleConfig.loginURL, postData, headers).then(_successHandler, _errorHandler);
            function _successHandler(data){
                hideLoader();
                context.broadcast("onLoggedIn", {
                    response: data.data,
                    roles: data.roles
                });
                shortlist.syncShortlists();
                // searchService.syncSavedSearches();
                loginService.cleanUserDetails();
                closeLogin();
                if(data.data){
                    _setLoginCookie(data.data);
                }
            }
            function _errorHandler(error){
                showLoginError("Please try again later!!");
                console.log('error occurred in logging in :'+error);
            }
        }
        function setRegister(){
            removeLoginError();
            if(moduleConfig.isBuyer){
                $moduleEl.find('.js-username label').text('email');
                $moduleEl.find('.js-username .error').text('Please provide a valid email (3-50 chars)');
                $moduleEl.find('.js-remember-links').addClass('hide');
                $moduleEl.find('.js-new-register').addClass('hide');
                $moduleEl.find('.js-login-btn').text('sign up');
                $moduleEl.find('.js-name').removeClass('hide');
                loginService.setUserProp('NewRegistration',true);
            } else{
                closeLogin(startSellerRegistration);
            }
        }
        function resetRegister(){
            loginService.setUserProp('NewRegistration',false);
            $moduleEl.find('.js-username label').text('email/username');
            $moduleEl.find('.js-username .error').text('Please provide a valid username/email (3-50 chars)');
            $moduleEl.find('#main-login .js-succsess-error-msg').text('');
            $moduleEl.find('.js-remember-links').removeClass('hide');
            $moduleEl.find('.js-new-register').removeClass('hide');
            $moduleEl.find('.js-login-btn').text('Login');
            $moduleEl.find('.js-name').addClass('hide');
        }
        function backLogin(){
            var register = loginService.getUserProp('NewRegistration');
            if(register){
               resetRegister();
            } else {
                 changeTemplate(TEMPLATES.SOCIAL_LOGIN,TEMPLATES.MAIN_LOGIN,true);
            }
        }
        function changeTemplate(next,prev,back){
            if(back){
                if(next){
                    $moduleEl.find(next).removeClass('p-left').addClass('p-center');
                }
                if(prev){
                    $moduleEl.find(prev).removeClass('p-center').addClass('p-right');
                }
            }
            else{
                if(prev){
                    $moduleEl.find(prev).removeClass('p-center').addClass('p-left');
                }
                if(next){
                    $moduleEl.find(next).removeClass('p-right').addClass('p-center');
                }
            }
            setTimeout(function(){$moduleEl.find('input:visible').eq(0).focus();},400);
        }
        function bindControlEvents(){
            if($moduleEl){
                $moduleEl.find('input').on('focus', function(){
                    $(this).closest('.login-input-box').addClass('active');
                    if($(this).attr('id') === 'username'){
                        origUName = $(this).val();
                    }
                });
                $moduleEl.find('input').on('blur', function(){
                    if(!$(this).val()){
                        $(this).closest('.login-input-box').removeClass('active');
                    }
                });
            }
        }

        function unBindBlurEvents(){
            $moduleEl.find('input').off('focus');
            $moduleEl.find('input').off('blur');
            $moduleEl.find('#username').off('blur');
            $moduleEl.find('#password').off('blur');
        }

        var submitForReset = function(email){
            if(commonService.isEmail(email)){
                 var postData = {
                    "email" : email,
                    "source": window.location.hostname
                };
                apiService.post(moduleConfig.forgotPasswordURL, postData, null, true).then(_successHandler, _errorHandler);
                function _successHandler(response){                 // jshint ignore:line
                    utils.trackEvent('Email_Submit', 'Forgot_Password', 'Success');
                    $(moduleEl).find(TEMPLATES.FORGOT_PASSWORD + SUCCESS_ERROR_SELECTOR).html("a link to reset password has been sent to your email id");
                    setTimeout(closeLogin,2000);
                }
                function _errorHandler(){   // jshint ignore:line
                    utils.trackEvent('Email_Submit', 'Forgot_Password', 'Failure_sorry! you are not a registered user.');
                    $(moduleEl).find(TEMPLATES.FORGOT_PASSWORD + SUCCESS_ERROR_SELECTOR).html("Sorry! you are not a registered user.");
                }
            }
            else{
                utils.trackEvent('Reset_password', ERROR_CATEGORY, 'please provide a valid email');
                $(moduleEl).find(TEMPLATES.FORGOT_PASSWORD + ' .js-email-error').addClass('active');
                setTimeout(function(){
                    $(moduleEl).find(TEMPLATES.FORGOT_PASSWORD + ' .js-email-error').removeClass('active');
                },3000);
            }
        };

        
        
        return {
            behaviors: ['socialBehavior'],
            init: function() {
                moduleEl = context.getElement();
                moduleConfig = context.getConfig() || {};
                moduleEl = context.getElement();
                $ = context.getGlobal('jQuery');
                commonService = context.getService('CommonService');
                loginService = context.getService('LoginService');
                apiService = context.getService('ApiService');
                utils = context.getService('Utils');
                moduleConfig = $.extend(defaultConfig, moduleConfig);
                loginService.setBaseURL(moduleConfig.baseURL);
                $moduleEl = $(moduleEl);
                Application = Box.Application;
                html = doT.template(html)({
                    "sellerMicrositeUrl": moduleConfig.sellerMicrositeUrl
                });
                $moduleEl.append(html);
                changeTemplate(TEMPLATES.SOCIAL_LOGIN);
                bindControlEvents();
                context.broadcast('moduleLoaded', {
                    name: 'login'
                });
                context.broadcast('popup:open', {
                    'id': moduleEl.id
                });
                bindBlurBasicLoginScreen();
            },

            destroy: function() {
                moduleEl = null;
                var configScript = $moduleEl.find('script[type="text/x-config"]')[0];
                $moduleEl.empty();
                $moduleEl.html(configScript);
                commonService = null;
                loginService = null;
                apiService = null;
                userData = {};
                unBindBlurEvents();
            },
            onkeydown: function(event, element, elementType) {
                switch(elementType){
                    case "inputBox":
                        var key = event.keyCode || event.which;
                        if(key === 13){
                            checkAndLogin();
                            break;
                        }
                        break;    
                }
            },
            onclick: function(event, element, elementType) {
                pageType = utils.getTrackPageCategory();
                let pageCategory = 'Buyer_' + pageType;
                switch (elementType) {
                    case 'basic-login':
                        utils.trackEvent('Login_email', 'Login', pageCategory);
                        changeTemplate(TEMPLATES.MAIN_LOGIN,TEMPLATES.SOCIAL_LOGIN);
                        break;
                    case 'login-btn':
                        utils.trackEvent('LOGIN_Personal info', pageCategory, 'Login');
                        checkAndLogin();
                        break;
                    case 'forgot-password':
                        utils.trackEvent('LOGIN_Personal info', pageCategory, 'Forget_Password');
                        changeTemplate(TEMPLATES.FORGOT_PASSWORD,TEMPLATES.MAIN_LOGIN);
                        break;

                    case 'toggle-password':
                        utils.trackEvent('LOGIN_Personal info', pageCategory, 'Password_See');
                        $(element).closest('.login-input-box').find('input#password').
                        attr('type', function(index, attr){
                            return attr == 'password' ? 'text' : 'password';
                        });
                        break;
                    case 'forgot-submit':
                        utils.trackEvent('LOGIN_Personal info', pageCategory, 'Forget_Password_Submit');
                        var email = $moduleEl.find(TEMPLATES.FORGOT_PASSWORD + ' .js-email').val();
                        submitForReset(email);
                        break;
                    case 'backLogin':
                        utils.trackEvent('LOGIN_Personal info', pageCategory, 'Login_Back');
                       backLogin();
                        break;
                    case 'backForgotPassword':
                        utils.trackEvent('LOGIN_Personal info', pageCategory, 'Forget_Password_Back');
                        changeTemplate(TEMPLATES.MAIN_LOGIN,TEMPLATES.FORGOT_PASSWORD,true);
                        break;
                    case 'NewRegistration':
                        utils.trackEvent('SIGNUP_Personal Info', pageCategory, 'SignUp');
                        setRegister();
                        break;
                    case 'close-login':
                        utils.trackEvent('Login_closed', 'Login', pageCategory);
                        closeLogin();
                        break;
                    case 'fb-login':
                        utils.trackEvent('Login_facebook', 'Login', pageCategory);
                        break;
                    case 'gplus-login':
                        utils.trackEvent('Login_google', 'Login', pageCategory);
                        break;
                    case 'remember-me':
                        utils.trackEvent('LOGIN_Personal info', pageCategory, 'Remember_Me');
                        break;
                    case 'seller-login':
                        utils.trackEvent('Login_Seller', 'Login', pageCategory);
                        break;
                    }
                },
                messages: ['SocialLoggedIn','openLoginType','homeLoginUnload'],
                onmessage: function(name,data){
                    pageType = utils.getTrackPageCategory();
                    switch(name){
                        case 'SocialLoggedIn':
                            loginSuccess(data.data);
                            break;
                        case 'openLoginType':
                            utils.trackEvent('Form_open', 'Login', 'Buyer_' + pageType);
                            let loginType = data.type;
                            if(loginType && loginTypeStepMaping[loginType]) {
                                changeTemplate(loginTypeStepMaping[loginType].next,loginTypeStepMaping[loginType].prev,loginTypeStepMaping[loginType].back);
                            }
                            break;
                        case 'homeLoginUnload':
                            commonService.stopModule(moduleEl);
                        }
                    }
            };
    });
});
