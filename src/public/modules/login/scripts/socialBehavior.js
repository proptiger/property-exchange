"use strict";
define([
    "services/apiService",
    "services/loginService"
], function() {
    Box.Application.addBehavior('socialBehavior', function(context) {

        var config;
        var loginService = Box.Application.getService('LoginService');
        var moduleEl, $, $moduleEl;

        function internalLoginCall(provider, token) { //jshint ignore:line
            var loginData = {
                'provider': provider,
                'token': token
            };
            loginService.doSocialLogin(loginData).then(_successHandler, _errorHandler);

            function _successHandler(data) {
                console.log('logged in successfully using ' + provider);
                // var data = JSON.parse(response);
                loginService.setUserProp('id', data.data.id);
                loginService.setUserProp('fullName', data.data.firstName);
                loginService.setUserProp('profileImg', data.data.profileImageUrl);
                loginService.setUserProp('loginProvider', provider);
                Box.Application.broadcast('SocialLoggedIn', {
                    data: data,
                    provider:provider
                });
            }

            function _errorHandler(error) {
                console.log('error occurred');
                console.log(error);
            }
        }
        var showLoader = function() {
            $(".main-loading.box-loading").removeClass('hide');
        };
        var hideLoader = function() {
            $(".main-loading.box-loading").addClass('hide');
        };
        var fbLogin = (function() {
            var loginCallback = function(response) {
                // The response object is returned with a status field that lets the
                // app know the current login status of the person.
                // Full docs on the response object can be found in the documentation
                // for FB.getLoginStatus().
                if (response.status === 'connected') {
                    // Logged into your app and Facebook.
                    internalLoginCall('facebook', response.authResponse.accessToken);
                } else if (response.status === 'not_authorized') {
                    // The person is logged into Facebook, but not your app.
                } else {
                    // The person is not logged into Facebook, so we're not sure if
                    // they are logged into this app or not.
                }
            };
            return {
                load: function() {
                    $.getScript('//connect.facebook.net/en_US/sdk.js', function() {
                        FB.init({
                            appId: config.FBAppId,
                            cookie: true, // enable cookies to allow the server to access
                            // the session
                            xfbml: true, // parse social plugins on this page
                            version: 'v2.5' // use version 2.2
                        });

                    });
                },
                login: function() {
                    showLoader();
                    var FBrepeater = setInterval(function() {
                        if (FB) {
                            clearInterval(FBrepeater);
                            hideLoader();
                            FB.login(function(response) {
                                loginCallback(response);
                            }, {
                                scope: 'email,user_about_me'
                            });
                        }
                    }, 1000);


                },
            };
        })();

        var gPlus = (function() {
            var auth2, googleUser;
            return {
                load: function() {
                    $.getScript("//plus.google.com/js/client:plusone.js", function() {
                        if (window.gapi.auth2 && window.gapi.auth2.getAuthInstance()) {
                            auth2 = window.gapi.auth2.getAuthInstance();
                        } else {
                            window.gapi.load('auth2', function() {
                                window.gapi.auth2.init({
                                    client_id: config.gPlusAppId + '.apps.googleusercontent.com',
                                    scope: 'profile',
                                    redirect_uri: config.gPlusRedirectUri
                                }).then(
                                    function() {
                                        auth2 = window.gapi.auth2.getAuthInstance();
                                    },
                                    function(error) {
                                        console.log(error);
                                    });
                            });
                        }
                    });
                },
                login: function() {
                    showLoader();
                    var that = this;
                    var gPlusrepeater = setInterval(function() {
                        if (auth2) {
                            clearInterval(gPlusrepeater);
                            hideLoader();
                            auth2.isSignedIn.listen(that.signInCallback);
                            auth2.then(that.signInCallback());
                            if (!auth2.isSignedIn.get()) {
                                auth2.signIn();
                            }
                        }
                    }, 1000);
                },
                signInCallback: function() {
                    var authResult = window.gapi.auth2.getAuthInstance();
                    console.log(authResult);
                    if (auth2.isSignedIn.get()) {
                        googleUser = auth2.currentUser.get();
                        var token = googleUser.getAuthResponse().access_token;
                        internalLoginCall('google', token);
                    }
                }
            };
        })();

        function initializeSocialLogin() {
            fbLogin.load();
            gPlus.load();
        }

        return {
            messages: [],
            init: function() {
                moduleEl = context.getElement();
                config = context.getConfig();
                $ = context.getGlobal('jQuery');
                config = $.extend({}, config);
                $moduleEl = $(moduleEl);
                if (!config) {
                    config = {};
                }
                if (!config.loginURL) {
                    config.loginURL = '/xhr/userService/doLogin';
                }
                initializeSocialLogin();
            },
            destroy: function() {
                moduleEl = null;
                $ = null;
            },
            onmessage: function(name) {
                switch (name) {
                    case '':

                        break;
                }
            },
            onclick: function(event, element, elementType) {
                switch (elementType) {
                    case 'fb-login':
                        fbLogin.login();
                        break;
                    case 'gplus-login':
                        gPlus.login();
                        break;
                }
            }
        };
    });
});