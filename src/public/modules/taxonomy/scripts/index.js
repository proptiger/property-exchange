"use strict";
define([
    'services/commonService',
    'modules/taxonomy/scripts/behaviors/tracking'
], (CommonService) => {
    Box.Application.addModule('taxonomy', (context) => {
             
        var messages = [],
            moduleEl, $moduleEl, configData;  

        function init() {
            //initialization of all local variables
            moduleEl = context.getElement();
            $ = context.getGlobal('jQuery');
            $moduleEl = $(moduleEl);
            configData = context.getConfig();
        }

        function destroy() {
            //clear all the binding and objects
        }

        function onmessage() {
        }

        function onclick(event, element, elementType) {
            // bind custom messages/events
            let dataset = $(element).data() || {};
            switch (elementType) {
                case 'quick-links':
                case 'prop-link':
                case 'list-element':
                    var data = {};
                    data.label = configData.label;
                    data.element = element;
                    data.category = configData.category;
                    data.name = $(element).find('.txt-title').text();
                    context.broadcast('quicklinks_clicked', data);
                    break;
                case 'navigate':
                    if(dataset.url){
                        CommonService.ajaxify(dataset.url);
                    }
                    break;
            }
        }

        return {
            init,
            messages,
            behaviors : ['taxonomyTracking'],
            onmessage,
            onclick,
            destroy
        };
    });
});
