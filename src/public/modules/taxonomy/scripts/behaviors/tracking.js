'use strict';
define([
    'common/trackingConfigService',
    'services/trackingService'
], function(t, trackingService) {
    Box.Application.addBehavior('taxonomyTracking', function() {
        
        const QUIKLINKS_CLICKED = 'quicklinks_clicked';

        var messages = [QUIKLINKS_CLICKED];

        var onmessage = function(type, data) {
            let event,
                sourceModule = t[data.sourceModule], 
                category = data.category || t.DESCRIPTION_CATEGORY,
                label = data.label || t.QUICK_LINKS_LABEL,
                name = data.name;
            switch (type) {
                case QUIKLINKS_CLICKED:
                    event = t.CLICK_EVENT;
                    break;
            }

            let properties = {};
            properties[t.CATEGORY_KEY] = category;
            properties[t.SOURCE_MODULE_KEY] = sourceModule;
            properties.name = name;
            properties[t.LABEL_KEY] = label;

            trackingService.trackEvent(event, properties);
        };

        return {
            messages: messages,
            onmessage: onmessage,
            init: function() {},
            destroy: function() {}
        };
    });
});
