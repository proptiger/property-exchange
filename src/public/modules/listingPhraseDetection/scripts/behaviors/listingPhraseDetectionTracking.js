define([
    'services/trackingService',
    'common/trackingConfigService',
    'services/utils',
    'services/commonService'
], function (trackingService, t, utils, commonService) {
    'use strict';
    const BEHAVIOR_NAME = 'listingPhraseDetectionTracking';
    Box.Application.addBehavior(BEHAVIOR_NAME, (context) => {
        let moduleEl, $ ,listingsTracked = [];
        const LISTING_IN_VIEW_MESSAGE = 'phraseDetectionListingsInView';

        //function for building the trackingObj and calling the trackEvent of tracking Service
        let _trackEvent = function (event, label, dataset) {
            var trackingObj = {};
            trackingObj[t.CATEGORY_KEY] = 'Phrase';
            trackingObj[t.LABEL_KEY] = label;
            trackingObj[t.LISTING_ID_KEY] = dataset.listingId;
            trackingObj[t.SECTION_CLICKED]= dataset.sectionClicked;
            if(event == "Open"){
                trackingObj[t.CATEGORY_KEY] = 'Lead Form';
            }
            trackingService.trackEvent(event, trackingObj);
        };

        let bindElementForViewport = function () {
            let container = moduleEl.querySelector("[listings-phrase-detection-placeholder]");
            let elements = container.querySelectorAll("[data-type='list-element']");
            utils.trackElementInView(elements,".js-listing-phrase",'',LISTING_IN_VIEW_MESSAGE, true);
        };

        return {
            messages: ['phrase_moreLessClicked', 'phrase_listingCardClicked', 'phrase_ConnectClicked', 'serpPageScrolled', 'Phrase_listingsRendered', LISTING_IN_VIEW_MESSAGE],
            init: function (){
                moduleEl = context.getElement();
                $ = context.getGlobal('jQuery');
            },
            destroy: function () {
                let container = moduleEl.querySelector("[listings-phrase-detection-placeholder]");
                let elements = container.querySelectorAll("[data-type='list-element']");
                if(commonService && elements && elements.length){
                    commonService.unobserveDomElement(elements);
                }
                commonService = listingsTracked = null;
                moduleEl = null;
            },
            onmessage: function (msgName, data) {
                switch(msgName) {
                    case 'phrase_moreLessClicked':
                        _trackEvent('click', `seeMoreBtn_${data.dataset.seeMoreState}`, data.dataset);
                        break;
                    case 'phrase_listingCardClicked':
                            if(data && data.dataset){
                                data.dataset.sectionClicked = 'phraseDetectionListing';
                            }
                        _trackEvent('click', 'Property_Redirection', data.dataset);
                        break;
                    case 'phrase_ConnectClicked':
                            if(data && data.dataset){
                                data.dataset.sectionClicked = 'phraseDetectionConnectNow';
                            }
                            _trackEvent('Open', 'Phrase_SERP', data.dataset);
                        break;
                    case 'Phrase_listingsRendered':
                        bindElementForViewport();
                        break;
                    case LISTING_IN_VIEW_MESSAGE:
                        let dataElements = data.elements || [],target,dataset;
                        dataElements.forEach(element => {
                            target = element.target || element;
                            dataset = $(target).data()||{};
                            if ($(target).hasClass('js-listing-phrase') && dataset && listingsTracked.indexOf(dataset.listingId) == -1){
                                listingsTracked.push(dataset.listingId);
                                _trackEvent('Card seen', dataset.listingId, {listingId: dataset.listingId});
                            }
                        });
                        break;
                }
            }
        };
    });
});