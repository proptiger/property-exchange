define([
    'modules/listingPhraseDetection/scripts/behaviors/listingPhraseDetectionTracking',
    'doT!modules/listingPhraseDetection/views/index',
    'services/commonService',
    'services/filterConfigService',
    'services/apiService',
    'services/urlService',
    'common/sharedConfig',
    'services/utils'
], function (listingPhraseDetectionTracking, template, commonService, filterConfigService, apiService, urlService, sharedConfig, utils){
    const MODULE_NAME = "listingPhraseDetection";
    Box.Application.addModule(MODULE_NAME, (context) => {
        'use strict';

        let moduleEl, $, $moduleEl, moduleConfig, moduleData = [], isMobile;

        const config = {
            SELECTOR:{
                TEMPLATE_PLACEHOLDER: "[listings-phrase-detection-placeholder]",
                DETAILED_DESC_CONTAINER: "[data-detailed-desc]",
                PHRASE_LIST_CONTAINER: "[data-phrases-list]",
                MORE_LESS_BUTTON: "[data-type='more-btn']",
                IMAGE_CONTAINER: ".img-wrap"
            },
            CLASSES: {
                HIDE: "hide",
                SMALL_IMG: "small-image",
                FADEOUT: "fadeOut"
            }
        }

        /* function to prepare config object for lead form on serp page.
            @param {object} dataset : module configration object.
            @param {object} moduleData : module's global object holding the current set of listings.
            @return {object} : config object for lead form.
        */
        function _getLeadFromData(dataset = {}) {
            let configData = moduleData.find(item => {
                return item.listingId == dataset.listingId;
            }).configData;

           let rawData = $.extend(true,{},configData) || {};
            rawData = $.extend(rawData,dataset);
            if(dataset.step) {
                rawData.step = dataset.step;
            }
            if(rawData.propertyType) {
                rawData.propertyTypeLabel = rawData.propertyType;
            }
            if(rawData.unitTypeId) {
                rawData.propertyType = [rawData.unitTypeId];
            }
            if(rawData.price) {
                rawData.budget = rawData.maxBudget = rawData.minBudget = rawData.price;
            }
            rawData.bhk = rawData.bedrooms;
            rawData.listingId = rawData.id;
            rawData.salesType = rawData.listingCategory;
            rawData.id = "lead-popup";
            rawData.moduleId = moduleEl.id;
            return rawData;
        }

        function _openLeadForm(dataset = {}) {
            let rawData = _getLeadFromData(dataset);
            context.broadcast('leadform:open', rawData);
        }

        function _handleSeeMoreClick(element, data){
            let ele = $(element),
                parentElement = ele.closest('li'),
                isLess = ele.data("less"),
                newText = '';
            ele.data("less", !isLess);
            newText = isLess ? "- See Less" : "+ See More";
            parentElement.find(config.SELECTOR.DETAILED_DESC_CONTAINER)[`${isLess ? 'removeClass' : 'addClass'}`](config.CLASSES.FADEOUT);
            parentElement.find(config.SELECTOR.PHRASE_LIST_CONTAINER)[`${isLess ? 'addClass' : 'removeClass'}`](config.CLASSES.FADEOUT);
            !isMobile && parentElement.find(config.SELECTOR.IMAGE_CONTAINER)[`${isLess ? 'addClass' : 'removeClass'}`](config.CLASSES.SMALL_IMG)
            ele.html(newText);
            data.dataset.seeMoreState = isLess ? "open" : "close";
            context.broadcast('phrase_moreLessClicked', data);
        }


        return {
            behaviors: ['listingPhraseDetectionTracking'],
            init: function () {
                moduleEl = context.getElement();
                $ = context.getGlobal('jQuery');
                $moduleEl = $(moduleEl);
                moduleConfig = context.getConfig();
                isMobile = utils.isMobileRequest();
                moduleData = (moduleConfig.moduleData) || [];
                context.broadcast('moduleLoaded', {
                    name: MODULE_NAME,
                    id: moduleEl.id
                });
                context.broadcast("Phrase_listingsRendered", {moduleId: moduleEl.id});
            },
            destroy: function () {
                moduleEl = $ = moduleEl = moduleConfig = moduleData = null;
            },
            onclick: function (event, element, elementType) {
                let dataset = element.dataset || {};
                dataset = { ...dataset, ...($(element).closest('li').data())};
                let data = {moduleId: moduleEl.id, dataset};
                event.stopPropagation();
                switch(elementType){
                    case 'more-btn':
                        _handleSeeMoreClick(element, data);
                        break;
                    case 'openLeadForm':
                        context.broadcast("phrase_ConnectClicked", data);
                        _openLeadForm(dataset);
                        break;
                    case 'list-element':
                        context.broadcast('phrase_listingCardClicked', data);
                        let overviewUrl = $(element).closest('li').data('overviewUrl');
                        urlService.ajaxyUrlChange(overviewUrl);
                        break;
                }

            }
        }
    });
});