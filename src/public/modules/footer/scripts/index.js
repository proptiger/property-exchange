"use strict";
define([
    'services/commonService',
    'services/utils',
    'services/urlService',
    'modules/footer/scripts/behaviors/footerTracking'
], (commonService) => {
    Box.Application.addModule('footer', (context) => {
        let moduleEl,
            seoOpen = false;

        function init() {
            //initialization of all local variables
            moduleEl = context.getElement();
            commonService.startAllModules(moduleEl);
        }

        function loadSlider() {
            context.broadcast('loadModule', {
                name: 'slider',
                loadId: 'seo-footer'
            });
        }

        function expandit(element) {
            var windowWidth = $(window).width();
            var expandsection = $(element).siblings('ul,div');
            var container = $(element.parentElement);
            if (container.hasClass("open") && windowWidth < 768) {
                expandsection.slideUp();
                container.removeClass("open");
            } else {
                expandsection.slideDown();
                container.addClass("open");
            }
        }

        function destroy() {
            //clear all the binding and objects
        }

        function onmessage (name, data) {
            switch(name){
                case "divExpansion_close":
                    if(data.moduleEl && data.moduleEl.id == 'seo-footer-expansion'){
                        seoOpen = !seoOpen;
                        // utils.trackEvent('Click_'+pageType+'_Footer_Arrow', pageCategory, seoOpen ? 'Open' : 'Close');
                        loadSlider();
                    }
                break;
            }
        }

        function onclick(event, element, elementType) {
            //let pageType = utils.getTrackPageCategory(),
                //pageCategory = 'Buyer_' + pageType,
               let dataset = $(element).data();
            // bind custom messages/events
            switch (elementType) {
                case 'email-us':
                    context.broadcast('trackEmailUsFooter', {label: dataset.trackLabel});
                    break;

                case 'footer-anchor':
                    context.broadcast('trackSocialLinksFooter', {label: dataset.trackLabel,linkName: dataset.linkName,linkType: dataset.linkType});
                    break;

                case 'networkSites':
                    context.broadcast('trackNetworkSitesFooter', {label: dataset.trackLabel});
                    break;

                case 'list-element':
                    context.broadcast('trackListElementFooter', {label: dataset.trackLabel});
                    break;
                case 'expand':
                    expandit(element);
                    break;
                case 'about-us':
                    context.broadcast('trackAboutUsFooter', {label: $(element).text(),linkName: $(element).text(),linkType: "Other"});
                    window.location.href='about-us';
                    break;
                case 'privacy':
                    context.broadcast('trackPrivacyFooter', {label: $(element).text(),linkName: $(element).text(),linkType: "Other"});
                    window.location.href='privacy-policy';
                    break;
                case 'contact-us':
                    context.broadcast('trackContactUsFooter', {label: $(element).text(),linkName: $(element).text(),linkType: "Other"});
                    window.location.href='contact-us';
                    break;
                case 'terms-conditions':
                    context.broadcast('trackTermsFooter', {label: $(element).text(),linkName: $(element).text(),linkType: "Other"});
                    window.location.href='terms';
                    break;
                case 'home-loan':
                    context.broadcast('trackHomeLoanFooter', {label: $(element).text(),linkName: $(element).text(),linkType: "Other"});
                    window.location.href='home-loan';
                    break;
                case 'price-trends':
                    context.broadcast('trackPriceTrendsFooter', {label: $(element).text(),linkName: $(element).text(),linkType: "Other"});
                    window.location.href='http://pricetrends.makaan.com';
                    break;
                case 'forums':
                    context.broadcast('trackForumsFooter', {label: $(element).text(),linkName: $(element).text(),linkType: "Other"});
                    window.location.href='http://forums.makaan.com/';
                    break;
                case 'media-resources':
                    context.broadcast('trackMediaResourcesFooter', {label: $(element).text(),linkName: $(element).text(),linkType: "Other"});
                    window.location.href='media-resources';
                    break;
            }
        }

        return {
            init,
            behaviors: ['footerTracking'],
            messages: ['divExpansion_close'],
            onmessage,
            onclick,
            destroy
        };
    });
});
