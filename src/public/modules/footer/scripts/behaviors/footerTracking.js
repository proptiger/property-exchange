define([
    'common/trackingConfigService',
    'services/trackingService'
], function(t, trackingService) {
    'use strict';
    Box.Application.addBehavior('footerTracking', function() {

        var messages = [
            'trackAboutUsFooter',
            'trackPrivacyFooter',
            'trackContactUsFooter',
            'trackTermsFooter',
            'trackPriceTrendsFooter',
            'trackForumsFooter',
            'trackEmailUsFooter',
            'trackNetworkSitesFooter',
            'trackSocialLinksFooter',
            'trackListElementFooter',
            'trackMediaResourcesFooter'
        ];

        var onmessage = function(name, data) {
            let event = t.CLICK_EVENT,
                category = t.FOOTER_CATEGORY,
                label, source,
                properties = {},
                linkName,
                linkType;

            switch (name) {
                case 'trackAboutUsFooter':
                case 'trackPrivacyFooter':
                case 'trackContactUsFooter':
                case 'trackTermsFooter':
                case 'trackHomeLoanFooter':
                case 'trackPriceTrendsFooter':
                case 'trackForumsFooter':
                case 'trackMediaResourcesFooter':
                    label = data.label;
                    source = t.QUICK_LINKS_FOOTER;
                    linkName = data.linkName;
                    linkType = data.linkType;
                    break;

                case 'trackEmailUsFooter':
                    label = data.label;
                    source = t.EMAIL_US_FOOTER;
                    break;

                case 'trackNetworkSitesFooter':
                    label = data.label;
                    source = t.NETWORK_SITES_FOOTER;
                    break;
                case 'trackSocialLinksFooter':
                    label = data.label;
                    source = t.OTHERS_FOOTER;
                    properties[t.SOCIAL_INTERACTION_KEY] = label;
                    linkName = data.linkName;
                    linkType = data.linkType;
                    break;
            }

            properties[t.CATEGORY_KEY] = category;
            properties[t.LABEL_KEY] = label;
            properties[t.SOURCE_MODULE_KEY] = source;
            properties[t.LINK_NAME_KEY] = linkName;
            properties[t.LINK_TYPE_KEY] = linkType;

            trackingService.trackEvent(event, properties);
        };

        return {
            messages: messages,
            onmessage: onmessage,
            init: function() {},
            destroy: function() {}
        };
    });
});
