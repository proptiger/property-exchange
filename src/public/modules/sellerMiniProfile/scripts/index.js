define([
    'doT!modules/sellerMiniProfile/views/index',
    'doT!modules/sellerMiniProfile/views/header',
    'services/utils',
    'services/commonService',
    'services/filterConfigService',
    'services/urlService',
    'modules/sellerMiniProfile/scripts/behaviors/sellerMiniProfileTracking'
    ], function(template, headerTemplate, utils, commonService, filterConfigService, urlService){
        'use strict';
        const moduleName = 'sellerMiniProfile';
        Box.Application.addModule(moduleName, (context) => {
            var moduleElem, $moduleEl, $, isMobileRequest, moduleConfig, currentlyViewSeller, seller, 
                sellerScoreId, ratingBarsId, sellerReviewModuleId, similarPropertiesId;
            let messages = ["loadSellerMiniProfile","detailedReviewsLoaded",'start_gallery'];
            const CLASSES = {
                    HIDE: "hide",
                    SHOW_RATING: "show-rating",
                    scroll:'on-scroll',
                    showMiniProfile: 'show-mini-profile',
                    dealMaker: 'deal-maker',
                    detailedReview: 'show-all-review'
                },
                MODULE_SELECTOR_CLASSES = {
                    'SELLER_SCORE': ".js-company-rating",
                    'RATING_COUNT': ".js-rating-count",
                    'RATING_HIDE_SHOW': "[data-hide-show]"
                },
                REVIEW_SELECTOR_CLASSES={
                    "REVIEW_CONTAINER"  :  "[data-review-container]",
                    "DETAILED_REVIEW_CONTAINER": "[data-detailed-review]",
                    'REVIEW_COUNT' : ".js-review-count"
                },
                PROFILE_HEADER_CONTAINER = "[data-header-container]",
                MINIPROFILE_CONTAINER = '[data-miniprofile-container]',
                PROFILE_RATING_CONTAINER = "[data-rating-container]",
                PROFILE_SCORE_CONTAINER = "[data-score-container]",
                MINIPROFILE_DATA_CONTAINER = "[data-miniprofile-data-container]",
                HEADER_CHILD = "[data-headerHolder]";
            let behaviors = ['sellerMiniProfileTracking'];

            function display(show=false){
                if(show){
                    isMobileRequest ? $('body').addClass('popup-open'): void 0;
                    $moduleEl.find(MINIPROFILE_CONTAINER).addClass(CLASSES.showMiniProfile);
                } else {
                    isMobileRequest ? $('body').removeClass('popup-open'): void 0;
                    $moduleEl.find(MINIPROFILE_CONTAINER).removeClass(CLASSES.showMiniProfile);
                }
            }

            function scrollToTop(){
                $moduleEl.find(MINIPROFILE_DATA_CONTAINER).scrollTop(0);
            }

            function handleProfileView(data){
                $moduleEl.find(PROFILE_HEADER_CONTAINER).addClass(CLASSES.HIDE);
                $moduleEl.find(PROFILE_HEADER_CONTAINER).html(headerTemplate(data));
                $moduleEl.find(PROFILE_HEADER_CONTAINER).removeClass(CLASSES.HIDE);

                $moduleEl.find(PROFILE_RATING_CONTAINER).addClass(CLASSES.HIDE);

                $moduleEl.find(REVIEW_SELECTOR_CLASSES.REVIEW_CONTAINER).addClass(CLASSES.HIDE);
                $moduleEl.find(MINIPROFILE_CONTAINER).removeClass(CLASSES.detailedReview);
                
                $moduleEl.find(MINIPROFILE_CONTAINER).removeClass(CLASSES.scroll);
                $moduleEl.find(MINIPROFILE_CONTAINER).removeClass(CLASSES.dealMaker);
                
                if(data.reviewCount){
                    let reviewHeading = data.reviewCount >1 ? 'REVIEWS' : 'REVIEW';
                    $moduleEl.find(REVIEW_SELECTOR_CLASSES.REVIEW_COUNT).html(`${data.reviewCount} ${reviewHeading}`);
                    $moduleEl.find(REVIEW_SELECTOR_CLASSES.REVIEW_CONTAINER).removeClass(CLASSES.HIDE);
                }
                if(data.ratingCount){
                    $moduleEl.find(MODULE_SELECTOR_CLASSES.RATING_COUNT).html(`${data.ratingCount} ${data.ratingCount > 1 ? "RATINGS" : "RATING"}`);
                    $moduleEl.find(PROFILE_RATING_CONTAINER).removeClass(CLASSES.HIDE); 
                }
                if(data.sellerTransactionStatus.isDealMaker){
                    $moduleEl.find(MINIPROFILE_CONTAINER).addClass(CLASSES.dealMaker);
                }
                scrollToTop();
            }

            function renderSellerProfile(data){
                handleProfileView(data);
                display(true);

                sellerScoreId = sellerScoreId || moduleElem.querySelector('[data-module="sellerScore"]').id;

                commonService.bindOnModuleLoad('sellerScore', ()=>{
                    context.broadcast('reloadSellerScore', {
                        listingUserId: data.companyUserId,
                        companyRating: data.companyRating,
                        id: sellerScoreId
                    });
                },[sellerScoreId]);

                ratingBarsId = ratingBarsId || moduleElem.querySelector('[data-module="ratingBars"]').id;

                commonService.bindOnModuleLoad('ratingBars', ()=>{
                    context.broadcast('reloadRatingBars', {
                        ratingCount: data.ratingCount,
                        listingUserCompanyId: data.companyId,
                        listingUserCompanyUserId: data.companyUserId,
                        id: ratingBarsId
                    });
                },[ratingBarsId]);

                sellerReviewModuleId = sellerReviewModuleId || moduleElem.querySelector('[data-module="sellerReviews"]').id;

                if(data.reviewCount){
                    commonService.bindOnModuleLoad('sellerReviews', ()=>{
                        if(currentlyViewSeller.reviewCount){
                            context.broadcast('loadSellerReviews', {
                                companyId: data.companyId,
                                companyUserId: data.companyUserId,
                                companyName: data.companyName,
                                id: sellerReviewModuleId
                            });
                        }
                    },[sellerReviewModuleId]);
                }

                similarPropertiesId = similarPropertiesId || moduleElem.querySelector('[data-module="similarProperties"]').id;
                commonService.bindOnModuleLoad('similarProperties', ()=>{
                    context.broadcast('reloadSimilarProperties', {
                        id: similarPropertiesId,
                        queryParams: getDataForSimilarProperties()
                    });
                },[similarPropertiesId]);
            }

            function _showSellerListings(data){
                let url;
                if ( (seller.type == "AGENT" && seller.id) || (seller.type == "BUILDER" && (seller.builderId||seller.id)) ) {
                    seller.listingType = utils.getPageData('listingType') || "buy";
                    url = filterConfigService.getSellerListingUrl(seller, false);
                    urlService.ajaxyUrlChange(url);
                }
            }

            function getDataForSimilarProperties(){
                let url, queryParams;
                if ( (seller.type == "AGENT" && seller.id) || (seller.type == "BUILDER" && (seller.builderId||seller.id)) ) {
                    seller.listingType = utils.getPageData('listingType') || "buy";
                    url = filterConfigService.getSellerListingUrl(seller, false);
                    queryParams = url.split('?')[1];
                    return queryParams;
                }
            }

            function addDetailedReviews(detailedReviews){
                $moduleEl.find(REVIEW_SELECTOR_CLASSES.DETAILED_REVIEW_CONTAINER).html(detailedReviews);
                $moduleEl.find(MINIPROFILE_CONTAINER).addClass(CLASSES.detailedReview);
                $moduleEl.find(MINIPROFILE_CONTAINER).addClass(CLASSES.scroll);
            }

            function _scrollFunction(){
                let hasScroll = $moduleEl.find(MINIPROFILE_CONTAINER).hasClass(CLASSES.scroll);
                let scroll = $moduleEl.find(MINIPROFILE_DATA_CONTAINER).scrollTop();
                let hasClassReview = $moduleEl.find(MINIPROFILE_CONTAINER).hasClass(CLASSES.detailedReview);
                let headerParentHeight = $moduleEl.find(PROFILE_HEADER_CONTAINER).height();
                let headerChildHeight = 44;
                let threshold = headerParentHeight - headerChildHeight - 35;

                if(scroll >= threshold && !hasScroll){
                    $moduleEl.find(MINIPROFILE_CONTAINER).addClass(CLASSES.scroll);
                }
                else if(scroll < threshold && hasScroll && !hasClassReview){
                    $moduleEl.find(MINIPROFILE_CONTAINER).removeClass(CLASSES.scroll);  
                }
            }

            function trackMiniProfileClicks(message){
                context.broadcast( message,{
                    id: moduleElem.id,
                    sellerId: currentlyViewSeller.companyId,
                    sellerStatus: currentlyViewSeller.isPaidSeller,
                    sellerRating: currentlyViewSeller.ratingCount
                }); 
            }

            return {
                init: function(){
                    moduleElem = context.getElement();
                    $ = context.getGlobal('jQuery');
                    $moduleEl = $(moduleElem);
                    moduleConfig = context.getConfig() || {};
                    isMobileRequest = utils.isMobileRequest();
                    $moduleEl.html(template({}));
                    commonService.startAllModules(moduleElem);
                    $moduleEl.find(MINIPROFILE_DATA_CONTAINER).on('scroll', _scrollFunction);
                    context.broadcast('moduleLoaded', {
                        'name': moduleName,
                        'id': moduleElem.id
                    });
                },
                destroy: function(){
                    $moduleEl = null;
                    moduleElem = null;
                    $ = null;
                    isMobileRequest = null;
                    moduleConfig = null; 
                },
                messages,
                behaviors,
                onmessage: function(name, data){
                    switch(name){
                        case "loadSellerMiniProfile": 
                            currentlyViewSeller = data || {};
                            seller = {
                                id: data.companyId,
                                sellerUserId: data.companyUserId,
                                name: data.companyName,
                                type: data.companyType ? data.companyType.toUpperCase() : '',
                                builderId: data.builderId,
                                builderName: data.builderName
                            };
                            renderSellerProfile(currentlyViewSeller);
                            break;
                        case "detailedReviewsLoaded":
                            if(data.id != sellerReviewModuleId) return;
                            addDetailedReviews(data.detailedReviews);
                            break;
                        case "start_gallery":
                            display(false);
                            break;
                    }
                },
                onclick: function(event, element, elementType){
                    switch(elementType){
                        case "profileClose":
                            display(false);
                            break;
                        case "showHideRatingsDetails":
                            $moduleEl.find(PROFILE_RATING_CONTAINER).toggleClass(CLASSES.SHOW_RATING);
                            if($moduleEl.find(PROFILE_RATING_CONTAINER).hasClass(CLASSES.SHOW_RATING)){
                                trackMiniProfileClicks('trackSeeAllRatings');
                                $moduleEl.find(MODULE_SELECTOR_CLASSES.RATING_HIDE_SHOW).html("hide Details");
                            } else {
                                $moduleEl.find(MODULE_SELECTOR_CLASSES.RATING_HIDE_SHOW).html("show Details");
                            }
                            break;
                        case "showMatchingProperties":
                            trackMiniProfileClicks('trackSeeAllProperties');
                            _showSellerListings(currentlyViewSeller);
                            break;
                        case "view-profile":
                              trackMiniProfileClicks('trackProfileClick');
                            _showSellerListings(currentlyViewSeller);
                            break;
                        case "see-all-reviews":
                            trackMiniProfileClicks('trackSeeAllReviews');
                             sellerReviewModuleId = sellerReviewModuleId || moduleElem.querySelector('[data-module="sellerReviews"]').id;
                             context.broadcast('loadDetailedReviews',{id:sellerReviewModuleId,sellerData:currentlyViewSeller});
                             break;
                        case 'review-back':
                            $moduleEl.find(MINIPROFILE_CONTAINER).removeClass(CLASSES.detailedReview);
                            $moduleEl.find(MINIPROFILE_CONTAINER).removeClass(CLASSES.scroll);
                            break;
                        case 'connect-seller':
                            trackMiniProfileClicks('trackConnectClick');
                            if(currentlyViewSeller.leadFormData){
                                let leadFormData = $.extend(true,{},currentlyViewSeller.leadFormData);
                                leadFormData.source = 'SELLER_MINI_PROFILE';
                                context.broadcast('leadform:open', leadFormData);
                            }
                            break;
                    }
                }
            }
        });
    })