define([
    'common/trackingConfigService',
    'services/trackingService'
], function(t, trackingService) {
    'use strict';
    Box.Application.addBehavior('sellerMiniProfileTracking', function(context) {

        var element;
        const messages = [
            'trackProfileClick',
            'trackConnectClick',
            'trackSeeAllReviews',
            'trackSeeAllRatings',
            'trackSeeAllProperties'

        ];

        var init = function() {
            element = context.getElement();
        };

        var onmessage = function(name, data) {
            if (data.id === element.id) {
                let properties = {};
                let event,category, label, sellerId, sellerRating, sellerStatus,sectionClicked;
                sellerId = data.sellerId;
                sellerStatus = data.sellerStatus?t.SELLER_PAID_STATUS.PAID:t.SELLER_PAID_STATUS.NOT_PAID;
                sellerRating = data.sellerRating;
                switch (name) {
                    case 'trackProfileClick':
                        event = t.CLICK_EVENT;
                        category="Revamp_Buyer_Ratings";
                        label = 'RHS_Open_Seller Profile';
                        sectionClicked = 'viewProfileFromMiniProfile';
                        break;
                     case 'trackConnectClick':
                        event = t.OPEN_EVENT;
                        category = t["LEAD_FORM_CATEGORY"];
                        label='Seller Mini profile';
                        sectionClicked = 'miniProfileConnectNow';
                        break;
                     case 'trackSeeAllReviews':
                        event = "Reviews Modal Open";
                        category = "Revamp_Buyer_Ratings";
                        label="Mini Profile";
                        break;
                     case 'trackSeeAllRatings':
                        event = t.CLICK_EVENT;
                        category = "Revamp_Buyer_Ratings";
                        label='RHS Ratings expand';
                        break;
                    case 'trackSeeAllProperties':
                        event = t.CLICK_EVENT;
                        category = "Revamp_Buyer_Ratings";
                        label=' RHS Matching Properties';
                        break;
                    default:
                        return;
                }

                properties[t.CATEGORY_KEY] = category;
                if(label){
                    properties[t.LABEL_KEY] = label;
                }
                if(sellerId){
                    properties[t.SELLER_ID_KEY] = sellerId;
                }
                if(sellerRating){
                    properties[t.SELLER_SCORE_KEY] = sellerRating;
                }
                if(sellerStatus){
                    properties[t.SELLER_STATUS_KEY] = sellerStatus;
                }
                properties[t.SECTION_CLICKED] = sectionClicked;
                trackingService.trackEvent(event, properties);
            }
        };

        return {
            messages,
            onmessage,
            init,
            destroy: function() {}
        };
    });
});