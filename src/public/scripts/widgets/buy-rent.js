"use strict";
(function() {
    function triggerCustomEvent(el, name, message) {
        el.trigger({
            type: name,
            message: message || "",
            time: new Date()
        });
    }


    function Typeahead(element) {
        var config = {
            api: function(query) {
                return "/columbus/app/v5/typeahead?query=" + query + "&category=buy&view=buyer&sourceDomain=Makaan";
            },
            el: $(element),
            inputEl: $('input', $(element)),
            errorEl: $('[data-error]', $(element)),
            suggestion: $('[data-suggestion]', $(element)),
            selectedSuggestion: function() {
                return $('[data-suggestion] li.selected', $(element));
            },
            suggestionList: function() {
                return $('[data-suggestion] li', $(element));
            },
            classes: {
                selected: 'selected'
            },
            keys: {
                UP: 38,
                DOWN: 40,
                ENTER: 13,
                ESC: 27,
                BACK: 8
            },
            result: {}
        };
        var getResultHTML = function(data) {
            data = data || {};
            return "<li data-details=" + encodeURI(JSON.stringify(data)) + ">" + data.displayText + "</li>";
        };
        var resultPromise;
        var parseResult = function(data) {
            var result = [];
            var html = '';
            $.each(data, function(key, val) {
                var temp = {};
                val.type = val.type && val.type.toLowerCase();
                if (val.type == 'city' || val.type == 'locality') {
                    temp.type = val.type;
                    temp.idType = val.type + 'Id';
                    temp.id = val.entityId;
                    temp.url = val.redirectUrl;
                    temp.displayText = val.displayText;
                    html += getResultHTML(temp);
                    result.push(temp);
                }
            });
            return {
                html: html,
                data: result
            };
        };
        var cleanUp = function() {
            config.suggestion.html('');
        };
        var hideError = function() {
            config.errorEl.text('');
        };
        var selectValue = function() {
            var $this = $(this);
            config.inputEl.val($this.text()).data('details', $this.data('details'));
            cleanUp();
            triggerCustomEvent(config.el, 'typeahead-value-selected');
        };
        var getValue = function() {
            return {
                data: config.inputEl.val() && config.inputEl.data('details'),
                value: config.inputEl.val()
            };
        };
        var abortPreviousCall = function() {
            resultPromise && resultPromise.abort();  //jshint ignore:line
            resultPromise = null;
        };
        var navigateResults = function(direction) {
            var currentIndex = config.selectedSuggestion().index();
            var totalLength = config.suggestionList().length;
            if (direction) {
                currentIndex = (currentIndex + 1) % totalLength;
            } else {
                currentIndex = currentIndex - 1;
                currentIndex = currentIndex - 1 <= 0 ? totalLength - 1 : currentIndex;
            }
            config.suggestionList().removeClass(config.classes.selected);
            config.suggestionList().eq(currentIndex).addClass(config.classes.selected);
        };
        var triggerSelection = function() {
            config.selectedSuggestion().trigger('click');
        };

        var getTypeaheadResult = function(event, query) {
            hideError();
            abortPreviousCall();
            query = query || getValue().value;
            resultPromise = $.ajax(config.api(query));
            resultPromise.then(function(response) {
                var pasedData = {};
                response = response && response.data;
                pasedData = parseResult(response);
                config.suggestion.html(pasedData.html);
            });
        };
        var handleKeys = function(event) {
            var keycode = event.keyCode;
            if (keycode == config.keys["UP"]) {
                navigateResults(-1);
            } else if (keycode == config.keys["DOWN"]) {
                navigateResults(1);
            } else if (keycode == config.keys["ENTER"]) {
                triggerSelection();
            } else if (keycode == config.keys["ESC"]) {
                cleanUp();
            } else {
                config.inputEl.data('details', null);
                getTypeaheadResult(event);
            }
        };
        
        var focus = function(){
            config.inputEl.focus();
        };
        this.getValue = getValue;
        this.focus = focus;
        config.inputEl.on('keyup', handleKeys);
        config.suggestion.on('click', 'li', selectValue);
    }

    function buyRent() {
        var $widget = $('[data-makaan-widget="buy-rent"]');
        var selector = {
            step: '[data-step]',
            substep: '[data-sub-step]',
            userRent: 'input[data-rent]',
            button: '[data-button-type]',
            typeahead: '[data-typeahead]',
            error: '[data-error]',
            success: '[data-success]',
            selectedBHK: 'input[name="bhk"]:checked',
            suggestedButton: '[data-button-type] span.suggested',

        };
        var config = {
            classes: {
                hide: 'hide',
                selected: 'selected',
                left: 'left-side',
                right: 'right-side',
                notVisible: 'not-visible'
            },
            api: function(data) {
                return '/apis/widgets/buy-rent?' + data.entity.idType + '=' + data.entity.id + '&rent=' + data.userRent + '&bhk=' + data.bhk.join(',');
            },
            messages: {
                GENERIC: "some error occured",
                USER_INPUT_LOCATION: "please select your location",
                USER_INPUT_RENT: "please enter valid rent",
                PROCESSING: "processing..."
            },
            domain: "https://www.makaan.com",
            el: {
                steps: $(selector.step, $widget),
                getSubSteps: function(name) {
                    return $(selector.substep, $('[data-step="' + name + '"]'));
                },
                getParticularStep: function(name) {
                    return $('[data-step=' + name + ']', $widget);
                },
                getParticularSubStep: function(step, substep) {
                    return $('[data-step=' + step + ']', $widget).find('[data-sub-step=' + substep + ']');
                },
                getParticularButton: function(type) {
                    return $('[data-button-type=' + type + ']', $widget);
                },
                getSubStepButton: function(step, substep){
                    return $('[data-step='+step+'] [data-sub-step=' + substep + '] [data-button-type]', $widget);
                },
                getParticularSubStepWithIndex: function(step, index) {
                    return $('[data-step=' + step + ']', $widget).find('[data-index=' + index + ']');
                },
                getParticularSuggestedButton: function(type) {
                    return $('[data-button-type=' + type + '] span.suggested', $widget);
                },
                getResultDataPoint: function(point, $context) {
                    $context = $context || $widget;
                    return $('[data-point=' + point + ']', $context);
                },
                userRent: $(selector.userRent, $widget),
                suggestedButton: $(selector.suggestedButton, $widget),
                widget: $widget,
                input: $('input', $widget),
                button: $(selector.button, $widget),
                typeahead: $(selector.typeahead, $widget)
            }
        };
        var typeahead = new Typeahead(config.el.typeahead);

        var stepsData = {
            bhk: []
        };
        var showMessage = function(type, $el, msg) {
            if (type == "success") {
                $(selector.success, $el).text(msg);
            } else if (type == "error") {
                $(selector.error, $el).text(msg);
            }
        };
        var hideError = function($el) {
            showMessage('error', $el, '');
            showMessage('success', $el, '');
        };

        var hideMessage = hideError;

        var showError = function($el, key) {
            key = key || 'GENERIC';
            var msg = config.messages[key];
            showMessage('error', $el, msg);
            setTimeout(function() {
                hideError($el);
            }, 4000);
        };
         var highlightInput = function(name){
            switch(name){
                case "USER_INPUT_LOCATION":
                    //typeahead.focus();
                break;
                case "USER_INPUT_RENT":
                    config.el.userRent.focus();
                break;
            }
        }; 
        var changeStep = function(type, step, subStep) {
            var currentIndex;
            if (type == 'substep') {
                currentIndex = config.el.getParticularSubStep(step, subStep).data('index');
                config.el.getSubSteps(step).css('transform','translateX(-'+currentIndex * 100+'%)');
                setTimeout(function(){
                    highlightInput(subStep);
                },500);
            }
            if (type == 'step') {
                currentIndex = config.el.getParticularStep(step).data('index');
                $('.step-wrap').css('transform', 'translateX(-' + currentIndex * 100 + '%)');
                changeStep('substep', step, subStep);
            }
        };
        var createSERPUrl = function(category, data) {
            var url = config.domain + '/listings?beds=' + data.bhk.join(',') + '&listingType=' + category + '&' + data.entity.idType + '=' + data.entity.id;
            return url;
        };
        var getBuyRentSuggestion = function() {
            return $.ajax(config.api(stepsData));
        };
        var highlightButton = function(type, highlightSuggestion) {
            config.el.button.removeClass(config.classes.selected);
            config.el.getParticularButton(type).addClass(config.classes.selected);

            highlightSuggestion && config.el.suggestedButton.addClass(config.classes.hide);   //jshint ignore:line
            highlightSuggestion && config.el.getParticularSuggestedButton(type).removeClass(config.classes.hide);  //jshint ignore:line

        };
        var showHideButton = function(buttons, show) {
            if (show) {
                $.each(buttons, function(k, v) {
                    config.el.getParticularButton(v).removeClass(config.classes.hide);
                });
            } else {
                $.each(buttons, function(k, v) {
                    config.el.getParticularButton(v).addClass(config.classes.hide);
                });
            }

        };
        var updateResultHTML = function(data) {
            var buySubstep = config.el.getParticularSubStep('RESULT', 'RESULT_BUY');
            var rentSubstep = config.el.getParticularSubStep('RESULT', 'RESULT_RENT');
            var zeroSubstep = config.el.getParticularSubStep('RESULT', 'RESULT_ZERO');

            config.el.getResultDataPoint('location', buySubstep).text(stepsData.entity.displayText);
            config.el.getResultDataPoint('avg-emi', buySubstep).text(data.emi.buy.formattedAvgEmi);
            config.el.getResultDataPoint('affordable-emi', buySubstep).text(data.emi.buy.formattedAffordableEmi);
            config.el.getResultDataPoint('min-emi', buySubstep).text(data.emi.buy.formattedMinEmi);
            config.el.getResultDataPoint('max-emi', buySubstep).text(data.emi.buy.formattedMaxEmi);
            config.el.getResultDataPoint('count', buySubstep).text(data.count.buy);


            config.el.getResultDataPoint('location', rentSubstep).text(stepsData.entity.displayText);
            config.el.getResultDataPoint('avg-price', rentSubstep).text(data.price.rent.formattedAvgPrice);
            config.el.getResultDataPoint('affordable-emi', rentSubstep).text(data.emi.rent.formattedAffordableEmi);
            config.el.getResultDataPoint('min-price', rentSubstep).text(data.price.rent.formattedMinPrice);
            config.el.getResultDataPoint('max-price', rentSubstep).text(data.price.rent.formattedMaxPrice);
            config.el.getResultDataPoint('count', rentSubstep).text(data.count.rent);

            config.el.getResultDataPoint('affordable-emi', zeroSubstep).text(data.emi.rent.formattedAffordableEmi);

        };
       
        var moveNext = function(type, step) {

            switch (type) {
                case "USER_INPUT_LOCATION_SUBMIT":
                    changeStep('substep', step , 'USER_INPUT_RENT');
                    break;
                case "USER_INPUT_RENT_SUBMIT":
                    changeStep('substep', step, 'USER_INPUT_BHK');
                    break;
                case "USER_INPUT_BHK_SUBMIT":
                    var $el = config.el.getParticularStep(step);

                    showMessage('success', $el, config.messages['PROCESSING']);

                    getBuyRentSuggestion().then(function(suggestion) {
                        hideMessage($el);
                        updateResultHTML(suggestion);
                        if (suggestion.suggestion == 'buy') {
                            highlightButton('RESULT_BUY', true);
                            showHideButton(['RESULT_BUY', 'RESULT_RENT'], true);
                            changeStep('step', 'RESULT', 'RESULT_BUY');
                        } else if (suggestion.suggestion == 'rent') {
                            highlightButton('RESULT_RENT', true);
                            showHideButton(['RESULT_BUY', 'RESULT_RENT'], true);
                            changeStep('step', 'RESULT', 'RESULT_RENT');
                        } else {
                            showHideButton(['RESULT_BUY', 'RESULT_RENT']);
                            changeStep('step', 'RESULT', 'RESULT_ZERO');

                        }
                    }, () => {
                        showError(config.el.getParticularStep(step));
                    });
                    break;
                case "RESULT_RENT":
                    highlightButton(type);
                    changeStep('substep', 'RESULT', 'RESULT_RENT');
                    break;
                case "RESULT_BUY":
                    highlightButton(type);
                    changeStep('substep', 'RESULT', 'RESULT_BUY');
                    break;
                case "RESULT_RENT_EDIT":
                case "RESULT_BUY_EDIT":
                case "RESULT_ZERO_EDIT":
                    changeStep('step', 'USER_INPUT', 'USER_INPUT_LOCATION');
                    setTimeout(function(){
                        highlightInput('USER_INPUT_LOCATION');
                    },500);
                    break;
                case "RESULT_RENT_SUBMIT":
                    window.open(createSERPUrl('rent', stepsData));
                    break;
                case "RESULT_BUY_SUBMIT":
                    window.open(createSERPUrl('buy', stepsData));
                    break;
            }
            return true;
        };
        var populateData = function(step, substep) {
            var type = substep || step;
            var valid = true;
            switch (type) {
                case "USER_INPUT_LOCATION":
                    stepsData.entity = JSON.parse(decodeURI(typeahead.getValue().data));
                    valid = !!stepsData.entity;
                    break;
                case "USER_INPUT_RENT":
                    stepsData.userRent = config.el.userRent.val();
                    valid = !!stepsData.userRent;
                    break;
                case "USER_INPUT_BHK":
                    stepsData.bhk = $(selector.selectedBHK, config.el.widget).map(function() {
                        return this.value;
                    }).get();
                    break;
            }
            return valid;
        };
        var handleButton = function(event) {
            var target = $(event.currentTarget);
            var type = target.data('button-type');
            var stepEl = target.closest('[data-step]');
            var subStepEl = target.closest('[data-sub-step]');
            var index = target.closest('[data-index]');

            var step = stepEl.data('step');
            var subStep = subStepEl.data('sub-step');

            var valid = populateData(step, subStep);

            valid ? moveNext(type, step, subStep, index) : showError(stepEl, subStep || step);  //jshint ignore:line
        };

        var handleKeys = function(event) {
            var keycode = event.keyCode;
            var target = $(event.currentTarget);
            var substep = target.closest('[data-sub-step]').data('sub-step');
            var step = target.closest('[data-step]').data('step');
            if (keycode == 13) {
                config.el.getSubStepButton(step, substep).trigger('click');
            }
        };
        
        config.el.input.on('keyup',handleKeys);

        config.el.typeahead.on('typeahead-value-selected', function() {
            let step = "USER_INPUT";
            let subStep = "USER_INPUT_LOCATION";
            let stepEl = config.el.getParticularStep(step);
            (populateData(step, subStep) && moveNext("USER_INPUT_LOCATION_SUBMIT", step, subStep)) || showError(stepEl, subStep || step);  //jshint ignore:line
        });
        
        config.el.button.on('click', handleButton);
        
        highlightInput('USER_INPUT_LOCATION');


    }
    $(document).ready(buyRent);
})();
