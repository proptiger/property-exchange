define([
    'common/trackingConfigService',
    'services/trackingService'
], function(t, trackingService) {
    'use strict';
    Box.Application.addBehavior('semProjectTracking', function() {

        var messages = ['trackSemProjectTestimonialClick', 'trackSemProjectArrowClick'],
            event, category, label;

        var onmessage = function(name) {
            //let event, category, label, nonInteraction;
            switch (name) {
                case 'trackSemProjectTestimonialClick':
                    category = t.SEM_PAGES;
                    event = t.NAVIGATION_EVENT;
                    label = t.TESTIMONIAL;
                    break;
                case 'trackSemProjectArrowClick':
                    category = t.SEM_PAGES;
                    event = t.NAVIGATION_EVENT;
                    label = t.ARROW;
                    break;
            }

            let properties = {};
            properties[t.CATEGORY_KEY] = category;
            properties[t.LABEL_KEY] = label;
            if (event) {
                trackingService.trackEvent(event, properties);
            }
        };

        return {
            messages: messages,
            onmessage: onmessage,
            init: function() {},
            destroy: function() {}
        };
    });
});
