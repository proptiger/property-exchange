define([
    'common/trackingConfigService',
    'services/trackingService'
], function(t, trackingService) {
    'use strict';
    Box.Application.addBehavior('homeloanFlowTracking', function(context) {

        var element;
        const messages = [
            'trackPageLoaded',
            'trackHomeloanClick',
            'trackPropertyType',
            'trackSubmitOtp',
            'trackOtpVerified',
            'trackHomeLoanDashboard',
            'trackHomeLoanSubmit',
        ];

        var init = function() {
            element = context.getElement();
        };

        var onmessage = function(name, data) {
            if (data.id === element.id) {
                let properties = {};
                let event, category, label, value,page,sourcemodule;
                category = 'Lead Form';
                label = data.label || "Home Loan";
                switch (name) {
                    case 'trackPageLoaded':
                        event = t.VISIBLE_EVENT;
                        break;
                    case 'trackHomeloanClick':
                        event = t.CLICK_EVENT;
                        break;
                    case 'trackSubmitOtp':
                        event = t.SUBMIT_EVENT;
                        break;
                    case 'trackOtpVerified':
                        event = t.OTP_EVENT;
                        break;
                    case 'trackPropertyType':
                        event = t.CLICK_EVENT;
                        value = data.value;
                        break;
                    case 'trackHomeLoanDashboard':
                        sourcemodule = data.sourcemodule;
                        event = t.OPEN_LABEL;
                        page = data.page;
                        break;
                     case 'trackHomeLoanSubmit':
                        sourcemodule = data.source;
                        event = t.SUBMIT_LABEL;
                        page = data.page;
                        break;
                    default:
                        return;
                }
                properties[t.SOURCE_MODULE_KEY] = sourcemodule;
                properties[t.CATEGORY_KEY] = category;
                properties[t.LABEL_KEY] = label;
                properties[t.VALUE_KEY] = value;
                properties[t.PAGE_KEY] = page;

                trackingService.trackEvent(event, properties);
            }
        };

        return {
            messages,
            onmessage,
            init,
            destroy: function() {}
        };
    });
});
