define([
    'common/trackingConfigService',
    'services/trackingService'
], function(t, trackingService) {
    'use strict';
    Box.Application.addBehavior('serpProjectTracking', function() {

        var messages = [
            'trackMapCardClick',
            'trackPyrCardClick',
            'trackJarvisCardClick',
            'trackDownloadCardClick',
            'trackJourneyCardClick',
            'trackSimilarCardClick',
            'trackSimilarCardHover',
            'trackSerpPyrCardFail'
        ];

        var onmessage = function(name, data) {
            let event = t.NAVIGATION_EVENT,
                category = t.RHS_CATEGORY,
                label, sourceModule, nameField;

            switch (name) {
                case 'trackMapCardClick':
                    sourceModule = t.MAP_CARD_MODULE;
                    break;
                case 'trackPyrCardClick':
                    sourceModule = t.PYR_CARD_MODULE;
                    break;
                case 'trackSerpPyrCardFail':
                    label = 'trackSerpPyrCardFail';
                    sourceModule = t.PYR_CARD_MODULE;
                    break;
                case 'trackJarvisCardClick':
                    sourceModule = t.JARVIS_CARD_MODULE;
                    break;
                case 'trackDownloadCardClick':
                    sourceModule = t.DOWNLOAD_CARD_MODULE;
                    break;
                case 'trackJourneyCardClick':
                    sourceModule = t.JOURNEY_CARD_MODULE;
                    break;
                case 'trackSimilarCardHover':
                    event = t.HOVER_ON_CARD_EVENT;
                    nameField = data.id;
                    label = data.hoverTime;
                    if(data.type == "similarProject"){
                        sourceModule = t.SIMILAR_PROJECT;
                    }else if(data.type == "topLocality"){
                        sourceModule = t.TOP_LOCALITY;
                    }else if(data.type == "similarLocality"){
                        sourceModule = t.SIMILAR_LOCALITY;
                    }
                    break;
                case 'trackSimilarCardClick':
                    event = t.CLICK_EVENT;
                    label = data.id;
                    if(data.type == "similarProject"){
                        sourceModule = t.SIMILAR_PROJECT;
                    }else if(data.type == "topLocality"){
                        sourceModule = t.TOP_LOCALITY;
                    }else if(data.type == "similarLocality"){
                        sourceModule = t.SIMILAR_LOCALITY;
                    }
                    break;
            }
            let properties = {};
            properties[t.NAME_KEY] = nameField;
            properties[t.CATEGORY_KEY] = category;
            properties[t.LABEL_KEY] = label;
            properties[t.SOURCE_MODULE_KEY] = sourceModule;
            trackingService.trackEvent(event, properties);
        };

        return {
            messages: messages,
            onmessage: onmessage,
            init: function() {},
            destroy: function() {}
        };
    });
});