define([
    'services/leadPyrService',
    'services/utils',
    'services/defaultService',
], function(leadPyrService, utils, defaultService) {
    'use strict';
    Box.Application.addBehavior('leadBehaviour', function(context) {
        var attachedModuleEl;
        const messages = ["SHOW_ERROR",
            "HIDE_ERROR",
            "SHOW_SUCCESS",
            "singleSelectDropdownChanged",
            "singleSelectDropdownInitiated",
            "leadform:open"
        ];
        const config = {
            messages: leadPyrService.getValidationStrategyConfig(),
            defaultHideTime: 5000,
            selector: {
                "PHONE": "[data-type=PHONE_FIELD] input",
                "COUNTRY_ID": "input[data-country=COUNTRY_ID_FIELD]",
                "COUNTRY_CODE": "[data-country-code=COUNTRY_CODE_FIELD]",
                "EMAIL": "[data-type=EMAIL_FIELD] input",
                "NAME": "[data-type=NAME_FIELD] input",
                "OTP": "[data-type=OTP_FIELD] input",
                "VALID_EMAIL": "[data-type=VALID_EMAIL] input",
                "LISTINGS_CHECKBOX_CHECKED": "[data-similar-container] input[type=checkbox]:checked",
            }
        };

        function _openPopup(id) {
            context.broadcast('popup:open', {
                id: id,
                options: {
                    config: {
                        modal: true
                    }
                }
            });
        }

        function _showError({ moduleEl, errorType, field }) {
            moduleEl = moduleEl || attachedModuleEl;
            let message = config.messages[field][errorType]["MESSAGE"];
            $(moduleEl).find('[data-message=' + field + ']').text(message);
            context.broadcast("leadFormError", {
                id: moduleEl.id,
                label: message,
                field
            });
            _hideError({ moduleEl, field, time: config.defaultHideTime });
        }

        function _showSuccess({ moduleEl, successType, field }) {
            moduleEl = moduleEl || attachedModuleEl;
            let message = config.messages[field][successType]["MESSAGE"];
            message = '<span style="color:#1eb742">' + message + '</span>';
            $(moduleEl).find('[data-message=' + field + ']').html(message);
            _hideError({ moduleEl, field, time: config.defaultHideTime });
        }

        function _hideError({ moduleEl, field, time }) {
            moduleEl = moduleEl || attachedModuleEl;
            setTimeout(() => {
                $(moduleEl).find('[data-message=' + field + ']').html('');
            }, time);
        }

        function _validate(type) {
            let errorFlag = false,
                el = $(attachedModuleEl).find(config.selector[type]),
                value = el.val();

            switch (type) {
                case "PHONE":
                    let country = $(attachedModuleEl).find(config.selector["COUNTRY_ID"]).data("label");
                    if (!value) {
                        _showError({ errorType: 'EMPTY', field: 'PHONE' });
                        errorFlag = true;
                    } else if (value && !utils.validatePhone(value, country)) {
                        _showError({ errorType: 'INVALID', field: 'PHONE' });
                        errorFlag = true;
                    } else {
                        _hideError({ field: "PHONE" });
                    }
                    break;
                case "NAME":
                    if (!value) {
                        _showError({ errorType: 'EMPTY', field: 'NAME' });
                        errorFlag = true;
                    }else if (value && !utils.isName(value)) {
                        _showError({ errorType: 'INVALID', field: 'NAME' });
                        errorFlag = true;
                    } else {
                        _hideError({ field: "NAME" });
                    }
                    break;
                case "OTP":
                    if (!value) {
                        _showError({ errorType: 'EMPTY', field: 'OTP' });
                        errorFlag = true;
                    } else {
                        _hideError({ field: "OTP" });
                    }
                    break;
                case "EMAIL":
                    if (!value) {
                        _showError({ errorType: 'EMPTY', field: 'EMAIL' });
                        errorFlag = true;
                    }else if (value && !utils.isEmail(value, "")) {
                        _showError({ errorType: 'INVALID', field: 'EMAIL' });
                        errorFlag = true;
                    } else {
                        _hideError({ field: "EMAIL" });
                    }
                    break;
                case "LISTINGS_CHECKBOX_CHECKED":
                    if (!value) {
                        _showError({ errorType: 'MINIMUM_LISTING', field: 'GLOBAL' });
                        errorFlag = true;
                    } else {
                        _hideError({ field: "MINIMUM_LISTING" });
                    }
                    break;
                case "VALID_NAME":
                    el = $(attachedModuleEl).find(config.selector['NAME']);
                    value = el.val();
                    if (value && !utils.isName(value)) {
                        _showError({ errorType: 'INVALID', field: 'NAME' });
                        errorFlag = true;
                    } else {
                        _hideError({ field: "NAME" });
                    }
                    break;
                case "VALID_EMAIL":   
                    if (value && !utils.isEmail(value, "")) {
                        _showError({ errorType: 'INVALID', field: 'EMAIL' });
                        errorFlag = true;
                    } else {
                        _hideError({ field: "EMAIL" });
                    }
                    break;
            }

            return !errorFlag;
        }

        function _setCountry(data) {
            if (!data) {
                return;
            }
            if (data.code && data.value) {
                $(attachedModuleEl).find(config.selector["COUNTRY_CODE"]).text(data.code);
                $(attachedModuleEl).find(config.selector["COUNTRY_ID"]).val(data.value).data("label", data.label);
            }
        }
        function _openLeadPopup(data) {
            context.broadcast('Lead_Open_Step', data);
            _openPopup('leadOverviewPopup');
        }

        function onmessage(name, data = {}) {
            switch (name) {
                case "SHOW_ERROR":
                    _showError(data);
                    break;
                case "HIDE_ERROR":
                    _hideError(data);
                    break;
                case "SHOW_SUCCESS":
                    _showSuccess(data);
                    break;
                case "singleSelectDropdownChanged":
                case "singleSelectDropdownInitiated":
                    let params = data && data.params && JSON.parse(decodeURI(data.params));
                    _setCountry(params);
                    break;
                case "leadform:open":
                    if (data.id !== attachedModuleEl.id) {
                        return;
                    }
                    let __cookiedObj = leadPyrService.getEnquiryCookie() || {},
                        userPhone = __cookiedObj.phone;
                    delete __cookiedObj.salesType;
                    _openLeadPopup(data);
                    break;
            }
        }

        function onclick(event, element, elementType) {
            //let url;
            switch (elementType) {
                case "VALIDATE_AND_SUBMIT":
                    let validationStrategies = $(element).data("validation-strategies"),
                        nextStep = $(element).data("navigation-step"),
                        errorFlag = false;
                    validationStrategies = validationStrategies.split(",");
                    validationStrategies.forEach((type) => {
                        type = type.toUpperCase();
                        if (!_validate(type)) {
                            errorFlag = true;
                        }
                    });
                    if (!errorFlag) {
                        context.broadcast("navigateNextStep", {
                            id: attachedModuleEl.id,
                            step: nextStep
                        });
                    }
                    break;
            }
        }

        function onfocusout(event, element) {
            let $el = $(element),
                //value = $el.val(),
                field = $el.parents('[data-type]').data('type');
            switch (field) {
                case "PHONE_FIELD":
                    _validate("PHONE");
                    break;
                case "OTP_FIELD":
                    _validate("OTP");
                    break;
                case "VALID_EMAIL":
                    _validate("VALID_EMAIL");
                break;    
                case "EMAIL_FIELD":
                    _validate("EMAIL");
                    break;
            }
        }

        return {
            messages: messages,
            init: function() {
                attachedModuleEl = context.getElement();
            },
            destroy: function() {
                attachedModuleEl = null;
            },
            onmessage,
            onclick,
            onfocusout
        };
    });
});