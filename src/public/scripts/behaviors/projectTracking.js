define([
    'common/trackingConfigService',
    'services/trackingService',
    'services/utils'
], function(t, trackingService) {
    'use strict';
    Box.Application.addBehavior('projectTracking', function() {
        const VIEW_ALL_PROPERTY = 'project_configuration_viewAll',
            SIMILAR_SLIDER = 'project_similarSlider',
            SHOW_SELLER_POPUP = 'project_showSeller',
            VIEW_BUCKET_PROPERTY = 'project_viewProperty',
            BUCKET_TOGGLE = 'project_bucket_click',
            BHK_TAB_CLICKED = 'bhk_tab_clicked',
            BUDGET_TAB_CLICKED = 'budget_tab_clicked',
            BHK_SQFT_TAB_CLICKED = 'bhk_sqft_tab_clicked',
            PROPERTY_TYPE_TAB_CLICKED = 'property_type_tab_clicked';
        var messages = [
            'project_configTabClick',
            SIMILAR_SLIDER,
            VIEW_ALL_PROPERTY,
            SHOW_SELLER_POPUP,
            VIEW_BUCKET_PROPERTY,
            BUCKET_TOGGLE,
            BUDGET_TAB_CLICKED,
            BHK_SQFT_TAB_CLICKED,
            PROPERTY_TYPE_TAB_CLICKED,
            BHK_TAB_CLICKED,
            'trackGridCloseProject',
            'trackGridViewMoreProject',
            'trackGridSelectAllProject',
            'trackOpenFloatingLeadProject',
            'trackSubmitFloatingLeadProject',
            'trackViewOtherSellersProject',
            'trackProjectScoreProject',
            'trackStickyHeaderClickProject',
            'trackLocalityDetailClickProject',
            'trackCityDetailClickProject',
            'trackBuilderDetailClickProject',
            'zeroResultFound',
            'trackProjectGalleryBucketClick',
            'trackBuilderLisitngsClickProject',
            'trackProjectLeadFormOpen',
            'trackSimilarProjectLinkClick',
            'trackOpenMultipleSellerLeadProject',
            'trackOpenAvailableListingLeadProject',
            'trackCallNowSubmit',
            'trackOpenTopSellerLeadProject',
            'trackTabToggleProject',
            'trackViewOnMapProject',
            'trackMobileNeighbourhoodAmenity',
            'trackScrollOnClick',
            'trackAlpCloseClick',
            'trackAlpBuyLinkClick',
            'trackAlpRentLinkClick',
            'trackStickyNavBarCloseClick',
            'trackStickyNavBarBuyLinkClick',
            'trackStickyNavBarRentLinkClick',
            'trackPrevProjectBtnClick',
            'trackNextProjectBtnClick',
            'trackInteractiveUserScroll',
        ];

        let event,
            category,
            label,
            falseTrigger,
            sectionClicked;

        var init = function() {
            falseTrigger = true;
        };

        var onmessage = function(name, data = {}) {
            let sourceModuleConfig = t.LEAD_SOURCE_MODULES,
                labelConfig = t.LEAD_LABELS,
                budget,
                sourceModule,
                value,
                bhk,
                unitType,
                sellerId,
                sellerScore,
                listingId;
            let properties = {},
                _name,
                listingCategory,
                nonInteraction,
               // linkName,
                //linkType,
                rank;
            //let isMobileRequest = utils.isMobileRequest();

            switch (name) {
                case "trackAlpCloseClick":
                    event = t.CLICK_EVENT;
                    category = t.POPUP_CATEGORY;
                    label = t.CLOSE_LABEL;
                    break;
                case "trackAlpBuyLinkClick":
                    event = t.CLICK_EVENT;
                    category = t.POPUP_CATEGORY;
                    label = t.BUY_LABEL;
                    value = data.value;
                    sectionClicked = "projectPopupToSerp";
                    break;
                case "trackAlpRentLinkClick":
                    event = t.CLICK_EVENT;
                    category = t.POPUP_CATEGORY;
                    label = t.RENT_LABEL;
                    value = data.value;
                    sectionClicked = "projectPopupToSerp";
                    break;

                case "trackStickyNavBarBuyLinkClick":
                    event = t.CLICK_EVENT;
                    category = t.STICKY_HEADER_CATEGORY;
                    label = t.BUY_LABEL;
                    value = data.value;
                    sectionClicked = "projectStickyToSerp";
                    break;
                case "trackStickyNavBarRentLinkClick":
                    event = t.CLICK_EVENT;
                    category = t.STICKY_HEADER_CATEGORY;
                    label = t.RENT_LABEL;
                    value = data.value;
                    sectionClicked = "projectStickyToSerp";
                    break;

                case "trackScrollOnClick":
                    event = t.CLICK_EVENT;
                    category = t[data.category];
                    label = t[data.label];
                    break;
                case 'trackMobileNeighbourhoodAmenity':
                    event = t.INFO_TOGGLE;
                    category = t.MAPS_CATEGORY;
                    label = data.displayName;
                    break;

                case 'trackViewOnMapProject':
                    category = t.HEADLINE_CATEGORY;
                    event = t.VIEW_ON_MAP;
                    break;

                case 'trackTabToggleProject':
                    event = t.TOGGLE_EVENT;
                    category = t.EXTRA_DETAILS_CATEGORY;
                    label = data.tabName;
                    _name = data.name;
                    listingCategory = data.listingCategory;
                    sourceModule = data.source;
                    break;

                case 'trackOpenTopSellerLeadProject':
                    event = t.OPEN_EVENT;
                    category = t.LEAD_FORM_CATEGORY;
                    label = labelConfig.CallNow;
                    sourceModule = sourceModuleConfig && sourceModuleConfig.topSellers;
                    sellerId = data.sellerId;
                    sellerScore = data.sellerRating;
                    break;

                case 'trackOpenAvailableListingLeadProject':
                    event = t.OPEN_EVENT;
                    category = t.LEAD_FORM_CATEGORY;
                    label = labelConfig.CallNow;
                    sourceModule = sourceModuleConfig && sourceModuleConfig.availableListings;
                    sellerId = data.sellerId;
                    sellerScore = data.sellerRating;
                    listingId = data.listingId;
                    bhk = data.beds;
                    unitType = data.unitType;
                    break;

                case 'trackCallNowSubmit':
                    event = t.SUBMIT_EVENT;
                    category = t.LEAD_FORM_CATEGORY;
                    label = labelConfig.CallNow;
                    sourceModule = sourceModuleConfig && sourceModuleConfig.availableListings;
                    sellerId = data.companyId;
                    sellerScore = data.companyRating;
                    listingId = data.listingId;
                    bhk = data.beds;
                    unitType = data.unitType;
                    break;

                case 'trackOpenMultipleSellerLeadProject':
                    event = t.OPEN_EVENT;
                    category = t.LEAD_FORM_CATEGORY;
                    label = labelConfig.CallNow;
                    sourceModule = sourceModuleConfig && sourceModuleConfig.Headline;
                    sellerId = data.sellerIds && data.sellerIds.join();
                    sellerScore = data.sellerScore && data.sellerScore.join();
                    break;

                case 'project_configTabClick':
                    event = t.CATEGORY_TOGGLE_EVENT;
                    category = t.CONFIGURATION_CATEGORY;
                    listingCategory = data.listingCategory;
                    break;
                case PROPERTY_TYPE_TAB_CLICKED:
                    event = t.PROPERTY_TYPE_TOGGLE_EVENT;
                    category = t.CONFIGURATION_CATEGORY;
                    listingCategory = data.listingCategory;
                    unitType = data.unitType;
                    break;
                case SIMILAR_SLIDER:
                    event = t.INTERACTION_EVENT;
                    category = t.SIMILAR_CATEGORY;
                    label = data.event;
                    break;
                case BHK_TAB_CLICKED:
                    category = t.CONFIGURATION_CATEGORY;
                    event = t.CLICK_EVENT;
                    label = t.BHK_LABEL;
                    bhk = data.bhk;
                    listingCategory = data.listingCategory;
                    unitType = data.unitType;
                    break;
                case VIEW_ALL_PROPERTY:
                    event = t.VIEW_ALL_PROPERTY;
                    category = t.CONFIGURATION_CATEGORY;
                    break;
                case SHOW_SELLER_POPUP:
                    value = data.sellercount;
                    budget = data.budget;
                    event = t.SHOW_SELLER;
                    category = t.CONFIGURATION_CATEGORY;
                    unitType = data.unitType;
                    break;
                case VIEW_BUCKET_PROPERTY:
                    value = data.properyCount;
                    budget = data.budget;
                    event = t.VIEW_PROPERTY_EVENT;
                    category = t.CONFIGURATION_CATEGORY;
                    unitType = data.unitType;
                    break;
                case 'project_similarSlider':
                    event = t.INTERACTION_EVENT;
                    category = t.SIMILAR_CATEGORY;
                    label = data.event;
                    break;
                case 'trackSimilarProjectLinkClick':
                    event = t.CLICK_EVENT;
                    category = t.RHS_CATEGORY;
                    label = data.projectName;
                    sourceModule = sourceModuleConfig[data.source];
                    rank = data.rank;
                    sectionClicked = 'similarProject';
                    break;

                case 'trackGridCloseProject':
                    event = t.NAVIGATION_EVENT;
                    label = labelConfig.MultiSeller + '_1';
                    category = t.LEAD_FORM_CATEGORY;
                    sourceModule = sourceModuleConfig[data.gridSource];
                    listingCategory = data.listingCategory;
                    break;

                case 'trackGridViewMoreProject':
                    event = t.VIEW_MORE_EVENT;
                    category = t.LEAD_FORM_CATEGORY;
                    sourceModule = sourceModuleConfig[data.source];
                    listingCategory = data.listingCategory;
                    break;

                case 'trackGridSelectAllProject':
                    event = t.VIEW_MORE_EVENT;
                    category = t.LEAD_FORM_CATEGORY;
                    sourceModule = sourceModuleConfig[data.source];
                    listingCategory = data.listingCategory;
                    break;

                case BUCKET_TOGGLE:
                    event = t.BUCKET_EXPAND_EVENT;
                    category = t.CONFIGURATION_CATEGORY;
                    budget = data.budget;
                    bhk = data.bhk;
                    unitType = data.unitType;
                    break;

                case 'trackOpenFloatingLeadProject':
                    category = t.LEAD_FORM_CATEGORY;
                    event = t.OPEN_EVENT;
                    label = labelConfig.CallNow;
                    sourceModule = sourceModuleConfig.ProjectFloatButton;
                    sellerId = data.sellerIds;
                    sellerScore = data.sellerScore;
                    break;

                case 'trackSubmitFloatingLeadProject':
                    category = t.LEAD_FORM_CATEGORY;
                    event = t.SUBMIT_EVENT;
                    label = labelConfig.CallNow;
                    sourceModule = sourceModuleConfig.ProjectFloatButton;
                    break;
                case 'trackViewOtherSellersProject':
                    category = t.LEAD_FORM_CATEGORY;
                    event = t.VIEW_OTHER_SELLERS;
                    label = labelConfig.OTHER_SELLERS;
                    sourceModule = sourceModuleConfig.VIEW_OTHER_PROJECT;
                    break;

                case 'trackProjectScoreProject':
                    category = t.DESCRIPTION_CATEGORY;
                    event = t.INTERACTION_EVENT;
                    label = t.PROJECT_SCORE_LABEL;
                    break;

                case 'trackStickyHeaderClickProject':
                    category = t.STICKY_HEADER_CATEGORY;
                    event = t.CLICK_EVENT;
                    label = data.label;
                    break;

                case 'trackLocalityDetailClickProject':
                    category = t.DISCOVER_MORE_CATEGORY;
                    event = t.CLICK_EVENT;
                    label = t.LOCALITY_DETAILS_LABEL;
                    value = data.value;
                    break;

                case 'trackCityDetailClickProject':
                    category = t.DISCOVER_MORE_CATEGORY;
                    event = t.CLICK_EVENT;
                    label = t.CITY_DETAILS_LABEL;
                    value = data.value;
                    break;

                case 'trackBuilderDetailClickProject':
                    category = t.DISCOVER_MORE_CATEGORY;
                    event = t.CLICK_EVENT;
                    label = t.BUILDER_LISTINGS_LABEL;
                    value = data.value;
                    break;

                case 'trackBuilderLisitngsClickProject':
                    category = t.BUILDER_LISTINGS_CATEGORY;
                    event = t.CLICK_EVENT;
                    if(data.label === 'Past Projects'){
                        label = t.PAST_LABEL;
                    } else if(data.label === 'Ongoing Projects'){
                        label = t.CURRENT_LABEL;
                    }
                    value = data.value;
                    break;
                case 'trackProjectGalleryBucketClick':
                    event = t.GALLERY_INTERACTION_EVENT;
                    category = t.GALLERY_CATEGORY;
                    _name = data.name || t.CLICK_NAME;
                    label = data.bucket;
                    sourceModule = data.sourceModule;
                    properties[t.ADDITIONAL_CD57] = data.sourceElement;
                    break;
               case BUDGET_TAB_CLICKED:
                    category = t.CONFIGURATION_CATEGORY;
                    event = t.CLICK_EVENT;
                    label = data.budget;
                    _name =  t.BUDGET_RANGE_LABEL;
                    listingCategory = data.listingCategory;
                    unitType = data.unitType;
                    break;

               case BHK_SQFT_TAB_CLICKED:
                    category = t.CONFIGURATION_CATEGORY;
                    event = t.CLICK_EVENT;
                    _name =  t.BHK_SQFT_LABEL;
                    label = data.sqft;
                    bhk = data.bhk;
                    listingCategory = data.listingCategory;
                    unitType = data.unitType;
                    break;
                case 'zeroResultFound':
                    category = t.ZERO_RESULTS_CATEGORY;
                    event = t.ERROR_EVENT;
                    listingCategory = data.listingCategory;
                    unitType = data.unitType;
                    sourceModule = t.CONFIGURATION_CATEGORY;
                    nonInteraction = (data && data.nonInteraction) ? 1 : 0;
                    label = undefined;
                    value = undefined;
                    break;
                case 'trackProjectLeadFormOpen':
                    category = t.LEAD_FORM_CATEGORY;
                    label = data.label;
                    event = t.OPEN_EVENT;
                    if(data.sourceModule) {
                        sourceModule = sourceModuleConfig[data.sourceModule];
                    }
                    break;
                case 'trackNextProjectBtnClick':
                    category = t.RHS_CATEGORY;
                    label = data.label;
                    event = t.CLICK_EVENT;
                    sourceModule = data.sourceModule;
                    break;
                case 'trackPrevProjectBtnClick':
                    category = t.RHS_CATEGORY;
                    label = data.label;
                    event = t.CLICK_EVENT;
                    sourceModule = data.sourceModule;
                    break;
                case 'trackInteractiveUserScroll':
                    category = t.NAVIGATION_CATEGORY;
                    event = t.USER_SCROLL_EVENT;
                    break;
            }

            properties[t.CATEGORY_KEY] = category;
            properties[t.LABEL_KEY] = label;
            properties[t.VALUE_KEY] = value;
            properties[t.BUDGET_KEY] = budget;
            properties[t.SOURCE_MODULE_KEY] = sourceModule;
            properties[t.BHK_KEY] = t.makeValuesUniform(bhk,t.BHK_MAP)  ;
            properties[t.UNIT_TYPE_KEY]  = t.makeValuesUniform(unitType,t.UNIT_TYPE_MAP) ;
            properties[t.NAME_KEY]  = _name;
            properties[t.LISTING_CATEGORY_KEY] = t.makeValuesUniform(listingCategory,t.LISTING_CATEGORY_MAP) ;
            properties[t.LISTING_ID_KEY] = listingId;
            properties[t.NON_INTERACTION_KEY] = nonInteraction;
            properties[t.LINK_NAME_KEY] = data.linkName;
            properties[t.LINK_TYPE_KEY] = data.linkType;
            properties[t.SELLER_ID_KEY] = sellerId;
            properties[t.SELLER_SCORE_KEY] = sellerScore;
            properties[t.RANK_KEY] = rank;
            properties[t.SECTION_CLICKED] = sectionClicked;

            trackingService.trackEvent(event, properties);
        };


        var onclick = function(event, clickElement, elementType){
            let eventName;
            if(!falseTrigger) {

                let sourceModule,value,nonInteraction;

                let clickElementData = $(clickElement).data();
                let amenityLabels = t.MAP_LABELS;
                let properties = {};
                switch(elementType){
                    case 'tab':
                        if(clickElementData.trackType === 'map-tabs'){
                            eventName = t.INFO_TOGGLE;
                            category = t.MAPS_CATEGORY;
                            label = amenityLabels[clickElementData.trackLabel];
                            nonInteraction = 0;
                        }
                        break;

                    default:
                    return;
                }

                properties[t.CATEGORY_KEY] = category;
                properties[t.LABEL_KEY] = label;
                properties[t.SOURCE_MODULE_KEY] = sourceModule;
                properties[t.VALUE_KEY] = value;
                properties[t.NON_INTERACTION_KEY] = nonInteraction;

                if(eventName){
                    trackingService.trackEvent(eventName, properties);
                }
            }
            else {
                falseTrigger = false;
            }
        };

        return {
            messages: messages,
            onmessage: onmessage,
            init: init,
            destroy: function() {},
            onclick
        };
    });
});
