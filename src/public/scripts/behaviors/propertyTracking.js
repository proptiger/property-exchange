define([
    'common/trackingConfigService',
    'services/trackingService',
    'services/utils'
], function(t, trackingService,utils) {
    'use strict';
    Box.Application.addBehavior('propertyTracking', function(context) {

        //let event,
          let  category,
            label,
            falseTrigger;

        var messages = [
            'property_similarSlider',
            'trackGridViewMoreProperty',
            'trackGridSelectAllProperty',
            'trackGridCloseProperty',
            'trackOpenFloatingLeadProperty',
            'trackNextPrevHeadlineProperty',
            'trackViewOtherSellersProperty',
            'trackProjectScoreProperty',
            'trackLocalityScoreProperty',
            'trackStickyHeaderClickProperty',
            'trackLocalityDetailClickProperty',
            'trackBuilderDetailClickProperty',
            'trackPropertyGalleryBucketClick',
            'trackProjectDetailClickProperty',
            'trackBuilderLisitngsClickProperty',
            'trackReportErrorPopup',
            'trackPropertyLeadFormOpen',
            'trackSimilarPropertyLinkClick',
            'rightSimilarPropertyScroll',
            'leftSimilarPropertyScroll',
            'similarPropertyClick',
            'similarPropertySeeAll',
            'trackLeadOpenProperty',
            'trackCallNowSubmit',
            'trackViewOnMapProperty',
            'trackTabToggleProperty',
            'trackMorePropertiesProperty',
            'trackMorePropertiesSeeAllProperty',
            'trackMobileNeighbourhoodAmenity',
            'trackDealMakerLeadOpen',
            "trackLeadOpen",
            "trackInteractiveUserScroll",
            "trackSellerTooltip",
            'trackSelectKnowMore',
            'msKnowMorePopupClose',
            'selectBannerSeen',
            'selectReminderSeen',
            'selectListingSeen',
            'trackSelectBadgeTooltip',
            'trackPopupSearchProperties',
            'trackPopupUnlockVoucher',
            'trackReminderUnlock',
            'trackSimilarCardSeen',
            'trackSimilarCardClick'
        ];

        var init = function() {
            falseTrigger = true;
        };



        var onmessage = function(eventName, data) {

                let event = t.NAVIGATION_EVENT,
                    config = utils.getPageExtraData(),
                    category = t.HEADER_CATEGORY,
                    sourceModuleConfig = t.LEAD_SOURCE_MODULES,
                    labelConfig = t.LEAD_LABELS,
                    pageConfig = context.getConfig(),
                    sourceModule,
                    label,
                    value,
                    phone,
                    name,
                    localityId = window.pageData.localityId,
                    cityId = window.pageData.cityId ,
                    listingId = window.pageData.listingId,
                    projectId = window.pageData.projectId,
                    builderId = window.pageData.builderId,
                    suburbId = window.pageData.suburbId,
                    listingCategory = config.listingCategory,
                    isSelectListing = pageConfig && pageConfig.isMakaanSelectSeller=="true" && listingCategory && listingCategory.toLowerCase()=="rental",
                    projectStatus = config.projectStatus,
                    isPaidSeller = pageConfig.isPaidSeller,
                    rating = pageConfig.sellerRating,
                    budget = config.budget,
                    unitType = config.unitType,
                    sellerId = config.sellerId || config.companyId,
                    bhk = config.beds,
                    sellerScore = config.sellerRating || config.companyRating,
                    landmarkId,
                    listingScore = config.listingScore,
                    linkName,
                    linkType,
                    rank,
                    sectionClicked;
            let nonInteraction;
            let properties = {};
            switch (eventName) {
                case 'trackMobileNeighbourhoodAmenity':
                    event = t.INFO_TOGGLE;
                    category = t.MAPS_CATEGORY;
                    label = data.displayName;
                    break;

                case 'trackMorePropertiesSeeAllProperty':
                    sectionClicked = "similarPropertySeeAll";
                case 'trackMorePropertiesProperty':
                    event = t.INTERACTION_EVENT;
                    category = t.SIMILAR_CATEGORY;
                    sourceModule = t.SIMILAR_PROPERTY_MODULE[data.source];
                    label = t.CLICK_LABEL;
                    rank = data.rank;
                    sectionClicked = sectionClicked || "similarPropertyClick";
                    break;

                case 'trackTabToggleProperty':
                    event = t.TOGGLE_EVENT;
                    category = t.EXTRA_DETAILS_CATEGORY;
                    label = data.tabName;
                    name = data.name;
                    sourceModule = data.source;
                    break;

                case 'trackViewOnMapProperty':
                    category = t.HEADLINE_CATEGORY;
                    event = t.VIEW_ON_MAP;
                    break;

                case 'trackLeadOpenProperty':
                    category = t.LEAD_FORM_CATEGORY;
                    event = t.OPEN_EVENT;
                    label = labelConfig.CallNow;
                    sourceModule = sourceModuleConfig.Headline;
                    break;

                case 'trackCallNowSubmit':
                    category = t.LEAD_FORM_CATEGORY;
                    event = t.SUBMIT_EVENT;
                    label = labelConfig.CallNow;
                    sourceModule = sourceModuleConfig[data.source];
                    break;

                case 'property_similarSlider':
                    event = t.INTERACTION_EVENT;
                    category = t.SIMILAR_CATEGORY;
                    label = data.event;
                    break;

                case 'trackGridCloseProperty':
                    event = t.NAVIGATION_EVENT;
                    label = labelConfig.MultiSeller + '_1';
                    category = t.LEAD_FORM_CATEGORY;
                    sourceModule = sourceModuleConfig[data.gridSource];
                    properties[t.LISTING_CATEGORY_KEY] = t.makeValuesUniform(data.listingCategory,t.LISTING_CATEGORY_MAP) ;
                    break;

                case 'trackGridViewMoreProperty':
                    event = t.VIEW_MORE_EVENT;
                    category = t.LEAD_FORM_CATEGORY;
                    sourceModule = sourceModuleConfig[data.source];
                    properties[t.LISTING_CATEGORY_KEY] = t.makeValuesUniform(data.listingCategory,t.LISTING_CATEGORY_MAP) ;

                case 'trackGridSelectAllProperty':
                    event = t.VIEW_MORE_EVENT;
                    category = t.LEAD_FORM_CATEGORY;
                    sourceModule = sourceModuleConfig[data.source];
                    properties[t.LISTING_CATEGORY_KEY] = t.makeValuesUniform(data.listingCategory,t.LISTING_CATEGORY_MAP) ;
                    break;

                case 'trackSimilarPropertyLinkClick':
                    event = t.CLICK_EVENT;
                    category = t.RHS_CATEGORY;
                    label = data.propertyName;
                    linkName = data.linkName;
                    linkType = data.linkType;
                    sourceModule = sourceModuleConfig[data.source];
                    rank = data.rank;
                    sectionClicked = "otherSimilarProperties";
                    break;

                case 'trackOpenFloatingLeadProperty':
                    category = t.LEAD_FORM_CATEGORY;
                    event = t.OPEN_EVENT;
                    label = labelConfig.PropertyFloatButton;
                    sourceModule = sourceModuleConfig.PropertyFloatButton;
                    break;

                case 'trackNextPrevHeadlineProperty':
                    category = t.HEADLINE_CATEGORY;
                    event = t.NAVIGATION_EVENT;
                    label = data.type == 'next-listing' ? t.NEXT_LABEL : t.PREVIOUS_LABEL;
                    break;
                case 'trackViewOtherSellersProperty':
                    category = t.LEAD_FORM_CATEGORY;
                    event = t.VIEW_OTHER_SELLERS;
                    label = labelConfig.OTHER_SELLERS;
                    sourceModule = sourceModuleConfig.VIEW_OTHER_PROPERTY;
                    break;

                case 'trackLocalityScoreProperty':
                    category = t.DESCRIPTION_CATEGORY;
                    event = t.INTERACTION_EVENT;
                    label = t.LOCALITY_SCORE_LABEL;
                    break;

                case 'trackProjectScoreProperty':
                    category = t.DESCRIPTION_CATEGORY;
                    event = t.INTERACTION_EVENT;
                    label = t.PROJECT_SCORE_LABEL;
                    break;

                case 'trackStickyHeaderClickProperty':
                    category = t.STICKY_HEADER_CATEGORY;
                    event = t.CLICK_EVENT;
                    label = data.label;
                    break;

                case 'trackLocalityDetailClickProperty':
                    category = t.DISCOVER_MORE_CATEGORY;
                    event = t.CLICK_EVENT;
                    label = t.LOCALITY_DETAILS_LABEL;
                    value = data.value;
                    break;

                case 'trackBuilderDetailClickProperty':
                    category = t.DISCOVER_MORE_CATEGORY;
                    event = t.CLICK_EVENT;
                    label = t.BUILDER_LISTINGS_LABEL;
                    value = data.value;
                    break;

                case 'trackProjectDetailClickProperty':
                    category = t.DISCOVER_MORE_CATEGORY;
                    event = t.CLICK_EVENT;
                    label = t.PROJECT_DETAILS_LABEL;
                    value = data.value;
                    break;

                case 'trackBuilderLisitngsClickProperty':
                    category = t.BUILDER_LISTINGS_CATEGORY;
                    event = t.CLICK_EVENT;
                    if(data.label === 'Past Projects'){
                        label = t.PAST_LABEL;
                    } else if(data.label === 'Ongoing Projects'){
                        label = t.CURRENT_LABEL;
                    }
                    value = data.value;
                    break;

                case 'trackPropertyGalleryBucketClick':
                    event = t.GALLERY_INTERACTION_EVENT;
                    category = t.GALLERY_CATEGORY;
                    name = t.CLICK_NAME;
                    label = data.bucket;
                    sourceModule = data.sourceModule;
                    properties[t.ADDITIONAL_CD57] = data.sourceElement;
                    break;

                case 'trackReportErrorPopup':
                    event = t.REPORT_ERROR_EVENT;
                    category = t.DESCRIPTION_CATEGORY;
                    sourceModule = t.DESCRIPTION_MODULE;
                    break;


                case 'trackPropertyLeadFormOpen':
                    category = t.LEAD_FORM_CATEGORY;
                    label = data.label;
                    event = t.OPEN_EVENT;
                    if(data.sourceModule) {
                        sourceModule = sourceModuleConfig[data.sourceModule];
                    }
                    break;

                case 'similarPropertyClick':
                case 'similarPropertySeeAll':
                case 'leftSimilarPropertyScroll':
                case 'rightSimilarPropertyScroll':
                    event = t.INTERACTION_EVENT;
                    category = t.SIMILAR_CATEGORY;
                    sourceModule = t.SIMILAR_PROPERTY_MODULE[data.cardType];
                    label = data.label;
                    localityId = data.localityId;
                    cityId = data.cityId;
                    projectId = data.projectId;
                    builderId = data.builderId;
                    projectStatus =  data.projectStatus && t.PROJECT_STATUS_NAME[(data.projectStatus).toLowerCase()] || data.projectStatus;
                    sellerId = data.sellerId;
                    listingId = data.listingId;
                    bhk = data.bhk;
                    unitType = data.unitType;
                    listingCategory = data.listingCategory;
                    sellerScore = data.sellerScore;
                    budget = data.budget;
                    listingScore = data.listingScore;
                    suburbId = data.suburbId;
                    sectionClicked = (eventName == "similarPropertyClick" || eventName == "similarPropertySeeAll") ? eventName : '';
                    break;

                case 'trackDealMakerLeadOpen':
                    event = t.OPEN_EVENT;
                    category = t.LEAD_FORM_CATEGORY;
                    sourceModule = t.DEAL_MAKER;
                    if(data.label){
                        label = data.label;
                    }
                    break;

                case 'trackLeadOpen':
                    event = t.OPEN_EVENT;
                    category = t.LEAD_FORM_CATEGORY;
                    sourceModule = t.LEAD_SOURCE_MODULES[data.source];
                    break;

                case 'trackInteractiveUserScroll':
                    category = t.NAVIGATION_CATEGORY;
                    event = t.USER_SCROLL_EVENT;
                    break;
                case 'trackSellerTooltip':
                    category=t.DEAL_REVIEW_CATEGORY;
                    event= data.event;
                    label=data.label;
                    break;
                 case 'trackSelectKnowMore':
                    event = t.CARD_CLICK;
                    label="Program_Details_Modal";
                    category=t.SELECT_VOUCHER_GENERATION_CATEGORY;
                    sourceModule = data.source;
                    break;
                case 'msKnowMorePopupClose':
                    event = t.CLOSE_EVENT;
                    label="Program_Details_Modal";
                    category=t.SELECT_VOUCHER_GENERATION_CATEGORY;
                    break;
                case 'selectBannerSeen':
                    event = t.CARD_SEEN;
                    label="Select_Banner";
                    category=t.SELECT_VOUCHER_GENERATION_CATEGORY;
                    sourceModule = 'Property';
                    nonInteraction = 1;
                    break;
                case 'selectListingSeen':
                    event = 'Listing Seen';
                    label="Seller_Promo_Property";
                    category=t.SELECT_VOUCHER_GENERATION_CATEGORY;
                    sourceModule = data.source;
                    nonInteraction = 1;
                    break;
                case 'trackSelectBadgeTooltip':
                    event = 'HOVER_SELECT_TAG';
                    label="Seller_Promo_Property";
                    category=t.SELECT_VOUCHER_GENERATION_CATEGORY;
                    break;
                case 'trackPopupSearchProperties':
                    event = t.CLICK_EVENT;
                    label="Search_Select_Properties";
                    category=t.SELECT_VOUCHER_GENERATION_CATEGORY;
                    break;
                case 'trackPopupUnlockVoucher':
                    event = t.CLICK_EVENT;
                    label="Unlock_Select_Voucher";
                    category=t.SELECT_VOUCHER_GENERATION_CATEGORY;
                    break;
                case 'selectBannerSeen':
                    event = t.CARD_SEEN;
                    label="Select_Reminder";
                    category='Makaan Select- Promo & Voucher Generation- buyers';
                    sourceModule = 'Property';
                    break;
                case 'trackReminderUnlock':
                    event = t.CLICK_EVENT;
                    label="Select_Reminder_Unlock";
                    category='Makaan Select- Promo & Voucher Generation- buyers';
                    sourceModule = 'Property';
                    break;
                case 'selectReminderSeen':
                    event = t.CARD_SEEN;
                    label="Select_Reminder";
                    category='Makaan Select- Promo & Voucher Generation- buyers';
                    sourceModule = "Property";
                    break;
                case 'trackSimilarCardSeen':
                    event = t.CARD_SEEN;
                    label = "SERP_Nav";
                    category = 'Similar Properties';
                    sourceModule = "Property";
                    properties[t.PAGE_KEY] = data.pageType;
                    sourceModule = data.card;
                    nonInteraction = 1;
                    break;
                case 'trackSimilarCardClick':
                    event = t.CARD_CLICK;
                    label = "SERP_Nav";
                    category = 'Similar Properties';
                    sourceModule = "Property";
                    properties[t.PAGE_KEY] = data.pageType;
                    sourceModule = data.card;
                    sectionClicked = 'lookingForMore';
                    break;
            }

            properties[t.CATEGORY_KEY] = category;
            properties[t.LABEL_KEY] = label;
            properties[t.SOURCE_MODULE_KEY] = sourceModule;
            properties[t.VALUE_KEY] = value;
            properties[t.LOCALITY_ID_KEY] = localityId;
            properties[t.CITY_ID_KEY] = cityId;
            properties[t.LISTING_ID_KEY] = listingId;
            properties[t.PROJECT_ID_KEY] = projectId;
            properties[t.BUILDER_ID_KEY] = builderId;
            properties[t.SUBURB_ID_KEY] = suburbId;
            properties[t.LISTING_CATEGORY_KEY] = t.makeValuesUniform(listingCategory,t.LISTING_CATEGORY_MAP) ;
            properties[t.PROJECT_STATUS_KEY] = t.makeValuesUniform(projectStatus,t.PROJECT_STATUS_NAME) ;
            properties[t.BUDGET_KEY] = budget;
            properties[t.UNIT_TYPE_KEY] = t.makeValuesUniform(unitType,t.UNIT_TYPE_MAP) ;
            properties[t.SELLER_ID_KEY] = sellerId;
            properties[t.SELLER_SCORE_KEY] = rating;
            properties[t.SELLER_STATUS_KEY] = isPaidSeller?t.SELLER_PAID_STATUS.PAID:t.SELLER_PAID_STATUS.NOT_PAID;
            properties[t.NAME_KEY] = name;
            properties[t.PHONE_USER_KEY] = phone;
            properties[t.BHK_KEY] = t.makeValuesUniform(`${bhk}`,t.BHK_MAP) || bhk;
            properties[t.SELLER_SCORE_KEY] = sellerScore;
            properties[t.RANK_KEY] = rank;
            properties[t.LANDMARK_ID_KEY] = landmarkId;
            properties[t.LISTING_SCORE_KEY] = listingScore;
            properties[t.LINK_NAME_KEY] = linkName;
            properties[t.LINK_TYPE_KEY] = linkType;
            properties[t.NON_INTERACTION_KEY] = nonInteraction;
            properties[t.SELECT_PROPERTY_KEY] = isSelectListing ? 'Select Listing':undefined;
            properties[t.SECTION_CLICKED] = sectionClicked;

            trackingService.trackEvent(event, properties);
        };

        var onclick = function(event, clickElement, elementType) {

            if (!falseTrigger) {

                let sourceModule, value;

                let clickElementData = $(clickElement).data();
                let amenityLabels = t.MAP_LABELS;
                let properties = {};
                let nonInteraction;
                switch (elementType) {
                    case 'tab':
                        if (clickElementData.trackType === 'map-tabs') {
                            event = t.INFO_TOGGLE;
                            category = t.MAPS_CATEGORY;
                            label = amenityLabels[clickElementData.trackLabel];
                            nonInteraction = 0;
                        }
                        break;

                    default:
                        return;
                }

                properties[t.CATEGORY_KEY] = category;
                properties[t.LABEL_KEY] = label;
                properties[t.SOURCE_MODULE_KEY] = sourceModule;
                properties[t.VALUE_KEY] = value;
                properties[t.NON_INTERACTION_KEY] = nonInteraction;

                if (event) {
                    trackingService.trackEvent(event, properties);
                }
            } else {
                falseTrigger = false;
            }
        };

        return {
            messages: messages,
            onmessage: onmessage,
            init: init,
            destroy: function() {},
            onclick
        };
    });
});
