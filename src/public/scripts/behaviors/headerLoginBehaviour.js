define([
    "services/utils",
    'services/loginService'
    ],
function(utils, loginService) {
    "use strict";
    Box.Application.addBehavior("headerLoginBehaviour", function(context) {
        const config = context.getGlobal('config'), 
            UserImageSelector = "[data-user-image]",
            LoginSelector = "[data-type='login']",
            LoginElement = "[data-login-element]",
            UserNameSelector = "[data-user-name]",
            AvatarDataType = "[data-link-type='avatar']",
            UserNameDataType = "[data-link-type='user-name']",
            NameInitial = "[data-initial]",
            SellerSiteLink = "[data-type='track-sell-your-property']",
            HIDE = "hide",
            menuDrawerTemplateSeller = 'doT!modules/menuDrawer_SEO/views/menuDrawerSeller';

        var isLoggedIn,
            isMobile,
            onSellerJourneyPage,
            moduleEl,
            $moduleEl,
            userImageLink,
            userNameLink,
            isSellerLoggedIn;

        function toggleListPropertyHeader(loginSeller) {
            let el = $('.sellersitelink');
            if (loginSeller) {
                el.addClass('hidden');
            } else {
                el.removeClass('hidden');
            }
        }

        function _setUserName(status, data) {
            data = data || {};
            let avatar = utils.getAvatar(data.firstName),
                userImageEl = $moduleEl.find(UserImageSelector),
                userNameEl = $moduleEl.find(UserNameSelector),
                loginSelectorEL = $moduleEl.find(LoginSelector),
                loginEl = $moduleEl.find(LoginElement),
                sellerLink = $moduleEl.find(SellerSiteLink);

            if (status === 'login') {
                isLoggedIn = true;
                context.broadcast('userLoggedIn', data);
                data.firstName = isMobile ? `` : data.firstName;
                userNameLink.text(data.firstName).removeClass(HIDE);
                userNameEl.removeClass(HIDE);
                loginSelectorEL.addClass(HIDE);
                if (data.profileImageUrl) {
                    userImageEl.find('img').removeAttr('data-src').attr('src', data.profileImageUrl).removeClass(HIDE);
                    userImageEl.find(NameInitial).text('');
                } else {
                    userImageEl.css({
                        'background-color': avatar.backgroundColor,
                        'text-color': avatar.textColor
                    }).find(NameInitial).text(avatar.text);
                    userImageEl.find('.avatar').addClass(HIDE);
                }
                userImageEl.removeClass(HIDE);

                if (data.roles && data.roles.indexOf('Seller') > -1) {
                    isSellerLoggedIn = true;
                    loginEl.addClass('my-dashboard');
                    userNameLink.text(isMobile ? '' : 'my-dashboard').attr('href', context.getConfig().environment + config.sellerDashboardUrl).attr('data-seller', '');
                    userImageLink.attr('href', context.getConfig().environment + config.sellerDashboardUrl);
                    toggleListPropertyHeader(true);
                    if(moduleEl.id.indexOf('header') > 0){
                        context.broadcast('changeMenuDrawerTemplate', {
                            template: menuDrawerTemplateSeller
                        });
                    }
                }
            } else
            if (status === 'logout') {
                isSellerLoggedIn = false;
                isLoggedIn = false;
                context.broadcast('userLoggedOut');
                userImageEl.find(NameInitial).text('');
                userImageEl.removeAttr('style').find('img').addClass(HIDE) ;//.attr('src', DummyImage);
                context.broadcast('changeMenuDrawerTemplate', {});
                userImageLink.attr('href', config.buyerDashboardUrl);
                if (onSellerJourneyPage) {
                    userImageEl.find(AvatarDataType).removeAttr('href');
                }
                loginEl.removeClass('my-dashboard');
                userImageEl.find('.avatar').removeClass(HIDE);
                userNameLink.text('').addClass(HIDE).removeAttr('data-seller');
                loginSelectorEL.removeClass(HIDE);
                toggleListPropertyHeader(false);
            }
        }
        
        return {
            messages:['setUserName'],
            init: function() {
                moduleEl = context.getElement();
                $moduleEl = $(moduleEl);
                isMobile = utils.isMobileRequest(),
                onSellerJourneyPage = window.location.pathname.includes(config.sellerJourneyUrl);
                userImageLink = $moduleEl.find(UserImageSelector).find(AvatarDataType);
                userNameLink = $moduleEl.find(UserNameSelector).find(UserNameDataType);
            },

            onmessage: function(name, data) {
                switch(name){
                    case 'setUserName':
                        if(data.moduleId != moduleEl.id){
                            return;
                        }
                        _setUserName(data.status, data.data);
                        break;
                }
            },
            onclick: function(event, element, elementType){
                let  clickElementData = $(element).data() || {};
                switch(elementType){
                    case 'login':
                        context.broadcast('trackLoginClick',clickElementData);
                        loginService.openLoginPopup();
                        break;
                    case 'track-my-journey':
                        if (!isLoggedIn && onSellerJourneyPage) {
                            loginService.openLoginPopup();
                        }
                        if (isSellerLoggedIn) {
                            context.broadcast('trackMyDashboardClick',clickElementData);
                        }
                        context.broadcast('trackMyJourneyClick',clickElementData);
                        break;
                }
            }
        };
    });
});
