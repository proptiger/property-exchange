define([
    'common/trackingConfigService',
    'services/trackingService',
    'services/utils'
], function(t, trackingService, utils) {
    'use strict';
    Box.Application.addBehavior('localityOverviewTracking', function() {

        let event,
            category,
            label,
            labelConfig = t.LEAD_LABELS;

        const AVAILABLE_PROP_LINK = 'available-prop-link',
            TAXONOMY_CARD_LINK = 'taxonomy-card-link',
            PRICE_TREND_LINK = 'price-trend-link',
            PROJECT_CLASS_LINK = 'project-classification-link',
            PROJECT_CLASS_MORE = 'project-classification-view-more',
            BUILDER_INTERACTION = 'builder_interaction',
            BUILER_CLICK = 'builder_click',
            SELLER_INTERACTION = "seller_interaction",
            SELLER_CLICK = "seller_click",
            NEARBY_LOCALITY_INT = "nearby_locality_interaction",
            NEARBY_LOCALITY_CLICK = 'nearby_locality_click',
            PAGE_CATEGORY_TAB = "page-category-tab",
            PROPERTY_TYPE_TAB = 'propertytype_tab',
            PRICE_TREND_TAB = 'price-trend-tab',
            TOP_SELLER_TAB = 'top-seller-tabs',
            LOCALITY_COMPARE_TAB = 'localityCompare-tab',
            LIFESTYLE_TAB = 'lifestyle-tab',
            LIFESTYLE_SUB_TAB = 'lifestyle-sub-tab',
            LIFESTYLE_BULLET = 'lifestyle-slider-bullet',
            OPEN_LEAD_VIEW_PHONE = 'openLeadFormViewPhone',
            NAVIGATION = 'navigation',
            LOCALITY_SCORE_POPUP = 'locality-score-popup',
            TRACK_SPONSORED_AUTO_SCROLL = 'trackLocalitySponsoredAutoScroll',
            TRACK_SPONSORED_AUTO_SCROLL_MOBILE = 'trackLocalitySponsoredAutoScrollMobile',
            backbtnClick = 'blogLocalityBackbtnClick',
            nextbtnClick = 'blogLocalityNextbtnClick',
            localityPostClick = 'localityPostClick',
            localityBloglogoClick = 'localityBloglogoClick';

        var messages = [TRACK_SPONSORED_AUTO_SCROLL_MOBILE, TRACK_SPONSORED_AUTO_SCROLL, AVAILABLE_PROP_LINK, TAXONOMY_CARD_LINK, PRICE_TREND_LINK, PROJECT_CLASS_LINK, PROJECT_CLASS_MORE, BUILDER_INTERACTION, SELLER_INTERACTION, NEARBY_LOCALITY_INT, PAGE_CATEGORY_TAB, PROPERTY_TYPE_TAB, PRICE_TREND_TAB, TOP_SELLER_TAB, LOCALITY_COMPARE_TAB, LIFESTYLE_TAB, LIFESTYLE_SUB_TAB, SELLER_CLICK, BUILER_CLICK, NEARBY_LOCALITY_CLICK, LIFESTYLE_BULLET, OPEN_LEAD_VIEW_PHONE, NAVIGATION, LOCALITY_SCORE_POPUP, backbtnClick, nextbtnClick, localityPostClick, localityBloglogoClick,"linkClicked","trackCallNowSubmit"];


        var    onmessage = function(eventName, data={}) {
                var name, listingCategory, unitType, bhk, labelValue, budget, rank, projectStatus, source, sellerId,
                    localityId,
                    cityId,
                    projectId,
                    builderId,
                    nonInteraction,
                    temp;
                switch (eventName) {
                    case AVAILABLE_PROP_LINK:
                        event = t.CLICK_EVENT;
                        category = t.CONFIGURATION_CATEGORY;
                        name = data.name;
                        listingCategory = data.category;
                        unitType = data.unitType;
                        bhk = data.bhk;
                        break;
                    case "linkClicked":
                        event = t.CLICK_EVENT;
                        break;    
                    case TAXONOMY_CARD_LINK:
                        event = t.CLICK_EVENT;
                        category = t.PROPERTIES_IN_AREA_CATEGORY;
                        label = data.label;
                        name = data.name;
                        budget = data.budget;
                        rank = data.rank;
                        listingCategory = data.dominantUnit;
                        break;
                    case PRICE_TREND_LINK:
                        event = t.CLICK_EVENT;
                        label = data.label;
                        listingCategory = data.category;
                        name = data.name;
                        break;
                    case PROJECT_CLASS_LINK:
                        event = t.CLICK_EVENT;
                        label = data.label;
                        labelValue = data.value;
                        listingCategory = data.dominantUnit;
                        projectStatus = data.projectStatus;
                        break;
                    case PROJECT_CLASS_MORE:
                        event = t.CLICK_EVENT;
                        label = data.label + ' view more';
                        labelValue = data.value;
                        listingCategory = data.dominantUnit;
                        projectStatus = data.projectStatus;
                        break;
                    case BUILDER_INTERACTION:
                        event = t.INTERACTION_EVENT;
                        label = data.label;
                        listingCategory = data.dominantUnit;
                        category = t.BUILDERS_CATEGORY;
                        break;
                    case BUILER_CLICK:
                        event = t.CLICK_EVENT;
                        listingCategory = data.dominantUnit;
                        category = t.BUILDERS_CATEGORY;
                        break;
                    case SELLER_INTERACTION:
                        event = t.INTERACTION_EVENT;
                        label = data.label;
                        listingCategory = data.listingCategory;
                        category = t.TOP_SELLER_CATEGORY;
                        break;
                    case SELLER_CLICK:
                        event = t.CLICK_EVENT;
                        listingCategory = data.listingCategory;
                        category = t.TOP_SELLER_CATEGORY;
                        name = data.name;
                        break;
                    case NEARBY_LOCALITY_INT:
                        event = t.INTERACTION_EVENT;
                        label = data.label;
                        listingCategory = data.dominantUnit;
                        category = t.NEARBY_LOCALITY_CATEGORY;
                        break;
                    case NEARBY_LOCALITY_CLICK:
                        event = t.CLICK_EVENT;
                        label = data.label;
                        category = t.NEARBY_LOCALITY_CATEGORY;
                        //name =
                        break;
                    case PAGE_CATEGORY_TAB:
                        event = t.INFO_TOGGLE;
                        category = t.HEADLINE_CATEGORY;
                        listingCategory = data.element.dataset.pageCategory;
                        break;
                    case PROPERTY_TYPE_TAB:
                        event = t.INFO_TOGGLE;
                        listingCategory = data.listingCategory;
                        category = t.CONFIGURATION_CATEGORY;
                        unitType = data.unitType;
                        break;
                    case PRICE_TREND_TAB:
                        event = t.INFO_TOGGLE;
                        listingCategory = data.listingCategory;
                        category = t.PRICE_TREND_CATEGORY;
                        break;
                    case TOP_SELLER_TAB:
                        event = t.INFO_TOGGLE;
                        category = t.TOP_SELLER_CATEGOTY;
                        listingCategory = data.listingCategory;
                        break;
                    case LOCALITY_COMPARE_TAB:
                        event = t.INFO_TOGGLE;
                        category = t.COMPARE_CATEGORY;
                        listingCategory = data.listingCategory;
                        break;
                    case LIFESTYLE_TAB:
                        event = t.INFO_TOGGLE;
                        category = t.LIFESTYLE_CATEGORY;
                        name = data.name;
                        listingCategory = data.dominantUnit;
                        break;
                    case LIFESTYLE_SUB_TAB:
                        name = data.name;
                        category = t.LIFESTYLE_CATEGORY;
                        event = t.SUB_MENU;
                        label = data.label;
                        break;
                    case LIFESTYLE_BULLET:
                        event = t.INTERACTION_EVENT;
                        category = t.LIFESTYLE_CATEGORY;
                        label = 'Image';
                        name = data.name;
                        break;

                    case OPEN_LEAD_VIEW_PHONE:
                        category = t.LEAD_FORM_CATEGORY;
                        event = t.OPEN_EVENT;
                        label = labelConfig.ViewPhone;
                        if(utils.isMobileRequest()) {
                            label = labelConfig.CallNow;
                        }
                        break;

                    case 'trackCallNowSubmit':
                        event = t.SUBMIT_EVENT;
                        label = labelConfig.CallNow;
                        category = t.LEAD_FORM_CATEGORY;
                        break;
                            
                    case NAVIGATION:
                        label = data.label;
                        category = t.STICKY_MENU;
                        event = t.CLICK_EVENT;
                        break;
                    case LOCALITY_SCORE_POPUP:
                        label = data.label;
                        category = t.POPUP_CATEGORY;
                        event = t.CLICK_EVENT;
                        break;

                    case TRACK_SPONSORED_AUTO_SCROLL:
                        category = t.BP_CAROUSEL_CATEGORY;
                        source = t.BANNER_MODULE;
                        event = t.SCROLL_EVENT;
                        projectStatus = data.projectStatus;
                        unitType = (data.unitType || []).join();
                        bhk = (data.bedrooms || []).join();
                        budget = (data.rawPrice || []).join();
                        localityId = data.localityId;
                        cityId = data.cityId;
                        projectId = data.projectId;
                        builderId = data.builderId;
                        nonInteraction = 1;
                        break;

                    case TRACK_SPONSORED_AUTO_SCROLL_MOBILE:
                        temp = data && data.data;
                        if(temp){
                            category = t.BP_CAROUSEL_CATEGORY;
                            source = t.BANNER_MODULE;
                            event = t.SCROLL_EVENT;
                            projectStatus = temp.projectStatus;
                            unitType = (temp.unitType || []).join();
                            bhk = (temp.bedrooms || []).join();
                            budget = (temp.rawPrice || []).join();
                            localityId = temp.localityId;
                            cityId = temp.cityId;
                            projectId = temp.projectId;
                            builderId = temp.builderId;
                        }
                        break;
                    case backbtnClick:
                        category = t.LOCALITY_BLOG;
                        event = t.CLICK_EVENT;
                        label = t.BACK_LABEL;
                        break;

                    case nextbtnClick:
                        category = t.LOCALITY_BLOG;
                        event = t.CLICK_EVENT;
                        label = t.NEXT_LABEL;
                        break;

                    case localityPostClick:
                        category = t.LOCALITY_BLOG;
                        event = t.CLICK_EVENT;
                        label = data;
                        break;

                    case localityBloglogoClick:
                        category = t.LOCALITY_BLOG;
                        event = t.CLICK_EVENT;
                        label = t.LOGO;
                        break;
                }

                let properties = {};

                properties[t.CATEGORY_KEY] = category;
                properties[t.NAME_KEY] = name;
                properties[t.LISTING_CATEGORY_KEY] = t.makeValuesUniform(listingCategory,t.LISTING_CATEGORY_MAP) ;
                properties[t.UNIT_TYPE_KEY] = t.makeValuesUniform(unitType,t.UNIT_TYPE_MAP) ;
                properties[t.BHK_KEY] = t.makeValuesUniform(bhk,t.BHK_MAP) ;
                properties[t.BUDGET_KEY] = budget;
                properties[t.LABEL_KEY] = label;
                properties[t.VALUE_KEY] = labelValue;
                properties[t.RANK_KEY] = rank;
                properties[t.PROJECT_STATUS_KEY] = t.makeValuesUniform(projectStatus,t.PROJECT_STATUS_NAME) ;

                properties[t.SOURCE_MODULE_KEY] = source;
                properties[t.SELLER_ID_KEY] = sellerId;

                properties[t.CITY_ID_KEY] = cityId;
                properties[t.PROJECT_ID_KEY] = projectId;
                properties[t.BUILDER_ID_KEY] = builderId;
                properties[t.UNIT_TYPE_KEY] = t.makeValuesUniform(data.propertyType,t.UNIT_TYPE_MAP) ;
                properties[t.NON_INTERACTION_KEY] = nonInteraction;
                properties[t.LINK_NAME_KEY] = data.linkName;
                properties[t.LINK_TYPE_KEY] = data.linkType;

                if(event){
                    trackingService.trackEvent(event, properties);
                }
            };


        var onclick = function(event, clickElement, elementType){
            let sourceModule,value;

            let clickElementData = $(clickElement).data();
            let amenityLabels = t.MAP_LABELS;
            let properties = {};
            switch(elementType){
                case 'tab':
                    if(clickElementData.trackType === 'map-tabs'){
                        event = t.INFO_TOGGLE;
                        category = t.MAPS_CATEGORY;
                        label = amenityLabels[clickElementData.trackLabel];
                    }
                    break;

                default:
                return;
            }
            
            properties[t.CATEGORY_KEY] = category;
            properties[t.LABEL_KEY] = label;
            properties[t.SOURCE_MODULE_KEY] = sourceModule;
            properties[t.VALUE_KEY] = value;

            if(event){
                trackingService.trackEvent(event, properties);
            }
        };
        return {
            messages: messages,
            onmessage: onmessage,
            init: function() {},
            destroy: function() {},
            onclick
        };
    });
});
