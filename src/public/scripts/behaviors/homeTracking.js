define([
    'common/trackingConfigService',
    'services/trackingService'
], function(t, trackingService) {
    'use strict';
    Box.Application.addBehavior('homeTracking', function() {

        var messages = [
            'trackHomeCategoryOptionClick',
            'trackGetAppClickHome',
            'trackGetAppErrorHome',
            'homePostClick',
            'blogLogoClick',
            'sellPropertyClick',
            'homepageSectionVisible',
            'trackMoveToSearchClicked',
            'trackHomePageUserCity',
            'trackPopularLocalityDropdown',
            'trackPopularLocalityClicked',
            'trackListingSnapshotsClick',
            'trackRecentActivityTabs'
        ];

        var onmessage = function(eventName, data) {
            let event = t.NAVIGATION_EVENT,
                category = t.HEADER_CATEGORY,
                label,
                sourceModule,
                name,
                projectStatus,
                budget,
                bhk,
                localityId,
                cityId,
                projectId,
                builderId,
                unitType,
                nonInteraction,
                phone,
                sectionClicked;

            switch (eventName) {
                case 'trackHomeCategoryOptionClick':
                    label = t.LISTING_CATEGORY_MAP[data.listingType.toUpperCase()];
                    break;
                case 'trackGetAppClickHome':
                    category = t.ALL_LINK_CATEGORYL;
                    event = t.CLICK_EVENT;
                    label = t.GET_APP_LABEL;
                    phone = data.phone;
                    break;

                case 'trackGetAppErrorHome':
                    category = t.ALL_LINK_CATEGORYL;
                    event = t.ERROR_EVENT;
                    label = data.label;
                    phone = data.phone;
                    nonInteraction = 1;
                    break;

                case 'homePostClick':
                    category = t.HOME_BLOG;
                    event = t.CLICK_EVENT;
                    label = data;
                    break;

                case 'blogLogoClick':
                    category = t.HOME_BLOG;
                    event = t.CLICK_EVENT;
                    label = t.LOGO;
                    break;
                case 'sellPropertyClick':
                    category = 'sell-rent-property';
                    event = t.CLICK_EVENT;
                    break;
                case 'homepageSectionVisible':
                    category = t.HOME_LABEL;
                    event = t.SCROLL_EVENT;
                    label = data.label;
                    nonInteraction=1;
                    break;
                case 'trackMoveToSearchClicked':
                    category = 'scrollup';
                    event = t.CLICK_EVENT;
                    label = data.label;
                    break;
                case 'trackHomePageUserCity':
                    category = 'HomePageBenefits';
                    event = 'Citydetails';
                    label = data && data.userIP;
                    nonInteraction = 1;
                    break;
                case 'trackPopularLocalityDropdown':
                    category = 'homepage_drowpdown';
                    event = t.CLICK_EVENT;
                    label = data.label;
                    break;
                case 'trackPopularLocalityClicked':
                    category = 'homepage_popular';
                    event = t.CLICK_EVENT;
                    label = data.label;
                    sectionClicked = 'popularLocalities';
                    break;
                case 'trackListingSnapshotsClick':
                    category = 'homepage_link';
                    event = t.CLICK_EVENT;
                    label = data.label;
                    sectionClicked = data.label;
                    break;
                case 'trackRecentActivityTabs':
                    category = 'homepage_tab';
                    event = t.CLICK_EVENT;
                    label = data.label;
                    break;
            }

            let properties = {};
            properties[t.CATEGORY_KEY] = category;
            properties[t.LABEL_KEY] = label;

            properties[t.SOURCE_MODULE_KEY] = sourceModule;
            properties[t.PROJECT_NAME_KEY] = name;
            properties[t.PROJECT_STATUS_KEY] = t.makeValuesUniform(projectStatus,t.PROJECT_STATUS_NAME) ;
            properties[t.BUDGET_KEY] = budget;
            properties[t.BHK_KEY] = t.makeValuesUniform(bhk,t.BHK_MAP);
            properties[t.LOCALITY_ID_KEY] = localityId;
            properties[t.CITY_ID_KEY] = cityId;
            properties[t.PROJECT_ID_KEY] = projectId;
            properties[t.BUILDER_ID_KEY] = builderId;
            properties[t.UNIT_TYPE_KEY] = t.makeValuesUniform(unitType,t.UNIT_TYPE_MAP) ;
            properties[t.NON_INTERACTION_KEY] = nonInteraction;
            properties[t.PHONE_USER_KEY] = phone;
            if(data && data.userCity){
                properties[t.VISITOR_CITY_KEY] = data.userCity;
            }
            properties[t.SECTION_CLICKED] = sectionClicked;
            trackingService.trackEvent(event, properties);
        };

        return {
            messages: messages,
            onmessage: onmessage,
            init: function() {},
            destroy: function() {}
        };
    });
});
