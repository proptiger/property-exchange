define([
    'common/trackingConfigService',
    'services/trackingService'
], function(t, trackingService) {
    'use strict';
    Box.Application.addBehavior('trendTracking', function(context) {

        var messages = ['trackTrendDescClick', 'trackTrendTableClick', 'trackTrendTabClick'],
            event, category, label;


        onmessage = function(name, data) {
            let event, category, label, nonInteraction;
            switch (name) {
                case 'trackTrendDescClick':
                    category = t.DESCRIPTION_CATEGORY;
                    event = t.CLICK_EVENT;
                    label = data.source;
                    break;
                case 'trackTrendTableClick':
                    category = t.PRICE_TREND_CATEGORY;
                    event = t.CLICK_EVENT;
                    label = data.source;
                    break;
                case 'trackTrendTabClick':
                    category = t.HEADER_CATEGORY;
                    event = t.NAVIGATION_EVENT;
                    label = data.source;
                    break;
            }

            let properties = {};
            properties[t.CATEGORY_KEY] = category;
            properties[t.LABEL_KEY] = label;
            if (event) {
                trackingService.trackEvent(event, properties);
            }
        };

        return {
            messages: messages,
            onmessage: onmessage,
            init: function() {},
            destroy: function() {}
        };
    });
});
