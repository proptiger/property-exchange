define([
    'common/trackingConfigService',
    'services/trackingService'
], function(t, trackingService) {
    'use strict';
    Box.Application.addBehavior('serpTracking', function() {

        var messages = [
            'trackBuyRentCardClick',
            'trackMapCardClick',
            'trackPyrCardClick',
            'trackJarvisCardClick',
            'trackDownloadCardClick',
            'trackJourneyCardClick',
            'trackSimilarCardClick',
            'trackSimilarCardHover',
            'trackSerpPyrCardFail',
            'serpPostClick',
            'serpBlogNextbtnClick',
            'serpBlogBackbtnClick',
            'seeMoreClick',
            'trackSellerProfileTooltip',
            'trackSellerProfileConnect',
            'trackSellerScoreTooltipClicked',
            'trackSelectKnowMore',
            'msKnowMorePopupClose',
            'selectBannerSeen',
            'selectListingSeen',
            'trackPopupSearchProperties',
            'trackPopupUnlockVoucher',
            'selectReminderSeen',
            'selectListingSeen',
            'trackReminderUnlock',
            'trackDeficitSellerListingSeen',
            'trackPrivilegeCardSeen',
            'trackPrivilegeCardClick'
        ];

        var onmessage = function(name, data={}) {
            let event = t.NAVIGATION_EVENT,
                category = t.RHS_CATEGORY,
                label, sourceModule, nameField,linkType,linkName, sellerId, sellerRating, sellerStatus,sectionClicked,
                changeSource = true,nonInteraction,listingId,
                properties = {}, pageType, listingType, cityId, sellerType;

            switch (name) {
                case 'seeMoreClick':
                    category = t.SERP_BLOG;
                    event = t.CLICK_EVENT;
                    label = t.MORE_LABEL;
                    break;
                case 'serpBlogBackbtnClick':
                    category = t.SERP_BLOG;
                    event = t.CLICK_EVENT;
                    label = t.BACK_LABEL;
                    break;
                case 'serpBlogNextbtnClick':
                    category = t.SERP_BLOG;
                    event = t.CLICK_EVENT;
                    label = t.NEXT_LABEL;
                    break;
                case 'serpPostClick':
                    category = t.SERP_BLOG;
                    event = t.CLICK_EVENT;
                    label = data;
                    break;
                case 'trackBuyRentCardClick':
                    label = t.LISTING_CATEGORY_MAP[data.listingType.toUpperCase()];
                    sourceModule = t.BUY_RENT_CARD_MODULE;
                    break;
                case 'trackMapCardClick':
                    sourceModule = t.MAP_CARD_MODULE;
                    break;
                case 'trackPyrCardClick':
                    sourceModule = t.PYR_CARD_MODULE;
                    break;
                case 'trackSerpPyrCardFail':
                    label = 'trackSerpPyrCardFail';
                    sourceModule = t.PYR_CARD_MODULE;
                    break;
                case 'trackJarvisCardClick':
                    sourceModule = t.JARVIS_CARD_MODULE;
                    break;
                case 'trackDownloadCardClick':
                    sourceModule = t.DOWNLOAD_CARD_MODULE;
                    break;
                case 'trackJourneyCardClick':
                    sourceModule = t.JOURNEY_CARD_MODULE;
                    break;
                case 'trackSimilarCardHover':
                    event = t.HOVER_ON_CARD_EVENT;
                    nameField = data.id;
                    label = data.hoverTime;
                    if(data.type == "similarProject"){
                        sourceModule = t.SIMILAR_PROJECT;
                    }else if(data.type == "topLocality"){
                        sourceModule = t.TOP_LOCALITY;
                    }else if(data.type == "similarLocality"){
                        sourceModule = t.SIMILAR_LOCALITY;
                    }
                    break;
                case 'trackSimilarCardClick':
                    event = t.CLICK_EVENT;
                    label = data.id;
                    if(data.type == "similarProject"){
                        sourceModule = t.SIMILAR_PROJECT;
                    }else if(data.type == "topLocality"){
                        sourceModule = t.TOP_LOCALITY;
                    }else if(data.type == "similarLocality"){
                        sourceModule = t.SIMILAR_LOCALITY;
                    }
                    linkName = data.linkName;
                    linkType = data.linkType;
                    break;
                case 'trackSellerProfileTooltip':
                    category=t.DEAL_REVIEW_CATEGORY;
                    event= t.TOOLTIP_EVENT;
                    label=data.label;
                    sellerId = data.sellerId;
                    sellerStatus = data.sellerStatus=='paid'?t.SELLER_PAID_STATUS.PAID:t.SELLER_PAID_STATUS.NOT_PAID;
                    sellerRating = data.sellerRating;
                    break;
                case 'trackSellerProfileConnect':
                    event=t.OPEN_EVENT;
                    label=data.label;
                    category='Lead Form';
                    sellerId = data.sellerId;
                    sellerStatus = data.sellerStatus=='paid'?t.SELLER_PAID_STATUS.PAID:t.SELLER_PAID_STATUS.NOT_PAID;
                    sellerRating = data.sellerRating;
                    sourceModule = data.source;
                    sectionClicked = 'sellerProfileCardConnect';
                    break;
                case 'trackSellerScoreTooltipClicked':
                    event = 'Tap';
                    label = 'sellerScoreMainProfile';
                    category ='dealreview';
                    break;
                case 'trackSelectKnowMore':
                    event = t.CARD_CLICK;
                    label="Program_Details_Modal";
                    category=t.SELECT_VOUCHER_GENERATION_CATEGORY;
                    sourceModule = data.source;
                    changeSource = false;
                    break;
                case 'msKnowMorePopupClose':
                    event = t.CLOSE_EVENT;
                    label="Program_Details_Modal";
                    category=t.SELECT_VOUCHER_GENERATION_CATEGORY;
                    break;
                case 'selectBannerSeen':
                    event = t.CARD_SEEN;
                    label="Select_Banner";
                    category=t.SELECT_VOUCHER_GENERATION_CATEGORY;
                    sourceModule = data.source || "Serp";
                    changeSource = false;
                    nonInteraction = 1;
                    break;
                case 'selectListingSeen':
                    event = 'Listing Seen';
                    label="Seller_Promo_Serp";
                    category=t.SELECT_VOUCHER_GENERATION_CATEGORY;
                    sourceModule = data.source;
                    changeSource = false;
                    nonInteraction = 1;
                    break;
                case 'trackPopupSearchProperties':
                    event = t.CLICK_EVENT;
                    label="Search_Select_Properties";
                    category=t.SELECT_VOUCHER_GENERATION_CATEGORY;
                    break;
                case 'trackPopupUnlockVoucher':
                    event = t.CLICK_EVENT;
                    label="Unlock_Select_Voucher";
                    category=t.SELECT_VOUCHER_GENERATION_CATEGORY;
                    break;
                case 'selectReminderSeen':
                    event = t.CARD_SEEN;
                    label="Select_Reminder";
                    category=t.SELECT_VOUCHER_GENERATION_CATEGORY;
                    sourceModule = data.source || "Serp";
                    changeSource = false;
                    break;
                case 'trackReminderUnlock':
                    event = t.CLICK_EVENT;
                    label="Select_Reminder_Unlock";
                    category=t.SELECT_VOUCHER_GENERATION_CATEGORY;
                    sourceModule = data.source || "Serp";
                    changeSource = false;
                    break;
                case 'trackDeficitSellerListingSeen':
                    event = t.CARD_SEEN;
                    category = 'Deficit Campaign Sellers';
                    label = 'card_seen_nearby';
                    listingId = data.listingId;
                    sellerId = data.sellerId;
                    sourceModule = data.nearbyIndex;
                    changeSource = false;
                    break;
                case 'trackPrivilegeCardSeen':
                    event = t.CARD_SEEN;
                    category = 'No_Brokerage_Agents';
                    label = 'privilege_seller';
                    pageType = data.pageType;
                    listingType = data.listingType;
                    cityId = data.cityId;
                    nonInteraction = 1;
                    break;
                case 'trackPrivilegeCardClick':
                    event = t.CARD_CLICK;
                    category = 'No_Brokerage_Agents';
                    label = data.sellerId;
                    pageType = data.pageType;
                    listingType = data.listingType;
                    cityId = data.cityId;
                    listingId = data.listingId;
                    sellerId = data.sellerId;
                    sellerType = data.sellerType;
                    break;
            }
            properties[t.NAME_KEY] = nameField;
            properties[t.CATEGORY_KEY] = category;
            properties[t.LABEL_KEY] = label;
            properties[t.SOURCE_MODULE_KEY] = sourceModule;
            properties[t.LINK_NAME_KEY] = linkName && linkName.toLowerCase();
            properties[t.LINK_TYPE_KEY] = linkType && linkType.toLowerCase();
            properties[t.NON_INTERACTION_KEY] = nonInteraction;
            properties[t.SECTION_CLICKED] = sectionClicked;
            if (pageType){
                properties[t.PAGE_KEY] = pageType;
            }
            if (listingType){
                properties[t.LISTING_CATEGORY_KEY] = listingType;
            }
            if (cityId) {
                properties[t.CITY_ID_KEY] = cityId;
            }
            if (sellerType) {
                properties[t.SELLER_TYPE_KEY] = data.sellerType;
            }
            if(sellerId){
                properties[t.SELLER_ID_KEY] = sellerId;
            }
            if(sellerRating){
                properties[t.SELLER_SCORE_KEY] = sellerRating;
            }
            if(sellerStatus){
                properties[t.SELLER_STATUS_KEY] = sellerStatus;
            }
            if(!data.isOriginalSearchResult && changeSource){
                properties[t.SOURCE_MODULE_KEY] = 'Targetted';
            }
            if(listingId){
                properties[t.LISTING_ID_KEY] = listingId;
            }
            trackingService.trackEvent(event, properties);
        };

        return {
            messages: messages,
            onmessage: onmessage,
            init: function() {},
            destroy: function() {}
        };
    });
});
