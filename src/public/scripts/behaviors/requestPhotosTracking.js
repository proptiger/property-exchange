define([
    'common/trackingConfigService',
    'services/trackingService'
], function(t, trackingService) {
    'use strict';
    Box.Application.addBehavior('requestPhotosTracking', function() {

    	function trackRequestPhotosClick() {
    		let event = t.CLICK_EVENT, properties = {};
    		properties[t.CATEGORY_KEY] = t.IMAGE_EXPERIENCE_CATEGORY;
    		properties[t.LABEL_KEY] = t.NO_IMAGE_LABEL;
    		trackingService.trackEvent(event, properties);
    	}

    	return {
    		onclick: function(event, element, elementType) {
    			if(elementType === 'dummy-img'){
    				trackRequestPhotosClick();
    			}
    		}
    	}
    });
});