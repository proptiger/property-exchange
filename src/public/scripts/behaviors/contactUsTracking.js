define([
    'common/trackingConfigService',
    'services/trackingService'
], function(t, trackingService) {
    'use strict';
    Box.Application.addBehavior('contactUsTracking', function() {

        var messages = [
            'trackSuccessContactUs',
            'trackErrorContactUs'
        ];

        var onmessage = function(eventName, data) {
            let event,
                category,
                label,
                nonInteraction,
                page;

            switch (eventName) {
                case 'trackSuccessContactUs':
                    category = t.CONTACT_US_CATEGORY;
                    event = t.CLICK_EVENT;
                    label = t.SUBMIT_LABEL;
                    page = t.PAGE_TYPE_MAP && t.PAGE_TYPE_MAP.HOME_PAGE_URLS;
                    break;

                case 'trackErrorContactUs':
                    category = t.CONTACT_US_CATEGORY;
                    event = t.ERROR_EVENT;
                    label = data.label;
                    page = t.PAGE_TYPE_MAP && t.PAGE_TYPE_MAP.HOME_PAGE_URLS;
                    nonInteraction = 1;
                    break;

            }

            let properties = {};
            properties[t.CATEGORY_KEY] = category;
            properties[t.LABEL_KEY] = label;
            properties[t.PAGE_KEY] = page;
            properties[t.NON_INTERACTION_KEY] = nonInteraction;

            trackingService.trackEvent(event, properties);
        };

        return {
            messages: messages,
            onmessage: onmessage,
            init: function() {},
            destroy: function() {}
        };
    });
});
