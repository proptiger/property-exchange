define([
    'common/trackingConfigService',
    'services/trackingService'
], function(t, trackingService) {
    'use strict';
    Box.Application.addBehavior('mediaStaticTracking', function() {

        var messages = [
                'trackNewsHeaderNewsBytesClick',
                'trackNewsHeaderPressReleasesClick',
                'trackNewsHeaderMediaResourcesClick',
                'trackNewsMediaImageDownload',
                'trackNewsPaginationClick',
            ],
            event, category, label;

        var onmessage = function(name, data) {
            //let event, category, label;
            switch (name) {
                case 'trackNewsHeaderNewsBytesClick':
                    category = t.MEDIA_HEADER;
                    event = t.MEDIA_NEWS_BYTES;
                    label = t.MEDIA_MEDIA_RESOURCES;
                    break;
                case 'trackNewsHeaderPressReleasesClick':
                    category = t.MEDIA_HEADER;
                    event = t.MEDIA_PRESS_RELEASE;
                    label = t.MEDIA_MEDIA_RESOURCES;
                    break;
                case 'trackNewsHeaderMediaResourcesClick':
                    category = t.MEDIA_HEADER;
                    event = t.MEDIA_MEDIA_RESOURCES;
                    label = t.MEDIA_MEDIA_RESOURCES;
                    break;
                case 'trackNewsMediaImageDownload':
                    category = t.MEDIA_MEDIA_RESOURCES;
                    event = 'downloadLogo';
                    label = data.text;
                    break;
                case 'trackNewsPaginationClick':
                    category = 'pagination';
                    event = 'pagination';
                    label = t.MEDIA_MEDIA_RESOURCES;
                    break;
            }

            let properties = {};
            properties[t.CATEGORY_KEY] = category;
            properties[t.LABEL_KEY] = label;
            if (event) {
                trackingService.trackEvent(event, properties);
            }
        };

        return {
            messages: messages,
            onmessage: onmessage,
            init: function() {},
            destroy: function() {}
        };
    });
});
