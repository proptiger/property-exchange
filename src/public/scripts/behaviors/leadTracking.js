define([
    'common/trackingConfigService',
    'services/trackingService',
    'services/urlService'
], function(t, trackingService, urlService) {
    'use strict';
    Box.Application.addBehavior('leadTracking', function(context) {

        var element;
        const messages = [
            'trackOpenPyr',
            'trackFormFillPyr',
            'trackSubmitPyr',
            'trackViewPhonePyr',
            'trackConnectNowPyr',
            'trackCallNowPyr',
            'trackBackButtonPyr',
            'trackClosePyr',
            'trackDismissPyr',
            'trackResendOtpPyr',
            'trackOtpCorrectPyr',
            'trackOtpIncorrectPyr',
            'trackShowOtpPyr',
            'trackPyrError',
            'trackLeadContinueHomeloan',
            'trackLeadContinueSearch',
            'Lead_PYR_API_SUCCESS',
            'Lead_PYR_API_FAILURE'
        ];

        var init = function() {
            element = context.getElement();
        };

        var onmessage = function(name, data) {
            if (data.id === element.id) {
                let properties = {};
                let category = t.LEAD_FORM_CATEGORY,
                    labelConfig = t.LEAD_LABELS,
                    sourceModuleConfig = t.LEAD_SOURCE_MODULES,
                    trackData = data.trackData,
                    value = urlService.getUrlParam('page') || 1,
                    event, label, sourceModule, nonInteraction,uniqueIds,sectionClicked;

                switch (name) {
                    case 'trackOtpCorrectPyr':
                        event = t.OTP_EVENT;
                        label = labelConfig['Correct'];
                        break;

                    case 'trackOtpIncorrectPyr':
                        event = t.OTP_EVENT;
                        label = labelConfig['Incorrect'];
                        break;

                    case 'trackShowOtpPyr':
                        event = t.OTP_EVENT;
                        label = labelConfig['Visible'];
                        break;

                    case 'trackResendOtpPyr':
                        event = t.OTP_EVENT;
                        label = labelConfig['Resend'];
                        break;

                    case 'trackOpenPyr':
                        event = t.OPEN_EVENT;
                        label = labelConfig[trackData.id];
                        sectionClicked = 'pyrLeadForm';
                        break;

                    case 'trackFormFillPyr':
                        event = t.FORM_FILLED_EVENT;
                        label = labelConfig[trackData.id];
                        break;

                    case 'trackSubmitPyr':
                        event = t.SUBMIT_EVENT;
                        label = labelConfig[trackData.enquiryType] || labelConfig.PYR;
                        break;

                    case 'trackViewPhonePyr':
                        event = t.SUBMIT_EVENT;
                        label = labelConfig.ViewPhone;
                        break;

                    case 'trackConnectNowPyr':
                        event = t.SUBMIT_EVENT;
                        label = labelConfig.ConnectNow;
                        break;

                    case 'trackCallNowPyr':
                        event = t.SUBMIT_EVENT;
                        label = labelConfig.CallNow;
                        break;

                    case 'trackBackButtonPyr':
                        event = t.NAVIGATION_EVENT;
                        label = labelConfig[trackData.id] + '_' + trackData.currentStep;
                        value = t.LEAD_VALUES.Back;
                        break;

                    case 'trackDismissPyr':
                        event = t.NAVIGATION_EVENT;
                        label = labelConfig[trackData.id] + '_' + trackData.currentStep;
                        value = t.LEAD_VALUES.Dismiss;
                        break;

                    case 'trackClosePyr':
                        event = t.NAVIGATION_EVENT;
                        label = labelConfig[trackData.id] + '_' + trackData.currentStep;
                        value = t.LEAD_VALUES.Close;
                        break;

                    case 'trackPyrError':
                        category = t.LEAD_FORM_ERROR_CATEGORY;
                        event = t.ERROR_EVENT;
                        label = data.label;
                        nonInteraction = 0;
                        break;
                    case 'trackLeadContinueHomeloan' :
                        event = 'continue';
                        label = "homeloanYes" ;
                        break;
                    case 'trackLeadContinueSearch' :
                        event = 'continue';
                        label = "homeloanNo" ;
                        break;
                    case 'Lead_PYR_API_SUCCESS':
                        event = t.LEAD_API_SUCCESS_EVENT;
                        label = 'PYR Lead Form';
                        uniqueIds = data && data.uniqueIds;
                        break;
                    case 'Lead_PYR_API_FAILURE':
                        event = t.LEAD_API_FAILURE_EVENT;
                        label = 'PYR Lead Form';
                        uniqueIds = data && data.uniqueIds;
                        break;

                    default:
                        return;
                }

                sourceModule = sourceModuleConfig[trackData.id];
                properties = $.extend(true, properties, trackData);

                delete properties.id;
                delete properties.pyrType;
                delete properties.currentStep;
                delete properties.label;

                properties[t.CATEGORY_KEY] = category;
                properties[t.LABEL_KEY] = label;
                properties[t.VALUE_KEY] = value;
                properties[t.SOURCE_MODULE_KEY] = sourceModule;
                properties[t.NON_INTERACTION_KEY] = nonInteraction;
                properties[t.SECTION_CLICKED] = sectionClicked;
                if(uniqueIds && uniqueIds.length){
                    uniqueIds.forEach(function(uniqueId){
                        properties[t.UNIQUE_ENQUIRY_ID_KEY] = uniqueId;
                        trackingService.trackEvent(event, properties);
                    });
                }else{
                    trackingService.trackEvent(event, properties);
                }
            }
        };

        return {
            messages,
            onmessage,
            init,
            destroy: function() {}
        };
    });
});
