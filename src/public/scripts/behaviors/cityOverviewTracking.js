define([
    'common/trackingConfigService',
    'services/trackingService'
], function(t, trackingService) {
    'use strict';
    Box.Application.addBehavior('cityOverviewTracking', function() {

        const PRICE_TREND_GRAPH = 'cityoverview_priceTrendTab';

        var messages = [
                'trackCitySponsoredAutoScroll',
                PRICE_TREND_GRAPH,
                'trackHoverGrowthTooltipsCity',
                'trackPropertyRangeBarClickCityPage',
                'trackPropertyRangeFilters',
                'trackPropertiesInAreaHeadingCityPage',
                'trackPropertiesInAreaCityPage',
                'trackPopularLocalityRentCityPage',
                'trackPopularLocalityBuyCityPage',
                'trackPopularProjectsCityPage',
                'trackCitySponsoredAutoScrollMobile',
                'trackStickyHeaderClickCity',
                'blogCityBackbtnClick',
                'blogCityNextbtnClick',
                'cityPostClick',
                'cityBloglogoClick',
                'trackPopularLocalityPage',
                'trackCityTopLocality'
            ],
            event,
            category,
            label,
            source,
            sellerId,
            labelValue,
            listingType,
            localityId,
            rank,
            projectId,
            projectStatus,
            name,
            unitType,
            budget,
            bhk,
            builderId,
            cityId,
            nonInteraction,
            temp,
            linkName,
            linkType;

        var onmessage = function(eventName, data = {}) {

            switch (eventName) {
                case PRICE_TREND_GRAPH:
                    event = t.TIME_PERIOD_EVENT;
                    category = t.PRICE_TREND_CATEGORY;
                    label = data.label;
                    nonInteraction = 0;

                    break;
                case "trackCityTopLocality":
                    event = t.CLICK_EVENT;
                    linkName = data.linkName;
                    linkType = data.linkType;
                    break;        
                case 'trackPopularLocalityBuyCityPage':
                case 'trackPopularLocalityRentCityPage':
                case 'trackPopularLocalityPage':
                    category = t.POPULAR_IN_CITY_CATEGORY;
                    event = t.LOCALITIES_EVENT;
                    listingType = data.listingType;
                    localityId = data.localityId;
                    rank = data.rank;
                    linkName = data.linkName;
                    linkType = data.linkType;
                    break;

                case 'trackPopularProjectsCityPage':
                    category = t.POPULAR_IN_CITY_CATEGORY;
                    event = t.PROJECTS_CATEGORY;
                    projectStatus = data.projectStatus;
                    localityId = data.localityId;
                    projectId = data.projectId;
                    rank = data.rank;
                    linkName = data.linkName;
                    linkType = data.linkType;
                    break;

                case 'trackPropertiesInAreaCityPage':
                    category = t.PROPERTIES_IN_AREA_CATEGORY;
                    event = t.CLICK_EVENT;
                    rank = data.rank;
                    name = data.name;
                    bhk = data.bhk;
                    budget = data.budget;
                    listingType = data.listingType;
                    unitType = data.unitType;
                    linkName = data.linkName;
                    linkType = data.linkType;
                    break;

                case 'trackPropertiesInAreaHeadingCityPage':
                    category = t.PROPERTIES_IN_AREA_CATEGORY;
                    event = t.CLICK_EVENT;
                    name = data.name;
                    linkName = data.linkName;
                    linkType = data.linkType;
                    break;

                case 'trackPropertyRangeFilters':
                    category = t.PROPERTY_RANGE_CATEGORY;
                    event = t.CITY_CHART_SELECTION_EVENT;
                    label = t.FILTERS_APPLIED_LABEL;
                    unitType = data.unitType;
                    listingType = data.listingType;
                    bhk = data.bhk;
                    nonInteraction = 0;
                    break;    

                case 'trackPropertyRangeBarClickCityPage':
                    category = t.PROPERTY_RANGE_CATEGORY;
                    event = t.CITY_CHART_SELECTION_EVENT;
                    label = t.LISTING_SELECTED_LABEL;
                    labelValue = data.value;
                    unitType = data.unitType;
                    listingType = data.listingType;
                    bhk = data.bhk;
                    nonInteraction = 0;
                    break;

                case 'trackHoverGrowthTooltipsCity':
                    category = t.HEADLINE_CATEGORY;
                    event = t.GROWTH_TOOLTIP_EVENTS;
                    nonInteraction = 1;
                    break;

                case 'trackCitySponsoredAutoScroll':
                    category = t.BP_CAROUSEL_CATEGORY;
                    source = t.BANNER_MODULE;
                    event = t.SCROLL_EVENT;
                    projectStatus = data.projectStatus;
                    bhk = (data.bedrooms || []).join();
                    budget = (data.rawPrice || []).join();
                    localityId = data.localityId;
                    cityId = data.cityId;
                    projectId = data.projectId;
                    builderId = data.builderId;
                    unitType = (data.unitType || []).join();
                    nonInteraction = 1;
                    break;

                case 'trackCitySponsoredAutoScrollMobile':
                    temp = data && data.data;
                    if(temp){
                        category = t.BP_CAROUSEL_CATEGORY;
                        source = t.BANNER_MODULE;
                        event = t.SCROLL_EVENT;
                        projectStatus = temp.projectStatus;
                        unitType = (temp.unitType || []).join();
                        bhk = (temp.bedrooms || []).join();
                        budget = (temp.rawPrice || []).join();
                        localityId = temp.localityId;
                        cityId = temp.cityId;
                        projectId = temp.projectId;
                        builderId = temp.builderId;
                    }
                    break;

                case 'trackStickyHeaderClickCity':
                    category = t.STICKY_HEADER_CATEGORY;
                    event = t.CLICK_EVENT;
                    label = data.label;
                    break;
                case 'blogCityBackbtnClick':
                    category = t.CITY_BLOG;
                    event = t.CLICK_EVENT;
                    label = t.BACK_LABEL;
                    break;

                case 'blogCityNextbtnClick':
                    category = t.CITY_BLOG;
                    event = t.CLICK_EVENT;
                    label = t.NEXT_LABEL;
                    break;

                case 'cityPostClick':
                    category = t.CITY_BLOG;
                    event = t.CLICK_EVENT;
                    label = data;
                    break;

                case 'cityBloglogoClick':
                    category = t.CITY_BLOG;
                    event = t.CLICK_EVENT;
                    label = t.LOGO;
                    break;
            }

            let properties = {};
            properties[t.SELLER_ID_KEY] = sellerId;
            properties[t.CATEGORY_KEY] = category;
            properties[t.LABEL_KEY] = label;
            properties[t.VALUE_KEY] = labelValue;
            properties[t.SOURCE_MODULE_KEY] = source;
            properties[t.RANK_KEY] = rank;
            properties[t.LOCALITY_ID_KEY] = localityId;
            properties[t.LISTING_CATEGORY_KEY] = t.makeValuesUniform(listingType,t.LISTING_CATEGORY_MAP) ;
            properties[t.PROJECT_ID_KEY] = projectId;
            properties[t.PROJECT_STATUS_KEY] = t.makeValuesUniform(projectStatus,t.PROJECT_STATUS_NAME) ;
            properties[t.NAME_KEY] = name;
            properties[t.UNIT_TYPE_KEY] = t.makeValuesUniform(unitType,t.UNIT_TYPE_MAP) ;
            properties[t.BHK_KEY] = t.makeValuesUniform(bhk,t.BHK_MAP) ;
            properties[t.BUDGET_KEY] = budget;
            properties[t.NON_INTERACTION_KEY] = nonInteraction;
            properties[t.BUILDER_ID_KEY] = builderId;
            properties[t.CITY_ID_KEY] = cityId;
            properties[t.LINK_NAME_KEY] = linkName;
            properties[t.LINK_TYPE_KEY] = linkType;

            if (event) {
                trackingService.trackEvent(event, properties);
            }
        };

        return {
            messages: messages,
            onmessage: onmessage,
            init: function() {},
            destroy: function() {}
        };
    });
});
