define([
    "services/urlService",
    "services/apiService",
    "services/utils"
    ],
function() {
    "use strict";
    Box.Application.addBehavior("infiniteScroll", function(context) {

        const LISTING_WRAPPER_SELECTOR = '[data-listing-wrapper]',
            INFINITE_LOADING_LOGO = '[infiniteScroll-loading]',
            INFINITE_SCROLL_PAGE_LIMIT = 2;
        var  urlService, currentPage, totalPages, serpApiUrl, apiService, commonService, apiResponse, utils, indexthreshold;


        function _getSerpApiUrl() {
            return window.location.href;
        }

        function _getParsedHtml(html){
            let result = {};
            let $html = $(html);
            result.propcardHtml = $html.find(LISTING_WRAPPER_SELECTOR).html();
            return result;
        }

        function _getFitBoundStatus() {
            let urlParam = urlService.getAllUrlParam();
            if (urlParam.hasOwnProperty('path')) {
                return false;
            } else if (urlParam.hasOwnProperty('latitude') && urlParam.hasOwnProperty('longitude') && urlParam.viewport) {
                if (urlParam.viewport && urlParam.viewport.split(',').length === 3) {
                    return false;
                }
            }
            return true;
        }

        function _loadMoreListings() {
            serpApiUrl = _getSerpApiUrl();
            serpApiUrl = urlService.changeMultipleUrlParam({'page':currentPage,'indexthreshold':indexthreshold}, serpApiUrl, true);
            apiResponse = apiService.get(serpApiUrl);
            apiResponse.then(function(response) {
                let parsedHTML;
                if(utils.getPageData('isMap')) {
                    context.broadcast('drawMarkers', {markers:response.listingData.data, markerType: 'listing', markersFitFlag:_getFitBoundStatus(), infinite: true});
                    parsedHTML = response;
                } else {
                    parsedHTML = _getParsedHtml(response.propcardHtml);
                }
                indexthreshold += (parseInt(response.listingCount));
                context.broadcast('updateListings', {
                    propcardHtml: parsedHTML.propcardHtml,
                    totalCount: response.totalCount,
                    listingCount: response.listingCount,
                    isZeroListingPage: response.isZeroListingPage,
                    listingData: response.listingData,
                    infinite: true
                });
            },function(){
                context.broadcast('morelistingsNotLoaded');
            });
        }

        return {
            messages:['loadMoreListings','moreListingsLoaded','listingsUpdated','morelistingsNotLoaded'],
            init: function() {
                urlService = context.getService("URLService");
                apiService = context.getService("ApiService");
                commonService = context.getService("CommonService");
                utils = context.getService("Utils");
                currentPage = parseInt(urlService.getUrlParam('page')) || 1;
                totalPages = utils.getPageExtraData().totalPages;
                indexthreshold = utils.getPageExtraData().listingCount;
            },

            onmessage: function(name) {
                if(name === 'listingsUpdated') {
                    currentPage = 1;
                    totalPages = utils.getPageExtraData().totalPages;
                    indexthreshold = utils.getPageExtraData().listingCount;
                } else if(name === 'loadMoreListings') {
                    if(currentPage < totalPages && currentPage < INFINITE_SCROLL_PAGE_LIMIT){
                        currentPage += 1;
                        $(INFINITE_LOADING_LOGO).removeClass('hide');
                        _loadMoreListings();
                    }
                } else if(name === 'moreListingsLoaded' || name === 'morelistingsNotLoaded') {
                    $(INFINITE_LOADING_LOGO).addClass('hide');
                }
            }
        };
    });
});
