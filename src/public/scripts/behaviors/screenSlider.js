define([
	"services/commonService"
	], (commonService) => {
	'use strict';
	const BEHAVIOR_NAME="screenSlider";

	Box.Application.addBehavior(BEHAVIOR_NAME, (context) => {

		let moduleEl,
			wrapper = "[js-wrapper]",
			messages = ["switch-screen"];

		function _changeScreen(screen, blockRestart){
			let html = screen.screen(screen.data);
			$(moduleEl).find(wrapper).html(html);
			if(blockRestart){
				commonService.startAllModules(moduleEl);
			} else {
				commonService.restartAllModules(moduleEl);
			}
		}

		function init(){
			moduleEl = context.getElement();
		}

		function onmessage(name, data){
			if(moduleEl.id != data.id){
				return;
			}
			switch(name){
				case "switch-screen":
					_changeScreen(data.screen, data.blockRestart);
			}
		}

		return {
			messages,
			init,
			onmessage,
			destroy: function(){}
		};
	});
});