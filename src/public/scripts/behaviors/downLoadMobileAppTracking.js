define([
    'common/trackingConfigService',
    'services/trackingService'
], function(t, trackingService) {
    'use strict';
    Box.Application.addBehavior('downLoadMobileAppTracking', function() {

        var messages = [];

        const BUYER_TAB = 'buyer',
            SELLER_TAB = 'seller';

        var onclick = function(eventData, element, elementType) {
            let event = t.NAVIGATION_EVENT,
                category = t.DOWNLOAD_APP_CATEGORY,
                label,
                name,
                elementData = $(element).data();

            switch(elementType){
                case 'android-buyer':
                    label = t.BUYER_APP_LABEL;
                    name = t.ANDROID_APP_NAME;
                    break;

                case 'ios-buyer':
                    label = t.BUYER_APP_LABEL;
                    name = t.IOS_APP_NAME;
                    break;

                case 'ANDROID_HOVER':
                    label = t.SELLER_APP_LABEL;
                    name = t.ANDROID_APP_NAME;
                    break;

                case 'IOS_HOVER':
                    label = t.SELLER_APP_LABEL;
                    name = t.IOS_APP_NAME;
                    break;

                case 'tab':
                    event = t.TOGGLE_EVENT;
                    if(elementData.trackType === BUYER_TAB){
                        label = t.BUYER_APP_LABEL;
                    } else if (elementData.trackType === SELLER_TAB){
                        label = t.SELLER_APP_LABEL;
                    } else {
                        return;
                    }
                    break;

                default:
                    return;
            }

            let properties = {};
            properties[t.CATEGORY_KEY] = category;
            properties[t.LABEL_KEY] = label;
            properties[t.NAME_KEY] = name;

            trackingService.trackEvent(event, properties);
        };

        return {
            messages,
            onclick,
            init: function() {},
            destroy: function() {}
        };
    });
});