define([
    'common/trackingConfigService',
    'services/trackingService'
], function(t, trackingService) {
    'use strict';
    Box.Application.addBehavior('pressReleasesTracking', function() {

        var messages = [
                'trackNewsHeaderNewsBytesClick',
                'trackNewsHeaderPressReleasesClick',
                'trackNewsHeaderMediaResourcesClick',
                'trackNewsCardClick',
                'trackNewsPaginationClick',
            ],
            event, category, label;

        var onmessage = function(name, data) {
            //let event, category, label;
            switch (name) {
                case 'trackNewsHeaderNewsBytesClick':
                    category = t.MEDIA_HEADER;
                    event = t.MEDIA_NEWS_BYTES;
                    label = t.MEDIA_PRESS_RELEASE;
                    break;
                case 'trackNewsHeaderPressReleasesClick':
                    category = t.MEDIA_HEADER;
                    event = t.MEDIA_PRESS_RELEASE;
                    label = t.MEDIA_PRESS_RELEASE;
                    break;
                case 'trackNewsHeaderMediaResourcesClick':
                    category = t.MEDIA_HEADER;
                    event = t.MEDIA_MEDIA_RESOURCES;
                    label = t.MEDIA_PRESS_RELEASE;
                    break;
                case 'trackNewsCardClick':
                    category = t.MEDIA_PRESS_RELEASE;
                    event = 'click-card-read-more';
                    label = data.text;
                    break;
                case 'trackNewsPaginationClick':
                    category = 'pagination';
                    event = 'pagination';
                    label = t.MEDIA_PRESS_RELEASE;
                    break;
            }

            let properties = {};
            properties[t.CATEGORY_KEY] = category;
            properties[t.LABEL_KEY] = label;
            if (event) {
                trackingService.trackEvent(event, properties);
            }
        };

        return {
            messages: messages,
            onmessage: onmessage,
            init: function() {},
            destroy: function() {}
        };
    });
});
