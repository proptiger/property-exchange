define([
    'common/trackingConfigService',
    'services/trackingService'
], function(t, trackingService) {
    'use strict';
    Box.Application.addBehavior('shareCouponPageTracking', function() {

        var messages = [
          'trackVerifyCouponClick',
          'trackSearchPropertyClick',
          'trackShareCouponPageSeen'
        ];

        var onmessage = function(name, data={}) {
            let event = t.CLICK_EVENT,
                category = t.SELECT_VOUCHER_REDEMPTION_CATEGORY,
                sourceModule = data.source,
                value = data.value,
                label;
            switch (name) {
                case 'trackVerifyCouponClick':
                    label = 'Redeem_voucher';
                    break;
                case 'trackSearchPropertyClick':
                    label = 'Search Property';
                    break;
                case 'trackShareCouponPageSeen':
                    event = 'Page Seen';
                    label =  'Redeem_voucher';
                    break;
            }
            let properties = {};
            properties[t.CATEGORY_KEY] = category;
            properties[t.LABEL_KEY] = label;
            properties[t.SOURCE_MODULE_KEY] = sourceModule;
            properties[t.USER_ID_KEY] = value;
            
            trackingService.trackEvent(event, properties);
        };

        return {
            messages: messages,
            onmessage: onmessage,
            init: function() {},
            destroy: function() {}
        };
    });
});
