'use strict';
define('infra',[
    'jquery',
    't3',
    'doTCompiler',
    'promise',
    'nanobar',
    'text',
    'json',
    'ajaxify',
    'doT'
    // 'jwPlayer'
], function($,Box,doT, es6Promise) {
	window.Box = Box;
    window.doT = doT;
    window.Promise = es6Promise.Promise;
});