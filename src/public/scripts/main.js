require.config({
    "baseUrl": "/",
    "paths": {
        "t3": "scripts/vendor/t3",
        "jquery": "bower_components/jquery/dist/jquery.min",
        "ajaxify": "scripts/vendor/ajaxify.min",
        "highcharts": "bower_components/highcharts/highcharts",
        "highcharts-more": "bower_components/highcharts/highcharts-more",
        "highcharts-no-data": "bower_components/highcharts/modules/no-data-to-display",
        "socket.io":"scripts/vendor/socket.io",
        "highcharts3d": "bower_components/highcharts/highcharts-3d",
        "nanobar": "scripts/vendor/nanobar",
        "doTCompiler": "bower_components/doT/doT.min",
        "text": "bower_components/requirejs-text/text",
        "json": "bower_components/requirejs-json/json",
        "doT": "bower_components/requirejs-doT/doT",
        "config": "config",
        "app": "scripts/app",
        "slider": "scripts/vendor/slider.min",
        "infra": "scripts/infra",
        "services": "scripts/services",
        "bundles": "scripts/bundles",
        "common": "scripts/common",
        "behaviors": "scripts/behaviors",
        "vendor": "scripts/vendor",
        "jwPlayer": "bower_components/jwplayer/jwplayer",
        "sw": "sw",
        "moment": "scripts/vendor/moment.min",
        "promise": "scripts/vendor/es6-promise",
        "buy-rent": "scripts/widgets/buy-rent",
        "trackAmplitude": "https://d24n15hnbwhuhn.cloudfront.net/libs/amplitude-3.4.0-min.gz"
    },
    "map": {
      "*": {
        "css": "bower_components/require-css/css"
      }
    },
    "shim": {
        "t3": {
            "deps": ["jquery"]
        },
        "ajaxify": {
            "deps": ["jquery"]
        },
        "buy-rent": {
            "deps": ["jquery"]
        },
        "highcharts": ["jquery"],
        "highcharts-more": ["highcharts"],
        "highcharts-no-data": ["highcharts"],
        "highcharts3d": ["highcharts"]
    },
    "waitSeconds": 200,
    "doT": {
        "ext": ".html"
    }
});
