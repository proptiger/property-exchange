"use strict";
define([
    'modules/socialSharing/scripts/services/socialSharingService',
    'services/commonService',
    'services/utils',
    'modules/projectsNewLaunches/scripts/index',
    'modules/priceTrends/scripts/index',
    'modules/chart/scripts/index',
    'modules/homeloan/scripts/index',
    'modules/banks/scripts/index',
    'modules/emiCalculator/scripts/index',
    'buy-rent'
], () => {
    Box.Application.addModule("bwPage", (context) => {

        const CommonService = context.getService('CommonService'),
            UtilService = context.getService('Utils'),
            //logger = context.getService('Logger'),
            defaultService = context.getService('DefaultService'),
            searchService = context.getService('SearchService'),
            SocialSharingService = context.getService('SocialSharingService');

        var moduleEl, $moduleEl, $, configData, moduleId;
        var messages = ['typeaheadInputFocusin'],
            behaviors = [];

        // function onfocusAnimate() {
        //     var windowWidth = $(window).width();
        //     if (windowWidth > 768) {
        //         $("html, body").animate({ scrollTop: 330 }, 500);
        //         return false;
        //     } else {
        //         $("html, body").animate({ scrollTop: 200 }, 500);
        //         return false;
        //     }
        // }
        function renderPopularSeraches(cityName){
            var params = {
                query : cityName,
                usercity : cityName,
                rows : 5,
                enhance : 'gp',
                category:'buy',
                view : 'buyer'
            };
            var popularSearchTemplate = "<span class='popular-title'>popular searches : </span>";             
           
            var  data  = {
                searchParams : params
            };
            var suggestions = [];
            searchService.getTypeaheadResults(data).then(response=>{
                if(response){
                    $.each(response,(index,item)=>{
                        if(item && item.type==='Popular Suggestions'){
                            suggestions = item.values;
                        }
                    });
                    if(suggestions && suggestions.length>0){
                        $.each(suggestions,(index,item)=>{
                            popularSearchTemplate += "<a href='" + item.redirectUrl + "' class='no-ajaxy popular-links'>" + item.displayText + "</a>";
                        });
                    }
                }
                $moduleEl.find('.js-popularSearches').html(popularSearchTemplate);
            });

        }
        function init() {
            moduleEl = context.getElement();
            moduleId = moduleEl.id;
            configData = context.getConfig() || {};
            $ = context.getGlobal('jQuery');
            $moduleEl = $(moduleEl);
            defaultService.getCityByLocation().then(function(city) {
                renderPopularSeraches(city.label);
            }, function() {
               //for error show search from new-delhi
               renderPopularSeraches('delhi');
            });
            $moduleEl.find('.view-date').html(new Date().toString().slice(0, -24));
            //initialization of all local variables
            window.addEventListener('scroll', _scrollFunction,{passive:true});
            
            CommonService.bindOnLoadPromise().then(function() {

                context.broadcast('loadModule', [{
                    name: 'pyr'
                }]);

                context.broadcast('loadModule', [{
                    name: 'chart',
                    id: 'emiChart'
                }]);
            });

        }

        function _scrollFunction() {
            var scrollTop = $(window).scrollTop(),
                stickyParent = $moduleEl.find('.to-fix'),
                sticky= $moduleEl.find('.fixed-el'),
                elTop = stickyParent.offset().top,
                absolutePosition = 50;
            if (scrollTop >= elTop - absolutePosition) {
                sticky.addClass('posfix');
                sticky.css({position:"fixed", top: absolutePosition + 'px' });
            } else {
                sticky.removeClass('posfix');
                sticky.removeAttr('style');
            }
        }

        function destroy() {
            window.removeEventListener('scroll', _scrollFunction,{passive:true});
            //clear all the binding and objects
        }

        function onmessage(name) {
            // bind custom messages/events
            switch (name) {
                case 'typeaheadInputFocusin':
                    //onfocusAnimate();
                    break;
            }
        }

        function onclick(event, element, elementType) {
            // bind custom messages/events
            switch (elementType) {
                case 'menu-toggle':
                    $moduleEl.toggleClass('open-bw-menu');
                    $('body').toggleClass('overflowx-hidden');
                    break;

                case 'showHideBankDetails':
                    if (!($moduleEl.find('.js-showHideBankDetails').hasClass('show'))) {
                        UtilService.gotoEl('.home-loan-wrap', 0, 0);
                    }
                    break;
                case "Facebook-Share":
                case "Facebook-Feed":
                case "Facebook-Send":
                case "Twitter":
                case "Linkedin":
                case "Gplus":
                case "Email":
                case "Gmail":
                case "Whatsapp":
                    var data = {};
                    data.title =  $("meta[name='og:title']").attr('content');
                    data.image = $("meta[name='og:image:url']").attr('content');
                    data.description = $("meta[name='og:description']").attr('content') || '';
                    data.description = data.description.substring(0, 100) + '...';
                    data.location =  window.location.href;
                    data.FBAppId = window.FB;
                    SocialSharingService.shareOn(elementType, data);
                    break;
            }
        }

        return {
            init,
            messages,
            behaviors,
            onmessage,
            onclick,
            destroy
        };
    });
});
