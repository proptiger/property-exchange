"use strict";
define([
    'common/sharedConfig',
    'services/commonService',
    'services/apiService',
    'services/utils',
    'services/urlService',
    'behaviors/cityOverviewTracking'
], (sharedConfig) => {

    Box.Application.addModule('cityOverviewPage', (context) => {

        //track actions
        //const SEARCH_ACTION_BUY = 'SEARCH_City_Buy',
          //  SEARCH_ACTION_RENT = 'SEARCH_City_Rent',
            //PROPERTY_RANGE_ACTION = 'SELECT_City_Property Range',
            const PRICE_TRENDS_ACTION = 'SELECT_City_Price Trends',
            PAGE_CATEGORY = 'Buyer_City';

        const CommonService = context.getService('CommonService'),
            apiService = context.getService('ApiService'),
            utils = context.getService('Utils'),
            //urlService = context.getService('URLService'),
            query = Box.DOM.query,
            queryAll = Box.DOM.queryAll;
        var moduleEl,
            messages = ['trackBarGraphClick', 'renderChart', 'dropdownsChanged', 'mapSvgLegendsModuleInitiated', 'tabClicked', 'city_sponsored_project_slider_element_clicked', 'city_sponsored_project_scale_slider_auto_scroll', 'tabs_click_track', 'city_sponsored_project_scale_carousel_element_clicked', 'city_sponsored_project_scale_slider_Arrow_element_clicked', 'city_sponsored_project_slider_element_viewed', 'slider_clicked', 'makaaniq-post_slider_element_clicked'],
            behaviors = ['cityOverviewTracking'],
            cityMinMaxPrices,
            viewPropertyLink,
            application = Box.Application,
            scope = {};

        function _loadRentSlider() {
            context.broadcast('loadModule', {
                name: 'slider',
                loadId: 'locality_rent_slider'
            });
        }

        function _loadLandmarkRentSlider() {
            context.broadcast('loadModule', {
                name: 'slider',
                loadId: 'landmark_rent_slider'
            });
        }

        function _handleTabEvents(data) {
            if (!(data && data.getAttribute('data-target-id'))) {
                return;
            }
            let foldType = data.getAttribute('data-fold');
            if (foldType === "card") {
                let checkBoxElem = $(data).parents().find('.js-fold');
                if (checkBoxElem) {
                    checkBoxElem.prop('checked', false);
                }
            }
        }

        function _render3dChart(data = {}) {
            var chartData, meta;
            meta = data;
            chartData = {
                id: "propertyRangeChart",
                hasModuleLoaded: true,
                type: '3dchart',
                parent: 'propertyRangeChartContainer',
                meta: meta
            };
            _initChart(chartData);
        }

        function init() {
            moduleEl = context.getElement();
            viewPropertyLink = moduleEl.querySelector('.js-view-property');
            scope.propertyRange = {};
            var propertyContainerId = "propertyRangeChartContainer",
                containerElement = document.getElementById(propertyContainerId),
                data = $(containerElement).data() || {};
            cityMinMaxPrices = {
                buyMinPrice: data.buyminprice,
                buyMaxPrice: data.buymaxprice,
                rentMinPrice: data.rentminprice,
                rentMaxPrice: data.rentmaxprice,
                cityMaxPriceAllowed: data.maxallowedprice,
                cityMinPriceAllowed: data.minallowedprice,
                cityMinRentAllowed: data.minallowedrent,
                cityMaxRentAllowed: data.maxallowedrent
            };
            if ($('#sponsored').length) {
                let projectIds = '';
                $('#sponsored .slider-js').each(function() {
                    projectIds += projectIds ? ',' + $(this).data('projectid') : $(this).data('projectid');
                });
            }

            CommonService.bindOnLoadPromise().then(() => {
                let onLoadClass;
                $(moduleEl).find('[data-onLoadClass]').each(function() {
                    onLoadClass = $(this).data('onloadclass');
                    $(this).addClass(onLoadClass);
                });

                context.broadcast('loadModule', {
                    name: 'chart'
                });
            });

            // As soon as module gets loaded , trigger default action
            CommonService.bindOnModuleLoad('chart', (id) => {
                if (id === "price-trend-chart-three-years") {
                    let data, tabElement, meta = {};

                    tabElement = query(moduleEl, '#price-trend [data-module="tabs"]');
                    // meta = context.application.getModuleConfig(tabElement);
                    meta = $(tabElement).find('script[type="text/x-config"]').text();
                    try {
                        meta = JSON.parse(meta);
                    }catch(ex){
                        meta = {};
                    }
                    meta.duration = 36;
                    data = {
                        id: "price-trend-chart-three-years",
                        hasModuleLoaded: true,
                        type: 'chart',
                        parent: 'price-demand',
                        meta: meta
                    };
                    _initChart(data);
                } else if (id === "propertyRangeChart") {
                    context.broadcast('loadModule', {
                        name: 'multipleDropdownsModule',
                        loadId: "multipleDropdownsModuleWrapper"
                    });
                    _render3dChart();
                }
            }, ["price-trend-chart-three-years", "propertyRangeChart"]);

            // loads submodules once mapsModule is loaded
            // CommonService.bindOnModuleLoad('mapsModule', _loadSubmodules);

            // CommonService.bindOnLoadPromise().then(() => {
            //     context.broadcast('loadModule', {
            //         name: 'mapsModule'
            //     });
            // });
            utils.setLastVisitedPageDetails(window.pageData); //for saving context of repeat users
            utils.updatePushNotificationTags('CITY_OVERVIEW');
            window.addEventListener('scroll', _scrollFunction,{passive:true});
        }
        function _scrollFunction(){
             context.broadcast('cityOverviewPageScrolled');
        }

        function onmessage(name, data) {
            switch (name) {
                case 'renderChart':
                    _initChart(data);
                    break;
                case 'mapSvgLegendsModuleInitiated':
                    context.broadcast('activateMasterPlan', true);
                    break;
                case 'dropdownsChanged':
                    trackPropertyRange(data);
                    _render3dChart(data);
                    break;
                case 'tabClicked':
                    _handleTabEvents(data.element);
                    break;
                case 'city_sponsored_project_slider_element_clicked':
                    if (data && data.data && data.eventSource) {
                        if ($(data.eventSource.target).data("projectfield") == "projectField") {
                            data.data.trackLabel = $(data.eventSource.target).text();
                        } else {
                            data.data.trackLabel = "card";
                        }
                        if (data.data.overviewUrl) {
                            if (data.data.isAbsoluteUrl) {
                                window.open(data.data.overviewUrl, '_blank');
                            } else {
                                CommonService.ajaxify(data.data.overviewUrl);
                            }
                        }
                    }
                    break;
                case 'city_sponsored_project_scale_carousel_element_clicked':
                    if (data.overviewUrl) {
                        if (data.isAbsoluteUrl) {
                            window.open(data.overviewUrl, '_blank');
                        } else {
                            CommonService.ajaxify(data.overviewUrl);
                        }
                    }
                    break;
                case 'tabs_click_track':
                    if (data.moduleEl.id == 'price-trend-parend') {
                        context.broadcast('cityoverview_priceTrendTab', {
                            label: (data.element.dataset || {}).trackLabel
                        });
                    }
                    break;
                case 'trackBarGraphClick':
                    context.broadcast('trackPropertyRangeBarClickCityPage', $.extend(scope.propertyRange, { value: data.y, budget: (data.name + ',' + (data.name + data.gap)) }));
                    break;
                case 'city_sponsored_project_scale_slider_auto_scroll':
                    context.broadcast('trackCitySponsoredAutoScroll', data);
                    break;
                case 'city_sponsored_project_slider_element_viewed':
                    context.broadcast('trackCitySponsoredAutoScrollMobile', data);
                    break;
                case "slider_clicked":
                    if (data.element.id == 'makaaniq-post') {
                        if (data.event == 'Right') {
                            context.broadcast('blogCityNextbtnClick');
                        } else if (data.event == 'Left') {
                            context.broadcast('blogCityBackbtnClick');
                        }
                    }
                    break;
                case 'makaaniq-post_slider_element_clicked':
                    if (data.data) {
                        context.broadcast('localityPostClick', data.data);
                        window.open(data.data, '_blank');
                    }
                    break;

            }
        }

        function trackPropertyRange(data) {
            scope.propertyRange.listingType = data.buyOrRent ? data.buyOrRent : 'both';
            scope.propertyRange.unitType = data.propertyType ? data.propertyType.join() : "";
            scope.propertyRange.bhk = data.beds ? data.beds.join() : "";
            context.broadcast('trackPropertyRangeFilters', scope.propertyRange);
            return;
        }

        // function _loadSubmodules() {
        //     context.broadcast('loadModule', [{
        //         parent: 'mapsModule',
        //         name: 'mapSvgLegendsModule'
        //     }]);
        // }

        function openPyrPopup() {
            context.broadcast('popup:open', {
                id: 'pyrPopup',
                options: {
                    config: {
                        modal: true
                    }
                }
            });
        }

        var getBudgetRange = function(array) {
            let first, last;
            for (let i = 0, len = array.length; i < len; i++) {
                if (!first && array[i].propertyCount) {
                    first = array[i].startPoint;
                } else if (first && !last && !array[i].propertyCount) {
                    last = parseInt(array[i - 1].startPoint) + parseInt(array[i - 1].gap);
                }
            }
            return first + ',' + last;
        };

        var _renderChart = function(data) {

            var id = data.id,
                type = data.type,
                hasModuleLoaded = data.hasModuleLoaded ? data.hasModuleLoaded : false;

            if (!hasModuleLoaded) {
                context.broadcast('loadModule', {
                    name: 'chart',
                    loadId: id
                });
            }

            if (type === "3dchart") {

                let query = {
                        'min': cityMinMaxPrices.cityMinPriceAllowed,
                        'max': cityMinMaxPrices.cityMaxPriceAllowed
                    },
                    serpFilters = {},
                    url;
                url = viewPropertyLink.getAttribute("data-buyurl");
                if (data.meta.beds) {
                    query.bhk = data.meta.beds;
                    serpFilters.beds = data.meta.beds;
                }
                query.listingType = data.meta.buyOrRent ? data.meta.buyOrRent : 'buy';

                if (query.listingType === "rent") {
                    query.min = cityMinMaxPrices.cityMinRentAllowed;
                    query.max = cityMinMaxPrices.cityMaxRentAllowed;
                    url = viewPropertyLink.getAttribute("data-renturl");
                    $('[data-chart-lbl-buy]').addClass('hide');
                    $('[data-chart-lbl-rent]').removeClass('hide');
                } else {
                    $('[data-chart-lbl-rent]').addClass('hide');
                    $('[data-chart-lbl-buy]').removeClass('hide');
                }
                if (data.meta.propertyType) {
                    query.propertyType = data.meta.propertyType;
                    serpFilters.propertyType = data.meta.propertyType;
                }

                // set view property url based on filter

                let viewPropertyUrl = url.split("?")[0],
                    filterUrl = utils.generateSerpFilter(serpFilters),
                    urlSeparator;

                viewPropertyUrl = filterUrl ? viewPropertyUrl + filterUrl : viewPropertyUrl;
                viewPropertyLink.setAttribute('href', viewPropertyUrl);
                urlSeparator = viewPropertyUrl.indexOf('?') == -1 ? '?' : '&';

                // static series for now
                let priceRangeApiUrl = sharedConfig.apiHandlers.getCityPriceRange({
                    "cityId": utils.getPageData("cityId"),
                    "query": query
                }).url;
                apiService.get(priceRangeApiUrl).then((response) => {
                    scope.propertyRange.budget = getBudgetRange(response);
                    let result = [],
                        series = [];
                    if (response && response.length) {
                        $.each(response, (k, v) => {
                            v.startPoint = parseInt(v.startPoint);
                            v.gap = parseInt(v.gap);

                            let filters = 'budget=' + v.startPoint + ',' + (v.startPoint + v.gap);
                            result.push({
                                name: v.startPoint,
                                y: v.propertyCount,
                                url: viewPropertyUrl + urlSeparator + filters,
                                gap: v.gap
                            });
                        });
                        series = [{
                            showInLegend: false,
                            data: result
                        }];
                        $(context.element).find('[data-type=chart-parent]').removeClass('hide');
                    }
                    context.broadcast('chartDataLoaded', {
                        series: series,
                        id: id
                    });
                }, () => {
                    context.broadcast('chartDataLoaded', {
                        series: [],
                        id: id
                    });
                });
            } else if (type === "chart") {
                let cityId = data.meta.cityId,
                    duration = data.meta.duration,
                    xPlotBand,
                    __seriesData,
                    dataPointMaxIndex = 0,
                    series = [];
                var url = sharedConfig.apiHandlers.getCityPriceTrend({
                    cityId: cityId,
                    query: {
                        duration: duration,
                        compare: 'topLocality'
                    }
                }).url;
                apiService.get(url).then(function(response) {
                    if (response) {
                        var __city, i, j, result = [],
                            key, value,
                            __month, sortedMonth, dataPointMaxCount = 0,
                            currentCount;
                        for (i = 0; i < Object.keys(response).length; i++) {
                            __city = Object.keys(response)[i];
                            var __subObj = response[__city];
                            sortedMonth = Object.keys(__subObj).sort();
                            result = [];
                            currentCount = 0;
                            for (j = 0; j < sortedMonth.length; j++) {
                                key = sortedMonth[j];
                                __month = parseInt(key, 10);
                                value = Math.round(__subObj[key]);
                                if (value) {
                                    result.push([__month, value]);
                                }
                                currentCount++;
                                if (currentCount >= dataPointMaxCount) {
                                    dataPointMaxCount = currentCount;
                                    dataPointMaxIndex = i;
                                }
                            }
                            if (result.length) {
                                series.push({
                                    name: __city,
                                    data: result
                                });
                            }
                        }
                    }
                    __seriesData = series[dataPointMaxIndex];
                    if (__seriesData && __seriesData.data && Object.keys(__seriesData.data).length >= 2) {

                        let xAxisNegativeThreshold = 0;
                        if (duration == 12) {
                            xAxisNegativeThreshold = 24 * 3600 * 1000 * 2 * 50;
                        } else if (duration == 36) {
                            xAxisNegativeThreshold = 24 * 3600 * 1000 * 3 * 50;
                        } else if (duration == 60) {
                            xAxisNegativeThreshold = 24 * 3600 * 1000 * 6 * 50;
                        }
                        let xAxisLength = Object.keys(__seriesData.data).length;
                        xPlotBand = {
                            color: '#D4E8F6',
                            from: __seriesData.data[(xAxisLength - 2)][0] - xAxisNegativeThreshold,
                            to: __seriesData.data[(xAxisLength - 1)][0] + 24 * 3600 * 1000 * 0.5 * 50
                        };
                    }
                    context.broadcast('chartDataLoaded', {
                        series: series,
                        id: id,
                        xPlotBand: xPlotBand
                    });
                }, () => {
                    context.broadcast('chartDataLoaded', {
                        series: [],
                        id: id
                    });
                });
            }
        };

        function _initChart(data) {
            let moduleId = data.id,
                hasModuleLoaded = data.hasModuleLoaded ? data.hasModuleLoaded : false,
                chartElem = document.getElementById(moduleId);
            _showChart(data);

            if (!application.isStarted(chartElem) || hasModuleLoaded) {
                _renderChart(data);
            }
        }

        function _showChart(data) {
            let elems, id = data.id,
                parent = data.parent,
                type = data.type,
                idAppend = "";

            idAppend = (type === "pie") ? "-pie" : "";
            elems = queryAll("#" + parent, 'div[data-container-type="chart"]');

            if (typeof elems !== 'undefined') {
                for (let i = 0; i < elems.length; i++) {
                    if (elems[i].id && elems[i].id === (id + idAppend)) {
                        elems[i].classList.remove("hidden");
                    } else {
                        if (!elems[i].classList.contains("hidden")) {
                            elems[i].className += " hidden";
                        }
                    }
                }
            }
        }
        var onclick = function(event, clickElement, elementType) {
            // let actionMap = {
            //         "affordable properties": "SELECT_City_Properties in City_Affordable",
            //         "luxury properties": "SELECT_City_Properties in City_Luxury",
            //         "budget homes": "SELECT_City_Properties in City_Budget",
            //         "best properties": "SELECT_City_Properties in City_Best properties",
            //         "new rental properties": "SELECT_City_Properties in City_New Rental",
            //         "popular localities": "SELECT_City_Popular Localities",
            //         "top projects": "SELECT_City_Top Projects"
            //     },
                let dataset = $(clickElement).data(),
                redirectUrl;
            switch (elementType) {
                case "project-classification-link":
                    $(clickElement).addClass('active');
                    break;
                case 'breadcrum':
                    if (dataset.trackLabel) {
                        utils.trackEvent('CLICK_Property_You are Here', PAGE_CATEGORY, dataset.trackLabel);
                    }
                    break;
                case 'openPyrForm':
                    let data = {
                        id: 'pyr-form',
                        pyrData: {
                            stage: "step1"
                        },
                        trackData: {
                            id: 'CityPyr'
                        }
                    };
                    context.broadcast('openPyr', data);
                    openPyrPopup();
                    break;
                case 'ajaxy-redirect':
                    redirectUrl = clickElement.getAttribute("data-url") || $(clickElement).find('[data-type=ajaxy-redirect]').attr("data-url");
                    if (redirectUrl) {
                        context.broadcast('trackPropertiesInAreaHeadingCityPage', { name: dataset.name, linkName: dataset.linkName, linkType: dataset.linkType });
                        CommonService.ajaxify(redirectUrl);
                    }
                    break;
                case "score-ring-popup":
                    event.stopPropagation();
                    let popupid = $(clickElement).data('openid');
                    context.broadcast('popup:open', {
                        id: popupid,
                        modal: false
                    });
                    if (dataset.trackAction && dataset.trackLabel) {
                        utils.trackEvent(dataset.trackAction, PAGE_CATEGORY, dataset.trackLabel);
                    }
                    break;
                case "loc-link":
                    context.broadcast('trackPopularLocalityPage', dataset);
                    break;
                case 'popular-locality-buy':
                    context.broadcast('trackPopularLocalityBuyCityPage', { listingType: 'buy', rank: dataset.index, localityId: dataset.localityId, linkName: dataset.linkName, linkType: dataset.linkType });
                    break;
                case 'popular-locality-rent':
                    context.broadcast('trackPopularLocalityRentCityPage', { listingType: 'rent', rank: dataset.index, localityId: dataset.localityId, linkName: dataset.linkName, linkType: dataset.linkType });
                    break;
                case 'popular-projects':
                    context.broadcast('trackPopularProjectsCityPage', { rank: dataset.index, projectStatus: dataset.projectStatus, projectId: dataset.projectId, localityId: dataset.localityId, linkName: dataset.linkName, linkType: dataset.linkType });
                    break;
                case 'properties-in-area':
                    context.broadcast('trackPropertiesInAreaCityPage', { rank: dataset.index, name: dataset.name, listingType: dataset.listingType, unitType: dataset.unitType, budget: dataset.budget, bhk: dataset.bhk, linkName: dataset.linkName, linkType: dataset.linkType });
                    break;

                case 'chart':
                    let duration;
                    duration = dataset.duration;
                    if (dataset.track === 'market-analysis') {
                        utils.trackEvent(PRICE_TRENDS_ACTION, PAGE_CATEGORY, (parseInt(duration) / 12 > 1) ? (parseInt(duration) / 12) + ' yrs' : duration + ' mths');
                    }
                    break;

                case 'cityTopLocality':
                    if (dataset.trackAction && dataset.trackLabel) {
                        utils.trackEvent(dataset.trackAction, PAGE_CATEGORY, dataset.trackLabel);
                    }
                    context.broadcast('trackCityTopLocality', dataset);
                    break;

                case 'cityViewProperty':
                    if (dataset.trackAction && dataset.trackLabel) {
                        utils.trackEvent(dataset.trackAction, PAGE_CATEGORY, dataset.trackLabel);
                    }
                    break;
                case 'navigate':
                    context.broadcast('trackStickyHeaderClickCity', { label: dataset.trackLabel });
                    break;
                case 'tab':
                    if (clickElement.dataset.targetId == 'rent') {
                        _loadRentSlider();
                    } else if (clickElement.dataset.targetId == 'landmark-rent') {
                        _loadLandmarkRentSlider();
                    }
                    break;
                case 'social-share':
                    context.broadcast('open_share_box', { page: window.pageData.trackPageType });
                    break;
                case 'bloglogo':
                    context.broadcast('cityBloglogoClick');
                    break;
                case 'postRead':
                    dataset = $(clickElement).closest('[data-type="list-element"]').find('[data-type="postUrl"]').attr('itemid');
                    context.broadcast('cityPostClick', dataset);
                    event.stopPropagation();
                    break;
            }
        };
        var onmouseover = function(event, clickElement, elementType) {
            let dataset = $(clickElement).data();
            switch (elementType){
                case 'cityPageTooltips':
                    context.broadcast('trackHoverGrowthTooltipsCity', { type: dataset.trackType });
                    break;
            }
        };

        return {
            messages,
            behaviors,
            init,
            onmessage,
            onclick,
            onmouseover
        };
    });
});
