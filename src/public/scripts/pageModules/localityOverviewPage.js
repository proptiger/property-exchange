"use strict";
define([
    "common/sharedConfig",
    "services/apiService",
    "services/commonService",
    "services/utils",
    "services/filterConfigService",
    'behaviors/localityOverviewTracking'
], function(sharedConfig, apiService, CommonService, utils, filterConfigService) {
    Box.Application.addModule('localityOverviewPage', function(context) {
        const apiHandlers = sharedConfig.apiHandlers,
            // TYPEAHEAD_IDS = ['locality-typeahead-buy', 'locality-typeahead-rent'],
            query = Box.DOM.query,
            queryAll = Box.DOM.queryAll,
            trigger = Box.DOM.trigger,
            application = Box.Application,
            INCLUDE_NEARBY_LOCALITIES_COOKIE = sharedConfig.includeNearbyLocalitiesCookieName;

        var moduleEl,
            moduleConfig,
            tempPromises = [];

        function _loadSubmodules() {
            context.broadcast('loadModule', [{
                parent: 'mapsModule',
                name: 'communicatorModule'
            }, {
                parent: 'mapsModule',
                name: 'customDirectionModule'
            }, {
                parent: 'mapsModule',
                name: 'nearByAmenityModule'
            }
            // , {
            //     parent: 'mapsModule',
            //     name: 'mapSvgLegendsModule'
            // }
            ]);
        }

        var isMapModuleCalled;

        function _scrollFunction() {
            //let scrollTop = $(window).scrollTop();
            context.broadcast('localityOverviewPageScrolled');
            if (!isMapModuleCalled && !utils.isMobileRequest() && utils.checkInViewPort('.js-map-section')) {
                isMapModuleCalled = true;
                CommonService.bindOnLoadPromise().then(() => {
                    context.broadcast('loadModule', {
                        "name": 'mapsModule'
                    });
                });
                CommonService.bindOnModuleLoad('mapsModule', _loadSubmodules);
            }
        }

        function abortAllPendingAPis() {
            for (let i = 0; i < tempPromises.length; i++) {
                try {
                    tempPromises[i].abort();
                } catch (e) {
                    console.log('in catch inside searchService function abortAllPendingAPis of abort', e);
                }
            }
            tempPromises = [];
        }

        var _renderChart = function(data) {

            var id = data.id,
                type = data.type,
                hasModuleLoaded = data.hasModuleLoaded ? data.hasModuleLoaded : false;

            if (!hasModuleLoaded) {
                context.broadcast('loadModule', {
                    name: 'chart',
                    loadId: id
                });
            }

            if (type === "chart") {
                let localityId = data.meta.localityId,
                    latitude = data.meta.latitude,
                    longitude = data.meta.longitude,
                    duration = data.meta.duration,
                    xPlotBand,
                    __seriesData,
                    series = [],
                    dataPointMaxCount = 0,
                    dataPointMaxIndex = 0;
                var url = sharedConfig.apiHandlers.getLocalityPriceTrend({
                    localityId: localityId,
                    query: {
                        lat: latitude,
                        lon: longitude,
                        duration: duration,
                        compare: 'nearByLocality',
                        category: data.category || 'buy'
                    }
                }).url;
                apiService.get(url).then(function(response) {
                    if (response) {
                        var __city, i, j, result = [],
                            key, value,
                            __month, sortedMonth,
                            currentCount;
                        for (i = 0; i < Object.keys(response).length; i++) {
                            __city = Object.keys(response)[i];
                            var __subObj = response[__city];
                            sortedMonth = Object.keys(__subObj).sort();
                            result = [];
                            currentCount = 0;
                            for (j = 0; j < sortedMonth.length; j++) {
                                key = sortedMonth[j];
                                __month = parseInt(key, 10);
                                value = Math.round(__subObj[key]);
                                if (value) {
                                    result.push([__month, value]);
                                }
                                currentCount++;
                                if (currentCount >= dataPointMaxCount) {
                                    dataPointMaxCount = currentCount;
                                    dataPointMaxIndex = i;
                                }
                            }
                            if (result.length) {
                                series.push({
                                    name: __city,
                                    data: result
                                });
                            }
                        }
                    }
                    __seriesData = series[dataPointMaxIndex];
                    // if (__seriesData && __seriesData.data && Object.keys(__seriesData.data).length >= 2) {
                    //     var xAxisNegativeThreshold = 0;
                    //     if (duration == 12) {
                    //         xAxisNegativeThreshold = 24 * 3600 * 1000 * 2 * 50;
                    //     } else if (duration == 36) {
                    //         xAxisNegativeThreshold = 24 * 3600 * 1000 * 3 * 50;
                    //     } else if (duration == 60) {
                    //         xAxisNegativeThreshold = 24 * 3600 * 1000 * 6 * 50;
                    //     }
                    //     var xAxisLength = Object.keys(__seriesData.data).length;
                    //     xPlotBand = {
                    //         color: '#D4E8F6',
                    //         from: __seriesData.data[(xAxisLength - 2)][0] - xAxisNegativeThreshold,
                    //         to:  __seriesData.data[(xAxisLength - 1)][0] + 24 * 3600 * 1000 * 0.5 * 50
                    //     };
                    // }
                    context.broadcast('chartDataLoaded', {
                        series: series,
                        id: id,
                        xPlotBand: xPlotBand
                    });
                }, function() {
                    context.broadcast('chartDataLoaded', {
                        series: [],
                        id: id
                    });
                });
            }
        };

        function _handleTabEvents(data) {
            if (!(data && data.getAttribute('data-target-id'))) {
                return;
            }
            let targetId = data.getAttribute('data-target-id');
            if (targetId === 'neighbourhoodContent') {
                abortAllPendingAPis();
                context.broadcast('removeCustomDirectionModule');
                context.broadcast('clearMarkerOverlay', 'nearbyLocality');
                context.broadcast('loadNearByAmenityModule');
                context.broadcast('activateMasterPlan', false);
                context.broadcast('clearMarkerOverlay');
            } else if (targetId === 'masterPlanContent') {
                abortAllPendingAPis();
                context.broadcast('activateMasterPlan', true);
                context.broadcast('clearMarkerOverlay', 'nearbyLocality');
                context.broadcast('removeAmenityModule');
                context.broadcast('removeCustomDirectionModule');
                context.broadcast('removeCommunicatorModule');
            } else if (targetId === 'commuteContent') {
                abortAllPendingAPis();
                context.broadcast('activateMasterPlan', false);
                context.broadcast('clearMarkerOverlay', 'nearbyLocality');
                context.broadcast('removeAmenityModule');
                context.broadcast('loadCustomDirectionModule');
                context.broadcast('clearMarkerOverlay');
            } else if (targetId == 'nearbyLocalityMap') {
                context.broadcast('activateMasterPlan', false);
                context.broadcast('removeAmenityModule');
                context.broadcast('removeCustomDirectionModule');
                // abort all pending apis for nearby locality marker
                abortAllPendingAPis();
                tempPromises.push(apiService.get(apiHandlers.getNearByLocality().url + `?lat=${moduleConfig.latitude}&lon=${moduleConfig.longitude}&localityId=${moduleConfig.localityId}&rows=15`));
                tempPromises[0].then((response) => {
                    context.broadcast('drawMarkers', {
                        markers: response.data,
                        markerType: 'nearbyLocality',
                        markersFitFlag: true
                    });
                }, (error) => {
                    logger.info(JSON.stringify(error));
                });
            } else if (data.getAttribute('data-section') === "chart") {
                let firstChartElement = Box.DOM.query(`#${data.dataset.targetId}`, '[data-lazyModule="chart"]');
                if (firstChartElement) {
                    _renderChart({ id: firstChartElement.id });
                }
                if ($(data).hasClass('js-property-pie')) {
                    $(Box.DOM.queryAll(moduleEl, '[data-type="property-check"]')).prop('checked', false);
                }
            } else if (data.getAttribute('data-target-tab') == '#lifestyle-desc') {
                context.broadcast('loadModule', {
                    name: 'slider',
                    loadId: `${targetId}-slider-parent`
                });
            } else if (data.getAttribute('data-target-tab') == '#buyrent-top-agent') {
                context.broadcast('loadModule', {
                    name: 'slider',
                    loadId: `${targetId}-slider`
                });
            }

            if (data.getAttribute('data-page-category')) {
                let category = data.getAttribute('data-page-category');
                moduleConfig.dominantUnit = category;
                context.broadcast('switchTabs', { moduleIdArray: ['top-seller-slider', 'price-trend', 'locality-compare'], category });
            }
        }

        function _openPopup(config) {
            context.broadcast('popup:open', {
                id: config.id,
                options: {
                    config: {
                        modal: config.modal
                    }
                }
            });
        }

        function _openLeadForm(elementData) {
            let rawData = {
                cityId: utils.getPageData("cityId"),
                localityId: utils.getPageData("localityId"),
                id: "lead-popup"
            };
            rawData = $.extend(rawData, elementData);
            context.broadcast('leadform:open', rawData);
        }

        var _initChart = function(data) {
            var moduleId = data.id,
                hasModuleLoaded = data.hasModuleLoaded ? data.hasModuleLoaded : false,
                chartElem = document.getElementById(moduleId);
            //_showChart(data);
            if (!application.isStarted(chartElem) || hasModuleLoaded) {
                _renderChart(data);
            }
        };

        // var _showChart = function(data) {
        //     var elems, i, id = data.id,
        //         parent = data.parent,
        //         type = data.type,
        //         idAppend = "";

        //     idAppend = (type === "pie") ? "-pie" : "";
        //     elems = queryAll("#" + parent, 'div[data-container-type="chart"]');

        //     if (typeof elems !== 'undefined') {
        //         for (i = 0; i < elems.length; i++) {
        //             if (elems[i].id && elems[i].id === (id + idAppend)) {
        //                 elems[i].classList.remove("hidden");
        //             } else {
        //                 if (!elems[i].classList.contains("hidden")) {
        //                     elems[i].className += " hidden";
        //                 }
        //             }
        //         }
        //     }
        // };


        function init() {
            moduleEl = context.getElement();
            moduleConfig = context.getConfig();

            if (!utils.isMobileRequest()) {
                window.addEventListener('scroll', _scrollFunction,{passive:true});
            }

            // As soon as module gets loaded , trigger default action
            CommonService.bindOnModuleLoad('chart', function(id) {
                if (id === "price-trend-chart-buy" || id === "price-trend-chart-rent") {
                    var data, tabElement, meta = {};

                    tabElement = query(moduleEl, '#price-trend [data-module="tabs"]');
                    // meta = context.application.getModuleConfig(tabElement);
                    meta = moduleConfig; //$(tabElement).find('script[type="text/x-config"]').text();
                    meta.type = 'price-trend';
                    try {
                        meta = JSON.parse(meta);
                    } catch (ex) {
                        meta = {};
                    }
                    meta = moduleConfig; //$(tabElement).find('script[type="text/x-config"]').text();
                    meta.type = 'price-trend';
                    meta.duration = 36;
                    data = {
                        id,
                        meta,
                        hasModuleLoaded: true,
                        type: 'chart',
                        parent: 'price-demand',
                        category: id.split('-')[3]
                    };
                    _initChart(data);
                }
            }, ["price-trend-chart-buy", "price-trend-chart-rent"]);


            CommonService.bindOnLoadPromise().then(() => {

                let onLoadClass;
                $(moduleEl).find('[data-onLoadClass]').each(function() {
                    onLoadClass = $(this).data('onloadclass');
                    $(this).addClass(onLoadClass);
                });

                context.broadcast('loadModule', {
                    name: 'chart',
                    loadId: Box.DOM.query(moduleEl, '#buy-rent-availiable-property [data-lazymodule="chart"]').id
                });

                context.broadcast('loadModule', {
                    name: 'chart',
                    loadId: Box.DOM.query(moduleEl, '#price-demand [data-lazymodule="chart"]').id
                });

                // context.broadcast('loadModule', {
                //     name: 'slider',
                //     loadId: Box.DOM.query(moduleEl, '#buyrent-top-agent [data-lazymodule="slider"]').id
                // });

            });
            utils.setLastVisitedPageDetails(window.pageData); //for saving context of repeat users
            utils.updatePushNotificationTags('LOCALITY_OVERVIEW');
        }

        function _broadcastTracking(event, data) {
            data.dominantUnit = moduleConfig.dominantUnit;
            context.broadcast(event, data);
        }

        function _clearNearbySerpCookie() {
            utils.setcookie(INCLUDE_NEARBY_LOCALITIES_COOKIE, false);
        }

        function onclick(event, clickElement, elementType) {
            let dataset;
            switch (elementType) {
                case "score-ring-popup":
                    let popupid = $(clickElement).data('openid');
                    _openPopup({ id: popupid, modal: false });
                    _broadcastTracking('locality-score-popup', { label: `locality score - ${clickElement.dataset.score}` });
                    break;
                case "builder-link":
                case "loc-link":
                    _broadcastTracking('linkClicked', $(clickElement).data() || {});
                    break;
                case "openLeadFormViewPhone":
                    dataset = $(clickElement).closest("[data-type=list-element]").data("element-data");
                    var step = $(clickElement).data('step');
                    var salesType = $(clickElement).data('category');
                    let obj = {
                        companyId: dataset.id,
                        companyName: dataset.name,
                        companyPhone: dataset.contact,
                        companyRating: dataset.rating,
                        companyImage: dataset.image,
                        companyUserId: dataset.userId,
                        companyType: dataset.type,
                        step,
                        salesType
                    };
                    _broadcastTracking('openLeadFormViewPhone', {});
                    _openLeadForm(obj);
                    break;
                case "available-prop-link":
                    _clearNearbySerpCookie();
                    _broadcastTracking('available-prop-link', clickElement.dataset);
                    CommonService.ajaxify(clickElement.dataset.redirectUri);
                    break;
                case "taxonomy-card-link":
                    _clearNearbySerpCookie();
                    _broadcastTracking('taxonomy-card-link', clickElement.dataset);
                    CommonService.ajaxify(clickElement.dataset.redirectUri);
                    break;
                case "price-trend-link":
                    _clearNearbySerpCookie();
                    _broadcastTracking('price-trend-link', clickElement.dataset);
                    break;
                case "project-classification-link":
                    $(clickElement).addClass('active');
                    _broadcastTracking('project-classification-link', clickElement.dataset);
                    break;
                case "project-taxonomy-view-more":
                    _broadcastTracking('project-classification-view-more', clickElement.dataset);
                    break;
                case "serp-link":
                    _broadcastTracking('linkClicked', $(clickElement).data() || {});
                    _clearNearbySerpCookie();
                    break;
                case 'social-share':
                    context.broadcast('open_share_box', { page: window.pageData.trackPageType });
                    break;
                case 'bloglogo':
                    context.broadcast('localityBloglogoClick');
                    break;
                case 'postRead':
                    dataset = $(clickElement).closest('[data-type="list-element"]').find('[data-type="postUrl"]').attr('itemid');
                    context.broadcast('localityPostClick', dataset);
                    event.stopPropagation();
                    break;
            }
        }

        function onmessage(name, data) {
            switch (name) {
                case 'tabClicked':
                    _handleTabEvents(data.element);
                    break;
                case 'mapSvgLegendsModuleInitiated':
                    trigger(query(moduleEl, '[data-target-id=masterPlanContent]'), 'click');
                    break;
                case 'accordianClicked':
                    let sliderElement = queryAll(data.element, '[data-lazymodule="slider"]');
                    if (sliderElement.length) {
                        context.broadcast('loadModule', {
                            name: 'slider',
                            loadId: sliderElement[0].id
                        });
                    }
                    break;
                case 'nearByAmenityModuleInitiated':
                    if (!utils.isMasterPlanSupported({
                            id: moduleConfig.cityId,
                            name: moduleConfig.cityName
                        })) {
                        trigger(query(moduleEl, "[data-target-id='nearbyLocalityMap']"), 'click');
                    }
                    break;
                case "top-agent-buy-slider_slider_element_clicked":
                case "top-agent-rent-slider_slider_element_clicked":
                    let sellerData = data.data,
                        listingType = (name.indexOf('buy') > -1) ? 'buy' : 'rent',
                        url = filterConfigService.getSellerListingUrl({
                            id: sellerData.id,
                            name: sellerData.name,
                            type: 'AGENT'
                        }, false, {
                            listingType: listingType
                        });
                    _clearNearbySerpCookie();
                    CommonService.ajaxify(url);
                    break;
                case 'locality_sponsored_project_slider_element_clicked':
                    if (data && data.data && data.eventSource) {
                        if ($(data.eventSource.target).data("projectfield") == "projectField") {
                            data.data.trackLabel = $(data.eventSource.target).text();
                        } else {
                            data.data.trackLabel = "card";
                        }
                        if (data.data.overviewUrl) {
                            CommonService.ajaxify(data.data.overviewUrl);
                        }
                    }
                    break;
                case 'locality_sponsored_project_scale_carousel_element_clicked':
                    if (data.overviewUrl) {
                        CommonService.ajaxify(data.overviewUrl);
                    }
                    break;
                case "slider_clicked":
                    if (data.element.id == "top-builder-slider") {
                        if (data.event.toLowerCase() == 'right' || data.event.toLowerCase() == 'left') {
                            _broadcastTracking('builder_interaction', { label: data.event });
                        } else if (data.event.toLowerCase() == 'click') {
                            _broadcastTracking('builder_click', {});
                        }
                    } else if (data.element.id == "top-agent-buy-slider" || data.element.id == "top-agent-rent-slider") {
                        let listingCategory = data.element.id.indexOf('buy') > -1 ? 'buy' : 'rent';
                        if (data.event.toLowerCase() == 'right' || data.event.toLowerCase() == 'left') {
                            _broadcastTracking('seller_interaction', { label: data.event, listingCategory });
                        } else if (data.event.toLowerCase() == 'click') {
                            _broadcastTracking('seller_click', { event: data.event, listingCategory, name: JSON.parse(data.dataset.elementData).listingCount });
                        }
                    } else if (data.element.id == "nearby-locality-slider") {
                        if (data.event.toLowerCase() == 'right' || data.event.toLowerCase() == 'left') {
                            _broadcastTracking('nearby_locality_interaction', { label: data.event });
                        } else if (data.event.toLowerCase() == 'click') {
                            _broadcastTracking('nearby_locality_click', { label: data.dataset.localityId });
                        }
                    } else if (data.element.dataset.type == 'lifestyle-slider') {
                        _broadcastTracking('lifestyle-slider-bullet', { name: data.element.id.replace(/-/g, ' ') });
                    } else if (data.element.id == 'project-class-slider') {
                        _broadcastTracking('project-classification-link', data.dataset);
                    } else if (data.element.id == 'makaaniq-post') {
                        if (data.event == 'Right') {
                            context.broadcast('blogLocalityNextbtnClick');
                        } else if (data.event == 'Left') {
                            context.broadcast('blogLocalityBackbtnClick');
                        }
                    }
                    break;
                case 'tabs_click_track':
                    if (data.moduleEl.id == 'page-category-tab') {
                        _broadcastTracking('page-category-tab', data);
                    } else if (data.moduleEl.id == "buy-unittype-tab" || data.moduleEl.id == "rent-unittype-tab") {
                        let listingCategory = data.moduleEl.id.indexOf('buy') > -1 ? 'buy' : 'rent',
                            unitType = data.element.dataset.unitType;
                        _broadcastTracking('propertytype_tab', { listingCategory, unitType });
                    } else if (data.moduleEl.id == 'price-trend') {
                        let listingCategory = data.element.dataset.category;
                        _broadcastTracking('price-trend-tab', { listingCategory });
                    } else if (data.moduleEl.id == 'top-seller-slider') {
                        let listingCategory = data.element.dataset.category;
                        _broadcastTracking('top-seller-tabs', { listingCategory });
                    } else if (data.moduleEl.id == 'locality-compare') {
                        let listingCategory = data.element.dataset.category;
                        _broadcastTracking('localityCompare-tab', { listingCategory });
                    } else if (data.moduleEl.id == 'lifestyle-tabs') {
                        let name = data.element.dataset.targetId;
                        _broadcastTracking('lifestyle-tab', { name });
                    } else if (data.moduleEl.id == 'lifestyle-sub-tabs') {
                        let label = data.element.dataset.targetId.replace(/-/g, ' ');
                        _broadcastTracking('lifestyle-sub-tab', { label });
                    }
                    break;
                case 'serpLinkClicked':
                    _clearNearbySerpCookie();
                    break;
                case "track_navigation":
                    _broadcastTracking('navigation', { label: data.element.dataset.goto });
                    break;

                case 'locality_sponsored_project_scale_slider_auto_scroll':
                    context.broadcast('trackLocalitySponsoredAutoScroll', data);
                    break;

                case 'locality_sponsored_project_slider_element_viewed':
                    context.broadcast('trackLocalitySponsoredAutoScrollMobile', data);
                    break;
                case 'makaaniq-post_slider_element_clicked':
                    if (data.data) {
                        context.broadcast('localityPostClick', data.data);
                        window.open(data.data, '_blank');
                    }
                    break;
            }
        }

        function destroy() {
            //clear all the binding and objects
            if (!utils.isMobileRequest()) {
                window.removeEventListener('scroll', _scrollFunction,{passive:true});
            }
        }

        return {
            init,
            messages: ['locality_sponsored_project_slider_element_viewed', 'locality_sponsored_project_scale_slider_auto_scroll', 'locality_sponsored_project_slider_element_clicked', 'locality_sponsored_project_scale_carousel_element_clicked', 'tabClicked', 'mapSvgLegendsModuleInitiated', 'accordianClicked', 'nearByAmenityModuleInitiated', 'top-agent-buy-slider_slider_element_clicked', 'top-agent-rent-slider_slider_element_clicked', 'slider_clicked', 'tabs_click_track', 'serpLinkClicked', 'track_navigation', 'makaaniq-post_slider_element_clicked'],
            behaviors: ['localityOverviewTracking'],
            onmessage,
            onclick,
            destroy
        };
    });
});
