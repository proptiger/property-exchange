/**
 * Description: wrapper module for project page
 * @author: [Aditya Jha]
 * Date: Nov 30, 2015
 **/
"use strict";
define([
    'services/urlService',
    'services/localStorageService',
    'services/utils',
    'common/sharedConfig',
    'services/filterConfigService',
    'services/experimentsService',
    'services/commonService',
    'services/coachMarkService',
    'behaviors/propertyTracking',
    'behaviors/requestPhotosTracking',
    'services/viewStatus',
], function(urlService, LocalStorageService,UtilService,sharedConfig,filterConfigService, experimentsService) {
    Box.Application.addModule('propertyPage', function(context) {

        const PAGE_CATEGORY = 'Buyer_Property';

        const COUPON_COOKIE_NAME = "SELECT_COUPON_GENERATED",makaanSelectCity = sharedConfig.makaanSelectCities;

        const CommonService = context.getService('CommonService'),
            viewStatusService = context.getService('viewStatus'),
            coachMarkService = context.getService('coachMarkService'),
            query = Box.DOM.query,
            queryAllData = Box.DOM.queryAllData,
            SELLER_POPUP_BTN = "seller-popup-btn",
            SELLER_POPUP_ID = "seller-detail-popup",
            SELLER_GRID_CLASS = "js-seller-grid",
            NEXT_LISTING = "[data-next]",
            PREV_LISTING = "[data-prev]",
            SOURCEID = "sourceid",
            HIDE = 'hide',
            DISABLED = 'disabled',
            SELLER_LEAD_FORM_ID = "main_lead_form",
            ELEMENT_WIDTH = 220,
            MAKAAN_SELECT_URL = '/makaan-select',
            SIMILAR_PROP_CARD = ".js-similar-prop-card",
            TRACK_SIMILAR_LISTING_IN_VIEW = 'trackSimilarListingInView';

        let BUDGET_PERCENTAGE = .30; 
        let lastScrollTop=0;

        var moduleEl,
            config,
            //falseTrigger = true,
            messages = [
                'openmultipleSellerLead',
                'nearByAmenityModuleClicked',
                'customDirectionModuleClicked',
                'tabClicked',
                'nearByAmenityModuleInitiated',
                'popup:change',
                'Lead_Flow_Completed',
                'slider_clicked',
                'trackClosePopup',
                'trackSelectAllGrid',
                'trackViewMoreGrid',
                'openLeadForm',
                "popup:closed",
                "elementClicked",
                TRACK_SIMILAR_LISTING_IN_VIEW
            ],
            behaviors = ['propertyTracking', 'requestPhotosTracking'],
            galleryData,
            _state = {
                previousIndex : -1
            },
            eventFired = {};
        const ELEMENT_TYPE_SOURCE = {
            "image-div": "tap",
            "more-img": "count"
        };

        function _setPrevNextLink(id){
            let listings = LocalStorageService.getSessionItem('nextprev');
            let nextId, prevId, index;
                listings  = (listings && JSON.parse(listings)) || [];
                index = listings.indexOf(id);
            if(index != -1){
                prevId = listings[index-1];
                nextId = listings[index+1];
            }
            if(prevId){
                $(moduleEl).find(PREV_LISTING).data('id',prevId).removeClass(DISABLED);
            }
            if(nextId){
                $(moduleEl).find(NEXT_LISTING).data('id',nextId).removeClass(DISABLED);
            }
        }

        function _setPrevNextListing(){
            if(window.listingIds){
                window.listingIds = $(window.listingIds).toArray();
                LocalStorageService.setSessionItem('nextprev',JSON.stringify(window.listingIds));
            }
            _setPrevNextLink(window.pageData.listingId);
        }

        function _navigateToListingId(id){
            let url = window.location.href.replace(window.pageData.listingId, id);
            CommonService.ajaxify(url);
        }

        function _loadImages(){
            CommonService.bindOnLoadPromise().then(function() {
                $(moduleEl).find('[data-onloadimage]').addClass("onloadimage").removeAttr('data-onloadimage');
                $(moduleEl).find('.js-background-image').addClass('onloadimage');

                if(UtilService.isMobileRequest()) {
                    //Load the carousel module with config
                    CommonService.bindOnModuleLoad('carousel', () => {
                        let pageData = UtilService.getPageData();

                        context.broadcast("carousel-configData", {
                            id: pageData.listingId,
                            projectId: pageData.projectId,
                            propertyId: pageData.propertyId,
                            imageCount: config.imageCount,
                            builderId: pageData.builderId,
                            companyId: config.sellerId,
                            companyRating: config.sellerRating,
                            projectStatus: pageData.projectStatus,
                            price: config.price,
                            propertyType: pageData.unitType,
                            bedrooms: pageData.bhk,
                            localityId: pageData.localityId,
                            suburbId: pageData.suburbId,
                            cityId: pageData.cityId,
                            listingScore: pageData.listingScore,
                            listingCategory: pageData.listingCategory,
                            defaultImageId: config.defaultImageId,
                            mainImageWidth: config.mainImageWidth,
                            mainImageHeight: config.mainImageHeight,
                            gaCategory: "Gallery"
                        });
                    });
                }
            });
        }

        function showHideSelectBanner(){
            let pageData = UtilService.getPageData(),
                cityId = pageData.cityId,
                couponData = LocalStorageService.getItem(COUPON_COOKIE_NAME);
            if(makaanSelectCity.indexOf(cityId) > -1 && pageData.listingType=="rent"){
                if(!couponData){
                    $(moduleEl).find('.js-select-banner').removeClass('hide');
                    context.broadcast('selectBannerSeen');
                    showSelectCoachMark();
                }else {
                    if(UtilService.isStorageExceedsTime(couponData,sharedConfig.couponShareEligibleTime)){
                        $(moduleEl).find('.js-select-reminder').removeClass('hide');
                        showSelectCoachMark();
                        context.broadcast('selectReminderSeen');
                    }
                    if(config.isMakaanSelectSeller && config.isMakaanSelectSeller.toString() == "true" && pageData.listingType=="rent"){
                        $(moduleEl).find('.js-ms-promotion').removeClass('hide');
                        context.broadcast('selectListingSeen',{source:pageData.listingId||''});
                    }
                }
            }
        }
        function switchExperiments(){
            experimentsService.switchElements('makaan_pdp_similar');
            experimentsService.experimentElementsClass(document.getElementsByClassName('body-content'), 'makaan_prop_page', 'new', 'new-prop-view');
        }

        function init() {
            // loads submodules once mapsModule is loaded
            var isMobileRequest = UtilService.isMobileRequest();
            if (isMobileRequest) {
               switchExperiments();
            }
            moduleEl = context.getElement();
            config = context.getConfig() || {};

            _loadImages();

            if(isMobileRequest && experimentsService.getExperimentVariation('makaan_pdp_deficit')=="show"){
                $(moduleEl).find('.js-prop-deficit').removeClass('hide');
            }

            CommonService.loadGoogleAds();

            viewStatusService.setAsSeen({
                id: config.listingId,
                type: 'listing'
            });
            CommonService.bindOnModuleLoad('lead', function(id){
                var urlParams = urlService.getAllUrlParam();
                if(!$(moduleEl).find("[data-prop-list] li").length){
                    context.broadcast('lead_no_similar', {
                        "id": id
                    });
                }
                if(urlParams.event=="OpenLeadForm"){
                    _openLeadForm();
                }
            },['main_lead_form','lead-popup']);
            if (isMobileRequest) {
                _setSectionMobileWidth();
                _trackSimilarPropertyScroll();
            }
            window.addEventListener('scroll', _scrollFunction,{passive:true});

            CommonService.bindOnLoadPromise().then(function(){
                context.broadcast('loadModule', [{
                    name: 'reportError',
                    id: 'propertyReportError'
                }, {
                    name: 'dealMaker-notification'
                }]);

                context.broadcast('loadModule', [{
                    name: 'similarProperty',
                    id: 'similarPropertyContainer'
                }]);

                if (!isMobileRequest) {
                    context.broadcast('loadModule', [{
                        name: 'chart',
                        id: 'emiChart'
                    }]);
                    $(moduleEl).on('mouseenter','[data-type="propertySelectBadge"]',(event)=>{
                        context.broadcast('trackSelectBadgeTooltip');
                    });
                }
            });
            let ratingBarsModule = moduleEl.querySelector('[data-module="ratingBars"]');
            if(ratingBarsModule){
                CommonService.bindOnModuleLoad('ratingBars', ()=>{
                    context.broadcast('reloadRatingBars', {
                        ratingCount: config.ratingsCount,
                        listingUserCompanyId: config.companyId,
                        listingUserCompanyUserId: config.sellerUserId,
                        id: ratingBarsModule.id
                    });
                }, [ratingBarsModule.id]);
            }

            let sellerScoreModule = moduleEl.querySelector('[data-module="sellerScore"]');
            if(sellerScoreModule){
                CommonService.bindOnModuleLoad('sellerScore', () => {
                    context.broadcast('reloadSellerScore', {
                        companyRating: config.sellerRating,
                        listingUserId: config.sellerUserId,
                        isSellerProfile: false,
                        id: sellerScoreModule.id
                    });
                }, [sellerScoreModule.id]);
            }

            updateOtherSellersLink();
            $(window).trigger("scroll");
            CommonService.bindOnLoad(_setPrevNextListing);
            UtilService.updatePushNotificationTags('LISTING_OVERVIEW');
            showHideSelectBanner();
            let urlData = urlService.getAllUrlParam();
            if (urlData.showPopup && urlData.showPopup == 'mProfile' && urlData.uid) {
                CommonService.bindOnModuleLoad('userProfiling', function () {
                    context.broadcast("mProfilePopupOpen", {
                        showPopup: urlData.showPopup,
                        userId: urlData.uid,
                        source: urlData.source
                    });
                });
            } 
            checkSimilarListingInView();
        }

        function showSelectCoachMark(){
            if (UtilService.isMobileRequest()) {
                coachMarkService.initBannerCoachMark();
            }
        }
        

        function getLeadData(clickElementData = {}){
            let pageData = UtilService.getPageData();
            config = config || {};
            let rawData = {
                companyId: config.sellerId,
                companyName: config.sellerName,
                companyPhone: config.sellerPhone,
                companyRating: config.sellerRating,
                companyImage: config.sellerImage,
                companyAssist: config.sellerAssist,
                companyUserId: config.sellerUserId,
                companyType: config.sellerType,
                cityId: pageData.cityId,
                listingId: pageData.listingId,
                propertyType: [config.propertyTypeId],
                propertyTypeLabel: pageData.unitType,
                projectId: pageData.projectId,
                projectName: pageData.projectName,
                imageUrl: config.imageUrl,
                localityId: pageData.localityId,
                bhk: pageData.bhk,
                minBudget: pageData.budget,
                maxBudget: pageData.budget,
                budget: pageData.budget,
                id: "lead-popup",
                moduleId: moduleEl.id,
                isAccountLocked: config.isAccountLocked,
                isMakaanSelectSeller : config.isMakaanSelectSeller,
                hasTenantPreferences: config.hasTenantPreferences,
                imageCount: config.imageCount,
                isCommercial: pageData.isCommercial
            };
            rawData = $.extend(rawData, clickElementData);
            return rawData;
        }

        function _trackSimilarPropertyScroll() {
            $(moduleEl).find('.js-similarPropProject-upperPane').on("scroll",function(e){
                _trackScroll(e,'project');
            });
            $(moduleEl).find('.js-similarPropSeller-upperPane').on("scroll",function(e){
                _trackScroll(e,'seller');
            });
        }

        function _trackScroll(e,cardType) {
            let horizontal = e.currentTarget.scrollLeft,
                data;

            let index = Math.ceil((horizontal/ELEMENT_WIDTH)+0.5)-1;
            if(index != _state.previousIndex){
                data = $(moduleEl).find(`.pick-${cardType}-${index}`).data('element-data');
                if(index < _state.previousIndex) {
                    context.broadcast("leftSimilarPropertyScroll",$.extend({'label':'Left','cardType':cardType},data));
                } else if(index > _state.previousIndex) {
                    context.broadcast("rightSimilarPropertyScroll",$.extend({'label':'Right','cardType':cardType},data));
                }
                _state.previousIndex = index;
            }
        }

        function _openLeadForm(dataset = {}) {
            let leadData = getLeadData(dataset);
            context.broadcast('leadform:open', leadData);
        }

        //Duration of the top scrolling animation (in ms)
        let scroll_top_duration = 700;
        // scrolling function
        function targetedScroll(id) {
            // scrollTop is either the top offset of the element whose id is passed, or 0
            //var scrollTop = id ? $('#' + id).offset().top : 0;
            var scrollTop = id ? ($('#' + id).offset().top - 200) : 0;

            $('body,html').animate({
                scrollTop: scrollTop,
            }, scroll_top_duration);
        }

        function _startGallery(moduleName, moduleId){
            CommonService.bindOnLoadPromise().then(() => {
                context.broadcast('loadModule', {
                    "name": moduleName,
                    "loadId" : moduleId
                });
            });
        }
        function _loadRatingReviewWrapper(element){
            context.broadcast('loadRatingReviewWrapper',{
                element,
                position: 'top_bottom',
                companyName: config.sellerName,
                companyType: config.sellerType,
                rating: (config.sellerRating||0),
                companyRating: config.sellerRating,
                ratingCount: config.ratingsCount,
                reviewCount: config.reviewsCount,
                companyId: config.sellerId,
                companyUserId: config.sellerUserId,
                leadFormData: getLeadData({
                    source: "Headline",
                    step: "CALL_NOW",
                    type: "mobile-connect-now"
                }),
                isPaidSeller: config.isPaidSeller
            });
        }

        function launchGallery(dataset, elementType){
            let variation = experimentsService.getExperimentVariation('makaan_gallery_revamp'),
                galleryData = {
                    id: "main_gallery_desktop",
                    startIndex: dataset.index
                },
                moduleName = "gallery", 
                moduleId = "main_gallery_desktop";

            if(UtilService.isMobileRequest()){
                switch(variation){
                    case 'gallery_new':
                        galleryData['version'] = "v2_mobile";
                    break;

                    case 'gallery_grid':
                        galleryData['id'] = "galleryGrid";
                        moduleName = moduleId = "galleryGrid";
                    break;
                }
            } 
            context.broadcast("trackPropertyGalleryBucketClick",{ bucket: dataset.bucket,sourceModule: dataset.sourcemodule, sourceElement:  ELEMENT_TYPE_SOURCE[elementType]});

            if (!CommonService.checkModuleLoaded(moduleName, moduleId)) {
                _startGallery(moduleName, moduleId);
            }

            CommonService.bindOnModuleLoad(moduleName, function() {
                context.broadcast('start_gallery', galleryData);
            }, [moduleId]);
        }

        function onclick(event, clickElement, elementType) {
            let clickElementData = $(clickElement).data() || {},popupId;
             switch (elementType) {
                case "scrollto":
                    // bind smooth scroll to any anchor on the page
                    event.preventDefault();
                    targetedScroll($(clickElement).data('goto'));
                    break;
                case 'view-on-map':
                    let leadData = getLeadData();
                    $.extend(clickElementData, leadData);   
                    clickElementData.price = config.displayPrice;
                    clickElementData.priceUnit = config.displayPriceUnit;
                    clickElementData.size = config.size;
                    clickElementData.priceperunit = config.pricePerUnit;
                    clickElementData.avatarText = config.selleravatarText;
                    clickElementData.avatarTextColor = config.selleravatarTextColor;
                    clickElementData.avatarBGColor = config.selleravatarBackgroundColor;
                    context.broadcast('viewOnMap', clickElementData);
                    context.broadcast('trackViewOnMapProperty');
                    break;
                case 'breadcrum':
                    if(clickElementData.trackLabel){
                        UtilService.trackEvent('CLICK_Property_You are Here', PAGE_CATEGORY, clickElementData.trackLabel);
                    }
                    break;
                case 'top-button':
                    UtilService.gotoEl('body', 0, 0);
                    break;
                case 'floating-callback':
                    context.broadcast('trackOpenFloatingLeadProperty');
                    _openLeadForm(clickElementData);
                    break;
                case 'mobile-connect-now':
                    context.broadcast('trackLeadOpenProperty');
                    _openLeadForm(clickElementData);
                    break;
                case "Dealmaker_Open_lead_form":
                    context.broadcast('trackDealMakerLeadOpen',{
                        label: config.isPaidSeller?'connectRHS_DM':'connectRHS'
                    });
                    clickElementData.source = "deal_maker";
                    _openLeadForm(clickElementData);
                    break;
                case "dummy-img":
                case "connect-now":
                    var label = '';
                    let __aElem = $(clickElement);
                    if(__aElem && __aElem[0] && __aElem[0].text) {
                        let _label = __aElem[0].text.toLowerCase();
                        if(_label === 'request for site visit' ) {
                            label = 'keyDetails';
                        } else if (_label === 'request more details' ) {
                            label = 'amenities';
                        } else if (_label === 'connect now' ) {
                            label = 'propertyDetails';
                        } else if ( _label.indexOf('gallery') >= 0 || _label.indexOf('interested in this property') >=0 ) {
                            label = 'gallery';
                        }
                    }
                    if(label) {
                        context.broadcast('trackPropertyLeadFormOpen', {action:"open", label, sourceModule: clickElementData.source});
                    }
                    _openLeadForm(clickElementData);
                     break;
                case 'builder-facts-item':
                    if(clickElementData.trackValue){
                        context.broadcast('trackBuilderLisitngsClickProperty',{
                            value: clickElementData.trackValue,
                            label: clickElementData.trackLabel
                        });
                    }
                    let url = clickElementData.url;
                    if(url){
                        urlService.ajaxyUrlChange(url);
                    }
                    break;
                case 'next-listing':
                case 'prev-listing':
                    context.broadcast('trackNextPrevHeadlineProperty', {type:elementType});
                    let id = $(clickElement).data('id');
                    if(id){
                      _navigateToListingId(id);
                    }
                    break;
                case SELLER_POPUP_BTN:
                    let source = clickElementData.openid;
                    _openSellerPopup(source);
                    context.broadcast('trackViewOtherSellersProperty', {});
                    break;
                case "image-div":
                case "more-img":
                case "open-desktop-gallery":
                    launchGallery(clickElementData, elementType);
                    break;
                case 'navigate':
                    context.broadcast('trackStickyHeaderClickProperty', {label: clickElementData.trackLabel});
                    break;

                case 'track-locality-detail':
                    context.broadcast('trackLocalityDetailClickProperty', {
                        value: clickElementData.trackValue
                    });
                    break;

                case 'track-builder-detail':
                    context.broadcast('trackBuilderDetailClickProperty', {
                        value: clickElementData.trackValue
                    });
                    break;

                case 'track-project-overview':
                    context.broadcast('trackProjectDetailClickProperty');
                    break;

                /* </tracking>  */
                case "score-ring-popup":
                    let popupid = clickElementData.openid;
                    _openPopup({
                        id: popupid
                    });
                    if(clickElementData.trackType === 'Project'){
                        context.broadcast('trackProjectScoreProperty');
                    } else if(clickElementData.trackType === 'Locality'){
                        context.broadcast('trackLocalityScoreProperty');
                    }
                    break;

                case 'social-share':
                    context.broadcast('open_share_box', { page: window.pageData.trackPageType });
                    break;

                case 'report-error':
                    _openPopup({
                        id: 'reportErrorPopup'
                    });
                    context.broadcast('trackReportErrorPopup');
                    break;
                case "prop-link":
                    context.broadcast('trackSimilarPropertyLinkClick',clickElementData);
                    break;
                case 'similar_prop_card':
                    context.broadcast('similarPropertyClick',$.extend({'label':'Click'},$(clickElement).data('element-data'), clickElementData));
                    break;
                case 'seeAll-btn':
                    context.broadcast('similarPropertySeeAll',({'label':'See All','cardType': $(clickElement).data('cardtype')}));
                    break;
                case 'more_prop_card':
                    context.broadcast('trackMorePropertiesProperty', clickElementData);
                    break;
                case 'more-seeAll-btn':
                    context.broadcast('trackMorePropertiesSeeAllProperty', clickElementData);
                    break;
                case 'loadReviews':
                    CommonService.bindOnModuleLoad('sellerReviews', ()=>{
                        context.broadcast('popup:open',{
                            id: 'sellerReviewsPopup'
                        });
                        context.broadcast('loadSellerReviews:fromSummary',{
                            companyId: config.sellerId,
                            companyUserId: config.sellerUserId,
                            companyName: config.sellerName,
                            companyImage: config.sellerImage,
                            companyType : config.sellerType && config.sellerType.toLowerCase()==="broker"?"AGENT":config.sellerType,
                            isDealMaker: config.isDealMaker,
                            isExpertDealMaker:config.isExpertDealMaker,
                            source: 'reviewToolTip',
                            otherInfo: {
                                sellerRating: config.rating,
                                sellerStatus: config.isPaidSeller
                            }
                        });
                    });
                break;
                case 'seller-rating':
                    context.broadcast('popup:open', {
                        id: 'ratingReviewWrapperPopup',
                        options: {
                            config: {
                                modal: false
                            }
                        }
                    });
                    _loadRatingReviewWrapper();
                    context.broadcast('trackSellerTooltip',{
                        event: 'click',
                        label: 'property'
                    });
                    break;
                default:
                    break;
                case'sideWidgetMSKnowMore':
                    context.broadcast('trackSelectKnowMore',{source:'DealMaker_SideWidget'});
                    popupId = UtilService.isMobileRequest() ? 'msKnowMorePopup':'msKnowMoreDeskPopup';
                    _openPopup({id :popupId,modal:true});
                    break;
                case 'selectKnowMore':
                    context.broadcast('hideSelectCoachMark', {});
                    context.broadcast('trackSelectKnowMore',{source:'Property'});
                    popupId = UtilService.isMobileRequest() ? 'msKnowMorePopup':'msKnowMoreDeskPopup';
                    _openPopup({id :popupId,modal:true});
                    break;
                case 'promotionKnowMore':
                    context.broadcast('trackSelectKnowMore',{source:'Promotion_Property'});
                    popupId = UtilService.isMobileRequest() ? 'msKnowMorePopup':'msKnowMoreDeskPopup';
                    _openPopup({id :popupId,modal:true});
                    break;
                case 'reminderUnlock':
                    context.broadcast('hideSelectCoachMark', {});
                    context.broadcast('trackReminderUnlock');
                    window.location.href = MAKAAN_SELECT_URL;
                    break;
                case 'propertySelectBadge':
                    if(UtilService.isMobileRequest()){
                        context.broadcast('trackSelectBadgeTooltip');
                    }
                    break;
                case 'similar-prop-card':
                    _redirectingToSerp();
                    context.broadcast("trackSimilarCardClick", {
                        card: clickElementData.card,
                        pageType: clickElementData.page
                    });
                    break;
            }
        }

        function _redirectingToSerp() {
            let pageData = UtilService.getPageData(),
                budget = pageData.budget,
                filtersSet = {
                    listingType: pageData.listingType,
                    cityName: encodeURI(pageData.cityName),
                    localityName: encodeURI(pageData.localityName),
                    suburbName: encodeURI(pageData.suburbName),
                    cityId: pageData.cityId,
                    localityId: pageData.localityId,
                    suburbId: pageData.suburbId
                };

            const hasForwardFlowVariation = experimentsService.hasExperimentVariation("makaan_similar_filter", "new");
            if(hasForwardFlowVariation) {
                const beds = pageData.bhk;
                filtersSet.beds = beds && (pageData.bhk > 1 ? [beds, beds+1, beds - 1] : [beds, beds+1]).join(",");
                filtersSet.furnished = pageData.furnished === "Unfurnished" ? "unfurnished,semifurnished" : "furnished,semifurnished";
                BUDGET_PERCENTAGE = 0.1;
            }
            if (budget) {
                filtersSet.minPrice = Math.floor(budget - budget * BUDGET_PERCENTAGE);
                filtersSet.maxPrice = Math.ceil(budget + budget * BUDGET_PERCENTAGE);
            }
            let url = UtilService.generateSerpFilter(filtersSet);
                url = `listings${url}`;
            window.open(url,'_self');
        }

        function _openSellerPopup(source) {
            let sellerpopup = $(`#${SELLER_POPUP_ID}`),
                gridContent = $(`#${source} .${SELLER_GRID_CLASS}`).detach();
            sellerpopup.data(SOURCEID, source);
            sellerpopup.append(gridContent);
            context.broadcast('popup:open', {
                id: SELLER_POPUP_ID
            });
        }

        function _setSectionMobileWidth() {
            let scrollingList = $(moduleEl).find('.js-mobscroll');
            for (let i = 0, length = scrollingList.length; i < length; i++) {
                let currentElement = $(scrollingList[i]),
                    elements = currentElement.find('.js-moblist-item'),
                    length = elements.length,
                    width = $(elements[0]).outerWidth(true);
                currentElement.width(width * length);
            }
        }

        function _openPopup(config) {
            context.broadcast('popup:open', {
                id: config.id,
                options: {
                    config: {
                        modal: config.modal
                    }
                }
            });
        }

        function onmessage(name, data) {
            if (name == 'tabClicked') {
                _togglePlayer($(data.element).data());
                _handleTabEvents(data);
            } else if (name === 'nearByAmenityModuleInitiated') {
                let selector = query(moduleEl, "[data-target-id=neighbourhoodContent]");
                if (selector) {
                    selector.click();
                } else {
                    context.broadcast('loadNearByAmenityModule');
                }
            } else if (name == 'popup:change') {
                if (data && data.el && data.el.id == SELLER_POPUP_ID) {
                    let sourceid = $(data.el).data(SOURCEID),
                        content = $(data.el).find(`.${SELLER_GRID_CLASS}`).detach();
                    $(`#${sourceid}`).html(content);
                }
            } else if (name == 'openmultipleSellerLead') {
                let _data = {
                    id: 'twosteppyr-lead',
                    type: "GET_CALLBACK",
                    isPopup: true,
                    otherCompany: data.sellerIdArray,
                    sourceSection: "PropertyMultiSeller",
                    salesType: UtilService.getPageData('listingType'),
                    trackOpen : true,
                    sectionClicked: 'propertySimilarSellers',
                    label: 'PYR',
                    category: 'LEAD FORM'
                };

                CommonService.bindOnModuleLoad('twosteppyr', function(){
                    context.broadcast('openTwoStepPyr', _data);
                    _openPopup({
                        id: 'twosteppyr-popup',
                        modal: true
                    });
                });

            } else if(name === "Lead_Flow_Completed"){
                if(data.id === SELLER_LEAD_FORM_ID) {
                    let similar = $(moduleEl).find("[data-lead-similar]");
                    $(moduleEl).find("[data-lead-container]").html(similar.html()).removeClass('hide');
                    similar.html('');
                }
            } else if (name == 'slider_clicked' && data.element.id=='similar-property') {
                context.broadcast('property_similarSlider', {event:data.event});
            } else if (name === 'trackViewMoreGrid' && data.moduleEl) {
                let broadcastData = $(data.moduleEl).data();
                if(broadcastData.source){
                    context.broadcast('trackGridViewMoreProperty', broadcastData);
                }
            } else if (name === 'trackSelectAllGrid' && data.moduleEl) {
                let broadcastData = $(data.moduleEl).data();
                if(broadcastData.source){
                    context.broadcast('trackGridSelectAllProperty', broadcastData);
                }
            } else if (name === 'trackClosePopup'){
                let broadcastData = $(data.moduleEl).data();
                if(broadcastData.gridSource){
                    context.broadcast('trackGridCloseProperty', broadcastData);
                }
            } else if(name === 'openLeadForm'){
                context.broadcast("trackLeadOpen", data);
                _openLeadForm(data);
            }else if (name === 'popup:closed') {
                if(data && data.moduleEl && (data.popupId=="msKnowMorePopup" || data.popupId=="msKnowMoreDeskPopup"))
                context.broadcast('msKnowMorePopupClose');
            }else if(name=="elementClicked"){
                let url;
                switch (data.elementType){
                    case 'searchSelectProperties':
                        context.broadcast('trackPopupSearchProperties');
                        let urlparams = {postedBy:'selectAgent',sellerId:'',builderId:'',projectId:''};
                        url =  filterConfigService.getUrlWithUpdatedParams({overrideParams: urlparams});
                        window.location.href = url;
                        break;
                    case 'unlockSelectVoucher':
                        context.broadcast('trackPopupUnlockVoucher')
                        window.open(MAKAAN_SELECT_URL);
                        break;
                }
            } else if (name === TRACK_SIMILAR_LISTING_IN_VIEW) {
                if (UtilService.checkInViewPort(SIMILAR_PROP_CARD)) {
                    data.elements && data.elements.forEach(function (item) {
                        if (item.target.hasChildNodes()){
                            let cardData = item.target.dataset;
                            if (cardData) {
                                context.broadcast("trackSimilarCardSeen", {
                                    card: cardData.card,
                                    pageType: cardData.page
                                });
                            }
                        }
                    });
                }
            }
        }

        function checkSimilarListingInView(){
            let elements = moduleEl.querySelectorAll(SIMILAR_PROP_CARD);
            if (elements.length) {
                UtilService.trackElementInView(elements, SIMILAR_PROP_CARD, '', TRACK_SIMILAR_LISTING_IN_VIEW, true);
            }
        }

        function destroy() {
            window.removeEventListener('scroll', _scrollFunction,{passive:true});
            $('.header').removeClass(HIDE);
            if (!UtilService.isMobileRequest()) {
                $(moduleEl).find('.js-similarPropProject-upperPane').off("scroll");
                $(moduleEl).find('.js-similarPropSeller-upperPane').off("scroll");
                $(moduleEl).off('mouseenter','[data-type="propertySelectBadge"]');
            }
        }

        function _loadSubmodules() {
            context.broadcast('loadModule', [{
                parent: 'mapsModule',
                name: 'customDirectionModule'
            }, {
                parent: 'mapsModule',
                name: 'nearByAmenityModule'
            }, {
                parent: 'mapsModule',
                name: 'mapSvgLegendsModule'
            }, {
                parent: 'mapsModule',
                name: 'communicatorModule'
            }]);
        }

        function updateOtherSellersLink(){
            let sellerCount = $(Box.DOM.query(moduleEl, "[data-other-sellers]")).data('seller-count');
            if(sellerCount){
                $('.js-seller-count-parent').removeClass("hide");
                $('.js-seller-count').append(' ('+sellerCount+')');
            }
        }

        function _togglePlayer(data) {
            let playerAction = data.player;
            if (playerAction == 'play') {
                context.broadcast('loadModule', [{
                    name: 'jwplayer'
                }]);
            } else if (playerAction == 'pause') {
                context.broadcast('pausePlayer');
            }
        }

        var sticky = $('[data-sidebar-fixed-container]'),
            footer = $('[data-page-footer]'),
            //topFold = $('[data-top-fold]'),
            navigation = $('[data-type="navigation"]'),
           // navigationOffset = navigation.offset() && $('[data-type="navigation"]').offset().top,
            stickyTop = (sticky.length && sticky.offset().top),
            galleryContainerHeight = $('[data-top-area-wrap]').offset().top + $('[data-top-area-wrap]').outerHeight(true);

        var isMapModuleCalled, userScrolled;
        function _scrollFunction() {
            context.broadcast("propertyPageScrolled");
            sticky = sticky || $('[data-sidebar-fixed-container]');
            let scrollTop = $(window).scrollTop(),
                stickyHeight = sticky.outerHeight(),
                footTop = footer.offset().top,
                topFoldHeight = galleryContainerHeight, //topFold.outerHeight(true),
                headerHeight = $('[data-header]').outerHeight(true),
                carousel = UtilService.isMobileRequest() && $(moduleEl).find(".imgWrap").length ? $(moduleEl).find(".imgWrap") : 
                                $(queryAllData(moduleEl, "container", "carousel")),
                fixedHeaderEl = $(query(moduleEl, "#fixedDataHeader")),
                fixedHeaderElHeight = fixedHeaderEl.outerHeight(true),
                boundary = carousel.offset().top + carousel.outerHeight(true),
                //boundaryNavigation = navigationOffset && (navigationOffset - fixedHeaderElHeight - headerHeight),
                navigationHeight = navigation.outerHeight(true);
                //windowHeight = $(window).height(),
                //isMobileRequest = UtilService.isMobileRequest();


            if(!isMapModuleCalled && !UtilService.isMobileRequest() && UtilService.checkInViewPort('.js-map-section')){
                isMapModuleCalled = true;
                CommonService.bindOnLoadPromise().then(()=>{
                    context.broadcast('loadModule', {
                        "name": 'mapsModule'
                    });
                });
                CommonService.bindOnModuleLoad('mapsModule', _loadSubmodules);
            }
           if(scrollTop > boundary) {
               fixedHeaderEl.addClass('posfixed').removeClass(HIDE);
               if(scrollTop > lastScrollTop) {
                    $('.header').addClass(HIDE);  //jshint ignore:line
                    fixedHeaderEl.removeClass('withheader');
                    sticky.removeClass('withheader');
               } else {
                    $('.header').removeClass(HIDE);  //jshint ignore:line
                    fixedHeaderEl.addClass('withheader');
                    sticky.addClass('withheader');
               }

               if(!userScrolled){
                   userScrolled = true;
                   context.broadcast('trackInteractiveUserScroll');
               }

           } else {
               fixedHeaderEl.removeClass('posfixed').addClass(HIDE);
           }

            if (sticky.length > 0) {
                if (scrollTop > boundary) {
                    fixedHeaderElHeight = fixedHeaderElHeight || fixedHeaderEl.outerHeight(true);
                }
            }

            if(scrollTop < stickyTop - headerHeight - fixedHeaderElHeight - navigationHeight){
                sticky.removeClass('posfix');
            }else{

                let absolutePosition  = footTop - stickyHeight - topFoldHeight - fixedHeaderElHeight - headerHeight - navigationHeight - $('[data-fixed-lead-stop-bottom]').outerHeight(true) - 100; // 350 is popular searches (seo widget) height
                if(scrollTop > absolutePosition + topFoldHeight){
                    sticky.css({position:"absolute",top:(absolutePosition+fixedHeaderElHeight+navigationHeight + headerHeight) +'px',transition:'0s'});
                    sticky.removeClass('posfix');
                }else{
                    sticky.removeAttr('style');
                    sticky.addClass('posfix');
                }
            }

            lastScrollTop = scrollTop; //rememeber this for next scroll event
        }

        function trackTabEvent(data){
            let target = data.element,
                source = data.prevElement;
            let trackData = {};
            trackData = $(target).data();
            trackData.source = $(source).data('tabName');
            if(UtilService.isMobileRequest()){
                context.broadcast("trackTabToggleProperty", trackData);
            }
        }

        function _handleTabEvents(data) {
            let originalData = $.extend(true, data);
            data = data.element;
            if (!(data && data.getAttribute('data-target-id'))) {
                return;
            }
            trackTabEvent(originalData);
            let targetId = data.getAttribute('data-target-id');
            if (targetId === 'neighbourhoodContent') {
                context.broadcast('removeCustomDirectionModule');
                context.broadcast('loadNearByAmenityModule');
                context.broadcast('activateMasterPlan', false);
            } else if (targetId === 'masterPlanContent') {
                context.broadcast('activateMasterPlan', true);
                context.broadcast('removeAmenityModule');
                context.broadcast('removeCustomDirectionModule');
                context.broadcast('removeCommunicatorModule');
            } else if (targetId === 'commuteContent') {
                context.broadcast('activateMasterPlan', false);
                context.broadcast('removeAmenityModule');
                context.broadcast('loadCustomDirectionModule');
            }
        }
        function onmouseover(event, element, elementType) {
            switch(elementType){
                case 'rating':
                case 'reviews':
                case 'dealsclosed':
                    //to avoid duplicate event firing, check if already event triggered or not
                    if(!eventFired[elementType]){
                        context.broadcast('trackSellerTooltip',{
                            event: 'hover',
                            label: elementType
                        });
                        eventFired[elementType] = true;
                    }
                break;
            }
        }
        return {
            messages,
            behaviors,
            init,
            onclick,
            onmessage,
            destroy,
            onmouseover
        };
    });
});
