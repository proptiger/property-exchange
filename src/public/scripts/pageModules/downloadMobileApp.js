"use strict";
define([
    'common/sharedConfig',
    'services/apiService',
    'services/utils',
    'services/commonService',
    'behaviors/downLoadMobileAppTracking'
], function() {
    Box.Application.addModule("downloadMobileApp", (context) => {

       // const CommonService = context.getService('CommonService'),
         //   logger = context.getService('Logger');
        var config, moduleEl, $, $moduleEl;

        var behaviors = ['downLoadMobileAppTracking'];

        function init() {
            moduleEl = context.getElement();
            $ = context.getGlobal('jQuery');
            $moduleEl = $(moduleEl);
            //initialization of all local variables
        }

        function destroy() {
            //clear all the binding and objects
            config = null;
            moduleEl = null;
            $moduleEl = null;
        }

        function onmouseover(event, element, elementType){
            switch(elementType){
                case "ANDROID_HOVER":
                    document.getElementById("togglebgimg").removeClass('iphone').addClass('android');
                break;
                case "IOS_HOVER":
                    document.getElementById("togglebgimg").removeClass('android').addClass('iphone');
                break;
            }
        }

        function onmessage() {
            // bind custom messages/events
        }

        function onclick() {
            // bind custom messages/events
        }

        return {
            init,
            onmouseover,
            behaviors,
            onmessage,
            onclick,
            destroy
        };
    });
});

