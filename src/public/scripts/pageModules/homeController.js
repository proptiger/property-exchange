"use strict";
define([
    'common/sharedConfig',
    'services/shortlist',
    'services/viewStatus',
    'services/commonService',
    'services/defaultService',
    'services/loginService',
    'services/localStorageService',
    'services/apiService',
    'services/utils',
  
    'behaviors/homeTracking'
], function(sharedConfig,
    shortlistService,
    viewStatusService,
    commonService,
    defaultService,
    loginService,
    localStorageService) {
    Box.Application.addModule('homeController', function(context) {
        const SLIDER = '[data-module=slider]';
        const LoginService = context.getService('LoginService'),
            apiService = context.getService('ApiService'),
            utilService = context.getService("Utils"),
            smsUrl = sharedConfig.apiHandlers.smsService().url,
            queryData = Box.DOM.queryData,
            MOVE_TO_SEARCH = '.moveuptosearch',
            SEARCH_WRAP='#homeSearchWrapper',
            RECENT_DEAL_WRAP = '.recent-deals-section',
            TOP_DEAL_MAKER_WRAP = '.top-dealmaker-wrap',
            FEATURED_PROJECT_WRAP = '.featured-project-wrap',
            SELLER_RATED_WRAP = '.how-sellers-reated-section',
            SELL_RENT_WRAP = '.sell-rent-section',
            BLOG_WRAP = '.blog-section',
            INVESTORS_WRAP = '.our-investors-section',
            SMS_TEMPLATE_STRING =
            "Get connected with 10000+ highly rated sellers on Makaan. Download makaan app now. http:\/\/bit.ly/2yl2tO9";
        const FAVOURITE_LINK = 'favourites',
            RECENT_ACTIVITY_LINK='rencentlyViewed';
        let defaultCityId, trackCityClick=true;
        var config, moduleEl, $, $moduleEl, $window, instances = [];
        let recentVisitListingIds,
            shortListListingIds,
            $moveToSearch,
            SECTION_VISIBLE_EVENTS = {
                RECENT_DEAL:{
                    eventFired:false,
                    label:'recentDealSection',
                    wrap: RECENT_DEAL_WRAP
                },
                TOP_DEAL_MAKER:{
                    eventFired:false,
                    label:'topDealMakerSection',
                    wrap: TOP_DEAL_MAKER_WRAP
                },
                FEATURED_PROJECT:{
                    eventFired:false,
                    label:'feactureProjectSection',
                    wrap: FEATURED_PROJECT_WRAP
                },
                SELLER_RATED:{
                    eventFired:false,
                    label:'sellerRatedSection',
                    wrap: SELLER_RATED_WRAP
                },
                SELL_RENT:{
                    eventFired:false,
                    label:'sellRentSection',
                    wrap: SELL_RENT_WRAP
                },
                BLOG:{
                    eventFired:false,
                    label:'makaanIQBlogSection',
                    wrap: BLOG_WRAP
                },
                INVESTORS:{
                    eventFired:false,
                    label:'investorDetailsSection',
                    wrap: INVESTORS_WRAP
                }
            };

        function toggleTab(type) {
            var rentTab = moduleEl.querySelector('[data-type="rent"]');
            var buyTab = moduleEl.querySelector('[data-type="buy"]');
            var agentTab = moduleEl.querySelector('[data-type="agent"]');
            var residentialTab = moduleEl.querySelector('[data-type="residential"]');
            var commercialTab = moduleEl.querySelector('[data-type="commercial"]');
            var rentSelector = $(rentTab);
            var buySelector = $(buyTab);
            var agentSelector = $(agentTab);
            var residentialSelector = $(residentialTab);
            var commercialSelector = $(commercialTab);
            var listingType = utilService.getPageData('listingType');
            var isCommercial = utilService.getPageData('isCommercial');
            var broadcastEvent = 'pageDataListingTypeChange';
            utilService.setPageData('agentSearchEnabled', "No");
            if (type == "buy") {
                rentTab.classList.remove("selected");
                agentTab.classList.remove("selected");
                buySelector.addClass("selected");
                listingType = isCommercial ? 'commercialBuy' : 'buy';
                utilService.setPageData('listingType', listingType);
            } else if(type=="rent"){
                buyTab.classList.remove("selected");
                agentTab.classList.remove("selected");
                rentSelector.addClass("selected");
                listingType = isCommercial ? 'commercialLease' : 'rent';
                utilService.setPageData('listingType', listingType);
            } else if(type=='agent') {
                rentTab.classList.remove("selected");
                buyTab.classList.remove("selected");
                agentSelector.addClass("selected")
                listingType='agent';
                broadcastEvent='listingTypeChange';
                utilService.setPageData('agentSearchEnabled', "Yes");
            } else if(type=='residential') {
                commercialTab.classList.remove("selected");
                residentialSelector.addClass("selected");
                rentSelector.html('<span class="triangle">Rent</span>');
                listingType = (listingType == 'commercialBuy') ? 'buy' : ((listingType == 'commercialLease') ? 'rent' : listingType);
                utilService.setPageData('isCommercial', false);
                utilService.setPageData('listingType', listingType);
                context.broadcast('businessTypeChanged');
            } else {
                residentialTab.classList.remove("selected");
                commercialSelector.addClass("selected");
                rentSelector.html('<span class="triangle">Lease</span>');
                listingType = (listingType == 'buy') ? 'commercialBuy' : ((listingType == 'rent') ? 'commercialLease' : listingType);
                utilService.setPageData('isCommercial', true);
                utilService.setPageData('listingType', listingType);
                context.broadcast('businessTypeChanged');
            }
            context.broadcast(broadcastEvent, { listingType: listingType, id: 'homePageSearchBox', 'config': { 'name': 'searchTypeAhead' } });
        }


        function sendSms(element) {
            var SMSdata,
                userContact = moduleEl.querySelector('.js-sms-contact').value,
                smsSuccessMsg = $(".js-download-app-sms").find('[data-type="smsSuccess"]');
            SMSdata = {
                phone: userContact,
                smsString: SMS_TEMPLATE_STRING
            };
            element.value = "Sending....";
            element.removeAttribute("data-type");
            apiService.post(smsUrl, SMSdata).then(() => {
                element.value = "Resend sms";
                smsSuccessMsg.removeClass('hidden');
                element.setAttribute("data-type", "sendSms");
            }, () => {
                element.value = "Get link in sms";
                element.setAttribute("data-type", "sendSms");
            });
        }

        function setUrlBuyRentTab() {
            let listingType = utilService.getPageData('listingType');
            toggleTab(listingType);
        }

        function changeCity(cityId){
            context.broadcast('updateSingleDropdownByValue', {
                id: 'cityList',
                value: cityId
            })
        }
        function loadPastActivityModules(){
            //show popular localities only if shortlist or recentviewed are not available
            let displayPopularLocalities = true, displayFav = false, displayRecentView=false;
            shortListListingIds= shortlistService.getAllShortlist('listing',3)
                .map((shortlist)=>{
                    return shortlist.id
                });
            if(shortListListingIds && shortListListingIds.length>0){
                displayPopularLocalities = false;
                displayFav = true;
            }
            let recentVisitListingIds = [], recentVisitProjectIds=[];
            viewStatusService.recentlyViewed({
                    "numberOfItems": 3
                }).map((recentListing) => {
                    if(recentListing.type=='project'){
                        recentVisitProjectIds.push(recentListing.id)
                    } else {
                        recentVisitListingIds.push(recentListing.id)
                    }
                });
            if((recentVisitListingIds && recentVisitListingIds.length>0) ||
                (recentVisitProjectIds && recentVisitProjectIds.length>0)) {
                displayPopularLocalities = false;
                displayRecentView=true;
            }
            commonService.bindOnModuleLoad('listingSnapshot',(moduleId)=>{
                let listingIds, projectIds;
                switch(moduleId){
                    case FAVOURITE_LINK:
                        listingIds = shortListListingIds
                    break;
                    case RECENT_ACTIVITY_LINK:
                        listingIds = recentVisitListingIds;
                        projectIds = recentVisitProjectIds;
                    break;
                }
                context.broadcast('loadListingSnapshots',
                {
                    listingIds: listingIds,
                    projectIds: projectIds,
                    moduleId:moduleId
                });
            });
            if(displayPopularLocalities){
                defaultService.getCityByLocation().then(function(resp) {
                    let cityId = resp.id||defaultCityId;
                    commonService.bindOnModuleLoad('singleSelectDropdown',()=>{
                        trackCityClick = false;
                        changeCity(cityId);
                    });
                    commonService.bindOnModuleLoad('popularLocalities',()=>{
                        context.broadcast('loadPopularLocalities', {
                            cityId: cityId
                        });
                    });
                    $moduleEl.find('.js-popularLocalities-wrap').removeClass('hide');

                }, function(err) {
                    trackCityClick = false;
                    changeCity(defaultCityId)
                });
            }  else {
                $moduleEl.find('.past-activity-container').removeClass('hide');
                if(!displayFav){
                    commonService.bindOnModuleLoad('tabs',()=>{
                        context.broadcast('switchTabByTargetId',{
                            targetId: 'rencentlyViewed-wrap'
                        })
                    })
                }

            }
        }

        function getStrongHtmlText(text){
            return `<strong>${text}</strong>`;
        }
        //based on last visited page updated text for continue of previous flow
        //pages like property, static, price trend etc are excluded as they don't have forward flow
        function updateLastVisitContext(){
            let lastVisitContext = utilService.getLastVisitedPageDetails();
            if(!lastVisitContext){
                return;
            }
            let {url, moduleName, pageLevel, displayAreaLabel,
                listingType, sellerName, localityName, projectName, cityName, builderName} = lastVisitContext;
            if(!moduleName || !url || !displayAreaLabel){
                return;
            }

            let userName = '',
                withFilters = false,
                searchType=listingType,
                contactedData = localStorageService.getItem('enquiry_info') || utilService.getCookie("user_enquiry") || utilService.getCookie("user_enquiry_info") || {},
                buyRent = listingType?listingType + ' ':'', lastVisitContextText='';
            try {
                contactedData = JSON.parse(contactedData) || {};
                if(typeof contactedData != "object"){ // backward compatibilty
                    contactedData = JSON.parse(contactedData) || {};
                }
            } catch(e){
                contactedData = {};
            }
            if(contactedData.name){
                userName = ' '+contactedData.name
            }
            displayAreaLabel = displayAreaLabel.replace(' - ',' ');
            moduleName = moduleName.toLowerCase();
            lastVisitContextText = (function (name){
                var strings = {
                    user: "Hi "+name,
                    welcome: "Welcome back",
                    trail: " continue your home search"
                };
                return "<strong>"+(name?strings.user:strings.welcome)+",</strong>"+strings.trail;
            })(userName);
            switch(moduleName){
                case 'project':
                case 'cityoverview':
                case 'localityoverview':
                break;
                case 'serp':
                    withFilters = true;
                    // displayAreaLabel=getStrongHtmlText(displayAreaLabel+' ?');
                    // if(sellerName){ //sellerSERP
                    //     if(buyRent){
                    //         buyRent = ' ' + getStrongHtmlText(buyRent+'ing');
                    //     }
                    //     lastVisitContextText = `Hey${userName}, interested in${buyRent} properties listed ${displayAreaLabel}`;
                    // } else {
                    //     if(buyRent){
                    //         buyRent = ' to ' + getStrongHtmlText(buyRent);
                    //     }
                    //     if(pageLevel=='city'){ //city SERP
                    //         lastVisitContextText = `Hey${userName}, looking for a property${buyRent} ${displayAreaLabel}`;
                    //     }  else { //project/locality serp
                    //         lastVisitContextText = `Hey${userName}, are you still looking for a property${buyRent} ${displayAreaLabel}`;
                    //     }
                    // }
                break;
                // case 'project':
                // case 'cityoverview':
                    // displayAreaLabel = getStrongHtmlText(displayAreaLabel.substr(3) + ' ?'); //remove 'in' from the text
                    // lastVisitContextText = `Hey${userName}, looking for more information on ${displayAreaLabel}`;
                // break;
                // case 'localityoverview':
                    // if(buyRent){
                    //     buyRent = ' to ' + getStrongHtmlText(buyRent);
                    // }
                    // displayAreaLabel=getStrongHtmlText(displayAreaLabel + ' ?');
                    // lastVisitContextText = `Hey${userName}, looking for a property${buyRent} ${displayAreaLabel}`;
                //break;
                case 'allbrokers':
                    // displayAreaLabel=getStrongHtmlText(displayAreaLabel+ ' ?');
                    // lastVisitContextText = `Hey${userName}, looking for a property agent ${displayAreaLabel}`;
                    searchType='agent'
                break;
            }
            $moduleEl.find('.js-repeat-user-title').html(lastVisitContextText);
            $moduleEl.find('.js-title').addClass('hide');
            if(searchType){
                toggleTab(searchType)
            }
            commonService.bindOnModuleLoad('searchFilter',()=>{
                context.broadcast('pageDataListingTypeChange', { listingType: searchType, id: 'homePageSearchBox', 'config': { 'name': 'searchTypeAhead' } });
            });

        }

        function _smoothScroll(target, delta=0){
            let offset = 0;
            if(target){
                offset = $(target).offset().top;
            }
            $('html, body').animate({
                scrollTop: offset + delta
            }, 1000);
        }
        function _scrollFunction(){
            context.broadcast('homePageScrolled');
            let scrollTop = $(window).scrollTop();

            if(utilService.checkInViewPort(SEARCH_WRAP)){
                $moveToSearch.addClass('hide');
            } else {
                $moveToSearch.removeClass('hide');
            }
            for(let i in SECTION_VISIBLE_EVENTS){
                let item = SECTION_VISIBLE_EVENTS[i];
                if(!item.eventFired && utilService.checkInViewPort(item.wrap)){
                    item.eventFired = true;
                    context.broadcast('homepageSectionVisible',{
                        label: item.label
                    })
                }
            }
        }
        function trackUserCity() {
            defaultService.getCityByLocation().then(function(resp) {
                let userCity, userIP;
                if(resp){
                    userCity = resp.userCity;
                    userIP = resp.userIP;
                }
                context.broadcast('trackHomePageUserCity',{
                    userCity,
                    userIP
                });
            }, function(err) {
                context.broadcast('trackHomePageUserCity')
            });
        }
        function init() {
            moduleEl = context.getElement();
            config=context.getConfig();
            $ = context.getGlobal('jQuery');
            $moduleEl = $(moduleEl);
            defaultCityId = config.defaultCityId||18 //Mumbai;
            setUrlBuyRentTab(); // set buy/rent based on url

            LoginService.isUserLoggedIn().then((response) => {
                response = (response && response.data) || {};
                context.broadcast('login', { response });
            }, () => {
                context.broadcast('notLoggedIn');
            });
            $moveToSearch = $moduleEl.find(MOVE_TO_SEARCH);
            window.addEventListener('scroll', _scrollFunction,{passive:true}); //jshint ignore:line
            commonService.bindOnLoadPromise().then(function() {
                context.broadcast('loadModule', [{
                    name: 'searchFilter'
                },{
                    name: 'singleSelectDropdown'
                },{
                    name: 'popularLocalities'
                },{
                    name: 'recentDeals'
                },{
                    name: 'listingSnapshot'
                }
                ]);

                $moduleEl.find('[data-onloadimage]').addClass("onloadimage").removeAttr('data-onloadimage');
                commonService.addImageObserver('.js-blogslider', 'data-imagesrc');
                updateLastVisitContext();
                loadPastActivityModules();
            });
            trackUserCity();
        }

        function onclick(event, clickElement, elementType) {
            //let pageType = utilService.getTrackPageCategory();
            let dataset = $(clickElement).data();
            switch (elementType) {
                case 'buy':
                case 'rent':
                    context.broadcast('trackHomeCategoryOptionClick', { listingType: elementType });
                    toggleTab(elementType);
                    break;
                case 'agent':
                    context.broadcast('trackHomeCategoryOptionClick', { listingType: elementType });
                    toggleTab(elementType);
                    break;
                case 'residential':
                case 'commercial':
                    toggleTab(elementType);
                    break;
                case 'sendSms':
                    sendSms(clickElement);
                    context.broadcast('trackGetAppClickHome', {
                        phone: $('.js-sms-contact').val()
                    });
                    break;
                case 'blog-logo':
                    context.broadcast('blogLogoClick');
                    break;
                case 'homePostName':
                    dataset = $(clickElement).closest('[data-type="list-element"]').find('[data-type="homePostUrl"]').attr('itemid');
                    context.broadcast('homePostClick', dataset);
                    event.stopPropagation();
                    break;
                case 'scrollDown':
                    _smoothScroll(RECENT_DEAL_WRAP,0);
                break;
                case 'move-up-to-search':
                    _smoothScroll(null,250);

                    //set focus on typeahead
                    context.broadcast('typeaheadSetFocus',{id:'homePageSearchBox'});

                    //track
                    let dataset = clickElement.dataset, label = ''
                    if(dataset && dataset.source){
                        label = dataset.source;
                    }
                    context.broadcast('trackMoveToSearchClicked',{
                        label
                    });
                break;
                case 'sell-property':
                    context.broadcast('sellPropertyClick');
                    loginService.openLoginPopup();
                break;
                case 'reset-filters':
                    context.broadcast('reset-filters-clicked');
                break;
            }
        }


        function oninput(event, element, elementType) {
            switch (elementType) {
                case 'send-sms-contact':
                    let phone = element.value,
                        sendSmsButton = queryData(moduleEl, 'type', 'sendSms'),
                        smsErrorMsg = $(queryData(moduleEl, 'type', 'smsError'));
                    if (phone.search(/[^\d]/g) > -1) {
                        phone = phone.replace(/[^\d]/g, '');
                        element.value = phone;
                    }
                    if (phone.length == 10) {
                        if (utilService.validatePhone(phone, "india")) {
                            sendSmsButton.removeAttribute("disabled");
                            smsErrorMsg.addClass('hidden');
                        } else {
                            context.broadcast('trackGetAppErrorHome', {
                                phone: $('.js-sms-contact').val(),
                                label: 'incorrect number provided'
                            });
                            smsErrorMsg.removeClass('hidden');
                        }
                    } else {
                        if (!sendSmsButton.hasAttribute("disabled")) {
                            sendSmsButton.setAttribute("disabled", "disabled");
                        }
                        if (!smsErrorMsg.hasClass('hidden')) {
                            smsErrorMsg.addClass('hidden');
                        }
                    }
                    break;

            }
        }

        function onmessage(name, data) {
            switch (name) {
                case 'makaaniq-post_slider_element_clicked':
                    if (data.data) {
                        context.broadcast('homePostClick', data.data);
                        window.open(data.data, '_blank');
                    }
                    break;

                case 'singleSelectDropdownChanged':
                    if(data.id=='cityList'){
                        context.broadcast('loadPopularLocalities', {
                            cityId: data.value
                        });
                        //this check is to avoid default tracking getting fired on page load
                        //tracking would be enabled after first time.
                        if(trackCityClick){
                            context.broadcast('trackPopularLocalityDropdown',{
                                label: data.label
                            });
                        } else {
                            trackCityClick = true;
                        }
                    }
                    break;
                case 'singleSelectDropdownHeadClicked':
                    if(data.id=='cityList'){
                        context.broadcast('trackPopularLocalityDropdown',{
                            label: 'city'
                        });
                    }
                break;
                case 'popularLocalityClicked':
                    context.broadcast('trackPopularLocalityClicked',{
                        label: data.index
                    });
                break;
                case 'listingSnapshotClicked':
                    context.broadcast('trackListingSnapshotsClick',{
                        label: data.id
                    });
                break;
                case 'tabs_click_track':
                    if(data.moduleEl && data.moduleEl.id=='pastActivityTabs' && data.element){
                        context.broadcast('trackRecentActivityTabs',{
                            label: data.element.innerText
                        });
                    }
                break;
            }
        }

        function destroy() {
            window.removeEventListener('scroll', _scrollFunction,{passive:true});
            config = null;
            moduleEl = null;
            $moduleEl = null;
        }
        return {
            messages: [
            'makaaniq-post_slider_element_clicked',
            'singleSelectDropdownChanged',
            'singleSelectDropdownHeadClicked',
            'popularLocalityClicked',
            'listingSnapshotClicked',
            'tabs_click_track'
            ],
            behaviors: ['homeTracking'],
            init,
            onclick,
            destroy,
            onmessage,
            oninput
        };
    });
});
