"use strict";
define([
    'services/commonService',
    'services/trackingService',
    'behaviors/semProjectTracking'
], () => {
    Box.Application.addModule("semProject", (context) => {

        //const CommonService = context.getService('CommonService'),
          //  logger = context.getService('Logger');
        var messages = [],
            behaviors = ['semProjectTracking'],
            query, moduleEl, testimonialCarousel, carouselSlidingTime = 5000,
            slidingAnimationTime = 1000;

        function init() {
            //initialization of all local variables
            query = Box.DOM.query;
            moduleEl = context.getElement();
            autoStartCarousel();
        }

        function destroy() {
            //clear all the binding and objects
        }

        function onmessage() {
            // bind custom messages/events
        }

        function onclick(event, element, elementType) {
            switch (elementType) {
                case "GET_CALL_BACK":
                case "MOBILE_GALLERY":
                case "VIEW_FULL_IMAGE":
                case "REQUEST_FLOOR_PLAN":
                case "GET_THE_BEST_DEAL":
                case "GET_DETAILED_INSIGHTS":
                case "GET_IN_TOUCH":
                    _openPopup('leadPopup', element);
                    context.broadcast('trackSemProjectLeadFormOpen',{
                        sourceModule: $(element).text()
                    });
                    break;
                case "Phone":
                    context.broadcast('trackSemProjectPhoneClick', { source: $(element).data('source') });
                    break;
                case "INDICATOR":
                    context.broadcast('trackSemProjectTestimonialClick');
                    clearInterval(testimonialCarousel);
                    slideTestimonial(element);
                    break;
                case "VIEW_FULL_GALLERY":
                    if (Box.Application.isStarted(Box.DOM.query(moduleEl, "#main_gallery"))) {
                        context.broadcast('start_gallery', {
                            "id": 'main_gallery'
                        });
                    } else {
                        context.broadcast('loadModule', {
                            "name": 'gallery',
                            "loadId": 'main_gallery'
                        });
                    }
                    break;
                case "ICON_DOWN_ROUND":
                    context.broadcast('trackSemProjectArrowClick');
                    $('html body').animate({
                        scrollTop: ($('.icon-down-round').offset().top)
                    }, slidingAnimationTime);
                    break;
            }
        }

        function _openPopup(id, element) {
            context.broadcast('popup:open', {
                id: id,
                options: {
                    config: {
                        modal: false
                    }
                },
                source: $(element).data('source')
            });
        }

        function slideTestimonial(element) {
            $('.indicator').each(function() {
                $('.indicator').removeClass("active-indicator");
            });
            $(element).addClass("active-indicator");
            $('.comment-list').animate({
                marginLeft: '-' + $(element).index() * 100 + '%'
            }, 0);
        }

        function autoStartCarousel() {
            var counter = 1;
            testimonialCarousel = setInterval(function() {
                $('.indicator').each(function() {
                    $('.indicator').removeClass("active-indicator");
                });
                $('.indicator').eq(counter).addClass("active-indicator");
                $('.comment-list').animate({
                    marginLeft: '-' + counter * 100 + '%'
                }, 0);
                counter = (counter + 1) % 4;
            }, carouselSlidingTime);
        }

        return {
            init,
            messages,
            behaviors,
            onmessage,
            onclick,
            destroy
        };
    });
});
