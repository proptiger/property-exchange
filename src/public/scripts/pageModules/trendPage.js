"use strict";
define([
    'common/sharedConfig',
    'services/commonService',
    'services/apiService',
    'behaviors/trendTracking'
], (sharedConfig, commonService) => {
    Box.Application.addModule("trendPage", (context) => {

        const CommonService = context.getService('CommonService'),
            ApiService = context.getService('ApiService');
           // logger = context.getService('Logger');
        const config = {
            selector: {
                "SEARCH_ELEMENT": "[data-search-element]"
            },
            classes: {
                HIDE: "hidden"
            }
        };
        var messages = ["popup:closed"],
            behaviors = ["trendTracking"],
            $moduleEl;

        function _search(element) {
            let val = $(element).val();
            $.each($(config.selector["SEARCH_ELEMENT"], $moduleEl), function() {
                let tr = $(this).parent('tr');
                if ($(this).text().toLowerCase().indexOf(val) == -1) {
                    tr.addClass(config.classes["HIDE"]);
                } else {
                    tr.removeClass(config.classes["HIDE"]);
                }
            });
        }

        function _getTrendApi(type, id, unitType, duration) {
            var url;
            switch (type) {
                case 'locality':
                    url = sharedConfig.apiHandlers.getLocalityPriceTrend({
                        localityId: id,
                        query: {
                            duration: duration,
                            category: 'buy',
                            unitType: unitType || ''
                        }
                    }).url;
                    break;
                case 'project':
                    url = sharedConfig.apiHandlers.getProjectPriceTrend({
                        projectId: id,
                        query: {
                            duration: duration,
                            category: 'buy',
                            unitType: unitType || ''
                        }
                    }).url;
                    break;
                case 'city':
                    url = sharedConfig.apiHandlers.getCityPriceTrend({
                        cityId: id,
                        query: {
                            duration: duration,
                            category: 'buy',
                            unitType: unitType || ''
                        }
                    }).url;
                    break;
            }
            return url;
        }

        function _generateChart(trendType, id, unitType, duration = 36) {
            let series = [],
                chartId = "trendChart";

            var url = _getTrendApi(trendType, id, unitType, duration);

            context.broadcast('loadModule', {
                name: 'chart'
            });
            commonService.bindOnModuleLoad('chart', () => {
                ApiService.get(url).then(function(response) {
                    if (response) {
                        var __city, i, j, result = [],
                            key, value,
                            __month, sortedMonth;
                        for (i = 0; i < Object.keys(response).length; i++) {
                            __city = Object.keys(response)[i];
                            var __subObj = response[__city];
                            sortedMonth = Object.keys(__subObj).sort();
                            result = [];
                            for (j = 0; j < sortedMonth.length; j++) {
                                key = sortedMonth[j];
                                __month = parseInt(key, 10);
                                value = Math.round(__subObj[key]);
                                if (value) {
                                    result.push([__month, value]);
                                }
                            }
                            if (result.length) {
                                series.push({
                                    name: __city,
                                    data: result
                                });
                            }
                        }
                    }
                    context.broadcast('chartDataLoaded', {
                        series: series,
                        id: chartId
                    });
                }, function() {
                    context.broadcast('chartDataLoaded', {
                        series: [],
                        id: chartId
                    });
                });
            });
        }

        function _openPopup(id) {
            context.broadcast('popup:open', {
                id: id,
                options: {
                    config: {
                        modal: true
                    }
                }
            });
        }

        function _clearChartData() {
            context.broadcast('chartDataDeleted',{});
        }

        function showPriceTrend(trendType, id, unitType) {
            _generateChart(trendType, id, unitType);
            _openPopup('trendPopup');
        }

        function init() {
            //initialization of all local variables
            $moduleEl = $(context.getElement());
        }

        function destroy() {
            //clear all the binding and objects
        }

        function onmessage(name) {
            // bind custom messages/events
            switch (name) {
                case "popup:closed":
                    _clearChartData('trendPopup');
                    break;
            }
        }

        function onkeyup(event, element, elementType) {
            switch (elementType) {
                case "search":
                    _search(element);
                    break;
            }
        }

        function onclick(event, element, elementType) {
            // bind custom messages/events
            let dataset;
            switch (elementType) {
                case "navigate_serp":
                    let url = $(element).data('url');
                    url && CommonService.ajaxify(url); //jshint ignore:line
                    context.broadcast('trackTrendTableClick', { source: $(element).data('source') });
                    break;
                case "open_locality_trend":
                    dataset = $(element).data();
                    showPriceTrend('locality', dataset.id, dataset.unittype);
                    context.broadcast('trackTrendTableClick', { source: $(element).data('source') });
                    break;
                case "open_project_trend":
                    dataset = $(element).data();
                    showPriceTrend('project', dataset.id);
                    context.broadcast('trackTrendTableClick', { source: $(element).data('source') });
                    break
                case "open_city_trend":
                    dataset = $(element).data();
                    showPriceTrend('city', dataset.id, dataset.unittype);
                    context.broadcast('trackTrendTableClick', { source: $(element).data('source') });
                    break
                case "pageDescription":
                    if (event.target && event.target.tagName === 'A') {
                        context.broadcast('trackTrendDescClick', { source: event.target.href });
                    }
                    break;
                case "toggleLink":
                case "tab":
                    context.broadcast('trackTrendTabClick', { source: $(element).data('source') });
                    break;
                case "table":
                    context.broadcast('trackTrendTableClick', { source: $(element).data('source') });
                    break;
            }
        }

        return {
            init,
            messages,
            behaviors,
            onmessage,
            onclick,
            onkeyup,
            destroy
        };
    });
});
