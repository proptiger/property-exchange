"use strict";
define([
    'common/sharedConfig',
    'services/apiService',
    'services/urlService',
], function(sharedConfig, apiService) {
    Box.Application.addModule('unsubscribeController', function(context) {

        const unsubscribeBtnType = 'unsubscribe',
            //homeUrl = '/',
            queryData = Box.DOM.queryData,
            disableClass = 'disable',
            hideClass = 'hide';

        let config, unsubscribeKey, unsubscribeUrl, h2Element, innerContentElement, homeBtn, unsubscribeBtn;

        function init() {
            const _moduleEl = context.getElement();
            config = context.getConfig() || {};
            unsubscribeKey = encodeURIComponent(config.unsubscribeKey);
            unsubscribeUrl = sharedConfig.apiHandlers.unsubscribeUrl({ unsubscribeKey }).url;
            h2Element = queryData(_moduleEl, 'type', 'heading');
            innerContentElement = queryData(_moduleEl, 'type', 'inner-content');
            homeBtn = queryData(_moduleEl, 'type', 'home');
            unsubscribeBtn = queryData(_moduleEl, 'type', 'unsubscribe');
        }

        function onclick(event, element, type) {
            switch (type) {
                case unsubscribeBtnType:
                    if (!$(element).hasClass(disableClass)) {
                        $(element).addClass(disableClass);
                        apiService.get(unsubscribeUrl).then(() => {
                                $(h2Element).text("it's not the same without you");
                                $(innerContentElement).text('you have been successfully unsubscribed from the requested makaan.com emails');
                                $(homeBtn).removeClass(hideClass);
                                $(unsubscribeBtn).addClass(hideClass);
                            }

                            , () => {
                                //handle error in unsubscribe
                                $(element).removeClass(disableClass);
                            });
                    }
            }
        }

        return {
            init,
            onclick
        };
    });
});
