/**
 * This is a Page Module used as a controller on share Coupon Page.
 * It handles the click button on the main landing page.
 * JIRA : MAKAAN-4194.
 */
"use strict";
define(['services/filterConfigService',
        'services/defaultService',
        'services/commonService',
        'behaviors/shareCouponPageTracking',
        'services/urlService',
    ], function(filterConfigService,defaultService,CommonService,shareCouponPageTracking,urlService){
    const ModuleName = "shareCouponPage";
    Box.Application.addModule(ModuleName, (context) => {
        let moduleEl, moduleConfig, $, $moduleEl,urlData;

        const selector = {
            "LOADER":'.js-shareCoupon-loading',
            "NO_COUPON_GENERATED":'.js-noCoupon-generated',
            "COUPON_GENERATED":'.js-coupon-generated',
            "UNDERPROCESSS_COUPON_GENERATED":'.js-underProcess-coupon-generated',
            "COUPON_CODE":'.js-couponCode',
            "UNDERPROCESS_COUPON_CODE":'.js-underProcess-couponCode',
            "UNLOCK_COUPON_HC":'.js-unlock-coupon-hc',
            "HAPPY_COUSTOMER":'.js-happy-customer'
        };
         //handle showing or hiding processing button
        function _showProcessing(show = true) {
            let actionButton = $moduleEl.find("[data-normal]"),
                processingButton = $moduleEl.find("[data-processing]");
            if (!show) {
                processingButton.addClass("hide");
                actionButton.removeClass("hide");
                $moduleEl.find('.js-knowMore').removeClass('hide');
            } else {
                processingButton.removeClass("hide");
                actionButton.addClass("hide");
                $moduleEl.find('.js-knowMore').addClass('hide');
            }
        }
        //hide or show Loader
        function _showPopupLoader(show = false){
            if(show){
                $(selector['LOADER']).removeClass('hide');    
            } else {
                $(selector['LOADER']).addClass('hide');
            }
        }
        //clear error message from screen after certain time interval
        function clearMessage(tmpElement) {
             setTimeout(() => {
                 tmpElement.text('');
             }, 4000);
         }
         // function for redirection url 
         function getRedirectionUrl(data){
            let urlparams = {};
            urlparams.cityId = 21;
            urlparams.cityName = 'Pune';
            urlparams.listingType = 'rent';
            urlparams.postedBy = 'selectAgent';
            urlparams.sellerId = '';
            urlparams.builderId = '';
            urlparams.projectId = '';
            let url =  filterConfigService.getUrlWithUpdatedParams({overrideParams: urlparams});
            window.location.href = url;
            _showProcessing(false);
        }

        function handleCouponRedemptionView(data={}){
            if(data.couponGenerated){
                if(data.underProcess){
                    $moduleEl.find(selector.UNDERPROCESS_COUPON_CODE)[0].innerHTML = data.couponDisplayCode;
                    $moduleEl.find(selector.COUPON_GENERATED).addClass('hide');
                    $moduleEl.find(selector.UNDERPROCESSS_COUPON_GENERATED).removeClass('hide');
                    $moduleEl.find(selector.UNLOCK_COUPON_HC).addClass('hide');
                }else{
                    $moduleEl.find(selector.COUPON_CODE)[0].innerHTML=data.couponDisplayCode;
                    $moduleEl.find(selector.UNDERPROCESSS_COUPON_GENERATED).addClass('hide');
                    $moduleEl.find(selector.COUPON_GENERATED).removeClass('hide');
                    $moduleEl.find(selector.UNLOCK_COUPON_HC).removeClass('hide');  
                }
                $moduleEl.find(selector.NO_COUPON_GENERATED).addClass('hide');
                $moduleEl.find(selector.HAPPY_COUSTOMER).addClass('with-coupon');
            }else{
                $moduleEl.find(selector.COUPON_GENERATED).addClass('hide');
                $moduleEl.find(selector.UNDERPROCESS_COUPON_GENERATED).addClass('hide');
                $moduleEl.find(selector.NO_COUPON_GENERATED).removeClass('hide');
                $moduleEl.find(selector.HAPPY_COUSTOMER).removeClass('with-coupon');
                $moduleEl.find(selector.UNLOCK_COUPON_HC).removeClass('hide');
            }     
             _showPopupLoader(false);
        }

        return {
            messages:['HideProcessing','hideLoader'],
            behaviors: ['shareCouponPageTracking'],
            init: function(){
                moduleEl = context.getElement();
                moduleConfig = context.getConfig();
                $ = context.getGlobal('jQuery');
                $moduleEl = $(moduleEl);
                CommonService.startAllModules(moduleEl);
                urlData = urlService.getAllUrlParam();
                let source = urlData.uid ? 'Link':'';
                context.broadcast('trackShareCouponPageSeen',{source,value:urlData.uid||''});

                CommonService.bindOnModuleLoad('shareCoupon', () => {
                    context.broadcast('loadCouponRedemptionView', {});
                });
            },
            onclick:function(event, element, elementType){
                switch(elementType){
                    case 'homeFinalized':
                        _showProcessing(true);
                        let dataset  = element.dataset;
                        context.broadcast('trackVerifyCouponClick',{source: dataset && dataset.source || '',value:urlData.uid||'' });
                        context.broadcast('HomeFinalized'); // broadcast message to shareCoupon module to verify user and start the flow
                        break;
                    case 'redirectToSerp':
                        _showProcessing(true);
                        context.broadcast('trackSearchPropertyClick',{source: 'MAIN',value:urlData.uid||'' });
                        getRedirectionUrl({listingCategory:'rent'}); // calling function for redirecting to serp page 
                        break;
                    case 'share-coupon-whatsapp':
                        context.broadcast('sharingIconClicked',{medium: 'Whatsapp'});
                        break;
                    case 'share-coupon-sms':
                        context.broadcast('sharingIconClicked',{medium: 'Sms'});
                        break;
                    case 'share-coupon-gmail':
                        context.broadcast('sharingIconClicked',{medium: 'Gmail'});
                        break;

                }

            },
            onmessage: function(name, data){
                switch(name){
                    case 'HideProcessing':
                        _showProcessing(false,data);
                        if(data && data.msg){
                            var tmpElement = $moduleEl.find('.js-errorMsg');
                            tmpElement.text(data.msg);
                            clearMessage(tmpElement);
                        }
                        break;
                    case 'hideLoader':
                        handleCouponRedemptionView(data);
                        break;
                }
            },
            destroy: function(){
                moduleEl = $ = $moduleEl = moduleConfig = null;
            }
        }
    });
});
