"use strict";
define(['services/loggerService',
    'services/apiService','services/loginService',
    'services/utils'
],function(){
    Box.Application.addModule('sellerHomePage',function(context){
        var config, moduleEl, $, $moduleEl, $window;
        var utils, commonService, loginService;

        var startModule = function(module){
            if(!module){
                return;
            }
            var data = {
                messageData:[{
                    name:module,
                    loadId:module
                }],
                'message':'loadModule'
            };
            commonService.initializeCallbacks({data:data});
        };
        var stopModule = function(module){
            if(!module){
                return;
            }
            Box.Application.stop(document.getElementById(module));
        };
        var setDomainCookie = function(){
            var baseDomain = (document.domain.match(/([^.]+)\.\w{2,3}(?:\.\w{2})?$/) || [])[1],
            expireAfter = new Date();
            baseDomain = (baseDomain) ? '.'+baseDomain+'.com' : document.domain;
            expireAfter.setDate(expireAfter.getDate() + 7);
            utils.setcookie('websiteversion','new',baseDomain,expireAfter);
        }
        var preLogout = function(){
            loginService.logoutUser();
        }
        /*
            Function to toggle video play/pause
        */
        var toggleVideo = function(){
            if (navigator.userAgent.toLowerCase().indexOf('firefox') == -1) {
                var homeVideo=document.getElementById('homeVideo');
                if (homeVideo.paused){
                  homeVideo.play();
                }else{
                  homeVideo.pause();
                }
            }
        }

        return {
                init: function() {
                    config = context.getConfig();
                    moduleEl = context.getElement();
                    $ = context.getGlobal('jQuery');
                    $moduleEl = $(moduleEl);
                    $window = $(window);
                    commonService = context.getService('CommonService');
                    loginService = context.getService('LoginService');
                    context.broadcast('moduleLoaded', {
                        name: 'sellerHomePage'
                    });

                    utils = context.getService('Utils');
                    $moduleEl.find('[data-type=start-login]').removeClass("hide");
                    $moduleEl.find('[data-type=register-seller]').removeClass("hide");
                    startModule('get-in-touch');
                    setDomainCookie();
                },
                destroy: function(){
                    moduleEl = null;
                    $ = null;
                    $moduleEl = null;
                    loginService=null;
                    $window.off('scroll');
                },
                onclick: function(event, element, elementType){
                    switch (elementType) {
                        case 'register-seller':
                            let dataset=$(element).data();
                            loginService.setUserProp('sellerMicrositeUrl', dataset.href);
                            loginService.openLoginPopup('checkRegistered');
                            break;
                        case 'toggle-video':
                            toggleVideo();
                            break;
                        default:
                            break;
                    }
                }
            };
    });
});
