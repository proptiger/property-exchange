"use strict";
define([
    'services/commonService',
    'scripts/vendor/staticPages/bootstrap'
], () => {
    Box.Application.addModule("termsNconditions", () => {

        var messages = [],
            behaviors = [];

        function init() {
            var hash = window.location.hash;
            hash = hash || '#Contract'; //to open contract tab by default
            var $el = $(`[href="${hash}"]`);
            let clickElemId = $el.data("topLevel");
            if ($el.length) {
                let $clickElem = clickElemId ? $(`[href="${clickElemId}"]`) : $el;
                $clickElem.click();
                $('body').animate({
                    scrollTop: $el.offset().top
                }, 'slow');
            }
        }

        function destroy() {
            //clear all the binding and objects
        }

        function onmessage() {
            // bind custom messages/events
        }

        function onclick(event, element, elementType) {
            switch (elementType) {}
            // bind custom messages/events
        }

        return {
            init,
            messages,
            behaviors,
            onmessage,
            onclick,
            destroy
        };
    });
});
