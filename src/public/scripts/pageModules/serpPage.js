/**
 * Description: wrapper module for SERP page
 * @author: [Aditya Jha]
 * Date: Nov 30, 2015
 **/
"use strict";
define([
    'services/filterConfigService',
    'services/urlService',
    'services/localStorageService',
    'services/utils',
    'common/sharedConfig',
    'services/experimentsService',
    'services/commonService',
    'services/coachMarkService',
    'services/apiService',
    'behaviors/serpTracking',
    'behaviors/infiniteScroll'
], (filterConfigService, urlService, localStorageService, UtilService, sharedConfig, experimentsService) => {
    Box.Application.addModule('serpPage', (context) => {
        const COUPON_COOKIE_NAME = "SELECT_COUPON_GENERATED",makaanSelectCity=sharedConfig.makaanSelectCities;
        const $ = context.getGlobal('jQuery'),
            apiService = context.getService('ApiService'),
            commonService = context.getService('CommonService'),
            coachMarkService = context.getService('coachMarkService'),
            SIMILAR_CARD_SELECTOR = '[data-similar-card]',
            LISTING_WRAPPER_SELECTOR = '[data-listing-wrapper]',
            TOP_RANK_HOVER_ELEMENT = "[data-type='sidebar-toprank']",
            CONTENT_WRAPPER = '[search-result-wrap]',
            topSellerFallbackPlaceholder = '[top-sellers-fallback-placeholder]',
            NUMBER_OF_ADS = 3,
            MAKAAN_SELECT_URL = '/makaan-select',
            DEFICIT_LISTINGS_IN_VIEW_MESSAGE = 'trackeDeficitSellerListingInView',
            PRIVILEGE_SELLER_LISTING = '.js-privilege-seller-listings',
            PRIVILEGE_LISTINGS_IN_VIEW_MESSAGE = 'trackPrivilegeListingInView';
        
        var sidebarCountPreviousApiCall,
            previousApiCall = null,
            serpPageElement,
            serpScrollTrackingEventSent = false,
            hoverStartTimeRHS,
            currentSection = null,
            adsRequestedMap = {},
            messages = ['serpListingMouseentered', 'serpListingMouseLeft', 'searchParamsChanged', 'pageChanged', 'showon-map', 'slider_clicked', 'makaaniq-post_slider_element_clicked', 'moreListingsLoaded', 'listingsUpdated', 'morelistingsNotLoaded', "popup:closed", "serpPageScrolled", "elementClicked", DEFICIT_LISTINGS_IN_VIEW_MESSAGE, PRIVILEGE_LISTINGS_IN_VIEW_MESSAGE],
            behaviors = ['serpTracking', 'infiniteScroll'],
            moduleEl,
            $moduleEl,config,
            loadMoreFlag = true,
            trackedSelectListings=[],
            selectBannerTracked = false , selectReminderTracked = false,trackedDeficitSellerListing=[];

        function _openPopup(id, closeOnEsc) {
            context.broadcast('popup:open', {
                id: id,
                options: {
                    config: {
                        modal: closeOnEsc || false
                    }
                }
            });
        }

        function _loadImages(){
            commonService.bindOnLoadPromise().then(function() {
                $(moduleEl).find('[data-onloadimage]').addClass("onloadimage").removeAttr('data-onloadimage');
            });
        }
        function abExperimenthandling(){
            experimentsService.switchElements('makaan_serp_noresult');
            if(experimentsService.getExperimentVariation('makaan_deficit_rent')=="show"){
                $moduleEl.find('.js-deficit-listing-scroll').removeClass('hide');
            }
            if (UtilService.isMobileRequest()) {
                experimentsService.experimentElementsClass(document.getElementsByClassName('list-mainarea'), 'makaan_serp_card','new','single-line-view');
            }
        }

        function init() {
            config = context.getConfig();
            serpPageElement = context.getElement();
            moduleEl = context.getElement();
            $moduleEl = $(moduleEl);
            abExperimenthandling();
            window.addEventListener('scroll', _scrollFunction,{passive:true});
            bindMouseEvents(TOP_RANK_HOVER_ELEMENT);

            commonService.bindOnLoadPromise().then(function() {
                _updateGoogleAd();
            });

            commonService.loadGoogleAds();
            UtilService.setLastVisitedPageDetails(window.pageData); //for saving context of repeat users
            UtilService.updatePushNotificationTags('VISITED_SERP');

            if (urlService.getUrlParam("setAlert") && urlService.getUrlParam("email")) {
                commonService.bindOnModuleLoad('saveSearch', function() {
                    _openPopup('saveSearchPopup');
                    context.broadcast('directSetAlertWelcome', { trackId: 'SerpPageSetAlert' });
                });
            }

            _loadImages();

            context.broadcast('moduleLoaded', {
                'name': 'serpPage',
                'id': moduleEl.id
            });

            let urlData = urlService.getAllUrlParam();
            if (urlData.showPopup && urlData.showPopup == 'mProfile' && urlData.uid) {
                commonService.bindOnModuleLoad('userProfiling', function () {
                    context.broadcast("mProfilePopupOpen", {
                        showPopup: urlData.showPopup,
                        userId: urlData.uid,
                        source: urlData.source
                    });
                });
            }
            UtilService.setPageData('paidSellerUserIds',urlData.paidSellerUserIds||'');

            checkDeficitSellerListingsInView();
            checkPrivilegeListingInView();
            showHideSelectBanner();
            trackSelectListings();
        }

        function checkPrivilegeListingInView(){
            let elements = moduleEl.querySelectorAll(PRIVILEGE_SELLER_LISTING);
            if (elements.length) {
                UtilService.trackElementInView(elements, PRIVILEGE_SELLER_LISTING, '', PRIVILEGE_LISTINGS_IN_VIEW_MESSAGE);
            }
        }

        function showHideSelectBanner(){
            let cityId = UtilService.getPageData('cityId'),listingType = UtilService.getPageData('listingType'),
                couponData = localStorageService.getItem(COUPON_COOKIE_NAME);
            if(makaanSelectCity.indexOf(cityId) > -1 && listingType=="rent"){
                if(!couponData){
                    $moduleEl.find('.js-select-banner').removeClass('hide');
                    context.broadcast('selectBannerSeen');
                }else if(UtilService.isStorageExceedsTime(couponData,sharedConfig.couponShareEligibleTime)){
                    $moduleEl.find('.js-select-reminder').removeClass('hide');
                    context.broadcast('selectReminderSeen');
                }
                showSelectCoachMark();
            }
        }

        function showSelectCoachMark(){
            if (UtilService.isMobileRequest()) {
                coachMarkService.initBannerCoachMark();
            }
        }

        function onclick(event, element, elementType) {
            let url,label,source='';
            context.broadcast('userClicked', {
                element: element,
                elementType: elementType
            });
            let clickElementData = $(element).data(),
                pageType = UtilService.getTrackPageCategory(),
                pageCategory = 'Buyer_' + pageType,
                $element = $(element);
            switch (elementType) {
                case 'back-btn':
                case 'next-btn':
                case 'view-listings':
                    if (clickElementData.trackLabel && clickElementData.trackAction) {
                        UtilService.trackEvent(clickElementData.trackAction, pageCategory, clickElementData.trackLabel);
                    }
                    break;
                case 'saveSearch':
                    commonService.bindOnModuleLoad('saveSearch', function() {
                        context.broadcast('openSaveSearch', { trackId: 'SerpPageSetAlert' });
                        _openPopup('saveSearchPopup');
                    });
                    break;
                case 'showon-map':
                    context.broadcast('trackMapCardClick');
                    context.broadcast('showMapView');
                    break;
                case 'buy-rent-toggle':
                    let switchto = $(element).data('switchto');
                    context.broadcast('trackBuyRentCardClick', { listingType: switchto });
                    context.broadcast('pageDataListingTypeChange', {
                        listingType: switchto,
                        id: 'serpSideCard'
                    });
                    urlService.ajaxyUrlChange(filterConfigService.getUrlWithUpdatedParams({
                        skipFilterData: true,
                        overrideParams: {
                            listingType: switchto
                        }
                    }), true);
                    break;
                case 'builder-ongoing-projects':
                    url = filterConfigService.getUrlWithUpdatedParams({
                        overrideParams: {
                            possession: 'any',
                            ageOfProperty: null
                        }
                    });
                    urlService.ajaxyUrlChange(url);
                    break;
                case 'builder-completed-projects':
                    url = filterConfigService.getUrlWithUpdatedParams({
                        overrideParams: {
                            ageOfProperty: 'any',
                            possession: null
                        }
                    });
                    urlService.ajaxyUrlChange(url);
                    break;
                case 'open-pyr':
                    if (!commonService.checkModuleLoaded('pyr', 'serpPyrForm')) {
                        context.broadcast('trackSerpPyrCardFail');
                    } else {
                        context.broadcast('trackPyrCardClick');
                    }

                    let localityId = UtilService.getPageData('localityId'),
                        localityName = UtilService.getPageData('localityName');
                    let data = {
                        id: 'serpPyrForm',
                        pyrData: {
                            stage: "step1"
                        },
                        trackData: {
                            id: 'SerpPyrRhs'
                        }
                    };
                    if (localityId && localityName) {
                        localityId = typeof localityId != 'string' ? [localityId] : localityId.split(',');
                        localityName = typeof localityName != 'string' ? [localityName] : localityName.split(',');
                        
                        data.pyrData.localityIds = [];
                        localityId.forEach((item, index) => {
                            data.pyrData.localityIds.push({
                                id: item,
                                name: localityName[index],
                                type: 'locality'
                            });
                        })
                    }
                    context.broadcast('openPyr', data);
                    _openPopup('serpPyrPopup', true);
                    break;
                case 'open-jarvis':
                    context.broadcast('trackJarvisCardClick');
                    let jarvisChatElem = document.querySelector("#makaan-man");
                    if (jarvisChatElem) {
                        jarvisChatElem.click();
                    }
                    break;
                case 'download-app':
                    context.broadcast('trackDownloadCardClick');
                    break;
                case 'dashboard-link':
                    context.broadcast('trackJourneyCardClick');
                    break;
                case 'sidebar-toprank':
                     url = $element.data('listingurl');
                    if(url){
                        url += '?'+filterConfigService.getUrlWithUpdatedParams({skipHref:true,skipPageData:false,skipFilterData:false});
                        urlService.ajaxyUrlChange(url);
                    }
                    context.broadcast('trackSimilarCardClick', {
                        id: $element.data('id'),
                        type: $element.data('similar-type'),
                        linkName: $element.data('link-name'),
                        linkType: $element.data('link-type')
                    });
                    event.preventDefault();
                    break;
                case 'stop-anchor':
                    event.preventDefault();
                    break;
                case 'postRead':
                    let dataset = $(element).find('a').attr('href');
                    context.broadcast('serpPostClick', dataset);
                    event.stopPropagation();
                    break;
                case 'see-more':
                    context.broadcast('seeMoreClick');
                    break;
                case 'selectKnowMore':
                    context.broadcast('hideSelectCoachMark', {});
                    source = element.classList.contains('js-repeat-selectPromo') ? 'Serp_Repeat':'Serp';
                    context.broadcast('trackSelectKnowMore',{source});
                    let popupid = UtilService.isMobileRequest() ? 'msKnowMorePopup':'msKnowMoreDeskPopup'
                    _openPopup(popupid,true);
                    break;
                case 'reminderUnlock':
                    source = element.classList.contains('js-repeat-selectReminder') ? 'Serp_Repeat':'Serp';
                    context.broadcast('trackReminderUnlock',{source});
                    window.location.href = MAKAAN_SELECT_URL;
                    break;
                case 'privilege-seller-card':
                    let cardData = $element.data();
                        cardData.cityId = UtilService.getPageData('cityId');
                        cardData.pageType = UtilService.getTrackPageCategory();
                        cardData.listingType = UtilService.getPageData("listingType");
                    context.broadcast("trackPrivilegeCardClick", cardData);
                    window.location.href = cardData.ref;
                    break;
            }
        }

        function bindMouseEvents(element) {
            $(element).on('mouseenter', onmouseenter).on('mouseleave', onmouseleave);
        }

        function unbindMouseEvents(element) {
            $(element).off('mouseenter', onmouseenter).off('mouseleave', onmouseleave);
        }

        function onmouseenter() {
            hoverStartTimeRHS = new Date().valueOf();
        }

        function onmouseleave() {
            let $element = $(this);
            context.broadcast('trackSimilarCardHover', {
                id: $element.data('id'),
                type: $element.data('similar-type'),
                hoverTime: new Date().valueOf() - hoverStartTimeRHS
            });
        }

        function onmessage(name, data) {
            if (name == 'serpListingMouseentered') {
                context.broadcast('triggerListingMouseenter', data);
            } else if (name == 'serpListingMouseLeft') {
                context.broadcast('triggerListingMouseleave', data);
            } else if (name === 'searchParamsChanged') {
                _searchParamsChanged();
                serpScrollTrackingEventSent = false;
            } else if (name === 'pageChanged') {
                $('body').animate({
                    scrollTop: 0
                }, 0);
            } else if (name === 'makaaniq-post_slider_element_clicked' && data.data) {
                context.broadcast('serpPostClick', data.data);
                window.open(data.data, '_blank');
            } else if (name === "slider_clicked" && data.element.id == 'makaaniq-post') {
                if (data.event == 'Right') {
                    context.broadcast('serpBlogNextbtnClick');
                } else if (data.event == 'Left') {
                    context.broadcast('serpBlogBackbtnClick');
                }
            } else if (name === 'moreListingsLoaded' || name === 'listingsUpdated' || name === 'morelistingsNotLoaded') {
                loadMoreFlag = true;

            }
            else if (name === 'popup:closed') {
                if(data && data.moduleEl && (data.popupId=="msKnowMorePopup"|| data.popupId=="msKnowMoreDeskPopup"))
                context.broadcast('msKnowMorePopupClose');
            }else if(name==="serpPageScrolled"){
                trackSelectListings();
            }else if(name=="elementClicked"){
                let url;
                switch (data.elementType){
                    case 'searchSelectProperties':
                        context.broadcast('trackPopupSearchProperties');
                        let urlparams = {postedBy:'selectAgent',sellerId:'',builderId:'',projectId:''};
                        url =  filterConfigService.getUrlWithUpdatedParams({overrideParams: urlparams});
                        window.location.href = url;
                        break;
                    case 'unlockSelectVoucher':
                        context.broadcast('trackPopupUnlockVoucher')
                        window.open(MAKAAN_SELECT_URL);
                        break;
                }
            }else if(name==DEFICIT_LISTINGS_IN_VIEW_MESSAGE){
                let dataElements = data.elements || [],target,dataset;
                dataElements.forEach(element => {
                    target = element.target || element;
                    dataset = $(target).data()||{};
                    if ($(target).hasClass('js-deficit-listing-scroll') && !($(target).hasClass('hide')) && dataset && trackedDeficitSellerListing.indexOf(dataset.listingId) == -1){
                        trackedDeficitSellerListing.push(dataset.listingId);
                        context.broadcast('trackDeficitSellerListingSeen',{listingId:dataset.listingId,sellerId:dataset.sellerId,nearbyIndex:dataset.trackScroll});
                    }
                });
            } else if (name == PRIVILEGE_LISTINGS_IN_VIEW_MESSAGE) {
                let dataElements = data.elements || [],target,dataset;
                dataElements.forEach(element => {
                    target = element.target || element;
                    dataset = $(target).data()||{};
                    if ($(target).hasClass('js-privilege-seller-listings') && dataset) {
                        let cardData = {
                            cityId: UtilService.getPageData('cityId'),
                            pageType: UtilService.getTrackPageCategory(),
                            listingType: UtilService.getPageData("listingType")
                        };
                        context.broadcast("trackPrivilegeCardSeen", cardData);
                    }
                });
            }
        }

        function destroy() {
            window.removeEventListener('scroll', _scrollFunction,{passive:true});
            moduleEl = null;
            currentSection = null;
            adsRequestedMap = {};
            sidebarCountPreviousApiCall = null;
            previousApiCall = null;
            unbindMouseEvents(TOP_RANK_HOVER_ELEMENT);
        }

        function _getSerpApiUrl() {
            return window.location.href;
        }

        function _updateSimilar(html) {
            $(serpPageElement).find(SIMILAR_CARD_SELECTOR).html(html);
        }

        //RemoveMpromise
        // function _updateMPromiseContainer(html) {
        //     $(serpPageElement).find(MPROMISE_WRAPPER_SELECTOR).html(html);
        // }

        function _getParseHtml(html) {
            let result = {};
            let $html = $(html);
            result.propcardHtml = $html.find(LISTING_WRAPPER_SELECTOR).html();
            result.similarHtml = $html.find(SIMILAR_CARD_SELECTOR).html();
            //RemoveMpromise
            //result.mpromiseHtml = $html.find(MPROMISE_WRAPPER_SELECTOR).html();
            return result;
        }

        let preventScrollListener;

        function _searchParamsChanged() {
            if (previousApiCall) {
                previousApiCall.abort();
                previousApiCall = null;
            }
            commonService.stopAllModules($(CONTENT_WRAPPER));
            previousApiCall = apiService.get(_getSerpApiUrl());
            window.nanobar.go(50);
            previousApiCall.then(function(response) {
                previousApiCall = null;
                preventScrollListener = true;
                UtilService.gotoEl('body', 0, 0, function() {
                    preventScrollListener = false;
                });
                let parsedHTML = _getParseHtml(response.propcardHtml);
                _updateSimilar(parsedHTML.similarHtml);
                //RemoveMpromise
                //_updateMPromiseContainer(parsedHTML.mpromiseHtml);
                context.broadcast('updateListings', {
                    propcardHtml: parsedHTML.propcardHtml,
                    totalCount: response.totalCount,
                    listingCount: response.listingCount,
                    isZeroListingPage: response.isZeroListingPage,
                    callback: function() {
                        commonService.startAllModules($(CONTENT_WRAPPER),null, null, null, null, true);
                        commonService.loadGoogleAds();
                        showHideSelectBanner();
                        commonService.bindOnModuleLoad('topSellers', () => {
                            $(topSellerFallbackPlaceholder).removeClass('hide');
                            context.broadcast('serpSearchParamsChanged', {isZeroListingPage: response.isZeroListingPage});
                        });
                    }
                });
               
                window.nanobar.go(100);

            });
        }

        function _updateSidebarRentBuyCount() {

            if (sidebarCountPreviousApiCall) {
                sidebarCountPreviousApiCall.abort();
                sidebarCountPreviousApiCall = null;
            }

            let listingType = UtilService.getPageData("listingType"),
                countListingType = listingType === 'buy' ? 'rent' : 'buy',
                url = filterConfigService.getUrlWithUpdatedParams({
                    skipFilterData: true,
                    overrideParams: {
                        listingType: countListingType,
                        sidebarCount: true
                    }
                });

            sidebarCountPreviousApiCall = apiService.get(url);
            sidebarCountPreviousApiCall.then(() => {
                sidebarCountPreviousApiCall = null;
                // serpPageElement.querySelector('#'+BUY_RENT_SWITCH_CARD_ID).innerHTML = response.totalCount || ''; // update count in respective element
            });

        }

        if (!UtilService.isMobileRequest()) { // because sidebar is available only for desktop
            commonService.bindOnLoadPromise().then(() => {
                _updateSidebarRentBuyCount(); // update sidebar buy/rent count
            });
        }

        function _updateGoogleAd(scrollTop){
            if(UtilService.isMobileRequest()){
                return;
            }
            scrollTop = scrollTop || $(window).scrollTop();
            let sectionHeight = $(window).height();
            let documentHeight = $(document).height();
            let sections = Math.floor(documentHeight/NUMBER_OF_ADS);
            let viewArea = scrollTop + sectionHeight;
            let newSection = Math.floor(viewArea/sections);
            if(newSection == currentSection){
                adsRequestedMap[currentSection] = true;
                return;
            }
            currentSection = newSection;
            let $elem = $(moduleEl).find(`[data-repeat-ad=true]`).addClass('hide').siblings(`[data-section=${currentSection}]`).removeClass("hide");
            if(!adsRequestedMap[currentSection]){
                let $parent = $elem.parent();
                $parent.prepend($elem.clone());
                $elem.remove();
                $elem.removeAttr('data-adsbygoogle-status').html();
                (window.adsbygoogle = window.adsbygoogle || []).push({});
                adsRequestedMap[currentSection] = true;
            }
        }
        function trackSelectListings(){
            let viewPortElements = UtilService.attributeListInViewPort('.js-selectSellerBadge', 'listingid');
            let listingId;
            viewPortElements.forEach(el => {
                listingId = el||'';
                if(trackedSelectListings.indexOf(listingId)==-1){
                    trackedSelectListings.push(listingId);
                    context.broadcast('selectListingSeen',{source:listingId});
                }
            });
        }


        function checkSelectBannersInView(){
                if((UtilService.checkInViewPort('.js-repeat-selectPromo')) && !selectBannerTracked){
                    selectBannerTracked = true;
                    context.broadcast('selectBannerSeen',{source:'Serp_Repeat'});
                }
                if((UtilService.checkInViewPort('.js-repeat-selectReminder')) && !selectReminderTracked){
                    selectReminderTracked = true;
                    context.broadcast('selectReminderSeen',{source:'Serp_Repeat'});
                }
        }
        function checkDeficitSellerListingsInView(){
            let elements = moduleEl.querySelectorAll('.js-deficit-listing-scroll');
            UtilService.trackElementInView(elements,'.js-deficit-listing-scroll', '',DEFICIT_LISTINGS_IN_VIEW_MESSAGE);
        }

        function _scrollFunction() {
            context.broadcast('serpPageScrolled');

            let scrollTop = $(window).scrollTop(),
                topFold = $('[data-top-fold]'),
                filters = $('[data-filters]'),
                topFoldHeight = topFold.outerHeight(true),
                cardholderHeight = $('[data-cardholder]').outerHeight(),
                margin = UtilService.isMobileRequest() ? 20 : 30,
                criticalHeightForMoreListing = (5 * (cardholderHeight + margin)) - margin,
                listingContainer = $('[search-result-wrap]'),
                listingContainerTop = listingContainer.offset().top,
                listingContainerHeight = listingContainer.outerHeight(),
                listingContainerBottom = listingContainerTop + listingContainerHeight,
                $agentProfieContainer = $moduleEl.find('.js-profile-container');

            if (scrollTop <= topFoldHeight-160) { // 160 is total height of fixed elements
                filters.removeClass('posfix');
                 $agentProfieContainer.removeClass('fixme');
            } else {
                filters.addClass('posfix');
                $agentProfieContainer.addClass('fixme');
            }
            if (scrollTop > (listingContainerBottom - criticalHeightForMoreListing) && loadMoreFlag) {
                loadMoreFlag = false;
                context.broadcast("loadMoreListings");
            }
            checkSelectBannersInView();
            _updateGoogleAd(scrollTop);

        }
        return {
            messages,
            init,
            destroy,
            onmessage,
            onclick,
            behaviors
        };
    });
});
