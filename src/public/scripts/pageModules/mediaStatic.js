"use strict";
define([
    'services/commonService',
    'services/trackingService',
    'behaviors/mediaStaticTracking'
], () => {
    Box.Application.addModule("mediaStatic", (context) => {

        //const CommonService = context.getService('CommonService'),
            //logger = context.getService('Logger');
        var messages = [],
            behaviors = ['mediaStaticTracking'];

        function init() {
            //initialization of all local variables
        }

        function destroy() {
            //clear all the binding and objects
        }

        function onmessage() {
            // bind custom messages/events
        }

        function onclick(event, element, elementType) {
            // bind custom messages/events
            let text, pageType = 'mediaResources';
            switch (elementType) {
                case 'header-news-bytes':
                    context.broadcast('trackNewsHeaderNewsBytesClick', { pageType: pageType });
                    break;
                case 'header-press-releases':
                    context.broadcast('trackNewsHeaderPressReleasesClick', { pageType: pageType });
                    break;
                case 'header-media-resources':
                    context.broadcast('trackNewsHeaderMediaResourcesClick', { pageType: pageType });
                    break;
                case 'download':
                    text = $(element).attr('data-value');
                    context.broadcast('trackNewsMediaImageDownload', { text: text });
                    break;
                case 'pagination':
                    context.broadcast('trackNewsPaginationClick', { pageType: pageType });
                    break;
            }

        }
        return {
            init,
            messages,
            behaviors,
            onmessage,
            onclick,
            destroy
        };
    });
});
