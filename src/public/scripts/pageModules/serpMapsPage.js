 /**
   * Description: wrapper module for maps SERP page
   * @author: [Aditya Jha]
   * Date: Dec 8, 2015
**/

"use strict";
define([
    'services/filterConfigService',
    'services/pushNotificationService',
    'services/commonService',
    'services/apiService',
    'services/urlService',
    'services/utils',
    'behaviors/infiniteScroll'
], function(filterConfigService, pushNotificationService) {
    Box.Application.addModule('serpMapsPage', function(context) {

        var commonService, apiService, urlService, $, serpMapsPage,
            previousApiCall, scrollElementId, utils, loadMoreFlag, scrollContainer;

        var _getSerpApiUrl = function() {
            return window.location.href;
        };

        function _makePageSettings() {
            let urlParam = urlService.getAllUrlParam();
            if(urlParam.hasOwnProperty('path')) {
    			context.broadcast('drawPolygonFromEncodedPath', unescape(urlParam.path));

                context.broadcast('hideSearchInViewPortModule');
                $('.map-btn-wrapper.top-control').css('display','table');
    		} else if(urlParam.hasOwnProperty('latitude') && urlParam.hasOwnProperty('longitude') && urlParam.viewport) {
                context.broadcast('setMapToViewPort', urlParam.viewport);
    		} else if(!urlParam.hasOwnProperty('path')) {
                context.broadcast('removeDrawnShape');
            }
            if(!utils.isMobileRequest()) {
                $('#mapsAlvSidebar').animate({scrollTop:0}, 500);
            }
        }

        function _scrollFunction() {
            let listingContainer = $('[search-result-wrap]'),
                listingContainerHeight = listingContainer.outerHeight(),
                cardholderHeight = $('[data-cardholder]').outerHeight(),
                criticalHeightForMoreListing = 5*(cardholderHeight + 20)-20,
                scrollTop = $(scrollContainer).scrollTop();

            if (!utils.isMobileRequest() && scrollTop > (listingContainerHeight - criticalHeightForMoreListing) && loadMoreFlag) {
                loadMoreFlag = false;
                context.broadcast("loadMoreListings");
            }
        }

        var _getFitBoundStatus = function() {
            let urlParam = urlService.getAllUrlParam();
            if(urlParam.hasOwnProperty('path')) {
                return false;
            } else if(urlParam.hasOwnProperty('latitude') && urlParam.hasOwnProperty('longitude') && urlParam.viewport) {
                if(urlParam.viewport && urlParam.viewport.split(',').length ===3){
                    return false;
                }
            }
            return true;
        };

        function _loadSubmodules() {

            // if(!utils.isMobileRequest()){
            //     context.broadcast('loadModule', [{
            //         parent: 'mapsModule',
            //         name: 'mapSvgLegendsModule'
            //     }, {
            //         parent: 'mapsModule',
            //         name: 'drawShapeModule'
            //     }, {
            //         parent: 'mapsModule',
            //         name: 'heatMapModule'
            //     }, {
            //         parent: 'mapsModule',
            //         name: 'searchInViewPortModule'
            //     }]);
            // }
            apiService.get(_getSerpApiUrl()).then(function(response){
    			context.broadcast('drawMarkers', {markers:response.listingData.data, markerType: 'listing', markersFitFlag:_getFitBoundStatus()});
    		});
        }

        var _searchParamsChanged = function() {
            _makePageSettings();
    		if(previousApiCall) {
    			previousApiCall.abort();
    			previousApiCall = null;
    		}
    		previousApiCall = apiService.get(_getSerpApiUrl());
            window.nanobar.go(50);
    		previousApiCall.then(function(response){
                window.nanobar.go(100);
                utils.gotoEl('body',0,0);
    			previousApiCall = null;
    			context.broadcast('drawMarkers', {markers:response.listingData.data, markerType: 'listing', markersFitFlag:_getFitBoundStatus()});
                context.broadcast('updateListings', {
                    propcardHtml: response.propcardHtml,
                    listingCount: response.listingCount,
                    isZeroListingPage: response.isZeroListingPage
                });
    		});
    	};

        return {
            messages: ['serpListingMouseentered', 'serpListingMouseLeft','toggleHeatMapClicked', 'svgLegendButtonClicked', 'searchParamsChanged', 'pageChanged', 'markerClicked', 'mapSubmoduleLoaded', 'showDrawShapeMessage', 'favourites-synced', 'moreListingsLoaded', 'listingsUpdated', 'morelistingsNotLoaded'],
            behaviors: ['infiniteScroll'],
            init: function() {
                serpMapsPage = context.getElement();
                commonService = context.getService('CommonService');
                apiService = context.getService('ApiService');
                urlService = context.getService('URLService');
                loadMoreFlag = true;
                $ = context.getGlobal('jQuery');
                scrollElementId = context.getConfig('elementId');
                scrollContainer = $(serpMapsPage).find('#'+scrollElementId);
                $(scrollContainer).on('scroll', _scrollFunction);
                utils = context.getService('Utils');
                // loads submodules once mapsModule is loaded
                commonService.bindOnModuleLoad('mapsModule', function(){
                    _loadSubmodules();
                });

                commonService.bindOnLoadPromise().then(function(){
                    context.broadcast('loadModule', [{
                        name: 'sellerReviews'
                    }]);
                });

                utils.updatePushNotificationTags('VISITED_SERP_MAPS');
            },
            destroy: function() {
                previousApiCall = null;
                $(scrollContainer).off('scroll', _scrollFunction);
            },
            onmessage: function(name, data) {
                if(name == 'serpListingMouseentered'){
                    context.broadcast('triggerListingMouseenter', data);
                }else if(name == 'serpListingMouseLeft'){
                    context.broadcast('triggerListingMouseleave', data);
                } else if(name == 'toggleHeatMapClicked'){
                    if(data){
                        $(serpMapsPage).find('.bot-control').addClass('heat-map-opened');
                    } else{
                        $(serpMapsPage).find('.bot-control').removeClass('heat-map-opened');
                    }
                } else if(name == 'svgLegendButtonClicked'){
                    if(data){
                        $(serpMapsPage).find('.bot-control').addClass('master-plan-opened');
                    } else{
                        $(serpMapsPage).find('.bot-control').removeClass('master-plan-opened');
                    }
                } else if(name === 'searchParamsChanged') {
                    _searchParamsChanged();
                } else if(name === 'pageChanged') {
                    if(utils.isMobileRequest()) {
                        $('#mapsAlvSidebar').animate({scrollLeft:0}, 500);
                    }else{
                        $('#'+scrollElementId).animate({scrollTop:0}, 1000);
                    }
                } else if (name === 'markerClicked') {
                    let selector = data.selector;
                    if(selector && selector.split('_')[0] == 'listing') {
                        if(utils.isMobileRequest()) {
                            $('#mapsAlvSidebar').animate({scrollLeft:($('[data-selector="'+ data.selector + '"]')).position().left}, 500);
                        } else {
                            $('#mapsAlvSidebar').animate({scrollTop:($('[data-selector="'+ data.selector + '"]')).position().top}, 500);
                        }
                        let select = $('#mapsAlvSidebar').find('[data-selector="'+ data.selector + '"]');
                        if(select){
                            select.addClass('blink');
                            setTimeout(function() {
                                select.removeClass('blink');
                            }, 3000);
                        }
                    }
                } else if(name == 'mapSubmoduleLoaded') {
                    if(data == 'searchInViewPortModule') {
                        _makePageSettings();
                    }
                }else if(name == 'showDrawShapeMessage'){
                    context.broadcast('popup:open', {id: 'drawOnMapPopup'});
                    $('#drawOnMapPopup').find('.js-drawshap-message').html(data.message);
                }else if(name == 'favourites-synced'){
                    context.broadcast('updateMarkerShortlistStatus', {
                        all: true,
                        markerType: 'listing'
                    });
                } else if(name == 'moreListingsLoaded' || name == 'listingsUpdated' || name === 'morelistingsNotLoaded') {
                    loadMoreFlag = true;
                }
            },
            onclick: function(event, element, elementType) {
                let url;
                context.broadcast('userClicked', {
                    element: element,
                    elementType: elementType
                });
                if(elementType === 'saveSearch') {
                    commonService.bindOnModuleLoad('saveSearch', function() {
                        context.broadcast('openSaveSearch', {trackId: 'SerpMapsSetAlert'});
                        context.broadcast('popup:open', {id: 'saveSearchPopup'});
                    });
                }else if(elementType === 'builder-ongoing-projects'){
                        url = filterConfigService.getUrlWithUpdatedParams({
                            overrideParams: {
                                possession: 'any',
                                ageOfProperty: null
                            }
                        });
                        urlService.ajaxyUrlChange(url);
                }else if(elementType === 'builder-completed-projects'){
                    url = filterConfigService.getUrlWithUpdatedParams({
                        overrideParams: {
                            ageOfProperty: 'any',
                            possession: null
                        }
                    });
                    urlService.ajaxyUrlChange(url);
                }
            }
        };
    });
});
