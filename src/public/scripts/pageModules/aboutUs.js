"use strict";
define([
    'services/commonService',
    'scripts/dependency/owlBundle'
], () => {
    Box.Application.addModule("aboutUs", () => {

      //  const CommonService = context.getService('CommonService'),
          //  logger = context.getService('Logger');
        var messages = [],
            behaviors = [];

        function init() {
            //initialization of all local variables
            $("#owl-demo").owlCarousel({
                navigation: true,
                slideSpeed: 300,
                paginationSpeed: 400,
                // transitionStyle : "fadeUp",
                autoPlay: false,
                mouseDrag: true,
                singleItem: true
                // "singleItem:true" is a shortcut for:
                // items : 1,
                // itemsDesktop : false,
                // itemsDesktopSmall : false,
                // itemsTablet: false,
                // itemsMobile : false

            });

            $("#owl-demo1").owlCarousel({
                navigation: true,
                slideSpeed: 600,
                paginationSpeed: 800,
                // transitionStyle : "fadeUp",
                autoPlay: false,
                mouseDrag: true,
                singleItem: true
                // "singleItem:true" is a shortcut for:
                // items : 1,
                // itemsDesktop : false,
                // itemsDesktopSmall : false,
                // itemsTablet: false,
                // itemsMobile : false

            });
            window.addEventListener('scroll', scrollHandler, {passive:true});
        }

        function scrollHandler() {
            //console.log("its workign here");
            $('#akl').each(function() {
                var imagePos = $(this).offset().top;

                var topOfWindow = $(window).scrollTop();
                if (imagePos < topOfWindow + 400) {
                    $(this).addClass("fadeIn");
                }
            });

            $('#avl').each(function() {
                var imagePos = $(this).offset().top;

                var topOfWindow = $(window).scrollTop();
                if (imagePos < topOfWindow + 400) {
                    $(this).addClass("fadeIn");
                }
            });

            $('#agl').each(function() {
                var imagePos = $(this).offset().top;

                var topOfWindow = $(window).scrollTop();
                if (imagePos < topOfWindow + 400) {
                    $(this).addClass("fadeIn");
                }
            });

            $('#arl').each(function() {
                var imagePos = $(this).offset().top;

                var topOfWindow = $(window).scrollTop();
                if (imagePos < topOfWindow + 400) {
                    $(this).addClass("fadeIn");
                }
            });

            $('#atl').each(function() {
                var imagePos = $(this).offset().top;

                var topOfWindow = $(window).scrollTop();
                if (imagePos < topOfWindow + 600) {
                    $(this).addClass("fadeIn");
                }
            });
        }

        function destroy() {
            //clear all the binding and objects
            window.removeEventListener('scroll', scrollHandler, {passive:true});
        }

        function onmessage() {
            // bind custom messages/events
        }

        function onclick() {
            // bind custom messages/events
        }

        return {
            init,
            messages,
            behaviors,
            onmessage,
            onclick,
            destroy
        };
    });
});
