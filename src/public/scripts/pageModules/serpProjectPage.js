/**
 * Description: wrapper module for SERP page
 * @author: [Aditya Jha]
 * Date: Nov 30, 2015
 **/
"use strict";
define([
    'services/filterConfigService',
    'services/urlService',
    "common/sharedConfig",
    'services/commonService',
    'services/apiService',
    'services/utils',
    'behaviors/serpProjectTracking',
    'scripts/dependency/owlBundle'
], (filterConfigService, urlService, sharedConfig) => {
    Box.Application.addModule('serpProjectPage', (context) => {

        const $ = context.getGlobal('jQuery'),
            apiService = context.getService('ApiService'),
            commonService = context.getService('CommonService'),
            UtilService = context.getService('Utils'),
            //query = Box.DOM.query,
            //queryAllData = Box.DOM.queryAllData,
            SIMILAR_CARD_SELECTOR = '[data-similar-card]',
            PROJECT_WRAPPER_SELECTOR = '[data-project-wrapper]',
            TOP_RANK_HOVER_ELEMENT = "[data-type='sidebar-toprank']",
            OWL_CONFIG = sharedConfig.OWL_CAROUSEL_CONFIG;


        var previousApiCall = null,
            serpPageElement,
            serpScrollTrackingEventSent = false,
            hoverStartTimeRHS,
            messages = ['searchParamsChanged', 'pageChanged', 'showon-map', 'popup:opened', 'popup:main_top_seller'],
            behaviors = ['serpProjectTracking'];

        function _openPopup(id, closeOnEsc) {
            context.broadcast('popup:open', {
                id: id,
                options: {
                    config: {
                        modal: closeOnEsc || false
                    }
                }
            });
        }

        function init() {
            serpPageElement = context.getElement();
            window.addEventListener('scroll', _scrollFunction,{passive:true});
            bindMouseEvents(TOP_RANK_HOVER_ELEMENT);

            if(!UtilService.isMobileRequest()){
                commonService.bindOnLoadPromise().then(function(){
                    context.broadcast('loadModule', [{
                        name: 'pyr',
                        id: 'serpPyrForm'
                    }]);
                });
            }

            if(urlService.getUrlParam("setAlert") && urlService.getUrlParam("email")) {
                commonService.bindOnModuleLoad('saveSearch', function() {
                    _openPopup('saveSearchPopup');
                    context.broadcast('directSetAlertWelcome', {trackId: 'SerpPageSetAlert'});
                });
            }
        }

        function onclick(event, element, elementType) {
            let url;
            context.broadcast('userClicked', {
                element: element,
                elementType: elementType
            });
            let clickElementData = $(element).data(),
                pageType = UtilService.getTrackPageCategory(),
                pageCategory = 'Buyer_' + pageType,
                $element = $(element);
            switch (elementType) {
                case 'back-btn':
                case 'next-btn':
                    if (clickElementData.trackLabel && clickElementData.trackAction) {
                        UtilService.trackEvent(clickElementData.trackAction, pageCategory, clickElementData.trackLabel);
                    }
                    break;
                case 'saveSearch':
                    commonService.bindOnModuleLoad('saveSearch', function() {
                        context.broadcast('openSaveSearch', {trackId: 'SerpPageSetAlert'});
                        _openPopup('saveSearchPopup');
                    });
                    break;
                case 'builder-ongoing-projects':
                    url = filterConfigService.getUrlWithUpdatedParams({
                        overrideParams: {
                            possession: 'any',
                            ageOfProperty: null,
                            makeProjectUrl: true
                        }
                    });
                    urlService.ajaxyUrlChange(url);
                    break;
                case 'builder-completed-projects':
                    url = filterConfigService.getUrlWithUpdatedParams({
                        overrideParams: {
                            ageOfProperty: 'any',
                            possession: null,
                            makeProjectUrl: true
                        }
                    });
                    urlService.ajaxyUrlChange(url);
                    break;
                case 'open-pyr':

                    if(!commonService.checkModuleLoaded('pyr','serpPyrForm')){
                        context.broadcast('trackSerpPyrCardFail');
                    }else {
                        context.broadcast('trackPyrCardClick');
                    }

                    let localityId = UtilService.getPageData('localityId'),
                        localityName = UtilService.getPageData('localityName');
                    let data = {
                        id: 'serpPyrForm',
                        pyrData: {
                            stage: "step1"
                        },
                        trackData: {
                            id: 'SerpPyrRhs'
                        }
                    };
                    if (localityId && localityName) {
                        data.pyrData.localityIds = [];
                        data.pyrData.localityIds = [{
                            id: localityId,
                            name: localityName,
                            type: 'locality'
                        }];
                    }
                    context.broadcast('openPyr', data);
                    _openPopup('serpPyrPopup', true);
                    break;
                case 'open-jarvis':
                    context.broadcast('trackJarvisCardClick');
                    let jarvisChatElem = document.querySelector("#makaan-man");
                    if (jarvisChatElem) {
                        jarvisChatElem.click();
                    }
                    break;
                case 'download-app':
                    context.broadcast('trackDownloadCardClick');
                    break;
                case 'dashboard-link':
                    context.broadcast('trackJourneyCardClick');
                    break;
                case 'sidebar-toprank':
                    url = filterConfigService.getUrlWithUpdatedParams({
                        skipHref: false,
                        skipPageData: false,
                        skipFilterData: false,
                        makeProjectUrl: true,
                        overrideParams: {
                            localityId: $element.data('id')
                        }
                    });

                    urlService.ajaxyUrlChange(url);
                    context.broadcast('trackSimilarCardClick', {
                        id: $element.data('id'),
                        type: $element.data('similar-type')
                    });

                    event.preventDefault();
                    break;

                case 'stop-anchor':
                    event.preventDefault();
                    break;
            }
        }

        function bindMouseEvents(element){
            $(element).on('mouseenter', onmouseenter).on('mouseleave',onmouseleave);
        }

        function unbindMouseEvents(element){
            $(element).off('mouseenter', onmouseenter).off('mouseleave',onmouseleave);
        }

        function onmouseenter() {
            hoverStartTimeRHS = new Date().valueOf();
        }

        function onmouseleave() {
            let $element = $(this);
            context.broadcast('trackSimilarCardHover',{
                id: $element.data('id'),
                type: $element.data('similar-type'),
                hoverTime : new Date().valueOf() - hoverStartTimeRHS
            });
        }


        function onmessage(name) {
            if (name === 'searchParamsChanged') {
                _searchParamsChanged();
                serpScrollTrackingEventSent = false;
            } else if (name === 'pageChanged') {
                $('body').animate({
                    scrollTop: 0
                }, 0);
            } else if (name === 'popup:opened') {
                $('#owl-demo1').owlCarousel(OWL_CONFIG);
            } else if (name === 'popup:main_top_seller') {
                $('#owl-demo1').owlCarousel(OWL_CONFIG);
            }
        }

        function destroy() {
            window.removeEventListener('scroll', _scrollFunction,{passive:true});
            previousApiCall = null;
            unbindMouseEvents(TOP_RANK_HOVER_ELEMENT);
        }

        function _getSerpApiUrl() {
            return window.location.href;
        }

        function _updateSimilar(html){
            $(serpPageElement).find(SIMILAR_CARD_SELECTOR).html(html);
        }

        function _getParseHtml(html){
            let result = {};
            let $html = $(html);
            result.propcardHtml = $html.find(PROJECT_WRAPPER_SELECTOR).html();
            result.similarHtml = $html.find(SIMILAR_CARD_SELECTOR).html();
            return result;
        }

        let preventScrollListener;

        function _searchParamsChanged() {
            if (previousApiCall) {
                previousApiCall.abort();
                previousApiCall = null;
            }
            previousApiCall = apiService.get(_getSerpApiUrl());
            window.nanobar.go(50);
            previousApiCall.then(function(response) {
                previousApiCall = null;
                preventScrollListener = true;
                UtilService.gotoEl('body', 0, 0, function() {
                    preventScrollListener = false;
                });
                let parsedHTML = _getParseHtml(response.propcardHtml);
                _updateSimilar(parsedHTML.similarHtml);
                context.broadcast('updateProjects', {
                    propcardHtml: parsedHTML.propcardHtml,
                    projectCount: response.projectCount
                });
                window.nanobar.go(100);

            });
        }

        let sticky = $('[data-sidebar-fixed-container]'),
        stickyTop = sticky.length && sticky.offset().top;

        function _scrollFunction() {

            sticky = sticky || $('[data-sidebar-fixed-container]');
            stickyTop = stickyTop || (sticky.length && sticky.offset().top);
            let scrollTop = $(window).scrollTop(),
                stickyHeight = sticky.outerHeight(),
                footer = $('[data-page-footer]'),
                footTop = footer.offset().top,
                topFold = $('[data-top-fold]'),
                filters = $('[data-filters]'),
                filtersHeight = filters.outerHeight(true),
                topFoldHeight = topFold.outerHeight(true),
                //mixPanelElem = serpPageElement.querySelector('[data-type="mixpanel-step"]'),
                headerHeight = $('[data-header]').outerHeight(true);

            if (scrollTop <= topFoldHeight) {
                filters.removeClass('posfix');
            } else {
                filters.addClass('posfix');
            }
            // for side bar top rank cards sticky ----- start-------
            if(scrollTop < stickyTop - headerHeight - filtersHeight){
                sticky.removeClass('posfix');
            }else{

                let absolutePosition  = footTop - stickyHeight - topFoldHeight - filtersHeight - headerHeight - 365;
                if(scrollTop > absolutePosition + topFoldHeight){
                    sticky.css({position:"absolute",top:absolutePosition +'px'});
                    sticky.removeClass('posfix');
                }else{
                    sticky.removeAttr('style');
                    sticky.addClass('posfix');
                }
            }
        }
        return {
            messages,
            behaviors,
            init,
            destroy,
            onmessage,
            onclick
        };
    });
});
