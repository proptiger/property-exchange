"use strict";
define([
    'common/sharedConfig',
    'services/apiService',
    'services/utils',
    'services/commonService',
    'behaviors/contactUsTracking',
    'scripts/vendor/staticPages/bootstrap'
], function(sharedConfig, apiService) {
    Box.Application.addModule("contactUs", (context) => {

        //const CommonService = context.getService('CommonService'),
           const mailUrl = sharedConfig.apiHandlers.mailerService().url;
           // logger = context.getService('Logger');
        var messages = [],
            config, moduleEl, $, $moduleEl,
            behaviors = ['contactUsTracking'];


        function init() {
            moduleEl = context.getElement();
            $ = context.getGlobal('jQuery');
            $moduleEl = $(moduleEl);
        }

        function sendMail(){
            var mailData,
                username = moduleEl.querySelector('.js-username').value,
                userContact = moduleEl.querySelector('.js-mobileNumber').value,
                useremail = moduleEl.querySelector('.js-email').value,
                mailSubject = moduleEl.querySelector('.js-subject-mail').value,
                mailMessage = moduleEl.querySelector('.js-mail-message').value,
                cityElem = moduleEl.querySelector('.js-cityList ul li.active'),
                successmsg = moduleEl.querySelector('.success-msg'),
                errormsg = moduleEl.querySelector('.error-msg-submit'),
                city;
                if(cityElem) {
                    city =  cityElem.innerHTML.trim();
                }
                mailData = {
                    name : username,
                    phone: userContact,
                    email: useremail,
                    subject: mailSubject,
                    message: mailMessage,
                    city: city
                };
            if(username && userContact && useremail && mailSubject && mailMessage && cityElem){
                apiService.postJSON(mailUrl, mailData).then( () => {
                    $(successmsg).removeClass("hidden");
                    $(errormsg).addClass("hidden");
                    submitTracking("success",successmsg);
                },()=> {
                    $(errormsg).addClass("hidden");
                    submitTracking("error",errormsg);
                });
            }else{
                $(errormsg).removeClass("hidden");
                submitTracking("error",errormsg);
            }
        }
        function submitTracking(type,message){
             //var pageType="Buyer_Static_Pages";
            // var dataset = {
            //         trackAction: "Submit_ContactUS_Form",
            //         PAGE_CATEGORY: pageType,
            //         trackLabel: ""
            //     };
            switch(type){
                case "success":
                    context.broadcast('trackSuccessContactUs');
                break;
                case "error":
                    context.broadcast('trackErrorContactUs', {
                        label: $(message).text()
                    });
                break;
            }

        }
        function destroy() {
            //clear all the binding and objects
            config = null;
            moduleEl = null;
            $moduleEl = null;
        }

        function onmessage() {
            // bind custom messages/events
        }

        function onclick(event, element, elementType) {
            // bind custom messages/events
            switch (elementType) {
                case 'submitForm':
                    event.preventDefault();
                    sendMail(element);
                    break;
            }
        }

        return {
            init,
            messages,
            behaviors,
            onmessage,
            onclick,
            destroy
        };
    });
});
