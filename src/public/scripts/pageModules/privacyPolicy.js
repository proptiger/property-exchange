"use strict";
define([
    'services/commonService',
    'scripts/vendor/staticPages/bootstrap'
], () => {
    Box.Application.addModule("privacyPolicy", () => {

        //const CommonService = context.getService('CommonService'),
           // logger = context.getService('Logger');
        var messages = [],
            behaviors = [];

        function init() {
            //initialization of all local variables
        }

        function destroy() {
            //clear all the binding and objects
        }

        function onmessage() {
            // bind custom messages/events
        }

        function onclick() {
            // bind custom messages/events
        }

        return {
            init,
            messages,
            behaviors,
            onmessage,
            onclick,
            destroy
        };
    });
});
