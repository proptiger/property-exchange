"use strict";
define([
    'services/commonService',
], () => {
    Box.Application.addModule("partnerWithMakaan", (context) => {

        var currentSection;

        function _openGetInTouch(section){
            context.broadcast("open-get-in-touch", section);
        }

        function _openPopupSlider(data, element){
            let title = $(element).html();
            data.title = title;
            context.broadcast("open-popup-slider", data);
        }

        function init() {
            //initialization of all local variables
            $("#owl-demo, #owl-demo2, #owl-demo3, #owl-demo4, #owl-demo5, #owl-demo6, #owl-demo7").owlCarousel({
                navigation: true,
                slideSpeed: 300,
                paginationSpeed: 400,
                // transitionStyle : "fadeUp",
                autoPlay: false,
                mouseDrag: true,
                singleItem: true
                // "singleItem:true" is a shortcut for:
                // items : 1,
                // itemsDesktop : false,
                // itemsDesktopSmall : false,
                // itemsTablet: false,
                // itemsMobile : false

            });

            context.broadcast("loadModule", [{
                name: "get-in-touch",
            }, {
                name: "popup-slider"
            }]);

            $(".scroll-down >span").click(function() {
                $('html, body').animate({
                    scrollTop: $(".why-partner").offset().top - 98
                }, 800);
            });
        }

        function onclick(event, element, elementType){
            let data = $(element).data();
            switch(elementType){
                case "get-in-touch":
                    let currentSectionId = currentSection && currentSection.id || data.section;
                    _openGetInTouch({currentSection: currentSectionId});
                    break;
                case "more-info":
                    _openPopupSlider(data, element);
                    break;
            }
        }

        let onmessage = {
            "scrollSectionChanged": function (data){
                currentSection = data.element;
            }
        };

        return {
            init,
            onclick,
            onmessage
        };
    });
});
