"use strict";

define(['services/commonService', 'common/utilFunctions', 'services/utils','behaviors/homeloanFlowTracking'], function(commonService, utilFunctions, UtilService) {
    Box.Application.addModule('homeLoanDashboard', function(context) {
        let moduleEl, $, $moduleEl, moduleId;
        const eligibilityPercentageExceedLimit = 60;
        var preObj = {};

        function _switchCalc(elementName) {
            $moduleEl.find('.calc-btn').removeClass('active');
            $moduleEl.find('[data-name=' + elementName + ']').addClass('active');
            if (elementName == 'EMI') {
                $moduleEl.find('.home-loan-wrap').removeClass('hide');
                $moduleEl.find('.emi-calc').addClass('hide');
            } else if (elementName == 'eli') {
                $moduleEl.find('.home-loan-wrap').addClass('hide');
                $moduleEl.find('.emi-calc').removeClass('hide');
            }
        }

        function _calcHgHeight(percent) {
            percent = (percent * 100) / 60
            let height = (percent * 70) / 100
            $moduleEl.find('.js-hg-top').css('height', (70 - height));
            $moduleEl.find('.js-hg-bot').css('height', height);
        }

        function _updateEliValues(data) {
            if (!data.grossEarning == 0) {
                let percent = 0;
                if (!data.existingEmi == 0) {
                    percent = (((data.existingEmi) / (data.grossEarning / 12)) * 100).toFixed(0);
                }
                if (data.loanAmount < 0) {
                    $moduleEl.find('[data-type=emi-error]').addClass('show-emi-error');
                    $moduleEl.find('[data-type=emi-error]').text('Value cannot exceed ' + eligibilityPercentageExceedLimit + '% of your current Salary');
                } else {
                    $moduleEl.find('[data-type=emi-error]').removeClass('show-emi-error');
                    setTimeout(_calcHgHeight(parseInt(percent)), 0);

                    percent = percent.toString() + '%';
                    $moduleEl.find('[data-type=eli-percent]').html(percent);
                    $moduleEl.find('[data-type=eli-amt]').html(utilFunctions.toCurrencyFormat(data.loanAmount));
                }
            } else {
                $moduleEl.find('[data-type=eli-percent]').html('0%');
                $moduleEl.find('[data-type=eli-amt]').html('0');
            }
        }

        function _showApplyHomeLoanPopup() {
            context.broadcast('popup:open', {
                id: 'applyHomeLoan',
                options: {
                    config: {
                        id: 'homeLoanPopup',
                        modal: true,
                        escKey: true
                    }
                }
            });
        }

        function _broadcastToHomeloanFlow(message,sourcemodule) {
            context.broadcast('loadModule', {
                name: 'homeloanFlow'
            });
            commonService.bindOnModuleLoad('homeloanFlow', () => {
                context.broadcast(message, {
                    startScreen:'summary',
                    preObject: preObj,
                    page:'HOME LOAN',
                    source:sourcemodule
                });
            });
        }

        // Events
        var init = () => {
            moduleEl = context.getElement();
            moduleId = moduleEl.id;
            $ = context.getGlobal('jQuery');
            $moduleEl = $(moduleEl);
            commonService.startAllModules(moduleEl);
            context.broadcast('trackPageLoaded', { id: moduleId, label: 'homeloan' });
        };
        var messages = ['makaaniq-post_slider_element_clicked', 'eligibilityCalculated', 'moduleLoaded', 'openHomeloanPopup', "popup:closed", 'interestedBankList'];
        var behaviors = ['homeloanFlowTracking'];
        var onmessage = (name, data) => {
            switch (name) {
                case 'eligibilityCalculated':
                    data = data.data;
                    if (data.profession && data.grossEarning && data.loanAmount) {
                        preObj.income_profession = data.profession;
                        preObj.income_grossEarning = data.grossEarning;
                        preObj.income_existingEmi = data.existingEmi;
                        var loanAmount = UtilService.formatNumber(data.loanAmount, { precision : 2, returnSeperate : true, convertThousand : true, seperator : window.numberFormat });
                        preObj.income_loan = loanAmount.val;
                        preObj.income_loanUnit = loanAmount.unit;
                        _updateEliValues(data);
                    } else {
                        preObj.income_grossEarning = '';
                        preObj.income_existingEmi = '';
                        preObj.income_loan = '';
                        preObj.income_loanUnit = '';
                        $moduleEl.find('[data-type=eli-percent]').html('0%');
                        $moduleEl.find('[data-type=eli-amt]').html('0');
                    }
                    break;

                case 'openHomeloanPopup':
                    _showApplyHomeLoanPopup();
                    break;
                case "popup:closed":
                    let ele = document.getElementById('homeloanFlow');
                    commonService.stopAllModules(ele);
                    commonService.stopModule(ele);
                    break;
                case 'moduleLoaded':
                    if (data.name == "eligibilityCalculatorLoaded") {
                        $moduleEl.find('[data-type=eli-amt]').html('0');
                    }
                    break;
                case 'interestedBankList':
                    preObj.bank_selectedBanks = data.selected;
                    break;

            }
        };

        var onclick = (event, element, elementType) => {
            let sourcemodule;
            switch (elementType) {
                case 'applyHomeLoan1':
                    sourcemodule ='Apply Now 1';
                    context.broadcast('trackHomeLoanDashboard', { id: moduleId, label: 'HOME LOAN' , page:'HOME LOAN' , sourcemodule});
                   _broadcastToHomeloanFlow('homeloanFlow',sourcemodule);
                    break;
                case 'applyHomeLoan2':
                    sourcemodule ='Apply Now 2';
                    context.broadcast('trackHomeLoanDashboard', { id: moduleId, label: 'HOME LOAN' , page:'HOME LOAN' , sourcemodule});
                   _broadcastToHomeloanFlow('homeloanFlow',sourcemodule);
                    break;
                case 'calculate_homeloan':
                    $('html,body').animate({
                        scrollTop: $moduleEl.find('.homeloan-calcs').offset().top-50
                    }, 2000);
                    break;
            }
        };

        return {
            messages,
            behaviors,
            onclick,
            onchange,
            onmessage,
            init,
        };
    });
});
