"use strict";
define([
    'services/commonService',
    'services/filterConfigService',
    'services/utils'
], () => {
    Box.Application.addModule("agentPage", (context) => {

        const CommonService = context.getService('CommonService'),
            FilterConfigService = context.getService('FilterConfigService');
        var messages = [],
            behaviors = [],
            utils = context.getService('Utils');

        function _openLeadForm(dataset){
            let rawData = {
                cityId: dataset.cityid,
                localityId: dataset.localityid,
                companyId: dataset.companyid,
                companyName: dataset.companyname,
                companyPhone: dataset.companyphone,
                companyRating: dataset.companyrating,
                companyImage: dataset.companyimage,
                companyAssist: dataset.companyassist,
                companyUserId: dataset.companyuserid,
                step : dataset.step || "MAIN",
                companyType: 'broker',
                id: "lead-popup"
            };

            context.broadcast('Lead_Open_Step', rawData);
            _openPopup("leadOverviewPopup");
        }
        function _openPopup(id) {
            context.broadcast('popup:open', {
                id: id,
                options: {
                    config: {
                        modal: true
                    }
                }
            });
        }

        function init() {
            utils.setLastVisitedPageDetails(window.pageData); //for saving context of repeat users
            utils.updatePushNotificationTags('AGENT_PAGE');
        }

        function destroy() {
            //clear all the binding and objects
        }

        function onmessage() {
            // bind custom messages/events
        }

        function onclick(event, element, elementType) {
            // bind custom messages/events
                let dataset, url;

            switch(elementType){
                case "connect-now":
                     dataset = $(element).data() || {};
                    _openLeadForm(dataset);
                break;
                case "view-phone":
                    dataset = $(element).data() || {};
                    dataset.step="VIEW_PHONE_MORPHED";
                    _openLeadForm(dataset);
                break;
                case "navigate_buy":
                case "navigate":
                case "navigate_total":
                    dataset = $(element).data() || {};
                    if(dataset.url){
                        url = dataset.url;
                    } else {
                        url = FilterConfigService.getSellerListingUrl({
                            id: dataset.id,
                            name: dataset.name,
                            sellerUserId: dataset.sellerUserId,
                            type: 'AGENT'
                        }, false, {
                            listingType: 'buy'
                        });
                    }
                    CommonService.ajaxify(url);
                break;
                case "navigate_rent":
                    dataset = $(element).data() || {};
                    url = FilterConfigService.getSellerListingUrl({
                        id: dataset.id,
                        name: dataset.name,
                        type: 'AGENT'
                    }, false, {
                        listingType: 'rent'
                    });
                    CommonService.ajaxify(url);
                break;
                case "top-seo-dropdown-item":
                    dataset = $(element).data() || {};
                    if(dataset.url){
                        CommonService.ajaxify(dataset.url);
                    }
                break;
                case "buy_serp":
                    url = FilterConfigService.getUrlWithUpdatedParams({ overrideParams: { listingType: 'buy' } });
                    CommonService.ajaxify(url);
                    break;
                case "rent_serp":
                    url = FilterConfigService.getUrlWithUpdatedParams({ overrideParams: { listingType: 'rent' } });
                    CommonService.ajaxify(url);
                    break;
            }
        }
        return {
            init,
            messages,
            behaviors,
            onmessage,
            onclick,
            destroy
        };
    });
});