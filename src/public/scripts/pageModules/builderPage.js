"use strict";
define([
    'common/trackingConfigService',
    'services/commonService',
    'services/trackingService'
], (t, commonService, trackingService) => {
    Box.Application.addModule("builderPage", (context) => {     // jshint ignore: line

        var messages = [],
            behaviors = [];

        function init() {
            
        }

        function destroy() {
            //clear all the binding and objects
        }

        function onmessage() {
            // bind custom messages/events
        }

        function onclick(event, element, elementType) {
            // bind custom messages/events
            let url, properties = {};
            event = t.CLICK_EVENT;

            switch(elementType){
                case "card_navigate":

                    properties[t.CATEGORY_KEY] = t.BUILDER_CARD;
                    properties[t.SOURCE_MODULE_KEY] = t.BUILDER_MODULE;
                    trackingService.trackEvent(event, properties);

                    url = $(element).data('url') || {};
                    commonService.ajaxify(url);
                    break;

                case "topCard_navigate":
                    properties[t.CATEGORY_KEY] = t.BUILDER_TOPCARD;
                    properties[t.SOURCE_MODULE_KEY] = t.BUILDER_MODULE;
                    trackingService.trackEvent(event, properties);

                    url = $(element).data('url') || {};
                    commonService.ajaxify(url);
                    break;

                case "table_navigate":
                    properties[t.CATEGORY_KEY] = t.BUILDER_TABLE;
                    properties[t.SOURCE_MODULE_KEY] = t.BUILDER_MODULE;
                    trackingService.trackEvent(event, properties);

                    url = $(element).data('url') || {};
                    commonService.ajaxify(url);
                    break;
            }
        }

        return {
            init,
            messages,
            behaviors,
            onmessage,
            onclick,
            destroy
        };
    });
});