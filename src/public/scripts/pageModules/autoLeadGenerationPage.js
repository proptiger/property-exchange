"use strict";
define([
      
    ], function(){
    const ModuleName = "autoLeadGenerationPage";
    Box.Application.addModule(ModuleName, (context) => {
        let moduleEl, moduleConfig, $, $moduleEl;

        const selector = {
            "THANKYOU_MESSAGE": ".js-thankyou",
            "INTERESED_BUTTON": ".interestedButton",
        }

        return {
            init: function(){
                moduleEl = context.getElement();
                moduleConfig = context.getConfig();
                $ = context.getGlobal('jQuery');
                $moduleEl = $(moduleEl);
            },
            onclick: function(event, element, elementType) {
                switch(elementType) {
                    case 'userInterested':
                        $moduleEl.find(selector.THANKYOU_MESSAGE).removeClass('hide');
                        $moduleEl.find(selector.INTERESED_BUTTON).addClass('hide');
                        context.broadcast('userInterested');
                        break;
                }
            },
            destroy: function(){
                moduleEl = $ = $moduleEl = moduleConfig = null;
            }
        }
    });
});
