"use strict";
define([
    'services/commonService',
    'services/trackingService',
    'behaviors/pressReleasesTracking'
], () => {
    Box.Application.addModule("pressReleases", (context) => {

        //const CommonService = context.getService('CommonService'),
          //  logger = context.getService('Logger');
        var messages = [],
            behaviors = ['pressReleasesTracking'],
            query;

        function init() {
            //initialization of all local variables
            query = Box.DOM.query;
        }

        function destroy() {
            //clear all the binding and objects
        }

        function onmessage() {
            // bind custom messages/events
        }

        function onclick(event, element, elementType) {
            // bind custom messages/events
            let text, pageType = 'pressReleases';
            switch (elementType) {
                case 'header-news-bytes':
                    context.broadcast('trackNewsHeaderNewsBytesClick', { pageType: pageType });
                    break;
                case 'header-press-releases':
                    context.broadcast('trackNewsHeaderPressReleasesClick', { pageType: pageType });
                    break;
                case 'header-media-resources':
                    context.broadcast('trackNewsHeaderMediaResourcesClick', { pageType: pageType });
                    break;
                case 'more':
                    text = $(element).attr('data-value');
                    context.broadcast('trackNewsCardClick', { pageType: pageType, text: text });
                    break;
                case 'pagination':
                    context.broadcast('trackNewsPaginationClick', { pageType: pageType });
                    break;
                case 'post':
                case 'image':
                case 'title':
                    text = $(element.closest("[data-type=post]")).attr('data-value');
                    window.open("/news-bytes/" + text,"_self");
            }
        }
        return {
            init,
            messages,
            behaviors,
            onmessage,
            onclick,
            destroy
        };
    });
});
