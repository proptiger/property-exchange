"use strict";
define([
    'common/sharedConfig',
    'services/urlService',
    'common/trackingConfigService',
    'services/apiService',
    'services/experimentsService',
    'services/commonService',
    'services/filterConfigService',
    'services/viewStatus',
    'services/utils',
    'behaviors/projectTracking',
    'behaviors/requestPhotosTracking',
    'scripts/dependency/owlBundle'
], (sharedConfig, urlService, t, apiService, experimentsService) => {
    Box.Application.addModule("projectPage", (context) => {

        const PAGE_CATEGORY = "Buyer_Project";


        const commonService = context.getService('CommonService'),
            filterConfigService = context.getService('FilterConfigService'),
            UtilService = context.getService('Utils'),
            logger = context.getService('Logger'),
            query = Box.DOM.query,
            application = Box.Application,
            queryAllData = Box.DOM.queryAllData,
            viewStatusService = context.getService('viewStatus'),
            utils = context.getService('Utils'),
            ACTIVE = 'active',
            HIDE = 'hide',
            PYR_OPEN = 'floating-callback',
            BUCKET_HEAD = 'js-bucket-head',
            PROJECT_BUCKET = 'js-project-bucket',
            //ACCORDIAN = "js-accordian",
            //ACCORDIAN_SEC = "js-accordian-sec",
            //ACCORDIAN_HEAD = "js-accordian-head",
            SELLER_POPUP_BTN = "seller-popup-btn",
            TIMELINE_POINT = "contruction_timeline_point",
            CONDSTRUCTION_SEC_ID = "construction",
            ROTATE_ICON_SELECTOR = ".js-rotate-icon",
            CONFIGURATION_MOBILE_BHK_TABS_SELECTOR = "js-configuration-mobile-bhk-tabs",
            TOP_SELLER_LEAD_FORM_ID = "main_lead_form",
            // propertyTypeConfig = filterConfigService.getFilterModuleConfig("propertyType").getTemplateData({
            //     listingType: 'buy'
            // }),
            floorPlanGallerySelectorPattern = new RegExp("floorplan-gallery-[a-zA-z_]*-[a-zA-z_]*-[0-9]*-[0-9]*-[0-9]*"),
            CONFIGURATION_WRAP_SELECTOR = "configuration-wrap",
            OWL_CONFIG = sharedConfig.OWL_CAROUSEL_CONFIG;

        let lastScrollTop = 0;

        var messages = ['trackSelectAllGrid',
                'trackViewMoreGrid',
                'trackClosePopup',
                'trackPriceTrends',
                'openmultipleSellerLead',
                'nearByAmenityModuleClicked',
                'customDirectionModuleClicked',
                'renderChart',
                'nearByAmenityModuleInitiated',
                'tabClicked',
                'construction_slider_element_clicked',
                'popup:change',
                'construction_slider_next_clicked',
                'construction_slider_prev_clicked',
                'slider_clicked',
                'Lead_Flow_Completed',
                'configuration-window-buy-apartment_slider_element_viewed',
                'configuration-window-buy-villa_slider_element_viewed',
                'configuration-window-buy-plot_slider_element_viewed',
                'configuration-window-rent-apartment_slider_element_viewed',
                'configuration-window-rent-villa_slider_element_viewed',
                'configuration-window-rent-plot_slider_element_viewed',
                'tabs_click_track',
                'gallery_destroyed',
                'callConnect',
                'otpBackButtonClicked',
                'popup:opened',
                'leadPopupClosed',
                'searchSimilarClicked'
            ],
            behaviors = ['projectTracking', 'requestPhotosTracking'],
            moduleEl,
            // falseTrigger = true,
            galleryData,
            moduleConfig;
        const ELEMENT_TYPE_SOURCE = {
            "image-div": "tap",
            "more-img": "count"
        };

        function _loadSubmodules() {
            context.broadcast('loadModule', [{
                parent: 'mapsModule',
                name: 'communicatorModule'
            }, {
                parent: 'mapsModule',
                name: 'customDirectionModule'
            }, {
                parent: 'mapsModule',
                name: 'nearByAmenityModule'
            } 
            // ,{
            //     parent: 'mapsModule',
            //     name: 'mapSvgLegendsModule'
            // }
            ]);
        }

        function _updateKeyDetails() {
            let size = {};
            size.min = $('.js-sponsored-range').data('size-min');
            size.max = $('.js-sponsored-range').data('size-max');
            let listingCategory = $('.js-sponsored-range').data('listing-category');
            if (size.min && size.max) {
                if (size.min != size.max) {
                    size.formatted = UtilService.formatPriceWithComma(size.min) + ' - ' + UtilService.formatPriceWithComma(size.max) + ' sqft';
                } else {
                    size.formatted = UtilService.formatPriceWithComma(size.min) + ' sqft';
                }
                $('[data-item="size"] .val').text(size.formatted);
            }
            $('[data-val="pricePerSqft"]').text($('.js-sponsored-range').data('price-unit-area')).parent().removeClass('hidden');
            if (listingCategory) {
                $('[data-item="availability"] .val').text(listingCategory);
            }
        }

        function _loadImages(){
            commonService.bindOnLoadPromise().then(function() {
                $(moduleEl).find('[data-onloadimage]').addClass("onloadimage").removeAttr('data-onloadimage');

                if(UtilService.isMobileRequest()) {
                    //Load the carousel module with config
                    commonService.bindOnModuleLoad('carousel', () => {
                        let pageData = UtilService.getPageData();

                        context.broadcast("carousel-configData", {
                            id: pageData.projectId,
                            projectId: pageData.projectId,
                            imageCount: moduleConfig.imageCount,
                            builderId: pageData.builderId,
                            companyId: pageData.builderId,
                            projectStatus: moduleConfig.projectStatus,
                            price: moduleConfig.price,
                            propertyType: moduleConfig.propertyType,
                            bedrooms: moduleConfig.bedrooms,
                            localityId: pageData.localityId,
                            suburbId: pageData.suburbId,
                            cityId: pageData.cityId,
                            listingScore: moduleConfig.score,
                            listingCategory: moduleConfig.listingCategory,
                            defaultImageId: moduleConfig.defaultImageId,
                            mainImageWidth: moduleConfig.mainImageWidth,
                            mainImageHeight: moduleConfig.mainImageHeight,
                            gaCategory: "Gallery"
                        });
                    });
                }
            });
        }

        function init() {
            var isMobileRequest = UtilService.isMobileRequest();

            logger.info('in the project page module');
            moduleEl = context.getElement();
            moduleConfig = context.getConfig() || {};
            let projectId = moduleConfig.projectId,
                projectName = moduleConfig.projectName,
                builderName = moduleConfig.builderName,
                noPropertyListedElem = query(moduleEl, '[data-no-property-found]'),
                listingCategory = UtilService.getPageData("listingType"),
                noConfigurationSelector = "[dummy-configuration-only][data-domain='" + listingCategory + "'][data-unittype='apartment']",
                noConfigurationElem = query(moduleEl, noConfigurationSelector);

            $(moduleEl).find('.js-background-image').addClass('onloadimage');

            viewStatusService.setAsSeen({
                id: projectId,
                type: 'project',
                data: {
                    label: projectName,
                    builderName: builderName
                }
            });


            if (noConfigurationElem) {
                _trackZeroResultFound({
                    listingCategory: listingCategory && UtilService.toTitleCase(listingCategory)
                });
            }

            _loadImages();

            commonService.loadGoogleAds();

            commonService.bindOnModuleLoad('lead', function(id) {
                var urlParams = urlService.getAllUrlParam();
                if (!$(moduleEl).find("[data-lead-similar]").length) {
                    context.broadcast('lead_no_similar', {
                        "id": id
                    });
                }
                if(urlParams.event=="OpenLeadForm"){
                    _openLeadForm();
                }
                $('#owl-demo').owlCarousel(OWL_CONFIG);
            }, [TOP_SELLER_LEAD_FORM_ID, 'lead-popup']);

            if (!isMobileRequest) {
                commonService.bindOnLoadPromise().then(() => {
                    context.broadcast('loadModule', [{
                        name: 'chart'
                    }]);
                });
            }

            _updateKeyDetails();

            if (isMobileRequest) {
                _setSectionMobileWidth();

            }

            window.addEventListener('scroll', _scrollFunction,{passive:true}); //jshint ignore:line
            updateOtherSellersLink({ isSemPage: moduleConfig.isSemPage });
            $(window).trigger("scroll");

            if (noPropertyListedElem) {
                _trackZeroResultFound();
            }

            utils.setLastVisitedPageDetails(window.pageData); //for saving context of repeat users
            utils.updatePushNotificationTags('PROJECT_OVERVIEW');
        }

        function _setSectionMobileWidth() {
            let scrollingList = $(moduleEl).find('.js-mobscroll');
            for (let i = 0, length = scrollingList.length; i < length; i++) {
                let currentElement = $(scrollingList[i]),
                    elements = currentElement.find('.js-moblist-item'),
                    length = elements.length,
                    width = $(elements[0]).outerWidth(true);
                currentElement.width(width * length);
            }
        }

        function _openPopup(config) {
            context.broadcast('popup:open', {
                id: config.id,
                options: {
                    config: {
                        modal: config.modal
                    }
                }
            });
        }

        function _openPYR(id, params) {
            let pyrData = {
                    localityIds: [{
                        id: params.localityid,
                        name: params.localityname,
                        type: "locality"
                    }],
                    salesType: params.domain,
                    stage: "step1"
                },
                data;
            if (params.rangemin !== "dummy") {
                pyrData.budget = {
                    start: [params.rangemin, params.rangemax]
                };
            }
            if (params.bhk) {
                pyrData.bhk = params.bhk.toString().split(",");
            }
            data = {
                pyrData: pyrData,
                trackData: {
                    id: 'ProjectConfigurationShareRequirements',
                    [`${t.BUDGET_KEY}`]: params.rangemin + ',' + params.rangemax,
                    [`${t.LISTING_CATEGORY_KEY}`]: params.domain
                }
            };
            context.broadcast('openPyr', data);
            _openPopup({ id: id, modal: true });
        }

        function _openSellerPopup(source) {
            let gridContent = $(`#${source} .js-seller-grid`).detach(),
                popup = $('#seller-detail-popup');
            popup.data('sourceid', source);
            popup.append(gridContent);
            _openPopup({
                id: 'seller-detail-popup',
                modal: false
            });
        }

        function updateOtherSellersLink() {
            let sellerPopupBox = $(Box.DOM.query(moduleEl, "#all-seller-details")),
                sellerCount = sellerPopupBox.data('seller-count');
            //minPrice, maxPrice;
            if (sellerCount) {
                $('.js-seller-count-parent').removeClass("hide");
            }
        }


        // function _parsePriceTrend(response) {
        //     let result = {};
        //     result.series = [];
        //     result.id = 'chart-price-trend';
        //     for (let key in response) {
        //         if (response.hasOwnProperty(key)) {
        //             let res = [],
        //                 val = response[key];
        //             for (let index in val) {
        //                 res.push(val[index]);
        //             }
        //             result.series.push({
        //                 name: key,
        //                 data: res
        //             });
        //         }
        //     }
        //     return result;
        // }
        function callConnect() {
            $('#con').click();
            // context.broadcast('trackProjectLeadFormOpen', { action: "open",label:'projectDetails' });
            // _openLeadForm(dataset);
            // $('#leadOverviewPopup').show();
        }

        function trackTabEvent(data) {
            let target = data.element,
                source = data.prevElement;
            let trackData = {};
            trackData = $(target).data();
            trackData.source = $(source).data('tabName');
            if (UtilService.isMobileRequest() && trackData.tabName) {
                context.broadcast('trackTabToggleProject', trackData);
            }
        }

        function _handleTabEvents(data = {}) {
            let originalData = $.extend(true, data);
            data = data.element;
            if (!(data && data.getAttribute('data-target-id'))) {
                return;
            }
            trackTabEvent(originalData);
            var targetId = data.getAttribute('data-target-id');
            if (targetId === 'neighbourhoodContent') {
                context.broadcast('removeCustomDirectionModule');
                context.broadcast('loadNearByAmenityModule');
                context.broadcast('activateMasterPlan', false);
            } else if (targetId === 'masterPlanContent') {
                context.broadcast('activateMasterPlan', true);
                context.broadcast('removeAmenityModule');
                context.broadcast('removeCustomDirectionModule');
                context.broadcast('removeCommunicatorModule');
            } else if (targetId === 'commuteContent') {
                context.broadcast('activateMasterPlan', false);
                context.broadcast('removeAmenityModule');
                context.broadcast('loadCustomDirectionModule');
            }
        }

        //Duration of the top scrolling animation (in ms)
        let scroll_top_duration = 700;
        // scrolling function
        function targetedScroll(id) {
            // scrollTop is either the top offset of the element whose id is passed, or 0
            var scrollTop = id ? ($('#' + id).offset().top - 200) : 0;

            $('body,html').animate({
                scrollTop: scrollTop,
            }, scroll_top_duration);
        }

        function launchGallery(dataset, elementType){
            let variation = experimentsService.getExperimentVariation('makaan_gallery_revamp'),
                galleryData = {
                    id: "main_gallery_desktop",
                    startIndex: dataset.index
                },
                moduleName = "gallery", 
                moduleId = "main_gallery_desktop";

            if(UtilService.isMobileRequest()){
                switch(variation){
                    case 'gallery_new':
                        galleryData['version'] = "v2_mobile";
                    break;

                    case 'gallery_grid':
                        galleryData['id'] = "galleryGrid";
                        moduleName = moduleId = "galleryGrid";
                    break;
                }
            } 
            context.broadcast("trackProjectGalleryBucketClick", { bucket: dataset.bucket, sourceModule: dataset.sourcemodule, name: dataset.name, sourceElement: ELEMENT_TYPE_SOURCE[elementType] });

            if (!commonService.checkModuleLoaded(moduleName, moduleId)) {
                context.broadcast('loadModule', {
                    "name": moduleName,
                    "loadId": moduleId
                });
            }

            commonService.bindOnModuleLoad(moduleName, function() {
                context.broadcast('start_gallery', galleryData);
            }, [moduleId]);
        }

        function onclick(event, clickElement, elementType) {
            let dataset = $(clickElement).data() || {},
                rawData = {},
                sellerIds = [],
                sellerScore = [];
            rawData.id = "lead-popup";
            let clickElementData;
            switch (elementType) {
                case 'redirectUrl':
                    if(dataset.url){
                        urlService.ajaxyUrlChange(dataset.url);
                    }
                    break;
                case "alpClose":
                    $(clickElement).closest(`[data-type=availablePropPopup]`).addClass('hide');
                    context.broadcast("trackAlpCloseClick", dataset);
                    break;
                case "alpBuyLink":
                    context.broadcast("trackAlpBuyLinkClick", dataset);
                    break;
                case "alpRentLink":
                    context.broadcast("trackAlpRentLinkClick", dataset);
                    break;

                case "stickyNavBuyLink":
                    context.broadcast("trackStickyNavBarBuyLinkClick", dataset);
                    break;
                case "stickyNavRentLink":
                    context.broadcast("trackStickyNavBarRentLinkClick", dataset);
                    break;

                case "scrollto":
                    // bind smooth scroll to any anchor on the page
                    event.preventDefault();
                    targetedScroll($(clickElement).data('goto'));
                    context.broadcast("trackScrollOnClick", dataset);
                    break;
                case 'openTopSellerLead':
                    context.broadcast('trackOpenTopSellerLeadProject', dataset.seller);
                    let _dataset = $.extend(true, {}, dataset);
                    _dataset = $.extend(true, _dataset, dataset.seller);
                    _openSingleSellerLeadForm(_dataset);
                    break;

                case 'showSingleSellerLeadForm':
                    context.broadcast('trackOpenAvailableListingLeadProject', dataset.listing);
                    _dataset = $.extend(true, {}, dataset);
                    _dataset = $.extend(true, _dataset, dataset.listing);
                    _openSingleSellerLeadForm(_dataset);
                    break;
                case 'view-on-map':
                    context.broadcast('viewOnMap', dataset);
                    context.broadcast('trackViewOnMapProject');
                    break;
                case 'breadcrum':
                    if (dataset.trackLabel) {
                        UtilService.trackEvent('CLICK_Property_You are Here', PAGE_CATEGORY, dataset.trackLabel);
                    }
                    break;
                case "proj-link":
                    console.log(dataset);
                    context.broadcast('trackSimilarProjectLinkClick', dataset);
                    break;

                case BUCKET_HEAD:
                    $(clickElement).closest(`[data-type=${PROJECT_BUCKET}]`).toggleClass(ACTIVE);
                    if ($(clickElement).closest(`[data-type=${PROJECT_BUCKET}]`).hasClass(ACTIVE)) {
                        context.broadcast('project_bucket_click', {
                            budget: dataset.budget,
                            bhk: dataset.bhk,
                            unitType: dataset.unitType
                        });
                    }
                    break;

                case SELLER_POPUP_BTN:
                    let source = dataset.openid,
                        data = dataset;
                    _openSellerPopup(source);
                    if (data.trackType === 'viewOtherSellers') {
                        context.broadcast('trackViewOtherSellersProject', {});
                    } else {
                        context.broadcast('project_showSeller', { sellercount: data.sellercount, budget: data.budget, unitType: data.unitType });
                    }
                    break;
                case "image-div":
                case "more-img":
                case "open-desktop-gallery":
                    launchGallery(dataset, elementType);
                    break;
                case TIMELINE_POINT:
                    $(`[data-type="${TIMELINE_POINT}"].${ACTIVE}`).removeClass(ACTIVE);
                    $(clickElement).addClass(ACTIVE);
                    _slideTO(dataset.count, CONDSTRUCTION_SEC_ID);
                    break;
                case "configuration-bhk-tabs":
                    let bhkIndex = dataset.index,
                        selectorAppend = dataset.uniqueselector;
                    $(moduleEl).find('.' + CONFIGURATION_MOBILE_BHK_TABS_SELECTOR + '-' + selectorAppend + ' ul li').each((index, element) => {
                        if (element.getAttribute("data-index") == bhkIndex) {
                            $(element).addClass("selected");
                        } else {
                            $(element).removeClass("selected");
                        }
                    });
                    $("#configuration-window-" + selectorAppend).find("li").not("li li").each((index, element) => {
                        if (element.getAttribute("data-selector") === (selectorAppend + "-" + bhkIndex)) {
                            _slideTO(index + 1, "configuration-window-" + selectorAppend);
                            return false;
                        }
                    });
                    let bhk = dataset && dataset.bhk,
                        unitType = dataset && dataset.unittype,
                        listingCategory = dataset && dataset.domain;
                    context.broadcast("bhk_tab_clicked", { bhk, unitType, listingCategory });
                    break;
                case 'builder-facts-item':
                    if (dataset.trackValue) {
                        context.broadcast('trackBuilderLisitngsClickProject', {
                            value: dataset.trackValue,
                            label: dataset.trackLabel
                        });
                    }
                    let url = dataset.url;
                    if (url) {
                        urlService.ajaxyUrlChange(url);
                    }
                    break;

                case 'top-button':
                    UtilService.gotoEl('body', 0, 0);
                    break;

                case 'pyr-button':
                    context.broadcast('loadModule', {
                        "name": 'pyr',
                    });
                    commonService.bindOnModuleLoad('pyr', function() {
                        _openPYR('pyrPopup', dataset);
                    });
                    break;

                case "score-ring-popup":
                    let popupid = dataset.openid;
                    _openPopup({
                        id: popupid
                    });
                    if (dataset.trackType === 'Project') {
                        context.broadcast('trackProjectScoreProject');
                    }
                    break;

                    /* <tracking> */
                case 'navigate':
                    context.broadcast('trackStickyHeaderClickProject', { label: dataset.trackLabel });
                    break;

                case 'track-locality-detail':
                    context.broadcast('trackLocalityDetailClickProject', {
                        value: dataset.trackValue
                    });
                    break;

                case 'track-city-detail':
                    context.broadcast('trackCityDetailClickProject', {
                        value: dataset.trackValue
                    });
                    break;

                case 'track-builder-detail':
                    context.broadcast('trackBuilderDetailClickProject', {
                        value: dataset.trackValue
                    });
                    break;

                    /* </tracking>  */
                case 'saveSearch':
                    commonService.bindOnModuleLoad('saveSearch', function() {
                        context.broadcast('openSaveSearch', { trackId: 'projectPageSetAlert' });
                        _openPopup({ id: 'saveSearchPopup', modal: true });
                    });
                    break;
                case "viewProperties":
                    //trackActionLabel(dataset);
                    let redirectUri = $(clickElement).data('redirecturi');
                    dataset = clickElement.dataset;
                    urlService.ajaxyUrlChange(redirectUri);
                    context.broadcast('project_viewProperty', { properyCount: dataset.propertycount, budget: dataset.budget, unitType: dataset.unitType, linkName: dataset.linkName, linkType: dataset.linkType });
                    break;
                case 'dummy-img':
                case 'openLeadForm':
                    _openTwoStepPyrForm(dataset);
                    break;

                case 'view_all_property':
                    context.broadcast('project_configuration_viewAll', dataset);
                    break;
                case 'social-share':
                    context.broadcast('open_share_box', { page: window.pageData.trackPageType });
                    break;
                case 'open-floor-plan-in-gallery':
                    let galleryId = dataset.openid;

                    if (!commonService.checkModuleLoaded('gallery', galleryId)) {
                        context.broadcast('loadModule', {
                            "name": 'gallery',
                            "loadId": galleryId
                        });
                    }
                    commonService.bindOnModuleLoad('gallery', function() {
                        context.broadcast('start_gallery', {
                            "id": galleryId
                        });

                        $("." + CONFIGURATION_WRAP_SELECTOR).css({ 'z-index': 11 });
                        if (UtilService.isMobileRequest()) {
                            $(moduleEl).find(".configuration-window-wrap").css({ 'overflow-x': 'hidden' });
                            $(moduleEl).find(".configuration-window-wrap ul").css({ 'transform': 'none' });
                        }
                    }, [galleryId]);

                    break;
                case "more-price-bucket":
                    $(ROTATE_ICON_SELECTOR).toggleClass("rotate");
                    break;

                case PYR_OPEN:
                    rawData = _openLeadForm(dataset);
                    sellerIds = [];
                    sellerScore = [];
                    sellerIds.push(rawData.companyId);
                    sellerScore.push(rawData.companyRating);
                    $.isArray(rawData.otherCompany) && rawData.otherCompany.map(otherCompany => {
                        sellerIds.push(otherCompany.companyId);
                        sellerScore.push(otherCompany.companyRating);
                        return;
                    }); //jshint ignore:line
                    context.broadcast('trackOpenFloatingLeadProject', { sellerIds, sellerScore });
                    break;

                case "mobile-connect-now":
                    rawData = _openLeadForm(dataset);
                    sellerIds = [];
                    sellerScore = [];
                    sellerIds.push(rawData.companyId);
                    sellerScore.push(rawData.companyRating);
                    $.isArray(rawData.otherCompany) && rawData.otherCompany.map(otherCompany => {
                        sellerIds.push(otherCompany.companyId);
                        sellerScore.push(otherCompany.companyRating);
                        return;
                    }); //jshint ignore:line
                    context.broadcast('trackOpenMultipleSellerLeadProject', { sellerScore, sellerIds });
                    break;

                case "connect-now":
                    var label = '';
                    let __aElem = $(clickElement);
                    clickElementData = $(clickElement).data();
                    if (__aElem && __aElem[0] && __aElem[0].className) {
                        let _label = __aElem[0].className;
                        if (_label.indexOf('projectDetails') >= 0) {
                            label = 'projectDetails';
                        } else if (_label.indexOf('amenities') >= 0) {
                            label = 'amenities';
                        } else if (_label.indexOf('js-request-banner') >= 0) {
                            label = 'js-request-banner';
                        } else if (_label.indexOf('construction') >= 0) {
                            label = 'construction';
                        } else if (_label.indexOf('gallery') >= 0 || _label.indexOf('request photos') >= 0) {
                            label = 'gallery';
                        } else if (_label.indexOf('headline') >= 0) {
                            label = 'Headline';
                        }
                    }
                    if (label) {
                        context.broadcast('trackProjectLeadFormOpen', { action: "open", label, sourceModule: clickElementData.source });
                    }
                    _openLeadForm(dataset);
                    break;
                case 'request-price':
                    clickElementData = $(clickElement).data();
                    bhk = clickElementData && clickElementData.bhk;
                    unitType = clickElementData && clickElementData.unitType;
                    let domain = clickElementData && clickElementData.domain;
                    listingCategory = "Primary,Resale";
                    let projectId = clickElementData && clickElementData.projectid,
                        query = {},
                        cityId = UtilService.getPageData("cityId");

                    if (domain === 'rent') {
                        listingCategory = "Rental";
                    }

                    query = {
                        listingCategory,
                        unitType,
                        bhk,
                        projectId
                    };

                    url = sharedConfig.apiHandlers.getCityTopSellers({ cityId, query }).url;
                    commonService.bindOnLoadPromise().then(() => {
                        return apiService.get(url).then(function(response) {
                            if (response && response.length) {
                                rawData = _preparePriceRequestLeadData(response, clickElementData);
                                if (!rawData) {
                                    _openSaveSearch("PriceRequest", domain);
                                    return;
                                }
                                context.broadcast('Lead_Open_Step', rawData);
                                context.broadcast('trackProjectLeadFormOpen', { action: "open", sourceModule: "PriceRequest" });
                                _openPopup({
                                    id: 'leadOverviewPopup',
                                    modal: true
                                });
                            } else {
                                _openSaveSearch("PriceRequest", domain);
                            }
                        });
                    });

                    break;
                case 'searchSimilar':
                    redirectToSerp();
                    break;
            }
        }

        function redirectToSerp(){
            let urlparams = {};
            let projectPageData = UtilService.getPageData()||{};
            urlparams.cityId = projectPageData.cityId;
            urlparams.cityName = projectPageData.cityName;
            urlparams.listingType = projectPageData.listingType;
            urlparams.localityId = projectPageData.localityId;
            if(moduleConfig.minPrice || moduleConfig.maxPrice){
                urlparams.budget = moduleConfig.minPrice ? `${moduleConfig.minPrice},${moduleConfig.maxPrice}`:`${moduleConfig.maxPrice}`;
            }
            urlparams.localityName = projectPageData.localityName || '';
            let url =  filterConfigService.getUrlWithUpdatedParams({skipFilterData: true, overrideParams: urlparams});
            window.location.href = url;
        }

        function _preparePriceRequestLeadData(sellerResponse, clickElementData) {
            let companyIds = [],
                rawData;
            for (let key in sellerResponse) {
                companyIds.push({ companyId: sellerResponse[key].id });
            }
            companyIds.shift();
            if (companyIds.length) {
                companyIds = companyIds.splice(0, 4);
            }
            rawData = getLeadData();
            if (!rawData) {
                return rawData;
            }
            rawData.companyId = sellerResponse[0].id;
            rawData.companyName = sellerResponse[0].name;
            rawData.companyPhone = sellerResponse[0].contact||'';
            rawData.companyUserId = sellerResponse[0].userId;
            rawData.configurationText = {
                text: clickElementData && clickElementData.text,
                heading: "get price details",
                footer: "request price"
            };
            rawData.salesType = clickElementData.domain;
            rawData.otherCompany = companyIds;
            rawData.source = "PriceRequest";

            return rawData;
        }

        function _openSaveSearch(trackId, listingCategory) {
            trackId = trackId || 'projectPageSetAlert';
            commonService.bindOnModuleLoad('saveSearch', function() {
                context.broadcast('openSaveSearch', {
                    trackId,
                    listingCategory
                });
                _openPopup({
                    id: 'saveSearchPopup',
                    modal: true
                });
            });
        }

        function getLeadData(dataset = {}) {

            let __leadFormElem = query(moduleEl, "#" + TOP_SELLER_LEAD_FORM_ID),
                rawData;
            if (__leadFormElem) {
                if(!UtilService.isMobileRequest()){
                    application.stop(__leadFormElem);
                    application.start(__leadFormElem);
                }
                rawData = application.getModuleConfig(__leadFormElem);
                rawData.id = "lead-popup";
                rawData.moduleId = moduleEl.id;
                delete dataset.id;
                rawData = $.extend(rawData, dataset);
            }
            rawData.imageCount = moduleConfig.imageCount;

            return rawData;
        }

        function _openTwoStepPyrForm(dataset = {}) {
            let seller = {
                    id: dataset.sellerid || dataset.sellerId,
                    userId: dataset.selleruserid || dataset.sellerUserId,
                    name: dataset.sellername || dataset.sellerName,
                    phone: dataset.sellerphone || dataset.sellerContactNumber,
                    rating: dataset.sellerrating || dataset.sellerRating,
                    image: dataset.sellerimage || dataset.sellerImage,
                    assist: dataset.sellerassist || dataset.sellerAssist
                },
                data;
            seller = dataset.otherCompany || [seller];
            data = {
                id: 'twosteppyr-lead',
                type: "GET_CALLBACK",
                isPopup: true,
                otherCompany: seller,
                cityId: dataset.cityid,
                salesType: dataset.listingcategory || dataset.listingCategory,
                listingId: dataset.listingid || dataset.listingId,
                localityId: dataset.localityid || dataset.localityId,
                projectId: moduleConfig.projectId,
                sourceSection: dataset.source,
                trackOpen: true
            };
            context.broadcast('openTwoStepPyr', data);
            _openPopup({ id: 'twosteppyr-popup', modal: true });
        }

        function _openLeadForm(dataset = {}) {
            let rawData = getLeadData(dataset);
            if (!rawData) {
                _openSaveSearch("", UtilService.getPageData("listingType"));
                return;
            }
            context.broadcast('leadform:open', rawData);
            return rawData;
        }

        function _openSingleSellerLeadForm(dataset) {
            let rawData = {
                id: "lead-popup",
                moduleId: moduleEl.id
            };
            delete dataset.id;
            rawData = $.extend(rawData, dataset);
            if (rawData.unitType) {
                rawData.propertyTypeLabel = rawData.unitType;
            }
            rawData.projectId = UtilService.getPageData("projectId");
            if (rawData.unitTypeId) {
                rawData.propertyType = [rawData.unitTypeId];
            }
            rawData.salesType = rawData.listingCategory;
            context.broadcast('leadform:open', rawData);
        }

        function destroy() {
            //clear all the binding and objects
            window.removeEventListener('scroll', _scrollFunction,{passive:true}); //jshint ignore:line
            $('.header').removeClass(HIDE);
        }

        function _trackZeroResultFound(params = {}) {
            let data = {
                nonInteraction: 1
            };
            data = $.extend(true, data, params);
            context.broadcast("zeroResultFound", data);
        }

        function onmessage(name, data) {
            let configurationWindowSliderMessages = ['configuration-window-buy-apartment_slider_element_viewed', 'configuration-window-buy-villa_slider_element_viewed', 'configuration-window-buy-plot_slider_element_viewed', 'configuration-window-rent-apartment_slider_element_viewed', 'configuration-window-rent-villa_slider_element_viewed', 'configuration-window-rent-plot_slider_element_viewed'];
            if (name === 'nearByAmenityModuleInitiated') {
                Box.DOM.query(context.getElement(), "[data-target-id=neighbourhoodContent]").click();
            } else if (name === 'tabClicked') {
                _handleTabEvents(data);
            } else if (name === "construction_slider_element_clicked") {

                if (!commonService.checkModuleLoaded('gallery', 'construction_gallery')) {
                    context.broadcast('loadModule', {
                        "name": 'gallery',
                        "loadId": 'construction_gallery'
                    });
                }

                commonService.bindOnModuleLoad('gallery', function() {
                    context.broadcast('start_gallery', {
                        "id": 'construction_gallery'
                    });
                }, ["construction_gallery"]);

            } else if (name === "construction_slider_next_clicked") {
                let nextThumbnail = Box.DOM.queryData(moduleEl, 'count', data.index);
                if (nextThumbnail) {
                    $(moduleEl).find('[data-count]').removeClass(ACTIVE);
                    $(nextThumbnail).addClass(ACTIVE);
                }
            } else if (name === "construction_slider_prev_clicked") {
                let nextThumbnail = Box.DOM.queryData(moduleEl, 'nextcount', data.index);
                if (nextThumbnail) {
                    $(moduleEl).find('[data-count]').removeClass(ACTIVE);
                    $(nextThumbnail).addClass(ACTIVE);
                }
            } else if (name == "popup:change") {
                if (data && data.el && data.el.id == "seller-detail-popup") {
                    let sourceid = $(data.el).data('sourceid'),
                        content = $(data.el).find('.js-seller-grid').detach();
                    $(`#${sourceid}`).html(content);
                }
            } else if (name == 'openmultipleSellerLead') {
                let moduleElData = data.moduleEl && $(data.moduleEl).data();
                let _data = {
                    otherCompany: data.sellerIdArray,
                    projectId: moduleConfig.projectId,
                    listingCategory: moduleElData.listingCategory
                };
                _openTwoStepPyrForm(_data);
            } else if (name == 'slider_clicked' && data.element.id == 'similar-property') {
                context.broadcast('project_similarSlider', { event: data.event });
            } else if (name === "Lead_Flow_Completed") {
                if (data.id === TOP_SELLER_LEAD_FORM_ID) {
                    let similar = $(moduleEl).find("[data-lead-similar]");
                    $(moduleEl).find("[data-lead-container]").html(similar.html()).removeClass('hide');
                    similar.html('');
                }
            } else if (configurationWindowSliderMessages.indexOf(name) >= 0) {
                let index = data.data;
                if (index) {
                    let bhkIndex = index.split("-").pop(),
                        selectorAppend = index.split("-").slice(0, -1).join("-");
                    $(moduleEl).find('.' + CONFIGURATION_MOBILE_BHK_TABS_SELECTOR + '-' + selectorAppend + ' ul li').each((index, element) => {
                        if (element.getAttribute("data-index") == bhkIndex) {
                            $(element).addClass("selected");
                        } else {
                            $(element).removeClass("selected");
                        }
                    });
                }
            } else if (name === 'tabs_click_track') {
                let moduleElId = data.moduleEl.id,
                    element = data.element;
                if (_generateNamesForDomainAndUnitType("price-range-tabs").indexOf(moduleElId) >= 0) {
                    let _data = element && $(element).data(),
                        budget = _data && _data.budget,
                        unitType = _data && _data.unittype,
                        listingCategory = _data && _data.domain;
                    context.broadcast("budget_tab_clicked", { budget, listingCategory, unitType });
                } else if (_generateNamesForDomainAndUnitType("bhk-size-tabs", element.getAttribute("data-rangebucket")).indexOf(moduleElId) >= 0) {
                    let _data = element && $(element).data(),
                        bhk = _data && _data.bhk,
                        sqft = _data && _data.sqft,
                        unitType = _data && _data.unittype,
                        listingCategory = _data && _data.domain;
                    context.broadcast("bhk_sqft_tab_clicked", { bhk, sqft, listingCategory, unitType });
                } else if (["tab-buy-rent"].indexOf(moduleElId) >= 0) {
                    let listingCategory = element.getAttribute("data-domain"),
                        noConfigurationSelector = "[dummy-configuration-only][data-domain='" + listingCategory + "']",
                        noConfigurationElem = query(moduleEl, noConfigurationSelector);
                    if (noConfigurationElem) {
                        _trackZeroResultFound({
                            listingCategory: listingCategory && UtilService.toTitleCase(listingCategory)
                        });
                    }
                    context.broadcast("project_configTabClick", { listingCategory });
                } else if (["tab-buy-property-type", "tab-rent-property-type"].indexOf(moduleElId) >= 0) {
                    let _data = element && $(element).data(),
                        unitType = (_data && _data.propertytype) || "",
                        listingCategory = _data && _data.domain,
                        noConfigurationSelector = "[dummy-configuration-only]",
                        noConfigurationElem = query(moduleEl, noConfigurationSelector),
                        noPropertyListedElem = query(moduleEl, '[data-no-' + listingCategory + '-' + unitType + '-found]');
                    noConfigurationSelector = "[dummy-configuration-only][data-domain='" + listingCategory + "'][data-unittype='" + unitType.toLowerCase() + "']";
                    noConfigurationElem = query(moduleEl, noConfigurationSelector);
                    if (noPropertyListedElem || noConfigurationElem) {
                        _trackZeroResultFound({
                            listingCategory: listingCategory && UtilService.toTitleCase(listingCategory),
                            unitType
                        });
                    }
                    context.broadcast("property_type_tab_clicked", { unitType, listingCategory });

                }
            } else if (name === "gallery_destroyed") {
                if (!UtilService.isMobileRequest()) {
                    $("." + CONFIGURATION_WRAP_SELECTOR).css({ 'z-index': 9 });
                } else {
                    $(moduleEl).find(".configuration-window-wrap").css({ 'overflow-x': 'auto' });
                    $("." + CONFIGURATION_WRAP_SELECTOR).css({ 'z-index': '' });
                }
            } else if (name === "callConnect") {
                callConnect({type:'connect-now'});
                context.broadcast('trackProjectLeadFormOpen',{label:'Footer'});
            }else if(name === "otpBackButtonClicked"){
                $('#owl-demo').owlCarousel(OWL_CONFIG);
                $('#owl-demo1').owlCarousel(OWL_CONFIG);
            }else if(name ==="popup:opened"){
                if(data && data.id=="leadOverviewPopup")
                    $('#owl-demo1').owlCarousel(OWL_CONFIG);
            }else if(name==="leadPopupClosed"){
                if(data && data.id=="lead-popup"){
                    $('#owl-demo').owlCarousel(OWL_CONFIG);
                }
            } else if(name === "searchSimilarClicked") {
                redirectToSerp();
            }
        }


        var sticky = $('[data-sidebar-fixed-container]'),
            footer = $('[data-page-footer]'),
            //topFold = $('[data-top-fold]'),
            navigation = $('[data-type="navigation"]'),
            // navigationOffset = navigation.offset() && $('[data-type="navigation"]').offset().top,
            stickyTop = (sticky.length && sticky.offset().top),
            galleryContainerHeight = $('[data-top-area-wrap]').offset().top + $('[data-top-area-wrap]').outerHeight(true);


        var isMapModuleCalled, userScrolled;

        function _scrollFunction() {
            sticky = sticky || $('[data-sidebar-fixed-container]');
            let scrollTop = $(window).scrollTop(),
                stickyHeight = sticky.outerHeight(),
                footTop = footer.offset().top,
                topFoldHeight = galleryContainerHeight, //topFold.outerHeight(true),
                headerHeight = $('[data-header]').outerHeight(true),
                carousel = UtilService.isMobileRequest() && $(moduleEl).find(".imgWrap").length ? $(moduleEl).find(".imgWrap") : $(queryAllData(moduleEl, "container", "carousel")),
                fixedHeaderEl = $(query(moduleEl, "#fixedDataHeader")),
                fixedHeaderElHeight = fixedHeaderEl.outerHeight(true),
                boundary = carousel.offset().top + carousel.outerHeight(true),
                //boundaryNavigation = navigationOffset && (navigationOffset - fixedHeaderElHeight - headerHeight),
                navigationHeight = navigation.outerHeight(true),
                //StickyFooterShowHeight = sticky.offset() && sticky.offset().top,
                bottomGreyPaneOffset = $('[data-type=bottomGreyPane]').offset() && $('[data-type=bottomGreyPane]').offset().top,
                bottomGreyPaneHeight = $('[data-type=bottomGreyPane]').outerHeight(true),
                stickyFooter = $('[data-type=sticky-footer]');
                stickyFooter.removeClass('posfix');

            //windowHeight = $(window).height(),
            //isMobileRequest = UtilService.isMobileRequest();

            if (!isMapModuleCalled && !UtilService.isMobileRequest() && UtilService.checkInViewPort('.js-map-section')) {
                isMapModuleCalled = true;
                commonService.bindOnLoadPromise().then(() => {
                    context.broadcast('loadModule', {
                        "name": 'mapsModule'
                    });
                });
                commonService.bindOnModuleLoad('mapsModule', _loadSubmodules);
            }

            if (scrollTop > boundary) {
                fixedHeaderEl.addClass('posfixed').removeClass(HIDE);
                if (scrollTop > lastScrollTop) {
                    $('.header').addClass(HIDE); //jshint ignore:line
                    fixedHeaderEl.removeClass('withheader');
                    sticky.removeClass('withheader');
                } else {
                    $('.header').removeClass(HIDE); //jshint ignore:line
                    fixedHeaderEl.addClass('withheader');
                    sticky.addClass('withheader');
                }

                if(!userScrolled){
                    userScrolled = true;
                    context.broadcast('trackInteractiveUserScroll');
                }

            } else {
                fixedHeaderEl.removeClass('posfixed').addClass(HIDE);
            }

            if (sticky.length > 0) {
                if (scrollTop > boundary) {
                    fixedHeaderElHeight = fixedHeaderElHeight || fixedHeaderEl.outerHeight(true);
                }
            }

            let absolutePosition = footTop - stickyHeight - topFoldHeight - fixedHeaderElHeight - headerHeight - navigationHeight - $('[data-fixed-lead-stop-bottom]').outerHeight(true);

            if (scrollTop < stickyTop - headerHeight - fixedHeaderElHeight - navigationHeight) {
                sticky.removeClass('posfix');
            } else {
                if (scrollTop > absolutePosition + topFoldHeight) {
                    sticky.css({ position: "absolute", top: (absolutePosition + fixedHeaderElHeight + navigationHeight) + 'px', transition: '0s' }); // removing +headerHeight beacuse of scrollup-down function
                    sticky.removeClass('posfix');
                } else {
                    sticky.removeAttr('style');
                    sticky.addClass('posfix');
                }
            }


            if (scrollTop > absolutePosition + topFoldHeight) {
                stickyFooter.addClass('posfix');
            }
            if(scrollTop > bottomGreyPaneOffset - fixedHeaderElHeight - headerHeight - navigationHeight - bottomGreyPaneHeight) {
                stickyFooter.removeClass('posfix');
            }

            lastScrollTop = scrollTop; //rememeber this for next scroll event
        }

        function _slideTO(index, moduleId) {
            context.broadcast("slideTo", {
                id: moduleId,
                index: parseInt(index)
            });

        }

        function _generateNamesForDomainAndUnitType(prefix = '', suffix = '') {
            if (suffix) {
                suffix = '-' + suffix;
            }
            return [`${prefix}-buy-apartment${suffix}`,
                `${prefix}-buy-villa${suffix}`,
                `${prefix}-buy-plot${suffix}`,
                `${prefix}-rent-apartment${suffix}`,
                `${prefix}-rent-villa${suffix}`,
                `${prefix}-rent-plot${suffix}`
            ];
        }

        // function onmouseover(event, clickElement, elementType){
        //     switch(elementType){
        //         case 'score-ring-popup':
        //             let label = $(clickElement).data('trackLabel');
        //             if(label){
        //                 UtilService.trackEvent('Hover_Project Info', PAGE_CATEGORY, label);
        //             }
        //             break;
        //     }
        // }
        return {
            init,
            messages,
            behaviors,
            onmessage,
            onclick,
            destroy
        };
    });
});
