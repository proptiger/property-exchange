/*
 * Description: service to get the view status of a given project
 * status can be ['new', 'seen']
 * @author [Aditya Jha]
 * the data is stored in json format in localStorage
 * "MAKAAN_SEEN_LISTINGS": {"type":[{"id":"xxxx","time":"234562345","data":"any data"}]}
 */

'use strict';

define([
    'services/localStorageService',
    'services/loggerService'
], (localStorageService, logger) => {
    const SERVICE_NAME = 'viewStatus';

    Box.Application.addService(SERVICE_NAME, () => {
        const KEY = 'MAKAAN_RECENTS';
        const SECONDS_DIFF = 172800; // 2-days

        var _binarySearch = function(data, param) {
            let start = 0,
                end = data.length - 1,
                mid;
            while (start <= end) {
                mid = start + (end - start) / 2;
                mid = Math.floor(mid);
                if (data[mid].id == param.id) {
                    return mid;
                } else if (data[mid].id < param.id) {
                    start = mid + 1;
                } else {
                    end = mid - 1;
                }
            }
            return -1;
        };

        var _compareRecentlyViewed = function(a, b) {
            return b.time - a.time;
        };

        var _isSeen = function(param) {
            let seenItems = localStorageService.getItem(KEY);
            if (seenItems && seenItems.hasOwnProperty(param.type)) {
                let index = _binarySearch(seenItems[param.type], param);
                if(index !== -1) {
                    return true;
                }
            }
            return undefined;
        };

        var _isNew = function(param) {
            let diff = (param.serverTime - param.time) / 1000;
            if (diff <= SECONDS_DIFF) {
                return true;
            }
            return undefined;
        };

        var _isContacted = function(param){
            let allContactedListings = localStorageService.getSessionItem('contacted') || {};
            return allContactedListings.ids && allContactedListings.ids.indexOf(param.id) !== -1 ? true : false;
        };

        /*
         * param: object with keys time, id, type
         */
        function getStatus(param) {
            if(_isContacted(param)){
                return "contacted";
            }else if (_isSeen(param)) {
                return "seen";
            } else if (_isNew(param)) {
                return "new";
            }
            return undefined;
        }

        /*
         * param: object with keys id, type
         */
        function setAsSeen(param) {
            if (!param.id || !param.type) {
                logger.error('parameters missing for set as seen');
                return;
            }
            let seenItems = localStorageService.getItem(KEY);
            if (seenItems && seenItems.hasOwnProperty(param.type) && _binarySearch(seenItems[param.type], param) === -1) {
                // the type is present but the id is not
                seenItems[param.type].push({
                    id: param.id,
                    type: param.type,
                    time: (new Date().valueOf()),
                    data: param.data,
                });
                seenItems[param.type].sort(function(a, b) {
                    return a.id - b.id;
                });
            } else if (!seenItems || (seenItems && !seenItems.hasOwnProperty(param.type))) {
                if(!seenItems) {
                    seenItems= {};
                }
                seenItems[param.type] = [];
                seenItems[param.type].push({
                    id: param.id,
                    type: param.type,
                    time: (new Date().valueOf()),
                    data: param.data,
                });
            } else {
                // for else case, the type is present and the id is also present
                let index = _binarySearch(seenItems[param.type], param);
                seenItems[param.type][index] = {
                    id: param.id,
                    type: param.type,
                    time: (new Date().valueOf()),
                    data: param.data,
                };
            }
            localStorageService.setItem(KEY, seenItems);
        }

        /*
         *   returns an array of seen items of a given type
         *   param: {type, numberOfItems}
         *   if no item is present return an empty array
         */
        function recentlyViewed(param) {
            let seenItems = localStorageService.getItem(KEY);
            if(seenItems && !param.type){
                let projects = seenItems['project'] || [],
                    listings = seenItems['listing'] || [];
                $.merge(projects, listings);
                projects = projects.sort(function(a, b){
                    return b.time - a.time;
                });
                if(param.numberOfItems){
                    return projects.slice(0, param.numberOfItems);
                } else {
                    return projects;
                }
            }
            if (!seenItems || !seenItems.hasOwnProperty(param.type)) {
                // no item present, return an empty array
                return [];
            }
            // we have items that are seen for given type
            seenItems[param.type].sort(_compareRecentlyViewed);
            if(!param.numberOfItems) {
                return seenItems[param.type];
            } else {
                return seenItems[param.type].slice(0, param.numberOfItems);
            }
        }

        /*
         *   Exposed functions
         *   @getStatus: returns where a 'type' is new or seenItems
         *   @setAsSeen: sets a project as seen
         *   @recentlyViewed: returns an array of seen items of a given type
         */
        return {
            getStatus,
            setAsSeen,
            recentlyViewed
        };
    });

    return Box.Application.getService(SERVICE_NAME);
});
