"use strict";
define([
    'common/sharedConfig',
    'services/utils',
    'services/apiService',
    'services/urlService',
    'services/commonService',
], (sharedConfig, utilService, apiService, urlService, commonService) => {
    const SERVICE_NAME = 'DefaultService';

    Box.Application.addService(SERVICE_NAME, () => {
        function getCountries() {
            var _this = this;
            if (!_this.countries) {
                _this.countries = commonService.bindOnLoadPromise().then(() => {
                    return apiService.get(sharedConfig.clientApis.getCountry().url).then(function(response) {
                            if (response && response.data && response.data.length) {
                                return utilService.parseCountries(response.data);
                            } else {
                                _this.countries = null;
                                return [];
                            }
                        },
                        function() {
                            _this.countries = null;
                        }
                    );
                });
            }
            return _this.countries;
        }

        function getCities(onlyTopCities) {
            var _this = this;
            var category = onlyTopCities ? 'topCities' : 'cities';
            if (!_this[category]) {
                let url;
                if(onlyTopCities){
                    url = sharedConfig.clientApis.getTopCities().url;
                } else {
                    url = sharedConfig.clientApis.getCity().url;
                }
                _this[category] = apiService.get(url).then(function(response) {
                        if (response && response.data && response.data.length) {
                            return utilService.parseCity(response.data);
                        } else {
                            _this[category] = null;
                            return [];
                        }
                    },
                    function() {
                        _this[category] = null;
                    }
                );
            }
            return _this[category];
        }

        function getAllianceCities() {
            var _this = this;
            if (!_this.allianceCities || !_this.allianceCities.length) {
                return apiService.get(sharedConfig.clientApis.getAllianceCities().url).then(function(response) {
                        if (response && response.data && response.data.length) {
                            let allianceCities = utilService.parseCity(response.data);
                            _this.allianceCities = allianceCities && typeof allianceCities.sort === 'function' && allianceCities.sort((a, b) => {
                                if (a && b) {
                                    if (a.displayPriority > b.displayPriority) {
                                        return 1;
                                    } else if (a.displayPriority < b.displayPriority) {
                                        return -1;
                                    } else {
                                        return a.label > b.label ? 1 : -1;
                                    }
                                }
                            });
                            return _this.allianceCities;
                        }
                    },
                    function() {
                        _this.allianceCities = [];
                    }
                );
            } else {
                return new Promise(function(resolve, reject) {
                    if (_this.allianceCities) {
                        resolve(_this.allianceCities);
                    } else {
                        reject();
                    }
                });
            }
        }

        function getCityByLocation() {
            var _this = this;
            if (!_this.myCity) {
                _this.myCity = commonService.bindOnLoadPromise().then(() => {
                    return apiService.get(sharedConfig.clientApis.getCityByLocation().url);
                }, function() {
                    _this.myCity = null;
                }).then(function(response) {
                    if (response && response.data && response.data.city) {
                        let city = response.data.city;
                        return {
                            id: city.id,
                            label: city.label, // City present in the DB
                            userCity: response.data.userCity, // City as per the IP of user
                            userIP: response.data.clientIp
                        };
                    } else {
                        _this.myCity = null;
                        return {
                            id: null,
                            label: null
                        };
                    }
                });
            }
            return _this.myCity;
        }

        function getLeadPrice() {
            let _this = this;
            if (!_this.leadPrice) {
                return apiService.get(sharedConfig.apiHandlers.getCommonApiData({
                    dataType: "cityLeadPrice"
                }).url).then((response) => {
                    _this.leadPrice = response;
                    return _this.leadPrice;
                }, (err) => {
                    _this.leadPriceError = err;
                });
            } else {
                return new Promise(function(resolve, reject) {
                    if (_this.leadPrice) {
                        resolve(_this.leadPrice);
                    } else {
                        reject(_this.leadPriceError);
                    }
                });
            }
        }

        function trackCookies() {
            let url = sharedConfig.apiHandlers.trackCookies({
                query: urlService.getAllUrlParam()
            }).url;
            apiService.get(url).then(()=>{
                console.info('--tracker cookies success--');
                utilService.setPageData(sharedConfig.TRACK_COOKIE_STATUS, true);
            }, function(){
                utilService.setPageData(sharedConfig.TRACK_COOKIE_STATUS, false);
            });
        }

        function getDecryptedTokenValue(token){
            return apiService.postJSON(sharedConfig.apiHandlers.getDecryptedToken().url,{token});
        }

        return {
            getCountries,
            getCityByLocation,
            getCities,
            getAllianceCities,
            getLeadPrice,
            trackCookies,
            getDecryptedTokenValue
        };
    });

    return Box.Application.getService(SERVICE_NAME);
});
