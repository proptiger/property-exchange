"use strict";
define([
    'common/trackingConfigService',
    "common/sharedConfig",
    'services/defaultService',
    'services/bankService',
    'services/urlService',
    'services/utils',
], function(t,sharedConfig) {
    const SERVICE_NAME = 'FilterConfigService';

    Box.Application.addService(SERVICE_NAME, function() {

        const URLService = Box.Application.getService('URLService'),
            UtilService = Box.Application.getService('Utils'),
            DefaultService = Box.Application.getService('DefaultService'),
            BankService=Box.Application.getService('bankService');

        var filterModuleConfig = {
            'projectStatus': {
                'templateData': {
                    'label': '',
                    'name': 'projectStatus',
                    'defaultSelected': [],
                    'options': [{
                        'label': 'Launching Soon',
                        'value': 'launching-soon'
                    }, {
                        'label': 'New Launch',
                        'value': 'new-launch'
                    }, {
                        'label': 'Under Construction',
                        'value': 'under-construction'
                    }, {
                        'label': 'Ready to Move In',
                        'value': 'ready-to-move-in'
                    }]
                },
                setSelectedValue: function(urlParam, pageData) {
                    if (urlParam) {
                        this.templateData.selectedValue = urlParam.split(',');
                    } else if (pageData && pageData.filters && pageData.filters.projectStatus) {
                        this.templateData.selectedValue = pageData.filters.projectStatus.split(',');
                    } else {
                        this.templateData.selectedValue = this.templateData.defaultSelected;
                    }
                }
            },
            'countries': {
                templateData: {
                    name: 'countries',
                    label: 'country',
                    defaultSelected: "1",
                    error: ""
                },
                setTemplateData: function() {
                    var self = this;
                    return DefaultService.getCountries().then(function(response) {
                            self.templateData.options = response;
                        },
                        function() {
                            return [];
                        }
                    );
                }
            },
            'cities': {
                templateData: {
                    name: 'cities',
                    label: 'city',
                    defaultSelected: "1",
                },
                setTemplateData: function() {
                    var self = this;
                    return DefaultService.getCities().then(function(response) {
                            self.templateData.options = response;
                        },
                        function() {
                            return [];
                        }
                    );
                }
            },
            'banks': {
                templateData: {
                    name: 'banks',
                    label: 'Select Bank'
                },
                setTemplateData: function() {
                    var self = this;
                    return BankService.getBanks().then(function(response) {
                            self.templateData.options = response;
                        },
                        function() {
                            return [];
                        }
                    );
                }
            },
            'topCities': {
                templateData: {
                    name: 'cities',
                    label: 'city'
                },
                setTemplateData: function() {
                    var self = this, onlyTopCities=true;
                    return DefaultService.getCities(onlyTopCities).then(function(response) {
                            self.templateData.options = response;
                        },
                        function() {
                            return [];
                        }
                    );
                }
            },
            'allianceCities': {
                templateData: {
                    name: 'allianceCity',
                    label: 'city'
                },
                setTemplateData: function() {
                    var self = this;
                    return DefaultService.getAllianceCities().then(function(response) {
                            self.templateData.options = response;
                        },
                        function() {
                            return [];
                        }
                    );
                },
                setSelectedValue: function(urlParam, pageData, userDefault) {
                    if(userDefault){
                        this.templateData.selectedValue = [parseInt(userDefault)];
                    } else if(urlParam){
                        this.templateData.selectedValue = [parseInt(urlParam)];
                    }
                }
            },
            buyOrRent: {
                'templateData': {
                    'label': '',
                    'name': 'buyOrRent',
                    'defaultSelected': ['both'],
                    'options': [{
                        'label': 'Both',
                        'value': 'both'
                    }, {
                        'label': 'Buy',
                        'value': 'buy'
                    }, {
                        'label': 'Rent',
                        'value': 'rent'
                    }]
                }
            },
            branch:{                            //branches of ahmedabad as by default ahmedabad is selected
                templateData: {
                    label: 'branch',
                    name: 'branch',
                    defaultSelected:'',
                    options: [{
                        'label': 'Ahmedabad',
                        'value': 'Ahmedabad'
                    }, {
                        'label': 'Odhav',
                        'value': 'Odhav'
                    }]
                }                                  
            },
            gender: {
                'templateData': {
                    'label': '',
                    'name': 'gender',
                    'defaultSelected': ['Male'],
                    'options': [{
                        'label': 'Male',
                        'value': 'Male'
                    }, {
                        'label': 'Female',
                        'value': 'Female'
                    }]
                }
            },
            existingEmi: {
                'templateData': {
                    'label': 'Existing EMI',
                    'name': 'existingEmi',
                    'defaultSelected': ['no'],
                    'options': [{
                        'label': 'NO',
                        'value': 'no'
                    }, {
                        'label': 'Yes',
                        'value': 'yes'
                    }]
                }
            },
             coApplicant: {
                'templateData': {
                    'label': 'Co-applicant',
                    'name': 'coApplicant',
                    'defaultSelected': ['no'],
                    'options': [{
                        'label': 'NO',
                        'value': 'no'
                    }, {
                        'label': 'Yes',
                        'value': 'yes'
                    }]
                }
            },
             propertyIdentifies: {
                'templateData': {
                    'label': 'Property Identifies',
                    'name': 'propertyIdentifies',
                    'defaultSelected': ['no'],
                    'options': [{
                        'label': 'NO',
                        'value': 'no'
                    }, {
                        'label': 'Yes',
                        'value': 'yes'
                    }]
                }
            },
            bankPrefrence: {
                'templateData': {
                    'label': 'Bank Preference',
                    'name': 'bankPrefrence',
                    'defaultSelected': ['no'],
                    'options': [{
                        'label': 'NO',
                        'value': 'no'
                    }, {
                        'label': 'Yes',
                        'value': 'yes'
                    }]
                }
            },
            loanDuration: {
                'templateData': {
                    'label': '',
                    'name': 'loanDuration',
                    'defaultSelected': ['less than 15 days'],
                    'options': [{
                        'label': '< 15 Days',
                        'value': 'less than 15 days'
                    }, {
                        'label': '< 1 Month',
                        'value': 'less than 1 month'
                    }, {
                        'label': '< 2 Months',
                        'value': 'less than 2 months'
                    },{
                        'label': '> 2 Months',
                        'value': 'more than 2 months'
                    }]
                }
            },
            profession: {
                'templateData': {
                    'label': 'I am',
                    'name': 'income',
                    'defaultSelected': ['Salaried'],
                    'options': [{
                        'label': 'Salaried',
                        'value': 'Salaried'
                    }, {
                        'label': 'Self Employed Professional',
                        'value': 'Self Employed Professional'
                    }, {
                        'label': 'Self Employed Non Professional',
                        'value': 'Self Employed Non Professional'
                    }]
                }
            },
            homeLoanPropertyType: {
                'templateData': {
                    'label': 'I am buying a ',
                    'name': 'propertyType',
                    'defaultSelected': ['Residential Home'],
                    'options': [{
                        'label': 'Residential Home',
                        'value': 'Residential Home',
                        'id':1
                    }, {
                        'label': 'Residential Plot',
                        'value': 'Residential Plot',
                        'id':2
                    }, {
                        'label': 'Residential plot and construction',
                        'value': 'Residential plot and construction',
                        'id':3
                    }, {
                        'label': 'Residential home construction',
                        'value': 'Residential home construction',
                        'id':4
                    }, {
                        'label': 'Home Improvement',
                        'value': 'Home Improvement',
                        'id':5
                    }, {
                        'label': 'Home Extension',
                        'value': 'Home Extension',
                        'id':6
                    }]
                }
            },
            relationship: {
                'templateData': {
                    'label': 'Relationship ',
                    'name': 'coRelation',
                    'defaultSelected': ['wife'],
                    'options': [{
                        'label': 'Wife',
                        'value': 'wife',
                        'id':1
                    }, {
                        'label': 'Husband',
                        'value': 'husband',
                        'id':2
                    }, {
                        'label': 'Daughter',
                        'value': 'daughter',
                        'id':3
                    }, {
                        'label': 'Son',
                        'value': 'son',
                        'id':4
                    }, {
                        'label': 'Father',
                        'value': 'father',
                        'id':5
                    }, {
                        'label': 'Mother',
                        'value': 'mother',
                        'id':6
                    }, {
                        'label': 'Brother',
                        'value': 'brother',
                        'id':7
                    }, {
                        'label': 'Father-in-law',
                        'value': 'father-in-law',
                        'id':8
                    }, {
                        'label': 'Mother-in-law',
                        'value': 'mother-in-law',
                        'id':9
                    }, {
                        'label': 'Son-in-law',
                        'value': 'son-in-law',
                        'id':10
                    }, {
                        'label': 'Daughter-in-law',
                        'value': 'daughter-in-law',
                        'id':11
                    }]
                }
            },
            'beds': {
                'gaKey': t.BHK_KEY,
                'templateData': {
                    'name': 'beds',
                    'label': 'BHK',
                    'defaultSelected': [],
                    'options': [{
                        'label': '1',
                        'text': 'BHK',
                        'value': '1'
                    }, {
                        'label': '2',
                        'text': 'BHK',
                        'value': '2'
                    }, {
                        'label': '3',
                        'text': 'BHK',
                        'value': '3'
                    }, {
                        'label': '3+',
                        'text': 'BHK',
                        'value': '3plus'
                    }]
                },
                setSelectedValue: function(urlParam, pageData,userDefault) {
                    if(userDefault){
                        this.templateData.selectedValue = userDefault;
                    } else if (urlParam) {
                        this.templateData.selectedValue = urlParam.split(',');
                    } else if (pageData && pageData.filters && pageData.filters.beds) {
                        this.templateData.selectedValue = pageData.filters.beds.split(',');
                    } else {
                        this.templateData.selectedValue = this.templateData.defaultSelected;
                    }
                }
            },
            'plot': {
                'gaKey': t.UNIT_TYPE_KEY,
                'templateData': {
                    'label': 'plot',
                    'name': 'propertyType',
                    'defaultSelected': [],
                    'options': [{
                        'label': 'plot',
                        'text': 'land',
                        'value': 'residential-plot',
                        'hideOptional': true
                    }]
                },
            },
            'baths': {
                'templateData': {
                    'name': 'baths',
                    'options': [{
                        'label': '1+',
                        'value': '1plus'
                    }, {
                        'label': '2+',
                        'value': '2plus'
                    }, {
                        'label': '3+',
                        'value': '3plus'
                    }, {
                        'label': 'Any',
                        'value': 'any'
                    }]
                }
            },
            'sellerRating': {
                'templateData': {
                    'name': 'sellerRating',
                    'options': [{
                        'label': 'All',
                        'value': 'any'
                    }, {
                        'label': 'Rated 4+',
                        'value': '4plus'
                    }, {
                        'label': 'Rated 3+',
                        'value': '3plus'
                    }]
                }
            },
            'propertyType': {
                'gaKey': t.UNIT_TYPE_KEY,
                'templateData': {
                    'label': 'Property Type',
                    'quickFilterLabel': 'Property',
                    'name': 'propertyType',
                    'defaultSelected': [],
                },
                options: {
                    buy: [{
                        'id': 1,
                        'label': 'Apartment',
                        'value': 'apartment',
                    }, {
                        'id': 7,
                        'label': 'Builder Floor',
                        'value': 'builder-floor',
                        'alternateValue': 'independent-floor'
                    }, /*{
                        'id': 9,
                        'label': 'Studio Apartment',
                        'value': 'studio-apartment'
                    },*/ {
                        'id': 2,
                        'label': 'Villa',
                        'value': 'villa'
                    }, {
                        'id': 3,
                        'label': 'Residential Plot',
                        'value': 'residential-plot'
                    }, {
                        'id': 19,
                        'label': 'Independent House',
                        'value': 'independent-house'
                    }, /* {
                        'id': 8,
                        'label': 'Farmhouse',
                        'value': 'farmhouse'
                    }, {
                        'id': 20,
                        'label': 'Penthouse',
                        'value': 'penthouse'
                    }*/],
                    rent: [{
                        'id': 1,
                        'label': 'Apartment',
                        'value': 'apartment'
                    }, {
                        'id': 7,
                        'label': 'Builder Floor',
                        'value': 'builder-floor',
                        'alternateValue': 'independent-floor'
                    },/* {
                        'id': 9,
                        'label': 'Studio Apartment',
                        'value': 'studio-apartment'
                    },*/  {
                        'id': 2,
                        'label': 'Villa',
                        'value': 'villa'
                    }, {
                        'id': 10,
                        'label': 'Independent House',
                        'value': 'independent-house'
                    },/* {
                        'id': 8,
                        'label': 'Farmhouse',
                        'value': 'farmhouse'
                    }, {
                        'id': 20,
                        'label': 'Penthouse',
                        'value': 'penthouse'
                    }*/],
                    commercialBuy: [{
                        'id': 5,
                        'label': 'Shop',
                        'value': 'shop'
                    }, {
                        'id': 6,
                        'label': 'Office',
                        'value': 'office'
                    }],
                    commercialLease: [{
                        'id': 5,
                        'label': 'Shop',
                        'value': 'shop'
                    }, {
                        'id': 6,
                        'label': 'Office',
                        'value': 'office'
                    }]
                },
                setTemplateData: function(pageData) {
                    this.templateData.options = this.options[pageData.listingType || "buy"];
                },
                getTemplateData: function(pageData) {
                    return this.options[pageData.listingType || "buy"];
                },
                setSelectedValue: function(urlParam, pageData) {
                    if (urlParam) {
                        this.templateData.selectedValue = urlParam.split(',');
                    } else if (pageData && pageData.filters && pageData.filters.propertyType) {
                        this.templateData.selectedValue = pageData.filters.propertyType.split(',');
                    } else {
                        this.templateData.selectedValue = this.templateData.defaultSelected;
                    }
                }
            },
            'budgetQuickFilter' : {
                'gaKey': t.BUDGET_KEY,
                'templateData': {
                    'label': 'Budget',
                    'name': 'budget',
                    'defaultSelected': []
                },
                options: {
                    buy: [{
                        'id': 1,
                        'label': '0-25L',
                        'value': ',2500000'
                    },{
                        'id': 2,
                        'label': '25-45L',
                        'value': '2500000,4500000'
                    },{
                        'id': 3,
                        'label': '45-80L',
                        'value': '4500000,8000000'
                    },{
                        'id': 3,
                        'label': '80L-1.2Cr',
                        'value': '8000000,12000000'
                    },{
                        'id': 3,
                        'label': '1.2-2Cr',
                        'value': '12000000,20000000'
                    },{
                        'id': 3,
                        'label': '2 Cr +',
                        'value': '20000000,'
                    }],
                    rent: [{
                        'id': 1,
                        'label': '0-15K',
                        'value': ',15000'
                    },{
                        'id': 1,
                        'label': '15-35K',
                        'value': '15000,35000'
                    },{
                        'id': 1,
                        'label': '35-70K',
                        'value': '35000,70000'
                    },{
                        'id': 1,
                        'label': '70K +',
                        'value': '70000,'
                    }]
                },
                setTemplateData: function(pageData) {
                    this.templateData.options = this.options[pageData.listingType || "buy"];
                },
                getTemplateData: function(pageData) {
                    return this.options[pageData.listingType || "buy"];
                }
            },
            'budget': {
                'gaKey': t.BUDGET_KEY,
                'templateData': {
                    'name': 'budget',
                    'isCurrency': true,
                    'min': {
                        'label': 'Min'
                    },
                    'max': {
                        'label': 'Max'
                    }
                },
                setSelectedValue: function(urlParam, pageData) {
                    let paramArray;
                    if (urlParam) {
                        paramArray = urlParam.split(',');
                        this.templateData.selectedValue = {
                            min: {
                                value: paramArray[0],
                                label: UtilService.formatNumber(paramArray[0], { type : 'number', seperator : window.numberFormat })
                            },
                            max: {
                                value: paramArray[1],
                                label: UtilService.formatNumber(paramArray[1], { type : 'number', seperator : window.numberFormat })
                            }
                        };
                    } else if (pageData && pageData.filters && pageData.filters.budget) {
                        paramArray = pageData.filters.budget.split(',');
                        this.templateData.selectedValue = {
                            min: {
                                value: paramArray[0],
                                label: UtilService.formatNumber(paramArray[0], { type : 'number', seperator : window.numberFormat })
                            },
                            max: {
                                value: paramArray[1],
                                label: UtilService.formatNumber(paramArray[1], { type : 'number', seperator : window.numberFormat })
                            }
                        };
                    } else {
                        this.templateData.selectedValue = this.templateData.defaultSelected;
                    }

                },
                setTemplateData: function(pageData) {
                    
                    function _getPriceKeyValue(str){
                        let value  = str.split(' '), val,
                            obj    = {
                                'label': str
                            };

                        switch (value[1]) {
                            case 'K':
                                val = parseFloat(value[0])*1000;
                                break;
                            case 'L':
                                val = parseFloat(value[0])*100000;
                                break;
                            case 'Cr':
                                val = parseFloat(value[0])*10000000;
                                break;
                            case 'M':
                                val = parseFloat(value[0])*1000000;
                                break;
                            case 'B':
                                val = parseFloat(value[0])*1000000000;
                                break;
                            case 'Any':
                                val = '';
                                break;
                            default:
                                val = value[0];

                        }
                    
                        val = val.toString().replace(/,/g,'');
 
                        val = (val == 'Any') ? '' : val ;

                        obj.value = `${val}`;
                        return obj;
                    }

                    function _getUsableObject(obj){
                        let modifiedObj = {};
                        for(let key in obj){
                            (function(key){       //jshint ignore:line
                                modifiedObj[key] = [];
                                for(let i = 0; i < obj[key].length; i++){
                                    modifiedObj[key].push(_getPriceKeyValue(obj[key][i]));
                                }

                            })(key);
                        }
                        return modifiedObj;
                    }

                    if(window.numberFormat.format === 3){
                         var min = {
                            buy: ['0', '100,000', '200,000', '400,000', '600,000', '1 M', '1.2 M', '1.5 M', '2 M', '3 M', '5 M', '8 M', '10 M','20 M','40 M'],
                            rent: ['0', '2,500', '5,000', '7,500', '10,000', '12,500', '15,000', '20,000', '25,000', '30,000', '40,000', '50,000'],
                            commercialBuy: ['0', '100,000', '200,000', '400,000', '600,000', '1 M', '1.2 M', '1.5 M', '2 M', '3 M', '5 M', '8 M', '10 M','20 M','40 M'],
                            commercialLease: ['0', '2,500', '5,000', '7,500', '10,000', '12,500', '15,000', '20,000', '25,000', '30,000', '40,000', '50,000']  
                        };

                        var max = {
                            buy: ['100,000', '200,000', '400,000', '600,000', '1 M', '1.2 M', '1.5 M', '2 M', '3 M', '5 M', '8 M', '10 M', '20 M', '40 M', 'Any'],
                            rent: ['2,500', '5,000', '7,500', '10,000', '12,500', '15,000', '20,000', '25,000', '30,000', '40,000', '50,000', 'Any'],
                            commercialBuy: ['100,000', '200,000', '400,000', '600,000', '1 M', '1.2 M', '1.5 M', '2 M', '3 M', '5 M', '8 M', '10 M', '20 M', '40 M', 'Any'],
                            commercialLease: ['2,500', '5,000', '7,500', '10,000', '12,500', '15,000', '20,000', '25,000', '30,000', '40,000', '50,000', 'Any'] 
                        };
                    
                    } else {
                        var min = {
                            buy: ['0', '25 L', '50 L', '1 Cr', '1.5 Cr', '2 Cr', '2.5 Cr', '3 Cr', '4 Cr', '5 Cr', '7 Cr', '10 Cr', '20 Cr'],
                            rent: ['0', '5 K', '10 K', '15 K', '20 K', '25 K', '30 K', '40 K', '50 K', '60 K', '75 K', '1 L', '1.5 L', '2 L', '4 L', '7 L', '10 L'],
                            commercialBuy: ['0', '25 L', '50 L', '1 Cr', '1.5 Cr', '2 Cr', '2.5 Cr', '3 Cr', '4 Cr', '5 Cr', '7 Cr', '10 Cr', '20 Cr'],
                            commercialLease: ['0', '5 K', '10 K', '15 K', '20 K', '25 K', '30 K', '40 K', '50 K', '60 K', '75 K', '1 L', '1.5 L', '2 L', '4 L', '7 L', '10 L']
                        };

                        var max = {
                            buy: ['25 L', '50 L', '1 Cr', '1.5 Cr', '2 Cr', '2.5 Cr', '3 Cr', '4 Cr', '5 Cr', '7 Cr', '10 Cr', '20 Cr', 'Any'],
                            rent: ['5 K', '10 K', '15 K', '20 K', '25 K', '30 K', '40 K', '50 K', '60 K', '75 K', '1 L', '1.5 L', '2 L', '4 L', '7 L', '10 L', 'Any'],
                            commercialBuy: ['25 L', '50 L', '1 Cr', '1.5 Cr', '2 Cr', '2.5 Cr', '3 Cr', '4 Cr', '5 Cr', '7 Cr', '10 Cr', '20 Cr', 'Any'],
                            commercialLease: ['5 K', '10 K', '15 K', '20 K', '25 K', '30 K', '40 K', '50 K', '60 K', '75 K', '1 L', '1.5 L', '2 L', '4 L', '7 L', '10 L', 'Any']
                        };
                    }
                    
                    min = _getUsableObject(min);
                    max = _getUsableObject(max);
                    this.templateData.min.options = min[pageData.listingType];
                    this.templateData.max.options = max[pageData.listingType];
                },
                getModuleConfig: function(type, pageData) {
                    var config = {
                        rent: {
                             'start' : [0,1000000],
                             'doubleHandle': true,
                             'snap': true,
                            'type': 'budget',
                            'range': {
                                'min': {
                                    value: 0,
                                    label: '0'
                                },
                                '3%': {
                                    value: 2500,
                                    label: '<span>&#8377;</span> 2.5 K'
                                },
                                '9%': {
                                    value: 5000,
                                    label: '<span>&#8377;</span> 5 K'
                                },
                                '12%': {
                                    value: 7500,
                                    label: '<span>&#8377;</span> 7.5 K'
                                },
                                '15%': {
                                    value: 10000,
                                    label: '<span>&#8377;</span> 10 K'
                                },
                                '18%': {
                                    value: 12500,
                                    label: '<span>&#8377;</span> 12.5 K'
                                },
                                '21%': {
                                    value: 15000,
                                    label: '<span>&#8377;</span> 15 K'
                                },
                                '24%': {
                                    value: 17500,
                                    label: '<span>&#8377;</span> 17.5 K'
                                },
                                '27%': {
                                    value: 20000,
                                    label: '<span>&#8377;</span> 20 K'
                                },
                                '30%': {
                                    value: 22500,
                                    label: '<span>&#8377;</span> 22.5 K'
                                },
                                '33%': {
                                    value: 25000,
                                    label: '<span>&#8377;</span> 25 K'
                                },
                                '36%': {
                                    value: 27500,
                                    label: '<span>&#8377;</span> 27.5 K'
                                },
                                '39%': {
                                    value: 30000,
                                    label: '<span>&#8377;</span> 30 K'
                                },
                                '42%': {
                                    value: 32500,
                                    label: '<span>&#8377;</span> 32.5 K'
                                },
                                '45%': {
                                    value: 35000,
                                    label: '<span>&#8377;</span> 35 K'
                                },
                                '48%': {
                                    value: 37500,
                                    label: '<span>&#8377;</span> 37.5 K'
                                },
                                '51%': {
                                    value: 40000,
                                    label: '<span>&#8377;</span> 40 K'
                                },
                                '54%': {
                                    value: 42500,
                                    label: '<span>&#8377;</span> 42.5 K'
                                },
                                '57%': {
                                    value: 45000,
                                    label: '<span>&#8377;</span> 45 K'
                                },

                                '60%': {
                                    value: 47500,
                                    label: '<span>&#8377;</span> 47.5 K'
                                },
                                '63%': {
                                    value: 50000,
                                    label: '<span>&#8377;</span> 50 K'
                                },
                                '69%': {
                                    value: 60000,
                                    label: '<span>&#8377;</span> 60 K'
                                },
                                '72%': {
                                    value: 70000,
                                    label: '<span>&#8377;</span> 70 K'
                                },
                                '75%': {
                                    value: 80000,
                                    label: '<span>&#8377;</span> 80 K'
                                },
                                '78%': {
                                    value: 90000,
                                    label: '<span>&#8377;</span> 90 K'
                                },
                                '81%': {
                                    value: 100000,
                                    label: '<span>&#8377;</span> 1 L'
                                },
                                '84%': {
                                    value: 125000,
                                    label: '<span>&#8377;</span> 1.25 L'
                                },
                                '88%': {
                                    value: 150000,
                                    label: '<span>&#8377;</span> 1.5 L'
                                },
                                '92%': {
                                    value: 175000,
                                    label: '<span>&#8377;</span> 1.75 L'
                                },
                                '96%': {
                                    value: 200000,
                                    label: '<span>&#8377;</span> 2 L'
                                },
                                'max': {
                                    value: 1000000,
                                    label: '<span>&#8377;</span> 2+ L'
                                },
                            }
                        },
                        buy: {
                             'start' : [0,50000000],
                             'doubleHandle': true,
                             'snap': true,
                             'type': 'budget',
                             'range': {
                                'min': {
                                    value: 0,
                                    label: '0'
                                },
                                '5%': {
                                    value: 500000,
                                    label: '<span>&#8377;</span> 5 L'
                                },
                                '10%': {
                                    value: 1000000,
                                    label: '<span>&#8377;</span> 10 L'
                                },
                                '15%': {
                                    value: 1500000,
                                    label: '<span>&#8377;</span> 15 L'
                                },
                                '20%': {
                                    value: 2000000,
                                    label: '<span>&#8377;</span> 20 L'
                                },
                                '25%': {
                                    value: 2500000,
                                    label: '<span>&#8377;</span> 25 L'
                                },
                                '30%': {
                                    value: 3000000,
                                    label: '<span>&#8377;</span> 30 L'
                                },
                                '35%': {
                                    value: 3500000,
                                    label: '<span>&#8377;</span> 35 L'
                                },
                                '40%': {
                                    value: 4000000,
                                    label: '<span>&#8377;</span> 40 L'
                                },
                                '45%': {
                                    value: 4500000,
                                    label: '<span>&#8377;</span> 45 L'
                                },
                                '50%': {
                                    value: 5000000,
                                    label: '<span>&#8377;</span> 50 L'
                                },
                                '54%': {
                                    value: 6000000,
                                    label: '<span>&#8377;</span> 60 L'
                                },
                                '58%': {
                                    value: 7000000,
                                    label: '<span>&#8377;</span> 70 L'
                                },
                                '62%': {
                                    value: 8000000,
                                    label: '<span>&#8377;</span> 80 L'
                                },
                                '66%': {
                                    value: 9000000,
                                    label: '<span>&#8377;</span> 90 L'
                                },
                                '70%': {
                                    value: 10000000,
                                    label: '<span>&#8377;</span> 1.0 Cr'
                                },
                                '74%': {
                                    value: 12000000,
                                    label: '<span>&#8377;</span> 1.2 Cr'
                                },
                                '78%': {
                                    value: 14000000,
                                    label: '<span>&#8377;</span> 1.4 Cr'
                                },
                                '82%': {
                                    value: 16000000,
                                    label: '<span>&#8377;</span> 1.6 Cr'
                                },
                                '86%': {
                                    value: 18000000,
                                    label: '<span>&#8377;</span> 1.8 Cr'
                                },
                                '90%': {
                                    value: 20000000,
                                    label: '<span>&#8377;</span> 2.0 Cr'
                                },
                                '94%': {
                                    value: 25000000,
                                    label: '<span>&#8377;</span> 2.5 Cr'
                                },
                                '96%': {
                                    value: 30000000,
                                    label: '<span>&#8377;</span> 3.0 Cr'
                                },
                                'max': {
                                    value: 50000000,
                                    label: '<span>&#8377;</span> 3+ Cr'
                                }
                            }
                        }
                    };
                    var urlParam = URLService.getUrlParam(type),
                        configType = (pageData.listingType == 'rent' || pageData.listingType == 'commercialLease') ? "rent" : "buy",
                        thisConfig = config[configType];

                    urlParam = urlParam || (pageData && pageData.filters && pageData.filters.budget ? pageData.filters.budget : null);
                    if (urlParam) {
                        let paramArray = urlParam.split(',');
                        thisConfig = UtilService.deepCopy(thisConfig);
                        thisConfig.start[0] = paramArray[0] || thisConfig.start[0];
                        thisConfig.start[1] = paramArray[1] || thisConfig.start[1];
                    }
                    return thisConfig;
                }
            },
            'floor': {
                'templateData': {
                    'name': 'floor',
                    'optionSuffix': '',
                    'min': {
                        'label': 'Min',
                        'options': [{
                            'label': 'ground',
                            'value': '0'
                        }, {
                            'label': '1',
                            'value': '1'
                        }, {
                            'label': '5',
                            'value': '5'
                        }, {
                            'label': '9',
                            'value': '9'
                        }, {
                            'label': '13',
                            'value': '13'
                        }, {
                            'label': '17',
                            'value': '17'
                        }]
                    },
                    'max': {
                        'label': 'Max',
                        'options': [{
                            'label': '4',
                            'value': '4'
                        }, {
                            'label': '8',
                            'value': '8'
                        }, {
                            'label': '12',
                            'value': '12'
                        }, {
                            'label': '16',
                            'value': '16'
                        }, {
                            'label': 'Any',
                            'value': ''
                        }]
                    }
                },
                setSelectedValue: function(params) {
                    var paramArray = params.split(',');
                    this.templateData.selectedValue = {
                        min: {
                            value: paramArray[0] || '',
                            label: UtilService.formatNumber(paramArray[0], { type : 'number', seperator : window.numberFormat })
                        },
                        max: {
                            value: paramArray[1] || '',
                            label: UtilService.formatNumber(paramArray[1], { type : 'number', seperator : window.numberFormat })
                        }
                    };
                },
                getModuleConfig: function(type, pageData) {
                    var config = {
                        'start' : [0,120],
                        'doubleHandle': true,
                        'snap': true,
                        'type': 'floor',
                        'range': {
                            'min': {
                                value: 0,
                                label: '0'
                            },
                            '3%': {
                                value: 4,
                                label: '<span>&#8377;</span> 4th'
                            },
                            '9%': {
                                value: 8,
                                label: '<span>&#8377;</span> 8th'
                            },
                            '12%': {
                                value: 12,
                                label: '<span>&#8377;</span> 12th'
                            },
                            '15%': {
                                value: 16,
                                label: '<span>&#8377;</span> 16th'
                            },
                            '18%': {
                                value: 20,
                                label: '<span>&#8377;</span> 20th'
                            },
                            '21%': {
                                value: 24,
                                label: '<span>&#8377;</span> 24th'
                            },
                            '24%': {
                                value: 28,
                                label: '<span>&#8377;</span> 28th'
                            },
                            '27%': {
                                value: 32,
                                label: '<span>&#8377;</span> 32th'
                            },
                            '30%': {
                                value: 36,
                                label: '<span>&#8377;</span> 36th'
                            },
                            '33%': {
                                value: 40,
                                label: '<span>&#8377;</span> 40th'
                            },
                            '36%': {
                                value: 44,
                                label: '<span>&#8377;</span> 44th'
                            },
                            '39%': {
                                value: 48,
                                label: '<span>&#8377;</span> 48th'
                            },
                            '42%': {
                                value: 52,
                                label: '<span>&#8377;</span> 52th'
                            },
                            '45%': {
                                value: 56,
                                label: '<span>&#8377;</span> 56th'
                            },
                            '48%': {
                                value: 60,
                                label: '<span>&#8377;</span> 60th'
                            },
                            '51%': {
                                value: 64,
                                label: '<span>&#8377;</span> 64th'
                            },
                            '54%': {
                                value: 68,
                                label: '<span>&#8377;</span> 68th'
                            },
                            '57%': {
                                value: 72,
                                label: '<span>&#8377;</span> 72th'
                            },

                            '60%': {
                                value: 76,
                                label: '<span>&#8377;</span> 76th'
                            },
                            '63%': {
                                value: 80,
                                label: '<span>&#8377;</span> 80th'
                            },
                            '69%': {
                                value: 84,
                                label: '<span>&#8377;</span> 84th'
                            },
                            '72%': {
                                value: 88,
                                label: '<span>&#8377;</span> 88th'
                            },
                            '75%': {
                                value: 92,
                                label: '<span>&#8377;</span> 92th'
                            },
                            '78%': {
                                value: 96,
                                label: '<span>&#8377;</span> 96th'
                            },
                            '81%': {
                                value: 100,
                                label: '<span>&#8377;</span> 100th'
                            },
                            '84%': {
                                value: 104,
                                label: '<span>&#8377;</span> 104th'
                            },
                            '88%': {
                                value: 108,
                                label: '<span>&#8377;</span> 108th'
                            },
                            '92%': {
                                value: 112,
                                label: '<span>&#8377;</span> 112th'
                            },
                            '96%': {
                                value: 116,
                                label: '<span>&#8377;</span> 116th'
                            },
                            'max': {
                                value: 120,
                                label: '<span>&#8377;</span> 120th+'
                            }
                        }
                    };
                    var urlParam = URLService.getUrlParam(type),
                        thisConfig = config;

                    urlParam = urlParam || (pageData && pageData.filters && pageData.filters.budget ? pageData.filters.budget : null);
                    if (urlParam) {
                        let paramArray = urlParam.split(',');
                        thisConfig = UtilService.deepCopy(thisConfig);
                        thisConfig.start[0] = paramArray[0] || thisConfig.start[0];
                        thisConfig.start[1] = paramArray[1] || thisConfig.start[1];
                    }
                    return thisConfig;
                }
            },
            'areaQuickFilter' : {
                'gaKey': 'area',
                'templateData': {
                    'label': 'Area',
                    'name': 'area',
                    'defaultSelected': []
                },
                options: [{
                    'id': 1,
                    'label': '1.6k-3k sq.ft.',
                    'value': '1600,3000'
                },{
                    'id': 2,
                    'label': '3k-5.3k sq.ft.',
                    'value': '3000,5300'
                },{
                    'id': 3,
                    'label': '5.3k-9.3k sq.ft.',
                    'value': '5300,9300'
                },{
                    'id': 4,
                    'label': '9.3k + sq.ft.',
                    'value': '9300,'
                }],
                setTemplateData: function() {
                    this.templateData.options = this.options;
                },
                getTemplateData: function() {
                    return this.options;
                }
            },
            'area': {
                'templateData': {
                    'name': 'area',
                    'optionSuffix': 'sq ft',
                    'min': {
                        'label': 'Min',
                        'options': [{
                            'label': '0',
                            'value': '0'
                        }, {
                            'label': '500',
                            'value': '500'
                        }, {
                            'label': '1,000',
                            'value': '1000'
                        }, {
                            'label': '1,500',
                            'value': '1500'
                        }, {
                            'label': '2,000',
                            'value': '2000'
                        }]
                    },
                    'max': {
                        'label': 'Max',
                        'options': [{
                            'label': '500',
                            'value': '500'
                        }, {
                            'label': '1,000',
                            'value': '1000'
                        }, {
                            'label': '1,500',
                            'value': '1500'
                        }, {
                            'label': '2,000',
                            'value': '2000'
                        }, {
                            'label': 'Any',
                            'value': ''
                        }]
                    }
                },
                setSelectedValue: function(params) {
                    var paramArray = params.split(',');
                    this.templateData.selectedValue = {
                        min: {
                            value: paramArray[0] || '',
                            label: UtilService.formatNumber(paramArray[0], { type : 'number', seperator : window.numberFormat })
                        },
                        max: {
                            value: paramArray[1] || '',
                            label: UtilService.formatNumber(paramArray[1], { type : 'number', seperator : window.numberFormat })
                        }
                    };
                },
                getModuleConfig: function(type, pageData) {
                    var config = {
                        buy: {
                            'start': [0, 5000],
                            'doubleHandle': true,
                            'snap': true,
                            'range': {
                                'min': {
                                    value: 0,
                                    label: '0'
                                },
                                '10%': {
                                    value: 500,
                                    label: '500 sqft'
                                },
                                '20%': {
                                    value: 1000,
                                    label: '1,000 sqft'
                                },
                                '30%': {
                                    value: 1500,
                                    label: '1,500 sqft'
                                },
                                '40%': {
                                    value: 2000,
                                    label: '2,000 sqft'
                                },
                                '50%': {
                                    value: 2500,
                                    label: '2,500 sqft'
                                },
                                '60%': {
                                    value: 3000,
                                    label: '3,000 sqft'
                                },
                                '70%': {
                                    value: 3500,
                                    label: '3,500 sqft'
                                },
                                '80%': {
                                    value: 4000,
                                    label: '4,000 sqft'
                                },
                                '90%': {
                                    value: 4500,
                                    label: '4,500 sqft'
                                },
                                'max': {
                                    value: 5000,
                                    label: '5,000+ sqft'
                                }
                            }
                        }
                    };
                    var urlParam = URLService.getUrlParam(type),
                        configType = (pageData.listingType == 'buy' || pageData.listingType == 'commercialBuy' || pageData.listingType == 'commercialLease') ? "buy" : "",
                        thisConfig = config[configType];

                    urlParam = urlParam || (pageData && pageData.filters && pageData.filters.area ? pageData.filters.area : null);
                    if (urlParam) {
                        let paramArray = urlParam.split(',');
                        thisConfig = UtilService.deepCopy(thisConfig);
                        thisConfig.start[0] = paramArray[0] || thisConfig.start[0];
                        thisConfig.start[1] = paramArray[1] || thisConfig.start[1];
                    }
                    return thisConfig;
                }
            },
            'newOrResale': {
                'templateData': {
                    'label': '',
                    'name': 'newOrResale',
                    'defaultSelected': [''],
                    'options': [{
                        'label': 'Both',
                        'value': ''
                    }, {
                        'label': 'New',
                        'value': 'new',
                        'alternateValue': 'primary'
                    }, {
                        'label': 'Resale',
                        'value': 'resale'
                    }]
                }
            },
            'sortBy': {
                'templateData': {
                    'label': 'Sort By',
                    'name': 'sortBy',
                    'defaultSelected': ['relevance'],
                    'options': [{
                        'label': 'Relevance',
                        'value': 'relevance'
                    },{
                        'label': 'Popularity',
                        'value': 'popularity'
                    }, {
                        'label': 'Price (High to Low)',
                        'value': 'price-desc'
                    }, {
                        'label': 'Price (Low to High)',
                        'value': 'price-asc'
                    }, {
                        'label': 'Seller Rating',
                        'value': 'rating-desc'
                    }, {
                        'label': 'Date Posted',
                        'value': 'date-desc'
                    }]
                },
                setTemplateData: function(pageData) {

                    let templateDataOptionsLength = this.templateData.options.length;
                    if(this.templateData.options[templateDataOptionsLength-1].value === 'distance-asc'){
                        this.templateData.options.splice(templateDataOptionsLength-1, 1);
                    }

                    if (pageData.latitude && pageData.longitude) {
                        let distanceSort = {
                            'label': 'Landmark Distance',
                            'value': 'distance-asc'
                        };
                        this.templateData.options.push(distanceSort);
                    }
                },
                setSelectedValue: function(urlParam, pageData) {
                    if(urlParam){
                        this.templateData.selectedValue = urlParam;
                    }else if (pageData && pageData.sortBy) {
                        this.templateData.selectedValue = pageData.sortBy;
                    } else {
                        this.templateData.selectedValue = this.templateData.defaultSelected;
                    }
                }
            },
            'projectSortBy': {
                'templateData': {
                    'label': 'Sort By',
                    'name': 'sortBy',
                    'defaultSelected': ['relevance'],
                    'options': [{
                        'label': 'Relevance',
                        'value': 'relevance'
                    },{
                        'label': 'Popularity',
                        'value': 'popularity'
                    },{
                        'label': 'Price (High to Low)',
                        'value': 'price-desc'
                    }, {
                        'label': 'Price (Low to High)',
                        'value': 'price-asc'
                    }]
                },
                setSelectedValue: function(urlParam, pageData) {
                    if(urlParam){
                        this.templateData.selectedValue = urlParam;
                    }else if (pageData && pageData.sortBy) {
                        this.templateData.selectedValue = pageData.sortBy;
                    } else {
                        this.templateData.selectedValue = this.templateData.defaultSelected;
                    }
                }
            },
            'postedBy': {
                'templateData': {
                    'label': 'Posted By',
                    'name': 'postedBy'
                },
                'options': [{
                    'label': '<span class="select-highlighter">Select</span> Agent',
                    'value': 'selectAgent',
                    'listingType':'rent' // select agent to be shown only for rent (handeled only for multiselect checkbox cases)
                },{
                    'label': 'Agent',
                    'value': 'agent'
                }, {
                    'label': 'Builder',
                    'value': 'builder'
                }, {
                    'label': 'Owner',
                    'value': 'owner'
                }],
                setSelectedValue: function(urlParam, pageData) {
                    if (urlParam) {
                        this.templateData.selectedValue = urlParam.split(',');
                    } else if (pageData && pageData.filters && pageData.filters.postedBy) {
                        this.templateData.selectedValue = [pageData.filters.postedBy];
                    } else {
                        this.templateData.selectedValue = this.templateData.defaultSelected;
                    }
                },
                setTemplateData:  function(pageData) {
                    this.templateData.options = this.options.filter((option) => {
                        return option.value !== 'selectAgent' || 
                        (sharedConfig.makaanSelectCities.indexOf(pageData.cityId) > -1 && pageData.listingType === "rent");
                    });
                },
                getSelectedValues: function(paramsObj){
                    let postedBy = paramsObj.postedBy && paramsObj.postedBy.split(',')||[],
                    selectAgentIndex = postedBy.indexOf('selectAgent');

                    if(sharedConfig.makaanSelectCities.indexOf(paramsObj.cityId)<0){
                        if(selectAgentIndex > -1){
                            postedBy.splice(selectAgentIndex,1);
                        }
                    }

                    postedBy = postedBy.toString();

                    return postedBy;
                }
            },
            'possession': {
                'templateData': {
                    'label': 'Possession In',
                    'name': 'possession',
                    'options': [{
                        'label': '1 Year',
                        'value': '1'
                    }, {
                        'label': '2 Years',
                        'value': '2'
                    }, {
                        'label': '3 Years',
                        'value': '3'
                    }, {
                        'label': 'Any',
                        'value': 'any'
                    }]
                }
            },
            'ageOfProperty': {
                'templateData': {
                    'commaAllowedForValue': true,
                    'label': 'Age of Property',
                    'name': 'ageOfProperty',
                    'options': [{
                        'label': '0-1 Year',
                        'value': '0,1'
                    }, {
                        'label': '1-2 Years',
                        'value': '1,2'
                    }, {
                        'label': '2-5 Years',
                        'value': '2,5'
                    }, {
                        'label': 'Any',
                        'value': 'any'
                    }]
                }
            },
            'currentlyLeasedOut': {
                'templateData': {
                    'commaAllowedForValue': true,
                    'label': 'Pre Leased',
                    'name': 'currentlyLeasedOut',
                    'options': [{
                        'label': 'Yes',
                        'value': 'yes'
                    }, {
                        'label': 'No',
                        'value': 'no'
                    }]
                }
            },
            'availability': {
                'templateData': {
                    'name': 'availability',
                    'options': [{
                        'label': 'immediate',
                        'value': 'immediate'
                    }, {
                        'label': 'In a week',
                        'value': '7'
                    }, {
                        'label': 'In a month',
                        'value': '31'
                    }, {
                        'label': 'Anytime',
                        'value': 'any'
                    }]
                }
            },
            'furnished': {
                'templateData': {
                    'name': 'furnished',
                    'quickFilterLabel': 'Furnishing',
                    'label': 'Furnished',
                    'options': [{
                        'label': 'Unfurnished',
                        'value': 'unfurnished'
                    }, {
                        'label': 'Semi-furnished',
                        'value': 'semifurnished'
                    }, {
                        'label': 'Fully Furnished',
                        'value': 'furnished'
                    }]
                },
                setSelectedValue: function(urlParam, pageData) {
                    if (urlParam) {
                        this.templateData.selectedValue = urlParam.split(',');
                    } else if (pageData && pageData.filters && pageData.filters.furnished) {
                        this.templateData.selectedValue = [pageData.filters.furnished];
                    } else {
                        this.templateData.selectedValue = this.templateData.defaultSelected;
                    }
                }
            },
            'furnishings': {
                'templateData': {
                    'name': 'furnishings',
                    'label': 'Select Furnishings',
                    'options': [{
                        'label': 'Wardrobe',
                        'value': 'wardrobe'
                    }, {
                        'label': 'Double Bed',
                        'value': 'double-bed'
                    }, {
                        'label': 'TV',
                        'value': 'tv'
                    }, {
                        'label': 'Refrigerator',
                        'value': 'refrigerator'
                    }, {
                        'label': 'Sofa',
                        'value': 'sofa'
                    }, {
                        'label': 'Washing Machine',
                        'value': 'washing-machine'
                    }, {
                        'label': 'Wifi',
                        'value': 'wifi'
                    }, {
                        'label': 'Microwave',
                        'value': 'microwave'
                    }, {
                        'label': 'Dining Table',
                        'value': 'dining-table'
                    }, {
                        'label': 'Gas connection',
                        'value': 'gas-connection'
                    }, {
                        'label': 'AC',
                        'value': 'ac'
                    }, {
                        'label': 'Bed',
                        'value': 'bed'
                    }]
                }
            },
            'restrictions': {
                'templateData': {
                    'name': 'restrictions',
                    'label': 'Select Restrictions',
                    'options': [{
                        'label': 'Bachelors',
                        'value': 'bachelors'
                    }, {
                        'label': 'Family',
                        'value': 'family'
                    }, {
                        'label': 'Pets Allowed',
                        'value': 'pets-allowed'
                    }, {
                        'label': 'Non Vegetarian',
                        'value': 'non-vegetarian'
                    }, {
                        'label': 'Company Lease',
                        'value': 'company-lease'
                    }]
                }
            },
            'assist': {
                'templateData': {
                    'name': 'assist',
                    'options': [{
                        'value': 'true'
                    }]
                }
            },
            'doubleBeds': {
                'templateData': {
                    'name': 'doubleBeds',
                    'options': [{
                        'value': 'true'
                    }]
                }
            }
        };

        var _renderHTML = function(template, fData, placeholder, moduleConfig) {
            return new Promise(function(resolve, reject) {
                let templateData            = UtilService.deepCopy(fData.templateData);
                    templateData.moduleId   = placeholder.id;
                    templateData.moduleConfig = moduleConfig;
                    templateData.applyrequired = $(placeholder).data('applyrequired');
                    templateData.mCards = $(placeholder).data('mcards');
                let html = template(templateData);
                if (html) {
                    placeholder.innerHTML = html;
                    resolve();
                } else {
                    reject();
                }
            });
        };

        // Common method to rendering filter modules
        var renderFilter = function(placeholder, pageData, template, templateFromPromise, userDefault, config) {
            var type = $(placeholder).data('type');
            var dataSelectedValue = $(placeholder).data('selectedValue');
            if (!type || !filterModuleConfig[type]) {
                return;
            }
            var fData = filterModuleConfig[type];
            var urlParam = URLService.getUrlParam(type);
            if (userDefault && fData.setSelectedValue) {
                fData.setSelectedValue(urlParam, pageData, userDefault);
            } else if (!userDefault && fData.setSelectedValue) {
                fData.setSelectedValue(urlParam, pageData);
            } else if (urlParam && filterModuleConfig[type].templateData.commaAllowedForValue) {
                fData.templateData.selectedValue = urlParam;
            }else if(urlParam){
                fData.templateData.selectedValue = urlParam.split(',');
            } else {
                fData.templateData.selectedValue = userDefault || dataSelectedValue || fData.templateData.defaultSelected ;
            }
            fData.templateData.pageListingType = pageData.listingType;
            if (fData.setTemplateData) {
                if (templateFromPromise) {
                    // render skeleton prior promise
                    _renderHTML(template, fData, placeholder, config);
                    return fData.setTemplateData(pageData).then(function() {
                        return _renderHTML(template, fData, placeholder, config);
                    });
                } else {
                    fData.setTemplateData(pageData);
                    return _renderHTML(template, fData, placeholder, config);
                }
            }
            return _renderHTML(template, fData, placeholder, config);
        };


        // Returns filter module config
        var getFilterModuleConfig = function(name) {
            if (name) {
                return filterModuleConfig[name];
            }
            return filterModuleConfig;
        };
         var returnFilterSelectedValues = function(name,paramsObj) {
            if (name && filterModuleConfig[name] && typeof filterModuleConfig[name].getSelectedValues ==="function") {
                return filterModuleConfig[name].getSelectedValues(paramsObj);
            }
        };

        var getSelectedQueryFilterKeyValObj = function(href) {

            let taxonomySupported = ['beds', 'budget', 'postedBy', 'furnished', 'propertyType', 'projectStatus']; //and all values as substring for taxonomy filters

            let selectFilterkeyValObj = {},
                filterModuleConfigKeys = Object.keys(filterModuleConfig),
                urlParam = URLService.getAllUrlParam(href),
                pageData = UtilService.getPageData();
            for (let key in urlParam) {
                if (urlParam[key] && filterModuleConfigKeys.indexOf(key) > -1) {
                    selectFilterkeyValObj[key] = urlParam[key];
                }
            }

            // include taxonomy filters if not present in query string
            for (let key in pageData.filters) {
                if (key != 'listingType' && pageData.filters[key] && (taxonomySupported.indexOf(key) > -1) && !selectFilterkeyValObj[key]) {
                    selectFilterkeyValObj[key] = pageData.filters[key];
                }
            }

            return selectFilterkeyValObj;
        };

        var getUrlFilterKeyValueObj = function(url){
            url = url || window.location.href;
            let selectFilterkeyValObj = {},
                filterModuleConfigKeys = Object.keys(filterModuleConfig),
                urlParam = URLService.getAllUrlParam(url);
            for (let key in urlParam) {
                if (urlParam[key] && filterModuleConfigKeys.indexOf(key) > -1) {
                    selectFilterkeyValObj[key] = urlParam[key];
                }
            }
            return selectFilterkeyValObj;
        }

        var isFilterApplied = function(){
            let selectedFilter = getSelectedQueryFilterKeyValObj();
            delete selectedFilter.newOrResale;
            delete selectedFilter.listingType;
            return !$.isEmptyObject(selectedFilter);
        };

        var _getURL = function(urlObj, query) {
            if (!query) {
                return urlObj;
            }

            let queryStr = '',
                seperator;
            if (typeof query === 'string') {
                queryStr = query;
            } else {
                for (let key in query) {
                    if(query[key]){
                        queryStr += `&${key}=${query[key]}`;
                    }
                }
                queryStr = queryStr.slice(1);
            }

            seperator = urlObj.indexOf('?') > -1 ? '&' : '?';
            urlObj += `${seperator}${queryStr}`;
            return urlObj;
        };

        var getUrlWithUpdatedParams = function({skipHref, skipPageData, skipFilterData, overrideParams, makeProjectUrl} = {}) {
            let validKeys = ['listingType', 'pageType', 'stateName', 'cityName', 'localityName', 'suburbName',  'builderName', 'companyName', 'projectName', 'placeName','stateId', 'cityId', 'localityId', 'localityOrSuburbId', 'suburbId', 'builderId', 'projectId', 'placeId', 'sellerId','sellerUserId', 'companyId', 'templateId','sortBy','paidSellerUserIds'],
                pageData = UtilService.getPageData(),
                dataObj = {},
                responseString;

            if (!skipPageData && pageData) {
                for (let key in pageData) {
                    if (validKeys.indexOf(key) > -1 && pageData[key]) {
                        dataObj[key] = pageData[key];
                    }
                }

                let path = URLService.getUrlParam('path');
                if(path && path.length){
                        dataObj['path'] =  path;
                }

            }

            if (!skipFilterData) {
                let filterData = getSelectedQueryFilterKeyValObj() || {};
                for (let key in filterData) {
                    if (filterData[key]) {
                        dataObj[key] = filterData[key];
                    }
                }
            }

            if (overrideParams) {
                for (let key in overrideParams) {
                    if (overrideParams[key]) {
                        dataObj[key] = overrideParams[key];
                    }else {
                        delete dataObj[key];
                    }
                }
            }
            let pathName;
            if(makeProjectUrl){
                pathName = UtilService.projectPropertyPathName();
            }else{
                pathName = UtilService.listingsPropertyFullPath();
            }

            if(overrideParams && overrideParams.isMultiLocalitySearch){
                dataObj.pageType = "LISTINGS_PROPERTY_URLS";
                dataObj.templateId = "MAKAAN_MULTIPLE_LOCALITY_LISTING_BUY";
                ['suburbId', 'suburbName', 'localityId', 'localityName'].forEach(attribute => {
                    if(!overrideParams[attribute]){
                        delete dataObj[attribute];
                    }
                })
            }

            responseString = _getURL(`${pathName}`, dataObj);

            if (skipHref) {
                responseString = responseString.split('?')[1];
            }

            return responseString;
        };

        var getSellerListingUrl = function(seller, isMap=false, extra={}) {
            let prefix = isMap ? '/maps' : '',
                url = `${prefix}/seller-property`,
                pageType = isMap ? 'SELLER_PROPERTY_URLS_MAPS' : 'SELLER_PROPERTY_URLS',
                selectorQuery, extraParams;

            if (seller.id && seller.sellerUserId) {
                extraParams = {
                    'pageType': pageType,
                    'sellerId': seller.id,
                    'sellerUserId': seller.sellerUserId,
                    'sellerName': seller.name,
                    'listingType': seller.listingType
                };

            } else if (seller.type == 'BUILDER' && seller.builderId) {
                extraParams = {
                    'pageType': pageType,
                    'builderId': seller.builderId,
                    'builderName': seller.builderName,
                    'listingType': seller.listingType
                };
            }
            if(extraParams){
                if($.isPlainObject(extra)){
                    $.extend(true, extraParams, extra);
                }
                selectorQuery = getUrlWithUpdatedParams({
                    skipHref: true,
                    overrideParams: extraParams
                });

                url += `?${selectorQuery}`;
            }
            return url;
        };

        //to get page filters and serp page type info
        var getTrackPageFilters = function(){
            let pageData = UtilService.getPageData(),
                filters = getSelectedQueryFilterKeyValObj(),
                trackFilters = {};
            if(pageData.stateId){
                trackFilters.stateId = pageData.stateId;
            }
            if(pageData.cityId){
                trackFilters.cityId = pageData.cityId;
            }
            if(pageData.localityId || pageData.localityOrSuburbId){
                trackFilters.localityIds = pageData.localityId || pageData.localityOrSuburbId;
            }
            if(pageData.sellerId){
                trackFilters.sellerId = pageData.sellerId;
            }
            if(pageData.builderId){
                trackFilters.builderId = pageData.builderId;
            }
            if(pageData.projectId){
                trackFilters.projectId = pageData.projectId;
            }
            if(pageData.listingType){
                trackFilters.listingType = pageData.listingType;
            }
            for(var filter in filters){
                trackFilters[filter] = filters[filter];
            }
            return trackFilters;
        };

        var propertyTypeMap = [{
            'id': 1,
            'label': 'Apartment',
            'value': 'apartment',
        }, {
            'id': 7,
            'label': 'Builder Floor',
            'value': 'builder-floor'
        }, {
            'id': 2,
            'label': 'Villa',
            'value': 'villa'
        }, {
            'id': 3,
            'label': 'Residential Plot',
            'value': 'residential-plot'
        }, {
            'id': 19,
            'label': 'Independent House',
            'value': 'independent-house'
        }];

        return {
            renderFilter,
            getFilterModuleConfig,
            getSelectedQueryFilterKeyValObj,
            getUrlWithUpdatedParams,
            getSellerListingUrl,
            getTrackPageFilters,
            isFilterApplied,
            propertyTypeMap,
            getUrlFilterKeyValueObj,
            returnFilterSelectedValues
        };
    });

    return Box.Application.getService(SERVICE_NAME);
});
