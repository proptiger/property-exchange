define([
    'common/trackingConfigService',
    'common/sharedConfig',
    'services/localStorageService',
    "services/utils"
], (t, sharedConfig, localStorageService, utils) => {
    'use strict';
    const SERVICE_NAME = 'gaTrackingService';

    const INTERACTIVE_EVENTS = {
        CATEGORIES: [t.LEAD_FORM_CATEGORY, t.FILTER_CATEGORY, t.SIMILAR_CATEGORY, t.FEEDBACK_CATEGORY, t.DEAL_REVIEW_CATEGORY, t.DESCRIPTION_CATEGORY,
                    t.SIMILAR_SECTION_CATEGORY, t.PRICE_TREND_CATEGORY, t.RHS_CATEGORY],
        EVENTS: [t.SUGGESTION_CHOSEN_EVENT, t.GALLERY_INTERACTION_EVENT, t.VIEW_DETAILS_EVENT, t.VIEW_PROJECT_EVENT, t.VIEW_LOCALITY_EVENT,
                t.VIEW_SELLER_DETAILS_EVENT, t.SHORTLIST_EVENT, t.SUBMIT_EVENT],
        LABELS: [t.LOGIN_LABEL],
        EXCLUDE_NAMES_MATCHING: "Scroll"
    };

    Box.Application.addService(SERVICE_NAME, () => {

        function trackEvent(event, properties) {
            var experiments = utils.getCookie(sharedConfig.EXPERIMENTS_COOKIE_NAME);
            if(typeof ga === 'function'){

                let dripUserId = (localStorageService.getSessionItem('leadDrip') || {}).userId,
                    leadUserId = localStorageService.getItem(sharedConfig.leadClientIdCookieKey) || '';

                let dimension11Value = dripUserId || leadUserId;

                let fieldsObject = {
                    'dimension1': properties[t.PAGE_KEY],
                    'dimension2': properties[t.SELLER_TYPE_KEY],
                    'dimension13': properties[t.NAME_KEY],
                    'dimension14': properties[t.URL_KEY],
                    'dimension4': properties[t.SOURCE_MODULE_KEY],
                    'dimension5': properties[t.LISTING_CATEGORY_KEY],
                    'dimension6': properties[t.PROJECT_STATUS_KEY],
                    'dimension7': properties[t.BUDGET_KEY],
                    'dimension8': properties[t.UNIT_TYPE_KEY],
                    'dimension9': properties[t.BHK_KEY],
                    'dimension10': properties[t.SELLER_ID_KEY],
                    'dimension11': dimension11Value,
                    'dimension12': properties[t.PHONE_USER_KEY],
                    'dimension15': properties[t.VISITOR_CITY_KEY],
                    'dimension16': properties[t.NAME_FLAG_KEY],
                    'dimension17': properties[t.EMAIL_FLAG_KEY],
                    'dimension20': properties[t.SELLER_STATUS_KEY],
                    'dimension21': properties[t.QUESTION],  // for user profiling
                    'dimension22': properties[t.ANSWER],    // for user profiling
                    'dimension24': properties[t.HAS_TENANT_PREFERENCES],
                    'dimension43': properties[t.USER_ID_KEY],//currently used for makaan Select
                    'dimension44': properties[t.COUPON_CODE_KEY],
                    'dimension26': _removeDuplicates(properties[t.CITY_ID_KEY]),
                    'dimension27': _checkMultipleIds(properties[t.RANK_KEY]),
                    'dimension29': _checkMultipleIds(properties[t.LISTING_ID_KEY]),
                    'dimension28': _checkMultipleIds(properties[t.SELLER_SCORE_KEY]),
                    'dimension30': _checkMultipleIds(properties[t.PROJECT_ID_KEY]),
                    'dimension31': _checkMultipleIds(properties[t.LOCALITY_ID_KEY]),
                    'dimension32': _checkMultipleIds(properties[t.SUBURB_ID_KEY]),
                    'dimension33': _checkMultipleIds(properties[t.LISTING_SCORE_KEY]),
                    'dimension49': properties[t.ADDITIONAL_CD49],
                    'dimension50': properties[t.RECENCY_KEY],
                    'dimension53': properties[t.ADDITIONAL_CD53],
                    'dimension56': properties[t.SELECT_PROPERTY_KEY],
                    'dimension57': properties[t.ADDITIONAL_CD57],
                    'dimension58': properties[t.EXIT_INTENT_ENCRYPT_DATA],
                    'dimension59': properties[t.UNIQUE_ENQUIRY_ID_KEY], // contains unique id returned by petra/sapphire/kira enquiry API'S
                    'dimension60': experiments && decodeURIComponent(experiments) || t.NA_LABEL,
                    'dimension61': properties[t.SECTION_CLICKED],
                    'dimension64': properties[t.FILTER_USED]||'na', //na for no filters applied
                    'dimension66': navigator.cookieEnabled ? t.YES_LABEL : (navigator.cookieEnabled === false ? t.NO_LABEL : t.NA_LABEL),
                    'dimension67': utils.getPageData(sharedConfig.TRACK_COOKIE_STATUS) ? t.YES_LABEL : 
                        (utils.getPageData(sharedConfig.TRACK_COOKIE_STATUS) === false ? t.NO_LABEL : t.NA_LABEL),
                    'metric7': _checkMultipleIds(properties[t.BUILDER_ID_KEY]),
                    'metric8': _checkMultipleIds(properties[t.LANDMARK_ID_KEY]),
                    'metric9': _checkMultipleIds(properties[t.CHAT_SCORE_KEY]),
                    'metric13': _checkMultipleIds(properties[t.ALLIANCE_ID_KEY]),
                    'nonInteraction': properties[t.NON_INTERACTION_KEY]
                };
                ga('send', 'event', properties[t.CATEGORY_KEY], event, properties[t.LABEL_KEY], properties[t.VALUE_KEY] || undefined, fieldsObject);

                if((INTERACTIVE_EVENTS.CATEGORIES.indexOf(properties[t.CATEGORY_KEY]) > -1 ||
                    INTERACTIVE_EVENTS.EVENTS.indexOf(event) > -1 ||
                    INTERACTIVE_EVENTS.LABELS.indexOf(properties[t.LABEL_KEY]) > -1) &&
                    (!properties[t.VALUE_KEY] || typeof properties[t.VALUE_KEY].indexOf != "function" || 
                    properties[t.VALUE_KEY].indexOf(INTERACTIVE_EVENTS.EXCLUDE_NAMES_MATCHING) === -1)) {
                    Box.Application.broadcast('interactiveEventTriggered');
                }
            }
        }

        function _removeDuplicates(val){
            let uniqueIds = [];
                val = val && val.toString().split(",")||[];
                uniqueIds = val.filter(function(item, pos) {
                    return val.indexOf(item) == pos;
                });
                return _checkMultipleIds(uniqueIds.join(','));
        }

        function _checkMultipleIds(val){
            return val ? ((val.toString().split(",").length > 1 ) ? val : parseInt(val)) : undefined;
        }

        return {
            trackEvent
        };
    });

    return Box.Application.getService(SERVICE_NAME);
});
