define(["services/localStorageService"], (localStorageService) => {
    'use strict';
    const SERVICE_NAME = 'AmplitudeTrackingService';

    Box.Application.addService(SERVICE_NAME, () => {
        var trackAmplitude;
        if (document.readyState.toLowerCase() === 'complete') {
            loadAmplitude();
        } else {
            $(window).load(loadAmplitude);
        }

        let MessageQueue = function({type, key, maxSize}){
            this.type = type;
            this.key = key || type;
            this.maxSize = maxSize || 100;
            this.clearQueue = function(){
                localStorageService.deleteItem(this.key);
                return this;
            };
            this.items = function(){
                return localStorageService.getItem(this.key) || [];
            };
            return this;
        };

        MessageQueue.prototype.addToQueue = function(data){
            let item = this.items();
            if(item.length < this.maxSize){
                item.push(data);
                localStorageService.setItem(this.key, item);
            }
            return this;
        };

        MessageQueue.prototype.getQueuedItems = function(options = {}){
            let item = this.items();
            if(options.clearQueue){
                this.clearQueue();
            }
            return item;
        };

        let queue = new MessageQueue({type: "amplitude"});

        function _fireQueuedEvents(){
            let items = queue.getQueuedItems({clearQueue: true});
            for(var key in items){
                trackEvent(items[key].event, items[key].properties);
            }
        }

        function loadAmplitude(){
          require(['trackAmplitude'],(trackAmplitudeFile)=>{
              if(trackAmplitudeFile && window.AMPLITUDE_TRACKING_ID){
                  trackAmplitudeFile.getInstance().init(window.AMPLITUDE_TRACKING_ID);
                  trackAmplitude = trackAmplitudeFile;
                  _fireQueuedEvents();
              }
          });
        }
        function trackEvent(event, properties) {
            if (typeof trackAmplitude != 'undefined'  && typeof trackAmplitude.getInstance === "function") {
                trackAmplitude.logEvent(event,properties);
            } else {
                queue.addToQueue({event, properties});
            }
        }

        // /**
        //  * userId : string
        //  * traits : object with all properties
        //  */
        function trackIdentity(userId, traits) {
            if (typeof trackAmplitude != 'undefined'  && typeof trackAmplitude.getInstance === "function" ) {
                trackAmplitude.getInstance().setUserId(userId);
                trackAmplitude.getInstance().setUserProperties(traits);
            }
        }

        function trackPageView(pageType, onmitureData) {
            let data = $.extend(true,{
                    url: document.location.href,
                    path: document.location.pathname
                },onmitureData);
            trackEvent(pageType,data);


            if(window.serviceWorkerRegisteration){
                window.serviceWorkerRegisteration(function(){
                    trackIdentity(undefined, {
                        'pwaActive': true
                    });
                });
            }

        }

        return {
            trackEvent,
            trackIdentity,
            trackPageView
        };
    });

    return Box.Application.getService(SERVICE_NAME);
});
