define(['services/utils','services/trackingService'], function(utils,trackingService) {
    'use strict';

    const SERVICE_NAME = 'LoadingService';

    Box.Application.addService(SERVICE_NAME, function(application) {
        const $ = application.getGlobal('jQuery');

        // function loadVizuryTracking(){
        //     (function() {
        //         try {
        //             var viz = document.createElement("script");
        //             viz.type = "text/javascript";
        //             viz.async = true;
        //             //PIXEL_ACCOUNT_ID
        //             viz.src = ("https:" == document.location.protocol ?"https://in-tags.vizury.com" : "http://in-tags.vizury.com")+ "/analyze/pixel.php?account_id=" + window.PIXEL_ACCOUNT_ID;

        //             var s = document.getElementsByTagName("script")[0];
        //             s.parentNode.insertBefore(viz, s);
        //             viz.onload = function() {
        //                 try {
        //                     window.pixel.parse();
        //                 } catch (i) {
        //                 }
        //             };
        //             viz.onreadystatechange = function() {
        //                 if (viz.readyState == "complete" || viz.readyState == "loaded") {
        //                     try {
        //                         window.pixel.parse();
        //                     } catch (i) {
        //                     }
        //                 }
        //             };
        //         } catch (i) {
        //         }
        //     })();
        // }

        // function loadGTM() {
        //     (function(w, d, s, l, i) {
        //         w[l] = w[l] || [];
        //         w[l].push({
        //             'gtm.start': new Date().getTime(),
        //             event: 'gtm.js'
        //         });
        //         var f = d.getElementsByTagName(s)[0],
        //             j = d.createElement(s),
        //             dl = l != 'dataLayer' ? '&l=' + l : '';
        //         j.async = true;
        //         j.src = '//www.googletagmanager.com/gtm.js?id=' + i + dl;
        //         f.parentNode.insertBefore(j, f);
        //     })(window, document, 'script', 'dataLayer', 'GTM-NPHWR8');
        // }

        // function loadGA() {
        //     (function(i, s, o, g, r, a, m) {
        //         i['GoogleAnalyticsObject'] = r;
        //         i[r] = i[r] || function() {
        //             (i[r].q = i[r].q || []).push(arguments);
        //         }, i[r].l = 1 * new Date();    //jshint ignore:line
        //         a = s.createElement(o);
        //         m = s.getElementsByTagName(o)[0];
        //         a.async = 1;
        //         a.src = g;
        //         m.parentNode.insertBefore(a, m);
        //     })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        //     // ga('create', 'UA-74526872-1', 'auto');
        //     window.ga('create', window.GA_TRACKING_ID, { 'useAmpClientId': true, 'siteSpeedSampleRate': 90 });
        //     window.ga('send', 'pageview');
        // }

        // function loadDTM() {
        //     let src = "//assets.adobedtm.com/564c704e1b52f6b221368fa1537c84e4521dd6ea/satelliteLib-" + window.DTM_TRACKING_ID + ".js",
        //         script = $("<script type='text/javascript' onload='_satellite.pageBottom();'>").attr("src", src).attr("async", true);
        //     $('head').append(script);
        // }

        function loadAllTracking(pageObj) {
            // loadNewRelic();
            // loadGTM();
            // loadGA();
            // loadMixpanel();
            //loadDTM();
            //loadAudienceManager(pageObj);
          //  loadVizuryTracking();
            trackingService.trackPageView(pageObj);

            // exposing track event function
            window.trackEventOld = utils.trackEvent;
            window.trackEvent = trackingService.trackEvent;
            window.trackPageView = trackingService.trackPageView;
        }

        return {
            // loadGTM,
            // loadGA,
            // loadDTM,
            loadAllTracking
        };
    });

    return Box.Application.getService(SERVICE_NAME);
});
