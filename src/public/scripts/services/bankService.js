'use strict';
define(['services/utils',
    'common/sharedConfig',
    'services/apiService',
    'services/commonService',
    'services/utils'
], (Utils, sharedConfig, apiService,commonService,utilService) => {
    const SERVICE_NAME = 'bankService';
    Box.Application.addService(SERVICE_NAME, () => {
        function _parseBanks(cList) {
            var nList = [];var idList=[];
            $.each(cList, function(key, val) {
                idList.push(val.id);
                nList.push({
                    value: val.id,
                    label: val.name.indexOf('(') > 0 ? val.name.substr(0, val.name.indexOf('(')) : val.name,
                    text: `(${val.minInterestRate} %)`
                });
            });
            Box.Application.broadcast("saveAllBanksId",{idList});
            return nList;
        }

        let getBankApplicationTracking = (userId, successCallback, errorCallback)=>{
            apiService.get(sharedConfig.apiHandlers.getBankApplicationTracking(userId).url).then(successCallback,errorCallback);
        };

        function getBanks() {
            var _this = this;
            if (!_this.banks) {
                _this.banks = commonService.bindOnLoadPromise().then(() => {
                    return apiService.get(sharedConfig.apiHandlers.getBankDetails().url).then(function(response) {
                            if (response && response.results && response.results.length) {
                                return _parseBanks(response.results);
                            } else {
                                _this.banks = null;
                                return [];
                            }
                        },
                        function() {
                            _this.banks = null;
                        }
                    );
                });
            }
            return _this.banks;
        }
         

        return {
            getBankApplicationTracking,
            getBanks
        }
    });
    return Box.Application.getService(SERVICE_NAME);
});
