'use strict';
define([
    'services/utils',
    'services/loggerService',
    'services/urlService',
    'services/commonService',
    'services/defaultService',
    'services/trackingService',
    'services/requirmentNotificationService',
    'common/sharedConfig',
    'common/trackingConfigService'
], (utils, Logger, urlService, CommonService, DefaultService, trackingService, RequirmentNotificationService, SharedConfig, t) => {
    let SERVICE_NAME = 'AjaxifyService';

    Box.Application.addService(SERVICE_NAME, (application) => {
        const $ = application.getGlobal('jQuery');

        function prontoRequest() {
            //RequirmentNotificationService.setReferrer();
            Logger.log('Pronto Request Event');
            window.nanobar.go(20);
        }

        function prontoBeforeLoad() {
            window.nanobar.go(40);
            let el = Box.DOM.query(document, "#content");
            CommonService.stopAllModules(el);
            el = Box.DOM.query(document, "#outside-container");
            CommonService.stopAllModules(el);
            el = Box.DOM.query(document, "#page-popup-container");
            CommonService.stopAllModules(el);
            el = Box.DOM.query(document, "#seofooter-wrap");
            CommonService.stopAllModules(el);
            Logger.log('Pronto Before Load Event');
        }

        function prontoLoad() {
            window.nanobar.go(60);
            Logger.log('Pronto Load Event');
            urlService.setPrevLocation(window.location);

        }


        function prontoRender() {
            window.nanobar.go(80);
            Logger.log('Pronto Render Event');
        }

        function prontoCssLoaded() {
            window.nanobar.go(100);
            application.broadcast("pageLoaded");
            let el = Box.DOM.query(document, "#content"),
                pageModule = CommonService.getPageModule(),
                dependencies = CommonService.getAllPageDependencies(pageModule);

            CommonService.removePageModule();
            utils.triggerPageInteractive();
            DefaultService.trackCookies();

            require(dependencies, () => {
                CommonService.startAllModules(el, pageModule.path, () => {
                    if (pageModule.el) {
                        pageModule.el.setAttribute("data-module", pageModule.name);
                    }
                    Logger.log('Page rendered ...');
                });
                CommonService.loadLazyModules(el);
                CommonService.loadAutoLazyModules(el);

                el = Box.DOM.query(document, "#outside-container");
                CommonService.startAllModules(el);
                CommonService.loadLazyModules(el);
                CommonService.loadAutoLazyModules(el);

                el = Box.DOM.query(document, "#page-popup-container");
                CommonService.startAllModules(el);
                CommonService.loadLazyModules(el);
                CommonService.loadAutoLazyModules(el);

                el = Box.DOM.query(document, "#seofooter-wrap");
                CommonService.startAllModules(el);
                CommonService.loadLazyModules(el);
                CommonService.loadAutoLazyModules(el);

                el = Box.DOM.query(document, "#verticalscrollspy");
                CommonService.startAllModules(el);
                CommonService.loadLazyModules(el);
                CommonService.loadAutoLazyModules(el);
                
                setTimeout(CommonService.restartImageObserver);

            });

            if(!utils.isMobileRequest() && utils.getPageData('isMap')){
                $('body').addClass('dmap-stopscroll'); // to handle scroll on map
            }else {
                $('body').removeClass('dmap-stopscroll'); // to handle scroll on map
            }

            // to Hide/Show footer based on page
            if (utils && utils.getPageData('hideFooter')) {
                $('[data-page-footer]').css('display', 'none');
            } else {
                $('[data-page-footer]').css('display', 'block');
            }
            $('body').removeClass('openmenu');

            if(utils.getPageData('listingType')){
                application.broadcast('updateOnlyHeaderBuyRentLabel',{listingType: utils.getPageData('listingType')});
            }
            // tracking event on scroll
            bindScrollForTracking();
            // setTimeout(function() {
            //     RequirmentNotificationService.openUserRequirmentPopup();
            // });
            let pageType = utils.getPageData('pageType');
            if (pageType && pageType.indexOf("PROJECT_SERP") != -1) {
                application.broadcast('hideBuyRentHeader');
            } else {
                application.broadcast('showBuyRentHeader');
            }

            //re-init Trovit Tracking
            CommonService.loadTrovitTracking();

        }

        function scrollHandler() {
            var cutoff = Math.floor($(document).outerHeight()/2),
                scrollTop = $(window).scrollTop();
                if(scrollTop > cutoff){
                    trackingService.trackEvent(t.HALF_PAGE_VIEW_EVENT, {
                        [t.CATEGORY_KEY]: t.NAVIGATION_CATEGORY,
                        [t.NON_INTERACTION_KEY]: 1
                    });
                    unbindScrollForTracking();
                }
        }

        function prontoError() {
            window.nanobar.go(100);
        }
        function bindProntoEvents() {
            // pronto.request – Fired before new Pronto request is made
            $(window).on('pronto.request', prontoRequest);

            // pronto.beforeload – Fired before new Pronto request is loaded
            $(window).on('pronto.beforeload', prontoBeforeLoad);

            // pronto.load – Fired after new Pronto request is loaded
            $(window).on('pronto.load', prontoLoad);

            // pronto.render – Fired after new Pronto request is rendered
            $(window).on('pronto.render', prontoRender);

            // css.loaded - Fired when css has been loaded
            $(window).on('pronto.cssLoaded', prontoCssLoaded);

            // pronto.error – Fired when $.ajax() fails
            $(window).on('pronto.error', prontoError);

            //bind scroll tracking
            bindScrollForTracking();
        }

        function unBindProntoEvents() {
            // pronto.request – Fired before new Pronto request is made
            $(window).off('pronto.request', prontoRequest);

            // pronto.beforeload – Fired before new Pronto request is loaded
            $(window).off('pronto.beforeload', prontoBeforeLoad);

            // pronto.load – Fired after new Pronto request is loaded
            $(window).off('pronto.load', prontoLoad);

            // pronto.render – Fired after new Pronto request is rendered
            $(window).off('pronto.render', prontoRender);

            // css.loaded - Fired when css has been loaded
            $(window).on('pronto.cssLoaded', prontoCssLoaded);
        }

        function bindScrollForTracking(){
            window.addEventListener('scroll', scrollHandler,{passive:true});
        }
        function unbindScrollForTracking(){
            window.removeEventListener('scroll', scrollHandler,{passive:true});
        }

        // pronto.submit – Fired when a “form” button is pressed
        // pronto.error – Fired when $.ajax() fails
        // pronto.idle – Fired on specified interval of user inactivity
        // pronto.active – Fired when user becomes active (again)

        return {
            bindProntoEvents,
            unBindProntoEvents
        };

    });

    return Box.Application.getService(SERVICE_NAME);
});
