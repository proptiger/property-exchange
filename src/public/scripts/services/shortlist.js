/*
 * Service to add, remove and check shortlisted items
 * uses localStorage
 * @author: [Aditya Jha]
 */

define([
    'services/localStorageService',
    'services/utils',
    'services/apiService',
    'common/sharedConfig',
    'services/loggerService'
], (localStorageService, utils, apiService, sharedConfig, logger) => {
    "use strict";
    const SERVICE_NAME = 'shortlistService';

    Box.Application.addService(SERVICE_NAME, () => {

        const SHORTLIST_KEY = 'MAKAAN_SHORTLISTS',
            LISTING_TYPE = 'listing',
            PROJECT_TYPE = 'project';

        var loggedInData = {},
            isLoggedIn = false;

        function _getCurrentPosition(items, queryItem) {
            let id = "id";
            if(queryItem.type == 'project') {
                id = "projectId";
            }
            for(let i=0;i<items.length;i++) {
                if(items[i][id] == queryItem[id]) {
                    return i;
                }
            }
            return -1;
        }

        function syncShortlists(){
            let listings = [],
                projects = [];
            apiService.get(sharedConfig.apiHandlers.shortlist().url).then(response=>{
                listings = getUnsyncLocal(LISTING_TYPE, response);
                projects = getUnsyncLocal(PROJECT_TYPE, response);
                postShortlists(listings, projects).then(results=>{
                    response.listing = response.listing.concat(results.listing);
                    response.project = response.project.concat(results.project);
                    isLoggedIn = true;
                    loggedInData = response;
                    localStorageService.deleteItem(SHORTLIST_KEY);
                    Box.Application.broadcast("favourites-synced");
                }, (error)=>{
                    logger.error("error in sync shortlist post ", error);
                });
            }, error => {
                logger.error("error in sync shortlist", error);
            });
        }

        /*
            listings: [{ listingId, projectId}],
            projects: [{ projectId}]
        */
        function postShortlists(listings, projects){
            listings = listings ? JSON.stringify(listings) : undefined;
            projects = projects ? JSON.stringify(projects) : undefined;
            let query = {};
            if(listings){
                query.listings = listings;
            }
            if(projects){
                query.projects = projects;
            }
            return apiService.postJSON(sharedConfig.apiHandlers.shortlist({query}).url).then(results=>{
                return results;
            });
        }

        function setSyncedShortlist(){
            apiService.get(sharedConfig.apiHandlers.shortlist().url).then(response=>{
                isLoggedIn = true;
                loggedInData = response;
                Box.Application.broadcast("favourites-synced");
            }, error => {
                logger.error("error in setSyncedShortlist ", error);
            });
        }

        function getUnsyncLocal(type, apiData){
            let shortlists = getShortlists();
            if(shortlists && shortlists.hasOwnProperty(type)) {
                let thisTypeShortlists = shortlists[type];
                let thisTypeUnsyncItems = thisTypeShortlists.filter(thisTypeShortlist =>{
                    if(type === 'project'){
                        return !checkShortlist({
                            type: type,
                            projectId: thisTypeShortlist.projectId
                        }, apiData);
                    } else if(type=== 'listing'){
                        return !checkShortlist({
                            type: type,
                            id: thisTypeShortlist.id
                        }, apiData);
                    }
                }).map(thisTypeUnsync=>{
                    return type === 'project' ? {
                        projectId: thisTypeUnsync.projectId
                        } : {
                        projectId: thisTypeUnsync.projectId,
                        listingId: thisTypeUnsync.id
                    };
                });
                return thisTypeUnsyncItems;
            }
            return;
        }

        function getShortlists(){
            if(isLoggedIn){
                return loggedInData;
            }
            let shortlists = localStorageService.getItem(SHORTLIST_KEY);
            if(!shortlists){
                shortlists = {
                    listing: [],
                    project: []
                };
                localStorageService.setItem(SHORTLIST_KEY, shortlists);
            }
            return shortlists;
        }

        function deleteAllLocalShortlists(){
            localStorageService.deleteItem(SHORTLIST_KEY);
            isLoggedIn = false;
            Box.Application.broadcast("favourites-synced");
            return;
        }

        /*
        * check if the parameter item is present in the shorlist
        * @param: queryItem: object with keys 'type', 'id', 'data' | 'type' and 'id' are necessary
        */
        function checkShortlist(queryItem, newData) {
            let shortlists = newData || getShortlists();
            let thisTypeShortlists = shortlists[queryItem.type];
            let position = _getCurrentPosition(thisTypeShortlists, queryItem);
            // if(position >= 0 && thisTypeShortlists[position].status) {
            //     return true;
            // }
            if(position >= 0){
                return true;
            }
            return false;
        }

        /*
        * add an item to the shorlist
        * @param: shortlistedItem: object with keys 'type', 'id', 'data' | 'type' and 'id' are necessary
        */
        function addShortlist(shortlistedItem) {
            // add the item to the shortlist if not present already
            let shortlists = getShortlists();
            let thisTypeShortlists = shortlists[shortlistedItem.type];
            let currentPosition = _getCurrentPosition(thisTypeShortlists, shortlistedItem);
            let shortlistType = shortlistedItem.type;
            if(currentPosition >= 0) {
                // thisTypeShortlists[currentPosition].status = 1;
                // thisTypeShortlists[currentPosition].timestamp = new Date().valueOf();
                return;
            } else {
                if(isLoggedIn){
                    if(shortlistType === 'listing'){
                        let listing = {
                            listingId: shortlistedItem.id,
                            projectId: shortlistedItem.projectId
                        };
                        postShortlists([listing]).then(result=>{
                            thisTypeShortlists.push(result.listing[0]);
                        }, error=>{
                            logger.error("error in add shortlist ", error);
                        });
                    } else if(shortlistType === 'project'){
                        let project = {
                            projectId: shortlistedItem.projectId
                        };
                        postShortlists(undefined, [project]).then(result=>{
                            thisTypeShortlists.push(result.project[0]);
                        }, error=>{
                            logger.error("error in add shortlist ", error);
                        });
                    }
                    return;
                }
                thisTypeShortlists.push({
                    status: 1,
                    timestamp: new Date().valueOf(),
                    id: shortlistedItem.id,
                    projectId: shortlistedItem.projectId,
                    data: shortlistedItem.data
                });
                localStorageService.setItem(SHORTLIST_KEY, shortlists);
            }
        }

        /*
        * remove and already shortlisted item
        * this function assumes that the parameter being passed is present in shortlist
        * @param: shortlistedItem: object with keys 'type', 'id', 'data' | 'type' and 'id' are necessary
        */
        function removeShortlist(shortlistedItem) {
            let shortlists = getShortlists();
            let thisTypeShortlists = shortlists[shortlistedItem.type];
            let index = -1,
                id = "id",
                wishListId;
            if(shortlistedItem.type == 'project') {
                id = "projectId";
            }
            for(let i=0;i<thisTypeShortlists.length;i++) {
                if(thisTypeShortlists[i][id] == shortlistedItem[id]) {
                    index = i;
                    wishListId = thisTypeShortlists[i].wishListId;
                    // thisTypeShortlists[i].status = 0;
                    break;
                }
            }
            if(index !== -1) {
                if(isLoggedIn && wishListId){
                    deleteShortlist(wishListId).then(result=>{   //jshint ignore:line
                        thisTypeShortlists.splice(index, 1);
                    }, error =>{
                        logger.error("error in remove shortlist ", error);
                    });
                    return;
                }
                thisTypeShortlists.splice(index, 1);
                shortlists[shortlistedItem.type] = thisTypeShortlists;
                localStorageService.setItem(SHORTLIST_KEY, shortlists);
            }
        }

        function deleteShortlist(wishListId){
            let query = {wishListId};
            return apiService.deleteEntry(sharedConfig.apiHandlers.shortlist({query}).url).then(response=>{
                return response;
            });
        }

        function _getItemsCount(shortlistType) {
            let shortlists = getShortlists();

            // let thisTypeShortlists = shortlists[shortlistType];
            // let count = 0;
            // for(let i=0; i<thisTypeShortlists.length; i++) {
            //     if(thisTypeShortlists[i].status) {
            //         count++;
            //     }
            // }
            return shortlists[shortlistType].length;
        }

        function getAllShortlist(shortlistType, count=_getItemsCount(shortlistType)) {
            let shortlists = getShortlists();
            let thisTypeShortlists = shortlists[shortlistType];
            // let ret = [];
            // for(let i=0; i<thisTypeShortlists.length && count>0; i++) {
            //     if(thisTypeShortlists[i].status) {
            //         ret.push(thisTypeShortlists[i]);
            //         count--;
            //     }
            // }
            return thisTypeShortlists.slice(0, count);
        }

        return {
            checkShortlist,
            addShortlist,
            removeShortlist,
            getAllShortlist,
            syncShortlists,
            setSyncedShortlist,
            deleteAllLocalShortlists
        };
    });

    return Box.Application.getService(SERVICE_NAME);
});