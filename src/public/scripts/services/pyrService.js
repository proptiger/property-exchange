'use strict';
define(['common/sharedConfig',
    'services/apiService',
    'services/utils',
    'services/trackingService',
    'services/localStorageService',
    'services/filterConfigService',
    'services/leadPyrService'
], function(sharedConfig) {
    const SERVICE_NAME = 'pyrService';
    Box.Application.addService('pyrService', function(application) {

        const apiService = application.getService('ApiService'),
            //trackingService = application.getService('TrackingService'),
            utils = application.getService('Utils'),
            localStorageService = application.getService('localStorageService'),
            filterConfigService = application.getService('FilterConfigService'),
            leadPyrService = application.getService('LeadPyrService'),
            COOKIE_NAME = "enquiry_info",
            LOGIN_COOKIE = "user_info",
            enquiryApi = sharedConfig.apiHandlers.postEnquiry().url,
            tempEnquiryApi = sharedConfig.apiHandlers.postTempEnquiry().url,
            propertyTypeConfig = filterConfigService.getFilterModuleConfig("propertyType").getTemplateData({
                listingType: 'buy'
            });

        var setEnquiryCookie = function(enquiryInfo) {
            leadPyrService.setEnquiryCookie(enquiryInfo);
        };

        var getLoginCookie = function() {
            let cookieData = utils.getCookie(LOGIN_COOKIE);
            if(cookieData && cookieData.length){
                try{
                     cookieData =  JSON.parse(cookieData);
                } catch(e) {
                }
            }
            return cookieData;
        };

        var deleteCookieKey = function(key) {
            let cookie = localStorageService.getItem(COOKIE_NAME);
            if(cookie && cookie.length) {
                try{
                    cookie =  JSON.parse(cookie);
                  } catch(e) {
                  }
            }
            if(cookie && cookie[key]) {
                delete cookie[key];
                localStorageService.setItem(COOKIE_NAME, JSON.stringify(cookie));
            }
        };

        var getEnquiryCookie = leadPyrService.getEnquiryCookie;

        var submitEnquiry = function(enquiryInfo, temp=false){
            var apiUrl = temp ? tempEnquiryApi : enquiryApi;
            var _enquiryInfo =  $.extend(true,{},enquiryInfo);
             if(_enquiryInfo.localityIds && _enquiryInfo.localityIds.length) {
                  _enquiryInfo.localityIds = enquiryInfo.localityIds.filter(localityIds => localityIds.type.toLowerCase() === 'locality').map(localityIds => localityIds.id);
                  _enquiryInfo.suburbIds   = enquiryInfo.localityIds.filter(localityIds => localityIds.type.toLowerCase() === 'suburb').map(localityIds => localityIds.id);
             }

             if(enquiryInfo.bhk && enquiryInfo.bhk.length) {
                 let index = enquiryInfo.bhk.indexOf("3plus");
                 if(index >= 0 ) {
                     _enquiryInfo.bhk.splice(index,1);
                     Array.prototype.push.apply(_enquiryInfo.bhk, ["4","5","6","7","8","9","10"]);
                 }
                 enquiryInfo.bhk = enquiryInfo.bhk.filter(n => n );
             }

             ["localityIds","suburbIds","propertyTypes"].forEach(function(key){
                 if(_enquiryInfo[key] && !_enquiryInfo[key].length) {
                     delete _enquiryInfo[key];
                 }
             });
             return leadPyrService.postLead(apiUrl,_enquiryInfo);
         };

       var _getPropertyTypeById = function(id) {
             let res = propertyTypeConfig.filter((obj) => {
                 if (obj.id == id) {
                     return true;
                 }
             });
             return (res[0] || {}).value;
       };

       var _getSavedSearchName = function(data) {
           let savedName;
           if(data.localityIds && data.localityIds.length ) {
               savedName =  (data.localityIds.length <= 1 ) ? data.localityIds[0].name : data.localityIds[0].name + "+" + (data.localityIds.length - 1);
           } else if(data.cityName) {
               savedName = data.cityName;
           } else if(data.bhks && data.bhks.length) {
               savedName = data.bhks.join(",") + "BHK";
               savedName += data.minBudget + " " +  data.maxBudget + " Budget";
           } else {
               savedName = "Save Search 1";
           }
           savedName =  savedName ? savedName : "Save Search 1";
           return savedName;
       };

       var saveSearch = function(data) {
           let email = data.email,
               apiUrl = window.location.pathname,
               filterUrl,
               filtersSet = {},
               savedName,
               trackLabel = "";

           filtersSet.beds = data.bhk;
           filtersSet.listingType = data.salesType;
           filtersSet.minPrice = data.minBudget;
           filtersSet.maxPrice = data.maxBudget;
           if(data.propertyTypes && data.propertyTypes.length) {
               filtersSet.propertyType = data.propertyTypes.map(elem => _getPropertyTypeById(elem) );
           }
           if(data.localityIds && data.localityIds.length) {
                filtersSet.localityOrSuburbId = data.localityIds.filter(localityIds => localityIds.type.toLowerCase() === 'locality').map(localityIds => localityIds.id);
                filtersSet.localityOrSuburbId = filtersSet.localityOrSuburbId.concat(data.localityIds.filter(localityIds => localityIds.type.toLowerCase() === 'suburb').map(localityIds => localityIds.id));
           }

           savedName = _getSavedSearchName(data);
           filterUrl = utils.generateSerpFilter(filtersSet).replace("?","");
           trackLabel = filterUrl.replace(/=/g, ":").replace(/&/g, ";");
           apiUrl += '?name=' + savedName + '&' + filterUrl + '&savedSearches=true&nonLoggedIn=true&email='+ email;
           return apiService.get(apiUrl).then((response) => {
               Box.Application.broadcast('trackPyrSetAlert', {trackLabel});
               return response;
           },(err) => {
               Box.Application.broadcast('trackPyrSetAlertError', {error:err.responseJSON && err.responseJSON.body && err.responseJSON.body.error && err.responseJSON.body.error.msg});
               return err;
           });
       };

        var getPropertyAdvisors =  leadPyrService.getTopSellers;

        var updateLeadByPUT = function(userEnquiryId, data, otp, temp = false) {
            var url = temp ? tempEnquiryApi : enquiryApi,
                putData = {
                    userEnquiryId: userEnquiryId,
                    otp: otp,
                    phone: data.phone,
                    userId: data.userId
                };
                if(data.name){
                  putData.name = data.name;
                }
                if(data.email){
                  putData.email = data.email;
                }
            return leadPyrService.verifyOTP(url,putData);
        };


        var postTempLead = function(enquiryInfo){
            return submitEnquiry(enquiryInfo, true);
        };

        var stripQuotes = leadPyrService.stripQuotes;


        return {
            setEnquiryCookie,
            getEnquiryCookie,
            submitEnquiry,
            updateLeadByPUT,
            getPropertyAdvisors,
            deleteCookieKey,
            getLoginCookie,
            saveSearch,
            postTempLead,
            stripQuotes
        };

    });
    return Box.Application.getService(SERVICE_NAME);
});
