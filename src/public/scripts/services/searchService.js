"use strict";
define([
    'common/sharedConfig',
    'services/commonService',
    'services/apiService',
    'services/defaultService',
    'services/localStorageService',
    'services/loggerService',
    'services/typeAheadService',
    'services/utils',
    'services/reverseParseListingSelector',
    'services/filterConfigService'
], function(sharedConfig, commonService, apiService, defaultService, localStorageService, loggerService, typeaheadService, utilService, reverseParser, filterConfigService) {
    const SERVICE_NAME = 'SearchService';
    Box.Application.addService(SERVICE_NAME, function() {

        let cityObj, defaultSearchSuggestions = [],
            recentSearches, tempPromises = [],
            recentSearchCount = 5;
        const RECENT_VISITED_KEY="MAKAAN_RECENTS",
            ENQUIRY_KEY="enquiry_info";

        function _getCity() {
            let pageData = utilService.getPageData(),
                cityId = pageData.cityId,
                cityName = pageData.cityName;

            if (cityId && cityName) {
                return new Promise(function(resolve) {
                    let city = {
                        id: cityId,
                        label: cityName
                    };
                    resolve(city);
                });
            } else {
                return defaultService.getCityByLocation().then((response) => {
                    return response;
                }, (response) => {
                    return response;
                });
            }
        }

         var _parseTopLocalities = function(localities, alreadySelected) {

            let category = utilService.getPageData('listingType'),
                urlKey = category && category.toLowerCase() == 'rent' ? 'rentUrl' : 'buyUrl',
                currentCity = alreadySelected && alreadySelected.length ? alreadySelected[0].dataset.city.toLowerCase() : '';
            // Add top localities
            let topLocalities = [];
            for (let i = 0; i < localities.length; i++) {

                let locality = localities[i],
                    cityName = locality.suburb && locality.suburb.city ? locality.suburb.city.label : '';

                if(currentCity && cityName.toLowerCase() != currentCity){
                    continue;
                }

                let obj = {
                    id: "TYPEAHEAD-LOCALITY-" + locality.localityId,
                    type: "LOCALITY",
                    showType: 'location',
                    displayText: `${locality.label} - ${cityName}`,
                    redirectUrl: locality[urlKey],
                    latitude: locality.latitude,
                    longitude: locality.longitude,
                    city: cityName,
                    entityId: locality.localityId,
                    entityName: locality.label
                };
                topLocalities.push(obj);
            }
            return topLocalities;
        };

        let suggestionResults;

        var _getDefaultSearchSuggestions = function(city) {

            if (suggestionResults) {
                return new Promise(function(resolve) {
                    resolve(suggestionResults);
                });
            }

            let promises = [],
                promisesData = {};

            function _overviewLinks(localities) {
                //add overview links
                let overviewLinks = [];
                if (localities && localities.length) {
                    let temp = {
                        id: "LOCALITY-OVERVIEW-" + localities[0].localityId,
                        type: "LOCALITY-OVERVIEW",
                        showType: 'overview',
                        displayText: 'Know more about ' + localities[0].label + ' - ' + localities[0].suburb.city.label,
                        redirectUrl: localities[0].overviewUrl
                    };
                    overviewLinks.push(temp);
                }

                return overviewLinks;
            }


            // abort all pending apis for search
            abortAllPendingAPis();

            if(city.id){
                /*tempPromises.push(apiService.get(sharedConfig.clientApis.getCityTopLocalities({
                    cityId: city.id
                }).url));

                promises.push(tempPromises[0].then((response) => {
                    promisesData.topLocalities = response.data;
                }));*/

                /*tempPromises.push(apiService.get(sharedConfig.clientApis.getPopularSuggestions({
                    entityId: city.id,
                    rows: 2
                }).url));*/
            }


            /*promises.push(tempPromises[0].then((response) => {
                promisesData.topLocalities = response.data;
            }));

            (promises.push(tempPromises[1].then((response) => {
                promisesData.popularSuggestions = response.data;
            }));*/


            return commonService.bindOnLoadPromise().then(() => {

                return Promise.all(promises).then(() => {
                    let result = [];
                    let topLocalities = _parseTopLocalities(promisesData.topLocalities || []),
                        overviewLinks = _overviewLinks(promisesData.topLocalities || []),
                        popularSuggestions = promisesData.popularSuggestions || [];

                    if (topLocalities && topLocalities.length > 0) {
                        result.push({
                            type: "Top Localities",
                            values: topLocalities
                        });
                    }

                    if (overviewLinks && overviewLinks.length > 0) {
                        result.push({
                            type: "Know more",
                            values: overviewLinks
                        });
                    }

                    if (popularSuggestions && popularSuggestions.length > 0) {
                        result.push({
                            type: "Popular Suggestions",
                            values: popularSuggestions
                        });
                    }

                    suggestionResults = result;
                    return result;

                }, () => {
                    loggerService.error(`default suggestions api erorr`);
                    return [];
                });
            });

        };

        let userSoftCity;
        function _updateUserSoftCity(){
            let recentSearchesLength = recentSearches  && recentSearches.length,
                cityName = utilService.getPageData('cityName');

            if(cityName){
                userSoftCity = cityName;
            }else if(recentSearchesLength){
                let length = recentSearchesLength > 3 ? 3 : recentSearchesLength;
                for(var i=0; i<length; i++){
                    if(recentSearches[i].city && recentSearches[i].city != 'undefined'){
                        cityName = recentSearches[i].city;
                        userSoftCity = cityName;
                        break;
                    }
                }
            }

            if(!cityName){
                _getCity().then((city) => {
                    cityObj = city;
                    cityName = cityObj && cityObj.label;
                    userSoftCity = cityName || userSoftCity;
                });
            }

        }

        function _updateCityAndSuggestions(skipSearchSuggestions) {

            let listingType = utilService.getPageData('listingType') || 'buy';
            recentSearches = localStorageService.getRecentSearches(recentSearchCount, listingType); //// get recent search from typeahead

            _updateUserSoftCity();

            return _getCity().then((city) => {
                cityObj = city;
            }).then(() => {
                if (cityObj && !skipSearchSuggestions && !recentSearches) {
                    return _getDefaultSearchSuggestions(cityObj).then((suggestions) => {
                        defaultSearchSuggestions = suggestions;
                        return defaultSearchSuggestions;
                    }, () => {
                        return defaultSearchSuggestions;
                    });
                }
            }, () => {
                return defaultSearchSuggestions;
            });
        }
        _updateCityAndSuggestions();

        function abortAllPendingAPis() {
            for (let i = 0; i < tempPromises.length; i++) {
                try {
                    tempPromises[i].abort();
                } catch (e) {
                    console.log('in catch inside searchService function abortAllPendingAPis of abort', e);
                }
            }
            tempPromises = [];
        }

        var _filterResults = function(results, alreadySelected) {
            var newResults = [], selectedIds = [];
            for(let i in alreadySelected){
                selectedIds.push(alreadySelected[i].tagid);
            }
            for (let i in results) {
                if (results[i] && results[i].id && selectedIds.indexOf(results[i].id) == -1) {
                    newResults.push(results[i]);
                }
            }
            return newResults;
        };

        function _setTypeToShow(data) {

            if (!data) {
                return;
            }

            var type = data.type ? data.type.toLowerCase() : '',
                showType = '';
            //create google place id url
            if (type == 'gp') {
                showType = 'landmark';
            } else if(type=='city'){
                showType = 'city';
            } else if (['locality', 'suburb'].indexOf(type) > -1) {
                showType = 'location';
            }else if (type === 'builder' || type === 'buildercity' ) {
                showType = 'builder';
            } else if (type === 'project') {
                showType = 'project';
            } else if (type.indexOf('overview') > -1) {
                showType = 'overview';
            }

            data.showType = showType;
        }

        var _fetchTypeAheadResponse = function (searchParams, config, selectedValues) {

            // abort all pending apis for search
            abortAllPendingAPis();

            tempPromises.push(typeaheadService.fetchSearchResults({
                searchParams, config
            }));

            return tempPromises[0].then(function(response) {

                let category = utilService.getPageData('listingType'),
                    listingType = category ? (category.toLowerCase() == 'rent' ? 'rent' : (category.toLowerCase() == 'commercialbuy' ? 'commercialBuy' : (category.toLowerCase() == 'commerciallease' ? 'commercialLease' : 'buy'))) : 'buy';

                // filter duplicate results
                response.data = _filterResults(response.data, selectedValues);

                let result = [],
                    matchingResults = [],
                    overviewLinks = [],
                    popularLinks = [];



                for (let i in response.data) {
                    let r = response.data[i],
                        type = r.type ? r.type.toLowerCase() : '';
                        //href;

                    _setTypeToShow(r);

                    if(r.isSuggestion) {
                        let parsedSelector = reverseParser.reverseParseSelector(r.redirectUrlFilters);
                        if(parsedSelector.projectId) {
                            parsedSelector.pageType = 'PROJECT_URLS';
                            parsedSelector.projectName = r.entityName;
                            r.showType = 'project';
                            r.entityId = parsedSelector.projectId;
                            if(r.builderName) {
                               parsedSelector.builderName = r.builderName; 
                            }
                        } else if(parsedSelector.builderId) {
                            parsedSelector.pageType = 'BUILDER_TAXONOMY_URLS';
                            parsedSelector.builderName = r.entityName;
                            r.showType = 'builder';
                            r.entityId = parsedSelector.builderId;
                        } else if(parsedSelector.localityId) {
                            parsedSelector.pageType = 'LOCALITY_TAXONOMY_URLS';
                            parsedSelector.localityName = r.entityName;
                            r.showType = 'location';
                            r.entityId = parsedSelector.localityId;
                        }  else if(parsedSelector.suburbId) {
                            parsedSelector.pageType = 'SUBURB_TAXONOMY_URLS';
                            parsedSelector.suburbName = r.entityName;
                            r.showType = 'location';
                            r.entityId = parsedSelector.suburbId;
                        }  else if(parsedSelector.cityId) {
                            parsedSelector.pageType = 'CITY_TAXONOMY_URLS';
                            parsedSelector.cityName = r.entityName;
                            r.showType = 'location';
                            r.entityId = parsedSelector.cityId;
                        }
                        if(r.city) {
                            parsedSelector.cityName = r.city;
                        }
                        r.redirectUrl = filterConfigService.getUrlWithUpdatedParams({skipHref : false, skipPageData : true, skipFilterData : true, overrideParams : parsedSelector});
                    }
                    //create google place id url
                    if (type == 'gp' && r.entityName && r.googlePlaceId) {
                        r.displayText = `properties near ${r.displayText}`;
                        let label = r.entityName.replace(/&/g, 'and').replace(/\./g, '').replace(/,/g, '').replace(/ /g, '-').toLowerCase();
                        r.redirectUrl = `/${listingType}/nearby/${label}/${r.googlePlaceId}`;
                    }

                    if (type.indexOf('overview') > -1) {
                        overviewLinks.push(r);
                    } else if (type.indexOf('typeahead-suggestion') > -1) {
                        popularLinks.push(r);
                    } else {
                        matchingResults.push(r);
                    }
                }

                if (matchingResults.length > 0) {
                    result.push({
                        type: "",
                        values: matchingResults
                    });
                }
                if (overviewLinks.length > 0) {
                    result.push({
                        type: "Know more",
                        values: overviewLinks
                    });
                }
                if (popularLinks.length > 0) {
                    result.push({
                        type: "Popular Suggestions",
                        values: popularLinks
                    });
                }
                return result;
            });
        };

        var getResultItemObject = function(data) {
            let temp = utilService.deepCopy(data);
            temp.type = data.optiontype;

            _setTypeToShow(temp);

            return {
                id: temp.tagid,
                type: temp.tagtype,
                showType: temp.showType,
                displayText: temp.val,
                redirectUrl: temp.link,
                latitude: temp.latitude,
                longitude: temp.longitude,
                city: temp.city,
                cityId: temp.cityid,
                localityId: temp.localityid,
                entityId: temp.entityid,
                entityName: temp.entityname
            };
        };

       

        var getTypeaheadResults = function(data) {

            // updates city in case pageData has changed
            _updateCityAndSuggestions(true);

            let pageData = utilService.getPageData(),
                searchParams = data.searchParams,
                config = data.config,
                selectedValues = data.tagsArray,
                //name = config.name,
                searchKey = searchParams.query,
                searchLocalityOnly = config.searchLocalityOnly || false;

            // Return empty results if searchKey is less than 2 characters
            if (searchKey && searchKey.length < 2) {
                return [];
            }

            // Set multipleSearch flag
            var multipleSearch = false;
            if (selectedValues) {
                for (let sv in selectedValues) {
                    if (config && config.multiselect && config.multiselect.indexOf(selectedValues[sv].dataset.tagtype) > -1) {
                        multipleSearch = true;
                    } else {
                        multipleSearch = false;
                        break;
                    }
                }
            }

            if (!multipleSearch && !searchKey && !searchLocalityOnly) {

                if (recentSearches) {
                    return new Promise(function(resolve) {
                        let result = [{
                            type: "Recent Searches",
                            values: recentSearches
                        }];
                        resolve(result);
                    });
                }

                return _updateCityAndSuggestions().then((defaultSearchSuggestions) => {
                    return defaultSearchSuggestions;
                });

            } else if (!multipleSearch && searchKey && !searchLocalityOnly) {
                // get city,locality,suburb,builder,project,gp results from columbus
                let searchParams = {
                    query: searchKey,
                    rows: 5,
                    enhance: 'gp',
                    view: 'buyer',
                    category: pageData.listingType || 'buy',
                    usercity: userSoftCity
                };
                if (!userSoftCity && cityObj) {
                    // enhance results by city
                    searchParams.usercity = cityObj.label;
                }
                return _fetchTypeAheadResponse(searchParams, config, selectedValues);
            } else if (multipleSearch && !searchKey && !searchLocalityOnly) {
                // get localities nearby selected locality/suburb
                let refLocation = selectedValues[selectedValues.length - 1],
                    lat = refLocation.dataset.latitude,
                    lon = refLocation.dataset.longitude;
                
                abortAllPendingAPis(); // abort all pending apis for search
                if (lat && lon) {
                    tempPromises.push(apiService.get(sharedConfig.clientApis.nearbyLocalities({lat,lon}).url));
                }
                return tempPromises[0].then(function (response) {
                let result = [];
                // Add nearby localities
                let nearbyLocalities = _parseTopLocalities(response.data, selectedValues);
                        // filter duplicate results
                    nearbyLocalities = _filterResults(nearbyLocalities, selectedValues);
                    if (nearbyLocalities.length > 0) {
                        result.push({
                            type: "Nearby Localities",
                            values: nearbyLocalities
                        });
                    }
                    return result;
                });
            } else if ((multipleSearch && searchKey) || searchLocalityOnly) {
                // get locality/suburb results from columbus in the same city
                let cityName = selectedValues && selectedValues[0] && selectedValues[0].dataset && selectedValues[0].dataset.city || pageData.cityName;
                let searchParams = {
                    query: searchKey,
                    typeAheadType: 'locality,suburb',
                    city: cityName,
                    rows: 5,
                    view: 'buyer',
                    category: pageData.listingType || 'buy'
                };
                return _fetchTypeAheadResponse(searchParams, config, selectedValues);
            }
        };

        var syncSavedSearches = function(){
            if(window.syncSearch){
                let searches = [];
                $.each(window.syncSearch, (k, v)=>{
                    let obj = {};
                    obj['email'] = v.email;
                    obj['id'] = obj['id'] || [];
                    obj['id'].push(v.id);
                    searches.push(obj);
                });
                let apiUrl = window.location.pathname;
                    apiUrl += '?sync=' + encodeURI(JSON.stringify(searches)) + '&savedSearches=true';
                apiService.get(apiUrl).then(()=>{
                    window.syncSearch = undefined;
                },()=>{

                });
            }
        };
        var recentVisitedPropProject = function(count=1, category="listing"){
            let recentVisitedItems = localStorageService.getItem(RECENT_VISITED_KEY)
            let categoryObj = recentVisitedItems[category];
            if(categoryObj && categoryObj.length>0){
                return categoryObj.sort((a,b)=>{
                    return b.timestamp - a.timestampe
                }).slice(0,count)
            } 
            return null
        }
        var getListingSERP =function(serpURL,serpParams,onlyListingCount=false,searchType){
             return apiService.get(sharedConfig.apiHandlers.getListingSERP({serpURL,serpParams,onlyListingCount,searchType}).url)
        }

        var getMultipleListingSERP = function(queryParams){
            return apiService.get(sharedConfig.apiHandlers.getListingCount(queryParams).url);
        }
        var getTopAgentsURL =function(entityType,entityValue){
             return apiService.get(sharedConfig.apiHandlers.getTopAgentsURL({entityType,entityValue}).url)
        }
        var _getKeysFromObject = function (obj, key) {
            var values = [];
            for (let i in obj) {
                let val = obj[i];
                values.push(val[key]);
            }
            return values;
        }
        var _getDataForTagType = function (selectedItems, key, tagType) {
            let result = [];
            for (let tagid in selectedItems) {
                if (selectedItems[tagid]["type"] == tagType) {
                    result.push(selectedItems[tagid][key]);
                }
            }
            return result.length ? result : null;
        }
        var getCityLocalitySuburbOverrideData = function (selectedSearchTypeItems) {
            let result = {
                localityOrSuburbId: _getKeysFromObject(selectedSearchTypeItems, 'entityId').join(),
                cityId: _getKeysFromObject(selectedSearchTypeItems, 'cityId')[0],
                cityName: _getKeysFromObject(selectedSearchTypeItems, 'city')[0],
                listingType: utilService.getPageData('listingType'),
                pageType: utilService.getPageData('isMap') ? 'LISTINGS_PROPERTY_URLS_MAPS' : 'LISTINGS_PROPERTY_URLS',
                templateId: "MAKAAN_MULTIPLE_LOCALITY_LISTING_BUY"
            };
            let localityIds = _getDataForTagType(selectedSearchTypeItems, 'entityId', 'LOCALITY');
            if (localityIds) {
                result['localityId'] = localityIds;
            }
            let localityNames = _getDataForTagType(selectedSearchTypeItems, 'entityName', 'LOCALITY')
            if (localityNames) {
                result['localityName'] = localityNames;
            }
            let suburbIds = _getDataForTagType(selectedSearchTypeItems, 'entityId', 'SUBURB');
            if (suburbIds) {
                result['suburbId'] = suburbIds;
            }
            let suburbNames = _getDataForTagType(selectedSearchTypeItems, 'entityName', 'SUBURB');
            if (suburbNames) {
                result['suburbName'] = suburbNames;
            }
            return result;
        }
        return {
            getResultItemObject,
            getTypeaheadResults,
            abortAllPendingAPis,
            getCity: _getCity,
            syncSavedSearches,
            recentVisitedPropProject,
            getListingSERP,
            getTopAgentsURL,
            getMultipleListingSERP,
            getCityLocalitySuburbOverrideData
        };
    });

    return Box.Application.getService(SERVICE_NAME);
});
