'use strict';
define([
    'common/utilFunctions',
    'services/loggerService',
], (utils, logger) => {
    const SERVICE_NAME = 'localStorageService';

    Box.Application.addService(SERVICE_NAME, (app) => {

        const RECENT_SEARCH_KEY = "recent-search";

        function setItem(key, value) {
            try{
                window.localStorage.setItem(key, JSON.stringify(value));
            }catch(e){
                logger.error('in catch of setLocalStorageKey: ',JSON.stringify(e));
            }
        }

        function getItem(key) {
            let value;
            try{
                value = window.localStorage.getItem(key);
                value = JSON.parse(value);
            } catch(e){
                value = {};
            }
            return value;
        }

        function deleteItem(key) {
            return (delete window.localStorage[key]);
        }

        function setSessionItem(name, val){
            try{
                window.sessionStorage.setItem(name, JSON.stringify(val));
            }catch(e){
                logger.error('in catch of setLocalStorageKey: ',JSON.stringify(e));
            }
        }

        function getSessionItem(name) {
            let value;
            try{
                value = window.sessionStorage.getItem(name);
                value = JSON.parse(value);
            }catch(e){
                value = {};
            }
            return value;
        }

        function getRecentSearches(n, category) {
            var obj, lang='';
            try {
                obj = this.getItem(RECENT_SEARCH_KEY)[(category||'').toLowerCase()];
                lang = utils.getVernacLanguage() || '';
            } catch(e) {}
            if (!obj) return;
            return Object.values(obj).filter(v=>v.lang===lang).sort((i, j)=>(j.timestamp-i.timestamp)).slice(0, n);
        }

        function setRecentSearches(searchItem, category) {
            var lang = '', categoryObj, obj = this.getItem(RECENT_SEARCH_KEY) || {};
            try {
                lang = utils.getVernacLanguage() || '';
            } catch(e) {}
            category = (category||'').toLowerCase();
            categoryObj = obj[category] || {};
            categoryObj[searchItem.id] = Object.assign(searchItem, {
                lang,
                timestamp: (new Date()).valueOf(),
            });
            obj[category] = categoryObj;
            this.setItem(RECENT_SEARCH_KEY, obj);
            return obj;
        }

        return {
            setItem,
            getItem,
            deleteItem,
            getSessionItem,
            setSessionItem,
            getRecentSearches,
            setRecentSearches
        };
    });

    return Box.Application.getService(SERVICE_NAME);
});
