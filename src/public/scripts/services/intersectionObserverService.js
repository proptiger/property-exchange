// import { isContext } from "vm";

define([], () => {
    'use strict';
    const SERVICE_NAME = 'intersectionObserverService';

    Box.Application.addService(SERVICE_NAME, (context) => {

        let isObserverSupported = true,
            observer = null,
            selector = '',
            replaceWith = 'data-src',
            backgroundImagePatternSelector = "data-background-image",
            backgroundImageSelector = '.dummy-heroshot, .dummy-taxonomy, .dummy-agent-bground, .dummy-builder-bground, .dummy-locality-bground, .dummy-lifestyle, .dummy-similar-img, .dummy-placeholder, .dummy-bg, .dummy-overlay',
            images = [],
            backgroundImages = [],
            dummyBackgroundElements = [],
            observerConfig = {
                "rootMargin": /* Math.max(document.documentElement.clientWidth, window.innerWidth || 0)/2+ */ "500px 10px"
            },
            elementObserver = null,
            elementObserverConfig = {
                    threshold: [0.5]
            },
            elementBroadcaseMessages = {},
            onlyOnceObservedElements = [],
            lazyModuleObserver = null,
            inViewLazyModulesSelector = "[data-loadOn=inview]";
        
        // check for InteractionObserver Support
        if (!('IntersectionObserver' in window)) {
            isObserverSupported = false;
        } else {
            observer = new IntersectionObserver( _intersectionAction, observerConfig);
            elementObserver = new IntersectionObserver(_elementIntersectionAction, elementObserverConfig);
            lazyModuleObserver = new IntersectionObserver(_lazyModuleIntersectionAction, observerConfig);
        }

        function _intersectionAction(entries, observer){
            entries.forEach(entry => {
                let index, srcElem;
                if(entry.isIntersecting){
                    entry = entry.target;
                    index = images.indexOf(entry);
                    if(index>-1){
                        srcElem = images.splice(index,1)[0];
                        let src = srcElem && srcElem.getAttribute(replaceWith);
                        src && srcElem.setAttribute('src', src);  //jshint ignore:line
                    } else {
                        index = dummyBackgroundElements.indexOf(entry);
                        if(index>-1){
                            srcElem = dummyBackgroundElements.splice(index,1)[0];
                            srcElem && $(srcElem).removeClass(backgroundImageSelector.replace(/[.,]/g,''));  //jshint ignore:line
                        }
                        index = backgroundImages.indexOf(entry);
                        if(index>-1){
                            srcElem = backgroundImages.splice(index,1)[0];
                            let src = srcElem && srcElem.getAttribute(backgroundImagePatternSelector);
                            src && srcElem.setAttribute('style', "background-image:url('" + src + "')");  //jshint ignore:line
                        }
                    }
                    srcElem && srcElem.addEventListener("error",imageErrorHandler);
                    // not observing handled element
                    observer.unobserve(entry);
                }
            });
        }

        function imageErrorHandler(){
            let srcElem = this;
            let retrycount = parseInt(srcElem.getAttribute('data-retrycount'));
            if(isNaN(retrycount)){
                retrycount = 1;
            }
            if(retrycount > 0 && retrycount <= 3){
                window.setTimeout(function(){
                    let src = srcElem && srcElem.getAttribute(replaceWith);
                    srcElem && srcElem.addEventListener("error",imageErrorHandler);
                    src && srcElem.setAttribute('src', src);
                    srcElem && srcElem.setAttribute('data-retrycount' , ++retrycount);
                }, 3000);
            }
            else{
                this.src='/images/dummyPX.png';
            }
            this.removeEventListener("error" , imageErrorHandler);                       
        }

        function addToObserver(container) {
            if(observer && !container) return null;
            if(container == document) {
                return restartObserver(replaceWith);
            }
            //for images
            Box.DOM.queryAll(container, selector).forEach(image =>{
                if(!images.some((i)=>{
                    return i === image;
                })) {
                    images.push(image);
                    observer.observe(image);
                }
            });
            //for backgroundImages
            Box.DOM.queryAll(container, "[" + backgroundImagePatternSelector + "]").forEach(image =>{
                if(!backgroundImages.some((i)=>{
                    return i === image;
                })) {
                    backgroundImages.push(image);
                    observer.observe(image);
                }
            });
            //for dummyBackgroundImages
            Box.DOM.queryAll(container, backgroundImageSelector).forEach(image =>{
                if(!dummyBackgroundElements.some((i)=>{
                    return i === image;
                })) {
                    dummyBackgroundElements.push(image);
                    observer.observe(image);
                }
            });
            return observer;
        }

        function restartObserver(rWith = 'data-src'){
            selector = `[${rWith}]`;
            replaceWith = rWith;

            observer.disconnect();

            images.splice(0,images.length);
            images = Box.DOM.queryAll(document, selector);
            images.forEach(image => {
                observer.observe(image);
            });

            dummyBackgroundElements.splice(0,images.length);
            dummyBackgroundElements = Box.DOM.queryAll(document, backgroundImageSelector);
            dummyBackgroundElements.forEach(image => {
                observer.observe(image);
            });

            backgroundImages.splice(0,images.length);
            backgroundImages = Box.DOM.queryAll(document, "[" + backgroundImagePatternSelector + "]");
            backgroundImages.forEach(image => {
                observer.observe(image);
            });

            return observer;
        }

        function checkAndUnobserveElements(entries) {
            let elementsToUnobserve = entries.filter(entry=> {
                return onlyOnceObservedElements.indexOf(entry.target) > -1;
            });

            unobserveElements(elementsToUnobserve.map(entry => {return entry.target}));
        }

        function _elementIntersectionAction(entries) {
            entries = entries.filter(entry=>entry.isIntersecting);
            if(entries && entries.length) {
                let messages = Object.keys(elementBroadcaseMessages);
                messages.forEach(function(message) {
                    let elements = elementBroadcaseMessages[message];
                    let currentIntersecting = entries.filter(entry=> {
                        return elements.indexOf(entry.target) > -1;
                    });

                    if(currentIntersecting.length > 0) {
                        context.broadcast(message, {
                            elements: currentIntersecting
                        });
                    }
                });

                checkAndUnobserveElements(entries);
            }
        }


        function observeElements(elements, broadcastMessage, observeOnlyOnce) {
            if (!isObserverSupported || !elements || !elements.length) {
                return false;
            } else {
                elements.forEach(element => {
                    elementObserver.observe(element);
                    if(observeOnlyOnce) {
                        onlyOnceObservedElements.push(element);
                    }
                });
                if(elementBroadcaseMessages[broadcastMessage]) {
                    elementBroadcaseMessages[broadcastMessage] = 
                    elementBroadcaseMessages[broadcastMessage].concat(Array.from(elements));
                } else {
                    elementBroadcaseMessages[broadcastMessage] = Array.from(elements);
                }
                return true;
            }
        }

        function unobserveElements(elements) {
            if (!isObserverSupported || !elements || !elements.length) {
                return false;
            } else {
                elements.forEach(element => {
                    elementObserver.unobserve(element);

                    let messages = Object.keys(elementBroadcaseMessages);
                    messages.forEach(function(message) {
                        //Remove from broadcast messages map
                        let observedElements = elementBroadcaseMessages[message];
                        let index = observedElements.indexOf(element);
                        if(index > -1) {
                            observedElements.splice(index, 1);
                        }
                        if(observedElements.length === 0) {
                            delete elementBroadcaseMessages[message];
                        }
                        //Remove from onlyOnceTrackedElements
                        index = onlyOnceObservedElements.indexOf(element);
                        if(index > -1) {
                            onlyOnceObservedElements.splice(index, 1);
                        }
                    });
                });
                return true;
            }
        }

        function _lazyModuleIntersectionAction(entries) {
            entries = entries.filter(entry=>entry.isIntersecting);
            if(entries && entries.length) {
                let commonService = Box.Application.getService("CommonService");
                let data =[];
                entries.forEach((entry) => {
                    let moduleName = entry.target.dataset.lazyModule || entry.target.dataset.lazymodule;
                    data.push({
                        name: moduleName,
                        loadId: entry.target.id
                    });
                    lazyModuleObserver.unobserve(entry.target);
                });

                commonService.loadModule(data, "inview");
            }
        }

        function observeLazyModules(container = document) {
            if(isObserverSupported && container) { // For null check
                container = container.length ? container[0] : container;
                let elements = container.querySelectorAll(inViewLazyModulesSelector);
                elements.forEach(element => {
                    lazyModuleObserver.observe(element);
                });
                return true;
            }
            return false;
        }

        return {
            isObserverSupported,
            replaceWith,
            addToObserver,
            restartObserver,
            observeElements,
            unobserveElements,
            observeLazyModules
        };
    });

    return Box.Application.getService(SERVICE_NAME);
});
