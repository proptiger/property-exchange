"use strict";
define([
], () => {
    const SERVICE_NAME = 'allianceIconService';

    Box.Application.addService(SERVICE_NAME, () => {

        const serviceIconMap = {
            10: '<i class="iconalliance-legal-advice"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span></i>', //legal advice
            11: '<i class="iconalliance-home-insurance"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span></i>', // home insurance
            12: '<i class="iconalliance-rent-agreement"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span></i>', // rental agreement
            13: '<i class="iconalliance-documentation-and-registry"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span></i>', // documentation and registry
            14: '<i class="iconalliance-movers-packers"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span></i>', // packers and movers
            15: '<i class="iconalliance-furniture-rental"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span></i>', // furniture on rent
            16: '<i class="iconalliance-interior-design"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span></i>', // interior design
            17: '<i class="iconalliance-modular-kitchen"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span></i>', // modular kitchens and wardrobes
            18: '<i class="iconalliance-buy-furniture-online"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span></i>', // buy furniture online
            19: '<i class="iconalliance-bathroom-cleaning"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span></i>', // bathroom cleaning
            20: '<i class="iconalliance-kitchen-cleaning"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span></i>', //kitchen cleaning
            21: '<i class="iconalliance-pest-control"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span><span class="path13"></span><span class="path14"></span><span class="path15"></span><span class="path16"></span><span class="path17"></span></i>', // pest control
            22: '<i class="iconalliance-sofa-cleaning"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span></i>', // sofa cleaning
            23: '<i class="iconalliance-electrician"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span></i>', // electricians
            24: '<i class="iconalliance-home-cleaning"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span></i>', // home cleaning
            25: '<i class="iconalliance-party-planners"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span></i>', // party planners
            26: '<i class="iconalliance-home-chef"><span class="path1"></span><span class="path2"></span><span class="path3"></span></i>', // house chef
            27: '<i class="iconalliance-home-inspection"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span></i>', // home inspection
            28: '<i class="iconalliance-appliance-installation"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span></i>', // appliances installation
            29: '<i class="iconalliance-taxi-service"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span></i>' // cab services 
        };

        function getServiceIcon(serviceId){
            return serviceIconMap[serviceId];
        }
        
        return {
            getServiceIcon
        };
    });

    return Box.Application.getService(SERVICE_NAME);
});
