'use strict';
define(['common/sharedConfig',
    'services/utils',
    'services/localStorageService',
    'services/apiService',
    'services/urlService',
    'services/sessionService'
], function(SharedConfig, Utils, LocalStorageService, ApiService, UrlService, sessionService) {

    const SERVICE_NAME = 'FeedbackService';
    Box.Application.addService(SERVICE_NAME, function(application) {
        const config = {
            otpValidationTime: 30*60*1000,
            buyerUserId: "mkn_lead_userid",
            ratedSellerStorage: "rated_sellers",
            enquiryDroppedStorage: "eqd",
            reasonListTypes: {
                badSeller : "badSeller",
                badProperty: "badProperty",
                good: "good"
            },
            feedbackStorageKey: "feedbackSessionData",
            feedbackSeen: "seen",
            feedbackDataExpiryKey: "expires"
        };
        var feedbackPromise;

        function _getEmptyPromise(data={}){
            var defer = $.Deferred();
            defer.resolve(data);
            return defer.promise();
        }

        function getRatedSellers(){
            return LocalStorageService.getSessionItem(config.ratedSellerStorage) || [];
        }

        function addRatedSeller(sellers){
            let storedRatedSellers = LocalStorageService.getItem(config.ratedSellerStorage) || [];
            if(!$.isArray(sellers)){
                sellers = [sellers];
            }
            $.each(sellers,(k,v)=>{
                if(storedRatedSellers.indexOf(v) == -1){
                    storedRatedSellers.push(v);
                }
            });
            LocalStorageService.setSessionItem(config.ratedSellerStorage,storedRatedSellers);
        }

        function isFeedbackGiven(sellerId){
            let allRatedSellers = getRatedSellers();
            return allRatedSellers.indexOf(sellerId) !== -1 ? true : false;
        }

        function getCallFeedbackDetails(data){
            let api = SharedConfig.apiHandlers.getCallFeedbackDetails(data).url;
            return ApiService.get(api);
        }


        //return the list of seller(for which feedback is pending) for a given buyer.
        function getConnectedSellerList(buyerId, companyId){
            if(buyerId){
                let api = SharedConfig.apiHandlers.pendingFeedbacks({buyerId, companyId}).url;
                return ApiService.get(api);
            } else {
                return _getEmptyPromise();
            }
        }


        // TODO: cb function not executing
        function processFeedback(){
            let feedbackSessionData = getFeedbackData();
            let buyerIdFromParam = UrlService.getUrlParam(Utils.getFeedbackSMSUrlQueryKey()) || LocalStorageService.getItem(config.buyerUserId);
            if(buyerIdFromParam && !feedbackSessionData[config.feedbackSeen]){
                return getConnectedSellerList(buyerIdFromParam).then(response => {
                    if(response.length){
                        application.broadcast('open_feedback', {response,buyerIdFromParam});
                    }
                    return {response};
                }, error => {
                    return error;
                });
            }
            else {
                return new Promise((resolve) => {
                    resolve({response: []});
                });
            }
        }

        function getFeedbackStatus(cb){

            if(!feedbackPromise){
                feedbackPromise = processFeedback();
            }
            return feedbackPromise.then((result) => {
                if(typeof cb == "function"){
                    cb(result.trackingParams);
                }
                return result.response;
            }, (error) => {
                if(typeof cb == "function"){
                    cb();
                }
                return error;
            });
        }

        function addEnquiryDropped(enquiryId, listingId){
            let storedEnquiries = LocalStorageService.getItem(config.enquiryDroppedStorage) || {},
                currentSession = _getCurrentSession();
            if(!currentSession || !enquiryId) {
                return;
            }
            storedEnquiries[currentSession] = storedEnquiries[currentSession.c] || [];
            storedEnquiries[currentSession].push({id: enquiryId, listingId: listingId, d: new Date().getTime()});
            LocalStorageService.setItem(config.enquiryDroppedStorage,storedEnquiries);
        }

        function getEnquiryDroppedInSession(sessionId, excludeSession=false) {
            let storedEnquiries = LocalStorageService.getItem(config.enquiryDroppedStorage) || {};
            if(!sessionId) {
                return undefined;
            }
            if(excludeSession){
                var enquiries = [];
                delete storedEnquiries[sessionId];
                for(var key in storedEnquiries){
                    enquiries = enquiries.concat(storedEnquiries[key]);
                }
                return enquiries;
            } else {
                return storedEnquiries[sessionId];
            }
        }

        function _getCurrentSession() {
            let currentSession = sessionService.getSessionId() || {};
            return currentSession.c;
        }

        function getReasonObject(category){
            let obj = category.answerOptions.map(item => {
                return {id: item.id, label: item.label, parentId: item.ratingParameterId};
            });
            return obj;
        }

        function parseFeedbackReasons(response){
            let masterReasons = {};
            if(response.ratingParameters){
                response.ratingParameters.forEach(category => {
                    switch(category.parameter){
                        case "SellerRating1":
                            masterReasons["ratingWithoutReason"] = category.id;
                            break;
                        case "SellerRating2a":
                            masterReasons[config.reasonListTypes.badProperty] = getReasonObject(category);
                            break;
                        case "SellerRating2b":
                            masterReasons[config.reasonListTypes.badSeller] = getReasonObject(category);
                            break;
                        case "SellerRating3":
                            masterReasons[config.reasonListTypes.good] = getReasonObject(category);
                            break;
                    }
                });
            }
            return masterReasons;
        }

        function getFeedbackMasterReasons(){
            let _this = this;
            if(!_this.masterFeedbackReasons){
                let url = SharedConfig.apiHandlers.noCacheApiWrapper(SharedConfig.clientApis.feedbackReasons({templateId: 6}).url).url;
                _this.masterFeedbackReasons = ApiService.get(url).then(response => {
                    return parseFeedbackReasons(response.data);
                }, () => {
                    _this.masterFeedbackReasons = null;
                    return [];
                });
            }
            return _this.masterFeedbackReasons;
        }

        function getReasonListName(data){
            if(data.result.rating >= 4){
                return config.reasonListTypes.good;
            } else {
                if(data.result.listingId){
                    return config.reasonListTypes.badProperty;
                } else {
                    return config.reasonListTypes.badSeller;
                }
            }
        }

        function submitRating(data){
            let url = SharedConfig.apiHandlers.callRating({sellerId: data.postData.sellerId}).url;
            let payload = {
                "buyerId": data.postData.buyerId,
                "ratings": [{"ratingParameterId": data.masterReasons.ratingWithoutReason, "value": data.postData.rating}]
            };
            return ApiService.postJSON(url, payload);
        }

        function submitReasons(data){
            let url = SharedConfig.apiHandlers.updateFeedbackRatingReason(data.postData).url;
            let payload = {
                "buyerId": data.postData.buyerId,
                "ratings": data.postData.reasons,
            };
            if(data.postData.comment){
                payload["comment"] = data.postData.comment;
            }
            return ApiService.putJSON(url, JSON.stringify(payload));
        }

        function getRating(buyerId, sellerId){
            if(buyerId && sellerId){
                let api = SharedConfig.apiHandlers.callRating({buyerId, sellerId}).url;
                return ApiService.get(api);
            }else {
                return _getEmptyPromise([{}]);
            }
        }

        function getFeedbackData(){
            let feedbackData = LocalStorageService.getItem(config.feedbackStorageKey);
            if(feedbackData && feedbackData[config.feedbackDataExpiryKey] && feedbackData[config.feedbackDataExpiryKey] > new Date()) {
                return feedbackData;
            } 
            return {};
        }

        function setFeedbackData(key, value){
            let feedbackData = getFeedbackData();
            feedbackData[key] = value;
            let date = new Date();
            date.setDate(date.getDate()+1);
            feedbackData[config.feedbackDataExpiryKey] = date.getTime();
            LocalStorageService.setItem(config.feedbackStorageKey, feedbackData);
        }

        return{
            getRatedSellers,
            addRatedSeller,
            isFeedbackGiven,
            getFeedbackStatus,
            addEnquiryDropped,
            getEnquiryDroppedInSession,
            getCurrentSession: _getCurrentSession,
            getFeedbackMasterReasons,
            getCallFeedbackDetails,
            getReasonListName,
            submitRating,
            submitReasons,
            getConnectedSellerList,
            getRating,
            getFeedbackData,
            setFeedbackData
        };

    });
    return Box.Application.getService(SERVICE_NAME);
});