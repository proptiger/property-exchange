"use strict";
define(['services/apiService', 'services/loggerService', 'services/commonService','services/utils', 'services/shortlist', 'common/sharedConfig'], function(apiService, logger, commonService, utils, shortlist, sharedConfig) {
    const SERVICE_NAME = 'LoginService';
    Box.Application.addService('LoginService', function() {
        var config = {
            baseURL: '/',
            internalLogoutUrl: 'xhr/userService/logout',
            checkUserExistURL: 'xhr/userService/checkUserExist',
            doSocialLoginURL: 'xhr/userService/doSocialLogin',
            termsAcceptedURL: 'xhr/userService/termsAccepted',
            getOTPURL: 'app/v1/otp',
            validateOTPURL: 'xhr/userService/validateOTP',
            validateOTPAndLogin: 'xhr/userService/verifyOTP',
            changePasswordLoggedIn: 'xhr/userService/changePasswordLoggedIn',
            checkUserLoggedIn: 'xhr/userService/checkLoggedIn',
            userLoginStatus: 'xhr/userService/userLoginStatus',
            registrationComplete: 'xhr/userService/registrationComplete',
            doLoginURL: 'xhr/userService/doLogin',
            forgotPasswordURL: 'xhr/userService/forgotPassword',
            createUserURL: 'xhr/userService/createUser'
        };
        var userData = {};
        var isLoggedIn, loginMode;
        var userDetails;
        var popupOpened = false;
        const LOGIN_ID = "login",
              LOGIN_COOKIE = "user_info";

        var cleanUserDetails = function() {
            userDetails = undefined;
            utils.deleteCookie(LOGIN_COOKIE);
        };

        var duplicateSeller = function(userId, duplicateUserIdsToBeAdded) {
            duplicateUserIdsToBeAdded = duplicateUserIdsToBeAdded.join(',');
            var {url} = sharedConfig.clientApis.duplicateUser({userId, duplicateUserIdsToBeAdded });
            return apiService.post(url, null);
        };

        var getClientIp = function() {
            var {url} = sharedConfig.clientApis.getCityByLocation();
            return apiService.get(url).then((data) => {
                return data.data.clientIp;
            });
        };

        var logoutUser = function() {
            return apiService.post(config.baseURL + config.internalLogoutUrl, null, true).then(_successHandler, _errorHandler);

            function _successHandler(response) { // jshint ignore:line
                logger.log('user logged in');
                isLoggedIn = false;
                userData = {};
                cleanUserDetails();
                Box.Application.broadcast("loggedOut", response);
                shortlist.deleteAllLocalShortlists();
                return response;
            }

            function _errorHandler(error) {
                logger.log('error occurred');
                logger.log(error);
                return error;
            }
        };
        var checkUserExists = function(email) {
             email = $.trim(email);
            return apiService.get(config.baseURL + config.checkUserExistURL + "?email=" + encodeURIComponent(email), {
                'email': email
            }, null, true).then(_successHandler, _errorHandler);

            function _successHandler(response) { // jshint ignore:line
                logger.log('user exists');
                return {
                    userExists: response.data
                };
            }

            function _errorHandler(error) {
                logger.log('error occurred');
                logger.log(error);
                return error;
            }
        };
        var doLogin = function(loginData){
            var headers = {
                contentType: 'application/x-www-form-urlencoded'
            };
            return apiService.post(config.baseURL + config.doLoginURL, loginData, headers, true);
        };
        var doSocialLogin = function(loginData) {
            return apiService.post(config.baseURL + config.doSocialLoginURL, loginData, null, true).then(_successHandler, _errorHandler);

            function _successHandler(response) {
                loginMode = loginData.provider; // jshint ignore:line
                logger.log('social login complete');
                return response;
            }

            function _errorHandler(error) {
                logger.log('error occurred');
                logger.log(error);
                return error;
            }
        };
        var termsAccepted = function() {
            return apiService.putJSON(config.baseURL + config.termsAcceptedURL, {}, null, true).then(_successHandler, _errorHandler);

            function _successHandler(response) {
                return response;
            }

            function _errorHandler(error) {
                return error;
            }
        };
        var getOTP = function() {
            return apiService.get(config.baseURL + config.getOTPURL, {}, null, true).then(_successHandler, _errorHandler);

            function _successHandler(response) {
                return response;
            }

            function _errorHandler(error) {
                return error;
            }
        };
        var validateOTPAndLogin = function(code, userId) {
            if (!code) {return;}
            var url = config.baseURL + config.validateOTPAndLogin;
            return apiService.post(url, {
                userId: userId,
                otp: code
            }, null, true).then(_successHandler, _errorHandler);

            function _successHandler(response) {
                return response;
            }

            function _errorHandler(error) {
                return error;
            }
        };
        var validateOTP = function(code) {
            if (!code) {return;}
            return apiService.post(config.baseURL + config.validateOTPURL, {
                'otp': code
            }, null, true).then(_successHandler, _errorHandler);

            function _successHandler(response) {
                return response;
            }

            function _errorHandler(error) {
                return error;
            }
        };
        var changePasswordLoggedIn = function(newPassword) {
            if (!newPassword) {return;}
            return apiService.post(config.baseURL + config.changePasswordLoggedIn, {
                newPassword: newPassword
            }, null, true).then(_successHandler, _errorHandler);

            function _successHandler(response) {
                return response;
            }

            function _errorHandler(error) {
                return error;
            }
        };
        var createUser = function(userData){
            return apiService.postJSON(config.baseURL + config.createUserURL, userData, null, true);
        };
        var getUser = function() {
            return userData;
        };
        var setBaseURL = function(url) {
            config.baseURL = url;
        };
        var popupOpen = function(type) {
            type = type || 'social';
            Box.Application.broadcast('popup:open', {
                id: 'loginPopup',
                options:{
                    config: {
                        modal: true,
                        escKey: false
                    }
                }
            });
            Box.Application.broadcast('openLoginType', {
                type: type
            });
        };
        var registrationComplete = function() {
            // function _successHandler(response) {
            //     return response;
            // }
            // function _errorHandler(error) {
            //     return error;
            // }
            return apiService.get(config.baseURL + config.registrationComplete).then(_successHandler, _errorHandler);
            function _successHandler(response){
                if(response.register.pending){
                    userData['id']= response.data.id;
                    userData['name']= response.data.firstName;
                    userData['fullName']= response.data.firstName;
                    userData['email']= response.data.email;
                    userData['contactNumber']= response.data.contactNumber;
                    userData['pending'] = response.register.pending;
                    userData['pendingRegistration']= response.register.pendingRegistration;
                    userData['profileImg']=  response.data.profileImageUrl;
                    userData['roles']= response.data.roles;
                    userData['acceptedTermAndConditions']= response.data.acceptedTermAndConditions.length;
                }
                return response;
            }
            function _errorHandler(error){
                return error;
            }
        };
        var startLogin = function(type){
            let loginElem = document.getElementById(LOGIN_ID);
            type = type || 'social';
            userData['type'] = type;
            if (Box.Application.isStarted(loginElem)) {
                popupOpen(type);
            } else {
                commonService.bindOnModuleLoad('newLoginRegister', () => {
                    popupOpen(type);
                });
                Box.Application.broadcast('loadModule', {
                    name: 'newLoginRegister'
                });
            }
        };
        var openLoginPopup = function(type) {
            if(popupOpened){
                return false;
            }
            setTimeout(function(){ popupOpened = false; }, 1000);
            if(type === 'checkRegistered'){
                popupOpened = true;
                userData['getRegistered'] = true;
                registrationComplete().then(function(response){
                if(response.register.pending){
                        startLogin();
                    }else if(response.data.roles){
                        //let location = window.location;
                        window.location = userData['sellerMicrositeUrl'];
                    }
                },function(){
                    startLogin();
                });
            }else{
                popupOpened = true;
                userData['getRegistered'] = null;
                startLogin(type);
            }
        };
        var closeLoginPopup = function(callback){
            userData['getRegistered'] = null;
            cleanUserDetails();
            var data={};
            if(callback) {
                data.options = {
                    callback: callback
                };
            }
            Box.Application.broadcast('homeLoginUnload');
            Box.Application.broadcast("popup:close", data);
        };
        
        var isUserLoggedIn = function() {
            function _successHandler(response) {
                var defer = $.Deferred();
                if(response && response.statusCode && response.statusCode == 401){
                    defer.reject(response);
                }else{
                    shortlist.setSyncedShortlist();
                    defer.resolve(response);

                }
                return defer.promise();
            }

            function _errorHandler(error) {
                return error;
            }

            if (!userDetails) {
                userDetails = commonService.bindOnLoadPromise().then(()=>{
                    return apiService.get(config.baseURL + config.userLoginStatus).then(_successHandler, _errorHandler);
                });
            }
            return userDetails;
        };
        var forgotPassword = function(postData){
            return apiService.post(config.baseURL + config.forgotPasswordURL, postData, null, true);
        };
        /*
            function to get category name
        */
        function getTrackingCategory(category){
            if(userData['getRegistered']){
                switch(category){
                    case 'Login':
                        return 'ListProperty_Login';
                        break;  //jshint ignore:line
                    default:
                        return category.replace('Login_', 'ListProperty_');
                        break;   //jshint ignore:line
                }
            }else{
                return category;
            }
        }

        /*
            function to get active screen
        */
        function getRegistrationScreenLabel(){
            if($('#city_seller_type').hasClass('p-center')){
                return 'sellerType';
            }
            if($('#sociable_connect').hasClass('p-center')){
                return 'connectWith';
            }
            if($('#main_login').hasClass('p-center')){
                return 'emailSignUp';
            }
            if($('#change_phone').hasClass('p-center')){
                return 'enterMobile';
            }
            if($('[data-type=back-otp]').length){
                return 'enterOTP';
            }
            if($('#terms_condition').hasClass('p-center')){
                return 'T&C';
            }
        }
        return {
            logoutUser: logoutUser,
            checkUserExists: checkUserExists,
            doLogin: doLogin,
            doSocialLogin: doSocialLogin,
            getUser: getUser,
            setBaseURL: setBaseURL,
            termsAccepted: termsAccepted,
            getOTP: getOTP,
            getClientIp: getClientIp,
            duplicateSeller: duplicateSeller,
            validateOTP: validateOTP,
            validateOTPAndLogin: validateOTPAndLogin,
            changePasswordLoggedIn: changePasswordLoggedIn,
            openLoginPopup: openLoginPopup,
            closeLoginPopup: closeLoginPopup,
            isUserLoggedIn: isUserLoggedIn,
            cleanUserDetails: cleanUserDetails,
            createUser: createUser,
            forgotPassword: forgotPassword,
            getTrackingCategory: getTrackingCategory,
            getRegistrationScreenLabel: getRegistrationScreenLabel,
            registrationComplete:registrationComplete,
            setUserProp: function(prop, val) {
                userData[prop] = val;
            },
            getUserProp: function(prop) {
                return userData[prop];
            }
        };
    });
    return Box.Application.getService(SERVICE_NAME);
});
