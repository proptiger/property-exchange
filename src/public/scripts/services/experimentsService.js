'use strict';
/**
 * Service to do ab testing experiments. 
 * The service will extract current user's experiment value from cookie and store it as a private variable.
 * The module that need to perform the AB testing will call the functions of this service to execute those experiments.
 * 
 */
define(['services/loggerService',
	"services/utils",
	"common/sharedConfig",
    'common/experimentsConfig'], (Logger, utils, sharedConfig, experimentsConfig) => {
	const SERVICE_NAME = 'experimentsService',
	EXPERIMENTS_COOKIE_NAME = sharedConfig.EXPERIMENTS_COOKIE_NAME;

	Box.Application.addService(SERVICE_NAME, () => {
		let experiments = {};

		function deserializeExperimentsCookie (cookieString) {
			let output = {};
		  	if(cookieString){
		  		decodeURIComponent(cookieString)
		      		.split(';')
		      		.filter((split) => split)
		      		.forEach((split) => {
		      			let [k,v] = split.split('=');
		      			output[k] = v;
		      		});
	      	}
	      	return output;
		}
		experiments = deserializeExperimentsCookie(utils.getCookie(EXPERIMENTS_COOKIE_NAME));
		/**
		 * Checking the type of variation and executing the given callback
		 * @param  {String} experimentName 				The Experiment name, in experiments cookie
		 * @param  {String,Object,Array} variation      If String, will match exactly the experiment name value and add the passed className
		 *                                              If Object, will check variations value in keys and assign the class in value of variation object
		 *                                              If Array, will match multiple values to add the passed className
		 * @param  {Function} callback       			Callback to be executed on successfull matching of the variation
		 */
		function executeOnVariatoin(experimentName, variation, callback) {
			if(experiments && experimentName && experiments[experimentName] && variation &&
				typeof callback === "function") {
				switch(typeof variation) {
					case "string":
						if(experiments[experimentName] == variation) {
							callback();
						}
						break;
					case "object":
						if(Array.isArray(variation) && variation.length > 0) {
							variation.forEach(v => {
								if(experiments[experimentName] == v) {
									callback();
								}
							});
						} else if(Object.keys(variation).length > 0){
							Object.keys(variation).forEach(k => {
								if(experiments[experimentName] == k) {
									callback(variation[k]);
								}
							});
						}
						break;
				}
			}
		}

		/**
		 * Add class to the passed element if experiment name matches variation
		 * @param  {DomNode} element        			The dom node to chang
		 * @param  {String} experimentName 				The Experiment name, in experiments cookie
		 * @param  {String,Object,Array} variation      If String, will match exactly the experiment name value and add the passed className
		 *                                              If Object, will check variations value in keys and assign the class in value of variation object
		 *                                              If Array, will match multiple values to add the passed className
		 * @param  {String} className      				Optional for Object variations
		 */
		function experimentElementClass(element, experimentName, variation, className) {
			if(element && element.classList && typeof element.classList.add === "function" && className){
				executeOnVariatoin(experimentName, variation, function(param){
					element.classList.add(param || className);
				});
			}
		}

		/**
		 * Add class to the passed element if experiment name matches variation
		 * @param  {Array[DomNode]} elements        	The dom nodes to change
		 * @param  {String} experimentName 				The Experiment name, in experiments cookie
		 * @param  {String,Object,Array} variation      If String, will match exactly the experiment name value and add the passed className
		 *                                              If Object, will check variations value in keys and assign the class in value of variation object
		 *                                              If Array, will match multiple values to add the passed className
		 * @param  {String} className      				Optional for Object variations
		 */
		function experimentElementsClass(elements, experimentName, variation, className){
			if(elements && elements.length > 0) {
				for(let i = 0 ; i < elements.length ; i ++) {
					experimentElementClass(elements[i], experimentName, variation, className);
				}
			}
		}

		/**
		 * Change content of an element by checking the experiment value for ab testing
		 * @param  {DomNode} element        			The dom node to chang
		 * @param  {String} experimentName 				The Experiment name, in experiments cookie
		 * @param  {String,Object,Array} variation      If String, will match exactly the experiment name value and add the passed className
		 *                                              If Object, will check variations value in keys and assign the class in value of variation object
		 *                                              If Array, will match multiple values to add the passed className
 		 * @param  {String} contentString  				The content to be added in the element on matching the variagtion
		 */
		function experimentElementContent(element, experimentName, variation, contentString) {
			if(element) {
				executeOnVariatoin(experimentName, variation, function(param){
					element.innerHTML = param || contentString;
				});
			}
		}

		/**
		 * Change content of an element by checking the experiment value for ab testing
		 * @param  {Array[DomNode]} element        		The dom nodes to chang
		 * @param  {String} experimentName 				The Experiment name, in experiments cookie
		 * @param  {String,Object,Array} variation      If String, will match exactly the experiment name value and add the passed className
		 *                                              If Object, will check variations value in keys and assign the class in value of variation object
		 *                                              If Array, will match multiple values to add the passed className
 		 * @param  {String} contentString  				The content to be added in the element on matching the variagtion
		 */
		function experimentElementsContent(elements, experimentName, variation, contentString) {
			if(elements && elements.length > 0) {
				for(let i = 0 ; i < elements.length ; i ++) {
					experimentElementContent(elements[i], experimentName, variation, contentString);
				}
			}
		}

		/**
		 * Check if an experiment has a variation or not
		 * @param  {String}  				experimentName 	Name if the variation to check
		 * @param  {String/Array[String]}  	variation      	The variation to expect
		 * @return {Boolean}            	True or false depending upon the variation match
		 */
		function hasExperimentVariation(experimentName, variation) {
			if(experimentName && variation) {
				//Convert variation into array if not
				variation = Array.isArray(variation) ? variation : [variation];

				return variation.indexOf(experiments[experimentName]) > -1;
			}
			return false;
		}

		/**
		 * Get an variation corresponding to an experiment name
		 * @param  {String}  				experimentName 	Name if the variation to check
		 * @return {string/undefined}       variation name if any else returns undefined  	
		 */
		function getExperimentVariation(experimentName) {
			if(experiments && experimentName) {
				return experiments[experimentName];
			}
			return undefined;
		}
		/** 
		 * Switch the elements to display on the basis of the variation of an experiment
		 * @param  {String} experimentName Name of the experiment to check
		 * @param  {Object} variations     Key value pairs of experimentValue and element selector. Will remove the non matched elements
		 */
		function switchElements(experimentName, variations) {
			if(!variations && experimentsConfig[experimentName]){
				variations = experimentsConfig[experimentName];
			}
			let keys = variations && Object.keys(variations);
			if(keys && keys.length) {
				keys.forEach(k => {
					let elements = document.querySelectorAll(variations[k]);
					elements.forEach(element => {
						if(k === experiments[experimentName] && element) {
							element.classList.remove("hide");
						} else if(element) {
							document.querySelector(variations[k]).remove();
						}
					});
				})
			}
		}

		return {
			experimentElementClass,
			experimentElementsClass,
			experimentElementContent,
			experimentElementsContent,
			hasExperimentVariation,
			getExperimentVariation,
			switchElements
		}
	});

	return Box.Application.getService(SERVICE_NAME);
});