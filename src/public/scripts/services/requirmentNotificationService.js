define(['services/utils', 'services/localStorageService', 'services/commonService'], function(utils, LocalStorageService, CommonService) {
    'use strict';

    const SERVICE_NAME = 'requirmentNotificationService';

    Box.Application.addService(SERVICE_NAME, function(application) {
        const $ = application.getGlobal('jQuery'),
            PAGE_QUEUE_LENGTH = 2;

        var popupOpened = false;

        function _getPageType() {
            let currentPage;
            if (window.pageData.isSerp) {
                currentPage = 'serp';
            } else if (window.pageData.pageType == "PROPERTY_URLS") {
                currentPage = 'property';
            } else {
                currentPage = 'other';
            }
            return currentPage;
        }


        function _openPopupUser() {
            let localityId = utils.getPageData('localityId'),
                localityName = utils.getPageData('localityName');
               // cityId = utils.getPageData('cityId'),
                //cityName = utils.getPageData('cityName');
            let data = {
                id: 'userRequirmentPyr',
                pyrData: {
                    stage: "step1"
                }
            };
            if (localityId && localityName) {
                data.pyrData.localityIds = [];
                data.pyrData.localityIds = [{
                    id: localityId,
                    name: localityName,
                    type: 'locality'
                }];
            }
            application.broadcast('openPyr', data);
            _openPopup({
                id: 'userRequirmentPopup',
                modal: false
            });
        }

        function _openPopup(config) {
            application.broadcast('popup:open', {
                id: config.id,
                options: {
                    config: {
                        modal: config.modal
                    }
                }
            });
        }

        function setReferrer() {

            let pageQueue = LocalStorageService.getItem('pageQueue');
            pageQueue = utils.isEmptyObject(pageQueue) ? [] : pageQueue.split(',');
            if (pageQueue.length == PAGE_QUEUE_LENGTH) {
                pageQueue.shift();
            }
            pageQueue.push(_getPageType());
            LocalStorageService.setItem('pageQueue', pageQueue.join(','));
        }

        function openUserRequirmentPopup() {

            let currentPage = _getPageType();
            let pageQueue = (LocalStorageService.getItem('pageQueue') || []).split(',');
            if (pageQueue.length == 2 && pageQueue[0] == 'serp' && (pageQueue[1] == 'serp' || pageQueue[1] == 'property') && currentPage == 'other' && !popupOpened) {
                popupOpened = true;
                application.broadcast('open_alertconfirm_box', {
                    moduleId: 'userRequirmentAlert',
                    displayData: {
                        heading: 'See the best properties for you. Share your requirements with M',
                        cancelText: 'No',
                        confirmText: 'Yes'
                    },
                    jsonDump: {},
                    successCallback: _openPopupUser
                });
                _openPopup({
                    id: 'userRequirmentAlertPopup',
                    modal: false
                });
            }
        }

        function clearAllPageQueue() {
            CommonService.bindOnApplicationClose(() => {
                if (_getPageType !== 'property') {
                    LocalStorageService.deleteItem('pageQueue');
                }
            });
        }

        function sentMsgToChat(message) {
            $(document).trigger("manualInteraction", ["send_chat", {
                text: message
            }]);
            utils.trackEvent('Shown', 'M Suggestions');
        }

        return {
            setReferrer,
            openUserRequirmentPopup,
            clearAllPageQueue,
            sentMsgToChat
        };
    });

    return Box.Application.getService(SERVICE_NAME);
});
