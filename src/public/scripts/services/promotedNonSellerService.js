/**
 * Service to get promoted non sellers
 */
'use strict';
define(['services/utils'], (utils) => {
    const SERVICE_NAME = 'promotedNonSellerService';

  const NON_SELLERS = [
 //{
	//     "redirectUrl": "http://sternon.com/",
	//     "image": "/images/un-versioned/nonSellers/sternon-banner-1.png",
	//     "alt": "sternon",
	//     "logo": "/images/un-versioned/nonSellers/sternon-logo.jpg",
	//     "content": {
	//         "default": {
	//             "title": "Get USA Green Card for you & your family",
	//             "info": "Buy 2 apartments near Disneyworld & Florida"
	//         },
	//         "rhs": {
	//             "title": "Get USA Green Card for you & your family",
	//             "info": "Buy 2 apartments near Disneyworld & Florida"
	//         }
	//     },
	//     "cta": "Know More",
	//     "visiblityInCities": [],
	//     "visiblityInLocalities": ["50026","51737","80872","81060","88722","103794","128488","136752", //Jogeswari
	//     						"50016","56064","61690","64605","64826","65780","90321","91065","91363","103754"], //Powai
	//     "pagesToShowOn": ["serp"],
	//     "saleTypeSupported": ["buy"],
	//     "id": "ADID_NS001"
	// },{
	//     "redirectUrl": "http://sternon.com/",
	//     "image": "/images/un-versioned/nonSellers/sternon-banner-2.png",
	//     "alt": "sternon",
	//     "logo": "/images/un-versioned/nonSellers/sternon-logo.jpg",
	//     "content": {
	//         "default": {
	//             "title": "Get USA Green Card for you & your family",
	//             "info": "Buy 2 apartments near Disneyworld & Florida"
	//         },
	//         "rhs": {
	//             "title": "Get USA Green Card for you & your family",
	//             "info": "Buy 2 apartments near Disneyworld & Florida"
	//         }
	//     },
	//     "cta": "Know More",
	//     "visiblityInCities": [209],//Mohali
	//     "visiblityInLocalities": ["50028","52178","62885","65603","66418","66419","66420","66458","67579",//MALAD
	//     							"73353","73649","80865","80875","89897","95330","95896","96242","98738",//MALAD
	//     							"99343","103778","103821","116046","118320","126348","129658","130115","137678","155748","157133"],//MALAD
	//     "pagesToShowOn": ["serp"],
	//     "saleTypeSupported": ["buy"],
	//     "id": "ADID_NS002"
	// },
	{
	    "redirectUrl": "https://onlineapply.sbi.co.in/personal-banking/home-loan-callback?se=Makaan&cp=banner&Ag=direct&utm_source=makaan&utm_medium=banner",
	    "image": "/images/un-versioned/nonSellers/sbi-banner.jpg",
	    "alt": "sbi",
	    "visiblityInCities": ["panIndia"],
	    "pagesToShowOn": ["serp"],
	    "saleTypeSupported": ["buy"],
	    "bannerOnly":true,
	    "id": "ADID_NS003"
	}];

	const service = {
		getPromotedNonSellers: function({cityId, localityOrSuburbIds, listingType}){
			let nonSellers =  NON_SELLERS.filter((nonSeller) => {
				return (nonSeller.visiblityInCities.indexOf("panIndia") > -1 || nonSeller.visiblityInCities.indexOf(cityId) > -1 ||
						utils.arraysIntersect(nonSeller.visiblityInLocalities, localityOrSuburbIds)) &&
					nonSeller.saleTypeSupported.indexOf(listingType) > -1;
			});
			//Return only one
			return nonSellers.slice(0, 1)
		}
	};

	Box.Application.addService(SERVICE_NAME, service);
	return service;
});