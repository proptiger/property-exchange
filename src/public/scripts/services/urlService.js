"use strict";
define(['services/commonService'], (CommonService) => {

    const SERVICE_NAME = 'URLService';

    Box.Application.addService(SERVICE_NAME, (application) => {

        var prevLocation = window.location;

        window.onpopstate = function() {
            let currentLocation = window.location;
            if(currentLocation.pathname == prevLocation.pathname && currentLocation.search == prevLocation.search) {
                return;
            }
            application.broadcast('searchParamsChanged', {
                'reinitModules': true
            });
        };

        var querySequences = [
            'beds',
            'baths',
            'propertyType',
            'propertyStatus',
            'budget',
            'area',
            'postedBy',
            'ownership',
            'possession',
            'ageOfProperty',
            'newOrResale',
            'furnished',
            'locality',
            'builder',
            'amenities',
            'sortBy',
        ];


        /*
            This function is used to rearrange query params based on sequence priority
        */
        function _updateQuerySequences(thisUrl) {
            let url = thisUrl.split('?')[0],
                newQS = '',
                queryParamObj = getAllQueryStringParam(thisUrl);

            if (Object.keys(queryParamObj).length) {
                for (let i = 0; i < querySequences.length; i++) {
                    let key = querySequences[i];
                    if (queryParamObj[key]) {
                        newQS += '&' + key + '=' + queryParamObj[key];
                        delete queryParamObj[key];
                    }
                }

                for (let key in queryParamObj) {
                    newQS += '&' + key + '=' + queryParamObj[key];
                }

                newQS = newQS.substring(1); // remove first character

                url += '?' + newQS;
            }

            return url;
        }

        function setPrevLocation(path) {
            prevLocation = path;
        }

        function ajaxifySameUrl(){
            CommonService.ajaxify(window.location.href, true);
        }

        /**
         * thisUrl = the url to set in the address bar
         **/
        function ajaxyUrlChange(thisUrl, reload, noAjaxy=false, reinitModules=false) {
            let currentUrlPathName, currentUrlOrigin, thisUrlPathName, differentPath, multiLocalityPage,previousUrlLocalityOrSuburbId,newUrlLocalityOrSuburbId;
            if (!thisUrl || thisUrl === window.location.href) {
                return;
            }else {
                currentUrlOrigin   = window.location.origin;
                currentUrlPathName = window.location.pathname;
                thisUrlPathName    = thisUrl.split('?')[0];
                if(thisUrl.indexOf('localityOrSuburbId') > -1){
                    multiLocalityPage = true;
                    previousUrlLocalityOrSuburbId = getAllQueryStringParam(window.location.href)['localityOrSuburbId'];
                    newUrlLocalityOrSuburbId = getAllQueryStringParam(thisUrl)['localityOrSuburbId'];
                }
            }

            if([currentUrlPathName, `${currentUrlOrigin}${currentUrlPathName}`].indexOf(thisUrlPathName) == -1){
                differentPath = true;
            }
            if((multiLocalityPage && previousUrlLocalityOrSuburbId && newUrlLocalityOrSuburbId && previousUrlLocalityOrSuburbId != newUrlLocalityOrSuburbId )){
                differentPath = true;
            }

            thisUrl = _updateQuerySequences(thisUrl);
            if (!differentPath && !reload && window.history && window.history.pushState){
                window.history.pushState({ url: thisUrl }, 'state-'+thisUrl, thisUrl);
                /** WARNING HACK FOR SOLVING POPUP CLOSE RELOAD ISSUE  **/
                window.currentURL = thisUrl;
                /** WARNING HACK FOR SOLVING POPUP CLOSE RELOAD ISSUE  **/
                application.broadcast('searchParamsChanged', {reinitModules});
            } else {
                CommonService.ajaxify(thisUrl,false,noAjaxy);
            }
            prevLocation = window.location;
        }

        function getQueryStringParam(key, href) {
            let searchString = window.location.search;
            if (href) {
                searchString = '?' + href.split('?')[1];
            }
            key = key.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            let regex = new RegExp("[\\?&]" + key + "=([^&#]*)"),
                results = regex.exec(searchString);
            return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        }

        function getAllQueryStringParam(thisUrl) {
            let url = thisUrl || window.location.search;

            let questionMark = url.indexOf('?');
            if (questionMark === -1) {
                return {};
            }
            url = url.substring(questionMark + 1);
            url = url.split('&');

            let param = {};
            for (let i = 0; i < url.length; i++) {
                let keyValue = url[i].split('=');
                param[keyValue[0]] = keyValue[1];
            }
            return param;
        }

        function _updateQSP(url, key, value) {
            let  i = url.indexOf('#');
            let hash = (i === -1) ? ''  : url.substr(i);
                url = (i === -1) ? url : url.substr(0, i);
            let re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
            let separator = url.indexOf('?') !== -1 ? "&" : "?";
            if (!value) {
                return removeQueryStringParam(key, url, true);
            } else if (url.match(re)) {
                return url.replace(re, '$1' + key + "=" + value + '$2');
            } else {
                url = url + separator + key + "=" + value;
            }
            return url + hash;
        }

        function updateQueryStringParameter(key, value, href, returnNewHref) {
            let url = href || window.location.href;
            let newHref = _updateQSP(url, key, value);
            if (returnNewHref) {
                return newHref;
            }
            ajaxyUrlChange(newHref);
        }

        function updateMultipleQueryStringParameter(param, href, returnNewHref) {
            if (typeof(param) !== 'object') {
                console.log("object should only be passed");
                return;
            }
            let key = Object.keys(param);
            let url = href || window.location.href;
            for (let i = 0; i < key.length; i++) {
                url = _updateQSP(url, key[i], param[key[i]]);
            }
            if (returnNewHref) {
                return url;
            }
            ajaxyUrlChange(url);
        }

        function removeQueryStringParam(key, href, returnNewHref) {
            let url = href || window.location.href,
                queryIndex = url.indexOf('?'), urlparts = [];
            if(queryIndex > -1) {
                urlparts = [url.substring(0, queryIndex), url.substring(queryIndex+1)];
            }

            if (urlparts.length >= 2) {
                let prefix = encodeURIComponent(key) + '=',
                    pars = urlparts[1].split(/[&;]/g);

                //reverse iteration as may be destructive
                for (let i = pars.length; i-- > 0;) {
                    //idiom for string.startsWith
                    if (pars[i].lastIndexOf(prefix, 0) !== -1) {
                        pars.splice(i, 1);
                    }
                }

                url = urlparts[0];
                if (pars.length > 0) {
                    url = urlparts[0] + '?' + pars.join('&');
                }

                if (returnNewHref) {
                    return url;
                }
                ajaxyUrlChange(url);
            }
            return url;
        }

        function removeMultipleQueryStringParam(param, href, returnNewHref){
            if (typeof(param) !== 'object') {
                console.log("object should only be passed");
                return;
            }
            let key = Object.keys(param);
            let url = href || window.location.href;
            for (let i = 0; i < key.length; i++) {
                url = removeQueryStringParam(key[i], url, true);
            }
            if(returnNewHref){
                return url;
            }
            ajaxyUrlChange(url);
        }
        return {
            ajaxyUrlChange,
            ajaxifySameUrl,
            getUrlParam: getQueryStringParam,
            getAllUrlParam: getAllQueryStringParam,
            changeUrlParam: updateQueryStringParameter,
            removeUrlParam: removeQueryStringParam,
            changeMultipleUrlParam: updateMultipleQueryStringParameter,
            setPrevLocation: setPrevLocation,
            removeMultipleUrlParam: removeMultipleQueryStringParam
        };
    });

    return Box.Application.getService(SERVICE_NAME);
});
