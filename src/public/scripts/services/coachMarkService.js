"use strict";
define([
        'common/sharedConfig',
        'services/utils',
        'services/localStorageService',
        'services/commonService',
    ], (sharedConfig, utils, localStorageService, commonService) => {
    const SERVICE_NAME = 'coachMarkService';

    Box.Application.addService(SERVICE_NAME, (context) => {
          const LOCAL_STORAGE_SELECT_KEY = "SELECT_COUPON_GENERATED",
                msBannerKnowMore = '.js-ms-coach-km',
                msBannerReminderUnlock = '.js-ms-coach-ru',
                filterClass = '.formobilefilters',
                COACH_KNOW_MORE_TEXT = `<span onclick="Box.Application.broadcast('coachMarkKnowMore')"><span class="txt-content"><span>&#x20B9;2000 discount on brokerage by Select Sellers</span></span><span class="s-arrow"></span></span>`,
                COACH_UNLOCK_TEXT = `<span onclick="Box.Application.broadcast('coachMarkUnlockVoucher')"><span class="txt-content"><span>Don't forget to unlock Select Voucher before paying the brokerage</span></span><span class="s-arrow"></span></span>`
                ;
        
        function initBannerCoachMark() {
            if ($(msBannerKnowMore).length || $(msBannerReminderUnlock).length) {
                let text = COACH_UNLOCK_TEXT,
                    selector = msBannerReminderUnlock,
                    couponData = _getCouponData(),
                    coachMarkData = utils.selectCoachMarkVisibility(couponData,sharedConfig.couponShareEligibleTime),
                    tooltipClass = 'makaan-select-banner',
                    offset = 0;

                if (!utils.getPageData('isSerp')){
                    tooltipClass = `${tooltipClass} prop-banner`;
                    offset = 300;
                }

                if (coachMarkData && coachMarkData.coachMarkKey == sharedConfig.coachMarkSelector.knowMoreKey) {
                    text = COACH_KNOW_MORE_TEXT;
                    selector = msBannerKnowMore;
                }
                if (coachMarkData && coachMarkData.coachMarkFlag) {
                    $(filterClass).addClass('hide');
                    _initCoachMark({
                        text,
                        selector,
                        tooltipClass,
                        offset,
                        highlightClass: '',
                        position: 'bottom-right-aligned',
                        onClose: function(){
                            $(filterClass).removeClass('hide');
                        }
                    });
                }
            }
        }
        function _getCouponData(){
            return localStorageService.getItem(LOCAL_STORAGE_SELECT_KEY);
        }
        function _initCoachMark(coachMarkData) {
            context.broadcast('loadModule', {
                name: 'selectCoachMark',
                loadId: 'selectCoachMark'
            });
            commonService.bindOnModuleLoad('selectCoachMark', () => {
                if (utils.getPageData('isSerp')) {
                    commonService.bindOnModuleLoad('filter', () => {
                        context.broadcast('showBannerCoachMark', coachMarkData);
                    });
                } else {
                    context.broadcast('showBannerCoachMark', coachMarkData);
                }
            });
        }
        return {
            initBannerCoachMark
        };
    });
    return Box.Application.getService(SERVICE_NAME);
});
