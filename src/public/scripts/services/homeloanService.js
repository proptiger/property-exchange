'use strict';
define(['services/utils',
    'common/sharedConfig',
    'services/apiService',
    'common/utilFunctions',
    'services/commonService',
    'services/localStorageService',
    'services/utils',
    'services/filterConfigService',
    'services/urlService'
], (Utils, SharedConfig, ApiService, utilFunctions, commonService, localStorageService, UtilService, FilterConfigService, URLService) => {
    const SERVICE_NAME = 'homeloanService';
    Box.Application.addService(SERVICE_NAME, () => {
        const ApiHandlers = SharedConfig.apiHandlers;
        const clientApis = SharedConfig.clientApis;
        const query = Box.DOM.query;

        const homeloanConfig = {
            interest: 9,
            tenure: 20,
        };

        const textFieldConfig=[{
            key:'summary_name',
            selector:'.js-name',
            validation:['required'],
            errorMsg:"Please enter valid name"
        },{
            key:'summary_dob',
            selector:'.js-dobtimeStamp',
            validation:['required'],
            errorMsg:"Please enter valid date of birth"
        },{
            key:'summary_email',
            selector:'.js-email',
            validation:['required','validateEmail'],
            errorMsg:"Please enter valid email"
        },{
            key:'summary_mobile',
            selector:'.js-mobile',
            validation:['required','validateMobile'],
            errorMsg:"Please enter valid phone number"
        },{
            key:'income_grossEarning',
            selector:'.js-salary',
            validation:['required','isNumber','lessThanMax'],
            errorMsg:"Please enter valid income"
        },{
            key:'income_existingEmi',
            selector:'.js-emi',
            validation:['required','isNumber'],
            errorMsg:"Please enter valid emi"

        },{
            key:'loan_loanAmount',
            selector:'.js-loanAmount',
            validation:['required','isNumber','moreThanMinimum'],
            errorMsg:"Please enter valid loanAmount"
        }];
        const optionalTextFieldConfig=[{
            key:'co_name',
            selector:'.js-coName',
            validation:['required'],
            errorMsg:"Please enter valid name"
        },{
            key:'co_grossEarning',
            selector:'.js-coSalary',
            validation:['required','isNumber'],
            errorMsg:"Please enter valid income"
        }];

        const typeaheadFieldConfig=[{
            key:'summary_cityId',
            selector:"input[name='summary_city_name']",
            validation:['requiredResidenceCity'],
            errorMsg:"Please enter valid city",
        },{
            key:'project_cityId',
            selector:"input[name='project_city_name']",
            validation:['required'],
            errorMsg:"Please enter valid city name",
        }];
        function returnTextFieldConfig(){
            return textFieldConfig;
        }
        function returnTypeaheadFieldConfig(){
            return typeaheadFieldConfig;
        }
        function returnHomeloanConfig(){
            return homeloanConfig;
        }
        function returnOptionalTextFieldConfig(){
            return optionalTextFieldConfig;
        }

        var postEnquiry = (payload) => {
            return ApiService.postJSON(ApiHandlers.postEnquiry().url, payload);
        };
        var submitOtp=(payload,enquiryId, otp)=> {
            payload = JSON.stringify(payload);
            return ApiService.putJSON(clientApis.putEnquiryOTPVerified(enquiryId, otp).url,payload);
        };
        var checkIndiaBullsEligibilty = (payload) =>{
            payload = JSON.stringify(payload);
            return ApiService.postJSON(clientApis.checkIndiaBullsEligibilty().url,payload);
        };
        var postIndiaBullsEnquiry = (payload) =>{
            payload = JSON.stringify(payload);
            return ApiService.postJSON(clientApis.postIndiaBullsEnquiry().url,payload);
        };
        return {
            postEnquiry,
            submitOtp,
            checkIndiaBullsEligibilty,
            postIndiaBullsEnquiry,
            returnHomeloanConfig,
            returnTypeaheadFieldConfig,
            returnTextFieldConfig,
            returnOptionalTextFieldConfig
        }
    });
    return Box.Application.getService(SERVICE_NAME);
});
