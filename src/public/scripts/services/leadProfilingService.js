/*
 * @author [Gaurav Sharma]
 */

'use strict';

define(['common/sharedConfig',
    'services/apiService',
    'services/defaultService',
    'services/localStorageService',
], (SharedConfig, apiService, defaultService, localStorageService) => {
    const SERVICE_NAME = 'leadProfilingService';

    Box.Application.addService(SERVICE_NAME, () => {
        function getLeadProfileQuestion(data) {
            let url = SharedConfig.apiHandlers.getLeadProfileQuestion({query:data}).url;
            return apiService.get(url).then(function (response) {
                if (response) {
                    return response;
                }
                return {};
            });
        }

        function saveLeadProfileQuestion(data){
            let url = SharedConfig.apiHandlers.updateLeadProfileQuestion().url;
            if(data.id){
                return apiService.putJSON(url, JSON.stringify(data)).then((response) => {
                    return response || {};
                });
            } else {
                return apiService.postJSON(url, JSON.stringify(data)).then((response) => {
                    return response || {};
                });
            }
        }

        function checkUserPhoneVerified({userId}){
            return apiService.get(SharedConfig.apiHandlers.userDetails(userId).url).then(response => {
                let isVerified =  _checkPhoneVerified(response);
                return {
                    isVerified: isVerified,
                    details: response
                };
            }, (err) => {
                return err;
            }); 
        }

        function _checkPhoneVerified(response) {
            let isVerified = false;
            response.contactNumbers.forEach(function (element) {
                if (element.isVerified){
                    isVerified = true;
                }
            });
            return isVerified;
        }

        function getQuestionType(){
            return SharedConfig.LEAD_PROFILE_QUESTION_TYPE;
        }

        return {
            getLeadProfileQuestion,
            saveLeadProfileQuestion,
            getQuestionType,
            checkUserPhoneVerified
        };
    });

    return Box.Application.getService(SERVICE_NAME);
});