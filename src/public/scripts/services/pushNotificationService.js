define([
    "services/utils"
], () => {
    'use strict';
    const SERVICE_NAME = 'pushNotificationService';

    Box.Application.addService(SERVICE_NAME, () => {
        let utils = Box.Application.getService("Utils");
        const EVENT_TAG_MAP = {
            "LEAD_OPEN": {
                "Lead Interest": "true"
            },
            "LEAD_SUBMIT": {
                "Converted": "true"
            },
            "OTP_VERIFIED": {
                "Verified Convert": "true"
            },
            "VISITED_SERP": {},
            "VISITED_SERP_MAPS": {},
            "USER_MESSAGE_SENT": {
                "Chat Sent": "true"
            },
            "AGENT_MESSAGE_SENT": {
                "Chat Received": "true"
            },
            "CHAT_LEAD_FILLED": {
                "Chat Convert": "true"
            },
            "CHAT_OTP_VERIFIED": {
                "Chat Verified Convert": "true"
            },
            "VIEWED_PROPERTY": {
                "Viewed Property": "true"
            },
            "NewCall": {
                "Call Started": "true"
            },
            "Dial": {
                "Call Connected": "true"
            },
            "call_feedback": {
                "Call successful": "true"
            },
            "FEEDBACK_OPEN": {
                "Feedback Shown": "true"
            },
            "FEEDBACK_STORED": {}
        };
        const BUY_ROUND_OFF_LIMIT = 50000,
            RENT_ROUND_OFF_LIMIT = 1000;

        var eventsActivated = false;

        function activateEventTagging() {
            eventsActivated = true;
            window.sendNotificationTags = sendNotificationTags;
        }

        function _getNotificationTag(tag) {
            if (!eventsActivated) {
                return new Promise(function(resolve, reject){
                    reject('events not activated');
                });
            }
            let OneSignal = window.OneSignal || [];
            if(typeof OneSignal === "function") {
                return OneSignal.getTags().then(function(tagsData) {
                    return tagsData[tag];
                });
            } else {
                return new Promise(function(resolve, reject){
                    reject('unable to load OneSignal');
                });
            }
        }

        function sendNotificationTags(event, {
            cityId, localityId, projectId, budget, bhk, sellerId, callId, serpType, reason, 
            cityName, localityName, projectName
        } = {}) {
            if (!eventsActivated && !event && !EVENT_TAG_MAP[event]) {
                return;
            }
            if(!utils){
                utils = Box.Application.getService("Utils");
            }
            let OneSignal = window.OneSignal || [],
                tagData = EVENT_TAG_MAP[event] || {},
                pageData = utils.getPageData();

            cityId = cityId || pageData.cityId;
            localityId = localityId || pageData.localityId;
            projectId = projectId || pageData.projectId;
            serpType = serpType || pageData.trackPageType;
            cityName = cityName || pageData.cityName;
            localityName = localityName || pageData.localityName;
            projectName = projectName || pageData.projectName;
            budget = budget || pageData.budget;
            if (cityId && cityName) {
                tagData["City"] = cityId;
                tagData["cityName"] = cityName;
            }
            if (localityId && localityName) {
                tagData["Locality"] = localityId;
                tagData["localityName"] = localityName;
            }  
            if (projectId && projectName) {
                tagData["Project"] = projectId;
                tagData["ProjectName"] = projectName;
            }  
            
            function _addBudget() {
                if (pageData.listingType === "buy") {
                    tagData["Budget"] = Math.round(budget / BUY_ROUND_OFF_LIMIT) * BUY_ROUND_OFF_LIMIT;
                } else if (pageData.listingType === "rent") {
                    tagData["Budget"] = Math.round(budget / RENT_ROUND_OFF_LIMIT) * RENT_ROUND_OFF_LIMIT;
                }
            }

            function _addListingType() {
                if(pageData.listingType){
                    tagData["listingType"] = pageData.listingType;
                }
            }

            switch (event) {
                case "LEAD_OPEN":
                case "LEAD_SUBMIT":
                case "OTP_VERIFIED":
                case "USER_MESSAGE_SENT":
                case "AGENT_MESSAGE_SENT":
                case "CHAT_LEAD_FILLED":
                case "CHAT_OTP_VERIFIED":
                    break;

                case "VISITED_SERP":
                case "VISITED_SERP_MAPS":
                    //add remaining data into tagData
                    tagData["SERP type"] = serpType;
                    tagData["LAST_VISITED_PAGE"] = event;
                    break;
                case "LOCALITY_OVERVIEW":
                case "PROJECT_OVERVIEW":
                case "CITY_OVERVIEW":
                    tagData["LAST_VISITED_PAGE"] = event;
                break;
                case "AGENT_PAGE":
                    tagData["LAST_VISITED_PAGE"] = event;
                    _addListingType();
                break;
                case "LISTING_OVERVIEW":
                    tagData["LAST_VISITED_PAGE"] = event;
                    _addBudget();
                    _addListingType();
                    break;
                // case "VIEWED_PROPERTY":
                //     // add remaining data into tagData
                //     tagData["SERP type"] = serpType;
                //     _addBudget();
                //     tagData["BHK"] = bhk;
                //     break;

                case "NewCall":
                    tagData["Call ID"] = callId;
                    _addBudget();
                    tagData["BHK"] = bhk;
                    tagData["City"] = cityId;
                    tagData["Locality"] = localityId;
                    tagData["Seller"] = sellerId;
                    break;

                // case "call_feedback":
                //     _getNotificationTag("Feedback_Required").then((callIds) => {
                //         if(callIds){
                //             callIds = JSON.parse(callIds);
                //         } else {
                //             callIds = [];
                //         }
                //         callIds.push(callId);
                //         tagData["Feedback_Required"] = JSON.stringify(callIds);
                //         tagData["Call ID"] = callId;
                //         OneSignal.push(["sendTags", tagData]);
                //     });
                //     return;
                //     break;  //jshint ignore:line

                // case "Dial":
                // case "FEEDBACK_OPEN":
                //     tagData["Call ID"] = callId;
                //     break;

                // case "FEEDBACK_STORED":
                //     _getNotificationTag("Feedback_Required").then((callIds) => {
                //         if(callIds){
                //             callIds = JSON.parse(callIds);
                //         } else {
                //             callIds = [];
                //         }
                //         let index = callIds.indexOf(callId);
                //         callIds.splice(index, 1);
                //         tagData["Feedback_Required"] = JSON.stringify(callIds);
                //         tagData["Feedback Submitted"] = callIds.length > 0 ? "false" : "true";
                //         tagData["Call ID"] = callId;
                //         tagData["Reason"] = reason;
                //         OneSignal.push(["sendTags", tagData]);
                //     });
                //     return;
                //     break;  //jshint ignore:line

            }

            OneSignal.push(["sendTags", tagData]);
        }
        function getUserId(){
            if(OneSignal && OneSignal.getUserId){
                return OneSignal.getUserId()
            } else {
                return new Promise((resolve,reject)=>{
                    reject()
                })
            }
        }
        return {
            activateEventTagging,
            sendNotificationTags,
            getUserId
        };

    });

    return Box.Application.getService(SERVICE_NAME);
});
