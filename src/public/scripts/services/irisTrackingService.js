define(['common/utilFunctions', 'services/apiService', 'common/sharedConfig'], (Utils, apiService, sharedConfig)=>{
  'use strict';
  const SERVICE_NAME = 'IRISTrackingService';

  Box.Application.addService(SERVICE_NAME, () => {
    var userId, gaUid, anonymousId, amplitudeCookie;

    function setGaUid(){
      if (typeof window !== 'undefined' && window.ga) {
        window.ga((tracker) => {
          try{
            gaUid = tracker.get('clientId').split('.')[0];
          }
          catch(e){}          
        })
      }
      return gaUid;
    }

    function setIRISCookie(iris_id) {
      Utils.setcookie('iris_id', iris_id, {
        'max-age': 60 * 60 * 24 * 365 * 20,
        domain: document.domain,
        path: '/'
      });
    }

    function getAnonymousId() {
      let anonymousId = Utils.getCookie('iris_id'); //Check IRIS_ID cookie if available
      if (!anonymousId) {
        anonymousId = Utils.getCookie('ajs_anonymous_id') //read segment cookie if available
        if (anonymousId) {
          anonymousId = anonymousId.replace(/%22/g, ''); //to remove extra "" inserted by segment
        } else {
          anonymousId = Utils.generateUniqueId();
        }
        setIRISCookie(anonymousId);
      }
      return anonymousId;
    }

    function  postEvent(msgbody) {
      let url = sharedConfig.clientApis.eventRouterTracking().url;
      url = `${window.EVENT_ROUTER_URL}${url[0]==='/'?url.slice(1):url}`;
      apiService.postJSON(url, msgbody).then(response => {
        //consolelog("Event posted successfully");
      }, e => {
        console.error("error while posting event to event router");
      })
    }

    function trackEvent(event, properties){
      if (!gaUid) {
         setGaUid(); 
      }
      if (!anonymousId) {
        anonymousId = getAnonymousId();
      }
      let hostName = location.hostname.split('.'),
        domain=hostName.pop(),
        host=hostName.pop();
      
      var msgBody = {
        message: [{
          gaUid: gaUid,
          anonymousId: anonymousId,
          event: event,
          properties: properties,
          url: document.location.href,
          path: document.location.pathname,
          title: document.title,
          userAgent: navigator.userAgent,
          ip: Utils.getCookie('USER_IP'),
          timestamp: Date.now(),
          userId: userId,
          projectId: window.IRIS_PROJECT_ID,
        }]
      };
      postEvent(msgBody);
    }

    function trackPageView(pageType, onmitureData) {
       let data = $.extend(true,{
         url: document.location.href,
         path: document.location.pathname,
       },onmitureData);
       trackEvent(pageType, data);
    }

    function trackIdentity(usrId) {
        userId = usrId;
    }

    return {
      trackEvent,
      trackPageView,
      trackIdentity
    }
  })

  return Box.Application.getService(SERVICE_NAME);
})
