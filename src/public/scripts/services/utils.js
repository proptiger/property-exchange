"use strict";
define(['common/utilFunctions',
    'common/sharedConfig',
    'services/localStorageService',
    'common/trackingConfigService',
    /* 'services/amplitudeTrackingService', */
    'services/irisTrackingService',
    'services/pushNotificationService',
    'services/intersectionObserverService'
], (uf, sharedConfig, localStorageService, t, /* amplitudeTrackingService, */irisTrackingService,pushNotificationService,intersectionObserver) => {
    let utilfunc = uf;
    const SERVICE_NAME = 'Utils',
        LAST_VISITED_PAGE_DETAILS = 'LAST_VISITED_PAGE_DETAILS',
        IMAGE_AR_ALLOWED_DIFF = 75,
        IMAGE_AR_MIN_DIFF = 50,
        EXIT_INTENT_MAPPING = {
            0: 'AM',
            1: 'BT',
            2: 'YQ',
            3: 'PO',
            4: 'UE',
            5: 'TD',
            6: 'LF',
            7: 'NA',
            8: 'OV',
            9: 'IX'
        };
    Box.Application.addService(SERVICE_NAME, (application) => {

         function observeDomElement(elements = [],broadcastMessage='', trackOnlyOnce=false) {
            return intersectionObserver.observeElements(elements, broadcastMessage, trackOnlyOnce);
        }

        utilfunc.setcookie = function(cname, cvalue) {
            document.cookie = cname + "=" + cvalue + ";path=/";
        };
        utilfunc.getCookie = function(key) {
            var keyValue = document.cookie.match('(^|;) ?' + key + '=([^;]*)(;|$)');
            return keyValue ? keyValue[2] : null;
        };
        utilfunc.deleteCookie = function(key) {
            document.cookie = key + '=;path=/;expires=Thu, 01-Jan-70 00:00:01 GMT;';
        };
        utilfunc.isMobileRequest = function() {
            let ua = window.navigator.userAgent;
            if (/(mobile|nexus 7)/i.test(ua)) { // added nexus
                return true;
            }
            return false;
        };
        utilfunc.isIphone = function(){
            let ua = window && window.navigator && window.navigator.userAgent;
            let isIphone = ua && /iPhone/i.test(ua) || false;
            return isIphone;
        },
        utilfunc.getBrowserName = function(){
            let ua = window.navigator.userAgent;
            if((ua.indexOf("Opera") || ua.indexOf('OPR')) != -1 ) 
            {
                return 'Opera';
            }
            else if(ua.indexOf("Chrome") != -1 )
            {
                return 'Chrome';
            }
            else if(ua.indexOf("Safari") != -1)
            {
                return 'Safari';
            }
            else if(ua.indexOf("Firefox") != -1 ) 
            {
                 return 'Firefox';
            }
            else if((ua.indexOf("MSIE") != -1 ) || (!!(document.documentMode) === true )) //IF IE > 10
            {
              return 'IE'; 
            }  
            else 
            {
               return 'unknown';
            }
    
        };
        utilfunc.getOmniturePageName = function() {
            let pageData = uf.getPageData(),
                strategyPageTypeMap = t.PAGE_NAME_STRATEGY_MAP,
               _mapF = strategyPageTypeMap[pageData['pageType']],
               pageName = "Others";
             if(_mapF && typeof _mapF === "function" ) {
                pageName = _mapF(pageData);
             }
             return pageName;
        };

        utilfunc.getEmptyPromise = function( data = {}){
            var defer = $.Deferred();
            defer.resolve(data);
            return defer.promise();
        };

        utilfunc.getExitIntentPhoneEncrypt = function (number = '', separator = '-') {
            return number.toString().split('').map(item => EXIT_INTENT_MAPPING[item]).join(separator);
        };

        utilfunc.elementInViewport = function(el) {
            if(!el) {return false;}
            var top = el.offsetTop;
            var left = el.offsetLeft;
            var width = el.offsetWidth;
            var height = el.offsetHeight;

            while (el.offsetParent) {
                el = el.offsetParent;
                top += el.offsetTop;
                left += el.offsetLeft;
            }

            return (
                top < (window.pageYOffset + window.innerHeight) &&
                left < (window.pageXOffset + window.innerWidth) &&
                (top + height) > window.pageYOffset &&
                (left + width) > window.pageXOffset
            );
        };
        utilfunc.trackEvent = function(action, category, label, value, nonInteraction){
            var properties = {
                category: category,
                label: label,
                value: value,
                nonInteraction: nonInteraction ? 1 : undefined
            };
            //amplitudeTrackingService.trackEvent(action, properties);
            irisTrackingService.trackEvent(action, properties);
            if(typeof ga === 'function'){
                ga('send', 'event', category, action, label, value, nonInteraction ? {'nonInteraction': 1} : undefined);
            }
        };

        // userId : string
        // traits : object with all properties
        utilfunc.trackIdentity = function(userId, traits){
            //amplitudeTrackingService.trackIdentity(userId,traits);
            irisTrackingService.trackIdentity(userId,traits);
        };

        utilfunc.showOnSlowSpeed = function(element, className, speed) {
            if (window.clientCurrentSpeed < speed) {
                $(element).removeClass(className);
            }
        };



        utilfunc.getPageTimingData = function(){
            let timing = {};
            if(window.performance && window.performance.timing){
                let TimingService = window.performance.timing;
                timing.pageLoadTime = TimingService.domComplete - TimingService.connectStart;
                timing.domReadyTime = TimingService.domInteractive - TimingService.connectStart;
            }
            return timing;
        };

        utilfunc.getPageExtraData = function(){
            try{
                return JSON.parse($('script[type="text/x-page-config"]').eq(0).text());
            }catch(e){
                return {};
            }
        };

        utilfunc.getTrackPageCategory = function(){
            let pageType = utilfunc.getPageData('pageType'),
                pageTypeMap = {
                    'HOME_PAGE_URLS': 'Home',
                    'PROJECT_URLS_OVERVIEW': 'Project',
                    'CITY_URLS_OVERVIEW': 'City',
                    'SUBURB_URLS_OVERVIEW': 'Locality',
                    'LOCALITY_URLS_OVERVIEW': 'Locality',
                    'PROPERTY_URLS': 'Property',
                    'BUILDER_URLS': 'SERP_BUILDER',
                    'BUILDER_TAXONOMY_URLS': 'SERP_BUILDER',
                    'COMPANY_URLS': 'SERP_BROKER',
                    'SELLER_PROPERTY_URLS': 'SERP_SELLER',
                    'CITY_URLS': 'SERP_CITY',
                    'CITY_URLS_COMMERCIAL': 'SERP_CITY',
                    'CITY_TAXONOMY_URLS': 'SERP_CITY',
                    'STATIC_URLS': 'SERP_STATIC',
                    'PROJECT_URLS': 'SERP_PROJECT',
                    'NEARBY_LISTING_URLS': 'SERP_LANDMARK',
                    'NEARBY_URLS': 'SERP_LANDMARK',
                    'NEARBY_LISTING_TAXONOMY_URLS': 'SERP_LANDMARK',
                    'SIMILAR_PROPERTY_URLS': 'SERP_CHILD',
                    'LOCALITY_URLS': 'SERP_LOCALITY',
                    'LOCALITY_TAXONOMY_URLS': 'SERP_LOCALITY',
                    'SUBURB_URLS': 'SERP_SUBURB',
                    'SUBURB_TAXONOMY_URLS': 'SERP_SUBURB',
                    'LOCALITY_SUBURB_TAXONOMY': 'SERP_LOCALITY', // to handle fallback for previous support
                    'LISTINGS_PROPERTY_URLS': 'SERP_GENERAL',
                    'CITY_URLS_MAPS': 'SERP_MAP_CITY',
                    'CITY_TAXONOMY_URLS_MAPS': 'SERP_MAP_CITY',
                    'STATIC_URLS_MAPS': 'SERP_MAP_STATIC',
                    'PROJECT_URLS_MAPS': 'SERP_MAP_PROJECT',
                    'NEARBY_LISTING_URLS_MAPS': 'SERP_MAP_LANDMARK',
                    'NEARBY_URLS_MAPS': 'SERP_MAP_LANDMARK',
                    'NEARBY_LISTING_TAXONOMY_URLS_MAPS': 'SERP_MAP_LANDMARK',
                    'SIMILAR_PROPERTY_URLS_MAPS': 'SERP_MAP_CHILD',
                    'LOCALITY_URLS_MAPS': 'SERP_MAP_LOCALITY',
                    'LOCALITY_TAXONOMY_URLS_MAPS': 'SERP_MAP_LOCALITY',
                    'SUBURB_URLS_MAPS': 'SERP_MAP_SUBURB',
                    'SUBURB_TAXONOMY_URLS_MAPS': 'SERP_MAP_SUBURB',
                    'LOCALITY_SUBURB_TAXONOMY_MAPS': 'SERP_MAP_LOCALITY', // to handle fallback for previous support
                    'LISTINGS_PROPERTY_URLS_MAPS': 'SERP_MAP_GENERAL',
                    'BUILDER_URLS_MAPS': 'SERP_MAP_BUILDER',
                    'BUILDER_TAXONOMY_URLS_MAPS': 'SERP_MAP_BUILDER',
                    'COMPANY_URLS_MAPS': 'SERP_MAP_BROKER',
                    'SELLER_PROPERTY_URLS_MAPS': 'SERP_MAP_SELLER',
                    'BUYER_DASHBOARD': 'DASHBOARD',
                    'SERVICES': 'Services_BD',
                    '404': '404',
                    '500': '500',
                    'bw':'BUSINESS_WORLD'
                };
            return pageTypeMap[pageType];
        };

        utilfunc.getPageData = function(name) {

            let data = window.pageData || {};
            data.listingType = data.listingType || 'buy';

            if (name) {
                return data[name];
            }
            return data;
        };

        utilfunc.setPageData = function(name, value) {

            let data = window.pageData || {};

            if (name && (value || value===false)) {
                data[name] = value;
            }

            window.pageData = data;
            return data;
        };

        utilfunc.listingsPropertyFullPath = function() {
            let prefix = utilfunc.getPageData('isMap') ? '/maps' : '',
                pathName = utilfunc.listingsPropertyPathName();

            return `${prefix}${pathName}`;
        };

        utilfunc.gotoEl = function(dest, threshold, offset, callback) {
            callback = typeof callback !== 'function' ? function(){} : callback;
            let destination = $(dest),
                scrollTo = destination.offset().top;
            scrollTo -= threshold && threshold < scrollTo ? offset : 0;
            $('body,html').animate({
                'scrollTop': scrollTo
            }, 'slow', callback);
        };

        utilfunc.scrollSpy = function (cont, dest, offset=0, callback) {
            callback = typeof callback !== 'function' ? function(){} : callback;
            let container = $(cont),
                destination = $(dest),
                scrollTo = "+="+(destination.position().top + offset - container.position().top)+"px";
            container.animate({
                'scrollTop': scrollTo
            }, 'fast', callback);
        };

        /**
         * [scrollToTop description] Scrolls to top of page on event of an element
         * @param  {[String]} element Selector of element on wholse event page is scrolled
         * @return {[String]} event event triggering the scroll
         */
        utilfunc.scrollToTop = function(element, event) {
            $(element).on(event, function() {
                utilfunc.gotoEl('body', 0, 0);
            });
        };

        utilfunc.getGeoLocation = function(options) {
            return new Promise(function(resolve, reject) {
                navigator.geolocation.getCurrentPosition(function(position) {
                    resolve(position);
                }, function(error) {
                    reject(error);
                }, options);
            });
        };

        utilfunc.generateSerpFilter = function(data) {
            let filterKeyMap = {
                    beds: ["beds"],
                    propertyType: ["propertyType"],
                    area: ["minSize", "maxSize"],
                    budget: ["minPrice", "maxPrice"],
                    localityOrSuburbId: ["localityOrSuburbId"],
                    listingCategory: ["listingCategory"],
                    listingType: ["listingType"],
                    localityId: ["localityId"],
                    localityName: ["localityName"],
                    cityId: ["cityId"],
                    cityName: ["cityName"],
                    suburbId: ["suburbId"],
                    suburbName: ["suburbName"],
                    furnished: ["furnished"]
                },
                filter = {},
                key,
                k,
                urlObj,
                getURL = sharedConfig.getURL;
            for (key in filterKeyMap) {
                filter[key] = [];
                let mappedKeys = filterKeyMap[key];
                for (k = 0; k < mappedKeys.length; k++) {
                    if (data[mappedKeys[k]]) {
                        filter[key].push(data[mappedKeys[k]]);
                    }
                }
                let length = filter[key].length;
                if (length) {
                    // if (length === 2 && filter[key][0] === filter[key][1]) {
                    //     filter[key].splice(1, 1);
                    // }
                    filter[key] = filter[key].join(",");
                } else {
                    delete filter[key];
                }
            }
            urlObj = {
                url: ""
            };
            return Object.keys(filter).length ? getURL(urlObj, filter).url : null;
        };

        utilfunc.checkInViewPort = function(element, extra) {
            extra = extra || 65;
            let elem = element && $(element);
            if (!(elem && elem.length)) {
                return false;
            }
            let elementTop = elem.offset().top - extra,
                viewPort = {
                    top: $(window).scrollTop(),
                    bottom: $(window).scrollTop() + $(window).height()
                };
            return viewPort.top<=elementTop && viewPort.bottom>elementTop;
        };

        utilfunc.attributeListInViewPort = function(element,dataAttribute,broadcastMessage='') {
            let elem = element && $(element),inViewPort,currAttributeVal,dataAttributeList=[],elementsInViewPortList=[];
            if ((elem && elem.length)) {
                for (var i =0; i<elem.length ; i++){
                    inViewPort = utilfunc.checkInViewPort(elem[i]);
                    if(inViewPort){
                        currAttributeVal = $(elem[i]).data()[dataAttribute];
                        dataAttributeList.push(currAttributeVal);
                        elementsInViewPortList.push(elem[i]);
                    }
                }
            }
            if(elementsInViewPortList && elementsInViewPortList.length){
                application.broadcast(broadcastMessage, {
                    elements: elementsInViewPortList
                });
            }
            return dataAttributeList;
        };
        utilfunc.trackElementInView = function (elements,elementSelector,dataAttribute,broadcastMessage, trackOnlyOnce){
            if(!observeDomElement(elements,broadcastMessage, trackOnlyOnce)){
                utilfunc.attributeListInViewPort(elementSelector,dataAttribute,broadcastMessage);
                let scrollFunction  = function() {
                    utilfunc.attributeListInViewPort(elementSelector,dataAttribute,broadcastMessage);
                    if(trackOnlyOnce) {
                        window.removeEventListener('scroll',scrollFunction,{passive:true});
                    }
                };
                window.addEventListener('scroll',scrollFunction,{passive:true});
            }
        };


        utilfunc.isScrolledTo = function(elem) {
            if (elem && elem.length>0) {
                var docViewTop = $(window).scrollTop(); //num of pixels hidden above current screen
               // var docViewBottom = docViewTop + $(window).height();
                var elemTop = $(elem).offset().top; //num of pixels above the elem
                //var elemBottom = elemTop + $(elem).height();
                return ((elemTop <= docViewTop || elemTop >= docViewTop));
            }
            return false;
        };

        utilfunc.flattenObjectToArray = function(obj, result) {
            result = result || [];
            $.each(obj, (k, v) => {
                if ($.isArray(v)) {
                    result.push(v);
                } else if (typeof v === 'object') {
                    return utilfunc.flattenObjectToArray(v, result);
                }
            });
            return Array.prototype.concat.apply([], result);
        };

        utilfunc.isValidValue = function(value) {
            value = $.trim(value);
            if ( value.length > 2 && value.length < 50) {
                return true;
            }
            return false;
        };

        utilfunc.isArray = function(data) {
            if (typeof Array.isArray !== "undefined") {
                return Array.isArray(data);
            }

            return Object.prototype.toString.call(data) === '[object Array]';
        };

        utilfunc.triggerPageInteractive = function(){
            $(window).trigger('pageCompleted');
        };

        utilfunc.getTrackingListingCategory = function(listingCategory) {
            if (!listingCategory) {
                return;
            }

            return listingCategory;
        };

        utilfunc.copyToClipboard = function(targetArea) {
            // create hidden text element, if it doesn't already exist
            var targetId = "_hiddenCopyText_";

            var target = document.getElementById(targetId);
            if (!target) {
                target = document.createElement("textarea");
                target.style.position = "fixed";
                target.style.left = "0";
                target.style.top = "0";
                target.style.opacity = "0";
                target.style["z-index"] = "-1";
                target.setAttributeNode(document.createAttribute("readonly","true"));
                target.id = targetId;
                document.body.appendChild(target);
            }

            target.textContent = $(targetArea).html();

            // select the content
            var currentFocus = document.activeElement;
            target.focus();
            target.setSelectionRange(0, target.value.length);

            // copy the selection
            var succeed;
            try {
                succeed = document.execCommand("copy");
            } catch (e) {
                succeed = false;
            }
            // restore original focus
            if (currentFocus && typeof currentFocus.focus === "function") {
                currentFocus.focus();
            }

            // clear temporary content
            target.textContent = "";

            return succeed;
        };

        utilfunc.bindWindowEvent = function(name, callback){
            let scrollEventList = ["wheel", "mousewheel", "touchstart", "touchmove", "scroll"],
                supportsPassive = false;
            // polyfill for Feature Detection of 'passive' support if scroll event
            // Soure: https://github.com/WICG/EventListenerOptions/blob/gh-pages/explainer.md#feature-detection
            if(scrollEventList.indexOf(name)>-1){
                try {
                  var opts = Object.defineProperty({}, 'passive', {
                    get: function() {
                      supportsPassive = true;
                    }
                  });
                  window.addEventListener("testPassive", null, opts);
                  window.removeEventListener("testPassive", null, opts);
                } catch (e) {}
            }
            // Use our detect's results. passive applied if supported, capture will be false either way.
            window.addEventListener(name, callback, supportsPassive ? { passive: true } : false);
        };

        utilfunc.formatPhoneNumberStandard = function(phoneNumber) {
            var arrPhoneNumber = phoneNumber.split('');
            arrPhoneNumber.splice(3,0,' ');
            arrPhoneNumber.splice(8,0,' ');
            arrPhoneNumber.splice(13,0,' ');
            var newphoneNumber = arrPhoneNumber.join('');
            return newphoneNumber;
        };

        utilfunc.openPopup = function(id, isModal){
            application.broadcast('popup:open', {
                id: id,
                options: {
                    config: {
                        modal: isModal ? true : false
                    }
                }
            });
        };
        utilfunc.setLastVisitedPageDetails = function(pageData){
            if(pageData && pageData.url && pageData.displayAreaLabel){
                localStorageService.setItem(LAST_VISITED_PAGE_DETAILS, {
                    url: pageData.url,
                    moduleName: pageData.moduleName,
                    displayAreaLabel: pageData.displayAreaLabel,
                    pageLevel: pageData.pageLevel,
                    listingType: pageData.listingType,
                    sellerName: pageData.sellerName,
                    localityName: pageData.localityName || pageData.suburbName,
                    projectName: pageData.projectName,
                    cityName: pageData.cityName,
                    builderName: pageData.builderName,
                    suburbName: pageData.suburbName,
                    placeId: pageData.placeId
                });
            }
        }
        utilfunc.getLastVisitedPageDetails = function(){
            return localStorageService.getItem(LAST_VISITED_PAGE_DETAILS);
        }
        utilfunc.updatePushNotificationTags = function(type){
            pushNotificationService.sendNotificationTags(type);
        }
        utilfunc.selectCoachMarkVisibility = function (couponData,timeCheck) {
            let coachMarkFlag = false,
                coachMarkKey = sharedConfig.coachMarkSelector.knowMoreKey;
            if (couponData && utilfunc.isStorageExceedsTime(couponData,timeCheck)) {
                if (!localStorageService.getItem('SELECT_COACH_UNLOCK')) {
                    localStorageService.setItem('SELECT_COACH_UNLOCK', 1);
                    coachMarkFlag = true;
                }
                coachMarkKey = sharedConfig.coachMarkSelector.unlockKey;
            } else {
                if (!localStorageService.getItem('SELECT_COACH_KNOW_MORE')) {
                    localStorageService.setItem('SELECT_COACH_KNOW_MORE', 1);
                    coachMarkFlag = true;
                }
            }
            return {
                coachMarkFlag,
                coachMarkKey
            }
        }
        utilfunc.getImageClassForAR = function(imageWidth, imageHeight, boxWidth, boxHeight) {
            if(imageWidth && typeof imageWidth === "number" && 
                imageHeight && typeof imageHeight === "number") {
                let imageAspectRatio = imageWidth/imageHeight;
                let boxAspectRatio = boxWidth/boxHeight;

                let aspectRatioDiffPercentage = imageAspectRatio*100/boxAspectRatio;

                if(imageAspectRatio > boxAspectRatio || aspectRatioDiffPercentage >= IMAGE_AR_ALLOWED_DIFF) {
                    return "js-zoom-crop";
                } else if (aspectRatioDiffPercentage <= IMAGE_AR_ALLOWED_DIFF && aspectRatioDiffPercentage >= IMAGE_AR_MIN_DIFF) {
                    return "js-zoom-crop-allowed";
                }else if (aspectRatioDiffPercentage <= IMAGE_AR_MIN_DIFF) {
                    return "js-zoom-crop-min";
                }
                return "";   
            }
            return "js-zoom-crop";
        }
        utilfunc.arraysIntersect = function(source, destination) {
            return source && source.length &&
            destination && destination.length &&
            source.filter((s) => {
                return destination.indexOf(s) > -1;
            }).length > 0;
        }
        utilfunc.isStorageExceedsTime = function(cookieData,timeCheck=0){
            let now = new Date().getTime();
            return cookieData.createdAt ?(now - cookieData.createdAt >= timeCheck):false;
        }
        utilfunc.capitalize = function (str) {
            return (str.charAt(0).toUpperCase() + str.slice(1));
        }

        utilfunc.executeHotjarTrigger = function(triggerName) {
            window.hj=window.hj||function(){(window.hj.q=window.hj.q||[]).push(arguments);};

            window.hj('trigger', triggerName);
        }
        return utilfunc;
    });
    return Box.Application.getService(SERVICE_NAME);
});
