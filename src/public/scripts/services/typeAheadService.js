'use strict';
define(['common/sharedConfig',
    'services/apiService',
    'services/loggerService',
    'services/utils'

], function(sharedConfig, apiService, logger, utilService) {
    const SERVICE_NAME = 'TypeAheadService';

    Box.Application.addService(SERVICE_NAME, function() {


        var previousAPICall;

        function fetchSearchResults(data) {
            let defaultAPIParams = {
                    category: utilService.getPageData().listingType || 'buy',
                    view: 'buyer'
                };
            if (data && data.searchParams && data.searchParams.category && data.searchParams.view && data.searchParams.query) {

                let searchParams = utilService.deepCopy(data.searchParams,defaultAPIParams);

                let queryString = _createQueryParam(searchParams);
                logger.info('queryString', queryString);
                if (previousAPICall) {
                    previousAPICall.abort();
                    previousAPICall = null;
                }
                previousAPICall = apiService.get(sharedConfig.clientApis.searchTypeahead(searchParams).url);
                return previousAPICall;
            } else {
                return new Promise(function(resolve, reject) {
                    reject(new Error('incomplete parametes for search'));
                });
            }
        }

        function fetchParsedSearchResults(data) {
            return fetchSearchResults(data).then((response) => {
                return _parseSearchResult(response.data, data.config);
            });
        }
        function fetchReverseSearch(query,listingType) {
            let searchParams = {
                    category: listingType,
                    view: 'buyer',
                    rows: 1,
                    query: query,
                    usercity:"Gurgaon"
                };
            let queryString = _createQueryParam(searchParams);
            return apiService.get(sharedConfig.clientApis.searchTypeahead(searchParams).url)
        }
        function _createQueryParam(searchParam) {
            let keys = Object.keys(searchParam),
                queryString = '?';
            for (let i = 0, length = keys.length; i < length; i++) {
                let val = searchParam[keys[i]];
                if (val) {
                    queryString += keys[i] + '=' + val + '&';
                }
            }
            queryString.slice(0, -1);
            return queryString;
        }

        function _parseSearchResult(response, config) {
            let match = [],
                retSuggestion = [],
                nearBy = [];
            for (let count = 0, length = response.length; count < length; count++) {
                var __typeahead = response[count];
                if (config.includeGoogplePlace && __typeahead.isGooglePlace) {
                    nearBy.push(__typeahead);
                } else if (config.includeSuggestion && __typeahead.isSuggestion) {
                    retSuggestion.push(__typeahead);
                } else {
                    if (config.typeAheadType) {
                        let typeaheadArray = config.typeAheadType.split(',');
                        //if (__typeahead.type.toLowerCase() === config.typeAheadType.toLowerCase()) {
                        if(typeaheadArray.indexOf(__typeahead.type.toLowerCase())>-1){
                            match.push(__typeahead);
                        }
                    } else {
                        match.push(__typeahead);
                    }
                }
            }

            return {
                match: match,
                suggestion: retSuggestion,
                nearBy: nearBy
            };
        }
        return {
            fetchSearchResults,
            fetchParsedSearchResults,
            fetchReverseSearch
        };
    });

    return Box.Application.getService(SERVICE_NAME);
});
