define([], () => {
    'use strict';
    const SERVICE_NAME = 'facebookPixelTrackingService';

    Box.Application.addService(SERVICE_NAME, () => {
        if (document.readyState.toLowerCase() === 'complete') {
            loadFBPixel(window, document, 'script', 'https://connect.facebook.net/en_US/fbevents.js');
        } else {
            $(window).load(function(){
                loadFBPixel(window, document, 'script', 'https://connect.facebook.net/en_US/fbevents.js');
            });
        }

        function loadFBPixel(f, b, e, v, n, t, s){
            getPixelScript(f, b, e, v, n, t, s);
            window.fbq("init", window.FACEBOOK_PIXEL_TRACKING_ID);
        }

        function getPixelScript(f, b, e, v, n, t, s) {
            if (f.fbq) {
                return;
            }
            n = f.fbq = function() { // jshint ignore:line
              n.callMethod ?
                  n.callMethod.apply(n, arguments) : n.queue.push(arguments) // jshint ignore:line
            };
            if (!f._fbq){
                f._fbq = n;
            }
            n.push = n;
            n.loaded = !0;
            n.version = '2.0';
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s);
        }

        function trackEvent(event, properties) {
            if(typeof window.fbq == "function"){
                window.fbq("trackCustom", event, properties);
            }
        }

        function trackPageView(pageType, onmitureData) {
            let data = $.extend(true,{
                    url: document.location.href,
                    path: document.location.pathname
                },onmitureData);
            trackEvent(pageType, data);
        }

        return {
            trackEvent,
            trackPageView
        };
    });

    return Box.Application.getService(SERVICE_NAME);
});
