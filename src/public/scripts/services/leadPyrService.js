'use strict';
define(['common/sharedConfig',
    'services/utils',
    'services/localStorageService',
    'services/loginService',
    'services/apiService',
    'services/feedbackService',
    'services/commonService',
    'services/urlService',
    'services/trackingService',
    'services/filterConfigService',
    'common/trackingConfigService',
    'services/loggerService'
], function(SharedConfig, Utils, LocalStorageService, LoginService, ApiService, feedbackService, commonService, urlService, trackingService, filterConfigService, t, Logger) {
    const SERVICE_NAME = 'LeadPyrService';
    Box.Application.addService('LeadPyrService', function(Application) {
        const config = {
            otpValidationTime: 30 * 60 * 1000,
            defaultValues: {
                optInCheckedStatus: true
            },
            optInPersistanceInterval: 30 * 24 * 60 * 60 * 1000, // 1 MONTH
            NINETY_DAYS_MILLISEC : 7776000000,
            callIdsStorage: "call_ids",
            contactedStorage: "contacted",
            enquiryStorage: "enquiry_info",
            contactNumberStorage: "contact_numbers",
            ratedSellerStorage: "rated_sellers",
            buyerUserIdStorage : 'mkn_lead_userid',
            HOMELOAN_LQLEAD_STORAGE:'Homeloan_LqLead_Time',
            cookiesPriorityForUserData: ["enquiry", "user", "optIn"],
            cookies: {
                enquiry: "enquiry_info",
                user: "user_info",
                optIn: "opt_in"
            },
            dummy: {
                name: "name",
                email: "email"
            },
            indiaCountryCode: 1,
            cookieKeys: ["name", "email", "phone", "cityId", "countryId", "localityIds", "bhk"],
        };

        function getValidationStrategyConfig() {
            return {
                "EMAIL": {
                    "INVALID": {
                        "MESSAGE": "email is invalid"
                    },
                    "EMPTY": {
                        "MESSAGE": "email is required"
                    }
                },
                "PHONE": {
                    "INVALID": {
                        "MESSAGE": "phone number is invalid"
                    },
                    "EMPTY": {
                        "MESSAGE": "phone number is required"
                    }
                },
                "NAME": {
                    "INVALID": {
                        "MESSAGE": "name is invalid"
                    },
                    "EMPTY": {
                        "MESSAGE": "name is required"
                    }
                },
                "OTP": {
                    "INVALID": {
                        "MESSAGE": "otp is invalid"
                    },
                    "EMPTY": {
                        "MESSAGE": "otp is required"
                    }
                },
                "CHAT_REASON": {
                    "EMPTY": {
                        "MESSAGE": "please choose a reason"
                    }
                },
                "REASON": {
                    "EMPTY": {
                        "MESSAGE": "please choose a reason"
                    }
                },
                "RHS_MAIN_REASON": {
                    "EMPTY": {
                        "MESSAGE": "please choose a reason"
                    }
                },
                "GLOBAL": {
                    "GENERIC": {
                        "MESSAGE": "some error occured"
                    },
                    "MINIMUM_LISTING": {
                        "MESSAGE": "select atleast 1"
                    },
                    "SUCCESS_OTP": {
                        "MESSAGE": "we have sent an otp"
                    },
                    "SUCCESS": {
                        "MESSAGE": "Successfully Saved"
                    },
                    "REASON": {
                        "MESSAGE": "please provide reason"
                    },
                    "MINIMUM_SELLER": {
                        "MESSAGE": "select atleast 1 seller"
                    }
                },
                defaultHideTime: 5000
            };
        }

        function error(exception) {
            Logger.error('error', {
                exception: exception
            });
        }

        // @toDo : remove it
        function _setUserNumberStatus(currentData) {
            const sessionItemName = config.contactNumberStorage;
            var existingData = LocalStorageService.getSessionItem(sessionItemName);
            if (existingData && existingData.length) {
                try {
                    existingData = JSON.parse(existingData);
                } catch (e) {}
            }
            if (existingData) {
                currentData = $.extend(existingData, currentData);
            }
            LocalStorageService.setSessionItem(sessionItemName, currentData);
        }
        // @toDo : remove it
        function setNumber(number, merge = true) {
            let cookie = {},
                allNumbers = [],
                matched = false;
            if (!$.isArray(number) && $.isPlainObject(number)) {
                number = [number];
            } else {
                number = [];
            }
            if (!merge) {
                cookie.contactNumbers = number;
            } else {
                allNumbers = $.extend(true, allNumbers, getAllNumbers());
                $.each(number, (key, val) => {
                    $.each(allNumbers, (k, v) => {
                        if (v.contactNumber === val.contactNumber) {
                            $.extend(true, v, val);
                            matched = true;
                        }
                    });
                    if (!matched) {
                        allNumbers.push(val);
                    }
                });
                cookie.contactNumbers = allNumbers;
            }
            _setUserNumberStatus(cookie);
        }

        // @toDo : remove it
        function getAllNumbers() {
            let data = LocalStorageService.getSessionItem(config.contactNumberStorage) || {};
            return data.contactNumbers || [];
        }

        function checkOTPVerifiedFromLocalStorage(phone, isVerifiedParams) {
            var isVerified = false;
            if(!isVerifiedParams){
                let sessionId = feedbackService.getCurrentSession(),
                contactedStorage = LocalStorageService.getItem(config.contactNumberStorage) || {};
                contactedStorage[sessionId] = contactedStorage[sessionId] || [];
                isVerified = contactedStorage[sessionId].indexOf(phone.toString()) > -1;
            } else {
                let dpsessionData = LocalStorageService.getSessionItem("leadDrip") || {};
                isVerified = (phone == dpsessionData.phone) ? true : isVerified; 
            }
            return isVerified;
        }

        function checkOTPVerified(phone = "", isVerifiedParams) {
            let userDetails = LoginService.isUserLoggedIn();
            let isVerified = false;
            let isLoggedIn = false;
            return userDetails.then((response) => {
                let contactNumbers = (response && response.data && response.data.contactNumbers) || [];
                updateContactNumbers({
                    contactNumber: contactNumbers,
                    isVerified: true
                });
                $.each(contactNumbers, (key, val) => {
                    if (val.contactNumber === phone) {
                        isVerified = val.isVerified;
                    }
                });
                isLoggedIn = true;
                let buyerUserId = response && response.data && response.data.id;
                return { isVerified: isVerified || checkOTPVerifiedFromLocalStorage(phone, isVerifiedParams), 
                    isLoggedIn , buyerUserId };
            }, () => {
                let buyerUserId = LocalStorageService.getItem('mkn_lead_userid')||'';
                return { isVerified: checkOTPVerifiedFromLocalStorage(phone, isVerifiedParams), 
                    isLoggedIn , buyerUserId };
            });
        }

        function getEnquiryCookie() {
            let cookieData = LocalStorageService.getItem(config.enquiryStorage);
            if (cookieData && cookieData.length) {
                try {
                    cookieData = JSON.parse(cookieData);
                } catch (e) {}
            }
            return cookieData;
        }

        function pruneDummyNameAndEmailFields(data = {}) {
            let fields = ["name", "email"];
            fields.forEach((k) => {
                if (data[k] && (data[k].indexOf("name_") > -1 || data[k].indexOf("email_") > -1)) {
                    delete data[k];
                }
            });
        }

        function setEnquiryCookie(obj = {}) {
            var currentCookies = {};
            $.each(config.cookieKeys, (k, v) => {
                if (obj[v]) {
                    currentCookies[v] = obj[v];
                }
                if (v === "localityIds" && (($.isArray(obj[v]) && !$.isPlainObject(obj[v][0])) || (["string", "number"].indexOf(typeof obj[v]) > -1))) {
                    delete currentCookies[v];
                }
            });
            pruneDummyNameAndEmailFields(currentCookies);
            let COOKIE_NAME = config.enquiryStorage;
            var existingCookie = getEnquiryCookie(COOKIE_NAME);
            if (existingCookie && existingCookie.length) {
                try {
                    existingCookie = JSON.parse(existingCookie);
                } catch (e) {}
            }
            if (existingCookie) {
                currentCookies = $.extend(existingCookie, currentCookies);
            }
            LocalStorageService.setItem(COOKIE_NAME, JSON.stringify(currentCookies));
        }

        function stripQuotes(str) {
            if (!String.prototype.startsWith) {
                String.prototype.startsWith = function(searchString, position) { //jshint ignore:line
                    position = position || 0;
                    return this.indexOf(searchString, position) === position;
                };
            }
            if (!String.prototype.endsWith) {
                String.prototype.endsWith = function(suffix) { //jshint ignore:line
                    return this.indexOf(suffix, this.length - suffix.length) !== -1;
                };
            }

            if (!str) {
                return str;
            }
            if (str.startsWith("\"")) {
                str = str.substring(1, str.length);
            }
            if (str.endsWith("\"")) {
                str = str.substring(0, str.length - 1);
            }
            return str;
        }

        function prune(obj, val) {
            let temp = {};
            if (!val) {
                for (let i in obj) {
                    if (obj[i] === null || obj[i] === undefined) {
                        delete obj[i];
                    }
                }
            } else if ($.type(val) === "string") {
                let temp = {};
                temp[val] = obj[val];
                delete obj[val];
            } else if ($.isArray(val)) {
                $.each(val, (k, v) => {
                    temp[v] = obj[v];
                    delete obj[v];
                });
            } else if ($.isPlainObject(val)) {
                $.each(val, (k) => {
                    temp[k] = obj[k];
                    delete obj[k];
                });
            }
            return temp;
        }

        function updateContactNumbers(details) {
            if (details.isVerified) {
                let sessionId = feedbackService.getCurrentSession(),
                    contactedStorage = LocalStorageService.getItem(config.contactNumberStorage) || {},
                    number;
                contactedStorage[sessionId] = contactedStorage[sessionId] || [];
                if ($.isArray(details.contactNumber)) {
                    number = details.contactNumber;
                } else {
                    number = [details.contactNumber];
                }
                number = Utils.unique(contactedStorage[sessionId].concat(number));
                contactedStorage[sessionId] = number;
                LocalStorageService.setItem(config.contactNumberStorage, contactedStorage);
            }
        }
        // @toDo : remove it
        function clearAllNumbers() {
            setNumber([], false);
        }

        function setAlreadyContact(listingId) {
            let allContactedListings = LocalStorageService.getSessionItem(config.contactedStorage) || {};
            if (allContactedListings.ids && $.isArray(allContactedListings.ids)) {
                allContactedListings.ids.push(listingId);
            } else {
                allContactedListings.ids = [listingId];
            }
            LocalStorageService.setSessionItem(config.contactedStorage, allContactedListings);
        }

        function sendOTPOnCall({ userId, userNumber, element }) {

            if (element && !element.hasClass('link-disabled')) {
                let innnerHTML = element.text();
                element.addClass('link-disabled');
                element.text('calling you...');
                setTimeout(function() {
                    element.text(innnerHTML);
                    element.removeClass('link-disabled');
                }, 20000);
            } else {
                return;
            }

            let otpOnCallUrl = SharedConfig.apiHandlers.otpOnCall({
                userId,
                userNumber
            }).url;

            return ApiService.get(otpOnCallUrl);
        }

        function focusInput(element) {
            if (!element) {
                return; }

            $(element).val('').focus();
        }

        function emptyFunction(data) {
            return data;
        }

        function updateStepData(required, fields, moduleData) {
            let temp = {};
            if (!fields) {
                return temp; }
            $.each(fields, (key, val) => {
                if (moduleData[key] && $.isArray(moduleData[key])) {
                    temp[key] = $.extend(true, temp[key], moduleData[key]);
                    temp[key] = $.map(temp[key], (val) => {
                        return [val];
                    });
                } else if (moduleData[key] && $.isPlainObject(moduleData[key])) {
                    temp[key] = $.extend(true, temp[key], moduleData[key]);
                } else if (moduleData[key] && $.type(moduleData[key]) === 'boolean') {
                    temp[key] = moduleData[key];
                } else if (moduleData[key]) {
                    temp[key] = moduleData[key];
                } else if (required) {
                    error(new Error("Missing Parameter " + key + " in Lead Module"));
                } else if (val) {
                    temp[key] = val;
                }
            });
            return temp;
        }


        function _updateDisplayData(config, processedData) {
            let displayData = prune(processedData, config.displayFields);
            prune(displayData);
            return displayData;
        }

        function _getStep(step, config = {}) {
            step = step && step.toUpperCase();
            config.steps = config.steps || [];
            return step && config.steps.indexOf(step) !== -1 ? step : config.defautStep;
        }

        //ICRM-5791 ( This function will create a array of all the sellers of the project or property )
        function setMultipleSellerIDs(data) {
            let multipleSellerIds = [],
                displayData = data.displayData,
                postData = data.postData;
            if(displayData && displayData.otherCompany && displayData.otherCompany.length) {
                multipleSellerIds = displayData.otherCompany.map(element => element.companyUserId);
            }
            if(displayData && displayData.companyUserId && postData && postData.multipleCompanyIds && postData.multipleCompanyIds.length != multipleSellerIds.length ) {
                multipleSellerIds.push(displayData.companyUserId);
            }
            return multipleSellerIds;
        }

        function getOtherCompanyIds(data) {
            let companyIds = [];
            if (data.otherCompany) {
                $.each(data.otherCompany, (k, v) => {
                    companyIds.push(v.companyId || v.id);
                });
            }
            return companyIds;
        }

        function processStepData({ rawData, step, stepsConfig, config, updateDisplayData, updateOtherParameters, updateApiKeys, updateManualFields, getDefaultResponseForProcessedData }) {
            step = step || rawData.step || "MAIN";
            config = config || {};
            step = rawData.step = _getStep(step, config);
            updateDisplayData = updateDisplayData || _updateDisplayData;
            updateApiKeys = updateApiKeys || emptyFunction;
            updateManualFields = updateManualFields || emptyFunction;
            updateOtherParameters = updateOtherParameters || emptyFunction;
            getDefaultResponseForProcessedData = getDefaultResponseForProcessedData || emptyFunction;
            let processedData = {},
                displayData,
                moduleParameters,
                trackData = {},
                stepConfig = stepsConfig[step] || {},
                stepsSkipped;

            processedData = $.extend(true, processedData, updateStepData(true, stepConfig.required, rawData));
            processedData = $.extend(true, processedData, updateStepData(false, stepConfig.optional, rawData));
            processedData = $.extend(true, processedData, stepConfig.defaultValue);
            trackData = $.extend(true, trackData, updateStepData(false, stepConfig.trackFields, rawData));
            displayData = updateDisplayData(stepConfig, processedData);
            moduleParameters = getModuleParameters(rawData, config);
            updateOtherParameters(stepConfig, processedData, rawData);
            stepsSkipped = $.extend(true, [], rawData.stepsSkipped);
            updateApiKeys(processedData, config.apiKeyMap);
            ({ processedData, displayData, rawData } = updateManualFields({ processedData, displayData, rawData }, stepConfig.manualFields));
            prune(processedData);
            prune(trackData);

            return {
                postData: processedData,
                displayData: displayData,
                trackData: trackData,
                step: step,
                response: getDefaultResponseForProcessedData(),
                moduleParameters: moduleParameters,
                stepsSkipped: stepsSkipped
            };
        }

        function getUserDataFromCookies() {
            let cookiesData = {};
            $.each(config.cookiesPriorityForUserData, (key, val) => {
                let cookie = config.cookies[val],
                    data = Utils.getCookie(cookie);
                if (data || cookie == config.cookies.enquiry) {
                    try {
                        if (cookie == config.cookies.enquiry) {
                            // data = JSON.parse(JSON.parse(data));
                            data = {};
                            data = $.extend(true, data, getEnquiryCookie());
                        } else {
                            data = JSON.parse(data);
                        }
                        cookiesData = $.extend(true, cookiesData, data);
                    } catch (e) {}
                }
            });
            return cookiesData;
        }

        function getModuleParameters(rawData, config) {
            let modulesParameters = {};
            modulesParameters = $.extend(true, modulesParameters, updateStepData(false, config.modulesParameters, rawData));
            prune(rawData, config.modulesParameters);
            return modulesParameters;
        }

        function _createDummyData(field, phone) {
            return field && phone ? config.dummy[field] + '_' + phone : 'dummy';
        }

        function updateDummyFields(data, field) {
            if (!$.isArray(field)) {
                field = [field];
            }
            $.each(field, (k, v) => {
                switch (v) {
                    case "email":
                        data[v] = data[v] || _createDummyData(v, data['phone']) + '@email.com';
                        break;
                    case "name":
                        data[v] = data[v] || _createDummyData(v, data['phone']);
                        break;
                }
            });
        }

        function postLead(api, data) {
            let failedEnquiryApi = SharedConfig.apiHandlers.failedEnquiryPush().url;
            updateDummyFields(data, ['email', 'name']);
            setEnquiryCookie(data);
            return ApiService.postJSON(api, data).then((response) => {
                if(response.userId){
                    LocalStorageService.setItem(SharedConfig.leadClientIdCookieKey, response.userId); //storing user id in localStorage
                    //register user on push notification service
                    Application.broadcast('Lead_User_Created', {userId: response.userId});
                }
                feedbackService.addEnquiryDropped(response.id, data.listingId);
                return response || {};
            }, () => {
                ApiService.postJSON(failedEnquiryApi, JSON.stringify({
                    payload: data,
                    api
                }));
            });
        }
        function postUserDetails(api, data) {
            return ApiService.postJSON(api, data).then((response) => {
                return response || {};
            });
        }

        function verifyOTP(api, data) {
            let putData = {
                userEnquiryId: data.userEnquiryId,
                otp: data.otp,
                phone: data.phone,
                email: data.email,
                name: data.name,
                userId: data.userId
            };
            putData = JSON.stringify(putData);
            return ApiService.putJSON(api, putData).then(function(response) {
                return response && response.data;
            });
        }
        function verifyTempEnquiry(api, {tempEnquiryId}) {
            let putData = {
                tempEnquiryId,
                processingStatus:'processed'
            };
            putData = JSON.stringify(putData);
            return ApiService.putJSON(api, putData).then(function(response) {
                return response && response.data;
            });
        }

        function getTopSellers(data, count = undefined) {
            var cityId = data.cityId,
                query = {},
                utmSource = stripQuotes(urlService.getUrlParam('utm_source')),
                utmMedium = stripQuotes(urlService.getUrlParam('utm_medium'));

            if (utmSource) {
                query.utmSource = utmSource;
            }
            if (utmMedium) {
                query.utmMedium = utmMedium;
            }
            if (data.phone) {
                query.contactNumber = data.phone;
            }
            if (data.giveBoostToExpertDealMakers) {
                query.giveBoostToExpertDealMakers = data.giveBoostToExpertDealMakers;
            }
            if (data.bhk) {
                let index = data.bhk.indexOf("3plus");
                let _bhk = $.extend(true, [], data.bhk);
                if (index >= 0) {
                    _bhk.splice(index, 1);
                    Array.prototype.push.apply(_bhk, ["4", "5", "6", "7", "8", "9", "10"]);
                }
                _bhk = _bhk.filter(n => n);
                query.bhk = _bhk.join(",");
            }
            if (data.localityIds && data.localityIds.length) {
                query.localityIds = data.localityIds.join(',');
            }
            if (data.suburbIds && data.suburbIds.length) {
                query.suburbIds = data.suburbIds.join(',');
            }

            if (data.localityOrSuburbIds && data.localityOrSuburbIds.length) {
                query.localityOrSuburbIds = data.localityOrSuburbIds.join(',');
            }

            if(data.projectId && data.projectId.length){
                query.projectId = data.projectId.map(function(projectId) {
                    return projectId; }).join(",");
            }

            if (data.propertyTypes && data.propertyTypes.length) {
                query.unitType = data.propertyTypes.join(",");
            }
            if (data.salesType) {
                query.listingCategory = "Primary,Resale";
                if (data.salesType.toLowerCase() === "rent") {
                    query.listingCategory = "Rental";
                } else if(data.salesType.toLowerCase() === "all" ){
                    query.listingCategory = "All";
                }
            }
            if (data.minBudget && data.maxBudget) {
                query.budget = data.minBudget + "," + data.maxBudget;
            }
            if (count) {
                query.rows = count;
            }
            if (data.sellerType) {
                query.sellerType = data.sellerType;
            }

            if(data.expertFilter){
                query.expertFilter = data.expertFilter;
            }

            if(data.metaInfo){
                query.metaInfo = data.metaInfo;
            }

            if(data.hideSpecificExperts){
                query.hideSpecificExperts = data.hideSpecificExperts;
            }

            if(data.ignoreExpert){
                query.ignoreExpert = data.ignoreExpert;
            }

            return commonService.bindOnLoadPromise().then(() => {
                if (cityId) {
                    return _getTopSellers(cityId, query);
                } else {
                    return [];
                }
            });
        }

        function _getTopSellers(cityId, query){
            let url = SharedConfig.apiHandlers.getCityTopSellers({ cityId, query }).url;
            return ApiService.get(url).then(function(response) {
                if (response) {
                    return response;
                }
                return [];
            });
        }

        function isIndian(countryId) {
            return countryId == config.indiaCountryCode;
        }

        function getPropertyIdFromLabel(name, object) {
            let res = object.filter((obj) => {
                if (obj.value == name) {
                    return true;
                }
            });
            return (res[0] || {}).id;
        }

        function updateOptInStatus(status = true) {
            let event = t["CLICK_EVENT"];
            let properties = {};
            properties[t.CATEGORY_KEY] = t["BUYER_OPT_IN"];
            properties[t.LABEL_KEY] = status ? t["SMART_MULTIPLICATION_LABELS"]["ON"] : t["SMART_MULTIPLICATION_LABELS"]["OFF"];
            trackingService.trackEvent(event, properties);
            let value = JSON.stringify({
                optInCheckedStatus: status
            });
            let expiry = new Date(Date.now() + config.optInPersistanceInterval);
            let name = config.cookies.optIn;
            Utils.setCookie(name, value, {
                Expires: expiry,
                "Path": "/"
            });
        }

        function getOptInCheckedStatus(cookiesData) {
            let key = "optInCheckedStatus";
            cookiesData = cookiesData || getUserDataFromCookies();
            return (typeof cookiesData[key] == "boolean" ? cookiesData[key] : config.defaultValues[key]);
        }
        function getLeadFormRawData(step){
            let selectFilterkeyValObj = filterConfigService.getSelectedQueryFilterKeyValObj(),
                rawData = {},
                listingType = Utils.getPageData('listingType');

            rawData = {
                cityId: Utils.getPageData('cityId'),
                localityId: Utils.getPageData('localityId'),
                projectId: Utils.getPageData('projectId'),
                salesType: listingType,
                cityName: Utils.getPageData('cityName'),
                type: 'openLeadForm',
                smartMultiplication: false,
                isCommercial: Utils.getPageData('isCommercial')
            };
            if (selectFilterkeyValObj.beds) {
                rawData.bhk = selectFilterkeyValObj.beds.split(",");
            }
            if (selectFilterkeyValObj.budget) {
                let _budget = selectFilterkeyValObj.budget.split(",");
                rawData.minBudget = _budget[0];
                rawData.maxBudget = _budget[1];
            }
            if (selectFilterkeyValObj.propertyType) {
                let propertyTypeConfig, propertyTypeIds = [], propertyTypes;

                propertyTypeConfig = filterConfigService.getFilterModuleConfig("propertyType").getTemplateData({
                    listingType: ["rent", "rental"].indexOf(listingType) > -1?'rent':'buy'
                });
                propertyTypes = selectFilterkeyValObj.propertyType.split(",");
                propertyTypes.forEach((elem) => {
                    let _id = getPropertyIdFromLabel(elem, propertyTypeConfig);
                    if (_id) {
                        propertyTypeIds.push(_id);
                    }
                });
                if (propertyTypeIds.length) {
                    rawData.propertyTypes = propertyTypeIds;
                }
            }
            if(step){
                rawData.step = step;
            }
            return rawData;
        }
        function setHomeLoanLQDropTime(){
            let cookieData = {
                createdAt : new Date().getTime()
            };
            LocalStorageService.setItem(config.HOMELOAN_LQLEAD_STORAGE,cookieData);
        }
        function showHomeLoanCheckBox(){
            var homeLoanLeadCookieData = LocalStorageService.getItem(config.HOMELOAN_LQLEAD_STORAGE);
            if(homeLoanLeadCookieData){
                return Utils.isStorageExceedsTime(homeLoanLeadCookieData,config.NINETY_DAYS_MILLISEC);
            }else{
                return true;
            } 
        }
        function postHomeLoanLqLead(homeloanData){
            let homeLoanLeadData ={
                 email: homeloanData.summary_email,
                 phone: homeloanData.summary_mobile,
                 countryId: homeloanData.summary_countryId,
                 name: homeloanData.summary_name,
                 salesType: 'homeloan',
                 cityId: homeloanData.project_cityId,
                 cityName: homeloanData.project_cityName,
                 applicationType: Utils.isMobileRequest ? 'Mobile Site' : 'Desktop Site',
                 jsonDump: window.navigator.userAgent,
                 pageName: 'homeloan',
                 pageUrl: window.location.href.split('#')[0],
                 domainId: 1,
                 sendOtp: false,
                 enquiryType: { id: 11 },
                 multipleCompanyIds: [499], // Proptiger
                 propertyPrice:homeloanData.price_propertyPrice
             };
            return ApiService.postJSON(SharedConfig.apiHandlers.postEnquiry().url, homeLoanLeadData);
        }
        return {
            getEnquiryCookie,
            setEnquiryCookie,
            checkOTPVerified,
            getAllNumbers,
            setNumber,
            stripQuotes,
            prune,
            updateContactNumbers,
            clearAllNumbers,
            setAlreadyContact,
            sendOTPOnCall,
            focusInput,
            getModuleParameters,
            getValidationStrategyConfig,
            processStepData,
            setMultipleSellerIDs,
            postLead,
            postUserDetails,
            verifyOTP,
            getTopSellers,
            updateDummyFields,
            getUserDataFromCookies,
            getOtherCompanyIds,
            updateStepData,
            isIndian,
            pruneDummyNameAndEmailFields,
            getPropertyIdFromLabel,
            updateOptInStatus,
            getOptInCheckedStatus,
            getLeadFormRawData,
            setHomeLoanLQDropTime,
            showHomeLoanCheckBox,
            postHomeLoanLqLead,
            verifyTempEnquiry
        };
    });
    return Box.Application.getService(SERVICE_NAME);
});
