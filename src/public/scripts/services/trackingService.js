define([
    'services/utils',
    'services/defaultService',
    'services/gaTrackingService',
    'services/localStorageService',
    'common/trackingConfigService',
    'services/filterConfigService',
    /* 'services/amplitudeTrackingService', */
    'services/irisTrackingService',
    'services/facebookPixelTrackingService'
], function(utils, defaultService, gaTrackingService, localStorageService, t, filterConfigService, /* amplitudeTrackingService, */ irisTrackingService , facebookPixelTrackingService) {
    'use strict';

    const SERVICE_NAME = 'TrackingService';

    Box.Application.addService(SERVICE_NAME, function(application) {
        function trackEvent(eventName, data) {
            if(t.BLOCKED_EVENTS.indexOf(eventName) >= 0 || t.BLOCKED_EVENTS.indexOf(data[t.LABEL_KEY]) >= 0 ){
                return;
            }
            if(!data[t.NON_INTERACTION_KEY]){
                application.broadcast("interactive-event");
            }
            let pageData = utils.getPageData();
            data = data || {};
            data[t.URL_KEY] = data[t.URL_KEY] || document.location.href;
            data[t.PAGE_KEY] = data[t.PAGE_KEY] || t.PAGE_TYPE_MAP[pageData.pageType] || t.DEFAULT_PAGE_TYPE;
            data[t.OMNITUTRE_PAGE_NAME_KEY] = utils.getOmniturePageName();
            data[t.PROJECT_STATUS_KEY] = data[t.PROJECT_STATUS_KEY] || t.makeValuesUniform(pageData.projectStatus,t.PROJECT_STATUS_NAME) || pageData.projectStatus;
            data[t.UNIT_TYPE_KEY] = data[t.UNIT_TYPE_KEY] || t.makeValuesUniform(pageData.unitType,t.UNIT_TYPE_MAP) || pageData.unitType;
            data[t.BUDGET_KEY] = data[t.BUDGET_KEY] || pageData.budget;
            data[t.BHK_KEY] = data[t.BHK_KEY] || t.makeValuesUniform(pageData.bhk,t.BHK_MAP) ;
            data[t.SELLER_ID_KEY] = data[t.SELLER_ID_KEY] || (utils.isArray(pageData.sellerId) ? pageData.sellerId.join() : pageData.sellerId);
            data[t.SELLER_SCORE_KEY] = data[t.SELLER_SCORE_KEY] || pageData.sellerScore;
            data[t.LISTING_ID_KEY] = data[t.LISTING_ID_KEY] || pageData.listingId;
            data[t.LISTING_SCORE_KEY] = data[t.LISTING_SCORE_KEY] || pageData.listingScore;
            data[t.LANDMARK_ID_KEY] = data[t.LANDMARK_ID_KEY] || pageData.placeId;
            data[t.PROJECT_ID_KEY] = data[t.PROJECT_ID_KEY] || pageData.projectId;
            data[t.PROJECT_NAME_KEY] = pageData.projectName;
            data[t.BUILDER_ID_KEY] = data[t.BUILDER_ID_KEY] || pageData.builderId;
            data[t.LOCALITY_ID_KEY] = data[t.LOCALITY_ID_KEY] || pageData.localityId;
            data[t.SUBURB_ID_KEY] = data[t.SUBURB_ID_KEY] || pageData.suburbId;
            data[t.CITY_ID_KEY] = data[t.CITY_ID_KEY] || pageData.cityId;
            data[t.NON_INTERACTION_KEY] = data[t.NON_INTERACTION_KEY] || 0;

            if (!data[t.LISTING_CATEGORY_KEY]) {
                data[t.LISTING_CATEGORY_KEY] = t.LISTING_CATEGORY_MAP[pageData.listingType.toUpperCase()];
                if (pageData.listingCategory) {
                    data[t.LISTING_CATEGORY_KEY] = t.LISTING_CATEGORY_MAP[pageData.listingCategory.toUpperCase()];
                }
            }

            data[t.FILTER_USED] = _getFilterUsedData(data.href);
            //done to keep privacy of users
            data[t.NAME_FLAG_KEY] = data[t.NAME_KEY] ? 'Y' : 'N';

            if(isNaN(data[t.EMAIL_KEY])){
                data[t.EMAIL_FLAG_KEY] = data[t.EMAIL_KEY] ? 'Y' : 'N';
            }else {
                data[t.EMAIL_FLAG_KEY] = data[t.EMAIL_KEY];
            }


            data['Email'] = "";
            data['Phone'] = "";
            defaultService.getCityByLocation().then(function(city) {
                if (city) {
                    data[t.VISITOR_CITY_KEY] = city.label || city.userCity;
                }
                //amplitudeTrackingService.trackEvent(eventName, data);
                irisTrackingService.trackEvent(eventName, data);
                gaTrackingService.trackEvent(eventName, data);
                facebookPixelTrackingService.trackEvent(eventName, data);
            }, function() {
                //amplitudeTrackingService.trackEvent(eventName, data);
                irisTrackingService.trackEvent(eventName, data);
                gaTrackingService.trackEvent(eventName, data);
                facebookPixelTrackingService.trackEvent(eventName, data);
            });
        }
        function _getFilterUsedData(href){
            let filterOrderKeys = ["budget","beds","baths","furnished","propertyType","area","newOrResale","ageOfProperty","furnished","possession","postedBy","sortBy"],
                filterOrderKeysShortHandMap = {
                    "budget":"price",
                    "beds": "bed",
                    "baths":"bath",
                    "furnished":"furnishing",
                    "propertyType":"propertyType",
                    "area":"size",
                    "newOrResale":"newOrResale",
                    "ageOfProperty":"age",
                    "possession":"pos",
                    "postedBy":"postedBy",
                    "sortBy":"sortBy"
                };
            let _fData = filterConfigService.getSelectedQueryFilterKeyValObj(href),
                temp = [];
            for(var i in filterOrderKeys) {
                if(_fData[filterOrderKeys[i]]) {
                    let __t = (filterOrderKeysShortHandMap[filterOrderKeys[i]]) ? filterOrderKeysShortHandMap[filterOrderKeys[i]] + ":" + _fData[filterOrderKeys[i]] : _fData[filterOrderKeys[i]];
                    temp.push(__t);
                }
            }
            if(temp.length) {
                return temp.join("|");
            }
            return undefined;
        }

        function getOmniturePageviewData(trackCategory){
            let referrerPageType = localStorageService.getSessionItem('prevPageType');
            let field = {
                    "listingType": t.LISTING_CATEGORY_KEY,
                    "projectStatus":  t.PROJECT_STATUS_KEY,
                    "unitType": t.UNIT_TYPE_KEY,
                    "beds": t.BHK_KEY,
                    "budget": t.BUDGET_KEY,
                    "sellerId": t.SELLER_ID_KEY,
                    "sellerName":t.SELLER_NAME_KEY,
                    "propertyName": t.PROPERTY_NAME_KEY,
                    "email": t.EMAIL_KEY,
                    "phone": t.PHONE_USER_KEY,
                    "projectName": t.PROJECT_NAME_KEY,
                    "localityId": t.LOCALITY_ID_KEY,
                    "cityId": t.CITY_ID_KEY,
                    "visitorCity": t.VISITOR_CITY_KEY,
                    "listingId": t.LISTING_ID_KEY,
                    "projectId": t.PROJECT_ID_KEY,
                    "builderId": t.BUILDER_ID_KEY,
                    "placeId": t.PLACE_ID_KEY,
                    "selleScore": t.SELLER_SCORE_KEY,
                    "pageLoadTime": t.PAGE_LOAD_TIME_KEY,
                    "domReadyTime": t.DOM_READY_TIME_KEY
                },
                data = {},
                pageData = utils.getPageData(),
                userData = localStorageService.getItem('enquiry_info') || {},
                timingData = utils.getPageTimingData(),
                pageExtraData = utils.getPageExtraData();

            if(userData){
                try{
                     userData =  JSON.parse(userData);
                } catch(e) {
                    userData = {};
                }
            }
            $.each(field,(key,val)=>{
                if(pageData[key]){
                    data[val] = pageData[key];
                }else if(pageExtraData[key]){
                    data[val] = pageExtraData[key];
                }else if(userData[key]){
                    if(['phone','email'].indexOf(key)!=-1){
                        data[val] = userData[key];
                    }
                }else if(timingData[key]){
                    data[val] = timingData[key];
                }
            });

            data[t.LISTING_CATEGORY_KEY] = trackCategory || data[t.LISTING_CATEGORY_KEY];
            if(referrerPageType && referrerPageType.pageType){
                data[t.PREVIOUS_PAGE_NAME_KEY] = referrerPageType.pageType;
            }
            data[t.FILTER_USED] = _getFilterUsedData();
            return data;
        }
        function _appendVisitorCityAndFirePageView({cityLocationPromise, pageType, onmitureData}) {
            cityLocationPromise.then(function(city) {
                if (city) {
                    onmitureData[t.VISITOR_CITY_KEY] = city.label || city.userCity;
                }
                //amplitudeTrackingService.trackPageView(pageType, onmitureData);
                irisTrackingService.trackPageView(pageType,onmitureData);
                facebookPixelTrackingService.trackPageView(pageType, onmitureData);
            }, function() {
                //amplitudeTrackingService.trackPageView(pageType, onmitureData);
                irisTrackingService.trackPageView(pageType,onmitureData);
                facebookPixelTrackingService.trackPageView(pageType, onmitureData);
            });
        }

        function firePageView(pageType,trackCategory){
            let onmitureData = getOmniturePageviewData(trackCategory),
                // loginPromise = loginService.isUserLoggedIn(),
                cityLocationPromise = defaultService.getCityByLocation(),
                omPageName = utils.getOmniturePageName();
            onmitureData[t.LOGIN_STATUS] = "notloggedin";
            onmitureData[t.OMNITUTRE_PAGE_NAME_KEY] = omPageName;
            //done to keep privacy of users
            onmitureData['Phone'] = "";
            onmitureData['Email'] = "";
            // loginPromise.then((response) => {
            //     if(response){
            //         onmitureData[t.LOGIN_STATUS] = "loggedin";
            //     }
            //     _appendVisitorCityAndFirePageView({
            //         cityLocationPromise,
            //         pageType,
            //         onmitureData
            //     });
            // }, ()=>{
                _appendVisitorCityAndFirePageView({
                    cityLocationPromise,
                    pageType,
                    onmitureData
                });
            // });
            localStorageService.setSessionItem("prevPageType",{pageType: omPageName});
        }


        function trackPageView(pageObj = {}){
            let pageData = utils.getPageData(),
                pageTypeMap = t.PAGE_TYPE_MAP,
                pageType = pageObj.pageType || pageTypeMap[pageData['pageType']],
                trackCategory = pageObj.trackCategory || pageData['listingType'];

            if(['Home','Locality','City'].indexOf(pageType) > -1){
                firePageView(pageType,trackCategory);
            } else if(['SERP City','SERP Static','SERP Project','SERP Landmark','SERP General','SERP Locality', 'SERP Suburb','SERP MAP City','SERP MAP Static','SERP MAP Project','SERP MAP Landmark','SERP MAP General','SERP MAP Locality', 'SERP MAP Suburb','Property','Project'].indexOf(pageType) > -1){
                firePageView(pageType, trackCategory);
            } else if(['SERP Builder','SERP MAP Builder'].indexOf(pageType) > -1){
                if(pageData['cityId']){
                    firePageView(pageType + ' City', trackCategory);
                    return;
                }
                firePageView(pageType + ' India', trackCategory);
            } else if(['SERP Agent','SERP MAP Agent'].indexOf(pageType) > -1){
                if(pageData['listingId']){
                    firePageView(pageType + ' Property', trackCategory);
                } else if(pageData['projectId']){
                    firePageView(pageType + ' Project', trackCategory);
                } else if(pageData['cityName']){
                    firePageView(pageType + ' City', trackCategory);
                } else {
                    firePageView(pageType + ' India', trackCategory);
                }
            } else {
                pageType = pageType || 'Others';
                firePageView(pageType,trackCategory);
            }
        }

        return {
            trackEvent,
            trackPageView,
        };
    });

    return Box.Application.getService(SERVICE_NAME);
});
