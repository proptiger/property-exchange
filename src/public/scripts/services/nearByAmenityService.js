/**
 * @fileoverview A common service which hold list of supported amenities and fetches data of the same
 * @author [Aditya Jha]
 */

/*global Box*/
define([
    'common/sharedConfig',
    'services/apiService',
    'services/commonService'
], (sharedConfig, apiService, commonService) => {
    "use strict";
    const SERVICE_NAME = 'nearByAmenityService';

    Box.Application.addService(SERVICE_NAME, (application) => {
        var factory = {};
        var amenities = [{
            name:'school',
            displayName:'Schools',
            icon:'icon-school'
        }, {
            name:'restaurant',
            displayName:'Restaurants',
            icon:'icon-restaurant'
        }, {
            name:'hospital',
            displayName:'Hospitals',
            icon:'icon-hospital'
        }, {
            name:'atm',
            displayName:'ATMs',
            icon:'icon-atm'
        }, {
            name:'shopping_mall',
            displayName:'Shopping Malls',
            icon:'icon-cart'
        }, {
            name:'cinema',
            displayName:'Cinemas',
            icon:'icon-cinema'
        }, {
            name:'night_life',
            displayName:'Nightclubs',
            icon:'icon-night-life'
        }];

        factory.amenity = amenities;

        var _constructApiUrl = function(latitude, longitude, distance, start, rows) {
            return  sharedConfig.apiHandlers.getAmenities({
                query:{
                    latitude:latitude,
                    longitude:longitude,
                    distance:distance,
                    start:start,
                    rows:rows
                }
            }).url;
        };

        function limitResponse(response) {
            if (!response) {return;}

            for (let prop in response) {
                if (response[prop].length > 5) {
                    response[prop].length = 5;
                }
            }

            return response;
        }

        factory.fetchData = function(latitude, longitude, distance=5, start=0, rows=999) {
            if(!(latitude && longitude)) {return;}
            var url = _constructApiUrl(latitude, longitude, distance, start, rows);
            commonService.bindOnLoadPromise().then(() => {
                apiService.get(url, true).then((response) => {
                    response = limitResponse(response);
                    application.broadcast('nearByAmenityDataRecieved', {
                        data: response
                    });
                }, (error) => {
                    console.log(error);
                });
            });
        };

        factory.distance = 5;
        return factory;
    });

    return Box.Application.getService(SERVICE_NAME);
});
