/**
 * Name: sessionService.js
 * Description: Helper file for maintaining user sessions
 * author: [Ramakrishna]
 */


'use strict';

define(['services/localStorageService', "common/utilFunctions"], (localStorageService, utilFunctions) => {
	let SERVICE_NAME="SessionService";

	Box.Application.addService(SERVICE_NAME, (application) => {

		const
			UPDATE_SESSION_EVENT_KEY = "update_session",
			GET_SESSION_EVENT_KEY = "get_session",
			FEEDBACK_SESSION_EVENT_KEY = "update_feedback",
			SESSION_INFO = "session",
			SESSION_TIMEOUT = 0.5 * 1000,
			SESSION_KEY = "session_id",
			CONTACT_NUMBER_STORAGE = "contact_numbers",
			FEEDBACK_DATA_KEY = "feedbackSessionData";

		let $ = application && application.getGlobal('jQuery');
		let sessionId = localStorageService.getSessionItem(SESSION_KEY),
			feedbackData = null;

		function _updateContactNumberStorage() {
			let contactedStorage = localStorageService.getItem(CONTACT_NUMBER_STORAGE) || {},
			   keys = Object.keys(contactedStorage),
			   length = (keys && keys.length) || 0,
			   currentSessionId = getSessionId().c;

			for(let i = 0; i < length ; i++) {
				if(keys[i] !== currentSessionId) {
					delete contactedStorage[keys[i]];
				}
			}   
			localStorageService.setItem(CONTACT_NUMBER_STORAGE,contactedStorage);
		}

		function _updateSessionInfo(sessionId){
			let sessionInfo = localStorageService.getItem(SESSION_INFO) || {};
			if(sessionId.id != sessionInfo.c){
				sessionInfo.p = sessionInfo.c || null;
			}
			sessionInfo.c = sessionId.id;
			localStorageService.setItem(SESSION_INFO, sessionInfo);
		}

		function _initSessionManually(){
			sessionId = {
				id: utilFunctions.randomId(10),
				timestamp: Date.now()
			};
			localStorageService.setSessionItem(SESSION_KEY, sessionId);
			_updateSessionInfo(sessionId);
		}

		function _getSession(){
			feedbackData = localStorageService.getSessionItem(FEEDBACK_DATA_KEY);
			localStorageService.setItem(UPDATE_SESSION_EVENT_KEY, sessionId);
			localStorageService.deleteItem(UPDATE_SESSION_EVENT_KEY);
			localStorageService.setItem(FEEDBACK_SESSION_EVENT_KEY, feedbackData);
			localStorageService.deleteItem(FEEDBACK_SESSION_EVENT_KEY);
		}

		function _updateSession(e, cb){
			let value = e.newValue;
			if(value){
				if(e.key == UPDATE_SESSION_EVENT_KEY && !sessionId){
					sessionId = JSON.parse(e.newValue);
					cb();
					localStorageService.setSessionItem(SESSION_KEY, sessionId);
				} else if(e.key == FEEDBACK_SESSION_EVENT_KEY && !feedbackData){
					feedbackData = JSON.parse(e.newValue);
					cb();
					localStorageService.setSessionItem(FEEDBACK_DATA_KEY, feedbackData);
				}
			}
		}

		function _storageListener(event, cb){
			let e = event.originalEvent;
			switch(e.key){
				case GET_SESSION_EVENT_KEY:
					_getSession();		
					break;

				case UPDATE_SESSION_EVENT_KEY:
					_updateSession(e, cb);
					break;
				case FEEDBACK_SESSION_EVENT_KEY:
					_updateSession(e, cb);
			}
		}

		function startSession(cb) {
			if(typeof cb != 'function'){
				cb = function(){};
			}
			if(!sessionId){
				localStorageService.setItem(GET_SESSION_EVENT_KEY, {timestamp: Date.now()});
				setTimeout(() => {
					if(!sessionId){
						_initSessionManually();
						cb();
					}
				}, SESSION_TIMEOUT);
			} else {
				cb();
			}
			$(window).bind("storage", (event) => {
				_storageListener(event, cb);
			});

			_updateContactNumberStorage();
		}

		function getSessionId(){
			return localStorageService.getItem(SESSION_INFO) || {};
		}

		function _checkRepeatUser(){
			let sessionInfo = localStorageService.getItem(SESSION_INFO) || {};
			if(sessionInfo.p){
				return true;
			}
			return false;
		}

		function isRepeatUser(){
			return new Promise((resolve) => {
				if(sessionId){
					return resolve(_checkRepeatUser());
				} else {
					setTimeout(() => {
						return resolve(_checkRepeatUser());
					}, SESSION_TIMEOUT);
				}
			});
		}

		return {
			startSession,
			getSessionId,
			isRepeatUser
		};
	});
	return Box.Application.getService(SERVICE_NAME);
});
