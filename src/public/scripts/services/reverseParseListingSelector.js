"use strict";

define(['common/reverseParseListingSelectorFunc'
], (reverseParseSelectorFunc) => {
    var reverseParseSelector = function(selector){
        return reverseParseSelectorFunc.mainfunction(selector);
    }; 
    return {
        reverseParseSelector
    };
});