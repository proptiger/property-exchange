"use strict";
define(['services/loggerService', 'services/utils', 'services/intersectionObserverService', 'services/configService', 'bundles/dependencyMap'], (Logger, Utils, intersectionObserver, ConfigService, dependencyMap) => {
    const SERVICE_NAME = 'CommonService';

    Box.Application.addService(SERVICE_NAME, (application) => {

        let loadedModuleMap = {},
            modulesBindedOnLoad = {},
            googleAdScriptPromise,
            openedPopupCount = 0;

        const MODULE_LOADTIME_SELECTOR = 'data-loadOn',
            $ = application && application.getGlobal('jQuery') || window['jQuery'] ,
            LAZY_MODULE_SELECTOR = "data-lazyModule",
            NORMAL_MODULE_SELECTOR = "data-module",
            Application = Box.Application;

        var loadTrovitTracking = (function() {
            var trovitLoaded = false;
            return function() {
                let pageType = Utils.getPageData('pageType');
                if(!trovitLoaded && pageType!=="HOME_PAGE_URLS"){
                    trovitLoaded = true;
                    setTimeout(function(){
                        (function(i,s,o,g,r,a,m){
                            i['TrovitAnalyticsObject']=r;
                            i[r]=i[r]||function(){ (i[r].q=i[r].q||[]).push(arguments) },i[r].l=1*new Date();
                            a=s.createElement(o),m=s.getElementsByTagName(o)[0];
                            a.async=1;
                            a.src=g;
                            m.parentNode.insertBefore(a,m)
                        })(window,document,'script','https://analytics.trovit.com/trovit-analytics.js','ta');
                    });
                }
            }
        })();

        var loadHotJar = (function() {
            var hotJarLoaded = false;
            return function() {
                if(!hotJarLoaded){
                    hotJarLoaded = true;
                    (function(h,o,t,j,a,r){
                        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
                        h._hjSettings={hjid:1140251,hjsv:6};
                        a=o.getElementsByTagName('head')[0];
                        r=o.createElement('script');r.async=1;
                        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
                        a.appendChild(r);
                        })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
                }
            }
        })();
        /**
         * Retrieve the names of modules that are present in DOM
         * @param {boolean} flag for getting lazy modules or normal modules, if true is passed returns array of lazy modules path.
         * @returns {array} Array of modules path.
         */
        function getModulesPath(lazyload, container = document, parent, ignoreCounterModuleCheck) { //jshint ignore:line
            const MODULE_SELECTOR = lazyload ? LAZY_MODULE_SELECTOR : NORMAL_MODULE_SELECTOR;
            const COUNTER_MODULE_SELECTOR = lazyload ? NORMAL_MODULE_SELECTOR : LAZY_MODULE_SELECTOR;
            let modulesPaths = [],
                moduleName, path;
            let moduleElements = Box.DOM.queryAll(container, "[" + MODULE_SELECTOR + "]:not(["+COUNTER_MODULE_SELECTOR+"])");
            if(ignoreCounterModuleCheck){
                moduleElements = Box.DOM.queryAll(container, "[" + MODULE_SELECTOR + "]");
            }
            for (let i = 0; i < moduleElements.length; i++) {
                moduleName = moduleElements[i].getAttribute(MODULE_SELECTOR);
                path = getModuleDependency(moduleElements[i].dataset, {name: moduleName, parent});
                if (path && lazyload) {
                    modulesPaths.push({
                        'loadOn': moduleElements[i].getAttribute(MODULE_LOADTIME_SELECTOR),
                        'name': moduleName
                    });
                } else if (path && !lazyload) {
                    modulesPaths.push(path);
                }
            }
            return modulesPaths;
        }

        function getModuleDependency(moduleDataset, {name, parent}){
            let path = 'modules/';
            let fileName = dependencyMap[name];
            if(fileName){
                path = `scripts/dependency/${fileName}`;
            } else if(parent){
                path += `${parent}/submodules/${name}`;
            } else {
                path += `${name}/scripts/index`;
            }
            return path;
        }

        function loadModule(data, type, container = document) {
            let dependencies = [],
                names = [],
                path;

            let moduleDataset = getLazyModule(data.name);

            if (!Utils.isArray(data) && moduleDataset) {
                path = getModuleDependency(moduleDataset, data);
                dependencies.push(path);
                names.push({
                    'name': data.name,
                    'loadId': data.loadId
                });
            } else {
                for (let i = 0; i < data.length; i++) {
                    moduleDataset = getLazyModule(data[i].name);
                    path = getModuleDependency(moduleDataset, data[i]);
                    if (moduleDataset) {
                        names.push({
                            'name': data[i].name,
                            'loadId': data[i].loadId
                        });
                        dependencies.push(path);
                    }
                }

            }

            require(dependencies, function() {
                const MODULE_SELECTOR = 'data-lazyModule';
                let modules, selector;
                for (let i = 0; i < names.length; i++) {
                    selector = getModuleSelector(MODULE_SELECTOR, names[i], type);
                    modules = Box.DOM.queryAll(container, selector);
                    modules = modules ? modules : [];
                    let length = modules.length;
                    for (let j = 0; j < length; j++) {
                        $(modules[j]).attr('data-module', names[i].name);
                        Application.start(modules[j]);
                    }
                }
            });
        }

        function getModuleSelector(MODULE_SELECTOR, module, type){
            let selector = `[${MODULE_SELECTOR}=${module.name}]:not([data-loadOn])`;
            if (["event", "afterLoad", "inview"].indexOf(type) > -1 && !module.loadId) {
                selector = `[${MODULE_SELECTOR}=${module.name}][data-loadOn="${type}"]`;
            } else if (["event", "afterLoad", "inview"].indexOf(type) > -1 && module.loadId) {
                selector = `[${MODULE_SELECTOR}=${module.name}][data-loadOn="${type}"][id=${module.loadId}]`;
            }
            return selector;
        }

        function loadLazyModules(container = document) {
            let modulesDetails = getModulesPath(true, container);
            modulesDetails = removeDuplicateFromArray(modulesDetails);
            for (let i = 0; i < modulesDetails.length; i++) {
                if (['event', 'afterLoad', "inview"].indexOf(modulesDetails[i].loadOn) == -1) {
                    requestIdleCallback(()=>{return loadModule(modulesDetails[i], null, container);}, {timeout: 2000});
                }
            }
        }

        function loadAutoLazyModules(container = document){
            let modulesDetails = getModulesPath(true, container);
            let loadOnToCapture = ['afterLoad'];
            if(!intersectionObserver.observeLazyModules(container)){
                loadOnToCapture.push("inview");
            }
            modulesDetails = removeDuplicateFromArray(modulesDetails);
            for (let i = 0; i < modulesDetails.length; i++) {
                if (loadOnToCapture.indexOf(modulesDetails[i].loadOn) > -1) {
                    requestIdleCallback(()=>{return loadModule(modulesDetails[i], modulesDetails[i].loadOn, container);}, {timeout: 2000});
                }
            }
        }

        function checkModuleLoaded(moduleName, id) {
            let keys, length;
            if (!loadedModuleMap[moduleName]) {
                return false;
            }
            if (id) {
                return loadedModuleMap[moduleName][id] ? true : false;
            }
            keys = Object.keys(loadedModuleMap[moduleName]);
            length = keys.length;
            if (length === 0) {
                return false;
            }
            for (let i = 0; i < length; i++) {
                if (!loadedModuleMap[moduleName][keys[i]]) {
                    return false;
                }
            }
            return true;
        }

        function setModuleLoaded(moduleName, id) {
            loadedModuleMap[moduleName] = loadedModuleMap[moduleName] ? loadedModuleMap[moduleName] : {};
            loadedModuleMap[moduleName][id] = true;
        }

        function getAllModules(includeLazyModule, el = document) {
            let modules = [],
                moduleElements = Box.DOM.queryAll(el, '[data-module]');

            moduleElements = includeLazyModule ? moduleElements.concat(Box.DOM.queryAll(el, '[data-lazyModule]')) : moduleElements;

            for (let i = 0; moduleElements && i < moduleElements.length; i++) {
                var name = moduleElements[i].getAttribute('data-module');
                if (!name && includeLazyModule) {
                    name = moduleElements[i].getAttribute('data-lazyModule');
                }
                if (name) {
                    modules.push(name);
                }
            }
            return modules;
        }

        function getModule(moduleName, el=document){
            let modules = [];
            let moduleElem = Box.DOM.queryAll(el, `[data-module=${moduleName}],[data-lazyModule=${moduleName}]`);
            for (let i = 0; moduleElem && i < moduleElem.length; i++) {
                var id = moduleElem[i].id;
                var name = moduleElem[i].getAttribute('data-module');
                if (!name) {
                    name = moduleElem[i].getAttribute('data-lazyModule');
                }
                if (name) {
                    modules.push({name, id});
                }
            }
            return modules;
        }

        function getLazyModule(name) {
            let el = Box.DOM.query(document, "[data-lazyModule='" + name + "']");
            if(el){
                return el.dataset;
            }
            return false;
        }

        function initializeCallbacks(data) {
            data = data && data.data;
            let eventData = data.messageData;
            if (data.message == 'moduleLoaded') {
                if (!checkModuleLoaded(eventData.name, eventData.id)) {
                    setModuleLoaded(eventData.name, eventData.id);
                    // if (typeof(modulesBindedOnLoad[eventData.name]) == 'function') {
                    //     modulesBindedOnLoad[eventData.name](eventData.id);
                    // }
                    if(eventData.name && eventData.id && modulesBindedOnLoad[eventData.name] && typeof (modulesBindedOnLoad[eventData.name][eventData.id]) == 'function'){
                        modulesBindedOnLoad[eventData.name][eventData.id](eventData.id);
                    } else {
                        if(eventData.name && modulesBindedOnLoad[eventData.name] && typeof (modulesBindedOnLoad[eventData.name]['default']) == 'function'){
                            modulesBindedOnLoad[eventData.name]['default'](eventData.id);
                        }
                    }
                }
            } else if (data.message == 'loadModule') {
                if (typeof eventData === "object" && !Utils.isArray(eventData) && eventData.loadAll && eventData.el) {
                    let el = Box.DOM.query(document, eventData.el);
                    startAllModules(el);
                    if (eventData.loadLazy) {
                        loadLazyModules(el);
                    }
                } else {
                    loadModule(eventData, 'event');
                }
            } else if (data.message == 'loadImages') {
                addImageObserver(eventData.selector);
            }
        }

        function bindOnModuleLoad(moduleName, callback, ids) {

            if(!modulesBindedOnLoad[moduleName]){
                modulesBindedOnLoad[moduleName] = {};
            }

            if(ids && ids.length){
                for(let i = 0; i < ids.length; i++){
                    modulesBindedOnLoad[moduleName][ids[i]] = callback;
                }
            } else {
                modulesBindedOnLoad[moduleName]['default'] = callback;     
            }

            if (Utils.isArray(ids)) {
                let length = ids.length;
                for (let i = 0; i < length; i++) {
                    if (checkModuleLoaded(moduleName, ids[i])) {
                        callback(ids[i]);
                    }
                }
            } else {
                if (checkModuleLoaded(moduleName)) {
                    callback();
                }
            }
        }

        function _loadLazyImages(container = document, replaceWith = 'data-src') {
            let selector = `[${replaceWith}]`,
                backgroundImageSelector = '.dummy-heroshot, .dummy-taxonomy, .dummy-agent-bground, .dummy-builder-bground, .dummy-locality-bground, .dummy-lifestyle, .dummy-similar-img, .dummy-placeholder, .dummy-bg, .dummy-overlay',
                images = Box.DOM.queryAll(container, selector),
                backgroundImagePatternSelector = "data-background-image",
                backgroundImages = Box.DOM.queryAll(container, "[" + backgroundImagePatternSelector + "]"),
                dummyBackgroundElements = Box.DOM.queryAll(container, backgroundImageSelector);
            for (let i = 0, length=images.length; i < length; i++) {
                let src = images[i].getAttribute(replaceWith);
                src && images[i].setAttribute('src', src);  //jshint ignore:line
            }
            for (let i=0,length=dummyBackgroundElements.length;i<length;i++){
                let currentElement = dummyBackgroundElements[i];
                $(currentElement).removeClass(backgroundImageSelector.replace(/[.,]/g,''));
            }
            for (let i = 0, length = backgroundImages.length; i < length; i++) {
                let src = backgroundImages[i].getAttribute(backgroundImagePatternSelector);
                src && backgroundImages[i].setAttribute('style', "background-image:url('" + src + "')");  //jshint ignore:line
            }
        }

        function addImageObserver(container = document, replaceWith = 'data-src') {
            if(intersectionObserver.isObserverSupported && replaceWith == intersectionObserver.replaceWith) {
                let obv = intersectionObserver.addToObserver(container);
                if(obv == null){
                    _loadLazyImages(container, replaceWith);
                }
            } else {
                _loadLazyImages(container, replaceWith);
            }
        }

        function restartImageObserver(replaceWith = 'data-src'){
            if(intersectionObserver.isObserverSupported){
                let obv = intersectionObserver.restartObserver(replaceWith);
                if(obv == null){
                    _loadLazyImages(document, replaceWith);
                }
            } else {
                _loadLazyImages(document, replaceWith);
            }
        }

        function removeDuplicateFromArray(arr){
            arr = arr.filter( function( item, index, inputArray ) {
                   return inputArray.indexOf(item) == index;
            });
            return arr;
        }

        function startAllModules(el = document, extra = [], beforeInitCallback, afterInitCallback, parent, ignoreCounterModuleCheck) {   //jshint ignore:line
            let dependencies = getModulesPath(false, el, parent, ignoreCounterModuleCheck);
            dependencies = dependencies.concat(extra);
            dependencies = removeDuplicateFromArray(dependencies);
            if (dependencies.length > 0) {
                require(dependencies, () => {
                    if (beforeInitCallback && typeof beforeInitCallback === 'function') {
                        beforeInitCallback();
                    }
                    // Logger.log('Starting All Modules Inside ', el, '...');
                    if (el === document) {
                        Application.init();
                        Application.setErrorHandler((ex) => {
                            if(window.isLocal){
                                throw ex;
                            } else {
                                if(window.Raven){
                                    window.Raven.captureException(ex);
                                }
                            }
                        });
                        // Start the non-decendent modules manually
                        // assuming we got all the 1st level dependencies already
                        // TODO: How to check if it lies in dependency ??
                        // Box.DOM.queryAll(el, "[data-module]:not([data-lazyModule])").map((eMod)=>{
                        //     console.warn("Starting ---", eMod);
                        //     Application.start(eMod);
                        // });
                    } else {
                        Application.startAll(el);
                        loadAutoLazyModules(el);
                        addImageObserver(el);
                    }
                    // Logger.log('All Modules Inside ', el, ' Started...');
                    if (afterInitCallback && typeof afterInitCallback === 'function') {
                        afterInitCallback();
                    }
                });
            } else {
                addImageObserver(el);
            }
        }

        function stopAllModules(el = document) {
            // Logger.log('Stopping All Modules Inside ', el, '...');
            let allModules = getAllModules(true, el);
            $.each(allModules, (k, v) => {
                if (loadedModuleMap[v]) {
                    delete loadedModuleMap[v];
                }
                if (modulesBindedOnLoad[v]) {
                    delete modulesBindedOnLoad[v];
                }
            });
            Application.stopAll(el);
            // Logger.log('All Modules Inside ', el, 'Stopped...');
        }

        function stopModule(el = document) {
            let moduleName = el.getAttribute('data-module');
            if (moduleName) {
                if (loadedModuleMap[moduleName]) {
                    delete loadedModuleMap[moduleName];
                }
                if (modulesBindedOnLoad[moduleName]) {
                    delete modulesBindedOnLoad[moduleName];
                }
            }
            Application.stop(el);
        }

        function removePageModule() {
            let PageModuleElement = Box.DOM.query(document, "[data-controller]");
            if (PageModuleElement) {
                PageModuleElement.removeAttribute('data-module');
            }
        }

        function getPageModule() {
            let PageModuleElement = Box.DOM.query(document, "[data-controller]"),
                controllerName = PageModuleElement ? PageModuleElement.getAttribute("data-controller") : null,
                controllerPath = controllerName ? 'scripts/pageModules/' + controllerName : null;
            return {
                "name": controllerName,
                "path": controllerPath,
                "el": PageModuleElement
            };
        }

        function getAllPageDependencies(pageModule) {
            if(dependencyMap[pageModule]){
                return [dependencyMap[pageModule]];
            } else {
                return [pageModule]
            }
            return v;
        }

        function ajaxify(url, allowSameRequest = false, noAjaxy=false){
            var absURL = url;
            window.vernac = {
                domainURI: window.location.protocol+'//'+window.location.host,
                vernacURI: '/'
            }
            if(url.indexOf(window.vernac.domainURI) == -1){
                absURL = (window.vernac.domainURI || '').replace(/\/$/,'') + 
                        (window.vernac.vernacURI || '').replace(/\/$/,'') +
                        (url[0] !== '/' ? `/${url}` : url);
            }
            if(noAjaxy){
                window.location = absURL;
                return;
            }
            if(ConfigService.ajaxifyConfig.pluginon){
                $().ajaxify(absURL, allowSameRequest);
            }else{
                window.location = absURL;
            }
        }

        function bindOnLoad(cb){
            if (document.readyState.toLowerCase() === 'complete') {
                cb();
            } else {
                $(window).load(cb);
            }
        }

        function bindOnApplicationClose(cb){
            $(window).on("unload",cb);
        }

        function bindOnLoadPromise(){
            var defer = $.Deferred();
            if (document.readyState.toLowerCase() === 'complete') {
                defer.resolve();
            } else {
                $(window).load(defer.resolve);
            }
            return defer.promise();
        }

        function popupClosed() {
            --openedPopupCount;
        }

        function popupOpened() {
            ++openedPopupCount;
        }

        function getOpenedPopupCount() {
            return openedPopupCount;
        }

        function resetPopupCount() {
            openedPopupCount = 0;
        }

        function restartAllModules(element, ignoreCounterModuleCheck){
            stopAllModules(element);
            startAllModules(element, null, null, null, null, ignoreCounterModuleCheck);
        }

        function loadGoogleAds(elem = document){
            if(window.isLocal){
                return;
            }
            bindOnLoadPromise().then(() => {
                if(!googleAdScriptPromise){
                    googleAdScriptPromise = $.getScript("//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js");
                }
                return googleAdScriptPromise;
            }).then(() => {
                let notLoadedAds = $(elem).find('[data-google-add="true"]:not([data-adsbygoogle-status="done"]):not(.hide)');
                for(let i = 0; i < notLoadedAds.length; i++){
                    (adsbygoogle = window.adsbygoogle || []).push({});
                }
            });
        }

        function getAjaxifyConfig(){
            return ConfigService.ajaxifyConfig;
        }

        function observeDomElement(elements = [],broadcastMessage='elementInViewPort') {
            return intersectionObserver.observeElements(elements, broadcastMessage);
        }

        function unobserveDomElement(elements = []){
            return intersectionObserver.unobserveElements(elements);
        }

        return {
            getModulesPath,
            loadLazyModules,
            checkModuleLoaded,
            getAllModules,
            getModule,
            bindOnModuleLoad,
            initializeCallbacks,
            addImageObserver,
            restartImageObserver,
            startAllModules,
            stopAllModules,
            restartAllModules,
            stopModule,
            removePageModule,
            getPageModule,
            getAllPageDependencies,
            isEmail: Utils.isEmail,
            isValidValue: Utils.isValidValue,
            ajaxify,
            bindOnApplicationClose,
            bindOnLoadPromise,
            popupClosed,
            popupOpened,
            getOpenedPopupCount,
            bindOnLoad,
            resetPopupCount,
            loadGoogleAds,
            loadAutoLazyModules,
            getAjaxifyConfig,
            observeDomElement,
            unobserveDomElement,
            loadTrovitTracking,
            loadHotJar,
            loadModule
        };
    });
    return Box.Application.getService(SERVICE_NAME);
});
