"use strict";
(function() {

        const YEAR_TIME = 365 * 24 * 60 * 60 * 1000;
        var propertyMappingFunction,
            filters,
            listingFilter,
            andFilterLength,
            

        _getString = function(filterValue) {
            let value = Array.isArray(filterValue) ? filterValue : [filterValue];
            return value.join(',');
        },

        _getStringFromRange = function(value) {
            let stringVal;
            if (value && (value.from || value.to)) {
                stringVal = value.from || '';
                stringVal += value.to ? `,${value.to}` : '';
            }
            return stringVal;
        },

        

        _getPropertyType = function(unitTypeIds){
            let propertyType = [], unitType;
            unitTypeIds = Array.isArray(unitTypeIds) ? unitTypeIds : unitTypeIds.split(',');
            for(let j=0; j<unitTypeIds.length; j++){
                unitType = propertyMappingFunction(unitTypeIds[j]);
                if(unitType){
                    propertyType.push(unitType);
                }
                
            }
            return propertyType.join(',');
        },

        _setKeyValue = function(key, value, index) {
            if (!key && (value===undefined||value===null||value==="")) {
                return;
            }
            var currentTime,completionTime;
        switch (key) {
            case 'reraIdExpired':
                filters.reraIdExpired = value+'';
                break;
            case 'projectUnderInvestigation':
                filters.projectUnderInvestigation = value+'';
                break;
            case 'hasTownShip':
                filters.hasTownShip = _getString(value);
                break;
            case 'projectStatus':
                filters.projectStatus = _getProjectStatus(value);
                break;
            case 'bedrooms':
                // TODO: this causes a bug on 4bhk seo url
                if(value[0] >= 4) {
                    filters.beds = "3plus";
                }else {
                    filters.beds = _getString(value);
                }
                break;
            case 'stateId':
                filters.stateId = _getString(value);
                break;
            case 'cityId':
                filters.cityId = _getString(value);
                break;
            case 'localityId':
                filters.localityId = _getString(value);
                break;
            case 'suburbId':
                filters.suburbId = _getString(value);
                break;
            case 'projectId':
                filters.projectId = _getString(value);
                break;
            case 'localityOrSuburbId':
                filters.localityOrSuburbId = _getString(value);
                break;
            case 'builderId':
                filters.builderId = _getString(value);
                break;
            case 'facingId':
                filters.facingId = _getString(value);
                break;
            case 'unitTypeId':
                filters.propertyType = _getPropertyType(value);
                break;
            case 'listingFurnished':
                value = _getString(value);
                filters.furnished = value.toLowerCase().replace(/-/g,'');
                break;
            case 'listingCategory':
                value  = _getString(value).toLowerCase();
                if(value.indexOf("rental")>=0) {
                    filters.listingType = 'rent';
                }else {
                filters.listingType = 'buy';
                filters.newOrResale = value;
                }
                break;
            case 'listingSellerCompanyType':
                value  = _getString(value);
                filters.postedBy = value.toLowerCase();
                break;
            case 'price':
                filters.budget = _getStringFromRange(value);
                break;
            case 'latitude':
                filters.latitude = _getStringFromRange(value);
                break;
            case 'longitude':
                filters.longitude = _getStringFromRange(value);
                break;
            case 'size':
                filters.area = _getStringFromRange(value);
                break;
            case 'listingPossessionDate':
                currentTime = new Date();
                  var  possessionTime = new Date(value.to),
                    diff; 
                diff = parseInt((possessionTime - currentTime)/YEAR_TIME);
                filters.possession = diff;
                break;
            case 'listingMaxConstructionCompletionDate':
                filters.ageOfProperty = filters.ageOfProperty || []; 
                    completionTime = new Date(listingFilter.and[index]['range']['listingMaxConstructionCompletionDate'].to);
                    currentTime = new Date();
                filters.ageOfProperty[0] = parseInt((currentTime - completionTime)/YEAR_TIME);
                break;
            case 'listingMinConstructionCompletionDate':
                filters.ageOfProperty = filters.ageOfProperty || []; 
                    completionTime = new Date(listingFilter.and[index]['range']['listingMinConstructionCompletionDate'].from);
                    currentTime = new Date();
                filters.ageOfProperty[1] = parseInt((currentTime - completionTime)/YEAR_TIME);
                break;
            case 'listingConstructionStatusId':
                if(value == 2 && (!filters.ageOfProperty || !filters.ageOfProperty.length)){
                    filters.ageOfProperty = ['any'];
                }else if(value == 1 && !filters.possession){
                    filters.possession = 'any';
                }
                break;
            case 'listingAmenitiesIds':
                filters.listingAmenitiesIds = _getString(value);
                break;
            case 'listingViewDirectionIds':
                filters.listingViewDirectionIds = _getString(value);
                break;
            case 'rk':
                filters.rk = _getString(value);
                break;
            case 'countryId':
                filters.countryId = _getString(value);
                break;
            }

        },

        _getFilterObject = function() {
            let key, value, eachObj, objKey;
            for (let i = 0; i < andFilterLength; i++) {
                eachObj = listingFilter.and[i];
                objKey = Object.keys(eachObj).join(',');
                key = Object.keys(listingFilter.and[i][objKey]).join(',');
                value = listingFilter.and[i][objKey][key];
                //set key value for filter
                _setKeyValue(key, value, i);
            }
            if (filters.ageOfProperty) {
                filters.ageOfProperty = filters.ageOfProperty.join(',');
            }

            if(filters.newOrResale && filters.newOrResale.indexOf("primary")>=0 && filters.newOrResale.indexOf("resale")>=0) {
               filters.newOrResale =""; 
            }
        },

        mainfunction = function(selector, options={}) {
            if(typeof selector == 'string'){
                try{
                    selector = JSON.parse(selector) || {};
                }catch(e){
                    selector = {};
                }
            }
            let sortBy = {};
            filters = {},
                listingFilter = selector.filters || selector,
                sortBy = (selector.sort && selector.sort[0])  || null,
                andFilterLength = listingFilter && listingFilter.and && listingFilter.and.length ? listingFilter.and.length : 0;  //jshint ignore:line
            if (andFilterLength) {
                _getFilterObject();
            }
            if(options.sort){
                return {
                    filters,
                    sortBy
                };
            }
            return filters;
        },
  
        parsers = {
           mainfunction
        };

    if (typeof module !== 'undefined' && module.exports) {
        let utilsServer = require('./utilFunctions'),
            _ = require('lodash');
        var nodeMappings = function(unitTypeId){
            return utilsServer.getPropertyType(unitTypeId, true);
        };
        propertyMappingFunction = nodeMappings;

        var _getProjectStatus = function(unitType) {  //jshint ignore:line
            let projectStatus = utilsServer.getProjectStatus();
            return _.findKey(projectStatus, function(val) {
                return _.isEqual(val, unitType);
            });
        };

        module.exports = parsers;
    } else if (typeof define === 'function' && define.amd) { //to create a amd component
        define(['services/utils'],function(utilsClient) {

            var clientMappings = function(unitTypeId){
                return utilsClient.getPropertyType(unitTypeId, true);
            };
            propertyMappingFunction = clientMappings;
            return parsers;
        });
    } else {
        global = (function() {  //jshint ignore:line
            return this || (0, eval)('this'); //jshint ignore:line
        }());
        global.utils = parsers;
    }

}());


        
