/**
 * TATVIC -: Integrating JS script across buyer website for Last Click and First Click  Channel Attribution Implementation
 */
"use strict";
try {
   if (!document.referrer.match('www.makaan.com')) {
       var tvc_referrel_exclusion = ['makaan.com']; // Add exact referral exclusion urls without http/https, www and trailing slash
       var cookieExpireTime = 365; // Expire time is 1 year = 365 days

       if (typeof tvc_readCookie != 'function') {
           var tvc_readCookie = function (key) {
               var result;
               return (result = new RegExp('(?:^|; )' + encodeURIComponent(key) + '=([^;]*)').exec(document.cookie)) ? (result[1]) : null;
           }
       }
       if (typeof tvc_queryParameters != 'function') {
           var tvc_queryParameters = function (key) {
               var result;
               return (result = new RegExp('(?:\\?|\\&)' + encodeURIComponent(key) + '=([^\\&\\#]*)').exec(window.location.href)) ? (result[1]) : null;
           }
       }
       if (typeof tvc_setCookie != 'function') {
           var tvc_setCookie = function (cname, cvalue, exdays) {
               var d = new Date();
               d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
               var expires = 'expires=' + d.toUTCString();
               document.cookie = cname + '=' + cvalue + '; ' + expires;
           }
       }
       var tvcCampaignAttributation = function (isFirstUser) {
           if (tvc_queryParameters('gclid') || tvc_queryParameters('gclsrc')) {
               tvc_campaign_source = 'google';
               tvc_campaign_medium = 'cpc';
               tvcCampaignAtt = tvc_campaign_source + ' / ' + tvc_campaign_medium;
           } else if (tvc_queryParameters('utm_source')) {
               tvc_campaign_source = tvc_queryParameters('utm_source') ? tvc_queryParameters('utm_source') : '(not set)';
               tvc_campaign_medium = tvc_queryParameters('utm_medium') ? tvc_queryParameters('utm_medium') : '(not set)';
               tvcCampaignAtt = tvc_campaign_source + ' / ' + tvc_campaign_medium;
           } else if (tvc_referrer && tvc_referrel_exclusion.indexOf(tvc_referrer) == -1) {
               if (tvc_referrer.match(/(google\..*|bing.com|ask.com|yahoo.com|msn.com|isearch.babylon.com)/g)) {
                   tvc_campaign_source = tvc_referrer.match(/(google|bing|ask|yahoo|msn|babylon)/g)[0];
                   tvc_campaign_medium = 'organic';
                   tvcCampaignAtt = tvc_campaign_source + ' / ' + tvc_campaign_medium;
               } else {
                   tvc_campaign_source = tvc_referrer;
                   tvc_campaign_medium = 'referral';
                   tvcCampaignAtt = tvc_campaign_source + ' / ' + tvc_campaign_medium;
               }
           } else {
               tvc_campaign_source = '(direct)';
               tvc_campaign_medium = '(none)';
               tvcCampaignAtt = tvc_campaign_source + ' / ' + tvc_campaign_medium;
           }
           if (isFirstUser) {
               tvc_setCookie('tvc_sm_fc', tvcCampaignAtt, cookieExpireTime);
           }
           return tvcCampaignAtt;
       }
       var TVCbrowser = function () {
           var ua = navigator.userAgent,
               tem, M = ua.match(/(opera|chrome|safari|firefox|msie|trident|ucbrowser(?=\/))\/?\s*(\d+)/i) || [];
           if (/trident/i.test(M[1])) {
               tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
               return 'IE ' + (tem[1] || '');
           }
           if (M[1] === 'Chrome') {
               tem = ua.match(/\bOPR\/(\d+)/)
               if (tem != null) return 'Opera ' + tem[1];
           }
           M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?'];
           if ((tem = ua.match(/version\/(\d+)/i)) != null) M.splice(1, 1, tem[1]);
           return M.join(' ');
       }
       var tvc_referrer = document.referrer ? document.referrer.replace(/(?:http:\/\/|https:\/\/)(?:www.)?([a-zA-Z0-9\.-]*)\/.*/g, '$1') : '';
       var tvc_current_location = window.location.href;
       var tvc_lc_campaign, tvc_fc_campaign;
       var tvc_campaign_source = 'NA';
       var tvcCampaignAtt;
       var tvc_campaign_medium = 'NA';
       var tvc_cookie_source = 'NA';
       var tvc_cookie_medium = 'NA';
       var tvcDateFromCookie = tvc_readCookie('_ga').split('.')[tvc_readCookie('_ga').split('.').length - 1];
       var tvcCookieTime = new Date(tvcDateFromCookie * 1000);
       var tvcCurrentTime = new Date();
       var tvcTimeDiff = Math.abs(tvcCurrentTime.getTime() - tvcCookieTime.getTime()) / 1000;
       tvcTimeDiff /= 60;
       var tvcFinalTime = Math.abs(Math.round(tvcTimeDiff));
       console.log(tvcFinalTime);
       var tvc_SM_cookie = tvc_readCookie('tvc_sm_fc');
       if (tvc_SM_cookie) {
           tvc_cookie_source = tvc_SM_cookie.split(' / ')[0];
           tvc_cookie_medium = tvc_SM_cookie.split(' / ')[1];
           tvc_fc_campaign = tvc_cookie_source + ' / ' + tvc_cookie_medium;
           var isFirstUser = false;
           tvc_lc_campaign = tvcCampaignAttributation(isFirstUser);
           sendPageViewGA(tvc_fc_campaign, tvc_lc_campaign);
       } else {
           if (tvcFinalTime < 5) {
               var isFirstUser = true;
               tvc_fc_campaign = tvcCampaignAttributation(isFirstUser);
               tvc_lc_campaign = tvc_fc_campaign;
               sendPageViewGA(tvc_fc_campaign, tvc_lc_campaign);
           } else {
               var isFirstUser = false;
               tvc_fc_campaign = tvcCampaignAttributation(isFirstUser);
               tvc_lc_campaign = tvc_fc_campaign;
               sendPageViewGA(tvc_fc_campaign, tvc_lc_campaign);
           }
       }
   } else {
        sendPageViewGA();
   }
} catch (e) {
   // Custom LC & FC model error
   var tvcXhr = new XMLHttpRequest();
   tvcXhr.open('GET', "https://script.google.com/macros/s/AKfycbzxTRNZaQNH5AhzQPmOujnaTMCjmGJssBoWmN0sw5-lhOIlJcj7/exec?error_name=" + e.name + "&error_msg=" + e.message + "&browser=" + TVCbrowser + "&pageurl=" + document.location.href, true);
   tvcXhr.send();

    sendPageViewGA();
}
