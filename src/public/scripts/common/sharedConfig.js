 "use strict";
 (function() {
     var global, config;

     const ALLIANCE_API_END_POINT = 'petra/';
     const PAGE_NAME_PREFIX = "makaan";
     
     function getURL(urlObj, query) {
         if (!query) {
             return urlObj;
         }

         let queryStr, seperator;
         if (typeof query === 'string') {
             queryStr = query;
         } else {
             queryStr = '';
             for (let key in query) {
                 queryStr = queryStr + '&' + key + '=' + query[key];
             }
             queryStr = queryStr.slice(1);
         }
         seperator = urlObj.url.indexOf('?') > -1 ? '&' : '?';
         urlObj.url += `${seperator}${queryStr}`;
         return urlObj;
     }

     config = {
        DATA_MODULE_LISTING: '[data-module="listing"]',
        DATA_CURRENT_SLIDE_INDEX:'data-currentslideindex',
        EXPERIMENTS_COOKIE_NAME: 'experiments',
        OWL_CAROUSEL_CONFIG: {
            navigation: true,
            slideSpeed: 300,
            paginationSpeed: 400,
            autoPlay: true,
            mouseDrag: true,
            singleItem: true
            // transitionStyle : "fadeUp",
            // "singleItem:true" is a shortcut for:
            // items : 1,
            // itemsDesktop : false,
            // itemsDesktopSmall : false,
            // itemsTablet: false,
            // itemsMobile : false
        },
         PAGE_TYPE_MAP: {
             'HOME_PAGE_URLS': 'Home',
             'PROJECT_URLS_OVERVIEW': 'Project',
             'CITY_URLS_OVERVIEW': 'City',
             'SUBURB_URLS_OVERVIEW': 'Suburb',
             'LOCALITY_URLS_OVERVIEW': 'Locality',
             'PROPERTY_URLS': 'Property',

             'CITY_URLS': 'SERP City',
             'SUBURB_URLS': 'SERP Suburb',
             'PROJECT_URLS': 'SERP Project',
             'BUILDER_URLS': 'SERP Builder',
             'LOCALITY_URLS': 'SERP Locality',
             'CITY_TAXONOMY_URLS': 'SERP City',
             'SUBURB_TAXONOMY_URLS': 'SERP Suburb',
             'BUILDER_TAXONOMY_URLS': 'SERP Builder',
             'LOCALITY_TAXONOMY_URLS': 'SERP Locality',
             'LOCALITY_SUBURB_TAXONOMY': 'SERP Locality',
             'STATIC_URLS': 'SERP Static',
             'NEARBY_LISTING_URLS': 'SERP Landmark',
             'NEARBY_URLS': 'SERP Landmark',
             'NEARBY_LISTING_TAXONOMY_URLS': 'SERP Landmark',

             'SELLER_PROPERTY_URLS': 'SERP Agent',
             'SIMILAR_PROPERTY_URLS': 'SERP General',
             'LISTINGS_PROPERTY_URLS': 'SERP General',
             'PROJECT_SERP': 'SERP Project Card',
             'PROJECT_SERP_DYNAMIC_URLS': 'SERP Project Card',

             'CITY_URLS_MAPS': 'SERP MAP City',
             'SUBURB_URLS_MAPS': 'SERP MAP Suburb',
             'PROJECT_URLS_MAPS': 'SERP MAP Project',
             'BUILDER_URLS_MAPS': 'SERP MAP Builder',
             'LOCALITY_URLS_MAPS': 'SERP MAP Locality',
             'CITY_TAXONOMY_URLS_MAPS': 'SERP MAP City',
             'SUBURB_TAXONOMY_URLS_MAPS': 'SERP MAP Suburb',
             'BUILDER_TAXONOMY_URLS_MAPS': 'SERP MAP Builder',
             'LOCALITY_TAXONOMY_URLS_MAPS': 'SERP MAP Locality',
             'LOCALITY_SUBURB_TAXONOMY_MAPS': 'SERP MAP Locality',
             'STATIC_URLS_MAPS': 'SERP MAP Static',
             'NEARBY_LISTING_URLS_MAPS': 'SERP MAP Landmark',
             'NEARBY_URLS_MAPS': 'SERP Landmark',
             'NEARBY_LISTING_TAXONOMY_URLS_MAPS': 'SERP MAP Landmark',
             'SELLER_PROPERTY_URLS_MAPS': 'SERP MAP Agent',
             'SIMILAR_PROPERTY_URLS_MAPS': 'SERP MAP General',
             'LISTINGS_PROPERTY_URLS_MAPS': 'SERP MAP General',

             'SERVICES': 'Providers',
             'BUYER_DASHBOARD': 'Buyer Journey',

             'ALL_BUILDERS': 'ALL Builders',
             'TOP_BUILDERS': 'TOP Builders',
             'ALL_BROKERS': 'ALL Brokers',
             'TOP_BROKERS': 'TOP Brokers',
             'PRICE_TREND': 'Price Trend',

             'ALL_LOCALITIES': 'Overview',
             'STATIC_URLS_CITY_ALL_OVERVIEW': 'Overview',
             'STATIC_URLS_ALL_BUILDERS': 'Overview',
             'STATIC_URLS_ALL_BROKERS': 'Overview',

             'PARTNER_WITH_MAKAAN': "Benefits",

             '404': '404',
             '500': '500'
         },
         listingSellerTransactionMapping: {
            'CityExpertDealMaker': {label:'City Expert',badge:'dealmaker-expert-badge',displayClass:'edm'},
            'LocalityExpertDealMaker': {label:'Locality Expert',badge:'dealmaker-expert-badge',displayClass:'edm'},
            'SuburbExpertDealMaker': {label:'Locality Expert',badge:'dealmaker-expert-badge',displayClass:'edm'},
            'ExpertDealMaker': {label:'Expert Deal Maker',badge:'dealmaker-expert-badge',displayClass:'edm'},
            'DealMaker': {label:'Deal Maker',badge:'dealmaker-badge',displayClass:'dm'},
            'ACCOUNT_LOCKED': {label:'ACCOUNT_LOCKED',badge:'account-locked',displayClass:'al'}
        },
        LEAD_PROFILE_QUESTION_TYPE: {
            TEXT: 1,
            TEXT_AREA: 2,
            RADIO: 3,
            DATE_RANGE: 4,
            CHECKBOX: 5,
            TYPEAHEAD: 6,
            MULTIPLE: 7,
            MULTIPLE_TYPEAHEAD: 8
        },
        LP_NEXT_BTN_QUESTION_TYPE: [1, 5, 7, 8],
         enquiryTypeMapping: {
            "PYR"                   : 1,
            "GET_CALLBACK"          : 2,
            "CALL_NOW"              : 3,
            "GET_INSTANT_CALLBACK"  : 4,
            "CONTACT_SELLER"        : 5,
            "EMAIL_CLICK"           : 6,
            "VIEW_PHONE"            : 7,
            "SIMILAR_SELLERS"       : 8,
            "AUTO_PYR"              : 9,
            "ENQUIRY_TEMP"          : 10,
            "HOME_LOAN"             : 11,
            "AUTO_CALL_NOW"         : 12
        },
         PROMOTION_PAGE_IDS: {
            'MAKKAN_DOMAIN_ID' : 1,
            'RIPPLE_CASE_ID' : 1,     // ripple is a mongo DB collection name used for makaan platform.
            'RIPPLE_SALE_TYPE_ID' : 1,
            'INDIA_COUNTRY_ID' : 1,
            'RIPPLE_CASE_REASON_ID' : 1,
            'RIPPLE_PRODUCT_TYPE': 'LEAD_GEN'
        },
         selectCouponPrice : 2000,
         makaanSelectCities : [21,'21'],
         couponShareEligibleTime : 172800000,// FORTY_EIGHT_HR_MILLISEC
         coachMarkSelector: {
            knowMoreKey: 'Know More',
            unlockKey: 'Unlock Voucher'
         },
         unitTypeToIdMapping:{
            'apartment': [1],
            'builder-floor': [7],
            'independent-floor': [7],
            'independentfloor': [7],
            'independent-house': [19],
            'independenthouse': [19],
            'villa': [2],
            'residential-plot': [3],
            'plot': [3],
            'farmhouse': [8],
         },
         serpExperimentCities :['ChEnNaI',"gUrGaOn",5,11],
         serpContactExperimentCities:["gUrGaOn",11],
         recencySortedCities:[44,60,31],
         sellerFeedbackRestrictedPage: ['MAKAAN_SELECT'],
         propcard_experiment_key: "makaan_propcard_click",
         MPROFILE_CARD_SEEN :'m_profile_card_seen',
         MPROFILE_ANSWERED :'m_profile_answered',
         TRACK_COOKIE_STATUS: "track_cookie_status",
         PAGE_NAME_STRATEGY_MAP: {
             'HOME_PAGE_URLS': () => {
                 return `${PAGE_NAME_PREFIX}:home`;
             },
             'PROJECT_URLS_OVERVIEW': ({ projectName, projectId }) => {
                 return `${PAGE_NAME_PREFIX}:project detail page:${projectName}:${projectId}`;
             },
             'CITY_URLS_OVERVIEW': ({ cityId, cityName }) => {
                 return `${PAGE_NAME_PREFIX}:city landing page:${cityName}:${cityId}`;
             },
             'SUBURB_URLS_OVERVIEW': () => {
                 return `${PAGE_NAME_PREFIX}:Suburb`;
             },
             'LOCALITY_URLS_OVERVIEW': ({ localityId, localityName }) => {
                 return `${PAGE_NAME_PREFIX}:locality landing page:${localityName}:${localityId}`;
             },
             'PROPERTY_URLS': ({ propertyName, propertyId }) => {
                 return `${PAGE_NAME_PREFIX}:property detail page:${propertyName}:${propertyId}`;
             },

             'CITY_URLS': ({ cityId, cityName }) => {
                 return `${PAGE_NAME_PREFIX}:city listing page:${cityName}:${cityId}`;
             },
             'SUBURB_URLS': ({ suburbId, suburbName }) => {
                 return `${PAGE_NAME_PREFIX}:suburb listing page:${suburbId}:${suburbName}`;
             },
             'PROJECT_URLS': ({ projectId, projectName }) => {
                 return `${PAGE_NAME_PREFIX}:project serp page:${projectName}:${projectId}`;
             },
             'BUILDER_URLS': ({ builderId, builderName }) => {
                 return `${PAGE_NAME_PREFIX}:builder listing page:${builderId}:${builderName}`;
             },
             'LOCALITY_URLS': ({ localityId, localityName }) => {
                 return `${PAGE_NAME_PREFIX}:locality listing page:${localityName}:${localityId}`;
             },
             'CITY_TAXONOMY_URLS': ({ cityId, cityName }) => {
                 return `${PAGE_NAME_PREFIX}:city listing page:${cityName}:${cityId}`;
             },
             'SUBURB_TAXONOMY_URLS': ({ suburbId, suburbName }) => {
                 return `${PAGE_NAME_PREFIX}:suburb listing page:${suburbId}:${suburbName}`;
             },
             'BUILDER_TAXONOMY_URLS': ({ builderId, builderName }) => {
                 return `${PAGE_NAME_PREFIX}:builder listing page:${builderId}:${builderName}`;
             },
             'LOCALITY_TAXONOMY_URLS': ({ localityId, localityName }) => {
                 return `${PAGE_NAME_PREFIX}:locality listing page:${localityName}:${localityId}`;
             },
             'LOCALITY_SUBURB_TAXONOMY': ({ localityId, localityName }) => {
                 return `${PAGE_NAME_PREFIX}:locality listing page:${localityName}:${localityId}`;
             },
             'STATIC_URLS': () => {
                 return `${PAGE_NAME_PREFIX}:SERP Static`;
             },
             'NEARBY_LISTING_URLS': () => {
                 return `${PAGE_NAME_PREFIX}:SERP Landmark`;
             },
             'NEARBY_URLS': () => {
                 return `${PAGE_NAME_PREFIX}:SERP Landmark`;
             },
             'NEARBY_LISTING_TAXONOMY_URLS': () => {
                 return `${PAGE_NAME_PREFIX}:SERP Landmark`;
             },
             'SELLER_PROPERTY_URLS': ({ sellerId, sellerName }) => {
                 return `${PAGE_NAME_PREFIX}:broker listing page:${sellerId}:${sellerName}`;
             },
             'SIMILAR_PROPERTY_URLS': () => {
                 return `${PAGE_NAME_PREFIX}:SERP General`;
             },
             'LISTINGS_PROPERTY_URLS': () => {
                 return `${PAGE_NAME_PREFIX}:SERP General`;
             },

             'CITY_URLS_MAPS': ({ cityId, cityName }) => {
                 return `${PAGE_NAME_PREFIX}:city listing page map:${cityName}:${cityId}`;
             },
             'SUBURB_URLS_MAPS': ({ suburbId, suburbName }) => {
                 return `${PAGE_NAME_PREFIX}:suburb listing map page:${suburbId}:${suburbName}`;
             },
             'PROJECT_URLS_MAPS': ({ projectId, projectName }) => {
                 return `${PAGE_NAME_PREFIX}:project serp map page:${projectName}:${projectId}`;
             },
             'BUILDER_URLS_MAPS': ({ builderId, builderName }) => {
                 return `${PAGE_NAME_PREFIX}:builder listing page map:${builderId}:${builderName}`;
             },
             'LOCALITY_URLS_MAPS': ({ localityId, localityName }) => {
                 return `${PAGE_NAME_PREFIX}:locality listing page map:${localityName}:${localityId}`;
             },
             'CITY_TAXONOMY_URLS_MAPS': ({ cityId, cityName }) => {
                 return `${PAGE_NAME_PREFIX}:city listing page map:${cityName}:${cityId}`;
             },
             'SUBURB_TAXONOMY_URLS_MAPS': ({ suburbId, suburbName }) => {
                 return `${PAGE_NAME_PREFIX}:suburb listing map page:${suburbId}:${suburbName}`;
             },
             'BUILDER_TAXONOMY_URLS_MAPS': ({ builderId, builderName }) => {
                 return `${PAGE_NAME_PREFIX}:builder listing page map:${builderId}:${builderName}`;
             },
             'LOCALITY_TAXONOMY_URLS_MAPS': ({ localityId, localityName }) => {
                 return `${PAGE_NAME_PREFIX}:locality listing page map:${localityName}:${localityId}`;
             },
             'LOCALITY_SUBURB_TAXONOMY_MAPS': ({ localityId, localityName }) => {
                 return `${PAGE_NAME_PREFIX}:locality listing page map:${localityName}:${localityId}`;
             },
             'STATIC_URLS_MAPS': () => {
                 return `${PAGE_NAME_PREFIX}:SERP MAP Static`;
             },
             'NEARBY_LISTING_URLS_MAPS': () => {
                 return `${PAGE_NAME_PREFIX}:SERP MAP Landmark`;
             },
             'NEARBY_URLS_MAPS': () => {
                 return `${PAGE_NAME_PREFIX}:SERP MAP Landmark`;
             },
             'NNEARBY_LISTING_TAXONOMY_URLS_MAPS': () => {
                 return `${PAGE_NAME_PREFIX}:SERP MAP Landmark`;
             },
             'SELLER_PROPERTY_URLS_MAPS': ({ sellerId, sellerName }) => {
                 return `${PAGE_NAME_PREFIX}:broker listing page map:${sellerId}:${sellerName}`;
             },
             'SIMILAR_PROPERTY_URLS_MAPS': () => {
                 return `${PAGE_NAME_PREFIX}:SERP MAP General`;
             },
             'LISTINGS_PROPERTY_URLS_MAPS': () => {
                 return `${PAGE_NAME_PREFIX}:SERP MAP General`;
             },
             'SERVICES': () => {
                 return `Providers`;
             },
             'BUYER_DASHBOARD': () => {
                 return `Buyer Journey`;
             },

             'ALL_LOCALITIES': () => {
                 return `Overview`;
             },
             'STATIC_URLS_CITY_ALL_OVERVIEW': () => {
                 return `Overview`;
             },
             'STATIC_URLS_ALL_BUILDERS': () => {
                 return `Overview`;
             },
             'STATIC_URLS_ALL_BROKERS': () => {
                 return `Overview`;
             },
             '404': () => {
                 return `404`;
             },
             '500': () => {
                 return `500`;
             }
         },
         includeNearbyLocalitiesCookieName: 'makaanIncludeNearbyLocalities',
         leadClientIdCookieKey: 'mkn_lead_userid',
         vernacTrans: {
            "hi-in": "हिंदी में पढ़िए"
         },
         apiHandlers: {
             getAmenities({
                 cityId
             } = {}) {
                 return {
                     controller: 'amenitiesParser',
                     method: ['GET'],
                     url: cityId ? `/apis/getAmenities/${cityId}` : `/apis/getAmenities`,
                     route: '/apis/getAmenities/:cityId?'
                 };
             },
             getImages({
                 objectType,
                 objectId
             } = {}) {
                 return {
                     controller: 'imagesParser',
                     method: ['GET'],
                     url: `/apis/imageService/${objectType}/${objectId}`,
                     route: `/apis/imageService/:objectType/:objectId`
                 };
             },
             getNearByLocality() {
                 return {
                     controller: 'localityApi',
                     method: ['GET'],
                     url: '/apis/locality/near-by',
                     route: '/apis/locality/near-by'
                 };
             },
             getLocalityPriceTrend({
                 localityId
             } = {}) {
                 return {
                     controller: 'priceTrend',
                     method: ['GET'],
                     url: `/apis/locality/price-trend/${localityId}`,
                     route: '/apis/locality/price-trend/:localityId'
                 };
             },
             getSimilarProject({
                 projectId
             } = {}) {
                 return {
                     controller: 'similarProject',
                     method: ['GET'],
                     url: `/apis/similar/project/${projectId}`,
                     route: '/apis/similar/project/:projectId'
                 };
             },
             getSimilarListing({
                 listingId,
                 companyId,
                 isCommercial,
                 summary
             } = {}) {
                 return {
                     controller: 'similarListing',
                     method: ['GET'],
                     url: `/apis/similar/listing/${listingId}?companyId=${companyId || ""}&summary=${summary || ""}&distinctSeller=true&isCommercial=${isCommercial ? isCommercial : false}`,
                     route: '/apis/similar/listing/:listingId'
                 };
             },
             getSimilarConfigSeller() {
                 return {
                     controller: 'similarConfigSeller',
                     method: ['GET'],
                     url: `/apis/similar/seller`,
                     route: '/apis/similar/seller'
                 };
             },
             getCityTopLocality({
                 cityId,
                 listingCategory
             } = {}) {
                 return {
                     controller: 'cityApi',
                     method: ['GET'],
                     url: `/apis/city/top-locality/${cityId}?listingCategory=${listingCategory || "buy"}`,
                     route: '/apis/city/top-locality/:cityId'
                 };
             },
             getCityTopSellers({
                 cityId
             } = {}) {
                 return {
                     controller: 'sellerParser',
                     method: ['GET'],
                     url: `/apis/city/top-seller/${cityId}`,
                     route: '/apis/city/top-seller/:cityId'
                 };
             },
             getFeaturedProjects({
                 cityId
             } = {}) {
                 return {
                     controller: 'featuredProjectsParser',
                     method: ['GET'],
                     url: `/apis/city/featured-projects/${cityId}`,
                     route: '/apis/city/featured-projects/:cityId'
                 };
             },
             getDeveloperProjects({
                 productType
             } = {}) {
                 return {
                     controller: 'developerProjectsParser',
                     method: ['GET'],
                     url: `/apis/nc/developer-projects/${productType}`,
                     route: '/apis/nc/developer-projects/:productType'
                 };
             },
             getNewProjects({
                 cityId
             } = {}) {
                 return {
                     controller: 'newProjects',
                     method: ['GET'],
                     url: `/apis/city/new-projects/${cityId}`,
                     route: '/apis/city/new-projects/:cityId'
                 };
             },
             getBankDetails() {
                 return {
                     controller: 'banks',
                     method: ['GET'],
                     url: `/apis/bank/details`,
                     route: '/apis/bank/details'
                 };
             },
             getApprovedBankDetails({ projectId } = {}) {
                 return {
                     controller: 'approvedBanks',
                     method: ['GET'],
                     url: `/apis/approved-bank/details/${projectId}`,
                     route: '/apis/approved-bank/details/:projectId'
                 };
             },
             getCityPriceTrend({
                 cityId
             } = {}) {
                 return {
                     controller: 'priceTrend',
                     method: ['GET'],
                     url: `/apis/city/price-trend/${cityId}`,
                     route: '/apis/city/price-trend/:cityId'
                 };
             },
             getCityTopLocalities({
                 cityId
             } = {}) {
                 return {
                     controller: 'topLocalities',
                     method: ['GET'],
                     url: `/apis/city/topLocalities/${cityId}`,
                     route: '/apis/city/topLocalities/:cityId'
                 };
             },
             getCityPriceRange({
                 cityId
             } = {}) {
                 return {
                     controller: 'cityPriceRange',
                     method: ['GET'],
                     url: `/apis/city/price-range/${cityId}`,
                     route: '/apis/city/price-range/:cityId'
                 };
             },
             getProjectPriceTrend({
                 projectId
             } = {}) {
                 return {
                     controller: 'priceTrend',
                     method: ['GET'],
                     url: `/apis/project/price-trend/${projectId}`,
                     route: '/apis/project/price-trend/:projectId'
                 };
             },
             getLocalityDetail({ localityId } = {}) {
                 return {
                     controller: 'localityDetail',
                     method: ['GET'],
                     url: `/apis/locality-detail/${localityId}`,
                     route: `/apis/locality-detail/:localityId`
                 };
             },
             getProjectDetail({ projectId } = {}) {
                 return {
                     controller: 'projectDetail',
                     method: ['GET'],
                     url: `/apis/project-detail/${projectId}`,
                     route: `/apis/project-detail/:projectId`
                 };
             },
             getMultipleProjectDetails({projectIdList}={}){
                return {
                     controller: 'multipleProjectDetails',
                     method: ['GET'],
                     url: `/apis/project-details?projectIdList=${JSON.stringify(projectIdList)}`,
                     route: `/apis/project-details`
                 };
             },
             // getBankApplicationTracking(userId) {
             //     return {
             //         controller: 'bankApplicationTracking',
             //         method: ['GET'],
             //         url: `/apis/bank/application/tracking/${userId}`,
             //         route: `/apis/bank/application/tracking/:userId`
             //     }
             // },
             postEnquiry() {
                 return {
                     controller: 'enquiries',
                     method: ['POST', 'PUT'],
                     url: '/apis/nc/enquiries',
                     route: '/apis/nc/enquiries'
                 };
             },
             otpOnCall({ userId, userNumber } = {}) {
                 return {
                     controller: 'enquiries',
                     method: ['POST','GET'],
                     url: `/apis/nc/sendOtpOnCall/${userId}/${userNumber}?callType=otpOnCall`,
                     route: ['/apis/nc/sendOtpOnCall/:userId/:userNumber','/amp/apis/nc/sendOtpOnCall']
                 };
             },
             authenticateUser({ userId, userNumber } = {}) {
                 return {
                     controller: 'authenticateUser',
                     method: ['POST','GET'],
                     url: `/apis/nc/authenticateUser/${userId}/${userNumber}`,
                     route: '/apis/nc/authenticateUser/:userId/:userNumber'
                 };
             },
             postTempEnquiry() {
                 //let temp = 'temp';
                 return {
                     controller: 'enquiries',
                     method: ['POST', 'PUT'],
                     url: '/apis/nc/temp/enquiries',
                     route: '/apis/nc/:temp/enquiries'
                 };
             },
             verifyTempEnquiry() {
                 return {
                     controller: 'enquiries',
                     method: ['PUT'],
                     url: `/apis/nc/temp/enquiries/?callType=verifyTempEnquiry`,
                     route: '/apis/nc/:temp/enquiries'
                 };
             },
             failedEnquiryPush() {
                 return {
                     controller: 'failedEnquiryPush',
                     method: ['POST'],
                     url: '/apis/nc/failed-enquiry',
                     route: '/apis/nc/failed-enquiry'
                 };
             },
             buyerDashboard() {
                 return {
                     controller: 'buyerDashboardParser',
                     method: ['GET', 'POST', 'DELETE'],
                     url: '/apis/login/buyerDashboard',
                     route: '/apis/login/buyerDashboard'
                 };
             },
             alliances() {
                 return {
                     controller: 'alliances',
                     method: ['GET', 'POST', 'PATCH'],
                     url: '/apis/alliances',
                     route: '/apis/alliances'
                 };
             },
             shortlist() {
                 return {
                     controller: 'shortlist',
                     method: ['GET', 'POST', 'DELETE'],
                     url: '/apis/login/shortlist',
                     route: '/apis/login/shortlist'
                 };
             },
             smsService() {
                 return {
                     controller: 'smsService',
                     method: ['POST'],
                     url: '/apis/nc/smsService',
                     route: '/apis/nc/smsService'
                 };
             },
             callRating({sellerId, buyerId} = {}) {
                 return {
                     controller: 'callFeedbackRating',
                     method: ['POST', 'GET'],
                     url: `/apis/nc/call-rating/${sellerId}?buyerId=${buyerId || ''}`,
                     route: '/apis/nc/call-rating/:sellerId'
                 };
             },
             getVirtualNumber({ cityId, userId, listingType, location } = {}) {
                 return {
                     controller: 'virtualNumber',
                     method: ['GET'],
                     url: `/apis/virtualNumber/${listingType}/${cityId}/${userId}?referrer_test=${encodeURIComponent(location)}`, //TODO: referrer to be removed later
                     route: '/apis/virtualNumber/:listingType/:cityId/:userId'
                 };
             },
             mailerService() {
                 return {
                     controller: 'mailerService',
                     method: ['POST'],
                     url: '/apis/nc/mailer',
                     route: '/apis/nc/mailer'
                 };
             },
             // mpAnalytics() {
             //     return {
             //         controller: 'mpAnalytics',
             //         method: ['POST'],
             //         url: '/apis/mpAnalytics',
             //         route: '/apis/mpAnalytics'
             //     };
             // },
             makaanHealth() {
                 return {
                     controller: 'makaanHealth',
                     method: ['GET'],
                     url: '/apis/makaan-health',
                     route: '/apis/makaan-health'
                 };
             },
             unsubscribeUrl({
                 unsubscribeKey
             } = {}) {
                 return {
                     controller: 'unsubscribeService',
                     method: ['GET'],
                     url: `/apis/nc/unsubscribe-service?unsubscribeKey=${unsubscribeKey}`,
                     route: '/apis/nc/unsubscribe-service'
                 };
             },
             resetMasterDetails({
                 resetFlag
             } = {}) {
                 return {
                     controller: 'resetMasterDetails',
                     method: ['GET'],
                     url: `/apis/nc/reset-master-details?reset=${resetFlag}`,
                     route: '/apis/nc/reset-master-details'
                 };
             },
             getCallFeedbackDetails({listingId, companyId} = {}) {
                 return {
                     controller: 'callFeedbackDetails',
                     method: ['GET'],
                     url: `/apis/nc/call-details?listingId=${listingId || ''}&companyId=${companyId || ''}`,
                     route: '/apis/nc/call-details'
                 };
             },
             pageViewTest() {
                 return {
                     controller: 'pageViewTest',
                     method: ['GET'],
                     url: `/apis/pageviewtest`,
                     route: '/apis/pageviewtest'
                 };
             },
             saveSearch() {
                 return {
                     controller: 'saveSearch',
                     method: ['POST'],
                     url: `/apis/nc/saveSearch`,
                     route: '/apis/nc/saveSearch'
                 };
             },
             widget({
                 widgetName
             } = {}) {
                 return {
                     controller: 'widgets',
                     method: ['GET'],
                     url: `/apis/widgets/${widgetName}`,
                     route: '/apis/widgets/:widget'
                 };
             },
             getInstantCall() {
                 return {
                     controller: 'callNow',
                     method: ['POST'],
                     url: `/apis/nc/callNow`,
                     route: '/apis/nc/callNow'
                 };
             },
             seoText() {
                 return {
                     controller: 'seoText',
                     method: ['GET'],
                     url: `/apis/seo-text`,
                     route: '/apis/seo-text'
                 };
             },
             getRawSeller({
                 sellerUserId
             } = {}) {
                 return {
                     controller: 'rawSellerParser',
                     method: ['GET'],
                     url: `/apis/raw-seller/${sellerUserId}`,
                     route: '/apis/raw-seller/:sellerUserId'
                 };
             },
             reportErrorParser() {
                 return {
                     controller: 'reportErrorParser',
                     method: ['POST'],
                     url: '/apis/nc/reportError',
                     route: '/apis/nc/reportError'
                 };
             },
             getListingDetails({ listingIds } = {}) {
                 return {
                     controller: 'listingDetails',
                     method: ['GET'],
                     url: `/apis/listing-details?listingIds=${JSON.stringify(listingIds)}`,
                     route: `/apis/listing-details`
                 };
             },
             getListingSERP({serpURL,serpParams,onlyListingCount,searchType}={}){
                return {
                     controller: 'listingSERP',
                     method: ['GET'],
                     url: `/apis/listingSERP?serpURL=${serpURL}&serpParams=${serpParams}&onlyListingCount=${onlyListingCount}&searchType=${searchType}`,
                     route: `/apis/listingSERP`
                 };
             },
             getListingCount(filterString){
                return {
                    controller: 'listingSERP',
                    method: ['GET'],
                    url: `/apis/listingCount?${filterString}&serpUrl=true`,
                    route: `/apis/listingCount`
                }
             },
             getTopAgentsURL({entityType,entityValue}={}){
                return {
                     controller: 'topAgentsURL',
                     method: ['GET'],
                     url: `/apis/topAgentsURL?entityType=${entityType}&entityValue=${entityValue}`,
                     route: `/apis/topAgentsURL`
                 };
             },
             getRecentDeals({start,rows,entityId,entityValue}={}){
                return {
                    controller: 'recentDeals',
                    method: ['GET'],
                    url: `/apis/recentDeals?start=${start}&rows=${rows}&entityId=${entityId}&entityValue=${entityValue}`,
                    route: `/apis/recentDeals`
                };
             },
             getPopularLocalities({cityId}={}){
                return {
                    controller: 'popularLocalities',
                    method: ['GET'],
                    url: `/apis/popularLocalities?cityId=${cityId}`,
                    route: `/apis/popularLocalities`
                };
             },
             pendingFeedbacks({ buyerId, companyId} = {}) {
                 return {
                     controller: 'pendingFeedbacks',
                     method: ['GET'],
                     url: `/apis/nc/pending-feedbacks/${buyerId}?companyId=${companyId || ''}`,
                     route: `/apis/nc/pending-feedbacks/:buyerId`
                 };
             },
             pendingSiteVisitFeedbacks({ buyerId } = {}) {
                 return {
                     controller: 'pendingSiteVisitFeedbacks',
                     method: ['GET'],
                     url: `/apis/nc/pending-siteVisit-feedbacks/${buyerId}`,
                     route: `/apis/nc/pending-siteVisit-feedbacks/:buyerId`
                 };
             },
             updateFeedbackRatingReason({ratingSubmitId} = {}) {
                 return {
                     controller: 'postFeedbackRatingReason',
                     method: ['PUT'],
                     url: `/apis/nc/callfeedback-rating-reason/${ratingSubmitId}`,
                     route: `/apis/nc/callfeedback-rating-reason/:ratingSubmitId`
                 };
             },
             getCommonApiData({ dataType } = {}) {
                 return {
                     controller: 'commonApiParser',
                     method: ['GET'],
                     url: `/apis/common-api/${dataType}`,
                     route: `/apis/common-api/:dataType`
                 };
             },
             trackCookies() {
                 return {
                     controller: 'trackCookieParser',
                     method: ['GET'],
                     url: `/apis/nc/track-cookie`,
                     route: `/apis/nc/track-cookie`
                 };
             },
             leadForm({action} = {}){
                return  {
                    controller: 'leadFormParser',
                    method: ["POST"],
                    url: `/apis/nc/lead/${action}`,
                    route: [`/apis/nc/lead/:action`,`/amp/apis/nc/lead/:action`]
                };
             },
             uploadUserReviewImage(userId){
                return {
                    controller: 'uploadImage',
                    method: ['POST'],
                    url: `/apis/uploadImage?userId=${userId}`,
                    route: '/apis/uploadImage'
                };
            },
            sellerRatingMap(listingUserCompanyUserId){
                return {
                    controller: 'sellersRatingReview',
                    method: ['GET'],
                    url: `/apis/sellerRatingReview?listingUserCompanyUserId=${listingUserCompanyUserId}`,
                    route: '/apis/sellerRatingReview'
                };
            },
            noCacheApiWrapper(api){
                return {
                    controller: 'apiWrapperParser',
                    method: ['GET'],
                    url: `apis/nc/wrapper/${api}`,
                    route: '/apis/nc/wrapper/*'
                };
            },
            userDetails(userId){
                return {
                    controller: 'userDetails',
                    method: ['GET', 'POST'],
                    url: `/apis/nc/user-details-userId?userId=${userId}`,
                    route:['/apis/nc/user-details-userId','/amp/apis/nc/userDetails']
                };
            },
            userDetailsByContactNumber(contactNumber){
                return {
                    controller: 'userDetails',
                    method: ['GET', 'POST'],
                    url: `/apis/nc/user-details-contactNumber?contactNumber=${contactNumber}`,
                    route: '/apis/nc/user-details-contactNumber'
                };
            },
            tempRippleCase(rippleCaseId) {
                return {
                    controller: 'rippleCase',
                    method: ['POST', 'PUT'],
                    url: `/apis/nc/ripple-case?rippleCaseId=${rippleCaseId}`,
                    route: '/apis/nc/ripple-case'
                };
            },
            getDecryptedToken(){
                return {
                    controller: 'tokenDecryption',
                    method: ['POST'],
                    url: `/apis/nc/decrypt`,
                    route: '/apis/nc/decrypt'
                };
            },
            sellerScoreBreakup(companyUserId){
                return {
                    controller: 'sellerScoreParser',
                    method: ['GET'],
                    url: `/apis/seller-score-details/${companyUserId}`,
                    route: '/apis/seller-score-details/:companyUserId'
                }
            },
            getSellerMatchingProperties(){
                return {
                    controller: 'matchingPropertiesParser',
                    method: ['GET'],
                    url: `/apis/seller-matching-properties`,
                    route: '/apis/seller-matching-properties'
                }
            },
            getSponsoredProjectAds({cityId, listingType, page} = {}){
                return {
                    controller: 'sponsoredAdsParser',
                    method: '[GET]',
                    url: `apis/nc/sponsoredAds?page=${page}&cityId=${cityId}&listingType=${listingType}`,
                    route: '/apis/nc/sponsoredAds'
                }
            },
            couponDetails({userId,expiryDate}={}){
                return {
                    controller: 'couponDetails',
                    method: ['GET', 'POST'],
                    url: `/apis/nc/makaan-select-coupons?userId=${userId}&expiryDate=${expiryDate}`,
                    route: '/apis/nc/makaan-select-coupons'
                };
            },
            buyerSellers({buyerUserId,saleType,sellerUserId} = {}){
               return {
                    controller: 'buyerSellers',
                    method: ['GET','POST'],
                    url: `/apis/nc/buyer-sellers?buyerUserId=${buyerUserId}&saleType=${saleType}&sellerUserId=${sellerUserId}`,
                    route: '/apis/nc/buyer-sellers'
                }; 
            },
            getSellerReviews(companyUserId){
                return {
                    controller: 'sellerReviews',
                    method: '[GET]',
                    url: `apis/fetchSellerReview/${companyUserId}`,
                    route: '/apis/fetchSellerReview/:companyUserId'
                }
            },
            getLeadProfileQuestion() {
                return {
                    controller: 'leadProfileQuestions',
                    method: ['GET'],
                    url: `/apis/nc/get-lead-profile-question`,
                    route: '/apis/nc/get-lead-profile-question'
                };
            },
            updateLeadProfileQuestion() {
                return {
                    controller: 'leadProfileQuestions',
                    method: ['POST','PUT'],
                    url: `/apis/update-lead-profile-question`,
                    route: '/apis/update-lead-profile-question'
                };
            },
            getLeadDeficitSellerProjects(){
                return {
                    controller: 'deficitSellerProjectsParser',
                    method: '[GET]',
                    url: `/apis/nc/fetchLeadDeficitSellerProjects`,
                    route: '/apis/nc/fetchLeadDeficitSellerProjects'
                }
            },
            postAskYourQuestion(){
                return {
                    controller: 'askYourQuestionParser',
                    method: ['POST'],
                    url: `/apis/nc/postCustomerQuestion`,
                    route: '/apis/nc/postCustomerQuestion'
                }
            },
            getPlaceSuggestions(query){
                return {
                    controller: 'osmPlacesParser',
                    method: ['GET'],
                    url: `/apis/maps/getplaces?query=${query}`,
                    route: '/apis/maps/getplaces'
                }
            },
            getPlaceDetails({placeId, placeType} = {}){
                return {
                    controller: 'osmPlacesParser',
                    method: ['GET'],
                    url: `/apis/maps/getplace?placeId=${placeId}&placeType=${placeType}`,
                    route: '/apis/maps/getplace'
                }
            }
         },
         getURL: getURL,
         clientApis: {
             getCountry() {
                 return {
                     url: `/petra/data/v1/entity/country`
                 };
             },
             postTempRippleCase() {
                return{
                    url: `/mystique/v1/ripple/case-post`
                };
             },
             verifyRippleCase(rippleCaseId) {
                return{
                    url: `/mystique/v1/ripple/case-put/${rippleCaseId}?isLoginRequired=false`
                };
             },
             getCity() {
                 return {
                     url: `/petra/data/v1/entity/city?selector={"fields":["id","label"],"paging":{"start":0,"rows":600}}`
                 };
             },
             getTopCities() {
                 return {
                     url: `/petra/data/v1/entity/city?selector={"fields":["id","label"],"filters":{"and":[{"equal":{"displayPriority":[1,2]}}]},"sort": [{ "field": "displayPriority", "sortOrder": "ASC" }, { "field": "displayOrder", "sortOrder": "ASC" }],"paging":{"start":0,"rows":600}}`
                 };
             },
             getAllianceCities() {
                 return {
                     url: ALLIANCE_API_END_POINT + "data/v1/entity/company-coupon/cities?fields=id,label,displayPriority"
                 };
             },
             putEnquiryOTPVerified(enquiryId, otp) {
                 return {
                     url: `/petra/data/v1/entity/enquiry/${enquiryId}?otp=${otp}&debug=true`
                 };
             },
             getCityByLocation() {
                 return {
                     url: `/petra/app/v1/mylocation?selector={"fields":["id","authorized","label","status","url","geo"]}&sourceDomain=Makaan`
                 };
             },
             duplicateUser({userId, duplicateUserIdsToBeAdded}) {
                 return {
                     url: `/petra/data/v1/entity/cookie-duplicate-user?userId=${userId}&duplicateUserIdsToBeAdded=${duplicateUserIdsToBeAdded}`
                 };
             },
             getCityTopLocalities({
                 cityId
             } = {}) {
                 return {
                     url: `/petra/data/v3/entity/locality/top?selector={"fields":["localityId","label","buyUrl","rentUrl","overviewUrl","latitude","longitude","suburb","city"],"filters":{"and":[{"equal":{"localityDbStatus":["Active","ActiveInMakaan"]}},{"equal":{"cityId":"${cityId}"}}]},"paging":{"start":0,"rows":5}}&sourceDomain=Makaan`
                 };
             },
             getConversationStatus({ buyerId, date, rows }) {
                 let callDetailsDate;
                 if (!date) {
                     let date = new Date(),
                         last = new Date(date.getTime() - (7 * 24 * 60 * 60 * 1000));
                     callDetailsDate = last.toISOString();
                 } else {
                     callDetailsDate = date.toISOString();
                 }
                 return {
                     url: `/kira/data/v3/entity/call-log?filters=callDuration!=;communicationLog.callerUserId==${buyerId};createdAt=ge=${callDetailsDate}&fields=rating,callLog,communicationLog.virtualNumberMapping,id,userId,cityId,callerUserId,extraData,createdAt,callDuration,callStatus,callerNumber,listingCategory&joinType=left(callRatings)&start=0&rows=${rows || 10}`
                 };
             },
             nearbyLocalities({
                 lat,
                 lon
             } = {}) {
                 return {
                     url: `/petra/data/v3/entity/locality?selector={"fields":["localityId","label","buyUrl","rentUrl","overviewUrl","latitude","longitude","suburb","city"],"filters":{"and":[{"equal":{"localityDbStatus":["Active","ActiveInMakaan"]}},{"geoDistance":{"geo":{"distance":10,"lat":"${lat}","lon":"${lon}"}}}]},"paging":{"start":0,"rows":5},"sort":[{"field":"geoDistance","sortOrder":"ASC"}]}&sourceDomain=Makaan`
                 };
             },
             getPopularSuggestions({
                 entityId,
                 rows
             } = {}) {
                 return {
                     url: `/columbus/app/v1/popular/suggestions?entityId=${entityId}&rows=${rows}&sourceDomain=Makaan`
                 };
             },
             searchTypeahead(params) {
                 return {
                     url: `/columbus/app/v6/typeahead?query=${params.query}&typeAheadType=${params.typeAheadType?params.typeAheadType:''}&city=${params.city?params.city:''}&usercity=${params.usercity?params.usercity:''}&rows=${params.rows?params.rows:''}&enhance=${params.enhance?params.enhance:''}&category=${params.category}&view=${params.view}&sourceDomain=Makaan`
                 };
             },
             chatApi() {
                 return {
                     url: '/api/expose-session'
                 };
             },
            sellerFeedback(sellerId, userFullName, leadId){
                return {
                    url: `/themis/data/v2/entity/feedback/deal-closure/${sellerId}?userFullName=${userFullName}&leadId=${leadId}&sourceDomain=Makaan`
                };
            },
            pushNotificationUserUpdate(){
                return {
                    url: `/madrox/data/v1/entity/onesignal-user`
                };
            },
            getSellerFeedback(companyUserId){
                return {
                    url: `/apis/fetchSellerReview/${companyUserId}`
                };
            },
            sellerFeedbackUpdate(feedbackId){
                return {
                    url: `/themis/data/v1/entity/feedback/deal-closure/${feedbackId}?sourceDomain=Makaan`
                };
            },
            feedbackReasons({templateId}){
                return {
                    url: `/themis/data/v1/entity/feedback/template/${templateId}`
                };
            },
            checkIndiaBullsEligibilty(){
                return {
                    url: `/cyclops/data/v1/entity/homeloan/check-eligibility`
                };
            },
            postIndiaBullsEnquiry(){
                return {
                    url: `/cyclops/data/v1/entity/homeloan/indiabull-enquiry`
                };
            },
             eventRouterTracking() {
                 return {
                     url: `/api/v0/publish_event/makaanbuyer`
                 }
             }
        }
     };

     Object.keys(config.apiHandlers).map((key) => {
         let func = config.apiHandlers[key];
         config.apiHandlers[key] = (function() {
             return function(options) {
                 if (options && options.query) {
                     return getURL(func(options), options.query);
                 }
                 return func(options);
             };
         })();
     });
     //to create a node module
     if (typeof module !== 'undefined' && module.exports) {
         module.exports = config;
     }
     //to create a amd component
     else if (typeof define === 'function' && define.amd) {
         define(function() {
             return config;
         });
     }
     //to create a global file if both are not present
     else {
         global = (function() { //jshint ignore:line
             return this || (0, eval)('this'); //jshint ignore:line
         }());
         global.config = config;
     }

 })();
