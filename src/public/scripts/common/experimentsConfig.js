 "use strict";
 define(function() {
     return {
		makaan_propcard_click: {
			"true": ".more-img "
		},
		makaan_serp_noresult: {
			"ad": ".js-rec-not-found-ad",
			"text": ".js-rec-not-found-txt"
		},
		makaan_pdp_map: {
			"true": "new-map-view"
		},	
		makaan_serp_card: {
			"true": ".single-line-view"
		},
		makaan_pdp_contact: {
			"new": "#seller-contact-bar",
			"old": "#get-callback"
		}
     };
 });