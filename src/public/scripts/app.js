// If a man does his best, what else is there? - George S. Patton
"use strict";
define('app', [
        'infra',
        // 'services/commonService',
        // 'services/loggerService',
        // 'services/ajaxifyService',
        // 'services/loadingService',
        // 'services/defaultService',
        // 'services/feedbackService',
        // 'services/sessionService',
        // 'services/searchService',
        // 'services/leadPyrService',
        // 'services/viewStatus',
        //
        // 'services/typeAheadService',
        // 'services/reverseParseListingSelector',
        // 'behaviors/leadBehaviour'
    ], function() {
    require([
        'services/commonService',
        'services/loggerService',
        'services/ajaxifyService',
        'services/defaultService',
        'services/sessionService'
    ], function() {
        const Application = Box.Application,
            CommonService = Application.getService('CommonService'),
            Logger = Application.getService('Logger'),
            ConfigService = Application.getService('ConfigService'),
            AjaxifyService = Application.getService('AjaxifyService'),
            LoadingService = Application.getService('LoadingService'),
            DefaultService = Application.getService('DefaultService'),
            //UrlService = Application.getService('URLService'),
            //Utils = Application.getService('Utils'),
            // TrackingConfigServiceService = Application.getService('TrackingConfigService'),
            //TrackingService = Application.getService('TrackingService'),
            //RequirmentNotificationService = Application.getService('requirmentNotificationService'),
            FeedbackService = Application.getService('FeedbackService'),
            SessionService = Application.getService('SessionService'),
            $ = Application.getGlobal('jQuery');
            SessionService.startSession();
            // imageMapForCalculatingClientSpeed = {
            //     url: "https://www.makaan.com/images/img-dumy-d1.4281f939.jpg",
            //     size: 84992, // bytes,
            //     unit: " Kilobit/sec"
            // },
        function appInit() {
            // initialize Ajaxify
            $('#content,#header-link,#page-popup-container,#outside-container,#seofooter-wrap,#pageSpecificCss,#similarContent,#verticalscrollspy,#pageBottomCss,#menuDrawer_SEO').ajaxify(CommonService.getAjaxifyConfig());
            AjaxifyService.bindProntoEvents();
            // Load App Start
            let pageModule = CommonService.getPageModule();
            CommonService.startAllModules(document, pageModule.path, () => {
                if (pageModule.el) {
                    pageModule.el.setAttribute("data-module", pageModule.name);
                }
                Logger.log('Application Started...');
            });
            //RequirmentNotificationService.clearAllPageQueue();
            // Tracking Init
            // LoadingService.loadAllTracking();
        }

        function pushNotificationInit() {
            if('serviceWorker' in navigator){
                //being used in ConfigService so don't remove
                var registerServiceWorker; // jshint ignore:line
                setTimeout(function delayServiceWorker() {
                    navigator.serviceWorker.register('OneSignalSDKWorker.js', {  // Let's register our serviceworker
                    scope: './'   // The scope cannot be parent to the script url
                    }).then(function(registration){
                        console.log('Serviceworker registration Successfull with scope: ',registration.scope);
                        $.getScript("https://cdn.onesignal.com/sdks/OneSignalSDK.js", ()=>{
                            Application.broadcast("loadModule", {
                                name: 'pushNotification'
                            });
                        });
                    }).catch(function(error) { // registration failed
                        console.log('Registration failed with ' + error);
                    });
                }, 3000);
            }
        }

        function calculateClientSpeed() {
            //todo: removed calculation of speed for testing of event firing issue
            // let startTime,
            //     endTime,
            //     download = new Image(0, 0),
            //     cacheBuster;

            // download.onload = function() {
            //     endTime = (new Date()).getTime();
            //     let duration = (endTime - startTime) / 1000,
            //         bitsLoaded = imageMapForCalculatingClientSpeed.size * 8,
            //         speedKbps = (bitsLoaded / duration)/1024,
            //         clientSpeedEvent = {};
            //     window.clientSpeed = speedKbps;

            //     // Broadcast Client Speed Measured
            //     Application.broadcast('clientSpeedMeasured',{clientSpeed:window.clientSpeed});
            //     //todo: temp gaevent loop to check bounce rate.
            //     // if(gaEventLoop){
            //     //     window.clearInterval(gaEventLoop);
            //     // }

            //     // Send client download speed to google analytics
            //     clientSpeedEvent[TrackingConfigServiceService.CATEGORY_KEY] = TrackingConfigServiceService.PERFORMANCE;
            //     clientSpeedEvent[TrackingConfigServiceService.LABEL_KEY] = window.clientSpeed + imageMapForCalculatingClientSpeed.unit;
            //     clientSpeedEvent[TrackingConfigServiceService.NON_INTERACTION_KEY] = 1;
            //     TrackingService.trackEvent(TrackingConfigServiceService.DL_SPEED,clientSpeedEvent);
            // };

            // startTime    = (new Date()).getTime();
            // cacheBuster  = "?nnn=" + startTime;
            // download.src = imageMapForCalculatingClientSpeed.url + cacheBuster;
            // download.id  = 'speedTest';
            window.clientSpeed = 700;
            ga('send', 'event','dlSpeed','dlSpeed', 1,undefined,{'nonInteraction':1});
            Application.broadcast('clientSpeedMeasured',{clientSpeed:window.clientSpeed});
        }

        // Listener for all modules that are loaded and also for those modules that need to be loaded
        Application.on('message', CommonService.initializeCallbacks);

        /* till nanobar constructor inits */
        window.nanobar = {
            go: function(){}
        };
        CommonService.bindOnLoadPromise().then(()=>{
            //loader
            window.nanobar = new Nanobar({
                bg: '#e71c28',
                id: 'loaderNano'
            });
        });

        // Load Global level configs
        ConfigService.loadGlobalConfig();
        if (document.readyState.toLowerCase() === 'interactive' || document.readyState.toLowerCase() === 'complete') {
            appInit();
        } else {
            $(document).ready(appInit);
        }
        //track cookies
        setTimeout(DefaultService.trackCookies);
        // Load Jarvis
        CommonService.bindOnLoad(()=>{
            requestIdleCallback(() => {pushNotificationInit()}, {timeout: 5000});
            // start data-lazyModules
            requestIdleCallback(() => {CommonService.loadLazyModules()}, {timeout: 5000});
            // Loading Images
            requestIdleCallback(() => {CommonService.restartImageObserver()}, {timeout: 5000});
            // start data-modules/lazymodules with data-loadOn
            requestIdleCallback(()=>{CommonService.loadAutoLazyModules(document)}, {timeout: 5000});
            //init trovit tracking
            requestIdleCallback(() => {CommonService.loadTrovitTracking()},{timeout: 5000});

            //load Hot jar
            requestIdleCallback(() => {setTimeout(function(){CommonService.loadHotJar()},4000)}, {timeout: 5000});

            // Tracking Init
            calculateClientSpeed();
        });

        //this is to delete old makaan cookies. Safe to remove after May 2017
        document.cookie='mkn_searchcookie=;domain=.makaan.com;Path=/;expires=Wed, 31 Dec 1969 23:59:59 GMT';
        document.cookie='mp_d295cc4b1e79b2c907fdce24a56bc8ab_mixpanel=;domain=.makaan.com;Path=/;expires=Wed, 31 Dec 1969 23:59:59 GMT';
        document.cookie='mp_43e125677066e06b6e36cc132e3741b2_mixpanel=;domain=.makaan.com;Path=/;expires=Wed, 31 Dec 1969 23:59:59 GMT';
    });
});

require(['app'],function(){});
