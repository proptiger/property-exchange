'use strict';

(function ($, window, document, undefined) {

    'use strict';

    var defaults = {

        mode: 'lg-slide',

        // Ex : 'ease'
        cssEasing: 'ease',

        //'for jquery animation'
        easing: 'ease',
        speed: 600,
        height: '100%',
        width: '100%',
        addClass: '',
        startClass: 'lg-start-zoom',
        backdropDuration: 150,
        hideBarsDelay: 6000,

        useLeft: false,

        closable: true,
        loop: true,
        escKey: true,
        keyPress: true,
        controls: true,
        slideEndAnimatoin: true,
        hideControlOnEnd: false,
        mousewheel: true,

        // .lg-item || '.lg-sub-html'
        appendSubHtmlTo: '.lg-sub-html',

        /**
         * @desc number of preload slides
         * will exicute only after the current slide is fully loaded.
         *
         * @ex you clicked on 4th image and if preload = 1 then 3rd slide and 5th
         * slide will be loaded in the background after the 4th slide is fully loaded..
         * if preload is 2 then 2nd 3rd 5th 6th slides will be preloaded.. ... ...
         *
         */
        preload: 1,
        showAfterLoad: true,
        selector: '',
        selectWithin: '',
        nextHtml: '',
        prevHtml: '',

        // 0, 1
        index: false,

        iframeMaxWidth: '100%',

        download: false,
        counter: true,
        appendCounterTo: '.lg-inner',
        appendFullscreenTo: '.lg-inner',

        swipeThreshold: 50,
        enableSwipe: true,
        enableDrag: true,

        dynamic: false,
        dynamicEl: [],
        galleryId: 1
    };

    function Plugin(element, options) {

        // Current lightGallery element
        this.el = element;

        // Current jquery element
        this.$el = $(element);

        // lightGallery settings
        this.s = $.extend({}, defaults, options);

        // When using dynamic mode, ensure dynamicEl is an array
        if (this.s.dynamic && this.s.dynamicEl !== 'undefined' && this.s.dynamicEl.constructor === Array && !this.s.dynamicEl.length) {
            throw "When using dynamic mode, you must also define dynamicEl as an Array.";
        }

        // lightGallery modules
        this.modules = {};

        // false when lightgallery complete first slide;
        this.lGalleryOn = false;

        this.lgBusy = false;

        // Timeout function for hiding controls;
        this.hideBartimeout = false;

        // To determine browser supports for touch events;
        this.isTouch = 'ontouchstart' in document.documentElement;

        // DIsable hideControlOnEnd if sildeEndAnimation is true
        if (this.s.slideEndAnimatoin) {
            this.s.hideControlOnEnd = false;
        }

        // Gallery items
        if (this.s.dynamic) {
            this.$items = this.s.dynamicEl;
        } else {
            if (this.s.selector === 'this') {
                this.$items = this.$el;
            } else if (this.s.selector !== '') {
                if (this.s.selectWithin) {
                    this.$items = $(this.s.selectWithin).find(this.s.selector);
                } else {
                    this.$items = this.$el.find($(this.s.selector));
                }
            } else {
                this.$items = this.$el.children();
            }
        }

        this.s.wrapper = this.s.onBody == true && this.s.selectWithin == '' ? $('body') : $(this.s.selectWithin);
        // .lg-item
        this.$slide = '';

        // .lg-outer
        this.$outer = '';

        this.init();

        return this;
    }

    Plugin.prototype.init = function () {

        var _this = this;

        // s.preload should not be more than $item.length
        if (_this.s.preload > _this.$items.length) {
            _this.s.preload = _this.$items.length;
        }

        // if dynamic option is enabled execute immediately
        var _hash = window.location.hash;
        if (_hash.indexOf('lg=' + this.s.galleryId) > 0) {

            _this.index = parseInt(_hash.split('&slide=')[1], 10);

            _this.s.wrapper.addClass('lg-from-hash');
            if (!_this.s.wrapper.hasClass('lg-on')) {
                setTimeout(function () {
                    _this.build(_this.index);
                    _this.s.wrapper.addClass('lg-on');
                });
            }
        }

        if (_this.s.dynamic) {

            _this.$el.trigger('onBeforeOpen.lg');

            _this.index = _this.s.index || 0;

            // prevent accidental double execution
            if (!_this.s.wrapper.hasClass('lg-on')) {
                setTimeout(function () {
                    _this.build(_this.index);
                    _this.s.wrapper.addClass('lg-on');
                });
            }
        } else {

            // Using different namespace for click because click event should not unbind if selector is same object('this')
            _this.$items.on('click.lgcustom', function (event) {

                // For IE8
                try {
                    event.preventDefault();
                    event.preventDefault();
                } catch (er) {
                    event.returnValue = false;
                }

                _this.$el.trigger('onBeforeOpen.lg');
                _this.index = _this.s.index || _this.$items.index(this);
                // prevent accidental double execution
                if (!_this.s.wrapper.hasClass('lg-on')) {
                    _this.build(_this.index);
                    _this.s.wrapper.addClass('lg-on');
                }
            });
        }
        if (_this.s.autostart) {
            _this.$el.trigger('onBeforeOpen.lg');
            _this.index = _this.s.index || 0;
            setTimeout(function () {
                if (!_this.s.wrapper.hasClass('lg-on')) {
                    _this.build(_this.index);
                    _this.s.wrapper.addClass('lg-on');
                }
            }, 0);
        }
        if (_this.s.fullsize) {
            setTimeout(function () {
                _this.s.wrapper.addClass('fullsize');
                $('html').addClass('lg-fullsize-on');
            }, 0);
        }
    };

    Plugin.prototype.build = function (index) {

        var _this = this;

        _this.structure();

        // module constructor
        $.each($.fn.lightGallery.modules, function (key) {
            _this.modules[key] = new $.fn.lightGallery.modules[key](_this.el);
        });

        // initiate slide function
        _this.slide(index, false, false);

        if (_this.s.keyPress) {
            _this.keyPress();
        }

        if (_this.$items.length > 1) {

            _this.arrow();

            setTimeout(function () {
                _this.enableDrag();
                _this.enableSwipe();
            }, 50);

            if (_this.s.mousewheel) {
                _this.mousewheel();
            }
        }

        _this.counter();

        _this.closeGallery();

        _this.$el.trigger('onAfterOpen.lg');

        // Hide controllers if mouse doesn't move for some period
        // _this.$outer.on('mousemove.lg click.lg touchstart.lg', function () {

        //     _this.$outer.removeClass('lg-hide-items');

        //     clearTimeout(_this.hideBartimeout);

        //     // Timeout will be cleared on each slide movement also
        //     _this.hideBartimeout = setTimeout(function () {
        //         _this.$outer.addClass('lg-hide-items');
        //     }, _this.s.hideBarsDelay);
        // });
    };

    Plugin.prototype.structure = function () {
        var list = '';
        var controls = '';
        var i = 0;
        var subHtmlCont = '';
        var descCont = '';
        var template;
        var _this = this;
 
        // Create gallery items
        for (i = 0; i < this.$items.length; i++) {
            list += '<div class="lg-item"><div class="imgldr"><i class="icon-reset rispinner"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span></i></div></div>';
        }
        // Create controlls
        if (this.s.controls && this.$items.length > 1) {
            controls = '<div class="lg-actions">' + '<div data-type="left-click" data-track-label="Left" class="lg-prev lg-icon">' + this.s.prevHtml + '</div>' + '<div data-type="right-click" data-track-label="Right" class="lg-next lg-icon js-lgRight">' + this.s.nextHtml + '</div>' + '</div>';
        }
        if (this.s.appendSubHtmlTo === '.lg-sub-html' && !this.s.version) {
            subHtmlCont = '<div class="lg-sub-html"></div>';
        }

        if (this.s.showDescCont &&  this.s.mobile == true){
            descCont = '<div class="image-prop-info"><div class="image-track"><div class="image-position"><span class="current-index"></span><span class="total-count"></span></div><span class="image-title"></span></div></div>';
        }

        template = '<div class="lg-outer fixed' + this.s.addClass + ' ' + this.s.startClass + '">' + '<div class="lg" style="width:' + this.s.width + '; height:' + this.s.height + '">' + '<div class="lg-inner">' + list + subHtmlCont + '</div>' + controls +'<div class="lg-toolbar group">' + '<span class="lg-close mobile-close lg-icon"></span>' + '<span data-type="gallery-close" data-track-label="Close fullscreen" class="lg-fullscreen close lg-icon"></span>' + '<i class="icon-back-arrow no-desktop lg-close"><span class="path1"></span><span class="path2"></span><span class="path3"></span></i>' + '</div>' + '</div>' + '</div>';
        // '<span class="lg-close lg-icon"></span>' +

        _this.s.wrapper.append(template);
        // this.$outer = $('.lg-outer');
        this.$outer = _this.s.wrapper.find('.lg-outer');
        this.$slide = this.$outer.find('.lg-item');

        if(descCont){
            this.$outer.find(".lg-inner").append(descCont);
        }
        if (this.s.useLeft) {
            this.$outer.addClass('lg-use-left');

            // Set mode lg-slide if use left is true;
            this.s.mode = 'lg-slide';
        } else {
            this.$outer.addClass('lg-use-css3');
        }

        // For fixed height gallery
        _this.setTop();
        $(window).on('resize.lg orientationchange.lg', function () {
            setTimeout(function () {
                _this.setTop();
            }, 100);
        });

        // add class lg-current to remove initial transition
        this.$slide.eq(this.index).addClass('lg-current');

        // add Class for css support and transition mode
        if (this.doCss()) {
            this.$outer.addClass('lg-css3');
        } else {
            this.$outer.addClass('lg-css');

            // Set speed 0 because no animation will happen if browser doesn't support css3
            this.s.speed = 0;
        }

        this.$outer.addClass(this.s.mode);

        if (this.s.enableDrag && this.$items.length > 1) {
            this.$outer.addClass('lg-grab');
        }

        if (this.s.showAfterLoad) {
            this.$outer.addClass('lg-show-after-load');
        }

        if (this.doCss()) {
            var $inner = this.$outer.find('.lg-inner');
            $inner.css('transition-timing-function', this.s.cssEasing);
            $inner.css('transition-duration', this.s.speed + 'ms');
        }

        //$('.lg-backdrop').addClass('in');

        setTimeout(function () {
            _this.$outer.addClass('lg-visible');
        }, this.s.backdropDuration);

        if (this.s.download) {
            this.$outer.find('.lg-toolbar').append('<a id="lg-download" target="_blank" download class="lg-download lg-icon"></a>');
        }

        // Store the current scroll top value to scroll back after closing the gallery..
        this.prevScrollTop = $(window).scrollTop();
    };

    // For fixed height gallery
    Plugin.prototype.setTop = function () {
        if (this.s.height !== '100%') {
            var wH = $(window).height();
            var top = (wH - parseInt(this.s.height, 10)) / 2;
            var $lGallery = this.$outer.find('.lg');
            if (wH >= parseInt(this.s.height, 10)) {
                $lGallery.css('top', top + 'px');
            } else {
                $lGallery.css('top', '0px');
            }
        }
    };

    // Find css3 support
    Plugin.prototype.doCss = function () {
        // check for css animation support
        var support = function support() {
            var transition = ['transition', 'MozTransition', 'WebkitTransition', 'OTransition', 'msTransition', 'KhtmlTransition'];
            var root = document.documentElement;
            var i = 0;
            for (i = 0; i < transition.length; i++) {
                if (transition[i] in root.style) {
                    return true;
                }
            }
        };

        if (support()) {
            return true;
        }

        return false;
    };

    /**
     *  @desc Check the given src is video
     *  @param {String} src
     *  @return {Object} video type
     *  Ex:{ youtube  :  ["//www.youtube.com/watch?v=c0asJgSyxcY", "c0asJgSyxcY"] }
     */
    Plugin.prototype.isVideo = function (src, index) {

        var html, jwHtml;
        if (this.s.dynamic) {
            html = this.s.dynamicEl[index].html;
        } else {
            html = this.$items.eq(index).attr('data-html');
            jwHtml = this.$items.eq(index).attr('data-jwhtml');
        }

        if (!src && html) {
            return {
                html5: true
            };
        } else if (jwHtml) {
            return {
                jwHtml: true
            };
        }

        var youtube = src.match(/\/\/(?:www\.)?youtu(?:\.be|be\.com)\/(?:watch\?v=|embed\/)?([a-z0-9\-\_\%]+)/i);
        var vimeo = src.match(/\/\/(?:www\.)?vimeo.com\/([0-9a-z\-_]+)/i);
        var dailymotion = src.match(/\/\/(?:www\.)?dai.ly\/([0-9a-z\-_]+)/i);

        if (youtube) {
            return {
                youtube: youtube
            };
        } else if (vimeo) {
            return {
                vimeo: vimeo
            };
        } else if (dailymotion) {
            return {
                dailymotion: dailymotion
            };
        }
    };

    /**
     *  @desc Create image counter
     *  Ex: 1/10
     */
    Plugin.prototype.counter = function () {
        if (this.s.counter) {
            this.$outer.find(this.s.appendCounterTo).append('<div class="lg-counter"><span class="lg-counter-current">' + (parseInt(this.index, 10) + 1) + '</span> / <span class="lg-counter-all">' + this.$items.length + '</span></div>');
        }
        $('.lg-counter').addClass('hide');
    };

    /**
     *  @desc add sub-html into the slide
     *  @param {Number} index - index of the slide
     */
    Plugin.prototype.addHtml = function (index) {
        var subHtml = null;
        var subHtmlUrl;
        if (this.s.dynamic) {
            if (this.s.dynamicEl[index].subHtmlUrl) {
                subHtmlUrl = this.s.dynamicEl[index].subHtmlUrl;
            } else {
                subHtml = this.s.dynamicEl[index].subHtml;
            }
        } else {
            if (this.$items.eq(index).attr('data-sub-html-url')) {
                subHtmlUrl = this.$items.eq(index).attr('data-sub-html-url');
            } else {
                subHtml = this.$items.eq(index).attr('data-sub-html');
            }
        }

        if (!subHtmlUrl) {
            if (typeof subHtml !== 'undefined' && subHtml !== null) {

                // get first letter of subhtml
                // if first letter starts with . or # get the html form the jQuery object
                var fL = subHtml.substring(0, 1);
                if (fL === '.' || fL === '#') {
                    subHtml = $(subHtml).html();
                } else {
                    subHtml = subHtml;
                }
            } else {
                subHtml = '';
            }
        }
        if (this.s.appendSubHtmlTo === '.lg-sub-html') {

            if (subHtmlUrl) {
                this.$outer.find(this.s.appendSubHtmlTo).load(subHtmlUrl);
            } else {
                this.$outer.find(this.s.appendSubHtmlTo).html(subHtml);
            }
        } else {

            if (subHtmlUrl) {
                this.$slide.eq(index).load(subHtmlUrl);
            } else {
                this.$slide.eq(index).append(subHtml);
            }
        }
        // Add lg-empty-html class if title doesn't exist
        if (typeof subHtml !== 'undefined' && subHtml !== null) {
            if (subHtml === '') {
                this.$outer.find(this.s.appendSubHtmlTo).addClass('lg-empty-html');
            } else {
                this.$outer.find(this.s.appendSubHtmlTo).removeClass('lg-empty-html');
            }
        }

        this.$el.trigger('onAfterAppendSubHtml.lg', [index]);
    };

    /**
     *  @desc Preload slides
     *  @param {Number} index - index of the slide
     */
    Plugin.prototype.preload = function (index) {
        var i = 1;
        var j = 1;
        for (i = 1; i <= this.s.preload; i++) {
            if (i >= this.$items.length - index) {
                break;
            }

            this.loadContent(index + i, false, 0,true);
        }

        for (j = 1; j <= this.s.preload; j++) {
            if (index - j < 0) {
                break;
            }

            this.loadContent(index - j, false, 0,true);
        }
    };

    /**
     *  @desc Load slide content into slide.
     *  @param {Number} index - index of the slide.
     *  @param {Boolean} rec - if true call loadcontent() function again.
     *  @param {Boolean} delay - delay for adding complete class. it is 0 except first time.
     */
    Plugin.prototype.loadContent = function (index, rec, delay,fromPreload) {

        var _this = this;
        var _hasPoster = false;
        var _$img;
        var _src;
        var _poster;
        var _srcset;
        var _sizes;
        var _html;
        var _alt;
        var _title;
        var fromPreload = fromPreload || false;
        var getResponsiveSrc = function getResponsiveSrc(srcItms) {
            var rsWidth = [];
            var rsSrc = [];
            for (var i = 0; i < srcItms.length; i++) {
                var __src = srcItms[i].split(' ');

                // Manage empty space
                if (__src[0] === '') {
                    __src.splice(0, 1);
                }

                rsSrc.push(__src[0]);
                rsWidth.push(__src[1]);
            }

            var wWidth = $(window).width();
            for (var j = 0; j < rsWidth.length; j++) {
                if (parseInt(rsWidth[j], 10) > wWidth) {
                    _src = rsSrc[j];
                    break;
                }
            }
        };

        if (_this.s.dynamic) {

            if (_this.s.dynamicEl[index].poster) {
                _hasPoster = true;
                _poster = _this.s.dynamicEl[index].poster;
            }

            _html = _this.s.dynamicEl[index].html;
            _src = _this.s.dynamicEl[index].src;

            if (_this.s.dynamicEl[index].responsive) {
                var srcDyItms = _this.s.dynamicEl[index].responsive.split(',');
                getResponsiveSrc(srcDyItms);
            }

            _srcset = _this.s.dynamicEl[index].srcset;
            _sizes = _this.s.dynamicEl[index].sizes;
        } else {

            if (_this.$items.eq(index).attr('data-poster')) {
                _hasPoster = true;
                _poster = _this.$items.eq(index).attr('data-poster');
            }

            _html = _this.$items.eq(index).attr('data-html');
            _src = _this.$items.eq(index).attr('href') || _this.$items.eq(index).attr('data-src');

            if (_this.$items.eq(index).attr('data-responsive')) {
                var srcItms = _this.$items.eq(index).attr('data-responsive').split(',');
                getResponsiveSrc(srcItms);
            }

            _srcset = _this.$items.eq(index).attr('data-srcset');
            _sizes = _this.$items.eq(index).attr('data-sizes');
        }
        _alt = _this.$items.eq(index).attr('data-alt') || '';
        _title = _this.$items.eq(index).attr('data-title') || '';
        //if (_src || _srcset || _sizes || _poster) {

        var iframe = false;
        if (_this.s.dynamic) {
            if (_this.s.dynamicEl[index].iframe) {
                iframe = true;
            }
        } else {
            if (_this.$items.eq(index).attr('data-iframe') === 'true') {
                iframe = true;
            }
        }

        var _isVideo = _this.isVideo(_src, index);
        if (!_this.$slide.eq(index).hasClass('lg-loaded')) {
            if (iframe) {
                _this.$slide.eq(index).prepend('<div class="lg-video-cont" style="max-width:' + _this.s.iframeMaxWidth + '"><div class="lg-video"><iframe class="lg-object" frameborder="0" src="' + _src + '"  allowfullscreen="true"></iframe></div></div>');
            } else if (_hasPoster) {
                var videoClass = '';
                if (_isVideo && _isVideo.youtube) {
                    videoClass = 'lg-has-youtube';
                } else if (_isVideo && _isVideo.vimeo) {
                    videoClass = 'lg-has-vimeo';
                } else {
                    videoClass = 'lg-has-html5';
                }

                _this.$slide.eq(index).prepend('<div class="lg-video-cont ' + videoClass + ' "><div class="lg-video"><span class="lg-video-play"></span><img class="lg-object lg-has-poster" src="' + _poster + '" /></div></div>');
            } else if (_isVideo) {
                _this.$slide.eq(index).prepend('<div class="lg-video-cont "><div class="lg-video"></div></div>');
                _this.$el.trigger('hasVideo.lg', [index, _src, _html]);
                Box.Application.broadcast('galleryClientSideLoaded');
            } else {
                //to skip Lead form slide
                if (_this.$slide.length - 1 !== index){
                    _this.$slide.eq(index).prepend('<div class="lg-img-wrap"><img class="lg-object lg-image" src="' + _src + '"  alt="' + _alt + '" title="' + _title + '" itemprop="image" onload="Box.Application.broadcast(\'galleryClientSideLoaded\')"/></div>');
                }
            }

            _this.$el.trigger('onAferAppendSlide.lg', [index,fromPreload]);

            _$img = _this.$slide.eq(index).find('.lg-object');
            if (_sizes) {
                _$img.attr('sizes', _sizes);
            }

            if (_srcset) {
                _$img.attr('srcset', _srcset);
                try {
                    picturefill({
                        elements: [_$img[0]]
                    });
                } catch (e) {
                    console.error('Make sure you have included Picturefill version 2');
                }
            }
            if (this.s.appendSubHtmlTo !== '.lg-sub-html') {
                _this.addHtml(index);
            }
            _this.$slide.eq(index).addClass('lg-loaded');
        }

        _this.$slide.eq(index).find('.lg-object').on('load.lg error.lg', function () {

            // For first time add some delay for displaying the start animation.
            var _speed = 0;

            // Do not change the delay value because it is required for zoom plugin.
            // If gallery opened from direct url (hash) speed value should be 0
            if (delay && !_this.s.wrapper.hasClass('lg-from-hash')) {
                _speed = delay;
            }

            setTimeout(function () {
                _this.$slide.eq(index).addClass('lg-complete');
                _this.$slide.eq(index).find(".imgldr").addClass("hide");
                _this.$el.trigger('onSlideItemLoad.lg', [index, delay || 0]);
            }, _speed);
        });

        // @todo check load state for html5 videos
        if (_isVideo && _isVideo.html5 && !_hasPoster) {
            _this.$slide.eq(index).addClass('lg-complete');
        }

        if (rec === true) {
            if (!_this.$slide.eq(index).hasClass('lg-complete')) {
                _this.$slide.eq(index).find('.lg-object').on('load.lg error.lg', function () {
                    _this.$slide.eq(index).find(".imgldr").addClass("hide");
                    _this.preload(index);
                });
            } else {
                _this.preload(index);
            }
        }

        //}
    };

    /**
    *   @desc slide function for lightgallery
        ** Slide() gets call on start
        ** ** Set lg.on true once slide() function gets called.
        ** Call loadContent() on slide() function inside setTimeout
        ** ** On first slide we do not want any animation like slide of fade
        ** ** So on first slide( if lg.on if false that is first slide) loadContent() should start loading immediately
        ** ** Else loadContent() should wait for the transition to complete.
        ** ** So set timeout s.speed + 50
    <=> ** loadContent() will load slide content in to the particular slide
        ** ** It has recursion (rec) parameter. if rec === true loadContent() will call preload() function.
        ** ** preload will execute only when the previous slide is fully loaded (images iframe)
        ** ** avoid simultaneous image load
    <=> ** Preload() will check for s.preload value and call loadContent() again accoring to preload value
        ** loadContent()  <====> Preload();
     *   @param {Number} index - index of the slide
    *   @param {Boolean} fromTouch - true if slide function called via touch event or mouse drag
    *   @param {Boolean} fromThumb - true if slide function called via thumbnail click
    */

    function loadImage(startIndex, endIndex){
        $('.lg-group-outer-image .lg-group-list img').each(function(index){
            if(index >= startIndex && index <= endIndex && !$(this).attr('src') ){
                $(this).attr('src', $(this).attr('data-src'));
            }
        })
    }

    function scrollThumbs(index, prevIndex, mobile){
        if (mobile) {
            let element = $('.lg-group-outer-image .lg-group-item').get(index);
            if(element){
                element.scrollIntoView({ inline: "center", behavior: "smooth" });
            }
        } else {
            var listWidth = $('.lg-group-outer-image .lg-group-item').outerWidth(true);
            var groupList = $('.lg-group-outer-image .lg-group-list');
            var carausalWidth = window.innerWidth - 2 * $('.lg-group-outer-image span').outerWidth();
            var thumbsInView = parseInt(carausalWidth / listWidth, 10);
            var maxLeftMargin = $('.lg-group-outer-image .lg-group-list').width() - (thumbsInView * (listWidth));
            var slide = 0;
            var isAnimate = false;
            var diff = (index - 1) * (listWidth);

            if (index == 0 || $('.lg-group-outer-image .lg-group-list').width() < carausalWidth) {
                slide = 0;
                loadImage(index, index + thumbsInView + 3);
            } else if (diff > maxLeftMargin) {
                slide = maxLeftMargin - listWidth;
                loadImage(index - thumbsInView, index + thumbsInView + 3);
            } else {
                slide = diff;
                loadImage(index - 3, index + thumbsInView + 3);
            }
            groupList.animate({
                marginLeft: -slide + 'px'
            }, 500);
        }
    }

    function selectImage(index, mobile){
        $('.lg-group-outer li').removeClass('lg-group-active');
        $('.lg-group-outer').find('[data-index='+ index +']').addClass('lg-group-active')
    }

    function getGroupIndex(index){
        var titleList=$('.lg-group-outer-title').find('li'); 
        var groupIndexArray;
        var groupIndex;
        groupIndexArray = titleList.map(function(ind, title){
            return $(title).data().index;
        })
        for (var i = 0; i < groupIndexArray.length; i++){
            if(parseInt(groupIndexArray[groupIndexArray.length-1]) < index){
                groupIndex = parseInt(groupIndexArray[groupIndexArray.length-1]);
                break;
            }
            if(parseInt(groupIndexArray[i]) > parseInt(index) && i > 0){
                groupIndex = groupIndexArray[i-1];
                break;
            }
            if(parseInt(groupIndexArray[i]) == parseInt(index)){
                groupIndex = index;
                break;
            }
        }
        return groupIndex;
    }

    function updateImagePropCard(totalImageCount, currentIndex, categoryName){
        $('.image-track').find(".image-position .current-index").html(currentIndex);
        $('.image-track').find(".image-position .total-count").html('/ '+totalImageCount + '&nbsp;&nbsp;|');
        $('.image-track').find(".image-title").html(categoryName);
    }

    function getTitleElement(index, mobile){
        var groupIndex = getGroupIndex(index);
        var titleElement  = $('.lg-group-outer-title').find('[data-index='+ groupIndex +']').find('.lg-group-count');
        var count = $('.lg-group-outer-title').find('[data-index='+groupIndex +']').attr('count');
        return {titleElement: titleElement, groupIndex: groupIndex, count: count};
    }

    function changeThumbTitle(index , prevIndex, mobile, version){
        
        var title = getTitleElement(index, mobile);
        var firstVal = parseInt(index) - parseInt(title.groupIndex)  + 1;
        var prevTitle = getTitleElement(prevIndex, mobile);
        prevTitle.titleElement.text(' ( '+ prevTitle.count +' ) ');
        if(!version){
            title.titleElement.text(' ( ' +firstVal+ ' of ' + title.count +' ) ');    
        }
        title.titleElement.closest('li').addClass('lg-group-active');
        var categoryName = title.titleElement.closest('li').data('trackLabel');
        if (version){
            updateImagePropCard(title.count, firstVal, categoryName);
        }
    }

    Plugin.prototype.slide = function (index, fromTouch, fromThumb) {

        var _prevIndex = this.$outer.find('.lg-current').index();
        var _this = this;
        var mobile = this.s.mobile;
        var version = this.s.version;
        
        if(!mobile || (mobile && this.s.version)){
            scrollThumbs(index, _prevIndex, mobile); //thumbs for desktop
        }
        selectImage(index);
        // Prevent if multiple call
        // Required for hsh plugin
        if (_this.lGalleryOn && _prevIndex === index) {
            return;
        }
        var _length = this.$slide.length;
        var _time = _this.lGalleryOn ? this.s.speed : 0;
        var _next = false;
        var _prev = false;

        if (!_this.lgBusy) {

            this.$el.trigger('onBeforeSlide.lg', [_prevIndex, index, fromTouch, fromThumb]);

            _this.lgBusy = true;

            clearTimeout(_this.hideBartimeout);

            // Add title if this.s.appendSubHtmlTo === lg-sub-html
            if (this.s.appendSubHtmlTo === '.lg-sub-html') {

                // wait for slide animation to complete
                setTimeout(function () {
                    _this.addHtml(index);
                }, _time);
            }
            this.arrowDisable(index);
            if (!fromTouch) {

                // remove all transitions
                _this.$outer.addClass('lg-no-trans');

                this.$slide.removeClass('lg-prev-slide lg-next-slide');

                if (index < _prevIndex) {
                    _prev = true;
                    if (index === 0 && _prevIndex === _length - 1 && !fromThumb) {
                        _prev = false;
                        _next = true;
                    }
                } else if (index > _prevIndex) {
                    _next = true;
                    if (index === _length - 1 && _prevIndex === 0 && !fromThumb) {
                        _prev = true;
                        _next = false;
                    }
                }

                if (_prev) {

                    //prevslide
                    this.$slide.eq(index).addClass('lg-prev-slide');
                    this.$slide.eq(_prevIndex).addClass('lg-next-slide');
                } else if (_next) {

                    // next slide
                    this.$slide.eq(index).addClass('lg-next-slide');
                    this.$slide.eq(_prevIndex).addClass('lg-prev-slide');
                }
                // give 50 ms for browser to add/remove class
                setTimeout(function () {
                    _this.$slide.removeClass('lg-current');

                    //_this.$slide.eq(_prevIndex).removeClass('lg-current');
                    _this.$slide.eq(index).addClass('lg-current');

                    // reset all transitions
                    _this.$outer.removeClass('lg-no-trans');
                }, 50);
            } else {

                var touchPrev = index - 1;
                var touchNext = index + 1;

                if (index === 0 && _prevIndex === _length - 1) {

                    // next slide
                    touchNext = 0;
                    touchPrev = _length - 1;
                } else if (index === _length - 1 && _prevIndex === 0) {

                    // prev slide
                    touchNext = 0;
                    touchPrev = _length - 1;
                }

                this.$slide.removeClass('lg-prev-slide lg-current lg-next-slide');
                _this.$slide.eq(touchPrev).addClass('lg-prev-slide');
                _this.$slide.eq(touchNext).addClass('lg-next-slide');
                _this.$slide.eq(index).addClass('lg-current');
            }

            if (_this.lGalleryOn) {
                setTimeout(function () {
                    _this.loadContent(index, true, 0);
                }, this.s.speed + 50);

                setTimeout(function () {
                    _this.lgBusy = false;
                    _this.$el.trigger('onAfterSlide.lg', [_prevIndex, index, fromTouch, fromThumb]);
                }, this.s.speed + 50);
            } else {
                _this.loadContent(index, true, _this.s.backdropDuration);

                _this.lgBusy = false;
                _this.$el.trigger('onAfterSlide.lg', [_prevIndex, index, fromTouch, fromThumb]);
            }

            // To set index to given index
            _this.index = index;
            if (this.s.download) {
                var _src;
                if (_this.s.dynamic) {
                    _src = _this.s.dynamicEl[index].downloadUrl || _this.s.dynamicEl[index].src;
                } else {
                    _src = _this.$items.eq(index).attr('data-download-url') || _this.$items.eq(index).attr('href') || _this.$items.eq(index).attr('data-src');
                }

                $('#lg-download').attr('href', _src);
            }

            _this.lGalleryOn = true;

            if (this.s.counter) {
                // $('#lg-counter-current').text(index + 1);
                this.$outer.find('.lg-counter-current').text(index + 1);
            }
        }
        changeThumbTitle(index, _prevIndex, mobile, version);
    };

    /**
     *  @desc Go to next slide
     *  @param {Boolean} fromTouch - true if slide function called via touch event
     */
    Plugin.prototype.goToNextSlide = function (fromTouch) {
        var _this = this;
        if (!_this.lgBusy) {
            if (_this.index + 1 < _this.$slide.length) {
                _this.index++;
                _this.$el.trigger('onBeforeNextSlide.lg', [_this.index]);
                _this.slide(_this.index, fromTouch, false);
            } else {
                if (_this.s.loop) {
                    _this.index = 0;
                    _this.$el.trigger('onBeforeNextSlide.lg', [_this.index]);
                    _this.slide(_this.index, fromTouch, false);
                } else if (_this.s.slideEndAnimatoin) {
                    _this.$outer.addClass('lg-right-end');
                    setTimeout(function () {
                        _this.$outer.removeClass('lg-right-end');
                    }, 400);
                }
            }
        }
    };

    /**
     *  @desc Go to previous slide
     *  @param {Boolean} fromTouch - true if slide function called via touch event
     */
    Plugin.prototype.goToPrevSlide = function (fromTouch) {
        var _this = this;
        if (!_this.lgBusy) {
            if (_this.index > 0) {
                _this.index--;
                _this.$el.trigger('onBeforePrevSlide.lg', [_this.index, fromTouch]);
                _this.slide(_this.index, fromTouch, false);
            } else {
                if (_this.s.loop) {
                    _this.index = _this.$items.length - 1;
                    _this.$el.trigger('onBeforePrevSlide.lg', [_this.index, fromTouch]);
                    _this.slide(_this.index, fromTouch, false);
                } else if (_this.s.slideEndAnimatoin) {
                    _this.$outer.addClass('lg-left-end');
                    setTimeout(function () {
                        _this.$outer.removeClass('lg-left-end');
                    }, 400);
                }
            }
        }
    };

    Plugin.prototype.keyPress = function () {
        var _this = this;

        $(window).on('keyup.lg', function (e) {
            if (_this.$items.length > 1 && (_this.s.fullsize || _this.$outer.hasClass('lg-fullscreen-on'))) {
                if (_this.$items.length > 1) {
                    if (e.keyCode === 37) {
                        e.preventDefault();
                        _this.goToPrevSlide();
                    }

                    if (e.keyCode === 39) {
                        e.preventDefault();
                        _this.goToNextSlide();
                    }
                }
            }
        });

        function _handleESC(){
            if (!_this.$outer.hasClass('lg-thumb-open')) {
                _this.destroy();
            } else {
                _this.$outer.removeClass('lg-thumb-open');
            }
        }

        $(window).on('keydown.lg', function (e) {
            if (_this.s.escKey === true && e.keyCode === 27) {
                e.preventDefault();
                e.stopPropagation();
                _handleESC();
            }
        });
    };

    Plugin.prototype.arrow = function () {
        var _this = this;
        this.$outer.find('.lg-prev').on('click.lg', function (e) {
            _this.goToPrevSlide();
        });

        this.$outer.find('.lg-next').on('click.lg', function (e) {
            _this.goToNextSlide();
        });
    };

    Plugin.prototype.arrowDisable = function (index) {

        // Disable arrows if s.hideControlOnEnd is true
        if (!this.s.loop && this.s.hideControlOnEnd) {
            if (index + 1 < this.$slide.length) {
                this.$outer.find('.lg-next').removeAttr('disabled').removeClass('disabled');
            } else {
                this.$outer.find('.lg-next').attr('disabled', 'disabled').addClass('disabled');
            }

            if (index > 0) {
                this.$outer.find('.lg-prev').removeAttr('disabled').removeClass('disabled');
            } else {
                this.$outer.find('.lg-prev').attr('disabled', 'disabled').addClass('disabled');
            }
        }
    };

    Plugin.prototype.setTranslate = function ($el, xValue, yValue) {
        // jQuery supports Automatic CSS prefixing since jQuery 1.8.0
        if (this.s.useLeft) {
            $el.css('left', xValue);
        } else {
            $el.css({
                transform: 'translate3d(' + xValue + 'px, ' + yValue + 'px, 0px)'
            });
        }
    };

    Plugin.prototype.touchMove = function (startCoords, endCoords) {

        var distance = endCoords - startCoords;

        // reset opacity and transition duration
        this.$outer.addClass('lg-dragging');

        // move current slide
        this.setTranslate(this.$slide.eq(this.index), distance, 0);

        // move next and prev slide with current slide
        this.setTranslate($('.lg-prev-slide'), -this.$slide.eq(this.index).width() + distance, 0);
        this.setTranslate($('.lg-next-slide'), this.$slide.eq(this.index).width() + distance, 0);
    };

    Plugin.prototype.touchEnd = function (distance) {
        var _this = this;

        // keep slide animation for any mode while dragg/swipe
        if (_this.s.mode !== 'lg-slide') {
            _this.$outer.addClass('lg-slide');
        }

        this.$slide.not('.lg-current, .lg-prev-slide, .lg-next-slide').css('opacity', '0');

        // set transition duration
        setTimeout(function () {
            _this.$outer.removeClass('lg-dragging');
            if (distance < 0 && Math.abs(distance) > _this.s.swipeThreshold) {
                _this.goToNextSlide(true);
            } else if (distance > 0 && Math.abs(distance) > _this.s.swipeThreshold) {
                _this.goToPrevSlide(true);
            } else if (Math.abs(distance) < 5) {

                // Trigger click if distance is less than 5 pix
                _this.$el.trigger('onSlideClick.lg');
            }

            _this.$slide.removeAttr('style');
        });

        // remove slide class once drag/swipe is completed if mode is not slide
        setTimeout(function () {
            if (!_this.$outer.hasClass('lg-dragging') && _this.s.mode !== 'lg-slide') {
                _this.$outer.removeClass('lg-slide');
            }
        }, _this.s.speed + 100);
    };

    Plugin.prototype.enableSwipe = function () {
        var _this = this;
        var startCoords = 0;
        var endCoords = 0;
        var isMoved = false;

        if (_this.s.enableSwipe && _this.isTouch && _this.doCss()) {

            _this.$slide.on('touchstart.lg', function (e) {
                if (!_this.$outer.hasClass('lg-prevent') && !_this.lgBusy) {
                    e.preventDefault();
                    _this.manageSwipeClass();
                    startCoords = e.originalEvent.targetTouches[0].pageX;
                }
            });

            _this.$slide.on('touchmove.lg', function (e) {
                if (!_this.$outer.hasClass('lg-prevent')) {
                    e.preventDefault();
                    endCoords = e.originalEvent.targetTouches[0].pageX;
                    _this.touchMove(startCoords, endCoords);
                    isMoved = true;
                }
            });

            _this.$slide.on('touchend.lg', function () {
                if (!_this.$outer.hasClass('lg-prevent')) {
                    if (isMoved) {
                        isMoved = false;
                        _this.touchEnd(endCoords - startCoords);
                    } else {
                        _this.$el.trigger('onSlideClick.lg');
                    }
                }
            });
        }
    };

    Plugin.prototype.enableDrag = function () {
        var _this = this;
        var startCoords = 0;
        var endCoords = 0;
        var isDraging = false;
        var isMoved = false;
        if (_this.s.enableDrag && !_this.isTouch && _this.doCss()) {
            _this.$slide.on('mousedown.lg', function (e) {
                // execute only on .lg-object
                if (!_this.$outer.hasClass('lg-prevent')) {
                    if ($(e.target).hasClass('lg-object') || $(e.target).hasClass('lg-video-play')) {
                        e.preventDefault();

                        if (!_this.lgBusy) {
                            _this.manageSwipeClass();
                            startCoords = e.pageX;
                            isDraging = true;

                            // ** Fix for webkit cursor issue https://code.google.com/p/chromium/issues/detail?id=26723
                            _this.$outer.scrollLeft += 1;
                            _this.$outer.scrollLeft -= 1;

                            // *

                            _this.$outer.removeClass('lg-grab').addClass('lg-grabbing');

                            _this.$el.trigger('onDragstart.lg');
                        }
                    }
                }
            });

            _this.$el.on('mousemove.lg', function (e) {
                if (isDraging) {
                    isMoved = true;
                    endCoords = e.pageX;
                    _this.touchMove(startCoords, endCoords);
                    _this.$el.trigger('onDragmove.lg');
                }
            });

            _this.$el.on('mouseup.lg', function (e) {
                e.stopImmediatePropagation();
                if (isMoved) {
                    isMoved = false;
                    _this.touchEnd(endCoords - startCoords);
                    _this.$el.trigger('onDragend.lg');
                } else if ($(e.target).hasClass('lg-object') || $(e.target).hasClass('lg-video-play')) {
                    _this.$el.trigger('onSlideClick.lg');
                }

                // Prevent execution on click
                if (isDraging) {
                    isDraging = false;
                    _this.$outer.removeClass('lg-grabbing').addClass('lg-grab');
                }
            });
        }
    };

    Plugin.prototype.manageSwipeClass = function () {
        var touchNext = this.index + 1;
        var touchPrev = this.index - 1;
        var length = this.$slide.length;
        if (this.s.loop) {
            if (this.index === 0) {
                touchPrev = length - 1;
            } else if (this.index === length - 1) {
                touchNext = 0;
            }
        }

        this.$slide.removeClass('lg-next-slide lg-prev-slide');
        if (touchPrev > -1) {
            this.$slide.eq(touchPrev).addClass('lg-prev-slide');
        }

        this.$slide.eq(touchNext).addClass('lg-next-slide');
    };

    Plugin.prototype.mousewheel = function () {
        var _this = this;
        _this.$outer.on('mousewheel.lg', function (e) {

            if (!e.deltaY) {
                return;
            }

            if (e.deltaY > 0) {
                _this.goToPrevSlide();
            } else {
                _this.goToNextSlide();
            }

            e.preventDefault();
        });
    };

    Plugin.prototype.closeGallery = function () {

        var _this = this;
        var mousedown = false;
        this.$outer.find('.lg-close').on('click.lg', function () {
            _this.destroy();
        });

        if (_this.s.closable) {

            // If you drag the slide and release outside gallery gets close on chrome
            // for preventing this check mousedown and mouseup happened on .lg-item or lg-outer
            _this.$outer.on('mousedown.lg', function (e) {

                if ($(e.target).is('.lg-outer') || $(e.target).is('.lg-item ') || $(e.target).is('.lg-img-wrap')) {
                    mousedown = true;
                } else {
                    mousedown = false;
                }
            });

            _this.$outer.on('mouseup.lg', function (e) {

                if ($(e.target).is('.lg-outer') || $(e.target).is('.lg-item ') || $(e.target).is('.lg-img-wrap') && mousedown) {
                    if (!_this.$outer.hasClass('lg-dragging')) {
                        _this.destroy();
                    }
                }
            });
        }
    };

    Plugin.prototype.destroy = function (d) {

        var _this = this;

        _this.$el.trigger('onBeforeClose.lg');
        $(window).scrollTop(_this.prevScrollTop);

        /**
         * if d is false or undefined destroy will only close the gallery
         * plugins instance remains with the element
          * if d is true destroy will completely remove the plugin
         */

        if (d) {
            if (!_this.s.dynamic) {
                // only when not using dynamic mode is $items a jquery collection
                this.$items.off('click.lg click.lgcustom');
            }
            $.removeData(_this.el, 'lightGallery');
        }

        // Unbind all events added by lightGallery
        this.$el.off('.lg.tm');

        // Distroy all lightGallery modules
        $.each($.fn.lightGallery.modules, function (key) {
            if (_this.modules[key]) {
                // _this.modules[key].destroy();
            }
        });

        this.lGalleryOn = false;

        clearTimeout(_this.hideBartimeout);
        this.hideBartimeout = false;
        // $(window).off('.lg');
        _this.s.wrapper.removeClass('lg-on lg-from-hash fullsize');
        $('html').removeClass('lg-fullsize-on');

        if (_this.$outer) {
            _this.$outer.removeClass('lg-visible');
        }

        _this.s.wrapper.find('.lg-backdrop').removeClass('in');

        setTimeout(function () {
            if (_this.$outer) {
                _this.$outer.remove();
            }

            _this.s.wrapper.find('.lg-backdrop').remove();

            _this.$el.trigger('onCloseAfter.lg');
        }, _this.s.backdropDuration + 50);
    };

    $.fn.lightGallery = function (options) {
        return this.each(function () {
            if (!$.data(this, 'lightGallery')) {
                $.data(this, 'lightGallery', new Plugin(this, options));
            } else {
                try {
                    $(this).data('lightGallery').init();
                } catch (err) {
                    console.error('lightGallery has not initiated properly');
                }
            }
        });
    };

    $.fn.lightGallery.modules = {};
})(jQuery, window, document);

(function ($, window, document, undefined) {

    'use strict';

    var defaults = {
        fullScreen: true
    };

    var Fullscreen = function Fullscreen(element) {

        // get lightGallery core plugin data
        this.core = $(element).data('lightGallery');

        this.$el = $(element);

        // extend module defalut settings with lightGallery core settings
        this.core.s = $.extend({}, defaults, this.core.s);

        this.init();

        return this;
    };

    Fullscreen.prototype.init = function () {
        var fullScreen = '';
        if (this.core.s.fullScreen) {

            // check for fullscreen browser support
            if (!document.fullscreenEnabled && !document.webkitFullscreenEnabled && !document.mozFullScreenEnabled && !document.msFullscreenEnabled) {
                return;
            } else {
                fullScreen = '<span data-type="gallery-full-screen" title="full screen" data-track-label="Full Screen" class="lg-fullscreen lg-icon"></span>';
                //this.core.$outer.find('.lg-toolbar').append(fullScreen);
                this.core.$outer.find('.lg-inner').append(fullScreen);
                this.fullScreen();
            }
        }
    };

    Fullscreen.prototype.reuestFullscreen = function () {
        if (this.core.s.fullsize) return false;
        var instanceId = this.$el.attr("id"),
            el = document.getElementById(instanceId);
        if (el.requestFullscreen) {
            el.requestFullscreen();
        } else if (el.msRequestFullscreen) {
            el.msRequestFullscreen();
        } else if (el.mozRequestFullScreen) {
            el.mozRequestFullScreen();
        } else if (el.webkitRequestFullscreen) {
            el.webkitRequestFullscreen();
        }
    };

    Fullscreen.prototype.exitFullscreen = function () {
        if (document.exitFullscreen) {
            document.exitFullscreen();
        } else if (document.msExitFullscreen) {
            document.msExitFullscreen();
        } else if (document.mozCancelFullScreen) {
            document.mozCancelFullScreen();
        } else if (document.webkitExitFullscreen) {
            document.webkitExitFullscreen();
        }
    };

    // https://developer.mozilla.org/en-US/docs/Web/Guide/API/DOM/Using_full_screen_mode
    Fullscreen.prototype.fullScreen = function () {
        var _this = this;

        $(document).on('fullscreenchange.lg webkitfullscreenchange.lg mozfullscreenchange.lg MSFullscreenChange.lg', function () {
            if (_this.core.s.fullsize) return false;
            _this.core.$outer.toggleClass('lg-fullscreen-on');
            _this.core.s.wrapper.toggleClass('lg-mobile-fullscreen-on');
            _this.core.$outer.find(".lg-toolbar.group").toggleClass('hide');
            if (!_this.core.$outer.hasClass('lg-fullscreen-on')) {
                _this.core.$outer.addClass('fixed');
            }
        });

        var fullScreenHandler = function fullScreenHandler() {
            if (_this.core.s.fullsize) return false;
            if (!document.fullscreenElement && !document.mozFullScreenElement && !document.webkitFullscreenElement && !document.msFullscreenElement) {
                _this.reuestFullscreen();
                _this.core.$outer.removeClass('fixed');
            } else {
                _this.exitFullscreen();
                _this.core.$outer.addClass('fixed');
            }
        };
        this.core.$outer.find('.lg-fullscreen').on('click.lg', function (){
            fullScreenHandler();
        })
    };

    Fullscreen.prototype.destroy = function () {

        // exit from fullscreen if activated
        this.exitFullscreen();

        $(document).off('fullscreenchange.lg webkitfullscreenchange.lg mozfullscreenchange.lg MSFullscreenChange.lg');
    };

    $.fn.lightGallery.modules.fullscreen = Fullscreen;
})(jQuery, window, document);

(function ($, window, document, undefined) {

    'use strict';

    var defaults = {
        videoMaxWidth: '100%',
        youtubePlayerParams: false,
        vimeoPlayerParams: false,
        dailymotionPlayerParams: false,
        videojs: false
    };

    var Video = function Video(element) {

        this.core = $(element).data('lightGallery');

        this.$el = $(element);
        this.core.s = $.extend({}, defaults, this.core.s);
        this.videoLoaded = false;

        this.init();

        return this;
    };

    Video.prototype.init = function () {
        var _this = this;

        // Event triggered when video url found without poster
        _this.core.$el.on('hasVideo.lg.tm', function (event, index, src, html) {
            _this.core.$slide.eq(index).find('.lg-video').append(_this.loadVideo(src, 'lg-object', true, index, html));
            if (html) {
                if (_this.core.s.videojs) {
                    try {
                        videojs(_this.core.$slide.eq(index).find('.lg-html5').get(0), {}, function () {
                            if (!_this.videoLoaded) {
                                this.play();
                            }
                        });
                    } catch (e) {
                        console.error('Make sure you have included videojs');
                    }
                } else {
                    _this.core.$slide.eq(index).find('.lg-html5').get(0).play();
                }
            }
        });

        // Set max width for video
        _this.core.$el.on('onAferAppendSlide.lg.tm', function (event, index) {
            _this.core.$slide.eq(index).find('.lg-video-cont').css('max-width', _this.core.s.videoMaxWidth);
            _this.videoLoaded = true;
        });

        var loadOnClick = function loadOnClick($el) {
            // check slide has poster
            if ($el.find('.lg-object').hasClass('lg-has-poster') && $el.find('.lg-object').is(':visible')) {

                // chack already video element present
                if (!$el.hasClass('lg-has-video')) {

                    $el.addClass('lg-video-palying lg-has-video');

                    var _src;
                    var _html;
                    var _loadVideo = function _loadVideo(_src, _html) {

                        $el.find('.lg-video').append(_this.loadVideo(_src, '', false, _this.core.index, _html));

                        if (_html) {
                            if (_this.core.s.videojs) {
                                try {
                                    videojs(_this.core.$slide.eq(_this.core.index).find('.lg-html5').get(0), {}, function () {
                                        this.play();
                                    });
                                } catch (e) {
                                    console.error('Make sure you have included videojs');
                                }
                            } else {
                                _this.core.$slide.eq(_this.core.index).find('.lg-html5').get(0).play();
                            }
                        }
                    };

                    if (_this.core.s.dynamic) {

                        _src = _this.core.s.dynamicEl[_this.core.index].src;
                        _html = _this.core.s.dynamicEl[_this.core.index].html;

                        _loadVideo(_src, _html);
                    } else {

                        _src = _this.core.$items.eq(_this.core.index).attr('href') || _this.core.$items.eq(_this.core.index).attr('data-src');
                        _html = _this.core.$items.eq(_this.core.index).attr('data-html');

                        _loadVideo(_src, _html);
                    }

                    var $tempImg = $el.find('.lg-object');
                    $el.find('.lg-video').append($tempImg);

                    // @todo loading icon for html5 videos also
                    // for showing the loading indicator while loading video
                    if (!$el.find('.lg-video-object').hasClass('lg-html5')) {
                        $el.removeClass('lg-complete');
                        $el.find('.lg-video-object').on('load.lg error.lg', function () {
                            $el.addClass('lg-complete');
                        });
                    }
                } else {

                    var youtubePlayer = $el.find('.lg-youtube').get(0);
                    var vimeoPlayer = $el.find('.lg-vimeo').get(0);
                    var dailymotionPlayer = $el.find('.lg-dailymotion').get(0);
                    var html5Player = $el.find('.lg-html5').get(0);
                    if (youtubePlayer) {
                        youtubePlayer.contentWindow.postMessage('{"event":"command","func":"playVideo","args":""}', '*');
                    } else if (vimeoPlayer) {
                        try {
                            $f(vimeoPlayer).api('play');
                        } catch (e) {
                            console.error('Make sure you have included froogaloop2 js');
                        }
                    } else if (dailymotionPlayer) {
                        dailymotionPlayer.contentWindow.postMessage('play', '*');
                    } else if (html5Player) {
                        if (_this.core.s.videojs) {
                            try {
                                videojs(html5Player).play();
                            } catch (e) {
                                console.error('Make sure you have included videojs');
                            }
                        } else {
                            html5Player.play();
                        }
                    }

                    $el.addClass('lg-video-palying');
                }
            }
        };

        if (_this.core.doCss() && _this.core.$items.length > 1 && (_this.core.s.enableSwipe && _this.core.isTouch || _this.core.s.enableDrag && !_this.core.isTouch)) {
            _this.core.$el.on('onSlideClick.lg.tm', function () {
                var $el = _this.core.$slide.eq(_this.core.index);
                loadOnClick($el);
                if (!_this.core.$outer.hasClass('lg-fullscreen-on')) {
                    _this.core.modules.fullscreen.reuestFullscreen();
                }
            });
        } else {

            // For IE 9 and bellow
            _this.core.$slide.on('click.lg', function () {
                loadOnClick($(this));
            });
        }

        _this.core.$el.on('onBeforeSlide.lg.tm', function (event, prevIndex) {

            var $videoSlide = _this.core.$slide.eq(prevIndex);
            var youtubePlayer = $videoSlide.find('.lg-youtube').get(0);
            var vimeoPlayer = $videoSlide.find('.lg-vimeo').get(0);
            var dailymotionPlayer = $videoSlide.find('.lg-dailymotion').get(0);
            var html5Player = $videoSlide.find('.lg-html5').get(0);
            if (youtubePlayer) {
                youtubePlayer.contentWindow.postMessage('{"event":"command","func":"pauseVideo","args":""}', '*');
            } else if (vimeoPlayer) {
                try {
                    $f(vimeoPlayer).api('pause');
                } catch (e) {
                    console.error('Make sure you have included froogaloop2 js');
                }
            } else if (dailymotionPlayer) {
                dailymotionPlayer.contentWindow.postMessage('pause', '*');
            } else if (html5Player) {
                if (_this.core.s.videojs) {
                    try {
                        videojs(html5Player).pause();
                    } catch (e) {
                        console.error('Make sure you have included videojs');
                    }
                } else {
                    html5Player.pause();
                }
            }

            //$videoSlide.addClass('lg-complete');
        });

        _this.core.$el.on('onAfterSlide.lg.tm', function (event, prevIndex) {
            _this.core.$slide.eq(prevIndex).removeClass('lg-video-palying');
        });
    };

    Video.prototype.loadVideo = function (src, addClass, noposter, index, html) {
        var video = '';
        var autoplay = 1;
        var a = '';
        var isVideo = this.core.isVideo(src, index) || {};

        // Enable autoplay for first video if poster doesn't exist
        if (noposter) {
            if (this.videoLoaded) {
                autoplay = 0;
            } else {
                autoplay = 1;
            }
        }

        if (isVideo.youtube) {

            a = '?wmode=opaque&autoplay=' + autoplay + '&enablejsapi=1';
            if (this.core.s.youtubePlayerParams) {
                a = a + '&' + $.param(this.core.s.youtubePlayerParams);
            }

            video = '<iframe class="lg-video-object lg-youtube ' + addClass + '" width="560" height="315" src="//www.youtube.com/embed/' + isVideo.youtube[1] + a + '" frameborder="0" allowfullscreen></iframe>';
        } else if (isVideo.vimeo) {

            a = '?autoplay=' + autoplay + '&api=1';
            if (this.core.s.vimeoPlayerParams) {
                a = a + '&' + $.param(this.core.s.vimeoPlayerParams);
            }

            video = '<iframe class="lg-video-object lg-vimeo ' + addClass + '" width="560" height="315"  src="http://player.vimeo.com/video/' + isVideo.vimeo[1] + a + '" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>';
        } else if (isVideo.dailymotion) {

            a = '?wmode=opaque&autoplay=' + autoplay + '&api=postMessage';
            if (this.core.s.dailymotionPlayerParams) {
                a = a + '&' + $.param(this.core.s.dailymotionPlayerParams);
            }

            video = '<iframe class="lg-video-object lg-dailymotion ' + addClass + '" width="560" height="315" src="//www.dailymotion.com/embed/video/' + isVideo.dailymotion[1] + a + '" frameborder="0" allowfullscreen></iframe>';
        } else if (isVideo.html5) {
            var fL = html.substring(0, 1);
            if (fL === '.' || fL === '#') {
                html = $(html).html();
            }

            video = html;
        }

        return video;
    };

    Video.prototype.destroy = function () {
        this.videoLoaded = false;
    };

    $.fn.lightGallery.modules.video = Video;
})(jQuery, window, document);

(function ($, window, document, undefined) {

    'use strict';

    var defaults = {
        group: false
    };

    var group = function group(element) {

        this.core = $(element).data('lightGallery');

        this.$el = $(element);
        this.core.s = $.extend({}, defaults, this.core.s);
        if (this.core.s.group && this.core.$items.length > 0) {
            this.init();
        }

        return this;
    };

    group.prototype.init = function () {
        var _this = this;
        var groupList = '';
        var $groupCont;
        var $groupOuter;
        var timeout;
        var groupIndex = 0;
        var groupIndexArray = [];
        var groupCount = 0;
        var galleryData = this.core.s.galleryData;
        //create div for thumb and its title
        var div, clas;
        var boxDiv;
        var mobile = this.core.s.mobile;
        _this.core.$outer.find('.lg').append('<div class="lg-group-outer lg-group-outer-title title jcarousel" data-window="gallery_tabs"><div id="gallery_tabs" class="ulwrap"><ul class="lg-group-list" data-type="list-container"></ul><span class="crausal-next list-next js-next hide1024nbelow" data-type="next-btn"><i class="icon icon-chevron-down"></i></span><span class="crausal-prev list-prev js-prev hide1024nbelow" data-type="back-btn"><i class="icon icon-chevron-down"></i></span></div></div>');        
        
        $.each(_this.core.s.groupEl, function (key, value) {
            groupList += '<li class="lg-group-item" data-type="list-element" count = '+ value +' data-track-label="' + key + '" data-index="' + groupIndex + '"> <a href="javascript:void(0);" title="' + key + ' (' + value + ') ' + '">' + key + '<span class="lg-group-count"> ( ' + value + ' ) </span></a></li>';
            groupIndex += parseInt(value, 10);
            groupIndexArray.push(groupIndex);
            groupCount++;
        });
        $groupOuter = _this.core.$outer.find('.lg-group-outer .lg-group-list');
        $groupOuter.html(groupList);
        $groupCont = _this.core.$outer.find('.lg-group-item');
        $groupCont.on('click.lg', function () {
            var _$this = $(this),
                index = _$this.data("index");
            _this.core.slide(index, false, false);
        });
        _this.core.$el.on('onBeforeSlide.lg.tm', function (e, prevIndex, index) {
            var activeIndex;
            
            $.each(groupIndexArray, function (gindex) {
                if(!mobile){
                    if(index < groupIndexArray[gindex].groupIndex) {
                        activeIndex = gindex;
                        return false;
                    }
                }else{
                    if(index < groupIndexArray[gindex]) {
                        activeIndex = gindex;
                        return false;
                    }
                }
            });
            $groupCont.removeClass('lg-group-active');
            $groupCont.eq(activeIndex).addClass('lg-group-active');
        });

        //all related image carousel
        if(this.core.s.version == 'v3_mobile'){
            $('.lg-toolbar').remove();
        }
        if(this.core.s.mobile == false || (this.core.s.version == 'v2_mobile')){
            groupList = '';
            groupIndex = 0;
            groupIndexArray = [];
            var $imageGroupCont = 0;

            // if(this.core.s.mobile == false){
                $('.lg-toolbar').remove();    
            // }
            div = document.createElement('div');
            clas = document.createAttribute('class');
            clas.value = 'thumb-title';
            div.setAttributeNode(clas);
            _this.core.$outer.find('.lg').append(div);

            _this.core.$outer.find('.thumb-title').append('<div class="lg-group-outer lg-group-outer-image jcarousel" data-window="gallery_tabs"><div id="gallery_tabs" class="ulwrap  image-gallery"><ul class="lg-group-list" data-type="list-container"></ul><span class="crausal-next list-next js-next hide1024nbelow" data-type="next-btn"><i class="icon icon-chevron-down"></i></span><span class="crausal-prev list-prev js-prev hide1024nbelow" data-type="back-btn"><i class="icon icon-chevron-down"></i></span></div></div>');        
            $.each(_this.core.s.galleryData.data, function (key, value) {
                groupList += '<li class="lg-group-item" data-type="list-element" data-track-label="" data-index="" data-groupIndex=""> <a href="javascript:void(0);" title=""> <img data-src='+ value.thumb +'> </a></li> $';
            });
            $.each(_this.core.s.groupEl, function (key, value) {
                groupIndex += parseInt(value, 10);
                var title = key;
                groupIndexArray.push({groupIndex: groupIndex, title:title});
            });
            var list = groupList.split('$'),
                imageIndex = 0;
                list.length = list.length-1;
                groupList = '';
            for(var i = 0; i < groupIndexArray.length; i++){
                var count;
                var startIndex;
                if(i == 0 ){
                    startIndex = 0;
                    count = groupIndexArray[i].groupIndex;
                }else {
                    startIndex = groupIndexArray[i-1].groupIndex;
                    count = groupIndexArray[i].groupIndex - groupIndexArray[i-1].groupIndex;
                }
                for(var j = startIndex; j < startIndex + count; j++){
                    list[j] = list[j].replace('title=""', 'title="'+groupIndexArray[i].title+'"').replace('data-index=""', 'data-index="'+imageIndex+'"').replace('data-groupIndex=""', 'data-groupIndex="'+startIndex+'"').replace('data-track-label=""', 'data-track-label="'+groupIndexArray[i].title+'"');
                    groupList += list[j];
                    imageIndex++;
                }
            }
            $groupOuter = _this.core.$outer.find('.lg-group-outer-image .lg-group-list');
            $groupOuter.html(groupList);
            // $('.lg-group-outer-image').css('height', 70);
            $('.thumb-title .lg-group-outer').css('position', 'relative');
            $imageGroupCont = _this.core.$outer.find('.lg-group-outer-image .lg-group-item');
            $imageGroupCont.on('click.lg', function () {
                var _$this = $(this),
                index = _$this.data("index");
                _this.core.slide(index, false, true);
            });
            var _onmousout;
            if(this.core.s.mobile == false){
                $('.thumb-title , .lg-group-outer-title').hover( function () {
                    clearTimeout(_onmousout);
                    $('.thumb-title').css('bottom','35px'); 
                }, function(){
                   _onmousout = setTimeout(function(){
                        $('.thumb-title').css('bottom','-70px');
                    }, 2000);
                });
            } else {
                $('.thumb-title').css('bottom','10px');
            }
        }
    };

    group.prototype.destroy = function () {};

    $.fn.lightGallery.modules.group = group;
})(jQuery, window, document);

(function($, window, document, undefined) {

    'use strict';

    var defaults = {
        hash: true
    };

    var Hash = function(element) {

        this.core = $(element).data('lightGallery');

        this.core.s = $.extend({}, defaults, this.core.s);

        if (this.core.s.hash) {
            this.oldHash = window.location.hash;
            this.init();
        }

        return this;
    };

    Hash.prototype.init = function() {
        var _this = this;
        var _hash;

        window.location.hash = 'lg=' + _this.core.s.galleryId;

        // Change hash value on after each slide transition
        /* _this.core.$el.on('onAfterSlide.lg.tm', function(event, prevIndex, index) {
            window.location.hash = 'lg=' + _this.core.s.galleryId + '&slide=' + index;
            window.location.hash = 'lg=' + _this.core.s.galleryId; //Commenting because no need to update the hash, interfereing with back btn
        }); */

        // Listen hash change and change the slide according to slide value

        //No need to watch hash to close or slide the images
        window.addEventListener('hashchange', function() {
            _hash = window.location.hash;
            var _idx = parseInt(_hash.split('&slide=')[1], 10);
            // it galleryId doesn't exist in the url close the gallery
            if ((_hash.indexOf('lg=' + _this.core.s.galleryId) > -1)) {
                if(!isNaN(_idx)){
                    _this.core.slide(_idx);
                }
            } else if (_this.core.lGalleryOn) {
                _this.core.destroy();
            }
        });
    };

    Hash.prototype.destroy = function() {

        if(!this.core.s.hash){
            return;
        }
        // Reset to old hash value
        if (this.oldHash && this.oldHash.indexOf('lg=' + this.core.s.galleryId) < 0) {
            if (history.replaceState) {
                history.replaceState(null, null, this.oldHash);
            } else {
                window.location.hash = this.oldHash;
            }
        } else {
            if(window.location.hash.indexOf('lg=' + this.core.s.galleryId) > -1){
                window.history.back();
            } 
            // if (history.pushState) {
            //     history.pushState('', document.title, window.location.pathname + window.location.search);
            // } else {
            //     window.location.hash = '';
            // }
        }
       this.core.$el.off('.lg.hash');
    };

    $.fn.lightGallery.modules.hash = Hash;

})(jQuery, window, document);

(function($, window, document, undefined) {

    'use strict';

    var getUseLeft = function() {
        var useLeft = false;
        var isChrome = navigator.userAgent.match(/Chrom(e|ium)\/([0-9]+)\./);
        if (isChrome && parseInt(isChrome[2], 10) < 54) {
            useLeft = true;
        }

        return useLeft;
    };

    var defaults = {
        scale: 1,
        zoom: true,
        actualSize: true,
        enableZoomAfter: 300,
        useLeftForZoom: getUseLeft()
    };

    var zoomoutTime,zoominTime;
    zoomoutTime = zoominTime = false;

    var Zoom = function(element) {

        this.core = $(element).data('lightGallery');

        this.core.s = $.extend({}, defaults, this.core.s);

        if (this.core.s.zoom && this.core.doCss()) {
            this.init();

            // Store the zoomable timeout value just to clear it while closing
            this.zoomabletimeout = false;

            // Set the initial value center
            this.pageX = $(window).width() / 2;
            this.pageY = ($(window).height() / 2) + $(window).scrollTop();
        }

        return this;
    };

    Zoom.prototype.init = function() {

        var _this = this;
        var zoomIcons = '<span id="lg-zoom-in" class="lg-icon"></span><span id="lg-zoom-out" class="lg-icon"></span>';

        if (_this.core.s.actualSize) {
            zoomIcons += '<span id="lg-actual-size" class="lg-icon"></span>';
        }

        if (_this.core.s.useLeftForZoom) {
            _this.core.$outer.addClass('lg-use-left-for-zoom');
        } else {
            _this.core.$outer.addClass('lg-use-transition-for-zoom');
        }

        this.core.$outer.find('.lg-toolbar').append(zoomIcons);

        // Add zoomable class
        _this.core.$el.on('onSlideItemLoad.lg.tm.zoom', function(event, index, delay) {

            // delay will be 0 except first time
            var _speed = _this.core.s.enableZoomAfter + delay;

            // set _speed value 0 if gallery opened from direct url and if it is first slide
            if ($('body').hasClass('lg-from-hash') && delay) {

                // will execute only once
                _speed = 0;
            } else {

                // Remove lg-from-hash to enable starting animation.
                $('body').removeClass('lg-from-hash');
            }

            _this.zoomabletimeout = setTimeout(function() {
                _this.core.$slide.eq(index).addClass('lg-zoomable');
            }, _speed + 30);
        });

        var scale = 1;
        /**
         * @desc Image zoom
         * Translate the wrap and scale the image to get better user experience
         *
         * @param {String} scaleVal - Zoom decrement/increment value
         */
        var zoom = function(scaleVal) {

            var $image = _this.core.$outer.find('.lg-current .lg-image');
            var _x;
            var _y;

            // Find offset manually to avoid issue after zoom
            var offsetX = ($(window).width() - $image.prop('offsetWidth')) / 2;
            var offsetY = (($(window).height() - $image.prop('offsetHeight')) / 2) + $(window).scrollTop();

            _x = _this.pageX - offsetX;
            _y = _this.pageY - offsetY;

            var x = (scaleVal - 1) * (_x);
            var y = (scaleVal - 1) * (_y);

            $image.css('transform', 'scale3d(' + scaleVal + ', ' + scaleVal + ', 1)').attr('data-scale', scaleVal);

            if (_this.core.s.useLeftForZoom) {
                $image.parent().css({
                    left: -x + 'px',
                    top: -y + 'px'
                }).attr('data-x', x).attr('data-y', y);
            } else {
                $image.parent().css('transform', 'translate3d(-' + x + 'px, -' + y + 'px, 0)').attr('data-x', x).attr('data-y', y);
            }
        };

        var callScale = function() {
            if (scale > 1) {
                _this.core.$outer.addClass('lg-zoomed');
                _this.core.$outer.trigger("imageZoomIn");
            } else {
                _this.resetZoom();
            }

            if (scale < 1) {
                scale = 1;
            }

            zoom(scale);
        };

        var actualSize = function(event, $image, index, fromIcon) {
            var w = $image.prop('offsetWidth');
            var nw;
            if (_this.core.s.dynamic) {
                nw = _this.core.s.dynamicEl[index].width || $image[0].naturalWidth || w;
            } else {
                nw = _this.core.$items.eq(index).attr('data-width') || $image[0].naturalWidth || w;
            }

            var _scale;

            if (_this.core.$outer.hasClass('lg-zoomed')) {
                scale = 1;
            } else {
                if (nw > w) {
                    _scale = nw / w;
                    scale = _scale || 2;
                }
            }

            if (fromIcon) {
                _this.pageX = $(window).width() / 2;
                _this.pageY = ($(window).height() / 2) + $(window).scrollTop();
            } else {
                _this.pageX = event.pageX || event.originalEvent.targetTouches[0].pageX;
                _this.pageY = event.pageY || event.originalEvent.targetTouches[0].pageY;
            }

            callScale();
            setTimeout(function() {
                _this.core.$outer.removeClass('lg-grabbing').addClass('lg-grab');
            }, 10);
        };

        var tapped = false;

        // event triggered after appending slide content
        _this.core.$el.on('onAferAppendSlide.lg.tm.zoom', function(event, index) {

            // Get the current element
            var $image = _this.core.$slide.eq(index).find('.lg-image');

            $image.on('dblclick', function(event) {
                if(!_this.core.$outer.hasClass('lg-prevent')){
                    actualSize(event, $image, index);
                }
            });

            $image.on('touchstart', function(event) {
                if(!_this.core.$outer.hasClass('lg-prevent')){
                    if (!tapped) {
                        tapped = setTimeout(function() {
                            tapped = null;
                        }, 300);
                    } else {
                        clearTimeout(tapped);
                        tapped = null;
                        actualSize(event, $image, index);
                    }

                    event.preventDefault();
                }
            });
            
        });

        // Update zoom on resize and orientationchange
        $(window).on('resize.lg.zoom scroll.lg.zoom orientationchange.lg.zoom', function() {
            _this.pageX = $(window).width() / 2;
            _this.pageY = ($(window).height() / 2) + $(window).scrollTop();
            zoom(scale);
        });

        $('#lg-zoom-out').on('click.lg', function() {
            if (_this.core.$outer.find('.lg-current .lg-image').length) {
                scale -= _this.core.s.scale;
                callScale();
            }
        });

        $('#lg-zoom-in').on('click.lg', function() {
            if (_this.core.$outer.find('.lg-current .lg-image').length) {
                scale += _this.core.s.scale;
                callScale();
            }
        });

        $('#lg-actual-size').on('click.lg', function(event) {
            actualSize(event, _this.core.$slide.eq(_this.core.index).find('.lg-image'), _this.core.index, true);
        });

        // Reset zoom on slide change
        _this.core.$el.on('onBeforeSlide.lg.tm', function() {
            if(_this.core.$outer.hasClass("lg-zoomed")){
                scale = 1;
                _this.resetZoom();
            }
        });

        // _this.zoomSwipe();

    };

    // Reset zoom effect
    Zoom.prototype.resetZoom = function() {
        this.core.$outer.trigger("imageZoomOut");
        this.core.$outer.removeClass('lg-zoomed');
        this.core.$slide.find('.lg-img-wrap').removeAttr('style data-x data-y');
        this.core.$slide.find('.lg-image').removeAttr('style data-scale');

        // Reset pagx pagy values to center
        this.pageX = $(window).width() / 2;
        this.pageY = ($(window).height() / 2) + $(window).scrollTop();
    };

    Zoom.prototype.zoomSwipe = function() {
        var _this = this;
        var startCoords = {};
        var endCoords = {};
        var isMoved = false;

        // Allow x direction drag
        var allowX = false;

        // Allow Y direction drag
        var allowY = false;

        _this.core.$slide.on('touchstart.lg', function(e) {

            if (_this.core.$outer.hasClass('lg-zoomed') && !_this.core.$outer.hasClass('lg-prevent')) {
                var $image = _this.core.$slide.eq(_this.core.index).find('.lg-object');

                allowY = $image.prop('offsetHeight') * $image.attr('data-scale') > _this.core.$outer.find('.lg').height();
                allowX = $image.prop('offsetWidth') * $image.attr('data-scale') > _this.core.$outer.find('.lg').width();
                if ((allowX || allowY)) {
                    e.preventDefault();
                    startCoords = {
                        x: e.originalEvent.targetTouches[0].pageX,
                        y: e.originalEvent.targetTouches[0].pageY
                    };
                }
            }

        });

        _this.core.$slide.on('touchmove.lg', function(e) {

            if (_this.core.$outer.hasClass('lg-zoomed') && !_this.core.$outer.hasClass('lg-prevent')) {

                var _$el = _this.core.$slide.eq(_this.core.index).find('.lg-img-wrap');
                var distanceX;
                var distanceY;

                e.preventDefault();
                isMoved = true;

                endCoords = {
                    x: e.originalEvent.targetTouches[0].pageX,
                    y: e.originalEvent.targetTouches[0].pageY
                };

                // reset opacity and transition duration
                _this.core.$outer.addClass('lg-zoom-dragging');

                if (allowY) {
                    distanceY = (-Math.abs(_$el.attr('data-y'))) + (endCoords.y - startCoords.y);
                } else {
                    distanceY = -Math.abs(_$el.attr('data-y'));
                }

                if (allowX) {
                    distanceX = (-Math.abs(_$el.attr('data-x'))) + (endCoords.x - startCoords.x);
                } else {
                    distanceX = -Math.abs(_$el.attr('data-x'));
                }

                if ((Math.abs(endCoords.x - startCoords.x) > 15) || (Math.abs(endCoords.y - startCoords.y) > 15)) {

                    if (_this.core.s.useLeftForZoom) {
                        _$el.css({
                            left: distanceX + 'px',
                            top: distanceY + 'px'
                        });
                    } else {
                        _$el.css('transform', 'translate3d(' + distanceX + 'px, ' + distanceY + 'px, 0)');
                    }
                }

            }

        });

        _this.core.$slide.on('touchend.lg', function() {
            if (_this.core.$outer.hasClass('lg-zoomed') && !_this.core.$outer.hasClass('lg-prevent')) {
                if (isMoved) {
                    isMoved = false;
                    _this.core.$outer.removeClass('lg-zoom-dragging');
                    _this.touchendZoom(startCoords, endCoords, allowX, allowY);

                }
            }
        });

    };

    

    Zoom.prototype.touchendZoom = function(startCoords, endCoords, allowX, allowY) {

        var _this = this;
        var _$el = _this.core.$slide.eq(_this.core.index).find('.lg-img-wrap');
        var $image = _this.core.$slide.eq(_this.core.index).find('.lg-object');
        var distanceX = (-Math.abs(_$el.attr('data-x'))) + (endCoords.x - startCoords.x);
        var distanceY = (-Math.abs(_$el.attr('data-y'))) + (endCoords.y - startCoords.y);
        var minY = (_this.core.$outer.find('.lg').height() - $image.prop('offsetHeight')) / 2;
        var maxY = Math.abs(($image.prop('offsetHeight') * Math.abs($image.attr('data-scale'))) - _this.core.$outer.find('.lg').height() + minY);
        var minX = (_this.core.$outer.find('.lg').width() - $image.prop('offsetWidth')) / 2;
        var maxX = Math.abs(($image.prop('offsetWidth') * Math.abs($image.attr('data-scale'))) - _this.core.$outer.find('.lg').width() + minX);

        if ((Math.abs(endCoords.x - startCoords.x) > 15) || (Math.abs(endCoords.y - startCoords.y) > 15)) {
            if (allowY) {
                if (distanceY <= -maxY) {
                    distanceY = -maxY;
                } else if (distanceY >= -minY) {
                    distanceY = -minY;
                }
            }

            if (allowX) {
                if (distanceX <= -maxX) {
                    distanceX = -maxX;
                } else if (distanceX >= -minX) {
                    distanceX = -minX;
                }
            }

            if (allowY) {
                _$el.attr('data-y', Math.abs(distanceY));
            } else {
                distanceY = -Math.abs(_$el.attr('data-y'));
            }

            if (allowX) {
                _$el.attr('data-x', Math.abs(distanceX));
            } else {
                distanceX = -Math.abs(_$el.attr('data-x'));
            }

            if (_this.core.s.useLeftForZoom) {
                _$el.css({
                    left: distanceX + 'px',
                    top: distanceY + 'px'
                });
            } else {
                _$el.css('transform', 'translate3d(' + distanceX + 'px, ' + distanceY + 'px, 0)');
            }

        }
    };

    Zoom.prototype.destroy = function() {

        var _this = this;

        // Unbind all events added by lightGallery zoom plugin
        _this.core.$el.off('.lg.zoom');
        $(window).off('.lg.zoom');
        _this.core.$slide.off('.lg.zoom');
        _this.core.$el.off('.lg.tm.zoom');
        _this.resetZoom();
        clearTimeout(_this.zoomabletimeout);
        _this.zoomabletimeout = false;
    };

    $.fn.lightGallery.modules.zoom = Zoom;

})(jQuery, window, document);


