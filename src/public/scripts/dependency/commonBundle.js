// Our greatest glory is not in never failing, but in rising up every time we fail. — Ralph Waldo Emerson
'use strict';
define([
    'modules/footer/scripts/index',
    'modules/header/scripts/index',
    'modules/search/scripts/index',
    'modules/searchFilter/scripts/index',
    'modules/typeAhead/scripts/index',
    'modules/breadcrumb/scripts/index',
    'modules/menuDrawer_SEO/scripts/index',
    'modules/error/scripts/index',
    'modules/prompt/scripts/index',
    'modules/socialSharing/scripts/index',
    'modules/alertConfirmBox/scripts/index',
    'modules/showSimilarLocalities/scripts/index'
], () => {
    require([]);
});