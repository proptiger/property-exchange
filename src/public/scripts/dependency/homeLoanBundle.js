// When something is important enough, you do it even if the odds are not in your favor. - Elon Musk
'use strict';
define([
    'modules/banks/scripts/index',
    'modules/emiCalculator/scripts/index',
    'modules/homeloan/scripts/index'
], () => {
   require([]);
});
