// A hero is no braver than an ordinary man, but he is brave five minutes longer. — Ralph Waldo Emerson
'use strict';
define([
	'services/commonService',
	'services/feedbackService',
	'services/loadingService',
    'modules/jarvisWrapper/scripts/index',
    'modules/feedback/scripts/index',
    'modules/sellerFeedback/scripts/index',
    'modules/twosteppyr/scripts/index',
	'modules/lead/scripts/index',
    'modules/pyr/scripts/index'
], () => {
	const Application = Box.Application,
		LoadingService = Application.getService('LoadingService');
		LoadingService.loadAllTracking();
	require([]);
});
