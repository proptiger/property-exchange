'use strict';
define([
    'modules/sellerReviews/scripts/index',
    'modules/ratingBars/scripts/index',
    'modules/ratingReviewWrapper/scripts/index',
    'modules/sellerScore/scripts/index',
    'modules/sellerRating/scripts/index',
    'modules/dealMaker-notification/scripts/index',
], () => {
   require([]);
});
