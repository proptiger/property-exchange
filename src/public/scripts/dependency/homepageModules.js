'use strict';
define([
	'modules/listingSnapshot/scripts/index',
	'modules/popularLocalities/scripts/index',
    'modules/filter/scripts/index',
    'modules/searchFilter/scripts/index',
    'modules/recentDeals/scripts/index'
], function() {
    require([]);
});
