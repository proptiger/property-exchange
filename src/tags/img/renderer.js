/*
 * Name: <img> 
 * Description: Overrides <img> tag for W3C validations & bot-restrictions.
 * @author: [Bhavya Saggi]
 * @todo: [sets dummyPx.png as default src if not present]
 * Date: 6 Aug, 2018
 */
const utilService = require("services/utilService");
const hwRegex = (/([0-9]+).*/);
const MAX_IMAGES = 10;
const dummyImage = "/images/dummyPX.png"; //tag_version

function _linearAttribute(name, value, defaultValue="") {
	return `${name.toKebabCase()}="${value||defaultValue}" `;
}

exports.renderer = function(data, out) {
	let isAmp=false,
		bot=false,
		totalImages=0;

	if(out.stream.req){
		isAmp = out.stream.req.isAmp;
		bot = out.stream.req.bot;
		totalImages = out.stream.req.imgCount;
	}

	if(isAmp){
		out.w("");
	} else {
		let attributes = Object.keys(data).reduce(function(attrTotal,attr){
			if(attr == "src") {
				return bot ? attrTotal : attrTotal + _linearAttribute(attr, data[attr], dummyImage);
			} else if(attr == "dataSrc") {
				return bot ? attrTotal : attrTotal + _linearAttribute(attr, data[attr]);
			} else if(~['height','width'].indexOf(attr)){
				// height,width should be a number;
				// extract a number if possible, else skip it
				return hwRegex.test(data[attr]) ? attrTotal + _linearAttribute(attr, data[attr].replace(hwRegex,'$1')) : attrTotal;
			} else if(['invokeBody','renderBody'].indexOf(attr)<0) {
				return attrTotal + _linearAttribute(attr, data[attr]);
			} else {
				return attrTotal;
			}
		},'') || '';

		// always have alt attribute
		if(!data.hasOwnProperty("alt")) {
			attributes += `alt="" `;
		}
		// always have src attribute
		if(!data.hasOwnProperty("src")) {
			attributes += `src="${dummyImage}" `;
		}
		
		if(bot) {
			// Serve first 10 images to bots (in first-fold)
			if(totalImages<MAX_IMAGES) {
				attributes += `src="${data['dataSrc'] || data['src'] || dummyImage}" `;
			} else {
				attributes += `src="${data['src'] || dummyImage}" `;
				if(data['dataSrc']){
					attributes += `data-src="${data['dataSrc']}" `;
				}
			}			
		}

		// Increment count of number of images on page
		if(out.stream.req){
			out.stream.req.imgCount++;
		}
		
		out.w(`<img ${attributes.trim()}/>`);
	}
}
