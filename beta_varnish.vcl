vcl 4.0;
import std;
import directors;

acl purge_ip {
  "localhost";
  "127.0.0.1";
}

backend main_server1 {
  .host = "52.76.154.62";
 # .host = "varnish.makaan-ws.com";
 # .port = "80";
  .port = "8040";
  .probe = {
      .url = "/apis/makaan-health";
      .interval = 2s;
      .timeout = 5s;
      .window = 5;
      .threshold = 3;
  }
}

sub vcl_init {
  new back_main = directors.round_robin();
  back_main.add_backend(main_server1);
}

sub vcl_recv {
    set req.backend_hint = back_main.backend();

    if(req.method == "PURGE"){
      if(!client.ip ~ purge_ip){
        return (synth(405, "Purging from this location is not allowed"));
      }
      return (purge);
    }

    if (req.method != "GET" &&
      req.method != "HEAD" &&
      req.method != "PUT" &&
      req.method != "POST" &&
      req.method != "TRACE" &&
      req.method != "OPTIONS" &&
      req.method != "DELETE") {
        /* Non-RFC2616 or CONNECT which is weird. */
        return (pipe);
    }

    if (req.url ~ "\.js$" || req.url ~ "\.css$") {
        return (hash);
    }

    # pass microservices
    if(req.url ~ "^/petra" || 
      req.url ~ "^/madelyne" || 
      req.url ~ "^/dawnstar" || 
      req.url ~ "^/pixie" || 
      req.url ~ "^/sapphire" || 
      req.url ~ "^/kira" || 
      req.url ~ "^/compass" || 
      req.url ~ "^/xhr" || 
      req.url ~ "^/columbus" || 
      req.url ~ "^/madrox" || 
      req.url ~ "^/zenithar" || 
      req.url ~ "^/laravel" || 
      req.url ~ "^/shade" || 
      req.url ~ "^/athena" || 
      req.url ~ "^/cyclops" || 
      req.url ~ "^/themis" || 
      req.url ~ "^/dossier"){
      return (pass);
    }


    # allow utm parameters to pass to backend
    if(req.url ~ "utm_" || req.http.Cookie ~ "utm_"){
      return (pass);
    }

    # allow logged in users to be served directly from backend
    if(req.http.Cookie ~ "JSESSIONID"){
      return (pass);
    }


    # allow debug parameters
    if(req.url ~ "(debug=true)"){
      return (pass);
    }

    # varnish health
    if(req.url ~ "^/varnish-health"){
      return (synth(200, "Varnish is healthy"));
    }

    # pass no-cache, logged, buyer dashboard in apis
    if(req.url ~ "^/apis/nc" || 
      req.url ~ "^/apis/login"){
      return (pass);
    }

    if (req.method != "GET" && req.method != "HEAD") {
        /* We only deal with GET and HEAD by default */
        return (pass);
    }

    if (req.http.Authorization) {
        /* Not cacheable by default */
        return (pass);
    }

    # Normalization of Vary Headers(Accept encoding and User Agent)
    if (req.http.Accept-Encoding) {
        if (req.http.Accept-Encoding ~ "gzip") {
            set req.http.Accept-Encoding = "gzip";
        } elsif (req.http.Accept-Encoding ~ "sdch") {
            set req.http.Accept-Encoding = "sdch";
        } elsif (req.http.Accept-Encoding ~ "deflate") {
            set req.http.Accept-Encoding = "deflate";
        } elsif (req.http.Accept-Encoding ~ "MSIE 6") {
            set req.http.Accept-Encoding = "MSIE 6";
        } else {
            unset req.http.Accept-Encoding;
        }
    }

    # Caching Front-end API response for GoogleBot
    if(req.url ~ "^/apis/") {
        return (hash);
    }

    if (req.http.User-Agent ~ "(Googlebot|bot)") {
        return (pass);
    }

    return (hash);
}

sub vcl_hash {
  #hash_data(req.url);
  #utm parameters
  #vary headers and accept header
  #return (lookup);
}

sub vcl_backend_response {
  if(beresp.status >= 400){
    set beresp.ttl = 0s;
  }

  set beresp.grace = 6h;

  return (deliver);
}

sub vcl_hit {
  if (obj.ttl >= 0s) {
    # normal hit
    return (deliver);
  }
  if (std.healthy(req.backend_hint)) {
    # Backend is healthy. Limit age to 10s.
    if (obj.ttl + 10s > 0s) {
        set req.http.grace = "normal(limited)";
        return (deliver);
    } else {
        # No candidate for grace. Fetch a fresh object.
        # return(miss);
        return (fetch);
    }
  } else {
    # backend is sick - use full grace
    if (obj.ttl + obj.grace > 0s) {
        set req.http.grace = "full";
        return (deliver);
    } else {
        # no graced object.
        # return (miss);
        return (fetch);
    }
  }
}

#sub vcl_miss {
#  if(req.http.X-Cache ~ "MISS"){
#    set req.backend_hint = back_main.backend();
#  } else {
#    set req.http.X-Cache = "MISS";
#    #set req.backend_hint = back_main.backend();
#    set req.backend_hint = back_varnish.backend();
#    if(!std.healthy(req.backend_hint)){ 
#      set req.backend_hint = back_main.backend();
#    }
#  }
#  return (fetch);
#}

sub vcl_deliver {
  if(obj.hits > 0){
    set resp.http.X-VCache = "HIT-"+resp.http.Age;
  } else {
    set resp.http.X-VCache = "MISS"; 
  }

  ## DO NOT SERVERPUSH [ based on cookie.pushHeader ]
  set resp.http.X-SUPER = regsub(req.http.Cookie,".*pushHeader=([^;]+);.*","\1");
  if(resp.http.X-serverPush && resp.http.X-serverPush == resp.http.X-SUPER) {
    unset resp.http.link;
  }
  unset resp.http.X-SUPER;
  unset resp.http.X-serverPush;

  ## Omitting Headers
  unset resp.http.Age;
  unset resp.http.Via;
  unset resp.http.X-Varnish;
}
