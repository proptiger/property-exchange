define({
    loaders: {
        'host-node': 'requirejs',
        'host-browser': 'node_modules/requirejs/require.js'
    },
    reporters: [ "Pretty"],
    filterErrorStack: true,
    loaderOptions: {
        // Packages that should be registered with the loader in each testing environment
        packages: [{
            name: "intern",
            location: "node_modules/intern"
        },{
            name: "modules",
            location: ".tmp/modules"
        },{
            name: "unit",
            location: "compiled/tests/unit/client"
        }],
        "doT": {
            "ext": ".html"
        },
        paths: {
            vendor: 'src/public/scripts/vendor',
            modules: '.tmp/modules',
            common: '.tmp/scripts/common',
            text: "src/public/bower_components/requirejs-text/text",
            json: "src/public/bower_components/requirejs-json/json",
            doT: "src/public/bower_components/requirejs-doT/doT",
            public: ".tmp",
            "config": "src/public/config",
            "doTCompiler": "src/public/bower_components/doT/doT.min",
            "t3": "src/public/scripts/vendor/t3-test",
            "behaviors": ".tmp/scripts/behaviors",
            "services": ".tmp/scripts/services",
            "mocks": "compiled/tests/mocks"
        }
    },

    // Unit test suite(s)
    suites: [ 
              "unit/bootstrap",
              "unit/leadTest",
              "unit/utilTest"
            ],

    // Functional test suite(s) to execute against each browser once unit tests are completed
    functionalSuites: [],

    excludeInstrumentation: /^(?:tests|node_modules|src\/public\/scripts\/vendor)\/|src\/public\/bower_components/
});
