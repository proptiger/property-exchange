define([], function() {
    return {

        getLeadRawData: function() {
            return {
                name: "afroz",
                email: "afroz.alam@proptiger.com",
                phone: 9911869145,
                companyPhone: 76286328,
                domainId: 1,
                cityid: 21,
                cityName: "Pune",
                countryId: 1,
                step: "MAIN",
                enquiryType: {
                    id: 1
                },
                multipleCompanyIds: [499]
            }
        },
        getConnectNowData: function() {
            return {
                postData: {
                    phone: 9911,
                    salesType: "rent",
                    cityId: 1,
                    countryId: 1
                },
                displayData: {
                    companyUserId: 1,
                    companyPhone: 1
                },
                response: {
                    MAIN: {
                        tempEnquiryId: 1
                    }
                },
                moduleParameters: {
                    moduleId: "lead"
                },
                initialStep: "MAIN"
            }
        },
        getHomeLoanData: function() {
            var userData = this.getLeadRawData();
            userData.interestedBanks = [1];
            userData.loanAmount = 5000000;
            return userData;
        },

    }
});
