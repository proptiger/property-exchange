define({
    loaders: {
        'host-node': 'requirejs',
        'host-browser': 'node_modules/requirejs/require.js'
    },
    reporters: [ "Pretty"],
    filterErrorStack: true,
    loaderOptions: {
        // Packages that should be registered with the loader in each testing environment
        packages: [{
            name: "intern",
            location: "node_modules/intern"
        },{
            name: "modules",
            location: ".tmp/modules"
        },{
            name: "unit",
            location: "compiled/tests/unit/server"
        }],
        "doT": {
            "ext": ".html"
        },
        paths: {
             "services": "compiled/services",
             "configs": "compiled/configs",
             "public": "compiled/public"        
        }
    },

    // Unit test suite(s)
    suites: [ 
              "unit/bootstrap",
              "unit/cityServiceTest"
            ],

    // Functional test suite(s) to execute against each browser once unit tests are completed
    functionalSuites: [],

    excludeInstrumentation: /^(?:tests|node_modules|src\/public\/scripts\/vendor)\/|src\/public\/bower_components/
});
