/*
    All global dependencies injected here .
*/
define([],function(){
    let path = require('path'),
        CONFIG_PATH  = path.join('./', '.env');
    /* Resolving non-relative modules */
    require('app-module-path').addPath(path.resolve(process.cwd(),'compiled'));
    require('dotenv').config({path: CONFIG_PATH});
});


