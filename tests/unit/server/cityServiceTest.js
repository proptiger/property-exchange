"use strict";
/*
 * Test suite containing all unit test cases of city service .
 */

define([
        "services/cityService"
        ], 
    function(cityService) {
    var registerSuite = require('intern!object');
    var assert = require('intern/chai!assert');
    var expect = require('intern/chai!expect');
    var sinon = require('sinon');
   


    registerSuite({

        name: "city service unit tests",

        'min max listing function test': function() {
            let returnObj    = cityService.getMinMaxListingTypePrices(),
                expectedKeys = ["buyMinPrice","buyMaxPrice","rentMinPrice","rentMaxPrice"];
            expect(returnObj).to.contain.all.keys(expectedKeys);
        },
        "getAllCityList is called inside getAllCityWithPriceTrendUrl": function() {
            let cityServiceStub = sinon.stub(cityService,"getAllCityList",function() {
                return [{
                    id: 1,
                    name: "Pune"
                }]
            });
            cityService.getAllCityWithPriceTrendUrl({},{});
            assert.equal(cityServiceStub.calledOnce,true,"getAllCityList was called..");
            cityServiceStub.restore();
        }

    });
});
