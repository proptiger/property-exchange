/*
*  Test suite containing all unit test cases of utilTest
*  Auto generated from generator 1.0.0 from option node generator -n utilTest -g test -t unit
*/

define(["services/utils"],function(utils) {
    var registerSuite = require('intern!object');
    var assert        = require('intern/chai!assert');
    var expect        = require('intern/chai!expect');
    var sinon         = require('sinon');

    registerSuite({

        name: "utilTest unit tests",

        setup: function() {
            // executes before suite starts
        },
        beforeEach: function() {
            // executes before each test
        },
        afterEach: function() {
            // executes after each test
        },
        teardown: function() {
            // executes after suite ends
        },
        'it should return all query parameters of url passed': function() {
            let url = "http://www.makaan.com?page=listing&builderId=1234&builderName=afroz",
                expectedOutput = {
                    page: 'listing',
                    builderId: "1234",
                    builderName: 'afroz'
                };
            assert.deepEqual(utils.getAllQueryStringParam(url),expectedOutput,"it should return object having all keys");   
        }
    });
});
