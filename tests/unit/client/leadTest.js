/*
 * Test suite containing all unit test cases of lead module .
 */

define(["mocks/lead", 
        "modules/lead/scripts/services/leadService",
        "services/apiService",
        "services/loginService",
        "modules/lead/scripts/index"
        ], 
    function(leadMock, 
      leadService, 
      apiService,
      loginService
      ) {
    var registerSuite = require('intern!object');
    var assert        = require('intern/chai!assert');
    var expect        = require('intern/chai!expect');
    var sinon         = require('sinon');

    function _emptyPromise(data = {}) {
        return new Promise((resolve) => {
            setTimeout(()=> {
                resolve(data);
            },1000)
        });
    }

    registerSuite({

        name: "lead module unit tests",



        'test leadService is not null': function(){
            assert.isNotNull(leadService, 'lead instantiated correctly');
        },


        /* async test */
        "test post lead": function() {

            let dfd = this.async(3000),
                dummyPromise = _emptyPromise.bind(undefined, {
                    id: 1
                }),
                leadPyrService = {
                     postLead: dummyPromise,
                      prune: function() {

                      }
                },
                leadContext = new Box.TestServiceProvider({
                   'LeadPyrService': leadPyrService
                }),
                testLeadService = Box.Application.getServiceForTest('LeadService', leadContext),
                promise = testLeadService.postEnquiry({
                    postData: leadMock.getLeadRawData()
                });

            promise.then(dfd.callback(function(data) {
                expect(data).to.have.property("id");
            }));
        },


        "test type of connect now data": function() {
            let dfd = this.async(3000),
                dummyPromise = _emptyPromise.bind(undefined, {
                    data: {
                        id: 1,
                        virtualNumberMapping: {
                            virtualNumber: {
                                number: 99118561
                            }
                        }
                    }
                }),
                leadContext = new Box.TestServiceProvider({
                   'ApiService': {
                      postJSON: dummyPromise
                   }
                }),
                testLeadService = Box.Application.getServiceForTest('LeadService', leadContext),
                promise = testLeadService.connectBuyerSeller(leadMock.getConnectNowData());

            promise.then(dfd.callback(function(data) {
                expect(data).to.have.property("callId");
            }));
        },


        /* testing lead module click */
        "test module click": function() {
            // let dummyPromise = _emptyPromise.bind(undefined, {});

            // /* stubbing out isUserLoggedin function in LoginService'*/
            // let leadContext = new Box.TestServiceProvider({
            //    'LoginService': {
            //       isUserLoggedIn: dummyPromise
            //    }
            // });
            // leadContext.getConfig = sinon.stub().returns({});
            // leadContext.broadcast = sinon.stub();
            // leadContext.getElement = sinon.stub().returns({id: "test-lead-form"});
            // module = Box.Application.getModuleForTest('lead', leadContext);
            // module.init();

            // let spy = sinon.spy(module, 'init');

            // // create event object with correct target using jQuery
            // let target = $('[data-type=MAIN_SUBMIT]')[0];
            // let event = $.Event('click', {
            //     target: target
            // });

            // // call the method to test
            // module.onclick(event, target, 'MAIN_SUBMIT');
            // module.destroy();
            // assert.equal(spy.calledOnce,true,"init was called..");
            // spy.restore();
        }
    });
});
