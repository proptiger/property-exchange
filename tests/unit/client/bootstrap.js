/*
    All global dependencies (jquery,Box,window) injected here .
*/
define(["t3","mock-browser","jquery"],function(Box,mockBrowser,$){
    let path = require('path'),
        CONFIG_PATH  = path.join('./', '.env'),
        MockBrowser  = mockBrowser.mocks.MockBrowser,
        mockWindow   = MockBrowser.createWindow();
    global.Box       = Box;
    global.window    = mockWindow;
    global.document  = new MockBrowser().getDocument();
    global.$ = $((require("jsdom").jsdom().defaultView));
    /* Resolving non-relative modules */
    require('app-module-path').addPath(path.resolve(process.cwd(),'compiled'));
    require('dotenv').config({path: CONFIG_PATH});
});


