/*
    **********************************************
    ********* webp conversion module. ************
    **********************************************

    This module can be used by command $node webp <optional path>
    If optional path dir/file name is passed it can read the same and will only convert that specific file.
    If path not given it will convert all images from path src/public/images
    NOTE: 
    1) This module will not convert image is converted image is of bigger size compared to original image
    2) To convert images which doesn't work with this module use online tool https://webp-converter.com/
    3) For better results always give original uncompressed image as source
*/

var imagemin = require("imagemin"),    // The imagemin module.
    webp = require("imagemin-webp"),   // imagemin's WebP plugin.
    fs = require('fs'), 
    path = require('path');

var config = {
    baseList : [],
    basePath : 'src/public/images',
    quality: 80 //quality set for 90
}


if(process.argv.length>2){
    var allPaths = process.argv.slice(2);
    allPaths.forEach(function (val, index, array) {
      config.baseList.push(config.basePath + '/'  + val)
    });    
} else {
    config.baseList.push(config.basePath)
}

config.baseList.forEach(imageSrc=>{
    fs.stat(imageSrc,function(err,stats){
        if(err){
            console.log('Please check....path not found');
            console.log('*********************************************************');
            return;
        }
        if(stats && stats.isDirectory()){
            parseDir(imageSrc);  
        }
        if(stats && stats.isFile()){
            var fileDir = imageSrc.substr(0,imageSrc.lastIndexOf('/'));
            var fileName = imageSrc.substr(imageSrc.lastIndexOf('/')+1);
            convertToWebp(fileDir,fileName); 
        }
    });
})
function parseDir(imageSrc){
    fs.readdir(imageSrc,function(err,files){
        if(err){
            console.log('error--->',err);
            return;
        }
        console.log(files);
        files.forEach(function(file,index){
            var fromPath = path.join( imageSrc, file );
            fs.stat(fromPath,function(error,stat){
                if(stat && stat.isFile()){
                    convertToWebp(imageSrc,file);  
                }
                else if(stat && stat.isDirectory()){
                    parseDir(fromPath);
                }
            });
        });
    })  
}

function convertToWebp(src,file){
    fileType = path.extname(file);
    var source = src+'/'+file;
    if(['.jpg','.jpeg','.png'].indexOf(fileType)>=0){
        imagemin([source], src, {
            plugins: [webp({
              quality: config.quality // Quality setting from 0 to 100
            })]
        }).then((result)=>{
            //console.log("done-->",result[0].path);
        }).catch(err => {
            console.log("error-->",err);
        });
    }
  
}

