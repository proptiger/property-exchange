// Generated on 2015-10-05 using
// generator-webapp 1.1.0
'use strict';

// # Globbing
// for performance reasons we're only matching one level down:
// 'test/spec/{,*/}*.js'
// If you want to recursively match all subfolders, use:
// 'test/spec/**/*.js'

module.exports = function(grunt) {

    require('dotenv').config();

    // Time how long tasks take. Can help when optimizing build times
    require('time-grunt')(grunt);

    //require('./vermin')(grunt);

    // Automatically load required grunt tasks
    require('jit-grunt')(grunt, {
        useminPrepare: 'grunt-usemin'
    });


    grunt.loadNpmTasks('grunt-minified');
    grunt.loadNpmTasks('grunt-aws');

    // Configurable paths
    var config = {
            app: 'src/public',
            dist: 'dist',
            tmp: '.tmp',
            server: 'src',
            serverDist: 'compiled',
            testDir: 'tests'
        },
        requirejsOptions = {},
        customModules = [],
        otherModules = [],
        appPageModules = [], //'serpPage'  these are modules that are part of app.js and will not be in main.js mapping after versioning

        CDN_MAP = {
            CDN_JS: {
                include: ['.js', '.png', '.jpg', '.jpeg', '.webp'],
                path: appendSlash(process.env.CDN_URL_CSS)
            },
            CDN_CSS: {
                include: ['.css', '.ttf', '.woff2', '.svg', '.eot', '.woff'],
                path: appendSlash(process.env.CDN_URL_JS)
            },
            CDN_S3: {
                include: ['makaan-videos/'],
                path: appendSlash(process.env.CDN_URL_S3)
            }
        },
        DEFAULT_CDN = CDN_MAP.CDN_JS.path;

    function appendSlash(CDN) {
        return CDN && CDN[CDN.length - 1] != '/' ? CDN + '/' : CDN || '/';
    }

    function lazyLoadCssCDNPath(dest) {
        var cdn = appendSlash(CDN_MAP.CDN_CSS.path);
        dest = cdn && dest && dest.indexOf('/') == 0 ? dest.substr('1') : dest;
        return cdn + dest;
    }

    function getJarvisUrl() {
        var JARVIS_URL = appendSlash(process.env.JARVIS_URL);
        return modifyCDNForSWOrigin(JARVIS_URL);
    }

    function modifyCDNForSWOrigin(cdn_path) {
        cdn_path = cdn_path && cdn_path.trim();
        cdn_path = cdn_path && cdn_path != '/' ? cdn_path : null;
        return cdn_path;
    }

    function getCDNUrl(url, onlyCDNForSWOrigin) {
        var i, j, allCDN = Object.keys(CDN_MAP),
            responseStr;
        for (i = 0; i < allCDN.length; i++) {
            var CDN = CDN_MAP[allCDN[i]],
                allFileTypes = CDN['include'],
                CDN_PATH = CDN.path;
            for (j = 0; j < allFileTypes.length; j++) {
                if (url.indexOf(allFileTypes[j]) !== -1) {
                    responseStr = (url.indexOf('$') === -1 || url.indexOf('{') === -1) ? CDN_PATH + (url.indexOf('/') === 0 ? url.substring(1) : url) : url;
                    return onlyCDNForSWOrigin ? modifyCDNForSWOrigin(CDN_PATH) : responseStr;
                }
            }
        }
        responseStr = url.indexOf('{') === -1 ? DEFAULT_CDN + (url.indexOf('/') === 0 ? url.substring(1) : url) : url;
        return onlyCDNForSWOrigin ? modifyCDNForSWOrigin(DEFAULT_CDN) : responseStr;
    }

    // Define the configuration for all the tasks
    grunt.initConfig({

        // Project settings
        config: config,
        s3: {
            options: {
                accessKeyId: `${process.env.AWS_STATIC_ASSET_KEY}`,
                secretAccessKey: `${process.env.AWS_STATIC_ASSET_SECRET}`,
                bucket: `${process.env.AWS_STATIC_ASSET_BUCKET}`,
                region: `${process.env.AWS_STATIC_ASSET_REGION}`,
                access: 'public-read',
                gzip: true,
                concurrency: 10,
                overwrite: false,
                cacheTTL: Infinity,
                headers: {
                    "Cache-Control": "max-age=31536000000, public",
                    "Expires": new Date(Date.now() + 31536000000).toUTCString()
                }
            },
            prod: {
                cwd: `<%= config.dist %>`,
                src: `**`,
            }
        },
        minified: {
            files: {
                src: [
                    '<%= config.dist %>/sw-offline-google-analytics.js',
                    '<%= config.dist %>/sw.*.js'
                ],
                dest: '<%= config.dist %>/'
            },
            options: {
                sourcemap: false,
                allinone: false
            }
        },
        // ugilify
        uglify: {
            dist: {
                files: [{
                    expand: true,
                    cwd: '<%= config.dist %>/scripts',
                    src: ['{,**/}*.js'],
                    dest: '<%= config.dist %>/scripts'
                }, {
                    expand: true,
                    cwd: '<%= config.dist %>/modules',
                    src: ['{,**/}*.js'],
                    dest: '<%= config.dist %>/modules'
                }]
            }
        },

        compress: {
            main: {
                options: {
                    mode: 'brotli',
                    brotli: {
                    mode: 1
                    }
                },
                expand: true,
                cwd: '<%= config.dist %>/scripts',
                src: ['{,**/}*.js'],
                dest: '<%= config.dist %>/scripts',
                extDot: 'last',
                ext: '.js.br'
            }
        },

        // Empties folders to start fresh
        clean: {
            dist: {
                files: [{
                    dot: true,
                    src: [
                        '.tmp',
                        '<%= config.dist %>/*',
                        '!<%= config.dist %>/.git*',
                        '<%= config.serverDist %>/*'
                    ]
                }]
            },
            client: {
                files: [{
                    dot: true,
                    src: [
                        '.tmp'
                    ]
                }]
            },
            css: {
                files: [{
                    dot: true,
                    src: [
                        '<%= config.app %>/styles/css/*'
                    ]
                }]
            },
            server: {
                files: [{
                    dot: true,
                    src: [
                        '<%= config.serverDist %>/*'
                    ]
                }]
            }
        },

        mkdir: {
            all: {
                options: {
                    create: ['<%= config.serverDist %>/tags']
                },
            }
        },

        // Make sure code styles are up to par and there are no obvious mistakes
        eslint: {
            target: [
                'Gruntfile.js',
                '<%= config.app %>/scripts/{,*/}*.js',
                '<%= config.app %>/modules/{,*/}*.js'
            ]
        },

        // Compiles ES6 with Babel
        babel: {
            options: {
                sourceMap: false
            },
            client: {
                files: [{
                    expand: true,
                    src: ['!<%= config.tmp %>/sw-offline-google-analytics.js', '<%= config.tmp %>/scripts/{,**/}*.js', '!<%= config.tmp %>/scripts/vendor/{,**}*.js'],
                    ext: '.js'
                }, {
                    expand: true,
                    cwd: '<%= config.tmp %>/modules',
                    src: ['{,**/}*.js'],
                    dest: '<%= config.tmp %>/modules',
                    ext: '.js'
                }]
            },
            // server: {
            //     files: [{
            //         expand: true,
            //         src: ['<%= config.serverDist %>/{,**/}*.js', '!<%= config.serverDist %>/{,**/}*.marko.js', '!<%= config.serverDist %>/mock/{,**/}*.js'],
            //         ext: '.js'
            //     }]
            // }
        },

        // Compiles Sass to CSS and generates necessary files if requested
        sass: {
            server: {
                options: {
                    sourceMap: true,
                    sourceMapEmbed: true,
                    sourceMapContents: true,
                    includePaths: ['.'],
                    outputStyle: 'compressed'
                },
                files: [{
                    expand: true,
                    cwd: '<%= config.app %>/styles',
                    src: ['*.{scss,sass}', '{,**/}*.{scss,sass}'],
                    dest: '<%= config.app %>/styles/css',
                    ext: '.css'
                }]
            },
            dist: {
                options: {
                    sourceMap: false,
                    includePaths: ['.'],
                    outputStyle: 'compressed'
                },
                files: [{
                    expand: true,
                    cwd: '<%= config.app %>/styles',
                    src: ['*.{scss,sass}', '{,**/}*.{scss,sass}'],
                    dest: '<%= config.dist %>/styles/css',
                    ext: '.css'
                }]
            }
        },

        postcss: {
            options: {
                map: true,
                processors: [
                    // Add vendor prefixed styles
                    require('autoprefixer')({
                        browsers: ['> 1%', 'last 2 versions', 'Firefox ESR']
                    })
                ]
            },
            server: {
                files: [{
                    expand: true,
                    cwd: '<%= config.app %>/styles/css',
                    src: ['{,*/}*.css', '{,**/}*.css}'],
                    dest: '<%= config.app %>/styles/css'
                }]
            },
            dist: {
                files: [{
                    expand: true,
                    cwd: '<%= config.dist %>/styles/css',
                    src: ['{,*/}*.css', '{,**/}*.css}'],
                    dest: '<%= config.dist %>/styles/css'
                }]
            }
        },

        // Renames files for browser caching purposes
        filerev: {
            dist: {
                src: [
                    '!<%= config.dist %>/OneSignalSDKUpdaterWorker.js',
                    '!<%= config.dist %>/OneSignalSDKWorker.js',
                    '<%= config.dist %>/sw.js',
                    '<%= config.dist %>/styles/{,*/}*.css',
                    '!<%= config.dist %>/styles/css/*-inline.*',
                    '<%= config.dist %>/images/{,**/}*.*',
                    '!<%= config.dist %>/images/un-versioned/{,**/}*.*',
                    '<%= config.dist %>/fonts/{,*/}*.*',
                    '!<%= config.dist %>/images/dummy/*.*',
                    //'<%= config.dist %>/scripts/main.*'
                    // '<%= config.dist %>/*.{ico,png}'
                ]
            },
            otherJS: {
                src: [
                    '<%= config.dist %>/scripts/{,*/**/}*.js', //for images in scripts dependencies (ex call2)
                    '!<%= config.dist %>/scripts/vendor/*.js',
                    '!<%= config.dist %>/scripts/main.*',
                    '<%= config.dist %>/modules/{,*/**/}*.js', // for images in modules (ex bedge).
                ]
            },
            mainJS: {
                src: [
                    '<%= config.dist %>/scripts/main.*'
                ]
            }
        },

        svgmin: {
            dist: {
                files: [{
                    expand: true,
                    cwd: '<%= config.app %>/images',
                    src: '{,*/}*.svg',
                    dest: '<%= config.dist %>/images'
                }]
            }
        },

        useminPrepare: {
            html: ['<%= config.serverDist %>/amp/modules/{,**/}*.marko','<%= config.serverDist %>/templates/{,**/}*.marko','<%= config.serverDist %>/modules/{,**/}*.marko','<%= config.dist %>/modules/{,**/}*.html'],
            options: {
                root: '<%= config.app %>',
                dest: '<%= config.dist %>',
                flow: {
                    html: {
                        steps: {
                            css: ['concat', 'cssmin'],
                            xcss: ['concat', 'cssmin'],
                            js: ['concat']
                        },
                        post: {}
                    }
                }
            }
        },

        usemin: {
            html: ['<%= config.serverDist %>/amp/modules/{,**/}*.marko','<%= config.serverDist %>/amp/tags/{,**/}*.marko','<%= config.serverDist %>/templates/{,**/}*.marko', '<%= config.serverDist %>/tags/{,**/}*.marko', '<%= config.serverDist %>/modules/{,**/}*.marko', '<%= config.dist %>/scripts/pageModules/*.js', '<%= config.dist %>/modules/newLoginRegister/scripts/*.js', '<%= config.dist %>/scripts/dependency/*.js', '<%= config.dist %>/scripts/app.*.js','<%= config.dist %>/modules/{,**/}*.js'],
            js: ['<%= config.serverDist %>/services/templateLoader.js','<%= config.serverDist %>/tags/{,**/}renderer.js'],
            css: ['<%= config.dist %>/styles/{,**/}*.css'],
            xcss: ['<%= config.dist %>/styles/{,**/}*.css'],
            options: {
                assetsDirs: [
                    '<%= config.dist %>'
                ],
                patterns: {
                  js: [
                    //[/serverPushPath\.push\(['"](.*?)["']\).*?\/\/serverPush_version/gm, 'templateloader-serverpush'],
                    [/\s*=\s*['"](.*?)["'].*?\/\/tag_version/gm, 'html-tag overrides', null, function(out){
                        if (out.indexOf('data:') === 0 || out.indexOf('http:') === 0 || out.indexOf('https:') === 0 || out.indexOf('//') === 0 || out.trim().length === 0) {
                            return out;
                        } else {
                            return getCDNUrl(out);
                        }
                    }]
                  ],
                  html: [
                    [/<amp-img[^\>]*[^\>\S]+src=['"]([^'"\)#]+)(#.+)?["']/gm, 'Update the HTML with the new amp-img filenames'],
                    [/lazyScript\(['"]([^'"\)#]+)(#.+)?["']\)[;]+[ ]*\/\/LAZYSCRIPT/gm, 'Update lazyscript revisions', null, function(out){return getCDNUrl(out);}],
                    [/<script.*?src\s*=\s*['"]\${data\.layoutData\.prefixToUrl\(['"](.*?)["']\).*><\/script>/gm, 'Revision script tags with prefixToUrl'],
                    [/<script.*?type\s*=\s*"text\/x-config".*?>([\S\s]*?)<\/script>/gm, 'Update x-config with no whitespace', null, function(out){
                        try {
                            return JSON.stringify(JSON.parse(out));
                        } catch(e){
                           return out.replace(/[\s]*\n[\s]*/gi,'');
                        }
                        return out;
                    }]
                  ]
                },
                blockReplacements: {
                    css: function(block) {
                        var returnString = '',
                            blockDest = block && block.dest;
                        if(blockDest && blockDest.indexOf('amp-inline')>-1) {
                            let newHref = '../../../../dist'+blockDest;
                            returnString = '<style amp-custom><inline src="'+newHref+'"/></style>'
                        } else if(blockDest && blockDest.indexOf('-base-inline')>-1) {
                            let newHref = '../../dist'+blockDest+'?__inline=true'
                            returnString = '<link rel="stylesheet" href="' + newHref + '">';
                        } else if(blockDest && blockDest.indexOf('-inline')>-1) {
                            let newHref = '../../../dist'+blockDest+'?__inline=true'
                            returnString = '<link rel="stylesheet" href="' + newHref + '">';
                        } else if (blockDest && blockDest.indexOf('/css/vendor-v2.css') > -1) {
                            returnString = '<script> loadCSS("' + lazyLoadCssCDNPath(blockDest) + '"); </script>';
                        } else if (blockDest && blockDest.indexOf('-lazyload.')>-1) {
                            var revName = (grunt.filerev.summary[config.dist+ (block && block.dest || '')]).substring((config.dist && config.dist.length) || 0)
                            if(revName) {
                              return '<script> loadCSS("' + lazyLoadCssCDNPath(revName) + '"); </script>';
                            }
                        } else if(blockDest) {
                            returnString = '<link rel="stylesheet" href="' + blockDest + '"/>';
                        }
                        return returnString;
                    },
                    xcss: function(block) {
                        var revName = (grunt.filerev.summary[config.dist+ (block && block.dest || '')]).substring((config.dist && config.dist.length) || 0)
                        if(revName){
                            // Current Asset Directory is 'dist' + website url
                            return '<link rel="stylesheet" href="${data.layoutData.prefixToUrl(\'' + revName + '\')}"/>';
                        }
                        return '<link rel="stylesheet" href="'+(block && block.dest || '')+'"/>'
                    }

                }
            }
        },

        cdnify: {
            dist: {
                options: {
                    html: {
                        'img[video-src]': 'video-src',
                        'video[data-poster]': 'data-poster',
                        'link[href]': 'href',
                        'meta[itemprop="image"]': 'content'
                    },
                    rewriter: function(url) {
                        if (url.indexOf('data:') === 0 || url.indexOf('http:') === 0 || url.indexOf('https:') === 0 || url.indexOf('//') === 0 || url.trim().length === 0) {
                            return url;
                        } else {
                            url = getCDNUrl(url);
                            return url;
                        }
                    }
                },
                files: [{
                    expand: true,
                    cwd: './',
                    src: ['<%= config.serverDist %>/templates/{,**/}*.marko', '<%= config.serverDist %>/modules/{,**/}*.marko', '<%= config.dist %>/styles/css/*.css', '<%= config.serverDist %>/tags/{,**/}*.marko'],
                    dest: './'
                }]
            }
        },

        wrap: {
            basic: {
                src: ['compiled/services/*.js', 'compiled/configs/*.js', "compiled/public/*.js"],
                dest: '',
                options: {
                    wrapper: ['define(function (require, exports, module) {\n', '\n});'],
                    debug: false,
                }
            }
        },


        copy: {
            dist: {
                files: [{
                    expand: true,
                    dot: true,
                    cwd: '<%= config.app %>',
                    dest: '<%= config.dist %>',
                    src: [
                        'OneSignalSDKWorker.js',
                        'OneSignalSDKUpdaterWorker.js',
                        '*.{ico,png,txt,json,xml,jpg}',
                        'images/{,**/}*.*',
                        'akamai/{,**/}*.*',
                        'videos/{,*/}*.*',
                        '{,*/}*.html',
                        'fonts/{,*/}*.*',
                        'styles/vendor/{,**/}*.css',
                        '.well-known/{,*/}*.*',
                        'mailer/{,**/}*',
                        'sitemap'
                    ]
                }]
            },
            tmpDev: {
                files: [{
                    expand: true,
                    dot: true,
                    cwd: '<%= config.app %>',
                    dest: '<%= config.tmp %>',
                    src: [
                        'sw.js',
                        'sw-offline-google-analytics.js',
                        'modules/{,**/}*.*',
                        'scripts/{,**/}*.*',
                        // 'bower_components/{,**/}*.*'
                    ]
                }]
            },
            tmpProd: {
                files: [{
                    expand: true,
                    dot: true,
                    cwd: '<%= config.app %>',
                    dest: '<%= config.tmp %>',
                    src: [
                        'sw.js',
                        'sw-offline-google-analytics.js',
                        'modules/{,**/}*.js',
                        'modules/{,**/}*.svg',
                        'scripts/{,**/}*.*',
                        'config/{,**/}*.json',
                        'bower_components/{,**/}*.*',
                        'styles/vendor/{,**/}*.*'
                    ]
                }]
            },
            tmpProdStyle: {
                files: [{
                    expand: true,
                    dot: true,
                    cwd: '<%= config.app %>',
                    dest: '<%= config.tmp %>',
                    src: [
                        'styles/css/{,**/}*.css'
                    ]
                }]
            },
            server: {
                files: [{
                    expand: true,
                    dot: true,
                    cwd: '<%= config.server %>',
                    dest: '<%= config.serverDist %>',
                    src: ['**', '!public/**', '!{,**/}*.marko.js', 'public/scripts/common/*.js']
                }]
            },
            tests: {
                files: [{
                    expand: true,
                    dot: true,
                    cwd: 'tests',
                    dest: '<%= config.serverDist %>/tests',
                    src: ['**']
                }]
            }
        },
        watch: {
            sass: {
                files: [
                    '<%= config.app %>/styles/{,**/}*.scss'
                ],
                tasks: ['clean:css', 'sass:server', 'postcss:server']
            },
            js: {
                options: {
                    spawn: false
                },
                files: [
                    '<%= config.app %>/modules/{,**/}*.js',
                    '<%= config.app %>/modules/{,**/}*.html',
                    '<%= config.app %>/scripts/{,**/}*.js',
                    '!<%= config.app %>/scripts/vendor/{,**/}*.js'
                ],
                tasks: ['clean:client', 'copy:tmpDev', 'babel:client']
            }
        },
        concurrent: {
            copy: [
                'copy:tmpProd',
                'copy:server',
                'sass:server'
            ],
            compile: [
                'babel',
                'postcss:server'
            ],
            tests: ['copy:tmpDev', 'copy:server', 'copy:tests']
        },
        inline: {
            options: {
                uglify: true,
                exts: ['marko']
            },
            dist: {
                src: '<%= config.serverDist %>/templates/base.marko'
            },
            markoModules: {
                src: '<%= config.serverDist %>/modules/{,**/}*.marko'
            },
            dist_bw: {
                src: '<%= config.serverDist %>/templates/bw/base.marko'
            },
            amp:{
                src: '<%= config.serverDist %>/amp/modules/{,**/}*.marko'
            },
            components:{
                  options:{
                    cssmin: true
                  },
                  files: [{
                      expand: true,
                      dot: true,
                      cwd: '<%= config.app %>',
                      dest: '<%= config.tmp %>',
                      src: [
                          'modules/{,**/}*.html'
                      ]
                  }]
                }

        },
        shell: {
            options: {
                stderr: false
            },
            sitemap: {
                options: {
                    execOptions: {
                        cwd: config.dist
                    }
                },
                command: 'ln -s ../../../sitemap/ sitemap'
            },
            sitemapDelete: {
                command: 'rm -f dist/sitemap'
            },
            unitClient: {
                command: 'node_modules/.bin/intern-client config=tests/config-client'
            },
            unitServer: {
                command: 'node_modules/.bin/intern-client config=tests/config-server'
            },
            //todo: replace username with actual names
            dockerBuild: {
                options: {
                    execOptions: {
                        maxBuffer: 1024 * 1024 * 64,
                    },
                },
                command: 'git_last_commit=`git rev-parse --short HEAD` && docker build --build-arg GIT_COMMIT=$git_last_commit -t proptiger/buyer .'
            }
        }
    });


    grunt.registerTask('requirejsConfig', function() {
        /***
         ** Description: Building Module & submodule with require js tasks
         ** Start **
         ***/


		var dependencyMap = grunt.file.readJSON(config.tmp + '/scripts/bundles/dependency.json');
		var otherModulesPath = [];
		var excludeOtherModules = [];
		var matches = [].concat(grunt.file.expand(config.tmp + '/scripts/pageModules/*.js'));
		// updatePath task updates the revisioned names of the module hence variable is global
		otherModules= [].concat((grunt.file.expand(config.tmp + '/modules/*')).map(function(e){return e.split('/').pop()}));
		for (var x = 0; x < otherModules.length; x++) {
			otherModulesPath = otherModulesPath.concat(grunt.file.expand(config.tmp + '/modules/' + otherModules[x] + '/scripts/index.js')).concat(grunt.file.expand(config.tmp + '/modules/' + otherModules[x] + '/submodules/**.js'));
		}
        function createRequireModulesConfig(matches, excludeOtherModules) {
            var modules = [];
            if (matches.length > 0) {
                for (var x = 0; x < matches.length; x++) {
                    matches[x] = matches[x].replace(new RegExp(config.tmp + '/'), '');
                	if(dependencyMap[(matches[x].split('/')[1])] == 'commonBundle'){
                		continue;
                	}
                    var path = matches[x].substring(0, matches[x].lastIndexOf('/')),
                        filename = matches[x].substring(matches[x].lastIndexOf('/')).split('.js')[0],
                        excludeModule = path.indexOf('submodules') !== -1 ? path.split('submodules')[0] : '',
                        exclude = ['app', 'infra','scripts/dependency/commonBundle'].concat(excludeOtherModules);

                    excludeModule = excludeModule ? excludeModule + 'scripts/index' : '';
                    excludeModule ? exclude.push(excludeModule) : void 0;

                    modules.push({
                        name: path + filename,
                        exclude: exclude
                    });
                }
            }
            return modules;
        }
        // infra - collection of dependencies, inlined eventually
        // app - the initiator, contains basic blocks needed across website, after document.complete
        // dependency/* - manual bundles of most used modules or modules which need each other.
        var modules = [{
                name: 'infra'
            }, {
                name: 'app',
                exclude: ['infra']
            },{
              name:'scripts/dependency/commonBundle',
              exclude:['app','infra']
            },{
              name:'scripts/dependency/commonBundleAfterLoad',
              exclude:['app','infra','scripts/dependency/commonBundle']
            },{
              name:'scripts/dependency/homeLoanBundle',
              exclude:['app','infra','scripts/dependency/commonBundle']
            },{
              name:'scripts/dependency/homeLoanDashboard',
              exclude:['app','infra','scripts/dependency/commonBundle']
            },{
              name:'scripts/dependency/sellerBundle',
              exclude:['app','infra','scripts/dependency/commonBundle']
            },{
              name:'scripts/dependency/serpBundle',
              exclude:['app','infra','scripts/dependency/commonBundle']
            },{
              name:'scripts/dependency/loginBundle',
              exclude:['app','infra','scripts/dependency/commonBundle']
            }
            /*, {
                        name: 'sw-offline-google-analytics'
                    }*/
            /*, {
                        name: 'sw'
                    }*/
        ];

        requirejsOptions = {};

        modules = modules.concat(createRequireModulesConfig(matches,[]));
        modules = modules.concat(createRequireModulesConfig(otherModulesPath,[]));

        // TODO: FAIL THE BUILD IF SOME INCONSITENCIES FOUND IN BUNDLES
        requirejsOptions['compile'] = {
            options: {
                baseUrl: "./",
                appDir: config.tmp,
                optimize: 'uglify2',
                mainConfigFile: config.tmp + '/scripts/main.js',
                dir: config.dist,
                findNestedDependencies: true,
                skipDirOptimize: true,
                modules: modules,
                exclude: ['app', 'infra'],
                done: function(done, output) {
                        var duplicates = require('rjs-build-analysis').duplicates(output);
                        grunt.verbose.ok(duplicates);
                        var dupOutput = [];
                        grunt.log.ok('Checking for Duplicates in RequireJS build: ');
                        Object.keys(duplicates).forEach(function(dupKey){
                        	dupOutput = [];
                        	duplicates[dupKey] && (duplicates[dupKey]).forEach(function(dupItem){
                        		var bundled = ((dupItem.indexOf('modules/')>-1) && ((duplicates[dupKey]).indexOf('scripts/dependency/'+(dependencyMap[(dupItem.split('/')[1])])+'.js')>-1));
                        		if( dupItem == dupKey || dupItem.indexOf('dependency')>-1 || dupItem.indexOf('pageModules')>-1 || dupKey.indexOf('doT!')>-1 || dupKey.indexOf('text!')>-1 || dupKey.indexOf('bower_')>-1 || bundled){
                        			// IGNORE
                    			} else {
                    				dupOutput = dupOutput.concat([dupItem]);
                    			}
                    		});
                    		if (dupOutput.length>1){
                    			grunt.log.subhead("Duplicity for - ",dupKey);
                                dupOutput.forEach(function(d){return grunt.log.warn(d);});
                    		}
                        });
                        return done();
                      }
            }
        };

        grunt.config('requirejs', requirejsOptions);
        /*****  end *****/
    });

    grunt.registerTask('updateLazyloadCssPath', function() {
        var targetFile, file, fileUpdatedName,
            src, content;

        targetFile = grunt.file.expand(config.dist + '/styles/css/vendor-v2.*');
        if(targetFile[0]){
            file = targetFile[0].split('/');
            fileUpdatedName = file[file.length - 1];

            /** start: update base.marko file to update vendor-v2.css version no
             **/
            src = config.serverDist + '/templates/base.marko';
            content = grunt.file.read(src);
            content = content.replace(new RegExp('vendor-v2.css', 'g'), fileUpdatedName);
            grunt.file.write(src, content);

            src = config.serverDist + '/templates/bw/base.marko';
            content = grunt.file.read(src);
            content = content.replace(new RegExp('vendor-bw-v2.css', 'g'), fileUpdatedName);
            grunt.file.write(src, content);
            /** end: update base.marko file to update vendor-v2.css version no
             **/
        }
    });

    grunt.registerTask('updatePath', function() {
        var path = config.dist + '/scripts/',
            targetFilename = 'main', //'main',
            targetFile = grunt.file.expand(path + targetFilename + '.*'),
            content = grunt.file.read(targetFile[0]),
            version, filename;

        var i = null,
            j = null,
            temp = null,
            isAppPageModule,
            pathKey,
            modules = [],
            scripts = grunt.file.expand(config.dist + '/scripts/pageModules/*.js').concat(grunt.file.expand(config.dist + '/scripts/dependency/*.js')).concat(grunt.file.expand(config.dist + '/scripts/infra*.js'));
        // modules = modules.concat(grunt.file.expand(config.dist + '/modules/**/submodules/*.js'));
        var startFrom = content.indexOf('require.config');
        var startingIndex = content.indexOf('(', startFrom) + 1,
            endIndex = content.indexOf(')', startFrom),
            obj = JSON.parse(content.slice(startingIndex, endIndex));

        obj.baseUrl = CDN_MAP.CDN_JS.path + (obj.baseUrl.indexOf('/') === 0 ? obj.baseUrl.substring(1) : obj.baseUrl);

        for (i = 0; i < otherModules.length; i++) {
            modules = modules.concat(grunt.file.expand(config.dist + '/modules/' + otherModules[i] + '/scripts/*.js')).concat(grunt.file.expand(config.dist + '/modules/' + otherModules[i] + '/submodules/*.js'))
        }

        // adding modules path to require js config file
        for (i = 0; i < modules.length; i++) {
            temp = modules[i].split(config.dist + '/')[1].split('.');
            obj.paths[temp[0]] = temp[1] !== 'js' ? temp[0] + '.' + temp[1] : temp[0];
        }

        // adding other scripts path to require js config file
        for (i = 0; i < scripts.length; i++) {
            isAppPageModule = false;
            for (j = 0; j < appPageModules.length; j++) {
                if (scripts[i] && scripts[i].indexOf(appPageModules[j]) > -1) {
                    isAppPageModule = true;
                }
            }

            if (!isAppPageModule) {
                temp = scripts[i].split(config.dist + '/')[1].split('.');
                pathKey = temp && temp[0].indexOf('/infra') > -1 ? 'infra' : temp[0];
                if(pathKey=== "app" || pathKey==="infra" || pathKey === "scripts/dependency/commonBundle"){
                  obj.paths[pathKey] = temp[1] !== 'js' ? process.env.WEBSITE_URL+temp[0] + '.' + temp[1] : temp[0];
                }
                else{
                  obj.paths[pathKey] = temp[1] !== 'js' ? temp[0] + '.' + temp[1] : temp[0];
                }
            }
        }

        content = content.slice(0, startingIndex) + JSON.stringify(obj) + content.slice(endIndex);
        grunt.file.write(targetFile[0], content);

    });

    grunt.registerTask('updatePathInSW', function() {
        var path = config.dist + '/',
            targetFilename = 'sw',
            targetFile = grunt.file.expand(path + targetFilename + '.*'),
            content = grunt.file.read(targetFile[0]),
            version, filename;


        var i, temp, tempArray, file, f, src, tgt, swFileUpdatedName;

        //start: reading sw file name updated name
        file = targetFile[0];
        file = file.split('/');
        swFileUpdatedName = file[file.length - 1];
        // end: reading sw file name updated name

        var startFrom = content.indexOf('precacheConfigArray');
        var startingIndex = content.indexOf('[', startFrom) + 1,
            endIndex = content.indexOf(']', startFrom);

        var preCacheArray = JSON.parse(content.slice(startingIndex - 1, endIndex + 1));

        /** start: update origin for preCacheArray resources
         **/
        for (i = 0; i < preCacheArray.length; i++) {

            var lastIndexForExtension, extension, lastIndexForSlash, fileParentPath;

            lastIndexForExtension = preCacheArray[i].lastIndexOf('.');
            if (lastIndexForExtension > -1) {
                lastIndexForSlash = preCacheArray[i].lastIndexOf('/');
                fileParentPath = preCacheArray[i].substr(0, lastIndexForSlash + 1);
                extension = preCacheArray[i].substr(lastIndexForExtension + 1);
                path = preCacheArray[i].substr(0, lastIndexForExtension);
                file = grunt.file.expand('./dist' + path + '.*' + extension)[0];
                file = file.split('/');
                f = file[file.length - 1];
                file = f.split('.');
                src = new RegExp(fileParentPath + file[0] + '.' + extension, "g");
                tgt = getCDNUrl(fileParentPath + file[0] + '.' + file[1] + '.' + extension);
                content = content.replace(src, tgt);
            }
        }
        /** end: update origin for preCacheArray resources in router
         **/

        /** start: update origin for js/css/img resources in router
         **/

        content = content.replace('CDN_JS_PATH', getCDNUrl('.js', true));
        content = content.replace('CDN_CSS_PATH', getCDNUrl('.css', true));
        content = content.replace('CDN_JARVIS_PATH', getJarvisUrl());

        /** end: update origin for js/css resources in router
         **/
        grunt.file.write('./dist/' + swFileUpdatedName, content);


        /** start: update base.marko file with sw version no
         **/
        src = config.dist + '/OneSignalSDKWorker.js';
        content = grunt.file.read(src);
        content = content.replace(new RegExp('((sw.((.)+))|sw).js', 'g'), swFileUpdatedName);
        grunt.file.write(src, content);

        src = config.dist + '/OneSignalSDKUpdaterWorker.js';
        content = grunt.file.read(src);
        content = content.replace(new RegExp('((sw.((.)+))|sw).js', 'g'), swFileUpdatedName);
        grunt.file.write(src, content);
        /** end: update base.marko file with sw version no
         **/

    });

    grunt.registerTask('updateVersionFont', function() {
        var allFontsMarko = [{
            file: 'base.marko',
            path: config.serverDist + '/templates/'
        }, {
            file: 'view.marko',
            path: config.serverDist + '/modules/alliances/'
        }, {
            file: 'base.marko',
            path: config.serverDist + '/templates/bw/'
        }]
        for (var j = 0; j < allFontsMarko.length; j++) {
            var path = allFontsMarko[j].path,
                targetFilename = allFontsMarko[j].file,
                targetFile = grunt.file.expand(path + targetFilename),
                content = grunt.file.read(targetFile[0]);

            var files = grunt.file.expand(config.dist + '/fonts/*');
            for (var i = 0; i < files.length; i++) {
                var file = files[i];
                file = file.split('/');
                var f = file[file.length - 1];
                file = f.split('.');
                var src = new RegExp("\\b" + file[0] + '.' + file[2] + "\\b", "g");
                var tgt = file.join('.');
                content = content.replace(src, tgt);
            }

            grunt.file.write(path + targetFilename, content);
        }

    });
    grunt.registerTask('setupDocker', function() {
        var cmd = require('node-cmd');
        cmd.run(
            `
                mkdir docker
                cp package.json docker
                cp bower.json docker
                cp newrelic.js docker
                //cp .env docker
                cp -r bin docker
            `);
        if (grunt.option('target') === 'development') {
            console.log('copying complete source folder');
            cmd.run(
                `
                cp -r src docker/src
                rm -r docker/src/public/bower_components
            `);
        } else {
            console.log('copying only compiled and dist folders');
            cmd.run(
                `
                mkdir docker/compiled
                mkdir docker/dist
                cp -r compiled docker
                cp -r dist docker
                rm -r docker/dist/bower_components
            `);
        }

    });

    grunt.registerTask('updateSW', [
        'updatePathInSW',
        'minified',
    ]);

    grunt.registerTask('test', [
        'clean:client',
        'concurrent:tests',
        'babel',
        'wrap',
        'shell:unitClient',
        'shell:unitServer'
    ]);

    grunt.registerTask('s3assets', [
        's3:prod'
    ]);

    grunt.registerTask('build', [
        'shell:sitemapDelete',
        'clean:dist',
        'concurrent:copy',
        'concurrent:compile',
        'copy:tmpProdStyle',
        'inline:components',
        'requirejsConfig',
        'requirejs',
        'copy:dist',
        'useminPrepare',
        'concat',
        'cssmin',
        'filerev:dist',
        'usemin',
        'filerev:otherJS',
        'usemin',
        'updatePath',
        'filerev:mainJS',
        'usemin',
        'compress',
        'updateLazyloadCssPath',
        'updateSW',
        'updateVersionFont',
        'inline:dist',
        'inlineCSS',
        'inline:dist_bw',
        'cdnify',
        'shell:sitemap',
        's3assets'
    ]);

    grunt.registerTask('inlineCSS', [
      'inline:markoModules',
      'inline:amp'
    ]);

    grunt.registerTask('inline-task', [
        'inline:dist',
        'inline:serpMobile'
    ]);

    grunt.registerTask('dockerBuild', [
        'setupDocker'
    ]);

    grunt.registerTask('default', [
        'clean:dist',
        'concurrent:distCompile'
    ]);

    grunt.registerTask('servejs', [
        'clean:client',
        'copy:tmpDev',
        'babel:client',
        'watch'
    ]);

    grunt.registerTask('localServe', [
        'clean:client',
        'copy:tmpDev',
        'babel:client',
        'clean:css',
        'sass:server',
        'postcss:server'
    ]);

    grunt.registerTask('serve', [
        'localServe',
        'watch'
    ]);
};
