# property-exchange

makaan.com is web platform for real estate marketplace. On this platform users who are looking to buy/rent a house can connect to various agents/builders/house owners. They can also get connected with various alliances of Makaan.com

## architecture and technologies used

This project uses a balanced approach of thick client/server layers. First view for the pages are rendered at the server side. 
User interactions are handled at client side. Technologies used for the project are:

* express.js as nodejs web framework
* markojs for server template rendering
* Box/T3.js to establish client side mvc architecture
* doT.js for client side template rendering
* require.js for dependency resolution at client side

## project setup

* Install latest version of node (use NVM or N for better handling)
* Take a clone of the repo (git clone https://github.com/proptigercom/Property-exchange.git)
* Once git clone done, go inside property-exchange directory created via git repo
* to install all dependencies run below commands:
```sh
  $ npm install -g bower
  $ npm install
  $ bower install #if you run with permission errors see this page https://github.com/bower/bower/issues/2262
 ```
* To set configuration data for backend systems, rename "sample.env" to ".env" file. Make sure never to commit this .evm file as it contains environemnt specific configurations.
* Once dependencies are installed, use either dev or production mode for build: 
  * dev build needs below commands to be running in 2 terminal tabs
    ```sh 
    $ grunt serve
    $ npm start or $ nodemon
    ```
  * for production build 
  ```sh
    $ grunt build 
    $ NODE_ENV=production node bin/www
  ```
* If all goes good, you will see some usefull information on the terminal inlcuding Environment, directory and port number. If you see this information that means application is up and running. 
* to access application ie. see web pages, open browser with link http://localhost:'portnumber'/ (replace portnumber as displayed in above step)

## Docker based deployment


### Building Image through grunt

* Make sure node, npm, bower, grunt, grunt-cli are installed and updated.

* Run below commands by replace variables $GIT_COMMIT (lastcommit hash),$DocerRepo (docker repo for eg proptiger) and $BRANCH (branch which we want to build )

```sh
$ grunt build --force
$ grunt dockerBuild
$ docker build --build-arg GIT_COMMIT=$GIT_COMMIT -t $DockerRepo/buyer:$BRANCH .
$ e.g. docker build --build-arg GIT_COMMIT=b23984 -t proptiger/buyer:master .
```
* Push image to docker hub
** for login 
```sh
$ docker login
```
* push in docker hub
```sh
docker push $DockerRepo/buyer:$BRANCH 
```

### Run from Docker Image

Buyer code base requires .env file to run properly. It contains all dependent applications end points and other configuration info. See sample.env in the repo for reference. Create this files in your local file system, and run following command, with appropriate path of .env replaced


```sh
$ docker run --rm -v <path-to-.env>/.env:/usr/src/app/.env  <imagename>
$ e.g. docker run --rm -v .env/.env:/usr/src/app/.env  proptiger/buyer:latest
```

Docker run flag "--rm" caused container to be destroyed once it is stopped, remove this flag if you don't want such behavior.

By default container runs in foreground, to run in daemon mode use "-d" flag
```sh
$ e.g. docker run -d --name <some-name> -v <path-to-.env>/.env:/usr/src/app/.env -P5006:5006 <imagename>
```

## Run the unit tests for an app.
grunt test



