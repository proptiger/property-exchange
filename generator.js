"use strict";
var fs = require('fs');
var mkdirp = require('mkdirp');
var commandLineArgs = require('command-line-args');
var Handlebars = require('handlebars');

var commands = commandLineArgs([{
    name: 'name',
    alias: 'n',
    type: String
}, {
    name: 'generate',
    alias: 'g',
    type: String
}, {
    name: 'includeService',
    alias: 's',
    type: Boolean
}, {
    name: 'includeBehavior',
    alias: 'b',
    type: Boolean
}, {
    name: 'includeView',
    alias: 'v',
    type: Boolean
}, {
    name: 'forModule',
    alias: 'm',
    type: Boolean
}, {
    name: 'testType',
    alias: 't',
    type: String
}, {
    name: 'client',
    type: Boolean
},
, {
    name: 'server',
    type: Boolean
}
]);

var options = commands.parse(),
    name,
    sampleFilesPaths = {
        controller: __dirname + '/.sample/controller.js',
        view: __dirname + '/.sample/template.marko',
        mock: __dirname + '/.sample/mock.js',
        pageModule: __dirname + '/.sample/pageModule.js',
        serverService: __dirname + '/.sample/serviceServer.js',
        module: __dirname + '/.sample/module.js',
        behavior: __dirname + '/.sample/moduleBehavior.js',
        clientService: __dirname + '/.sample/moduleService.js',
        component: __dirname + '/.sample/component.html',
        clientTemplate: __dirname + '/.sample/component.html',
        moduleService: __dirname + '/.sample/moduleService.js',
        moduleBehavior: __dirname + '/.sample/moduleBehavior.js',
        pageCss: __dirname + '/.sample/page.scss',
        test: __dirname + '/.sample/test.js'
    };

Handlebars.registerHelper('if', function(conditional, options) {
    if (conditional) {
        return options.fn(this);
    }
});
Handlebars.registerHelper('capitalize', capitalize);

function capitalize(word) {
    //return word.slice(0, 1).toUpperCase() + word.slice(1).toLowerCase();
    return word;
}

if (options.generate) {
    checkValid(options.generate);
    switch (options.generate) {
        case "route":
            generateRoute(options.includeService);
            break;
        case "module":
            generateModule(options.includeService, options.includeBehavior, options.includeView);
            break;
        case "behavior":
            generateBehavior(options.forModule);
            break;
        case "clientService":
            generateClientService(options.forModule);
            break;
        case "serverService":
            generateServerService();
            break;
        case "model":
            generateServerService();
            break;
        case "view":
            generateDotTemplate(options.forModule);
            break;
        case "test":
            generateTestInterface();
            break; 
    }
}

function getDestFilePaths() {
    return {
        controller: 'src/modules/' + name + '/controller.js',
        view: 'src/modules/' + name + '/view.marko',
        mock: 'src/mock/' + name + '.js',
        pageModule: 'src/public/scripts/pageModules/' + name + 'Page.js',
        serverService: 'src/services/' + name + 'Service.js',
        component: 'src/modules/' + name + '/components/component.marko',
        clientTemplate: 'src/public/modules/' + name + '/views/template.html',
        moduleService: 'src/public/modules/' + name + '/scripts/services/' + name + 'Service.js',
        moduleBehavior: 'src/public/modules/' + name + '/scripts/behaviors/' + name + 'Behavior.js',
        module: 'src/public/modules/' + name + '/scripts/index.js',
        behavior: 'src/public/scripts/behaviors/' + name + '.js',
        clientService: 'src/public/scripts/services/' + name + '.js',
        pageCss: 'src/public/styles/' + name + '.scss',
        test: 'tests/'+ options.testType + "/" + options.executionEnvironment  + "/" + name + ".js"
    };
}

function generateTestInterface() {
        var files = ['test'];
        options.testType = options.testType || "unit";
        options.executionEnvironment = options.server ? "server" : "client";
        var destFilePaths = getDestFilePaths(),
        data = {
            routeName: name,
            type: options.testType
        },
        allFilesDetails = generateFilesDescription(files, data);

    checkFileExist(files, destFilePaths);
    writeFiles(allFilesDetails);
}

function generateFilesDescription(files, data) {
    var allFilesDetails = [],
        i, destFilePaths = getDestFilePaths();
    for (i = 0; i < files.length; i++) {
        var temp = {},
            file = files[i];
        temp.tmpl = generateTemplate(sampleFilesPaths[file]);
        temp.tmplPath = destFilePaths[file];
        temp.data = data;
        temp.logText = file;
        allFilesDetails.push(temp);
    }
    return allFilesDetails;
}

function generateTemplate(filepath) {
    return Handlebars.compile(fs.readFileSync(filepath, 'utf8'));
}

function writeFiles(allFilesDetails) {
    var i;
    for (i = 0; i < allFilesDetails.length; i++) {
        var fileDetails = allFilesDetails[i];
        fs.writeFile(fileDetails.tmplPath, fileDetails.tmpl(fileDetails.data));
        if (i === 0) {
            console.log(capitalize(options.generate) + " Created");
        }
        console.log(fileDetails.logText, ':', fileDetails.tmplPath);
    }
}

function generateRoute(includeService) {
    var files = ['controller', 'view', 'mock', 'pageModule', 'component', 'pageCss'];
    if (includeService) {
        files.push('serverService');
    };
    var destFilePaths = getDestFilePaths(),
        allFilesDetails,
        data = {
            routeName: name
        };

    checkFileExist(files, destFilePaths);
    mkdirp.sync('src/modules/' + name + '/components');
    allFilesDetails = generateFilesDescription(files, data);
    writeFiles(allFilesDetails);
}

function generateModule(includeService, includeBehavior, includeView) {
    var files = ['module'];
    if (includeService) {
        files.push('moduleService');
        mkdirp.sync('src/public/modules/' + name + '/scripts/services');
    }
    if (includeBehavior) {
        files.push('moduleBehavior');
        mkdirp.sync('src/public/modules/' + name + '/scripts/behaviors');
    }
    if (includeView) {
        files.push('clientTemplate');
        mkdirp.sync('src/public/modules/' + name + '/views');
    }
    mkdirp.sync('src/public/modules/' + name + '/scripts');
    var destFilePaths = getDestFilePaths(),
        data = {
            moduleName: name,
            includeService: includeService,
            includeBehavior: includeBehavior,
            includeView: includeView
        },
        allFilesDetails = generateFilesDescription(files, data);

    checkFileExist(files, destFilePaths);
    allFilesDetails = generateFilesDescription(files, data);
    writeFiles(allFilesDetails);
}

function generateBehavior(forModule) {
    var files;
    if (forModule) {
        files = ['moduleBehavior'];
        mkdirp.sync('src/public/modules/' + name + '/scripts/behaviors');
    } else {
        files = ['behavior'];
    }
    var destFilePaths = getDestFilePaths(),
        data = {
            moduleName: name
        },
        allFilesDetails = generateFilesDescription(files, data);

    checkFileExist(files, destFilePaths);
    writeFiles(allFilesDetails);
}

function generateServerService() {
    var files = ['serverService'],
        destFilePaths = getDestFilePaths(),
        data = {
            routeName: name
        },
        allFilesDetails = generateFilesDescription(files, data);

    checkFileExist(files, destFilePaths);
    writeFiles(allFilesDetails);
}

function generateClientService(forModule) {
    var files;
    if (forModule) {
        files = ['moduleService'];
        mkdirp.sync('src/public/modules/' + name + '/scripts/services');
    } else {
        files = ['clientService'];
    }
    var destFilePaths = getDestFilePaths(),
        data = {
            moduleName: name
        },
        allFilesDetails = generateFilesDescription(files, data);

    checkFileExist(files, destFilePaths);
    writeFiles(allFilesDetails);
}

function generateDotTemplate(forModule) {
    var files;
    if (forModule) {
        files = ['clientTemplate'];
        mkdirp.sync('src/public/modules/' + name + '/views');
        var destFilePaths = getDestFilePaths(),
            data = {
                moduleName: name
            },
            allFilesDetails = generateFilesDescription(files, data);

        checkFileExist(files, destFilePaths);
        writeFiles(allFilesDetails);
    }else{
        console.log('Do you want to generate view for a module?? If yes use -m flag');
    }
}

function checkFileExist(files, fileMap) {
    var stats, i;

    for (i = 0; i < files.length; i++) {
        try {
            stats = fs.lstatSync(fileMap[files[i]]);
            if (stats.isFile()) {
                console.log('File name exists(' + fileMap[files[i]] + '). Try different name');
                process.exit();
            }
        } catch (err) {
            continue;
        }
    }
    return true;
}

function checkValid(option) {
    switch (option) {
        case "route":
        case "module":
        case "behavior":
        case "serverService":
        case "clientService":
        case "view":
        case "test":
            if (options.name.match(/^[a-zA-Z]+$/)) {
                name = options.name;
            } else {
                console.log('Pass route name in camelcase format');
                process.exit(1);
            }
            break;
    }

}
