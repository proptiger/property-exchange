FROM node:argon

# Correct node version
RUN npm install -g n && \
        n 5.6.0 && \
        npm i -g pm2 && \
        npm install -g bower


# Create app directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

# Install node dependencies
COPY docker/package.json /usr/src/app/
RUN npm install

# Install bower dependencies
COPY docker/bower.json /usr/src/app/
RUN bower install --allow-root

# Bundle app source
COPY docker /usr/src/app

ARG GIT_COMMIT
LABEL com.proptiger.git.commit.id=$GIT_COMMIT

EXPOSE 3000
ENV NODE_ENV production
CMD ["node", "bin/www","--no-daemon"]
