"use strict";
define([
    'services/commonService',
], () => {
    Box.Application.addModule("{{routeName}}Page", (context) => {

        const CommonService = context.getService('CommonService'),
            logger = context.getService('Logger');
        var messages = [],
            behaviors = [];

        function init() {
            //initialization of all local variables
        }

        function destroy() {
            //clear all the binding and objects
        }

        function onmessage(name, data) {
            // bind custom messages/events
        }

        function onclick(event, element, elementType) {
            // bind custom messages/events
        }

        return {
            init,
            messages,
            behaviors,
            onmessage,
            onclick,
            destroy
        };
    });
});