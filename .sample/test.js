"use strict";
/*
*  Test suite containing all {{type}} test cases of {{routeName}}
*  Auto generated from generator 1.0.0 from option node generator -n {{routeName}} -g test -t {{type}} --{{client|server}}
*/

define([],function() {
    var registerSuite = require('intern!object');
    var assert        = require('intern/chai!assert');
    var expect        = require('intern/chai!expect');
    var sinon         = require('sinon');

    registerSuite({

        name: "{{routeName}} {{type}} tests",

        setup: function() {
            // executes before suite starts
        },
        beforeEach: function() {
            // executes before each test
        },
        afterEach: function() {
            // executes after each test
        },
        teardown: function() {
            // executes after suite ends
        },
        'test A': function() {
            // a test case
        }
    });
});
