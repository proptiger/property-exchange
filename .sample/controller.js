"use strict";
const routes = require('configs/routeConfig'),
    logger = require('services/loggerService'),
    template = require('./view.marko');

module.exports.setup(router) {
    router.get(routes.staticRoutes.{{routeName}}.url, routeHandler);
};

function routeHandler(req, res) {
    var parsedData = {};
    template.render(parsedData, res);
};
