"use strict";
define([
    {{#if includeView}}
    'doT!modules/{{moduleName}}/views/template',
    {{/if}}
    'services/loggerService',
    {{#if includeService}}
    'modules/{{moduleName}}/scripts/services/{{moduleName}}Service',
    {{/if}}
    {{#if includeBehavior}}
    'modules/{{moduleName}}/scripts/behaviors/{{moduleName}}Behavior'
    {{/if}}
], ({{#if includeView}}template{{/if}}) => {
    Box.Application.addModule('{{moduleName}}', (context) => {
        const Logger = context.getService('Logger'),
        {{#if includeService}}
            {{capitalize moduleName}}Service = context.getService('{{moduleName}}Service');
        {{/if}}
        let messages = [],
            behaviors = [];
        let element, moduleId, $, $moduleEl, moduleConfig;
        {{#if includeBehavior}}
        behaviors = ['{{moduleName}}Behavior'],
        {{/if}}

        function init() {
            //initialization of all local variables
            element = context.getElement();
            moduleId = element.id;
            $ = context.getGlobal('jQuery');
            moduleConfig = context.getConfig();
            $moduleEl = $(element);
        }

        function destroy() {
            //clear all the binding and objects
        }

        function onmessage(name, data) {
            // bind custom messages/events
        }

        function onclick(event, element, elementType) {
            // bind custom messages/events
        }

        return {
            init,
            messages,
            behaviors,
            onmessage,
            onclick,
            destroy
        };
    });
});