'use strict';
define(['services/loggerService'], () => {
    Box.Application.addBehavior("{{moduleName}}Behavior", (context) => {
        const Logger = application.getService('Logger');
        var messages = [];

        function init() {
            //initialization of all local variables
        }

        function destroy() {
            //clear all the binding and objects
        }

        function onmessage(name, data) {
            // bind custom messages/events
        }

        function onclick(event, element, elementType) {
            // bind custom messages/events
        }
        return {
            init,
            onclick,
            destroy,
            onmessage
        };
    });
});